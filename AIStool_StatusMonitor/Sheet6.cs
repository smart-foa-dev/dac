﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

namespace AIStool_StatusMonitor
{
    public partial class Sheet6
    {
        private void Sheet6_Startup(object sender, System.EventArgs e)
        {
            this.Visible = Excel.XlSheetVisibility.xlSheetHidden;
        }

        private void Sheet6_Shutdown(object sender, System.EventArgs e)
        {
        }

        #region VSTO デザイナーで生成されたコード

        /// <summary>
        /// デザイナーのサポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(Sheet6_Startup);
            this.Shutdown += new System.EventHandler(Sheet6_Shutdown);
        }

        #endregion

    }
}
