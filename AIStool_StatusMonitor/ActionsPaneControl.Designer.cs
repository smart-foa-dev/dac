﻿namespace AIStool_StatusMonitor
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class ActionsPaneControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ActionsPaneControl));
            this.btnWriting = new System.Windows.Forms.Button();
            this.cmbCalculate = new System.Windows.Forms.ComboBox();
            this.lblFormula = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblAssitant = new System.Windows.Forms.Label();
            this.DACHelp_picBox = new System.Windows.Forms.PictureBox();
            this.btnNavi = new System.Windows.Forms.Button();
            this.helpProvider1 = new System.Windows.Forms.HelpProvider();
            this.lbMission = new System.Windows.Forms.ListBox();
            this.lbCtm = new System.Windows.Forms.ListBox();
            this.lbElement = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAssistant = new System.Windows.Forms.TextBox();
            this.btnAssistant = new System.Windows.Forms.Button();
            this.txtFormula = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.DACHelp_picBox)).BeginInit();
            this.SuspendLayout();
            // 
            // btnWriting
            // 
            this.btnWriting.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnWriting.Location = new System.Drawing.Point(358, 50);
            this.btnWriting.Name = "btnWriting";
            this.btnWriting.Size = new System.Drawing.Size(112, 35);
            this.btnWriting.TabIndex = 0;
            this.btnWriting.Text = "書込";
            this.btnWriting.UseVisualStyleBackColor = true;
            this.btnWriting.Click += new System.EventHandler(this.btnWriting_Click);
            // 
            // cmbCalculate
            // 
            this.cmbCalculate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCalculate.Location = new System.Drawing.Point(112, 309);
            this.cmbCalculate.Name = "cmbCalculate";
            this.cmbCalculate.Size = new System.Drawing.Size(203, 20);
            this.cmbCalculate.TabIndex = 73;
            // 
            // lblFormula
            // 
            this.lblFormula.AutoSize = true;
            this.lblFormula.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.lblFormula.Location = new System.Drawing.Point(3, 58);
            this.lblFormula.Name = "lblFormula";
            this.lblFormula.Size = new System.Drawing.Size(93, 20);
            this.lblFormula.TabIndex = 5;
            this.lblFormula.Text = "項目演算";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.label3.Location = new System.Drawing.Point(3, 309);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "演算形式";
            // 
            // lblAssitant
            // 
            this.lblAssitant.AutoSize = true;
            this.lblAssitant.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.lblAssitant.Location = new System.Drawing.Point(54, 183);
            this.lblAssitant.Name = "lblAssitant";
            this.lblAssitant.Size = new System.Drawing.Size(72, 20);
            this.lblAssitant.TabIndex = 0;
            this.lblAssitant.Text = "補助式";
            // 
            // DACHelp_picBox
            // 
            this.DACHelp_picBox.Image = ((System.Drawing.Image)(resources.GetObject("DACHelp_picBox.Image")));
            this.DACHelp_picBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.DACHelp_picBox.Location = new System.Drawing.Point(446, 10);
            this.DACHelp_picBox.Name = "DACHelp_picBox";
            this.DACHelp_picBox.Size = new System.Drawing.Size(24, 24);
            this.DACHelp_picBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.DACHelp_picBox.TabIndex = 64;
            this.DACHelp_picBox.TabStop = false;
            this.DACHelp_picBox.Click += new System.EventHandler(this.DACHelp_picBox_Click);
            // 
            // btnNavi
            // 
            this.btnNavi.AutoSize = true;
            this.btnNavi.BackColor = System.Drawing.Color.DarkGoldenrod;
            this.btnNavi.Font = new System.Drawing.Font("メイリオ", 10F, System.Drawing.FontStyle.Bold);
            this.btnNavi.ForeColor = System.Drawing.Color.White;
            this.btnNavi.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnNavi.Location = new System.Drawing.Point(279, 3);
            this.btnNavi.Name = "btnNavi";
            this.btnNavi.Size = new System.Drawing.Size(57, 31);
            this.btnNavi.TabIndex = 65;
            this.btnNavi.Text = "NAVI";
            this.btnNavi.UseVisualStyleBackColor = false;
            this.btnNavi.Visible = false;
            this.btnNavi.Click += new System.EventHandler(this.btnNavi_Click);
            // 
            // helpProvider1
            // 
            this.helpProvider1.HelpNamespace = "E:\\smart-FOA\\AIS\\src\\AIS\\AIStool\\Resources\\AISTool-Help.chm";
            // 
            // lbMission
            // 
            this.lbMission.AllowDrop = true;
            this.lbMission.FormattingEnabled = true;
            this.lbMission.ItemHeight = 12;
            this.lbMission.Location = new System.Drawing.Point(3, 379);
            this.lbMission.Name = "lbMission";
            this.lbMission.Size = new System.Drawing.Size(153, 112);
            this.lbMission.TabIndex = 66;
            this.lbMission.SelectedIndexChanged += new System.EventHandler(this.lbMission_SelectedIndexChanged);
            // 
            // lbCtm
            // 
            this.lbCtm.FormattingEnabled = true;
            this.lbCtm.ItemHeight = 12;
            this.lbCtm.Location = new System.Drawing.Point(162, 379);
            this.lbCtm.Name = "lbCtm";
            this.lbCtm.Size = new System.Drawing.Size(153, 112);
            this.lbCtm.TabIndex = 67;
            this.lbCtm.SelectedIndexChanged += new System.EventHandler(this.lbCtm_SelectedIndexChanged);
            // 
            // lbElement
            // 
            this.lbElement.FormattingEnabled = true;
            this.lbElement.ItemHeight = 12;
            this.lbElement.Location = new System.Drawing.Point(321, 379);
            this.lbElement.Name = "lbElement";
            this.lbElement.Size = new System.Drawing.Size(153, 112);
            this.lbElement.TabIndex = 68;
            this.lbElement.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbElement_MouseDown);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(3, 343);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 20);
            this.label5.TabIndex = 69;
            this.label5.Text = "ミッション";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.label6.Location = new System.Drawing.Point(158, 343);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 20);
            this.label6.TabIndex = 69;
            this.label6.Text = "CTM";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(317, 343);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 20);
            this.label7.TabIndex = 69;
            this.label7.Text = "エレメント";
            // 
            // txtAssistant
            // 
            this.txtAssistant.AllowDrop = true;
            this.txtAssistant.Location = new System.Drawing.Point(3, 217);
            this.txtAssistant.Multiline = true;
            this.txtAssistant.Name = "txtAssistant";
            this.txtAssistant.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtAssistant.Size = new System.Drawing.Size(467, 72);
            this.txtAssistant.TabIndex = 71;
            this.txtAssistant.DragDrop += new System.Windows.Forms.DragEventHandler(this.textAssistant_DragDrop);
            this.txtAssistant.DragEnter += new System.Windows.Forms.DragEventHandler(this.textAssistant_DragEnter);
            this.txtAssistant.DragOver += new System.Windows.Forms.DragEventHandler(this.textAssistant_DragOver);
            this.txtAssistant.MouseLeave += new System.EventHandler(this.txtAssistant_MouseLeave);
            this.txtAssistant.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtAssistant_MouseMove);
            // 
            // btnAssistant
            // 
            this.btnAssistant.Font = new System.Drawing.Font("MS UI Gothic", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.btnAssistant.Location = new System.Drawing.Point(3, 176);
            this.btnAssistant.Name = "btnAssistant";
            this.btnAssistant.Size = new System.Drawing.Size(45, 35);
            this.btnAssistant.TabIndex = 0;
            this.btnAssistant.Text = "－";
            this.btnAssistant.UseVisualStyleBackColor = true;
            this.btnAssistant.Click += new System.EventHandler(this.btnAssistant_Click);
            // 
            // txtFormula
            // 
            this.txtFormula.AllowDrop = true;
            this.txtFormula.Location = new System.Drawing.Point(3, 98);
            this.txtFormula.Multiline = true;
            this.txtFormula.Name = "txtFormula";
            this.txtFormula.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtFormula.Size = new System.Drawing.Size(467, 72);
            this.txtFormula.TabIndex = 72;
            this.txtFormula.DragDrop += new System.Windows.Forms.DragEventHandler(this.txtFormula_DragDrop);
            this.txtFormula.DragEnter += new System.Windows.Forms.DragEventHandler(this.txtFormula_DragEnter);
            this.txtFormula.DragOver += new System.Windows.Forms.DragEventHandler(this.txtFormula_DragOver);
            this.txtFormula.MouseLeave += new System.EventHandler(this.txtFormula_MouseLeave);
            this.txtFormula.MouseMove += new System.Windows.Forms.MouseEventHandler(this.txtFormula_MouseMove);
            // 
            // ActionsPaneControl
            // 
            this.AutoSize = true;
            this.Controls.Add(this.txtFormula);
            this.Controls.Add(this.txtAssistant);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.lbElement);
            this.Controls.Add(this.lbCtm);
            this.Controls.Add(this.lbMission);
            this.Controls.Add(this.btnNavi);
            this.Controls.Add(this.DACHelp_picBox);
            this.Controls.Add(this.lblAssitant);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lblFormula);
            this.Controls.Add(this.cmbCalculate);
            this.Controls.Add(this.btnAssistant);
            this.Controls.Add(this.btnWriting);
            this.Name = "ActionsPaneControl";
            this.Size = new System.Drawing.Size(479, 494);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ActionsPaneControl_Paint);
            ((System.ComponentModel.ISupportInitialize)(this.DACHelp_picBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnWriting;
        private System.Windows.Forms.ComboBox cmbCalculate;
        private System.Windows.Forms.Label lblFormula;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblAssitant;
        private System.Windows.Forms.PictureBox DACHelp_picBox;
        private System.Windows.Forms.Button btnNavi;
        private System.Windows.Forms.HelpProvider helpProvider1;
        private System.Windows.Forms.ListBox lbMission;
        private System.Windows.Forms.ListBox lbCtm;
        private System.Windows.Forms.ListBox lbElement;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAssistant;
        private System.Windows.Forms.Button btnAssistant;
        private System.Windows.Forms.TextBox txtFormula;
    }
}
