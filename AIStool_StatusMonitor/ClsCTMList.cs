﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AIStool_StatusMonitor
{
    /// <summary>
    /// ミッション内のCTMを列挙するクラス
    /// </summary>
    [DataContract]
    public class ClsCTMList
    {
        /// <summary>
        /// CTMのID
        /// </summary>
        [DataMember]
        public string id { get; private set; }

        /// <summary>
        /// CTMの名前
        /// </summary>
        [DataMember]
        public List<CTMName> displayName { get; set; }

        /// <summary>
        /// 階層配下の内容
        /// </summary>
        [DataMember]
        public List<Children1> children { get; set; }

        /// <summary>
        /// CTMの名前を取得する
        /// </summary>
        public static ClsCTMList[] get()
        {
            string HostName = Globals.Sheet1.m_MissionHostName;
            string PortNo = Globals.Sheet1.m_MissionPortNo;
            WebClient wc = new WebClient();
            wc.Encoding = Encoding.UTF8;
            string url = String.Format("http://{0}:{1}/cms/rest/ctm/myDomain", HostName, PortNo);
            string ctmStr = wc.DownloadString(url);

            ClsCTMList[] ctmList = JsonConvert.DeserializeObject<ClsCTMList[]>(ctmStr);

            return convertCtmList(ctmList);
        }

        /// <summary>
        /// グループ階層化に対応するために
        /// グループなしにコンバートする
        /// </summary>
        /// <returns></returns>
        private static ClsCTMList[] convertCtmList(ClsCTMList[] source)
        {
            List<ClsCTMList> result = new List<ClsCTMList>();

            if (source != null)
            {
                foreach (ClsCTMList ctm in source)
                {
                    //変換用CTM情報
                    ClsCTMList ctmInfo = new ClsCTMList();
                    ctmInfo.children = new List<Children1>();
                    //変換用エレメント情報                            
                    Children1 children1Info = new Children1();
                    if (ctm.children == null)
                    {
                    }
                    else
                    {
                        for (int children1Index = 0; children1Index < ctm.children.Count; children1Index++)
                        {
                            Children1 children1 = ctm.children[children1Index];

                            //CTMの情報格納
                            if (children1Index == 0)
                            {
                                ctmInfo.id = ctm.id;
                                ctmInfo.displayName = ctm.displayName;

                            }

                            //エレメントの場合
                            if (children1.type == "el")
                            {
                                children1Info = new Children1();
                                children1Info.id = children1.id;
                                children1Info.displayName = children1.displayName;
                                children1Info.size = children1.size;
                                children1Info.type = children1.type;
                                children1Info.unit = children1.unit;
                                children1Info.datatype = children1.datatype;
                                //CTM直下にエレメント追加
                                ctmInfo.children.Add(children1Info);
                            }
                            else if (children1.children != null)
                            {
                                for (int children2Index = 0; children2Index < children1.children.Count; children2Index++)
                                {
                                    ClsCTMList.Children1.Children2 children2 = children1.children[children2Index];

                                    if (children2.type == "el")
                                    {
                                        children1Info = new Children1();
                                        children1Info.id = children2.id;
                                        children1Info.displayName = new List<Children1.Children1Name>();
                                        children1Info.displayName.Add(new Children1.Children1Name());
                                        children1Info.displayName[0].text = children2.displayName[0].text;
                                        children1Info.displayName[0].lang = children2.displayName[0].lang;
                                        children1Info.size = children2.size;
                                        children1Info.type = children2.type;
                                        children1Info.datatype = children2.datatype;
                                        if (children2.unit != null && children2.unit.Count > 0)
                                        {
                                            children1Info.unit = new List<Children1.Children1Unit>();
                                            children1Info.unit.Add(new Children1.Children1Unit());
                                            children1Info.unit[0].text = children2.unit[0].text;
                                            children1Info.unit[0].lang = children2.unit[0].lang;
                                        }
                                        //CTM直下にエレメント追加
                                        ctmInfo.children.Add(children1Info);
                                    }
                                    else if (children2.children != null)
                                    {
                                        foreach (ClsCTMList.Children1.Children2.Children3 children3 in children2.children)
                                        {
                                            //エレメントの場合
                                            if (children3.type == "el")
                                            {
                                                children1Info = new Children1();
                                                children1Info.id = children3.id;
                                                children1Info.displayName = new List<Children1.Children1Name>();
                                                children1Info.displayName.Add(new Children1.Children1Name());
                                                children1Info.displayName[0].text = children3.displayName[0].text;
                                                children1Info.displayName[0].lang = children3.displayName[0].lang;
                                                children1Info.size = children3.size;
                                                children1Info.type = children3.type;
                                                children1Info.datatype = children3.datatype;
                                                if (children3.unit != null && children3.unit.Count > 0)
                                                {
                                                    children1Info.unit = new List<Children1.Children1Unit>();
                                                    children1Info.unit.Add(new Children1.Children1Unit());
                                                    children1Info.unit[0].text = children3.unit[0].text;
                                                    children1Info.unit[0].lang = children3.unit[0].lang;
                                                }

                                                //CTM直下にエレメント追加
                                                ctmInfo.children.Add(children1Info);
                                            }
                                            else if (children3.children != null)
                                            {
                                                foreach (ClsCTMList.Children1.Children2.Children3.Children4 children4 in children3.children)
                                                {

                                                    //*仕様*グループ3階層以降はエレメントが無い okada-r 2015//11/30
                                                    //エレメントの場合
                                                    if (children4.type == "el")
                                                    {
                                                        children1Info = new Children1();
                                                        children1Info.id = children4.id;
                                                        children1Info.displayName = new List<Children1.Children1Name>();
                                                        children1Info.displayName.Add(new Children1.Children1Name());
                                                        children1Info.displayName[0].text = children4.displayName[0].text;
                                                        children1Info.displayName[0].lang = children4.displayName[0].lang;
                                                        children1Info.size = children4.size;
                                                        children1Info.type = children4.type;
                                                        children1Info.datatype = children4.datatype;
                                                        if (children4.unit != null && children4.unit.Count > 0)
                                                        {
                                                            children1Info.unit = new List<Children1.Children1Unit>();
                                                            children1Info.unit.Add(new Children1.Children1Unit());
                                                            children1Info.unit[0].text = children4.unit[0].text;
                                                            children1Info.unit[0].lang = children4.unit[0].lang;
                                                        }

                                                        //CTM直下にエレメント追加
                                                        ctmInfo.children.Add(children1Info);
                                                    }
                                                }


                                            }

                                        }

                                    }
                                }


                            }

                        }
                        //変換後のリスト作成の為、追加
                        result.Add(ctmInfo);
                    }

                }

            }

            return result.ToArray();
        }

        /// <summary>
        /// CTMの名前を格納するクラス
        /// </summary>
        [DataContract]
        public class CTMName
        {
            /// <summary>
            /// CTM名
            /// </summary>
            [DataMember]
            public string text { get; set; }

            /// <summary>
            /// CTMの言語
            /// </summary>
            [DataMember]
            public string lang { get; set; }
        }

        /// <summary>
        /// 配下の内容
        /// </summary>
        [DataContract]
        public class Children1
        {
            /// <summary>
            /// 階層のID
            /// </summary>
            [DataMember]
            public string id { get; set; }

            /// <summary>
            /// 階層の名前
            /// </summary>
            [DataMember]
            public List<Children1Name> displayName { get; set; }

            /// <summary>
            /// 階層の形式
            /// </summary>
            [DataMember]
            public string type { get; set; }

            /// <summary>
            /// エレメントの型
            /// </summary>
            [DataMember]
            public string datatype { get; set; }

            /// <summary>
            /// エレメントのサイズ
            /// </summary>
            [DataMember]
            public string size { get; set; }

            /// <summary>
            /// エレメントの単位
            /// </summary>
            [DataMember]
            public List<Children1Unit> unit { get; set; }

            /// <summary>
            /// 階層配下の内容
            /// </summary>
            [DataMember]
            public List<Children2> children { get; set; }

            [DataContract]
            public class Children1Name
            {
                /// <summary>
                /// 階層の名前
                /// </summary>
                [DataMember]
                public string text { get; set; }
                /// <summary>
                /// 階層の言語
                /// </summary>
                [DataMember]
                public string lang { get; set; }
            }

            [DataContract]
            public class Children1Unit
            {
                /// <summary>
                /// エレメントの単位
                /// </summary>
                [DataMember]
                public string text { get; set; }

                /// <summary>
                /// エレメントの言語
                /// </summary>
                [DataMember]
                public string lang { get; set; }
            }

            /// <summary>
            /// 配下の内容
            /// </summary>
            [DataContract]
            public class Children2
            {
                /// <summary>
                /// 階層のID
                /// </summary>
                [DataMember]
                public string id { get; set; }

                /// <summary>
                /// 階層の名前
                /// </summary>
                [DataMember]
                public List<Children2Name> displayName { get; set; }

                /// <summary>
                /// 階層の形式
                /// </summary>
                [DataMember]
                public string type { get; set; }

                /// <summary>
                /// エレメントの型
                /// </summary>
                [DataMember]
                public string datatype { get; set; }

                /// <summary>
                /// エレメントのサイズ
                /// </summary>
                [DataMember]
                public string size { get; set; }

                /// <summary>
                /// エレメントの単位
                /// </summary>
                [DataMember]
                public List<Children2Unit> unit { get; set; }

                /// <summary>
                /// 配下の内容
                /// </summary>
                [DataMember]
                public List<Children3> children { get; set; }

                [DataContract]
                public class Children2Name
                {
                    /// <summary>
                    /// 階層の名前
                    /// </summary>
                    [DataMember]
                    public string text { get; set; }

                    /// <summary>
                    /// 階層の言語
                    /// </summary>
                    [DataMember]
                    public string lang { get; set; }
                }

                [DataContract]
                public class Children2Unit
                {
                    /// <summary>
                    /// エレメントの単位
                    /// </summary>
                    [DataMember]
                    public string text { get; set; }

                    /// <summary>
                    /// エレメントの言語
                    /// </summary>
                    [DataMember]
                    public string lang { get; set; }
                }

                /// <summary>
                /// 配下の内容
                /// </summary>
                [DataContract]
                public class Children3
                {
                    /// <summary>
                    /// 階層のID
                    /// </summary>
                    [DataMember]
                    public string id { get; set; }

                    /// <summary>
                    /// 階層の名前
                    /// </summary>
                    [DataMember]
                    public List<Children3Name> displayName { get; set; }

                    /// <summary>
                    /// 階層の形式
                    /// </summary>
                    [DataMember]
                    public string type { get; set; }

                    /// <summary>
                    /// エレメントの型
                    /// </summary>
                    [DataMember]
                    public string datatype { get; set; }

                    /// <summary>
                    /// エレメントのサイズ
                    /// </summary>
                    [DataMember]
                    public string size { get; set; }

                    /// <summary>
                    /// エレメントの単位
                    /// </summary>
                    [DataMember]
                    public List<Children3Unit> unit { get; set; }

                    /// <summary>
                    /// 配下の内容
                    /// </summary>
                    [DataMember]
                    public List<Children4> children { get; set; }

                    [DataContract]
                    public class Children3Name
                    {
                        /// <summary>
                        /// 階層の名前
                        /// </summary>
                        [DataMember]
                        public string text { get; set; }

                        /// <summary>
                        /// 階層の言語
                        /// </summary>
                        [DataMember]
                        public string lang { get; set; }
                    }

                    [DataContract]
                    public class Children3Unit
                    {
                        /// <summary>
                        /// エレメントの単位
                        /// </summary>
                        [DataMember]
                        public string text { get; set; }

                        /// <summary>
                        /// エレメントの言語
                        /// </summary>
                        [DataMember]
                        public string lang { get; set; }
                    }

                    /// <summary>
                    /// 配下の内容
                    /// </summary>
                    [DataContract]
                    public class Children4
                    {
                        /// <summary>
                        /// 階層のID
                        /// </summary>
                        [DataMember]
                        public string id { get; set; }

                        /// <summary>
                        /// 階層の名前
                        /// </summary>
                        [DataMember]
                        public List<Children4Name> displayName { get; set; }

                        /// <summary>
                        /// 階層の形式
                        /// </summary>
                        [DataMember]
                        public string type { get; set; }

                        /// <summary>
                        /// エレメントの型
                        /// </summary>
                        [DataMember]
                        public string datatype { get; set; }

                        /// <summary>
                        /// エレメントのサイズ
                        /// </summary>
                        [DataMember]
                        public string size { get; set; }

                        /// <summary>
                        /// エレメントの単位
                        /// </summary>
                        [DataMember]
                        public List<Children4Unit> unit { get; set; }

                        /// <summary>
                        /// 配下の内容
                        /// </summary>
                        [DataMember]
                        public List<Children5> children { get; set; }

                        [DataContract]
                        public class Children4Name
                        {
                            /// <summary>
                            /// 階層の名前
                            /// </summary>
                            [DataMember]
                            public string text { get; set; }

                            /// <summary>
                            /// 階層の言語
                            /// </summary>
                            [DataMember]
                            public string lang { get; set; }
                        }

                        [DataContract]
                        public class Children4Unit
                        {
                            /// <summary>
                            /// エレメントの単位
                            /// </summary>
                            [DataMember]
                            public string text { get; set; }

                            /// <summary>
                            /// エレメントの言語
                            /// </summary>
                            [DataMember]
                            public string lang { get; set; }
                        }

                        /// <summary>
                        /// 配下の内容
                        /// </summary>
                        [DataContract]
                        public class Children5
                        {
                            /// <summary>
                            /// 階層のID
                            /// </summary>
                            [DataMember]
                            public string id { get; set; }

                            /// <summary>
                            /// 階層の名前
                            /// </summary>
                            [DataMember]
                            public List<Children5Name> displayName { get; set; }

                            /// <summary>
                            /// 階層の形式
                            /// </summary>
                            [DataMember]
                            public string type { get; set; }

                            [DataContract]
                            public class Children5Name
                            {
                                /// <summary>
                                /// 階層の名前
                                /// </summary>
                                [DataMember]
                                public string text { get; set; }

                                /// <summary>
                                /// 階層の言語
                                /// </summary>
                                [DataMember]
                                public string lang { get; set; }
                            }


                        }

                    }

                }

            }

        }


    }
}
