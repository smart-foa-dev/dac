﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Deployment.Application;

namespace AIStool_StatusMonitor.Help
{
    public partial class DACHelpInfo : Form
    {
        public DACHelpInfo()
        {
            InitializeComponent();
            ////自分自身のバージョン情報を取得する
            //System.Diagnostics.FileVersionInfo ver =
            //    System.Diagnostics.FileVersionInfo.GetVersionInfo(
            //    System.Reflection.Assembly.GetExecutingAssembly().Location);

            if (ApplicationDeployment.IsNetworkDeployed)
            {
                var version = ApplicationDeployment.CurrentDeployment.CurrentVersion;

                this.Text = string.Format("AIS_DAC({0}.{1}.{2}.{3})", version.Major, version.Minor, version.Build, version.Revision);
            }
        }
    }
}
