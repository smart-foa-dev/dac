﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace AIStool_StatusMonitor
{
    /// <summary>
    /// ログ出力クラス
    /// </summary>
    public class ClsWriteLog
    {

        /// <summary>
        /// ロック処理用
        /// </summary>
        private Object m_thisLock = new Object();

        /// <summary>
        /// ログ出力パス
        /// </summary>
        private string m_logFilePath = Path.GetFullPath(Globals.ThisWorkbook.Path);

        /// <summary>
        /// ログ格納フォルダ（クラス名）
        /// </summary>
        private string m_className = "DefaultClass";


        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ClsWriteLog(string className)
        {
            this.m_className = className;
        }


        /// <summary>
        /// ログ出力
        /// </summary>
        /// <param name="directoryName">クラス名</param>
        /// <param name="methodName">メソッド名</param>
        /// <param name="Event">内容</param>
        /// <param name="LogType">出力タイプ 0:Debug, 1:Release</param>
        /// <returns>0:正常終了 -1:異常終了</returns>
        public int OutPut(string methodName, object Event, int LogType = 1)
        {
            try
            {
                string StringItem = null;
                Exception VbExceptionItem = null;
                System.Text.StringBuilder MessageHeader = new System.Text.StringBuilder();
                System.Text.StringBuilder Messages = new System.Text.StringBuilder();
                string tempPath = null;

                DirectoryInfo DirInfo;
                DateTime OutputDate = DateTime.Now;
                
                m_logFilePath = Path.GetFullPath(Globals.ThisWorkbook.Path);


                //フォルダがない場合は作成する
                if (m_logFilePath == null)
                {
                    DirInfo = Directory.GetParent(Path.GetDirectoryName(Application.ExecutablePath));
                    tempPath = DirInfo.FullName;
                    //if (tempPath.EndsWith("/Local"))
                    if (tempPath.EndsWith("/ExcelResult"))
                    {
                        //tempPath = tempPath.Remove(tempPath.Length - 6);
                        tempPath = tempPath.Remove(tempPath.Length - 25);
                    }
                }
                else
                {
                    //if (Path.GetFileName(m_logFilePath) == "Local")
                    if (Path.GetFileName(m_logFilePath) == "ExcelResult")
                    {
                        //tempPath = m_logFilePath.Remove(m_logFilePath.Length - 6);
                        tempPath = m_logFilePath.Remove(m_logFilePath.Length - 25);
                    }
                    //else if (Path.GetFileName(m_logFilePath) == "Template")
                    else if (Path.GetFileName(m_logFilePath) == "StatusMonitor")
                    {
                        //tempPath = m_logFilePath.Remove(m_logFilePath.Length - 9);
                        tempPath = m_logFilePath.Remove(m_logFilePath.Length - 13);
                    }

                    else
                    {
                        tempPath = m_logFilePath;
                    }
                }

                if (tempPath == null)
                {
                    return -1;
                }

                tempPath += @"\Log";


                if (!System.IO.Directory.Exists(tempPath))
                {
                    System.IO.Directory.CreateDirectory(tempPath);
                }

                System.IO.StreamWriter textFile = null;
                string textPath = null;

#if DEBUG
                if (LogType >= 0)
                {
                    textPath = tempPath + "/" + this.m_className + "_" + OutputDate.ToString("yyyyMMdd") + ".log";
                }
#else
                if (LogType == 1)
                {
                    textPath = tempPath + "/" + OutputDate.ToString("yyyyMMdd") + ".log";
                }
#endif

#if DEBUG
                if (LogType >= 0)
                {
                    textFile = new System.IO.StreamWriter(textPath, true, System.Text.Encoding.Default);
                }
#else
                if (LogType == 1)
                {
                    textFile = new System.IO.StreamWriter(textPath, true, System.Text.Encoding.Default);
                }
#endif

                Messages.AppendLine("");
                MessageHeader.Append(OutputDate.ToString("yyyy/MM/dd HH:mm:ss.fff") + ", ");
                MessageHeader.Append(methodName + ", ");

                if ((Event == null))
                {

                }
                else
                {
                    //ExceptionObj = Stringの場合
                    if (Event is string)
                    {
                        StringItem = (string)Event;
                        //Stringにキャスト
                        Messages.Append(MessageHeader.Append(StringItem + "\n"));
                    }

                    //ExceptionObj = vbエラーの場合
                    if (Event is Exception)
                    {
                        VbExceptionItem = (Exception)Event;
                        //Exceptionにキャスト
                        MessageHeader.Append("Message:    " + VbExceptionItem.Message);
                        Messages.AppendLine(MessageHeader.ToString());
                        Messages.AppendLine("StackTrace: " + VbExceptionItem.StackTrace);

                        Messages.Append("----------------------------------------------------------------------------------");
                        Messages.Append("----------------------------------------------------------------------------------");
                        Messages.AppendLine("----------------------------------------------------------------------------------");

                    }

                }

#if DEBUG
                if (LogType >= 0)
                {
                    lock (m_thisLock)
                    {
                        textFile.WriteLine(Messages.ToString());
                        textFile.Close();
                    }
                }
#else
                if (LogType == 1)
                {
                    lock (m_thisLock)
                    {
                        textFile.WriteLine(Messages.ToString());
                        textFile.Close();
                    }
                }
#endif
            }
            catch (Exception ex)
            {
                ex = new Exception();
                return -1;
            }


            return 0;
        }

    }
}
