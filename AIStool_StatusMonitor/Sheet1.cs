﻿#define NT_MODIFY

using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualBasic;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using XlTool = Microsoft.Office.Tools.Excel;
using Office = Microsoft.Office.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AIStool_StatusMonitor
{
#if NT_MODIFY
    /// <summary>
    /// AISデータ加工用エクセルシート
    /// </summary>
    public partial class Sheet1
    {
        #region"メンバー"

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_menuButton;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_SampleButton;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_actionButton;

        /// <summary>
        /// 現在のアクティブセル
        /// </summary>
        private Excel.Range m_activeRange;

        /// <summary>
        /// ログ書き出し用クラス
        /// </summary>
        private ClsWriteLog m_writeLog;

        /// <summary>
        /// アプリケーション読み込みフラグ
        /// </summary>
        public bool flagLoading = false;

        /// <summary>
        /// ミッション投入用ホスト
        /// </summary>
        public string m_MissionHostName { get; set; }
        
        /// <summary>
        /// ミッション投入用ポート
        /// </summary>
        public string m_MissionPortNo { get; set; }

        /// <summary>
        /// 計算式入力開始列
        /// </summary>
        public static readonly int FOMULAR_START_COL = 2;
        public static readonly int FOMULAR_END_COL = 24;
        public static readonly int FOMULAR_ROW = 18;

        /// <summary>
        /// 統計対象ミッション
        /// </summary>
        private Dictionary<string, ClsAttribObject> m_missions = new Dictionary<string, ClsAttribObject>();

        /// <summary>
        /// 統計対象CTM
        /// </summary>
        private Dictionary<string, Dictionary<string, ClsAttribObject>> m_ctms = new Dictionary<string, Dictionary<string, ClsAttribObject>>();

        /// <summary>
        /// 統計対象Element
        /// </summary>
        private Dictionary<string, Dictionary<string, ClsAttribObject>> m_elements = new Dictionary<string, Dictionary<string, ClsAttribObject>>();

        #endregion
        
        #region VSTO デザイナーで生成されたコード

        /// <summary>
        /// デザイナーのサポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(this.Sheet1_Startup);
            this.Shutdown += new System.EventHandler(this.Sheet1_Shutdown);
            this.Change += new Microsoft.Office.Interop.Excel.DocEvents_ChangeEventHandler(this.Sheet1_Change);
            Globals.ThisWorkbook.BeforeSave += new Excel.WorkbookEvents_BeforeSaveEventHandler(Workbook_BeforeSave);
            this.BeforeDoubleClick += Sheet1_BeforeDoubleClick;
        }

        void Sheet1_BeforeDoubleClick(Excel.Range Target, ref bool Cancel)
        {
            string title = Cells[Target.Row, FOMULAR_START_COL].Value;
            string formula = Cells[Target.Row, Target.Column].Value;
            string assitant = Globals.ThisWorkbook.Sheets["退避"].Cells[Target.Row + 1, Target.Column].Value;

            if (FOMULAR_START_COL < Target.Column && title.Equals("演算行/定数"))
            {
                Cancel = true;
                Globals.ThisWorkbook.m_actionPanel.Formula = formula;
                Globals.ThisWorkbook.m_actionPanel.Assitant = assitant;
                Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;
            }
        }

        #endregion

        /// <summary>
        /// 開始時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sheet1_Startup(object sender, System.EventArgs e)
        {
            m_writeLog = new ClsWriteLog("Sheet1");
            //アプリケーション構成ファイル読み込み
            this.LoadConfiguration();            
            //起動処理呼び出し
            this.InitializeSheet();
        }


        /// <summary>
        /// 終了時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sheet1_Shutdown(object sender, System.EventArgs e)
        {
            this.SaveInitialize();
            Globals.ThisWorkbook.BeforeSave -= new Excel.WorkbookEvents_BeforeSaveEventHandler(Workbook_BeforeSave);
        }


        /// <summary>
        /// シート1を初期化する
        /// </summary>
        public void InitializeSheet()
        {
            string userId = "";

            try
            {
                //USER情報確認処理
                if (Globals.ThisWorkbook.Sheets["USERINFO"].Cells[1, 1].Value == null)
                {
                    flagLoading = true;
                    //ユーザー情報がなかった場合処理を抜け、ユーザー情報が入力されるまで待機する
                    return;
                }
                else
                {
                    if (flagLoading)
                    {
                        userId = Globals.ThisWorkbook.Sheets["USERINFO"].Cells[1, 1].Value.ToString();
                        userId = userId.Replace("'", "");
                    }
                    else
                    {
                        flagLoading = true;                        
                        InitializeSheet();
                        return;
                    }
                }

                //ロックを解除する
                this.Unprotect();

                //メニューを初期化する
                this.InitializeMenu();

                //前画面で設定したMission、CTM、ELEMENTを取得する
                this.GetTargetObjects();

                //DocumentActionPaneを初期化する
                Globals.ThisWorkbook.m_actionPanel.Missions = m_missions;
                Globals.ThisWorkbook.m_actionPanel.Ctms = m_ctms;
                Globals.ThisWorkbook.m_actionPanel.Elements = m_elements;
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("Sheet1_Startup", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// メニューを初期化する
        /// </summary>
        private void InitializeMenu()
        {
            //入力規則によりコマンドバーにボタン追加
            //コマンドメニューにアクションパネル開閉ボタン追加
            Office.MsoControlType menuItem3 = Office.MsoControlType.msoControlButton;
            if (m_actionButton != null)
            {
                m_actionButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);
                m_actionButton.Delete();
            }
            m_actionButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem3, missing, missing, 1, true);

            m_actionButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            m_actionButton.Caption = "アクションパネル開閉";
            m_actionButton.Tag = "0";
            m_actionButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);

            //コマンドメニューにサンプル実行ボタンを追加
            Office.MsoControlType menuItem2 = Office.MsoControlType.msoControlButton;
            if (m_SampleButton != null)
            {
                m_SampleButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);
                m_SampleButton.Delete();
            }
            m_SampleButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem2, missing, missing, 1, true);

            m_SampleButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            m_SampleButton.Caption = "サンプル表示";
            m_SampleButton.Tag = "0";
            m_SampleButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);

            //コマンドメニューにミッション実行ボタンを追加
            Office.MsoControlType menuItem = Office.MsoControlType.msoControlButton;
            if (m_menuButton != null)
            {
                m_menuButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
                m_menuButton.Delete();
            }
            m_menuButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem, missing, missing, 1, true);

            m_menuButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            m_menuButton.Caption = "ミッション実行";
            m_menuButton.Tag = "0";
            m_menuButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
        }

        /// <summary>
        /// 前画面で設定したMission、CTM、ELEMENTを取得する
        /// </summary>
        private void GetTargetObjects()
        {
            string[] pids;
            string[] ids;
            string[] displayNames;

            //Missionを読み込む
            ids = GetColumnValues(1);
            displayNames = GetColumnValues(2);
            for (int i = 0; i < ids.Length; i++)
            {
                string id = (string)ids.GetValue(i);
                string displayName = (string)displayNames.GetValue(i);
                m_missions.Add(id, new ClsAttribObject(id, id, displayName));
            }

            //CTMを読み込む
            pids = GetColumnValues(3);
            ids = GetColumnValues(4);
            displayNames = GetColumnValues(5);
            BuildDictionary(pids, ids, displayNames, m_ctms);

            //Elementを読み込む
            pids = GetColumnValues(6);
            ids = GetColumnValues(7);
            displayNames = GetColumnValues(8);
            BuildDictionary(pids, ids, displayNames, m_elements);
        }

        /// <summary>
        /// Excelの１列の値を取得する
        /// </summary>
        private string[] GetColumnValues(int col)
        {
            Excel.Worksheet sheet = Globals.ThisWorkbook.Sheets["ミッション"];
            Excel.Range idRange = sheet.UsedRange.Columns[col];
            return ((System.Array)idRange.Cells.Value).OfType<object>().Select(o => o.ToString()).ToArray();
        }

        /// <summary>
        /// 親子関係表すオブジェクトを作成する
        /// </summary>
        private void BuildDictionary(string[] pids, string[] ids, string[] displayNames, Dictionary<string, Dictionary<string, ClsAttribObject>> dict)
        {
            for (int i = 0; i < ids.Length; i++)
            {
                //親ID
                string pid = (string)pids.GetValue(i);
                //ID
                string id = (string)ids.GetValue(i);
                //表示名
                string displayName = (string)displayNames.GetValue(i);

                ClsAttribObject obj = new ClsAttribObject(pid, id, displayName);
                Dictionary<string, ClsAttribObject> list;

                if (dict.ContainsKey(pid))
                {
                    //親IDは既に存在する場合はオブジェクトリストに追加する
                    dict[pid].Add(id, obj);
                }
                else
                {
                    //親IDは存在しない場合はオブジェクトリストを作成する
                    list = new Dictionary<string, ClsAttribObject>();
                    list.Add(id, obj);
                    dict.Add(pid, list);
                }
            }
        }

        /// <summary>
        /// AISのアプリケーション構成ファイルを読み込む
        /// </summary>
        private void LoadConfiguration()
        {
            try
            {
                /*
                LoadCmsUiConf();
                m_MissionHostName = Config.CmsHost;
                m_MissionPortNo = Config.CmsPort;
                */
                string[] data = getHostAndPort();
                m_MissionHostName = data[0];
                m_MissionPortNo = data[1];

                // MessageBox.Show(string.Format("{0}:{1}", m_MissionHostName, m_MissionPortNo));
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("LoadConfiguration", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// Resource/Ais.conf 読み込み
        /// </summary>
        public const string BaseUrl = "http://{0}:{1}/cms/rest/";
        public static Conf Config { get; set; }
        public static readonly List<string> CatalogLangList = new List<string> { "ja", "en" };
        public static readonly List<string> CatalogLangListForDisplay = new List<string> { "Japanese", "English" };

        public static void LoadCmsUiConf()
        {
            string confPath = Path.GetDirectoryName(Globals.ThisWorkbook.Path) + @"\Ais.conf";
            using (StreamReader sr = new StreamReader(confPath))
            {
                Config = JsonConvert.DeserializeObject<Conf>(sr.ReadToEnd());
            }

            if (!CatalogLangList.Contains(Config.DefaultCatalogLang))
            {
                Config.DefaultCatalogLang = "ja";
            }
        }

        public class Conf
        {
            public string CmsHost;
            public string CmsPort = "60000";
            public string DefaultCatalogLang;
        }

        private string[] getHostAndPort()
        {
            string[] data = new string[2];
            data[0] = "localhost";
            data[1] = "60000";
            var cells = Globals.ThisWorkbook.Sheets["USERINFO"].Cells;
            var rangeA = getFindRange_Interop(cells, "CmsHost");
            if (rangeA != null)
            {
                data[0] = rangeA[1, 2].Value.ToString();
            }
            
            var rangeB = getFindRange_Interop(cells, "CmsPort");
            if (rangeB != null)
            {
                data[1] = rangeB[1, 2].Value.ToString();
            }

            return data;
        }

        /// <summary>
        /// Range取込
        /// </summary>
        /// <param name="srchRange"></param>
        /// <param name="strWhat"></param>
        /// <returns></returns>
        private Excel.Range getFindRange_Interop(Microsoft.Office.Interop.Excel.Range srchRange, String strWhat)
        {
            return srchRange.Find(strWhat,
                                System.Type.Missing,
                                Excel.XlFindLookIn.xlValues,
                                Excel.XlLookAt.xlWhole,
                                Excel.XlSearchOrder.xlByRows,
                                Excel.XlSearchDirection.xlNext,
                                false,
                                System.Type.Missing, System.Type.Missing);
        }

        
        /// <summary>
        /// 【イベント】プルダウン変更処理
        /// </summary>
        /// <param name="Target"></param>
        private void Sheet1_Change(Excel.Range Target)
        {
        }

        /// <summary>
        /// ミッション実行ボタン押下時処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDefault"></param>
        private void button_Click(Office.CommandBarButton ctrl, ref bool CancelDefault)
        {
            m_activeRange = Application.ActiveCell;

            Excel.Range cell1 = null;            
            Excel.Range rng = null;
            try
            {
                if (ctrl.Caption != "ミッション実行")
                {
                    return;
                }

                m_activeRange = Application.ActiveCell; //現在のアクティブセル位置
                string sTime = "";  //ミッションの検索範囲
                string eTime = "";
                cell1 = this.Cells[m_activeRange.Row, 2];
                
                if (cell1.Value is DateTime)
                {
                    DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                    DateTime start = this.Cells[m_activeRange.Row, 2].Value;
                    DateTime end;
                    //開始時刻作成 **********************************
                    cell1 = Globals.ThisWorkbook.Sheets["業務時間"].Range["A1"];
                    TimeSpan startTime = TimeSpan.MaxValue;
                    double tstart = double.MinValue;
                    if (cell1.Value == null || !(cell1.Value is double))
                    {
                        MessageBox.Show("開始時刻を[00:00]形式で入力してください。");
                        return;
                    }
                    else
                    {
                        tstart = cell1.Value;
                        startTime = start.AddDays(tstart) - epoch;
                    }

                    //終了時刻作成 **********************************
                    cell1 = Globals.ThisWorkbook.Sheets["業務時間"].Range["A2"];
                    TimeSpan endTime = TimeSpan.MaxValue;
                    if (cell1.Value == null || !(cell1.Value is double))
                    {
                        MessageBox.Show("終了時刻を[00:00]形式で入力してください。");
                        return;
                    }
                    else
                    {
                        double tend = cell1.Value;
                        // 終了時刻が開始時刻より前の場合次の日の時刻にする
                        if (tstart >= tend)
                        {
                            end = start.AddDays(1);
                        }
                        else
                        {
                            end = start;
                        }
                        end = end.AddDays(tend);
                        end = DateTime.Parse(end.ToString("yyyy/MM/dd HH:mm") + ":59.999");
                        endTime = (end - epoch);
                        //endTime = (end.AddDays(tend) - epoch);
                    }
                    
                    sTime = Math.Floor(startTime.TotalMilliseconds).ToString();
                    eTime = Math.Floor(endTime.TotalMilliseconds).ToString();
                }
                else
                {
                    MessageBox.Show("データ挿入行からミッションを実行してください");
                    return;
                }

                // 再計算停止
                this.Application.Calculation = Excel.XlCalculation.xlCalculationManual;
                this.Application.ScreenUpdating = false;
                this.Application.DisplayAlerts = false;

                foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions)
                {
                    ClsJsonData[] jsonData = null;
                    string missionId = mission.Key;
                    jsonData = ClsJsonData.get(missionId, sTime, eTime, m_writeLog);
                    int count = 0;
                    foreach (KeyValuePair<string, ClsAttribObject> attrObj in m_ctms[missionId])
                    {
                        string key = mission.Value.DisplayName + "." + attrObj.Value.DisplayName;
                        this.CTMWriteSheet(mission.Value, attrObj.Value, jsonData);
                        count++;
                    }
                    jsonData = null;
                }

                //メインシートにデータを挿入する
                CalcurateSheet();
                
                // 更新処理
                this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                this.Application.Calculate();
                this.Application.ScreenUpdating = true;
                
                this.Activate();
                this.Application.DisplayAlerts = true;

                MessageBox.Show("ミッション取得処理が正常に完了しました。");
            }

            //Mi失敗時
            catch (Exception ex)
            {                
                MessageBox.Show(ex.Message);
                m_writeLog.OutPut("button_Click", ex.Message + ex.StackTrace);
                this.Application.DisplayAlerts = true;

                // 更新処理
                this.Application.ScreenUpdating = true;
                this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                this.Application.Calculate();

                //Mi登録状態表示処理
                for (int j = 4; j < 15000; j += 3) // 3 ->セルの結合数
                {
                    cell1 = this.Cells[6, j];
                    //cell2 = this.Cells[6, j + 3];
                    rng = this.Range[cell1, cell1];
                    if (!String.IsNullOrEmpty(rng.Value))
                    {
                        if (rng.Value == "-") continue;
                        this.Range[this.Cells[7, j], this.Cells[7, j]].Value = "処理中断";
                    }
                    else
                    {
                        Excel.Range nextCell1 = this.Cells[6, j + 3]; // 次のセル
                        string nextMi = (string)this.Range[nextCell1, nextCell1].Value;

                        if (String.IsNullOrEmpty(nextMi))
                        {
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// ミッション結果の書き込みを行う
        /// </summary>
        private void CTMWriteSheet(ClsAttribObject mission, ClsAttribObject ctm, ClsJsonData[] jsonData)
        {
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;

            try
            {                
                int actMission_count = 0;
                int MissionValue_count = 0;
                //CTMを列挙
                string str = (actMission_count).ToString();             

                //CTMを列挙
                foreach (ClsJsonData CTM in jsonData)
                {
                    string str2 = (MissionValue_count).ToString();                    
                    //ミッションから対象のCTMを探す　見つかった場合のみ処理を行う
                    if (CTM.id == ctm.Id) // 2015.07.24
                    {
                        //新規シートの作成
                        string SheetName = mission.DisplayName + "." + ctm.DisplayName;
                        for (int i = 1; i <= Globals.ThisWorkbook.Worksheets.Count; i++)
                        {
                            if (SheetName == Globals.ThisWorkbook.Worksheets[i].Name)
                            {
                                Globals.ThisWorkbook.Worksheets[i].Delete();
                            }
                        }
                        Excel.Worksheet newWorksheet;
                        newWorksheet = (Excel.Worksheet)Globals.ThisWorkbook.Worksheets.Add(After: Globals.ThisWorkbook.Worksheets[Globals.ThisWorkbook.Worksheets.Count]);
                        newWorksheet.Name = SheetName;                        
                        newWorksheet.Select(1);

                        //CTMが上がっていなかった場合はループを抜ける
                        if (CTM.ctms.Count == 0)
                        {
                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            break;
                        }

                        //シート内にエレメントを挿入
                        int rowCount = 2;
                        int colCount = 0;
                        rng = (Excel.Range)Globals.ThisWorkbook.Application.Selection;
                        DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                        rng[1, 1] = "No.";
                        //ISSUE_NO.748 Sunyi 2018/08/13 Start
                        //rng[1, 2] = "収集時刻";
                        rng[1, 2] = "RECEIVE TIME";
                        //ISSUE_NO.748 Sunyi 2018/08/13 End

                        //CTMを時間で分けて列挙
                        foreach (ClsJsonData.Ctms ctms in CTM.ctms)
                        {
                            colCount = 3;
                            rng[rowCount, 1] = rowCount - 1;                            
                            rng[rowCount, 2] = epoch.AddMilliseconds(ctms.RT).ToString("M/d HH:mm:ss");

                            //CTM内のエレメントを列挙
                            foreach (string EleID in ctms.EL.Keys) // 1レコードのエレメント毎
                            {
                                Dictionary<string, ClsAttribObject> elements = m_elements[ctm.Id];
                                if (elements.ContainsKey(EleID))
                                {
                                    if (rowCount == 2)
                                    {
                                        //動的に列を追加
                                        rng[1, colCount] = elements[EleID].DisplayName;
                                    }

                                    if (ctms != null && ctms.EL != null && ctms.EL.Count > 0 && ctms.EL.ContainsKey(EleID) && ctms.EL[EleID] != null)
                                    {
                                        if (String.IsNullOrEmpty(ctms.EL[EleID].FN))
                                        {
                                            rng[rowCount, colCount] = ctms.EL[EleID].V;
                                        }
                                        else
                                        {
                                            // バルキーファイル
                                            rng[rowCount, colCount] = ctms.EL[EleID].FN;
                                        }
                                    }
                                    colCount++;
                                }
                            }
                            rowCount++;
                        }

                        //エレメントの計算値入力                        
                        rng[rowCount, 2] = "個数";

                        rng[rowCount + 1, 2] = "積算";
                        rng[rowCount + 2, 2] = "平均";
                        rng[rowCount + 3, 2] = "最大";
                        rng[rowCount + 4, 2] = "最小";
                        rng[rowCount + 5, 2] = "種類数";

                        for (int i = 3; i < colCount; i++)
                        {                        
                            string ColLetterTemp = newWorksheet.Cells[rowCount, i].Address;
                            string[] SplitLetter = ColLetterTemp.Split('$');
                            string ColLetter = SplitLetter[1];
                            rng[rowCount, i].Formula = string.Format("=COUNTA({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 1, i].Formula = string.Format("=SUM({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}2:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            rng[rowCount + 3, i].Formula = string.Format("=MAX({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 4, i].Formula = string.Format("=MIN({0}2:{0}{1})", ColLetter, rowCount - 1);                        
              
                        }
                        //罫線表示処理
                        cell1 = newWorksheet.Cells[1, 1];
                        cell2 = newWorksheet.Cells[rowCount - 1, 1];
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        cell1 = newWorksheet.Cells[1, 2];
                        cell2 = newWorksheet.Cells[rowCount + 5, colCount - 1];
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //セルサイズを自動調整
                        newWorksheet.Columns.AutoFit();
                    }
                    MissionValue_count++;
                }

                actMission_count++;

            }
            catch (Exception ex)
            {                
                MessageBox.Show(ex.Message);
                m_writeLog.OutPut("CTMSheetWrite", ex.Message + ex.StackTrace);
                Globals.ThisWorkbook.Application.DisplayAlerts = true;
            }


        }

        /// <summary>
        /// メインシートデータ挿入処理
        /// </summary>
        private void CalcurateSheet()
        {
            Excel.Range rng = null;
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;

            try
            {
                cell1 = this.Cells[m_activeRange.Row, 2];

                for (int i = 8; i <= 15000; i++)
                {
                    //演算行の位置を探す
                    cell1 = this.Cells[i, 2];
                    if (!(cell1.Value is string))
                    {
                        continue;
                    }
                    if (!(cell1.Value is double) && cell1.Value == "演算行/定数")
                    {
                        rng = this.Range[cell1, cell1].EntireRow;
                        break;
                    }
                }

                //演算行が見つからなかった場合は処理を抜ける
                if (rng == null)
                {
                    return;
                }

                //行位置の基準を設定する
                int rowPoint = rng.Row;

                //演算行の各セルを見て演算を行う
                for (int i = 3; i <= 15000; i++)
                {
                    /*
                     * 検索する文字列の形式は"シート名,列名,行名"+(x * "シート名,列名,行名")　の形式
                     */
                    //cell1 = this.Cells[rowPoint, i];
                    cell1 = Globals.ThisWorkbook.Sheets["退避"].Cells[rowPoint, i];
                    cell2 = this.Cells[m_activeRange.Row, i];

                    bool dataNothingChk = false; // データシートにデータがあるかチェック
                    try
                    {
                        if (cell1.Value != null)
                        {
                            string compStr = cell1.Value.ToString();
                            compStr = compStr.Replace("\"", "?\"?");
                            compStr = compStr.Replace("\"", "?\"?");
                            string[] culcStr = compStr.Split('?');

                            //シート検索フォーマットが入力されていた場合、計算を行う
                            if (culcStr.Length > 1)
                            {
                                for (int j = 0; j < culcStr.Length; j++)
                                {
                                    string[] culcSheet = culcStr[j].Split(',');
                                    //シート検索フォーマットが入力されていた場合は以下の処理を行う
                                    if (culcSheet.Length == 3)
                                    {
                                        Excel.Worksheet culcWorkSheet = Globals.ThisWorkbook.Worksheets[culcSheet[0]];
                                        if (culcWorkSheet.Cells[1, 1].Value.ToString() == "データ無し")
                                        {
                                            dataNothingChk = true;
                                            cell2.Value = 0; // データがない場合は0を入れる
                                            break;
                                        }
                                        int col = culcWorkSheet.Rows[1].Find(culcSheet[1]).Column;
                                        int row = culcWorkSheet.Columns[2].Find(culcSheet[2]).Row;
                                        if (culcSheet[2] == "種類数")
                                        {
                                            string ColLetterTemp = culcWorkSheet.Cells[row, col].Address;
                                            string[] SplitLetter = ColLetterTemp.Split('$');
                                            string ColLetter = SplitLetter[1];
                                            culcWorkSheet.Cells[row, col].FormulaArray = string.Format("=SUM(1/COUNTIF({0}2:{0}{1},{0}2:{0}{1}))", ColLetter, row - 6);
                                        }
                                        culcStr[j] = culcWorkSheet.Cells[row, col].Value.ToString();
                                    }
                                }

                                if (dataNothingChk != true) // 取得データがあれば処理続行
                                {
                                    bool kakFlag = true;
                                    for (int j = 0; j < culcStr.Length; j++)
                                    {
                                        if (culcStr[j] == "\"")
                                        {
                                            if (kakFlag)
                                            {
                                                culcStr[j] = "(";
                                                kakFlag = false;
                                            }
                                            else
                                            {
                                                culcStr[j] = ")";
                                                kakFlag = true;
                                            }
                                        }
                                    }

                                    //この時点で数値型と四則演算式の文字列になる
                                    string resultStr = String.Join(null, culcStr);
                                    cell2.Value = "=" + resultStr;
                                }
                                
                            }
                            //フォーマットが入力されていなかった場合はエクセルの基本の式を動かす
                            else
                            {
                                cell2.Value = String.Format("=\"{0}\"", compStr);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        m_writeLog.OutPut("calcurateSheet", ex.Message + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("calcurateSheet", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// 【イベント】サンプル表示ボタン押下時処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDefault"></param>
        private void btn_Sample_Click(Office.CommandBarButton ctrl, ref bool CancelDefault)
        {
            try
            {
                Office.CommandBarButton btnObj = ctrl;

                if (ctrl.Caption != "サンプル表示" && ctrl.Caption != "サンプル非表示")
                {
                    return;
                }

                if (btnObj.Caption == "サンプル表示")
                {
                    Globals.Sheet2.sampleOpen();
                    btnObj.Caption = "サンプル非表示";
                }
                else
                {
                    Globals.Sheet2.sampleClose();
                    btnObj.Caption = "サンプル表示";
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("btn_Sample_Click", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// アクションパネル開閉処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDedfault"></param>
        private void button_Action_Click(Office.CommandBarButton ctrl, ref bool CancelDedfault)
        {
            try
            {
                if (ctrl.Caption != "アクションパネル開閉")
                {
                    return;
                }

                if (Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane)
                {
                    Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = false;
                }
                else
                {
                    string formula = string.Empty;
                    string assitant = string.Empty;

                    Excel.Range range = Application.ActiveCell;
                    int row = range.Row;
                    int col = range.Column;

                    if (row == FOMULAR_ROW && col > FOMULAR_START_COL && col <= FOMULAR_END_COL)
                    {
                        formula = range[row, col].Value;
                        assitant = Globals.ThisWorkbook.Sheets["退避"].Cells[row + 1, col].Value;
                    }
                    Globals.ThisWorkbook.m_actionPanel.Formula = formula;
                    Globals.ThisWorkbook.m_actionPanel.Assitant = assitant;
                    Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("button_Action_Click", ex.Message + ex.StackTrace);
            }
        }


        /// <summary>
        /// ワークブック保存前イベント
        /// 次回起動時に問題が起きないように、各コントロール・入力規則を削除する
        /// </summary>
        /// <param name="SaveAsUI">名前を付けて保存かどうか</param>
        /// <param name="Cancel">Excel動作による保存フラグ</param>
        private void Workbook_BeforeSave(bool SaveAsUI, ref bool Cancel)
        {
            try
            {

                //保存先のファイルパスを指定する                
                string filePath = Path.GetDirectoryName(Globals.ThisWorkbook.Path);                
                FileSystem.ChDir(filePath);

                string fullPath = string.Empty;
                if (filePath.Contains("AIS_Template") == true)
                {
                    fullPath = filePath + @"\ExcelResult";  //上書き保存時のフォルダ比較用  
                }
                else
                {
                    fullPath = filePath + @"\AIS_Template\ExcelResult";  //上書き保存時のフォルダ比較用  
                }                
                FileSystem.ChDir(fullPath);       //移動                

                //上書き保存と名前付き保存で処理を分ける
                if (SaveAsUI)
                {                    
                    //名前を付けて保存
                    var FileName = Application.GetSaveAsFilename(Globals.ThisWorkbook.Name, "Excelファイル(*.xlsx),*.xlsx");

                    if (FileName is bool && FileName == false)  //キャンセル時はbool型が返る
                    {
                        Cancel = true;
                        return;
                    }
                    else  //保存時はファイル名が返る
                    {
                        Cancel = true;

                        //保存用コントロール初期化
                        this.SaveInitialize();

                        //セーブ実行
                        this.Application.EnableEvents = false;
                        Globals.ThisWorkbook.SaveAs(FileName);
                        this.Application.EnableEvents = true;
                        //コントロール類をもとに戻す
                        this.WorkbookAfterSave();
                    }
                }
                //名前をつけて保存時
                else
                {                    
                    //規定のフォルダか確認
                    if (Path.GetFullPath(Globals.ThisWorkbook.Path) != fullPath)
                    {
                        MessageBox.Show("このフォルダ上で上書きできません");
                        Cancel = true;
                        return;
                    }
                    else
                    {
                        Cancel = true;

                        this.SaveInitialize();

                        //セーブ実行　誤動作回避でイベントを切る
                        this.Application.EnableEvents = false;
                        Globals.ThisWorkbook.Save();
                        this.Application.EnableEvents = true;

                        //再読み込み
                        this.WorkbookAfterSave();
                    }
                }

            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("button_Action_Click", ex.Message + ex.StackTrace);
            }

        }


        /// <summary>
        /// セーブ時のコントロール初期化
        /// </summary>
        public void SaveInitialize()
        {
            //入力規則を廃棄する
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;
            Excel.Range cell1_Mi = null;
            Excel.Range cell2_MI = null;
            Excel.Range rng_Mi = null;

            int count = 0;
            cell1 = this.Cells[6, 4];

            //ミッション選択部分の入力規則を削除する
            while (cell1.Value != null)
            {
                cell1 = this.Cells[6, 4 + count];
                cell2 = this.Cells[6, 6 + count];
                if (cell1.Value == null) break;
                rng = this.Range[cell1, cell2];
                rng.Validation.Delete();

                cell1_Mi = this.Cells[5, 4 + count];
                cell2_MI = this.Cells[5, 6 + count];
                if (cell1.Value == null) break;
                rng_Mi = this.Range[cell1_Mi, cell2_MI];
                rng_Mi.Validation.Delete();

                count += 3;
            }

            //コマンドメニューのボタン類を削除
            if (m_menuButton != null)
            {
                m_menuButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
                m_menuButton.Delete();
                m_menuButton = null;
            }
            if (m_SampleButton != null)
            {
                m_SampleButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);
                m_SampleButton.Delete();
                m_SampleButton = null;
            }
            if (m_actionButton != null)
            {
                m_actionButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);
                m_actionButton.Delete();
                m_actionButton = null;
            }     
           
        }

        /// <summary>
        /// ワークブック保存後再読込関数
        /// </summary>
        private void WorkbookAfterSave()
        {
            //ロードフラグオン
            flagLoading = true;
            //読込開始            
            this.InitializeSheet();
        }
    }
#else
    /// <summary>
    /// AISデータ加工用エクセルシート
    /// </summary>
    public partial class Sheet1
    {
    #region"メンバー"
        /// <summary>
        /// 列位置調整用変数
        /// </summary>
        //private int m_colPoint;

        /// <summary>
        /// ミッション登録用の文字列
        /// </summary>
        private string m_queryStr_Mi;
        private string m_queryStr;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_menuButton;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_SampleButton;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_actionButton;

        /// <summary>
        /// 現在のアクティブセル
        /// </summary>
        private Excel.Range m_activeRange;

        /// <summary>
        /// ミッションの名前とIDを格納するクラス
        /// </summary>
        private ClsUserMission[] m_userMission;

        /// <summary>
        /// ミッションとCTMを対応付けするディクショナリ
        /// </summary>
        private Dictionary<string, ClsCTMList> m_missionDict;

        /// <summary>
        /// 実行したミッションの名前と実行結果を格納する
        /// </summary>
        //private Dictionary<string, ClsJsonData[]> m_actMission;
            
        /// <summary>
        /// ログ書き出し用クラス
        /// </summary>
        private ClsWriteLog m_writeLog;

        /// <summary>
        /// テスト用リスト
        /// </summary>
        private string[] MiList;

        /// <summary>
        /// アプリケーション読み込みフラグ
        /// </summary>
        public bool flagLoading = false;

        /// <summary>
        /// ユーザーが選択したCTMを格納する
        /// </summary>
        Dictionary<string, ClsCTMList> m_userMissionDict;

        /// <summary>
        /// ミッション投入用ホスト
        /// </summary>
        public string m_MissionHostName { get; set; }
        /// <summary>
        /// ミッション投入用ポート
        /// </summary>
        public string m_MissionPortNo { get; set; }

        /// <summary>
        /// 計算式入力開始列
        /// </summary>
        private const int FOMULAR_START_COL = 2;

        #endregion
        
    #region VSTO デザイナーで生成されたコード

        /// <summary>
        /// デザイナーのサポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(this.Sheet1_Startup);
            this.Shutdown += new System.EventHandler(this.Sheet1_Shutdown);
            this.Change += new Microsoft.Office.Interop.Excel.DocEvents_ChangeEventHandler(this.Sheet1_Change);
            Globals.ThisWorkbook.BeforeSave += new Excel.WorkbookEvents_BeforeSaveEventHandler(Workbook_BeforeSave);
            this.BeforeDoubleClick += Sheet1_BeforeDoubleClick;
        }

        void Sheet1_BeforeDoubleClick(Excel.Range Target, ref bool Cancel)
        {
            string title = Cells[Target.Row, FOMULAR_START_COL].Value;

            if (FOMULAR_START_COL < Target.Column && title.Equals("演算行/定数"))
            {
                Cancel = true;
                Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;
            }
        }

        #endregion

        /// <summary>
        /// 開始時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sheet1_Startup(object sender, System.EventArgs e)
        {
            m_writeLog = new ClsWriteLog("Sheet1");
            //アプリケーション構成ファイル読み込み
            this.LoadConfiguration();            
            //起動処理呼び出し
            this.InitializeSheet();
        }


        /// <summary>
        /// 終了時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sheet1_Shutdown(object sender, System.EventArgs e)
        {
            this.SaveInitialize();
            Globals.ThisWorkbook.BeforeSave -= new Excel.WorkbookEvents_BeforeSaveEventHandler(Workbook_BeforeSave);
        }


        /// <summary>
        /// シート1を初期化する
        /// </summary>
        public void InitializeSheet()
        {
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;
            Excel.Range cell1_Mi = null;
            Excel.Range cell2_Mi = null;
            Excel.Range rng_Mi = null;
            string userId = "";

            try
            {
                //USER情報確認処理
                if (Globals.ThisWorkbook.Sheets["USERINFO"].Cells[1, 1].Value == null)
                {
                    flagLoading = true;
                    //ユーザー情報がなかった場合処理を抜け、ユーザー情報が入力されるまで待機する
                    return;
                }
                else
                {
                    if (flagLoading)
                    {
                        userId = Globals.ThisWorkbook.Sheets["USERINFO"].Cells[1, 1].Value.ToString();
                        userId = userId.Replace("'", "");
                    }
                    else
                    {
                        flagLoading = true;                        
                        InitializeSheet();
                        return;
                    }
                }

                //ロックを解除する
                this.Unprotect();

                //入力規則によりコマンドバーにボタン追加
                //コマンドメニューにアクションパネル開閉ボタン追加
                Office.MsoControlType menuItem3 = Office.MsoControlType.msoControlButton;
                if (m_actionButton != null)
                {
                    m_actionButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);
                    m_actionButton.Delete();                    
                }
                m_actionButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem3, missing, missing, 1, true);

                m_actionButton.Style = Office.MsoButtonStyle.msoButtonCaption;
                m_actionButton.Caption = "アクションパネル開閉";
                m_actionButton.Tag = "0";
                m_actionButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);

                //コマンドメニューにサンプル実行ボタンを追加
                Office.MsoControlType menuItem2 = Office.MsoControlType.msoControlButton;
                if (m_SampleButton != null)
                {
                    m_SampleButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);
                    m_SampleButton.Delete();                    
                }
                m_SampleButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem2, missing, missing, 1, true);

                m_SampleButton.Style = Office.MsoButtonStyle.msoButtonCaption;
                m_SampleButton.Caption = "サンプル表示";
                m_SampleButton.Tag = "0";
                m_SampleButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);

                //コマンドメニューにミッション実行ボタンを追加
                Office.MsoControlType menuItem = Office.MsoControlType.msoControlButton;
                if (m_menuButton != null)
                {
                    m_menuButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
                    m_menuButton.Delete();                   
                }
                m_menuButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem, missing, missing, 1, true);

                m_menuButton.Style = Office.MsoButtonStyle.msoButtonCaption;
                m_menuButton.Caption = "ミッション実行";
                m_menuButton.Tag = "0";
                m_menuButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);

                //ユーザーが参照できるミッションを取得
                //Missionを呼び出してクラス内のメンバーにミッション-CTM-エレメントを格納する                
                m_missionDict = new Dictionary<string, ClsCTMList>();
                m_userMission = ClsUserMission.get(userId);

                //サーバー上でアクセスできるすべてのCTMを取得する
                ClsCTMList[] ctmList = ClsCTMList.get();

                m_queryStr_Mi = "-";
                foreach (ClsUserMission Missions in m_userMission)
                {
                    foreach(var mi in Missions.children)
                    {
                        ////ユーザーがミッションで得られるCTMのIDを取得する
                        if (mi.missionType == "1")
                        {
                            m_queryStr_Mi = m_queryStr_Mi + "," + mi.name;

                            ClsUserCTMList[] userCtmList = ClsUserCTMList.get(mi.id);
                            foreach (ClsUserCTMList userCtm in userCtmList)
                            {
                                //CTMIDとCTM名で対応させる
                                //ClsCTMList myCTM = ctmList.First(x => x.id == userCtm.ctmId);
                                //ミッションに追加されているが参照辞書から消されてしまった場合の対応 okada-r 2015/12/07
                                ClsCTMList myCTM = ctmList.FirstOrDefault(x => x.id == userCtm.ctmId);
                                if (myCTM != null)
                                {
                                    string resultName = mi.name + "." + myCTM.displayName[0].text; // 2015.07.24
                                    m_missionDict.Add(resultName, myCTM);
                                }                                
                            }
                        }
                    }
                }

                //プルダウン初期化 Maeda Add 2015.11.09 ↓
                cell1_Mi = this.Cells[5, 4];
                cell2_Mi = this.Cells[5, 6];
                rng_Mi = this.get_Range(cell1_Mi, cell2_Mi);
                // Maeda Add 2015.11.09 ↑

                //プルダウン初期化
                cell1 = this.Cells[6, 4];
                //cell2 = this.Cells[6, 5];
                cell2 = this.Cells[6, 6];
                rng = this.get_Range(cell1, cell2);
                MiList = m_missionDict.Keys.ToArray<string>();

                Array.Sort(MiList, new ClsNaturalComparer());

                int count = 0;

                // 文字数制限追加 maeda 2015.11.17 ↓
                if (m_queryStr_Mi.Length > 8000)
                {
                    string Msg = "ミッション一覧の文字数が制限を超えています。（8000文字以内）";
                    MessageBox.Show(Msg);
                    m_writeLog.OutPut("MiCTM_List :", Msg);
                    return;

                }
                // 文字数制限追加 maeda 2015.11.17 ↑

                while (cell1.Value != null)
                {
                    // Miリスト作成位置 maeda 2015.11.17 ↓
                    cell1_Mi = this.Cells[5, 4 + count];
                    cell2_Mi = this.Cells[5, 6 + count];
                    rng_Mi = this.Range[cell1_Mi, cell2_Mi];
                    // Miリスト作成位置 maeda 2015.11.17 ↑

                    cell1 = this.Cells[6, 4 + count];
                    //cell2 = this.Cells[6, 5 + count];
                    cell2 = this.Cells[6, 6 + count];
                    rng = this.get_Range(cell1, cell2);
                    if (cell1.Value == null) break;

                    // Mi初期データ設定 maeda 2015.11.17 ↓
                    string Mi_Val = cell1.Value;
                    string[] Mi_Vals = Mi_Val.Split('.');
                    // Mi初期データ設定 maeda 2015.11.17 ↑

                    m_queryStr = "-";
                    foreach (string miStr in MiList)
                    {
                        if (Mi_Vals[0] == miStr.Split('.')[0])
                        {
                            m_queryStr += "," + miStr;
                        }

                    }

                    if (m_queryStr.Length > 8000)
                    {
                        string Msg = "ミッションとCTM名称で構成されている一覧の文字数が制限を超えています。（8000文字以内）";
                        MessageBox.Show(Msg);
                        m_writeLog.OutPut("MiCTM_List :", Msg);
                        return;

                    }

                    // Miリスト設定 maeda 2015.11.17 ↓
                    rng_Mi.Validation.Delete();
                    rng_Mi.Merge();
                    rng_Mi.Value = Mi_Vals[0];
                    rng_Mi.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, m_queryStr_Mi, true);
                    rng_Mi.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    rng_Mi.HorizontalAlignment  = Excel.XlHAlign.xlHAlignCenter;
                    rng_Mi.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    rng_Mi.Select();
                    // Miリスト設定 maeda 2015.11.17 ↑

                    rng.Validation.Delete();
                    rng.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, m_queryStr, true);
                    int colCount = 4 + count; // 2015.11.04                                        
                    count += 3;
                }


                if (m_userMissionDict != null)
                {
                    Globals.ThisWorkbook.m_actionPanel.m_missionDict = m_userMissionDict;
                    Globals.ThisWorkbook.m_actionPanel.AdditionCTMData();
                    Globals.ThisWorkbook.m_actionPanel.controlClear();
                }

            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("Sheet1_Startup", ex.Message + ex.StackTrace);
            }
        }


        ///// <summary>
        ///// AISのアプリケーション構成ファイルを読み込む
        ///// </summary>
        //private void LoadConfiguration()
        //{
        //    try
        //    {
        //        //AISの構成ファイルパスを指定
        //        string filePath = Path.GetDirectoryName(Globals.ThisWorkbook.Path);                
                
        //        if (filePath.Contains("AIS_Template")) // 固定
        //        {
        //            filePath = filePath.Remove(filePath.Length - 13);
        //        }

        //        filePath += @"\AIStool.exe.config";
                
        //        ExeConfigurationFileMap appConfigFile = new ExeConfigurationFileMap { ExeConfigFilename = filePath};

        //        Configuration appConfig = ConfigurationManager.OpenMappedExeConfiguration(appConfigFile, ConfigurationUserLevel.None);
        //        ClientSettingsSection section = (ClientSettingsSection)appConfig.SectionGroups["userSettings"].Sections[0];
        //        SettingElementCollection keyList = (SettingElementCollection)section.Settings;

        //        m_MissionHostName = keyList.Get("PMHostName").Value.ValueXml.InnerText;
        //        m_MissionPortNo = keyList.Get("PMPortNo").Value.ValueXml.InnerText;
        //    }
        //    catch (Exception ex)
        //    {
        //        m_writeLog.OutPut("LoadConfiguration", ex.Message + ex.StackTrace);
        //    }
        //}

        /// <summary>
        /// AISのアプリケーション構成ファイルを読み込む
        /// </summary>
        private void LoadConfiguration()
        {
            try
            {
                LoadCmsUiConf();
                m_MissionHostName = Config.CmsHost;
                m_MissionPortNo = Config.CmsPort;

            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("LoadConfiguration", ex.Message + ex.StackTrace);
            }
        }


        /// <summary>
        /// Resource/Ais.conf 読み込み
        /// </summary>
        public const string BaseUrl = "http://{0}:{1}/cms/rest/";
        public static Conf Config { get; set; }
        public static readonly List<string> CatalogLangList = new List<string> { "ja", "en" };
        public static readonly List<string> CatalogLangListForDisplay = new List<string> { "Japanese", "English" };

        public static void LoadCmsUiConf()
        {
            string confPath = Path.GetDirectoryName(Globals.ThisWorkbook.Path) + @"\Ais.conf";
            using (StreamReader sr = new StreamReader(confPath))
            {
                Config = JsonConvert.DeserializeObject<Conf>(sr.ReadToEnd());
            }

            if (!CatalogLangList.Contains(Config.DefaultCatalogLang))
            {
                Config.DefaultCatalogLang = "ja";
            }
        }

        public class Conf
        {
            public string CmsHost;
            public string CmsPort = "60000";
            public string DefaultCatalogLang;
        }

        
        /// <summary>
        /// 【イベント】プルダウン変更処理
        /// </summary>
        /// <param name="Target"></param>
        private void Sheet1_Change(Excel.Range Target)
        {
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;
            Excel.Range cell1_Mi = null;
            Excel.Range cell2_Mi = null;
            Excel.Range rng_Mi = null;
       
            try
            {
                Excel.Range fullRow;
                fullRow = this.Rows[5];
                // CTMリスト作成 maeda 2015.11.17 ↓
                if (Target.Row == fullRow.Row)
                {
                    ////シートの保護を解除
                    this.Unprotect();

                    cell1_Mi = this.Cells[5, Target.Column];
                    cell2_Mi = this.Cells[5, Target.Column + 2];
                    rng_Mi = this.Range[cell1_Mi, cell2_Mi];

                    if (string.IsNullOrEmpty((string)cell1_Mi.Value) != true)
                    {
                        if (cell1_Mi.Value != "-")
                        {                          
                            m_queryStr = "-";

                            if (string.IsNullOrEmpty((string)cell1_Mi.Value) != true)
                            {
                                foreach (string miStr in MiList)
                                {
                                    string[] miStrs = miStr.Split('.');
                                    if (cell1_Mi.Value == miStrs[0])
                                    {
                                        m_queryStr += "," + miStr;
                                    }

                                }
                            }

                            cell1 = this.Cells[6, Target.Column];
                            cell2 = this.Cells[6, Target.Column + 2]; // 2つ結合

                            rng = this.Range[cell1, cell2];
                            rng.Merge();
                            rng.Validation.Delete();
                            rng.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, m_queryStr, true);
                            rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            rng.Interior.ColorIndex = 37;                            
                        }
                    }
                }
                // CTMリスト作成 maeda 2015.11.17 ↑

                fullRow = this.Rows[6];
                if (Target.Row == fullRow.Row)
                {
                    //シートの保護を解除
                    this.Unprotect();

                    //変更でミッションが選択されなかった場合は処理を抜ける
                    if (Target.Value == "-")
                    {
                        // 次の列にCTMが選択された場合　->　その列を削除
                        Excel.Range nextCell = this.Cells[6, Target.Column + 3];
                        if (string.IsNullOrEmpty((string)nextCell.Value) == false)
                        {
                            cell1 = this.Cells[5, Target.Column];
                            cell2 = this.Cells[7, Target.Column + 2];
                            rng = this.Range[cell1, cell2];
                            rng.Delete(Excel.XlDeleteShiftDirection.xlShiftToLeft);
                        }
                        return;
                    }

                    //同名ミッション選択時処理
                    //空白セルに到達した場合はプルダウンメニューを追加する
                    int count = 0;
                    for (int i = 4; i < 15000; i += 3) // 3 ->セルの結合数
                    {
                        cell1 = this.Cells[6, i];
                        cell2 = this.Cells[6, i + 2]; // 2つ結合
                        rng = this.Range[cell1, cell1];

                        if (cell1.Value == Target.Value || cell1.Value == "-")
                        {
                            count++;
                            //自分以外に同じ値があった場合
                            if (count == 2)
                            {
                                rng.Value = "-";
                                return;
                            }

                        }

                        //空白セルだった場合
                        else if (string.IsNullOrEmpty((string)cell1.Value))
                        {
                            // ミッションリスト作成 maeda 2015.11.17 ↓
                            cell1_Mi = this.Cells[5, i];
                            cell2_Mi = this.Cells[5, i + 2];
                            rng_Mi = this.Range[cell1_Mi, cell2_Mi];
                            rng_Mi.Merge();
                            rng_Mi.Validation.Delete();
                            rng_Mi.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, m_queryStr_Mi, true);
                            rng_Mi.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            rng_Mi.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            rng_Mi.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            rng_Mi.Value = "-";
                            //rng_Mi.Activate();
                            // ミッションリスト作成 maeda 2015.11.17 ↑

                            rng = this.Range[cell1, cell2];
                            rng.Validation.Delete();
                            rng.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, m_queryStr, true);
                            rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            rng.Interior.ColorIndex = 37;
                            cell1.Value = "-";
                            //文字列表示処理
                            cell1 = this.Cells[7, i];
                            cell2 = this.Cells[7, i + 2]; // 2つ結合
                            rng = this.Range[cell1, cell2];
                            rng.Value = "-";
                            rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {                
                m_writeLog.OutPut("Sheet1_Change", ex.Message + ex.StackTrace);
            }
        }



        /// <summary>
        /// ミッション実行ボタン押下時処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDefault"></param>
        private void button_Click(Office.CommandBarButton ctrl, ref bool CancelDefault)
        {
            m_activeRange = Application.ActiveCell;

            Excel.Range cell1 = null;            
            Excel.Range rng = null;
            try
            {
                if (ctrl.Caption != "ミッション実行")
                {
                    return;
                }


                m_userMissionDict = new Dictionary<string, ClsCTMList>(); //ユーザーが選択したミッションリスト

                m_activeRange = Application.ActiveCell; //現在のアクティブセル位置
                string sTime = "";  //ミッションの検索範囲
                string eTime = "";
                cell1 = this.Cells[m_activeRange.Row, 2];
                
                if (cell1.Value is DateTime)
                {
                    DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                    DateTime start = this.Cells[m_activeRange.Row, 2].Value;
                    DateTime end;
                    //開始時刻作成 **********************************
                    cell1 = this.Range["C3"];
                    TimeSpan startTime = TimeSpan.MaxValue;
                    double tstart = double.MinValue;
                    if (cell1.Value == null || !(cell1.Value is double))
                    {
                        MessageBox.Show("開始時刻を[00:00]形式で入力してください。");
                        return;
                    }
                    else
                    {
                        tstart = cell1.Value;
                        startTime = start.AddDays(tstart) - epoch;
                    }

                    //終了時刻作成 **********************************
                    cell1 = this.Range["C4"];
                    TimeSpan endTime = TimeSpan.MaxValue;
                    if (cell1.Value == null || !(cell1.Value is double))
                    {
                        MessageBox.Show("終了時刻を[00:00]形式で入力してください。");
                        return;
                    }
                    else
                    {
                        double tend = cell1.Value;
                        // 終了時刻が開始時刻より前の場合次の日の時刻にする
                        if (tstart >= tend)
                        {
                            end = start.AddDays(1);
                        }
                        else
                        {
                            end = start;
                        }
                        end = end.AddDays(tend);
                        end = DateTime.Parse(end.ToString("yyyy/MM/dd HH:mm") + ":59.999");
                        endTime = (end - epoch);
                        //endTime = (end.AddDays(tend) - epoch);
                    }
                    
                    sTime = Math.Floor(startTime.TotalMilliseconds).ToString();
                    eTime = Math.Floor(endTime.TotalMilliseconds).ToString();
                }
                else
                {
                    MessageBox.Show("データ挿入行からミッションを実行してください");
                    return;
                }
                
                // 再計算停止
                this.Application.Calculation = Excel.XlCalculation.xlCalculationManual;
                this.Application.ScreenUpdating = false;
                this.Application.DisplayAlerts = false;

                // ミッションをまとめて実行
                List<string> getMiList = new List<string>();
                List<int> getCol = new List<int>();
                for (int i = 4; i < 15000; i += 3) // 3 ->セルの結合数
                {
                    cell1 = this.Cells[6, i];                    
                    rng = this.Range[cell1, cell1];
                    string miName = (string)rng.Value;
                    if (!String.IsNullOrEmpty(miName))
                    {
                        if (miName == "-") continue;

                        m_userMissionDict.Add(miName, m_missionDict[miName]);
                        string[] miStr = miName.Split('.'); // 2015.07.24
                        getCol.Add(i);
                        getMiList.Add(miStr[0]);
                    }
                    else
                    {
                        Excel.Range nextCell1 = this.Cells[6, i + 3]; // 次のセル
                        string nextMi = (string)this.Range[nextCell1, nextCell1].Value;

                        if (String.IsNullOrEmpty(nextMi))
                        {
                            break;
                        }
                    }
                }

                foreach (string miName in getMiList.Distinct())
                {
                    ClsJsonData[] jsonData = null;
                    ClsUserMission.Missions mission = null;
                    foreach (ClsUserMission Missions in m_userMission)
                    {
                        if (Missions.children.Exists(x => x.name == miName))
                        {
                            mission = Missions.children.First(x => x.name == miName);
                        }
                    }

                    if (mission == null)
                    {                        
                        this.Application.ScreenUpdating = true;
                        this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                        this.Application.Calculate();
                        this.Application.DisplayAlerts = true;
                        MessageBox.Show("ミッションが存在しません(" + miName + ")");
                        return; // 処理終了
                    }
                    string missionId = mission.id;
                    jsonData = ClsJsonData.get(missionId, sTime, eTime, m_writeLog);

                    int count = 0;
                    foreach (string key in m_userMissionDict.Keys)
                    {
                        string[] miStr = key.Split('.'); // 2015.07.24
                        if (miStr.Count() > 0 && miStr[0] == miName)
                        {
                            int col = getCol[count];                            

                            this.CTMWriteSheet(key, jsonData);

                        }
                        count++;
                    }
                    jsonData = null;
                }

                //Mi登録状態表示処理
                for (int j = 4; j < 15000; j += 3) // 3 ->セルの結合数
                {
                    cell1 = this.Cells[6, j];                    
                    rng = this.Range[cell1, cell1];
                    if (!String.IsNullOrEmpty(rng.Value))
                    {
                        if (rng.Value == "-") continue;
                        this.Range[this.Cells[7, j], this.Cells[7, j]].Value = "完了";
                    }
                    else
                    {
                        Excel.Range nextCell1 = this.Cells[6, j + 3]; // 次のセル
                        string nextMi = (string)this.Range[nextCell1, nextCell1].Value;

                        if (String.IsNullOrEmpty(nextMi))
                        {
                            break;
                        }
                    }
                }

                //メインシートにデータを挿入する
                CalcurateSheet();
                
                // 更新処理
                this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                this.Application.Calculate();
                this.Application.ScreenUpdating = true;
                                
                //作業ウインドウにデータを送る
                Globals.ThisWorkbook.m_actionPanel.m_missionDict = m_userMissionDict;
                Globals.ThisWorkbook.m_actionPanel.AdditionCTMData();
                Globals.ThisWorkbook.m_actionPanel.controlClear();
                
                this.Activate();
                this.Application.DisplayAlerts = true;

                MessageBox.Show("ミッション取得処理が正常に完了しました。");
            }

            //Mi失敗時
            catch (Exception ex)
            {                
                MessageBox.Show(ex.Message);
                m_writeLog.OutPut("button_Click", ex.Message + ex.StackTrace);
                this.Application.DisplayAlerts = true;

                // 更新処理
                this.Application.ScreenUpdating = true;
                this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                this.Application.Calculate();

                //Mi登録状態表示処理
                for (int j = 4; j < 15000; j += 3) // 3 ->セルの結合数
                {
                    cell1 = this.Cells[6, j];
                    //cell2 = this.Cells[6, j + 3];
                    rng = this.Range[cell1, cell1];
                    if (!String.IsNullOrEmpty(rng.Value))
                    {
                        if (rng.Value == "-") continue;
                        this.Range[this.Cells[7, j], this.Cells[7, j]].Value = "処理中断";
                    }
                    else
                    {
                        Excel.Range nextCell1 = this.Cells[6, j + 3]; // 次のセル
                        string nextMi = (string)this.Range[nextCell1, nextCell1].Value;

                        if (String.IsNullOrEmpty(nextMi))
                        {
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// ミッション結果の書き込みを行う
        /// </summary>
        private void CTMWriteSheet(string miName, ClsJsonData[] jsonData)
        {
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;

            try
            {                
                int actMission_count = 0;
                int MissionValue_count = 0;
                //CTMを列挙
                string str = (actMission_count).ToString();             

                bool headderFlag = true;
                //CTMを列挙
                foreach (ClsJsonData CTM in jsonData)
                {
                    string str2 = (MissionValue_count).ToString();                    
                    //ミッションから対象のCTMを探す　見つかった場合のみ処理を行う                    
                    if (CTM.name == miName.Split('.')[1]) // 2015.07.24
                    {
                        //新規シートの作成
                        string SheetName = miName;
                        for (int i = 1; i <= Globals.ThisWorkbook.Worksheets.Count; i++)
                        {
                            if (SheetName == Globals.ThisWorkbook.Worksheets[i].Name)
                            {
                                Globals.ThisWorkbook.Worksheets[i].Delete();
                            }
                        }
                        Excel.Worksheet newWorksheet;
                        newWorksheet = (Excel.Worksheet)Globals.ThisWorkbook.Worksheets.Add(After: Globals.ThisWorkbook.Worksheets[Globals.ThisWorkbook.Worksheets.Count]);
                        newWorksheet.Name = SheetName;                        
                        newWorksheet.Select(1);

                        //CTMが上がっていなかった場合はループを抜ける
                        if (CTM.ctms.Count == 0)
                        {
                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            break;
                        }

                        //シート内にエレメントを挿入
                        int rowCount = 2;
                        int colCount = 0;
                        rng = (Excel.Range)Globals.ThisWorkbook.Application.Selection;
                        DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                        ClsCTMList myCTM = m_missionDict[miName];
                        rng[1, 1] = "No.";
                        //ISSUE_NO.748 Sunyi 2018/08/13 Start
                        //rng[1, 2] = "収集時刻";
                        rng[1, 2] = "RECEIVE TIME";
                        //ISSUE_NO.748 Sunyi 2018/08/13 End

                        //CTMを時間で分けて列挙
                        foreach (ClsJsonData.Ctms ctms in CTM.ctms)
                        {
                            
                            colCount = 3;
                            rng[rowCount, 1] = rowCount - 1;                            
                            rng[rowCount, 2] = epoch.AddMilliseconds(ctms.RT).ToString("M/d HH:mm:ss");

                            //CTM内のエレメントを列挙
                            foreach (string EleID in ctms.EL.Keys) // 1レコードのエレメント毎
                            {
                                
                                if (headderFlag)
                                {
                                    if (myCTM.children != null)
                                    {
                                        foreach (ClsCTMList.Children1 element in myCTM.children)
                                        {

                                            //動的に列を追加
                                            if (element != null && element.type == "el")
                                                //if (element != null && element.children != null)
                                            {
                                               if (element.id == EleID)
                                                {
                                                    //string elementName = element.children.Find(x => x.id == EleID).displayName[0].text;
                                                    string elementName = element.displayName[0].text;
                                                    rng[1, colCount] = elementName;
                                                    break;
                                                }
                                            }
                                        }
                                    
                                    }

                                    
                                }

                                if (ctms != null && ctms.EL != null && ctms.EL.Count > 0 && ctms.EL.ContainsKey(EleID) && ctms.EL[EleID] != null)
                                {

                                    if (String.IsNullOrEmpty(ctms.EL[EleID].FN))
                                    {
                                        rng[rowCount, colCount] = ctms.EL[EleID].V;
                                    }
                                    else
                                    {
                                        // バルキーファイル
                                        rng[rowCount, colCount] = ctms.EL[EleID].FN;
                                    }

                                }

                                colCount++;
                            }
                            headderFlag = false;
                            rowCount++;
                        }

                        //エレメントの計算値入力                        
                        rng[rowCount, 2] = "個数";

                        rng[rowCount + 1, 2] = "積算";
                        rng[rowCount + 2, 2] = "平均";
                        rng[rowCount + 3, 2] = "最大";
                        rng[rowCount + 4, 2] = "最小";
                        rng[rowCount + 5, 2] = "種類数";

                        for (int i = 3; i < colCount; i++)
                        {                        
                            string ColLetterTemp = newWorksheet.Cells[rowCount, i].Address;
                            string[] SplitLetter = ColLetterTemp.Split('$');
                            string ColLetter = SplitLetter[1];
                            rng[rowCount, i].Formula = string.Format("=COUNTA({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 1, i].Formula = string.Format("=SUM({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}2:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            rng[rowCount + 3, i].Formula = string.Format("=MAX({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 4, i].Formula = string.Format("=MIN({0}2:{0}{1})", ColLetter, rowCount - 1);                        
              
                        }
                        //罫線表示処理
                        cell1 = newWorksheet.Cells[1, 1];
                        cell2 = newWorksheet.Cells[rowCount - 1, 1];
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        cell1 = newWorksheet.Cells[1, 2];
                        cell2 = newWorksheet.Cells[rowCount + 5, colCount - 1];
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //セルサイズを自動調整
                        newWorksheet.Columns.AutoFit();
                    }
                    MissionValue_count++;
                }

                actMission_count++;

            }
            catch (Exception ex)
            {                
                MessageBox.Show(ex.Message);
                m_writeLog.OutPut("CTMSheetWrite", ex.Message + ex.StackTrace);
                Globals.ThisWorkbook.Application.DisplayAlerts = true;
            }


        }



        /// <summary>
        /// メインシートデータ挿入処理
        /// </summary>
        private void CalcurateSheet()
        {
            Excel.Range rng = null;
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;

            try
            {
                cell1 = this.Cells[m_activeRange.Row, 2];

                for (int i = 8; i <= 15000; i++)
                {
                    //演算行の位置を探す
                    cell1 = this.Cells[i, 2];
                    if (!(cell1.Value is string))
                    {
                        continue;
                    }
                    if (!(cell1.Value is double) && cell1.Value == "演算行/定数")
                    {
                        rng = this.Range[cell1, cell1].EntireRow;
                        break;
                    }
                }

                //演算行が見つからなかった場合は処理を抜ける
                if (rng == null)
                {
                    return;
                }

                //行位置の基準を設定する
                int rowPoint = rng.Row;

                //演算行の各セルを見て演算を行う
                for (int i = 3; i <= 15000; i++)
                {
                    /*
                     * 検索する文字列の形式は"シート名,列名,行名"+(x * "シート名,列名,行名")　の形式
                     */
                    cell1 = this.Cells[rowPoint, i];
                    cell2 = this.Cells[m_activeRange.Row, i];

                    bool dataNothingChk = false; // データシートにデータがあるかチェック
                    try
                    {
                        if (cell1.Value != null)
                        {
                            string compStr = cell1.Value.ToString();
                            compStr = compStr.Replace("\"", "?\"?");
                            compStr = compStr.Replace("\"", "?\"?");
                            string[] culcStr = compStr.Split('?');

                            //シート検索フォーマットが入力されていた場合、計算を行う
                            if (culcStr.Length > 1)
                            {
                                for (int j = 0; j < culcStr.Length; j++)
                                {
                                    string[] culcSheet = culcStr[j].Split(',');
                                    //シート検索フォーマットが入力されていた場合は以下の処理を行う
                                    if (culcSheet.Length == 3)
                                    {
                                        Excel.Worksheet culcWorkSheet = Globals.ThisWorkbook.Worksheets[culcSheet[0]];
                                        if (culcWorkSheet.Cells[1, 1].Value.ToString() == "データ無し")
                                        {
                                            dataNothingChk = true;
                                            cell2.Value = 0; // データがない場合は0を入れる
                                            break;
                                        }
                                        int col = culcWorkSheet.Rows[1].Find(culcSheet[1]).Column;
                                        int row = culcWorkSheet.Columns[2].Find(culcSheet[2]).Row;
                                        if (culcSheet[2] == "種類数")
                                        {
                                            string ColLetterTemp = culcWorkSheet.Cells[row, col].Address;
                                            string[] SplitLetter = ColLetterTemp.Split('$');
                                            string ColLetter = SplitLetter[1];
                                            culcWorkSheet.Cells[row, col].FormulaArray = string.Format("=SUM(1/COUNTIF({0}2:{0}{1},{0}2:{0}{1}))", ColLetter, row - 6);
                                        }
                                        culcStr[j] = culcWorkSheet.Cells[row, col].Value.ToString();
                                    }
                                }

                                if (dataNothingChk != true) // 取得データがあれば処理続行
                                {
                                    bool kakFlag = true;
                                    for (int j = 0; j < culcStr.Length; j++)
                                    {
                                        if (culcStr[j] == "\"")
                                        {
                                            if (kakFlag)
                                            {
                                                culcStr[j] = "(";
                                                kakFlag = false;
                                            }
                                            else
                                            {
                                                culcStr[j] = ")";
                                                kakFlag = true;
                                            }
                                        }
                                    }

                                    //この時点で数値型と四則演算式の文字列になる
                                    string resultStr = String.Join(null, culcStr);
                                    cell2.Value = "=" + resultStr;
                                }
                                
                            }
                            //フォーマットが入力されていなかった場合はエクセルの基本の式を動かす
                            else
                            {
                                cell2.Value = String.Format("=\"{0}\"", compStr);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        m_writeLog.OutPut("calcurateSheet", ex.Message + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("calcurateSheet", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// 【イベント】サンプル表示ボタン押下時処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDefault"></param>
        private void btn_Sample_Click(Office.CommandBarButton ctrl, ref bool CancelDefault)
        {
            try
            {
                Office.CommandBarButton btnObj = ctrl;

                if (ctrl.Caption != "サンプル表示" && ctrl.Caption != "サンプル非表示")
                {
                    return;
                }

                if (btnObj.Caption == "サンプル表示")
                {
                    Globals.Sheet2.sampleOpen();
                    btnObj.Caption = "サンプル非表示";
                }
                else
                {
                    Globals.Sheet2.sampleClose();
                    btnObj.Caption = "サンプル表示";
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("btn_Sample_Click", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// アクションパネル開閉処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDedfault"></param>
        private void button_Action_Click(Office.CommandBarButton ctrl, ref bool CancelDedfault)
        {
            try
            {
                if (ctrl.Caption != "アクションパネル開閉")
                {
                    return;
                }

                if (Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane)
                {
                    Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = false;
                }
                else
                {
                    Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("button_Action_Click", ex.Message + ex.StackTrace);
            }
        }


        /// <summary>
        /// ワークブック保存前イベント
        /// 次回起動時に問題が起きないように、各コントロール・入力規則を削除する
        /// </summary>
        /// <param name="SaveAsUI">名前を付けて保存かどうか</param>
        /// <param name="Cancel">Excel動作による保存フラグ</param>
        private void Workbook_BeforeSave(bool SaveAsUI, ref bool Cancel)
        {
            try
            {

                //保存先のファイルパスを指定する                
                string filePath = Path.GetDirectoryName(Globals.ThisWorkbook.Path);                
                FileSystem.ChDir(filePath);

                string fullPath = string.Empty;
                if (filePath.Contains("AIS_Template") == true)
                {
                    fullPath = filePath + @"\ExcelResult";  //上書き保存時のフォルダ比較用  
                }
                else
                {
                    fullPath = filePath + @"\AIS_Template\ExcelResult";  //上書き保存時のフォルダ比較用  
                }                
                FileSystem.ChDir(fullPath);       //移動                

                //上書き保存と名前付き保存で処理を分ける
                if (SaveAsUI)
                {                    
                    //名前を付けて保存
                    var FileName = Application.GetSaveAsFilename(Globals.ThisWorkbook.Name, "Excelファイル(*.xlsx),*.xlsx");

                    if (FileName is bool && FileName == false)  //キャンセル時はbool型が返る
                    {
                        Cancel = true;
                        return;
                    }
                    else  //保存時はファイル名が返る
                    {
                        Cancel = true;

                        //保存用コントロール初期化
                        this.SaveInitialize();

                        //セーブ実行
                        this.Application.EnableEvents = false;
                        Globals.ThisWorkbook.SaveAs(FileName);
                        this.Application.EnableEvents = true;
                        //コントロール類をもとに戻す
                        this.WorkbookAfterSave();
                    }
                }
                //名前をつけて保存時
                else
                {                    
                    //規定のフォルダか確認
                    if (Path.GetFullPath(Globals.ThisWorkbook.Path) != fullPath)
                    {
                        MessageBox.Show("このフォルダ上で上書きできません");
                        Cancel = true;
                        return;
                    }
                    else
                    {
                        Cancel = true;

                        this.SaveInitialize();

                        //セーブ実行　誤動作回避でイベントを切る
                        this.Application.EnableEvents = false;
                        Globals.ThisWorkbook.Save();
                        this.Application.EnableEvents = true;

                        //再読み込み
                        this.WorkbookAfterSave();
                    }
                }

            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("button_Action_Click", ex.Message + ex.StackTrace);
            }

        }


        /// <summary>
        /// セーブ時のコントロール初期化
        /// </summary>
        public void SaveInitialize()
        {
            //入力規則を廃棄する
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;
            Excel.Range cell1_Mi = null;
            Excel.Range cell2_MI = null;
            Excel.Range rng_Mi = null;

            int count = 0;
            cell1 = this.Cells[6, 4];

            //ミッション選択部分の入力規則を削除する
            while (cell1.Value != null)
            {
                cell1 = this.Cells[6, 4 + count];
                cell2 = this.Cells[6, 6 + count];
                if (cell1.Value == null) break;
                rng = this.Range[cell1, cell2];
                rng.Validation.Delete();

                cell1_Mi = this.Cells[5, 4 + count];
                cell2_MI = this.Cells[5, 6 + count];
                if (cell1.Value == null) break;
                rng_Mi = this.Range[cell1_Mi, cell2_MI];
                rng_Mi.Validation.Delete();

                count += 3;
            }

            //コマンドメニューのボタン類を削除
            if (m_menuButton != null)
            {
                m_menuButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
                m_menuButton.Delete();
                m_menuButton = null;
            }
            if (m_SampleButton != null)
            {
                m_SampleButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);
                m_SampleButton.Delete();
                m_SampleButton = null;
            }
            if (m_actionButton != null)
            {
                m_actionButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);
                m_actionButton.Delete();
                m_actionButton = null;
            }     
           
        }

        /// <summary>
        /// ワークブック保存後再読込関数
        /// </summary>
        private void WorkbookAfterSave()
        {
            //ロードフラグオン
            flagLoading = true;
            //読込開始            
            this.InitializeSheet();
        }
    }
#endif
}
