﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.IO;

namespace AIStool_StatusMonitor
{
    /// <summary>
    /// ユーザーの実行できるミッションを格納するクラス
    /// </summary>
    [DataContract]
    class ClsUserMission
    {
        /// <summary>
        /// エージェントの名前
        /// </summary>
        [DataMember]
        public string name { get; private set; }

        /// <summary>
        /// エージェントのID
        /// </summary>
        [DataMember]
        public string Id { get; private set; }

        [DataMember]
        public string t { get; private set; }

        [DataMember]
        public bool isShared { get; private set; }

        [DataMember]
        public List<object> agentUserList { get; set; }

        [DataMember]
        public List<Missions> children { get; private set; }

        [DataContract]
        public class Missions
        {
            /// <summary>
            /// ミッションのID
            /// </summary>
            [DataMember]
            public string id { get; set; }

            /// <summary>
            /// ミッションの名前
            /// </summary>
            [DataMember]
            public string name { get; set; }

            /// <summary>
            /// ミッションの形式
            /// </summary>
            [DataMember]
            public string missionType { get; set; }

            [DataMember]
            public string t { get; private set; }

            [DataMember]
            public List<object> missionUserList { get; set; }
        }


        /// <summary>
        /// 実行可能ミッションを取得する
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static ClsUserMission[] get()
        {
            WebClient wc = new WebClient();
            string HostName = Globals.Sheet1.m_MissionHostName;
            string PortNo = Globals.Sheet1.m_MissionPortNo;
            string url = String.Format("http://{0}:{1}/cms/rest/mib/mission/", HostName, PortNo);

            wc.Encoding = Encoding.UTF8;
            string MiStr = wc.DownloadString(url);

            ClsUserMission[] userMission = JsonConvert.DeserializeObject<ClsUserMission[]>(MiStr);
            return userMission;
        }


        /// <summary>
        /// ユーザーIDを指定して実行可能ミッションを取得する
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static ClsUserMission[] get(string userId)
        {

            string HostName = Globals.Sheet1.m_MissionHostName;
            string PortNo = Globals.Sheet1.m_MissionPortNo;
            string url = String.Format("http://{0}:{1}/cms/rest/mib/mission/?withAgent=true", HostName, PortNo);

            //webrequestを投げ、responseを受ける
            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
            req.CookieContainer = new CookieContainer();
            Cookie idInfo = new Cookie("userId", userId);

            //Cookieを格納する
            req.CookieContainer.Add(new Uri(url), idInfo);

            WebResponse res = req.GetResponse();
            string miStr = "";
            using (Stream stream = res.GetResponseStream())
            {
                using (StreamReader miStream = new StreamReader(stream, Encoding.UTF8))
                {
                    miStr = miStream.ReadToEnd();
                }
            }

            ClsUserMission[] userMission = JsonConvert.DeserializeObject<ClsUserMission[]>(miStr);
            return userMission;

        }
    }
}
