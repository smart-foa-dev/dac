﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using log4net;
using Updater.Task;

namespace Updater
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        private Moniter monitor;

        private GraphAlarm GraphAlarm;

        public MainWindow()
        {
            InitializeComponent();

            if (!App.mutex.WaitOne(0, false))
            {
                Application.Current.Shutdown();
                return;
            }

            this.monitor = new Moniter();
            this.monitor.Start();
            this.GraphAlarm = new GraphAlarm();
            this.GraphAlarm.Start();
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(MainWindow));
    }
}
