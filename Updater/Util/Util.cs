﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace Updater.Util
{
    class UpdaterUtil
    {
        [DllImport("user32.dll")]
        static extern int GetWindowThreadProcessId(int hWnd, out int lpdwProcessId);

        public static readonly string EXTENSION_ZIP = ".zip";
        public static readonly string EXTENSION_DAT = ".dat";

        public static bool WaitForFile(string file)
        {
            while (true)
            {
                try
                {
                    using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.None, 100))
                    {
                        fs.Close();
                        break;
                    }
                }
#pragma warning disable
                catch (Exception e)
#pragma warning restore
                {
                }

                System.Threading.Thread.Sleep(200);
            }

            return true;
        }

        public static bool isZipFile(string path)
        {
            return Path.GetExtension(path).Equals(EXTENSION_ZIP, StringComparison.OrdinalIgnoreCase);
        }

        public static bool isDatFile(string path)
        {
            return Path.GetExtension(path).Equals(EXTENSION_DAT, StringComparison.OrdinalIgnoreCase);
        }

        public static void TerminateExcelProcess(Microsoft.Office.Interop.Excel.Application excel)
        {
            int id;
            GetWindowThreadProcessId(excel.Hwnd, out id);
            Process process = Process.GetProcessById(id);
            if (process != null)
            {
                process.Kill();
            }
        }

    }
}
