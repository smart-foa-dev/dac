﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Updater.TextWrapperTransfer
{
    class CopyInfo
    {
        public string src;
        public string dest;
        public string user;
        public string password;
        public long oldest;
    }
}
