﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using Updater.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using FoaCore.Common.Util;

namespace Updater.TextWrapperTransfer
{
    class FileMerger
    {
        private static readonly int DATA_START_ROW = 4;
        protected static Dictionary<string, CopyInfo> infos = new Dictionary<string, CopyInfo>();
        private static log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public FileMerger()
        {
        }

        public static CopyInfo Merge(string src)
        {
            lock (infos)
            {
                string txt = Path.ChangeExtension(src, ".txt");
                string[] lines = File.ReadAllLines(txt);
                
                // get copy info
                string dest = lines[0];      // text file path in text wrapper
                string user = lines[1];      // user
                string password = lines[2];  // password

                // get source file parts( file name, time, guid )
                string[] parts = Path.GetFileNameWithoutExtension(src).Split('_');

                // create file will be transfered
                CopyInfo info;
                if (!infos.ContainsKey(dest)) {
                    string transferfile = parts[0] + "_" + new GUID().ToString() + ".txt";
                    info = new CopyInfo() { src = transferfile, dest = dest, user = user, password = password, oldest = int.Parse(parts[1]) };
                    infos.Add(dest, info);
                    SaveInfoJson();
                }
                // append content
                else
                {
                    info = infos[dest];
                }
                WriteData(lines, info);

                // delete source files
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        File.Delete(src);
                    }
#pragma warning disable
                    catch (Exception e)
#pragma warning restore
                    {
                        Thread.Sleep(500);
                        if (i == 4)
                        {
                            logger.Warn("Can not delete 5 times file=" + src);
                        }
                        continue;
                    }
                    break;
                }
                for (int i = 0; i < 5; i++)
                {
                    try
                    {
                        File.Delete(txt);
                    }
#pragma warning disable
                    catch (Exception e)
#pragma warning restore
                    {
                        Thread.Sleep(500);
                        if (i == 4)
                        {
                            logger.Warn("Can not delete 5 times file=" + txt);                            
                        }
                        continue;
                    }
                    break;
                }
                return info;
            }
        }

        public static void WriteData(string[] lines, CopyInfo info)
        {
            using (StreamWriter ws = File.AppendText(info.src))
            {
                for (int i = DATA_START_ROW-1; i < lines.Length; i++)
                {
                    ws.WriteLine(lines[i]);
                }
            }
        }

        public static void RemoveInfo(string key)
        {
            infos.Remove(key);
            SaveInfoJson();
        }

        public static void SaveInfoJson()
        {
            File.WriteAllText(UpdaterConf.Config.ManageJsonPath, JsonConvert.SerializeObject(infos).ToString(), new UTF8Encoding(true));
        }
    }
}
