﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Updater.Model;
using log4net;
using System.Threading;

namespace Updater.TextWrapperTransfer
{
    class AlarmFileMonitor
    {
        private Thread _updateThread;
        private bool _shutdown = false;

        public void star()
        {
            logger.Debug("Thread for graph alarm running start.");
            this._updateThread = new Thread(new ThreadStart(MonitorProc));
            this._updateThread.Start();
        }

        public void shutdown()
        {
            _shutdown = true;
        }

        private void MonitorProc()
        {
            while (!_shutdown)
            {
                List<CopyInfo> infos = new List<CopyInfo>();

                foreach (string file in Directory.EnumerateFiles(UpdaterConf.Config.AlarmParamPath, "*.c")) {
                    CopyInfo info = FileMerger.Merge(file);
                    infos.Add(info);
                }

                foreach (CopyInfo info in infos)
                {
                    ITransfer transfer = TextWrapperTransferFactory.GetTransfer(info);
                    transfer.Copy(info);
                }

                Thread.Sleep(UpdaterConf.Config.AlarmFileMonitorIntervalMS);
            }
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(AlarmFileMonitor));
    }
}
