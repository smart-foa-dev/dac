﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Updater.TextWrapperTransfer
{
    abstract class Transfer : ITransfer
    {
        public Transfer()
        {
        }

        public virtual void Copy(CopyInfo info)
        {
            Uri dest = new Uri(info.dest);

            if (IsDestFileExists(dest, info.user, info.password))
            {
                // TODO : write log

                SendTransterError(info);
            }
            else
            {
                try
                {
                    Copy(info.src, dest, info.user, info.password);
                }
#pragma warning disable
                catch (Exception e)
#pragma warning restore
                {
                    // TODO : write log

                    SendTransterError(info);
                }
            }
        }

        abstract protected bool IsDestFileExists(Uri dest, string user, string password);

        abstract protected void Copy(string src, Uri dest, string user, string password);

        protected void SendTransterError(CopyInfo info)
        {
            try
            {
                // TODO : send system ctm
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {
                // TODO : write log
            }
        }
    }
}
