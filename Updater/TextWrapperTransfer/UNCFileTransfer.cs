﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Updater.TextWrapperTransfer
{
    class UNCFileTransfer : Transfer
    {
        public UNCFileTransfer()
            : base()
        {
        }

        protected override bool IsDestFileExists(Uri dest, string user, string password)
        {
            if (string.IsNullOrEmpty(user))
            {
                return File.Exists(dest.LocalPath);
            }
            else
            {
                using (new NetworkImpersonationContext(dest.Host, user, password))
                {
                    return File.Exists(dest.LocalPath);
                }
            }
        }

        protected override void Copy(string src, Uri dest, string user, string password)
        {
            if (string.IsNullOrEmpty(user))
            {
                File.Copy(src, dest.LocalPath, true);
            }
            else
            {
                using (new NetworkImpersonationContext(dest.Host, user, password))
                {
                    File.Copy(src, dest.LocalPath, true);
                }
            }
        }
    }
}
