﻿using System;
using System.Collections.Generic;

namespace Updater.TextWrapperTransfer
{
    class TextWrapperTransferFactory
    {
        enum TransferType {
            UNC = 0,
            FTP = 1,
            File = 2
        }

        private static Dictionary<TransferType, ITransfer> transfers = new Dictionary<TransferType, ITransfer>()
        {
            {TransferType.UNC, new UNCFileTransfer()},
            {TransferType.FTP, new FTPFileTransfer()},
            {TransferType.File, new FileTransfer()},
        };
        
        public TextWrapperTransferFactory()
        {
        }

        public static ITransfer GetTransfer(CopyInfo info)
        {
            Uri dest = new Uri(info.dest);

            if (dest.IsUnc)
            {
                return transfers[TransferType.UNC];
            }
            else if (dest.Scheme.Equals(Uri.UriSchemeFtp))
            {
                return transfers[TransferType.FTP];
            }
            else if (dest.Scheme.Equals(Uri.UriSchemeFile))
            {
                return transfers[TransferType.File];
            }
            else
            {
                throw new NotSupportedException();
            }
        }
    }
}
