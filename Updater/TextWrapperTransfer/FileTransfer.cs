﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Updater.TextWrapperTransfer
{
    class FileTransfer : Transfer
    {
        public FileTransfer()
            : base()
        {
        }

        protected override bool IsDestFileExists(Uri dest, string user, string password)
        {
            return File.Exists(dest.LocalPath);
        }

        protected override void Copy(string src, Uri dest, string user, string password)
        {
            File.Copy(src, dest.LocalPath, false);
        }
    }
}
