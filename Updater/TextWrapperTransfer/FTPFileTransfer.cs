﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace Updater.TextWrapperTransfer
{
    class FTPFileTransfer : Transfer
    {
        public FTPFileTransfer()
            : base()
        {
        }

        protected override bool IsDestFileExists(Uri dest, string user, string password)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(dest);
            if (!string.IsNullOrEmpty(user))
            {
                request.Credentials = new NetworkCredential(user, password);
            }
            request.Method = WebRequestMethods.Ftp.GetFileSize;

            try
            {
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
            }
            catch (WebException ex)
            {
                FtpWebResponse response = (FtpWebResponse)ex.Response;
                if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                {
                    return false;
                }
            }

            return true;
        }

        protected override void Copy(string src, Uri dest, string user, string password)
        {
            // authenticate
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create(dest);
            if (!string.IsNullOrEmpty(user))
            {
                request.Credentials = new NetworkCredential(user, password);
            }
            request.UseBinary = true;
            request.Method = WebRequestMethods.Ftp.UploadFile;

            // upload file
            using (Stream st = request.GetRequestStream())
            using (FileStream fs = new FileStream(src, FileMode.Open))
            {
                Byte[] buf = new Byte[1024];
                int count = 0;

                do
                {
                    count = fs.Read(buf, 0, buf.Length);
                    if (count != 0)
                    {
                        st.Write(buf, 0, count);
                    }
                } while (count != 0);
            }
        }
    }
}
