﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Updater.Model
{
    public class EntityBase
    {
        /**
         * JsonNode判別用タイプ
         */
        public string t;

        /**
         * 作成ユーザID
         */
        public string userCreated;

        /**
         * 作成日時
         */
        public long dateCreated;

        /**
         * 更新ユーザID
         */
        public string userUpdated;

        /**
         * 更新日時
         */
        public long dateUpdated;

        /**
         * バージョン
         */
        public int version;
    }
}
