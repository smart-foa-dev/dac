﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Updater.Model
{
    public class TblWorkPlaceMission : EntityBase
    {
        [JsonProperty("offlineEnd")]
	    public long? condition_offline_end;

        [JsonProperty("offlineStart")]
        public long? condition_offline_start;

        [JsonProperty("onlinePeriod")]
        public int? condition_online_period;

        [JsonProperty("onlineTime")]
        public int? condition_online_time;

        [JsonProperty("mission_data")]
        public string mission_data;

        [JsonProperty("id")]
        public string mission_id;

        [JsonProperty("aisSearchType")]
        public string mission_period_type;

        [JsonProperty("missionNewType")]
        public string mission_type;

        [JsonProperty("work_place_id")]
        public string work_place_id;
    }
}
