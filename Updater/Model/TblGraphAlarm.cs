﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Updater.Model
{
    class TblGraphAlarm : EntityBase
    {
        [JsonProperty("file_id")]
        public string file_id;

        [JsonProperty("file_path")]
        public string file_path;

        [JsonProperty("refresh_period")]
        public int refresh_period;
    }
}
