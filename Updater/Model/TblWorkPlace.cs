﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Updater.Model
{
    class TblWorkPlace : EntityBase
    {
        [JsonProperty("work_place_id")]
        public string work_place_id;

        [JsonProperty("online_data_upd_time")]
        public long? online_data_upd_time;

        [JsonProperty("reference_time")]
        public long? reference_time;

        [JsonProperty("work_place_flag")]
        public int? work_place_flag;

        [JsonProperty("work_place_name")]
        public string work_place_name;
    }
}
