﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using log4net;
using Updater.Util;
using Updater.Task;
using System.IO.Compression;

namespace Updater.Model
{
    class TargetFileManager
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(TargetFileManager));

        private static readonly string EXTENSION_ZIP = ".zip";
        private static readonly string EXTENSION_DAT = ".dat";
        private static readonly string EXTENSION_XLSM = ".xlsm";

        private TblWorkPlaceReference refer;
        private string originalFile;
        private string localFile;
        private string targetFile;
        private FileType fileType;
        private DateTime lastOriginalFileWriteTime = DateTime.MinValue;

        public enum ActionStatus
        {
            OriginalFileNotExist,
            Error,
            Success
        };

        public enum FileType
        {
            Data,
            Zip,
            Unknown
        };

        public TargetFileManager(TblWorkPlaceReference refer)
        {
            this.refer = refer;
            this.fileType = FileType.Unknown;
            if (SetOriginTargetFile().Equals(ActionStatus.Success))
            {
                SetLocalTargetFile();
            }
        }

        public ActionStatus UpdateTargetFile()
        {
            logger.Debug("UpdateTargetFile " + this.targetFile);

            // copy file to local
            ActionStatus ret = CopyOriginFileToLocal();
            if (!ret.Equals(ActionStatus.Success))
            {
                return ret;
            }

            // call update macro
            Microsoft.Office.Interop.Excel.Application excel = null;
            Microsoft.Office.Interop.Excel.Workbooks workbooks = null;
            Microsoft.Office.Interop.Excel.Workbook workbook = null;
            try
            {
                logger.Debug("Open excel file " + this.targetFile);

                UpdaterUtil.WaitForFile(this.targetFile);

                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;
                workbooks = excel.Workbooks;
                workbook = workbooks.Open(this.targetFile);
                excel.Run("Main_Module.GetMissionInfoAllAuto", "0");
                workbook.Save();
            }
            catch (Exception e)
            {
                logger.Error("UpdateTargetFile error.", e);
                ret = ActionStatus.Error;
                return ret;
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close(false);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbook);
                    workbook = null;
                }

                if (workbooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbooks);
                    workbooks = null;
                }

                if (excel != null)
                {
                    excel.Quit();
                    UpdaterUtil.TerminateExcelProcess(excel);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
                    excel = null;
                }
            }

            // copy file to origin
            return CopyLocalFileToOrigin();
        }

        private ActionStatus CopyLocalFileToOrigin()
        {
            try
            {
                // forbid to overwrite user modified file (TODO : need to synchronize)
                if (this.fileType.Equals(FileType.Data))
                {
                    DateTime lastWriteTime = File.GetLastWriteTimeUtc(this.originalFile);
                    if (lastWriteTime <= this.lastOriginalFileWriteTime)
                    {
                        File.Copy(this.localFile, this.originalFile, true);
                        this.lastOriginalFileWriteTime = File.GetLastWriteTimeUtc(this.originalFile);
                    }
                }
                else
                {
                    string zipFileName = Path.GetFileName(this.targetFile);
                    using (ZipArchive archive = ZipFile.Open(this.originalFile, ZipArchiveMode.Update))
                    {
                        ZipArchiveEntry targetFile = null;
                        foreach (ZipArchiveEntry file in archive.Entries)
                        {
                            if (file.FullName.Equals(zipFileName, StringComparison.OrdinalIgnoreCase) && file.LastWriteTime.DateTime <= this.lastOriginalFileWriteTime)
                            {
                                targetFile = file;
                                break;
                            }
                        }
                        if (targetFile != null)
                        {
                            targetFile.Delete();
                            ZipFileExtensions.CreateEntryFromFile(archive, this.targetFile, zipFileName);
                            this.lastOriginalFileWriteTime = targetFile.LastWriteTime.DateTime;
                        }
                        else
                        {
                            logger.Warn(string.Format("CopyLocalFileToOrigin warning : target file {0} didn't exist in zip {1}.", zipFileName, this.originalFile));
                            return ActionStatus.OriginalFileNotExist;
                        }
                    }
                }
            }
            catch (DirectoryNotFoundException dnf)
            {
                logger.Error("CopyLocalFileToOrigin error.", dnf);
                return ActionStatus.OriginalFileNotExist;
            }
            catch (FileNotFoundException fnf)
            {
                logger.Error("CopyLocalFileToOrigin error.", fnf);
                return ActionStatus.OriginalFileNotExist;
            }
            catch (Exception e)
            {
                logger.Error("CopyLocalFileToOrigin error.", e);
                return ActionStatus.Error;
            }
            return ActionStatus.Success;
        }

        private void UnzipTargetFile()
        {
            string unzipPath = Path.GetDirectoryName(this.targetFile);
            string unzipFileName = Path.GetFileName(this.targetFile);
            Directory.CreateDirectory(unzipPath);

            using (ZipArchive archive = ZipFile.OpenRead(this.localFile))
            {
                foreach (ZipArchiveEntry file in archive.Entries)
                {
                    if (file.FullName.Equals(unzipFileName, StringComparison.OrdinalIgnoreCase) && file.LastWriteTime.DateTime > this.lastOriginalFileWriteTime)
                    {
                        ZipFileExtensions.ExtractToFile(file, this.targetFile, true);
                        this.lastOriginalFileWriteTime = file.LastWriteTime.DateTime;
                    }
                }
            }
        }

        private ActionStatus CopyOriginFileToLocal() {
            try
            {
                logger.Debug("CopyOriginFileToLocal begin.");

                if (string.IsNullOrEmpty(this.originalFile))
                {
                    return ActionStatus.OriginalFileNotExist;
                }

                if (this.fileType.Equals(FileType.Unknown))
                {
                    return ActionStatus.OriginalFileNotExist;
                }
                else if (this.fileType.Equals(FileType.Data))
                {
                    DateTime lastWriteTime = File.GetLastWriteTimeUtc(this.originalFile);
                    if (lastWriteTime > this.lastOriginalFileWriteTime)
                    {
                        logger.Debug("copy file from " + this.originalFile + " to " + this.localFile);
                        Directory.CreateDirectory(UpdaterConf.Config.UpdateDataPath);
                        File.Copy(this.originalFile, this.localFile, true);
                        this.lastOriginalFileWriteTime = lastWriteTime;
                    }
                }
                else if (this.fileType.Equals(FileType.Zip))
                {
                    DateTime lastWriteTime = File.GetLastWriteTimeUtc(this.originalFile);
                    if (lastWriteTime > this.lastOriginalFileWriteTime)
                    {
                        UnzipTargetFile();
                    }
                }
            }
            catch (DirectoryNotFoundException dnf)
            {
                logger.Error("CopyOriginFileToLocal error.", dnf);
                return ActionStatus.OriginalFileNotExist;
            }
            catch (FileNotFoundException fnf)
            {
                logger.Error("CopyOriginFileToLocal error.", fnf);
                return ActionStatus.OriginalFileNotExist;
            }
            catch (Exception e)
            {
                logger.Error("CopyOriginFileToLocal error.", e);
                return ActionStatus.Error;
            }
            logger.Debug("CopyOriginFileToLocal end.");
            return ActionStatus.Success;
        }

        private ActionStatus SetLocalTargetFile()
        {
            string fileName = Path.GetFileNameWithoutExtension(this.originalFile);
            if (this.fileType.Equals(FileType.Data))
            {
                this.localFile = Path.Combine(UpdaterConf.Config.UpdateDataPath, fileName + EXTENSION_XLSM);
                this.targetFile = this.localFile;
            }
            else if (this.fileType.Equals(FileType.Zip))
            {
                this.localFile = Path.Combine(UpdaterConf.Config.UpdateDataPath, this.originalFile);
                this.targetFile = Path.Combine(UpdaterConf.Config.UpdateDataPath, fileName, this.refer.file_name);
            }
            return ActionStatus.Success;
        }

        private ActionStatus SetOriginTargetFile()
        {
            string datFile = Path.Combine(UpdaterConf.Config.GripDataPath, this.refer.file_id) + EXTENSION_DAT;
            string zipFile = Path.Combine(UpdaterConf.Config.GripDataPath, this.refer.file_id) + EXTENSION_ZIP;

            if (File.Exists(datFile))
            {
                this.originalFile = datFile;
                this.fileType = FileType.Data;
            }
            else if (File.Exists(zipFile))
            {
                this.originalFile = zipFile;
                this.fileType = FileType.Zip;
            }
            else
            {
                this.originalFile = string.Empty;
                return ActionStatus.OriginalFileNotExist;
            }

            return ActionStatus.Success;
        }

    }
}
