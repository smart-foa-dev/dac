﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Updater.Model
{
    public class WorkplaceTarget
    {
        public string work_place_id;
        public List<TblWorkPlaceMission> missions;
        public List<TblWorkPlaceReference> refs;
    }
}
