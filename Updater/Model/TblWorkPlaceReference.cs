﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Updater.Task;

namespace Updater.Model
{
    public class TblWorkPlaceReference : EntityBase
    {
        [JsonProperty("file_id")]
        public string file_id;

        [JsonProperty("file_name")]
        public string file_name;

        [JsonProperty("file_type")]
        public string file_type;

        [JsonProperty("reference_count")]
        public int? reference_count;

        [JsonProperty("work_place_id")]
        public string work_place_id;

        public UpdateTask updateTask = null;

        public override bool Equals(Object obj)
        {
            TblWorkPlaceReference refer = obj as TblWorkPlaceReference;
            if (refer != null) {
                return this.work_place_id.Equals(refer.work_place_id) &&
                    this.file_id.Equals(refer.file_id) &&
                    this.file_name.Equals(refer.file_name, StringComparison.OrdinalIgnoreCase);
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (work_place_id + file_id + file_name).GetHashCode();
        }
    }
}
