﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using log4net;
using Updater.Util;

namespace Updater.Model
{
    class GraphAlarmFileManager
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(GraphAlarmFileManager));

        private static readonly string EXTENSION_ZIP = ".zip";
        private static readonly string EXTENSION_DAT = ".dat";
        private static readonly string EXTENSION_XLSM = ".xlsm";

        private GraphAlarmTarget refer;
        private string originalFile;
        private string localFile;
        private DateTime lastOriginalFileWriteTime = DateTime.MinValue;

        public enum ActionStatus
        {
            OriginalFileNotExist,
            Error,
            Success
        };

        public GraphAlarmFileManager(GraphAlarmTarget refer)
        {
            this.refer = refer;
            if (SetOriginTargetFile().Equals(ActionStatus.Success))
            {
                SetLocalTargetFile();
            }
        }

        public ActionStatus UpdateGraphAlarmFile()
        {
            logger.Debug("UpdateTargetFile " + this.originalFile);
            // TODO : if all starts, ends of all missions is invalid, don't do this action

            // copy file to local
            ActionStatus ret = CopyOriginFileToLocal();
            if (!ret.Equals(ActionStatus.Success))
            {
                return ret;
            }

            // call update macro
            Microsoft.Office.Interop.Excel.Application excel = null;
            Microsoft.Office.Interop.Excel.Workbooks workbooks = null;
            Microsoft.Office.Interop.Excel.Workbook workbook = null;
            try
            {
                logger.Debug("Open excel file " + this.localFile);
                UpdaterUtil.WaitForFile(this.localFile);
                excel = new Microsoft.Office.Interop.Excel.Application();

                excel.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;
                excel.Visible = false;

                workbooks = excel.Workbooks;
                workbook = workbooks.Open(this.localFile);
                excel.Run("GraphSet_Module.Graph_Alarm_Check");
                workbook.Save();

            }
            catch (Exception e)
            {
                logger.Error("UpdateTargetFile error.", e);
                ret = ActionStatus.Error;
                return ret;
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close(false);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbook);
                    workbook = null;
                }

                if (workbooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbooks);
                    workbooks = null;
                }

                if (excel != null)
                {
                    excel.Quit();
                    UpdaterUtil.TerminateExcelProcess(excel);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
                    excel = null;
                }
            }
            logger.Debug("CopyLocalFileToOrigin_start");
            // copy file to origin
            return CopyLocalFileToOrigin();
        }

        private ActionStatus CopyLocalFileToOrigin()
        {
            try
            {
                // forbid to overwrite user modified file (TODO : need to synchronize)
                DateTime lastWriteTime = File.GetLastWriteTimeUtc(this.originalFile);
                if (lastWriteTime <= this.lastOriginalFileWriteTime)
                {
                    if (File.Exists(this.originalFile) && File.Exists(this.localFile))
                    {
                        if (UpdaterUtil.WaitForFile(this.localFile) && UpdaterUtil.WaitForFile(this.originalFile))
                        {
                            File.Copy(this.localFile, this.originalFile, true);
                        }
                    }
                    else
                    {
                        return ActionStatus.OriginalFileNotExist;
                    }

                    this.lastOriginalFileWriteTime = File.GetLastWriteTimeUtc(this.originalFile);
                }
            }
            catch (DirectoryNotFoundException dnf)
            {
                logger.Error("CopyLocalFileToOrigin error.", dnf);

                return ActionStatus.OriginalFileNotExist;
            }
            catch (FileNotFoundException fnf)
            {
                logger.Error("CopyLocalFileToOrigin error.", fnf);
                return ActionStatus.OriginalFileNotExist;
            }
            catch (Exception e)
            {
                logger.Error("CopyLocalFileToOrigin error.", e);
                return ActionStatus.Error;
            }
            logger.Debug("CopyLocalFileToOrigin_end" + ActionStatus.Success.ToString());
            return ActionStatus.Success;
        }

        private ActionStatus CopyOriginFileToLocal() {
            try
            {
                logger.Debug("CopyOriginFileToLocal begin.");

                if (string.IsNullOrEmpty(this.originalFile))
                {
                    return ActionStatus.OriginalFileNotExist;
                }
                DateTime lastWriteTime = File.GetLastWriteTimeUtc(this.originalFile);
                if (lastWriteTime > this.lastOriginalFileWriteTime)
                {
                    this.lastOriginalFileWriteTime = lastWriteTime;
                    Directory.CreateDirectory(UpdaterConf.Config.UpdateDataPath);
                    logger.Debug("copy file from " + this.originalFile + " to " + this.localFile);
                    File.Copy(this.originalFile, this.localFile, true);
                }
            }
            catch (DirectoryNotFoundException dnf)
            {
                logger.Error("CopyOriginFileToLocal error.", dnf);
                return ActionStatus.OriginalFileNotExist;
            }
            catch (FileNotFoundException fnf)
            {
                logger.Error("CopyOriginFileToLocal error.", fnf);
                return ActionStatus.OriginalFileNotExist;
            }
            catch (Exception e)
            {
                logger.Error("CopyOriginFileToLocal error.", e);
                return ActionStatus.Error;
            }
            logger.Debug("CopyOriginFileToLocal end.");
            return ActionStatus.Success;
        }

        private ActionStatus SetLocalTargetFile()
        {
            string extension = Path.GetExtension(this.originalFile);
            if (extension.Equals(EXTENSION_DAT, StringComparison.OrdinalIgnoreCase))
            {
                extension = EXTENSION_XLSM;
            }
            this.localFile = Path.Combine(UpdaterConf.Config.UpdateDataPath, Path.GetFileNameWithoutExtension(this.originalFile) + extension);
            return ActionStatus.Success;
        }

        private ActionStatus SetOriginTargetFile()
        {
            string datFile = Path.Combine(UpdaterConf.Config.GripDataPath, this.refer.file_id) + EXTENSION_DAT;
            string zipFile = Path.Combine(UpdaterConf.Config.GripDataPath, this.refer.file_id) + EXTENSION_ZIP;
            if (File.Exists(datFile))
            {
                this.originalFile = datFile;
            }
            else if (File.Exists(zipFile))
            {
                this.originalFile = zipFile;
            }
            else
            {
                this.originalFile = string.Empty;
                return ActionStatus.OriginalFileNotExist;
            }

            return ActionStatus.Success;
        }

    }
}
