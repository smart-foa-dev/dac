﻿using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;

namespace Updater.Model
{
    public static class UpdaterConf
    {
        public const string BaseUrl = "http://{0}:{1}/cms/rest/";
        public const string GripServerBaseUrl = "http://{0}:{1}/grip/rest/";
        public static Conf Config { get; set; }
        public static readonly List<string> UiLangList = new List<string> { "ja", "en" };
        public static readonly List<string> UiLangListForDisplay = new List<string> { "Japanese", "English" };
        public static readonly List<string> CatalogLangList = new List<string> { "ja", "en", "de", "fr", "zh-tw", "zh-cn", "ko", "pt", "th", "ms", "in", "vi" };
        public static readonly List<string> CatalogLangListForDisplay = new List<string> { "Japanese", "English", "German", "French", "Traditional Chinese", "Simplified Chinese", "Korean", "Portuguese", "Thai", "Malay", "Indonesian", "Vietnamese" };
        public static readonly List<string> CatalogLangListForData = new List<string> { "Japanese", "English", "German", "French", "TraditionalChinese", "SimplifiedChinese", "Korean", "Portuguese", "Thai", "Malay", "Indonesian", "Vietnamese" };
        public static readonly string BaseDir = AppDomain.CurrentDomain.BaseDirectory;

        public static string UserId { get; set; }
        public static string Password { get; set; }
        public static bool IsErrorLogin { get; set; }
        public static string CatalogLang { get; set; }
        public static string UiLang { get; set; }

        public static string UserGuID { get; set; }

        public static bool IsExternal = false;

        static UpdaterConf() {
        }

        /// <summary>
        /// Conf/Ais.conf 読み込み
        /// </summary>
        public static void LoadUpdaterConf()
        {
            var baseDir = AppDomain.CurrentDomain.BaseDirectory;
            var confPath = System.IO.Path.Combine(baseDir, "Conf\\Updater.conf");

            using (StreamReader sr = new StreamReader(confPath))
            {
                string rte = sr.ReadToEnd().Replace("\\", "\\\\");
                Config = JsonConvert.DeserializeObject<Conf>(rte);
            }

            if (Config.UseProxy == false || String.IsNullOrEmpty(Config.ProxyURI))
            {
                Config.UseProxy = false;
                Config.ProxyURI = "";
                FoaCore.Net.FoaHttpClient.useProxy = false;
            }
            else
            {
                FoaCore.Common.Net.CmsHttpClient.proxy = new System.Net.WebProxy(Config.ProxyURI, true);
                FoaCore.Net.FoaHttpClient.proxy = new System.Net.WebProxy(Config.ProxyURI, true);
                FoaCore.Net.FoaHttpClient.useProxy = true;
            }
        }

        public class Conf
        {
            public string CmsHost;
            public string CmsPort = "60000";
            public string GripServerHost;
            public string GripServerPort = "60000";
            public string CmsDbHost = "localhost";
            public string CmsDbPort = "3306";
            public bool UseProxy = false;
            public string ProxyURI;
            public string ParamPath = System.Environment.GetEnvironmentVariable("FOA_HOME") + @"\excelengine\Updater\updaterparam";
            public string AlarmParamPath = System.Environment.GetEnvironmentVariable("FOA_HOME") + @"\excelengine\Updater\updateralarmparam";
            public string GripDataPath = System.Environment.GetEnvironmentVariable("FOA_HOME") + @"\grip\data\dashbox";
            public string UpdateDataPath = System.Environment.GetEnvironmentVariable("FOA_HOME") + @"\excelengine\Updater\dashbox";
            public int RefreshFileListSpanSec = 30;
            public int MaxConcurrentUpdater = 5;
            //Graph_Alarm 20190424 sunyi start
            public string TextWrapperPoolPath = System.Environment.GetEnvironmentVariable("FOA_HOME") + @"\excelengine\Updater\alarm";
            public string ManageJsonPath = System.Environment.GetEnvironmentVariable("FOA_HOME") + @"\excelengine\Updater\alarm\manage.json";
            public int AlarmFileMonitorIntervalMS = 1000;
            //Graph_Alarm 20190424 sunyi end
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(UpdaterConf).Name);
    }
}
