﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Updater.Model;
using System.Threading;
using log4net;
using System.IO;
using Updater.Util;

namespace Updater.Task
{
    class GraphAlarmTask
    {
        private GraphAlarmTarget target;
        private Dictionary<string, GraphAlarmTask> updaters;
        private bool shutdownThis = false;
        private Thread updateThread;
        private GraphAlarmTarget graphAlarm;
        private Object thisLock = new Object();

        private Dictionary<string, GraphAlarmFileManager> fileManagers = new Dictionary<string, GraphAlarmFileManager>();

        public GraphAlarmTask(GraphAlarmTarget target, Dictionary<string, GraphAlarmTask> updaters)
        {
            this.target = target;
            this.updaters = updaters;
        }

        public void star(GraphAlarmTarget graphAlarm)
        {
            this.graphAlarm = graphAlarm;
            logger.Debug("Thread for graph alarm running start.");
            this.updateThread = new Thread(new ThreadStart(UpdateProc));
            this.updateThread.Start();
        }

        public void stop()
        {
            logger.Debug("Thread for graph alarm running end.");
            this.updateThread.Abort();
        }

        private void UpdateProc()
        {
            logger.Debug("Thread for graph alarm" + this.target.file_id + " start.");
            while (true)
            {
                lock (this.thisLock)
                // TODO : get data
                {
                    try
                    {
                        logger.Debug("load data from graph alarm " + this.target.file_id + " start.");
                        GraphAlarmFileManager graphAlarmFileManager;
                        if (this.fileManagers.ContainsKey(this.graphAlarm.file_id))
                        {
                            logger.Debug("GraphAlarmFileManager" + this.graphAlarm.file_id);
                            graphAlarmFileManager = this.fileManagers[this.graphAlarm.file_id];
                        }
                        else
                        {
                            logger.Debug("new GraphAlarmFileManager");
                            graphAlarmFileManager = new GraphAlarmFileManager(this.graphAlarm);
                            this.fileManagers.Add(this.graphAlarm.file_id, graphAlarmFileManager);
                        }
                        logger.Debug("graphAlarmFileManager3");
                        GraphAlarmFileManager.ActionStatus ret = graphAlarmFileManager.UpdateGraphAlarmFile();

                        
                        if (ret.Equals(GraphAlarmFileManager.ActionStatus.OriginalFileNotExist))
                        {
                            logger.Debug("graphAlarm task stop" + this.target.file_id + "file deleted");
                            this.updaters.Remove(this.target.file_id);
                            string path = Path.Combine(UpdaterConf.Config.UpdateDataPath, this.target.file_id + ".xlsm");
                            
                            File.Delete(path);
                            stop();
                            //shutdownThis = true;
                            break;
                        }

                        string shutdownPath = Path.Combine(UpdaterConf.Config.UpdateDataPath, "shutdown");
                        if (ret.Equals(GraphAlarmFileManager.ActionStatus.Error) || File.Exists(shutdownPath))
                        {
                            logger.Debug("graphAlarm task stop" + this.target.file_id);
                            this.updaters.Remove(this.target.file_id);
                            stop();
                            //shutdownThis = true;
                            break;
                        }

                        //if (shutdownThis || GraphAlarmManager.shutdownEvent.WaitOne(0))
                        //{
                        //    this.updaters.Remove(this.target.file_id);
                        //    break;
                        //}
                    }
                    catch(Exception e)
                    {
                        logger.Debug("Thread for graph alarm" + this.target.file_id + e.Message);
                    }
                }
                logger.Debug("load data from graph alarm " + this.target.file_id + " end.");
                //Thread.Sleep(this.graphAlarm.refresh_period);
                Thread.Sleep(this.graphAlarm.refresh_period);
            }
            logger.Debug("Thread for graph alarm " + this.target.file_id + " end.");
        }

        public void changeStates(GraphAlarmTarget target)
        {
            lock (this.thisLock)
            {
                bool shutdown = true;

                if (!string.IsNullOrEmpty(target.file_id))
                {
                    shutdown = false;
                }

                if (shutdown)
                {
                    shutdownThis = true;
                    return;
                }
            }
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(GraphAlarmTask));

    }
}
