﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Updater.Model;
using System.Threading;
using log4net;
using System.IO;
using System.Windows;
using System.Collections.Concurrent;

namespace Updater.Task
{
    public class UpdateTask
    {
        private WorkplaceTarget target;
        private Thread updateThread;
        private int minMissionRefreshPeriod;
        private Dictionary<string, UpdateTask> updaters;
        private Object thisLock = new Object();
        private ManualResetEvent thisShutdownEvent = new ManualResetEvent(false);
        //private Dictionary<TblWorkPlaceReference, TargetFileManager> fileManagers = new Dictionary<TblWorkPlaceReference, TargetFileManager>();

        public UpdateTask(WorkplaceTarget target, Dictionary<string, UpdateTask> updaters)
        {
            this.target = target;
            this.updaters = updaters;
        }

        public void star(bool running)
        {
            this.minMissionRefreshPeriod = CalculateRefreshPeriod();
            if (running)
            {
                logger.Debug("Thread for UpdateTask running start.");
                this.updateThread = new Thread(new ThreadStart(UpdateProc));
                this.updateThread.Start();
            }
            else
            {
                logger.Debug("Thread for UpdateTask running end.");
                this.updateThread.Abort();//実際に終了
                this.updateThread = null;
            }
        }

        private int CalculateRefreshPeriod()
        {
            int period = int.MaxValue;
            foreach (TblWorkPlaceMission mission in target.missions)
            {
                period = Math.Min(mission.condition_online_period.HasValue ? mission.condition_online_period.Value : int.MaxValue, period); 
            }

            return period * 1000;
        }

        private void UpdateProc()
        {
            logger.Debug("Thread for workplace " + this.target.work_place_id + " start.");
            while (true)
            {
                /*
                lock (this.thisLock)
                {
                    foreach (TblWorkPlaceReference refer in target.refs)
                    {
                        logger.Debug("load data from workplace " + this.target.work_place_id + " for files " + refer.file_id + "[" + refer.file_name + "]");
                        TargetFileManager fileManager;
                        if (this.fileManagers.ContainsKey(refer))
                        {
                            fileManager = this.fileManagers[refer];
                        }
                        else
                        {
                            fileManager = new TargetFileManager(refer, this);
                            this.fileManagers.Add(refer, fileManager);
                        }
                        TargetFileManager.ActionStatus ret = fileManager.UpdateTargetFile();
                        if (ret.Equals(TargetFileManager.ActionStatus.OriginalFileNotExist))
                        {
                            shutdownThis = true;
                            break;
                        }
                    }
                    if (shutdownThis || UpdaterManager.shutdownEvent.WaitOne(0))
                    {
                        this.updaters.Remove(this.target.work_place_id);
                        break;
                    }
                }
                 * */

                lock (this.thisLock)
                {
                    if (this.thisShutdownEvent.WaitOne(0) || UpdaterManager.shutdownEvent.WaitOne(0))
                    {
                        this.updaters.Remove(this.target.work_place_id);
                        break;
                    }
                }

                foreach (TblWorkPlaceReference refer in target.refs)
                {
                    BlockingCollection<TblWorkPlaceReference> updateQueue = ((App)Application.Current).UpdateQueue;
                    refer.updateTask = this;
                    ((App)Application.Current).UpdateQueue.Add(refer);
                    logger.Debug(String.Format("Enqueued refer : wpId={0} fileId={1} fileName={2}", refer.work_place_id, refer.file_id, refer.file_name));
                }

                Thread.Sleep(this.minMissionRefreshPeriod);
            }
            logger.Debug("Thread for workplace " + this.target.work_place_id + " end.");
        }

        public void changeStates(WorkplaceTarget target)
        {
            lock (this.thisLock)
            {
                bool shutdown = true;

                foreach (TblWorkPlaceReference refer in target.refs)
                {
                    if (refer.reference_count > 0)
                    {
                        shutdown = false;
                        break;
                    }
                }

                if (shutdown)
                {
                    this.thisShutdownEvent.Set();
                    return;
                }
            }
        }

        public void shutdown()
        {
            lock (this.thisLock)
            {
                this.thisShutdownEvent.Set();
            }
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(UpdateTask));

    }
}
