﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Updater.Model;
using System.Collections.Concurrent;
using System.Windows;
using log4net;

namespace Updater.Task
{
    public class CentralThreadPool
    {
        public void Start()
        {
            for (int i = 0; i < UpdaterConf.Config.MaxConcurrentUpdater; i++)
            {
                Thread updateThread = new Thread(new ThreadStart(UpdateProc));
                updateThread.Start();
            }
        }

        public void UpdateProc()
        {
            BlockingCollection<TblWorkPlaceReference> updateQueue = ((App)Application.Current).UpdateQueue;
            HashSet<TblWorkPlaceReference> updatingReferences = ((App)Application.Current).updatingReferences;
            while (true)
            {
                TblWorkPlaceReference refer = updateQueue.Take();
                lock (((App)Application.Current).updateQueueLock)
                {
                    if (!updatingReferences.Contains(refer))
                    {
                        updatingReferences.Add(refer);
                    }
                    else
                    {
                        continue;
                    }
                }
                logger.Debug("load data from workplace " + refer.work_place_id + " for files " + refer.file_id + "[" + refer.file_name + "]");
                TargetFileManager fileManager = new TargetFileManager(refer);

                TargetFileManager.ActionStatus ret = fileManager.UpdateTargetFile();
                lock (((App)Application.Current).updateQueueLock)
                {
                    updatingReferences.Remove(refer);
                }
                if (ret.Equals(TargetFileManager.ActionStatus.OriginalFileNotExist))
                {
                    refer.updateTask.shutdown();
                }
            }
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(CentralThreadPool));
    }
}
