﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Updater.Model;
using Newtonsoft.Json;
using System.IO;
using log4net;
using Updater.Util;
using System.Threading;
using System.Collections.Concurrent;
using System.Windows;

namespace Updater.Task
{
    class UpdaterManager
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(UpdaterManager));
        private Dictionary<string, UpdateTask> updaters = new Dictionary<string, UpdateTask>();

        public static ManualResetEvent shutdownEvent = new ManualResetEvent(false);

        public UpdaterManager()
        {
        }

        public async void Start(string file, HashSet<string> files,bool running)
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                lock (updaters)
                {
                    try
                    {
                        if (UpdaterUtil.WaitForFile(file))
                        {
                            using (StreamReader sr = new StreamReader(file, Encoding.UTF8))
                            {
                                string json = sr.ReadToEnd();
                                sr.Close();

                                File.Delete(file);
                                files.Remove(file);

                                List<WorkplaceTarget> targets = JsonConvert.DeserializeObject<List<WorkplaceTarget>>(json);

                                if (targets != null)
                                {
                                    foreach (WorkplaceTarget target in targets)
                                    {
                                        if (!updaters.ContainsKey(target.work_place_id)) {
                                            logger.Debug("new UpdateTask " + target.work_place_id);
                                            UpdateTask task = new UpdateTask(target, updaters);
                                            updaters.Add(target.work_place_id, task);
                                            task.star(running);
                                        } else {
                                            logger.Debug("update UpdateTask " + target.work_place_id);
                                            UpdateTask task = updaters[target.work_place_id];
                                            task.changeStates(target);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error("start error.", e);
                    }
                }
            });
        }

        public void ShutDown()
        {
            shutdownEvent.Set();

            //Application.Current.Shutdown();
        }
    }
}
