﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Updater.Model;
using Newtonsoft.Json;
using System.IO;
using log4net;
using Updater.Util;
using System.Threading;
using System.Collections.Concurrent;
using System.Windows;

namespace Updater.Task
{
    class GraphAlarmManager
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(GraphAlarmManager));
        private Dictionary<string, GraphAlarmTask> updaters = new Dictionary<string, GraphAlarmTask>();

        public static ManualResetEvent shutdownEvent = new ManualResetEvent(false);

        public GraphAlarmManager()
        {
        }

        public async void Start(string file, HashSet<string> files, bool running)
        {
            if (!running)
            {
                logger.Debug("graphAlarmTask stop");
                foreach(var updater in updaters)
                {
                    GraphAlarmTask task = updater.Value;
                    task.stop();
                }
                updaters.Clear();
                return;
            }

            await System.Threading.Tasks.Task.Run(() =>
            {
                //if (!running)
                //{
                //    throw new TaskCanceledException();
                //}
                lock (updaters)
                {
                    try
                    {
                        if (UpdaterUtil.WaitForFile(file))
                        {
                            using (StreamReader sr = new StreamReader(file, Encoding.UTF8))
                            {
                                string json = sr.ReadToEnd();
                                sr.Close();

                                File.Delete(file);
                                files.Remove(file);

                                List<GraphAlarmTarget> targets = JsonConvert.DeserializeObject<List<GraphAlarmTarget>>(json);
                                logger.Debug("new graphAlarmTask ");
                                if (targets != null)
                                {
                                    foreach (GraphAlarmTarget target in targets)
                                    {
                                        if (!updaters.ContainsKey(target.file_id))
                                        {
                                            logger.Debug("new graphAlarmTask " + target.file_id);
                                            GraphAlarmTask task = new GraphAlarmTask(target, updaters);
                                            updaters.Add(target.file_id, task);
                                            task.star(target);
                                        }
                                        else
                                        {
                                            logger.Debug("old graphAlarmTask " + target.file_id);
                                            GraphAlarmTask task = new GraphAlarmTask(target, updaters);
                                            task.star(target);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Error("start error.", e);
                    }
                }
            });
        }

        public void ShutDown()
        {
            shutdownEvent.Set();

            Application.Current.Shutdown();
        }
    }
}
