﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Updater.Model;
using System.IO;
using System.Windows;
using log4net;
using Updater.Util;

namespace Updater.Task
{
    class GraphAlarm
    {
        //private Thread monitor = null;
        private const string SHUTDOWN_FILE = "shutdown";

        private FileSystemWatcher watcher = null;
        private GraphAlarmManager updater = null;
        private HashSet<string> files = new HashSet<string>();

        public void Start()
        {
            logger.Debug("GraphAlarm_Start_log:");
            updater = new GraphAlarmManager();

            this.watcher = new FileSystemWatcher();
            this.watcher.Path = UpdaterConf.Config.AlarmParamPath;
            this.watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
            this.watcher.Filter = "*";
            this.watcher.Created += new FileSystemEventHandler(OnChanged);
            this.watcher.Error += new ErrorEventHandler(OnError);
            this.watcher.EnableRaisingEvents = true;
        }

        public void OnError(object source, ErrorEventArgs e)
        {
            logger.Debug(e.ToString());
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            if (!e.ChangeType.Equals(WatcherChangeTypes.Created))
            {
                return;
            }
            lock (files)
            {
                if (files.Contains(e.FullPath))
                {
                    return;
                }
                else
                {
                    files.Add(e.FullPath);
                }
            }

            logger.Debug("GraphAlarmOnChanged " + e.FullPath);
            if (Path.GetFileName(e.FullPath).Equals(SHUTDOWN_FILE, StringComparison.OrdinalIgnoreCase))
            {
                //watcher.EnableRaisingEvents = false;
                //スレッド終了
                logger.Debug("GraphAlarm_thread_shutdown");
                updater.Start(e.FullPath, files, false);
                //updater.ShutDown();
                return;
            }
            logger.Debug("GraphAlarm_start" + e.FullPath);
            updater.Start(e.FullPath, files, true);
        }


        private static readonly ILog logger = LogManager.GetLogger(typeof(GraphAlarm));
    }
}
