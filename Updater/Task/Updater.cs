﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Updater.Model;
using Newtonsoft.Json;
using System.IO;
using log4net;
using Updater.Util;
using System.Threading;

namespace Updater.Task
{
    class Updater
    {
        private Dictionary<string, Updater> updaters = new Dictionary<string, Updater>();
        private string paramFile;

        public Updater(string file)
        {
            this.paramFile = file;
        }

        public async void start()
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
                    //Interlocked.Increment(ref threadCount);

                    using (StreamReader sr = new StreamReader(paramFile, Encoding.UTF8))
                    {
                        string json = sr.ReadToEnd();
                        sr.Close();

                        UpdaterUtil.TryDeleteFile(paramFile);

                        List<WorkplaceTarget> targets = JsonConvert.DeserializeObject<List<WorkplaceTarget>>(json);

                        if (targets != null)
                        {
                            foreach (WorkplaceTarget target in targets)
                            {
                                logger.Debug("Todo update workplace " + target.work_place_id);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error("start error.", e);
                }
            });
        }

        public async void start_online()
        {
            await System.Threading.Tasks.Task.Run(() =>
            {
                try
                {
                    //Interlocked.Increment(ref threadCount);

                    using (StreamReader sr = new StreamReader(paramFile, Encoding.UTF8))
                    {
                        string json = sr.ReadToEnd();
                        sr.Close();

                        UpdaterUtil.TryDeleteFile(paramFile);

                        List<TblGraphAlarm> targets = JsonConvert.DeserializeObject<List<TblGraphAlarm>>(json);

                        if (targets != null)
                        {
                            foreach (TblGraphAlarm target in targets)
                            {
                                logger.Debug("Todo update workplace " + target.file_id + target.file_path);
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error("start error.", e);
                }
            });
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(Updater));
    }
}
