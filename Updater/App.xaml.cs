﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Updater.Model;
using Updater.Task;
using log4net;
using System.Threading;
using System.Collections.Concurrent;

namespace Updater
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private static string thisId = "FD67BE56-1AD1-4E3F-8986-584CE7248CBC";

        public static Mutex mutex = new Mutex(false, "Global\\" + thisId);

        public BlockingCollection<TblWorkPlaceReference> UpdateQueue;
        public HashSet<TblWorkPlaceReference> updatingReferences;
        public CentralThreadPool UpdaterPool;
        public Object updateQueueLock = new Object();

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            UpdaterConf.LoadUpdaterConf();
            this.UpdateQueue = new BlockingCollection<TblWorkPlaceReference>();
            this.updatingReferences = new HashSet<TblWorkPlaceReference>();
            this.UpdaterPool = new CentralThreadPool();
            this.UpdaterPool.Start();
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(App));
    }
}
