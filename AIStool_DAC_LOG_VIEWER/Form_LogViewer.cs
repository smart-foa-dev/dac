﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace AIStool_DAC_LOG_VIEWER
{
    public partial class Form_LogViewer : Form
    {
        private System.IO.FileSystemWatcher watcher = null;

        private string logFullName;

        public Form_LogViewer(string logFullName)
        {
            InitializeComponent();

            label_logfile.Text = logFullName;

            if (logFullName.Equals("LogViewerの引数（タイプとファイル）の指定が正しくありません"))
            {

                button_surveillance_start.Enabled = false;
                button_surveillance_stop.Enabled = false;

            }
            else
            {
                this.logFullName = logFullName;
                readLogFile();
                startSurveillance();
            }
        }

        private void Form_LogViewer_Load(object sender, EventArgs e)
        {
        }

        private void button_surveillance_start_Click(object sender, EventArgs e)
        {
            startSurveillance();
        }

        private void button_surveillance_stop_Click(object sender, EventArgs e)
        {
            stopSurveillance();
        }

        private void startSurveillance()
        {
            if (watcher != null)
            {
                return;
            }

            watcher = new System.IO.FileSystemWatcher();

            //監視するディレクトリを指定
            watcher.Path = Path.GetDirectoryName(logFullName);

            //最終アクセス日時、最終更新日時、ファイル、フォルダ名の変更を監視する
            watcher.NotifyFilter =
                (System.IO.NotifyFilters.LastAccess
                | System.IO.NotifyFilters.LastWrite
                | System.IO.NotifyFilters.FileName
                | System.IO.NotifyFilters.DirectoryName);
            //すべてのファイルを監視
            watcher.Filter = Path.GetFileName(logFullName);

            //UIのスレッドにマーシャリングする
            //コンソールアプリケーションでの使用では必要ない
            watcher.SynchronizingObject = this;

            //イベントハンドラの追加
            watcher.Changed += new System.IO.FileSystemEventHandler(watcher_Changed);

            //監視を開始する
            watcher.EnableRaisingEvents = true;


            button_surveillance_start.Enabled = false;
            button_surveillance_stop.Enabled = true;
        
        }

        private void stopSurveillance()
        {
            watcher.EnableRaisingEvents = false;
            watcher.Dispose();
            watcher = null;

            button_surveillance_start.Enabled = true;
            button_surveillance_stop.Enabled = false;

        }


        //イベントハンドラ
        private void watcher_Changed(System.Object source,
            System.IO.FileSystemEventArgs e)
        {
            readLogFile();
        }

        private void readLogFile()
        {
            using (FileStream fs = new FileStream(logFullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (TextReader sr = new StreamReader(fs))
                {
                    RichTextBox_log.Clear();
                    RichTextBox_log.Focus();
                    RichTextBox_log.AppendText(sr.ReadToEnd());

                }
            }

        }

        private void button_close_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }        

    }
}
