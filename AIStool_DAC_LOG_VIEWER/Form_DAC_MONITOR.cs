﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;

namespace AIStool_DAC_LOG_VIEWER
{
    public partial class Form_DAC_MONITOR : Form
    {
        private System.IO.FileSystemWatcher watcher = null;

        private string logFullName;

        public Form_DAC_MONITOR(string logFullName)
        {
            InitializeComponent();

            this.logFullName = logFullName;

            if (logFullName.Equals("LogViewerの引数（タイプとファイル）の指定が正しくありません"))
            {
                label1.Text = "LogViewerの引数（タイプとファイル）の指定が正しくありません";
            }
            else
            {
                this.logFullName = logFullName;
                readLogFile();
                startSurveillance();
            }
        }

        private void Form_DAC_MONITOR_Load(object sender, EventArgs e)
        {

        }
        private void startSurveillance()
        {
            if (watcher != null)
            {
                return;
            }

            watcher = new System.IO.FileSystemWatcher();

            //監視するディレクトリを指定
            watcher.Path = Path.GetDirectoryName(logFullName);

            //最終アクセス日時、最終更新日時、ファイル、フォルダ名の変更を監視する
            watcher.NotifyFilter =
                (System.IO.NotifyFilters.LastAccess
                | System.IO.NotifyFilters.LastWrite
                | System.IO.NotifyFilters.FileName
                | System.IO.NotifyFilters.DirectoryName);
            //すべてのファイルを監視
            watcher.Filter = Path.GetFileName(logFullName);

            //UIのスレッドにマーシャリングする
            //コンソールアプリケーションでの使用では必要ない
            watcher.SynchronizingObject = this;

            //イベントハンドラの追加
            watcher.Changed += new System.IO.FileSystemEventHandler(watcher_Changed);

            //監視を開始する
            watcher.EnableRaisingEvents = true;

        }

        private void stopSurveillance()
        {
            watcher.EnableRaisingEvents = false;
            watcher.Dispose();
            watcher = null;
        }


        //イベントハンドラ
        private void watcher_Changed(System.Object source,
            System.IO.FileSystemEventArgs e)
        {
            readLogFile();
        }

        private void readLogFile()
        {
            using (FileStream fs = new FileStream(logFullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (TextReader sr = new StreamReader(fs))
                {

                }
            }

        }


    }
}
