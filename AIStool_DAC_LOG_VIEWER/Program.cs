﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AIStool_DAC_LOG_VIEWER
{
    static class Program
    {


        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (args.Length != 2)
            {
                Application.Run(new Form_DAC_MONITOR("LogViewerの引数（タイプとファイル）の指定が正しくありません"));
            }
            else
            {
                if (args[0].Equals("1"))
                {
                    Application.Run(new Form_LogViewer(args[1]));
                }
                else 
                {
                    Application.Run(new Form_DAC_MONITOR(args[1]));
                }
            }
        }
    }
}
