﻿namespace AIStool_DAC_LOG_VIEWER
{
    partial class Form_LogViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_surveillance_start = new System.Windows.Forms.Button();
            this.button_surveillance_stop = new System.Windows.Forms.Button();
            this.button_close = new System.Windows.Forms.Button();
            this.label_logfile = new System.Windows.Forms.Label();
            this.RichTextBox_log = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // button_surveillance_start
            // 
            this.button_surveillance_start.Location = new System.Drawing.Point(12, 1);
            this.button_surveillance_start.Name = "button_surveillance_start";
            this.button_surveillance_start.Size = new System.Drawing.Size(75, 23);
            this.button_surveillance_start.TabIndex = 0;
            this.button_surveillance_start.Text = "監視開始";
            this.button_surveillance_start.UseVisualStyleBackColor = true;
            this.button_surveillance_start.Click += new System.EventHandler(this.button_surveillance_start_Click);
            // 
            // button_surveillance_stop
            // 
            this.button_surveillance_stop.Enabled = false;
            this.button_surveillance_stop.Location = new System.Drawing.Point(93, 1);
            this.button_surveillance_stop.Name = "button_surveillance_stop";
            this.button_surveillance_stop.Size = new System.Drawing.Size(75, 23);
            this.button_surveillance_stop.TabIndex = 1;
            this.button_surveillance_stop.Text = "監視終了";
            this.button_surveillance_stop.UseVisualStyleBackColor = true;
            this.button_surveillance_stop.Click += new System.EventHandler(this.button_surveillance_stop_Click);
            // 
            // button_close
            // 
            this.button_close.Location = new System.Drawing.Point(765, 2);
            this.button_close.Name = "button_close";
            this.button_close.Size = new System.Drawing.Size(75, 23);
            this.button_close.TabIndex = 3;
            this.button_close.Text = "閉じる";
            this.button_close.UseVisualStyleBackColor = true;
            this.button_close.Click += new System.EventHandler(this.button_close_Click);
            // 
            // label_logfile
            // 
            this.label_logfile.AutoSize = true;
            this.label_logfile.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label_logfile.Location = new System.Drawing.Point(200, 6);
            this.label_logfile.Name = "label_logfile";
            this.label_logfile.Size = new System.Drawing.Size(39, 13);
            this.label_logfile.TabIndex = 4;
            this.label_logfile.Text = "label1";
            // 
            // RichTextBox_log
            // 
            this.RichTextBox_log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.RichTextBox_log.BackColor = System.Drawing.SystemColors.Window;
            this.RichTextBox_log.Location = new System.Drawing.Point(12, 30);
            this.RichTextBox_log.Name = "RichTextBox_log";
            this.RichTextBox_log.ReadOnly = true;
            this.RichTextBox_log.Size = new System.Drawing.Size(830, 350);
            this.RichTextBox_log.TabIndex = 5;
            this.RichTextBox_log.Text = "";
            // 
            // Form_LogViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 392);
            this.Controls.Add(this.RichTextBox_log);
            this.Controls.Add(this.label_logfile);
            this.Controls.Add(this.button_close);
            this.Controls.Add(this.button_surveillance_stop);
            this.Controls.Add(this.button_surveillance_start);
            this.Name = "Form_LogViewer";
            this.Text = "LogVIewer";
            this.Load += new System.EventHandler(this.Form_LogViewer_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_surveillance_start;
        private System.Windows.Forms.Button button_surveillance_stop;
        private System.Windows.Forms.Button button_close;
        private System.Windows.Forms.Label label_logfile;
        private System.Windows.Forms.RichTextBox RichTextBox_log;

    }
}