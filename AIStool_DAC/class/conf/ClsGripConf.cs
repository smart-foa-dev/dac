﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIStool_DAC.conf
{
    class ClsGripConf
    {
        #region Cms
        /// <summary>
        /// Host
        /// </summary>
        public string CmsHost { get; set; }

        /// <summary>
        /// Port
        /// </summary>
        public int CmsPort { get; set; }
        #endregion

        #region MongoDB
        /// <summary>
        /// Host
        /// </summary>
        public string DbHost { get; set; }

        /// <summary>
        /// Port
        /// </summary>
        public int DbPort { get; set; }
        public string DbUser = "grip";
        public string DbPasswd = "grip!23";
        #endregion

        #region GripSrv
        /// <summary>
        /// Host
        /// </summary>
        public string GripSrvHost { get; set; }

        /// <summary>
        /// Port
        /// </summary>
        public int GripSrvPort { get; set; }
        #endregion

        #region VideoFoaSrv
        /// <summary>
        /// Host
        /// </summary>
        public string VideoFoaSrvHost { get; set; }

        /// <summary>
        /// Port
        /// </summary>
        public int VideoFoaSrvPort { get; set; }

        /// <summary>
        /// Port-Fowarding Address
        /// </summary>
        public string VideoFoaSrvPortFwdIpAddress { get; set; }

        /// <summary>
        /// Temporary Download Path
        /// </summary>
        public string TempDownloadPath { get; set; }

        /// <summary>
        /// View Continue Timer Interval
        /// </summary>
        public int ViewContTimerInterval { get; set; }

        /// <summary>
        /// Video Window Max Limit
        /// /// </summary>
        public int VideoWindowMaxLimit { get; set; }
        #endregion

        #region Assets

        /// <summary>
        /// レイアウト図メタデータ、画像リソースの配置ディレクトリ
        /// </summary>
        public string MediaRoot { get; set; }

        #endregion

        public string CmsUiFolderPath = System.Environment.GetEnvironmentVariable("FOAHOME") + @"\cms-ui" + @"\" + System.Environment.GetEnvironmentVariable("FOADOMAINID");
        public string AisFolderPath = System.Environment.GetEnvironmentVariable("FOAHOME") + @"\Ais" + @"\" + System.Environment.GetEnvironmentVariable("FOADOMAINID");
        public bool RunMonitor = false;
        public int RowsPerPage = 100;
        public string SearchByInCondition = "auto";
        public bool SkipSameMainKey = false;
        public bool EventVideoMode = false;
        public int PageSize = 50;
        public int GetSearchResultSpanSec = 2;
        public int GetSearchResultTimes = 3;
        public int RunMonitorDelaySec = 30;
        public bool OnMultiLinkDiagram = false;
        public bool SkipAllNodesWhileNoCtms = false;
        public HashSet<string> SkipExcludeNodesWhileNoCtms = null;
        public HashSet<string> SkipIncludeNodesWhileNoCtms = null;
        public bool AllowTestMode = false;
        public string Gripversion = "V5";
        public int TestThreads = 3;
        public string MissionString = "";
        public bool DisplayLinkBox = true;
        public bool UpdateCheck = false;
    }


}
