﻿using System;
using AIStool_DAC.util.excel;
using AIStool_DAC.util.log;
using AIStool_DAC.read;
using System.IO;
using log4net;

namespace AIStool_DAC.conf
{
    public class ClsDACConf
    {
        private string GRIP_IP_KEY      = "Grip-Server IP";
        private string GRIP_PORT_KEY    = "Grip-Server PORT";


        private string AIS_ENTRY_FOLDER = "登録フォルダ";
        private string GRIP_R2_FOLDER = "Grip-R2 Folder";
        private string DAC_MONITOR = "DAC MONITOR";
        private string TIMEZONE_ID = "TimezoneId";
        private string OFFLINE_MODE = "Offline Mode";
        private string OFFLINE_MACRO = "Offline Macro";

        private string USE_PROXY = "ProxyServer使用";
        private string PROXY_URI = "ProxyServerURI";
        private string ENABLE_MULTI_DAC = "MultiDac有効";


        public string GripIp { get; set; }

        public string GripPort { get; set; }

        // public string GripR2Folder { get; set; }

        public string TimezoneId { get; set; }

        public ClsMissionLog.LOG_MONITOR_TYPE MonitorLevel
        {
            get
            {
                //DACモニター
                string tmpMonitorLevel = ClsExcelUtil.getDACConfByConfName(ClsReadData.AIS_RO_PARAM_SHEET_NAME, DAC_MONITOR);
                if (String.IsNullOrEmpty(tmpMonitorLevel))
                {
                    return ClsMissionLog.LOG_MONITOR_TYPE.NONE;
                }
                else if (tmpMonitorLevel.Equals("0"))
                {
                    return ClsMissionLog.LOG_MONITOR_TYPE.NONE;
                }

                else if (tmpMonitorLevel.Equals("1"))
                {
                    return ClsMissionLog.LOG_MONITOR_TYPE.DETAIL;
                }
                else
                {
                    return ClsMissionLog.LOG_MONITOR_TYPE.SIMPLE;
                }
            }
        }

        public string OfflineMode { get; set; }

        public string OfflineMacro { get; set; }

        // public string AisExeDir { get; set; }


        // AISディレクトリ
        // public string AisRegDir{get;set;}
        public string DacDir { get; set; }
        public string InfoDir
        {
            get
            {
                if (string.IsNullOrEmpty(DacDir))
                {
                    return string.Empty;
                } else
                {
                    return Path.Combine(DacDir, "__FOA");
                }
            }
        }
        public string DataDir { 
            get
            {
                if (string.IsNullOrEmpty(InfoDir))
                {
                    return string.Empty;
                } else
                {
                    return Path.Combine(InfoDir, "data");
                }
            }
        }
        public string LogDir
        {
            get
            {
                if (string.IsNullOrEmpty(InfoDir))
                {
                    return string.Empty;
                } else
                {
                    return Path.Combine(InfoDir, "log");
                }
            }
        }
        public string ManifestPath
        {
            get
            {
                if (string.IsNullOrEmpty(InfoDir))
                {
                    return string.Empty;
                } else
                {
                    return Path.Combine(InfoDir, "manifest.json");
                }
            }
        }
        // Use Proxy
        public bool UseProxy { get; set; }
        // Proxy URI
        public string ProxyURI { get; set; }

        // Enable MultiDoc
        public bool EnableMultiDoc { get; set; }


        public ClsDACConf(string filePath)
        {
            // Grip-Server
            GripIp = ClsExcelUtil.getDACConfByConfName(ClsReadData.AIS_RO_PARAM_SHEET_NAME,GRIP_IP_KEY);
            GripPort = ClsExcelUtil.getDACConfByConfName(ClsReadData.AIS_RO_PARAM_SHEET_NAME,GRIP_PORT_KEY);

            // AIS登録フォルダー・一時フォルダー・SubDACフォルダ

            
            DacDir = Path.GetDirectoryName(filePath);
            
            TimezoneId = ClsExcelUtil.getDACConfByConfName(ClsReadData.AIS_RO_PARAM_SHEET_NAME, TIMEZONE_ID);

            OfflineMode = ClsExcelUtil.getDACConfByConfName(ClsReadData.AIS_RO_PARAM_SHEET_NAME, OFFLINE_MODE);
            OfflineMacro = ClsExcelUtil.getDACConfByConfName(ClsReadData.AIS_RO_PARAM_SHEET_NAME, OFFLINE_MACRO);

            // Proxy情報
            string useProxyTmp = ClsExcelUtil.getDACConfByConfName(ClsReadData.AIS_PARAM_SHEET_NAME, USE_PROXY);
            if ( "true".Equals(useProxyTmp.ToLower()) )
            {
                UseProxy = true;
                ProxyURI = ClsExcelUtil.getDACConfByConfName(ClsReadData.AIS_PARAM_SHEET_NAME, PROXY_URI);
            }
            else{
                UseProxy = false;
                ProxyURI = "";
            }

            // MultiDac有効
            EnableMultiDoc = false;
            string enableMultiDac = ClsExcelUtil.getDACConfByConfName(ClsReadData.AIS_PARAM_SHEET_NAME, ENABLE_MULTI_DAC);
            if ("true".Equals(enableMultiDac.ToLower()))
            {
                EnableMultiDoc = true;
            }

        }
    }
}
