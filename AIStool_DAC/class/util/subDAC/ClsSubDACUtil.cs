﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Diagnostics;

using Excel = Microsoft.Office.Interop.Excel;

using AIStool_DAC.util.excel;
using AIStool_DAC.read;
using AIStool_DAC.util.log;

namespace AIStool_DAC.util.subDAC
{
    class ClsSubDACUtil
    {



        /**
         * paramシートからテンプレート名称を取得する
         *      SpreadsheetGearを使用
         * 
         */
        public static string getTemplateName(string filePath)
        {
            string templateName = "";

            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                // ワークブックオープン
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePath);

                SpreadsheetGear.IWorksheets sheets = workbook.Worksheets;

                SpreadsheetGear.IWorksheet sheet = sheets[ClsReadData.AIS_PARAM_SHEET_NAME];
                if (sheet != null)
                {
                    int rowPosition = 0;
                    SpreadsheetGear.IRange cell = null;

                    string tempCellName = "";

                    while (true)
                    {
                        cell = sheet.Cells[rowPosition, 0];
                        if (ClsExcelUtil.isEmpty(cell))
                        {
                            // セルが空の場合は、処理を終了
                            templateName = "";
                            break;
                        }
                        else
                        {
                            // テンプレート名称か確認
                            tempCellName = cell.Value.ToString();
                            if (ClsReadData.AIS_PARAM_TEMP_NAME.Equals(tempCellName))
                            {
                                // テンプレート名称の倍は、値を取得して終了
                                templateName = sheet.Cells[rowPosition, 1].Value.ToString();
                                break;

                            }
                        }


                        rowPosition++;
                    }
                }
            }
#pragma warning disable
            catch (Exception e)
            {
#pragma warning restore
                if (filePath.IndexOf("課題管理表") > 0)
                {
                    templateName = "集計表";
                }
            }
            finally
            {
                if (workbook != null)
                {
                    //                    workbook.Save();
                    workbook.Close();
                }

            }


            return templateName;
        }

        /**
         * paramシートからテンプレート名称を取得する
         *      SpreadsheetGearを使用
         * 
         */
        public static string getOnlineName(string filePath)
        {
            string onelineName = "";

            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                // ワークブックオープン
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePath);

                SpreadsheetGear.IWorksheets sheets = workbook.Worksheets;

                SpreadsheetGear.IWorksheet sheet = sheets[ClsReadData.AIS_PARAM_SHEET_NAME];
                if (sheet != null)
                {
                    int rowPosition = 0;
                    SpreadsheetGear.IRange cell = null;

                    string tempCellName = "";

                    while (true)
                    {
                        cell = sheet.Cells[rowPosition, 0];
                        if (ClsExcelUtil.isEmpty(cell))
                        {
                            // セルが空の場合は、処理を終了
                            onelineName = "";
                            break;
                        }
                        else
                        {
                            // テンプレート名称か確認
                            tempCellName = cell.Value.ToString();
                            if (ClsReadData.AIS_PARAM_ONLINE_NAME.Equals(tempCellName))
                            {
                                // テンプレート名称の倍は、値を取得して終了
                                onelineName = sheet.Cells[rowPosition, 1].Value.ToString();
                                break;

                            }
                        }


                        rowPosition++;
                    }
                }
            }
            catch
            {

            }
            finally
            {
                if (workbook != null)
                {
                    //                    workbook.Save();
                    workbook.Close();
                }

            }


            return onelineName;
        }
        public static bool createSubDACFile(string createFile, string date, DateTime dateTimeStart, DateTime dateTimeEnd, ClsMissionLog missionLog)
        {
            bool bRtn = false;

            string message="";
            missionLog.OutPut("CreateFile: "+createFile, "Sub DAC作成", ClsMissionLog.LOG_LEVEL.DEBUG);
            // Sub DAC作成前準備の実施
            bRtn = ClsSubDACUtil.setDateAndResult(
                createFile,
                date,
                dateTimeStart,
                dateTimeEnd,
                missionLog);
            if (bRtn)
            {
                // SubDACファイルを作成する
                bRtn = ClsSubDACUtil.calculateSubDACFile(createFile);
                if (!bRtn)
                {
                    message = createFile + "は期間変更の実行（マクロの実行、結果シートの複製）で失敗しました。\n";
                }
            }
            else
            {
                message = createFile + "は期間変更の準備（結果シートの初期化）で失敗しました。\n";
            }
            missionLog.OutPut("Sub DAC作成", message, ClsMissionLog.LOG_LEVEL.DEBUG);
            return bRtn;
        }
        /**
         *  SubDAC期間変更の前処理
         *      １．SubDACファイルの日付・業務期間を変更する。
         *      ２．結果シートのヘッダー行以外を削除する。
         */
        private static bool setDateAndResult(string createFile, string date, DateTime startDateTime, DateTime endDateTime, ClsMissionLog missionLog)
        {
            bool bRtn = false;

            Excel.Application app = null;
            Excel.Workbook workbook = null;



            string periodSheetName = "";
            string periodStartDate = "";
            string periodStartTime = "";
            string periodEndDate = "";
            string periodEndTime = "";

            bool bNormalFormat = false;
            bool bDateTimeCell = false;

            try
            {

                SpreadsheetGear.IWorkbook workbookTmp = null;
                SpreadsheetGear.IWorksheets sheetsTmp = null;

                workbookTmp = SpreadsheetGear.Factory.GetWorkbook(createFile);
                sheetsTmp = workbookTmp.Worksheets;

                // 結果シートの確認
                SpreadsheetGear.IWorksheet resultSheetTmp = null;
                resultSheetTmp = sheetsTmp[ClsReadData.RESULT_SHEET_NAME];
                if (null == resultSheetTmp)
                {
                    resultSheetTmp = sheetsTmp[ClsReadData.RESULT_SHEET_NAME2];
                    bNormalFormat = false;
                }
                else
                {
                    bNormalFormat = true;
                }

                int resultRowPosition = 0;
                int resultColPosition = 0;
                if(resultSheetTmp != null){
                    resultRowPosition = ClsExcelUtil.getLastRowPosition(resultSheetTmp);
                    resultColPosition = ClsExcelUtil.getLastColPosition(resultSheetTmp);
                    if (workbookTmp != null)
                    {
                        workbookTmp.Close();
                    }
                }

                app = ClsExcelUtil.StartExcelProcess();
                workbook = app.Workbooks.Open(createFile);
                Excel.Sheets subDACSheets = workbook.Worksheets;

                Excel.Worksheet addInParamSheet = subDACSheets["addinparam"];

                // 日付セル情報の取得
                if (addInParamSheet != null)
                {
                    int rowDatePosition = 1;
                    Excel.Range dateCell = null;

                    string tmp = "";

                    while (true)
                    {
                        dateCell = addInParamSheet.Cells[rowDatePosition, 1];
                        if (ClsExcelUtil.isEmpty(dateCell))
                        {
                            // セルが空の場合は、処理を終了
                            bRtn = false;
                            break;
                        }
                        else
                        {
                            // addinparam名称か確認
                            tmp = dateCell.Value.ToString();
                            if ("Period Sheet".Equals(tmp))
                            {
                                // 日付記載シート名の取得
                                dateCell = addInParamSheet.Cells[rowDatePosition, 2];
                                periodSheetName = dateCell.Value.ToString(); ;

                                //次のセルの名称を取して、日付セルの値を取得する。
                                dateCell = addInParamSheet.Cells[rowDatePosition+1, 1];
                                string startDateCell = dateCell.Value.ToString(); ;

                                if(startDateCell.Equals("Period Start Date Time Cell")){

                                    rowDatePosition++;
                                    dateCell = addInParamSheet.Cells[rowDatePosition, 2];
                                    periodStartDate = dateCell.Value.ToString();

                                    rowDatePosition++;
                                    dateCell = addInParamSheet.Cells[rowDatePosition, 2];
                                    periodEndDate = dateCell.Value.ToString();

                                    bDateTimeCell = true;

                                }

                                else if (startDateCell.Equals("Period Start Date Cell"))
                                {

                                    rowDatePosition++;
                                    dateCell = addInParamSheet.Cells[rowDatePosition, 2];
                                    periodStartDate = dateCell.Value.ToString();

                                    rowDatePosition++;
                                    dateCell = addInParamSheet.Cells[rowDatePosition, 2];
                                    periodStartTime = dateCell.Value.ToString();

                                    rowDatePosition++;
                                    dateCell = addInParamSheet.Cells[rowDatePosition, 2];
                                    periodEndDate = dateCell.Value.ToString();

                                    rowDatePosition++;
                                    dateCell = addInParamSheet.Cells[rowDatePosition, 2];
                                    periodEndTime = dateCell.Value.ToString();

                                    bDateTimeCell = false;

                                }


                                bRtn = true;
                                break;
                            }
                        }


                        rowDatePosition++;
                    }

                    if (bRtn)
                    {
                        // 日付セル情報の設定
                        Excel.Worksheet periodSheet = null;
                        if (bDateTimeCell)
                        {
                            if (!String.IsNullOrEmpty(periodSheetName))
                            {
                                periodSheet = subDACSheets[periodSheetName];

                                periodSheet.Cells.Range[periodStartDate].Value = startDateTime.ToString("yyyy/MM/dd HH:mm") + ":00.000";
                                // 2020.09.04 DACでMissionを実行すると、集計表の時間範囲は1分多かったという問題がありましたので変更した。
                                //periodSheet.Cells.Range[periodEndTime].Value = endDateTime.ToString("HH:mm") + ":59.999";
                                periodSheet.Cells.Range[periodEndDate].Value = endDateTime.ToString("yyyy/MM/dd HH:mm") + ":59.999";
                            }
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(periodSheetName))
                            {
                                periodSheet = subDACSheets[periodSheetName];

                                periodSheet.Cells.Range[periodStartDate].Value = startDateTime.ToString("yyyy/MM/dd");
                                periodSheet.Cells.Range[periodStartTime].Value = startDateTime.ToString("HH:mm") + ":00.000";
                                periodSheet.Cells.Range[periodEndDate].Value = endDateTime.ToString("yyyy/MM/dd");
                                // 2020.09.04 DACでMissionを実行すると、集計表の時間範囲は1分多かったという問題がありましたので変更した。
                                //periodSheet.Cells.Range[periodEndTime].Value = endDateTime.ToString("HH:mm") + ":59.000";
                                periodSheet.Cells.Range[periodEndTime].Value = endDateTime.ToString("HH:mm") + ":59.000";
                            }

                        }


                        string endAddress = "";
                        Excel.Range resultCell = null;

                        if (resultSheetTmp != null)
                        {
                            // resultシートを空にする
                            Excel.Worksheet resultSheet = null;
                            if (bNormalFormat)
                            {
                                resultSheet = subDACSheets[ClsReadData.RESULT_SHEET_NAME];
                            }
                            else
                            {
                                resultSheet = subDACSheets[ClsReadData.RESULT_SHEET_NAME2];
                            }

                            if (resultRowPosition > 1)
                            {
                                endAddress = resultSheet.Cells[resultRowPosition, resultColPosition].Address;
                                resultCell = resultSheet.Cells.Range["A2:" + endAddress];
                                //resultCell.Delete();
                            }
                        }

                        bRtn = true;
                    }

                }
                else
                {
                    bRtn = false;
                }



            }
            catch (Exception e)
            {
                bRtn = false;
                missionLog.OutPut("setDateAndResult", e.Message + "\n" + e.StackTrace, ClsMissionLog.LOG_LEVEL.DEBUG);
            }
            finally
            {
                if (workbook != null)
                {

                    workbook.Save();
                    workbook.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);

                }


                if (app != null)
                {
                    try
                    {
                        app.Quit();

                        System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                    }
                    catch
                    {

                    }
                }
            }

            return bRtn;

        }

        /*
         * SubDACファイルの期間変更計算＋結果シートの複製を実施する
         */
        private static bool calculateSubDACFile(string createFile)
        {
            bool bRtn = false;

            // 期間変更マクロを実行する
            bRtn = ClsExcelUtil.execMacro(createFile, "Main_Module.OperationTest");
            if (bRtn)
            {
                // Excel結果シートの複製
                bRtn = ClsExcelUtil.copySubDACResultSheet2(createFile);

                int refreshCount = ClsExcelUtil.refreshPivotInBook(createFile);

            }

            return bRtn;
        }

        /*
         * 引数の式がSubDAC参照式かどうかを判定する
         */
        public static Boolean isSubDACRefere(string subDACFormula)
        {
            bool bRtn = false;


            if (subDACFormula.StartsWith("SubDAC(") && subDACFormula.EndsWith(")"))
            {
                subDACFormula = subDACFormula.Substring(0, subDACFormula.Length - 1).Replace("SubDAC(", "");
                string[] arrayFormula = arrayFormula = subDACFormula.Split(':');

                if (arrayFormula != null && arrayFormula.Length == 3)
                {
                    bRtn = true;
                }
                else
                {
                    bRtn = false;
                }
            }
            else
            {
                bRtn = false;
            }

            return bRtn;

        }

        // Wang Issue reference to value of cell 20190228 Start
        public static Boolean isSelfDACRefere(string subDACFormula)
        {
            bool bRtn = false;


            if (subDACFormula.StartsWith("SelfDAC(") && subDACFormula.EndsWith(")"))
            {
                subDACFormula = subDACFormula.Substring(0, subDACFormula.Length - 1).Replace("SelfDAC(", "");
                string[] arrayFormula = subDACFormula.Split(':');

                if (arrayFormula != null && arrayFormula.Length == 3)
                {
                    bRtn = true;
                }
            }

            return bRtn;

        }
        // Wang Issue reference to value of cell 20190228 End

        /*
         * SubDAC参照式を解析して、その値を取得する
         *  date:計算値を求める日付
         *  dacName：DAC帳票名（拡張子なし）
         *  subDACFormula：SubDAC参照式
         */
        public static string getSubDACData(string date, string dacName, string subDACFormula)
        {
            //dac No.58,59 sun 20190712 
            //double→string
            string dValue = "";

            date = date.Replace("/", "");
            string[] arrayFormula = null;

            // SubDAC参照式のレイアウトをチェックする
            if (isSubDACRefere(subDACFormula))
            {
                subDACFormula = subDACFormula.Substring(0, subDACFormula.Length - 1).Replace("SubDAC(", "");
                arrayFormula = subDACFormula.Split(':');
            }
            else
            {
                return dValue;
            }


            // SubDAC参照式を分割して、SubDAC参照式の結果を取得する
            if (arrayFormula != null && arrayFormula.Length == 3)
            {
                //親のパスを取得
                if (!dacName.Equals(Globals.ThisWorkbook.clsSubDACControler.RootDACName))
                {
                    dacName = Globals.ThisWorkbook.clsSubDACControler.RootDACName + "\\" + dacName;
                }

                //string fileName = Globals.ThisWorkbook.clsDACConf.SubDacDir + dacName + "\\" + date + "\\" + arrayFormula[0] + "_" + date + ".xlsm";
                string fileName = System.IO.Path.Combine(Globals.ThisWorkbook.clsDACConf.DataDir, date, arrayFormula[0] + "_" + date + ".xlsm");
                //string fileName2 = Globals.ThisWorkbook.clsDACConf.SubDacDir + dacName + "\\" + date + "\\" + arrayFormula[0] + ".xlsm";
                string fileName2 = System.IO.Path.Combine(Globals.ThisWorkbook.clsDACConf.DataDir, date, arrayFormula[0] + ".xlsm");
                //参照式のコピーのため、実行の日を入れ替える
                string formula = arrayFormula[0].Substring(0, arrayFormula[0].LastIndexOf("_"));
                //string fileName3 = Globals.ThisWorkbook.clsDACConf.SubDacDir + dacName + "\\" + date + "\\" + formula + "_" + date + ".xlsm";
                string fileName3 = System.IO.Path.Combine(Globals.ThisWorkbook.clsDACConf.DataDir, date, formula + "_" + date + ".xlsm");
                if (System.IO.File.Exists(fileName))
                {
                    dValue = ClsExcelUtil.getCellValueByAddress(fileName, arrayFormula[1], arrayFormula[2]);
                }
                //第2階層の場合
                else if (System.IO.File.Exists(fileName2))
                {
                    dValue = ClsExcelUtil.getCellValueByAddress(fileName2, arrayFormula[1], arrayFormula[2]);
                }
                //参照式のコピーのため、実行の日を入れ替える
                else if (System.IO.File.Exists(fileName3))
                {
                    dValue = ClsExcelUtil.getCellValueByAddress(fileName3, arrayFormula[1], arrayFormula[2]);
                }
            }

            return dValue;

        }

        // Wang Issue reference to value of cell 20190228 Start
        public static string getSelfDACData(string date, string dacName, string subDACFormula, Excel.Sheets sheets)
        {
            //dac No.58,59 sun 20190712 
            //double→string
            string dValue = "";

            string[] arrayFormula = null;

            // SubDAC参照式のレイアウトをチェックする
            if (isSelfDACRefere(subDACFormula))
            {
                subDACFormula = subDACFormula.Substring(0, subDACFormula.Length - 1).Replace("SelfDAC(", "");
                arrayFormula = subDACFormula.Split(':');
            }
            else
            {
                return dValue;
            }


            // SubDAC参照式を分割して、SubDAC参照式の結果を取得する
            if (arrayFormula != null && arrayFormula.Length == 3)
            {
                try
                {
                    // Wang Issue R2-143 20190329 Start
                    //if (arrayFormula[0].Equals(System.IO.Path.GetFileNameWithoutExtension(Globals.ThisWorkbook.Name)))
                    if (string.IsNullOrEmpty(arrayFormula[0]) || arrayFormula[0].Equals(System.IO.Path.GetFileNameWithoutExtension(Globals.ThisWorkbook.Name)))
                    // Wang Issue R2-143 20190329 End
                    {
                        // Wang Issue reference to value of cell 20190228 Start
                        Globals.ThisWorkbook.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                        Globals.ThisWorkbook.Application.Calculate();
                        // Wang Issue reference to value of cell 20190228 End
                        Excel.Worksheet dataSheet = sheets[arrayFormula[1]];
                        Excel.Range dataCell = dataSheet.get_Range(arrayFormula[2]);
                        dValue = dataCell.Value.ToString();
                    }
                }
#pragma warning disable
                catch (Exception e)
#pragma warning restore
                {

                }
            }

            return dValue;

        }
        // Wang Issue reference to value of cell 20190228 End

 

    }
}
