﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

using AIStool_DAC.read;
using AIStool_DAC.conf;
using FoaCoreCom.dac;
using AIStool_DAC.util.log;
using log4net;

namespace AIStool_DAC.util.subDAC
{
    public class ClsSubDACControler
    {
        // DACファイル
        public string DACFullName { get; private set; }

        public string DACName { get; private set; }

        public string ParentDACName { get; private set; }

        // 階層
        public int Hierarchy { get; private set; }

        public FoaCoreCom.dac.model.DacManifest manifest {get;private set;}
        // SubDAC管理テキスト
        // public String SubDACControlText { get; private set; }

        // SubDAC管理シートの最新情報を保存するクラス
        // public LinkedList<ClsSubDACFile> ClsSubDACFileList { get; private set; }

        // private static readonly ILog logger = LogManager.GetLogger(typeof(ClsSubDACControler));
        private ClsDACConf clsDACConf { get; set; }

        public ClsSubDACControler(string dacFile,ClsDACConf clsDACConf)
        {
            DACFullName = dacFile;
            this.clsDACConf = clsDACConf;
            manifest = FoaCoreCom.dac.model.DacManifest.loadFromFile(clsDACConf.ManifestPath);
            // 階層の設定
            setHierarchy();

            // DAC名の設定
            setDACName();

            // 定義ファイルの決定
            //SubDACControlText = Path.Combine(ClsDACConf.SubDacDir, ParentDACName, DACName, DACName + ".txt");

            //setSubDACFileList();
        }
        /*
        private void setSubDACFileList()
        {
            if (ClsSubDACFileList == null)
            {
                ClsSubDACFileList = new LinkedList<ClsSubDACFile>();
            }
            ClsSubDACFileList.Clear();


            if (File.Exists(SubDACControlText))
            {
                using (StreamReader sr = new StreamReader(SubDACControlText, System.Text.Encoding.GetEncoding("shift_jis")))
                {
                    // ヘッダー行の読み飛ばし
                    if (sr.Peek() > -1)
                    {
                        Globals.ThisWorkbook.workbookLog.OutPut("Excel起動((setSubDACFileList)",
                            SubDACControlText + "ファイルの読み込み（ヘッダー行）" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                        sr.ReadLine();
                    }

                    // ヘッダー行以降の読み込み
                    while (sr.Peek() > -1)
                    {
                        Globals.ThisWorkbook.workbookLog.OutPut("Excel起動((setSubDACFileList)",
                            SubDACControlText + "ファイルの読み込み（データ行）" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);
                        ClsSubDACFile clsSubDACFile = new ClsSubDACFile(this.ParentDACName, sr.ReadLine());
                        ClsSubDACFileList.AddLast(clsSubDACFile);
                    }
                }

            }
            else
            {
                Globals.ThisWorkbook.workbookLog.OutPut("Excel起動((setSubDACFileList)",
                    SubDACControlText + "ファイルが存在しない" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);
            }
        }
        */
        /*
        public ClsSubDACFile createSubDACFile(string templateName, string subDACName)
        {
            ClsSubDACFile clsSubDACFile = new ClsSubDACFile(this.ParentDACName,this.DACName, templateName, subDACName);

            return clsSubDACFile;
        }
        */
        public FoaCoreCom.dac.model.DacManifest.ManifestFileInfo createSubDACFile(string filepath)
        {
            FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subdacFile = new FoaCoreCom.dac.model.DacManifest.ManifestFileInfo(filepath);
            return subdacFile;
        }

        /*
        *  ファイルの追加処理
        */
        /*
        public void addSubDACFile(ClsSubDACFile subDACFile)
        {
            
            // １．リストへファイル情報の追加
            subDACFile.Sort =ClsSubDACFileList.Count+1;
            ClsSubDACFileList.AddLast(subDACFile);

            //saveSubDACText();

        }
        */
        public void addSubDACFile(FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subDACFile)
        {
            Globals.ThisWorkbook.workbookLog.OutPut("addSubDACFile: ", subDACFile.name+","+subDACFile.displayname, util.log.ClsMissionLog.LOG_LEVEL.DEBUG);
            subDACFile.index = manifest.fileList.Count;
            Globals.ThisWorkbook.workbookLog.OutPut("addSubDACFile: ",  "At:" + subDACFile.index, util.log.ClsMissionLog.LOG_LEVEL.DEBUG);
            manifest.fileList.Add(subDACFile);
            manifest.Save();
        }
        /*
        public void deleteSubDACFile(string subDACName)
        {
            foreach (ClsSubDACFile clsSubDACFile in ClsSubDACFileList)
            {
                if (clsSubDACFile.Name.Equals(subDACName))
                {
                    ClsSubDACFileList.Remove(clsSubDACFile);
                    break;
                }
            }

            // 順序の並べ替え
            Array clsSubDACFileArray = Globals.ThisWorkbook.clsSubDACFileList.ToArray();
            Array.Sort(clsSubDACFileArray);
            int i = 0;
            foreach (ClsSubDACFile subDACfile in clsSubDACFileArray)
            {
                i++;
                subDACfile.Sort = i;
            }

            //saveSubDACText();
        }
        */
        public string getFullPath(FoaCoreCom.dac.model.DacManifest.ManifestFileInfo fileInfo,string date)
        {
            date = date.Replace("/", "");

            string fileName = "";
            if (fileInfo.template.Equals(Properties.Resources.TEXT_STD_MULTIDAC) || fileInfo.template.Equals(Properties.Resources.TEXT_STD_SPREADSHEET))
            {
                string filenamemain = Path.GetFileNameWithoutExtension(fileInfo.name);
                fileName = Path.Combine(this.clsDACConf.DataDir, date, filenamemain + "_" + date + ".xlsm");                
            }
            else
            {
                fileName = Path.Combine(this.clsDACConf.DacDir, fileInfo.name);
            }
            Globals.ThisWorkbook.workbookLog.OutPut("getFullPath: ", fileName, util.log.ClsMissionLog.LOG_LEVEL.DEBUG);
            return fileName;
        }

        public void deleteSubDACFile(string subDacguid)
        {
            manifest.DeleteFile(subDacguid);
        }
        public void deleteSubDACFileFromTime(string subDacguid, DateTime datetime)
        {
            manifest.DeleteFileFromTime(subDacguid, datetime);
        }
        /**
         * 最新のSubDAC管理リストの内容をSubDAC管理シートに反映する
         * 
         */
        /*
        public void saveSubDACText()
        {

            //DAC-61 sunyi 20181226 start
            //logger.Debug("SubDACControlText: " + SubDACControlText);
            Globals.ThisWorkbook.workbookLog.OutPut("SubDACControlText: ", SubDACControlText, ClsMissionLog.LOG_LEVEL.DEBUG);
            if (!Directory.Exists(System.IO.Path.GetDirectoryName(SubDACControlText)))
            {
                Directory.CreateDirectory(System.IO.Path.GetDirectoryName(SubDACControlText));
                if (!File.Exists(SubDACControlText))
                {
                    var file = File.Create(SubDACControlText);
                    file.Close();
                }
            }
            //DAC-61 sunyi 20181226 end
            using (StreamWriter sw = new StreamWriter(SubDACControlText, false, System.Text.Encoding.GetEncoding("shift_jis")))
            {
                sw.WriteLine("DAC名,テンプレート種別,ファイル名（拡張子あり）,並び順,削除日");
                foreach (ClsSubDACFile clsSubDacFile in ClsSubDACFileList.ToArray())
                {
                    //logger.Debug("SubDacFile: " + clsSubDacFile.RootPath);
                    Globals.ThisWorkbook.workbookLog.OutPut("SubDACFile: ", clsSubDacFile.RootPath, ClsMissionLog.LOG_LEVEL.DEBUG);
                    sw.WriteLine(clsSubDacFile.DACName + "," + clsSubDacFile.Kind + "," + clsSubDacFile.Name+clsSubDacFile.Ext + "," + clsSubDacFile.Sort + "," + clsSubDacFile.deleteDate);
                }
            }

        }
        */

        /**
         * SubDacファイルリストからSubDacファイル名（日付なし）でClsSubDacFileクラスを取得
         */
        /*
        public ClsSubDACFile getSubDACByName(string name)
        {
            foreach (ClsSubDACFile subDac in Globals.ThisWorkbook.clsSubDACFileList)
            {
                if (subDac.Name.Equals(name))
                {
                    return subDac;
                }
            }

            return null;

        }
        */
        public FoaCoreCom.dac.model.DacManifest.ManifestFileInfo getSubDACByName(string name)
        {
            foreach (var subDac in manifest.fileList)
            {
                Globals.ThisWorkbook.workbookLog.OutPut("getSubDACByName", "subDac: "+subDac.displayname + " compared Name:" + name, ClsMissionLog.LOG_LEVEL.DEBUG);
                if (subDac.displayname.Equals(name))
                {
                    return subDac;
                }
            }
            return null;
        }
        private void setHierarchy()
        {
            // Wang Issue DAC-43 20181114 Start
            //if (DACFullName.StartsWith(ClsDACConf.SubDacDir))
            Globals.ThisWorkbook.workbookLog.OutPut("Set Hierarchy", "DirFullName: " + DACFullName, ClsMissionLog.LOG_LEVEL.DEBUG);
            FoaCoreCom.dac.model.DacManifest.ManifestFileInfo maindac = manifest.GetMainDacInfo();
            Globals.ThisWorkbook.workbookLog.OutPut("Set Hierarchy", "maindac: " + (maindac == null ? "null":maindac.name), ClsMissionLog.LOG_LEVEL.DEBUG);
            if (maindac.name.Equals(Path.GetFileName(DACFullName)))
            {
                ParentDACName = "";
                Hierarchy = 1;
            } else
            {
                ParentDACName = Path.GetFileNameWithoutExtension(maindac.name);
                Hierarchy = 2;
            }
            /*
            Globals.ThisWorkbook.workbookLog.OutPut("Set Hierarchy", "SubDacDirName: " + ClsDACConf.SubDacDir, ClsMissionLog.LOG_LEVEL.DEBUG);
            //if (DACFullName.StartsWith(ClsDACConf.SubDacDir, StringComparison.OrdinalIgnoreCase))
            // Wang Issue DAC-43 20181114 End
            {
                string dir = Path.GetDirectoryName(DACFullName);
                Globals.ThisWorkbook.workbookLog.OutPut("Set Hierarchy", "DirName: " + dir, ClsMissionLog.LOG_LEVEL.DEBUG);
                Globals.ThisWorkbook.workbookLog.OutPut("Set Hierarchy", "SubDacDirName: " + ClsDACConf.SubDacDir, ClsMissionLog.LOG_LEVEL.DEBUG);
                // AISから出力した場合と、それ以外で場合分けする。
                // Wang Issue DAC-43 20181114 Start
                //if (dir.Equals(ClsDACConf.SubDacDir.Substring(0, ClsDACConf.SubDacDir.Length - 1)))
                if (dir.Equals(ClsDACConf.SubDacDir.Substring(0, ClsDACConf.SubDacDir.Length - 1), StringComparison.OrdinalIgnoreCase))
                // Wang Issue DAC-43 20181114 End
                {
                    ParentDACName = "";
                    Hierarchy = 1;
                }
                else{

                    // Wang Issue DAC-43 20181114 Start
                    //string dacPath = dir.Replace(ClsDACConf.SubDacDir, "");
                    // Wang Issue DAC-43 20181114 End
                    string dacPath = dir.ToLower().Replace(ClsDACConf.SubDacDir.ToLower(), "");

                    string[] arrayDacPath = dacPath.Split('\\');
                    Hierarchy = arrayDacPath.Length;




                    for(int i=0;i<Hierarchy-1;i++){
                        if(i==0){
                            ParentDACName = arrayDacPath[i] + '\\';
                        }
                        else{
                            ParentDACName = ParentDACName + arrayDacPath[i] + '\\';

                        }
                    }
                    ParentDACName = ParentDACName.Substring(0, ParentDACName.Length - 1);

                }

            }
            else
            {
                ParentDACName = "";
                Hierarchy = 1;
            }
            */
        }

        private void setDACName()
        {
            if (Hierarchy == 1)
            {
                DACName = Path.GetFileNameWithoutExtension(DACFullName);
            }
            else
            {
                DACName = Path.GetFileNameWithoutExtension(DACFullName);

                //パターンを指定してRegexオブジェクトを作成
                System.Text.RegularExpressions.Regex r =
                    new System.Text.RegularExpressions.Regex(@"_\d{8}$");

                DACName = r.Replace(DACName, "");

            }
        }

        public string RootDACName
        {
            get
            {
                if (ParentDACName.Equals(""))
                {
                    return DACName;
                }
                else
                {
                    string[] tmp = ParentDACName.Split('\\');
                    return tmp[0];
                }
            }
        }

        public string BasePath
        {
            get
            {
                Globals.ThisWorkbook.workbookLog.OutPut("ClsSubDACController BasePath",
                    //Path.Combine(Globals.ThisWorkbook.clsDACConf.SubDacDir, RootDACName + "\\"), ClsMissionLog.LOG_LEVEL.DEBUG);    
                    Path.GetDirectoryName(DACFullName), ClsMissionLog.LOG_LEVEL.DEBUG);
                return Path.GetDirectoryName(DACFullName);
                
            }
        }
    }
}
