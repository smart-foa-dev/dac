﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace AIStool_DAC.util.log
{
    /// <summary>
    /// ログ出力クラス
    /// </summary>
    public class ClsMissionLog
    {

        public enum LOG_LEVEL { DEBUG = 0, INFO = 1, WARN = 2, ERROR = 3 };
        public enum LOG_MONITOR_TYPE { NONE=0,DETAIL = 1, SIMPLE=2 };

        /// <summary>
        /// ロック処理用
        /// </summary>
        private Object m_thisLock = new Object();

        private string m_logClassName;

        private string m_logFilePath;

        private string m_logFileName;

        public string LogFullName
        {
            get
            {
                return Path.Combine(m_logFilePath, m_logFileName);
            }
        }

        private LOG_MONITOR_TYPE m_monitorType;

        private LOG_LEVEL m_logLevel;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public ClsMissionLog(string logClassName, string logFilePath, string logFileName, LOG_MONITOR_TYPE monitorType, LOG_LEVEL logLevel)
        {
            this.m_logClassName = logClassName;
            this.m_logFilePath = logFilePath;
            this.m_logFileName = logFileName;
            this.m_logLevel = logLevel;

            this.m_monitorType = monitorType;
        }

        public void OutPut(string methodName, object Event, LOG_LEVEL logLevel)
        {
            try
            {
                //
                if (logLevel < m_logLevel)
                {
                    return;
                }

                string StringItem = null;
                Exception VbExceptionItem = null;
                System.Text.StringBuilder MessageHeader = new System.Text.StringBuilder();
                System.Text.StringBuilder Messages = new System.Text.StringBuilder();
//                string tempPath = null;

                if (!Directory.Exists(m_logFilePath))
                {
                    Directory.CreateDirectory(m_logFilePath);
                }

                DateTime OutputDate = DateTime.Now;


                // モニターの種別により、ログファイルの上書き、ログファイルの出力内容を切り替える。
                using (StreamWriter logFileStreamWriter = new System.IO.StreamWriter(Path.Combine(m_logFilePath, m_logFileName), m_monitorType.Equals(LOG_MONITOR_TYPE.DETAIL)))
                {
                    if (this.m_monitorType.Equals(LOG_MONITOR_TYPE.DETAIL))
                    {

                        Messages.AppendLine("");
                        MessageHeader.Append(OutputDate.ToString("yyyy/MM/dd HH:mm:ss.fff") + ", ");
                        MessageHeader.Append(logLevel + ", ");
                        MessageHeader.Append(methodName + ", ");

                        if ((Event == null))
                        {

                        }
                        else
                        {
                            //ExceptionObj = Stringの場合
                            if (Event is string)
                            {
                                StringItem = (string)Event;
                                //Stringにキャスト
                                Messages.Append(MessageHeader.Append(StringItem));
                            }

                            //ExceptionObj = vbエラーの場合
                            else if (Event is Exception)
                            {
                                VbExceptionItem = (Exception)Event;
                                //Exceptionにキャスト
                                MessageHeader.Append("Message:    " + VbExceptionItem.Message);
                                Messages.AppendLine(MessageHeader.ToString());
                                Messages.AppendLine("StackTrace: " + VbExceptionItem.StackTrace);

                                Messages.Append("----------------------------------------------------------------------------------");
                                Messages.Append("----------------------------------------------------------------------------------");
                                Messages.AppendLine("----------------------------------------------------------------------------------");

                            }

                        }

                    }
                    else
                    {
                        if ((Event == null))
                        {

                        }
                        else
                        {
                            //ExceptionObj = Stringの場合
                            if (Event is string)
                            {
                                StringItem = (string)Event;
                                //Stringにキャスト
                                Messages.Append(StringItem);
                            }
                        }
                    }

                    lock (m_thisLock)
                    {
                        logFileStreamWriter.WriteLine(Messages.ToString());
                        logFileStreamWriter.Close();
                    }


                }
            }
            catch (Exception ex)
            {
                ex = new Exception();
            }

        }

    }
}
