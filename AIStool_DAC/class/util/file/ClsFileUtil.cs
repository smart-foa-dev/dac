﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

using System.IO;
using System.IO.Compression;
using AIStool_DAC.conf;
using AIStool_DAC.util.subDAC;

namespace AIStool_DAC.util.file
{
    public class ClsFileUtil
    {

        public static ClsWriteLog m_writeLog = new ClsWriteLog("ClsFileUtil");


        /**
         *  SubDAC格納用のディレクトリを作成する（日付箇所まで作成）
         */
        public static void createDirectory(string path)
        {
            try
            {
                System.IO.DirectoryInfo di =
                    System.IO.Directory.CreateDirectory(path);
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("createDirectory:" + path, ex.Message + ex.StackTrace);

            }
        }

        /*
         * 作成対象のファイルが存在していない場合は、別日のファイルから作成
         * 
         */
        
        public static string replicationFile(string date, FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subDACFile)
        {
            
            date = date.Replace("/", "");
            string createPath = "";
            string createFile = "";
            if(subDACFile.template.Equals(Properties.Resources.TEXT_STD_MULTIDAC) ||
                subDACFile.template.Equals(Properties.Resources.TEXT_STD_DAC) ||
                subDACFile.template.Equals(Properties.Resources.TEXT_STD_SPREADSHEET))
            {
                
                createPath = Path.Combine(Globals.ThisWorkbook.clsDACConf.DataDir,date);
                if(!Directory.Exists(createPath))
                {
                    Directory.CreateDirectory(createPath);
                }
                createFile = Path.Combine(createPath,subDACFile.displayname + "_" + date + ".xlsm");
            }
            else
            {
                createPath = Globals.ThisWorkbook.Path;
                //createFile = Path.Combine(createPath, subDACFile.displayname + subDACFile.extension);
                createFile = Path.Combine(createPath, subDACFile.name);
            }

            string fromFile = Path.Combine(Globals.ThisWorkbook.Path, subDACFile.name);

            // ファイルが存在しない場合は、直近のミッション実行済みのファイルから複製し、
            // ミッション実行済みファイルがない場合は、@@@付きのD&Dされたファイルから複製する。
            if (!System.IO.File.Exists(createFile))
            {
                /*
                string[] files = System.IO.Directory.GetFiles(
                    subDACFile.RootPath, subDACFile.displayname + "*.xlsm", System.IO.SearchOption.AllDirectories);

                //DAC-100 20190914 son start
                Array.Sort(files, CompareLastWriteTime);
                //DAC-100 20190914 son start

                // ミッション実行されたファイル、@@@付きファイルがない場合は空を返す。
                // 異常ケースなので、発生する可能性はない。
                if (files.Length == 0)
                {
                    createFile = "";
                }
                // 見つかったファイルが１つ場合は、ミッション実行済み、@@@付きなどを気にせずにそのファイルから複製する。
                else if (files.Length == 1)
                {
                    fromFile = files[0];
                }
                // ファイルが複数見つかった場合は、直近のミッション実行済みファイルを持ってくる。
                // 日付別でファイルをディレクトリに保存しているため、直近の日付のファイルが、後ろに来る。
                else
                {
                    // 検索されたファイルの後ろから順にチェックし、@@@付きを外す。
                    for (int i = files.Length - 1; i >= 0; i--)
                    {
                        // 先頭まで言った場合は、それを返す。
                        if (i == 0)
                        {
                            fromFile = files[0];
                            break;
                        }

                        // 次の直近のファイルから取得し、@@@付きでない場合は、そのファイルから複製する。
                        fromFile = files[i];
                        if (fromFile.IndexOf("@@@") < 0)
                        {
                            break;
                        }

                    }


                }
                */
                // ディレクトリ,ファイルの作成
                //ClsFileUtil.createDirectory(createPath);
                copyFile(fromFile, createFile);

            }

            return createFile;

        }
        
        public static int CompareLastWriteTime(string fileX, string fileY)
        {
            return DateTime.Compare(System.IO.File.GetLastWriteTime(fileX), System.IO.File.GetLastWriteTime(fileY));
        }

        /**
         *  ファイルコピー
         */
        public static bool copyFile(string fromFile, string toFile,bool secondFlag=false)
        {
            bool bRtn = false;


            for (int i = 0; i < 10; i++)
            {
                try
                {
                    System.IO.File.Copy(fromFile, toFile, true);
                    bRtn = true;
                    break;
                }
                catch(Exception ex)
                {
                    m_writeLog.OutPut("copyFile:fromFile=" + fromFile + ",toFile=" + toFile, ex.Message + ex.StackTrace);

                    bRtn = false;

                    if (!secondFlag)
                    {

                        MessageBox.Show("ファイルのコピーが失敗しました（コピー元：" + fromFile + "," + toFile + ")\n" +
                                        "ファイルのの操作を終了してからOKボタンを教えて下さい。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    }

                }
            }

            return bRtn;
        }

        /**
         * ディレクトリのリネーム
         */
        /*
        public static string moveDirectory(string dacFile)
        {
            string strRtn = "";
            bool bRtn = false;

            string subDAC = Globals.ThisWorkbook.clsDACConf.SubDacDir + System.IO.Path.GetFileNameWithoutExtension(dacFile);
            string subDACBack = Globals.ThisWorkbook.clsDACConf.AisTmpDir + System.IO.Path.GetFileNameWithoutExtension(dacFile) + "_back";

            
            // BACKアップが存在する場合は削除する
            if (System.IO.Directory.Exists(subDACBack))
            {
                bRtn = deleteDirectory(subDACBack);
                if(!bRtn){
                    return "";
                }
            }


            if (System.IO.Directory.Exists(subDAC))
            {
                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        System.IO.Directory.Move(subDAC, subDACBack);
                        strRtn = subDACBack;
                        bRtn = false;
                        break;
                    }
                    catch (Exception ex)
                    {
                        m_writeLog.OutPut("moveDirectory:dacFile=" + dacFile, ex.Message + ex.StackTrace);

                        strRtn = "";
                        string fileName = System.IO.Path.GetFileNameWithoutExtension(subDAC);
                        MessageBox.Show("このディレクトリ（" + subDAC + "）配下のファイルが操作されています。\n" +
                                        "ファイルの操作を終了してからOKボタンを教えて下さい。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    }
                }
            }

            return strRtn;
        }
        */
        /**
         *  １ディレクトリ削除
         */
        public static bool deleteDirectory(string dir)
        {
            bool bRtn = false;

            if (System.IO.Directory.Exists(dir))
            {
                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        System.IO.Directory.Delete(dir,true);
                        bRtn =  true;
                        break;
                    }
                    catch (Exception ex)
                    {
                        m_writeLog.OutPut("deleteDirectory:dir=" + dir, ex.Message + ex.StackTrace);

                        string fileName = System.IO.Path.GetFileNameWithoutExtension(dir);
                        MessageBox.Show("このディレクトリ（" + dir + "）が操作されています。\n" +
                                        "ディレクトリの操作を終了してからOKボタンを教えて下さい。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    }
                    bRtn = false;
                }
            }
            else
            {
                bRtn = true;
            }

            return bRtn;
        }


        /**
         *  １ファイル削除
         */
        public static bool deleteOneFile(string file)
        {
            bool bRtn = false;

            if (System.IO.File.Exists(file))
            {
                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        System.IO.File.Delete(file);
                        bRtn = true;
                        break;
                    }
                    catch (Exception ex)
                    {
                        m_writeLog.OutPut("deleteOneFile:file=" + file, ex.Message + ex.StackTrace);

                        string fileName = System.IO.Path.GetFileNameWithoutExtension(file);
                        MessageBox.Show("このファイル（" + fileName + "）が操作されています。\n" +
                                        "ファイルの操作を終了してからOKボタンを教えて下さい。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    }
                }
                bRtn = false;
            }
            else
            {
                bRtn = true;
            }

            return bRtn;
        }

        /**
         *  該当SubDACのファイルを全て検索して、すべて削除する。
         */
        public static void deleteAllFiles(FoaCoreCom.dac.model.DacManifest.ManifestFileInfo clsSubDACFile)
        {
            try
            {
                string[] datafolders = Directory.GetDirectories(Globals.ThisWorkbook.clsDACConf.DataDir);
                foreach (var datafolder in datafolders)
                {
                    string[] files = System.IO.Directory.GetFiles(
                    datafolder, clsSubDACFile.displayname + "*", System.IO.SearchOption.AllDirectories);

                    foreach (string fileName in files)
                    {
                        deleteOneFile(fileName);
                    }
                }
                string dacDir = Globals.ThisWorkbook.clsDACConf.DacDir;
                string filepath = Path.Combine(dacDir, clsSubDACFile.name);
                deleteOneFile(filepath);
                
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("deleteAllFiles:DacName=" + clsSubDACFile.displayname + ",subDACName=" + clsSubDACFile.displayname , ex.Message + ex.StackTrace);
            }
        }

        /* SubDACファイルの圧縮
         *  引数：DAC帳票
         *      DAC帳票フルパスから拡張子を除去したのがSubDAC用ルートフォルダー
         *      SubDAC用ルートフォルダー.zipが圧縮した結果
         *  戻り値：SubDAC用ZIPファイル
         */
        /*
        public static string compressSubDAC(string dacFullPath)
        {
            // ファイル名からSubDACパスを作成（拡張子のみを取りぞのく）
            //DAC-29 sunyi 20190117 start
            //選択したファイルのuuidを転送する
            //string startPath = Globals.ThisWorkbook.clsDACConf.SubDacDir + "\\" + Path.GetFileNameWithoutExtension(dacFullPath);
            string startPath = Globals.ThisWorkbook.clsDACConf.SubDacDir + Path.GetFileNameWithoutExtension(dacFullPath);
            //DAC-29 sunyi 20190117 end

            string zipPath = startPath + ".zip";


            for (int i = 0; i < 10; i++)
            {
                try
                {
                    deleteOneFile(zipPath);

                    ZipFile.CreateFromDirectory(
                        startPath, zipPath,
                        System.IO.Compression.CompressionLevel.Optimal,
                        true,
                        System.Text.Encoding.GetEncoding("shift_jis"));


                    break;

                }
                catch (Exception ex)
                {
                    m_writeLog.OutPut("compressSubDAC:dacFullPath=" + dacFullPath, ex.Message + ex.StackTrace);

                    if (i == 9)
                    {
                        throw ex;
                    }
                }
            }

            return zipPath;
        }
        */
        /* SubDACファイルの解凍
         *  引数：SubDAC帳票 Zipフルパス
         *      引数に指定されたファイルを
         *      SubDAC用ルートフォルダー＋ZIPファイル名（拡張子なし）に解凍
         *  戻り値：なし
         */
        /*
        public static void thawingSubDac(string zipPath)
        {

            //string SUB_DAC_DIR = @"C:\ais\DAC\";
            string SUB_DAC_DIR = Globals.ThisWorkbook.clsDACConf.SubDacDir;

            string startPath = SUB_DAC_DIR + "\\" + Path.GetFileNameWithoutExtension(zipPath);

            try
            {
                if (System.IO.Directory.Exists(startPath))
                {
                    deleteDirectory(startPath);
                }

                System.IO.Compression.ZipFile.ExtractToDirectory(
                    zipPath,
                    startPath,
                    System.Text.Encoding.GetEncoding("shift_jis"));

            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("thawingSubDac:zipPath=" + zipPath, ex.Message + ex.StackTrace);

            }
        }
        */
        // ファイルの行数を求める。
        public static int countLines(string fileName)
        {
            
            string[] lines = System.IO.File.ReadAllLines(fileName);
            return lines.Length;
        }

        // Wang Issue NO.864 20181005 Start
        /*
        public static void checkFileOpen(string fileName)
        {
            while (true)
            {
                try
                {
                    using (var fs = File.OpenWrite(fileName))
                    {

                    }
                    break;

                }
                catch
                {
                    MessageBox.Show("このファイル（" + fileName + "）が開かれているため、処理を継続できません。\n" +
                                    "ファイルを保存してから終了してください。 その後、OKボタンを押してください。",
                                    "エラー",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);

                }

            }
        }
        */
        public static bool checkFileOpen(string fileName, bool showMsg = true)
        {
            while (true)
            {
                try
                {
                    using (var fs = File.OpenWrite(fileName))
                    {

                    }
                    return false;

                }
                catch
                {
                    if (showMsg)
                    {
                        MessageBox.Show("このファイル（" + Path.GetFileName(fileName) + "）が開かれているため、処理を継続できません。\n" +
                                        "ファイルを保存してから終了してください。 その後、OKボタンを押してください。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                    }
                    return true;
                }

            }
        }
        // Wang Issue NO.864 20181005 Start

    }
}
