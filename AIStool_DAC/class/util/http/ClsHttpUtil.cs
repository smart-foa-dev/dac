﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;

using System.IO;

using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

using System.Threading;


namespace AIStool_DAC.util.http
{
    public class ClsHttpUtil
    {
        /*
        public static string getSubDAC(string fullpath)
        {
            string zipFile = "";

            // ファイル名を取得する。
            string fileName = Path.GetFileNameWithoutExtension(fullpath);

            string url = "http://" + Globals.ThisWorkbook.clsDACConf.GripIp + ":" + Globals.ThisWorkbook.clsDACConf.GripPort + "/grip/rest/dashbox/file/download/name";
            url = url + "?name=" + fileName + ".zip";

            WebClient client = new WebClient();

            if (Globals.ThisWorkbook.clsDACConf.UseProxy == true)
            {
                client.Proxy = new System.Net.WebProxy("http://" + Globals.ThisWorkbook.clsDACConf.ProxyURI);
            }
            else
            {
                client.Proxy = null;
            }

            // データを同期モードでダウンロードする
            byte[] sourceData = null;

            try
            {
                sourceData = client.DownloadData(url);
            }
            catch
            {

            }

            client.Dispose();


            if (sourceData == null || sourceData.Length == 0)
            {
                zipFile = "";
            }
            else
            {
                // 出力先を作成する
                zipFile = Globals.ThisWorkbook.clsDACConf.AisTmpDir + fileName + ".zip";

                // フォルダーが存在しない場合は作成する
                if (!Directory.Exists(Globals.ThisWorkbook.clsDACConf.AisTmpDir))
                {
                    Directory.CreateDirectory(Globals.ThisWorkbook.clsDACConf.AisTmpDir);
                }


                FileStream fs = new FileStream(zipFile, FileMode.Create);
                BinaryWriter bw = new BinaryWriter(fs);

                bw.Write(sourceData);

                //閉じる
                bw.Close();
                fs.Close();

            }

            return zipFile;

        }
        */
        /*
        public static string checkDACUpload(string fullpath)
        {
            string uuid = "";

            // ファイル名を取得する。
            string fileName = Path.GetFileNameWithoutExtension(fullpath);

            string url = "http://" + Globals.ThisWorkbook.clsDACConf.GripIp + ":" + Globals.ThisWorkbook.clsDACConf.GripPort + "/grip/rest/dashbox/file/download/name";
            url = url + "?name=" + fileName + ".txt";


            WebClient client = new WebClient();

            if (Globals.ThisWorkbook.clsDACConf.UseProxy == true)
            {
                client.Proxy = new System.Net.WebProxy("http://" + Globals.ThisWorkbook.clsDACConf.ProxyURI);
            }
            else
            {
                client.Proxy = null;
            }

            /*
            client.DownloadDataCompleted += (sender, eventArgs) =>
            {
                byte[] fileData = eventArgs.Result;

                using (FileStream fileStream = new FileStream(outputPath, FileMode.Create))
                {
                    fileStream.Write(fileData, 0, fileData.Length);
                    fileStream.Flush();
                    fileStream.Close();
                }
                downloadFlag = false;
            };
            

            // データを同期モードでダウンロードする
            byte[] sourceData = null;

            try
            {
                sourceData = client.DownloadData(url);
            }
            catch
            {

            }

            client.Dispose();


            if (sourceData == null || sourceData.Length == 0)
            {
                uuid = "";
            }
            else{
                uuid = System.Text.Encoding.UTF8.GetString(sourceData);
            }

            return uuid;
        }
        */
        //DAC-29 sunyi 20190117 start
        //選択したファイルのuuidを転送する
        //public static async Task<string> replaceDAC(string dacName,string uploadFile,bool dacFlag)
        public static async Task<string> replaceDAC(string dacName,string uploadFile,bool dacFlag,string uuid)
        //DAC-29 sunyi 20190117 end
        {
            try
            {
                string url = "http://" + Globals.ThisWorkbook.clsDACConf.GripIp + ":" + Globals.ThisWorkbook.clsDACConf.GripPort + "/grip/rest/dashbox/file/UploadReplace2";

                var content = new MultipartFormDataContent();
                var fileContent = new StreamContent(File.OpenRead(uploadFile));
                fileContent.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = "file",
                    FileName = Path.GetFileName(uploadFile)
                };
                
                content.Add(fileContent); // これをファイル数分繰り返す

                content.Add(new StringContent(dacName), "name");
                //DAC-29 sunyi 20190117 start
                //選択したファイルのuuidを転送する
                //content.Add(new StringContent("0"), "id");
                content.Add(new StringContent(uuid), "id");
                //DAC-29 sunyi 20190117 end
                content.Add(new StringContent(""+dacFlag), "flag");

                FoaCore.Net.FoaHttpClient client = null;
                if (Globals.ThisWorkbook.clsDACConf.UseProxy == true)
                {
                    client = new FoaCore.Net.FoaHttpClient(true,new WebProxy("http://" + Globals.ThisWorkbook.clsDACConf.ProxyURI));
                }
                else
                {
                    client = new FoaCore.Net.FoaHttpClient();
                }


                Task<string> task = client.Post(url, content);
                string textData = await task;

                while (true)
                {
                    if (task.IsCompleted)
                    {
                        break;
                    }
                    Thread.Sleep(1000);
                }

                return textData;
            }
            catch
            {

            }
            return "";

        }


    }
}
