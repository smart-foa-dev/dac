﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using System.Reflection;
using System.CodeDom.Compiler;

using Microsoft.Office.Tools.Excel;
using Microsoft.VisualBasic;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using XlTool = Microsoft.Office.Tools.Excel;
using Office = Microsoft.Office.Core;


using AIStool_DAC.util.excel;
using AIStool_DAC.util.date;
using AIStool_DAC.util.file;
using AIStool_DAC.util.subDAC;
using System.Windows.Forms;

namespace AIStool_DAC.util.cal
{
    public class ClsCalUtil
    {
        // Wang FoaStudio Issue DAC-70 20181210 Start
        private static readonly string MISSION_M = "ミッション_MISSION";
        private static readonly string MISSION_G = "_Routes";
        private static readonly string MISSION_GRIP = "ミッション_GRIP";
        // Wang FoaStudio Issue DAC-70 20181210 End

        private static readonly int varSheetIDRow = 1;
        private static readonly int varSheetIDCol = 600;


        /// <summary>
        /// 定数sheetName
        /// </summary>
        private static readonly string varSheetName = "DataSheet";


        /// <summary>
        /// ログ書き出し用クラス
        /// </summary>
        private static ClsWriteLog m_writeLog = new ClsWriteLog("ClsCalUtil");


        public static ClsBusinessHoursDto setBusinessHours(Excel.Sheets sheets, DateTime execDateTime)
        {
            Excel.Range startTimeCell;
            Excel.Range endTimeCell;

            startTimeCell = sheets["業務時間"].Range["A1"];
            endTimeCell = sheets["業務時間"].Range["A2"];

            if (ClsExcelUtil.isEmpty(startTimeCell) || !(startTimeCell.Value is double))
            {
                return null;
            }
            else if (ClsExcelUtil.isEmpty(endTimeCell) || !(endTimeCell.Value is double))
            {
                return null;
            }
            else
            {
                DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                ClsBusinessHoursDto clsBusinessHoursDto = new ClsBusinessHoursDto();

                // 業務時間セルの設定
                clsBusinessHoursDto.startTimeCell = startTimeCell;
                clsBusinessHoursDto.endTimeCell = endTimeCell;


                // 開始日を設定
                clsBusinessHoursDto.StartDateTime = execDateTime;

                // 終了日を設定
                // 終了時刻が開始時刻より前の場合次の日の時刻にする
                if (startTimeCell.Value >= endTimeCell.Value)
                {
                    clsBusinessHoursDto.EndDateTime = clsBusinessHoursDto.StartDateTime.AddDays(1);
                }
                else
                {
                    clsBusinessHoursDto.EndDateTime = clsBusinessHoursDto.StartDateTime;
                }

                // 開始時間を設定
                clsBusinessHoursDto.StartDateTime = clsBusinessHoursDto.StartDateTime.AddDays(startTimeCell.Value);
                clsBusinessHoursDto.StartDateTime = DateTime.Parse(clsBusinessHoursDto.StartDateTime.ToString("yyyy/MM/dd HH:mm") + ":00.000");

                // 終了時間を設定
                clsBusinessHoursDto.EndDateTime = clsBusinessHoursDto.EndDateTime.AddDays(endTimeCell.Value);
                clsBusinessHoursDto.EndDateTime = DateTime.Parse(clsBusinessHoursDto.EndDateTime.ToString("yyyy/MM/dd HH:mm") + ":59.999");

                // 開始・終了の標準時間からの経過ミリ秒を設定
                TimeSpan startTimeSpan = clsBusinessHoursDto.StartDateTime - epoch;
                TimeSpan endTimeSpan = clsBusinessHoursDto.EndDateTime - epoch;
                clsBusinessHoursDto.StartEpoMilliseconds = Math.Floor(startTimeSpan.TotalMilliseconds).ToString();
                clsBusinessHoursDto.EndEpoMilliseconds = Math.Floor(endTimeSpan.TotalMilliseconds).ToString();

                return clsBusinessHoursDto;

            }

        }

        public static Excel.Range setMainCalculateCell(Excel.Worksheet mainSheet, DateTime execDateTime)
        {

            Excel.Range mainBaseCell = ClsExcelUtil.findRangeByWord(mainSheet.Cells, "演算行/定数");

            int baseRowPosition = mainBaseCell.Row;
            int baseColumnPostion = mainBaseCell.Column;

            Excel.Range dateCell = null;
            for (int i = 1; i < 50; i++)
            {
                // 日付セルの取得
                dateCell = mainSheet.Cells[baseRowPosition + i, baseColumnPostion];
                if (!ClsExcelUtil.isEmpty(dateCell) && dateCell.Value is DateTime)
                {
                    if (execDateTime.Equals(dateCell.Value))
                    {
                        break;
                    }
                    else
                    {
                        dateCell = null;
                    }
                }
            }

            return dateCell;


        }

        public static int getTimeOutMilliSeconds(Excel.Sheets sheets)
        {
            var range = sheets["param"].Cells;
            var range2 = ClsExcelUtil.findRangeByWord(range, "タイムアウト秒数");
            int timeOutSeconds = 0;
            if (range2 != null)
            {
                string value = range2[1, 2].Text;
                if (!int.TryParse(value, out timeOutSeconds))
                {
                    timeOutSeconds = 10;
                }
            }

            return timeOutSeconds * 1000;
        }

        // Wang FoaStudio Issue DAC-70 20181210 Start
        /// <summary>
        /// Excelの１列の値を取得する
        /// </summary>
        private static string[] GetColumnValues(Excel.Sheets sheets, int col)
        {
            Excel.Worksheet sheet = sheets[MISSION_M];
            return GetColumnValues(sheet, col);
        }

        private static string[] GetColumnValues_G(Excel.Sheets sheets, int col)
        {
            Excel.Worksheet sheet = sheets[MISSION_GRIP];
            return GetColumnValues(sheet, col);
        }

        /// <summary>
        /// 親子関係表すオブジェクトを作成する
        /// </summary>
        private static void BuildDictionary_Grip(string pids, string displayNames, Dictionary<string, Dictionary<string, ClsAttribObject>> dict)
        {
            //親ID
            string pid = pids;
            //ID
            string id = displayNames;
            //表示名
            string displayName = displayNames;

            ClsAttribObject obj = new ClsAttribObject(pid, id, displayName);
            Dictionary<string, ClsAttribObject> list;

            if (dict.ContainsKey(pid))
            {
                if (!dict[pid].ContainsKey(id))
                {
                    dict[pid].Add(id, obj);
                }
                //親IDは既に存在する場合はオブジェクトリストに追加する
            }
            else
            {
                //親IDは存在しない場合はオブジェクトリストを作成する
                list = new Dictionary<string, ClsAttribObject>();
                list.Add(id, obj);
                dict.Add(pid, list);
            }
        }
        // Wang FoaStudio Issue DAC-70 20181210 End

        public static void GetTargetObjects(
            Excel.Sheets sheets,
            Dictionary<string, ClsAttribObject> m_missions,
            Dictionary<string, Dictionary<string, ClsAttribObject>> m_ctms,
            Dictionary<string, Dictionary<string, ClsAttribObject>> m_elements
        )
        {
            // Wang FoaStudio Issue DAC-70 20181210 Start
            /*
            string[] pids;
            string[] ids;
            string[] displayNames;

            Excel.Worksheet sheet = sheets["ミッション"];


            //Missionを読み込む
            ids = GetColumnValues(sheet, 1);
            displayNames = GetColumnValues(sheet, 2);
            for (int i = 0; i < ids.Length; i++)
            {
                string id = (string)ids.GetValue(i);
                string displayName = (string)displayNames.GetValue(i);
                m_missions.Add(id, new ClsAttribObject(id, id, displayName));
            }

            //CTMを読み込む
            pids = GetColumnValues(sheet, 3);
            ids = GetColumnValues(sheet, 4);
            displayNames = GetColumnValues(sheet, 5);
            BuildDictionary(pids, ids, displayNames, m_ctms);

            //Elementを読み込む
            string[] pids1;
            string[] pids2;
            pids1 = GetColumnValues(sheet, 9); // Use MissionID as pids1
            pids2 = GetColumnValues(sheet, 6); // Use CtmID as pids2
            ids = GetColumnValues(sheet, 7);
            displayNames = GetColumnValues(sheet, 8);
            BuildDictionary(pids1, pids2, ids, displayNames, m_elements);
            */

            string[] pids;
            string[] ids;
            string[] displayNames;
            try
            {
                //Missionを読み込む
                ids = GetColumnValues(sheets, 1);
                displayNames = GetColumnValues(sheets, 2);
                for (int i = 0; i < ids.Length; i++)
                {
                    string id = (string)ids.GetValue(i);
                    string displayName = (string)displayNames.GetValue(i);
                    m_missions.Add(id, new ClsAttribObject(id, id, displayName));
                }

                //CTMを読み込む
                pids = GetColumnValues(sheets, 3);
                ids = GetColumnValues(sheets, 4);
                displayNames = GetColumnValues(sheets, 5);
                BuildDictionary(pids, ids, displayNames, m_ctms);

                //Elementを読み込む
                string[] pids1;
                string[] pids2;
                pids1 = GetColumnValues(sheets, 9); // Use MissionID as pids1
                pids2 = GetColumnValues(sheets, 6); // Use CtmID as pids2
                ids = GetColumnValues(sheets, 7);
                displayNames = GetColumnValues(sheets, 8);
                BuildDictionary(pids1, pids2, ids, displayNames, m_elements);
                // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("ClsCalUtil.GetTargetObjects", ex.Message + ex.StackTrace);
            }
            // Multi_Dac 修正
            Excel.Range routesRange = null;
            try
            {
                int routeIndex = 0;
                for (int i = 1; i <= sheets.Count; i++)
                {
                    bool IsGrip = false;
                    Excel.Worksheet routesSheet = sheets[i];
                    if (routesSheet.Name.Equals(routeIndex.ToString() + MISSION_G))
                    {
                        routesRange = routesSheet.UsedRange;
                        IsGrip = true;
                        routeIndex++;
                    }
                    if (IsGrip)
                    {
                        string routeMissionName = "";
                        string routeCtmName = "";
                        string routeElementName = "";
                        for (int j = 1; j <= routesRange.Rows.Count; j++)
                        {
                            Excel.Range routesRange1 = routesRange.Cells[j, 1];
                            string gripMission = routesRange1.Value;
                            Excel.Range routesRange2 = routesRange.Cells[j, 2];
                            string gripCtm = routesRange2.Value;
                            Excel.Range routesRange3 = routesRange.Cells[j, 3];
                            string gripElement = routesRange3.Value;
                            if (gripMission != null)
                            {
                                routeMissionName = gripMission;
                                m_missions.Add(routeMissionName, new ClsAttribObject(routeMissionName, routeMissionName, routeMissionName));
                                if (gripCtm != null)
                                {
                                    routeCtmName = gripCtm;
                                    BuildDictionary_Grip(routeMissionName, routeCtmName, m_ctms);

                                    if (gripElement != null)
                                    {
                                        routeElementName = gripElement;
                                        BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                    }
                                }
                            }
                            if (gripMission == null && gripCtm != null)
                            {
                                if (gripCtm != null)
                                {
                                    routeCtmName = gripCtm;
                                    BuildDictionary_Grip(routeMissionName, routeCtmName, m_ctms);

                                    if (gripElement != null)
                                    {
                                        routeElementName = gripElement;
                                        BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                    }
                                }
                            }
                            if (gripMission == null && gripCtm != null)
                            {
                                if (gripCtm != null)
                                {
                                    routeCtmName = gripCtm;
                                    BuildDictionary_Grip(routeMissionName, routeCtmName, m_ctms);

                                    if (gripElement != null)
                                    {
                                        routeElementName = gripElement;
                                        BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                    }
                                }
                            }
                            if (gripMission == null && gripCtm == null && gripElement != null)
                            {
                                if (gripElement != null)
                                {
                                    routeElementName = gripElement;
                                    BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                }
                            }
                            //if (gripMission == null && gripCtm == null && gripElement == null)
                            //{
                            //    break;
                            //}
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            // Wang FoaStudio Issue DAC-70 20181210 End
        }

        private static string[] GetColumnValues(Excel.Worksheet sheet, int col)
        {
            if (sheet.UsedRange.Rows.Count > 1)
            {
                Excel.Range idRange = sheet.UsedRange.Columns[col];
                return ((System.Array)idRange.Cells.Value).OfType<object>().Select(o => o.ToString()).ToArray();
            }
            else // if only 1 element, 1 ctm, and 1 mission is selected, use the below code..
            {
                Microsoft.Office.Interop.Excel.Range cells = sheet.Cells;

                List<string> values = new List<string>();
                for (int rowIndex = 1; rowIndex <= sheet.UsedRange.Rows.Count; rowIndex++)
                {
                    if (cells[rowIndex, col].Value != null)
                        values.Add(cells[rowIndex, col].Value.ToString());
                }
                return values.ToArray();
            }
        }

        public static void ClearNotExistSheets(
            bool useProxy,
            string proxyURI,
            Excel.Sheets worksheets,
            Dictionary<string, ClsAttribObject> m_missions,
            Dictionary<string, Dictionary<string, ClsAttribObject>> m_ctms,
            string sTime, string eTime, int timeOutMiliSeconds
            )
        {
            HashSet<string> names = new HashSet<string>();

            foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions)
            {
                ClsJsonData[] jsonData = null;
                string missionId = mission.Key;
                jsonData = ClsJsonData.get(useProxy, proxyURI, missionId, sTime, eTime, m_writeLog, timeOutMiliSeconds);
                foreach (KeyValuePair<string, ClsAttribObject> attrObj in m_ctms[missionId])
                {
                    string key = mission.Value.DisplayName + "." + attrObj.Value.DisplayName;
                    names.UnionWith(GetNamesFromMission(mission.Value, attrObj.Value, jsonData));
                }
                jsonData = null;
            }

            foreach (Excel.Worksheet sheet in worksheets)
            {
                var SheetValue = sheet.Cells[varSheetIDRow, varSheetIDCol].Value;
                if (SheetValue != null)
                {
                    if (!names.Contains(SheetValue.ToString()))
                    {
                        sheet.Delete();
                    }
                }
            }
        }



        /// <summary>
        /// メインシートデータ挿入処理
        ///     isAfterFlag:業務終了時間が計算実行時間より後か、前か、
        /// </summary>
        public static void CalcurateSheet(string execDate, string dacName,
            Excel.Sheets sheets, Excel.Worksheet mainSheet, int resultRow, bool isAfterFlag)
        {
            Excel.Range formulaRange = null;        // 演算行の数式入力レンジ（複数セル）
            Excel.Range cellFormula = null;         // 演算式（メインDAC）

            Excel.Range resultCell = null;          // 演算結果格納セル

            try
            {

                Excel.Range mainBaseCell = ClsExcelUtil.findRangeByWord(mainSheet.Cells, "演算行/定数");

                formulaRange = mainSheet.Range[mainSheet.Cells[mainBaseCell.Row, mainBaseCell.Column], mainSheet.Cells[mainBaseCell.Row, mainBaseCell.Column]].EntireRow;

                //演算行が見つからなかった場合は処理を抜ける
                if (formulaRange == null)
                {
                    return;
                }

                //演算行の各セルを見て演算を行う
                for (int i = Sheet1.baseCalCell.Column + 1; i <= 1000; i++)
                {
                    /*
                     * 検索する文字列の形式は"シート名,列名,行名"+(x * "シート名,列名,行名")　の形式
                     */
                    cellFormula = mainSheet.Cells[mainBaseCell.Row, i];


                    resultCell = mainSheet.Cells[resultRow, i];

                    bool dataNothingChk = false; // データシートにデータがあるかチェック
                    try
                    {
                        // 演算行が入力されていない場合は処理の先頭に戻る
                        if (cellFormula.Value == null)
                        {
                            continue;
                        }

                        string strFormula = cellFormula.Value.ToString();
                        strFormula = strFormula.Replace("\r", string.Empty);
                        strFormula = strFormula.Replace("\n", string.Empty);
                        strFormula = strFormula.Replace("\"", "?\"?");
                        strFormula = strFormula.Replace("\"", "?\"?");
                        string[] arrayFormula = strFormula.Split('?');

                        //シート検索フォーマットが入力されていた場合、計算を行う
                        if (arrayFormula.Length > 1)
                        {
                            for (int j = 0; j < arrayFormula.Length; j++)
                            {
                                string[] culcSheet = arrayFormula[j].Split(',');
                                //シート検索フォーマットが入力されていた場合は以下の処理を行う
                                if (culcSheet.Length == 3)
                                {
                                    //各シートの600列の値を比較し、演算名と一致するシートが対象となる
                                    Excel.Worksheet culcWorkSheet = GetCulcWorkSheet(sheets, culcSheet[0]);

                                    // Wang New dac flow 20190308 Start
                                    //if (culcWorkSheet.Cells[1, 1].Value.ToString() == "データ無し")
                                    if (culcWorkSheet.Cells[1, 1].Value != null && culcWorkSheet.Cells[1, 1].Value.ToString() == "データ無し")
                                    // Wang New dac flow 20190308 End
                                    {
                                        arrayFormula[j] = "0";
                                    }
                                    else
                                    {
                                        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                                        // Multi_Dac 修正
                                        // Wang New dac flow 20190308 Start
                                        //int col = culcWorkSheet.Rows[1].Find(culcSheet[1]).Column;
                                        int col = culcWorkSheet.Rows[11].Find(culcSheet[1]).Column;
                                        // Wang New dac flow 20190308 End
                                        int row = 0;
                                        if (culcSheet[0].Contains("_Route-"))
                                        {
                                            row = culcWorkSheet.Columns[1].Find(culcSheet[2]).Row;
                                        }
                                        else
                                        {
                                         // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
                                            row = culcWorkSheet.Columns[2].Find(culcSheet[2]).Row;
                                        }

                                        if (culcSheet[2] == "種類数")
                                        {
                                            string ColLetterTemp = culcWorkSheet.Cells[row, col].Address;
                                            string[] SplitLetter = ColLetterTemp.Split('$');
                                            string ColLetter = SplitLetter[1];
                                            // Wang New dac flow 20190308 Start
                                            //culcWorkSheet.Cells[row, col].FormulaArray = string.Format("=SUM(1/COUNTIF({0}2:{0}{1},{0}2:{0}{1}))", ColLetter, row - 6);
                                            culcWorkSheet.Cells[row, col].FormulaArray = string.Format("=SUM(1/COUNTIF({0}12:{0}{1},{0}12:{0}{1}))", ColLetter, culcWorkSheet.UsedRange.Rows.Count);
                                            // Wang New dac flow 20190308 End

                                            arrayFormula[j] = culcWorkSheet.Cells[row, col].Value.ToString();
                                            try
                                            {
                                                double d = Double.Parse(arrayFormula[j]);
                                                if (d < 0)
                                                {
                                                    arrayFormula[j] = "0";
                                                }
                                            }
                                            catch
                                            {
                                                arrayFormula[j] = "0";
                                            }

                                        }
                                        else
                                        {
                                            arrayFormula[j] = culcWorkSheet.Cells[row, col].Value.ToString();

                                        }


                                    }
                                }
                            }


                            if (dataNothingChk != true) // 取得データがあれば処理続行
                            {
                                bool kakFlag = true;
                                for (int j = 0; j < arrayFormula.Length; j++)
                                {
                                    if (arrayFormula[j] == "\"")
                                    {
                                        if (kakFlag)
                                        {
                                            arrayFormula[j] = "(";
                                            kakFlag = false;
                                        }
                                        else
                                        {
                                            arrayFormula[j] = ")";
                                            kakFlag = true;
                                        }
                                    }
                                }

                                //この時点で数値型と四則演算式の文字列になる
                                string resultStr = String.Join(null, arrayFormula);

                                bool executable = isExecutable(resultStr);

                                if (executable == true)
                                {
                                    resultCell.Value = "=" + resultStr;
                                }
                                else
                                {
                                    resultCell.Value = "=" + resultStr.Replace("(", "\"").Replace(")", "\"");
                                }

                            }

                        }
                        // SubDAC参照式の場合
                        else if (ClsSubDACUtil.isSubDACRefere(strFormula))
                        {
                            string tmp = ClsSubDACUtil.getSubDACData(
                                execDate,
                                dacName,
                                strFormula);
                            if(!string.IsNullOrEmpty(tmp))
                            {
                                resultCell.Value = Double.Parse(tmp);
                            }
                            else
                            {
                                resultCell.Value = tmp;
                            }

                        }
                        // Wang Issue reference to value of cell 20190228 Start
                        // DAC参照式の場合
                        else if (ClsSubDACUtil.isSelfDACRefere(strFormula))
                        {
                            string tmp = ClsSubDACUtil.getSelfDACData(
                                execDate,
                                dacName,
                                strFormula,
                                sheets);
                            if (!string.IsNullOrEmpty(tmp))
                            {
                                resultCell.Value = Double.Parse(tmp);
                            }
                            else
                            {
                                resultCell.Value = tmp;
                            }

                        }
                        // Wang Issue reference to value of cell 20190228 End
                        //フォーマットが入力されていなかった場合はエクセルの基本の式を動かす
                        else
                        {
                            resultCell.Value = String.Format("=\"{0}\"", strFormula);
                        }

                        // 計算実行時間が業務終了時間以降の場合は、日付カラムの背景色を切り替える。
                        if (isAfterFlag)
                        {
                            // 日付の変更
                        }
                    }
                    catch (Exception ex)
                    {
                        m_writeLog.OutPut("calcurateSheet", ex.Message + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("calcurateSheet", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        ///各シートの600列の値を比較し
        ///演算名と一致するシートがある場合、削除して、新規する
        ///演算名と一致するシートがない場合、「Data+count+1」のシートを新規する
        /// </summary>
        private static string GetSheetName(Excel.Sheets sheets, ClsAttribObject mission, ClsAttribObject ctm, string SheetID)
        {
            // Delete data sheet
            foreach (Excel.Worksheet worksheet in sheets)
            {
                var value = worksheet.Cells[varSheetIDRow, varSheetIDCol].Value;
                if (value != null)
                {
                    if (value == SheetID)
                    {
                        worksheet.Delete();
                    }
                }

            }

            // Get existing data sheet names
            HashSet<string> dataSheetNames = new HashSet<string>();

            foreach (Excel.Worksheet newWorksheet in sheets)
            {
                string sheetName = newWorksheet.Name;
                string dataSheetPrefix = sheetName.Substring(0, Math.Min(9, sheetName.Length));

                if (dataSheetPrefix.Equals(varSheetName))
                {
                    dataSheetNames.Add(newWorksheet.Name);
                }
            }

            // Get new data sheet name
            string newDataSheetName = "";
            for (int index = 1; ; index++)
            {
                newDataSheetName = varSheetName + index;
                if (!dataSheetNames.Contains(newDataSheetName))
                {
                    break;
                }
            }

            return newDataSheetName;
        }



        /// <summary>
        /// 親子関係表すオブジェクトを作成する
        /// </summary>
        private static void BuildDictionary(string[] pids, string[] ids, string[] displayNames, Dictionary<string, Dictionary<string, ClsAttribObject>> dict)
        {
            for (int i = 0; i < ids.Length; i++)
            {
                //親ID
                string pid = (string)pids.GetValue(i);
                //ID
                string id = (string)ids.GetValue(i);
                //表示名
                string displayName = (string)displayNames.GetValue(i);

                ClsAttribObject obj = new ClsAttribObject(pid, id, displayName);
                Dictionary<string, ClsAttribObject> list;

                if (dict.ContainsKey(pid))
                {
                    //親IDは既に存在する場合はオブジェクトリストに追加する
                    dict[pid].Add(id, obj);
                }
                else
                {
                    //親IDは存在しない場合はオブジェクトリストを作成する
                    list = new Dictionary<string, ClsAttribObject>();
                    list.Add(id, obj);
                    dict.Add(pid, list);
                }
            }
        }
        private static void BuildDictionary(string[] pids1, string[] pids2, string[] ids, string[] displayNames, Dictionary<string, Dictionary<string, ClsAttribObject>> dict)
        {
            for (int i = 0; i < ids.Length; i++)
            {
                //親ID
                string pid = (string)pids1.GetValue(i) + (string)pids2.GetValue(i);
                //ID
                string id = (string)ids.GetValue(i);
                //表示名
                string displayName = (string)displayNames.GetValue(i);

                ClsAttribObject obj = new ClsAttribObject(pid, id, displayName);
                Dictionary<string, ClsAttribObject> list;

                if (dict.ContainsKey(pid))
                {
                    //親IDは既に存在する場合はオブジェクトリストに追加する
                    dict[pid].Add(id, obj);
                }
                else
                {
                    //親IDは存在しない場合はオブジェクトリストを作成する
                    list = new Dictionary<string, ClsAttribObject>();
                    list.Add(id, obj);
                    dict.Add(pid, list);
                }
            }
        }


        private static HashSet<string> GetNamesFromMission(ClsAttribObject mission, ClsAttribObject ctm, ClsJsonData[] jsonData)
        {
            HashSet<string> names = new HashSet<string>();

            //CTMを列挙
            foreach (ClsJsonData CTM in jsonData)
            {
                //ミッションから対象のCTMを探す　見つかった場合のみ処理を行う
                if (CTM.id == ctm.Id) // 2015.07.24
                {
                    //新規シートの作成
                    string SheetName = mission.DisplayName + "." + ctm.DisplayName;
                    names.Add(SheetName);
                }
            }

            return names;
        }

        /// <summary>
        ///各シートの600列の値を比較し、演算名と一致するシートが対象となる
        /// </summary>
        private static Excel.Worksheet GetCulcWorkSheet(Excel.Sheets sheets, string culcSheet)
        {
            Excel.Worksheet culcWorkSheet = null;

            foreach (Excel.Worksheet newWorksheet in sheets)
            {
                var value = newWorksheet.Cells[varSheetIDRow, varSheetIDCol].Value;
                if (value != null)
                {
                    if (culcSheet == value.ToString())
                    {
                        culcWorkSheet = newWorksheet;
                        break;
                    }
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                    // Multi_Dac 修正
                    else if (culcSheet.Contains(value.ToString()))
                    {
                        culcWorkSheet = newWorksheet;
                    }
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
                }
            }

            return culcWorkSheet;
        }

        public class ClsBusinessHoursDto
        {

            // 終了時間が現在時刻より後（true)か、前(false)か
            public bool isAfterFlag { get; private set; }

            // 業務時間セル共有用
            public Excel.Range startTimeCell { get; set; }
            public Excel.Range endTimeCell { get; set; }



            // 標準時間からの経過ミリ秒（ミッション取得用）
            public string StartEpoMilliseconds { get; set; }
            private string _endEpoMilliseconds;
            public string EndEpoMilliseconds
            {
                get
                {
                    return _endEpoMilliseconds;
                }
                set
                {
                    _endEpoMilliseconds = value;
                    isAfterFlag = ClsDateUtil.isNowAfter(value);
                }
            }

            // 日付（SubDAC計算用）
            public DateTime StartDateTime { get; set; }
            public DateTime EndDateTime { get; set; }
        }

        private static bool isExecutable(string exp)
        {
            bool executable = false;

            try
            {

                exp = exp.Replace("\"", "");

                //計算するためのコード
                string source =
                @"package Evaluator
                {
                    class Evaluator
                    {
                        public function Eval(expr : String) : String 
                        { 
                            return eval(expr); 
                        }
                    }
                }";


                //コンパイルするための準備
                CodeDomProvider cp = new Microsoft.JScript.JScriptCodeProvider();
                ICodeCompiler icc = cp.CreateCompiler();
                CompilerParameters cps = new CompilerParameters();
                CompilerResults cres;
                //メモリ内で出力を生成する
                cps.GenerateInMemory = true;
                //コンパイルする
                cres = icc.CompileAssemblyFromSource(cps, source);

                //コンパイルしたアセンブリを取得
                Assembly asm = cres.CompiledAssembly;
                //クラスのTypeを取得
                Type t = asm.GetType("Evaluator.Evaluator");
                //インスタンスの作成
                object eval = Activator.CreateInstance(t);
                //Evalメソッドを実行し、結果を取得
                string result = (string)t.InvokeMember(
                    "Eval",
                    BindingFlags.InvokeMethod,
                    null,
                    eval,
                    new object[] { exp }
                );

                //結果を表示
                Console.WriteLine(result);

                executable = true;


            }
            catch (Exception ex)
            {
                executable = false;
                Console.WriteLine(ex.Message);
            }

            return executable;

        }
    
    }
}
