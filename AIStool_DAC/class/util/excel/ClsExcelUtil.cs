﻿using System.Diagnostics;
using ExcelApp = Microsoft.Office.Interop.Excel.Application;
using ExcelWorkbook = Microsoft.Office.Interop.Excel.Workbook;
using System.Runtime.InteropServices;
using System;
using System.Text;
using System.Windows.Forms;

using System.IO;


using Excel = Microsoft.Office.Interop.Excel;

using Tool = Microsoft.Office.Tools.Excel.WorkbookBase;

using AIStool_DAC.util.subDAC;
using AIStool_DAC.read;
using Microsoft.JScript;

namespace AIStool_DAC.util.excel
{
    public class ClsExcelUtil
    {

        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);

        private static readonly int SWP_NOSIZE = 0x0001;
        private static readonly int SWP_NOZORDER = 0x0004;
        private static readonly int SWP_SHOWWINDOW = 0x0040;

        private static readonly int HWND_TOPMOST = -1;

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);

        [DllImport("user32.dll")]
        public static extern IntPtr GetDesktopWindow();

        private static int PROCESS_COUNT = 0;
        private static readonly int MAX_ARRANGE_PROCESSES = 10;
        private static readonly int OFFSET_POSITION = 32;
        //dac home No.40 start
        private static readonly string PARAM_SHEET = "param";
        //dac home No.40 end

        public struct Rect
        {
            public int Left { get; set; }
            public int Top { get; set; }
            public int Right { get; set; }
            public int Bottom { get; set; }
        }


        public static void setSizeAndPosition(Excel.Application app)
        {
            // Move excel out of screen
            IntPtr excelHwnd = new IntPtr(Globals.ThisWorkbook.Application.Hwnd);

            // Windowを表示する
            int flag = SWP_SHOWWINDOW;


            SetWindowPos(
                excelHwnd,          // Excel操作
                HWND_TOPMOST,       // 最前列表示（Z方向）
                10,                  // 表示位置（X方向）
                10,                  // 表示位置（Y方向）
                Screen.PrimaryScreen.Bounds.Width - 10, // 表示サイズ（X方向）
                Screen.PrimaryScreen.Bounds.Height -10, // 表示サイズ（Y方向）
                SWP_NOZORDER |  SWP_SHOWWINDOW      // ウィンドウ表示に関するフラグ
                );

            SetForegroundWindow(excelHwnd);



        }


        public static ExcelApp StartExcelProcess()
        {
            int flag = SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW;
            PROCESS_COUNT = PROCESS_COUNT % MAX_ARRANGE_PROCESSES;
            int offset = PROCESS_COUNT * OFFSET_POSITION;
            PROCESS_COUNT++;

            // Create excel application instance
            ExcelApp app = new ExcelApp();
            IntPtr excelHwnd = new IntPtr(app.Hwnd);


            // Move to out of screen
            int outx = Screen.PrimaryScreen.Bounds.Width + 1;
            int outy = Screen.PrimaryScreen.Bounds.Height + 1;
            SetWindowPos(
                excelHwnd, 
                0, 
                outx,   // 表示位置
                outy,   // 表示位置
                0,      // 表示サイズ 
                0,      // 表示サイズ
                flag
                );

            // Show excel application
            app.Visible = false;
            app.DisplayAlerts = false;

            return app;
        }



        /*
         * Cellが未設定かを確認
         */
        public static bool isEmpty(SpreadsheetGear.IRange cell)
        {
            if (cell == null || cell.Value == null || cell.Value.Equals(""))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /*
         * Cellが未設定かを確認
         */
        public static bool isEmpty(Excel.Range cell)
        {
            if (cell == null || cell.Value == null || cell.Value.Equals(""))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /*
         * 先頭行から開始して、最初に空になる行番号を取得する
         */
        public static int getLastRowPosition(SpreadsheetGear.IWorksheet sheet)
        {
            int resultRowPosition = 0;
            SpreadsheetGear.IRange cells = null;

            while (true)
            {
                cells = sheet.Cells[resultRowPosition, 0];
                if (ClsExcelUtil.isEmpty(cells))
                {
                    cells = sheet.Cells[resultRowPosition+1, 0];
                    if (ClsExcelUtil.isEmpty(cells))
                    {
                        break;
                    }
                    else
                    {
                        resultRowPosition++;
                    }
                }
                else
                {
                    resultRowPosition++;
                }

            }

            return resultRowPosition;
        }


        /// <summary>
        /// Range取込
        /// </summary>
        /// <param name="srchRange"></param>
        /// <param name="strWhat"></param>
        /// <returns></returns>
        public static Excel.Range findRangeByWord(Excel.Range srchRange, String strWhat)
        {
            return srchRange.Find(strWhat,
                                System.Type.Missing,
                                Excel.XlFindLookIn.xlValues,
                                Excel.XlLookAt.xlWhole,
                                Excel.XlSearchOrder.xlByRows,
                                Excel.XlSearchDirection.xlNext,
                                false,
                                System.Type.Missing, System.Type.Missing);
        }


        /// <summary>
        /// Range取込
        /// </summary>
        /// <param name="srchRange"></param>
        /// <param name="strWhat"></param>
        /// <returns></returns>
        public static Excel.Range findRangeByDateTime(Excel.Range srchRange, DateTime dateTime)
        {
            return srchRange.Find(dateTime,
                                System.Type.Missing,
                                Excel.XlFindLookIn.xlValues,
                                Excel.XlLookAt.xlWhole,
                                Excel.XlSearchOrder.xlByRows,
                                Excel.XlSearchDirection.xlNext,
                                false,
                                System.Type.Missing, System.Type.Missing);
        }

        /*
         * 
         * sheetの0行・0列からmaxRow行・maxCol列の範囲でsearchValueが見つかるセルを検索する。
         * 
         */
        public static Excel.Range searchCellByValue(Excel.Worksheet sheet, string searchValue, int maxRow, int maxCol)
        {
            Excel.Range cell = null;

            for (int row = 1; row < maxRow; row++)
            {
                for (int col = 1; col < maxCol; col++)
                {
                    try
                    {
                        cell = sheet.Cells[row, col];

                        string value = cell.Value;
                        if (!String.IsNullOrEmpty(value))
                        {
                            if (value.Equals(searchValue))
                            {
                                return cell;
                            }
                        }
                    }
                    catch
                    {

                    }

                }

            }

            return null;
        }

        /*
         * 先頭列から開始して、最初に空になる列番号を取得する
         */
        public static int getLastColPosition(SpreadsheetGear.IWorksheet sheet)
        {
            int resultColPosition = 0;
            SpreadsheetGear.IRange cells = null;

            while (true)
            {
                cells = sheet.Cells[0, resultColPosition];
                if (ClsExcelUtil.isEmpty(cells))
                {
                    break;
                }
                else
                {
                    resultColPosition++;
                }
            }
            return resultColPosition;
        }

        /*
         * SubDACファイルの最終行を取得
         *  SubDACシートの（ファイルリストの最終行+1）の行・1列のセルを返す。
         */
        public static Excel.Range getLastCell()
        {
            Excel.Worksheet subDacSheet = getSubDACSheet();

            // 該当シートの末尾を探す
            int rowPosition = 2;
            Excel.Range cell = subDacSheet.Cells[rowPosition, 1];
            while (true)
            {
                if (!ClsExcelUtil.isEmpty(cell))
                {
                    rowPosition++;
                    cell = subDacSheet.Cells[rowPosition, 1];
                }
                else
                {
                    break;
                }
            }

            return cell;

        }



        /*
         * Excelファイルを開く
         */
        public static void OpenExcelProcess2(string filepath, bool newProcess)
        {
            if (!filepath.StartsWith("\"") &&
                !filepath.EndsWith("\""))
            {
                filepath = "\"" + filepath + "\"";
            }

            var processStartInfo = new ProcessStartInfo();
            if (newProcess)
            {
                processStartInfo.FileName = "Excel.exe";
                processStartInfo.Arguments = "/x " + filepath;
            }
            else
            {
                processStartInfo.FileName = filepath;
            }

            using (Process proc = new Process())
            {
                proc.StartInfo = processStartInfo;
                proc.Start();
            }
        }


        // Wang Issue DAC-46 20181112 Start
        //public static void OpenExcelProcess(string filepath, bool newPro = true)
        public static void OpenExcelProcess(string filepath, bool newPro = true, bool bringToForeground = false)
        // Wang Issue DAC-46 20181112 End
        {
            Microsoft.Office.Interop.Excel.Application xl = null;
            Microsoft.Office.Interop.Excel.Workbooks wbs = null;
            Microsoft.Office.Interop.Excel.Workbook wb = null;

            try
            {
                xl = new Microsoft.Office.Interop.Excel.Application();
                xl.Visible = true;


                // Open excel file
                wbs = xl.Workbooks;
                wb = wbs.Open(filepath);
                //dac home No.40 start
                //第2階層からaddinを押せないように制限する
                try
                {
                    var range = GetSheetFromSheetName_Interop(wb, PARAM_SHEET).get_Range("A1", "J100");
                    // 登録フォルダ
                    var range2 = GetFindRange_Interop(range, "OpenFromR2");
                    if (range2 != null)
                    {
                        range2[1, 2].Value = "TRUE";
                    }
                }
#pragma warning disable
                catch (Exception e)
#pragma warning restore
                {
                }
                //dac home No.40 end


                // Wang Issue DAC-46 20181112 Start
                if (bringToForeground)
                {
                    IntPtr excelHwnd = new IntPtr(xl.Hwnd);
                    SetForegroundWindow(excelHwnd);
                }
                // Wang Issue DAC-46 20181112 End
            }
            finally
            {
                if (wb != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wb);
                    wb = null;
                }

                if (wbs != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wbs);
                    wbs = null;
                }

                if (xl != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xl);
                    xl = null;
                }

            }

        }

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        /**
         * Conf用のro paramから設定値を取得する
         *  引数：キー
         *  戻り値：値
         */
        public static string getDACConfByConfName(string sheetName,string confName)
        {
            string confValue = "";

            Excel.Worksheet noParamSheet = null;
            Excel.Range cell = null;

            // 該当シートの取得
            int sheetNo = getSheetNoByName(Globals.ThisWorkbook.Sheets, sheetName);
            if (sheetNo > 0)
            {
                noParamSheet = Globals.ThisWorkbook.Sheets[sheetNo];
            }

            int rowPosition = 1;
            while (true)
            {

                cell = noParamSheet.Cells[rowPosition, 1];

                if (isEmpty(cell))
                {
                    confValue = "";
                    break;
                }
                else
                {
                    string cellValue = (string)(cell.Value).ToString();
                    if (confName.Equals(cellValue))
                    {
                        cell = noParamSheet.Cells[rowPosition, 2];

                        if (cell.Value == null)
                        {
                            confValue = "";
                        }
                        else
                        {
                            confValue = (string)(cell.Value).ToString();
                        }

                        break;
                    }

                }

                rowPosition++;
            }


            return confValue;
        }


        /**
         * シートの取得
         *  シート名から該当シートを検索して、シート番号を返す。
         */
        private static int getSheetNoByName(Excel.Sheets sheets, string name)
        {
            int sheetNo = 0;
            foreach (Excel.Worksheet sh in sheets)
            {
                if (name.Equals(sh.Name))
                {
                    return sheetNo + 1;
                }
                sheetNo += 1;
            }
            return -1;
        }

        private static int getSubDACRowByName(string name)
        {
            Excel.Worksheet subDacSheet = getSubDACSheet();

            int rowPosition = 2;
            Excel.Range cell = subDacSheet.Cells[rowPosition, 2];
            while (true)
            {
                // セルが空でない場合
                if (!ClsExcelUtil.isEmpty(cell))
                {
                    // セルの内容とパス情報が一致した場合は処理を終了
                    if (cell.Value.Equals(name))
                    {
                        break;
                    }
                    // 一致しない場合は下のセルに移動する
                    else
                    {
                        rowPosition++;
                        cell = subDacSheet.Cells[rowPosition, 2];

                    }
                }
                // Path情報がシートに存在しない場合
                // 見つからずにSub DAC管理シートの末尾に来た場合
                else
                {
                    rowPosition = 0;
                    break;
                }
            }

            return rowPosition;


        }


        public static string getSubDACFile(string path)
        {
            return "";
        }



        /**
         * Sub DACファイルリストの取得
         *  存在する場合：該当シートを返す
         *  存在しない場合：新規にシートを作成して返す。
         */
        public static Excel.Worksheet getSubDACSheet()
        {
            
            Excel.Worksheet subDACSheet = null;
            // 該当シートの取得
            int sheetNo = getSheetNoByName(Globals.ThisWorkbook.Sheets, ClsReadData.SUB_DAC_SHEET);
            if (sheetNo > 0)
            {
                subDACSheet = Globals.ThisWorkbook.Sheets[sheetNo];
            }
            else
            {
                Excel.Range cell = null;

                subDACSheet = Globals.ThisWorkbook.Worksheets.Add(After: Globals.ThisWorkbook.ActiveSheet);
                subDACSheet.Name = ClsReadData.SUB_DAC_SHEET;
                subDACSheet.Visible = Excel.XlSheetVisibility.xlSheetVisible;

                cell = subDACSheet.Cells[1, 1];
                cell.Value = "テンプレート種別";
                cell = subDACSheet.Cells[1, 2];
                cell.Value = "ファイル名（拡張子なし）";
                cell = subDACSheet.Cells[1, 3];
                cell.Value = "並び順";

            }

            return subDACSheet;

        }

        /*
         * 期間変更処理
         */
        public static bool execMacro(string fileName, string macro)
        {
            bool bRtn = false;

            Excel.Application app = null;
            Excel.Workbooks workbooks = null;

            try
            {
                app = ClsExcelUtil.StartExcelProcess();


                workbooks = app.Workbooks;
                workbooks.Open(fileName);
                
                app.Run(macro);

                bRtn = true;
            }
            catch
            {
                bRtn = false;

            }
            finally
            {

                if (workbooks != null)
                {
                    try
                    {
                        // マクロがエラーで失敗した場合に、例外を発生せず、ファイルを保存していない部分があるため、一旦Saveする。
                        workbooks.Item[1].Save();
                    }
                    catch
                    {

                    }
                    finally
                    {
                        workbooks.Close();
                        System.Runtime.InteropServices.Marshal.ReleaseComObject(workbooks);

                    }
                }

                if (app != null)
                {
                    try
                    {
                        app.Quit();

                        System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                    }
                    catch
                    {

                    }
                }
                

            }

            return bRtn;
        }

        /*
         * Book内の全てのピボットをリフレッシュする
         */
        public static int refreshPivotInBook(string createFile)
        {
            int refreshCount = 0;

            ExcelApp app = null;
            Excel.Workbook workbook = null;

            Excel.Sheets sheets = null;


            try
            {
                app = ClsExcelUtil.StartExcelProcess();
                workbook = app.Workbooks.Open(createFile);
                sheets = workbook.Sheets;
                foreach (Excel.Worksheet workSheet in sheets)
                {
                    Excel.PivotTables pivotTables = (Excel.PivotTables)workSheet.PivotTables();

                    if (pivotTables.Count > 0)
                    {
                        foreach (Microsoft.Office.Interop.Excel.PivotTable table in pivotTables)
                        {
                            table.RefreshTable();
                            refreshCount++;
                        }
                    }
                }
            }
            catch
            {
                refreshCount = -1;
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Save();
                    workbook.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);

                }

                if (app != null)
                {
                    try
                    {
                        app.Quit();

                        System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                    }
                    catch
                    {

                    }
                }

            }

            return refreshCount;
        }

        /*
          * 結果シートの複製
          * 
          */
        public static bool copySubDACResultSheet2(string createFile)
        {
            bool bRtn = false;

            ExcelApp app = null;
            Excel.Workbook workbook = null;


            try
            {
                string templateName = ClsSubDACUtil.getTemplateName(createFile);

                SpreadsheetGear.IWorkbook workbookTmp = null;
                SpreadsheetGear.IWorksheets sheetsTmp = null;

                workbookTmp = SpreadsheetGear.Factory.GetWorkbook(createFile);
                sheetsTmp = workbookTmp.Worksheets;

                // 結果シートの確認
                SpreadsheetGear.IWorksheet resultSheetTmp = sheetsTmp[ClsReadData.RESULT_SHEET_NAME];
                if (null == resultSheetTmp)
                {
                    resultSheetTmp = sheetsTmp[ClsReadData.RESULT_SHEET_NAME2];
                }

                if (null == resultSheetTmp)
                {
                    return true;
                }

                int resultRowPosition = getLastRowPosition(resultSheetTmp);
                int resultColPosition = getLastColPosition(resultSheetTmp);
                if (workbookTmp != null)
                {
                    workbookTmp.Close();
                }

                app = ClsExcelUtil.StartExcelProcess();
                workbook = app.Workbooks.Open(createFile);
                Excel.Sheets subDACSheets = workbook.Worksheets;
                //ISSUE_NO.846 sunyi 2018/09/05 Start
                //ActiveSheetを戻す
                Excel.Worksheet activeSheet = workbook.ActiveSheet;
                //ISSUE_NO.846 sunyi 2018/09/05 End
                Excel.Worksheet resultSheet = null;


                // 結果シートの確認
                try
                {
                    resultSheet = subDACSheets[ClsReadData.RESULT_SHEET_NAME];
                }
                catch
                {

                }

                if (null == resultSheet)
                {
                    resultSheet = subDACSheets[ClsReadData.RESULT_SHEET_NAME2];
                }

                Excel.Worksheet resultCopySheet = null;
                try
                {
                    resultCopySheet = subDACSheets[ClsReadData.RESULT_COPY_SHEET_NAME];
                }
                catch{

                }

                if (resultCopySheet == null)
                {
                    // シートの追加
                    resultCopySheet = workbook.Worksheets.Add();
                    // コピーしたワークシートの名前を設定
                    resultCopySheet.Name = ClsReadData.RESULT_COPY_SHEET_NAME;
                }
                else
                {
                    // シートの追加
                    resultCopySheet = workbook.Worksheets.Add();
                    // コピーしたワークシートの名前を設定
                    resultCopySheet.Name = ClsReadData.RESULT_COPY_SHEET_NAME + "2";
                }

                // 結果シートの最終セルを確認する。


                // コピー元の指定
                string resultEndAddress = resultSheet.Cells[resultRowPosition, resultColPosition].Address;
                Excel.Range resultCells = resultSheet.Cells.Range["A1:" + resultEndAddress].EntireRow;

                // コピー先の指定
                string resultCopyEndAddress = resultCopySheet.Cells[resultRowPosition-1 + ClsReadData.RESULT_COPY_HEADER_COUNT, resultColPosition].Address; ;
                Excel.Range resultCopyCells = resultCopySheet.Cells.Range["A" + ClsReadData.RESULT_COPY_HEADER_COUNT + ":" + resultCopyEndAddress].EntireRow;

                // コピーの実施
                resultCells.Copy(resultCopyCells);
                resultCopySheet.Columns.AutoFit();
                if (resultCopySheet.Name.Equals(ClsReadData.RESULT_COPY_SHEET_NAME + "2"))
                {
                    Excel.Worksheet resultCopySheet1 = null;
                    Excel.Worksheet resultCopySheet2 = null;

                    resultCopySheet1 = subDACSheets[ClsReadData.RESULT_COPY_SHEET_NAME];
                    resultCopySheet2 = subDACSheets[ClsReadData.RESULT_COPY_SHEET_NAME + "2"];

                    Excel.Range rangeFrom = resultCopySheet1.Cells.Range["A1:A10"].EntireRow;
                    Excel.Range rangeTo = resultCopySheet2.Cells.Range["A1:A10"].EntireRow;

                    rangeFrom.Copy(rangeTo);

                    app.DisplayAlerts = false;
                    //dac home No.39 start
                    //参照式のため、シート削除をしてはいけません。
                    //resultCopySheet1.Delete();
                    //resultCopySheet2.Name = ClsReadData.RESULT_COPY_SHEET_NAME;
                    resultCopySheet1.UsedRange.Clear();
                    Excel.Range rangeTo1 = resultCopySheet1.UsedRange;
                    Excel.Range rangeFrom1 = resultCopySheet2.UsedRange;
                    rangeFrom1.Copy(rangeTo1);
                    resultCopySheet2.Delete();
                    //dac home No.39 end
                }
                else
                {

                    if (!"過去在庫状況".Equals(templateName) && !"在庫状況".Equals(templateName))
                    {
                        // 結果の書き込み
                        resultSheet = subDACSheets[ClsReadData.RESULT_COPY_SHEET_NAME];

                        Excel.Range cells = resultSheet.Cells;

                        Excel.Range cellCoount;
                        Excel.Range cellSum;
                        Excel.Range cellAverage;
                        Excel.Range cellMax;
                        Excel.Range cellMin;

                        Excel.Range cellCheck;

                        int iColumnPosition = 2;

                        cellCoount = cells[1, iColumnPosition];
                        cellSum = cells[2, iColumnPosition];
                        cellAverage = cells[3, iColumnPosition];
                        cellMax = cells[4, iColumnPosition];
                        cellMin = cells[5, iColumnPosition];

                        cellCoount.Value = "個数";
                        cellSum.Value = "積算";
                        cellAverage.Value = "平均";
                        cellMax.Value = "最大";
                        cellMin.Value = "最小";

                        cellCoount.HorizontalAlignment = SpreadsheetGear.HAlign.Center;
                        cellSum.HorizontalAlignment = SpreadsheetGear.HAlign.Center;
                        cellAverage.HorizontalAlignment = SpreadsheetGear.HAlign.Center;
                        cellMax.HorizontalAlignment = SpreadsheetGear.HAlign.Center;
                        cellMin.HorizontalAlignment = SpreadsheetGear.HAlign.Center;



                        string startAddress;
                        string endAddress;


                        while (true)
                        {
                            iColumnPosition++;

                            // 次の列のセルが入力されているか確認
                            cellCheck = cells[ClsReadData.RESULT_COPY_HEADER_COUNT, iColumnPosition];
                            if (ClsExcelUtil.isEmpty(cellCheck))
                            {
                                break;
                            }

                            // 次の列の演算式埋め込みセルを指定する。
                            cellCoount = cells[1, iColumnPosition];
                            cellSum = cells[2, iColumnPosition];
                            cellAverage = cells[3, iColumnPosition];
                            cellMax = cells[4, iColumnPosition];
                            cellMin = cells[5, iColumnPosition];

                            // 演算式の開始、終了位置を求める。
                            startAddress = cells[ClsReadData.RESULT_COPY_HEADER_COUNT+1, iColumnPosition].Address;
                            endAddress = System.Text.RegularExpressions.Regex.Replace(startAddress, @"\d", "") + "100000";

                            // 計算式を埋め込む
                            cellCoount.Value = "=COUNTA(" + startAddress + ":" + endAddress + ")";
                            cellSum.Value = "=SUM(" + startAddress + ":" + endAddress + ")";
                            cellAverage.Value = "=IFERROR(AVERAGE(" + startAddress + ":" + endAddress + "),\"\")";

                            cellMax.Value = "=MAX(" + startAddress + ":" + endAddress + ")";
                            cellMin.Value = "=MIN(" + startAddress + ":" + endAddress + ")";

                            cellCoount.HorizontalAlignment = SpreadsheetGear.HAlign.Right;
                            cellSum.HorizontalAlignment = SpreadsheetGear.HAlign.Right;
                            cellAverage.HorizontalAlignment = SpreadsheetGear.HAlign.Right;
                            cellMax.HorizontalAlignment = SpreadsheetGear.HAlign.Right;
                            cellMin.HorizontalAlignment = SpreadsheetGear.HAlign.Right;


                        }
                    }
                }
                //ISSUE_NO.846 sunyi 2018/09/05 Start
                //ActiveSheetを戻す
                // Wang Issue DAC-91 20190208 Start
                //activeSheet.Select();
                try
                {
                    activeSheet.Select();
                }
#pragma warning disable
                catch (Exception e)
#pragma warning restore
                {

                }
                // Wang Issue DAC-91 20190208 End
                //ISSUE_NO.846 sunyi 2018/09/05 End
                bRtn = true;

            }
            catch
            {
                bRtn = false;
            }
            finally
            {
                if (workbook != null)
                {

                    workbook.Save();
                    workbook.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);

                }


                if (app != null)
                {
                    try
                    {
                        app.Quit();

                        System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                    }
                    catch
                    {

                    }
                }



            }

            return bRtn;
        }

        /*
         * ExcelファイルからCellを取得して返す。
         * 例外が発生した場合はnullを返す。
         *  fileName:Excelファイル名
         *  preSheetName:シート名（この後ろに連番を付与する）
         *  address:Cellのアドレス
         */
        public static string getCellValueByAddress(string fileName, string sheetName, string address)
        {
            string dValue = "";
            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                // ワークブックオープン
                workbook = SpreadsheetGear.Factory.GetWorkbook(fileName);
                SpreadsheetGear.IWorksheet resultSheet = workbook.Worksheets[sheetName];

                dValue = resultSheet.Cells[address].Value.ToString();

                return dValue;
            }
            catch
            {
                return dValue;
            }
            finally
            {
                if (workbook != null)
                {
                    //workbook.Save();
                    workbook.Close();
                }
            }


        }


        public static bool isAddin(string createFile)
        {

            bool bRtn = false;

            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                workbook = SpreadsheetGear.Factory.GetWorkbook(createFile);

                SpreadsheetGear.ISheet addInParamSheet = SpreadsheetGear.Factory.GetWorkbook(createFile).Worksheets["addinparam"];
                if (addInParamSheet != null)
                {

                    bRtn = true;
                }
                else
                {
                    bRtn = false;
                }
            }
            catch
            {
                bRtn = true;
            }
            finally
            {
                if (workbook != null)
                {
                    //workbook.Save();
                    workbook.Close();
                }
            }

            return bRtn;

        }

        // CSVファイルからExcelファイルに変換する
        public static string convertFromCSV(string csvFileName,string excelFileName)
        {

            SpreadsheetGear.IWorkbook workbook = SpreadsheetGear.Factory.GetWorkbook(csvFileName);

            string sheetName = workbook.Sheets[0].Name;

            workbook.SaveAs(excelFileName, SpreadsheetGear.FileFormat.OpenXMLWorkbook);
            workbook.Close();
            return sheetName;

        }

        // 
        public static void copySheet(Tool thisworkBook, string fromFileName, string fromSheetName)
        {

            Excel.Application fromApp = null;
            Excel.Workbook fromWorkbook = null;

            try
            {
                fromApp = ClsExcelUtil.StartExcelProcess();
                fromWorkbook = fromApp.Workbooks.Open(fromFileName);

                fromApp.ActiveWorkbook.ActiveSheet.Move(thisworkBook.Worksheets[thisworkBook.Worksheets.Count]);
                /*
                fromWorkbook = fromApp.Workbooks.Open(fromFileName);
                int fromSheetNo = getSheetNoByName(fromWorkbook.Sheets, fromSheetName);
                Excel.Worksheet fromSheet = fromWorkbook.Sheets[fromSheetNo];
                */


            }
            catch
            {

            }
            finally
            {
                if (fromApp != null)
                {
                    try
                    {
                        fromApp.Quit();

                        System.Runtime.InteropServices.Marshal.ReleaseComObject(fromApp);
                    }
                    catch
                    {

                    }
                }

                if (fromWorkbook != null)
                {
                    fromWorkbook.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(fromWorkbook);

                }
            }

            

        }

        //dac home No.40 start
        /// <summary>
        /// シート名からシートを取得、該当するシートが存在しない場合は新規シートをブックに追加して取得
        /// </summary>
        /// <param name="book">シートが存在するブック</param>
        /// <param name="sheetName">シート名</param>
        /// <returns>該当するシート</returns>
        public static Microsoft.Office.Interop.Excel.Worksheet GetSheetFromSheetName_Interop(Microsoft.Office.Interop.Excel.Workbook book, string sheetName)
        {
            Microsoft.Office.Interop.Excel.Worksheet sheet = null;

            foreach (Microsoft.Office.Interop.Excel.Worksheet s in book.Sheets)
            {
                if (s.Name != sheetName)
                {
                    continue;
                }

                sheet = s;
                break;
            }

            // 見つからなかった場合はシートを追加
            if (sheet == null)
            {
                book.Sheets.Add(Type.Missing, book.Sheets[book.Sheets.Count]);
                sheet = book.Sheets[book.Sheets.Count];
                sheet.Name = sheetName;
            }

            return sheet;
        }

        /// <summary>
        /// Range取込
        /// </summary>
        /// <param name="srchRange"></param>
        /// <param name="strWhat"></param>
        /// <returns></returns>
        public static Microsoft.Office.Interop.Excel.Range GetFindRange_Interop(Microsoft.Office.Interop.Excel.Range srchRange, String strWhat)
        {
            return srchRange.Find(strWhat,
                                Type.Missing,
                                Microsoft.Office.Interop.Excel.XlFindLookIn.xlValues,
                                Microsoft.Office.Interop.Excel.XlLookAt.xlWhole,
                                Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows,
                                Microsoft.Office.Interop.Excel.XlSearchDirection.xlNext,
                                false,
                                Type.Missing, Type.Missing);
        }
        //dac home end

    }
}
