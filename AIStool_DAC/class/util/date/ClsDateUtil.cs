﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AIStool_DAC.util.subDAC;
using System.IO;
using log4net;
using AIStool_DAC.util.log;

namespace AIStool_DAC.util.date
{
    class ClsDateUtil
    {
        /*
         * 引数の時間（1971/01/01 09:00:00からの経過時間）が
         * 現在時刻より後か、先か
         *  true:   現在時刻が、引数時刻より同時刻以降
         *  false:  現在時刻が、引数時刻より以前
         */
        public static bool isNowAfter(string elapsedTime)
        {

            DateTime now = DateTime.Now;
            DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);

            now = DateTime.Parse(now.ToString("yyyy/MM/dd HH:mm") + ":59.999");
            TimeSpan timeSpan = (now - epoch);

            string strNow = Math.Floor(timeSpan.TotalMilliseconds).ToString();

            int result = strNow.CompareTo(elapsedTime);

            if (result >= 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        /*
        public static void setOperationDate(ClsSubDACFile clsSubDacFile,string operationDate)
        {
            string operationDateFile =  Path.Combine(clsSubDacFile.BasePath,"OperationDate.txt");
            //logger.Debug("OperationDateFile: " + operationDateFile);            
            Globals.ThisWorkbook.workbookLog.OutPut("setOperationDate: ", operationDateFile, ClsMissionLog.LOG_LEVEL.DEBUG);
            using (StreamWriter sw = new StreamWriter(operationDateFile))
            {
                sw.Write(operationDate);
            }
        }
        */
        public static string getOperationDate(ClsSubDACControler clsSubDACControler)
        {
            string strOperationDate;

            string operationDateFile = Path.Combine(clsSubDACControler.BasePath, "OperationDate.txt");

            if (!File.Exists(operationDateFile))
            {
                return null;
            }
            else
            {
                using (StreamReader sr = new StreamReader(operationDateFile))
                {
                    strOperationDate = sr.ReadToEnd();
                }

            }

            return strOperationDate;
        }
        private static readonly ILog logger = LogManager.GetLogger(typeof(ClsDateUtil).Name);
    }
}
