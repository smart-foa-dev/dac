﻿namespace AIStool_DAC
{
    [System.ComponentModel.ToolboxItemAttribute(false)]
    partial class SubDACActionsPaneControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblFormula = new System.Windows.Forms.Label();
            this.fileListPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.fileText = new System.Windows.Forms.TextBox();
            this.lblDate = new System.Windows.Forms.Label();
            this.fileContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.OpenMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DelelteControlMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filePicture = new System.Windows.Forms.PictureBox();
            this.noFileContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.DeleteMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.DelelteControlMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.MoveMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.fileContextMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.filePicture)).BeginInit();
            this.noFileContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblFormula
            // 
            this.lblFormula.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblFormula.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.lblFormula.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblFormula.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblFormula.Location = new System.Drawing.Point(8, 9);
            this.lblFormula.Name = "lblFormula";
            this.lblFormula.Size = new System.Drawing.Size(980, 45);
            this.lblFormula.TabIndex = 6;
            this.lblFormula.Text = "Sub DAC";
            this.lblFormula.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fileListPanel
            // 
            this.fileListPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fileListPanel.AutoSize = true;
            this.fileListPanel.BackColor = System.Drawing.Color.White;
            this.fileListPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.fileListPanel.Location = new System.Drawing.Point(8, 53);
            this.fileListPanel.Name = "fileListPanel";
            this.fileListPanel.Size = new System.Drawing.Size(980, 276);
            this.fileListPanel.TabIndex = 9;
            this.fileListPanel.DragDrop += new System.Windows.Forms.DragEventHandler(this.panel1_DragDrop);
            this.fileListPanel.DragEnter += new System.Windows.Forms.DragEventHandler(this.panel1_DragEnter);
            // 
            // fileText
            // 
            this.fileText.Location = new System.Drawing.Point(0, 0);
            this.fileText.Name = "fileText";
            this.fileText.Size = new System.Drawing.Size(100, 19);
            this.fileText.TabIndex = 0;
            // 
            // lblDate
            // 
            this.lblDate.BackColor = System.Drawing.Color.White;
            this.lblDate.Font = new System.Drawing.Font("MS UI Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lblDate.Location = new System.Drawing.Point(163, 18);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(163, 23);
            this.lblDate.TabIndex = 10;
            this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDate.TextChanged += new System.EventHandler(this.lblDate_TextChanged);
            // 
            // fileContextMenu
            // 
            this.fileContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenMenuItem,
            this.DeleteMenuItem,
            this.DelelteControlMenuItem,
            this.MoveMenuItem});
            this.fileContextMenu.Name = "contextMenu";
            this.fileContextMenu.Size = new System.Drawing.Size(166, 92);
            // 
            // OpenMenuItem
            // 
            this.OpenMenuItem.Name = "OpenMenuItem";
            this.OpenMenuItem.Size = new System.Drawing.Size(165, 22);
            this.OpenMenuItem.Text = "開く";
            this.OpenMenuItem.Click += new System.EventHandler(this.openMenuItem_Click);
            // 
            // DeleteMenuItem
            // 
            this.DeleteMenuItem.Name = "DeleteMenuItem";
            this.DeleteMenuItem.Size = new System.Drawing.Size(165, 22);
            this.DeleteMenuItem.Text = "削除";
            this.DeleteMenuItem.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // DelelteControlMenuItem
            // 
            this.DelelteControlMenuItem.Name = "DelelteControlMenuItem";
            this.DelelteControlMenuItem.Size = new System.Drawing.Size(165, 22);
            this.DelelteControlMenuItem.Text = "管理対象から削除";
            this.DelelteControlMenuItem.Click += new System.EventHandler(this.DelelteControlMenuItem_Click);
            // 
            // MoveMenuItem
            // 
            this.MoveMenuItem.Name = "MoveMenuItem";
            this.MoveMenuItem.Size = new System.Drawing.Size(165, 22);
            this.MoveMenuItem.Text = "１つ前へ移動";
            this.MoveMenuItem.Click += new System.EventHandler(this.MoveMenuItem_Click);
            // 
            // filePicture
            // 
            this.filePicture.Location = new System.Drawing.Point(0, 0);
            this.filePicture.Name = "filePicture";
            this.filePicture.Size = new System.Drawing.Size(100, 50);
            this.filePicture.TabIndex = 0;
            this.filePicture.TabStop = false;
            // 
            // noFileContextMenu
            // 
            this.noFileContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DeleteMenuItem2,
            this.DelelteControlMenuItem2,
            this.MoveMenuItem2});
            this.noFileContextMenu.Name = "contextMenu";
            this.noFileContextMenu.Size = new System.Drawing.Size(166, 70);
            // 
            // DeleteMenuItem2
            // 
            this.DeleteMenuItem2.Name = "DeleteMenuItem2";
            this.DeleteMenuItem2.Size = new System.Drawing.Size(165, 22);
            this.DeleteMenuItem2.Text = "削除";
            this.DeleteMenuItem2.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // DelelteControlMenuItem2
            // 
            this.DelelteControlMenuItem2.Name = "DelelteControlMenuItem2";
            this.DelelteControlMenuItem2.Size = new System.Drawing.Size(165, 22);
            this.DelelteControlMenuItem2.Text = "管理対象から削除";
            this.DelelteControlMenuItem2.Click += new System.EventHandler(this.DelelteControlMenuItem_Click);
            // 
            // MoveMenuItem2
            // 
            this.MoveMenuItem2.Name = "MoveMenuItem2";
            this.MoveMenuItem2.Size = new System.Drawing.Size(165, 22);
            this.MoveMenuItem2.Text = "１つ前へ移動";
            this.MoveMenuItem2.Click += new System.EventHandler(this.MoveMenuItem_Click);
            // 
            // SubDACActionsPaneControl
            // 
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.lblFormula);
            this.Controls.Add(this.fileListPanel);
            this.Name = "SubDACActionsPaneControl";
            this.Size = new System.Drawing.Size(1000, 364);
            this.fileContextMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.filePicture)).EndInit();
            this.noFileContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblFormula;
        private System.Windows.Forms.FlowLayoutPanel fileListPanel;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.TextBox fileText;
        private System.Windows.Forms.PictureBox filePicture;
        private System.Windows.Forms.ContextMenuStrip fileContextMenu;
        private System.Windows.Forms.ToolStripMenuItem OpenMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeleteMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DelelteControlMenuItem;
        private System.Windows.Forms.ToolStripMenuItem MoveMenuItem;
        private System.Windows.Forms.ContextMenuStrip noFileContextMenu;
        private System.Windows.Forms.ToolStripMenuItem DeleteMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem DelelteControlMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem MoveMenuItem2;
    }
}
