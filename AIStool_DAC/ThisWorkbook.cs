﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

using System.IO;
using System.Configuration;

using AIStool_DAC.util.excel;
using AIStool_DAC.util.http;
using AIStool_DAC.util.file;
using AIStool_DAC.util.subDAC;

using AIStool_DAC.read;
using AIStool_DAC.conf;
using AIStool_DAC.util.log;
using FoaCoreCom.dac;
using Newtonsoft.Json;
using log4net.Repository.Hierarchy;
using DAC.Common;

namespace AIStool_DAC
{
    public partial class ThisWorkbook
    {
        /// <summary>
        /// 作業ウインドウに追加するアクション
        /// </summary>
        public CalActionsPaneControl m_calActionPanel;
        public SubDACActionsPaneControl m_subDACActionPanel;

        public ClsSubDACControler clsSubDACControler;
        public ClsDACConf clsDACConf;

        // SubDAC管理シートの最新情報を保存するクラス
        //public LinkedList<ClsSubDACFile> clsSubDACFileList;
        public ClsMissionLog workbookLog;

        private void ThisWorkbook_Startup(object sender, System.EventArgs e)
        {
            string fullFileName = this.FullName;
            // Excelに設定されている
            
            System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("ja-JP");
            //System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");
            //CultureResources.ChangeCulture("en-US");
            this.clsDACConf = new ClsDACConf(fullFileName);
            InitializeDirectories(clsDACConf);

            DateTime now = DateTime.Now;            
            
            string logFile = string.Format("thisbook_log_{0}.txt", now.ToString().Replace("/", "").Replace(":", "").Replace(" ", ""));
            workbookLog = new ClsMissionLog("ThisBook",clsDACConf.LogDir, logFile, ClsMissionLog.LOG_MONITOR_TYPE.DETAIL, ClsMissionLog.LOG_LEVEL.DEBUG);
            workbookLog.OutPut("Excel起動(全体)", "Excel起動開始", ClsMissionLog.LOG_LEVEL.INFO);            
            /*
             * AISのRootフォルダーを判断する
             */
            // GripClientから起動＋Gripのパスが同じ
            
            // アクションパネルコントールの初期化
            m_calActionPanel = new CalActionsPaneControl();
            m_subDACActionPanel = new SubDACActionsPaneControl();

            // アクションパネル名を設定
            m_calActionPanel.Name = ClsReadData.CAL_ACTION_PANEL;
            m_subDACActionPanel.Name = ClsReadData.SUB_DAC_ACTION_PANEL;

            // DAC管理クラスの初期化(SubDACの読み込み）
            clsSubDACControler = new ClsSubDACControler(fullFileName,clsDACConf);
            //clsSubDACFileList = clsSubDACControler.ClsSubDACFileList;


            workbookLog.OutPut("Excel起動(全体)",
                    "clsSubDACControler.BasePath=" + clsSubDACControler.BasePath + System.Environment.NewLine +
                    "clsSubDACControler.FileList.Count()=" + clsSubDACControler.manifest.fileList.Count() + System.Environment.NewLine +
                    "clsSubDACControler.DACFullName=" + clsSubDACControler.DACFullName + System.Environment.NewLine +
                    "clsSubDACControler.DACName=" + clsSubDACControler.DACName + System.Environment.NewLine +
                    "clsSubDACControler.Hierarchy=" + clsSubDACControler.Hierarchy + System.Environment.NewLine +
                    "clsSubDACControler.ParentDACName=" + clsSubDACControler.ParentDACName + System.Environment.NewLine +
                    "clsSubDACControler.RootDACName=" + clsSubDACControler.RootDACName + System.Environment.NewLine
                    //"clsSubDACControler.SubDACControlText=" + clsSubDACControler.SubDACControlText + System.Environment.NewLine
                    , ClsMissionLog.LOG_LEVEL.DEBUG);



            // workbookLog.OutPut("Excel起動(全体)",
            //    "showSubDACActionPanelの表示" + clsDACConf.AisExeDir + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

            // SubDAC ActionPanelの表示
            if (clsDACConf.EnableMultiDoc)
            {
                showSubDACActionPanel();
            }
            else
            {
                hidePanel();
            }



            // 1 ダウンロードしたR2ファイルを起動している場合（R2からファイルを開いている場合はDACは最新と考える）	
	        // パスが同じ場合
            /*
            if (fullFileName.StartsWith(clsDACConf.GripR2Folder) || (fullFileName.IndexOf(@"\tmp\dashbox\", StringComparison.OrdinalIgnoreCase) > 0))
            {
                //this.clsDACConf.R2UploadFlag = true;

                string fileName = System.IO.Path.GetFileNameWithoutExtension(fullFileName);

                // 1 R2サーバーからSubDACデータを取得する
                string zipFilePath = ClsHttpUtil.getSubDAC(fileName);

                if (string.IsNullOrEmpty(zipFilePath))
                {
                    //workbookLog.OutPut("Excel起動(R2配下)",
                    //    "zipFilePath=" + zipFilePath + "の取得失敗" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                    //MessageBox.Show("このファイル（" + fileName + "）はR2上で管理されていますが、\n" +
                    //                "R2上のSubDACファイルの取得に失敗しました。\n" +
                    //                "そのため、ローカルのSubDACファイルの置換は実施されていません。",
                    //                "エラー",
                    //                MessageBoxButtons.YesNo,
                    //                MessageBoxIcon.Error);


                }
                else
                {
                    this.clsDACConf.R2UploadFlag = true;
                    workbookLog.OutPut("Excel起動(R2配下)",
                        "zipFilePath=" + zipFilePath + "の取得成功" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                    // 2 既存のSubDACを削除（またはリネームする）
                    string subDACBack = ClsFileUtil.moveDirectory(fileName);

                    workbookLog.OutPut("Excel起動(R2配下)",
                        "fileName=" + fileName + "を、subDACBack=" + subDACBack + "にリネーム" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                    // 3 ZIPをSubDAC保存場所に移動して、解凍する。
                    System.IO.Compression.ZipFile.ExtractToDirectory(
                        zipFilePath,
                        clsDACConf.SubDacDir,
                        System.Text.Encoding.GetEncoding("shift_jis"));

                    workbookLog.OutPut("Excel起動(R2配下)",
                        "zipFilePath=" + zipFilePath + "の解凍 "+ clsDACConf.SubDacDir + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                    // DAC管理クラスの初期化(SubDACの読み込み）
                    clsSubDACControler = new ClsSubDACControler(fullFileName, clsDACConf);
                    clsSubDACFileList = clsSubDACControler.ClsSubDACFileList;

                    workbookLog.OutPut("Excel起動(R2配下)",
                        "clsSubDACControlerの再読み込み" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                    workbookLog.OutPut("Excel起動(R2配下)",
                        "clsSubDACControler.BasePath=" + clsSubDACControler.BasePath + System.Environment.NewLine +
                        "clsSubDACControler.ClsSubDACFileList.Count()=" + clsSubDACControler.ClsSubDACFileList.Count() + System.Environment.NewLine +
                        "clsSubDACControler.DACFullName=" + clsSubDACControler.DACFullName + System.Environment.NewLine +
                        "clsSubDACControler.DACName=" + clsSubDACControler.DACName + System.Environment.NewLine +
                        "clsSubDACControler.Hierarchy=" + clsSubDACControler.Hierarchy + System.Environment.NewLine +
                        "clsSubDACControler.ParentDACName=" + clsSubDACControler.ParentDACName + System.Environment.NewLine +
                        "clsSubDACControler.RootDACName=" + clsSubDACControler.RootDACName + System.Environment.NewLine +
                        "clsSubDACControler.SubDACControlText=" + clsSubDACControler.SubDACControlText + System.Environment.NewLine
                        , ClsMissionLog.LOG_LEVEL.DEBUG);


                    // 4 リネームしたSubDAC用ディレクトリを削除する。
                    //ClsFileUtil.deleteDirectory(subDACBack);
                }
            }
            // 2 ダウンロードしたR2ファイルを起動していない場合
            else
            {
                workbookLog.OutPut("Excel起動(R2配下以外)",
                    "DACがR2配下以外で起動された" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                this.clsDACConf.R2UploadFlag = false;

                // 1 サーバーにDAC帳票がアップされているか確認する。
                string uuid = ClsHttpUtil.checkDACUpload(this.FullName);

                if (!String.IsNullOrEmpty(uuid))
                {
                //    workbookLog.OutPut("Excel起動(R2配下以外)",
                //        "このDACはR2上で管理されている。" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);


                //    // 2 UUIDが取得できた場合は、R2上に最新Excelがあり、そちらを開くとダイアログを表示する。
                //    string fileName = System.IO.Path.GetFileNameWithoutExtension(this.FullName);
                //    // No. 827 Modified 2018/08/24 by Yakiyama. Start //ERRORメッセージからINFOメッセージに変更。
                //    MessageBox.Show("このファイル（" + fileName + "）はR2上で管理されています。",
                //                    "情報",
                //                    MessageBoxButtons.OK,
                //                    MessageBoxIcon.Information);
                //    // No. 827 Modified 2018/08/24 by Yakiyama. End //ERRORメッセージからINFOメッセージに変更。
                }
                else
                {
                    workbookLog.OutPut("Excel起動(R2配下以外)",
                        "このDACはR2上で管理はされていない。" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                }
            }
            */
        }

        public void InitializeDirectories(ClsDACConf clsDACConf)
        {
            if(string.IsNullOrEmpty(clsDACConf.DacDir))
            {
                return;
            }
            if(!Directory.Exists(clsDACConf.DataDir))
            {
                Directory.CreateDirectory(clsDACConf.DataDir);
            }
        }
        public bool isSubDACPanelShown()
        {
            return this.ActionsPane.Controls.Count > 0 && this.ActionsPane.Controls[0] is SubDACActionsPaneControl && this.Application.DisplayDocumentActionTaskPane;
        }

        public bool isCalActionPanelShown()
        {
            return this.ActionsPane.Controls.Count > 0 && this.ActionsPane.Controls[0] is CalActionsPaneControl && this.Application.DisplayDocumentActionTaskPane;
        }

        public void hidePanel()
        {
            this.ActionsPane.Controls.Clear();
            this.Application.DisplayDocumentActionTaskPane = false;
            
        }

        public void showSubDACActionPanel()
        {
            this.ActionsPane.Controls.Clear();
            this.ActionsPane.Controls.Add(m_subDACActionPanel);
            this.Application.CommandBars["Task Pane"].Position = Microsoft.Office.Core.MsoBarPosition.msoBarBottom;
            this.Application.DisplayDocumentActionTaskPane = true;
        }

        public void showCalActionPanel()
        {
            this.ActionsPane.Controls.Clear();
            this.ActionsPane.Controls.Add(m_calActionPanel);
            this.Application.CommandBars["Task Pane"].Position = Microsoft.Office.Core.MsoBarPosition.msoBarRight;
            this.Application.DisplayDocumentActionTaskPane = true;
        }

        private void ThisWorkbook_Shutdown(object sender, System.EventArgs e)
        {
//            string str = "test";
        }

        #region VSTO デザイナーで生成されたコード

        /// <summary>
        /// デザイナーのサポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisWorkbook_Startup);
            this.Shutdown += new System.EventHandler(ThisWorkbook_Shutdown);

            this.SheetChange += new Excel.WorkbookEvents_SheetChangeEventHandler(workbook_SheetChange);

            this.BeforeClose += new Excel.WorkbookEvents_BeforeCloseEventHandler(ThisWorkbook_BeforeClose);
        }

        private void workbook_SheetChange(object Sh, Excel.Range Target)
        {
//            int i = 0;
        }

        void ThisWorkbook_BeforeClose(ref bool Cancel)
        {
            if (!this.Saved)
            {
                /*
                if (this.clsSubDACControler.Hierarchy == 1)
                {
                    if (this.clsDACConf.R2UploadFlag)
                    {
                        DialogResult result = MessageBox.Show("このファイルはR2上へアップされていません。\n" +
                                        "R2上へアップする場合は「はい」ボタンを選択し、Excelの保存ボタンをクリック後にファイルを閉じてください。" +
                                        "R2上へアップしない場合は「いいえ」ボタンを選択してください。" ,
                                        "エラー",
                                        MessageBoxButtons.YesNo,
                                        MessageBoxIcon.Warning);

                        switch (result)
                        {
                            case DialogResult.Yes:
                                Cancel = true;
                                break;

                            case DialogResult.No:
                                this.Saved = true;
                                break;
                        }
                    }
                }
                */
            }
            else
            {
//                int i = 2;
            }
        }
        #endregion

    }
}
