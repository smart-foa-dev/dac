﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Runtime.Serialization.Json;
using System.IO;
using Microsoft.Win32;
using System.Windows.Forms;
using System.Diagnostics;

namespace AIStool_DAC
{
    /// <summary>
    /// ミッション投入で取得したJsonデータの処理クラス
    /// メンバーはgetJsonDataメソッドでのみ変更可能
    /// </summary>
    [DataContract]
    public class ClsJsonData
    {
        /// <summary>
        /// ミッションID
        /// </summary>
        [DataMember]
        public string id { get; private set; }

        /// <summary>
        /// CTM名
        /// </summary>
        [DataMember]
        public string name { get; private set; }

        /// <summary>
        /// ミッションの取得数
        /// </summary>
        [DataMember]
        public int num { get; private set; }

        /// <summary>
        /// ミッションで取得したCTMのリスト
        /// </summary>
        [DataMember]
        public List<Ctms> ctms { get; private set; }


        /// <summary>
        /// プログラムミッションを投入しJsonを変換したCTMデータを受け取る
        /// </summary>
        /// <param name="mid">ミッションのID(msec)</param>
        /// <param name="start">収集開始日時(msec)</param>
        /// <param name="end">収集終了日時(msec)</param>
        /// <returns></returns>
        public static ClsJsonData[] get(bool useProxy,string proxyUri, string mid, string start, string end, ClsWriteLog writeLog, int timeOutMiliSeconds)
        {
            if (timeOutMiliSeconds < 10000)
            {
                timeOutMiliSeconds = 10000;
            }

            WebClientWithTimeOut client = new WebClientWithTimeOut();
            string path = string.Empty; //20150827
            try
            {
                //ミッション投入のパラメータを設定する
                string uri = "";
                string hostName = Globals.Sheet1.m_MissionHostName;
                string portNo = Globals.Sheet1.m_MissionPortNo;
                string limit = "0";
                string lang = "ja";
                //string jsonStr = "";    //jsonを受け取るオブジェクト


                uri = String.Format("http://{0}:{1}/cms/rest/mib/mission/pm?id={2}&start={3}&end={4}&limit={5}&lang={6}&noBulky=true",
                           hostName, portNo, mid, start, end, limit, lang);

                //WebClient wc = new WebClient();
                //wc.Encoding = Encoding.UTF8;
                //jsonStr = wc.DownloadString(uri);
                //writeLog.OutPut("取得Json", mid + " : " + jsonStr);
                //ClsJsonData[] ctmArray = JsonConvert.DeserializeObject<ClsJsonData[]>(jsonStr);

                // 20150827 ***************************************************************************************
                ClsJsonData[] ctmArray = null;
                string clrVersionRuntime = System.Runtime.InteropServices.RuntimeEnvironment.GetSystemVersion();
                double nowVer = 0;
                using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\"))
                {
                    int releaseKey = Convert.ToInt32(ndpKey.GetValue("Release"));
                    nowVer = CheckFor45DotVersion(releaseKey);
                }

                if (nowVer < 4.5)
                {
                    // ファイルに落とす
                    DateTime dt = DateTime.Now;
                    if (Directory.Exists(Path.GetFullPath(Globals.ThisWorkbook.Path) + @"\tempfiles") == false)
                    {
                        Directory.CreateDirectory(Path.GetFullPath(Globals.ThisWorkbook.Path) + @"\tempfiles");
                    }
                    path = Path.GetFullPath(Globals.ThisWorkbook.Path) + @"\tempfiles\" + dt.ToString("yyyyMMddhhmmssfff") + ".json";
                    client.Timeout = timeOutMiliSeconds;

                    if (useProxy == true)
                    {
                        client.Proxy = new System.Net.WebProxy("http://" + proxyUri);
                    }
                    else
                    {
                        client.Proxy = null;
                    }

                    client.DownloadFile(uri, path);

                    StreamReader sr;
                    using (sr = new StreamReader(path, Encoding.UTF8))
                    {
                        JsonSerializer Serialize = new JsonSerializer();
                        ctmArray = (ClsJsonData[])Serialize.Deserialize(sr, typeof(ClsJsonData[]));
                    }
                }
                else if (nowVer >= 4.5)
                {
                    // メモリに取得
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                    req.Timeout = timeOutMiliSeconds;   // mili seconds

                    if (useProxy == true)
                    {
                        System.Net.WebProxy proxy =
                            new System.Net.WebProxy("http://" + proxyUri);
                        req.Proxy = proxy;
                    }
                    else
                    {
                        req.Proxy = null;
                    }

                    using (HttpWebResponse webres = (HttpWebResponse)req.GetResponse())
                    {
                        using (Stream st = webres.GetResponseStream())
                        {
                            System.Runtime.Serialization.Json.DataContractJsonSerializerSettings dataSetting = new DataContractJsonSerializerSettings();
                            dataSetting.UseSimpleDictionaryFormat = true;

                            var serializer = new DataContractJsonSerializer(typeof(List<ClsJsonData>), dataSetting);
                            List<ClsJsonData> getJson = (List<ClsJsonData>)serializer.ReadObject(st);
                            ctmArray = getJson.ToArray();
                        }
                    }
                }
                // 20150827 ***************************************************************************************

                return ctmArray;
            }
            catch (WebException)
            {
                throw new WebException("ミッションの結果を取得できませんでした。CTMやエレメントが削除されていないか確認してください。");
            }
            finally
            {
                // 20150827
                client.Dispose();
                if (File.Exists(path) == true)
                {
                    File.Delete(path);
                }
            }

        }

        /// <summary>
        /// プログラムミッションを投入しJsonを変換したCTMデータを受け取る
        /// </summary>
        /// <param name="mid">ミッションのID(msec)</param>
        /// <param name="start">収集開始日時(msec)</param>
        /// <param name="end">収集終了日時(msec)</param>
        /// <returns></returns>
        public static string getString(bool useProxy,string proxyUri,string mid, string start, string end, ClsWriteLog writeLog, int timeOutMiliSeconds)
        {
            if (timeOutMiliSeconds < 10000)
            {
                timeOutMiliSeconds = 10000;
            }

            WebClientWithTimeOut client = new WebClientWithTimeOut();
            string path = string.Empty; //20150827
            try
            {
                //ミッション投入のパラメータを設定する
                string uri = "";
                string hostName = Globals.Sheet1.m_MissionHostName;
                string portNo = Globals.Sheet1.m_MissionPortNo;
                string limit = "0";
                string lang = "ja";
                //string jsonStr = "";    //jsonを受け取るオブジェクト


                uri = String.Format("http://{0}:{1}/cms/rest/mib/mission/pm?id={2}&start={3}&end={4}&limit={5}&lang={6}&noBulky=true",
                           hostName, portNo, mid, start, end, limit, lang);

                //WebClient wc = new WebClient();
                //wc.Encoding = Encoding.UTF8;
                //jsonStr = wc.DownloadString(uri);
                //writeLog.OutPut("取得Json", mid + " : " + jsonStr);
                //ClsJsonData[] ctmArray = JsonConvert.DeserializeObject<ClsJsonData[]>(jsonStr);

                // 20150827 ***************************************************************************************

                string jsonData = "";


                string clrVersionRuntime = System.Runtime.InteropServices.RuntimeEnvironment.GetSystemVersion();
                double nowVer = 0;
                using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\"))
                {
                    int releaseKey = Convert.ToInt32(ndpKey.GetValue("Release"));
                    nowVer = CheckFor45DotVersion(releaseKey);
                }

                if (nowVer < 4.5)
                {
                    // ファイルに落とす
                    DateTime dt = DateTime.Now;
                    if (Directory.Exists(Path.GetFullPath(Globals.ThisWorkbook.Path) + @"\tempfiles") == false)
                    {
                        Directory.CreateDirectory(Path.GetFullPath(Globals.ThisWorkbook.Path) + @"\tempfiles");
                    }
                    path = Path.GetFullPath(Globals.ThisWorkbook.Path) + @"\tempfiles\" + dt.ToString("yyyyMMddhhmmssfff") + ".json";
                    client.Timeout = timeOutMiliSeconds;


                    if (useProxy == true)
                    {
                        client.Proxy = new System.Net.WebProxy("http://" + proxyUri);
                    }
                    else
                    {
                        client.Proxy = null;
                    }

                    client.DownloadFile(uri, path);

                    using (StreamReader sr = new StreamReader(path, Encoding.UTF8))
                    {
                        jsonData = sr.ReadToEnd();
                    }
                }
                else if (nowVer >= 4.5)
                {
                    // メモリに取得
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                    req.Timeout = timeOutMiliSeconds;   // mili seconds

                    if (useProxy == true)
                    {
                        System.Net.WebProxy proxy =
                            new System.Net.WebProxy("http://" + proxyUri);
                        req.Proxy = proxy;
                    }
                    else
                    {
                        req.Proxy = null;
                    }


                    using (HttpWebResponse webres = (HttpWebResponse)req.GetResponse())
                    {
                        using (Stream st = webres.GetResponseStream())
                        using (StreamReader sr = new StreamReader(st))
                        {
                            jsonData = sr.ReadToEnd();
                        }
                    }
                }
                // 20150827 ***************************************************************************************
                if (true)
                {
                    MessageBox.Show("ClsJsonData.cs:279 json=" + jsonData.ToString());
                }
                return jsonData;
            }
            catch (WebException)
            {
                throw new WebException("ミッションの結果を取得できませんでした。CTMやエレメントが削除されていないか確認してください。");
            }
            finally
            {
                // 20150827
                client.Dispose();
                if (File.Exists(path) == true)
                {
                    File.Delete(path);
                }
            }

        }


        // 20150827
        // Checking the version using >= will enable forward compatibility, 
        // however you should always compile your code on newer versions of
        // the framework to ensure your app works the same.
        private static double CheckFor45DotVersion(int releaseKey)
        {
            if (releaseKey >= 393273)
            {
                return 4.6; //"4.6 RC or later";
            }
            if ((releaseKey >= 379893))
            {
                return 4.5;  //"4.5.2 or later";
            }
            if ((releaseKey >= 378675))
            {
                return 4.5;  //"4.5.1 or later";
            }
            if ((releaseKey >= 378389))
            {
                return 4.5; //"4.5 or later";
            }
            // This line should never execute. A non-null release key should mean
            // that 4.5 or later is installed.
            return 4.0;  //"No 4.5 or later version detected";
        }

        private class WebClientWithTimeOut : WebClient
        {
            private int timeout = 10 * 1000;
            public int Timeout
            {
                get
                {
                    return this.timeout;
                }
                set
                {
                    this.timeout = value;
                }
            }

            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest request = base.GetWebRequest(uri);
                request.Timeout = this.timeout;
                return request;
            }
        }

        // Type created for JSON at <<root>> --> ctms
        /// <summary>
        /// ミッションで取得したCTM
        /// </summary>
        [DataContract]
        public class Ctms
        {
            /// <summary>
            /// エレメントのリスト
            /// </summary>
            [DataMember]
            //public List<ELcls> EL { get; private set; }
            public Dictionary<string, ELcls> EL { get; private set; }
            /// <summary>
            /// CTMが生成された時刻
            /// </summary>
            [DataMember]
            public long RT { get; private set; }

            // Type created for JSON at <<root>> --> EL
            /// <summary>
            /// エレメントの内容
            /// </summary>
            [DataContract]
            public class ELcls
            {
                ///// <summary>
                ///// エレメントID
                ///// </summary>
                //[DataMember]
                //public string ID { get; private set; }

                /// <summary>
                /// エレメントの型
                /// </summary>
                [DataMember]
                public string T { get; private set; }

                /// <summary>
                /// エレメントの値
                /// </summary>
                [DataMember]
                public string V { get; private set; }

                /// <summary>
                /// エレメントのバルキーリンク
                /// </summary>
                [DataMember]
                public string FN { get; private set; }
            }
        }
    }
}
