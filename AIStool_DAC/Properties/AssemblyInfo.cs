﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

// アセンブリに関する一般情報は、以下の属性セットによって
// 制御されます。アセンブリに関連付けられている情報を変更するには、
// これらの属性値を変更します。
[assembly: AssemblyTitle("AIStool_DAC")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("AIStool_DAC")]
[assembly: AssemblyCopyright("Copyright ©  2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// ComVisible を false に設定すると、その型はこのアセンブリ内で COM コンポーネントから 
// 参照不可能になります。COM からこのアセンブリ内の型にアクセスする場合は、
// その型の ComVisible 属性を true に設定してください。
[assembly: ComVisible(false)]

// このプロジェクトが COM に公開される場合、次の GUID がタイプ ライブラリの ID になります。
[assembly: Guid("a6a94f76-ce9c-48e1-9458-9306683b02c5")]

// アセンブリのバージョン情報は、以下の 4 つの値で構成されています。
//
//      メジャー バージョン
//      マイナー バージョン 
//      ビルド番号
//      リビジョン
//
// すべての値を指定するか、以下のように '*' を使ってビルドおよびリビジョン番号を
// 既定値にすることができます。
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.*")]
[assembly: AssemblyFileVersion("1.1.*")]
[assembly: AssemblyInformationalVersion("XYZ")]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = @"aislog4net.config", Watch = true)]
