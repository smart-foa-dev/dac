﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

using System.Drawing;


namespace AIStool_DAC
{
    /*
    public class ClsSubDACFile : System.IComparable
    {
        
        public ClsSubDACFile(string parentDacName,string csvData)
        {
            string[] arrayData = csvData.Split(',');

            if (arrayData.Length == 5)
            {
                this.DACName = arrayData[0];
                this.Kind = arrayData[1];
                this.Name = arrayData[2];
                this.Sort = Double.Parse(arrayData[3]);
                this.deleteDate = Int32.Parse(arrayData[4]);
            }
            else if (arrayData.Length == 4)
            {
                this.DACName = arrayData[0];
                this.Kind = arrayData[1];
                this.Name = arrayData[2];
                this.Sort = Double.Parse(arrayData[3]);
                this.deleteDate = 0;
            }
            this.Ext = arrayData[2];
            this.ParentDACName = parentDacName;

        }

        public ClsSubDACFile(string parentDacName, string dacName, string kind, string name, double sort = 0, int deleteDate = 0)
        {
            this.ParentDACName = parentDacName;
            this.DACName = dacName;
            this.Kind = kind;
            this.Name = name;
            this.Ext = name;
            this.Sort = sort;
            this.deleteDate = deleteDate;

        }


        ///////////////////////////////////////////////////

        private string ParentDACName{get;set;}

        // DACファイル名
        private string _dacName;
        public string DACName
        {
            get
            {
                return this._dacName;
            }
            set
            {
                // 拡張子を取り除く
                this._dacName = System.IO.Path.GetFileNameWithoutExtension(value);
            }
        }

        // ファイル種別
        public string Kind{get;set;}

        // SubDACファイル名
        private string _name;
        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                _name = System.IO.Path.GetFileNameWithoutExtension(value);
            }
        }
        private string _ext;
        public string Ext
        {
            get
            {
                return _ext;
            }
            set
            {
                _ext = Path.GetExtension(value);
            }
        }
        // 並び順
        public double Sort{get;set;}

        // 削除日付
        public int deleteDate { get; set; }

        // 
        public string RootPath
        {
            get{
                return Path.Combine(Globals.ThisWorkbook.clsDACConf.SubDacDir,ParentDACName,DACName + "\\");
            }
        }


        public string RootDACName
        {
            get
            {
                if (ParentDACName.Equals(""))
                {
                    return DACName;
                }
                else
                {
                    string[] tmp = ParentDACName.Split('\\');
                    return tmp[0];
                }
            }
        }

        public string BasePath
        {
            get
            {
                return Path.Combine(Globals.ThisWorkbook.clsDACConf.SubDacDir, RootDACName + "\\");

            }
        }

        public string getFullPath(string date)
        {
            date = date.Replace("/", "");

            string fileName = "";
            if (Kind.Equals(Properties.Resources.TEXT_STD_MULTIDAC) || Kind.Equals(Properties.Resources.TEXT_STD_SPREADSHEET))
            {
                fileName = RootPath + date + "\\" + Name + "_" + date + ".xlsm";
            }
            else
            {
                fileName = RootPath + "\\" + Name + Ext;
            }
            Globals.ThisWorkbook.workbookLog.OutPut("getFullPath: ", fileName, util.log.ClsMissionLog.LOG_LEVEL.DEBUG);
            return fileName;
        }

        public bool isExce(string date)
        {
            return System.IO.File.Exists(getFullPath(date));

        }

        public override bool Equals(object obj)
        {
            //objがnullか、型が違うときは、等価でない
            if (obj == null || this.GetType() != obj.GetType())
            {
                return false;
            }

            //Numberで比較する
            ClsSubDACFile param = (ClsSubDACFile)obj;

            return (this.Name == param.Name);
        }

        public override string ToString()
        {
            return Kind + ":" + Name;
        }

        //Equalsがtrueを返すときに同じ値を返す
        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        //自分自身がobjより小さいときはマイナスの数、大きいときはプラスの数、
        //同じときは0を返す
        public int CompareTo(object obj)
        {
            //nullより大きい
            if (obj == null)
            {
                return 1;
            }

            //違う型とは比較できない
            if (this.GetType() != obj.GetType())
            {
                throw new ArgumentException("別の型とは比較できません。", "obj");
            }

            ClsSubDACFile subDacFile = (ClsSubDACFile)obj;

            if (this.Sort == subDacFile.Sort)
            {
                return 0;
            }
            else if (this.Sort < subDacFile.Sort)
            {
                return -1;
            }
            else
            {
                return 1;
            }
        }




    }
    */
}
