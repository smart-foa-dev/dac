﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Office.Tools.Excel;
using Microsoft.VisualBasic;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using XlTool = Microsoft.Office.Tools.Excel;
using Office = Microsoft.Office.Core;

using System.IO;
using System.Windows.Forms;

using AIStool_DAC.util.excel;
using AIStool_DAC.util.date;
using AIStool_DAC.util.file;
using AIStool_DAC.util.subDAC;
using AIStool_DAC.util.cal;
using AIStool_DAC.util.log;

using AIStool_DAC.read;
using AIStool_DAC.conf;
using FoaCoreCom.common.main;
//FOR COM高速化 lm  2018/06/21 Add Start
using FoaCoreCom.ais.retriever;
using Newtonsoft.Json.Linq;
using FoaCoreCom.common.dto;
using System.Net;
using FoaCore.Common.Util;
using CsvHelper;
using System.Threading;
//FOR COM高速化 lm  2018/06/21 Add End

namespace AIStool_DAC
{
    public class ClsSecondDAC
    {
        /*
        public const int varSheetIDRow = 1;
        public const int varSheetIDCol = 600;

        public string varSheetName = "DataSheet";

        private string MISSION_GRIP = "ミッション_GRIP";

        /// ログ書き出し用クラス
//        private ClsWriteLog m_writeLog;

        /// 統計対象ミッション
        private Dictionary<string, ClsAttribObject> m_missions = new Dictionary<string, ClsAttribObject>();

        /// 統計対象CTM
        private Dictionary<string, Dictionary<string, ClsAttribObject>> m_ctms = new Dictionary<string, Dictionary<string, ClsAttribObject>>();

        /// 統計対象Element
        private Dictionary<string, Dictionary<string, ClsAttribObject>> m_elements = new Dictionary<string, Dictionary<string, ClsAttribObject>>();

        // Wang New dac flow 20190408 Start
        private const int TitleRow = 11;
        //private const int FormulaRow = 1;
        // Wang New dac flow 20190408 End


        //dac_home No.36 start
        private const int MaxNum = 26;
        private const int COLUMN_WIDTH = 7;
        private const int FormulaRow = 2;
        //dac_home No.36 end

        ClsSubDACControler clsSubDACControler=null;

        /// <summary>
        ///
        /// </summary>
        /// <param name="execDateTime">実行日</param>
        /// <param name="clsBusinessHoursDto">業務開始・終了時間</param>
        /// <param name="dacFile">DACファイル</param>
        /// <param name="clsDACConf">設定ファイル情報</param>
        /// <returns>結果</returns>
        public bool doOffline(DateTime execDateTime, ClsCalUtil.ClsBusinessHoursDto clsBusinessHoursDto, FoaCoreCom.dac.model.DacManifest.ManifestFileInfo dacFile, ClsDACConf clsDACConf, ClsMissionLog missionLog)
        {

//            bool bRtn = false;
            
            Excel.Application app = null;
            Excel.Workbook workbook = null;
            Excel.Sheets sheets = null;

//            Excel.Range rng = null;

            try
            {
                missionLog.OutPut("第n階層DAC", "計算準備処理の開始", ClsMissionLog.LOG_LEVEL.DEBUG);

                clsSubDACControler = new ClsSubDACControler(dacFile.getFullPath(execDateTime.ToString("yyyyMMdd")), clsDACConf);

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                    "ミッション実行(オフライン)の計算を開始します。", ClsMissionLog.LOG_LEVEL.INFO);

                if (clsSubDACControler.ClsSubDACFileList.Count == 0)
                {

                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "SubDACが0件のため、DACの計算のみを実施します。",
                        ClsMissionLog.LOG_LEVEL.WARN);
                }
                else
                {
                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        clsSubDACControler.ClsSubDACFileList.Count + "件のためSubDAC計算とDAC計算を実施します。",
                        ClsMissionLog.LOG_LEVEL.DEBUG);
                }

                // 対象DACファイルのフルパスを取得
                string dacFileName = dacFile.getFullPath(execDateTime.ToString("yyyyMMdd"));

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                    "Excelファイル（" + dacFileName + "）をオープンします。", ClsMissionLog.LOG_LEVEL.DEBUG);


                // DAC帳票を開く
                app = ClsExcelUtil.StartExcelProcess();

                workbook = app.Workbooks.Open(dacFileName);
                sheets = workbook.Sheets;
                //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 start
                //Excel.Worksheet mainSheet = sheets["ＤＡＣ帳票_フォーマットイメージ"]; 
                Excel.Worksheet mainSheet = sheets[Sheet1.DAC_SHEET];
                //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 end
                if (mainSheet == null)
                {
                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 start
                        //"ＤＡＣ帳票_フォーマットイメージシートがないため、計算が実行できないため、処理を終了します。",
                        Sheet1.DAC_SHEET + "がないため、計算が実行できないため、処理を終了します。",
                        //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 end
                        ClsMissionLog.LOG_LEVEL.ERROR);
                }

                // メインシートの算行/定数セル
                Excel.Range mainCalculateCell = null;

                // 演算行/定数行の該当日付のセルを取得
                mainCalculateCell = ClsCalUtil.setMainCalculateCell(mainSheet, execDateTime);
                if (ClsExcelUtil.isEmpty(mainCalculateCell))
                {
                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 start
                        //"ＤＡＣ帳票_フォーマットイメージシートに、第一階層DACと同一の日付セルがないため、処理を終了します。",
                        Sheet1.DAC_SHEET + "に、第一階層DACと同一の日付セルがないため、処理を終了します。",
                        //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 end
                        ClsMissionLog.LOG_LEVEL.ERROR);
                    return false;
                }


                // SubDAC作成、CMS問い合わせ前に時間を比較する
                bool isAfterFlag = ClsDateUtil.isNowAfter(clsBusinessHoursDto.EndEpoMilliseconds);


                Array clsSubDACFileArray = clsSubDACControler.ClsSubDACFileList.ToArray();
                Array.Sort(clsSubDACFileArray);

                //３．ファイルリスト用パネルにファイルの一覧を表示する。
                foreach (FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subDACFile in clsSubDACFileArray)
                {
                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "SubDACファイル（" + subDACFile.displayname + "）の計算を開始します。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                    string createFile = ClsFileUtil.replicationFile(execDateTime.ToString("yyyyMMdd"), subDACFile);
                    if (String.IsNullOrEmpty(createFile))
                    {
                        missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                             "このSubDACファイル（" + subDACFile.displayname + "）は１ファイルもドラッグ＆ドロップされていないため、残りのSubDACの計算を終了します。",
                             ClsMissionLog.LOG_LEVEL.ERROR);

                        break;
                    }

                    if ("コメント".Equals(subDACFile.template))
                    {
                        missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                             "SubDACファイル（" + subDACFile.displayname + "）はコメントのため計算対象外になります",
                             ClsMissionLog.LOG_LEVEL.INFO);

                    }
                    else if ("DAC".Equals(subDACFile.template))
                    {
                        missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                            "SubDACファイル（" + subDACFile.displayname + "）はDACファイルのため、" + (clsSubDACControler.Hierarchy + 1) + "階層目の計算を開始します。",
                            ClsMissionLog.LOG_LEVEL.INFO);

                        // SubDACのDAC帳票の処理を実行する。
                        ClsSecondDAC clsSecondDAC = new ClsSecondDAC();
                        clsSecondDAC.doOffline(mainCalculateCell.Value, clsBusinessHoursDto, subDACFile, clsDACConf, missionLog);


                        missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                            "SubDACファイル（" + subDACFile.displayname + "）の" + (clsSubDACControler.Hierarchy + 1) + "階層目の計算が終了しました。",
                            ClsMissionLog.LOG_LEVEL.INFO);

                    }
                    else
                    {
                        // SubDACの計算は実施しない
                    }

                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                          "このSubDACファイル（" + subDACFile.displayname + "）の計算が終了しました。。",
                            ClsMissionLog.LOG_LEVEL.INFO);
                }


                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                    "SubDACファイル（" + clsSubDACControler.ClsSubDACFileList.Count + "ファイル）の処理が終了しました。",
                    ClsMissionLog.LOG_LEVEL.INFO);


                ///////////////////////////////////////////////////////////////////////////////////////////////

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "DACシートの計算を開始します。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                // 
                // 再計算停止
                app.Calculation = Excel.XlCalculation.xlCalculationManual;
                app.ScreenUpdating = false;
                app.DisplayAlerts = false;

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "CTM問合せ結果の出力を開始します。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                //メインシートにデータを挿入する
                ClsCalUtil.CalcurateSheet(
                    execDateTime.ToString("yyyy/MM/dd"),
                    dacFile.displayname,
                    sheets,
                    mainSheet,
                    mainCalculateCell.Row, isAfterFlag);

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "CTM問合せ結果の出力が終了しました。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                    "ミッションの計算が終了しました", ClsMissionLog.LOG_LEVEL.INFO);

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                // 更新処理
                app.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                app.Calculate();
                app.ScreenUpdating = true;

                app.DisplayAlerts = true;


            }
            catch (Exception ex)
            {

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")で例外発生により処理終了",
                    ex.Message + ex.StackTrace, ClsMissionLog.LOG_LEVEL.INFO);


            }
            finally
            {
                if (workbook != null)
                {
                    // 警告を一旦出さなくする。
                    app.DisplayAlerts = false;
                    workbook.Save();
                    app.DisplayAlerts = true;
                    workbook.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);

                }


                if (app != null)
                {
                    try
                    {
                        app.Quit();

                        System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                    }
                    catch
                    {

                    }
                }

            }


            return false;
        }



        /// <summary>
        ///
        /// </summary>
        /// <param name="execDateTime">実行日</param>
        /// <param name="clsBusinessHoursDto">業務開始・終了時間</param>
        /// <param name="dacFile">DACファイル</param>
        /// <param name="clsDACConf">設定ファイル情報</param>
        /// <returns>結果</returns>
        public bool doMission(DateTime execDateTime, ClsCalUtil.ClsBusinessHoursDto clsBusinessHoursDto,FoaCoreCom.dac.model.DacManifest.ManifestFileInfo dacFile, ClsDACConf clsDACConf,ClsMissionLog missionLog)
        {
            
            bool bRtn = false;
           
            Excel.Application app = null;
            Excel.Workbook workbook = null;
            Excel.Sheets sheets = null;

            Excel.Range rng = null;

            try
            {
                missionLog.OutPut("第n階層DAC","計算準備処理の開始", ClsMissionLog.LOG_LEVEL.DEBUG);

                clsSubDACControler = new ClsSubDACControler(dacFile.getFullPath(execDateTime.ToString("yyyyMMdd")), clsDACConf);

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                    "ミッションの計算を開始します。", ClsMissionLog.LOG_LEVEL.INFO);

                if (clsSubDACControler.ClsSubDACFileList.Count == 0)
                {

                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "SubDACが0件のため、DACの計算のみを実施します。",
                        ClsMissionLog.LOG_LEVEL.WARN);
                }
                else
                {
                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        clsSubDACControler.ClsSubDACFileList.Count + "件のためSubDAC計算とDAC計算を実施します。",
                        ClsMissionLog.LOG_LEVEL.DEBUG);
                }

                // 対象DACファイルのフルパスを取得
                string dacFileName = dacFile.getFullPath(execDateTime.ToString("yyyyMMdd"));

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                    "Excelファイル（" + dacFileName + "）をオープンします。", ClsMissionLog.LOG_LEVEL.DEBUG);


                // DAC帳票を開く
                app = ClsExcelUtil.StartExcelProcess();

                workbook = app.Workbooks.Open(dacFileName);
                sheets = workbook.Sheets;

                //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 start
                //Excel.Worksheet mainSheet = sheets["ＤＡＣ帳票_フォーマットイメージ"];
                Excel.Worksheet mainSheet = sheets[Sheet1.DAC_SHEET];
                //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 end
                if (mainSheet == null)
                {
                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 start
                        //"ＤＡＣ帳票_フォーマットイメージシートがないため、計算が実行できないため、処理を終了します。",
                        Sheet1.DAC_SHEET + "がないため、計算が実行できないため、処理を終了します。",
                        //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 end
                        ClsMissionLog.LOG_LEVEL.ERROR);
                }

                // メインシートの算行/定数セル
                Excel.Range mainCalculateCell = null;

                // 演算行/定数行の該当日付のセルを取得
                mainCalculateCell = ClsCalUtil.setMainCalculateCell(mainSheet, execDateTime);
                if (ClsExcelUtil.isEmpty(mainCalculateCell))
                {
                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 start
                        //"ＤＡＣ帳票_フォーマットイメージシートに、第一階層DACと同一の日付セルがないため、処理を終了します。",
                        Sheet1.DAC_SHEET + "に、第一階層DACと同一の日付セルがないため、処理を終了します。",
                        //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 end
                        ClsMissionLog.LOG_LEVEL.ERROR);
                    return false;
                }

                // DACファイルの業務時間を更新する
                sheets["業務時間"].Range["A1"].Value = clsBusinessHoursDto.startTimeCell.Value;
                sheets["業務時間"].Range["A2"].Value = clsBusinessHoursDto.endTimeCell.Value;

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                          "第1階層の開始時間と終了時間をDACファイルに反映します。",
                         ClsMissionLog.LOG_LEVEL.DEBUG);



                // SubDAC作成、CMS問い合わせ前に時間を比較する
                bool isAfterFlag = ClsDateUtil.isNowAfter(clsBusinessHoursDto.EndEpoMilliseconds);

                if(!isAfterFlag){
                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "業務時間より前にミッションが実行されています。", ClsMissionLog.LOG_LEVEL.WARN);
                }


                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                    "SubDACファイル（" + clsSubDACControler.ClsSubDACFileList.Count + "ファイル）の処理を開始します。",
                    ClsMissionLog.LOG_LEVEL.INFO);


                Array clsSubDACFileArray =  clsSubDACControler.ClsSubDACFileList.ToArray();
                Array.Sort(clsSubDACFileArray);

                //３．ファイルリスト用パネルにファイルの一覧を表示する。
                foreach (FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subDACFile in clsSubDACFileArray)
                {
                    if (!subDACFile.template.Equals(Properties.Resources.TEXT_STD_MULTIDAC) &&
                       !subDACFile.template.Equals(Properties.Resources.TEXT_STD_SPREADSHEET) &&
                       !subDACFile.template.Equals(Properties.Resources.TEXT_STD_DAC))
                    {
                        continue;
                    }
                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "SubDACファイル（" + subDACFile.displayname + "）の計算を開始します。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                    string createFile = ClsFileUtil.replicationFile(execDateTime.ToString("yyyyMMdd"), subDACFile);
                    if (String.IsNullOrEmpty(createFile))
                    {
                       missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                            "このSubDACファイル（" + subDACFile.displayname + "）は１ファイルもドラッグ＆ドロップされていないため、残りのSubDACの計算を終了します。",
                            ClsMissionLog.LOG_LEVEL.ERROR);

                        break;
                    }

                    if ("コメント".Equals(subDACFile.template))
                    {
                       missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                            "SubDACファイル（" + subDACFile.displayname + "）はコメントのため計算対象外になります",
                            ClsMissionLog.LOG_LEVEL.INFO);

                    }
                    else if ("DAC".Equals(subDACFile.template))
                    {
                        missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                            "SubDACファイル（" + subDACFile.displayname + "）はDACファイルのため、" + (clsSubDACControler.Hierarchy+1) + "階層目の計算を開始します。",
                            ClsMissionLog.LOG_LEVEL.INFO);

                        // SubDACのDAC帳票の処理を実行する。
                        ClsSecondDAC clsSecondDAC = new ClsSecondDAC();
                        clsSecondDAC.doMission(mainCalculateCell.Value,clsBusinessHoursDto, subDACFile,clsDACConf,missionLog);


                        missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                            "SubDACファイル（" + subDACFile.displayname + "）の" + (clsSubDACControler.Hierarchy+1) + "階層目の計算が終了しました。",
                            ClsMissionLog.LOG_LEVEL.INFO);

                    }
                    else
                    {
                        missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                            "SubDACファイル（" + subDACFile.displayname + "）の計算を開始します。",
                            ClsMissionLog.LOG_LEVEL.INFO);

                        // 期間変更アドインがあるSubDACの場合のみ計算する
                        if (ClsExcelUtil.isAddin(createFile))
                        {

                            // SubDACファイルの作成
                            bRtn = ClsSubDACUtil.createSubDACFile(
                                createFile,
                                execDateTime.ToString("yyyy/MM/dd"),
                                clsBusinessHoursDto.StartDateTime,
                                clsBusinessHoursDto.EndDateTime,
                                missionLog
                                );


                            // 致命的エラーの場合は処理を抜ける
                            if (!bRtn)
                            {
                                // 致命的なエラーの場合は、コピーしたファイルを削除する。
                                ClsFileUtil.deleteOneFile(createFile);

                                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                                    "このSubDACファイル（" + subDACFile.displayname + "）の計算でエラーが発生したため、SubDACの計算を終了します。",
                                    ClsMissionLog.LOG_LEVEL.ERROR);


                                break;
                            }


                        }
                        else
                        {
                            missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                                "このSubDACファイル（" + subDACFile.displayname + "）は計算の対象外のため、計算を実施しません。",
                                ClsMissionLog.LOG_LEVEL.WARN);

                            bRtn = false;
                        }
                    }

                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                          "このSubDACファイル（" + subDACFile.displayname + "）の計算が終了しました。。",
                            ClsMissionLog.LOG_LEVEL.INFO);
                }


                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                    "SubDACファイル（" + clsSubDACControler.ClsSubDACFileList.Count + "ファイル）の処理が終了しました。",
                    ClsMissionLog.LOG_LEVEL.INFO);


                ///////////////////////////////////////////////////////////////////////////////////////////////

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "DACシートの計算を開始します。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                // 
                // 再計算停止
                app.Calculation = Excel.XlCalculation.xlCalculationManual;
                app.ScreenUpdating = false;
                app.DisplayAlerts = false;

                ClsCalUtil.GetTargetObjects(sheets, m_missions, m_ctms, m_elements);

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                    "計算対象のミッション・CTM・エレメントの取得(" 
                    + m_missions.Count + "," + m_ctms.Count + "," + m_elements.Count + ")" ,
                    ClsMissionLog.LOG_LEVEL.INFO);

               
                // タイムアウトまでの時間をparamシートから読み込み
                int timeOutMiliSeconds = ClsCalUtil.getTimeOutMilliSeconds(sheets);

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "CTM問合せタイムアウトミリ秒数（" + timeOutMiliSeconds +"）を取得しました。",
                        ClsMissionLog.LOG_LEVEL.DEBUG);

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "CTM問合せを開始します。",
                        ClsMissionLog.LOG_LEVEL.DEBUG);

                //ClsCalUtil.ClearNotExistSheets(
                //    clsDACConf.UseProxy, clsDACConf.ProxyURI, sheets, m_missions, m_ctms, clsBusinessHoursDto.StartEpoMilliseconds, clsBusinessHoursDto.EndEpoMilliseconds, timeOutMiliSeconds);

                // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                // Multi_Dac 修正
                bool IsGripMission = false;
                Dictionary<string, ClsAttribObject> m_missions_tmp = new Dictionary<string, ClsAttribObject>();
                // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end

                //dac home No.36 start
                int datasheetIndex = 1;
                //dac home No.36 end
                // ミッション数分だけループ
                foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions)
                {
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                    // Multi_Dac 修正
                    if (mission.Key.Contains("_Route-"))
                    {
                        IsGripMission = true;
                        m_missions_tmp.Add(mission.Key, mission.Value);
                        continue;
                    }
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
                    //string jsonData;
                    string missionId = mission.Key;

                    missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                            "useProxy=" + clsDACConf.UseProxy + ",proxyUri=" + clsDACConf.ProxyURI,
                            ClsMissionLog.LOG_LEVEL.DEBUG);

                    // JSONDATAの取得
                    //FOR COM高速化 lm  2018/06/21 Modify Start
                    //jsonData = ClsJsonData.getString(clsDACConf.UseProxy, clsDACConf.ProxyURI,missionId, clsBusinessHoursDto.StartEpoMilliseconds, clsBusinessHoursDto.EndEpoMilliseconds, m_writeLog, timeOutMiliSeconds);

                    string uri = "";
                    string hostName = Globals.Sheet1.m_MissionHostName;
                    string portNo = Globals.Sheet1.m_MissionPortNo;
                    string limit = "0";
                    string lang = "ja";
                    string msg = "";
                    string zipUrl = "";
                    string unZipUrl = "";

                    uri = String.Format("http://{0}:{1}/cms/rest/mib/mission/pmzip?id={2}&start={3}&end={4}&limit={5}&lang={6}&noBulky=true",
                               hostName, portNo, missionId, clsBusinessHoursDto.StartEpoMilliseconds, clsBusinessHoursDto.EndEpoMilliseconds,limit, lang);

                    //情報を格納するZIPファイルパスを取得する
                    CtmDataRetriever ctmDataRetriever = new FoaCoreCom.ais.retriever.CtmDataRetriever();
                    zipUrl = ctmDataRetriever.GetProgramMissionZipFileAsync(uri, Globals.ThisWorkbook.clsDACConf.ProxyURI, out msg);

                    //Zipファイルを解凍する
                    ZipFileHandler zipFileHandler = new FoaCoreCom.ais.retriever.ZipFileHandler();
                    unZipUrl = zipFileHandler.unZipFile(zipUrl);

                    // ファイル件数を取得
                    Dictionary<string, string> dicResultData = new Dictionary<string, string>();
                    DirectoryInfo folder = new DirectoryInfo(unZipUrl);
                    FileInfo[] fil = folder.GetFiles("*.csv");
                    int fileCnt = 0;
                    if (fil != null)
                    {
                        fileCnt = fil.Length;
                    }

                    //FOR COM高速化 lm  2018/06/21 Modify End

                    // JSONDATAを解析して、CTM毎のCSV加工した文字列を取得する
                    int elementCount = 0;
                    foreach (var item in m_elements.Values)
                    {
                        if (elementCount < item.Values.Count)
                        {
                            elementCount = item.Values.Count;
                        }
                    }
                    // string[,] keyOrderMatrix = new string[(m_ctms[mission.Value.Id]).Count, (m_elements.ElementAt(0).Value).Count + 100];
                    string[,] keyOrderMatrix = new string[(m_ctms[mission.Value.Id]).Count, elementCount + 1];
                    missionLog.OutPut("doMission", "keyOrderMatrix: [" + (m_ctms[mission.Value.Id]).Count + "," + elementCount + "]", ClsMissionLog.LOG_LEVEL.DEBUG);
                    int ctmIndex = 0;
                    int elementIndex = 0;
                    foreach (KeyValuePair<string, ClsAttribObject> ctm in m_ctms[mission.Value.Id])
                    {
                        elementIndex = 0;                        
                        keyOrderMatrix[ctmIndex, elementIndex] = ctm.Value.Id;
                        elementIndex++;
                        missionLog.OutPut("doMission", "elementIndex: " + elementIndex, ClsMissionLog.LOG_LEVEL.DEBUG);
                        foreach (KeyValuePair<string, ClsAttribObject> element in m_elements[mission.Value.Id + ctm.Value.Id])
                        {
                            keyOrderMatrix[ctmIndex, elementIndex] = element.Value.Id;
                            elementIndex++;
                            missionLog.OutPut("doMission", "elementIndex: " + elementIndex + " element: " + element.Value.DisplayName, ClsMissionLog.LOG_LEVEL.DEBUG);
                        }
                        ctmIndex++;
                        missionLog.OutPut("doMission", "ctmIndex: " + ctmIndex, ClsMissionLog.LOG_LEVEL.DEBUG);
                    }
                    CtlAisProcess ctlAisProcess = new CtlAisProcess();
                    //FOR COM高速化 lm  2018/06/21 Delete Start
                    //Dictionary<string, string> ctmDataDic = ctlAisProcess.parseCtmMission(jsonData, clsDACConf.TimezoneId, ref keyOrderMatrix);
                    //FOR COM高速化 lm  2018/06/21 Delete End

                    // CTMごとに結果シートを作成する
                    int iCounter = 0;
                    foreach (KeyValuePair<string, ClsAttribObject> ctm in m_ctms[mission.Value.Id])
                    {
                        
                        // 結果シートの追加
                        
                        // シート名の決定
                        string SheetID = mission.Value.DisplayName + "." + ctm.Value.DisplayName;
                        //dac home No.36 start
                        //string SheetName = GetSheetName(sheets,mission.Value, ctm.Value, SheetID);
                        // Delete data sheet
                        foreach (Excel.Worksheet worksheet in sheets)
                        {
                            var value = worksheet.Cells[varSheetIDRow, varSheetIDCol].Value;
                            if (value != null)
                            {
                                if (value == SheetID)
                                {
                                    worksheet.UsedRange.Clear();
                                }
                            }

                        }

                        if (SheetID.Length > MaxNum)
                        {
                            SheetID = SheetID.Substring(0, MaxNum);
                        }

                        string SheetName = datasheetIndex + "." + SheetID;
                        // シートの追加
                        //Excel.Worksheet newWorksheet;
                        //newWorksheet = sheets.Add(After: sheets[sheets.Count]);
                        //newWorksheet.Name = SheetName;

                        // シートの追加
                        Excel.Worksheet newWorksheet = null;
                        try
                        {
                            newWorksheet = sheets[SheetName];
                        }
#pragma warning disable
                        catch (Exception e)
#pragma warning restore
                        {

                        }
                        if (newWorksheet == null)
                        {
                            newWorksheet = (Excel.Worksheet)sheets.Add(After: sheets[sheets.Count]);
                            newWorksheet.Name = SheetName;
                        }
                        datasheetIndex++;
                        //newWorksheet.Select(1);
                        newWorksheet.Select();
                        newWorksheet.Cells[1, 1].Select();
                        //dac home No.36 end
                        newWorksheet.Cells[varSheetIDRow, varSheetIDCol].Value = SheetID;

                        //FOR COM高速化 lm  2018/06/21 Modify Start
                        // 結果がない場合
                        //if (ctmDataDic.Count <= iCounter)
                        if (fileCnt <= iCounter)
                        {
                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            continue;
                        }

                        // 結果が空の場合
                        // Wang Issue of DAC-49 20181109 Start
                        //Dictionary<string, string> ctmDataDic = ctlAisProcess.ReadCtmInfoFromCSV(unZipUrl, ctm.Value.Id, Globals.ThisWorkbook.clsDACConf.TimezoneId);
                        HashSet<string> elements = new HashSet<string>();
                        elements.UnionWith(this.m_elements[mission.Value.Id + ctm.Value.Id].Keys);
                        elements.Add("RT");
                        Dictionary<string, string> ctmDataDic = ctlAisProcess.ReadCtmInfoFromCSV(unZipUrl, ctm.Value.Id, Globals.ThisWorkbook.clsDACConf.TimezoneId, elements);
                        // Wang Issue of DAC-49 20181109 End
                        string result = ctmDataDic[ctm.Value.Id];
                        if (string.IsNullOrEmpty(result))
                        {
                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            continue;
                        }

                        
                        // シート先頭行のエレメント名の挿入
                        
                        rng = (Excel.Range)workbook.Application.Selection;
                        DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                        rng[TitleRow, 1] = "No.";
                        rng[TitleRow, 2] = "RECEIVE TIME";

                        int iColCount = 3;
                        foreach (KeyValuePair<string, ClsAttribObject> element in m_elements[mission.Value.Id + ctm.Value.Id])
                        {
                            rng[TitleRow, iColCount++] = element.Value.DisplayName;
                        }

                        
                        // 結果の貼付け
                        
                        // コピー先のセルの指定
                        Excel.Range pasteCell = rng.Cells[TitleRow + 1, 1];

                        // 一旦、メモリーストリームにバイト単位で読み込む。
                        byte[] bs = Encoding.Default.GetBytes(result);
                        MemoryStream ms = new MemoryStream(bs);

                        //クリップボードに文字列をセット
                        DataObject data = new DataObject(DataFormats.CommaSeparatedValue, ms);
                        Clipboard.SetDataObject(data, false);

                        // コピー先にクリップボードの内容を貼り付ける。
                        newWorksheet.Paste(pasteCell, System.Type.Missing);

                        
                        // 演算式の挿入
                        
                        // シートの最終行の設定(FIX)
			string[] sp = new string[] { "\r\n"};
                        int rowCount = TitleRow; // result.Split(sp,StringSplitOptions.None).Length + 1;

			// TitleRow から探す
			for (int i = 1 ; i < 100000; i++) {  // title部分は数える必要はないけど。
			    Excel.Range tmp = null;
			    tmp = rng.Cells[TitleRow + i, 2]; // RECEIVE TIMEのセルを参照してデータ数を確認する。
			    if (tmp == null) {
			       break;
			    }
			    if (tmp.Value == null) {
			       break;
			    }
			    if (tmp.Value.ToString() == null) {
			       break;
                            }
			    rowCount = TitleRow + i; // データが入っていることが確認できたrow数
		        }
			rowCount++; // 最下行の下のセルまで
			
                        rng[1, 1] = mission.Value.DisplayName + "." + ctm.Value.DisplayName;

                        rng[FormulaRow, 2] = "個数";
                        rng[FormulaRow + 1, 2] = "積算";
                        rng[FormulaRow + 2, 2] = "平均";
                        rng[FormulaRow + 3, 2] = "最大";
                        rng[FormulaRow + 4, 2] = "最小";
                        rng[FormulaRow + 5, 2] = "種類数";

                        for (int i = 3; i < iColCount; i++)
                        {
                            string ColLetterTemp = newWorksheet.Cells[rowCount, i].Address;
                            string[] SplitLetter = ColLetterTemp.Split('$');
                            string ColLetter = SplitLetter[1];

                            rng[FormulaRow, i].Formula = string.Format("=COUNTA({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 1, i].Formula = string.Format("=SUM({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}12:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            rng[FormulaRow + 3, i].Formula = string.Format("=MAX({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 4, i].Formula = string.Format("=MIN({0}12:{0}{1})", ColLetter, rowCount - 1);
                            // Wang New dac flow 20190408 End
                        }

                        
                        // Excelレイアウト調整
                        
                        //罫線表示処理
                        Excel.Range cell1 = newWorksheet.Cells[FormulaRow, 2];
                        Excel.Range cell2 = newWorksheet.Cells[FormulaRow + 5, iColCount - 1];
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        cell1 = newWorksheet.Cells[TitleRow, 1];
                        cell2 = newWorksheet.Cells[rowCount - 1, iColCount - 1];
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //セルサイズを自動調整
                        newWorksheet.Columns.AutoFit();
                        //dac home No.36 start
                        newWorksheet.Cells[1, 1].ColumnWidth = COLUMN_WIDTH;
                        //dac home No.36 end
                    }
                }

                // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                // Multi_Dac 修正
                if (IsGripMission)
                {
                    string[] gripMissionId;
                    JToken missionToken = JToken.Parse("{}");
                    int routeIndex = 0;
                    string resultFolder = "";
                    gripMissionId = GetColumnValues_G(1,sheets);

                    GripDataRetrieverDac gripDataRetrieverDac = new FoaCoreCom.ais.retriever.GripDataRetrieverDac();
                    ClsSearchInfo clsSearchInfo = new ClsSearchInfo();
                    WebProxy proxy = new WebProxy();

                    List<string> SearchIds = new List<string>();

                    foreach (var missionId in gripMissionId)
                    {
                        //URLのパラメータ
                        clsSearchInfo.MissionId = missionId;
                        clsSearchInfo.GripHostName = Globals.Sheet1.m_MissionHostName;
                        clsSearchInfo.GripPortNo = int.Parse(Globals.Sheet1.m_MissionPortNo);
                        clsSearchInfo.SkipSameMainKey = false;
                        clsSearchInfo.SearchByInCondition = "auto";
                        clsSearchInfo.SDateUnixTimeString = clsBusinessHoursDto.StartEpoMilliseconds;
                        clsSearchInfo.EDateUnixTimeString = clsBusinessHoursDto.EndEpoMilliseconds;
                        clsSearchInfo.onlineId = "";
                        clsSearchInfo.SearchId = new GUID().ToString();

                        SearchIds.Add(clsSearchInfo.SearchId);

                        resultFolder = gripDataRetrieverDac.GetDacGripDatatCsvSync(clsSearchInfo, proxy, routeIndex);

                        routeIndex++;
                    }

                    routeIndex = 0;
                    foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions_tmp)
                    {
                        //string SheetID = mission.Value.DisplayName + "." + mission.Value.DisplayName;
                        //string SheetName = GetSheetName(mission.Value, mission.Value, SheetID);

                        //Microsoft.Office.Interop.Excel.Worksheet newWorksheet;
                        //newWorksheet = (Excel.Worksheet)Globals.ThisWorkbook.Worksheets.Add(After: Globals.ThisWorkbook.Worksheets[Globals.ThisWorkbook.Worksheets.Count]);
                        //newWorksheet.Name = SheetName;

                        //gripDataRetrieverDac.writeGripRouteSheets("C:\\Users\\FOA\\AppData\\Local\\Temp\\grip_temp\\" + clsSearchInfo.SearchId, routeIndex, newWorksheet);
                        //routeIndex++;

                        CtlAisProcess ctlAisProcess = new CtlAisProcess();
                        // CTMごとに結果シートを作成する
                        
                        // 結果シートの追加
                         
                        // シート名の決定
                        string SheetID = mission.Value.DisplayName;

                        //dac home No.36 start
                        //string SheetName = GetSheetName(mission.Value, mission.Value, SheetID);
                        // Delete data sheet
                        foreach (Excel.Worksheet worksheet in sheets)
                        {
                            var value = worksheet.Cells[varSheetIDRow, varSheetIDCol].Value;
                            if (value != null)
                            {
                                if (value == SheetID)
                                {
                                    worksheet.UsedRange.Clear();
                                }
                            }

                        }

                        if (SheetID.Length > MaxNum)
                        {
                            SheetID = SheetID.Substring(0, MaxNum);
                        }

                        string SheetName = datasheetIndex + "." + SheetID;



                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "シート名(" + SheetName + ")の作成を開始します。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        // シートの追加
                        //Excel.Worksheet newWorksheet;
                        //newWorksheet = (Excel.Worksheet)Globals.ThisWorkbook.Worksheets.Add(After: Globals.ThisWorkbook.Worksheets[Globals.ThisWorkbook.Worksheets.Count]);
                        //newWorksheet.Name = SheetName;
                        ////newWorksheet.Select(1);
                        // シートの追加
                        Excel.Worksheet newWorksheet = null;
                        try
                        {
                            newWorksheet = sheets[SheetName];
                        }
#pragma warning disable
                        catch (Exception e)
#pragma warning restore
                        {

                        }
                        if (newWorksheet == null)
                        {
                            newWorksheet = (Excel.Worksheet)sheets.Add(After: sheets[sheets.Count]);
                            newWorksheet.Name = SheetName;
                        }
                        newWorksheet.Select();
                        newWorksheet.Cells[1, 1].Select();
                        datasheetIndex++;
                        //dac home No.36 end

                        newWorksheet.Cells[varSheetIDRow, varSheetIDCol].Value = SheetID;

                        // 結果が空の場合
                        string result = "";
                        //FOR COM高速化 lm  2018/06/21 Modify Start
                        //string csvFilePath = unZipUrl + "\\" + ctm.Value.Id;
                        string pathString = "";
                        foreach (var searchId in SearchIds)
                        {
                            pathString = gripDataRetrieverDac.DlDir + "\\" + searchId + "\\" + mission.Value.Id + ".csv";
                            if (File.Exists(pathString))
                            {
                                pathString = gripDataRetrieverDac.DlDir + "\\" + searchId;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        Dictionary<string, string> ctmDataDic = ctlAisProcess.ReadCtmInfoFromCSV(pathString, mission.Value.Id, Globals.ThisWorkbook.clsDACConf.TimezoneId,null);
                        if (ctmDataDic != null)
                        {
                            result = ctmDataDic[mission.Value.Id];
                            if (string.IsNullOrEmpty(result))
                            {
                                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                        "処理中のCTMはファイルが作成されていません",
                                                        ClsMissionLog.LOG_LEVEL.WARN);

                                newWorksheet.Cells[1, 1].Value = "データ無し";
                                continue;
                            }
                        }
                        //FOR COM高速化 lm  2018/06/21 Modify End
                        else
                        {
                            missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                    "処理中のCTMはファイルが作成されていません",
                                                    ClsMissionLog.LOG_LEVEL.WARN);

                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            continue;
                        }

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")があるので、シートへの出力を開始します",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "エレメント情報の出力を開始します。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        
                        // シート先頭行のエレメント名の挿入
                        
                        rng = (Excel.Range)workbook.Application.Selection;
                        DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                        // Wang New dac flow 20190408 Start
                        //rng[1, 1] = "No.";
                        rng[TitleRow, 1] = "No.";
                        // Wang New dac flow 20190408 End
                        //ISSUE_NO.748 Sunyi 2018/08/13 Start
                        //rng[1, 2] = "収集時刻";
                        //rng[1, 2] = "RECEIVE TIME";
                        //ISSUE_NO.748 Sunyi 2018/08/13 End
                        int iColCount = 2;
                        string csvFile = pathString + "\\" + mission.Value.Id + ".csv";
                        using (var reader = new CsvReader(new StreamReader(csvFile, Encoding.GetEncoding("UTF-8"))))
                        {
                            reader.Read();
                            var headers = reader.FieldHeaders;
                            foreach (var header in headers)
                            {
                                // Wang New dac flow 20190408 Start
                                //rng[1, iColCount++] = header;
                                rng[TitleRow, iColCount++] = header;
                                // Wang New dac flow 20190408 End
                            }
                        }
                        
                        // 結果の貼付け
                       
                        // コピー先のセルの指定
                        // Wang New dac flow 20190408 Start
                        //Excel.Range pasteCell = rng.Cells[2, 1];
                        Excel.Range pasteCell = rng.Cells[TitleRow + 1, 1];
                        // Wang New dac flow 20190408 End

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")の読込を開始します。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        // 一旦、メモリーストリームにバイト単位で読み込む。
                        byte[] bs = Encoding.Default.GetBytes(result);
                        MemoryStream ms = new MemoryStream(bs);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")の読込結果をクリップボードにセットします。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        //クリップボードに文字列をセット
                        DataObject data = new DataObject(DataFormats.CommaSeparatedValue, ms);
                        Clipboard.SetDataObject(data, false);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")のクリップボードの内容をCTM用のExcelシートにペーストします。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        Thread.Sleep(1000);

                        // コピー先にクリップボードの内容を貼り付ける。
                        newWorksheet.Paste(pasteCell, System.Type.Missing);

                        
                        // 演算式の挿入
                        
                        // 最終行の設定
                        int rowCount = result.Split('\n').Length + 1;


                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "CTM用のExcelシートには、" + rowCount + "件をペーストしました。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "CTM用のExcelシートの末尾への計算式の埋込を開始します",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        //エレメントの計算値入力                        
                        // Wang New dac flow 20190408 Start
                        //rng[rowCount, 1] = "個数";
                        //rng[rowCount + 1, 1] = "積算";
                        //rng[rowCount + 2, 1] = "平均";
                        //rng[rowCount + 3, 1] = "最大";
                        //rng[rowCount + 4, 1] = "最小";
                        //rng[rowCount + 5, 1] = "種類数";

                        //dac home No.36 start
                        rng[1, 1] = mission.Value.DisplayName;
                        //dac home No.36 end

                        rng[FormulaRow, 1] = "個数";
                        rng[FormulaRow + 1, 1] = "積算";
                        rng[FormulaRow + 2, 1] = "平均";
                        rng[FormulaRow + 3, 1] = "最大";
                        rng[FormulaRow + 4, 1] = "最小";
                        rng[FormulaRow + 5, 1] = "種類数";
                        // Wang New dac flow 20190408 End

                        for (int i = 2; i < iColCount; i++)
                        {
                            string ColLetterTemp = newWorksheet.Cells[rowCount, i].Address;
                            string[] SplitLetter = ColLetterTemp.Split('$');
                            string ColLetter = SplitLetter[1];

                            // Wang New dac flow 20190408 Start
                            //rng[rowCount, i].Formula = string.Format("=COUNTA({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 1, i].Formula = string.Format("=SUM({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}2:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            //rng[rowCount + 3, i].Formula = string.Format("=MAX({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 4, i].Formula = string.Format("=MIN({0}2:{0}{1})", ColLetter, rowCount - 1);

                            rng[FormulaRow, i].Formula = string.Format("=COUNTA({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 1, i].Formula = string.Format("=SUM({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}12:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            rng[FormulaRow + 3, i].Formula = string.Format("=MAX({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 4, i].Formula = string.Format("=MIN({0}12:{0}{1})", ColLetter, rowCount - 1);
                            // Wang New dac flow 20190408 End
                        }

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "CTM用のExcelシートの罫線・サイズ等のレイアウト調整を開始します",
                                                ClsMissionLog.LOG_LEVEL.INFO);
                        
                        // Excelレイアウト調整
                        
                        //罫線表示処理
                        // Wang New dac flow 20190408 Start
                        //Excel.Range cell1 = newWorksheet.Cells[1, 1];
                        //Excel.Range cell2 = newWorksheet.Cells[rowCount - 1, 1];
                        Excel.Range cell1 = newWorksheet.Cells[FormulaRow, 2];
                        Microsoft.Office.Interop.Excel.Range cell2 = newWorksheet.Cells[FormulaRow + 5, iColCount - 1];
                        // Wang New dac flow 20190408 End
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        // Wang New dac flow 20190408 Start
                        //cell1 = newWorksheet.Cells[1, 2];
                        //cell2 = newWorksheet.Cells[rowCount + 5, iColCount - 1];
                        cell1 = newWorksheet.Cells[TitleRow, 1];
                        cell2 = newWorksheet.Cells[rowCount - 1, iColCount - 1];
                        // Wang New dac flow 20190408 End
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //セルサイズを自動調整
                        newWorksheet.Columns.AutoFit();
                        //dac home No.36 start
                        newWorksheet.Cells[1, 1].ColumnWidth = COLUMN_WIDTH;
                        //dac home No.36 end

                    }
                }
                // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End

                
                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "CTM問合せが終了しました",
                        ClsMissionLog.LOG_LEVEL.DEBUG);


                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "CTM問合せ結果の出力を開始します。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                //メインシートにデータを挿入する
                ClsCalUtil.CalcurateSheet(
                    execDateTime.ToString("yyyy/MM/dd"),
                    dacFile.displayname,
                    sheets,
                    mainSheet,
                    mainCalculateCell.Row, isAfterFlag);

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                        "CTM問合せ結果の出力が終了しました。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")",
                    "ミッションの計算が終了しました", ClsMissionLog.LOG_LEVEL.INFO);

                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


                // 更新処理
                app.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                app.Calculate();
                app.ScreenUpdating = true;

                app.DisplayAlerts = true;


            }
            catch (Exception ex)
            {

                missionLog.OutPut("第" + clsSubDACControler.Hierarchy + "階層DAC(" + clsSubDACControler.DACName + ")で例外発生により処理終了",
                    ex.Message + ex.StackTrace, ClsMissionLog.LOG_LEVEL.INFO);


            }
            finally
            {
                if (workbook != null)
                {
                    // 警告を一旦出さなくする。
                    app.DisplayAlerts = false;
                    workbook.Save();
                    app.DisplayAlerts = true;
                    workbook.Close();
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(workbook);

                }


                if (app != null)
                {
                    try
                    {
                        app.Quit();

                        System.Runtime.InteropServices.Marshal.ReleaseComObject(app);
                    }
                    catch
                    {

                    }
                }

            }


            return false;
        }

        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
        // Multi_Dac 修正
        private string[] GetColumnValues_G(int col,Excel.Sheets sheets)
        {
            //Excel.Worksheet sheet = Globals.ThisWorkbook.Sheets["ミッション"];
            Excel.Worksheet sheet = sheets[MISSION_GRIP];
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
            if (sheet.UsedRange.Rows.Count > 1)
            {
                Excel.Range idRange = sheet.UsedRange.Columns[col];
                return ((System.Array)idRange.Cells.Value).OfType<object>().Select(o => o.ToString()).ToArray();
            }
            else // if only 1 element, 1 ctm, and 1 mission is selected, use the below code..
            {
                Microsoft.Office.Interop.Excel.Range cells = sheet.Cells;

                List<string> values = new List<string>();
                for (int rowIndex = 1; rowIndex <= sheet.UsedRange.Rows.Count; rowIndex++)
                {
                    if (cells[rowIndex, col].Value != null)
                        values.Add(cells[rowIndex, col].Value.ToString());
                }
                return values.ToArray();
            }
        }

        /// <summary>
        ///各シートの600列の値を比較し
        ///演算名と一致するシートがある場合、削除して、新規する
        ///演算名と一致するシートがない場合、「Data+count+1」のシートを新規する
        /// </summary>
        public string GetSheetName(ClsAttribObject mission, ClsAttribObject ctm, string SheetID)
        {
            // Delete data sheet
            foreach (Excel.Worksheet worksheet in Globals.ThisWorkbook.Sheets)
            {
                var value = worksheet.Cells[varSheetIDRow, varSheetIDCol].Value;
                if (value != null)
                {
                    if (value == SheetID)
                    {
                        worksheet.Delete();
                    }
                }

            }

            // Get existing data sheet names
            HashSet<string> dataSheetNames = new HashSet<string>();

            foreach (Excel.Worksheet newWorksheet in Globals.ThisWorkbook.Sheets)
            {
                string sheetName = newWorksheet.Name;
                string dataSheetPrefix = sheetName.Substring(0, Math.Min(9, sheetName.Length));

                if (dataSheetPrefix.Equals(varSheetName))
                {
                    dataSheetNames.Add(newWorksheet.Name);
                }
            }

            // Get new data sheet name
            string newDataSheetName = "";
            for (int index = 1; ; index++)
            {
                newDataSheetName = varSheetName + index;
                if (!dataSheetNames.Contains(newDataSheetName))
                {
                    break;
                }
            }

            return newDataSheetName;
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End

        /// <summary>
        ///各シートの600列の値を比較し
        ///演算名と一致するシートがある場合、削除して、新規する
        ///演算名と一致するシートがない場合、「Data+count+1」のシートを新規する
        /// </summary>
        public string GetSheetName(Excel.Sheets sheets, ClsAttribObject mission, ClsAttribObject ctm, string SheetID)
        {
            // Delete data sheet
            foreach (Excel.Worksheet worksheet in sheets)
            {
                var value = worksheet.Cells[varSheetIDRow, varSheetIDCol].Value;
                if (value != null)
                {
                    if (value == SheetID)
                    {
                        worksheet.Delete();
                    }
                }

            }

            // Get existing data sheet names
            HashSet<string> dataSheetNames = new HashSet<string>();

            foreach (Excel.Worksheet newWorksheet in sheets)
            {
                string sheetName = newWorksheet.Name;
                string dataSheetPrefix = sheetName.Substring(0, Math.Min(9, sheetName.Length));

                if (dataSheetPrefix.Equals(varSheetName))
                {
                    dataSheetNames.Add(newWorksheet.Name);
                }
            }

            // Get new data sheet name
            string newDataSheetName = "";
            for (int index = 1; ; index++)
            {
                newDataSheetName = varSheetName + index;
                if (!dataSheetNames.Contains(newDataSheetName))
                {
                    break;
                }
            }

            return newDataSheetName;
        }
    */
    }

}
