﻿#define NT_MODIFY

using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;
using System.Windows.Forms;
using System.Xml.Linq;
using Microsoft.Office.Tools.Excel;
using Microsoft.VisualBasic;
using Microsoft.VisualStudio.Tools.Applications.Runtime;
using Excel = Microsoft.Office.Interop.Excel;
using XlTool = Microsoft.Office.Tools.Excel;
using Office = Microsoft.Office.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


using FoaCore.Common.Util;

using System.Threading.Tasks;
using System.Threading;

using AIStool_DAC.util.date;
using AIStool_DAC.util.excel;
using AIStool_DAC.util.file;
using AIStool_DAC.util.http;
using AIStool_DAC.util.subDAC;
using AIStool_DAC.util.cal;
using AIStool_DAC.util.log;

using AIStool_DAC.conf;
using AIStool_DAC.read;
using FoaCoreCom.common.main;
//FOR COM高速化 lm  2018/06/21 Add Start
using FoaCoreCom.ais.retriever;
//FOR COM高速化 lm  2018/06/21 Add End

using FoaCoreCom.common.dto;
using FoaCoreCom.ais.constant;
using FoaCoreCom.dac;
using FoaCore.Net;
using System.Net;
using CsvHelper;
using log4net;
using AIStool_DAC.Properties;
using System.Globalization;

namespace AIStool_DAC
{
#if NT_MODIFY
    /// <summary>
    /// AISデータ加工用エクセルシート
    /// </summary>
    public partial class Sheet1
    {
        #region"メンバー"

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_menuButton;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
//        private Office.CommandBarButton m_SampleButton;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_actionButton;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_offlineButton;


        /// <summary>
        /// 現在のアクティブセル
        /// </summary>
        private Excel.Range m_activeRange;

        /// <summary>
        /// ログ書き出し用クラス
        /// </summary>
        private ClsWriteLog m_writeLog;

        /// <summary>
        /// アプリケーション読み込みフラグ
        /// </summary>
        public bool flagLoading = false;

        /// <summary>
        /// 定数sheetID
        /// </summary>
        public const int varSheetIDRow = 1;
        public const int varSheetIDCol = 600;
        //dac_home datasheet start
        private const int MaxNum = 26;
        private const int COLUMN_WIDTH = 7;
        private const int FormulaRow = 2;
        //dac_home datasheet end
        // Wang New dac flow 20190308 Start
        private const int TitleRow = 11;
        //private const int FormulaRow = 1;
        // Wang New dac flow 20190308 End

        /// <summary>
        /// 定数sheetName
        /// </summary>
        public string varSheetName = "DataSheet";

        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
        // Multi_Dac 修正
        private string MISSION_M = @"ミッション_MISSION";
        private string MISSION_G = "_Routes";
        private string MISSION_GRIP = @"ミッション_GRIP";

        /// <summary>
        /// 一時ファイル置き場
        /// </summary>
        public static readonly string DlDir = System.AppDomain.CurrentDomain.BaseDirectory + "\\grip_temp";
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End

        /// <summary>
        /// ミッション投入用ホスト
        /// </summary>
        /// 
        public string m_MissionHostName { get; set; }

        /// <summary>
        /// ミッション投入用ポート
        /// </summary>
        public string m_MissionPortNo { get; set; }

        //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 start
        /// <summary>
        /// ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート
        /// </summary>
        public static readonly string DAC_SHEET = Resources.DACSHEET; //"ＤＡＣシート"; // MAX: Column 'BJ'

        /// <summary>
        /// 計算式入力開始列
        /// </summary>
        //public static readonly int FOMULAR_END_COL = 62; // MAX: Column 'BJ'
        public static readonly int FOMULAR_END_COL = 402; // MAX: Column 'BJ'
        //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 end

        public static readonly int ASSITANT_ROW = 2;

        /// <summary>
        /// 統計対象ミッション
        /// </summary>
        private Dictionary<string, ClsAttribObject> m_missions = new Dictionary<string, ClsAttribObject>();

        /// <summary>
        /// 統計対象CTM
        /// </summary>
        private Dictionary<string, Dictionary<string, ClsAttribObject>> m_ctms = new Dictionary<string, Dictionary<string, ClsAttribObject>>();

        /// <summary>
        /// 統計対象Element
        /// </summary>
        private Dictionary<string, Dictionary<string, ClsAttribObject>> m_elements = new Dictionary<string, Dictionary<string, ClsAttribObject>>();

        // 演算行／定数セル
        public static Excel.Range baseCalCell = null;

        // 業務時間セル
        public static Excel.Range baseTimeCell = null;

        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        /// <summary>
        /// Sub DAC sheet name
        /// </summary>
        private const string DAC_REFERE_SHEET = "dac_refere";
        // Wang Issue NO.687 2018/05/18 End

        #endregion


        #region VSTO デザイナーで生成されたコード

        /// <summary>
        /// デザイナーのサポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(this.Sheet1_Startup);
            this.Shutdown += new System.EventHandler(this.Sheet1_Shutdown);

            this.Change += new Microsoft.Office.Interop.Excel.DocEvents_ChangeEventHandler(this.Sheet1_Change);
            this.SelectionChange += new Microsoft.Office.Interop.Excel.DocEvents_SelectionChangeEventHandler(this.selectionChange);

            Globals.ThisWorkbook.BeforeSave += new Excel.WorkbookEvents_BeforeSaveEventHandler(Workbook_BeforeSave);
            Globals.ThisWorkbook.AfterSave += new Excel.WorkbookEvents_AfterSaveEventHandler(Workbook_AfterSave);

            this.BeforeDoubleClick += Sheet1_BeforeDoubleClick;
        }

        void Sheet1_BeforeDoubleClick(Excel.Range Target, ref bool Cancel)
        {

            // m_subDacActionPanelを開いているか確認
            Control[] con = Globals.ThisWorkbook.ActionsPane.Controls.Find(ClsReadData.CAL_ACTION_PANEL, false);

            // アクションパネルが開かれていない場合
            if (con.Length == 1 && con[0].Name.Equals("m_calActionPanel"))
            {

                // 項目演算・補助式の値を格納
                string formula = string.Empty;
                string assitant = string.Empty;

                // 現在のセルを取得
                Excel.Range range = Application.ActiveCell;
                int row = range.Row;
                int col = range.Column;

                // 現在のセルが演算行の場合
                if (
                        row == Sheet1.baseCalCell.Row
                    && col > Sheet1.baseCalCell.Column && col <= FOMULAR_END_COL)
                {
                    // アクションパネルの項目演算に、Excelのアクティブセルの値を設定する
                    // 退避した値を持ってこないと、SubDAC参照式やExcel演算式が入力されてしまう。
                    formula = Globals.ThisWorkbook.Sheets["退避"].Cells[1, col].Value;

                    // アクションパネルの補助式に、退避シートから取得した補助式の値を設定する。
                    assitant = Globals.ThisWorkbook.Sheets["退避"].Cells[ASSITANT_ROW, col].Value;
                }


                Globals.ThisWorkbook.m_calActionPanel.Formula = formula;
                Globals.ThisWorkbook.m_calActionPanel.Assitant = assitant;
                Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;


            }
        }

        #endregion

        /// <summary>
        /// 開始時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sheet1_Startup(object sender, System.EventArgs e)
        {

            Globals.ThisWorkbook.workbookLog.OutPut("Sheet1_Startup.",
                "Sheet1の起動" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

            m_writeLog = new ClsWriteLog("Sheet1");
            //アプリケーション構成ファイル読み込み
            this.LoadConfiguration();
            //起動処理呼び出し
            this.InitializeSheet();

            // このシートをActiveにする
            this.Activate();



            // 業務時間の位置を確定する
            Sheet1.baseTimeCell = ClsExcelUtil.findRangeByWord(this.Cells, "開始");
            if (null == Sheet1.baseTimeCell)
            {
                MessageBox.Show("操業時刻セルが見つかりません");
                Globals.ThisWorkbook.workbookLog.OutPut("Sheet1_Startup.",
                    "操業時刻セルが見つかりません" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.WARN);

            }
            else
            {
                Globals.ThisWorkbook.workbookLog.OutPut("Sheet1_Startup.",
                    "操業時刻セルが見つかりました（" + Sheet1.baseTimeCell.Column + "," + Sheet1.baseTimeCell.Row + "）" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

            }
            
            // 演算行／定数の位置を確定する
            // Wang New dac flow 20190308 Start
            //Sheet1.baseCalCell = ClsExcelUtil.searchCellByValue((Excel.Worksheet)Globals.ThisWorkbook.ActiveSheet, "演算行/定数", 50, 50);
            Sheet1.baseCalCell = ClsExcelUtil.searchCellByValue((Excel.Worksheet)Globals.ThisWorkbook.Worksheets[DAC_SHEET], "演算行/定数", 50, 50);
            // Wang New dac flow 20190308 End
            if (null == Sheet1.baseCalCell)
            {
                MessageBox.Show("演算行/定数セルが見つかりません");
                Globals.ThisWorkbook.workbookLog.OutPut("Sheet1_Startup.",
                    "演算行/定数セルが見つかりません" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.WARN);
            }
            else
            {
                Globals.ThisWorkbook.workbookLog.OutPut("Sheet1_Startup.",
                    "演算行/定数セルが見つかりました（" + Sheet1.baseCalCell.Column + "," + Sheet1.baseCalCell.Row + "）" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

            }


            // N階層の表示
            Excel.Range hierarchyRange = this.Cells[2, 2];
            if (!ClsExcelUtil.isEmpty(hierarchyRange))
            {
                string value = hierarchyRange.Value;
                System.Text.RegularExpressions.Regex r =
                    new System.Text.RegularExpressions.Regex(@"第\d+階層");
                value = r.Replace(value, "第" + Globals.ThisWorkbook.clsSubDACControler.Hierarchy + "階層");
                hierarchyRange.Value = value.Replace("n", "" + Globals.ThisWorkbook.clsSubDACControler.Hierarchy);

            }




            if (Globals.ThisWorkbook.clsSubDACControler.Hierarchy == 1)
            {

                Globals.ThisWorkbook.workbookLog.OutPut("Sheet1_Startup.",
                    "第1階層としてオープン" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                ClsExcelUtil.setSizeAndPosition(this.Application);
                this.selectionChange(Application.ActiveCell);

            }
            else
            {

                if (Globals.ThisWorkbook.clsSubDACControler.Hierarchy == 2)
                {

                    NamedRange rng =
                        this.Controls.AddNamedRange(this.Rows["1:2"], "NamedRange1");

                    rng.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.BurlyWood);

                }
                else if (Globals.ThisWorkbook.clsSubDACControler.Hierarchy == 3)
                {
                    NamedRange rng =
                        this.Controls.AddNamedRange(this.Rows["1:2"], "NamedRange1");

                    rng.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightGreen);

                }
    

                Globals.ThisWorkbook.workbookLog.OutPut("Sheet1_Startup.",
                    "日付の取得開始" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                // 日付を取得する
                string strOperationDate = ClsDateUtil.getOperationDate(Globals.ThisWorkbook.clsSubDACControler);

                Globals.ThisWorkbook.workbookLog.OutPut("Sheet1_Startup.",
                    "日付の取得終了（" + strOperationDate + "）" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);

                if (!string.IsNullOrEmpty(strOperationDate))
                {
                    DateTime operationDate;
                    DateTime.TryParse(strOperationDate, out operationDate);

                    // 検索開始場所を設定する
                    int row = baseCalCell.Row;
                    int column = baseCalCell.Column;

                    // 一致する日付があるかを検索する
                    bool bMatch = false;
                    for (int rowMove = 0; rowMove < 100; rowMove++)
                    {
                        row = row + 1;
                        DateTime dateTime;
                        try
                        {
                            Excel.Range tmpRange = this.Cells[row, column];

                            if (!ClsExcelUtil.isEmpty(tmpRange) && tmpRange.Value is DateTime)
                            {
                                dateTime = tmpRange.Value;
                                if (operationDate.ToString("yyyy/MM/dd") == dateTime.ToString("yyyy/MM/dd"))
                                {
                                    bMatch = true;
                                    break;
                                }
                            }
                        }
                        catch
                        {
                            bMatch = false;
                            break;
                        }

                    }

                    Globals.ThisWorkbook.workbookLog.OutPut("Sheet1_Startup.",
                        "日付の一致結果（" + bMatch + "）" + System.Environment.NewLine, ClsMissionLog.LOG_LEVEL.DEBUG);


                    // 一致した場合は、日付セルを選択して、selectChangeイベントハンドラを呼び出す。
                    if (bMatch)
                    {
                        Excel.Range tmpRange = this.Cells[row, column];
                        tmpRange.Select();

                        this.selectionChange(Application.ActiveCell);
                    }


                }
            }

        }


        /// <summary>
        /// 終了時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sheet1_Shutdown(object sender, System.EventArgs e)
        {
            this.SaveInitialize();
            Globals.ThisWorkbook.BeforeSave -= new Excel.WorkbookEvents_BeforeSaveEventHandler(Workbook_BeforeSave);
        }


        /// <summary>
        /// シート1を初期化する
        /// </summary>
        public void InitializeSheet()
        {
            string userId = "";

            try
            {
                //USER情報確認処理
                if (Globals.ThisWorkbook.Sheets["USERINFO"].Cells[1, 1].Value == null)
                {
                    flagLoading = true;
                    //ユーザー情報がなかった場合処理を抜け、ユーザー情報が入力されるまで待機する
                    return;
                }
                else
                {
                    if (flagLoading)
                    {
                        userId = Globals.ThisWorkbook.Sheets["USERINFO"].Cells[1, 1].Value.ToString();
                        userId = userId.Replace("'", "");
                    }
                    else
                    {
                        flagLoading = true;
                        InitializeSheet();
                        return;
                    }
                }

                //ロックを解除する
                this.Unprotect();

                //メニューを初期化する
                this.InitializeMenu();

                //前画面で設定したMission、CTM、ELEMENTを取得する
                this.GetTargetObjects();

                //DocumentActionPaneを初期化する
                Globals.ThisWorkbook.m_calActionPanel.Ctms = m_ctms;
                Globals.ThisWorkbook.m_calActionPanel.Elements = m_elements;
                Globals.ThisWorkbook.m_calActionPanel.Missions = m_missions;
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("Sheet1_Startup", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// メニューを初期化する
        /// </summary>
        private void InitializeMenu()
        {
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            ////入力規則によりコマンドバーにボタン追加
            ////コマンドメニューにアクションパネル開閉ボタン追加


            //Office.MsoControlType menuItem4 = Office.MsoControlType.msoControlButton;
            //if (m_offlineButton != null)
            //{
            //    m_offlineButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Offline_Click);
            //    m_offlineButton.Delete();
            //}
            //m_offlineButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem4, missing, missing, 1, true);

            //m_offlineButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            //m_offlineButton.Caption = "ミッション実行(オフライン)";
            //m_offlineButton.Tag = "0";
            //m_offlineButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Offline_Click);




            //Office.MsoControlType menuItem3 = Office.MsoControlType.msoControlButton;
            //if (m_actionButton != null)
            //{
            //    m_actionButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);
            //    m_actionButton.Delete();
            //}
            //m_actionButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem3, missing, missing, 1, true);

            //m_actionButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            //m_actionButton.Caption = "アクションパネル切替";
            //m_actionButton.Tag = "0";
            //m_actionButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);



            ////コマンドメニューにサンプル実行ボタンを追加
            //Office.MsoControlType menuItem2 = Office.MsoControlType.msoControlButton;
            //if (m_SampleButton != null)
            //{
            //    m_SampleButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);
            //    m_SampleButton.Delete();
            //}
            //m_SampleButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem2, missing, missing, 1, true);

            //m_SampleButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            //m_SampleButton.Caption = "サンプル表示";
            //m_SampleButton.Tag = "0";
            //m_SampleButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);



            ////コマンドメニューにミッション実行ボタンを追加
            //Office.MsoControlType menuItem = Office.MsoControlType.msoControlButton;
            //if (m_menuButton != null)
            //{
            //    m_menuButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
            //    m_menuButton.Delete();
            //}
            //m_menuButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem, missing, missing, 1, true);

            //m_menuButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            //m_menuButton.Caption = "ミッション実行";
            //m_menuButton.Tag = "0";
            //m_menuButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
            try
            {
                Worksheet subDac = Globals.ThisWorkbook.Worksheets[DAC_REFERE_SHEET];
                if (subDac != null) return;
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }

            Office.CommandBar contextMenu = Application.CommandBars["Cell"];

            ////コマンドメニューにサンプル実行ボタンを追加
            //Office.MsoControlType menuItem2 = Office.MsoControlType.msoControlButton;
            //if (m_SampleButton != null)
            //{
            //    m_SampleButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);
            //    m_SampleButton.Delete();
            //}
            //m_SampleButton = (Office.CommandBarButton)contextMenu.Controls.Add(menuItem2, missing, missing, 1, true);

            //m_SampleButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            //m_SampleButton.Caption = Properties.Resources.DAC_MENU_SHOW_SAMPLE;
            //m_SampleButton.Tag = "0";
            //m_SampleButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);



            //入力規則によりコマンドバーにボタン追加
            //コマンドメニューにアクションパネル開閉ボタン追加

            Office.MsoControlType menuItem4 = Office.MsoControlType.msoControlButton;
            if (m_offlineButton != null)
            {
                m_offlineButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Offline_Click);
                m_offlineButton.Delete();
            }
            m_offlineButton = (Office.CommandBarButton)contextMenu.Controls.Add(menuItem4, missing, missing, 1, true);

            m_offlineButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            m_offlineButton.Caption = Properties.Resources.DAC_MENU_EXECUTE_MISSION_OFFLINE;
            m_offlineButton.Tag = "0";
            m_offlineButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Offline_Click);
            m_offlineButton.BeginGroup = false;




            Office.MsoControlType menuItem3 = Office.MsoControlType.msoControlButton;
            if (m_actionButton != null)
            {
                m_actionButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);
                m_actionButton.Delete();
            }
            m_actionButton = (Office.CommandBarButton)contextMenu.Controls.Add(menuItem3, missing, missing, 1, true);

            m_actionButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            m_actionButton.Caption = Properties.Resources.DAC_MENU_SWITCH_PANE;
            m_actionButton.Tag = "0";
            m_actionButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);
            m_actionButton.BeginGroup = false;



            //コマンドメニューにミッション実行ボタンを追加
            Office.MsoControlType menuItem = Office.MsoControlType.msoControlButton;
            if (m_menuButton != null)
            {
                m_menuButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
                m_menuButton.Delete();
            }
            m_menuButton = (Office.CommandBarButton)contextMenu.Controls.Add(menuItem, missing, missing, 1, true);

            m_menuButton.Style = Office.MsoButtonStyle.msoButtonCaption;
            m_menuButton.Caption = Properties.Resources.DAC_MENU_EXECUTE_MISSION;
            m_menuButton.Tag = "0";
            m_menuButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
            m_menuButton.BeginGroup = false;
            Globals.ThisWorkbook.workbookLog.OutPut("Initialize Menu", "OK", ClsMissionLog.LOG_LEVEL.DEBUG);
            contextMenu.Controls[5].BeginGroup = true;
            // Wang Issue NO.687 2018/05/18 End
        }

        /// <summary>
        /// 前画面で設定したMission、CTM、ELEMENTを取得する
        /// </summary>
        private void GetTargetObjects()
        {
            string[] pids;
            string[] ids;
            string[] displayNames;

            // Wang Issue DAC-98 20190416 Start
            m_missions.Clear();
            m_ctms.Clear();
            m_elements.Clear();
            // Wang Issue DAC-98 20190416 End

            //Missionを読み込む
            if (HasMissionSheet())
            {
                ids = GetColumnValues(1);
                displayNames = GetColumnValues(2);
                for (int i = 0; i < ids.Length; i++)
                {
                    string id = (string)ids.GetValue(i);
                    string displayName = (string)displayNames.GetValue(i);
                    m_missions.Add(id, new ClsAttribObject(id, id, displayName));
                }

                //CTMを読み込む
                pids = GetColumnValues(3);
                ids = GetColumnValues(4);
                displayNames = GetColumnValues(5);
                BuildDictionary(pids, ids, displayNames, m_ctms);

                //Elementを読み込む
                string[] pids1;
                string[] pids2;
                pids1 = GetColumnValues(9); // Use MissionID as pids1
                pids2 = GetColumnValues(6); // Use CtmID as pids2
                ids = GetColumnValues(7);
                displayNames = GetColumnValues(8);
                BuildDictionary(pids1, pids2, ids, displayNames, m_elements);
            }
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
            // Multi_Dac 修正
            Excel.Range routesRange = null;
            try
            {
                int routeIndex = 0;
                for (int i = 1; i <= Globals.ThisWorkbook.Sheets.Count; i++)
                {
                    bool IsGrip = false;
                    Excel.Worksheet routesSheet = Globals.ThisWorkbook.Sheets[i];
                    if (routesSheet.Name.Equals(routeIndex.ToString() + MISSION_G))
                    {
                        routesRange = routesSheet.UsedRange;
                        IsGrip = true;
                        routeIndex++;
                    }
                    if (IsGrip)
                    {
                        string routeMissionName = "";
                        string routeCtmName = "";
                        string routeElementName = "";
                        for (int j = 1; j <= routesRange.Rows.Count; j++)
                        {
                            Excel.Range routesRange1 = routesRange.Cells[j, 1];
                            string gripMission = routesRange1.Value;
                            Excel.Range routesRange2 = routesRange.Cells[j, 2];
                            string gripCtm = routesRange2.Value;
                            Excel.Range routesRange3 = routesRange.Cells[j, 3];
                            string gripElement = routesRange3.Value;
                            if (gripMission != null)
                            {
                                routeMissionName = gripMission;
                                m_missions.Add(routeMissionName, new ClsAttribObject(routeMissionName, routeMissionName, routeMissionName));
                                if (gripCtm != null)
                                {
                                    routeCtmName = gripCtm;
                                    BuildDictionary_Grip(routeMissionName, routeCtmName, m_ctms);

                                    if (gripElement != null)
                                    {
                                        routeElementName = gripElement;
                                        BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                    }
                                }
                            }
                            if (gripMission == null && gripCtm != null)
                            {
                                if (gripCtm != null)
                                {
                                    routeCtmName = gripCtm;
                                    BuildDictionary_Grip(routeMissionName, routeCtmName, m_ctms);

                                    if (gripElement != null)
                                    {
                                        routeElementName = gripElement;
                                        BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                    }
                                }
                            }
                            if (gripMission == null && gripCtm != null)
                            {
                                if (gripCtm != null)
                                {
                                    routeCtmName = gripCtm;
                                    BuildDictionary_Grip(routeMissionName, routeCtmName, m_ctms);

                                    if (gripElement != null)
                                    {
                                        routeElementName = gripElement;
                                        BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                    }
                                }
                            }
                            if (gripMission == null && gripCtm == null && gripElement != null)
                            {
                                if (gripElement != null)
                                {
                                    routeElementName = gripElement;
                                    BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                }
                            }
                            //if (gripMission == null && gripCtm == null && gripElement == null)
                            //{
                            //    break;
                            //}
                        }
                    }
                }
            }
            catch(Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        /// <summary>
        /// 親子関係表すオブジェクトを作成する
        /// </summary>
        private void BuildDictionary_Grip(string pids, string displayNames, Dictionary<string, Dictionary<string, ClsAttribObject>> dict)
        {
            //親ID
            string pid = pids;
            //ID
            string id = displayNames;
            //表示名
            string displayName = displayNames;

            ClsAttribObject obj = new ClsAttribObject(pid, id, displayName);
            Dictionary<string, ClsAttribObject> list;

            if (dict.ContainsKey(pid))
            {
                if (!dict[pid].ContainsKey(id))
                {
                    dict[pid].Add(id, obj);
                }
                //親IDは既に存在する場合はオブジェクトリストに追加する
            }
            else
            {
                //親IDは存在しない場合はオブジェクトリストを作成する
                list = new Dictionary<string, ClsAttribObject>();
                list.Add(id, obj);
                dict.Add(pid, list);
            }
        }

        private string[] GetColumnValues_G(int col)
        {
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
            // Multi_Dac 修正
            //Excel.Worksheet sheet = Globals.ThisWorkbook.Sheets["ミッション"];
            Excel.Worksheet sheet = Globals.ThisWorkbook.Sheets[MISSION_GRIP];
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
            if (sheet.UsedRange.Rows.Count > 1)
            {
                Excel.Range idRange = sheet.UsedRange.Columns[col];
                return ((System.Array)idRange.Cells.Value).OfType<object>().Select(o => o.ToString()).ToArray();
            }
            else // if only 1 element, 1 ctm, and 1 mission is selected, use the below code..
            {
                Microsoft.Office.Interop.Excel.Range cells = sheet.Cells;

                List<string> values = new List<string>();
                for (int rowIndex = 1; rowIndex <= sheet.UsedRange.Rows.Count; rowIndex++)
                {
                    if (cells[rowIndex, col].Value != null)
                        values.Add(cells[rowIndex, col].Value.ToString());
                }
                return values.ToArray();
            }
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End

        /// <summary>
        /// Excelの１列の値を取得する
        /// </summary>
        private string[] GetColumnValues(int col)
        {
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
            // Multi_Dac 修正
            //Excel.Worksheet sheet = Globals.ThisWorkbook.Sheets["ミッション"];
            List<string> values = new List<string>();
            try
            {
                Excel.Worksheet sheet = Globals.ThisWorkbook.Sheets[MISSION_M];
                // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
                if (sheet.UsedRange.Rows.Count > 1)
                {
                    Excel.Range idRange = sheet.UsedRange.Columns[col];
                    return ((System.Array)idRange.Cells.Value).OfType<object>().Select(o => o.ToString()).ToArray();
                }
                else // if only 1 element, 1 ctm, and 1 mission is selected, use the below code..
                {
                    Microsoft.Office.Interop.Excel.Range cells = sheet.Cells;

            
                    for (int rowIndex = 1; rowIndex <= sheet.UsedRange.Rows.Count; rowIndex++)
                    {
                        if (cells[rowIndex, col].Value != null)
                            values.Add(cells[rowIndex, col].Value.ToString());
                    }
                    return values.ToArray();
                }
            }
            catch (Exception e)
            {
                Globals.ThisWorkbook.workbookLog.OutPut("GetColumnValues", "Column: "+col.ToString() + e.Message+Environment.NewLine+e.StackTrace, ClsMissionLog.LOG_LEVEL.DEBUG);
                return values.ToArray();
            }
        }

        /// <summary>
        /// 親子関係表すオブジェクトを作成する
        /// </summary>
        private void BuildDictionary(string[] pids, string[] ids, string[] displayNames, Dictionary<string, Dictionary<string, ClsAttribObject>> dict)
        {
            for (int i = 0; i < ids.Length; i++)
            {
                //親ID
                string pid = (string)pids.GetValue(i);
                //ID
                string id = (string)ids.GetValue(i);
                //表示名
                string displayName = (string)displayNames.GetValue(i);

                ClsAttribObject obj = new ClsAttribObject(pid, id, displayName);
                Dictionary<string, ClsAttribObject> list;

                if (dict.ContainsKey(pid))
                {
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                    // Multi_Dac 修正
                    if (!dict[pid].ContainsKey(id))
                    {
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
                        dict[pid].Add(id, obj);
                    }
                    //親IDは既に存在する場合はオブジェクトリストに追加する
                }
                else
                {
                    //親IDは存在しない場合はオブジェクトリストを作成する
                    list = new Dictionary<string, ClsAttribObject>();
                    list.Add(id, obj);
                    dict.Add(pid, list);
                }
            }
        }
        private void BuildDictionary(string[] pids1, string[] pids2, string[] ids, string[] displayNames, Dictionary<string, Dictionary<string, ClsAttribObject>> dict)
        {
            for (int i = 0; i < ids.Length; i++)
            {
                //親ID
                string pid = (string)pids1.GetValue(i) + (string)pids2.GetValue(i);
                //ID
                string id = (string)ids.GetValue(i);
                //表示名
                string displayName = (string)displayNames.GetValue(i);

                ClsAttribObject obj = new ClsAttribObject(pid, id, displayName);
                Dictionary<string, ClsAttribObject> list;

                if (dict.ContainsKey(pid))
                {
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                    // Multi_Dac 修正
                    if (!dict[pid].ContainsKey(id))
                    {
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
                        //親IDは既に存在する場合はオブジェクトリストに追加する
                        dict[pid].Add(id, obj);
                    }
                }
                else
                {
                    //親IDは存在しない場合はオブジェクトリストを作成する
                    list = new Dictionary<string, ClsAttribObject>();
                    list.Add(id, obj);
                    dict.Add(pid, list);
                }
            }
        }

        /// <summary>
        /// AISのアプリケーション構成ファイルを読み込む
        /// </summary>
        private void LoadConfiguration()
        {
            try
            {
                /*
                LoadCmsUiConf();
                m_MissionHostName = Config.CmsHost;
                m_MissionPortNo = Config.CmsPort;
                */
                string[] data = getHostAndPort();
                m_MissionHostName = data[0];
                m_MissionPortNo = data[1];

                // MessageBox.Show(string.Format("{0}:{1}", m_MissionHostName, m_MissionPortNo));
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("LoadConfiguration", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// Resource/Ais.conf 読み込み
        /// </summary>
        public const string BaseUrl = "http://{0}:{1}/cms/rest/";
        public static Conf Config { get; set; }
        public static readonly List<string> CatalogLangList = new List<string> { "ja", "en" };
        public static readonly List<string> CatalogLangListForDisplay = new List<string> { "Japanese", "English" };

        public static void LoadCmsUiConf()
        {
            string confPath = Path.GetDirectoryName(Globals.ThisWorkbook.Path) + @"\Ais.conf";
            using (StreamReader sr = new StreamReader(confPath))
            {
                Config = JsonConvert.DeserializeObject<Conf>(sr.ReadToEnd());
            }

            if (!CatalogLangList.Contains(Config.DefaultCatalogLang))
            {
                Config.DefaultCatalogLang = "ja";
            }
        }

        public class Conf
        {
            public string CmsHost;
            public string CmsPort = "60000";
            public string DefaultCatalogLang;
        }

        private string[] getHostAndPort()
        {
            string[] data = new string[2];
            data[0] = "localhost";
            data[1] = "60000";
            var cells = Globals.ThisWorkbook.Sheets["param"].Cells;
            var rangeA = getFindRange_Interop(cells, "サーバIPアドレス");
            if (rangeA != null)
            {
                data[0] = rangeA[1, 2].Value.ToString();
            }

            var rangeB = getFindRange_Interop(cells, "サーバポート番号");
            if (rangeB != null)
            {
                data[1] = rangeB[1, 2].Value.ToString();
            }

            return data;
        }

        /// <summary>
        /// Range取込
        /// </summary>
        /// <param name="srchRange"></param>
        /// <param name="strWhat"></param>
        /// <returns></returns>
        private Excel.Range getFindRange_Interop(Microsoft.Office.Interop.Excel.Range srchRange, String strWhat)
        {
            return srchRange.Find(strWhat,
                                System.Type.Missing,
                                Excel.XlFindLookIn.xlValues,
                                Excel.XlLookAt.xlWhole,
                                Excel.XlSearchOrder.xlByRows,
                                Excel.XlSearchDirection.xlNext,
                                false,
                                System.Type.Missing, System.Type.Missing);
        }


        /// <summary>
        /// 【イベント】プルダウン変更処理
        /// </summary>
        /// <param name="Target"></param>
        private void Sheet1_Change(Excel.Range Target)
        {
            // Wang Issue R2-143 20190329 Start
            if (Target.Value == null)
            {
                return;
            }

            string value = Target.Value.ToString();
            if (ClsSubDACUtil.isSubDACRefere(value))
            {
                value = BriefSelfDacRefer(value);
                if (!string.IsNullOrEmpty(value))
                {
                    Target.Value = value;
                }
            }
            // Wang Issue R2-143 20190329 End
        }

        // Wang Issue R2-143 20190329 Start
        private string BriefSelfDacRefer(string value)
        {
            string subDACFormula = value.Substring(0, value.Length - 1).Replace("SubDAC(", "");
            string[] arrayFormula = arrayFormula = subDACFormula.Split(':');
            if (arrayFormula[0].Equals(System.IO.Path.GetFileNameWithoutExtension(Globals.ThisWorkbook.Name)))
            {
                arrayFormula[0] = "";
                return "SelfDAC(" + string.Join(":", arrayFormula) + ")";
            }
            return null;
        }
        // Wang Issue R2-143 20190329 End

        private bool HasMissionSheet()
        {
            foreach (Excel.Worksheet sheet in Globals.ThisWorkbook.Worksheets)
            {
                if(sheet.Name.Equals(MISSION_M))
                {
                    return true;
                }
            }
            return false;
        }


        /// <summary>
        /// 【イベント】プルダウン変更処理
        /// </summary>
        /// <param name="Target"></param>
        private void selectionChange(Excel.Range Target)
        {
            // 選択されたセルの列を確認（日付列か）
            if (Target.Column == Sheet1.baseCalCell.Column && Target.Value is DateTime)
            {
                DateTime dateTime;
                string strDateTime;
                Globals.ThisWorkbook.workbookLog.OutPut("selectionChanged:", "["+Target.Column + "," + Target.Row + "]", ClsMissionLog.LOG_LEVEL.DEBUG);
                try
                {
                    dateTime = Target.Value;
                    CultureInfo originalCulture = Thread.CurrentThread.CurrentCulture;
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("ja-JP");
                    strDateTime = dateTime.ToShortDateString();
                    Thread.CurrentThread.CurrentUICulture = originalCulture;
                    Globals.ThisWorkbook.workbookLog.OutPut("selectionChanged:", strDateTime, ClsMissionLog.LOG_LEVEL.DEBUG);
                }
                catch
                {
                    strDateTime = "";

                }

                if (Globals.ThisWorkbook.ActionsPane.Controls.Contains(Globals.ThisWorkbook.m_subDACActionPanel))
                {
                    if (!string.IsNullOrEmpty(strDateTime))
                    {
                        Globals.ThisWorkbook.m_subDACActionPanel.getLblDate().Text = strDateTime;
                        Globals.ThisWorkbook.workbookLog.OutPut("selectionChanged:", Globals.ThisWorkbook.m_subDACActionPanel.getLblDate().Text, ClsMissionLog.LOG_LEVEL.DEBUG);
                    }
                }

            }
            else
            {

            }
        }






        /// <summary>
        /// ミッション実行（オフライン）ボタン押下時処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDefault"></param>
        private void button_Offline_Click(Office.CommandBarButton ctrl, ref bool CancelDefault)
        {
            Excel.Range rng = null;

            ClsMissionLog missionLog;


            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

            System.Diagnostics.Process logViewerProcess = null;

            DateTime now = DateTime.Now;
            string logFile ="";
            if (Globals.ThisWorkbook.clsDACConf.MonitorLevel.Equals(ClsMissionLog.LOG_MONITOR_TYPE.DETAIL))
            {
                logFile = string.Format("offlinemission_log_{0}.txt", now.ToString().Replace("/", "").Replace(":", "").Replace(" ", ""));
            }
            else
            {
                logFile ="message.txt";
            }

            missionLog = new ClsMissionLog("Offline", Globals.ThisWorkbook.clsSubDACControler.BasePath, logFile, Globals.ThisWorkbook.clsDACConf.MonitorLevel,ClsMissionLog.LOG_LEVEL.DEBUG);

            try
            {
                // 時間計測開始
                sw.Start();


                /*
                 * 第1階層用の処理
                 */
                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                //if (ctrl.Caption != "ミッション実行(オフライン)")
                if (ctrl.Caption != Properties.Resources.DAC_MENU_EXECUTE_MISSION_OFFLINE)
                // Wang Issue NO.687 2018/05/18 End
                {
                    return;
                }

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")" ,
                    "ミッション実行(オフライン)の計算を開始します。", ClsMissionLog.LOG_LEVEL.INFO);

                m_activeRange = Application.ActiveCell; //現在のアクティブセル位置

                // 標準時間からの経過ミリ秒（ミッション取得用）
//                string startEpoMilliseconds = "";  
                string endEpoMilliseconds = "";

                if (!ClsExcelUtil.isEmpty(m_activeRange) && m_activeRange.Value is DateTime)
                {

                    // 時刻のチェックは行わない

                }
                else
                {
                    MessageBox.Show(
                        "演算行/定数の日付が入力されているセルから\n" + "ミッションを実行してください");
                        // + "(" + m_activeRange.Row + "行・" + Sheet1.baseCalCell.Column + "列目のデータに日付が設定されていません"
                        
                    missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                        "演算行/定数の日付が入力されていないセルからミッション実行したため、処理を終了します", ClsMissionLog.LOG_LEVEL.WARN);

                    return;
                }


                DialogResult dialogResult = MessageBox.Show("ミッション実行(オフライン)により、全てのDACファイルの内容が更新されます。\n" +
                                 "実行する場合は、全てのDACファイルを閉じてから、OKボタンを押してください。\n" +
                                 "実行しない場合は、キャンセルボタンを押してください。",
                                "警告",
                                MessageBoxButtons.OKCancel,
                                MessageBoxIcon.Warning);

                if (dialogResult == DialogResult.Cancel)
                {
                    return;
                }

                if (Globals.ThisWorkbook.clsDACConf.MonitorLevel != ClsMissionLog.LOG_MONITOR_TYPE.NONE)
                {
                    try
                    {
                        // ログモニターの起動
                        /*
                        logViewerProcess =
                            System.Diagnostics.Process.Start(Path.Combine(
                                Globals.ThisWorkbook.clsDACConf.AisExeDir, "AIStool_DAC_LOG_VIEWER.exe"),
                                    "\"" + (int)(Globals.ThisWorkbook.clsDACConf.MonitorLevel) + "\"" + " "
                                + "\"" + missionLog.LogFullName + "\"");
                        */
                    }
                    catch
                    {
                        MessageBox.Show("DAC動作確認モニターの起動に失敗しました。\n" +
                                        "お使いのウィルス監視ソフトの設定を変更するか、\n" +
                                        "DAC動作確認モニタを使用しない設定に変更する必要があります。\n" +
                                        "OKボタンをクリックしていただくと、ミッション処理は継続して実行されます。",
                                        "警告",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Warning);
                    }
                }


                /* SubDAC作成処理
                 *  １．SubDAC作成対象の取得
                 */
                string message = "";
                string date = Globals.ThisWorkbook.m_subDACActionPanel.getLblDate().Text;
                string createFile = "";
                bool bRtn = false;


                LinkedList<FoaCoreCom.dac.model.DacManifest.ManifestFileInfo> successFileList = new LinkedList<FoaCoreCom.dac.model.DacManifest.ManifestFileInfo>();

                /*
                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "SubDACファイル（" + Globals.ThisWorkbook.clsSubDACFileList.Count + "ファイル）の処理を開始します。",
                    ClsMissionLog.LOG_LEVEL.INFO);
                */

                Array clsSubDACFileArray =  Globals.ThisWorkbook.clsSubDACControler.manifest.fileList.GetRange(1,Globals.ThisWorkbook.clsSubDACControler.manifest.fileList.Count-1).ToArray();
                Array.Sort(clsSubDACFileArray);


                // 再計算停止
                this.Application.Calculation = Excel.XlCalculation.xlCalculationManual;
                this.Application.ScreenUpdating = false;
                this.Application.DisplayAlerts = false;

                //３．ファイルリスト用パネルにファイルの一覧を表示する。
                foreach (FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subDACFile in clsSubDACFileArray)
                {
                    if (Globals.ThisWorkbook.clsDACConf.OfflineMode.Equals("0"))
                    {
                        missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                            "オフライン実行モードが0(ファイルコピーをしない)のため、ファイルコピーを実行しない。",
                            ClsMissionLog.LOG_LEVEL.INFO);

                        continue;
                    }


                    missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                        "SubDACファイル（" + subDACFile.displayname + "）の処理を開始します。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                    string[] arrayDate = Globals.ThisWorkbook.m_subDACActionPanel.getLblDate().Text.Split('/');
                    int nowDay = Int32.Parse(arrayDate[2]);
                    // すでに削除されているため、処理対象外になる。
                    /*
                    if (subDACFile.deleteDate != 0 && nowDay >= subDACFile.deleteDate)
                    {
                        missionLog.OutPut("第1階層SubDAC(" + subDACFile.Name + ")",
                            "SubDACファイル（" + subDACFile.Name + "）は" + subDACFile.deleteDate + "日に削除されたため、" + nowDay + "日には処理されません。",
                            ClsMissionLog.LOG_LEVEL.INFO);

                        continue;
                    }
                    */


                    // ファイルのフルパスを取得（存在しない場合はコピー）
                    createFile = ClsFileUtil.replicationFile(date, subDACFile);


                    if (String.IsNullOrEmpty(createFile))
                    {
                        MessageBox.Show("このSubDACファイル（" + subDACFile.displayname + "）は１ファイルもドラッグ＆ドロップされていないため、ミッション実行を終了します。\n" +
                                        "管理対象から削除後、再度SubDACファイルをドラッグ＆ドロップしてからミッション実行を行ってください。",
                                        "エラー",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);

                        missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                            "このSubDACファイル（" + subDACFile.displayname + "）は１ファイルもドラッグ＆ドロップされていないため、ミッション実行を終了します。",
                            ClsMissionLog.LOG_LEVEL.ERROR);

                        return;
                    }


                    if ("コメント".Equals(subDACFile.template))
                    {
                        missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                            "SubDACファイル（" + subDACFile.displayname + "）はコメントのため計算対象外になります",
                            ClsMissionLog.LOG_LEVEL.INFO);

                    }
                    /*
                    else if ("DAC".Equals(subDACFile.template))
                    {

                        missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                            "SubDACファイル（" + subDACFile.displayname + "）はDACファイルのため、2階層目の計算を開始します。",
                            ClsMissionLog.LOG_LEVEL.INFO);


                        ClsCalUtil.ClsBusinessHoursDto clsBusinessHoursDto = ClsCalUtil.setBusinessHours(Globals.ThisWorkbook.Sheets, m_activeRange.Value);

                        // SubDACのDAC帳票の処理を実行する。
                        ClsSecondDAC clsSecondDAC = new ClsSecondDAC();
                        clsSecondDAC.doOffline(m_activeRange.Value, clsBusinessHoursDto, subDACFile, Globals.ThisWorkbook.clsDACConf, missionLog);


                        missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                            "SubDACファイル（" + subDACFile.displayname + "）の2階層目の計算が終了しました。",
                            ClsMissionLog.LOG_LEVEL.INFO);

                    }
                    */
                    else
                    {

                        if (String.IsNullOrEmpty(Globals.ThisWorkbook.clsDACConf.OfflineMacro))
                        {
                            missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                "マクロの指定がないため、計算は実行しない",
                                ClsMissionLog.LOG_LEVEL.INFO);

                        }
                        else
                        {

                            missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                "マクロの指定（" +  Globals.ThisWorkbook.clsDACConf.OfflineMacro + "）があるため、計算を実行します。",
                                ClsMissionLog.LOG_LEVEL.INFO);


                            bRtn = ClsExcelUtil.execMacro(createFile, Globals.ThisWorkbook.clsDACConf.OfflineMacro);
                            if (bRtn)
                            {
                                int refreshCount = ClsExcelUtil.refreshPivotInBook(createFile);

                                missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                    "マクロの指定（" + Globals.ThisWorkbook.clsDACConf.OfflineMacro + "）の計算が成功しました。",
                                    ClsMissionLog.LOG_LEVEL.INFO);
                            }
                            else
                            {
                                missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                    "マクロの指定（" + Globals.ThisWorkbook.clsDACConf.OfflineMacro + "）の計算に失敗しました。",
                                    ClsMissionLog.LOG_LEVEL.INFO);

                            }

                        }
                    }

                    if (bRtn)
                    {

                        // 期間計算が成功したファイルを別リストで保存する
                        message = message + subDACFile.displayname + "は期間変更を実施しました。\n";
                        successFileList.AddLast(subDACFile);


                        missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                            "このSubDACファイル（" + subDACFile.displayname + "）の計算が終了しました。。",
                            ClsMissionLog.LOG_LEVEL.INFO);

                    }


                }


                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                        "DACメインシートへの結果の出力を開始します。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                //メインシートにデータを挿入する
                /*
                CalcurateSheet(isAfterFlag);
                */

                bool isAfterFlag = ClsDateUtil.isNowAfter(endEpoMilliseconds);
                //メインシートにデータを挿入する
                ClsCalUtil.CalcurateSheet(
                    m_activeRange.Value.ToString("yyyy/MM/dd"),
                    Globals.ThisWorkbook.clsSubDACControler.DACName,
                    Globals.ThisWorkbook.Sheets,
                    //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 start
                    //Globals.ThisWorkbook.Sheets["ＤＡＣ帳票_フォーマットイメージ"],
                    Globals.ThisWorkbook.Sheets[DAC_SHEET],
                    //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 end
                    m_activeRange.Row, isAfterFlag);

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "DACメインシートへの結果の出力が終了しました。",
                    ClsMissionLog.LOG_LEVEL.INFO);


                // 更新処理
                this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                this.Application.Calculate();
                this.Application.ScreenUpdating = true;

                this.Activate();
                this.Application.DisplayAlerts = true;

                // Cleanup, related with AIS Fix no.76
                //int formulaHeader = findFormulaRow();
                //CleanupColumnData(formulaHeader);

                // ファイルが作成されたので、再表示する
                Globals.ThisWorkbook.m_subDACActionPanel.printSubDAC();

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "ミッション全体の実行時間は（" + sw.Elapsed + "）になります。", ClsMissionLog.LOG_LEVEL.INFO);

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "ミッションの計算が終了しました。", ClsMissionLog.LOG_LEVEL.INFO);


                sw.Stop();

                if (Globals.ThisWorkbook.clsDACConf.MonitorLevel.Equals(ClsMissionLog.LOG_MONITOR_TYPE.SIMPLE))
                {
                    if (logViewerProcess != null && !logViewerProcess.HasExited)
                        logViewerProcess.Kill();
                }

                MessageBox.Show("ミッション取得処理(オフライン)が完了しました。");


            }
            //Mi失敗時
            catch (Exception ex)
            {

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")で例外発生により処理終了",
                    ex.Message + ex.StackTrace, ClsMissionLog.LOG_LEVEL.INFO);


                MessageBox.Show(ex.Message);
                m_writeLog.OutPut("button_Click", ex.Message + ex.StackTrace);
                this.Application.DisplayAlerts = true;

                // 更新処理
                this.Application.ScreenUpdating = true;
                this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                this.Application.Calculate();

                //Mi登録状態表示処理
                Excel.Range cell1;
                for (int j = 4; j < 15000; j += 3) // 3 ->セルの結合数
                {
                    cell1 = this.Cells[6, j];
                    //cell2 = this.Cells[6, j + 3];
                    rng = this.Range[cell1, cell1];
                    if (!String.IsNullOrEmpty(rng.Value))
                    {
                        if (rng.Value == "-") continue;
                        this.Range[this.Cells[7, j], this.Cells[7, j]].Value = "処理中断";
                    }
                    else
                    {
                        Excel.Range nextCell1 = this.Cells[6, j + 3]; // 次のセル
                        string nextMi = (string)this.Range[nextCell1, nextCell1].Value;

                        if (String.IsNullOrEmpty(nextMi))
                        {
                            break;
                        }
                    }
                }

                MessageBox.Show("ミッション取得処理(オフライン)が完了しました。");
            }
            finally
            {
                if (Globals.ThisWorkbook.clsDACConf.MonitorLevel.Equals(ClsMissionLog.LOG_MONITOR_TYPE.SIMPLE))
                {
                    if (logViewerProcess != null && !logViewerProcess.HasExited)
                    logViewerProcess.Kill();
                }
            }
        }

        /// <summary>
        /// ミッション実行ボタン押下時処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDefault"></param>
        private void button_Click(Office.CommandBarButton ctrl, ref bool CancelDefault)
        {
            Excel.Range rng = null;

            ClsMissionLog missionLog;


            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();

            System.Diagnostics.Process logViewerProcess = null;

            DateTime now = DateTime.Now;
            string logFile ="";
            if (Globals.ThisWorkbook.clsDACConf.MonitorLevel.Equals(ClsMissionLog.LOG_MONITOR_TYPE.DETAIL))
            {
                logFile = string.Format("mission_log_{0}.txt", now.ToString().Replace("/", "").Replace(":", "").Replace(" ", ""));
            }
            else
            {
                logFile ="message.txt";
            }
            logFile = string.Format("mission_log_{0}.txt", now.ToString().Replace("/", "").Replace(":", "").Replace(" ", ""));
            m_writeLog.OutPut("button_Click", (Globals.ThisWorkbook.clsDACConf == null ? "null" : Globals.ThisWorkbook.clsDACConf.LogDir));
            missionLog = new ClsMissionLog("Mission", Globals.ThisWorkbook.clsDACConf.LogDir, logFile, ClsMissionLog.LOG_MONITOR_TYPE.DETAIL, ClsMissionLog.LOG_LEVEL.DEBUG);
            missionLog.OutPut("button_Click", "start", ClsMissionLog.LOG_LEVEL.DEBUG);
            try
            {
                m_writeLog.OutPut("button_Click", "Ctrl: "+(ctrl == null ? "null" : ctrl.Caption));
                // 時間計測開始
                sw.Start();


                /*
                 * 第1階層用の処理
                 */
                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                //if (ctrl.Caption != "ミッション実行")
                if (ctrl.Caption != Properties.Resources.DAC_MENU_EXECUTE_MISSION)
                // Wang Issue NO.687 2018/05/18 End
                {
                    return;
                }

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")" , 
                    "ミッションの計算を開始します。",ClsMissionLog.LOG_LEVEL.INFO);

                m_activeRange = Application.ActiveCell; //現在のアクティブセル位置

                // 標準時間からの経過ミリ秒（ミッション取得用）
                string startEpoMilliseconds = "";  
                string endEpoMilliseconds = "";


                // 日付（SubDAC計算用）
                DateTime startDateTime;
                DateTime endDateTime;
                m_writeLog.OutPut("button_Click", "日付start");
                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "開始・終了時間の取得処理を開始します。", ClsMissionLog.LOG_LEVEL.INFO);

                if (!ClsExcelUtil.isEmpty(m_activeRange) && m_activeRange.Value is DateTime)
                {

                    DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                    startDateTime = this.Cells[m_activeRange.Row, Sheet1.baseCalCell.Column].Value;


                    //開始時刻作成 **********************************
                    Excel.Range startTimeCell = null;
                    startTimeCell = this.Cells[Sheet1.baseTimeCell.Row,Sheet1.baseTimeCell.Column+1];
                    TimeSpan startTimeSpan = TimeSpan.MaxValue;
                    if (startTimeCell.Value == null || !(startTimeCell.Value is double))
                    {
                        MessageBox.Show("開始時刻を[00:00]形式で入力してください。");
                        missionLog.OutPut("第1階層DAC", "開始時刻の形式エラーのため処理を終了します", ClsMissionLog.LOG_LEVEL.WARN);

                        return;
                    }
                    else
                    {
                        Globals.ThisWorkbook.Sheets["業務時間"].Range["A1"].Value = startTimeCell.Value;

                        startDateTime = startDateTime.AddDays(startTimeCell.Value);

                        startTimeSpan = startDateTime - epoch;
                    }

                    //終了時刻作成 **********************************
                    Excel.Range endTimeCell = null;
                    endTimeCell = this.Cells[Sheet1.baseTimeCell.Row + 1, Sheet1.baseTimeCell.Column + 1]; ;
                    TimeSpan endTimeSpan = TimeSpan.MaxValue;
                    if (endTimeCell.Value == null || !(endTimeCell.Value is double))
                    {
                        MessageBox.Show("終了時刻を[00:00]形式で入力してください。");
                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                    "終了時刻の形式エラーのため処理を終了します", ClsMissionLog.LOG_LEVEL.WARN);
                        return;
                    }
                    else
                    {
                        Globals.ThisWorkbook.Sheets["業務時間"].Range["A2"].Value = endTimeCell.Value;
                        // 終了時刻が開始時刻より前の場合次の日の時刻にする
                        if (startTimeCell.Value >= endTimeCell.Value)
                        {
                            endDateTime = startDateTime.AddDays(1);
                        }
                        else
                        {
                            endDateTime = startDateTime;
                        }
                        endDateTime = endDateTime.AddDays(-startTimeCell.Value).AddDays(endTimeCell.Value);
                        //endDateTime = DateTime.Parse(endDateTime.ToString("yyyy/MM/dd HH:mm") + ":59.999");
                        //endDateTime = DateTime.Parse(endDateTime.ToString("yyyy/MM/dd HH:mm") + ":59.000");
                        endTimeSpan = (endDateTime - epoch);
                    }

                    startEpoMilliseconds = Math.Floor(startTimeSpan.TotalMilliseconds).ToString();
                    endEpoMilliseconds = Math.Floor(endTimeSpan.TotalMilliseconds).ToString();

                    missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                        "開始時間=" + startDateTime.ToString("yyyy/MM/dd HH:mm") + ",終了時間="+endDateTime.ToString("yyyy/MM/dd HH:mm"),
                         ClsMissionLog.LOG_LEVEL.DEBUG);

                }
                else
                {
                    MessageBox.Show(
                        "演算行/定数の日付が入力されているセルから\n" + "ミッションを実行してください");
                        // + "(" + m_activeRange.Row + "行・" + Sheet1.baseCalCell.Column + "列目のデータに日付が設定されていません"
                        
                    missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                        "演算行/定数の日付が入力されていないセルからミッション実行したため、処理を終了します", ClsMissionLog.LOG_LEVEL.WARN);

                    return;
                }
                m_writeLog.OutPut("button_Click", "日付end");
                // No. 826 Modified 2018/08/24 by Yakiyama. Start //階層DACではない、通常のDACミッションでは警告を表示しない。
                string workBookFileName = System.IO.Path.GetFileName(Globals.ThisWorkbook.FullName);
                if (workBookFileName.StartsWith("DAC_") == false)
                {
                    DialogResult dialogResult = MessageBox.Show("ミッション実行により、DACに含まれる全てのファイルが更新されます。\n" +
                                     "実行する場合は、DACに含まれる全てのファイルを閉じてから、OKボタンを押してください。\n" +
                                     "実行しない場合は、キャンセルボタンを押してください。",
                                    "警告",
                                    MessageBoxButtons.OKCancel,
                                    MessageBoxIcon.Warning);

                    if (dialogResult == DialogResult.Cancel)
                    {
                        return;
                    }
                }
                // No. 826 Modified 2018/08/24 by Yakiyama. End //階層DACではない、通常のDACミッションでは警告を表示しない。

                if (Globals.ThisWorkbook.clsDACConf.MonitorLevel != ClsMissionLog.LOG_MONITOR_TYPE.NONE)
                {
                    try
                    {
                        // ログモニターの起動
                        /*
                         logViewerProcess =
                            System.Diagnostics.Process.Start(Path.Combine(
                                Globals.ThisWorkbook.clsDACConf.AisExeDir, "AIStool_DAC_LOG_VIEWER.exe"),
                                    "\"" + (int)(Globals.ThisWorkbook.clsDACConf.MonitorLevel) + "\"" + " "
                                + "\"" + missionLog.LogFullName + "\"");
                        */
                    }
                    catch
                    {
                        MessageBox.Show("DAC動作確認モニターの起動に失敗しました。\n" +
                                        "お使いのウィルス監視ソフトの設定を変更するか、\n" +
                                        "DAC動作確認モニタを使用しない設定に変更する必要があります。\n" +
                                        "OKボタンをクリックしていただくと、ミッション処理は継続して実行されます。",
                                        "警告",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Warning);
                    }
                }



                // SubDAC作成、CMS問い合わせ前に時間を比較する
                bool isAfterFlag = ClsDateUtil.isNowAfter(endEpoMilliseconds);
                if(!isAfterFlag){
                    missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                        "業務時間より前にミッションが実行されています。", ClsMissionLog.LOG_LEVEL.WARN);
                }

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "開始・終了時間の取得処理が終了しました。", ClsMissionLog.LOG_LEVEL.INFO);

                /* SubDAC作成処理
                 *  １．SubDAC作成対象の取得
                 */
                string message = "";
                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "Before getLblDate", ClsMissionLog.LOG_LEVEL.INFO);

                string date = Globals.ThisWorkbook.m_subDACActionPanel.getLblDate().Text;
                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "After getLblDate", ClsMissionLog.LOG_LEVEL.INFO);
                string createFile = "";
                bool bRtn = false;


                LinkedList<FoaCoreCom.dac.model.DacManifest.ManifestFileInfo> successFileList = new LinkedList<FoaCoreCom.dac.model.DacManifest.ManifestFileInfo>();

                /*
                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "SubDACファイル（" + Globals.ThisWorkbook.clsSubDACFileList.Count + "ファイル）の処理を開始します。",
                    ClsMissionLog.LOG_LEVEL.INFO);
                */
                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "manifest: " + (Globals.ThisWorkbook.clsSubDACControler.manifest == null ? "null" : Globals.ThisWorkbook.clsSubDACControler.manifest.fileList.Count.ToString()), ClsMissionLog.LOG_LEVEL.INFO);

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "FileCount: "+Globals.ThisWorkbook.clsSubDACControler.manifest.fileList.Count, ClsMissionLog.LOG_LEVEL.INFO);
                if (Globals.ThisWorkbook.clsSubDACControler.manifest.fileList.Count > 1)                
                {
                    Array clsSubDACFileArray = Globals.ThisWorkbook.clsSubDACControler.manifest.fileList.GetRange(1, Globals.ThisWorkbook.clsSubDACControler.manifest.fileList.Count - 1).ToArray();
                    Array.Sort(clsSubDACFileArray);
                    m_writeLog.OutPut("button_Click", "SubDAC start");
                    // 再計算停止
                    this.Application.Calculation = Excel.XlCalculation.xlCalculationManual;
                    this.Application.ScreenUpdating = false;
                    this.Application.DisplayAlerts = false;

                    //３．ファイルリスト用パネルにファイルの一覧を表示する。
                    foreach (FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subDACFile in clsSubDACFileArray)
                    {
                        /*
                        if (!subDACFile.template.Equals(Properties.Resources.TEXT_STD_MULTIDAC) &&
                            !subDACFile.template.Equals(Properties.Resources.TEXT_STD_SPREADSHEET) &&
                            !subDACFile.template.Equals(Properties.Resources.TEXT_STD_DAC))
                        */
                        if (!subDACFile.template.Equals(Properties.Resources.TEXT_STD_SPREADSHEET))
                        {
                            continue;
                        }
                        missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                            "SubDACファイル（" + subDACFile.displayname + "）の処理を開始します。",
                            ClsMissionLog.LOG_LEVEL.INFO);

                        string[] arrayDate = Globals.ThisWorkbook.m_subDACActionPanel.getLblDate().Text.Split('/');
                        int nowDay = Int32.Parse(arrayDate[2]);
                        // すでに削除されているため、処理対象外になる。
                        /*
                        if (subDACFile.deleteDate != 0 && nowDay >= subDACFile.deleteDate)
                        {
                            missionLog.OutPut("第1階層SubDAC(" + subDACFile.Name + ")",
                                "SubDACファイル（" + subDACFile.Name + "）は" + subDACFile.deleteDate + "日に削除されたため、" + nowDay + "日には処理されません。",
                                ClsMissionLog.LOG_LEVEL.INFO);

                            continue;
                        }
                        */



                        // ファイルのフルパスを取得（存在しない場合はコピー）
                        createFile = ClsFileUtil.replicationFile(date, subDACFile);


                        if (String.IsNullOrEmpty(createFile))
                        {
                            MessageBox.Show("このSubDACファイル（" + subDACFile.displayname + "）は１ファイルもドラッグ＆ドロップされていないため、ミッション実行を終了します。\n" +
                                            "管理対象から削除後、再度SubDACファイルをドラッグ＆ドロップしてからミッション実行を行ってください。",
                                            "エラー",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Error);

                            missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                "このSubDACファイル（" + subDACFile.displayname + "）は１ファイルもドラッグ＆ドロップされていないため、ミッション実行を終了します。",
                                ClsMissionLog.LOG_LEVEL.ERROR);

                            return;
                        }


                        if ("コメント".Equals(subDACFile.template))
                        {
                            missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                "SubDACファイル（" + subDACFile.displayname + "）はコメントのため計算対象外になります",
                                ClsMissionLog.LOG_LEVEL.INFO);

                        }
                        /*
                        else if ("DAC".Equals(subDACFile.template) || subDACFile.template.Equals(Properties.Resources.TEXT_STD_MULTIDAC))
                        {

                            missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                "SubDACファイル（" + subDACFile.displayname + "）はDACファイルのため、2階層目の計算を開始します。",
                                ClsMissionLog.LOG_LEVEL.INFO);


                            ClsCalUtil.ClsBusinessHoursDto clsBusinessHoursDto = ClsCalUtil.setBusinessHours(Globals.ThisWorkbook.Sheets, m_activeRange.Value);

                            // SubDACのDAC帳票の処理を実行する。
                            ClsSecondDAC clsSecondDAC = new ClsSecondDAC();
                            clsSecondDAC.doMission(m_activeRange.Value, clsBusinessHoursDto, subDACFile, Globals.ThisWorkbook.clsDACConf, missionLog);


                            missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                "SubDACファイル（" + subDACFile.displayname + "）の2階層目の計算が終了しました。",
                                ClsMissionLog.LOG_LEVEL.INFO);

                        }
                        */
                        else
                        {

                            missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                "SubDACファイル（" + subDACFile.displayname + "）の計算を開始します。",
                                ClsMissionLog.LOG_LEVEL.INFO);


                            // 期間変更アドインがあるSubDACの場合のみ計算する
                            if (ClsExcelUtil.isAddin(createFile))
                            {
                                if (ClsFileUtil.checkFileOpen(createFile))
                                {
                                    try
                                    {
                                        using (var fs = File.OpenWrite(createFile))
                                        {

                                        };

                                    }
                                    catch
                                    {
                                        // 更新処理
                                        this.Application.DisplayAlerts = true;
                                        this.Application.ScreenUpdating = true;
                                        this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                                        this.Application.Calculate();
                                        return;
                                    }
                                }

                                // SubDACファイルの作成
                                bRtn = ClsSubDACUtil.createSubDACFile(
                                    createFile,
                                    date,
                                    startDateTime,
                                    endDateTime,
                                    missionLog
                                    );


                                // 致命的エラーの場合は処理を抜ける
                                if (!bRtn)
                                {
                                    MessageBox.Show("このSubDACファイル（" + subDACFile.displayname + "）のミッション実行で失敗したため、処理を中断します。\n" +
                                                    message,
                                                    "エラー",
                                                    MessageBoxButtons.OK,
                                                    MessageBoxIcon.Error);

                                    // 致命的なエラーの場合は、コピーしたファイルを削除する。
                                    ClsFileUtil.deleteOneFile(createFile);

                                    // ファイルが作成されたので、再表示する
                                    Globals.ThisWorkbook.m_subDACActionPanel.printSubDAC();

                                    missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                        "このSubDACファイル（" + subDACFile.displayname + "）の計算でエラーが発生したため、SubDACの計算を終了します。",
                                        ClsMissionLog.LOG_LEVEL.ERROR);

                                    MessageBox.Show("ミッション取得処理が完了しました。");

                                    return;

                                }
                            }
                            else
                            {

                                missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                    "このSubDACファイル（" + subDACFile.displayname + "）は計算の対象外のため、計算を実施しません。",
                                    ClsMissionLog.LOG_LEVEL.WARN);

                                message = message + subDACFile.displayname + "は期間変更の対象外です。\n";
                                bRtn = false;
                            }
                        }

                        if (bRtn)
                        {

                            // 期間計算が成功したファイルを別リストで保存する
                            message = message + subDACFile.displayname + "は期間変更を実施しました。\n";
                            successFileList.AddLast(subDACFile);


                            missionLog.OutPut("第1階層SubDAC(" + subDACFile.displayname + ")",
                                "このSubDACファイル（" + subDACFile.displayname + "）の計算が終了しました。。",
                                ClsMissionLog.LOG_LEVEL.INFO);

                        }


                    }
                }
                
                /*
                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "SubDACファイル（" + Globals.ThisWorkbook.clsSubDACFileList.Count + "ファイル）の処理が終了しました。",
                    ClsMissionLog.LOG_LEVEL.INFO);
                */
                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                        "DACシートの計算を開始します。",
                        ClsMissionLog.LOG_LEVEL.INFO);



                /*
                */
                // タイムアウトまでの時間をparamシートから読み込み
                var range = Globals.ThisWorkbook.Sheets["param"].Cells;
                var range2 = getFindRange_Interop(range, "タイムアウト秒数");
                int timeOutSeconds = 0;
                if (range2 != null)
                {
                    string value = range2[1, 2].Text;
                    if (!int.TryParse(value, out timeOutSeconds))
                    {
                        timeOutSeconds = 10;
                    }
                }
                int timeOutMiliSeconds = timeOutSeconds * 1000;

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                        "CTM問合せタイムアウトミリ秒数（" + timeOutMiliSeconds +"）を取得しました。",
                        ClsMissionLog.LOG_LEVEL.DEBUG);

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                        "CTM問合せを開始します。",
                        ClsMissionLog.LOG_LEVEL.DEBUG);

                /*
                  CTM結果が空にする必要がないためコメントアウト
                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                        "既存シートのクリアを開始します。",
                                        ClsMissionLog.LOG_LEVEL.INFO);
                
                // 既存シートのクリア
                this.ClearNotExistSheets(startEpoMilliseconds, endEpoMilliseconds, timeOutMiliSeconds);
                */
                // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                // Multi_Dac 修正
                bool IsGripMission = false;
                Dictionary<string, ClsAttribObject> m_missions_tmp = new Dictionary<string, ClsAttribObject>();

                ////AIS_DAC No.80 sunyi 20181217 start
                ////実行する時、最新のミッションのDataSheetを作るため、旧DataSheetを削除する
                //foreach (Excel.Worksheet worksheet in Globals.ThisWorkbook.Sheets)
                //{
                //    if (worksheet.Name.Contains(varSheetName))
                //    {
                //        // Wang Issue reference to value of cell 20190228 Start
                //        //worksheet.Delete();
                //        worksheet.UsedRange.Clear();
                //        // Wang Issue reference to value of cell 20190228 End
                //    }
                //}
                ////AIS_DAC No.80 sunyi 20181217 end

                // Wang New dac flow 20190308 Start
                // Wang Issue reference to value of cell 20190228 Start
                int datasheetIndex = 1;
                // Wang Issue reference to value of cell 20190228 End
                // Wang New dac flow 20190308 End

                // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
                // ミッション数分だけループ
                foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions)
                {
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                    // Multi_Dac 修正
                    if (mission.Key.Contains("_Route-"))
                    {
                        IsGripMission = true;
                        m_missions_tmp.Add(mission.Key, mission.Value);
                        continue;
                    }

                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
                    //FOR COM高速化 lm  2018/06/21 Modify Start
                    //string jsonData;
                    //FOR COM高速化 lm  2018/06/21 Modify End
                    string missionId = mission.Key;

                    missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                            "CMSからミッションID(missionId)の情報の取得を開始します。",
                                            ClsMissionLog.LOG_LEVEL.INFO);

                    missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                            "useProxy=" + Globals.ThisWorkbook.clsDACConf.UseProxy + ",ProxyUri=" + Globals.ThisWorkbook.clsDACConf.UseProxy,
                                                ClsMissionLog.LOG_LEVEL.INFO);

                    // JSONDATAの取得

                    //FOR COM高速化 lm  2018/06/21 Modify Start
                    //jsonData = ClsJsonData.getString(Globals.ThisWorkbook.clsDACConf.UseProxy, Globals.ThisWorkbook.clsDACConf.ProxyURI, missionId, startEpoMilliseconds, endEpoMilliseconds, m_writeLog, timeOutMiliSeconds);
                  
                    string uri = "";
                    string hostName = Globals.Sheet1.m_MissionHostName;
                    string portNo = Globals.Sheet1.m_MissionPortNo;
                    string limit = "0";
                    string lang = "ja";
                    string msg = "";
                    string zipUrl = "";
                    string unZipUrl = "";

                    uri = String.Format("http://{0}:{1}/cms/rest/mib/mission/pmzip?id={2}&start={3}&end={4}&limit={5}&lang={6}&noBulky=true",
                               hostName, portNo, missionId, startEpoMilliseconds, endEpoMilliseconds, limit, lang);

                    //情報を格納するZIPファイルパスを取得する
                    CtmDataRetriever ctmDataRetriever = new FoaCoreCom.ais.retriever.CtmDataRetriever();
                    zipUrl = ctmDataRetriever.GetProgramMissionZipFileAsync(uri, Globals.ThisWorkbook.clsDACConf.ProxyURI, out msg);

                    //logger.Debug("ZipFile: " + zipUrl);
                    missionLog.OutPut("ZipFile: ", zipUrl, ClsMissionLog.LOG_LEVEL.DEBUG);
                    //Zipファイルを解凍する
                    ZipFileHandler zipFileHandler = new FoaCoreCom.ais.retriever.ZipFileHandler();
                    unZipUrl = zipFileHandler.unZipFile(zipUrl);
                    missionLog.OutPut("UnzipFile: ", unZipUrl, ClsMissionLog.LOG_LEVEL.DEBUG);
                    if (string.IsNullOrEmpty(unZipUrl))
                    {
		        // 2019/08/30 iwai
			
                        missionLog.OutPut(mission.Value.ToString(),
                                            "が見つかりませんでした。ミッションが削除されていないか確認ください。",
                                            ClsMissionLog.LOG_LEVEL.INFO);
                        MessageBox.Show(mission.Value.DisplayName.ToString() + "が見つかりませんでした。ミッションが削除されていないか確認ください。");
                        //continue;
			return;
                    }

                    missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                            "データ解析のための準備（エレメント配列の作成）を開始します。",
                                            ClsMissionLog.LOG_LEVEL.INFO);

                    //missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    //                        "JSONデータ解析のための準備（エレメント配列の作成）を開始します。",
                    //                       ClsMissionLog.LOG_LEVEL.INFO);
                    //FOR COM高速化 lm  2018/06/21 Modify End

                    // JSONDATAを解析して、CTM毎のCSV加工した文字列を取得する
                    int elementCount = 0;
                    foreach (var item in m_elements.Values)
                    {
                        if (elementCount < item.Values.Count)
                        {
                            elementCount = item.Values.Count;
                        }
                    }
                     missionLog.OutPut("doMission", "keyOrderMatrix: [" + (m_ctms[mission.Value.Id]).Count + "," + elementCount + "]", ClsMissionLog.LOG_LEVEL.DEBUG);
                    // string[,] keyOrderMatrix = new string[(m_ctms[mission.Value.Id]).Count, (m_elements.ElementAt(0).Value).Count + 100];
                    string[,] keyOrderMatrix = new string[(m_ctms[mission.Value.Id]).Count, elementCount+1];
                    //string[,] keyOrderMatrix = new string[(m_ctms[mission.Value.Id]).Count, (m_elements.ElementAt(0).Value).Count+100 ];
                    int ctmIndex = 0;
                    int elementIndex = 0;
                    foreach (KeyValuePair<string, ClsAttribObject> ctm in m_ctms[mission.Value.Id])
                    {
                        elementIndex = 0;
                        keyOrderMatrix[ctmIndex, elementIndex] = ctm.Value.Id;
                        elementIndex++;
                        foreach (KeyValuePair<string, ClsAttribObject> element in m_elements[mission.Value.Id + ctm.Value.Id])
                        {
                            keyOrderMatrix[ctmIndex, elementIndex] = element.Value.Id;
                            missionLog.OutPut("doMission", "elementIndex: " + elementIndex + " element: " + element.Value.DisplayName, ClsMissionLog.LOG_LEVEL.DEBUG);
                            elementIndex++;
                        }
                        ctmIndex++;
                    }

                    missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                            "JSONデータ解析を開始します。",
                                            ClsMissionLog.LOG_LEVEL.INFO);

                    CtlAisProcess ctlAisProcess = new CtlAisProcess();
                    //FOR COM高速化 lm  2018/06/21 Modify Start
                    //Dictionary<string,string> ctmDataDic = ctlAisProcess.parseCtmMission(jsonData, Globals.ThisWorkbook.clsDACConf.TimezoneId, ref keyOrderMatrix);
                    //Dictionary<string, string> ctmDataDic = ctlAisProcess.ReadAllCtmInfoFromCSV(unZipUrl);

                    // ファイル件数を取得
                    Dictionary<string, string> dicResultData = new Dictionary<string, string>();
                    DirectoryInfo folder = new DirectoryInfo(unZipUrl);
                    FileInfo[] fil = folder.GetFiles("*.csv");
                    int fileCnt = 0;
                    if (fil != null)
                    {
                        fileCnt = fil.Length;
                    }

                    missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                            "データ解析が終了しました（CTM" + Convert.ToString(fileCnt) + "件分のファイルを取得しました)",
                                            ClsMissionLog.LOG_LEVEL.INFO);
                    
                    //missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    //                       "JSONデータ解析が終了しました（CTM" + ctmDataDic.Count + "件分のファイルを取得しました)",
                     //                      ClsMissionLog.LOG_LEVEL.INFO);
                    //FOR COM高速化 lm  2018/06/21 Modify End

                    missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                            "CTM毎の結果シートの作成を開始します。",
                                            ClsMissionLog.LOG_LEVEL.INFO);

                    // CTMごとに結果シートを作成する
                    int iCounter = 0;
                    // Wang New dac flow 20190308 Start
                    //// Wang Issue reference to value of cell 20190228 Start
                    //int datasheetIndex = 1;
                    //// Wang Issue reference to value of cell 20190228 End
                    // Wang New dac flow 20190308 End
                    foreach (KeyValuePair<string, ClsAttribObject> ctm in m_ctms[mission.Value.Id])
                    {
                        /*
                         * 結果シートの追加
                         */
                        // シート名の決定
                        string SheetID = mission.Value.DisplayName + "." + ctm.Value.DisplayName;
                        
                        //dac_home start
                        ////string SheetName = varSheetName + datasheetIndex;
                        //// Wang Issue reference to value of cell 20190228 Start
                        ////string SheetName = GetSheetName(mission.Value, ctm.Value, SheetID);
                        //string SheetName = varSheetName + datasheetIndex;
                        //// Wang Issue reference to value of cell 20190228 End
                        if (SheetID.Length > MaxNum)
                        {
                            SheetID = SheetID.Substring(0, MaxNum);
                        }

                        //AIS_DAC No.80 sunyi 20181217 start
                        //実行する時、最新のミッションのDataSheetを作るため、旧DataSheetを削除する
                        foreach (Excel.Worksheet worksheet in Globals.ThisWorkbook.Sheets)
                        {
                            //dac_home datasheet start
                            int firstdot = worksheet.Name.IndexOf('.');
                            if (firstdot == -1)
                            {
                                continue;
                            }
                            string namewithoutdot = worksheet.Name.Substring(firstdot + 1);
                            //if (worksheet.Name.EndsWith(SheetID))
                            if (namewithoutdot.Equals(SheetID))
                            {
                                //dac_home datasheet start
                                // Wang Issue reference to value of cell 20190228 Start
                                //worksheet.Delete();
                                worksheet.UsedRange.Clear();
                                // Wang Issue reference to value of cell 20190228 End
                            }
                        }
                        //AIS_DAC No.80 sunyi 20181217 end

                        string SheetName = datasheetIndex + "." + SheetID;
                        //dac_home end

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "シート名(" + SheetName + ")の作成を開始します。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        // シートの追加
                        // Wang Issue reference to value of cell 20190228 Start
                        //Excel.Worksheet newWorksheet;
                        //newWorksheet = (Excel.Worksheet)Globals.ThisWorkbook.Worksheets.Add(After: Globals.ThisWorkbook.Worksheets[Globals.ThisWorkbook.Worksheets.Count]);
                        //newWorksheet.Name = SheetName;
                        
                        Excel.Worksheet newWorksheet = null;
                        try
                        {
                            newWorksheet = Globals.ThisWorkbook.Worksheets[SheetName];
                        }
#pragma warning disable
                        catch (Exception e)
#pragma warning restore
                        {

                        }
                        if (newWorksheet == null)
                        {
                            newWorksheet = (Excel.Worksheet)Globals.ThisWorkbook.Worksheets.Add(After: Globals.ThisWorkbook.Worksheets[Globals.ThisWorkbook.Worksheets.Count]);
                            newWorksheet.Name = SheetName;
                        }
                        newWorksheet.Select();
                        newWorksheet.Cells[1, 1].Select();
                        datasheetIndex++;
                        // Wang Issue reference to value of cell 20190228 End

                        //newWorksheet.Select(1);
                        newWorksheet.Cells[varSheetIDRow, varSheetIDCol].Value = SheetID;

                        // 結果がない場合
                        //FOR COM高速化 lm  2018/06/22 Modify Start
                        //if (ctmDataDic.Count <= iCounter)
                        if (fileCnt <= iCounter)
                        {
                        //FOR COM高速化 lm  2018/06/22 Modify End
                            newWorksheet.Cells[1, 1].Value = "データ無し";

                            missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                    "CTMファイル作成件数が0件です。",
                                                    ClsMissionLog.LOG_LEVEL.WARN);
                            continue;
                        }

                        // 結果が空の場合
                        string result = "";
                        //FOR COM高速化 lm  2018/06/21 Modify Start
                        //string csvFilePath = unZipUrl + "\\" + ctm.Value.Id;
                        // Wang Issue of DAC-49 20181109 Start
                        //Dictionary<string, string> ctmDataDic = ctlAisProcess.ReadCtmInfoFromCSV(unZipUrl, ctm.Value.Id, Globals.ThisWorkbook.clsDACConf.TimezoneId);
                        HashSet<string> elements = new HashSet<string>();
                        elements.UnionWith(this.m_elements[mission.Value.Id + ctm.Value.Id].Keys);
                        elements.Add("RT");
                        Dictionary<string, string> ctmDataDic = ctlAisProcess.ReadCtmInfoFromCSV(unZipUrl, ctm.Value.Id, Globals.ThisWorkbook.clsDACConf.TimezoneId, elements);
                        // Wang Issue of DAC-49 20181109 End
                        if (ctmDataDic != null)
                        {
                            result = ctmDataDic[ctm.Value.Id];
                            if (string.IsNullOrEmpty(result))
                            {
                                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                        "処理中のCTMはファイルが作成されていません",
                                                        ClsMissionLog.LOG_LEVEL.WARN);

                                newWorksheet.Cells[1, 1].Value = "データ無し";
                                continue;
                            }
                        }
                        //FOR COM高速化 lm  2018/06/21 Modify End
                        else
                        {
                            missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                    "処理中のCTMはファイルが作成されていません",
                                                    ClsMissionLog.LOG_LEVEL.WARN);
                            //dac No.21 sun start
                            //ctmが削除された場合
                            MessageBox.Show(ctm.Value.DisplayName.ToString() + "が見つかりませんでした。CTMが削除されていないか確認ください。");
                            //dac No.21 sun end
                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            continue;
                        }

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")があるので、シートへの出力を開始します",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "エレメント情報の出力を開始します。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        /*
                         * シート先頭行のエレメント名の挿入
                         */
                        rng = (Excel.Range)Globals.ThisWorkbook.Application.Selection;
                        DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                        // Wang New dac flow 20190308 Start
                        //rng[1, 1] = "No.";
                        rng[TitleRow, 1] = "No.";
                        // Wang New dac flow 20190308 End
                        //ISSUE_NO.748 Sunyi 2018/08/13 Start
                        //rng[1, 2] = "収集時刻";
                        // Wang New dac flow 20190308 Start
                        //rng[1, 2] = "RECEIVE TIME";
                        rng[TitleRow, 2] = "RECEIVE TIME";
                        // Wang New dac flow 20190308 End
                        //ISSUE_NO.748 Sunyi 2018/08/13 End

                        int iColCount = 3;
                        foreach (KeyValuePair<string, ClsAttribObject> element in m_elements[mission.Value.Id+ctm.Value.Id])
                        {
                            // Wang New dac flow 20190308 Start
                            //rng[1, iColCount++] = element.Value.DisplayName;
                            rng[TitleRow, iColCount++] = element.Value.DisplayName;
                            // Wang New dac flow 20190308 End
                        }

                        /*
                         * 結果の貼付け
                         */
                        // コピー先のセルの指定
                        // Wang New dac flow 20190308 Start
                        //Excel.Range pasteCell = rng.Cells[2, 1];
                        Excel.Range pasteCell = rng.Cells[TitleRow + 1, 1];
                        // Wang New dac flow 20190308 End

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")の読込を開始します。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        // 一旦、メモリーストリームにバイト単位で読み込む。
                        byte[] bs = Encoding.Default.GetBytes(result);
                        MemoryStream ms = new MemoryStream(bs);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")の読込結果をクリップボードにセットします。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        //クリップボードに文字列をセット
                        DataObject data = new DataObject(DataFormats.CommaSeparatedValue, ms);
                        Clipboard.SetDataObject(data, false);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")のクリップボードの内容をCTM用のExcelシートにペーストします。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        Thread.Sleep(1000);

                        // コピー先にクリップボードの内容を貼り付ける。
                        newWorksheet.Paste(pasteCell, System.Type.Missing);

                        /*
                         * 演算式の挿入
                         */
                        //Multi_Dac No.60 sunyi 2018/12/05 start
                        // 最終行の設定
                        //int rowCount = result.Split('\n').Length+1;
                        int rowCount = newWorksheet.UsedRange.Rows.Count + 1;
                        //Multi_Dac No.60 sunyi 2018/12/05 end

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "CTM用のExcelシートには、" + rowCount + "件をペーストしました。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "CTM用のExcelシートの末尾への計算式の埋込を開始します",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        //エレメントの計算値入力                        
                        // Wang New dac flow 20190308 Start
                        //rng[rowCount, 2] = "個数";

                        //rng[rowCount + 1, 2] = "積算";
                        //rng[rowCount + 2, 2] = "平均";
                        //rng[rowCount + 3, 2] = "最大";
                        //rng[rowCount + 4, 2] = "最小";
                        //rng[rowCount + 5, 2] = "種類数";

                        //dac_home start
                        rng[1, 1] = mission.Value.DisplayName + "." + ctm.Value.DisplayName;
                        //dac_home end
                        rng[FormulaRow, 2] = "個数";

                        rng[FormulaRow + 1, 2] = "積算";
                        rng[FormulaRow + 2, 2] = "平均";
                        rng[FormulaRow + 3, 2] = "最大";
                        rng[FormulaRow + 4, 2] = "最小";
                        rng[FormulaRow + 5, 2] = "種類数";
                        // Wang New dac flow 20190308 End

                        for (int i = 3; i < iColCount; i++)
                        {
                            string ColLetterTemp = newWorksheet.Cells[rowCount, i].Address;
                            string[] SplitLetter = ColLetterTemp.Split('$');
                            string ColLetter = SplitLetter[1];
                            // Wang New dac flow 20190308 Start
                            //rng[rowCount, i].Formula = string.Format("=COUNTA({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 1, i].Formula = string.Format("=SUM({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}2:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            //rng[rowCount + 3, i].Formula = string.Format("=MAX({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 4, i].Formula = string.Format("=MIN({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow, i].Formula = string.Format("=COUNTA({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 1, i].Formula = string.Format("=SUM({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}12:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            rng[FormulaRow + 3, i].Formula = string.Format("=MAX({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 4, i].Formula = string.Format("=MIN({0}12:{0}{1})", ColLetter, rowCount - 1);
                            // Wang New dac flow 20190308 End

                        }

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "CTM用のExcelシートの罫線・サイズ等のレイアウト調整を開始します",
                                                ClsMissionLog.LOG_LEVEL.INFO);
                        /*
                         * Excelレイアウト調整
                         */
                        //罫線表示処理
                        // Wang New dac flow 20190308 Start
                        //Excel.Range cell1 = newWorksheet.Cells[1, 1];
                        //Excel.Range cell2 = newWorksheet.Cells[rowCount - 1, 1];
                        Excel.Range cell1 = newWorksheet.Cells[FormulaRow, 2];
                        Excel.Range cell2 = newWorksheet.Cells[FormulaRow + 5, iColCount - 1];
                        // Wang New dac flow 20190308 End
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        // Wang New dac flow 20190308 Start
                        //cell1 = newWorksheet.Cells[1, 2];
                        //cell2 = newWorksheet.Cells[rowCount + 5, iColCount - 1];
                        cell1 = newWorksheet.Cells[TitleRow, 1];
                        cell2 = newWorksheet.Cells[rowCount - 1, iColCount - 1];
                        // Wang New dac flow 20190308 End
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //セルサイズを自動調整
                        newWorksheet.Columns.AutoFit();
                        //dac_home datasheet start
                        newWorksheet.Cells[1, 1].ColumnWidth = COLUMN_WIDTH;
                        //dac_home datasheet end
                    }

                }

                // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                // Multi_Dac 修正
                if(IsGripMission)
                {
                    string[] gripMissionId;
                    JToken missionToken = JToken.Parse("{}");
                    int routeIndex = 0;
                    string resultFolder = "";
                    gripMissionId = GetColumnValues_G(1);

                    GripDataRetrieverDac gripDataRetrieverDac = new FoaCoreCom.ais.retriever.GripDataRetrieverDac();
                    ClsSearchInfo clsSearchInfo = new ClsSearchInfo();
                    WebProxy proxy = new WebProxy();

                    List<string> SearchIds = new List<string>();

                    foreach (var missionId in gripMissionId)
                    {
                        //URLのパラメータ
                        clsSearchInfo.MissionId = missionId;
                        clsSearchInfo.GripHostName = Globals.Sheet1.m_MissionHostName;
                        clsSearchInfo.GripPortNo = int.Parse(Globals.Sheet1.m_MissionPortNo);
                        clsSearchInfo.SkipSameMainKey = false;
                        clsSearchInfo.SearchByInCondition = "auto";
                        clsSearchInfo.SDateUnixTimeString = startEpoMilliseconds;
                        clsSearchInfo.EDateUnixTimeString = endEpoMilliseconds;
                        clsSearchInfo.onlineId = "";
                        clsSearchInfo.SearchId = new GUID().ToString();

                        SearchIds.Add(clsSearchInfo.SearchId);

                        resultFolder = gripDataRetrieverDac.GetDacGripDatatCsvSync(clsSearchInfo, proxy,routeIndex);
                        //dac No.21 sun start
                        if (string.IsNullOrEmpty(resultFolder))
                        {
                            MessageBox.Show("削除されているミッションがあります。確認ください。");
                        }
                        //dac No.21 sun end
                        routeIndex++;
                    }

                    routeIndex = 0;
                    // Wang New dac flow 20190308 Start
                    //int datasheetIndex = 1;
                    // Wang New dac flow 20190308 End
                    foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions_tmp)
                    {
                        CtlAisProcess ctlAisProcess = new CtlAisProcess();
                        // CTMごとに結果シートを作成する
                        /*
                         * 結果シートの追加
                         */
                        // シート名の決定
                        string SheetID = mission.Value.DisplayName;
                        //dac_home start
                        ////string SheetName = GetSheetName(mission.Value, mission.Value, SheetID);
                        //string SheetName = varSheetName + datasheetIndex;
                        if (SheetID.Length > MaxNum)
                        {
                            SheetID = SheetID.Substring(0, MaxNum);
                        }
                        //AIS_DAC No.80 sunyi 20181217 start

                        //実行する時、最新のミッションのDataSheetを作るため、旧DataSheetを削除する
                        foreach (Excel.Worksheet worksheet in Globals.ThisWorkbook.Sheets)
                        {
                            //dac_home datasheet start
                            int firstdot = worksheet.Name.IndexOf('.');
                            if (firstdot == -1)
                            {
                                continue;
                            }
                            string namewithoutdot = worksheet.Name.Substring(firstdot + 1);
                            //if (worksheet.Name.EndsWith(SheetID))
                            if (namewithoutdot.Equals(SheetID))
                            {
                                //dac_home datasheet start
                                // Wang Issue reference to value of cell 20190228 Start
                                //worksheet.Delete();
                                worksheet.UsedRange.Clear();
                                // Wang Issue reference to value of cell 20190228 End
                            }
                        }
                        //AIS_DAC No.80 sunyi 20181217 end

                        string SheetName = datasheetIndex + "." + SheetID;
                        //dac_home end

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "シート名(" + SheetName + ")の作成を開始します。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        // シートの追加
                        Excel.Worksheet newWorksheet = null;
                        try
                        {
                            newWorksheet = Globals.ThisWorkbook.Worksheets[SheetName];
                        }
#pragma warning disable
                        catch (Exception e)
#pragma warning restore
                        {

                        }
                        if (newWorksheet == null)
                        {
                            newWorksheet = (Excel.Worksheet)Globals.ThisWorkbook.Worksheets.Add(After: Globals.ThisWorkbook.Worksheets[Globals.ThisWorkbook.Worksheets.Count]);
                            newWorksheet.Name = SheetName;
                        }
                        newWorksheet.Select();
                        newWorksheet.Cells[1, 1].Select();
                        datasheetIndex++;

                        //newWorksheet.Select(1);
                        newWorksheet.Cells[varSheetIDRow, varSheetIDCol].Value = SheetID;

                        // 結果が空の場合
                        string result = "";
                        //FOR COM高速化 lm  2018/06/21 Modify Start
                        //string csvFilePath = unZipUrl + "\\" + ctm.Value.Id;
                        string pathString = "";
                        foreach (var searchId in SearchIds)
                        {
                            pathString = gripDataRetrieverDac.DlDir + "\\" + searchId + "\\" + mission.Value.Id + ".csv";
                            if (File.Exists(pathString))
                            {
                                pathString = gripDataRetrieverDac.DlDir + "\\" + searchId;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        Dictionary<string, string> ctmDataDic = ctlAisProcess.ReadCtmInfoFromCSV(pathString, mission.Value.Id, Globals.ThisWorkbook.clsDACConf.TimezoneId,null);
                        if (ctmDataDic != null)
                        {
                            result = ctmDataDic[mission.Value.Id];
                            if (string.IsNullOrEmpty(result))
                            {
                                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                        "処理中のCTMはファイルが作成されていません",
                                                        ClsMissionLog.LOG_LEVEL.WARN);

                                newWorksheet.Cells[1, 1].Value = "データ無し";
                                continue;
                            }
                        }
                        //FOR COM高速化 lm  2018/06/21 Modify End
                        else
                        {
                            missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                    "処理中のCTMはファイルが作成されていません",
                                                    ClsMissionLog.LOG_LEVEL.WARN);

                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            continue;
                        }

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")があるので、シートへの出力を開始します",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "エレメント情報の出力を開始します。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        /*
                         * シート先頭行のエレメント名の挿入
                         */
                        rng = (Excel.Range)Globals.ThisWorkbook.Application.Selection;
                        DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                        // Wang New dac flow 20190308 Start
                        //rng[1, 1] = "No.";
                        rng[TitleRow, 1] = "No.";
                        // Wang New dac flow 20190308 End
                        //ISSUE_NO.748 Sunyi 2018/08/13 Start
                        //rng[1, 2] = "収集時刻";
                        //rng[1, 2] = "RECEIVE TIME";
                        //ISSUE_NO.748 Sunyi 2018/08/13 End
                        int iColCount = 2;                       
                        string csvFile = pathString + "\\" + mission.Value.Id + ".csv";
                        using (var reader = new CsvReader(new StreamReader(csvFile, Encoding.GetEncoding("UTF-8"))))
                        {
                            reader.Read();
                            var headers = reader.FieldHeaders;
                            foreach (var header in headers)
                            {
                                // Wang New dac flow 20190308 Start
                                //rng[1, iColCount++] = header;
                                rng[TitleRow, iColCount++] = header;
                                // Wang New dac flow 20190308 End
                            }
                        }
                        /*
                         * 結果の貼付け
                         */
                        // コピー先のセルの指定
                        // Wang New dac flow 20190308 Start
                        //Excel.Range pasteCell = rng.Cells[2, 1];
                        Excel.Range pasteCell = rng.Cells[TitleRow + 1, 1];
                        // Wang New dac flow 20190308 End

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")の読込を開始します。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        // 一旦、メモリーストリームにバイト単位で読み込む。
                        byte[] bs = Encoding.Default.GetBytes(result);
                        MemoryStream ms = new MemoryStream(bs);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")の読込結果をクリップボードにセットします。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        //クリップボードに文字列をセット
                        DataObject data = new DataObject(DataFormats.CommaSeparatedValue, ms);
                        Clipboard.SetDataObject(data, false);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "処理中のCTMのファイル(" + result + ")のクリップボードの内容をCTM用のExcelシートにペーストします。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        Thread.Sleep(1000);

                        // コピー先にクリップボードの内容を貼り付ける。
                        newWorksheet.Paste(pasteCell, System.Type.Missing);

                        /*
                         * 演算式の挿入
                         */
                        // 最終行の設定
                        // Wang New dac flow 20190308 Start
                        //int rowCount = result.Split('\n').Length + 1;
                        int rowCount = newWorksheet.UsedRange.Rows.Count + 1;
                        // Wang New dac flow 20190308 End


                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "CTM用のExcelシートには、" + rowCount + "件をペーストしました。",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "CTM用のExcelシートの末尾への計算式の埋込を開始します",
                                                ClsMissionLog.LOG_LEVEL.INFO);

                        //エレメントの計算値入力                        
                        // Wang New dac flow 20190308 Start
                        //rng[rowCount, 1] = "個数";

                        //rng[rowCount + 1, 1] = "積算";
                        //rng[rowCount + 2, 1] = "平均";
                        //rng[rowCount + 3, 1] = "最大";
                        //rng[rowCount + 4, 1] = "最小";
                        //rng[rowCount + 5, 1] = "種類数";

                        //dac_home datasheet start
                        rng[1, 1] = mission.Value.DisplayName;
                        //dac_home datasheet end
                        rng[FormulaRow, 1] = "個数";

                        rng[FormulaRow + 1, 1] = "積算";
                        rng[FormulaRow + 2, 1] = "平均";
                        rng[FormulaRow + 3, 1] = "最大";
                        rng[FormulaRow + 4, 1] = "最小";
                        rng[FormulaRow + 5, 1] = "種類数";
                        // Wang New dac flow 20190308 End

                        for (int i = 2; i < iColCount; i++)
                        {
                            string ColLetterTemp = newWorksheet.Cells[rowCount, i].Address;
                            string[] SplitLetter = ColLetterTemp.Split('$');
                            string ColLetter = SplitLetter[1];
                            // Wang New dac flow 20190308 Start
                            //rng[rowCount, i].Formula = string.Format("=COUNTA({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 1, i].Formula = string.Format("=SUM({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}2:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            //rng[rowCount + 3, i].Formula = string.Format("=MAX({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 4, i].Formula = string.Format("=MIN({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow, i].Formula = string.Format("=COUNTA({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 1, i].Formula = string.Format("=SUM({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}12:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            rng[FormulaRow + 3, i].Formula = string.Format("=MAX({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 4, i].Formula = string.Format("=MIN({0}12:{0}{1})", ColLetter, rowCount - 1);
                            // Wang New dac flow 20190308 End

                        }

                        missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                                "CTM用のExcelシートの罫線・サイズ等のレイアウト調整を開始します",
                                                ClsMissionLog.LOG_LEVEL.INFO);
                        /*
                         * Excelレイアウト調整
                         */
                        //罫線表示処理
                        // Wang New dac flow 20190308 Start
                        //Excel.Range cell1 = newWorksheet.Cells[1, 1];
                        //Excel.Range cell2 = newWorksheet.Cells[rowCount - 1, 1];
                        Excel.Range cell1 = newWorksheet.Cells[FormulaRow, 2];
                        Excel.Range cell2 = newWorksheet.Cells[FormulaRow + 5, iColCount - 1];
                        // Wang New dac flow 20190308 End
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        // Wang New dac flow 20190308 Start
                        //cell1 = newWorksheet.Cells[1, 2];
                        //cell2 = newWorksheet.Cells[rowCount + 5, iColCount - 1];
                        cell1 = newWorksheet.Cells[TitleRow, 1];
                        cell2 = newWorksheet.Cells[rowCount - 1, iColCount - 1];
                        // Wang New dac flow 20190308 End
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //セルサイズを自動調整
                        newWorksheet.Columns.AutoFit();
                        //dac_home datasheet start
                        newWorksheet.Cells[1, 1].ColumnWidth = COLUMN_WIDTH;
                        //dac_home datasheet end

                    }
                }
                // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                                        "CTM毎の結果シートの作成が終了しました。",
                                        ClsMissionLog.LOG_LEVEL.INFO);

                ////////////////////////////////////////////////////////////////////////////////////////

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                        "DACメインシートへの結果の出力を開始します。",
                        ClsMissionLog.LOG_LEVEL.INFO);

                //メインシートにデータを挿入する
                /*
                CalcurateSheet(isAfterFlag);
                */


                //メインシートにデータを挿入する
                ClsCalUtil.CalcurateSheet(
                    m_activeRange.Value.ToString("yyyy/MM/dd"),
                    Globals.ThisWorkbook.clsSubDACControler.DACName,
                    Globals.ThisWorkbook.Sheets,
                    //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 start
                    //Globals.ThisWorkbook.Sheets["ＤＡＣ帳票_フォーマットイメージ"],
                    Globals.ThisWorkbook.Sheets[DAC_SHEET],
                    //foastudio ＤＡＣ帳票_フォーマットイメージ ⇒ ＤＡＣシート sunyi 2018/11/01 end
                    m_activeRange.Row, isAfterFlag);

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "DACメインシートへの結果の出力が終了しました。",
                    ClsMissionLog.LOG_LEVEL.INFO);


                // 更新処理
                this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                this.Application.Calculate();
                this.Application.ScreenUpdating = true;

                this.Activate();
                this.Application.DisplayAlerts = true;

                // Cleanup, related with AIS Fix no.76
                //int formulaHeader = findFormulaRow();
                //CleanupColumnData(formulaHeader);

                // ファイルが作成されたので、再表示する
                Globals.ThisWorkbook.m_subDACActionPanel.printSubDAC();

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "ミッション全体の実行時間(オフライン)は（" + sw.Elapsed + "）になります。", ClsMissionLog.LOG_LEVEL.INFO);

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")",
                    "ミッションの計算(オフライン)が終了しました。", ClsMissionLog.LOG_LEVEL.INFO);


                sw.Stop();

                if (Globals.ThisWorkbook.clsDACConf.MonitorLevel.Equals(ClsMissionLog.LOG_MONITOR_TYPE.SIMPLE))
                {
                    if (logViewerProcess != null && !logViewerProcess.HasExited)
                        logViewerProcess.Kill();
                }

                MessageBox.Show("ミッション取得処理が完了しました。");


            }
            //Mi失敗時
            catch (Exception ex)
            {

                missionLog.OutPut("第1階層DAC(" + Globals.ThisWorkbook.clsSubDACControler.DACName + ")で例外発生により処理終了",
                    ex.Message + ex.StackTrace, ClsMissionLog.LOG_LEVEL.INFO);


                MessageBox.Show(ex.Message);
                m_writeLog.OutPut("button_Click", ex.Message + ex.StackTrace);
                this.Application.DisplayAlerts = true;

                // 更新処理
                this.Application.ScreenUpdating = true;
                this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                this.Application.Calculate();

                //Mi登録状態表示処理
                Excel.Range cell1;
                for (int j = 4; j < 15000; j += 3) // 3 ->セルの結合数
                {
                    cell1 = this.Cells[6, j];
                    //cell2 = this.Cells[6, j + 3];
                    rng = this.Range[cell1, cell1];
                    if (!String.IsNullOrEmpty(rng.Value))
                    {
                        if (rng.Value == "-") continue;
                        this.Range[this.Cells[7, j], this.Cells[7, j]].Value = "処理中断";
                    }
                    else
                    {
                        Excel.Range nextCell1 = this.Cells[6, j + 3]; // 次のセル
                        string nextMi = (string)this.Range[nextCell1, nextCell1].Value;

                        if (String.IsNullOrEmpty(nextMi))
                        {
                            break;
                        }
                    }
                }

                MessageBox.Show("ミッション取得処理が完了しました。");
            }
            finally
            {
                if (Globals.ThisWorkbook.clsDACConf.MonitorLevel.Equals(ClsMissionLog.LOG_MONITOR_TYPE.SIMPLE))
                {
                    if (logViewerProcess != null && !logViewerProcess.HasExited)
                    logViewerProcess.Kill();
                }
            }
        }

        private HashSet<string> GetNamesFromMission(ClsAttribObject mission, ClsAttribObject ctm, ClsJsonData[] jsonData)
        {
            HashSet<string> names = new HashSet<string>();

            //CTMを列挙
            foreach (ClsJsonData CTM in jsonData)
            {
                //ミッションから対象のCTMを探す　見つかった場合のみ処理を行う
                if (CTM.id == ctm.Id) // 2015.07.24
                {
                    //新規シートの作成
                    string SheetName = mission.DisplayName + "." + ctm.DisplayName;
                    names.Add(SheetName);
                }
            }

            return names;
        }

        private void ClearNotExistSheets(bool useProxy,string proxyUri,string sTime, string eTime, int timeOutMiliSeconds)
        {
            HashSet<string> names = new HashSet<string>();

            foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions)
            {
                ClsJsonData[] jsonData = null;
                string missionId = mission.Key;
                jsonData = ClsJsonData.get(useProxy,proxyUri,missionId, sTime, eTime, m_writeLog, timeOutMiliSeconds);
                foreach (KeyValuePair<string, ClsAttribObject> attrObj in m_ctms[missionId])
                {
                    string key = mission.Value.DisplayName + "." + attrObj.Value.DisplayName;
                    names.UnionWith(this.GetNamesFromMission(mission.Value, attrObj.Value, jsonData));
                }
                jsonData = null;
            }

            foreach (Excel.Worksheet sheet in Globals.ThisWorkbook.Worksheets)
            {
                var SheetValue = sheet.Cells[varSheetIDRow, varSheetIDCol].Value;
                if (SheetValue != null)
                {
                    if (!names.Contains(SheetValue.ToString()))
                    {
                        sheet.Delete();
                    }
                }
            }
        }



 

        /// <summary>
        ///各シートの600列の値を比較し
        ///演算名と一致するシートがある場合、削除して、新規する
        ///演算名と一致するシートがない場合、「Data+count+1」のシートを新規する
        /// </summary>
        public string GetSheetName(ClsAttribObject mission, ClsAttribObject ctm, string SheetID)
        {
            // Delete data sheet
            foreach (Excel.Worksheet worksheet in Globals.ThisWorkbook.Sheets)
            {
                var value = worksheet.Cells[varSheetIDRow, varSheetIDCol].Value;
                if (value != null)
                {
                    if (value == SheetID)
                    {
                        worksheet.Delete();
                    }
                }

            }

            // Get existing data sheet names
            HashSet<string> dataSheetNames = new HashSet<string>();

            foreach (Excel.Worksheet newWorksheet in Globals.ThisWorkbook.Sheets)
            {
                string sheetName = newWorksheet.Name;
                string dataSheetPrefix = sheetName.Substring(0, Math.Min(9, sheetName.Length));

                if (dataSheetPrefix.Equals(varSheetName))
                {
                    dataSheetNames.Add(newWorksheet.Name);
                }
            }

            // Get new data sheet name
            string newDataSheetName = "";
            for (int index = 1; ; index++)
            {
                newDataSheetName = varSheetName + index;
                if (!dataSheetNames.Contains(newDataSheetName))
                {
                    break;
                }
            }

            return newDataSheetName;
        }

        /// <summary>
        ///各シートの600列の値を比較し、演算名と一致するシートが対象となる
        /// </summary>
        public Excel.Worksheet GetCulcWorkSheet(string culcSheet)
        {
            Excel.Worksheet culcWorkSheet = null;

            foreach (Excel.Worksheet newWorksheet in Globals.ThisWorkbook.Sheets)
            {
                var value = newWorksheet.Cells[varSheetIDRow, varSheetIDCol].Value;
                if (value != null)
                {
                    if (culcSheet == value.ToString())
                    {
                        culcWorkSheet = newWorksheet;
                        break;
                    }
                }
            }

            return culcWorkSheet;
        }

        /*
        /// <summary>
        /// Find the row of Formula Header '演算行/定数'
        /// </summary>
        public static int findFormulaRow()
        {
            Excel.Worksheet currentSheet = Globals.ThisWorkbook.Application.Sheets["ＤＡＣ帳票_フォーマットイメージ"];

            Excel.Range cell1 = null;

            try
            {
                if (currentSheet.Cells[Sheet1.FOMULAR_ROW, Sheet1.baseCalCell.Column].Value != null
                    && currentSheet.Cells[Sheet1.FOMULAR_ROW, Sheet1.baseCalCell.Column].Value.ToString() == "演算行/定数")
                {
                    return Sheet1.FOMULAR_ROW;
                }
                else
                {
                    int formulaRow = 0;
                    for (int row = 1; row <= 15000; row++)
                    {
                        cell1 = currentSheet.Cells[row, Sheet1.baseCalCell.Column];
                        if (cell1.Value != null && cell1.Value.ToString() == "演算行/定数")
                        {
                            formulaRow = row;
                            break;
                        }
                    }
                    return formulaRow;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        */

        /// <summary>
        /// Cleanup Un-used Column data without any inputted formula header
        /// </summary>
        private void CleanupColumnData(int rowFormulaHeader)
        {
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range cell3 = null;
            Excel.Range cell4 = null;

            try
            {
                for (int col = 3; col <= FOMULAR_END_COL; col++)
                {
                    // Check the formula header
                    cell1 = this.Cells[rowFormulaHeader, col];
                    // Check the First Data row
                    cell2 = this.Cells[rowFormulaHeader + 1, col];
                    // If the row formula header is empty, and the first row data is not empty,
                    // then proceed to cleanup the entire column data
                    if (cell1.Value == null && cell2.Value != null)
                    {
                        for (int row = rowFormulaHeader + 1; row <= 15000; row++)
                        {
                            cell3 = this.Cells[row, 2];
                            cell4 = this.Cells[row + 1, 2];
                            if (cell3.Value != null && cell3.Value.ToString() == "週計"
                                && cell4.Value == null)
                            {
                                //MessageBox.Show("Checkpoint..!");
                                break; // checkpoint..
                            }
                            else
                            {
                                //MessageBox.Show("Begin Cleanup..!");
                                this.Cells[row, col].ClearContents();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("cleanupColumnData", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// メインシートデータ挿入処理
        ///     isAfterFlag:業務終了時間が計算実行時間より後か、前か、
        /// </summary>
        private void CalcurateSheet(bool isAfterFlag)
        {
            Excel.Range formulaRange = null;        // 演算行の数式入力レンジ（複数セル）
            Excel.Range cellFormula = null;         // 演算式（メインDAC）

            Excel.Range resultCell = null;          // 演算結果格納セル

            try
            {

                formulaRange = this.Range[this.Cells[Sheet1.baseCalCell.Row, Sheet1.baseCalCell.Column], this.Cells[Sheet1.baseCalCell.Row, Sheet1.baseCalCell.Column]].EntireRow;

                //演算行が見つからなかった場合は処理を抜ける
                if (formulaRange == null)
                {
                    return;
                }

                //演算行の各セルを見て演算を行う
                for (int i = Sheet1.baseCalCell.Column + 1; i <= 15000; i++)
                {
                    /*
                     * 検索する文字列の形式は"シート名,列名,行名"+(x * "シート名,列名,行名")　の形式
                     */
                    cellFormula = this.Cells[Sheet1.baseCalCell.Row, i];


                    resultCell = this.Cells[m_activeRange.Row, i];


                    bool dataNothingChk = false; // データシートにデータがあるかチェック
                    try
                    {
                        // 演算行が入力されていない場合は処理の先頭に戻る
                        if (cellFormula.Value == null)
                        {
                            continue;
                        }

                        string strFormula = cellFormula.Value.ToString();
                        strFormula = strFormula.Replace("\r", string.Empty);
                        strFormula = strFormula.Replace("\n", string.Empty);
                        strFormula = strFormula.Replace("\"", "?\"?");
                        strFormula = strFormula.Replace("\"", "?\"?");
                        string[] arrayFormula = strFormula.Split('?');

                        //シート検索フォーマットが入力されていた場合、計算を行う
                        if (arrayFormula.Length > 1)
                        {
                            for (int j = 0; j < arrayFormula.Length; j++)
                            {
                                string[] culcSheet = arrayFormula[j].Split(',');
                                //シート検索フォーマットが入力されていた場合は以下の処理を行う
                                if (culcSheet.Length == 3)
                                {
                                    //各シートの600列の値を比較し、演算名と一致するシートが対象となる
                                    Excel.Worksheet culcWorkSheet = GetCulcWorkSheet(culcSheet[0]);

                                    // Wang New dac flow 20190308 Start
                                    //if (culcWorkSheet.Cells[1, 1].Value.ToString() == "データ無し")
                                    if (culcWorkSheet.Cells[1, 1].Value != null || culcWorkSheet.Cells[1, 1].Value.ToString() == "データ無し")
                                    // Wang New dac flow 20190308 End
                                    {
                                        arrayFormula[j] = "0";
                                    }
                                    else
                                    {
                                        int col = culcWorkSheet.Rows[1].Find(culcSheet[1]).Column;
                                        int row = culcWorkSheet.Columns[2].Find(culcSheet[2]).Row;
                                        if (culcSheet[2] == "種類数")
                                        {
                                            string ColLetterTemp = culcWorkSheet.Cells[row, col].Address;
                                            string[] SplitLetter = ColLetterTemp.Split('$');
                                            string ColLetter = SplitLetter[1];
                                            culcWorkSheet.Cells[row, col].FormulaArray = string.Format("=SUM(1/COUNTIF({0}2:{0}{1},{0}2:{0}{1}))", ColLetter, row - 6);
                                        }
                                        arrayFormula[j] = culcWorkSheet.Cells[row, col].Value.ToString();
                                    }
                                }
                            }


                            if (dataNothingChk != true) // 取得データがあれば処理続行
                            {
                                bool kakFlag = true;
                                for (int j = 0; j < arrayFormula.Length; j++)
                                {
                                    if (arrayFormula[j] == "\"")
                                    {
                                        if (kakFlag)
                                        {
                                            arrayFormula[j] = "(";
                                            kakFlag = false;
                                        }
                                        else
                                        {
                                            arrayFormula[j] = ")";
                                            kakFlag = true;
                                        }
                                    }
                                }

                                //この時点で数値型と四則演算式の文字列になる
                                string resultStr = String.Join(null, arrayFormula);
                                resultCell.Value = "=" + resultStr;
                            }

                        }
                        // SubDAC参照式の場合
                        else if (ClsSubDACUtil.isSubDACRefere(strFormula))
                        {
                            string tmp = ClsSubDACUtil.getSubDACData(
                                Globals.ThisWorkbook.m_subDACActionPanel.getLblDate().Text,
                                System.IO.Path.GetFileNameWithoutExtension(Globals.ThisWorkbook.FullName),
                                strFormula);

                            if (!string.IsNullOrEmpty(tmp))
                            {
                                resultCell.Value = Double.Parse(tmp);
                            }
                            else
                            {
                                resultCell.Value = tmp;
                            }

                        }
                        // Wang Issue reference to value of cell 20190228 Start
                        // DAC参照式の場合
                        else if (ClsSubDACUtil.isSelfDACRefere(strFormula))
                        {
                            string tmp = ClsSubDACUtil.getSelfDACData(
                                Globals.ThisWorkbook.m_subDACActionPanel.getLblDate().Text,
                                System.IO.Path.GetFileNameWithoutExtension(Globals.ThisWorkbook.FullName),
                                strFormula, Globals.ThisWorkbook.Worksheets);

                            if (!string.IsNullOrEmpty(tmp))
                            {
                                resultCell.Value = Double.Parse(tmp);
                            }
                            else
                            {
                                resultCell.Value = tmp;
                            }
                        }
                        // Wang Issue reference to value of cell 20190228 End
                        //フォーマットが入力されていなかった場合はエクセルの基本の式を動かす
                        else
                        {
                            resultCell.Value = String.Format("=\"{0}\"", strFormula);
                        }


                        // 計算実行時間が業務終了時間以降の場合は、日付カラムの背景色を切り替える。
                        if (isAfterFlag)
                        {
                            // 日付の色を変える処理は削除
                        }
                    }
                    catch (Exception ex)
                    {
                        m_writeLog.OutPut("calcurateSheet", ex.Message + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("calcurateSheet", ex.Message + ex.StackTrace);
            }
        }

        ///// <summary>
        ///// 【イベント】サンプル表示ボタン押下時処理
        ///// </summary>
        ///// <param name="ctrl"></param>
        ///// <param name="CancelDefault"></param>
        //private void btn_Sample_Click(Office.CommandBarButton ctrl, ref bool CancelDefault)
        //{
        //    try
        //    {
        //        Office.CommandBarButton btnObj = ctrl;

        //        // Wang Issue NO.687 2018/05/18 Start
        //        // 1.階層DAC追加(オプションテンプレート表示統一)
        //        // 2.DAC Excel メニュー整理
        //        //if (ctrl.Caption != "サンプル表示" && ctrl.Caption != "サンプル非表示")
        //        //{
        //        //    return;
        //        //}

        //        //if (btnObj.Caption == "サンプル表示")
        //        //{
        //        //    Globals.Sheet2.sampleOpen();
        //        //    btnObj.Caption = "サンプル非表示";
        //        //}
        //        //else
        //        //{
        //        //    Globals.Sheet2.sampleClose();
        //        //    btnObj.Caption = "サンプル表示";
        //        //}
        //        if (ctrl.Caption != Properties.Resources.DAC_MENU_SHOW_SAMPLE && ctrl.Caption != Properties.Resources.DAC_MENU_HIDE_SAMPLE)
        //        {
        //            return;
        //        }

        //        if (btnObj.Caption == Properties.Resources.DAC_MENU_SHOW_SAMPLE)
        //        {
        //            Globals.Sheet2.sampleOpen();
        //            btnObj.Caption = Properties.Resources.DAC_MENU_HIDE_SAMPLE;
        //        }
        //        else
        //        {
        //            Globals.Sheet2.sampleClose();
        //            btnObj.Caption = Properties.Resources.DAC_MENU_SHOW_SAMPLE;
        //        }
        //        // Wang Issue NO.687 2018/05/18 End
        //    }
        //    catch (Exception ex)
        //    {
        //        m_writeLog.OutPut("btn_Sample_Click", ex.Message + ex.StackTrace);
        //    }
        //}




        private void showCalActionPanel()
        {
            // 項目演算・補助式の値を格納
            string formula = string.Empty;
            string assitant = string.Empty;

            // 現在のセルを取得
            Excel.Range range = Application.ActiveCell;
            int row = range.Row;
            int col = range.Column;

            // 現在のセルが演算行の場合
            if (
                    row == Sheet1.baseCalCell.Row
                && col > Sheet1.baseCalCell.Column && col <= FOMULAR_END_COL)
            {
                // アクションパネルの項目演算に、Excelのアクティブセルの値を設定する
                // 退避した値を持ってこないと、SubDAC参照式やExcel演算式が表示されてしまう。
                formula = Globals.ThisWorkbook.Sheets["退避"].Cells[1, col].Value;


                // アクションパネルの補助式に、退避シートから取得した補助式の値を設定する。
                assitant = Globals.ThisWorkbook.Sheets["退避"].Cells[ASSITANT_ROW, col].Value;
            }


            Globals.ThisWorkbook.m_calActionPanel.Formula = formula;
            Globals.ThisWorkbook.m_calActionPanel.Assitant = assitant;
            Globals.ThisWorkbook.showCalActionPanel();
        }


        /// <summary>
        /// アクションパネル開閉処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDedfault"></param>
        private void button_Action_Click(Office.CommandBarButton ctrl, ref bool CancelDedfault)
        {
            try
            {
                // メニューが違うので何もしない。
                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                //if (ctrl.Caption != "アクションパネル切替")
                if (ctrl.Caption != Properties.Resources.DAC_MENU_SWITCH_PANE)
                // Wang Issue NO.687 2018/05/18 End
                {
                    return;
                }

                if (Globals.ThisWorkbook.isSubDACPanelShown())
                {
                    showCalActionPanel();
                }
                else if (Globals.ThisWorkbook.isCalActionPanelShown())
                {
                    if (Globals.ThisWorkbook.clsDACConf.EnableMultiDoc)
                    {
                        Globals.ThisWorkbook.showSubDACActionPanel();
                    }
                    else
                    {
                        Globals.ThisWorkbook.hidePanel();
                    }
                }
                else
                {
                    if (Globals.ThisWorkbook.clsDACConf.EnableMultiDoc)
                    {
                        Globals.ThisWorkbook.showSubDACActionPanel();
                    }
                    else
                    {
                        showCalActionPanel();
                    }
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("button_Action_Click", ex.Message + ex.StackTrace);
            }
        }

        private async Task SaveMethod()
        {
            bool bRtn = false;

            try
            {
                /*
                //ProcessStartInfoオブジェクトを作成する
                System.Diagnostics.ProcessStartInfo psi =
                    new System.Diagnostics.ProcessStartInfo();
                //起動するファイルのパスを指定する
                psi.FileName = Path.Combine(Globals.ThisWorkbook.clsDACConf.AisExeDir,"AIStool_DAC_Updater.exe");
                //コマンドライン引数を指定する
                psi.Arguments =     Globals.ThisWorkbook.FullName + " " 
                                +   Globals.ThisWorkbook.clsDACConf.SubDacDir + " " 
                                +   Globals.ThisWorkbook.clsDACConf.GripIp + " " 
                                +   Globals.ThisWorkbook.clsDACConf.GripPort;
                
                //アプリケーションを起動する
                System.Diagnostics.Process.Start(psi);
                */

                //if (Globals.ThisWorkbook.clsDACConf.R2UploadFlag)
                {
                    //DAC-61 sunyi 20100115 start
                    //MessageBox.Show("保存中です。\n" + "保存完了のメッセージが出るまで、しばらくお待ちください");
                    //DAC-61 sunyi 20100115 end
                    this.Application.EnableEvents = false;

                    string fileName = System.IO.Path.GetFileNameWithoutExtension(Globals.ThisWorkbook.FullName);

                    //DAC-29 sunyi 20190117 start
                    //選択したファイルのuuidを転送する
                    string uuid = Path.GetFileName(Path.GetDirectoryName(Globals.ThisWorkbook.FullName));
                    //DAC-29 sunyi 20190117 end

                    // 1	DAC帳票（自分自身）をコピーする
                    //string toDACFile = Globals.ThisWorkbook.clsDACConf.AisTmpDir + Path.GetFileName(Globals.ThisWorkbook.FullName);
                    //bRtn = ClsFileUtil.copyFile(Globals.ThisWorkbook.FullName, toDACFile);

                    string dacName = Path.GetFileNameWithoutExtension(Globals.ThisWorkbook.FullName);

                    // 2	DAC帳票をアップする処理を追加する。
                    //DAC-29 sunyi 20190117 start
                    //選択したファイルのuuidを転送する
                    //Task<string> taskDac = ClsHttpUtil.replaceDAC(dacName, toDACFile, true);
                    //Task<string> taskDac = ClsHttpUtil.replaceDAC(dacName, toDACFile, true, uuid);
                    //DAC-29 sunyi 20190117 end
                    //選択したファイルのuuidを転送する
                    //string dac = await taskDac;
                    //while (true)
                    //{
                    //    if (taskDac.IsCompleted)
                    //    {
                    //        break;
                    //    }
                    //    Thread.Sleep(1000);
                    //}

                    // 3	SubDAC帳票を圧縮する。
                    //string subDACZip = ClsFileUtil.compressSubDAC(Globals.ThisWorkbook.FullName);
                    // 4	SubDAC帳票をアップする。
                    //DAC-29 sunyi 20190117 start
                    //選択したファイルのuuidを転送する
                    //Task<string> taskSubDac = ClsHttpUtil.replaceDAC(dacName, subDACZip, false);
                    //Task<string> taskSubDac = ClsHttpUtil.replaceDAC(dacName, subDACZip, false, "0");
                    //DAC-29 sunyi 20190117 end
                    //string subDac = await taskSubDac;
                    //while (true)
                    //{
                    //    if (taskSubDac.IsCompleted)
                    //    {
                    //        break;
                    //    }
                    //    Thread.Sleep(1000);
                    //}



                    // 5	SubDAC帳票を削除する。
                    //ClsFileUtil.deleteOneFile(subDACZip);

                    this.Application.EnableEvents = true;

                    //DAC-61 sunyi 20100115 start
                    //MessageBox.Show("保存を完了しました。");
                    //DAC-61 sunyi 20100115 end
                }

            }
            catch (Exception ex)
            {
                //DAC-61 sunyi 20100115 start
                MessageBox.Show("保存を完了しました。");
                //DAC-61 sunyi 20100115 end
                ////DAC-61 sunyi 20181226 start
                //MessageBox.Show("SaveMethod_AfterSave:", ex.Message.ToString() + ex.StackTrace.ToString());
                ////DAC-61 sunyi 20181226 end
                m_writeLog.OutPut("SaveMethod", ex.Message + ex.StackTrace);
            }

        }

        /// <summary>
        /// ワークブック保存前イベント
        /// 次回起動時に問題が起きないように、各コントロール・入力規則を削除する
        /// </summary>
        /// <param name="SaveAsUI">名前を付けて保存かどうか</param>
        /// <param name="Cancel">Excel動作による保存フラグ</param>
        private  void Workbook_BeforeSave(bool SaveAsUI, ref bool Cancel)
        {

            try
            {
                HashSet<string> openningSubDacs = Globals.ThisWorkbook.m_subDACActionPanel.GetOpenningSubDacs();
                if (openningSubDacs.Count > 0) {
                    string subDacs = string.Empty;
                    foreach (string subDac in openningSubDacs)
                    {
                        subDacs += "\n" + Path.GetFileNameWithoutExtension(subDac);
                    }

                    MessageBox.Show("Sub DACのファイルが開かれています。Sub DACのファイルを閉じてから保存してください。" + subDacs,
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
                    Cancel = true;
                    return;
                }


                // SubDAC管理シートにSubDAC管理リストの最新情報を反映する。
                //Globals.ThisWorkbook.clsSubDACControler.saveSubDACText(); 
                

                /*
                 
                //保存先のファイルパスを指定する                
                string filePath = Path.GetDirectoryName(Globals.ThisWorkbook.Path);
                FileSystem.ChDir(filePath);

                string fullPath = string.Empty;
                if (filePath.Contains("AIS_Template") == true)
                {
                    fullPath = filePath + @"\ExcelResult";  //上書き保存時のフォルダ比較用  
                }
                else
                {
                    fullPath = filePath + @"\AIS_Template\ExcelResult";  //上書き保存時のフォルダ比較用  
                }

                if (File.Exists(fullPath))
                {
                    FileSystem.ChDir(fullPath);       //移動                
                }

                //上書き保存と名前付き保存で処理を分ける
                if (SaveAsUI)
                {
                    //名前を付けて保存
                    var FileName = Application.GetSaveAsFilename(Globals.ThisWorkbook.Name, "Excelファイル(*.xlsx),*.xlsx");

                    if (FileName is bool && FileName == false)  //キャンセル時はbool型が返る
                    {
                        Cancel = true;
                        return;
                    }
                    else  //保存時はファイル名が返る
                    {
                        Cancel = true;

                        //保存用コントロール初期化
                        this.SaveInitialize();

                        //セーブ実行
                        this.Application.EnableEvents = false;
                        Globals.ThisWorkbook.SaveAs(FileName);
                        this.Application.EnableEvents = true;
                        //コントロール類をもとに戻す
                        this.WorkbookAfterSave();
                    }
                }
                //名前をつけて保存時
                else
                {
                    //規定のフォルダか確認
                    if (Path.GetFullPath(Globals.ThisWorkbook.Path) != fullPath)
                    {
                        MessageBox.Show("このフォルダ上で上書きできません");
                        Cancel = true;
                        return;
                    }
                    else
                    {
                        Cancel = true;

                        this.SaveInitialize();

                        //セーブ実行　誤動作回避でイベントを切る
                        this.Application.EnableEvents = false;
                        Globals.ThisWorkbook.Save();
                        this.Application.EnableEvents = true;

                        //再読み込み
                        this.WorkbookAfterSave();
                    }
                }
                 */
            }
            catch (Exception ex)
            {
                //DAC-61 sunyi 20181226 start
                MessageBox.Show("SaveMethod_BeforeSave:", ex.Message.ToString() + ex.StackTrace.ToString());
                //DAC-61 sunyi 20181226 end
                m_writeLog.OutPut("Workbook_BeforeSave", ex.Message + ex.StackTrace);
            }

        }

        private async void Workbook_AfterSave(bool success)
        {
            // 保存が成功した場合は自分自身をアップする
            if (success)
            {
                if (Globals.ThisWorkbook.clsSubDACControler.Hierarchy == 1)
                {
                    await SaveMethod(); // TODO: 2020/07/02 iwai 
                }
            }
            else
            {
                MessageBox.Show("ブックの保存に失敗しました",
                                "エラー",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Error);
            }
        }


        /// <summary>
        /// セーブ時のコントロール初期化
        /// </summary>
        public void SaveInitialize()
        {
            //入力規則を廃棄する
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;
            Excel.Range cell1_Mi = null;
            Excel.Range cell2_MI = null;
            Excel.Range rng_Mi = null;

            int count = 0;
            cell1 = this.Cells[6, 4];

            //ミッション選択部分の入力規則を削除する
            while (cell1.Value != null)
            {
                cell1 = this.Cells[6, 4 + count];
                cell2 = this.Cells[6, 6 + count];
                if (cell1.Value == null) break;
                rng = this.Range[cell1, cell2];
                rng.Validation.Delete();

                cell1_Mi = this.Cells[5, 4 + count];
                cell2_MI = this.Cells[5, 6 + count];
                if (cell1.Value == null) break;
                rng_Mi = this.Range[cell1_Mi, cell2_MI];
                rng_Mi.Validation.Delete();

                count += 3;
            }

            //コマンドメニューのボタン類を削除
            if (m_offlineButton != null)
            {
                m_offlineButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Offline_Click);
                m_offlineButton.Delete();
                m_offlineButton = null;
            }


            if (m_menuButton != null)
            {
                m_menuButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
                m_menuButton.Delete();
                m_menuButton = null;
            }
            //if (m_SampleButton != null)
            //{
            //    m_SampleButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);
            //    m_SampleButton.Delete();
            //    m_SampleButton = null;
            //}
            if (m_actionButton != null)
            {
                m_actionButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);
                m_actionButton.Delete();
                m_actionButton = null;
            }


        }

        /// <summary>
        /// ワークブック保存後再読込関数
        /// </summary>
        private void WorkbookAfterSave()
        {
            //ロードフラグオン
            flagLoading = true;
            //読込開始            
            this.InitializeSheet();
        }
        // private static readonly ILog logger = LogManager.GetLogger(typeof(Sheet1).Name);
    }
#else
    /// <summary>
    /// AISデータ加工用エクセルシート
    /// </summary>
    public partial class Sheet1
    {
    #region"メンバー"
        /// <summary>
        /// 列位置調整用変数
        /// </summary>
        //private int m_colPoint;

        /// <summary>
        /// ミッション登録用の文字列
        /// </summary>
        private string m_queryStr_Mi;
        private string m_queryStr;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_menuButton;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_SampleButton;

        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_actionButton;

        /// <summary>
        /// 現在のアクティブセル
        /// </summary>
        private Excel.Range m_activeRange;

        /// <summary>
        /// ミッションの名前とIDを格納するクラス
        /// </summary>
        private ClsUserMission[] m_userMission;

        /// <summary>
        /// ミッションとCTMを対応付けするディクショナリ
        /// </summary>
        private Dictionary<string, ClsCTMList> m_missionDict;

        /// <summary>
        /// 実行したミッションの名前と実行結果を格納する
        /// </summary>
        //private Dictionary<string, ClsJsonData[]> m_actMission;
            
        /// <summary>
        /// ログ書き出し用クラス
        /// </summary>
        private ClsWriteLog m_writeLog;

        /// <summary>
        /// テスト用リスト
        /// </summary>
        private string[] MiList;

        /// <summary>
        /// アプリケーション読み込みフラグ
        /// </summary>
        public bool flagLoading = false;

        /// <summary>
        /// ユーザーが選択したCTMを格納する
        /// </summary>
        Dictionary<string, ClsCTMList> m_userMissionDict;

        /// <summary>
        /// ミッション投入用ホスト
        /// </summary>
        public string m_MissionHostName { get; set; }
        /// <summary>
        /// ミッション投入用ポート
        /// </summary>
        public string m_MissionPortNo { get; set; }

        /// <summary>
        /// 計算式入力開始列
        /// </summary>
        private const int FOMULAR_START_COL = 2;

    #endregion
        
    #region VSTO デザイナーで生成されたコード

        /// <summary>
        /// デザイナーのサポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(this.Sheet1_Startup);
            this.Shutdown += new System.EventHandler(this.Sheet1_Shutdown);
            this.Change += new Microsoft.Office.Interop.Excel.DocEvents_ChangeEventHandler(this.Sheet1_Change);
            Globals.ThisWorkbook.BeforeSave += new Excel.WorkbookEvents_BeforeSaveEventHandler(Workbook_BeforeSave);
            this.BeforeDoubleClick += Sheet1_BeforeDoubleClick;
        }

        void Sheet1_BeforeDoubleClick(Excel.Range Target, ref bool Cancel)
        {
            string title = Cells[Target.Row, FOMULAR_START_COL].Value;

            if (FOMULAR_START_COL < Target.Column && title.Equals("演算行/定数"))
            {
                Cancel = true;
                Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;
            }
        }

    #endregion

        /// <summary>
        /// 開始時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sheet1_Startup(object sender, System.EventArgs e)
        {
            m_writeLog = new ClsWriteLog("Sheet1");
            //アプリケーション構成ファイル読み込み
            this.LoadConfiguration();            
            //起動処理呼び出し
            this.InitializeSheet();

            this.Activate();
        }


        /// <summary>
        /// 終了時処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Sheet1_Shutdown(object sender, System.EventArgs e)
        {
            this.SaveInitialize();
            Globals.ThisWorkbook.BeforeSave -= new Excel.WorkbookEvents_BeforeSaveEventHandler(Workbook_BeforeSave);
        }


        /// <summary>
        /// シート1を初期化する
        /// </summary>
        public void InitializeSheet()
        {
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;
            Excel.Range cell1_Mi = null;
            Excel.Range cell2_Mi = null;
            Excel.Range rng_Mi = null;
            string userId = "";

            try
            {
                //USER情報確認処理
                if (Globals.ThisWorkbook.Sheets["USERINFO"].Cells[1, 1].Value == null)
                {
                    flagLoading = true;
                    //ユーザー情報がなかった場合処理を抜け、ユーザー情報が入力されるまで待機する
                    return;
                }
                else
                {
                    if (flagLoading)
                    {
                        userId = Globals.ThisWorkbook.Sheets["USERINFO"].Cells[1, 1].Value.ToString();
                        userId = userId.Replace("'", "");
                    }
                    else
                    {
                        flagLoading = true;                        
                        InitializeSheet();
                        return;
                    }
                }

                //ロックを解除する
                this.Unprotect();

                //入力規則によりコマンドバーにボタン追加
                //コマンドメニューにアクションパネル開閉ボタン追加
                Office.MsoControlType menuItem3 = Office.MsoControlType.msoControlButton;
                if (m_actionButton != null)
                {
                    m_actionButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);
                    m_actionButton.Delete();                    
                }
                m_actionButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem3, missing, missing, 1, true);

                m_actionButton.Style = Office.MsoButtonStyle.msoButtonCaption;
                m_actionButton.Caption = "アクションパネル開閉";
                m_actionButton.Tag = "0";
                m_actionButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);

                //コマンドメニューにサンプル実行ボタンを追加
                Office.MsoControlType menuItem2 = Office.MsoControlType.msoControlButton;
                if (m_SampleButton != null)
                {
                    m_SampleButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);
                    m_SampleButton.Delete();                    
                }
                m_SampleButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem2, missing, missing, 1, true);

                m_SampleButton.Style = Office.MsoButtonStyle.msoButtonCaption;
                m_SampleButton.Caption = "サンプル表示";
                m_SampleButton.Tag = "0";
                m_SampleButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);

                //コマンドメニューにミッション実行ボタンを追加
                Office.MsoControlType menuItem = Office.MsoControlType.msoControlButton;
                if (m_menuButton != null)
                {
                    m_menuButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
                    m_menuButton.Delete();                   
                }
                m_menuButton = (Office.CommandBarButton)Application.CommandBars["Cell"].Controls.Add(menuItem, missing, missing, 1, true);

                m_menuButton.Style = Office.MsoButtonStyle.msoButtonCaption;
                m_menuButton.Caption = "ミッション実行";
                m_menuButton.Tag = "0";
                m_menuButton.Click += new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);

                //ユーザーが参照できるミッションを取得
                //Missionを呼び出してクラス内のメンバーにミッション-CTM-エレメントを格納する                
                m_missionDict = new Dictionary<string, ClsCTMList>();
                m_userMission = ClsUserMission.get(userId);

                //サーバー上でアクセスできるすべてのCTMを取得する
                ClsCTMList[] ctmList = ClsCTMList.get();

                m_queryStr_Mi = "-";
                foreach (ClsUserMission Missions in m_userMission)
                {
                    foreach(var mi in Missions.children)
                    {
                        ////ユーザーがミッションで得られるCTMのIDを取得する
                        if (mi.missionType == "1")
                        {
                            m_queryStr_Mi = m_queryStr_Mi + "," + mi.name;

                            ClsUserCTMList[] userCtmList = ClsUserCTMList.get(mi.id);
                            foreach (ClsUserCTMList userCtm in userCtmList)
                            {
                                //CTMIDとCTM名で対応させる
                                //ClsCTMList myCTM = ctmList.First(x => x.id == userCtm.ctmId);
                                //ミッションに追加されているが参照辞書から消されてしまった場合の対応 okada-r 2015/12/07
                                ClsCTMList myCTM = ctmList.FirstOrDefault(x => x.id == userCtm.ctmId);
                                if (myCTM != null)
                                {
                                    string resultName = mi.name + "." + myCTM.displayName[0].text; // 2015.07.24
                                    m_missionDict.Add(resultName, myCTM);
                                }                                
                            }
                        }
                    }
                }

                //プルダウン初期化 Maeda Add 2015.11.09 ↓
                cell1_Mi = this.Cells[5, 4];
                cell2_Mi = this.Cells[5, 6];
                rng_Mi = this.get_Range(cell1_Mi, cell2_Mi);
                // Maeda Add 2015.11.09 ↑

                //プルダウン初期化
                cell1 = this.Cells[6, 4];
                //cell2 = this.Cells[6, 5];
                cell2 = this.Cells[6, 6];
                rng = this.get_Range(cell1, cell2);
                MiList = m_missionDict.Keys.ToArray<string>();

                Array.Sort(MiList, new ClsNaturalComparer());

                int count = 0;

                // 文字数制限追加 maeda 2015.11.17 ↓
                if (m_queryStr_Mi.Length > 8000)
                {
                    string Msg = "ミッション一覧の文字数が制限を超えています。（8000文字以内）";
                    MessageBox.Show(Msg);
                    m_writeLog.OutPut("MiCTM_List :", Msg);
                    return;

                }
                // 文字数制限追加 maeda 2015.11.17 ↑

                while (cell1.Value != null)
                {
                    // Miリスト作成位置 maeda 2015.11.17 ↓
                    cell1_Mi = this.Cells[5, 4 + count];
                    cell2_Mi = this.Cells[5, 6 + count];
                    rng_Mi = this.Range[cell1_Mi, cell2_Mi];
                    // Miリスト作成位置 maeda 2015.11.17 ↑

                    cell1 = this.Cells[6, 4 + count];
                    //cell2 = this.Cells[6, 5 + count];
                    cell2 = this.Cells[6, 6 + count];
                    rng = this.get_Range(cell1, cell2);
                    if (cell1.Value == null) break;

                    // Mi初期データ設定 maeda 2015.11.17 ↓
                    string Mi_Val = cell1.Value;
                    string[] Mi_Vals = Mi_Val.Split('.');
                    // Mi初期データ設定 maeda 2015.11.17 ↑

                    m_queryStr = "-";
                    foreach (string miStr in MiList)
                    {
                        if (Mi_Vals[0] == miStr.Split('.')[0])
                        {
                            m_queryStr += "," + miStr;
                        }

                    }

                    if (m_queryStr.Length > 8000)
                    {
                        string Msg = "ミッションとCTM名称で構成されている一覧の文字数が制限を超えています。（8000文字以内）";
                        MessageBox.Show(Msg);
                        m_writeLog.OutPut("MiCTM_List :", Msg);
                        return;

                    }

                    // Miリスト設定 maeda 2015.11.17 ↓
                    rng_Mi.Validation.Delete();
                    rng_Mi.Merge();
                    rng_Mi.Value = Mi_Vals[0];
                    rng_Mi.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, m_queryStr_Mi, true);
                    rng_Mi.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                    rng_Mi.HorizontalAlignment  = Excel.XlHAlign.xlHAlignCenter;
                    rng_Mi.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    rng_Mi.Select();
                    // Miリスト設定 maeda 2015.11.17 ↑

                    rng.Validation.Delete();
                    rng.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, m_queryStr, true);
                    int colCount = 4 + count; // 2015.11.04                                        
                    count += 3;
                }


                if (m_userMissionDict != null)
                {
                    Globals.ThisWorkbook.m_actionPanel.m_missionDict = m_userMissionDict;
                    Globals.ThisWorkbook.m_actionPanel.AdditionCTMData();
                    Globals.ThisWorkbook.m_actionPanel.controlClear();
                }

            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("Sheet1_Startup", ex.Message + ex.StackTrace);
            }
        }


        ///// <summary>
        ///// AISのアプリケーション構成ファイルを読み込む
        ///// </summary>
        //private void LoadConfiguration()
        //{
        //    try
        //    {
        //        //AISの構成ファイルパスを指定
        //        string filePath = Path.GetDirectoryName(Globals.ThisWorkbook.Path);                
                
        //        if (filePath.Contains("AIS_Template")) // 固定
        //        {
        //            filePath = filePath.Remove(filePath.Length - 13);
        //        }

        //        filePath += @"\AIStool.exe.config";
                
        //        ExeConfigurationFileMap appConfigFile = new ExeConfigurationFileMap { ExeConfigFilename = filePath};

        //        Configuration appConfig = ConfigurationManager.OpenMappedExeConfiguration(appConfigFile, ConfigurationUserLevel.None);
        //        ClientSettingsSection section = (ClientSettingsSection)appConfig.SectionGroups["userSettings"].Sections[0];
        //        SettingElementCollection keyList = (SettingElementCollection)section.Settings;

        //        m_MissionHostName = keyList.Get("PMHostName").Value.ValueXml.InnerText;
        //        m_MissionPortNo = keyList.Get("PMPortNo").Value.ValueXml.InnerText;
        //    }
        //    catch (Exception ex)
        //    {
        //        m_writeLog.OutPut("LoadConfiguration", ex.Message + ex.StackTrace);
        //    }
        //}

        /// <summary>
        /// AISのアプリケーション構成ファイルを読み込む
        /// </summary>
        private void LoadConfiguration()
        {
            try
            {
                LoadCmsUiConf();
                m_MissionHostName = Config.CmsHost;
                m_MissionPortNo = Config.CmsPort;

            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("LoadConfiguration", ex.Message + ex.StackTrace);
            }
        }


        /// <summary>
        /// Resource/Ais.conf 読み込み
        /// </summary>
        public const string BaseUrl = "http://{0}:{1}/cms/rest/";
        public static Conf Config { get; set; }
        public static readonly List<string> CatalogLangList = new List<string> { "ja", "en" };
        public static readonly List<string> CatalogLangListForDisplay = new List<string> { "Japanese", "English" };

        public static void LoadCmsUiConf()
        {
            string confPath = Path.GetDirectoryName(Globals.ThisWorkbook.Path) + @"\Ais.conf";
            using (StreamReader sr = new StreamReader(confPath))
            {
                Config = JsonConvert.DeserializeObject<Conf>(sr.ReadToEnd());
            }

            if (!CatalogLangList.Contains(Config.DefaultCatalogLang))
            {
                Config.DefaultCatalogLang = "ja";
            }
        }

        public class Conf
        {
            public string CmsHost;
            public string CmsPort = "60000";
            public string DefaultCatalogLang;
        }

        
        /// <summary>
        /// 【イベント】プルダウン変更処理
        /// </summary>
        /// <param name="Target"></param>
        private void Sheet1_Change(Excel.Range Target)
        {
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;
            Excel.Range cell1_Mi = null;
            Excel.Range cell2_Mi = null;
            Excel.Range rng_Mi = null;
       
            try
            {
                Excel.Range fullRow;
                fullRow = this.Rows[5];
                // CTMリスト作成 maeda 2015.11.17 ↓
                if (Target.Row == fullRow.Row)
                {
                    ////シートの保護を解除
                    this.Unprotect();

                    cell1_Mi = this.Cells[5, Target.Column];
                    cell2_Mi = this.Cells[5, Target.Column + 2];
                    rng_Mi = this.Range[cell1_Mi, cell2_Mi];

                    if (string.IsNullOrEmpty((string)cell1_Mi.Value) != true)
                    {
                        if (cell1_Mi.Value != "-")
                        {                          
                            m_queryStr = "-";

                            if (string.IsNullOrEmpty((string)cell1_Mi.Value) != true)
                            {
                                foreach (string miStr in MiList)
                                {
                                    string[] miStrs = miStr.Split('.');
                                    if (cell1_Mi.Value == miStrs[0])
                                    {
                                        m_queryStr += "," + miStr;
                                    }

                                }
                            }

                            cell1 = this.Cells[6, Target.Column];
                            cell2 = this.Cells[6, Target.Column + 2]; // 2つ結合

                            rng = this.Range[cell1, cell2];
                            rng.Merge();
                            rng.Validation.Delete();
                            rng.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, m_queryStr, true);
                            rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            rng.Interior.ColorIndex = 37;                            
                        }
                    }
                }
                // CTMリスト作成 maeda 2015.11.17 ↑

                fullRow = this.Rows[6];
                if (Target.Row == fullRow.Row)
                {
                    //シートの保護を解除
                    this.Unprotect();

                    //変更でミッションが選択されなかった場合は処理を抜ける
                    if (Target.Value == "-")
                    {
                        // 次の列にCTMが選択された場合　->　その列を削除
                        Excel.Range nextCell = this.Cells[6, Target.Column + 3];
                        if (string.IsNullOrEmpty((string)nextCell.Value) == false)
                        {
                            cell1 = this.Cells[5, Target.Column];
                            cell2 = this.Cells[7, Target.Column + 2];
                            rng = this.Range[cell1, cell2];
                            rng.Delete(Excel.XlDeleteShiftDirection.xlShiftToLeft);
                        }
                        return;
                    }

                    //同名ミッション選択時処理
                    //空白セルに到達した場合はプルダウンメニューを追加する
                    int count = 0;
                    for (int i = 4; i < 15000; i += 3) // 3 ->セルの結合数
                    {
                        cell1 = this.Cells[6, i];
                        cell2 = this.Cells[6, i + 2]; // 2つ結合
                        rng = this.Range[cell1, cell1];

                        if (cell1.Value == Target.Value || cell1.Value == "-")
                        {
                            count++;
                            //自分以外に同じ値があった場合
                            if (count == 2)
                            {
                                rng.Value = "-";
                                return;
                            }

                        }

                        //空白セルだった場合
                        else if (string.IsNullOrEmpty((string)cell1.Value))
                        {
                            // ミッションリスト作成 maeda 2015.11.17 ↓
                            cell1_Mi = this.Cells[5, i];
                            cell2_Mi = this.Cells[5, i + 2];
                            rng_Mi = this.Range[cell1_Mi, cell2_Mi];
                            rng_Mi.Merge();
                            rng_Mi.Validation.Delete();
                            rng_Mi.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, m_queryStr_Mi, true);
                            rng_Mi.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            rng_Mi.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            rng_Mi.VerticalAlignment = Excel.XlHAlign.xlHAlignCenter;
                            rng_Mi.Value = "-";
                            //rng_Mi.Activate();
                            // ミッションリスト作成 maeda 2015.11.17 ↑

                            rng = this.Range[cell1, cell2];
                            rng.Validation.Delete();
                            rng.Validation.Add(Excel.XlDVType.xlValidateList, Excel.XlDVAlertStyle.xlValidAlertStop, Excel.XlFormatConditionOperator.xlBetween, m_queryStr, true);
                            rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            rng.Interior.ColorIndex = 37;
                            cell1.Value = "-";
                            //文字列表示処理
                            cell1 = this.Cells[7, i];
                            cell2 = this.Cells[7, i + 2]; // 2つ結合
                            rng = this.Range[cell1, cell2];
                            rng.Value = "-";
                            rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;
                            
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {                
                m_writeLog.OutPut("Sheet1_Change", ex.Message + ex.StackTrace);
            }
        }



        /// <summary>
        /// ミッション実行ボタン押下時処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDefault"></param>
        private void button_Click(Office.CommandBarButton ctrl, ref bool CancelDefault)
        {
            m_activeRange = Application.ActiveCell;

            Excel.Range cell1 = null;            
            Excel.Range rng = null;
            try
            {
                if (ctrl.Caption != "ミッション実行")
                {
                    return;
                }


                m_userMissionDict = new Dictionary<string, ClsCTMList>(); //ユーザーが選択したミッションリスト

                m_activeRange = Application.ActiveCell; //現在のアクティブセル位置
                string sTime = "";  //ミッションの検索範囲
                string eTime = "";
                cell1 = this.Cells[m_activeRange.Row, 2];
                
                if (cell1.Value is DateTime)
                {
                    DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                    DateTime start = this.Cells[m_activeRange.Row, 2].Value;
                    DateTime end;
                    //開始時刻作成 **********************************
                    cell1 = this.Range["C3"];
                    TimeSpan startTime = TimeSpan.MaxValue;
                    double tstart = double.MinValue;
                    if (cell1.Value == null || !(cell1.Value is double))
                    {
                        MessageBox.Show("開始時刻を[00:00]形式で入力してください。");
                        return;
                    }
                    else
                    {
                        tstart = cell1.Value;
                        startTime = start.AddDays(tstart) - epoch;
                    }

                    //終了時刻作成 **********************************
                    cell1 = this.Range["C4"];
                    TimeSpan endTime = TimeSpan.MaxValue;
                    if (cell1.Value == null || !(cell1.Value is double))
                    {
                        MessageBox.Show("終了時刻を[00:00]形式で入力してください。");
                        return;
                    }
                    else
                    {
                        double tend = cell1.Value;
                        // 終了時刻が開始時刻より前の場合次の日の時刻にする
                        if (tstart >= tend)
                        {
                            end = start.AddDays(1);
                        }
                        else
                        {
                            end = start;
                        }
                        end = end.AddDays(tend);
                        end = DateTime.Parse(end.ToString("yyyy/MM/dd HH:mm") + ":59.999");
                        endTime = (end - epoch);
                        //endTime = (end.AddDays(tend) - epoch);
                    }
                    
                    sTime = Math.Floor(startTime.TotalMilliseconds).ToString();
                    eTime = Math.Floor(endTime.TotalMilliseconds).ToString();
                }
                else
                {
                    MessageBox.Show("データ挿入行からミッションを実行してください");
                    return;
                }
                
                // 再計算停止
                this.Application.Calculation = Excel.XlCalculation.xlCalculationManual;
                this.Application.ScreenUpdating = false;
                this.Application.DisplayAlerts = false;

                // ミッションをまとめて実行
                List<string> getMiList = new List<string>();
                List<int> getCol = new List<int>();
                for (int i = 4; i < 15000; i += 3) // 3 ->セルの結合数
                {
                    cell1 = this.Cells[6, i];                    
                    rng = this.Range[cell1, cell1];
                    string miName = (string)rng.Value;
                    if (!String.IsNullOrEmpty(miName))
                    {
                        if (miName == "-") continue;

                        m_userMissionDict.Add(miName, m_missionDict[miName]);
                        string[] miStr = miName.Split('.'); // 2015.07.24
                        getCol.Add(i);
                        getMiList.Add(miStr[0]);
                    }
                    else
                    {
                        Excel.Range nextCell1 = this.Cells[6, i + 3]; // 次のセル
                        string nextMi = (string)this.Range[nextCell1, nextCell1].Value;

                        if (String.IsNullOrEmpty(nextMi))
                        {
                            break;
                        }
                    }
                }

                foreach (string miName in getMiList.Distinct())
                {
                    ClsJsonData[] jsonData = null;
                    ClsUserMission.Missions mission = null;
                    foreach (ClsUserMission Missions in m_userMission)
                    {
                        if (Missions.children.Exists(x => x.name == miName))
                        {
                            mission = Missions.children.First(x => x.name == miName);
                        }
                    }

                    if (mission == null)
                    {                        
                        this.Application.ScreenUpdating = true;
                        this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                        this.Application.Calculate();
                        this.Application.DisplayAlerts = true;
                        MessageBox.Show("ミッションが存在しません(" + miName + ")");
                        return; // 処理終了
                    }
                    string missionId = mission.id;
                    jsonData = ClsJsonData.get(missionId, sTime, eTime, m_writeLog);

                    int count = 0;
                    foreach (string key in m_userMissionDict.Keys)
                    {
                        string[] miStr = key.Split('.'); // 2015.07.24
                        if (miStr.Count() > 0 && miStr[0] == miName)
                        {
                            int col = getCol[count];                            

                            this.CTMWriteSheet(key, jsonData);

                        }
                        count++;
                    }
                    jsonData = null;
                }

                //Mi登録状態表示処理
                for (int j = 4; j < 15000; j += 3) // 3 ->セルの結合数
                {
                    cell1 = this.Cells[6, j];                    
                    rng = this.Range[cell1, cell1];
                    if (!String.IsNullOrEmpty(rng.Value))
                    {
                        if (rng.Value == "-") continue;
                        this.Range[this.Cells[7, j], this.Cells[7, j]].Value = "完了";
                    }
                    else
                    {
                        Excel.Range nextCell1 = this.Cells[6, j + 3]; // 次のセル
                        string nextMi = (string)this.Range[nextCell1, nextCell1].Value;

                        if (String.IsNullOrEmpty(nextMi))
                        {
                            break;
                        }
                    }
                }

                //メインシートにデータを挿入する
                CalcurateSheet();
                
                // 更新処理
                this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                this.Application.Calculate();
                this.Application.ScreenUpdating = true;
                                
                //作業ウインドウにデータを送る
                Globals.ThisWorkbook.m_actionPanel.m_missionDict = m_userMissionDict;
                Globals.ThisWorkbook.m_actionPanel.AdditionCTMData();
                Globals.ThisWorkbook.m_actionPanel.controlClear();
                
                this.Activate();
                this.Application.DisplayAlerts = true;

                MessageBox.Show("ミッション取得処理が正常に完了しました。");
            }

            //Mi失敗時
            catch (Exception ex)
            {                
                MessageBox.Show(ex.Message);
                m_writeLog.OutPut("button_Click", ex.Message + ex.StackTrace);
                this.Application.DisplayAlerts = true;

                // 更新処理
                this.Application.ScreenUpdating = true;
                this.Application.Calculation = Excel.XlCalculation.xlCalculationAutomatic;
                this.Application.Calculate();

                //Mi登録状態表示処理
                for (int j = 4; j < 15000; j += 3) // 3 ->セルの結合数
                {
                    cell1 = this.Cells[6, j];
                    //cell2 = this.Cells[6, j + 3];
                    rng = this.Range[cell1, cell1];
                    if (!String.IsNullOrEmpty(rng.Value))
                    {
                        if (rng.Value == "-") continue;
                        this.Range[this.Cells[7, j], this.Cells[7, j]].Value = "処理中断";
                    }
                    else
                    {
                        Excel.Range nextCell1 = this.Cells[6, j + 3]; // 次のセル
                        string nextMi = (string)this.Range[nextCell1, nextCell1].Value;

                        if (String.IsNullOrEmpty(nextMi))
                        {
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// ミッション結果の書き込みを行う
        /// </summary>
        private void CTMWriteSheet(string miName, ClsJsonData[] jsonData)
        {
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;

            try
            {                
                int actMission_count = 0;
                int MissionValue_count = 0;
                //CTMを列挙
                string str = (actMission_count).ToString();             

                bool headderFlag = true;
                //CTMを列挙
                foreach (ClsJsonData CTM in jsonData)
                {
                    string str2 = (MissionValue_count).ToString();                    
                    //ミッションから対象のCTMを探す　見つかった場合のみ処理を行う                    
                    if (CTM.name == miName.Split('.')[1]) // 2015.07.24
                    {
                        //新規シートの作成
                        string SheetName = miName;
                        for (int i = 1; i <= Globals.ThisWorkbook.Worksheets.Count; i++)
                        {
                            if (SheetName == Globals.ThisWorkbook.Worksheets[i].Name)
                            {
                                Globals.ThisWorkbook.Worksheets[i].Delete();
                            }
                        }
                        Excel.Worksheet newWorksheet;
                        newWorksheet = (Excel.Worksheet)Globals.ThisWorkbook.Worksheets.Add(After: Globals.ThisWorkbook.Worksheets[Globals.ThisWorkbook.Worksheets.Count]);
                        newWorksheet.Name = SheetName;                        
                        newWorksheet.Select(1);

                        //CTMが上がっていなかった場合はループを抜ける
                        if (CTM.ctms.Count == 0)
                        {
                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            break;
                        }

                        //シート内にエレメントを挿入
                        int rowCount = 2;
                        int colCount = 0;
                        rng = (Excel.Range)Globals.ThisWorkbook.Application.Selection;
                        DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                        ClsCTMList myCTM = m_missionDict[miName];
                        rng[1, 1] = "No.";
                        //ISSUE_NO.748 Sunyi 2018/08/13 Start
                        //rng[1, 2] = "収集時刻";
                        rng[1, 2] = "RECEIVE TIME";
                        //ISSUE_NO.748 Sunyi 2018/08/13 End

                        //CTMを時間で分けて列挙
                        foreach (ClsJsonData.Ctms ctms in CTM.ctms)
                        {
                            
                            colCount = 3;
                            rng[rowCount, 1] = rowCount - 1;                            
                            rng[rowCount, 2] = epoch.AddMilliseconds(ctms.RT).ToString("M/d HH:mm:ss");

                            //CTM内のエレメントを列挙
                            foreach (string EleID in ctms.EL.Keys) // 1レコードのエレメント毎
                            {
                                
                                if (headderFlag)
                                {
                                    if (myCTM.children != null)
                                    {
                                        foreach (ClsCTMList.Children1 element in myCTM.children)
                                        {

                                            //動的に列を追加
                                            if (element != null && element.type == "el")
                                                //if (element != null && element.children != null)
                                            {
                                               if (element.id == EleID)
                                                {
                                                    //string elementName = element.children.Find(x => x.id == EleID).displayName[0].text;
                                                    string elementName = element.displayName[0].text;
                                                    rng[1, colCount] = elementName;
                                                    break;
                                                }
                                            }
                                        }
                                    
                                    }

                                    
                                }

                                if (ctms != null && ctms.EL != null && ctms.EL.Count > 0 && ctms.EL.ContainsKey(EleID) && ctms.EL[EleID] != null)
                                {

                                    if (String.IsNullOrEmpty(ctms.EL[EleID].FN))
                                    {
                                        rng[rowCount, colCount] = ctms.EL[EleID].V;
                                    }
                                    else
                                    {
                                        // バルキーファイル
                                        rng[rowCount, colCount] = ctms.EL[EleID].FN;
                                    }

                                }

                                colCount++;
                            }
                            headderFlag = false;
                            rowCount++;
                        }

                        //エレメントの計算値入力                        
                        rng[rowCount, 2] = "個数";

                        rng[rowCount + 1, 2] = "積算";
                        rng[rowCount + 2, 2] = "平均";
                        rng[rowCount + 3, 2] = "最大";
                        rng[rowCount + 4, 2] = "最小";
                        rng[rowCount + 5, 2] = "種類数";

                        for (int i = 3; i < colCount; i++)
                        {                        
                            string ColLetterTemp = newWorksheet.Cells[rowCount, i].Address;
                            string[] SplitLetter = ColLetterTemp.Split('$');
                            string ColLetter = SplitLetter[1];
                            rng[rowCount, i].Formula = string.Format("=COUNTA({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 1, i].Formula = string.Format("=SUM({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}2:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            rng[rowCount + 3, i].Formula = string.Format("=MAX({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 4, i].Formula = string.Format("=MIN({0}2:{0}{1})", ColLetter, rowCount - 1);                        
              
                        }
                        //罫線表示処理
                        cell1 = newWorksheet.Cells[1, 1];
                        cell2 = newWorksheet.Cells[rowCount - 1, 1];
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        cell1 = newWorksheet.Cells[1, 2];
                        cell2 = newWorksheet.Cells[rowCount + 5, colCount - 1];
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //セルサイズを自動調整
                        newWorksheet.Columns.AutoFit();
                    }
                    MissionValue_count++;
                }

                actMission_count++;

            }
            catch (Exception ex)
            {                
                MessageBox.Show(ex.Message);
                m_writeLog.OutPut("CTMSheetWrite", ex.Message + ex.StackTrace);
                Globals.ThisWorkbook.Application.DisplayAlerts = true;
            }


        }



        /// <summary>
        /// メインシートデータ挿入処理
        /// </summary>
        private void CalcurateSheet()
        {
            Excel.Range rng = null;
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;

            try
            {
                cell1 = this.Cells[m_activeRange.Row, 2];

                for (int i = 8; i <= 15000; i++)
                {
                    //演算行の位置を探す
                    cell1 = this.Cells[i, 2];
                    if (!(cell1.Value is string))
                    {
                        continue;
                    }
                    if (!(cell1.Value is double) && cell1.Value == "演算行/定数")
                    {
                        rng = this.Range[cell1, cell1].EntireRow;
                        break;
                    }
                }

                //演算行が見つからなかった場合は処理を抜ける
                if (rng == null)
                {
                    return;
                }

                //行位置の基準を設定する
                int rowPoint = rng.Row;

                //演算行の各セルを見て演算を行う
                for (int i = 3; i <= 15000; i++)
                {
                    /*
                     * 検索する文字列の形式は"シート名,列名,行名"+(x * "シート名,列名,行名")　の形式
                     */
                    cell1 = this.Cells[rowPoint, i];
                    cell2 = this.Cells[m_activeRange.Row, i];

                    bool dataNothingChk = false; // データシートにデータがあるかチェック
                    try
                    {
                        if (cell1.Value != null)
                        {
                            string compStr = cell1.Value.ToString();
                            compStr = compStr.Replace("\"", "?\"?");
                            compStr = compStr.Replace("\"", "?\"?");
                            string[] culcStr = compStr.Split('?');

                            //シート検索フォーマットが入力されていた場合、計算を行う
                            if (culcStr.Length > 1)
                            {
                                for (int j = 0; j < culcStr.Length; j++)
                                {
                                    string[] culcSheet = culcStr[j].Split(',');
                                    //シート検索フォーマットが入力されていた場合は以下の処理を行う
                                    if (culcSheet.Length == 3)
                                    {
                                        Excel.Worksheet culcWorkSheet = Globals.ThisWorkbook.Worksheets[culcSheet[0]];
                                        if (culcWorkSheet.Cells[1, 1].Value.ToString() == "データ無し")
                                        {
                                            dataNothingChk = true;
                                            cell2.Value = 0; // データがない場合は0を入れる
                                            break;
                                        }
                                        int col = culcWorkSheet.Rows[1].Find(culcSheet[1]).Column;
                                        int row = culcWorkSheet.Columns[2].Find(culcSheet[2]).Row;
                                        if (culcSheet[2] == "種類数")
                                        {
                                            string ColLetterTemp = culcWorkSheet.Cells[row, col].Address;
                                            string[] SplitLetter = ColLetterTemp.Split('$');
                                            string ColLetter = SplitLetter[1];
                                            culcWorkSheet.Cells[row, col].FormulaArray = string.Format("=SUM(1/COUNTIF({0}2:{0}{1},{0}2:{0}{1}))", ColLetter, row - 6);
                                        }
                                        culcStr[j] = culcWorkSheet.Cells[row, col].Value.ToString();
                                    }
                                }

                                if (dataNothingChk != true) // 取得データがあれば処理続行
                                {
                                    bool kakFlag = true;
                                    for (int j = 0; j < culcStr.Length; j++)
                                    {
                                        if (culcStr[j] == "\"")
                                        {
                                            if (kakFlag)
                                            {
                                                culcStr[j] = "(";
                                                kakFlag = false;
                                            }
                                            else
                                            {
                                                culcStr[j] = ")";
                                                kakFlag = true;
                                            }
                                        }
                                    }

                                    //この時点で数値型と四則演算式の文字列になる
                                    string resultStr = String.Join(null, culcStr);
                                    cell2.Value = "=" + resultStr;
                                }
                                
                            }
                            //フォーマットが入力されていなかった場合はエクセルの基本の式を動かす
                            else
                            {
                                cell2.Value = String.Format("=\"{0}\"", compStr);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        m_writeLog.OutPut("calcurateSheet", ex.Message + ex.StackTrace);
                    }
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("calcurateSheet", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// 【イベント】サンプル表示ボタン押下時処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDefault"></param>
        private void btn_Sample_Click(Office.CommandBarButton ctrl, ref bool CancelDefault)
        {
            try
            {
                Office.CommandBarButton btnObj = ctrl;

                if (ctrl.Caption != "サンプル表示" && ctrl.Caption != "サンプル非表示")
                {
                    return;
                }

                if (btnObj.Caption == "サンプル表示")
                {
                    Globals.Sheet2.sampleOpen();
                    btnObj.Caption = "サンプル非表示";
                }
                else
                {
                    Globals.Sheet2.sampleClose();
                    btnObj.Caption = "サンプル表示";
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("btn_Sample_Click", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// アクションパネル開閉処理
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="CancelDedfault"></param>
        private void button_Action_Click(Office.CommandBarButton ctrl, ref bool CancelDedfault)
        {
            try
            {
                if (ctrl.Caption != "アクションパネル開閉")
                {
                    return;
                }

                if (Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane)
                {
                    Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = false;
                }
                else
                {
                    Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;
                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("button_Action_Click", ex.Message + ex.StackTrace);
            }
        }


        /// <summary>
        /// ワークブック保存前イベント
        /// 次回起動時に問題が起きないように、各コントロール・入力規則を削除する
        /// </summary>
        /// <param name="SaveAsUI">名前を付けて保存かどうか</param>
        /// <param name="Cancel">Excel動作による保存フラグ</param>
        private void Workbook_BeforeSave(bool SaveAsUI, ref bool Cancel)
        {
            try
            {

                //保存先のファイルパスを指定する                
                string filePath = Path.GetDirectoryName(Globals.ThisWorkbook.Path);                
                FileSystem.ChDir(filePath);

                string fullPath = string.Empty;
                if (filePath.Contains("AIS_Template") == true)
                {
                    fullPath = filePath + @"\ExcelResult";  //上書き保存時のフォルダ比較用  
                }
                else
                {
                    fullPath = filePath + @"\AIS_Template\ExcelResult";  //上書き保存時のフォルダ比較用  
                }                
                FileSystem.ChDir(fullPath);       //移動                

                //上書き保存と名前付き保存で処理を分ける
                if (SaveAsUI)
                {                    
                    //名前を付けて保存
                    var FileName = Application.GetSaveAsFilename(Globals.ThisWorkbook.Name, "Excelファイル(*.xlsx),*.xlsx");

                    if (FileName is bool && FileName == false)  //キャンセル時はbool型が返る
                    {
                        Cancel = true;
                        return;
                    }
                    else  //保存時はファイル名が返る
                    {
                        Cancel = true;

                        //保存用コントロール初期化
                        this.SaveInitialize();

                        //セーブ実行
                        this.Application.EnableEvents = false;
                        Globals.ThisWorkbook.SaveAs(FileName);
                        this.Application.EnableEvents = true;
                        //コントロール類をもとに戻す
                        this.WorkbookAfterSave();
                    }
                }
                //名前をつけて保存時
                else
                {                    
                    //規定のフォルダか確認
                    if (Path.GetFullPath(Globals.ThisWorkbook.Path) != fullPath)
                    {
                        MessageBox.Show("このフォルダ上で上書きできません");
                        Cancel = true;
                        return;
                    }
                    else
                    {
                        Cancel = true;

                        this.SaveInitialize();

                        //セーブ実行　誤動作回避でイベントを切る
                        this.Application.EnableEvents = false;
                        Globals.ThisWorkbook.Save();
                        this.Application.EnableEvents = true;

                        //再読み込み
                        this.WorkbookAfterSave();
                    }
                }

            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("button_Action_Click", ex.Message + ex.StackTrace);
            }

        }


        /// <summary>
        /// セーブ時のコントロール初期化
        /// </summary>
        public void SaveInitialize()
        {
            //入力規則を廃棄する
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range rng = null;
            Excel.Range cell1_Mi = null;
            Excel.Range cell2_MI = null;
            Excel.Range rng_Mi = null;

            int count = 0;
            cell1 = this.Cells[6, 4];

            //ミッション選択部分の入力規則を削除する
            while (cell1.Value != null)
            {
                cell1 = this.Cells[6, 4 + count];
                cell2 = this.Cells[6, 6 + count];
                if (cell1.Value == null) break;
                rng = this.Range[cell1, cell2];
                rng.Validation.Delete();

                cell1_Mi = this.Cells[5, 4 + count];
                cell2_MI = this.Cells[5, 6 + count];
                if (cell1.Value == null) break;
                rng_Mi = this.Range[cell1_Mi, cell2_MI];
                rng_Mi.Validation.Delete();

                count += 3;
            }

            //コマンドメニューのボタン類を削除
            if (m_menuButton != null)
            {
                m_menuButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Click);
                m_menuButton.Delete();
                m_menuButton = null;
            }
            if (m_SampleButton != null)
            {
                m_SampleButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(btn_Sample_Click);
                m_SampleButton.Delete();
                m_SampleButton = null;
            }
            if (m_actionButton != null)
            {
                m_actionButton.Click -= new Office._CommandBarButtonEvents_ClickEventHandler(button_Action_Click);
                m_actionButton.Delete();
                m_actionButton = null;
            }     
           
        }

        /// <summary>
        /// ワークブック保存後再読込関数
        /// </summary>
        private void WorkbookAfterSave()
        {
            //ロードフラグオン
            flagLoading = true;
            //読込開始            
            this.InitializeSheet();
        }
    }
#endif
}
