﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.IO;

using AIStool_DAC.util.file;
using AIStool_DAC.util.subDAC;
using AIStool_DAC.util.excel;
using AIStool_DAC.util.date;


using AIStool_DAC.read;


using Excel = Microsoft.Office.Interop.Excel;
using log4net;
using System.Diagnostics;
using AIStool_DAC.util.log;
using System.Globalization;

namespace AIStool_DAC
{
    public partial class SubDACActionsPaneControl : UserControl
    {
        //private static readonly ILog logger = LogManager.GetLogger(typeof(SubDACActionsPaneControl));

        // Wang Issue NO.864 20181005 Start
        private HashSet<string> openningSubDacs = new HashSet<string>();
        public HashSet<string> GetOpenningSubDacs()
        {
            HashSet<string> removeSet = new HashSet<string>();
            foreach (string openningSubDac in openningSubDacs)
            {
                if (!ClsFileUtil.checkFileOpen(openningSubDac, false))
                {
                    removeSet.Add(openningSubDac);
                }
            }

            foreach (string removeSubDac in removeSet) {
                openningSubDacs.Remove(removeSubDac);
            }

            return openningSubDacs;
        }
        // Wang Issue NO.864 20181005 End

        public SubDACActionsPaneControl()
        {
            InitializeComponent();
            this.fileListPanel.AllowDrop = true;

            // SubDacファイルリストの初期化
            this.fileListPanel.Controls.Clear();

        }        
        public Label getLblDate()
        {
            return this.lblDate;
        }

        /**
         * ドラッグ＆ドロップしたファイルが正しいか確認
         * １．日付選択
         * ２．ファイル数
         * ３．拡張子
         * ４．AIS
         * ５．ファイル種別
         * ６．同一ファイル
         */
        private FoaCoreCom.dac.model.DacManifest.ManifestFileInfo chkDropFile(string[] fileFullPaths)
        {
            //Dac_Home sunyi 20190703 start
            Excel.Worksheet worksheet = Globals.ThisWorkbook.Sheets[ClsReadData.AIS_PARAM_SHEET_NAME];
            Excel.Range cellsCondition = worksheet.Cells;
            /*
            for (int i = 1; i <= 30; i++)
            {
                if (cellsCondition[i, 1].Value == null) continue;
                //R2以外は第二階層に貼り付けない
                
                if (cellsCondition[i, 1].Value == "OpenFromR2")
                {
                    if (cellsCondition[i, 2].Value == null)
                    {
                        MessageBox.Show("SubDACへの貼り付けはR2 Viewで行ってください",
                            //"エラー",
                            //MessageBoxButtons.OK,
                            //MessageBoxIcon.Error);
                                "警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                        return null;
                    }
                }
                
            }
            */
            //Dac_Home sunyi 20190703 start

            //DAC-54,55 sunyi 20190117 start
            // １．日付選択の確認
            if (lblDate.Text != null && lblDate.Text.Equals(""))
            {
                MessageBox.Show("日付が選択されていません。\n" +
                                "日付を選択してからファイルをドラッグ＆ドロップしてください",
                                //"エラー",
                                //MessageBoxButtons.OK,
                                //MessageBoxIcon.Error);
                                "警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return null;
            }


            // ２．ドロップされたファイル数の確認
            if (fileFullPaths.Length != 1)
            {
                MessageBox.Show("複数ファイルをドラッグ＆ドロップしています。\n" +
                                "１ファイルづつドラッグ＆ドロップしてください",
                                //"エラー",
                                //MessageBoxButtons.OK,
                                //MessageBoxIcon.Error);
                                "警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return null;
            }

            // ファイル名とファイルフルパスの取得
            string fileFullPath = fileFullPaths[0];
            string fileName = Path.GetFileName(fileFullPaths[0]);
            Globals.ThisWorkbook.workbookLog.OutPut("fileFullPath: ", fileFullPath, ClsMissionLog.LOG_LEVEL.DEBUG);
            Globals.ThisWorkbook.workbookLog.OutPut("fileName: ", fileName, ClsMissionLog.LOG_LEVEL.DEBUG);
            string filewithoutext = Path.GetFileNameWithoutExtension(fileFullPaths[0]);
            string ext = Path.GetExtension(fileFullPaths[0]);
            string destPath = Globals.ThisWorkbook.Path;
            string newfileName = fileName;
            int i = 0;
            string newdestPath = Path.Combine(destPath, newfileName);
            while(File.Exists(newdestPath))
            {
                i++;
                newfileName = filewithoutext + "(" + i + ")" + ext;
                newdestPath = Path.Combine(destPath, newfileName);
            }
            Globals.ThisWorkbook.workbookLog.OutPut("newdestPath: ", newdestPath, ClsMissionLog.LOG_LEVEL.DEBUG);            
            if (newdestPath.Equals(Globals.ThisWorkbook.FullName))
            {
                return null;
            }
            File.Copy(fileFullPath, newdestPath);

            // 同一ディレクトリ内のD&Dは無視する

            string dacDir = Globals.ThisWorkbook.clsDACConf.DacDir;
            if (fileFullPath.StartsWith(dacDir))
            {
                return null;
            }
                        
            
             System.Globalization.CultureInfo cultureInfo = System.Globalization.CultureInfo.CreateSpecificCulture("ja-JP");

            // ３．ファイルの拡張子の確認
#if false
            if (
                    !fileName.EndsWith(".xls",true,cultureInfo)
                && !fileName.EndsWith(".xlsx",true,cultureInfo)
                && !fileName.EndsWith(".xlsm",true,cultureInfo)
            )
            {
                MessageBox.Show("ファイルの拡張子が異なります。\n" +
                                "正しいExcleファイルをドラッグ＆ドロップしてください。",
                                //"エラー",
                                //MessageBoxButtons.OK,
                                //MessageBoxIcon.Error);
                                "警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return null;
            }

#endif
            // ４．AISからのD&Dか確認
            //logger.Info("orig check" + Globals.ThisWorkbook.clsDACConf.AisRegDir);
            //Globals.ThisWorkbook.workbookLog.OutPut("orig check: ", Globals.ThisWorkbook.clsDACConf.AisRegDir, ClsMissionLog.LOG_LEVEL.DEBUG);
#if false
            if (!fileFullPath.StartsWith(Globals.ThisWorkbook.clsDACConf.AisRegDir, true, cultureInfo))
            {
                MessageBox.Show(//"AISのアウトプットファイルをドラッグ＆ドロップしてください。",
                                "AIS(集計表)またはDACのアウトプットファイルをドラッグ＆ドロップ \n" +
                                "してください。",
                                //"エラー",
                                //MessageBoxButtons.OK,
                                //MessageBoxIcon.Error);
                                "警告",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Warning);
                return null;
            }
#endif
            string templateName = "";
            // ５．ファイル種別の確認
            if (
                    !fileName.EndsWith(".xls", true, cultureInfo)
                && !fileName.EndsWith(".xlsx", true, cultureInfo)
                && !fileName.EndsWith(".xlsm", true, cultureInfo)
                && !fileName.EndsWith(".dac", true, cultureInfo)
            )
            {
            }
            else
            {
               
                //templateName = ClsSubDACUtil.getTemplateName(fileFullPath);
                int index = fileName.IndexOf('_');
                if (index > 0)
                {
                    templateName = fileName.Substring(0, index);
                }
                //logger.Debug("Template: " + templateName);
                Globals.ThisWorkbook.workbookLog.OutPut("Template: ", templateName, ClsMissionLog.LOG_LEVEL.DEBUG);                
                if (templateName.Equals(Properties.Resources.TEXT_STD_MULTIDAC) ||fileName.EndsWith(".dac", true, cultureInfo))
                {
                    //dac home 
                    MessageBox.Show("DACファイルは追加できません。",
                        //"エラー",
                        //MessageBoxButtons.OK,
                        //MessageBoxIcon.Error);
                        "警告",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);

                    return null;
                    //第2階層まで制限する
                    /*
                    if (Globals.ThisWorkbook.clsSubDACControler.Hierarchy >= 2)
                    {
                        MessageBox.Show("DACファイルは2階層以降は追加できません。",
                        //"エラー",
                        //MessageBoxButtons.OK,
                        //MessageBoxIcon.Error);
                        "警告",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);

                        return null;

                    }
                    */
                }

            }
            // SubDAC管理クラスを作成
            //ClsSubDACFile subDacFile = Globals.ThisWorkbook.clsSubDACControler.createSubDACFile(templateName, fileName);
            FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subDacFile = Globals.ThisWorkbook.clsSubDACControler.createSubDACFile(newdestPath);
            Globals.ThisWorkbook.workbookLog.OutPut("chkDropFile: ", subDacFile == null ? "null":subDacFile.name, ClsMissionLog.LOG_LEVEL.DEBUG);

            // ６．同一ファイルの確認
            /*
            if (Globals.ThisWorkbook.clsSubDACFileList.Contains(subDacFile))
            {
                MessageBox.Show("同一Excleファイルがすでにドラッグ＆ドロップされています。",
                //"エラー",
                //MessageBoxButtons.OK,
                //MessageBoxIcon.Error);
                "警告",
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning);

                return null;
            }
            */
            //DAC-54,55 sunyi 20190117 end

            Globals.ThisWorkbook.workbookLog.OutPut("subDacFile: ", subDacFile, ClsMissionLog.LOG_LEVEL.DEBUG);
            return subDacFile;
        }
        /*
        private void copySubDACFile(string fromFile, ClsSubDACFile subDacFile)
        {
            // ディレクトリの作成
            ClsFileUtil.createDirectory(subDacFile.RootPath + lblDate.Text.Replace("/", ""));
            string extension = Path.GetExtension(fromFile);
            string toFile = "";
            if (subDacFile.Kind.Equals(Properties.Resources.TEXT_STD_MULTIDAC) || 
                subDacFile.Kind.Equals(Properties.Resources.TEXT_STD_DAC) ||
                subDacFile.Kind.Equals(Properties.Resources.TEXT_STD_SPREADSHEET))
            {
                toFile = subDacFile.RootPath + lblDate.Text.Replace("/", "") + "\\" + subDacFile.Name + "_" + ClsReadData.UNEXEC_SUB_DAC_FILE_KEY + "_" + lblDate.Text.Replace("/", "") + ".xlsm";
            }
            else
            {
                toFile = subDacFile.RootPath + "\\" + subDacFile.Name + extension;
            }
            //logger.Debug("Copy File: " + fromFile + " to " + toFile);
            Globals.ThisWorkbook.workbookLog.OutPut("Copy File: ", fromFile + " to " + toFile, ClsMissionLog.LOG_LEVEL.DEBUG);
            // ファイルのコピー
            bool result = ClsFileUtil.copyFile(fromFile, toFile);
            //logger.Debug("Copy File: " + result);
            Globals.ThisWorkbook.workbookLog.OutPut("Copy File: ", "result: " + result, ClsMissionLog.LOG_LEVEL.DEBUG);
        }
        */
        /**
         * SubDacファイルリストから並び順（ソート）でClsSubDacFileクラスを取得
         */
        /*
        private ClsSubDACFile getSubDACBySort(double sort)
        {
            foreach (ClsSubDACFile subDac in Globals.ThisWorkbook.clsSubDACFileList)
            {
                if (subDac.Sort == sort)
                {
                    return subDac;
                }
            }

            return null;

        }
        */
        private FoaCoreCom.dac.model.DacManifest.ManifestFileInfo getSubDACBySort(double sort)
        {
            foreach (var subDac in Globals.ThisWorkbook.clsSubDACControler.manifest.fileList)
            {
                if (subDac.index == sort)
                {
                    return subDac;
                }
            }
            return null;
        }
        /**
         * Excelファイルを開く
         * 
         */
        // Wang Issue NO.864 20181005 Start
        /*
        private void openExcel(string textFileName){

            // 日付・SubDACファイル名（日付なし）を取得
            string date = lblDate.Text;

            // リストからClsSubDACFileを取得
            ClsSubDACFile subDacFile = Globals.ThisWorkbook.clsSubDACControler.getSubDACByName(textFileName);

            // ファイルが存在する場合のみファイルを開く。
            if (subDacFile.isExce(date))
            {
                if (subDacFile.Kind.Equals("DAC"))
                {
                    ClsDateUtil.setOperationDate(subDacFile, date);
                }
                ClsExcelUtil.OpenExcelProcess(subDacFile.getFullPath(date), true);

            }

        }
        */
        private void openExcel(string textFileName)
        {

            // 日付・SubDACファイル名（日付なし）を取得
            string date = lblDate.Text;
            ClsSubDACControler subDacController = Globals.ThisWorkbook.clsSubDACControler;
            // リストからClsSubDACFileを取得
            Globals.ThisWorkbook.workbookLog.OutPut("openExcel", subDacController == null ? "null" : subDacController.BasePath, ClsMissionLog.LOG_LEVEL.DEBUG);
            Globals.ThisWorkbook.workbookLog.OutPut("openExcel", "textFileName: "+ textFileName, ClsMissionLog.LOG_LEVEL.DEBUG);
            FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subDacFile = Globals.ThisWorkbook.clsSubDACControler.getSubDACByName(textFileName);
            Globals.ThisWorkbook.workbookLog.OutPut("openExcel", subDacFile == null ? "null" : subDacFile.name, ClsMissionLog.LOG_LEVEL.DEBUG);
            if (subDacFile.template.Equals(Properties.Resources.TEXT_STD_MULTIDAC) || 
                subDacFile.template.Equals(Properties.Resources.TEXT_STD_SPREADSHEET)||
                subDacFile.template.Equals(Properties.Resources.TEXT_STD_DAC))
            {
                // ファイルが存在する場合のみファイルを開く。
                if (System.IO.File.Exists(subDacController.getFullPath(subDacFile,date)))
                {
                    if (subDacFile.template.Equals(Properties.Resources.TEXT_STD_MULTIDAC) ||
                        subDacFile.template.Equals(Properties.Resources.TEXT_STD_DAC))
                    {
                        //ClsDateUtil.setOperationDate(subDacFile, date);
                    }
                    string subDacFilePath = subDacController.getFullPath(subDacFile,date);
                    // Wang Issue DAC-46 20181112 Start
                    //ClsExcelUtil.OpenExcelProcess(subDacFilePath, true);
                    ClsExcelUtil.OpenExcelProcess(subDacFilePath, true, true);
                    // Wang Issue DAC-46 20181112 End
                    if (!openningSubDacs.Contains(subDacFilePath))
                    {
                        openningSubDacs.Add(subDacFilePath);
                    }
                }
            }
            else
            {
                Process.Start(subDacController.getFullPath(subDacFile,date));
            }
        }
        // Wang Issue NO.864 20181005 End

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        /*
         * Drag時のアクション
         *      ファイルのみDrop可能にする。
         */
        private void panel1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] drags = null;
                try
                {
                    // ドラッグ中のファイルやディレクトリの取得
                    drags = (string[])e.Data.GetData(DataFormats.FileDrop);

                }
                catch
                {
                    return;
                }

                
                foreach (string d in drags)
                {
                    if (!System.IO.File.Exists(d))
                    {
                        // ファイル以外であればイベント・ハンドラを抜ける
                        return;
                    }
                }
                e.Effect = DragDropEffects.Copy;
            }
        }

        /*
         * Drag&Drop時のアクション
         *      １．ドロップ内容の確認
         *      ２．ファイルのコピー
         *      ３．SubDAC管理シートへの追加
         *      ４．Sub DACの表示
         */
        private void panel1_DragDrop(object sender, DragEventArgs e)
        {
            string[] fileFullPaths = null;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                //コントロール内にドロップされたとき実行される
                //ドロップされたすべてのファイル名を取得する
                fileFullPaths = (string[])e.Data.GetData(DataFormats.FileDrop, false);
            }
            else
            {
                MessageBox.Show("");
                return;
            }

             /*
             * １．ドロップ内容の確認
             */
            FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subDACFile = chkDropFile(fileFullPaths);
            if (null == subDACFile)
            {
                return;
            }
            /*
            if (!Directory.Exists(Path.GetDirectoryName(Globals.ThisWorkbook.clsSubDACControler.SubDACControlText)))
            {

                Directory.CreateDirectory((Path.GetDirectoryName(Globals.ThisWorkbook.clsSubDACControler.SubDACControlText)));
            }
            if (!File.Exists(Globals.ThisWorkbook.clsSubDACControler.SubDACControlText))
            {
                using (StreamWriter sw = new StreamWriter(Globals.ThisWorkbook.clsSubDACControler.SubDACControlText, false, System.Text.Encoding.GetEncoding("shift_jis")))
                {
                    sw.WriteLine("DAC名,テンプレート種別,ファイル名（拡張子あり）,並び順");
                }
            }
            */
            //２．ファイルのコピー
            //copySubDACFile(fileFullPaths[0], subDACFile);

            //３．SubDAC管理シートへの追加
            Globals.ThisWorkbook.clsSubDACControler.addSubDACFile(subDACFile);

            //４．Sub DACの表示
            printSubDAC();
        }






        /**
         * Sub DACファイル一覧を表示する。
         */
        public void printSubDAC(){

            int oneFilePanelXSize = 130;
            int oneFilePanelYSize = 130;
            try
            {
                // 表示一覧Listを空にする
                this.fileListPanel.Controls.Clear();

                // 日付が選択されていない場合は、ファイルリストを表示しない
                if (lblDate == null || lblDate.Text.Equals(""))
                {
                    //logger.Debug("lblDate is Null");
                    Globals.ThisWorkbook.workbookLog.OutPut("printSubDAC", "lblDate is Null", ClsMissionLog.LOG_LEVEL.DEBUG);
                    return;
                }
                if (Globals.ThisWorkbook.clsSubDACControler.manifest.fileList.Count > 1)
                {
                    Array clsSubDACFileArray = Globals.ThisWorkbook.clsSubDACControler.manifest.fileList.GetRange(1, Globals.ThisWorkbook.clsSubDACControler.manifest.fileList.Count - 1).ToArray();
                    Array.Sort(clsSubDACFileArray);
                    ClsSubDACControler subDACController = Globals.ThisWorkbook.clsSubDACControler;
                    Globals.ThisWorkbook.workbookLog.OutPut("printSubDAC", "clsSubDACFileArraySize = " + clsSubDACFileArray.Length, ClsMissionLog.LOG_LEVEL.DEBUG);
                    //３．ファイルリスト用パネルにファイルの一覧を表示する。
                    foreach (FoaCoreCom.dac.model.DacManifest.ManifestFileInfo subDACfile in clsSubDACFileArray)
                    {
                        Globals.ThisWorkbook.workbookLog.OutPut("printSubDAC", subDACController.getFullPath(subDACfile, lblDate.Text), ClsMissionLog.LOG_LEVEL.DEBUG);
                        Globals.ThisWorkbook.workbookLog.OutPut("printSubDAC", subDACfile.extension, ClsMissionLog.LOG_LEVEL.DEBUG);
                        string[] date = getLblDate().Text.Split('/');
                        int now = Int32.Parse(date[2]);

                        // すでに削除されているため、処理対象外になる。
                        
                        if (!string.IsNullOrEmpty(subDACfile.deletedDate))
                        {
                            IFormatProvider formatProvider = CultureInfo.InvariantCulture;
                            DateTime currentDate = DateTime.ParseExact(getLblDate().Text, "yyyy/MM/dd", formatProvider);
                            DateTime deletedDate = DateTime.ParseExact(subDACfile.deletedDate, "yyyy/MM/dd", formatProvider);
                            if (currentDate > deletedDate)
                            {
                                continue;
                            }
                        }
                        
                        // ３－１．１ファイル用のパネル＋Excel画像＋テキストを作成
                        Panel oneFilePanel = new Panel();
                        TextBox fileText = new TextBox();
                        PictureBox filePicture = new PictureBox();

                        // 
                        // oenFilePanel（１ファイル用パネル）
                        // 
                        // 背景色
                        if (!subDACfile.template.Equals(Properties.Resources.TEXT_STD_MULTIDAC)
                            && !subDACfile.template.Equals(Properties.Resources.TEXT_STD_SPREADSHEET)
                            && !subDACfile.template.Equals(Properties.Resources.TEXT_STD_DAC))
                        {
                            oneFilePanel.BackColor = ClsReadData.EXCEC_SUB_DAC_COLOR;
                            // 右クリックメニュー
                            oneFilePanel.ContextMenuStrip = this.fileContextMenu;
                        }
                        else
                        {
                            if (System.IO.File.Exists(subDACController.getFullPath(subDACfile, lblDate.Text)))
                            {
                                oneFilePanel.BackColor = ClsReadData.EXCEC_SUB_DAC_COLOR;
                                // 右クリックメニュー
                                oneFilePanel.ContextMenuStrip = this.fileContextMenu;
                            }
                            else
                            {
                                oneFilePanel.BackColor = ClsReadData.UNEXCEC_SUB_DAC_COLOR;
                                // 右クリックメニュー
                                oneFilePanel.ContextMenuStrip = this.noFileContextMenu;
                            }
                        }
                        // 名前
                        oneFilePanel.Name = "oneFilePanel";
                        // パネルサイズ
                        oneFilePanel.Size = new System.Drawing.Size(oneFilePanelXSize, oneFilePanelYSize);
                        // ファイルフルパス管理用のテキスト
                        oneFilePanel.Text = (subDACfile.displayname);
                        // パネルへの追加
                        this.fileListPanel.Controls.Add(oneFilePanel);
                        Globals.ThisWorkbook.workbookLog.OutPut("printSubDAC", "fileListPanel Count: " + this.fileListPanel.Controls.Count, ClsMissionLog.LOG_LEVEL.DEBUG);

                        //
                        // イベントの追加
                        //
                        // ダブルクリック
                        //if (!subDACfile.Kind.Equals("DAC") && !subDACfile.Equals("集計表"))
                        //{
                        //    oneFilePanel.DoubleClick += new System.EventHandler(this.OneFilePanel_DoubleClick);
                        //}
                        oneFilePanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OneFilePanel_MouseDown);



                        // 
                        // fileText（１ファイル名用テキストボックス）
                        // 
                        // 名前
                        fileText.Name = "fileText";
                        // oneFilePanel（１ファイル用パネル）内の表示位置を設定
                        fileText.Location = new System.Drawing.Point(4, 58);
                        // フォント
                        fileText.Font = new System.Drawing.Font("MS UI Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
                        // 複数行
                        fileText.Multiline = true;
                        // サイズ
                        fileText.Size = new System.Drawing.Size(120, 50);
                        // テキストボックス内のテキスト表示位置
                        fileText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
                        // リードオンリー
                        fileText.ReadOnly = true;
                        // テキストにファイルのフルパスを表示
                        fileText.Text = (subDACfile.displayname);
                        // パネルへの追加
                        oneFilePanel.Controls.Add(fileText);

                        //
                        // イベントの追加
                        //
                        // ダブルクリック
                        fileText.DoubleClick += new System.EventHandler(this.FileText_DoubleClick);

                        // 
                        // filePicture（１ファイルExcel画像用）
                        // 
                        // 名前
                        filePicture.Name = "filePicture";
                        // 画像ファイル
                        if (subDACfile.extension.Equals(".xlsx", StringComparison.InvariantCultureIgnoreCase) ||
                            subDACfile.extension.Equals(".xls", StringComparison.InvariantCultureIgnoreCase) ||
                            subDACfile.extension.Equals(".xlsm", StringComparison.InvariantCultureIgnoreCase))
                        {
                            filePicture.Image = global::AIStool_DAC.Properties.Resources.xlsx;
                        }
                        else if (subDACfile.extension.Equals(".docx", StringComparison.InvariantCultureIgnoreCase) ||
                            subDACfile.extension.Equals(".doc", StringComparison.InvariantCultureIgnoreCase) ||
                            subDACfile.extension.Equals(".docm", StringComparison.InvariantCultureIgnoreCase))
                        {
                            filePicture.Image = global::AIStool_DAC.Properties.Resources.docx;
                        }
                        else if (subDACfile.extension.Equals(".pptx", StringComparison.InvariantCultureIgnoreCase) ||
                           subDACfile.extension.Equals(".ppt", StringComparison.InvariantCultureIgnoreCase) ||
                           subDACfile.extension.Equals(".pptm", StringComparison.InvariantCultureIgnoreCase))
                        {
                            filePicture.Image = global::AIStool_DAC.Properties.Resources.pptx;
                        }
                        else if (subDACfile.extension.Equals(".pdf", StringComparison.InvariantCultureIgnoreCase))
                        {
                            filePicture.Image = global::AIStool_DAC.Properties.Resources.pdf;
                        }
                        else if (subDACfile.extension.Equals(".zip", StringComparison.InvariantCultureIgnoreCase))
                        {
                            filePicture.Image = global::AIStool_DAC.Properties.Resources.zip;
                        }
                        else
                        {
                            filePicture.Image = global::AIStool_DAC.Properties.Resources.others;
                        }
                        // oneFilePanel（１ファイル用パネル）内の表示位置を設定
                        filePicture.Location = new System.Drawing.Point(37, 4);
                        // サイズ
                        filePicture.Size = new System.Drawing.Size(50, 50);
                        // ファイルフルパス管理用のテキスト
                        filePicture.Text = (subDACfile.displayname);
                        // パネルへの追加
                        oneFilePanel.Controls.Add(filePicture);

                        //
                        // イベントの追加
                        //
                        // ダブルクリック
                        filePicture.DoubleClick += new System.EventHandler(this.FilePicture_DoubleClick);


                    }
                }
            }
            catch (Exception e)
            {
                Globals.ThisWorkbook.workbookLog.OutPut("PrintSubDAC", Globals.ThisWorkbook.FullName + ":" + e.StackTrace, ClsMissionLog.LOG_LEVEL.DEBUG);
            }
        }

        /*
         * ダブルクリック処理
         */
        private void OneFilePanel_DoubleClick(object sender, EventArgs e)
        {
            string str = ((Panel)sender).Text;
            openExcel(str);
        }
        private void FileText_DoubleClick(object sender, EventArgs e)
        {
            string str = ((TextBox)sender).Text;
            openExcel(str);
        }
        private void FilePicture_DoubleClick(object sender, EventArgs e)
        {
            string str = ((PictureBox)sender).Text;
            openExcel(str);
        }



        /**
         * 日付ラベル変更時
         *　　ファイルリストを再表示する
         */
        private void lblDate_TextChanged(object sender, EventArgs e)
        {
            Globals.ThisWorkbook.workbookLog.OutPut("lblDate_TextChanged: ", lblDate.Text, ClsMissionLog.LOG_LEVEL.DEBUG);
            printSubDAC();
        }

        /**
         * １ファイルパネル + 右クリック + 開く
         *　　選択されたファイルを開く
         */
        private void openMenuItem_Click(object sender, EventArgs e)
        {
            // 右クリック元のパネル情報の取得
            Control source = fileContextMenu.SourceControl;
            Panel panel = (Panel)source;
            openExcel(panel.Text);
        }

        /**
         * １ファイルパネル + 右クリック + 削除
         *　　選択された１ファイルのみを物理削除する
         *　　
         */
        private void deleteMenuItem_Click(object sender, EventArgs e)
        {
            // 右クリック元のパネル情報の取得
            Control source = null;

            source = fileContextMenu.SourceControl;
            if (null == source)
            {
                source = noFileContextMenu.SourceControl;
            }

            Panel panel = (Panel)source;
            ClsSubDACControler subDACController = Globals.ThisWorkbook.clsSubDACControler;
            // 該当ファイルの管理クラスを取得
            FoaCoreCom.dac.model.DacManifest.ManifestFileInfo clsSubDACFile = Globals.ThisWorkbook.clsSubDACControler.getSubDACByName(panel.Text);
            //string[] date = getLblDate().Text.Split('/');
            IFormatProvider provider = CultureInfo.InvariantCulture;
            string format = "yyyy/MM/dd";
            DateTime dateTime = DateTime.ParseExact(getLblDate().Text, format, provider);
            
            Globals.ThisWorkbook.clsSubDACControler.deleteSubDACFileFromTime(clsSubDACFile.guid, dateTime);
            // 該当日付の１つの物理ファイルを削除する
            ClsFileUtil.deleteOneFile(subDACController.getFullPath(clsSubDACFile,lblDate.Text));

            // 削除日付を設定する
            
            //clsSubDACFile.deleteDate = Int32.Parse(date[2]);

            // 再表示をかける。
            printSubDAC();
        }

        /**
         * １ファイルパネル + 右クリック + 管理対象から除外
         *　　選択された１ファイルのみを物理削除する
         *　　Sub DAC管理シートから1行削除する。
         *　　
         */
        private void DelelteControlMenuItem_Click(object sender, EventArgs e)
        {
            // 右クリック元のパネル情報の取得
            Control source = null;
            source = fileContextMenu.SourceControl;
            if (source == null)
            {
                source = noFileContextMenu.SourceControl;
            }
            Panel panel = (Panel)source;

            // 該当ファイルの管理クラスを取得
            FoaCoreCom.dac.model.DacManifest.ManifestFileInfo clsSubDACFile = Globals.ThisWorkbook.clsSubDACControler.getSubDACByName(panel.Text);

            // Sub DACファイルのリストから該当情報を削除する。
            Globals.ThisWorkbook.clsSubDACControler.deleteSubDACFile(clsSubDACFile.guid);



            // 物理ファイルも削除する。
            ClsFileUtil.deleteAllFiles(clsSubDACFile);

            // SubDAc表示を行う。
            printSubDAC();

        }


        /*
         * 1つ前へ移動を選択
         * 
         */
        private void MoveMenuItem_Click(object sender, EventArgs e)
        {
            FoaCoreCom.dac.model.DacManifest.ManifestFileInfo selectSubDAC;
            FoaCoreCom.dac.model.DacManifest.ManifestFileInfo beforSubDAC;

            // 右クリック元のパネル情報の取得
            Control source = null;
            source = fileContextMenu.SourceControl;
            if (source == null)
            {
                source = noFileContextMenu.SourceControl;
            }
            Panel panel = (Panel)source;

            // 該当ファイルの管理クラスを取得
            selectSubDAC = Globals.ThisWorkbook.clsSubDACControler.getSubDACByName(panel.Text);


            // 該当ファイルの並び順が１より大きい場合のみ前へ移動させる
            if (selectSubDAC.index > 1)
            {
                // 1つ前の並び順の管理クラスを取得
                beforSubDAC = getSubDACBySort(selectSubDAC.index-1);

                // 並び順を入れ替える
                selectSubDAC.index +=-1;
                beforSubDAC.index += 1;
            }
            printSubDAC();
        }

        private void OneFilePanel_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button != MouseButtons.Left)
            {
                return;
            }
            Panel panel = (Panel)sender;
            ClsSubDACControler subDACController = Globals.ThisWorkbook.clsSubDACControler;
            if (panel.BackColor == ClsReadData.EXCEC_SUB_DAC_COLOR)
            {

                string filePath = "";
                foreach (FoaCoreCom.dac.model.DacManifest.ManifestFileInfo clsSubDACFile in subDACController.manifest.fileList)
                {
                    if (clsSubDACFile.displayname.Equals(panel.Text))
                    {
                        filePath = subDACController.getFullPath(clsSubDACFile, Globals.ThisWorkbook.m_subDACActionPanel.lblDate.Text);
                        break;
                    }
                }

                string[] files = { (string)filePath };
                DataObject dataObj = new DataObject(DataFormats.FileDrop, files);

                //ドラッグを開始する
                DragDropEffects dde =
                    panel.DoDragDrop(dataObj, DragDropEffects.Copy);
            }

        }

    }

}
