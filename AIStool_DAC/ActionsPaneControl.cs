﻿#define NT_MODIFY

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Office = Microsoft.Office.Core;
using System.Data;
using Excel = Microsoft.Office.Interop.Excel;

namespace AIStool_DAC
{
#if NT_MODIFY
    public partial class ActionsPaneControl : UserControl
    {
        /// <summary>
        /// 統計対象ミッション
        /// </summary>
        private Dictionary<string, ClsAttribObject> m_missions;
        private int m_missionIndex = -1;

        /// <summary>
        /// 統計対象CTM
        /// </summary>
        private Dictionary<string, Dictionary<string, ClsAttribObject>> m_ctms;
        private int m_ctmIndex = -1;

        /// <summary>
        /// 統計対象Element
        /// </summary>
        private Dictionary<string, Dictionary<string, ClsAttribObject>> m_elements;
        private int m_elementIndex = -1;

        /// <summary>
        /// ログ出力クラス
        /// </summary>
        private ClsWriteLog m_writeLog;

        private ToolTip m_tooltip = new ToolTip();
        private System.Drawing.Point m_mousePoint = new System.Drawing.Point();
        private const string ITEM_SEPARATOR = "+-*/= \t\r\n()";
        private const string ITEM_TRIM_CHARS = " \t\"";
        private const string LINE_SEPARATOR = "\r\n";
        private const string ASSISTANT_SEPARATOR = "=";
        private string m_mouseItem = string.Empty;

        private string m_formula = string.Empty;
        private string m_assitant = string.Empty;

        private static readonly string[] ASSITANT_CAPTION = { "＋", "－" };
        private bool m_showAssitant = true;

        private readonly string[] m_calculate = { "個数", "積算", "平均", "最大", "最小", "種類数" };

        /// <summary>
        /// デフォルトコンストラクタ
        /// </summary>
        public ActionsPaneControl()
        {
            InitializeComponent();
            m_writeLog = new ClsWriteLog("ActionsPaneControl");
            CustomInitialize();
        }

        public string Formula
        {
            set
            {
                this.m_formula = value;
                this.txtFormula.Text = value;
            }
        }

        public string Assitant
        {
            set
            {
                this.m_assitant = value;
                this.txtAssistant.Text = value;
            }
        }

        public Dictionary<string, ClsAttribObject> Missions
        {
            set
            {
                this.m_missions = value;

                List<ClsAttribObject> missionList = new List<ClsAttribObject>();
                foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions)
                {
                    missionList.Add(mission.Value);
                }
                this.lbMission.DataSource = missionList;
                this.lbMission.DisplayMember = "DisplayName";
            }
        }

        public Dictionary<string, Dictionary<string, ClsAttribObject>> Ctms
        {
            set
            {
                this.m_ctms = value;
            }
        }

        public Dictionary<string, Dictionary<string, ClsAttribObject>> Elements
        {
            set
            {
                this.m_elements = value;
            }
        }

        /// <summary>
        /// 【イベント】書き込みボタンクリック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWriting_Click(object sender, EventArgs e)
        {
            //退避データ一桁から書き込み
            int tmprow = 1;

            try
            {
                Excel.Range activeCell = Globals.ThisWorkbook.Application.ActiveCell;
                int row = activeCell.Row;
                int col = activeCell.Column;
                int formulaRow = Sheet1.findFormulaRow();
                if (formulaRow == 0)
                {
                    MessageBox.Show("演算行/定数セルが見つかりません。");
                    return;
                }

                if (row != formulaRow || col <= Sheet1.FOMULAR_START_COL || col > Sheet1.FOMULAR_END_COL)
                {
                    MessageBox.Show("演算行/定数セルを選択してください。");
                }
                else
                {
                    string formula;
                    try
                    {
                        Dictionary<string, string> assistants = ParseAssistant();

                        formula = ExpandFormula(txtFormula.Text, assistants);
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("項目演算が不正である。確認してください。");
                        return;
                    }

                    //Cleanup the entire column first..
                    Excel.Range rng = null;
                    Excel.Range cell1 = null;
                    cell1 = Globals.ThisWorkbook.Sheets["退避"].Cells[1, col];
                    rng = Globals.ThisWorkbook.Sheets["退避"].Range[cell1, cell1].EntireColumn;
                    //rng.ClearContents();

                    Globals.ThisWorkbook.Application.ActiveCell.Value = txtFormula.Text;
                    Globals.ThisWorkbook.Sheets["退避"].Cells[tmprow, col].Value = formula;
                    Globals.ThisWorkbook.Sheets["退避"].Cells[tmprow + 1, col].Value = txtAssistant.Text;

                }
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("btnWriting_Click", ex.Message + ex.StackTrace);
            }
        }

        private string ExpandFormula(string origin, Dictionary<string, string> assistants)
        {
            string formula = string.Empty;

            int startIndex = -1;
            int endIndex = -1;

            for (int i = 0; i < origin.Length; i++)
            {
                char c = origin[i];

                if (ITEM_SEPARATOR.IndexOf(c) != -1)
                {
                    formula += c;
                    continue;
                }

                startIndex = i;
                endIndex = i;

                for (int j = endIndex + 1; j < origin.Length; j++)
                {
                    c = origin[j];

                    if (ITEM_SEPARATOR.IndexOf(c) == -1)
                    {
                        endIndex = j;
                    }
                    else
                    {
                        break;
                    }
                }

                string item = origin.Substring(startIndex, endIndex - startIndex + 1);

                try
                {
                    string ctmDisplayName = string.Empty;
                    string elementDisplayName = string.Empty;
                    string calculate = string.Empty;
                    string missionDisplayName = FindMissionDisplayName(item.Trim(ITEM_TRIM_CHARS.ToCharArray()),
                        ref ctmDisplayName, ref elementDisplayName, ref calculate);
                    formula += "\"" + missionDisplayName + "." + ctmDisplayName + "," + elementDisplayName + "," + calculate + "\"";
                }
                catch (Exception e)
                {
                    if (assistants.ContainsKey(item))
                    {
                        formula += "(" + ExpandFormula(assistants[item], assistants) + ")";
                    }
                    else
                    {
                        Double num = 0;
                        if (Double.TryParse(item, out num))
                        {
                            formula += num;
                        }
                        else
                        {
                            throw e;
                        }
                    }
                }

                i = endIndex;
            }

            if (startIndex == -1)
            {
                throw new Exception();
            }

            return formula;
        }

        private Dictionary<string, string> ParseAssistant()
        {
            Dictionary<string, string> assistants = new Dictionary<string, string>();
            string[] lines = txtAssistant.Text.Split(LINE_SEPARATOR.ToCharArray());
            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line))
                {
                    continue;
                }

                string[] parts = line.Split(ASSISTANT_SEPARATOR.ToCharArray());
                if (parts.Length == 2)
                {
                    assistants.Add(parts[0].Trim(), parts[1].Trim());
                }
            }

            return assistants;
        }

        /// <summary>
        /// コンボボックスデータソース追加
        /// </summary>
        private void CustomInitialize()
        {
            cmbCalculate.DataSource = m_calculate;
        }

        /// <summary>
        /// ヘルプ画像フォーム
        /// </summary>
        private static Help.DACHelpInfo _helpInfo;
        /// <summary>
        /// ヘルプ表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DACHelp_picBox_Click(object sender, EventArgs e)
        {
            try
            {

                //多重起動防止
                if (_helpInfo == null || _helpInfo.IsDisposed)
                {
                    _helpInfo = new Help.DACHelpInfo();
                    _helpInfo.Show();
                }

            }
            catch (Exception ex)
            {
                // ログ出力
                m_writeLog.OutPut("DACHelp_picBox_Click", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// ヘルプ画像フォーム
        /// </summary>
        private static Help.DACHelp _help;
        /// <summary>
        /// ヘルプ画像表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNavi_Click(object sender, EventArgs e)
        {
            try
            {
                //多重起動防止
                if (_help == null || _help.IsDisposed)
                {
                    _help = new Help.DACHelp();
                    _help.Show();
                }
            }
            catch (Exception ex)
            {
                // ログ出力
                m_writeLog.OutPut("btnNavi_Click", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// ElementのDragを開始
        /// </summary>
        private void lbElement_MouseDown(object sender, MouseEventArgs e)
        {
            if (lbElement.Items.Count == 0)
                return;

            int index = lbElement.IndexFromPoint(e.X, e.Y);
            string s = lbElement.Items[index].ToString();
            DragDropEffects dde = DoDragDrop(s, DragDropEffects.All);

            if (dde == DragDropEffects.All)
            {
                lbElement.Items.RemoveAt(lbElement.IndexFromPoint(e.X, e.Y));
            }
        }

        /// <summary>
        /// Clipboardの内容をTextBoxへペストする
        /// </summary>
        private void SetElementContent(TextBox tb)
        {
            //ミッション名、CTM名、Element名、演算式を取得する
            string mission = (lbMission.SelectedItem as ClsAttribObject).DisplayName;
            string ctm = (lbCtm.SelectedItem as ClsAttribObject).DisplayName;
            string element = (lbElement.SelectedItem as ClsAttribObject).DisplayName;
            string calculate = (string)cmbCalculate.SelectedValue;

            //挿入文字："ミッション名.CTM名.Element名,演算式"
            string str = "\"" + mission + "." + ctm + "." + element + "," + calculate + "\"";

            //カーソル位置に挿入し、挿入した文字列の最後にカーソルを置く
            string prevValue = tb.Text;
            int selectIndex = tb.SelectionStart;

            if (!string.IsNullOrEmpty(prevValue) && prevValue[Math.Min(selectIndex, prevValue.Length - 1)] == '\n')
            {
                selectIndex--;
            }

            string value = prevValue.Substring(0, selectIndex) + str;
            int index = value.Length;
            value += prevValue.Substring(selectIndex);
            tb.Text = value;
            tb.SelectionStart = index;
        }

        /// <summary>
        /// Mission選択の切れ替え
        /// </summary>
        private void lbMission_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetChildrenList(this.lbMission, this.lbCtm, m_ctms, ref m_missionIndex, ref m_ctmIndex);
        }

        /// <summary>
        /// CTM選択の切れ替え
        /// </summary>
        private void lbCtm_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetChildrenList(this.lbMission, this.lbCtm, this.lbElement, m_elements, ref m_missionIndex, ref m_ctmIndex, ref m_elementIndex);
        }

        /// <summary>
        /// Missionを選択した場合はCTMリストを更新する、又は、CTMを選択した場合はElementリストを更新する
        /// </summary>
        private void SetChildrenList(ListBox plb, ListBox lb, Dictionary<string, Dictionary<string, ClsAttribObject>> objects, ref int pindex, ref int index)
        {
            //選択中項目をクリックする場合は処理しない
            if (plb.SelectedIndex == pindex)
            {
                return;
            }

            //親に対して子のDictionaryを取得する
            string pId = (plb.SelectedItem as ClsAttribObject).Id;
            Dictionary<string, ClsAttribObject> children = objects[pId];

            //子のリストを作成する
            List<ClsAttribObject> childrenList = new List<ClsAttribObject>();
            foreach (KeyValuePair<string, ClsAttribObject> child in children)
            {
                childrenList.Add(child.Value);
            }

            //リストボックスのDataSourceを設定し、表示名フィールドを指定する
            lb.DataSource = childrenList;
            lb.DisplayMember = "DisplayName";

            //リストボックスの選択項目を初期化する
            pindex = plb.SelectedIndex;
            index = -1;
            lb.SelectedIndex = 0;
        }
        private void SetChildrenList(ListBox plb1, ListBox plb2, ListBox lb, Dictionary<string, Dictionary<string, ClsAttribObject>> objects, ref int pindex1, ref int pindex2, ref int index)
        {
            //選択中項目をクリックする場合は処理しない
            if (plb1.SelectedIndex == pindex1 && plb2.SelectedIndex == pindex2)
            {
                return;
            }

            //親に対して子のDictionaryを取得する
            string pId1 = (plb1.SelectedItem as ClsAttribObject).Id;
            string pId2 = (plb2.SelectedItem as ClsAttribObject).Id;
            Dictionary<string, ClsAttribObject> children = objects[pId1 + pId2];

            //子のリストを作成する
            List<ClsAttribObject> childrenList = new List<ClsAttribObject>();
            foreach (KeyValuePair<string, ClsAttribObject> child in children)
            {
                childrenList.Add(child.Value);
            }

            //リストボックスのDataSourceを設定し、表示名フィールドを指定する
            lb.DataSource = childrenList;
            lb.DisplayMember = "DisplayName";

            //リストボックスの選択項目を初期化する
            pindex1 = plb1.SelectedIndex;
            pindex2 = plb2.SelectedIndex;
            index = -1;
            lb.SelectedIndex = 0;
        }

        /// <summary>
        /// 画面初回表示の場合は、Missionリストの選択項目を初期化する
        /// </summary>
        private void ActionsPaneControl_Paint(object sender, PaintEventArgs e)
        {
            if (-1 == m_missionIndex)
            {
                SetChildrenList(this.lbMission, this.lbCtm, m_ctms, ref m_missionIndex, ref m_ctmIndex);
            }
        }

        /// <summary>
        /// Elementを補助式にDrop処理
        /// </summary>
        private void textAssistant_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Effect == DragDropEffects.Copy)
            {
                //文字列を挿入する
                SetElementContent(txtAssistant);
            }
        }

        /// <summary>
        /// Elementを補助式にDrag開始処理
        /// </summary>
        private void textAssistant_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
            txtAssistant.Focus();
        }

        /// <summary>
        /// Elementを補助式にDrag処理
        /// </summary>
        private void textAssistant_DragOver(object sender, DragEventArgs e)
        {
            SetCursorWhenDragOver(txtAssistant, e);
        }

        /// <summary>
        /// 補助式のツールチップを表示する
        /// </summary>
        private void txtAssistant_MouseMove(object sender, MouseEventArgs e)
        {
            ShowToolTip(txtAssistant, e);
        }

        /// <summary>
        /// Mouse位置にて計算式の項を取得する
        /// </summary>
        private string GetItemByMouse(TextBox tb, System.Drawing.Point point)
        {
            string content = tb.Text;
            if (string.IsNullOrEmpty(content))
            {
                return string.Empty;
            }

            //開始、終了位置を初期化する
            int index = tb.GetCharIndexFromPosition(m_mousePoint);
            int startIndex = index;
            int endIndex = index;

            System.Drawing.Point charPoint = tb.GetPositionFromCharIndex(index);
            if (point.X > charPoint.X + 20 || point.Y > charPoint.Y + 20)
            {
                return string.Empty;
            }

            //開始位置を計算する
            for (int i = index; i > -1; i--)
            {
                if (-1 == ITEM_SEPARATOR.IndexOf(content[i]))
                {
                    startIndex = Math.Min(index, i);
                }
                else
                {
                    break;
                }
            }

            //終了位置を計算する
            for (int i = index; i < content.Length; i++)
            {
                if (-1 == ITEM_SEPARATOR.IndexOf(content[i]))
                {
                    endIndex = Math.Max(index, i);
                }
                else
                {
                    break;
                }
            }

            //項を取得する
            return content.Substring(startIndex, endIndex - startIndex + 1);
        }

        /// <summary>
        /// 補助式のツールチップを非表示する
        /// </summary>
        private void txtAssistant_MouseLeave(object sender, EventArgs e)
        {
            m_tooltip.Hide(txtAssistant);
            m_mouseItem = string.Empty;
        }

        /// <summary>
        /// Elementを項目演算にDrag開始処理
        /// </summary>
        private void txtFormula_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
            txtFormula.Focus();
        }

        /// <summary>
        /// Elementを項目演算にDrop処理
        /// </summary>
        private void txtFormula_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Effect == DragDropEffects.Copy)
            {
                SetElementContent(txtFormula);
            }
        }

        /// <summary>
        /// Elementを項目演算にDrag処理
        /// </summary>
        private void txtFormula_DragOver(object sender, DragEventArgs e)
        {
            SetCursorWhenDragOver(txtFormula, e);
        }

        /// <summary>
        /// 項目演算のツールチップを表示する
        /// </summary>
        private void txtFormula_MouseMove(object sender, MouseEventArgs e)
        {
            ShowToolTip(txtFormula, e);
        }

        /// <summary>
        /// 項目演算のツールチップを非表示する
        /// </summary>
        private void txtFormula_MouseLeave(object sender, EventArgs e)
        {
            m_tooltip.Hide(txtFormula);
            m_mouseItem = string.Empty;
        }

        /// <summary>
        /// ElementをTextBoxにDrag時、挿入位置を移動する
        /// </summary>
        private void SetCursorWhenDragOver(TextBox tb, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;

            //Mouse位置に合わせて入力カーソルを移動する
            System.Drawing.Point eventPoint = new System.Drawing.Point(e.X, e.Y);
            System.Drawing.Point clientPoint = tb.PointToClient(eventPoint);
            int index = tb.GetCharIndexFromPosition(clientPoint);
            var position = tb.GetPositionFromCharIndex(index);
            if (clientPoint.X > position.X) index++;
            tb.SelectionStart = index;
        }

        /// <summary>
        /// ツールチップでMission名を表示する
        /// </summary>
        private void ShowToolTip(TextBox tb, MouseEventArgs e)
        {
            string item = string.Empty;

            try
            {
                m_mousePoint.X = e.X;
                m_mousePoint.Y = e.Y;

                //Mouse位置にて式を取得する
                item = GetItemByMouse(tb, m_mousePoint);
                if (string.IsNullOrEmpty(item))
                {
                    m_tooltip.Hide(tb);
                    m_mouseItem = string.Empty;
                }

                //式が変更した場合、ツールチップを表示する
                if (!m_mouseItem.Equals(item))
                {
                    string ctmDisplayName = string.Empty;
                    string elementDisplayName = string.Empty;
                    string calculate = string.Empty;
                    string missonDisplayName = FindMissionDisplayName(item.Trim(ITEM_TRIM_CHARS.ToCharArray()),
                        ref ctmDisplayName, ref elementDisplayName, ref calculate);

                    m_mouseItem = item;
                    string tooltip = "\"" + ctmDisplayName + "." + elementDisplayName + "\"のミッションは\"" + missonDisplayName + "\"である";
                    m_tooltip.Show(tooltip, tb, m_mousePoint.X + 20, m_mousePoint.Y + 20);
                }
            }
            catch (Exception)
            {
                //式を認識できない場合
                if (!m_mouseItem.Equals(item) && !string.IsNullOrEmpty(item))
                {
                    m_mouseItem = item;
                    m_tooltip.Show("\"" + item + "\"認識できない", tb, m_mousePoint.X + 20, m_mousePoint.Y + 20);
                }
            }
        }

        private string FindMissionDisplayName(string item, ref string ctmDisplayName, ref string elementDisplayName, ref string calculate)
        {
#if false
            //式を分割する
            string[] parts = item.Split(',');

            if (!string.IsNullOrEmpty(item) && parts.Length != 2)
            {
                throw new Exception();
            }

            string[] displayNames = parts[0].Split('.');

            if (!string.IsNullOrEmpty(item) && parts.Length != 2)
            {
                throw new Exception();
            }

            //Calculate名を取得する
            calculate = parts[1];
            if (!m_calculate.Contains(parts[1]))
            {
                throw new Exception();
            }
            
            //CTM名を取得する
            ctmDisplayName = displayNames[0];

            //Element名を取得する
            elementDisplayName = displayNames[1];

            string ret = string.Empty;
            foreach (KeyValuePair<string, Dictionary<string, ClsAttribObject>> ctms in m_ctms)
            {
                foreach (KeyValuePair<string, ClsAttribObject> ctm in ctms.Value)
                {
                    if (ctm.Value.DisplayName.Equals(ctmDisplayName))
                    {
                        string ctmId = ctm.Value.Id;
                        string missionId = ctm.Value.Pid;

                        Dictionary<string, ClsAttribObject> elements = m_elements[missionId + ctmId];
                        foreach (KeyValuePair<string, ClsAttribObject> element in elements)
                        {
                            if (element.Value.DisplayName.Equals(elementDisplayName))
                            {
                                ret = m_missions[missionId].DisplayName;
                                return ret;
                            }
                        }
                    }
                }
            }

            throw new Exception();
#else
            //式を分割する
            string[] parts = item.Split(',');

            if (!string.IsNullOrEmpty(item) && parts.Length != 2)
            {
                throw new Exception();
            }

            string[] displayNames = parts[0].Split('.');

            if (!string.IsNullOrEmpty(item) && displayNames.Length != 3)
            {
                throw new Exception();
            }

            //Calculate名を取得する
            calculate = parts[1];
            if (!m_calculate.Contains(parts[1]))
            {
                throw new Exception();
            }

            //CTM名を取得する
            string missionDisplayName = displayNames[0];

            //CTM名を取得する
            ctmDisplayName = displayNames[1];

            //Element名を取得する
            elementDisplayName = displayNames[2];

            return missionDisplayName;
#endif
        }

        private void btnAssistant_Click(object sender, EventArgs e)
        {
            m_showAssitant = !m_showAssitant;
            if (m_showAssitant)
            {
                txtAssistant.Show();
                btnAssistant.Text = ASSITANT_CAPTION[1];
            }
            else
            {
                txtAssistant.Hide();
                btnAssistant.Text = ASSITANT_CAPTION[0];
            }
        }
    }
#else
    public partial class ActionsPaneControl : UserControl
    {
        /// <summary>
        /// ミッションとCTMを対応付けするディクショナリ
        /// </summary>
        public List<string> m_mission;


        /// <summary>
        /// ミッションとCTMを対応付けするディクショナリ
        /// </summary>
        public Dictionary<string, ClsCTMList> m_missionDict;

        /// <summary>
        /// CTMの名前
        /// </summary>
        public string m_ctmName {get;set;}

        /// <summary>
        /// ログ出力クラス
        /// </summary>
        private ClsWriteLog m_writeLog;

        private string[] m_strItems;
        private string m_strItem;

        private object m_prevFocus = null;

        /// <summary>
        /// デフォルトコンストラクタ
        /// </summary>
        public ActionsPaneControl()
        {
            InitializeComponent();
            m_writeLog = new ClsWriteLog("ActionsPaneControl");
            m_strItems = new string[] { "", "", "" };

            List<string> tempMissions = new List<string>();
            tempMissions.Add("mission#001");
            tempMissions.Add("mission#002");
            tempMissions.Add("mission#003");

            List<string> tempCtm = new List<string>();
            tempCtm.Add("Ctm#001");
            tempCtm.Add("Ctm#002");
            tempCtm.Add("Ctm#003");

            List<string> tempElement = new List<string>();
            tempElement.Add("Element#001");
            tempElement.Add("Element#002");
            tempElement.Add("Element#003");

            lbCtm.DataSource = tempCtm;
            lbElement.DataSource = tempElement;
        }

        /// <summary>
        /// 【イベント】書き込みボタンクリック処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnWriting_Click(object sender, EventArgs e)
        {
            try
            {
                Globals.Sheet1.Unprotect();
                this.controlClear();

            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("btnWriting_Click", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// コンボボックスデータソース追加
        /// </summary>
        public void AdditionCTMData()
        {
            try
            {
                string[] source = { "", "個数", "積算", "平均", "最大", "最小",　"種類数" };
                
                List<string> keys = m_missionDict.Keys.ToList<string>();
                keys.Insert(0, "");
                cmbCulculate.DataSource = source;

                lbMission.DataSource = m_mission;
            }
            catch (Exception ex)
            {
                m_writeLog.OutPut("AdditionCTMData", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// コントロールのテキスト状態を初期化する
        /// </summary>
        public void controlClear()
        {
            this.cmbCulculate.SelectedIndex = 0;
            m_strItems = new string[]{"", "", ""};
        }



        /// <summary>
        /// ヘルプ画像フォーム
        /// </summary>
        private static Help.DACHelpInfo _helpInfo;
        /// <summary>
        /// ヘルプ表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DACHelp_picBox_Click(object sender, EventArgs e)
        {
            try
            {

                //多重起動防止
                if (_helpInfo == null || _helpInfo.IsDisposed)
                {
                    _helpInfo = new Help.DACHelpInfo();
                    _helpInfo.Show();
                }
               
            }
            catch (Exception ex)
            {
                // ログ出力
                m_writeLog.OutPut("DACHelp_picBox_Click", ex.Message + ex.StackTrace);
            }
        }

        /// <summary>
        /// ヘルプ画像フォーム
        /// </summary>
        private static Help.DACHelp _help;
        /// <summary>
        /// ヘルプ画像表示
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnNavi_Click(object sender, EventArgs e)
        {
            try
            {
                //多重起動防止
                if (_help == null || _help.IsDisposed)
                {
                    _help = new Help.DACHelp();
                    _help.Show();
                }
            }
            catch (Exception ex)
            {
                // ログ出力
                m_writeLog.OutPut("btnNavi_Click", ex.Message + ex.StackTrace);
            }
        }

        private void lbElement_MouseDown(object sender, MouseEventArgs e)
        {
            if (lbElement.Items.Count == 0)
                return;

            int index = lbElement.IndexFromPoint(e.X, e.Y);
            string s = lbElement.Items[index].ToString();
            DragDropEffects dde1 = DoDragDrop(s,
                DragDropEffects.All);

            if (dde1 == DragDropEffects.All)
            {
                lbElement.Items.RemoveAt(lbElement.IndexFromPoint(e.X, e.Y));
            }
        }

        private void setElementContent(string str)
        {
            string prevValue = txtAssistant.Text;
            string value = prevValue.Substring(0, txtAssistant.SelectionStart) + str;
            int index = value.Length;
            value += prevValue.Substring(txtAssistant.SelectionStart);
            txtAssistant.Text = value;
            txtAssistant.SelectionStart = index;
        }

        private void textBox1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Effect == DragDropEffects.Copy)
            {
                string str = (string)e.Data.GetData(DataFormats.StringFormat);
                setElementContent(str);
            }
        }

        private void textBox1_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
            txtAssistant.Focus();
        }

        private void textBox1_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
            System.Drawing.Point eventPoint = new System.Drawing.Point(e.X, e.Y);
            System.Drawing.Point clientPoint = txtAssistant.PointToClient(eventPoint);

            int index = txtAssistant.GetCharIndexFromPosition(clientPoint);
            var position = txtAssistant.GetPositionFromCharIndex(index);
            if (clientPoint.X > position.X) index++;

            txtAssistant.SelectionStart = index;
        }

        private void lbElement_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void lbElement_DoubleClick(object sender, EventArgs e)
        {
        }

        private void lbElement_Enter(object sender, EventArgs e)
        {
        }

        private void lbElement_MouseDoubleClick(object sender, MouseEventArgs e)
        {
        }
    }
#endif
}
