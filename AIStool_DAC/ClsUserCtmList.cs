﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace AIStool_DAC
{
    /// <summary>
    /// ユーザーがアクセスできるCTMのIDを格納するクラス
    /// </summary>
    [DataContract]
    class ClsUserCTMList
    {
        /// <summary>
        /// CTMのID
        /// </summary>
        [DataMember]
        public string ctmId { get; private set; }

        /// <summary>
        /// CTMのIDを取得する
        /// </summary>
        /// <param name="Mid">ミッションのID</param>
        public static ClsUserCTMList[] get(string Mid)
        {
            string HostName = Globals.Sheet1.m_MissionHostName;
            string PortNo = Globals.Sheet1.m_MissionPortNo;
            WebClient wc = new WebClient();
            wc.Encoding = Encoding.UTF8;
            string url = String.Format("http://{0}:{1}/cms/rest/mib/mission/ctm?missionId={2}", HostName, PortNo, Mid);
            string ctmStr = wc.DownloadString(url);

            ClsUserCTMList[] userCtmList = JsonConvert.DeserializeObject<ClsUserCTMList[]>(ctmStr);

            return userCtmList;
        }
    }
}
