﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Http;

using GripDataRetriever;

namespace GripDataRetriever.Util
{
    public class HttpUtil
    {
        public static HttpClient getProxyHttpClient()
        {
            HttpClient client = null;

            if (GripDataRetriever.UseProxy == true)
            {

                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.Proxy = new WebProxy("http://" + GripDataRetriever.ProxyUri);
                clientHandler.UseProxy = true;
                client = new HttpClient(clientHandler);
            }
            else
            {
                client = new HttpClient();
            }

            client.Timeout = TimeSpan.FromSeconds(3000);

            return client;

        }

        public static HttpClientHandler getProxyHttpClientHandler()
        {
            HttpClientHandler clientHandler = null;

            if (GripDataRetriever.UseProxy == true)
            {

            }
            else
            {
                clientHandler = new HttpClientHandler();
            }


            return clientHandler;

        }

        public static HttpWebRequest getProxyHttpWebRequest(string url)
        {
            HttpWebRequest request = null;

            if (GripDataRetriever.UseProxy == true)
            {

            }
            else
            {
                request = (HttpWebRequest)WebRequest.Create(url);
            }


            return request;

        }

    }
}
