﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.ComponentModel;

namespace GripDataRetriever.Entity
{
    public class CmsEntity : INotifyPropertyChanged
    {
        /// <summary>
        /// Id
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 名前
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 選択
        /// </summary>
        private bool isSelected;
        public bool IsSelected { get { return isSelected; } set { isSelected = value; OnChanged("IsSelected"); } }

        /// <summary>
        /// タイプ
        /// </summary>
        public string T { get; set; }
        /// <summary>
        /// 作成ユーザID
        /// </summary>
        public string Uc { get; set; }
        /// <summary>
        /// 作成日時
        /// </summary>
        public long Dc { get; set; }
        /// <summary>
        /// 更新ユーザID
        /// </summary>
        public string Uu { get; set; }
        /// <summary>
        /// 更新日時
        /// </summary>
        public long Du { get; set; }

        /// <summary>
        /// PropertyChangedEvent
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnChanged(string prop)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }

        /// <summary>
        /// Jsonにシリアライズします。
        /// Jsonのキーは、PascalCaseからCamelCaseに変換されます。
        /// </summary>
        /// <returns></returns>
        public string ToJson()
        {
            var camelCaseFormatter = new JsonSerializerSettings();
            camelCaseFormatter.ContractResolver = new CamelCasePropertyNamesContractResolver();
            return JsonConvert.SerializeObject(this, camelCaseFormatter);
        }

        /// <summary>
        /// Jsonをパースして型パラメータのオブジェクトにデシリアライズします。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T Parse<T>(string json)
        {
            T obj = JsonConvert.DeserializeObject<T>(json);
            return obj;
        }

        /// <summary>
        /// Jsonをパースして型パラメータのオブジェクトにデシリアライズします。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="json"></param>
        /// <returns></returns>
        public static T Parse<T>(JToken json)
        {
            T obj = JsonConvert.DeserializeObject<T>(json.ToString());
            return obj;
        }
    }
}
