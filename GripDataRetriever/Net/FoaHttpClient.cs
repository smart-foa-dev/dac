﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
//using System.Web.Security;

namespace GripDataRetriever.Net
{
    /// <summary>
    /// Use HttpClient as base.
    /// Set Cookie on login.
    /// </summary>
    public class FoaHttpClient : HttpClient
    {
        /// <summary>
        /// クッキーコンテナ
        /// </summary>
        private static readonly CookieContainer cookieJar = new CookieContainer();

        /// <summary>
        /// クッキーの設定。
        /// </summary>
        /// <param name="url"></param>
        /// <param name="userInfo"></param>
        //public static void SetCookie(string url, UserInfo userInfo)
        //{
        //    Uri uri = new Uri(url);
        //    Uri trueUri = new Uri(uri.GetLeftPart(UriPartial.Authority));
        //    string userId = HttpUtility.UrlEncode(userInfo.UserId, Encoding.UTF8);
        //    cookieJar.Add(trueUri, new Cookie("userId", userId));
        //}

        public bool UseProxy { get; set; }
        public string ProxyUri { get; set; }

        public FoaHttpClient()
        {
            this.UseProxy = false;
            this.ProxyUri = "";
        }

        public FoaHttpClient(bool useProxy,string proxyUri)
        {


            if (useProxy == true && !string.IsNullOrEmpty(proxyUri))
            {
                this.UseProxy = useProxy;
                this.ProxyUri = proxyUri;
            }
            else
            {
                this.UseProxy = false;
                this.ProxyUri = "";
            }

        }

        public async Task<string> Post(string url, HttpContent content)
        {
            var baseAddress = new System.Uri(url);
            var handler = new HttpClientHandler();
            handler.CookieContainer = cookieJar;

            if (UseProxy == true)
            {
                handler.UseProxy = true;
                handler.Proxy = new WebProxy("http://" + ProxyUri);
            }
            else
            {
                handler.UseProxy = false;
            }

            var client = new HttpClient(handler);
            client.BaseAddress = baseAddress;

            var resp = await client.PostAsync(url, content);
            if (!resp.IsSuccessStatusCode)
            {
                throw new Exception("Network failure. StatusCode=" + resp.StatusCode);
            }
            var textData = await resp.Content.ReadAsStringAsync();
            return textData;

            /*
            var baseAddress = new System.Uri(url);
            using (var handler = new HttpClientHandler() { CookieContainer = cookieJar })
            using (var client = new HttpClient(handler) { BaseAddress = baseAddress })
            {
                var resp = await client.PostAsync(url, content);
                if (!resp.IsSuccessStatusCode)
                {
                    throw new Exception("Network failure. StatusCode=" + resp.StatusCode);
                }
                var textData = await resp.Content.ReadAsStringAsync();
                return textData;
            }
             */
        }

        public async Task<HttpResponseMessage> Put(string url, HttpContent content)
        {
            var baseAddress = new System.Uri(url);
            var handler = new HttpClientHandler();
            handler.CookieContainer = cookieJar;

            if (UseProxy == true)
            {
                handler.UseProxy = true;
                handler.Proxy = new WebProxy("http://" + ProxyUri);
            }
            else
            {
                handler.UseProxy = false;
            }

            var client = new HttpClient(handler);
            client.BaseAddress = baseAddress;

            var resp = await client.PutAsync(url, content);
            if (!resp.IsSuccessStatusCode)
            {
                throw new Exception("Network failure. StatusCode=" + resp.StatusCode);
            }
            return resp;

            /*
            var baseAddress = new System.Uri(url);
            using (var handler = new HttpClientHandler() { CookieContainer = cookieJar })
            using (var client = new HttpClient(handler) { BaseAddress = new System.Uri(url) })
            {
                var resp = await client.PutAsync(url, content);
                if (!resp.IsSuccessStatusCode)
                {
                    throw new Exception("Network failure. StatusCode=" + resp.StatusCode);
                }
                return resp;
            }
             */
        }

        public async Task<HttpResponseMessage> Delete(string url)
        {
            var baseAddress = new System.Uri(url);
            var handler = new HttpClientHandler();
            handler.CookieContainer = cookieJar;

            if (UseProxy == true)
            {
                handler.UseProxy = true;
                handler.Proxy = new WebProxy("http://" + ProxyUri);
            }
            else
            {

                handler.UseProxy = false;
            }

            var client = new HttpClient(handler);
            client.BaseAddress = baseAddress;

            var resp = await client.DeleteAsync(url);
            if (!resp.IsSuccessStatusCode)
            {
                throw new Exception("Network failure. StatusCode=" + resp.StatusCode);
            }
            return resp;

            /*

            var baseAddress = new System.Uri(url);
            using (var handler = new HttpClientHandler() { CookieContainer = cookieJar })
            using (var client = new HttpClient(handler) { BaseAddress = new System.Uri(url) })
            {
                var resp = await client.DeleteAsync(url);
                if (!resp.IsSuccessStatusCode)
                {
                    throw new Exception("Network failure. StatusCode=" + resp.StatusCode);
                }
                return resp;
            }
             */
        }

        public async Task<string> Get(string url)
        {
            var baseAddress = new System.Uri(url);
            var handler = new HttpClientHandler();
            handler.CookieContainer = cookieJar;

            if (UseProxy == true)
            {
                handler.UseProxy = true;
                handler.Proxy = new WebProxy("http://" + ProxyUri);
            }
            else
            {
                handler.UseProxy = false;
            }

            var client = new HttpClient(handler);
            client.BaseAddress = baseAddress;

            var resp = await client.GetAsync(url);
            if (!resp.IsSuccessStatusCode)
            {
                throw new Exception("Network failure. StatusCode=" + resp.StatusCode);
            }
            var textData = await resp.Content.ReadAsStringAsync();
            return textData;

            /*
            var baseAddress = new System.Uri(url);
            using (var handler = new HttpClientHandler() { CookieContainer = cookieJar })
            using (var client = new HttpClient(handler) { BaseAddress = new System.Uri(url) })
            {
                var resp = await client.GetAsync(url);
                if (!resp.IsSuccessStatusCode)
                {
                    throw new Exception("Network failure. StatusCode=" + resp.StatusCode);
                }
                var textData = await resp.Content.ReadAsStringAsync();
                return textData;
            }
             */
        }
        public async Task<HttpResponseMessage> Get(string url, HttpCompletionOption option)
        {
            var baseAddress = new System.Uri(url);
            var handler = new HttpClientHandler();
            handler.CookieContainer = cookieJar;

            if (UseProxy == true)
            {
                handler.UseProxy = true;
                handler.Proxy = new WebProxy("http://" + ProxyUri);
            }
            else
            {
                handler.UseProxy = false;
            }
            var client = new HttpClient(handler);
            client.BaseAddress = baseAddress;

            var resp = await client.GetAsync(url, option);
            return resp;
            /*
            var baseAddress = new System.Uri(url);
            using (var handler = new HttpClientHandler() { CookieContainer = cookieJar })
            using (var client = new HttpClient(handler) { BaseAddress = new System.Uri(url) })
            {
            }
             */
        }

    }
}
