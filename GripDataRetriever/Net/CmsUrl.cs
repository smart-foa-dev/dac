﻿using GripDataRetriever.Conf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GripDataRetriever.Net
{
    /// <summary>
    /// CMS-UIがアクセスする各アプリケーションのサーバのURLを取得します。
    /// </summary>
    static class CmsUrl
    {
        /// <summary>
        /// GRIPサーバのAPIのベースURL
        /// </summary>
        /// <returns></returns>
        //public static string GetGripBaseUrl()
        //{
        //    return string.Format(AisConf.GripServerBaseUrl, AisConf.Config.GripServerHost, AisConf.Config.GripServerPort);
        //}
        public static string GetGripBaseUrl(string ip = null, string port = null)
        {
            return string.Format(AisConf.GripServerBaseUrl, 
                string.IsNullOrEmpty(ip) ? AisConf.Config.GripServerHost : ip, 
                string.IsNullOrEmpty(port) ? AisConf.Config.GripServerPort : port);
        }

        #region Search
        public static class Search
        {
            public static string Base(string ip = null, string port = null)
            {
                return GetGripBaseUrl(ip, port) + "search";
            }

            public static string DoSearch(string ip = null, string port = null)
            {
                return Base(ip, port) + "/search";
            }

            public static string CreateCsv(string ip = null, string port = null)
            {
                return Base(ip, port) + "/createcsv";
            }

            public static string GetCsvBlock(string ip = null, string port = null)
            {
                return Base(ip, port) + "/getcsvblock";
            }

            public static string Download(string ip = null, string port = null)
            {
                return Base(ip, port) + "/download?searchId={0}&endId={1}&pathId={2}";
            }
            // Grip高速化 zhengchao 2018/06/18 update Start
            public static string DownloadZip(string ip = null, string port = null)
            {
                return Base(ip, port) + "/downloadzip?searchId={0}&endId={1}&pathId={2}";
            }
            // Grip高速化 zhengchao 2018/06/22 update End
            public static string Paths(string ip = null, string port = null)
            {
                return Base(ip, port) + "/paths";
            }

            public static class Mission
            {
                public static string Base(string ip = null, string port = null)
                {
                    return Search.Base(ip, port) + "/mission";
                }

                public static string DoSearch(string ip = null, string port = null)
                {
                    return Mission.Base() + "/search?missionId={0}&searchId={1}&start={2}&end={3}&searchByInCondition={4}&skipSameMainKey={5}";
                }

                public static string CreateCsv(string ip = null, string port = null)
                {
                    return Mission.Base(ip, port) + "/createcsv?missionId={0}&searchId={1}&start={2}&end={3}&searchByInCondition={4}&skipSameMainKey={5}&noBulky=true&linkBulky=true";
                }

                public static string CreateCsvOnline(string ip = null, string port = null)
                {
                    return Mission.Base(ip, port) + "/createcsv?missionId={0}&searchId={1}&start={2}&end={3}&searchByInCondition={4}&skipSameMainKey={5}&onlineId={6}&period={7}&collectEnd={8}&noBulky=true&linkBulky=true";
                }
            }
        }
        #endregion
    }
}
