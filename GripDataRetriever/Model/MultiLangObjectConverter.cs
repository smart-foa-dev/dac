﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GripDataRetriever.Model
{
    public class MultiLangObjectConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(MultiLangObject).IsAssignableFrom(objectType);
        }


        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JArray array = JArray.Load(reader);
            MultiLangObject ml = new MultiLangObject();
            if (array != null)
            {
                foreach (var x in array)
                {
                    ml.Put((string)x["lang"], (string)x["text"]);
                }
            }
            return ml;
        }


        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            JArray array = new JArray();
            MultiLangObject ml = (MultiLangObject)value;
            foreach (String lang in ml.GetLanguages())
            {
                JObject obj = new JObject();
                obj["lang"] = lang;
                obj["text"] = ml.Get(lang);
                array.Add(obj);
            }
            writer.WriteRaw(array.ToString());
        }
    }
}
