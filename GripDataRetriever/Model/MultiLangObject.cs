﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GripDataRetriever.Model
{
    /// <summary>
    /// 多言語メッセージを格納するクラス。
    /// This class is not thread-safe.
    /// </summary>
    [JsonConverter(typeof(MultiLangObjectConverter))]
    public class MultiLangObject
    {
        /// internal map instance
        protected Dictionary<String, String> map;

        /// Constructs a new instance.
        public MultiLangObject()
        {
            map = new Dictionary<String, String>();
        }

        /// @return true if empty
        public Boolean IsEmpty()
        {
            return (map.Count == 0);
        }

        /// @return size of entries
        public int Count()
        {
            return map.Count;
        }

        /// @param language
        ///            language
        /// @return value, or null if no such language exists
        public String Get(String language)
        {
            return Get(language, false);
        }

        public String Get(String lang, Boolean getAny)
        {
            String val = null;
            map.TryGetValue(lang, out val);
            if (val != null)
            {
                return val;
            }
            if (!getAny)
            {
                return null;
            }
            List<String> keys = map.Keys.ToList();
            if (keys.Count > 0)
            {
                map.TryGetValue(keys[0], out val);
                return val;
            }
            return "";
        }

        /// @return List of languages
        public List<String> GetLanguages()
        {
            return map.Keys.ToList();
        }

        /// @param language
        ///            language
        /// @param value
        ///            value
        /// @return true if added as a new entry, or false if replacing old entry
        public Boolean Put(String language, String value)
        {
            if (language == null)
            {
                throw new ArgumentNullException("language");
            }
            if (value == null)
            {
                throw new ArgumentNullException("value");
            }
            var ret = !map.ContainsKey(language);
            map[language] = value;
            return ret;
        }

        public Boolean ContainsValue(String value)
        {
            return map.ContainsValue(value);
        }

        public MultiLangObject Clone()
        {
            MultiLangObject cloned = new MultiLangObject();
            foreach (String lang in map.Keys)
            {
                cloned.Put(lang, map[lang]);
            }
            return cloned;
        }
    }
}
