﻿using GripDataRetriever.Entity;
using System.Collections.Generic;

namespace GripDataRetriever.Model
{
    public class GripMissionCtm : CmsEntity
    {
        public int MissionType { get; set; }

        public Node StartNode { get; set; }
        public List<Node> EndNodes { get; set; }

        public string LinkDocumentId { get; set; }

        /// <summary>
        /// 表示名
        /// </summary>
        //[JsonIgnore]
        //public List<LangObject> DisplayNameList { get; set; }

        /// <summary>
        /// 表示名テキスト
        /// </summary>
        public string DisplayName { get; set; }
    }

    public class Node
    {
        public string CtmId { get; set; }
        public List<string> ElementIds { get; set; }
    }
}
