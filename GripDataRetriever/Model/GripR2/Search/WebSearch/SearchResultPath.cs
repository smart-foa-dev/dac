﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FoaCore.Model.GripR2.Search.WebSearch
{
    [DataContract]
    public class SearchResultPath
    {
        [DataMember]
        public bool forward;
        [DataMember]
        public List<SearchResultNode> nodes;

        public bool existsData = false;
        public SearchResultBlock resultBlock;
    }
}
