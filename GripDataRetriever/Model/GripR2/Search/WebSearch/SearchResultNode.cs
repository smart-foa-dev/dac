﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FoaCore.Model.GripR2.Search.WebSearch
{
    [DataContract]
    public class SearchResultNode
    {
        [DataMember]
        public string id;
        [DataMember]
        public string ctmId;
        [DataMember]
        public string className;
        [DataMember]
        public string ctmType;
        [DataMember]
        public string link;
    }
}
