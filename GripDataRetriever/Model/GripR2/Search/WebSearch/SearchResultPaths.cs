﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FoaCore.Model.GripR2.Search.WebSearch
{
    [DataContract]
    public class SearchResultPaths
    {
        [DataMember]
        public string searchId;

        [DataMember]
        public List<SearchResultPath> paths;
    }
}
