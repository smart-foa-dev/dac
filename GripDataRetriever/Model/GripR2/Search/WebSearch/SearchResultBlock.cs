﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace FoaCore.Model.GripR2.Search.WebSearch
{
    [DataContract]
    public class SearchResultBlock
    {
        private const string STATUS_ERROR = "Error";
        private const string STATUS_CREATING = "Creating";
        private const string STATUS_NORMAL = "Normal";

        [DataMember]
        public List<int> rtCols;
        [DataMember]
        public List<int> branchRtCols;
        [DataMember]
        public string searchId;
        [DataMember]
        public int endId;
        [DataMember]
        public int pathId;
        [DataMember]
        public int totleDataRows;
        [DataMember]
        public int validDataRows;
        [DataMember]
        public int beginRow;
        [DataMember]
        public int endRow;
        [DataMember]
        public int blockRows;
        [DataMember]
        public string data;
        [DataMember]
        public string status;

        public bool isErrorStatus()
        {
            return STATUS_ERROR.Equals(status);
        }
        public bool isCreatingStatus()
        {
            return STATUS_CREATING.Equals(status);
        }
        public bool isNormalStatus()
        {
            return STATUS_NORMAL.Equals(status);
        }
        public int getRtCol(int index)
        {
            if (rtCols[index] > -1)
            {
                return rtCols[index];
            }
            else
            {
                return branchRtCols[index];
            }
        }
        public int getIndexOf(int col)
        {
            if (rtCols.Contains(col))
            {
                return rtCols.IndexOf(col);
            }
            else
            {
                return branchRtCols.IndexOf(col);
            }
        }
    }
}
