﻿using FoaCore.Common.Util;
using FoaCore.Common.Util.GripR2;
using FoaCore.Model.GripR2.Search.WebSearch;
using GripDataRetriever.Model;
using GripDataRetriever.Net;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using FoaCoreCom.ais.retriever;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.Zip.Compression;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;

namespace GripDataRetriever
{
    [ComVisible(true), ClassInterface(ClassInterfaceType.AutoDual)]
    public class GripDataRetriever
    {
        public static bool UseProxy { get; set; }
        public static string ProxyUri { get; set; }
#if !DEBUG
        public const string SEARCH_CONDITION_IN = "in";
        public const string SEARCH_CONDITION_SCOPE = "scope";
        public const string SEARCH_CONDITION_AUTO = "auto";

        public const int WAITTIME_1 = 1000;
        public const int WAITTIME_2 = 1000;

        public const int ROWS_PER_PAGE = 100;



        public GripDataRetriever()
        {
            UseProxy = false;
            ProxyUri = "";
        }

        public async Task<string> GetGripDatatCsv2(GripMissionCtm mission, string searchId, long start, long end)
        {
            return await GetGripDatatCsv2(null, null, mission, searchId, start, end, SEARCH_CONDITION_AUTO, true);
        }
        public async Task<string> GetGripDatatCsv2(string ip, string port, GripMissionCtm mission, long start, long end)
        {
            return await GetGripDatatCsv2(ip, port, mission, new GUID().ToString(), start, end, SEARCH_CONDITION_AUTO, true);
        }
        public async Task<string> GetGripDatatCsv2(string ip, string port, GripMissionCtm mission, string searchId, long start, long end)
        {
            return await GetGripDatatCsv2(ip, port, mission, searchId, start, end, SEARCH_CONDITION_AUTO, true);
        }
        public async Task<string> GetGripDatatCsv2(GripMissionCtm mission, string searchId, long start, long end, string searchByInCondition, bool skipSameMainKey)
        {
            return await GetGripDatatCsv2(null, null, mission, searchId, start, end, searchByInCondition, skipSameMainKey);
        }
        public async Task<string> GetGripDatatCsv2(string ip, string port, GripMissionCtm mission, string searchId, long start, long end, string searchByInCondition, bool skipSameMainKey)
        {
            return await GetGripDatatCsv2Online(null, null, mission, searchId, start, end, searchByInCondition, skipSameMainKey, "", 0, 0);
        }
        public async Task<string> GetGripDatatCsv2Online(GripMissionCtm mission, long start, long end, string onlineId, long period, long collectEnd)
        {
            return await GetGripDatatCsv2Online(null, null, mission, start, end, onlineId, period, collectEnd);
        }
        public async Task<string> GetGripDatatCsv2Online(string ip, string port, GripMissionCtm mission, long start, long end, string onlineId, long period, long collectEnd)
        {
            return await GetGripDatatCsv2Online(ip, port, mission, start, end, SEARCH_CONDITION_AUTO, true, onlineId, period, collectEnd);
        }
        public async Task<string> GetGripDatatCsv2Online(string ip, string port, GripMissionCtm mission, long start, long end, string searchByInCondition, bool skipSameMainKey, string onlineId, long period, long collectEnd)
        {
            return await GetGripDatatCsv2Online(ip, port, mission, new GUID().ToString(), start, end, searchByInCondition, skipSameMainKey, onlineId, period, collectEnd);
        }
        public async Task<string> GetGripDatatCsv2Online(string ip, string port, GripMissionCtm mission, string searchId, long start, long end, string searchByInCondition, bool skipSameMainKey, string onlineId, long period, long collectEnd)
        {
            string textData = null;
            string response1 = null;
            await Task.Run(() =>
            {
                try
                {
                    string url;
                    if (string.IsNullOrEmpty(onlineId))
                    {
                        url = string.Format(CmsUrl.Search.Mission.CreateCsv(ip, port), mission.Id, searchId, start, end, searchByInCondition, skipSameMainKey);
                    }
                    else
                    {
                        url = string.Format(CmsUrl.Search.Mission.CreateCsvOnline(ip, port), mission.Id, searchId, start, end, searchByInCondition, skipSameMainKey, onlineId, period, collectEnd);
                    }
                    var client = new FoaHttpClient(UseProxy,ProxyUri);
                    var response = client.Get(url);
                    textData = response.Result;
                }
                catch (SocketException se)
                {
                    response1 = se.Message;
                }
            });

            string msg = "検索データの取得に失敗しました。 ";
            if (response1 != null)
            {
                msg += response1;
                Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            if (string.IsNullOrEmpty(textData))
            {
                msg += GripDataResponseType.ErrorNotFound;
                Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            List<SearchResultPaths> s = null;
            if (string.IsNullOrEmpty(textData) || "[]" == textData)
            {
                msg += GripDataResponseType.ErrorNotFound;
                Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            try
            {
                #region JSON Deserialize
                MemoryStream stream1 = new MemoryStream(Encoding.UTF8.GetBytes(textData));
                DataContractJsonSerializer ser1 = new DataContractJsonSerializer(typeof(List<SearchResultPaths>));
                s = (List<SearchResultPaths>)ser1.ReadObject(stream1);
                #endregion
            }
            catch
            {
                msg += GripDataResponseType.ErrorProcess;
                Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }
            if (null == s)
            {
                msg += GripDataResponseType.ErrorProcess;
                Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
            }

            bool isNoData = true;
            await Task.Run(() =>
            {
                Thread.Sleep(WAITTIME_1);
                for (int i = 0; i < s.Count; i++)
                {
                    SearchResultPaths ps = s[i];
                    if (null == ps.paths || 0 == ps.paths.Count) continue;
                    if (string.IsNullOrEmpty(onlineId) == false)
                    {
                        searchId = ps.searchId;
                    }
                    for (int j = 0; j < ps.paths.Count; j++)
                    {
                        SearchResultPath p = ps.paths[j];
                        if (null == p.nodes || 2 > p.nodes.Count) continue;

                        string url2 = CmsUrl.Search.GetCsvBlock(ip, port);
                        SearchResultBlock block = new SearchResultBlock
                        {
                            searchId = searchId,
                            endId = (i + 1),
                            pathId = (j + 1),
                            beginRow = 1,
                            endRow = ROWS_PER_PAGE
                        };

                        #region JSON Serialize
                        MemoryStream stream2 = new MemoryStream();
                        DataContractJsonSerializer ser2 = new DataContractJsonSerializer(typeof(SearchResultBlock));
                        ser2.WriteObject(stream2, block);
                        stream2.Position = 0;
                        StreamReader sr2 = new StreamReader(stream2);
                        string json = sr2.ReadToEnd();
                        #endregion

                        while (true)
                        {
                            Thread.Sleep(WAITTIME_2);
                            StringContent theContent = new StringContent(json, new UTF8Encoding(false), "application/json");
                            var client2 = new FoaHttpClient(UseProxy, ProxyUri);
                            var textData2 = client2.Post(url2, theContent);

                            #region JSON Deserialize
                            MemoryStream stream3 = new MemoryStream(Encoding.UTF8.GetBytes(textData2.Result));
                            DataContractJsonSerializer ser3 = new DataContractJsonSerializer(typeof(SearchResultBlock));
                            var resultBlock = (SearchResultBlock)ser3.ReadObject(stream3);
                            #endregion

                            if (resultBlock.isCreatingStatus()) continue;
                            p.resultBlock = resultBlock;
                            break;
                        }
                        if (0 < p.resultBlock.blockRows && p.resultBlock.isNormalStatus())
                        {
                            p.existsData = true;
                            isNoData = false;
                        }
                    }
                }
            });
            if (isNoData)
            {
                return null;
                //return GripDataResponseType.NoFolder;
            }

            // 一時ダウンロード用のディレクトリ作成
            string pathString = DlDir + "\\" + searchId;
            if (!Directory.Exists(DlDir)) Directory.CreateDirectory(DlDir);
            if (!Directory.Exists(DlDir + "\\" + searchId)) Directory.CreateDirectory(DlDir + "\\" + searchId);
            string nowDateTime = DateTime.Now.ToString(FileDateFormat2);

            await Task.Run(() =>
            {
                int resultNo = 0;
                Thread.Sleep(WAITTIME_1);
                for (int i = 0; i < s.Count; i++)
                {
                    SearchResultPaths ps = s[i];
                    for (int j = 0; j < ps.paths.Count; j++)
                    {
                        SearchResultPath p = ps.paths[j];
                        if (!p.existsData) continue;

                        int endId = p.resultBlock.endId; int pathId = p.resultBlock.pathId;
                        string url2 = string.Format(CmsUrl.Search.Download(ip, port), searchId, endId, pathId);
                        string[] titleFields = GetTitleFields(p.resultBlock.data);
                        string ctmName = titleFields[p.resultBlock.getRtCol(p.resultBlock.rtCols.Count - 1)];

                        Thread.Sleep(WAITTIME_2);
                        WebClient client2 = new WebClient();
                        byte[] fileData = client2.DownloadData(new System.Uri(url2));
                        string stringData = Encoding.GetEncoding("Shift_JIS").GetString(fileData);
                        string oldStringData = GripR2CsvTranslator.TranslateBack(stringData, p.resultBlock);
                        string resultCtmName = TrimmingString(this.InvalidChars.Aggregate(ctmName, (str, c) => str.Replace(c.ToString(), string.Empty)));
                        string wkbName = string.Format("{0}_{1}_{2}{3}", resultNo, resultCtmName, "Route-", p.resultBlock.pathId);
                        string wkbPath = string.Format("{0}\\{1}\\{2}.csv", DlDir, searchId, wkbName);
                        File.WriteAllText(wkbPath, oldStringData, new UTF8Encoding(true));
                        string wkbHPath = string.Format("{0}\\{1}\\{2}.h", DlDir, searchId, wkbName);
                        client2.Dispose();
                        resultNo++;
                    }
                }
            });

            return pathString;
        }

        public string GetGripDatatCsvFromProxy(bool useProxy, string proxyUri, string ip, string port, string missionId, string start, string end)
        {
            // Proxy情報を設定する

            if (useProxy == true && !string.IsNullOrEmpty(proxyUri))
            {
                UseProxy = useProxy;
                ProxyUri = proxyUri;
            }
            else
            {
                UseProxy = false;
                ProxyUri = "";
            }
            return GetGripDatatCsv(ip, port, missionId, start, end);
        }

        // GetGripDatatCsv
        public string GetGripDatatCsv(string ip, string port, string missionId, string start, string end)
        {
            return GetGripDatatCsv(ip, port, missionId, new GUID().ToString(), start, end, SEARCH_CONDITION_AUTO, true);
        }

        // GetGripDatatCsv_2
        public string GetGripDatatCsv(string missionId, string searchId, string start, string end)
        {
            return GetGripDatatCsv(null, null, missionId, searchId, start, end, SEARCH_CONDITION_AUTO, true);
        }

        // GetGripDatatCsv_3
        public string GetGripDatatCsv(string ip, string port, string missionId, string searchId, string start, string end)
        {
            return GetGripDatatCsv(ip, port, missionId, searchId, start, end, SEARCH_CONDITION_AUTO, true);
        }

        // GetGripDatatCsv_4
        public string GetGripDatatCsv(string missionId, string searchId, string start, string end, string searchByInCondition, bool skipSameMainKey)
        {
            return GetGripDatatCsv(null, null, missionId, searchId, start, end, searchByInCondition, skipSameMainKey);
        }

        // GetGripDatatCsv_5
        public string GetGripDatatCsv(string ip, string port, string missionId, string searchId, string start, string end, string searchByInCondition, bool skipSameMainKey)
        {
            return GetGripDatatCsvOnline(ip, port, missionId, searchId, start, end, searchByInCondition, skipSameMainKey, "", "", "");
        }

        public string GetGripDatatCsvOnlineFromProxy(bool useProxy, string proxyUri, string ip, string port, string missionId, string start, string end, string onlineId, string period, string collectEnd)
        {
            // Proxy情報を設定する

            if (useProxy == true && !string.IsNullOrEmpty(proxyUri))
            {
                UseProxy = useProxy;
                ProxyUri = proxyUri;
            }
            else
            {
                UseProxy = false;
                ProxyUri = "";
            }

            return GetGripDatatCsvOnline(ip, port, missionId, start, end, SEARCH_CONDITION_AUTO, true, onlineId, period, collectEnd);
        }


        // GetGripDatatCsvOnline
        public string GetGripDatatCsvOnline(string missionId, string start, string end, string onlineId, string period, string collectEnd)
        {
            return GetGripDatatCsvOnline(null, null, missionId, start, end, onlineId, period, collectEnd);
        }
        // GetGripDatatCsvOnline_2
        public string GetGripDatatCsvOnline(string ip, string port, string missionId, string start, string end, string onlineId, string period, string collectEnd)
        {
            return GetGripDatatCsvOnline(ip, port, missionId, start, end, SEARCH_CONDITION_AUTO, true, onlineId, period, collectEnd);
        }
        // GetGripDatatCsvOnline_3
        public string GetGripDatatCsvOnline(string ip, string port, string missionId, string start, string end, string searchByInCondition, bool skipSameMainKey, string onlineId, string period, string collectEnd)
        {
            return GetGripDatatCsvOnline(ip, port, missionId, new GUID().ToString(), start, end, searchByInCondition, skipSameMainKey, onlineId, period, collectEnd);
        }

        // GetGripDatatCsvOnline_4
        public string GetGripDatatCsvOnline(string ip, string port, string missionId, string searchId, string start, string end, string searchByInCondition, bool skipSameMainKey, string onlineId, string period, string collectEnd)
        {
            string url;
            if (string.IsNullOrEmpty(onlineId))
            {
                url = string.Format(CmsUrl.Search.Mission.CreateCsv(ip, port), missionId, searchId, start, end, searchByInCondition, skipSameMainKey);
            }
            else
            {
                url = string.Format(CmsUrl.Search.Mission.CreateCsvOnline(ip, port), missionId, searchId, start, end, searchByInCondition, skipSameMainKey, onlineId, period, collectEnd);
            }

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);

            if (UseProxy == true)
            {
                System.Net.WebProxy proxy = new System.Net.WebProxy("http://" + ProxyUri);
                req.Proxy = proxy;
            }

            req.Method = "GET";

            string textData;
            using (var res = (HttpWebResponse)req.GetResponse())
            {
                using (var streamReader = new StreamReader(res.GetResponseStream()))
                {

                    textData = streamReader.ReadToEnd();
                }
            }

            if (string.IsNullOrEmpty(textData))
            {
                return string.Empty;
            }

            List<SearchResultPaths> searchResultPathList = null;
            if (string.IsNullOrEmpty(textData) || "[]" == textData)
            {
                return string.Empty;
            }

            try
            {
                #region JSON Deserialize
                using (MemoryStream memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(textData)))
                {
                    DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<SearchResultPaths>));
                    searchResultPathList = (List<SearchResultPaths>)serializer.ReadObject(memoryStream);
                }
                #endregion
            }
            catch
            {
                return string.Empty;
            }
            if (null == searchResultPathList)
            {
                return string.Empty;
            }

            bool isNoData = true;
            Thread.Sleep(WAITTIME_1);
            for (int i = 0; i < searchResultPathList.Count; i++)
            {
                SearchResultPaths searchResultPaths = searchResultPathList[i];
                if (null == searchResultPaths.paths || 0 == searchResultPaths.paths.Count) continue;
                if (string.IsNullOrEmpty(onlineId) == false)
                {
                    searchId = searchResultPaths.searchId;
                }
                for (int j = 0; j < searchResultPaths.paths.Count; j++)
                {
                    SearchResultPath p = searchResultPaths.paths[j];
                    if (null == p.nodes || 2 > p.nodes.Count) continue;

                    string url2 = CmsUrl.Search.GetCsvBlock(ip, port);
                    SearchResultBlock block = new SearchResultBlock
                    {
                        searchId = searchId,
                        endId = (i + 1),
                        pathId = (j + 1),
                        beginRow = 1,
                        endRow = ROWS_PER_PAGE
                    };

                    #region JSON Serialize
                    string json;
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        DataContractJsonSerializer serialzer = new DataContractJsonSerializer(typeof(SearchResultBlock));
                        serialzer.WriteObject(memoryStream, block);
                        memoryStream.Position = 0;
                        using (StreamReader streamReader = new StreamReader(memoryStream))
                        {
                            json = streamReader.ReadToEnd();
                        }
                    }
                    #endregion

                    while (true)
                    {
                        Thread.Sleep(WAITTIME_2);
                        HttpWebRequest req2 = (HttpWebRequest)WebRequest.Create(url2);

                        req2.ContentType = "application/json";
                        req2.Method = "POST";
                        req2.KeepAlive = false;

                        if (UseProxy == true)
                        {
                            System.Net.WebProxy proxy = new System.Net.WebProxy("http://" + ProxyUri);
                            req2.Proxy = proxy;
                        }
                        else
                        {
                            req2.Proxy = null;

                        }



                        using (var streamWriter = new StreamWriter(req2.GetRequestStream()))
                        {
                            streamWriter.Write(json);
                            streamWriter.Flush();
                            streamWriter.Close();
                        }

                        SearchResultBlock resultBlock;
                        using (var res2 = (HttpWebResponse)req2.GetResponse())
                        {
                            #region JSON Deserialize
                            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(SearchResultBlock));
                            resultBlock = (SearchResultBlock)serializer.ReadObject(res2.GetResponseStream());
                            #endregion
                        }


                        if (resultBlock == null || resultBlock.isCreatingStatus()) continue;
                        p.resultBlock = resultBlock;
                        break;
                    }
                    if (0 < p.resultBlock.blockRows && p.resultBlock.isNormalStatus())
                    {
                        p.existsData = true;
                        isNoData = false;
                    }
                }
            }
            if (isNoData)
            {
                return string.Empty;
            }

            // 一時ダウンロード用のディレクトリ作成
            string pathString = DlDir + "\\" + searchId;
            if (!Directory.Exists(DlDir)) Directory.CreateDirectory(DlDir);
            if (!Directory.Exists(DlDir + "\\" + searchId)) Directory.CreateDirectory(DlDir + "\\" + searchId);
            string nowDateTime = DateTime.Now.ToString(FileDateFormat2);

            int resultNo = 0;
            Thread.Sleep(WAITTIME_1);
            for (int i = 0; i < searchResultPathList.Count; i++)
            {
                SearchResultPaths searchResultPaths = searchResultPathList[i];
                for (int j = 0; j < searchResultPaths.paths.Count; j++)
                {
                    SearchResultPath p = searchResultPaths.paths[j];
                    if (!p.existsData) continue;

                    int endId = p.resultBlock.endId; int pathId = p.resultBlock.pathId;
                    // For Com高速化 zhengchao 2018/06/18 update Start
                    //URLを作成する
                    string url3 = string.Format(CmsUrl.Search.DownloadZip(ip, port), searchId, endId, pathId);
                    // For Com高速化 zhengchao 2018/06/22 update End
                    string[] titleFields = GetTitleFields(p.resultBlock.data);
                    string ctmName = titleFields[p.resultBlock.getRtCol(p.resultBlock.rtCols.Count - 1)];

                    Thread.Sleep(WAITTIME_2);
                    using (WebClient client = new WebClient())
                    {

                        if (UseProxy == true)
                        {
                            System.Net.WebProxy proxy = new System.Net.WebProxy("http://" + ProxyUri);
                            client.Proxy = proxy;
                        }
                        else
                        {
                            client.Proxy = null;

                        }

                        // Grip高速化 zhengchao 2018/06/18 update Start
                        //フォルダーを作成する
                        req.Method = "GET";
                        string pathStringZip = DlDir + "\\" + searchId;
                        if (!Directory.Exists(DlDir)) Directory.CreateDirectory(DlDir);
                        if (!Directory.Exists(DlDir + "\\" + searchId)) Directory.CreateDirectory(DlDir + "\\" + searchId);
                        string folderPath = pathStringZip;
                        //臨時ファイルを作成する
                        string tmpFilepathZip = System.IO.Path.Combine(folderPath, "data.zip");
                        bool failed = false;
                        string message = "";
                        //圧縮ファイルリームを読む
                        HttpClient httpClient = CtmDataRetriever.GetHttpClient(ProxyUri);
                        var task = httpClient.GetAsync(url3, HttpCompletionOption.ResponseHeadersRead).ContinueWith(response0 =>
                        {
                            try
                            {
                                var response = response0.Result;
                                if (!response.IsSuccessStatusCode)
                                {
                                    message = "Failed to get Mission result: " + url3 + " StatusCode=" + (int)(response.StatusCode);
                                    failed = true;
                                }
                                else
                                {
                                    var fs = File.Open(tmpFilepathZip, FileMode.Create);
                                    var responseStream = response.Content.ReadAsStreamAsync().Result;
                                    byte[] bArr = new byte[4096];
                                    int size = responseStream.Read(bArr, 0, (int)bArr.Length);
                                    while (size > 0)
                                    {
                                        fs.Write(bArr, 0, size);
                                        size = responseStream.Read(bArr, 0, (int)bArr.Length);
                                    }
                                    fs.Close();
                                    responseStream.Close();
                                }
                            }
                            catch (AggregateException e)
                            {
                                Exception ex = e.InnerException;
                                message = "Failed to get Mission result: " + url3 + " Exception=" + ex.Message;
                                failed = true;
                            }
                            catch (Exception e)
                            {
                                message = "Failed to get Mission result: " + url3 + " Exception=" + e.Message;
                                failed = true;
                            }
                        });
                        task.GetAwaiter().GetResult();
                        if (failed)
                        {
                            Directory.Delete(folderPath);
                            folderPath = null;
                        }
                        //Zipファイルを解凍する
                        string stringDataZip = String.Empty;
                        //unZip後の保存パス
                        stringDataZip = unZipFile(tmpFilepathZip);
                        //臨時フォルダーを作成する
                        DirectoryInfo folder = new DirectoryInfo(stringDataZip);
                        FileInfo[] fil = folder.GetFiles(searchId + "*.csv");
                        string stringData = String.Empty;
                        StringBuilder stringTemp = new StringBuilder();
                        foreach (FileInfo fileTemp in fil)
                        {
                            //FileStream fileStream = new FileStream(fileTemp.FullName, FileMode.Open, FileAccess.Read);
                            ////ファイルの字符コードはShift_JISに変換する
                            //byte[] bytes = new byte[fileStream.Length];
                            //fileStream.Seek(0, SeekOrigin.Begin);
                            //fileStream.Read(bytes, 0, bytes.Length);
                            //stringData = Encoding.GetEncoding("Shift_JIS").GetString(bytes);
                            //stringTemp.Append(stringData);
                            ////ファイルリームはNullかどうかを判断する
                            //if (fileStream != null)
                            //{
                            //    fileStream.Close();
                            //}
                            using (StreamReader sr = new StreamReader(fileTemp.FullName, Encoding.GetEncoding("shift-jis")))
                            {
                                while (!sr.EndOfStream)
                                {
                                    stringData = sr.ReadLine().ToString();
                                    stringTemp.Append(stringData + "\r\n");
                                }
                            }
                            //臨時ファイルを削除する
                            fileTemp.Delete();
                        }
                        //臨時ファイルを削除する
                        System.IO.FileInfo tempZip = new System.IO.FileInfo(@tmpFilepathZip);
                        tempZip.Delete();
                        string oldStringData = GripR2CsvTranslator.TranslateBack(stringTemp.ToString(), p.resultBlock);
                        // Grip高速化 zhengchao 2018/06/22 update End
                        string resultCtmName = TrimmingString(this.InvalidChars.Aggregate(ctmName, (str, c) => str.Replace(c.ToString(), string.Empty)));
                        string wkbName = string.Format("{0}_{1}_{2}{3}", resultNo, resultCtmName, "Route-", p.resultBlock.pathId);
                        string wkbPath = string.Format("{0}\\{1}\\{2}.csv", DlDir, searchId, wkbName);
                        File.WriteAllText(wkbPath, oldStringData, new UTF8Encoding(true));
                        string wkbHPath = string.Format("{0}\\{1}\\{2}.h", DlDir, searchId, wkbName);
                        List<int> rtCols2 = new List<int>();
                        for (int rc = 0; rc < p.resultBlock.rtCols.Count; rc++)
                        {
                            rtCols2.Add(p.resultBlock.getRtCol(rc) + 1 + rc);
                        }
                    }
                    resultNo++;
                }
            }

            return pathString;
        }
        // Grip高速化 zhengchao 2018/06/18 update Start
        public static string unZipFile(string zipFileUrl)
        {
            //unZip後の保存パス
            String unZipDir = null;

            //拡張子名を取得する
            String extensionName = Path.GetExtension(zipFileUrl);

            //拡張子名は「.zip」かどうかを判断する
            if (!extensionName.Equals(".zip"))
            {
                return unZipDir;
            }

            //zipFileは存在かどうかを判断する
            if (!File.Exists(zipFileUrl))
            {
                return unZipDir;
            }

            //コードフォーマットを修正する
            Encoding utf = Encoding.GetEncoding("utf-8");
            ZipConstants.DefaultCodePage = utf.CodePage;

            //unZip後の保存パス
            unZipDir = Path.GetDirectoryName(zipFileUrl);

            ZipInputStream s = null;
            try
            {
                //ファイルを解凍する
                if (!unZipDir.EndsWith("/"))
                {
                    String unZipDirNew = unZipDir + "/";
                    s = new ZipInputStream(File.OpenRead(zipFileUrl));
                    ZipEntry theEntry;
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        string directoryName = Path.GetDirectoryName(theEntry.Name);
                        string fileName = Path.GetFileName(theEntry.Name);

                        //フォルダーは存在かどうかを判断する
                        if (directoryName.Length > 0)
                        {
                            //フォルダーを作成する
                            Directory.CreateDirectory(unZipDirNew + directoryName);
                        }
                        if (!directoryName.EndsWith("/"))
                        {
                            directoryName += "/";
                            if (fileName != String.Empty)
                            {
                                using (FileStream streamWriter = File.Create(unZipDirNew + theEntry.Name))
                                {
                                    int size = 2048;
                                    byte[] data = new byte[2048];
                                    while (true)
                                    {
                                        size = s.Read(data, 0, data.Length);
                                        if (size > 0)
                                        {
                                            streamWriter.Write(data, 0, size);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    s.Close();
                }
                return unZipDir;
            }
            catch (Exception)
            {
                s.Close();
                unZipDir = null;
                return unZipDir;
            }
        }
        // Grip高速化 zhengchao 2018/06/22 update End
        public string CancelExecution()
        {
            return string.Empty;
        }

        public string GetStatus()
        {
            return GripDataResponseType.StatusIdle;
        }






        #region util
        /// <summary>
        /// 一時ファイル置き場
        /// </summary>
        //public static readonly string DlDir = System.AppDomain.CurrentDomain.BaseDirectory + "\\grip_temp";
        public static readonly string DlDir = System.Environment.GetEnvironmentVariable("FOA_HOME") + @"\dll\grip_temp";

        /// <summary>
        /// ファイル名に付与する時刻のフォーマット
        /// </summary>
        const string FileDateFormat = "yyyyMMddHHmmssfff";

        /// <summary>
        /// ファイル名に付与する時刻のフォーマット
        /// </summary>
        const string FileDateFormat2 = "yyyy-MM-dd-HH-mm-ss-fff";

        /// <summary>
        /// ファイル禁止文字列
        /// </summary>
        readonly char[] InvalidChars = System.IO.Path.GetInvalidFileNameChars();

        /// <summary>
        /// トリミングするときのデフォルト文字数
        /// </summary>
        const int MaxTrimLength = 25;

        /// <summary>
        /// 指定した文字列を指定した長さでトリミングします。
        /// 長さが0以下の場合は25文字でトリミングします。
        /// </summary>
        /// <param name="v">文字列</param>
        /// <param name="length">トリミングする桁数</param>
        /// <returns>トリミングした文字列</returns>
        public static string TrimmingString(string v, int length = MaxTrimLength)
        {
            if (length < 1) length = MaxTrimLength;
            if (!string.IsNullOrEmpty(v) && v.Length > length)
            {
                v = v.Substring(0, length);
            }
            return v;
        }

        private string[] GetTitleFields(string data)
        {
            MemoryStream titleStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(data));

            string[] titleFields = null;
            using (TextFieldParser parser = new TextFieldParser(titleStream))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    titleFields = parser.ReadFields();
                    break;
                }
                parser.Close();
            }
            titleStream.Close();

            return titleFields;
        }

        /// <summary>
        /// grip_tempを削除
        /// </summary>
        public static void DeleteTempDir()
        {
            try
            {
                if (Directory.Exists(DlDir))
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(DlDir);
                    DeleteTempDir(dirInfo);
                }
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// grip_tempを削除
        /// </summary>
        /// <param name="dirInfo"></param>
        private static void DeleteTempDir(DirectoryInfo dirInfo)
        {
            try
            {
                // ディレクトリ内のファイルの読み取り専用解除
                FileInfo[] fileInfo = dirInfo.GetFiles();
                foreach (FileInfo f in fileInfo)
                {
                    if ((f.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        f.Attributes = FileAttributes.Normal;
                    }
                }

                // ディレクトリ内のサブフォルダ確認
                foreach (System.IO.DirectoryInfo subDirInfo in dirInfo.GetDirectories())
                {
                    DeleteTempDir(subDirInfo);
                }

                // このディレクトリの読み取り専用解除
                if ((dirInfo.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    dirInfo.Attributes = FileAttributes.Directory;
                }

                // まとめて削除
                dirInfo.Delete(true);
            }
            catch
            {
                throw;
            }
        }
        #endregion

#else
        private const int POLLING_INTERVAL = 3 * 1000; // 3秒
        public async Task<string> GetGripDatatCsv2(GripMissionCtm mission, long start, long end)
        {
            string path = null;
            string response1 = null;
            await Task.Run(() =>
            {
                string missionId = mission.Id;
                string ip = "localhost";
                int port = 20000;

                try
                {
                    string response = SendMessage(ip, port, GripDataCommandType.Execute, missionId, start, end);
                    if (response != GripDataResponseType.OnSearching)
                    {
                        response1 = response;
                        return;
                    }

                    while (true)
                    {
                        Thread.Sleep(POLLING_INTERVAL);

                        response = SendMessage(ip, port, GripDataCommandType.GetResult, missionId);
                        if (!GripDataResponseType.ErrorResponseList.Contains(response))
                        {
                            path = response;
                            break;
                        }
                        if (response == GripDataResponseType.NoFolder)
                        {
                            //logger.Warn("Search No Data: " + response);
                            //responseWarning = "指定した期間で、データが存在しません。";
                            break;
                        }
                    }
                }
                catch (SocketException se)
                {
                    response1 = se.Message;
                }
            });

            if (response1 != null)
            {
                string msg = "Gripミッションの取得に失敗しました。 " + response1;

                Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            return path;
        }

        public string GetGripDatatCsv(string missionId, string start, string end)
        {
            //string missionId = mission.Id;
            string ip = "localhost";
            int port = 20000;

            string response = string.Empty;
            string path = string.Empty;

            response = SendMessage(ip, port, GripDataCommandType.Execute, missionId, start, end);
            if (response != GripDataResponseType.OnSearching)
            {
                return string.Empty;
            }

            while (true)
            {
                Thread.Sleep(3 * 1000);

                response = SendMessage(ip, port, GripDataCommandType.GetResult, missionId);
                if (!GripDataResponseType.ErrorResponseList.Contains(response))
                {
                    path = response;
                    break;
                }
                if (response == GripDataResponseType.NoFolder)
                {
                    //logger.Warn("Search No Data: " + response);
                    //responseWarning = "指定した期間で、データが存在しません。";
                    break;
                }
            }

            return path;
        }

        public string GetStatus()
        {
            //string missionId = mission.Id;
            string ip = "localhost";
            int port = 20000;

            string response = string.Empty;
            response = SendMessage(ip, port, GripDataCommandType.GetStatus);

            return response;
        }

        public string CancelExecution()
        {
            //string missionId = mission.Id;
            string ip = "localhost";
            int port = 20000;

            string response = string.Empty;
            response = SendMessage(ip, port, GripDataCommandType.CancelExecution);

            return response;
        }

        public string SendMessage(string ip, int port, string command, string missionId, long start = -1, long end = -1)
        {
            string requestText = CreateMessageText(command, missionId, start, end);
            byte[] requestBytes = CreateSendMessage(requestText);

            TcpClient client = new TcpClient(ip, port);
            NetworkStream stream = client.GetStream();
            stream.Write(requestBytes, 0, requestBytes.Length);

            string responseText = GetResponseMessage(stream);
            return responseText;
        }
        public string SendMessage(string ip, int port, string command, string missionId, string start, string end)
        {
            string requestText = CreateMessageText(command, missionId, start, end);
            byte[] requestBytes = CreateSendMessage(requestText);

            TcpClient client = new TcpClient(ip, port);
            NetworkStream stream = client.GetStream();
            stream.Write(requestBytes, 0, requestBytes.Length);

            string responseText = GetResponseMessage(stream);
            return responseText;
        }
        public string SendMessage(string ip, int port, string command)
        {
            string requestText = CreateMessageText(command);
            byte[] requestBytes = CreateSendMessage(requestText);

            TcpClient client = new TcpClient(ip, port);
            NetworkStream stream = client.GetStream();
            stream.Write(requestBytes, 0, requestBytes.Length);

            string responseText = GetResponseMessage(stream);
            return responseText;
        }

        public string CreateMessageText(string command, string missionId, long start = -1, long end = -1)
        {
            return "cmd=" + command + "&" + "mission_id=" + missionId + (start < 0 ? string.Empty : "&" + "start_time=" + start.ToString()) + (end < 0 ? string.Empty : "&" + "end_time=" + end.ToString());
        }
        public string CreateMessageText(string command, string missionId, string start, string end)
        {
            return "cmd=" + command + "&" + "mission_id=" + missionId + (start == null ? string.Empty : "&" + "start_time=" + start) + (end == null ? string.Empty : "&" + "end_time=" + end);
        }
        public string CreateMessageText(string command)
        {
            return "cmd=" + command;
        }
        public byte[] CreateSendMessage(string message)
        {
            byte[] msgBytes = System.Text.Encoding.UTF8.GetBytes(message);
            int len = msgBytes.Length;
            byte[] lenBytes = BitConverter.GetBytes(len);
            byte[] headerBytes = new byte[4];
            for (int i = 0; i < 4; i++)
                headerBytes[i] = lenBytes[4 - 1 - i];

            byte[] tcpDataBytes = new byte[4 + len];
            System.Buffer.BlockCopy(headerBytes, 0, tcpDataBytes, 0, 4);
            System.Buffer.BlockCopy(msgBytes, 0, tcpDataBytes, 4, len);

            return tcpDataBytes;
        }

        public byte[] GetResponseMessageByteBlock(NetworkStream stream)
        {

            //サーバーから送られたデータを受信する
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            byte[] resBytes = new byte[4096];
            int resSize = 0;
            //データの一部を受信する
            resSize = stream.Read(resBytes, 0, resBytes.Length);
            //Readが0を返した時はサーバーが切断したと判断
            if (resSize == 0)
            {
                Console.WriteLine("サーバーが切断しました。");
                //break;
            }
            //受信したデータを蓄積する
            ms.Write(resBytes, 0, resSize);
            //まだ読み取れるデータがあるか、データの最後が\nでない時は、
            // 受信を続ける
            //do
            //{
            //    //データの一部を受信する
            //    resSize = stream.Read(resBytes, 0, resBytes.Length);
            //    //Readが0を返した時はサーバーが切断したと判断
            //    if (resSize == 0)
            //    {
            //        Console.WriteLine("サーバーが切断しました。");
            //        break;
            //    }
            //    //受信したデータを蓄積する
            //    ms.Write(resBytes, 0, resSize);
            //    //まだ読み取れるデータがあるか、データの最後が\nでない時は、
            //    // 受信を続ける
            //} while (stream.DataAvailable || resBytes[resSize - 1] != '\n');

            byte[] responseBytes = ms.GetBuffer();
            // メッセージヘッダを読み込む
            byte[] headerBytes = new byte[4]; // 4バイトのヘッダ(メッセージ長)を受け取る
            // CGから送られるバイト長データがBIG ENDIANの場合、ここでReverseして、
            // INT型にパース出来るようにする。
            System.Buffer.BlockCopy(responseBytes, 0, headerBytes, 0, 4);
            Array.Reverse(headerBytes);
            int msgLen = BitConverter.ToInt32(headerBytes, 0);
            Console.WriteLine(msgLen.ToString());

            byte[] msgBytes = new byte[msgLen];
            System.Buffer.BlockCopy(responseBytes, 4, msgBytes, 0, msgLen);

            return msgBytes;
        }

        public string GetResponseMessageText(byte[] msgBytes)
        {
            string responseText = System.Text.Encoding.UTF8.GetString(msgBytes, 0, msgBytes.Length);
            return responseText;
        }

        public string GetResponseMessage(NetworkStream stream)
        {
            return GetResponseMessageText(GetResponseMessageByteBlock(stream));
        }
#endif
    }

    class GripDataCommandType
    {
        public const string Execute = "execute";
        public const string GetResult = "get_result";
        public const string GetStatus = "get_status";
        public const string CancelExecution = "cancel_execution";
    }
    class GripDataResponseType
    {
        public const string ErrorRequestEmpty = "ERR_REQUEST_EMPTY";
        public const string StatusBusy = "STATUS_BUSY";
        public const string NoFolder = "NO_FOLDER";
        public const string ErrorBusy = "ERR_BUSY";
        public const string ErrorNotFound = "ERR_NOT_FOUND";
        public const string ErrorProcess = "ERR_PROCESS";
        public const string ErrorNotFoundStartNode = "ERR_NOT_FOUND_START_NODE";
        public const string ErrorNotFoundEndNode = "ERR_NOT_FOUND_END_NODE";
        public const string ErrorNotFoundTraceNode = "ERR_NOT_FOUND_TRACE_NODE";
        public const string OnSearching = "ON_SEARCHING";
        public const string StatusIdle = "STATUS_IDLE";
        public const string OnCanceling = "ON_CANCELING";

        public static List<string> ErrorResponseList = new List<string>() { ErrorRequestEmpty, StatusBusy, NoFolder, ErrorBusy, ErrorNotFound, ErrorProcess, ErrorNotFoundStartNode, ErrorNotFoundEndNode, ErrorNotFoundTraceNode, OnSearching, StatusIdle, OnCanceling };
    }
}
