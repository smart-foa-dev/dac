﻿using FoaCore.Model.GripR2.Search.WebSearch;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoaCore.Common.Util.GripR2
{
    public static class GripR2CsvTranslator
    {
        // Wang Issue of branch search 20181120 Start
//        public const string HEADER_RECEIVE_TIME = "受信時刻";

//        public static string TranslateBack(string text, SearchResultBlock block)
//        {
//            StringBuilder builder = new StringBuilder();

//            StringReader reader = new StringReader(text);
//            TextFieldParser parser = new TextFieldParser(reader);
//            using (parser)
//            {
//                parser.TextFieldType = FieldType.Delimited; // 区切り形式とする
//                parser.SetDelimiters(","); // 区切り文字はコンマとする
//                //parser.CommentTokens = new string[] { "#" }; // #で始まる行はコメントとする
//                //parser.HasFieldsEnclosedInQuotes = true; // 引用符付きとする
//                //parser.TrimWhiteSpace = true; // 空白文字を取り除く
//                parser.HasFieldsEnclosedInQuotes = false;
//                parser.TrimWhiteSpace = false;

//                // Wang Issue of branch search 20181119 Start
//                parser.ReadLine(); // skip branch header
//                // Wang Issue of branch search 20181119 End

//                int indexRow = 0;
//                List<string> outNodeType = new List<string>();
//                string[] nodeType = null;
//                List<string> ctmNameList = new List<string>();
//                List<string> dataTypeList = new List<string>();
//                while (!parser.EndOfData)
//                {
//                    string[] row = parser.ReadFields(); // 1行読み込んでフィールド毎に配列に入れる

//                    // Wang Issue of branch search 20181119 Start
//                    //if (indexRow == 0)
//                    //{
//                    //    nodeType = row;
//                    //    indexRow++;
//                    //    continue;
//                    //}
//                    // Wang Issue of branch search 20181119 End

//                    //row[0] = row[0].Replace(" ", "_"); // 空白を_に変換する
//                    //row[1] = row[1].Replace(" ", "_"); // 空白を_に変換する
//                    //CsvData.Add(row[0], row[1]); // コレクションに追加する

//                    if (row.Length == 0) continue;
//                    if (row.Length == 1 && string.IsNullOrEmpty(row[0].Trim())) continue;

//                    List<string> outRow = new List<string>();
//                    for (int indexCol = 0; indexCol < row.Length; indexCol++)
//                    {
//                        if (block.rtCols.Contains(indexCol) || block.branchRtCols.Contains(indexCol))
//                        {
//                            if (indexRow == 0)
//                            {
//                            }
//                            else if (indexRow == 1)
//                            {
//                                ctmNameList.Add(row[indexCol]);
//                                outRow.Add(row[indexCol]);
//                                outRow.Add(HEADER_RECEIVE_TIME + "_" + (block.getIndexOf(indexCol) + 1));

//#if DISPLAY_NODE_TYPE
//                                outNodeType.Add(nodeType[indexCol]);
//                                outNodeType.Add(string.Empty);
//#endif
//                            }
//                            else if (indexRow == 2)
//                            {
//                                outRow.Add(row[indexCol]);
//                                outRow.Add("DATETIME");
//                                dataTypeList.Add(row[indexCol]);
//                            }
//                            else if (indexRow == 3)
//                            {
//                                outRow.Add(row[indexCol]);
//                                outRow.Add(string.Empty);
//                            }
//                            else
//                            {
//                                outRow.Add(ctmNameList[block.getIndexOf(indexCol)]);
//                                outRow.Add(row[indexCol]);
//                            }
//                        }
//                        else
//                        {
//                            outRow.Add(row[indexCol]);
//#if DISPLAY_NODE_TYPE
//                            if (indexRow == 1)
//                            {
//                                outNodeType.Add(string.Empty);
//                            }
//#endif
//                        }
//                    }
//                    outRow.Add(string.Empty);

//#if DISPLAY_NODE_TYPE
//                    if (indexRow == 1)
//                    {
//                        outNodeType.Add(string.Empty);
//                        builder.Append(string.Join(",", outNodeType));
//                    }
//#endif
//                    builder.Append(string.Join(",", outRow));
//                    builder.Append(Environment.NewLine);

//                    indexRow++;
//                }
//            }

//            //foreach (KeyValuePair<string, string> pair in CsvData)
//            //{
//            //    Console.WriteLine(pair.Key + " : " + pair.Value);
//            //}

//            return builder.ToString();
//        }

        public const string HEADER_RECEIVE_TIME = "受信時刻";

        public static string TranslateBack(string text, SearchResultBlock block)
        {
            StringBuilder builder = new StringBuilder();

            StringReader reader = new StringReader(text);
            TextFieldParser parser = new TextFieldParser(reader);
            using (parser)
            {
                parser.TextFieldType = FieldType.Delimited; // 区切り形式とする
                parser.SetDelimiters(","); // 区切り文字はコンマとする
                //parser.CommentTokens = new string[] { "#" }; // #で始まる行はコメントとする
                //parser.HasFieldsEnclosedInQuotes = true; // 引用符付きとする
                //parser.TrimWhiteSpace = true; // 空白文字を取り除く
                parser.HasFieldsEnclosedInQuotes = true;
                parser.TrimWhiteSpace = false;
                parser.ReadLine(); // skip branch header

                int indexRow = 0;
                List<string> ctmNameList = new List<string>();
                List<string> dataTypeList = new List<string>();
                while (!parser.EndOfData)
                {
                    string[] row = parser.ReadFields(); // 1行読み込んでフィールド毎に配列に入れる

                    //row[0] = row[0].Replace(" ", "_"); // 空白を_に変換する
                    //row[1] = row[1].Replace(" ", "_"); // 空白を_に変換する
                    //CsvData.Add(row[0], row[1]); // コレクションに追加する

                    if (row.Length == 0) continue;
                    if (row.Length == 1 && string.IsNullOrEmpty(row[0].Trim())) continue;

                    List<string> outRow = new List<string>();
                    //if (indexRow == 0)
                    //{
                    //    for (int indexCol = 0; indexCol < row.Length; indexCol++)
                    //    {
                    //        if (block.rtCols.Contains(indexCol))
                    //        {
                    //            ctmNameList.Add(row[indexCol]);
                    //            outRow.Add(row[indexCol]);
                    //            outRow.Add(Properties.FoaCoreText.HEADER_RECEIVE_TIME + "_" + (block.rtCols.IndexOf(indexCol) + 1));
                    //        }
                    //        else
                    //        {
                    //            outRow.Add(row[indexCol]);
                    //        }
                    //    }
                    //    outRow.Add(string.Empty);
                    //}
                    //else if (indexRow == 1)
                    //{
                    //    for (int indexCol = 0; indexCol < row.Length; indexCol++)
                    //    {
                    //        if (block.rtCols.Contains(indexCol))
                    //        {
                    //            outRow.Add(row[indexCol]);
                    //            outRow.Add("DATETIME");
                    //        }
                    //        else
                    //        {
                    //            outRow.Add(row[indexCol]);
                    //        }
                    //    }
                    //    outRow.Add(string.Empty);
                    //}
                    //else if (indexRow == 2)
                    //{
                    //    for (int indexCol = 0; indexCol < row.Length; indexCol++)
                    //    {
                    //        if (block.rtCols.Contains(indexCol))
                    //        {
                    //            outRow.Add(row[indexCol]);
                    //            outRow.Add(string.Empty);
                    //        }
                    //        else
                    //        {
                    //            outRow.Add(row[indexCol]);
                    //        }
                    //    }
                    //    outRow.Add(string.Empty);
                    //}
                    //else
                    //{
                    //    for (int indexCol = 0; indexCol < row.Length; indexCol++)
                    //    {
                    //        if (block.rtCols.Contains(indexCol))
                    //        {
                    //            outRow.Add(ctmNameList[block.rtCols.IndexOf(indexCol)]);
                    //            outRow.Add(row[indexCol]);
                    //        }
                    //        else
                    //        {
                    //            outRow.Add(row[indexCol]);
                    //        }
                    //    }
                    //    outRow.Add(string.Empty);
                    //}
                    //if (builder.Length > 0) builder.Append(Environment.NewLine);
                    //builder.Append(string.Join(",", outRow));

                    for (int indexCol = 0; indexCol < row.Length; indexCol++)
                    {
                        row[indexCol] = row[indexCol].Replace("\"", "\"\"");
                        if (block.rtCols.Contains(indexCol) || block.branchRtCols.Contains(indexCol))
                        {
                            if (indexRow == 0)
                            {
                                ctmNameList.Add(row[indexCol]);
                                outRow.Add("\"" + row[indexCol] + "\"");
                                outRow.Add("\"" + HEADER_RECEIVE_TIME + "_" + (block.getIndexOf(indexCol) + 1) + "\"");

#if DISPLAY_NODE_TYPE
                                outNodeType.Add("\""+nodeType[indexCol]+"\"");
                                outNodeType.Add("\""+string.Empty+"\"");
#endif
                            }
                            else if (indexRow == 1)
                            {
                                outRow.Add("\"" + row[indexCol] + "\"");
                                outRow.Add("\"" + "DATETIME" + "\"");
                                dataTypeList.Add(row[indexCol]);
                            }
                            else if (indexRow == 2)
                            {
                                outRow.Add("\"" + row[indexCol] + "\"");
                                outRow.Add("\"" + string.Empty + "\"");
                            }
                            else
                            {
                                outRow.Add("\"" + ctmNameList[block.getIndexOf(indexCol)] + "\"");
                                outRow.Add("\"" + row[indexCol] + "\"");
                                //outRow.Add(row[indexCol]);
                            }
                        }
                        else
                        {
                            //if (indexRow == 1)
                            //{
                            //    dataTypeList.Add(row[indexCol]);
                            //}
                            //if (indexRow > 2)
                            //{
                            //    if (dataTypeList[indexCol].ToUpper().Contains("INT"))
                            //    {
                            //        outRow.Add("=" + row[indexCol]);
                            //        //outRow.Add("=" + (row[indexCol].StartsWith("\"") ? row[indexCol].Split('"')[1] : row[indexCol]));
                            //    }
                            //    else
                            //    {
                            //        outRow.Add(row[indexCol]);
                            //    }
                            //}
                            //else
                            //{
                            //    outRow.Add(row[indexCol]);
                            //}
                            outRow.Add("\"" + row[indexCol] + "\"");
#if DISPLAY_NODE_TYPE
                            if (indexRow == 1)
                            {
                                outNodeType.Add("\"" + string.Empty + "\"");
                            }
#endif
                        }
                    }
                    outRow.Add("\"" + string.Empty + "\"");
#if DISPLAY_NODE_TYPE
                    if (indexRow == 1)
                    {
                        outNodeType.Add("\"" + string.Empty + "\"");
                        builder.Append(string.Join(",", outNodeType));
                    }
#endif
                    // Wang Issue of branch search 20181119 Start
                    //if (indexRow > 0) builder.Append(Environment.NewLine);
                    //builder.Append(string.Join(",", outRow));
                    builder.Append(string.Join(",", outRow));
                    builder.Append(Environment.NewLine);
                    // Wang Issue of branch search 20181119 End

                    indexRow += 1;
                }
            }

            //foreach (KeyValuePair<string, string> pair in CsvData)
            //{
            //    Console.WriteLine(pair.Key + " : " + pair.Value);
            //}

            return builder.ToString();
        }
        // Wang Issue of branch search 20181120 End
    }
}
