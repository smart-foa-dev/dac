﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FoaCore.Common.Util
{
    /// <summary>
    /// GUIDクラス
    /// </summary>
    public class GUID
    {
        /// <summary>
        /// .NET Guidオブジェクト
        /// </summary>
        private Guid guid;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public GUID()
        {
            guid = Guid.NewGuid();
        }
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="id"></param>
        public GUID(Guid id)
        {
            guid = id;
        }
        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="id"></param>
        public GUID(string id)
        {
            guid = new Guid(id);
        }

        /// <summary>
        /// GUID文字列を取得する
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return guid.ToString("N").ToUpper();
        }
        /// <summary>
        /// GUIDバイト配列を取得する (.NET形式と異なり、順序変更がない)
        /// </summary>
        /// <returns></returns>
        public byte[] ToByteArray()
        {
            byte[] bytes = guid.ToByteArray();
            byte[] firstBlock = new byte[4];
            Array.Copy(bytes, 0, firstBlock, 0, 4);
            Array.Reverse(firstBlock);
            byte[] secondBlock = new byte[2];
            Array.Copy(bytes, 4, secondBlock, 0, 2);
            Array.Reverse(secondBlock);
            byte[] thirdBlock = new byte[2];
            Array.Copy(bytes, 6, thirdBlock, 0, 2);
            Array.Reverse(thirdBlock);
            Array.Copy(firstBlock, 0, bytes, 0, 4);
            Array.Copy(secondBlock, 0, bytes, 4, 2);
            Array.Copy(thirdBlock, 0, bytes, 6, 2);
            return bytes;
        }

        public bool Equals(String id)
        {
            return id.ToUpper().Equals(guid.ToString("N").ToUpper());
        }

        public bool Equals(GUID id)
        {
            return id.ToString().Equals(guid.ToString("N").ToUpper());
        }
    }
}
