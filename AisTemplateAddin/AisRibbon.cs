﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using Office = Microsoft.Office.Core;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using System.Diagnostics;
using System.ServiceModel;
using AisTemplateAddin.DacService;

// TODO:   リボン (XML) アイテムを有効にするには、次の手順に従います。

// 1: 次のコード ブロックを ThisAddin、ThisWorkbook、ThisDocument のいずれかのクラスにコピーします。

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new AisRibbon();
//  }

// 2. ボタンのクリックなど、ユーザーの操作を処理するためのコールバック メソッドを、このクラスの
//    "リボンのコールバック" 領域に作成します。メモ: このリボンがリボン デザイナーからエクスポートされたものである場合は、
//    イベント ハンドラー内のコードをコールバック メソッドに移動し、リボン拡張機能 (RibbonX) のプログラミング モデルで
//    動作するように、コードを変更します。

// 3. リボン XML ファイルのコントロール タグに、コードで適切なコールバック メソッドを識別するための属性を割り当てます。  

// 詳細については、Visual Studio Tools for Office ヘルプにあるリボン XML のドキュメントを参照してください。


namespace AisTemplateAddin
{
    // Wang Show merge sheets addin button when this is "PAT" 20181025 Start
    public class MergeInfo
    {
        public List<string> titles;
        public Dictionary<string, List<int>> titleIndex;
        public Dictionary<string, int> dataRows;
        public Dictionary<string, string> ctmNames;
    }
    // Wang Show merge sheets addin button when this is "PAT" 20181025 End

    [ComVisible(true)]
    public class AisRibbon : Office.IRibbonExtensibility
    {
        internal Office.IRibbonUI ribbon;

        internal ThisAddIn addin;

        public AisRibbon()
        {
        }

        public AisRibbon(ThisAddIn addin)
        {
            this.addin = addin;
        }

        private bool isOpenFromR2 = true;

        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        private bool isNewPeriod = false;
        private const string ONREFRESH_PERIOD = "OnRefreshPeriod";
        private const string ONREFRESH_PERIOD_BYONETIME = "OnRefreshPeriodByOneTime";
        private const string REFRESH_PERIOD = "Common";
        private const string REFRESH_BYONETIME = "ByOneTime";
        private const string GREATE_BUTTON_VBA = "Main_Module.WriteValueTextBoxPeriod";
        //ISSUE_NO.810 sunyi 2018/08/02 End

        //AISMM No.52.53 sunyi 2018/12/07 start
        //自動更新モードのときテストデータがクリックできないように修正
        private const string AUTO_REFRESH_COMMAND_START = "Main_Module.AutoUpdateStart";
        private const string AUTO_REFRESH_COMMAND_STOP = "Main_Module.AutoUpdateStop";
        //AISMM No.52.53 sunyi 2018/12/07 end

        // Wang Issue AISMM-91 20190124 Start
        //// Wang Show merge sheets addin button when this is "PAT" 20181025 Start
        //private const string BULK_SHEET_NAME = "BULK";
        //private const int MAX_BUFF_ROWS = 5000;
        //// Wang Show merge sheets addin button when this is "PAT" 20181025 End
        // Wang Issue AISMM-91 20190124 End

        // Wang New dac flow 20190308 Start
        private const string DAC_SHEET = "ＤＡＣシート"; // MAX: Column 'BJ'
        // Wang New dac flow 20190308 End

        private const string SAVE_FILE_SUB = "Main_Module.SaveFileSub";
        //Dac_Home sunyi 20190530 start
        private const string SAVE_TEMPLATE_SUB = "Main_Module.SaveTemplateSub";
        //Dac_Home sunyi 20190530 end

        #region IRibbonExtensibility のメンバー

        /// <summary>
        /// Return ribbon UI
        /// </summary>
        public string GetCustomUI(string ribbonID)
        {
            return GetResourceText("AisTemplateAddin.AisRibbon.xml");
        }

        #endregion

        #region リボンのコールバック
        //ここにコールバック メソッドを作成します。コールバック メソッドの追加方法の詳細については、http://go.microsoft.com/fwlink/?LinkID=271226 にアクセスしてください。

        public void Ribbon_Load(Office.IRibbonUI ribbonUI)
        {
            this.ribbon = ribbonUI;
        }

        #endregion

        /// <summary>
        /// Show only the addin tab which spacified in excel file
        /// </summary>
        public bool GetTabVisible(Office.IRibbonControl control)
        {
            try
            {
                // Get addin tab id
                string id = control.Id;

                // Compare addin tab id with which defined in excel paramter
                if (id.Equals(Globals.ThisAddIn.addinParams.AddInTab))
                {
                    return true;
                }
            }
            catch
            {

            }

            return false;
        }

        /// <summary>
        /// Enable button while FIFO is online
        /// </summary>
        public bool EnabledOnlineButtonWhenAutoRefresh(Office.IRibbonControl control)
        {
            try
            {
                return Globals.ThisAddIn.addinParams.IsOnline() && Globals.ThisAddIn.addinParams.IsAutoFreshMode();
            }
            catch 
            {

            }

            return false;
        }

        /// <summary>
        /// Enable button while FIFO is online
        /// </summary>
        public bool EnabledOnlineButtonWhenNotAutoRefresh(Office.IRibbonControl control)
        {
            try
            {
                return Globals.ThisAddIn.addinParams.IsOnline() && !Globals.ThisAddIn.addinParams.IsAutoFreshMode();
            }
            catch 
            {

            }

            return false;
        }

        /// <summary>
        /// Enable button while Online is Period fixed
        /// </summary>
        public bool EnabledKindAndIsAutoFresh(Office.IRibbonControl control)
        {
            try
            {
                return Globals.ThisAddIn.addinParams.IsPeriodFixed() && !Globals.ThisAddIn.addinParams.IsAutoFreshMode();
            }
            catch 
            {

            }

            return false;
        }

        /// <summary>
        /// Enable button　更新停止
        /// </summary>
        public bool EnableWhenAutoRefresh(Office.IRibbonControl control)
        {
            try
            {
                return Globals.ThisAddIn.addinParams.IsAutoFreshMode();
            }
            catch 
            {

            }

            return false;
        }

        /// <summary>
        /// Enable button　自動更新とテスト
        /// </summary>
        public bool DisableWhenAutoRefresh(Office.IRibbonControl control)
        {
            try
            {
                return !Globals.ThisAddIn.addinParams.IsAutoFreshMode();
            }
            catch 
            {

            }

            return false;
        }

        //dac home start
        /// <summary>
        /// Enable button　自動更新とテスト
        /// </summary>
        public bool DisableWhenOpenFromR2(Office.IRibbonControl control)
        {
            Globals.ThisAddIn.addinParams = new AddInParams(Globals.ThisAddIn.Application.ActiveWorkbook);
            try
            {
                 isOpenFromR2 = Globals.ThisAddIn.addinParams.IsOpenFromR2();
                 return isOpenFromR2;
            }
            catch
            {

            }
            return true;
        }
        public bool DisableWhenOpenFromR2_g0(Office.IRibbonControl control)
        {
            return isOpenFromR2;
        }
        //dac home start

        //ISSUE_NO.727 sunyi 2018/06/14 Start
        public bool DisableWhenIsGrip(Office.IRibbonControl control)
        {
            try
            {
                return !Globals.ThisAddIn.addinParams.IsGripMode();
            }
            catch
            {

            }

            return true;
        }
        //ISSUE_NO.727 sunyi 2018/06/14 End

        /// <summary>
        /// Enable button　マルチのみ使用の自動更新
        /// </summary>
        public bool DisableWhenAutoRefreshMutil(Office.IRibbonControl control)
        {
            try
            {
                return !Globals.ThisAddIn.addinParams.IsAutoFreshMode() && !Globals.ThisAddIn.addinParams.IsSearchType();
            }
            catch
            {

            }

            return false;
        }
        /// <summary>
        /// Enable button　編集モードON
        /// </summary>
        public bool EnableWhenSwitchON(Office.IRibbonControl control)
        {
            try
            {
                return Globals.ThisAddIn.addinParams.IsCodeModeON();
            }
            catch
            {

            }

            return false;
        }

        /// <summary>
        /// Enable button　編集モードOFF
        /// </summary>
        public bool DisableWhenSwitchON(Office.IRibbonControl control)
        {
            try
            {
                return !Globals.ThisAddIn.addinParams.IsCodeModeON();
            }
            catch
            {

            }

            return false;
        }

        /// <summary>
        /// Enable button 期間指定
        /// </summary>
        public bool EnableWhenSearchTypeON(Office.IRibbonControl control)
        {
            try
            {
                //ISSUE_NO.727 sunyi 2018/06/14 Start
                //return Globals.ThisAddIn.addinParams.IsSearchType() && !Globals.ThisAddIn.addinParams.IsAutoFreshMode();
                return Globals.ThisAddIn.addinParams.IsSearchType() && !Globals.ThisAddIn.addinParams.IsAutoFreshMode() && !Globals.ThisAddIn.addinParams.IsGripMode();
                ////ISSUE_NO.727 sunyi 2018/06/14 End
            }
            catch
            {

            }

            return false;
        }

        //20170421 sun no.589 -s
        /// <summary>
        /// Enable button while Moment,PostStock,SpreadSheet,Chimney,Comment,FIFO,Frequency,LeadTime is CTM
        /// </summary>
        public bool EnabledTypeCtmButton(Office.IRibbonControl control)
        {
            try
            {
                return Globals.ThisAddIn.addinParams.IsTypeCtm();
            }
            catch
            {

            }

            return false;
        }
        //20170421 sun no.589 -e

        public bool EnabledButton(Office.IRibbonControl control)
        {
            try
            {
                return Globals.ThisAddIn.addinParams.IsTypeCtm() && Globals.ThisAddIn.addinParams.IsOnline() && !Globals.ThisAddIn.addinParams.IsAutoFreshMode();
            }
            catch 
            {

            }

            return false;
        }

        private bool RibbonButtonAction(Office.IRibbonControl control)
        {
            // Get index from button's tag
            string[] tag = control.Tag.Split('_');

            string methodName = tag.Count() > 1 ? tag[1] : "";

            try
            {
                if (string.IsNullOrEmpty(methodName))
                {
                    return true;
                }

                MethodInfo method = this.GetType().GetMethod(methodName, BindingFlags.NonPublic | BindingFlags.Instance);
                if (method != null)
                {
                    //ISSUE_NO.810 sunyi 2018/08/02 Start
                    //仕様変更、ボタン作成機能を追加する
                    //最初の期間再設定の場合、param「true」を追加する
                    if (methodName.Equals(ONREFRESH_PERIOD) || methodName.Equals(ONREFRESH_PERIOD_BYONETIME))
                    {
                        var ret = method.Invoke(this, new object[] { true });
                        return (bool)ret;
                    }
                    else
                    //ISSUE_NO.810 sunyi 2018/08/02 End
                    {
                        var ret = method.Invoke(this, new object[] { });
                        return (bool)ret;
                    }
                }
            }
            catch
            {

            }

            return true;
        }

        //Graph_Alarm Sunyi 2018/07/27 Start
        public bool OnGraphAlarmSettingCOM()
        {
            return OnGraphAlarmSetting();
        }
        private bool OnGraphAlarmSetting()
        {
            GraphAlarmSettingForm form = new GraphAlarmSettingForm();
            form.ShowDialog(GetOwnerWrapper());
            return form.DialogResult.Equals(System.Windows.Forms.DialogResult.OK);
        }
        //Graph_Alarm Sunyi 2018/07/27 End

        public bool OnRefreshPeriodCOM()
        {
            //階層ステータスモニターの場合
            if (Globals.ThisAddIn.addinParams.IsMultiStatusMonitor() || Globals.ThisAddIn.addinParams.IsTorqueStatusMonitor() || Globals.ThisAddIn.addinParams.IsStreamStatusMonitor())
            {
                return OnRefreshPeriodByOneTime();
            }

            return OnRefreshPeriod();
        }

        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        //期間再設定のインタフェース作成
        public bool OnRefreshPeriodCOM_Common()
        {
            return OnRefreshPeriod(false);
        }

        public bool OnRefreshPeriodCOM_ByOneTime()
        {
            return OnRefreshPeriodByOneTime(false);
        }
        //ISSUE_NO.810 sunyi 2018/08/02 End

        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        //private bool OnRefreshPeriod()
        private bool OnRefreshPeriod(bool IsNewPeriod = false)
        {
            //RefreshPeriodForm form = new RefreshPeriodForm();
            RefreshPeriodForm form = new RefreshPeriodForm(IsNewPeriod);
            //ISSUE_NO.810 sunyi 2018/08/02 End
            //ISSUE_NO.782 sunyi 2018/07/17 Start
            //form.ShowDialog(new WindowWrapper(Globals.ThisAddIn.Application.ActiveWindow.Hwnd));
            form.ShowDialog(GetOwnerWrapper());
            //ISSUE_NO.782 sunyi 2018/07/17 End
            //ISSUE_NO.810 sunyi 2018/08/02 Start
            //仕様変更、ボタン作成機能を追加する
            isNewPeriod = form.IsNew_Period;
            //ISSUE_NO.810 sunyi 2018/08/02 End
            return form.DialogResult.Equals(System.Windows.Forms.DialogResult.OK);
        }
        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        //private bool OnRefreshPeriodByOneTime()
        private bool OnRefreshPeriodByOneTime(bool IsNewPeriod= false)
        {
            //RefreshPeriodFormByOneTime form = new RefreshPeriodFormByOneTime();
            RefreshPeriodFormByOneTime form = new RefreshPeriodFormByOneTime(IsNewPeriod);
            //ISSUE_NO.810 sunyi 2018/08/02 End
            //ISSUE_NO.782 sunyi 2018/07/17 Start
            //form.ShowDialog(new WindowWrapper(Globals.ThisAddIn.Application.ActiveWindow.Hwnd));
            form.ShowDialog(GetOwnerWrapper());
            //ISSUE_NO.782 sunyi 2018/07/17 End
            //ISSUE_NO.810 sunyi 2018/08/02 Start
            //仕様変更、ボタン作成機能を追加する
            isNewPeriod = form.IsNew_Period;
            //ISSUE_NO.810 sunyi 2018/08/02 End
            return form.DialogResult.Equals(System.Windows.Forms.DialogResult.OK);
        }
        //ISSUE_NO.782 sunyi 2018/07/17 Start
        private WindowWrapper GetOwnerWrapper()
        {
            try
            {
                int wnd = Globals.ThisAddIn.Application.ActiveWindow.Hwnd;
                WindowWrapper wrapper = new WindowWrapper(wnd);
                return wrapper;
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }
            return null;
        }
        //ISSUE_NO.782 sunyi 2018/07/17 End

        // Wang Show merge sheets addin button when this is "PAT" 20181025 Start
        public void OnMergeData(Office.IRibbonControl control)
        {
            // Wang Issue AISMM-91 20190124 Start
            //Worksheet bulk;
            //List<string> csvSheets = Globals.ThisAddIn.addinParams.GetQuickGraphDataSheets();
            //Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
            //if (csvSheets.Count > 0)
            //{
            //    try
            //    {
            //        bulk = workbook.Sheets[BULK_SHEET_NAME];
            //        if (bulk != null)
            //        {
            //            bulk.Application.DisplayAlerts = false;
            //            bulk.Delete();
            //            bulk.Application.DisplayAlerts = true;
            //        }
            //    }
            //    catch (Exception e)
            //    {

            //    }

            //    try
            //    {
            //        bulk = workbook.Sheets.Add(After: workbook.Sheets[workbook.Sheets.Count]);
            //        bulk.Name = BULK_SHEET_NAME;

            //        // merge titles
            //        MergeInfo mergeInfo = MergeTitle(workbook, csvSheets);
            //        object[,] values = new object[MAX_BUFF_ROWS, mergeInfo.titles.Count];
            //        object[] tmpvalues = new object[mergeInfo.titles.Count];
            //        for (int i = 0; i < mergeInfo.titles.Count; i++)
            //        {
            //            values[0, i] = mergeInfo.titles[i];
            //        }

            //        // initialize data start row
            //        int[] srcRow = new int[csvSheets.Count];
            //        for (int i = 0; i < csvSheets.Count; i++)
            //        {
            //            srcRow[i] = 2;
            //        }

            //        // merge data
            //        int dstRow = 1;
            //        int sheetRow = 1;
            //        foreach (object[] rowValue in SetMinRTRowValues(workbook, mergeInfo, csvSheets, srcRow, tmpvalues))
            //        {
            //            for (int i = 0; i < mergeInfo.titles.Count; i++)
            //            {
            //                values[dstRow, i] = rowValue[i];
            //            }
            //            dstRow = (dstRow + 1) % MAX_BUFF_ROWS;

            //            if ((dstRow % MAX_BUFF_ROWS) == 0)
            //            {
            //                SetRangeValue(bulk.Cells, sheetRow, 1, values);
            //                sheetRow += MAX_BUFF_ROWS;
            //            }
            //        }

            //        if (dstRow > 0)
            //        {
            //            object[,] remainValues = GetRemainValues(values, dstRow);
            //            SetRangeValue(bulk.Cells, sheetRow, 1, remainValues);
            //        }

            //    }
            //    catch (Exception e)
            //    {

            //    }
            //}

            ResultSheetGenerator generator = ResultSheetGenerator.CreateGenerator(control);
            if (generator != null)
            {
                generator.MergeData();
            }
            // Wang Issue AISMM-91 20190124 End
        }

        public bool CanBeMerge(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.addinParams.MiscParams["SearchMode"] == "PAT")
                {
                    return true;
                }
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }
            return false;
        }
        // Wang Show merge sheets addin button when this is "PAT" 20181025 End

        // Wang Issue DAC-96 20190320 Start
        public void OnCreateDac(Office.IRibbonControl control)
        {
            try
            {
                string dacFile = this.addin.Application.ActiveWorkbook.FullName;

                using (DacServiceClient client = new DacServiceClient())
                {
                    client.Open();

                    try
                    {
                        //Dac_Home sunyi 20190530 start
                        string isTemplate = "";
                        for(int i = 1; i < 30 ; i++)
                        {
                            if (this.addin.Application.ActiveWorkbook.Worksheets["param"].Cells[i, 1].Value == null)
                            {
                                continue;
                            }
                            if (this.addin.Application.ActiveWorkbook.Worksheets["param"].Cells[i, 1].Value.ToString() == "IsTemplate")
                            {
                                if (this.addin.Application.ActiveWorkbook.Worksheets["param"].Cells[i, 2].Value != null)
                                {
                                    isTemplate = this.addin.Application.ActiveWorkbook.Worksheets["param"].Cells[i, 2].Value.ToString();
                                }
                                continue;
                            }
                        }
                        if (isTemplate.Equals("True"))
                        {
                            DateTime? start = GetTimeByCell(this.addin.Application.ActiveWorkbook.Worksheets[DAC_SHEET].Cells[5, 3].Value);
                            DateTime? end = GetTimeByCell(this.addin.Application.ActiveWorkbook.Worksheets[DAC_SHEET].Cells[6, 3].Value);

                            HideCloseMessage();
                            this.addin.Application.ActiveWorkbook.Save();
                            this.addin.Application.ActiveWorkbook.Close();
                            this.addin.Application.Quit();
                            client.UpdateDac(dacFile, start, end);
                        }
                        //Dac_Home sunyi 20190530 end
                        else if (client.ValidateDac(dacFile).Equals(DacExecutionResult.Success))
                        {
                            DateTime? start = GetTimeByCell(this.addin.Application.ActiveWorkbook.Worksheets[DAC_SHEET].Cells[5, 3].Value);
                            DateTime? end = GetTimeByCell(this.addin.Application.ActiveWorkbook.Worksheets[DAC_SHEET].Cells[6, 3].Value);

                            HideCloseMessage();
                            this.addin.Application.ActiveWorkbook.Save();
                            this.addin.Application.ActiveWorkbook.Close();
                            this.addin.Application.Quit();

                            client.CreateDac(dacFile, start, end);
                        }
                        else if (client.ValidateDac(dacFile).Equals(DacExecutionResult.InvalidateDac))
                        {
                            System.Windows.MessageBox.Show(Properties.Resources.AIS_E_003);
                        }
                    }
                    finally
                    {
                        client.Close();
                    }
                }
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {
                System.Windows.MessageBox.Show(Properties.Resources.ADDIN_E_001+e.Message + e.StackTrace);
            }
        }

        private DateTime? GetTimeByCell(object value)
        {
            if (value == null)
            {
                return null;
            }

            try
            {
                return DateTime.FromOADate(double.Parse(value.ToString()));
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {
                return null;
            }
        }
        // Wang Issue DAC-96 20190320 End

        /// <summary>
        /// Do something, and then call vb macro
        /// </summary>
        public void OnAddinAction(Office.IRibbonControl control)
        {
            try
            {
                
                // Get index from button's tag
                string[] tag = control.Tag.Split('_');
                int index = int.Parse(tag[0]);

                // Call vb macro before c# action
                if (index < Globals.ThisAddIn.addinParams.AddInPreCommand.Count)
                {
                    string command = Globals.ThisAddIn.addinParams.AddInPreCommand[index];
                    if (!string.IsNullOrEmpty(command))
                    {
                        Globals.ThisAddIn.Application.Run(command);
                    }
                }

                // Run some operation within this c# module
                if (RibbonButtonAction(control))
                {
                    //ISSUE_NO.810 sunyi 2018/08/02 Start
                    //仕様変更、ボタン作成機能を追加する
                    //ボタン作成の場合、isNewPeriod = true、マクロを実行する
                    if (isNewPeriod)
                    {
                        isNewPeriod = false;
                        //期間再設定か期間再設定（日）を取得する
                        string methodName = tag.Count() > 1 ? tag[1] : "";
                        if (methodName.Equals(ONREFRESH_PERIOD))
                        {
                            methodName = REFRESH_PERIOD;
                        }
                        else
                        {
                            methodName = REFRESH_BYONETIME;
                        }
                        Globals.ThisAddIn.Application.Run(GREATE_BUTTON_VBA, methodName);
                        return;
                    }
                    //ISSUE_NO.810 sunyi 2018/08/02 End

                    // Call vb macro after c# action
                    if (index < Globals.ThisAddIn.addinParams.AddInPostCommand.Count)
                    {
                        string command = Globals.ThisAddIn.addinParams.AddInPostCommand[index];

                        if (!string.IsNullOrEmpty(command))
                        {
                            // Wang Issue DAC-96 20190320 Start
                            if (command.Equals(SAVE_FILE_SUB) || command.Equals(SAVE_TEMPLATE_SUB))
                            {
                                HideCloseMessage();
                            }
                            // Wang Issue DAC-96 20190320 End

                            Globals.ThisAddIn.Application.Run(command);
                            Globals.ThisAddIn.addinParams.ReloadParam(command);
                        }
                    }
                }
            }
            catch
            {

            }
        }

        // Wang Issue DAC-96 20190320 Start
        private void HideCloseMessage()
        {
            this.addin.wbs.Remove(this.addin.Application.ActiveWorkbook);
        }
        // Wang Issue DAC-96 20190320 End

        //foastudio AisAddin sunyi 2018/10/26 start
        public void OnToggle(Office.IRibbonControl control, bool isPressed)
        {
            Globals.ThisAddIn.Application.Run("Main_Module.SetIsPressed", isPressed);
        }

        public void OnToggleUpdate(Office.IRibbonControl control, bool isUpdated)
        {
            //AISMM No.52.53 sunyi 2018/12/07 start
            //自動更新モードのときテストデータがクリックできないように修正
            //Globals.ThisAddIn.Application.Run("Main_Module.SetIsUpdated", isUpdated);
            if (isUpdated)
            {
                Globals.ThisAddIn.Application.Run(AUTO_REFRESH_COMMAND_START);
                Globals.ThisAddIn.addinParams.ReloadParam(AUTO_REFRESH_COMMAND_START);
            }
            else
            {
                Globals.ThisAddIn.Application.Run(AUTO_REFRESH_COMMAND_STOP);
                Globals.ThisAddIn.addinParams.ReloadParam(AUTO_REFRESH_COMMAND_STOP);
            }
            //AISMM No.52.53 sunyi 2018/12/07 end
        }

        //検証シート_マルチモニター_No.65 sunyi 2018/11/20 start
        public bool DisableWhenPressedUpdate(Office.IRibbonControl control)
        {
            try
            {
                return Globals.ThisAddIn.addinParams.IsPressedUpdate();
            }
            catch
            {

            }

            return false;
        }
        //検証シート_マルチモニター_No.65 sunyi 2018/11/20 end
        //foastudio AisAddin sunyi 2018/10/26 end


        // 2018/10/13 iwai start 
        // 期間検索、滞留数計算
        private string interval = "";

        public void OnAddinComboBox(Office.IRibbonControl control, string item)
        {
            this.interval = item;
        }

        // 期間検索　滞留計算の出力を時刻の昇順、降順?
        private string order = "降順"; // ascend またはdescend がはいる
        public void OnAddinComboBox2(Office.IRibbonControl control, string item)
        {
            this.order = item;
            //MessageBox.Show("order=" + item);
        }

        public bool IsInventoryCalc(Office.IRibbonControl control)
        {
            try
            {
                if (Globals.ThisAddIn.addinParams.MiscParams["SearchMode"] == "Period")
                {
                    return true;
                }
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }
            return false;
        }

        public bool IsNormalQuickGraph(Office.IRibbonControl control)
        {
            return !IsInventoryCalc(control);
        }

        public string GetInventoryInterval(Office.IRibbonControl control)
        {
            return "3分";
        }
        public string GetInventoryOrder(Office.IRibbonControl control)
        {
            return "降順";
        }
        public void OnAddinCSharpAction(Office.IRibbonControl control)
        {
            string version = Globals.ThisAddIn.addinParams.MiscParams["Version"];
            if (string.IsNullOrEmpty(version))
            {
                OnInventoryCalc(control, interval);
            }
            else if (version == "2")
            {
                OnInventoryCalc4(control, interval);
            }
            else 
            {
                // Not implemented
                MessageBox.Show("クイックグラフのバージョンが対応していません。新しいAIS Template Addinをインストールしてください");
            }
        }

        // 期間検索で、重複滞留数計算対象データを削除する
        public void OnRemoveDuplicate(Office.IRibbonControl control)
        {
            OnInventryRemoveDuplicatesForm test = new OnInventryRemoveDuplicatesForm();
            try
            {
                test.Show();

                Microsoft.Office.Interop.Excel.Worksheet currentSheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet;

                RemoveDuplicateData.CreateUniquDataSheet(currentSheet);

                test.Close();
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {
                test.Close();
            }
        }
        private bool isDateTimeOrNull(string a)
        {
            if (string.IsNullOrEmpty(a))
            {
                return true;
            }
            else
            {
                return isDateTime(a);
            }
        }
        private bool isDateTime(string a)
        {
            string[] datetimeregex = new string[] { @"\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{2}:\d{2}", @"\d{4}/\d{1,2}/\d{1,2} \d{1,2}:\d{2}:\d{2}", 
                                                    @"\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{2}",       @"\d{4}/\d{1,2}/\d{1,2} \d{1,2}:\d{2}" };
            if (string.IsNullOrEmpty(a))
            {
                return false;
            }
            foreach (string i in datetimeregex)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(a, i))
                {
                    return true;
                }
            }

            //else if (System.Text.RegularExpressions.Regex.IsMatch(a, @"\d{4}-\d{2}-\d{2} \d{1,2}:\d{2}:\d{2}"))
            //{
            // YYYY-MM-DD H:MM:SS
            //    return true;
            //}
            return false;
        }
        private bool GetOrder(string order) 
        {
            if (string.IsNullOrEmpty(order))
            {
                return false;
            }
            if (order.Contains("昇順"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private int GetInterval(string a, int defint)
        {
            if (string.IsNullOrEmpty(a))
            {
                return defint;
            }
            string[] reg = new string[] { @"(?<num>\d+?)秒間?", @"(?<num>\d+?)分間?", @"(?<num>\d+?)時間?" };
            int[] multi = new int[] { 1, 60, 3600 };
            int i = 0;
            foreach (string r in reg)
            {
                var m = new System.Text.RegularExpressions.Regex(r);
                var n = m.Match(a);
                while (n.Success)
                {
                    string numstr = n.Groups["num"].Value;
                    int num = int.Parse(numstr) * multi[i];
                    return num;
                }
                i = i + 1;
            }
            return defint;
        }
        private string getNewSheetNameForInventoryCalc(string sheetName)
        {
            int i,j;
            bool dupflag = false;
            for (j = 0; j < 1000; j++)
            {
                string checkName = "";
                if (j == 0)
                {
                    checkName = sheetName;
                }
                else
                {
                    checkName = sheetName + "(" + j.ToString("D") + ")";
                }
                dupflag = false;
                for (i = 1; i <= Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.Count; i++)
                {
                    if (Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets[i].Name == checkName)
                    {
                        //MessageBox.Show("dup name=" + checkName + " here=" + Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets[i].Name);
                        dupflag = true;
                        break;
                    }
                }
                if (dupflag == false)
                {
                    return checkName;
                }
            }
            // ERROR
            return sheetName;
        }
        /**
         * 期間検索第３弾
         * 　１つのシートで、１行の空きを認識してそれがグループとして分けられていると認識するようにする。
         * 　 2行の空白が存在した場合、グループセパレータとして認識する。
         */
        private void OnInventoryCalc3(Office.IRibbonControl control, string uiStr) { 
            // 先頭列は、CTMからで出の時間
            // 2列目は、CTMへの入りの時間
            // グループ分けは、１行の空行（判断はカラム1,2が入っていない場合と判断)
            // 2行が空行の場合はデータの終わりを表す。
            int interval = 60*3; // 秒で計算 // 3分がデフォルトなので
            int startLine = 2; // データの開始行
            bool orderAscend = false; // 時間の順
            const int MaxLineForExcel = 100000; // 100000行(先頭のstartLine除く)までのデータを対象にする。
           
            try
            {
                orderAscend = GetOrder(this.order);
                interval = Util.GetInterval(uiStr, interval); // Excel Addin で指定された時間間隔
                Worksheet sheet = Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet; // 現在のアクティブシートで滞留計算を行う。
                DateTime maxDateTime = DateTime.MinValue;
                DateTime minDateTime = DateTime.MaxValue;
                HashSet<string> groups = new HashSet<string>();
                int i = 0;
                //for (i = 0; i < MaxLineForExcel + 1; i++)
                for (int groupCount = 0; groupCount < 100; groupCount++)
                {
                    int startGroupNo = i;
                    while (i < MaxLineForExcel + 1)
                    {
                        string atimestr = (string)sheet.Range["A" + (i + startLine).ToString("D")].Text;
                        string btimestr = (string)sheet.Range["B" + (i + startLine).ToString("D")].Text;
                        //string groupstr = sheet.Range["C" + (i + startLine).ToString("D")].Text;
                        if (!(Util.isDateTimeOrNull(atimestr) && Util.isDateTimeOrNull(btimestr)))
                        {
                            // 時刻フォーマットではない場合はここでグループデータが終了と判断
                            break;
                        }
                        if (string.IsNullOrEmpty(atimestr) && string.IsNullOrEmpty(btimestr))
                        {
                            // 同時にデータがない場合はデータの終了と判断
                            break;
                        }
                    }
                    int endGroupNo = i;
                    // [startGroupNo,endGroupNo] までが１つのグループ
                    
                        // 最大時刻と、最小時刻を取り出す。
                    // 出力シートの時刻は、maxDateTime,minDateTimeの間のみ。
                    // グルーピングしたときの種類をすべて探す
                }
                
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }

        }
        private bool OnInventoryCalc4Exec(string colrow)
        {
            OnInventryForm form = new OnInventryForm();
            form.ShowDialog(GetOwnerWrapper());
            //            return form.DialogResult.Equals(System.Windows.Forms.DialogResult.OK);
            return true;
        }
        /**
        
        **/
        private void OnInventoryCalc4(Office.IRibbonControl control,string uiStr)
        {
            // カラムが選択されている場合は、続行する。されていない場合は、dialog出して戻る。
            string pos = "";
            foreach (Microsoft.Office.Interop.Excel.Range range in Globals.ThisAddIn.Application.ActiveCell) {
                pos = range.Columns.Address;
            }

            if (!string.IsNullOrEmpty(pos))
            {
                // pos は、 $D$3 のようなフォーマット $ に挟まれた部分を調べる。
                // MessageBox.Show("Column=" + pos);
                // １つのカラムのみ選択されている
                //MessageBox.Show("Column=" + pos);
                OnInventoryCalc4Exec(pos);
            }
            else
            {
                // カラムが選択されていないか、複数選択されている
                DialogResult result = MessageBox.Show(
                    "カラムを1つのみ選択してこのボタンを押してください。",
                    "エラー",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                if (result == DialogResult.OK)
                {
                    return;
                }
            }
        }

        /**
         * 期間検索第二弾
         *   グループ機能をいれたもの
         **/
        private void OnInventoryCalc2(Office.IRibbonControl control,string uiStr)
        {
            int interval = 60*3; // 秒で計算 // 3分がデフォルトなので
            int startLine = 2; // データの開始行
            bool orderAscend = false; // 時間の順
            const int MaxLineForExcel = 100000; // 100000行(先頭のstartLine除く)までのデータを対象にする。
            try
            {
                orderAscend = GetOrder(this.order);
                interval = GetInterval(uiStr, interval); // Excel Addin で指定された時間間隔
                Worksheet sheet = Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet; // 現在のアクティブシートで滞留計算を行う。
                // 先頭列はCTMからの出の時刻
                // 2列目はCTMへの入りの時刻
                // 3列目はグループ名(指定されずない場合ない場合、指定していながらない場合含む)
                // 4列目は出のCTM名
                // 5列目は入りのCTM名
                DateTime maxDateTime = DateTime.MinValue;
                DateTime minDateTime = DateTime.MaxValue;
                HashSet<string> groups = new HashSet<string>();
                int i = 0;
                for (i = 0; i < MaxLineForExcel + 1; i++)
                {
                    string atimestr = sheet.Range["A" + (i+startLine).ToString("D")].Text;
                    string btimestr = sheet.Range["B" + (i+startLine).ToString("D")].Text;
                    string groupstr = sheet.Range["C" + (i+startLine).ToString("D")].Text;
                    if (!(isDateTimeOrNull(atimestr) && isDateTimeOrNull(btimestr))) 
                    {
                        // 時刻フォーマットではない場合はここでデータが終了と判断
                        break;
                    }
                    if (string.IsNullOrEmpty(atimestr) && string.IsNullOrEmpty(btimestr))
                    {
                        // 同時にデータがない場合はデータの終了と判断
                        break;
                    }
                    // 最大時刻と、最小時刻を取り出す。
                    if (isDateTime(atimestr))
                    {
                        DateTime atime = DateTime.Parse(atimestr);
                        if (atime >= maxDateTime)
                        {
                            maxDateTime = atime;
                        }
                        if (atime < minDateTime)
                        {
                            minDateTime = atime;
                        }
                    }
                    // 最大時刻と、最小時刻を取り出す。
                    if (isDateTime(btimestr))
                    {
                        DateTime btime = DateTime.Parse(btimestr);
                        if (btime >= maxDateTime)
                        {
                            maxDateTime = btime;
                        }
                        if (btime < minDateTime)
                        {
                            minDateTime = btime;
                        }
                    }
                    // 出力シートの時刻は、maxDateTime,minDateTimeの間のみ。
                    // グルーピングしたときの種類をすべて探す
                    if (string.IsNullOrEmpty(groupstr)) {
                        groupstr = "";
                    }
                    groups.Add(groupstr); // Setなので重複は無いようにいれる。
                }
                int rowcount = i;
                
                string WarningMessage = "";
                if (rowcount >= MaxLineForExcel)
                {
                    WarningMessage = "\n警告：データ数が" + MaxLineForExcel + "以上あります。これを超える分は計算されません";
                }
                string currentSheetName = sheet.Name;
                string newSheetName = "滞留数-" + currentSheetName;

                // 出力先sheet名が重ならないように、postfixを加えたシート名を返す。
                newSheetName = getNewSheetNameForInventoryCalc(newSheetName); // 1000個まで探す。

                // 実行の確認ダイアログ
                DialogResult result = MessageBox.Show("バージョン: 2\n時間間隔:" + interval + "秒\nデータ数: " + rowcount.ToString("D") + "\n時刻順序:" + this.order + "\nシート名:" + newSheetName + WarningMessage, "期間検索",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1
                    );
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                /**
                 * maxDateTime,minDateTimeの時間でinterval間隔での配列を作る
                 * maxDateTime - minDateTime
                 */
                if (maxDateTime < minDateTime)
                {
                    // 時間が逆転しているので、計算しない
                    MessageBox.Show("時刻が逆になっているため計算できません。");
                    return;
                }
                TimeSpan allInterval = maxDateTime - minDateTime;

                long arraySize = (long)(allInterval.TotalSeconds / interval) + 1;
                if (arraySize > 1000000)
                {
                    result = MessageBox.Show("滞留間隔が短い、または間隔が長いため計算できません", "エラー",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (result == DialogResult.OK)
                    {
                        return;
                    }
                }
                // 滞留計算のカウントを以下で行う。
                // 1. カウント用のカウンタを準備
                int[] counter = new int[arraySize];
                DateTime[] ctime = new DateTime[arraySize];
                IDictionary<string,int[]> map = new Dictionary<string,int[]>();

                for (i = 0; i < arraySize; i++)
                {
                    ctime[i] = maxDateTime - TimeSpan.FromSeconds(interval * i);
                }
                foreach (string group in groups)
                {
                    map.Add(group, new int[arraySize]);
                    for (int j = 0 ; j < arraySize ; j++) {
                        map[group][j] = 0;
                    }
                }
                int revcount = 0; // 時刻が逆転しているものをカウントする。
                int nohitcount = 0; // 滞留数としてカウントされなかった個体の数。
                for (i = 0; i < rowcount ; i++)
                {
                    string atimestr = sheet.Range["A" + (i+startLine).ToString("D")].Text;
                    string btimestr = sheet.Range["B" + (i+startLine).ToString("D")].Text;
                    string groupstr = sheet.Range["C" + (i+startLine).ToString("D")].Text;
                    if (string.IsNullOrEmpty(groupstr)) {
                        groupstr = "";
                    }
                    if (!(isDateTimeOrNull(atimestr) && isDateTimeOrNull(btimestr)))
                    {
                        // 同時にない場合はここでデータが終了と判断
                        break;
                    }
                    DateTime atime, btime;
                    if (string.IsNullOrEmpty(atimestr))
                    {
                        atime = DateTime.MinValue;
                    }
                    else
                    {
                        //atime = DateTime.Parse(atimestr);
                        atime = DateTime.FromOADate(sheet.Range["A" + (i+startLine).ToString("D")].Value2);
                    }
                    if (string.IsNullOrEmpty(btimestr))
                    {
                        btime = DateTime.MaxValue;
                    }
                    else
                    {
                        //btime = DateTime.Parse(btimestr);
                        btime = DateTime.FromOADate(sheet.Range["B" + (i+startLine).ToString("D")].Value2);
                    }
                    if (atime.CompareTo(btime) > 0) {
                        // 逆転している。
                        revcount++;
                    }
                    Boolean flag = false;
                    for (int j = 0; j < arraySize; j++)
                    {
                        if (atime.CompareTo(ctime[j]) <= 0 && ctime[j].CompareTo(btime) < 0)
                        {
                            map[groupstr][j] = map[groupstr][j] + 1;
                            flag = true;
                        }
                    }
                    if (!flag) {
                        nohitcount++;
                    }
                }
                // 全計算終了。残り、シートに値を埋める。
                /**
                 *  結果が、ctime[],map[グループ][]に arraySize分入っているので、これを別シートに作る
                 **/
                Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.Add();
                Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet.Name = newSheetName;
                Worksheet outsheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets[newSheetName];

                // ヘッダ部分の書き込み。
                outsheet.Range["A1"].Value = "時刻";
                if (groups.Count == 1)
                {
                    outsheet.Range["B1"].Value = "滞留数";
                }
                else
                {
                    int ii = 2;
                    foreach (string group2 in groups)
                    {
                        Range rg3 = outsheet.Cells[1,ii];
                        rg3.Value = "滞留数(" + group2 + ")";
                        ii++;
                    }

                }

                // 表の書き込み。
                for (i = 0; i < arraySize; i++)
                {
                    int ii; 
                    if (orderAscend)
                    {
                        ii = ((int)arraySize) -1 - i ;
                    }
                    else
                    {
                        ii = i;
                    }
                    // 横一行分の書き込み。
                    outsheet.Range["A" + (ii + 2).ToString("D")].Value = ctime[i].ToString("yyyy/MM/dd HH:mm:ss"); // 文字列にconvert
                    int kk = 2;
                    foreach (string group2 in groups)
                    {
                        Range rg2 = outsheet.Cells[ii + 2, kk];
                        rg2.Value = map[group2][i]; // 個数
                        kk = kk + 1;
                    }
                    //outsheet.Range["B" + (i + 2).ToString("D")].Value = counter[i]; // 個数
                }
                outsheet.Columns.AutoFit(); // カラムの幅を自動的に見えるようにする 
                /**
                 * 処理が終わったので閉じる
                 */
            }
            catch (Exception ex)
            {
                MessageBox.Show("エラーが発生しました。" + ex.Message + " " + ex.StackTrace);
            }
        }
        /**
         * 期間検索のtableから、在庫の計算を行う。
         **/
        private void OnInventoryCalc(Office.IRibbonControl control,string uiStr)
        {
            int interval = 60; // 秒で計算
            int startLine = 3;
            try
            {
                interval = GetInterval(uiStr, interval);
                //Workbook book = Globals.ThisAddIn.Application.ActiveWorkbook;
                Worksheet sheet = Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet;

                /**
                 * テーブルを探す。テーブル判断条件は、 [i][0] != null && [i][1] =~ /^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{1,2}:[0-9]{2}:[0-9]{2}/ 
                 **/
                string[] colnames = new string[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                string[] targetColumns = new string[2] { "A", "B" };
                int i = 0;
                foreach (string a in colnames)
                {
                    // Columnが非表示かどうかチェックする。非表示の場合はその項目からskipする。
                    if (sheet.Range[a + "1"].EntireColumn.Hidden)
                    {
                        continue;
                    }
                    string name = sheet.Range[a + "2"].Text;
                    if (!string.IsNullOrEmpty(name))
                    {
                        bool flag = true;
                        bool timeflag = false;
                        for (int k = startLine; k < startLine + 100; k++) // 98行くらいまで、入っているデータがnullまたは、日時フォーマットかどうか調べる。
                        {
                            string obj = sheet.Range[a + k.ToString("D")].Text;
                            if (string.IsNullOrEmpty(obj))
                            {
                                continue;
                            }
                            if (isDateTime(obj))
                            {
                                timeflag = true;
                            }
                            else
                            {
                                // 違うデータが見つかった
                                flag = false;
                                break;
                            }
                        }
                        if ((flag == false) || (timeflag == false))
                        {
                            continue;
                        }
                        if (timeflag)
                        {
                            targetColumns[i] = a;
                            i++;
                            if (i == 2)
                            {
                                break;
                            }
                        }
                    }
                }
                if (i != 2)
                {
                    // カラムが足りない。
                    MessageBox.Show("対象となるカラムが2つ以上見つかりません。省略されず日時が表示されるように幅を広げてください。");
                    return;
                }
                /**
                 * targetColumnsに、時刻2つの列がある。
                 **/
                /*
                 * 大小の確認。targetColumn[0] < targetColumn[1] となるようにしたい。
                 */

                int revcount = 0;
                for (i = startLine; i < 100; i++)
                {
                    string acolname = targetColumns[0];
                    string bcolname = targetColumns[1];
                    string atimestr = sheet.Range[acolname + i.ToString("D")].Text;
                    string btimestr = sheet.Range[bcolname + i.ToString("D")].Text;

                    if (string.IsNullOrEmpty(atimestr) && string.IsNullOrEmpty(btimestr))
                    {
                        // データの終了
                    }
                    else if (isDateTime(atimestr) && isDateTime(btimestr))
                    {
                        DateTime atime = DateTime.Parse(atimestr);
                        DateTime btime = DateTime.Parse(btimestr);
                        if (atime <= btime)
                        {
                            /**
                             * OK
                             **/
                        }
                        else
                        {
                            /**
                             * 逆順にしたい
                             **/
                            revcount++;
                            if (revcount >= 3)
                            {
                                break;
                            }
                        }
                    }
                }
                if (revcount == 3)
                {
                    // Descend
                    string tmp = targetColumns[1];
                    targetColumns[1] = targetColumns[0];
                    targetColumns[0] = targetColumns[1];
                }
                else if (revcount == 0)
                {
                    // OK
                }
                else
                {
                    // UKNOWN

                }
                /**
                 * 10000行まで調べ時間の最大、最小を調べる。値が入っていないのは無視する。
                 **/
                DateTime maxDateTime = DateTime.MinValue;
                DateTime minDateTime = DateTime.MaxValue;
                int rowcount = 0;
                const int maxDataLine = 10001;
                for (i = startLine; i < maxDataLine + startLine; i++)
                {

                    string atimestr = sheet.Range[targetColumns[0] + i.ToString("D")].Text;
                    string btimestr = sheet.Range[targetColumns[1] + i.ToString("D")].Text;
                    if (string.IsNullOrEmpty(atimestr) && string.IsNullOrEmpty(btimestr))
                    {
                        // 同時にない場合はここでデータが終了と判断
                        break;
                    }
                    rowcount = rowcount + 1;
                    if (isDateTime(atimestr))
                    {
                        DateTime atime = DateTime.Parse(atimestr);
                        if (atime >= maxDateTime)
                        {
                            maxDateTime = atime;
                        }
                        if (atime < minDateTime)
                        {
                            minDateTime = atime;
                        }
                    }
                    if (isDateTime(btimestr))
                    {
                        DateTime btime = DateTime.Parse(btimestr);
                        if (btime >= maxDateTime)
                        {
                            maxDateTime = btime;
                        }
                        if (btime < minDateTime)
                        {
                            minDateTime = btime;
                        }
                    }
                }

                string WarningMessage = "";
                if (rowcount >= maxDataLine)
                {
                    WarningMessage = "\n警告：データ数が" + maxDataLine + "以上あります。これを超える分は計算されません";
                }
                string currentSheetName = sheet.Name;
                string newSheetName = "滞留数-" + currentSheetName;

                newSheetName = getNewSheetNameForInventoryCalc(newSheetName); // 1000個まで探す。

                DialogResult result = MessageBox.Show("時間間隔:" + interval + "秒\n対象カラム:" + targetColumns[0] + " " + targetColumns[1] + "\nデータ数: " + rowcount.ToString("D") + "\n時刻順序:" + this.order + "\nシート名:" + newSheetName + WarningMessage, "期間検索",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1
                    );
                if (result == DialogResult.Cancel)
                {
                    return;
                }
                /**
                 * maxDateTime,minDateTimeの時間でinterval間隔での配列を作る
                 * maxDateTime - minDateTime
                 */
                TimeSpan allInterval = maxDateTime - minDateTime;
                long arraySize = (long)(allInterval.TotalSeconds / interval) + 1;
                if (arraySize > 1000000)
                {
                    result = MessageBox.Show("滞留間隔が短い、または間隔が長いため計算できません", "エラー",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    if (result == DialogResult.OK)
                    {
                        return;
                    }
                }
                int[] counter = new int[arraySize];
                DateTime[] ctime = new DateTime[arraySize];
                for (i = 0; i < arraySize; i++)
                {
                    ctime[i] = maxDateTime - TimeSpan.FromSeconds(interval * i);
                    counter[i] = 0;
                }
                /**
                 * rowcount 行まで調べる。しかし、targetColumn[0],targetColumn[1] が同時にnullの場合にそこでとめる
                 **/
                //MessageBox.Show("8");
                for (i = startLine; i < rowcount + startLine; i++)
                {
                    string atimestr = sheet.Range[targetColumns[0] + i.ToString("D")].Text;
                    string btimestr = sheet.Range[targetColumns[1] + i.ToString("D")].Text;
                    if (string.IsNullOrEmpty(atimestr) && string.IsNullOrEmpty(btimestr))
                    {
                        // 同時にない場合はここでデータが終了と判断
                        break;
                    }
                    DateTime atime, btime;
                    if (string.IsNullOrEmpty(atimestr))
                    {
                        atime = DateTime.MinValue;
                    }
                    else
                    {
                        //atime = DateTime.Parse(atimestr);
                        atime = DateTime.FromOADate(sheet.Range[targetColumns[0] + i.ToString("D")].Value2);
                    }
                    if (string.IsNullOrEmpty(btimestr))
                    {
                        btime = DateTime.MaxValue;
                    }
                    else
                    {
                        //btime = DateTime.Parse(btimestr);
                        btime = DateTime.FromOADate(sheet.Range[targetColumns[1] + i.ToString("D")].Value2);
                    }
                    // 数の条件。
                    // A側の時刻に一致以上から、Bの時刻未満で計算。
                    // これより、A,B,C があったとしても、A<->Bの数と、B<->Cの数の合計は、A<->Cの数と一致するはず。
                    for (int j = 0; j < arraySize; j++)
                    {
                        if (atime.CompareTo(ctime[j]) <= 0 && ctime[j].CompareTo(btime) == -1)
                        {
                            counter[j] = counter[j] + 1;
                        }
                    }
                }
                //MessageBox.Show("9");
                /**
                 *  結果が、ctime,counterに arraySize分入っているので、これを別シートに作る
                 **/
                Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.Add();
                Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet.Name = newSheetName;
                Worksheet outsheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets[newSheetName];

                outsheet.Range["A1"].Value = "時刻";
                outsheet.Range["B1"].Value = "滞留数";
                for (i = 0; i < arraySize; i++)
                {
                    outsheet.Range["A" + (i + 2).ToString("D")].Value = ctime[i].ToString("yyyy/MM/dd HH:mm:ss"); // 文字列にconvert
                    outsheet.Range["B" + (i + 2).ToString("D")].Value = counter[i]; // 個数
                }
                outsheet.Columns.AutoFit(); // カラムの幅を自動的に見えるようにする 
                /**
                 * 処理が終わったので閉じる
                 */
            }
            catch (Exception ex)
            {
                MessageBox.Show("エラーが発生しました。" + ex.Message);
            }
        }
        // 2018/10/13 iwai end




        #region ヘルパー

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
