﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AisTemplateAddin
{
    public partial class OnInventryRemoveDuplicatesForm : Form
    {
        public OnInventryRemoveDuplicatesForm()
        {
            InitializeComponent();
            this.onInventryRemoveDuplicates1.textValue.Content = "処理中です。しばらく、お待ちください。";
        }
    }
}
