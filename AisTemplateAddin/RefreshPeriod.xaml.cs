﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;
using System.ComponentModel;
using FoaCore.Common.Util;
using Microsoft.Office.Interop.Excel;

namespace AisTemplateAddin
{
    /// <summary>
    /// RefreshPeriod.xaml の相互作用ロジック
    /// </summary>
    public partial class RefreshPeriod : UserControl
    {
        internal RefreshPeriodForm OwnerForm { get; set; }

        private const string PARAM_PERIOD_SHEET_NAME = "Period Sheet";
        private const string PARAM_PERIOD_START_DATE_CELL = "Period Start Date Cell";
        private const string PARAM_PERIOD_START_TIME_CELL = "Period Start Time Cell";
        private const string PARAM_PERIOD_END_DATE_CELL = "Period End Date Cell";
        private const string PARAM_PERIOD_END_TIME_CELL = "Period End Time Cell";
        private const string PARAM_PERIOD_START_DATETIME_CELL = "Period Start Date Time Cell";
        private const string PARAM_PERIOD_END_DATETIME_CELL = "Period End Date Time Cell";
        private const string PARAM_Chimney = "ChimneyTab";
        private const string PARAM_FIFO = "FIFOTab";
        private const string PARAM_StockTime = "StockTimeTab";
        private const string PARAM_Online = "OnlineTab";
        //Refresh_Period Sunyi 2018/07/05 Start
        //仕様変更、期間再設定に時間設定を実装する
        private const string PARAM_Stream = "StreamTab";
        //Refresh_Period Sunyi 2018/07/05 End

        public RefreshPeriod(bool IsNewPeriod)
        {
            InitializeComponent();
            //ISSUE_NO.810 sunyi 2018/08/02 Start
            //仕様変更、ボタン作成機能を追加する
            if (IsNewPeriod)
            {
                this.Period_Button.IsEnabled = true;
            }
            else
            {
                this.Period_Button.IsEnabled = false;
                this.Period_Button.Foreground = new SolidColorBrush(Colors.Gray);
            }
            //ISSUE_NO.810 sunyi 2018/08/02 End
            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }
        }

        public void InitializeWholeDay()
        {
            if (this.dateTimePicker_End.Value.HasValue)
            {
                this.dateTimePicker_End.Value = this.dateTimePicker_Start.Value.Value.AddMilliseconds(24*60*60*1000 - 1);
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {

        }

        private void Validation_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        private bool ValidatePeriodByTemplate(string addinTabId, DateTimePicker end, bool onlineFlg)
        {
            if (onlineFlg && end.Value < DateTime.Now)
            {
                if (addinTabId.Equals(PARAM_Chimney) || addinTabId.Equals(PARAM_FIFO))
                {
                    FoaMessageBox.ShowError("AIS_E_002");
                    return false;
                }

                if (addinTabId.Equals(PARAM_StockTime) || addinTabId.Equals(PARAM_Online))
                {
                    FoaMessageBox.ShowError("AIS_E_024");
                    return false;
                }
            }

            return true;
        }

        private void OnUpdateClick(object sender, RoutedEventArgs e)
        {
            DateTime startData;
            DateTime endData;

            if (GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out startData, out endData))
            {
                if (ValidatePeriodByTemplate(Globals.ThisAddIn.addinParams.AddInTab, this.dateTimePicker_End, Globals.ThisAddIn.addinParams.IsOnline()))
                {
                    try
                    {
                        string sheetName = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_SHEET_NAME);
                        string startDateCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_DATE_CELL);
                        string startTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_TIME_CELL);
                        string endDateCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_DATE_CELL);
                        string endTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_TIME_CELL);
                        string startDateTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_DATETIME_CELL);
                        string endDateTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_DATETIME_CELL);

                        Worksheet sheet = Globals.ThisAddIn.Application.ActiveWorkbook.Sheets[sheetName];

                        if (!string.IsNullOrEmpty(startDateCell))
                        {
                            sheet.Range[startDateCell].Value = startData.ToString("yyyy/MM/dd");
                        }
                        if (!string.IsNullOrEmpty(startTimeCell))
                        {
                            sheet.Range[startTimeCell].Value = startData.ToString("HH:mm:ss");
                        }
                        if (!string.IsNullOrEmpty(endDateCell))
                        {
                            sheet.Range[endDateCell].Value = endData.ToString("yyyy/MM/dd");
                        }
                        if (!string.IsNullOrEmpty(endTimeCell))
                        {
                            sheet.Range[endTimeCell].Value = endData.ToString("HH:mm:ss");
                        }
                        if (!string.IsNullOrEmpty(startDateTimeCell))
                        {
                            sheet.Range[startDateTimeCell].Value = startData.ToString("yyyy/MM/dd HH:mm:ss");
                        }
                        if (!string.IsNullOrEmpty(endDateTimeCell))
                        {
                            sheet.Range[endDateTimeCell].Value = endData.ToString("yyyy/MM/dd HH:mm:ss");
                        }

                        OwnerForm.DialogResult = System.Windows.Forms.DialogResult.OK;
                        OwnerForm.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        private bool IsDac()
        {
            return false;
        }

        private bool GetStartAndEndTime(DateTimePicker pickerStart, DateTimePicker pickerEnd, out DateTime start1, out DateTime end1)
        {
            start1 = DateTime.MinValue;
            end1 = DateTime.Now;

            DateTime? start = pickerStart.Value;
            DateTime? end = pickerEnd.Value;

            if (start == null || end == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return false;
            }

            if (end <= start && !IsDac())
            {
                FoaMessageBox.ShowError("AIS_E_019");
                return false;
            }

            start1 = start ?? DateTime.MinValue;
            end1 = end ?? DateTime.Now;

            start1 = start1.AddMilliseconds(-start1.Millisecond);
            end1 = end1.AddMilliseconds(999 - end1.Millisecond);

            return true;
        }
        //Refresh_Period Sunyi 2018/07/05 Start
        //仕様変更、期間再設定に時間設定を実装する
        private void OnGetTimeClick(object sender, RoutedEventArgs e)
        {
            string sheetName = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_SHEET_NAME);
            string startDateCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_DATE_CELL);
            string startTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_TIME_CELL);
            string endDateCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_DATE_CELL);
            string endTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_TIME_CELL);
            string startDateTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_DATETIME_CELL);
            string endDateTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_DATETIME_CELL);

            Worksheet sheet = Globals.ThisAddIn.Application.ActiveWorkbook.Sheets[sheetName];

            if (Globals.ThisAddIn.addinParams.AddInTab.Equals(PARAM_Stream))
            {
                if (!string.IsNullOrEmpty(startTimeCell))
                {
                    string start = (sheet.Range[startDateCell].Value).ToString("yyyy/MM/dd") + " " + (DateTime.FromOADate(sheet.Range[startTimeCell].Value)).ToString("HH:mm:ss");
                    this.dateTimePicker_Start.Value = DateTime.Parse(start);
                }
                if (!string.IsNullOrEmpty(endTimeCell))
                {
                    string end = (sheet.Range[endDateCell].Value).ToString("yyyy/MM/dd") + " " + (DateTime.FromOADate(sheet.Range[endTimeCell].Value)).ToString("HH:mm:ss");
                    this.dateTimePicker_End.Value = DateTime.Parse(end);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(startDateTimeCell))
                {
                    this.dateTimePicker_Start.Value = sheet.Range[startDateTimeCell].Value ?? DateTime.Now;
                }
                if (!string.IsNullOrEmpty(endDateTimeCell))
                {
                    this.dateTimePicker_End.Value = sheet.Range[endDateTimeCell].Value ?? DateTime.Now;
                }
            }
        }
        //Refresh_Period Sunyi 2018/07/05 End

        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        private void GreateButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                OwnerForm.IsNew_Period = true;
                OwnerForm.DialogResult = System.Windows.Forms.DialogResult.OK;
                OwnerForm.Close();
            }
            catch (Exception ex)
            {
            }
        }
        //ISSUE_NO.810 sunyi 2018/08/02 End
    }
}
