﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;

namespace AisTemplateAddin
{
    abstract class ResultSheetGenerator
    {
        protected const int COLUMN_WIDTH = 16;

        public static ResultSheetGenerator CreateGenerator(Office.IRibbonControl control)
        {
            if (control.Id.Equals("QuickGraphTab_MergeData"))
            {
                return new QuickGraphResultSheetGenerator() as ResultSheetGenerator;
            }
            else if (control.Id.Equals("MultiMonitorTab_MergeData"))
            {
                return new StatusMonitorResultSheetGenerator() as ResultSheetGenerator;
            }
            else {
                return null;
            }
        }

        protected object[,] GetRemainValues(object[,] values, long rows)
        {
            long cols = values.GetLongLength(1);

            object[,] ret = new object[rows, cols];

            for (long tr = 0; tr < rows; tr++)
            {
                for (long tc = 0; tc < cols; tc++)
                {
                    ret[tr, tc] = values[tr, tc];
                }
            }

            return ret;
        }

        protected long SetRangeValue(Range cells, int startRow, int strartCol, object[,] values)
        {
            object[,] remainValues;
            long rows = values.GetLongLength(0);
            long cols = values.GetLongLength(1);

            if (startRow > cells.Rows.Count)
            {
                return 0;
            }
            else
            {
                if ((startRow + rows) > cells.Rows.Count)
                {
                    rows = cells.Rows.Count - startRow + 1;
                    remainValues = GetRemainValues(values, rows);
                }
                else
                {
                    remainValues = values;
                }
            }

            Range range = cells[startRow, strartCol];
            range = range.get_Resize(rows, cols);

            range.set_Value(XlRangeValueDataType.xlRangeValueDefault, remainValues);
            return rows;
        }

        protected System.Collections.Generic.IEnumerable<object[]> SetMinRTRowValues(Workbook workbook, MergeInfo mergeInfo, List<string> csvSheets, int[] srcRow, object[] tmpvalues)
        {
            int yieldSheetIndex = -1;

            do
            {
                DateTime rt = DateTime.MinValue;
                yieldSheetIndex = -1;

                for (int i = 0; i < csvSheets.Count; i++)
                {
                    string sheetName = csvSheets[i];
                    int row = srcRow[i];
                    if (row > mergeInfo.dataRows[sheetName])
                    {
                        continue;
                    }

                    Worksheet srcSheet = workbook.Worksheets[sheetName];
                    string rtString = srcSheet.Cells[row, 1].Value == null ? string.Empty : srcSheet.Cells[row, 1].Value.ToString();
                    if (!string.IsNullOrEmpty(rtString))
                    {
                        DateTime tmp = DateTime.ParseExact(rtString, "yyyy/M/d H:m:s", null);
                        if (rt <= tmp)
                        {
                            rt = tmp;
                            yieldSheetIndex = i;
                        }
                    }
                }

                if (yieldSheetIndex > -1)
                {
                    string sheetName = csvSheets[yieldSheetIndex];
                    Worksheet srcSheet = workbook.Worksheets[sheetName];
                    for (int i = 0; i < tmpvalues.Length; i++)
                    {
                        tmpvalues[i] = null;
                    }
                    tmpvalues[0] = mergeInfo.ctmNames[sheetName];
                    for (int i = 0; i < mergeInfo.titleIndex[sheetName].Count; i++)
                    {
                        tmpvalues[mergeInfo.titleIndex[sheetName][i]] = srcSheet.Cells[srcRow[yieldSheetIndex], i + 1].Value;
                    }
                    srcRow[yieldSheetIndex]++;
                    yield return tmpvalues;
                }
            } while (yieldSheetIndex > -1);
        }

        protected void SetColumnWidth(Worksheet sheet)
        {
            sheet.UsedRange.ColumnWidth = 16;
        }

        abstract public void MergeData();
    }
}
