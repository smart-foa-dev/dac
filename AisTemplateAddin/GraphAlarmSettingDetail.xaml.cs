﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;
using System.ComponentModel;
using FoaCore.Common.Util;
using Microsoft.Office.Interop.Excel;
using System.Collections;

namespace AisTemplateAddin
{
    /// <summary>
    /// RefreshPeriod.xaml の相互作用ロジック
    /// </summary>
    public partial class GraphAlarmSettingDetail : UserControl
    {
        internal GraphAlarmSettingDetailForm OwnerDetailForm { get; set; }
        
        private string GraphParamName;

        public GraphAlarmSettingDetail(string paramName)
        {
            InitializeComponent();
            GraphParamName = paramName;
            Worksheet sheet = Globals.ThisAddIn.addinParams.GetGraphAlarmParamSheet(Globals.ThisAddIn.addinParams.PARAM_GRAPH_SHEET_NAME);
            GetGraphAlarmParam(sheet, GraphParamName);
        }

        private void Validation_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
        }

        private void Setting_Button_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(this.AlarmLineName.Text))
            {
                FoaMessageBox.ShowWarning("AIS_I_025");
                return;
            }
            if (this.DownCross.IsChecked == false && this.UpCross.IsChecked == false)
            {
                FoaMessageBox.ShowWarning("AIS_I_026");
                return;
            }

            Worksheet sheet = Globals.ThisAddIn.addinParams.GetGraphAlarmParamSheet(Globals.ThisAddIn.addinParams.PARAM_GRAPH_SHEET_NAME);
            if (sheet != null)
            {
                //write param column
                int ALnum = int.Parse(GraphParamName.Replace("AL", ""));

                SetGraphAlarmParam(sheet, ALnum);
            }
            ////修正
            //if (sheetName == ParamGraphName)
            //{
            //    break;
            //}
        
            OwnerDetailForm.Close();
        }

        private void SetGraphAlarmParam(Worksheet worksheetCondition, int ALnum)
        {
            DateTime startData;
            DateTime endData;

            Range cellsCondition = worksheetCondition.Cells;

            for (int i = 1; i <= 20; i++)
            {
                //アラーム_NO
                if (cellsCondition[i, 1].Value == "アラーム_NO")
                {
                    cellsCondition[i, ALnum+1].Value = "AL" + (ALnum).ToString();
                    continue;
                }

                // アラームライン名称
                if (cellsCondition[i, 1].Value == "アラームライン名称")
                {
                    cellsCondition[i, ALnum + 1].Value = this.AlarmLineName.Text;
                    continue;
                }

                // 監視期間_START
                if (cellsCondition[i, 1].Value == "監視期間_START")
                {
                    if (GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out startData, out endData))
                    {
                        cellsCondition[i, ALnum + 1].Value = startData.ToString("yyyy/MM/dd HH:mm:ss");
                    }
                    //if (start == DateTime.MinValue)
                    //{
                    //    throw new InvalidDataException("start is not specified.");
                    //}
                    continue;
                }

                // 監視期間_END
                if (cellsCondition[i, 1].Value == "監視期間_END")
                {
                    //if (start == DateTime.MinValue)
                    //{
                    //    throw new InvalidDataException("start is not specified.");
                    //}
                    if (GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out startData, out endData))
                    {
                        cellsCondition[i, ALnum + 1].Value = endData.ToString("yyyy/MM/dd HH:mm:ss");
                    }
                    continue;
                }

                // ラインタイプ_UL
                if (cellsCondition[i, 1].Value == "ラインタイプ_UL")
                {
                    cellsCondition[i, ALnum + 1].Value = this.LineType_UL.IsChecked.ToString();
                    continue;
                }

                // ラインタイプ_LL
                if (cellsCondition[i, 1].Value == "ラインタイプ_LL")
                {
                    cellsCondition[i, ALnum + 1].Value = this.LineType_LL.IsChecked.ToString();
                    continue;
                }

                // 開始値
                if (cellsCondition[i, 1].Value == "開始値")
                {
                    int int_i = 0;

                    if(int.TryParse(this.StartValue.Text, out int_i))
                    {
                        cellsCondition[i, ALnum + 1].Value = this.StartValue.Text;
                        continue;
                    }
                    else
                    {
                        FoaMessageBox.ShowWarning("AIS_I_027");
                    }
                }
                // 終了値
                if (cellsCondition[i, 1].Value == "終了値")
                {
                    int int_i = 0;

                    if (int.TryParse(this.EndValue.Text, out int_i))
                    {
                        cellsCondition[i, ALnum + 1].Value = this.EndValue.Text;
                        continue;
                    }
                    else
                    {
                        FoaMessageBox.ShowWarning("AIS_I_027");
                    }
                }
                // UPクロス
                if (cellsCondition[i, 1].Value == "UPクロス")
                {
                    cellsCondition[i, ALnum + 1].Value = this.UpCross.IsChecked.ToString();
                    continue;
                }

                // DOWNクロス
                if (cellsCondition[i, 1].Value == "DOWNクロス")
                {
                    cellsCondition[i, ALnum + 1].Value = this.DownCross.IsChecked.ToString();
                    continue;
                }

                //// タイム条件付き
                //if (cellsCondition[i, 1].Value == "タイム条件付き")
                //{
                //    cellsCondition[i, ALnum + 1].Value = this.TimeConditions.IsChecked.ToString();
                //    continue;
                //}
                // Sec.
                if (cellsCondition[i, 1].Value == "Sec.")
                {
                    if (this.DownCross.IsChecked == true || this.UpCross.IsChecked == true)
                    {
                        int int_i = 0;

                        if (int.TryParse(this.TimeValue.Text, out int_i))
                        {
                            cellsCondition[i, ALnum + 1].Value = this.TimeValue.Text;
                            continue;
                        }
                        else
                        {
                            FoaMessageBox.ShowWarning("AIS_I_027");
                        }
                    }
                    continue;
                }
                // 出力TEXT_FLAG
                if (cellsCondition[i, 1].Value == "出力TEXT_FLAG")
                {
                    cellsCondition[i, ALnum + 1].Value = this.OutputText.IsChecked.ToString();
                    continue;
                }
                // グラフ名称_FLAG
                if (cellsCondition[i, 1].Value == "グラフ名称_FLAG")
                {
                    cellsCondition[i, ALnum + 1].Value = this.GraphName_Flag.IsChecked.ToString();
                    continue;
                }
                // アラームライン名称_FLAG
                if (cellsCondition[i, 1].Value == "アラームライン名称_FLAG")
                {
                    cellsCondition[i, ALnum + 1].Value = this.AlarmLineName_Flag.IsChecked.ToString();
                    continue;
                }
                // アラームタイプ_FLAG
                if (cellsCondition[i, 1].Value == "アラームタイプ_FLAG")
                {
                    cellsCondition[i, ALnum + 1].Value = this.AlarmType_Flag.IsChecked.ToString(); ;
                    continue;
                }
                // 発生時刻_FLAG
                if (cellsCondition[i, 1].Value == "発生時刻_FLAG")
                {
                    cellsCondition[i, ALnum + 1].Value = this.DateTime_Flag.IsChecked.ToString(); ;
                    continue;
                }
                // グラフバルキー_FLAG"
                if (cellsCondition[i, 1].Value == "グラフバルキー_FLAG")
                {
                    cellsCondition[i, ALnum + 1].Value = this.GraphBulky_Flag.IsChecked.ToString(); ;
                    continue;
                }
                // 関連CTM_FALG
                if (cellsCondition[i, 1].Value == "関連CTM_FALG")
                {
                    cellsCondition[i, ALnum + 1].Value = this.CTM_Name_Flag.IsChecked.ToString(); ;
                    continue;
                }
                // 斜率
                if (cellsCondition[i, 1].Value == "斜率")
                {
                    decimal indexValue = 0;
                    decimal value = 0;
                    decimal ObliqueRate = 0;

                    if (this.StartValue.Text.Equals(this.EndValue.Text))
                    {
                        cellsCondition[i, ALnum + 1].Value = 0;
                    }
                    else
                    {
                        if (GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out startData, out endData))
                        {
                            indexValue = UnixTime.ToLong(endData) - UnixTime.ToLong(startData);
                            value = decimal.Parse(this.EndValue.Text) - decimal.Parse(this.StartValue.Text);
                            ObliqueRate = value / indexValue;
                        }
                        cellsCondition[i, ALnum + 1].Value = ObliqueRate;
                    }
                    continue;
                }
            }
        }

        private void GetGraphAlarmParam(Worksheet worksheetCondition, string GraphParamName)
        {
            Range cellsCondition = worksheetCondition.Cells;

            for (int j = 1; j <= Globals.ThisAddIn.addinParams.MaxNum; j++)
            {
                if (GraphParamName.Equals(cellsCondition[1, j + 1].Value))
                {
                    for (int i = 1; i <= 20; i++)
                    {
                        if (cellsCondition[i, j + 1].Value == null)
                        {
                            continue;
                        }
                        // アラームライン名称
                        if (cellsCondition[i, 1].Value == "アラームライン名称")
                        {
                            this.AlarmLineName.Text = cellsCondition[i, j + 1].Value;
                            continue;
                        }

                        // 監視期間_START
                        if (cellsCondition[i, 1].Value == "監視期間_START")
                        {
                            this.dateTimePicker_Start.Value = (cellsCondition[i, j + 1].Value) ?? DateTime.Now;
                            continue;
                        }

                        // 監視期間_END
                        if (cellsCondition[i, 1].Value == "監視期間_END")
                        {
                            this.dateTimePicker_End.Value = (cellsCondition[i, j + 1].Value) ?? DateTime.Now;
                            continue;
                        }

                        // ラインタイプ_UL
                        if (cellsCondition[i, 1].Value == "ラインタイプ_UL")
                        {
                            this.LineType_UL.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }

                        // ラインタイプ_LL
                        if (cellsCondition[i, 1].Value == "ラインタイプ_LL")
                        {
                            this.LineType_LL.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }

                        // 開始値
                        if (cellsCondition[i, 1].Value == "開始値")
                        {
                            this.StartValue.Text = cellsCondition[i, j + 1].Value.ToString();
                            continue;
                        }
                        // 終了値
                        if (cellsCondition[i, 1].Value == "終了値")
                        {
                            this.EndValue.Text = cellsCondition[i, j + 1].Value.ToString();
                            continue;
                        }
                        // UPクロス
                        if (cellsCondition[i, 1].Value == "UPクロス")
                        {
                            this.UpCross.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }

                        // DOWNクロス
                        if (cellsCondition[i, 1].Value == "DOWNクロス")
                        {
                            this.DownCross.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }

                        //// タイム条件付き
                        //if (cellsCondition[i, 1].Value == "タイム条件付き")
                        //{
                        //    this.TimeConditions.IsChecked = cellsCondition[i, j + 1].Value;
                        //    continue;
                        //}
                        // Sec.
                        if (cellsCondition[i, 1].Value == "Sec.")
                        {
                            this.TimeValue.Text = cellsCondition[i, j + 1].Value.ToString();
                            continue;
                        }
                        // 出力TEXT_FLAG
                        if (cellsCondition[i, 1].Value == "出力TEXT_FLAG")
                        {
                            this.OutputText.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }
                        // グラフ名称_FLAG
                        if (cellsCondition[i, 1].Value == "グラフ名称_FLAG")
                        {
                            this.GraphName_Flag.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }
                        // アラームライン名称_FLAG
                        if (cellsCondition[i, 1].Value == "アラームライン名称_FLAG")
                        {
                            this.AlarmLineName_Flag.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }
                        // アラームタイプ_FLAG
                        if (cellsCondition[i, 1].Value == "アラームタイプ_FLAG")
                        {
                            this.AlarmType_Flag.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }
                        // 発生時刻_FLAG
                        if (cellsCondition[i, 1].Value == "発生時刻_FLAG")
                        {
                            this.DateTime_Flag.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }
                        // グラフバルキー_FLAG"
                        if (cellsCondition[i, 1].Value == "グラフバルキー_FLAG")
                        {
                            this.GraphBulky_Flag.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }
                        // 関連CTM_FALG
                        if (cellsCondition[i, 1].Value == "関連CTM_FALG")
                        {
                            this.CTM_Name_Flag.IsChecked = cellsCondition[i, j + 1].Value;
                            continue;
                        }
                    }
                    break;
                }
            }
        }

        private bool GetStartAndEndTime(DateTimePicker pickerStart, DateTimePicker pickerEnd, out DateTime start1, out DateTime end1)
        {
            start1 = DateTime.MinValue;
            end1 = DateTime.Now;

            DateTime? start = pickerStart.Value;
            DateTime? end = pickerEnd.Value;

            if (start == null || end == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return false;
            }

            if (end <= start)
            {
                FoaMessageBox.ShowError("AIS_E_019");
                return false;
            }

            start1 = start ?? DateTime.MinValue;
            end1 = end ?? DateTime.Now;

            //start1 = start1.AddMilliseconds(-start1.Millisecond);
            //end1 = end1.AddMilliseconds(999 - end1.Millisecond);

            return true;
        }
    }
}
