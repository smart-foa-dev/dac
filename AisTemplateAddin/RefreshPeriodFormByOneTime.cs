﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AisTemplateAddin
{
    public partial class RefreshPeriodFormByOneTime : Form
    {
        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        public bool IsNew_Period = false;
        //public RefreshPeriodFormByOneTime()
        public RefreshPeriodFormByOneTime(bool IsNewPeriod)
        {
            //InitializeComponent();
            InitializeComponent(IsNewPeriod);
            //ISSUE_NO.810 sunyi 2018/08/02 Start
            //仕様変更、ボタン作成機能を追加する
            this.RefreshPeriodByOneTime.OwnerForm = this;
        }
    }
}
