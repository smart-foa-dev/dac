﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;
using System.ComponentModel;
using FoaCore.Common.Util;
using Microsoft.Office.Interop.Excel;
using System.Collections;

namespace AisTemplateAddin
{
    /// <summary>
    /// RefreshPeriod.xaml の相互作用ロジック
    /// </summary>
    public partial class GraphAlarmSetting : UserControl
    {
        internal GraphAlarmSettingForm OwnerForm { get; set; }
        internal GraphAlarmSettingDetailForm OwnerDetailForm { get; set; }

        private const string PARAM_PERIOD_SHEET_NAME = "Period Sheet";
        private const string PARAM_PERIOD_START_DATE_CELL = "Period Start Date Cell";
        private const string PARAM_PERIOD_START_TIME_CELL = "Period Start Time Cell";
        private const string PARAM_PERIOD_END_DATE_CELL = "Period End Date Cell";
        private const string PARAM_PERIOD_END_TIME_CELL = "Period End Time Cell";
        private const string PARAM_PERIOD_START_DATETIME_CELL = "Period Start Date Time Cell";
        private const string PARAM_PERIOD_END_DATETIME_CELL = "Period End Date Time Cell";
        private const string PARAM_Chimney = "ChimneyTab";
        private const string PARAM_FIFO = "FIFOTab";
        private const string PARAM_StockTime = "StockTimeTab";
        private const string PARAM_Online = "OnlineTab";
        //Refresh_Period Sunyi 2018/07/05 Start
        //仕様変更、期間再設定に時間設定を実装する
        private const string PARAM_Stream = "StreamTab";
        //Refresh_Period Sunyi 2018/07/05 End

        private List<GraphAlarmList> items = new List<GraphAlarmList>();

        public GraphAlarmSetting()
        {
            InitializeComponent();

            try{
                Worksheet sheet = Globals.ThisAddIn.addinParams.GetGraphAlarmParamSheet(Globals.ThisAddIn.addinParams.PARAM_GRAPH_SHEET_NAME);
                GetGraphAlarmParam(sheet, items);
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {
            }
              
            this.AL_Lists.ItemsSource = items;
        }

        public void InitializeWholeDay()
        {

        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {

        }

        private void Validation_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {

        }

        private bool ValidatePeriodByTemplate(string addinTabId, DateTimePicker end, bool onlineFlg)
        {
            if (onlineFlg && end.Value < DateTime.Now)
            {
                if (addinTabId.Equals(PARAM_Chimney) || addinTabId.Equals(PARAM_FIFO))
                {
                    FoaMessageBox.ShowError("AIS_E_002");
                    return false;
                }

                if (addinTabId.Equals(PARAM_StockTime) || addinTabId.Equals(PARAM_Online))
                {
                    FoaMessageBox.ShowError("AIS_E_024");
                    return false;
                }
            }

            return true;
        }

        private WindowWrapper GetOwnerWrapper()
        {
            try
            {
                int wnd = Globals.ThisAddIn.Application.ActiveWindow.Hwnd;
                WindowWrapper wrapper = new WindowWrapper(wnd);
                return wrapper;
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }
            return null;
        }

        private void AL_Setting_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var GraphAlarmList = this.AL_Lists.SelectedItem as GraphAlarmList;
            if (GraphAlarmList == null) return;
            GraphAlarmSettingDetailForm form = new GraphAlarmSettingDetailForm(GraphAlarmList.No);
            form.ShowDialog(GetOwnerWrapper());

            items.Clear();
            Worksheet sheet = Globals.ThisAddIn.addinParams.GetGraphAlarmParamSheet(Globals.ThisAddIn.addinParams.PARAM_GRAPH_SHEET_NAME);
            GetGraphAlarmParam(sheet, items);
            this.AL_Lists.Items.Refresh();
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            Worksheet sheet = Globals.ThisAddIn.addinParams.GetGraphAlarmParamSheet(Globals.ThisAddIn.addinParams.PARAM_GRAPH_SHEET_NAME);
            SetGraphAlarmParam(sheet);
            this.AL_Lists.Items.Refresh();
        }

        private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            var GraphAlarmList = this.AL_Lists.SelectedItem as GraphAlarmList;
            items.Remove(GraphAlarmList);

            Worksheet sheet = Globals.ThisAddIn.addinParams.GetGraphAlarmParamSheet(Globals.ThisAddIn.addinParams.PARAM_GRAPH_SHEET_NAME);
            DeleteGraphAlarmParam(sheet, GraphAlarmList.No);
            this.AL_Lists.Items.Refresh();
        }

        private void ItemsControl_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {

        }

        private void GetGraphAlarmParam(Worksheet worksheetCondition, List<GraphAlarmList> items)
        {
            Range cellsCondition = worksheetCondition.Cells;

            for (int j = 1; j <= Globals.ThisAddIn.addinParams.MaxNum; j++)
            {
                if (cellsCondition[1, j + 1].Value != null)
                {
                    items.Add(new GraphAlarmList(cellsCondition[1, j + 1].Value, cellsCondition[2, j + 1].Value));
                }
            }
        }

        private void SetGraphAlarmParam(Worksheet worksheetCondition)
        {
            Range cellsCondition = worksheetCondition.Cells;

            for (int j = 1; j <= Globals.ThisAddIn.addinParams.MaxNum; j++)
            {
                if(string.IsNullOrEmpty(cellsCondition[1, j + 1].Value))
                {
                    string strAL = "AL" + j.ToString();
                    cellsCondition[1, j + 1].Value = strAL;
                    items.Add(new GraphAlarmList(strAL, ""));
                    break;
                }
            }
        }

        private void DeleteGraphAlarmParam(Worksheet worksheetCondition, string al)
        {
            Range cellsCondition = worksheetCondition.Cells;

            for (int j = 1; j <= Globals.ThisAddIn.addinParams.MaxNum; j++)
            {
                if (al.Equals(cellsCondition[1, j + 1].Value))
                {
                    for (int i = 1; i < 20;i++ )
                    {
                        cellsCondition[i, j + 1].Value = "";
                    }
                    break;
                }
            }
        }
    }
}
