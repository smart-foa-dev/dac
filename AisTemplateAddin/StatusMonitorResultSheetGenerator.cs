﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;

namespace AisTemplateAddin
{
    class StatusMonitorResultSheetGenerator : ResultSheetGenerator
    {
        private const string RESULT_SHEET_NAME = "RESULT";
        private const int MAX_BUFF_ROWS = 5000;

        public override void MergeData()
        {
            Worksheet result;
            List<string> csvSheets = Globals.ThisAddIn.addinParams.GetCtmDataSheets();
            Workbook workbook = Globals.ThisAddIn.Application.ActiveWorkbook;
            if (csvSheets.Count > 0)
            {
                try
                {
                    result = workbook.Sheets[RESULT_SHEET_NAME];
                    if (result != null)
                    {
                        result.Application.DisplayAlerts = false;
                        result.Delete();
                        result.Application.DisplayAlerts = true;
                    }
                }
#pragma warning disable
                catch (Exception e)
#pragma warning restore
                {

                }

                try
                {
                    result = workbook.Sheets.Add(After: workbook.Sheets[workbook.Sheets.Count]);
                    result.Name = RESULT_SHEET_NAME;

                    // merge titles
                    MergeInfo mergeInfo = MergeTitle(workbook, csvSheets);
                    object[,] values = new object[MAX_BUFF_ROWS, mergeInfo.titles.Count];
                    object[] tmpvalues = new object[mergeInfo.titles.Count];
                    for (int i = 0; i < mergeInfo.titles.Count; i++)
                    {
                        values[0, i] = mergeInfo.titles[i];
                    }

                    // initialize data start row
                    int[] srcRow = new int[csvSheets.Count];
                    for (int i = 0; i < csvSheets.Count; i++)
                    {
                        srcRow[i] = 2;
                    }

                    // merge data
                    int dstRow = 1;
                    int sheetRow = 1;
                    foreach (object[] rowValue in SetMinRTRowValues(workbook, mergeInfo, csvSheets, srcRow, tmpvalues))
                    {
                        for (int i = 0; i < mergeInfo.titles.Count; i++)
                        {
                            values[dstRow, i] = rowValue[i];
                        }
                        dstRow = (dstRow + 1) % MAX_BUFF_ROWS;

                        if ((dstRow % MAX_BUFF_ROWS) == 0)
                        {
                            SetRangeValue(result.Cells, sheetRow, 1, values);
                            sheetRow += MAX_BUFF_ROWS;
                        }
                    }

                    if (dstRow > 0)
                    {
                        object[,] remainValues = GetRemainValues(values, dstRow);
                        SetRangeValue(result.Cells, sheetRow, 1, remainValues);
                    }

                    SetColumnWidth(result);
                }
#pragma warning disable
                catch (Exception e)
#pragma warning restore
                {

                }
            }
        }

        private MergeInfo MergeTitle(Workbook workbook, List<string> csvSheets)
        {
            MergeInfo info = new MergeInfo();
            info.titleIndex = new Dictionary<string, List<int>>();
            info.titles = new List<string>();
            info.titles.Add(Properties.Resources.QUICKGRAPH_BULK_TITLE_CTM);
            info.dataRows = new Dictionary<string, int>();
            info.ctmNames = new Dictionary<string, string>();

            Dictionary<string, int> titles = new Dictionary<string, int>();
            titles.Add(Properties.Resources.QUICKGRAPH_BULK_TITLE_CTM, 0);

            foreach (string csvSheet in csvSheets)
            {
                Worksheet sheet = workbook.Sheets[csvSheet];
                List<int> index = new List<int>();

                for (int i = 1; ; i++)
                {
                    string title = sheet.Cells[1, i].Value;

                    if (string.IsNullOrEmpty(title))
                    {
                        break;
                    }

                    if (!titles.ContainsKey(title))
                    {
                        int j = titles.Count;
                        titles.Add(title, j);
                        info.titles.Add(title);
                        index.Add(j);
                    }
                    else
                    {
                        index.Add(titles[title]);
                    }
                }
                info.titleIndex.Add(csvSheet, index);
                info.dataRows.Add(csvSheet, sheet.UsedRange.Rows.Count);
                info.ctmNames.Add(csvSheet, csvSheet);
            }

            return info;
        }
    }
}
