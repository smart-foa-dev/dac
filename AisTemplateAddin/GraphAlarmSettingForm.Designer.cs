﻿namespace AisTemplateAddin
{
    partial class GraphAlarmSettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.graphAlarmSettingControl = new System.Windows.Forms.Integration.ElementHost();
            this.GraphAlarmSetting = new AisTemplateAddin.GraphAlarmSetting();
            this.SuspendLayout();
            // 
            // refreshPeriodControl
            // 
            this.graphAlarmSettingControl.Location = new System.Drawing.Point(0, 0);
            this.graphAlarmSettingControl.Name = "graphAlarmSettingControl";
            this.graphAlarmSettingControl.Size = new System.Drawing.Size(350, 250);
            this.graphAlarmSettingControl.TabIndex = 0;
            this.graphAlarmSettingControl.Child = this.GraphAlarmSetting;
            // 
            // RefreshPeriodForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 250);
            this.Controls.Add(this.graphAlarmSettingControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GraphAlarmSettingForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "グラフアラーム設定";
            this.Load += new System.EventHandler(this.GraphAlarmSettingForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost graphAlarmSettingControl;
        private GraphAlarmSetting GraphAlarmSetting;
    }
}