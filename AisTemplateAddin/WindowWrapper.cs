﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AisTemplateAddin
{
    class WindowWrapper : System.Windows.Forms.IWin32Window
    {
        public WindowWrapper(int handle)
        {
            this.handle = new IntPtr(handle);
        }
 
        public IntPtr Handle
        {
            get { return this.handle; }
        }
 
        private IntPtr handle;
    }
}
