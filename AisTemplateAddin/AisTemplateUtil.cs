﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data;

namespace AisTemplateAddin
{
    [ComVisible(true)]
    public interface IAisTemplateAddinUtils
    {
        void Invalidate();
        bool RefreshPeriod();
        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        //インタフェース定義
        bool RefreshPeriod_Common();
        bool RefreshPeriod_ByOneTime();
        //ISSUE_NO.810 sunyi 2018/08/02 End
    }

    [ComVisible(true)]
    [ClassInterface(ClassInterfaceType.None)]
    public class AisTemplateAddinUtils : IAisTemplateAddinUtils
    {
        // This method tries to write a string to cell A1 in the active worksheet.
        public void Invalidate()
        {
            Globals.ThisAddIn.addinParams = new AddInParams(Globals.ThisAddIn.Application.ActiveWorkbook);
            Globals.ThisAddIn.aisRibbon.ribbon.Invalidate();
        }

        public bool RefreshPeriod()
        {
            Globals.ThisAddIn.addinParams = new AddInParams(Globals.ThisAddIn.Application.ActiveWorkbook);
            return Globals.ThisAddIn.aisRibbon.OnRefreshPeriodCOM();
        }
        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        public bool RefreshPeriod_Common()
        {
            Globals.ThisAddIn.addinParams = new AddInParams(Globals.ThisAddIn.Application.ActiveWorkbook);
            return Globals.ThisAddIn.aisRibbon.OnRefreshPeriodCOM_Common();
        }

        public bool RefreshPeriod_ByOneTime()
        {
            Globals.ThisAddIn.addinParams = new AddInParams(Globals.ThisAddIn.Application.ActiveWorkbook);
            return Globals.ThisAddIn.aisRibbon.OnRefreshPeriodCOM_ByOneTime();
        }
        //ISSUE_NO.810 sunyi 2018/08/02 End
    }

    public class GraphAlarmList
    {
        public string No { get; set; }
        public string Name { get; set; }

        public GraphAlarmList(string no, string name)
        {
            this.No = no;
            this.Name = name;
        }
    }
}
