﻿namespace AisTemplateAddin
{
    partial class OnInventryRemoveDuplicatesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RemoveDuplicatesHost1 = new System.Windows.Forms.Integration.ElementHost();
            this.onInventryRemoveDuplicates1 = new AisTemplateAddin.OnInventryRemoveDuplicates();
            this.SuspendLayout();
            // 
            // RemoveDuplicatesHost1
            // 
            this.RemoveDuplicatesHost1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RemoveDuplicatesHost1.Location = new System.Drawing.Point(0, 0);
            this.RemoveDuplicatesHost1.Name = "RemoveDuplicatesHost1";
            this.RemoveDuplicatesHost1.Size = new System.Drawing.Size(294, 37);
            this.RemoveDuplicatesHost1.TabIndex = 0;
            this.RemoveDuplicatesHost1.Text = "RemoveDuplicatesHost1";
            this.RemoveDuplicatesHost1.Child = this.onInventryRemoveDuplicates1;
            // 
            // OnInventryRemoveDuplicatesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(294, 37);
            this.ControlBox = false;
            this.Controls.Add(this.RemoveDuplicatesHost1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OnInventryRemoveDuplicatesForm";
            this.Text = "重複データ削除処理中・・・";
            //this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost RemoveDuplicatesHost1;
        private OnInventryRemoveDuplicates onInventryRemoveDuplicates1;
    }
}