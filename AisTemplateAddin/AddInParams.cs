﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;

namespace AisTemplateAddin
{
    public class AddInParams
    {
        public AddInParams(Workbook workbook)
        {
            AddInPreCommand = new List<string>();
            AddInPostCommand = new List<string>();
            AddinMiscParams = new Dictionary<string, string>();
            MiscParams = new Dictionary<string, string>();
            OnlineTemplate = new Dictionary<string, string>();

            //Graph_Alarm sunyi 2018/09/07 start
            MaxNum = 10;
            //Graph_Alarm sunyi 2018/09/07 end
            ReadParametersFromExcel(workbook);
        }

        #region consts
        /// <summary>
        /// Addin parameters sheet name
        /// </summary>
        private const string ADDIN_PARAM_SHEET = "AddinParam";

        /// <summary>
        /// Parameters sheet name
        /// </summary>
        private const string PARAM_SHEET = "param";

        /// <summary>
        /// Online Template sheet name
        /// </summary>
        private const string ONLINE_TEMPLATE_SHEET = "オンラインテンプレート";

        /// <summary>
        /// Tab ID for FIFO template
        /// </summary>
        private const string FIFO_TAB_ID = "FIFOTab";

        /// <summary>
        /// Online parameter
        /// </summary>
        private const string ONLINE_PARAM_NAME = "オンライン";
        private const string ONLINE_PARAM_VALUE = "ONLINE";

        /// <summary>
        /// CTM parameter
        /// </summary>
        private const string CTM_PARAM_NAME = "収集タイプ";
        private const string CTM_PARAM_VALUE = "CTM";
        //ISSUE_NO.727 sunyi 2018/06/14 Start
        //grip mission処理ができるように修正にする
        private const string CTM_PARAM_VALUE_GRIP = "GRIP";
        //ISSUE_NO.727 sunyi 2018/06/14 End

        /// <summary>
        /// PeriodFixed parameter
        /// </summary>
        private const string KIND_PARAM_NAME = "オンライン種別";
        private const string KIND_PARAM_VALUE = "期間固定";

        /// <summary>
        /// Show addin tab specified in excel file
        /// </summary>
        private const string ADDIN_TAB_PARAM = "AddIn Tab ID";

        /// <summary>
        /// Execute macro specified in excel file before c# action
        /// </summary>
        private const string ADDIN_PRE_COMMAND_PARAM = "AddIn Pre Command ID";

        /// <summary>
        /// Execute macro specified in excel file after c# action
        /// </summary>
        private const string ADDIN_POST_COMMAND_PARAM = "AddIn Post Command ID";

        /// <summary>
        /// Online template tab id
        /// </summary>
        private const string ONLINE_TAB_ID = "OnlineTab";

        /// <summary>
        /// Online template tab id
        /// </summary>
        private const string MULTI_STATUS_MONITOR_TAB_ID = "MultiMonitorTab";
        /// <summary>
        /// Torque template tab id
        /// </summary>
        private const string TORQUE_STATUS_MONITOR_TAB_ID = "TorqueTab";
        /// <summary>
        /// Stream template tab id
        /// </summary>
        private const string STREAM_STATUS_MONITOR_TAB_ID = "StreamTab";
        
        /// <summary>
        /// Auto fresh command
        /// </summary>
        private const string AUTO_REFRESH_COMMAND_START = "Main_Module.AutoUpdateStart";
        private const string AUTO_REFRESH_COMMAND_STOP = "Main_Module.AutoUpdateStop";
        private const string AUTO_REFRESH_KEY = "自動更新";
        private const string AUTO_REFRESH_ON = "ON";

        /// <summary>
        /// code mode switch
        /// </summary>
        private const string CODE_MODE_SWITCH_ON = "Main_Module.SwitchOn";
        private const string CODE_MODE_SWITCH_OFF = "Main_Module.SwitchOff";
        private const string MODE_SWITCH_KEY = "SwitchLink";
        private const string SWITCH_ON = "ON";

        /// <summary>
        /// code mode searchType
        /// </summary>
        //private const string SEARCH_TYPE_COMMAND_ON = "Main_Module.SwitchOn";
        //private const string SEARCH_TYPE_COMMAND_OFF = "Main_Module.SwitchOff";
        private const string SEARCH_TYPE_KEY = "検索タイプ";
        private const string SEARCH_TYPE_ON = "ON";

        /// <summary>
        /// enable multi DAC
        /// </summary>
        private const string ENABLE_MULTI_DAC = "MultiDac有効";
        private const string ENABLE_MULTI_DAC_TRUE = "TRUE";

        /// <summary>
        /// enable R2 multi DAC
        /// </summary>
        private const string ENABLE_OPEN_FROM_R2 = "OpenFromR2";
        private const string ENABLE_OPEN_FROM_R2_TRUE = "TRUE";

        // param シートに DomainID をいれる。
        private const string DOMAINID = "Domain Id";


        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        /// <summary>
        /// Parameters sheet name
        /// </summary>
        private const string DAC_REFERE_SHEET = "dac_refere";

        private bool isSubDAC = false;
        // Wang Issue NO.687 2018/05/18 End

        //Graph_Alarm sunyi 2018/09/07 start
        public string PARAM_GRAPH_SHEET_NAME = "GraphAlarm_Param";
        //Graph_Alarm sunyi 2018/09/07 end

        // Wang Show merge sheets addin button when this is "PAT" 20181025 Start
        private const string QUICKGRAPH_CSV_SHEET = "csvSheet";
        // Wang Show merge sheets addin button when this is "PAT" 20181025 End

        // Wang Issue AISMM-91 20190124 Start
        private const string STATUSMONITOR_CTM_CSV_SHEET = "ミッション_MISSION";
        // Wang Issue AISMM-91 20190124 End

        // Wang Issue DAC-13 20190320 Start
        private const string IS_FIRST_SAVE = "IsFirstSave";
        // Wang Issue DAC-13 20190320 End

        #endregion

        #region properties
        /// <summary>
        /// Tab id which defined in excel file
        /// </summary>
        internal string AddInTab { get; set; }

        /// <summary>
        /// Macros which defined in excel file
        /// It will be execute when button is clicked before c# action
        /// </summary>
        internal List<string> AddInPreCommand { get; set; }

        //Graph_Alarm sunyi 2018/09/07 start
        internal int MaxNum { get; set; }
        //Graph_Alarm sunyi 2018/09/07 end
        /// <summary>
        /// Macros which defined in excel file
        /// It will be execute when button is clicked after c# action
        /// </summary>
        internal List<string> AddInPostCommand { get; set; }

        /// <summary>
        /// Other addin parameters
        /// </summary>
        internal Dictionary<string, string> AddinMiscParams { get; set; }

        /// <summary>
        /// Parameters from online template parameters
        /// </summary>
        internal Dictionary<string, string> OnlineTemplate { get; set; }

        /// <summary>
        /// Parameters from param sheet
        /// </summary>
        internal Dictionary<string, string> MiscParams { get; set; }
        #endregion

        // Wang Issue DAC-13 20190320 Start
        public bool NotRegistered()
        {
            if (this.MiscParams.ContainsKey(IS_FIRST_SAVE))
            {
                string paramValue = this.MiscParams[IS_FIRST_SAVE];
                return string.IsNullOrEmpty(paramValue);
            }

            return false;
        }
        // Wang Issue DAC-13 20190320 End

        public bool IsOnline()
        {
            if (this.MiscParams.ContainsKey(ONLINE_PARAM_NAME))
            {
                string paramValue = this.MiscParams[ONLINE_PARAM_NAME];
                return paramValue.Equals(ONLINE_PARAM_VALUE);
            }

            return false;
        }

        public bool IsMultiStatusMonitor()
        {
            return MULTI_STATUS_MONITOR_TAB_ID.Equals(this.AddInTab);
        }

        public bool IsTorqueStatusMonitor()
        {
            return TORQUE_STATUS_MONITOR_TAB_ID.Equals(this.AddInTab);
        }

        public bool IsStreamStatusMonitor()
        {
            return STREAM_STATUS_MONITOR_TAB_ID.Equals(this.AddInTab);
        }

        //20170421 sun no.589 -s
        public bool IsTypeCtm()
        {
            if (this.MiscParams.ContainsKey(CTM_PARAM_NAME))
            {
                string paramValue = this.MiscParams[CTM_PARAM_NAME];
                return !paramValue.Equals(CTM_PARAM_VALUE);
            }

            return false;
        }
        //20170421 sun no.589 -e

        public bool IsPeriodFixed()
        {
            if (this.OnlineTemplate.ContainsKey(KIND_PARAM_NAME))
            {
                string paramValue = this.OnlineTemplate[KIND_PARAM_NAME];
                return paramValue.Equals(KIND_PARAM_VALUE);
            }

            return false;
        }

        /// <summary>
        /// Get parameters from param sheet
        /// </summary>
        private void GetParamFromParamSheet(Workbook workbook)
        {
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            //// Get parameter sheet
            //Worksheet paramSheet = workbook.Sheets[PARAM_SHEET];
            //if (paramSheet == null) return;
            // Wang Issue NO.687 2018/05/18 End

            try
            {
                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                // Get parameter sheet
                Worksheet paramSheet = workbook.Sheets[PARAM_SHEET];
                if (paramSheet == null) return;
                // Wang Issue NO.687 2018/05/18 End

                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                // Get parameter sheet
                try
                {
                    Worksheet dacRefere = workbook.Sheets[DAC_REFERE_SHEET];
                    if (dacRefere != null) isSubDAC = true;
                }
#pragma warning disable
                catch (Exception e)
#pragma warning restore
                {
                }
                // Wang Issue NO.687 2018/05/18 End

                // Loop parameter rows up to empty cell
                for (int i = 1; ; i++)
                {
                    // Get parameter name
                    var paramName = paramSheet.Cells[i, 1].Value;

                    if (paramName != null && !string.IsNullOrEmpty(paramName.ToString()))
                    {
                        // Cast parameter name to string
                        string paramNameString = paramName.ToString();
                        if (string.IsNullOrEmpty(paramNameString))
                        {
                            break;
                        }

                        // Get parameter value
                        var paramValue = paramSheet.Cells[i, 2].Value;

                        // Cast parameter value to string
                        string paramValueString = paramValue == null ? "" : paramValue.ToString();

                        // Assign to properties
                        this.MiscParams.Add(paramNameString, paramValueString);
                    }
                    else {
                        break;
                    }
                }
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {
            }
        }

        /// <summary>
        /// Get parameters from online template sheet
        /// </summary>
        private void GetParamFromOnlineTemplateSheet(Workbook workbook)
        {
            // Get parameter sheet
            Worksheet paramSheet = workbook.Sheets[ONLINE_TEMPLATE_SHEET];
            if (paramSheet == null) return;

            try
            {
                // Loop parameter rows up to empty cell
                for (int i = 1; ; i++)
                {
                    // Get parameter name
                    var paramName = paramSheet.Cells[i, 1].Value;

                    if (paramName != null && !string.IsNullOrEmpty(paramName.ToString()))
                    {
                        // Cast parameter name to string
                        string paramNameString = paramName.ToString();
                        if (string.IsNullOrEmpty(paramNameString))
                        {
                            break;
                        }

                        // Get parameter value
                        var paramValue = paramSheet.Cells[i, 2].Value;

                        // Cast parameter value to string
                        string paramValueString = paramValue == null ? "" : paramValue.ToString();

                        // Assign to properties
                        this.OnlineTemplate.Add(paramNameString, paramValueString);
                    }
                    else
                    {
                        break;
                    }
                }
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {
            }
        }

        // Wang Issue AISMM-91 20190124 Start
        public List<string> GetCtmDataSheets()
        {
            List<string> ret = new List<string>();

            // Get parameter sheet
            try
            {
                Worksheet paramSheet = Globals.ThisAddIn.Application.ActiveWorkbook.Sheets[STATUSMONITOR_CTM_CSV_SHEET];
                for (int i = 1; ; i++)
                {
                    string sheetName = paramSheet.Cells[i, 5].Value;
                    if (string.IsNullOrEmpty(sheetName))
                    {
                        break;
                    }
                    ret.Add(sheetName);
                }
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }

            return ret;
        }
        // Wang Issue AISMM-91 20190124 End

        // Wang Show merge sheets addin button when this is "PAT" 20181025 Start
        /// <summary>
        /// Get data sheets names from quick graph template csvSheet
        /// </summary>
        public List<string> GetQuickGraphDataSheets()
        {
            List<string> ret = new List<string>();

            // Get parameter sheet
            try
            {
                Worksheet paramSheet = Globals.ThisAddIn.Application.ActiveWorkbook.Sheets[QUICKGRAPH_CSV_SHEET];
                for (int i = 3; ; i += 5)
                {
                    string sheetName = paramSheet.Cells[i, 2].Value;
                    if (string.IsNullOrEmpty(sheetName))
                    {
                        break;
                    }
                    ret.Add(sheetName);
                }
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }

            return ret;
        }
        // Wang Show merge sheets addin button when this is "PAT" 20181025 End

        /// <summary>
        /// Get addin tab from parameter sheet
        /// </summary>
        private void ReadParametersFromExcel(Workbook workbook)
        {
            // Clean internal variables
            this.AddInTab = "";
            AddInPreCommand.Clear();
            AddInPostCommand.Clear();
            AddinMiscParams.Clear();
            MiscParams.Clear();
            OnlineTemplate.Clear();

            try
            {
                // Get parameters from param sheet
                GetParamFromParamSheet(workbook);

                // Get parameter sheet
                Worksheet paramSheet = workbook.Sheets[ADDIN_PARAM_SHEET];
                if (paramSheet == null) return;

                // Loop parameter rows up to empty cell
                for (int i = 1; ; i++)
                {
                    // Get parameter name
                    var paramName = paramSheet.Cells[i, 1].Value;

                    if (paramName != null && !string.IsNullOrEmpty(paramName.ToString()))
                    {
                        // Cast parameter name to string
                        string paramNameString = paramName.ToString();
                        if (string.IsNullOrEmpty(paramNameString))
                        {
                            break;
                        }

                        // Get parameter value
                        var paramValue = paramSheet.Cells[i, 2].Value;

                        // Cast parameter value to string
                        string paramValueString = paramValue == null ? "" : paramValue.ToString();

                        // Assign to properties
                        if (ADDIN_TAB_PARAM.Equals(paramNameString))
                        {
                            // In the case of addin tab id, set it to AddInTab in order to be displayed
                            this.AddInTab = paramValue == null ? "" : paramValue.ToString();

                            // Get parameters from online template sheet
                            if (ONLINE_TAB_ID.Equals(this.AddInTab))
                            {
                                GetParamFromOnlineTemplateSheet(workbook);
                            }
                        }
                        else if (ADDIN_PRE_COMMAND_PARAM.Equals(paramNameString))
                        {
                            // In the case of macro command id, add it to command list in order to be executed
                            this.AddInPreCommand.Add(paramValue == null ? "" : paramValue.ToString());
                        }
                        else if (ADDIN_POST_COMMAND_PARAM.Equals(paramNameString))
                        {
                            // In the case of macro command id, add it to command list in order to be executed
                            this.AddInPostCommand.Add(paramValue == null ? "" : paramValue.ToString());
                        }
                        else
                        {
                            this.AddinMiscParams.Add(paramNameString, paramValueString);
                        }
                    }
                    else
                    {
                        break;
                    }
                }

            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {
            }
        }

        public void ReloadParam(string command)
        {
            if (AUTO_REFRESH_COMMAND_START.Equals(command) || AUTO_REFRESH_COMMAND_STOP.Equals(command) || CODE_MODE_SWITCH_ON.Equals(command) || CODE_MODE_SWITCH_OFF.Equals(command))
            {
                MiscParams.Clear();
                GetParamFromParamSheet(Globals.ThisAddIn.Application.ActiveWorkbook);
                Globals.ThisAddIn.aisRibbon.ribbon.Invalidate();
            }
        }

        public bool IsAutoFreshMode()
        {
            if (this.MiscParams.ContainsKey(AUTO_REFRESH_KEY))
            {
                string paramValue = this.MiscParams[AUTO_REFRESH_KEY];
                return paramValue.Equals(AUTO_REFRESH_ON);
            }
            return false;
        }
        private string getDomainId()
        {
            if (this.MiscParams.ContainsKey(DOMAINID))
            {
                string paramValue = this.MiscParams[DOMAINID];
                return paramValue;
            }
            else
            {
                // domain id が存在しない場合。古いDacはこれになる。
                // logger.Error("domain id が入ってない sheetになっている。");

                if (this.MiscParams.ContainsKey("登録フォルダ"))
                {
                    string paramvalue = this.MiscParams["登録フォルダ"];
                    // C:\foa\ais\[0-9A-Z]+\registry.... となっているので domain id を取り出す。
                    string pattern = @"\ais\([0-9A-Z]+)\";
                    Regex reg = new Regex(pattern, RegexOptions.None);
                    MatchCollection matches = reg.Matches(paramvalue);
                    if (matches.Count > 0)
                    {
                        string domainid = matches[1].Value;
                        return domainid;
                    }
                }
                return ""; 
            }
        }
        public string getDomainIdPath()
        {
            string tmp = this.getDomainId();
            if (tmp.Equals(""))
            {
                return "."; // . はパスで使うので注意 e.g. "aaa\" + getDomainPath() + "\aaa"
            }
            else
            {
                return tmp;
            }
        }
        //dac home start
        public bool IsOpenFromR2()
        {
            if (this.MiscParams.ContainsKey(ENABLE_OPEN_FROM_R2))
            {
                string paramValue = this.MiscParams[ENABLE_OPEN_FROM_R2];
                return paramValue.ToUpper().Equals(ENABLE_OPEN_FROM_R2_TRUE);

                //Worksheet paramSheet = Globals.ThisAddIn.Application.ActiveWorkbook.Sheets[PARAM_SHEET];
                //for (int i = 1; i < 30; i++)
                //{
                //    string paramName = paramSheet.Cells[i, 1].Value;
                //    if (paramName.Equals(ENABLE_OPEN_FROM_R2))
                //    {
                //        paramSheet.Cells[i, 2].Value = "";
                //        break;
                //    }
                //}
                //return flag;
            }
            else
            {
                //Dac Sampleの場合
                return true;
            }
        }
        //dac home end

        //検証シート_マルチモニター_No.65 sunyi 2018/11/20 start
        public bool IsPressedUpdate()
        {
            if (this.MiscParams.ContainsKey(AUTO_REFRESH_KEY))
            {
                string paramValue = this.MiscParams[AUTO_REFRESH_KEY];
                if (paramValue.Equals(AUTO_REFRESH_ON))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return false;
        }
        //検証シート_マルチモニター_No.65 sunyi 2018/11/20 end

        //ISSUE_NO.727 sunyi 2018/06/14 Start
        //grip mission処理ができるように修正にする
        //GRIPの場合、無効する
        public bool IsGripMode()
        {
            if (this.MiscParams.ContainsKey(CTM_PARAM_NAME))
            {
                string paramValue = this.MiscParams[CTM_PARAM_NAME];
                return paramValue.Equals(CTM_PARAM_VALUE_GRIP);
            }
            return false;
        }
        //ISSUE_NO.727 sunyi 2018/06/14 End

        public bool IsCodeModeON()
        {
            if (this.MiscParams.ContainsKey(MODE_SWITCH_KEY))
            {
                string paramValue = this.MiscParams[MODE_SWITCH_KEY];
                return paramValue.Equals(SWITCH_ON);
            }

            return false;
        }
        public bool IsSearchType()
        {
            if (this.MiscParams.ContainsKey(SEARCH_TYPE_KEY))
            {
                string paramValue = this.MiscParams[SEARCH_TYPE_KEY];
                return paramValue.Equals(SEARCH_TYPE_ON);
            }

            return false;
        }
        public bool IsMultiDACEnable()
        {
            if (this.MiscParams.ContainsKey(ENABLE_MULTI_DAC))
            {
                string paramValue = this.MiscParams[ENABLE_MULTI_DAC];
                // Wang Issue NO.687 20180516 Start
                // Change value to upper before comparation
                //return paramValue.Equals(ENABLE_MULTI_DAC_TRUE);
                return paramValue.ToUpper().Equals(ENABLE_MULTI_DAC_TRUE);
                // Wang Issue NO.687 20180516 End
            }

            return false;
        }
        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        public bool IsSubDAC()
        {
            return isSubDAC;
        }
        // Wang Issue NO.687 2018/05/18 End

        public Worksheet GetGraphAlarmParamSheet(string name)
        {
            foreach (Worksheet worksheet in Globals.ThisAddIn.Application.ActiveWorkbook.Sheets)
            {
                if (worksheet.Name.Equals(name))
                {
                    Worksheet sheet = Globals.ThisAddIn.Application.ActiveWorkbook.Sheets[name];
                    return sheet;
                }
            }
            return null;
        }

        // Wang Issue reference to value of cell 20190228 Start
        internal bool IsDAC()
        {
            return this.AddInTab.Equals("DACTab");
        }
        // Wang Issue reference to value of cell 20190228 End
    }
}
