﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Office = Microsoft.Office.Core;
using System.Text.RegularExpressions;

//using Microsoft.Office.Interop.Excel;

namespace AisTemplateAddin
{
    /// <summary>
    /// OnInventry.xaml の相互作用ロジック
    /// </summary>
    public partial class OnInventry : UserControl
    {
        class ButtonInfo
        {
            private Button btn;
            private bool status;
            private int idx;

            public ButtonInfo(Button btn, bool init, int idx)
            {
                this.btn = btn;
                this.status = init;
                this.idx = idx;
            }
            public Button getButton()
            {
                return this.btn;
            }
            public void SetStatus(bool status)
            {
                this.status = status;
                if (status)
                {
                    btn.Opacity = 1.0;
                    //btn.Background = Brushes.Red;
                    btn.Foreground = Brushes.Red;
                }
                else
                {
                    btn.Opacity = 0.25;
                    //btn.Background = Brushes.White;
                    btn.Foreground = Brushes.Black;
                }
            }
            public bool GetStatus()
            {
                return this.status;
            }
        }

        private ButtonInfo[] letters = null;
        internal OnInventryForm OwnerForm { get; set; }
        public OnInventry()
        {
            InitializeComponent();

            //Globals.ThisAddIn.Application;
            letters = new ButtonInfo[] {
                new ButtonInfo(this.lblLetter00, true, 0),
                new ButtonInfo(this.lblLetter01, true, 1),
                new ButtonInfo(this.lblLetter02, true, 2),
                new ButtonInfo(this.lblLetter03, true, 3),
                new ButtonInfo(this.lblLetter04, true, 4),
                new ButtonInfo(this.lblLetter05, true, 5),
                new ButtonInfo(this.lblLetter06, true, 6),
                new ButtonInfo(this.lblLetter07, true, 7),
                new ButtonInfo(this.lblLetter08, true, 8),
                new ButtonInfo(this.lblLetter09, true, 9),
                new ButtonInfo(this.lblLetter10, true, 10),
                new ButtonInfo(this.lblLetter11, true, 11),
                new ButtonInfo(this.lblLetter12, true, 12),
                new ButtonInfo(this.lblLetter13, true, 13),
                new ButtonInfo(this.lblLetter14, true, 14),
                new ButtonInfo(this.lblLetter15, true, 15),
                new ButtonInfo(this.lblLetter16, true, 16),
                new ButtonInfo(this.lblLetter17, true, 17),
                new ButtonInfo(this.lblLetter18, true, 18),
                new ButtonInfo(this.lblLetter19, true, 19)
            };
            InitializeSub();
        }


        private void InitializeSub()
        {
            string pos = "";
            foreach (Microsoft.Office.Interop.Excel.Range range in Globals.ThisAddIn.Application.ActiveCell) {
                pos = range.Columns.Address;
            }
            List<string> cellpos = GetSelectedCell(pos);

            column = cellpos[0]; // e.g 'A' or 'B' or 'AC'
            string row = cellpos[1]; // e.g. '3' '50'


            // 選択されたCELLの値をbuttonに並べる。
            elementValue = Globals.ThisAddIn.Application.ActiveCell.Text; // 例として出す値
            InitButtons(elementValue);

            // エレメント名取得して設定
            elementName = Globals.ThisAddIn.Application.ActiveSheet.Range[column + "1"].Text;
            lblElementName.Content = elementName;
        }
        // 例として使う、エレメントの値
        private string elementValue = "";
        // エレメント名
        private string elementName = "";
        // 選択されたカラムの名前
        private string column; 

        public void SetSampleElement(string ele)
        {
            this.elementValue = ele;
        }
        private bool isNeedSortColumnCondition()
        {
            // ソートのカラム条件が必須な場合
            if (cmbSort.SelectedIndex == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool isNeedSortCondition() {
            if ((cmbSort.SelectedIndex == 1)  || (cmbSort.SelectedIndex == 2))  {
                // ソートエレメントを利用する場合。
                return true;
            }
            else {
                // ソートエレメントを利用しない場合
                return false;
            }
        }
        private string ApplySortCond(string x)
        {
            if (cmbSort.SelectedIndex == 0)
            {
                // NONE
                return "";
            }
            if (cmbSort.SelectedIndex == 1) 
            {
                // エレメント値でソート
                return x;
            }
            // 以下は、cmbSort.SelectedIndex = 2の場合
            string ret = "";
            if (cmbAlign.SelectedIndex == 0)
            {
                for (int i = 0; i < 20 ; i++)
                {
                    if (letters[i].GetStatus())
                    {
                        if (i < x.Length)
                        {
                            ret = ret + x.Substring(i, 1);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            else if (cmbAlign.SelectedIndex == 1)
            {// TODO: BUG
                for (int i = 19 ; i >= 0 ; i--)
                {
                    if (letters[i].GetStatus())
                    {
                        if (i-(20-x.Length) >= 0)
                        {
                            ret = x.Substring(i-(20-x.Length), 1) + ret;
                        }
                        else
                        {
                            break;
                        }
                    }
                }
            }
            return ret;
        }
        public List<string> GetSelectedCell(string pos)
        {
            List<string> column = new List<string>();
            Regex reg = new Regex(@"^\$([A-Z]+)\$([0-9]+)$");
            var matches = reg.Matches(pos);
            foreach (Match m in matches)
            {
                if (m.Success)
                {
                    for (int j = 1; j < m.Groups.Count; j++)
                    {
                        column.Add(m.Groups[j].Value);
                    }
                    break;
                }
            }
            return column;
        }
        
        private MessageBoxResult RunCalc(string groupColumn)
        {
            int interval = 60*3; // 秒で計算 // 3分がデフォルトなので
            int startLine = 2; // データの開始行
            bool orderAscend = false; // 時間の順
            const int MaxLineForExcel = 100000; // 100000行(先頭のstartLine除く)までのデータを対象にする。
            try
            {
                string uiStr = cmbInterval.SelectedValue.ToString();
                orderAscend = (cmbOrder.SelectedIndex == 1) ? true : false;
                interval = Util.GetInterval(uiStr, interval); // Excel Addin で指定された時間間隔
                Microsoft.Office.Interop.Excel.Worksheet sheet = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet; // 現在のアクティブシートで滞留計算を行う。
                // 先頭列はCTMからの出の時刻
                // 2列目はCTMへの入りの時刻
                // selectedされたcolumnをグループとして扱う。
                
                DateTime maxDateTime = DateTime.MinValue;
                DateTime minDateTime = DateTime.MaxValue;
                HashSet<string> groups = new HashSet<string>();
                int i = 0;
                for (i = 0; i < MaxLineForExcel + 1; i++)
                {
                    string atimestr = sheet.Range["A" + (i+startLine).ToString("D")].Text;
                    string btimestr = sheet.Range["B" + (i+startLine).ToString("D")].Text;
                    string groupstr = ApplySortCond(sheet.Range[groupColumn + (i+startLine).ToString("D")].Text);
                    if (!(Util.isDateTimeOrNull(atimestr) && Util.isDateTimeOrNull(btimestr))) 
                    {
                        // 時刻フォーマットではない場合はここでデータが終了と判断
                        break;
                    }
                    if (string.IsNullOrEmpty(atimestr) && string.IsNullOrEmpty(btimestr))
                    {
                        // 同時にデータがない場合はデータの終了と判断
                        break;
                    }
                    // 最大時刻と、最小時刻を取り出す。
                    if (Util.isDateTime(atimestr))
                    {
                        DateTime atime = DateTime.Parse(atimestr);
                        if (atime >= maxDateTime)
                        {
                            maxDateTime = atime;
                        }
                        if (atime < minDateTime)
                        {
                            minDateTime = atime;
                        }
                    }
                    // 最大時刻と、最小時刻を取り出す。
                    if (Util.isDateTime(btimestr))
                    {
                        DateTime btime = DateTime.Parse(btimestr);
                        if (btime >= maxDateTime)
                        {
                            maxDateTime = btime;
                        }
                        if (btime < minDateTime)
                        {
                            minDateTime = btime;
                        }
                    }
                    // 出力シートの時刻は、maxDateTime,minDateTimeの間のみ。
                    // グルーピングしたときの種類をすべて探す
                    if (string.IsNullOrEmpty(groupstr)) {
                        groupstr = "";
                    }
                    groups.Add(groupstr); // Setなので重複は無いようにいれる。
                }
                int rowcount = i;
                
                string WarningMessage = "";
                if (rowcount >= MaxLineForExcel)
                {
                    WarningMessage = "\n警告：データ数が" + MaxLineForExcel + "以上あります。これを超える分は計算されません";
                }
                string currentSheetName = sheet.Name;
                string newSheetName = "滞留数-" + currentSheetName;

                // 出力先sheet名が重ならないように、postfixを加えたシート名を返す。
                newSheetName = getNewSheetNameForInventoryCalc(newSheetName); // 1000個まで探す。

                // 実行の確認ダイアログ
                System.Windows.Forms.DialogResult result = System.Windows.Forms.MessageBox.Show(
                    "バージョン: 3\n時間間隔:" + interval + "秒\nデータ数: " + rowcount.ToString("D") + "\n時刻順序:" + this.cmbOrder.Text+ "\nソート数:" + groups.Count + "\nシート名:" + newSheetName + WarningMessage, 
                    "期間検索",
                    System.Windows.Forms.MessageBoxButtons.OKCancel, 
                    System.Windows.Forms.MessageBoxIcon.Information, 
                    System.Windows.Forms.MessageBoxDefaultButton.Button1
                    );
                if (result == System.Windows.Forms.DialogResult.Cancel)
                {
                    return MessageBoxResult.Cancel;
                }
                /**
                 * maxDateTime,minDateTimeの時間でinterval間隔での配列を作る
                 * maxDateTime - minDateTime
                 */
                if (maxDateTime < minDateTime)
                {
                    // 時間が逆転しているので、計算しない
                    MessageBox.Show("時刻が逆になっているため計算できません。");
                    return MessageBoxResult.Cancel;
                }
                TimeSpan allInterval = maxDateTime - minDateTime;

                long arraySize = (long)(allInterval.TotalSeconds / interval) + 1;
                if (arraySize > 1000000)
                {
                    result = System.Windows.Forms.MessageBox.Show("滞留間隔が短い、または間隔が長いため計算できません", 
                        "エラー",
                        System.Windows.Forms.MessageBoxButtons.OK, 
                        System.Windows.Forms.MessageBoxIcon.Error);
                    if (result == System.Windows.Forms.DialogResult.OK)
                    {
                        return MessageBoxResult.Cancel;
                    }
                }
                // 滞留計算のカウントを以下で行う。
                // 1. カウント用のカウンタを準備
                int[] counter = new int[arraySize];
                DateTime[] ctime = new DateTime[arraySize];
                IDictionary<string,int[]> map = new Dictionary<string,int[]>();

                for (i = 0; i < arraySize; i++)
                {
                    ctime[i] = maxDateTime - TimeSpan.FromSeconds(interval * i);
                }
                foreach (string group in groups)
                {
                    map.Add(group, new int[arraySize]);
                    for (int j = 0 ; j < arraySize ; j++) {
                        map[group][j] = 0;
                    }
                }
                int revcount = 0; // 時刻が逆転しているものをカウントする。
                int nohitcount = 0; // 滞留数としてカウントされなかった個体の数。
                for (i = 0; i < rowcount ; i++)
                {
                    string atimestr = sheet.Range["A" + (i+startLine).ToString("D")].Text;
                    string btimestr = sheet.Range["B" + (i+startLine).ToString("D")].Text;
                    string groupstr = ApplySortCond(sheet.Range[groupColumn + (i+startLine).ToString("D")].Text);
                    //string groupstr = sheet.Range["C" + (i + startLine).ToString("D")].Text;
                    if (string.IsNullOrEmpty(groupstr)) {
                        groupstr = "";
                    }
                    if (!(Util.isDateTimeOrNull(atimestr) && Util.isDateTimeOrNull(btimestr)))
                    {
                        // 同時にない場合はここでデータが終了と判断
                        break;
                    }
                    DateTime atime, btime;
                    if (string.IsNullOrEmpty(atimestr))
                    {
                        atime = DateTime.MinValue;
                    }
                    else
                    {
                        //atime = DateTime.Parse(atimestr);
                        atime = DateTime.FromOADate(sheet.Range["A" + (i+startLine).ToString("D")].Value2);
                    }
                    if (string.IsNullOrEmpty(btimestr))
                    {
                        btime = DateTime.MaxValue;
                    }
                    else
                    {
                        //btime = DateTime.Parse(btimestr);
                        btime = DateTime.FromOADate(sheet.Range["B" + (i+startLine).ToString("D")].Value2);
                    }
                    if (atime.CompareTo(btime) > 0) {
                        // 逆転している。
                        revcount++;
                    }
                    Boolean flag = false;
                    for (int j = 0; j < arraySize; j++)
                    {
                        if (atime.CompareTo(ctime[j]) <= 0 && ctime[j].CompareTo(btime) < 0)
                        {
                            map[groupstr][j] = map[groupstr][j] + 1;
                            flag = true;
                        }
                    }
                    if (!flag) {
                        nohitcount++;
                    }
                }
                // 全計算終了。残り、シートに値を埋める。
                /**
                 *  結果が、ctime[],map[グループ][]に arraySize分入っているので、これを別シートに作る
                 **/
                Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.Add();
                Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet.Name = newSheetName;
                Microsoft.Office.Interop.Excel.Worksheet outsheet = Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets[newSheetName];

                // ヘッダ部分の書き込み。
                outsheet.Range["A1"].Value = "時刻";
                if (groups.Count == 1)
                {
                    outsheet.Range["B1"].Value = "滞留数";
                }
                else
                {
                    int ii = 2;
                    foreach (string group2 in groups)
                    {
                        Microsoft.Office.Interop.Excel.Range rg3 = outsheet.Cells[1,ii];
                        rg3.Value = "滞留数(" + group2 + ")";
                        ii++;
                    }

                }

                // 表の書き込み。
                for (i = 0; i < arraySize; i++)
                {
                    int ii; 
                    if (orderAscend)
                    {
                        ii = ((int)arraySize) -1 - i ;
                    }
                    else
                    {
                        ii = i;
                    }
                    // 横一行分の書き込み。
                    outsheet.Range["A" + (ii + 2).ToString("D")].Value = ctime[i].ToString("yyyy/MM/dd HH:mm:ss"); // 文字列にconvert
                    int kk = 2;
                    foreach (string group2 in groups)
                    {
                        Microsoft.Office.Interop.Excel.Range rg2 = outsheet.Cells[ii + 2, kk];
                        rg2.Value = map[group2][i]; // 個数
                        kk = kk + 1;
                    }
                    //outsheet.Range["B" + (i + 2).ToString("D")].Value = counter[i]; // 個数
                }
                outsheet.Columns.AutoFit(); // カラムの幅を自動的に見えるようにする 
                                            /**
                                             * 処理が終わったので閉じる
                                             */
                return MessageBoxResult.OK;
            }
            catch (Exception ex)
            {
                MessageBox.Show("エラーが発生しました。" + ex.Message + " " + ex.StackTrace);
                return MessageBoxResult.Cancel;
            }

        }

        private string getNewSheetNameForInventoryCalc(string sheetName)
        {
            int i,j;
            bool dupflag = false;
            for (j = 0; j < 1000; j++)
            {
                string checkName = "";
                if (j == 0)
                {
                    checkName = sheetName;
                }
                else
                {
                    checkName = sheetName + "(" + j.ToString("D") + ")";
                }
                dupflag = false;
                for (i = 1; i <= Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets.Count; i++)
                {
                    if (Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets[i].Name == checkName)
                    {
                        //MessageBox.Show("dup name=" + checkName + " here=" + Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets[i].Name);
                        dupflag = true;
                        break;
                    }
                }
                if (dupflag == false)
                {
                    return checkName;
                }
            }
            // ERROR
            return sheetName;
        }


        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
        }

        private void ComboBox_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }

        private void CmbInterval_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
        int oldCmbSortSelectedIndex = 0;
        private void CmbSort_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // 0 -> 1, 2 はOK
            // 1 -> 0,2 は OK
            // 2 -> 0,1 は警告
            // oldが2の場合のみ警告でそれ以外はそのまま素通り
            ComboBox comboBox = (ComboBox)sender;
            if (oldCmbSortSelectedIndex == 2)
            {
                // 警告を出す。
                MessageBoxResult result = MessageBox.Show("ソート設定が消去されますがよろしいでしょうか？", "警告", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
            }
            if (cmbSort.SelectedIndex == 2)
            {
                VisibleButtons(elementValue);
            }
            else
            {
                HiddenButtons();
            }
            // 保存
            oldCmbSortSelectedIndex = cmbSort.SelectedIndex;
        }

        // 前方、後方のcombo box変更
        private void ComboBox_SelectionChanged_2(object sender, SelectionChangedEventArgs e)
        {
            if (letters == null || letters.Length == 0)
            {
            }
            else
            {
                MessageBoxResult result = MessageBox.Show("ソート設定が消去されますがよろしいでしょうか？", "警告", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                VisibleButtons(elementValue);
            }
        }
/*
        private void CbSort_Click(object sender,RoutedEventArgs e)
        {
            if ((bool)this.cbSort.IsChecked)
            {
                InitButtons(elementValue);
                VisibleButtons(elementValue);
            }
            else
            {
                // 設定が消えるけどよいか？
                ConfirmMessageForClear();
                HiddenButtons();
            }
        }
*/
        private void BtnOk_Click(object sender, RoutedEventArgs e)
        {
            // インターバルの情報
            string interval = cmbInterval.Text;
            // 順番の情報
            string orderAscend = cmbOrder.Text;
            // ボタン指定するか？
            MessageBoxResult result = RunCalc(column);
            if (result == MessageBoxResult.OK)
            {
                OwnerForm.DialogResult = System.Windows.Forms.DialogResult.OK;
                OwnerForm.Close();
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            // そのままにする、またはcloseする。
            MessageBoxResult result = ConfirmMessage();
            if (result == MessageBoxResult.OK)
            {
                OwnerForm.DialogResult = System.Windows.Forms.DialogResult.OK;
                OwnerForm.Close();
            }
        }
        private void InitButtons(string value)
        {
            for (int i = 0; i < 20; i++)
            {
                //letters[i].getButton().Opacity = 1.0;
                letters[i].getButton().Visibility = Visibility.Hidden;
                letters[i].SetStatus(true);
            }
            grpBtn.IsEnabled = false;
            cmbAlign.IsEnabled = false;
        }

        private void VisibleButtons(string value)
        {
            grpBtn.IsEnabled = true;
            cmbAlign.IsEnabled = true;
            if (letters == null || letters.Length == 0)
            {
                return;
            }
            if (cmbAlign.SelectedIndex == 0) // 前方
            {
                for (int i = 0; i < 20; i++)
                {
                    if (i < value.Length)
                    {
                        letters[i].getButton().Content = value.Substring(i, 1);
                    }
                    else
                    {
                        letters[i].getButton().Content = " ";
                    }
                    letters[i].getButton().Opacity = 1.0;
                    letters[i].getButton().Visibility = Visibility.Visible;
                }
            }
            else if (cmbAlign.SelectedIndex == 1)　// 後方
            {
                for (int i = 0; i < 20; i++)
                {
                    if (i >= (20 - value.Length))
                    {
                        letters[i].getButton().Content = value.Substring(i-(20-value.Length), 1);
                    }
                    else
                    {
                        letters[i].getButton().Content = " ";
                    }
                    letters[i].getButton().Opacity = 1.0;
                    letters[i].getButton().Visibility = Visibility.Visible;
                }
            }
        }

        private void HiddenButtons()
        {
            if (letters == null || letters.Length != 20)
            {
                return;
            }
            for (int i = 0; i < 20; i++)
            {
                //letters[i].getButton().Content = value.Substring(i, 1);
                letters[i].getButton().Opacity = 1.0;
                letters[i].getButton().Visibility = Visibility.Hidden;
            }
            grpBtn.IsEnabled = false;
            cmbAlign.IsEnabled =false;

        }

        private void ClickButton(int idx)
        {
            if (letters[idx].GetStatus())
            {
                letters[idx].SetStatus(false);
                letters[idx].getButton().Opacity = 0.25;
            }
            else
            {
                letters[idx].SetStatus(true);
                letters[idx].getButton().Opacity = 1.0;
            }
        }
        private void LblLetter00_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(0);
        }

        private void LblLetter01_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(1);
        }

        private void LblLetter02_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(2);
        }

        private void LblLetter03_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(3);
        }

        private void LblLetter04_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(4);
        }

        private void LblLetter05_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(5);
        }

        private void LblLetter06_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(6);
        }

        private void LblLetter07_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(7);
        }

        private void LblLetter08_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(8);
        }

        private void LblLetter09_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(9);
        }

        private void LblLetter10_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(10);
        }

        private void LblLetter11_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(11);
        }

        private void LblLetter12_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(12);
        }

        private void LblLetter13_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(13);
        }

        private void LblLetter14_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(14);
        }

        private void LblLetter15_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(15);
        }

        private void LblLetter16_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(16);
        }
        private void LblLetter17_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(17);
        }

        private void LblLetter18_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(18);
        }

        private void LblLetter19_Click(object sender, RoutedEventArgs e)
        {
            ClickButton(19);            
        }
        
        private void ErrorMessage()
        {
            MessageBox.Show("エレメントが選択されていません。選択してからボタンを押してください。");
        }

        private void ErrorMessageGrouping()
        {
            MessageBox.Show("ソート条件での種別が10を超えています。10以下になるようにするか、ソート指定をしないようにしてください。");
        }

        private void Procseding()
        {
            MessageBox.Show("時間間隔: {} データ数: {} 種別数: {} 時刻出力: {} シート名: {} で出力します。");
            // RESULTをみて続行するかどうかする
        }
        private MessageBoxResult ConfirmMessage()
        {
            return MessageBox.Show("キャンセルします。よろしいですか？","警告",MessageBoxButton.OKCancel,MessageBoxImage.Warning);
        }
        private MessageBoxResult ConfirmMessageForClear() // ソート条件のtoggleの変更の場合
        {
            return MessageBox.Show("ソート条件を消去し、文字列全体としてソートとしてよろしいですか？","警告",MessageBoxButton.OKCancel,MessageBoxImage.Warning);
        }

        private int GetIndexOfButton(Button b)
        {
            for (int i = 0; i < 20; i++)
            {
                if (letters[i].getButton() == b)
                {
                    return i;
                }
            }
            return -1;
        }
        private void SetButtonStatus(int a,int b,bool sts)
        {
            for (int i = a; i <= b; i++)
            {
                letters[i].SetStatus(sts);
            }
        }
        private int GetBtnNo(object sender)
        {
            //ContextMenu b = (ContextMenu)sender;
            MenuItem bbbb = (MenuItem)sender;
            //ContextMenu bbb = (ContextMenu)bbbb.Parent;
            string cm = bbbb.Name;
            string buttonNoStr = "";
            Regex reg = new Regex(@"_([0-9]+)$");
            var matches = reg.Matches(cm);
            
            foreach (Match m in matches)
            {
                if (m.Success)
                {
                    buttonNoStr = m.Groups[1].Value;
                    break;
                }
            }
            int btnNo = int.Parse(buttonNoStr);
            return btnNo;
        }
        // 右クリックのメニューからのイベントハンドラー
        private void MiRightOn_Click(object sender, RoutedEventArgs e)
        {
            int btnNo = GetBtnNo(sender);
            SetButtonStatus(btnNo, 19, true);
        }

        private void MiRightOff_Click(object sender, RoutedEventArgs e)
        {
            int btnNo = GetBtnNo(sender);
            SetButtonStatus(btnNo, 19, false);
        }

        private void MiLeftOn_Click(object sender, RoutedEventArgs e)
        {
            int btnNo = GetBtnNo(sender);
            SetButtonStatus(0, btnNo, true);
        }

        private void MiLeftOff_Click(object sender, RoutedEventArgs e)
        {
            int btnNo = GetBtnNo(sender);
            SetButtonStatus(0, btnNo, false);
        }
    }
}
