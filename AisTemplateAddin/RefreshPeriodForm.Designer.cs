﻿namespace AisTemplateAddin
{
    partial class RefreshPeriodForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        //private void InitializeComponent()
        private void InitializeComponent(bool IsNewPeriod)
        //ISSUE_NO.810 sunyi 2018/08/02 End
        {
            this.refreshPeriodControl = new System.Windows.Forms.Integration.ElementHost();
            //ISSUE_NO.810 sunyi 2018/08/02 Start
            //仕様変更、ボタン作成機能を追加する
            //this.RefreshPeriod = new AisTemplateAddin.RefreshPeriod();
            this.RefreshPeriod = new AisTemplateAddin.RefreshPeriod(IsNewPeriod);
            //ISSUE_NO.810 sunyi 2018/08/02 End
            this.SuspendLayout();
            // 
            // refreshPeriodControl
            // 
            this.refreshPeriodControl.Location = new System.Drawing.Point(0, 0);
            this.refreshPeriodControl.Name = "refreshPeriodControl";
            //ISSUE_NO.810 sunyi 2018/08/02 Start
            //仕様変更、ボタン作成機能を追加する
            //this.refreshPeriodControl.Size = new System.Drawing.Size(362, 193);
            this.refreshPeriodControl.Size = new System.Drawing.Size(460, 193);
            //ISSUE_NO.810 sunyi 2018/08/02 End
            this.refreshPeriodControl.TabIndex = 0;
            this.refreshPeriodControl.Child = this.RefreshPeriod;
            // 
            // RefreshPeriodForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //ISSUE_NO.810 sunyi 2018/08/02 Start
            //仕様変更、ボタン作成機能を追加する
            //this.ClientSize = new System.Drawing.Size(362, 193);
            this.ClientSize = new System.Drawing.Size(460, 193);
            //ISSUE_NO.810 sunyi 2018/08/02 End
            this.Controls.Add(this.refreshPeriodControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RefreshPeriodForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "期間再設定";
            this.Load += new System.EventHandler(this.RefreshPeriodForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost refreshPeriodControl;
        private RefreshPeriod RefreshPeriod;
    }
}