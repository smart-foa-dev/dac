﻿namespace AisTemplateAddin
{
    partial class GraphAlarmSettingDetailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent(string paramName)
        {
            this.graphAlarmSettingDetailControl = new System.Windows.Forms.Integration.ElementHost();
            this.GraphAlarmSettingDetail = new AisTemplateAddin.GraphAlarmSettingDetail(paramName);
            this.SuspendLayout();
            // 
            // graphAlarmSettingDetailControl
            // 
            this.graphAlarmSettingDetailControl.Location = new System.Drawing.Point(0, 1);
            this.graphAlarmSettingDetailControl.Name = "graphAlarmSettingDetailControl";
            this.graphAlarmSettingDetailControl.Size = new System.Drawing.Size(450, 650);
            this.graphAlarmSettingDetailControl.TabIndex = 0;
            this.graphAlarmSettingDetailControl.Child = this.GraphAlarmSettingDetail;
            // 
            // GraphAlarmSettingDetailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(450, 650);
            this.Controls.Add(this.graphAlarmSettingDetailControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "GraphAlarmSettingDetailForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Setting Detail";
            this.Load += new System.EventHandler(this.GraphAlarmSettingDetailForm_Load);
            this.ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost graphAlarmSettingDetailControl;
        private GraphAlarmSettingDetail GraphAlarmSettingDetail;
    }
}