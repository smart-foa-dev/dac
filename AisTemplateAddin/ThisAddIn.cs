﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Excel = Microsoft.Office.Interop.Excel;
using Office = Microsoft.Office.Core;
using Microsoft.Office.Tools.Excel;
using FoaCore.Common.Util;
using System.Globalization;
using System.Windows;
using AisTemplateAddin.Properties;
using System.Windows.Forms;

namespace AisTemplateAddin
{
    public partial class ThisAddIn
    {
        /// <summary>
        /// Ribbon object
        /// </summary>
        public AisRibbon aisRibbon = null;

        public AddInParams addinParams = null;
        
        private IAisTemplateAddinUtils utilities;

        // コンテキストメニューの設定を実施したかのフラグ
        private static bool addContextMenu = false;

        // Wang Issue reference to value of cell 20190228 Start
        // private const string DAC_SHEET = "ＤＡＣシート"; // MAX: Column 'BJ'
        /// <summary>
        /// コマンドメニュー追加用ボタン
        /// </summary>
        private Office.CommandBarButton m_CopyButton;
        // Wang Issue reference to value of cell 20190228 End

        // Wang Issue DAC-13 20190320 Start
        public HashSet<Microsoft.Office.Interop.Excel.Workbook> wbs = new HashSet<Microsoft.Office.Interop.Excel.Workbook>();
        // Wang Issue DAC-13 20190320 End

        protected override object RequestComAddInAutomationService()
        {
            if (utilities == null)
            {
                utilities = new AisTemplateAddinUtils();
            }

            return utilities;
        }

        /// <summary>
        /// Set workbook activate handler and initialize macro command list
        /// </summary>
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            FoaMessageBox.SetResource(typeof(Properties.Resources));
            // Set workbook activate handler
            this.Application.WorkbookActivate += new Excel.AppEvents_WorkbookActivateEventHandler(Application_WorkbookActivate);
            // Wang Issue DAC-13 20190320 Start
            this.Application.WorkbookOpen += new Excel.AppEvents_WorkbookOpenEventHandler(Application_WorkbookOpen);
            // Wang Issue DAC-13 20190320 End
        }

        // Wang Issue DAC-13 20190320 Start
        private void Application_WorkbookOpen(Excel.Workbook workbook)
        {
            wbs.Add(workbook);
            workbook.BeforeClose += new Excel.WorkbookEvents_BeforeCloseEventHandler(Workbook_BeforeClose);
        }

        private bool IsApplicationVisible()
        {
            if (!Globals.ThisAddIn.Application.Visible || Globals.ThisAddIn.Application.WindowState == Excel.XlWindowState.xlMinimized)
            {
                return false;
            }

            int X = Globals.ThisAddIn.Application.ActiveWindow.PointsToScreenPixelsX(0);
            int Y = Globals.ThisAddIn.Application.ActiveWindow.PointsToScreenPixelsY(0);

            return (X < Screen.PrimaryScreen.Bounds.Width && Y < Screen.PrimaryScreen.Bounds.Height);
        }

        private void Workbook_BeforeClose(ref bool Cancel)
        {
            if (this.addinParams.NotRegistered() && this.IsApplicationVisible() && wbs.Contains(this.Application.ActiveWorkbook))
            {
                if (System.Windows.MessageBox.Show(Resources.ADDIN_C_001, Resources.ADDIN_C_CONFIRM, MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                {
                    Cancel = true;
                }
                else
                {
                    //dac home No.14 20190709 sunyi start
                    this.Application.DisplayAlerts = false;
                    //dac home No.14 20190709 sunyi end
                    wbs.Remove(this.Application.ActiveWorkbook);
                }
            }
        }
        // Wang Issue DAC-13 20190320 End

        /*
        private int GetHierarchy(Excel.Workbook workbook)
        {
            try
            {
                Excel.Worksheet dacSheet = workbook.Sheets[DAC_SHEET];
                if (DAC_SHEET != null)
                {
                    string hierachy = dacSheet.Cells[2, 2].Value.ToString();
                    hierachy = hierachy.Substring(8, hierachy.Length - 3 - 8);
                    return int.Parse(hierachy);
                }
            }
            catch (Exception e)
            {

            }

            return 0;
        }

        private bool NeedTrimSubDacDate(Excel.Workbook workbook)
        {
            if (this.addinParams.IsDAC())
            {
                if (GetHierarchy(workbook) <= 1)
                {
                    return false;
                }
            }

            return true;
        }
         * */

        void SuDACRefer_Click(Microsoft.Office.Core.CommandBarButton Ctrl, ref bool CancelDefault)
        {
            string bookName;
            string sheetName;
            string cellAddress;

            int row;
            int column;

            string subDACRefer;

            Excel.Application xls = Globals.ThisAddIn.Application;
            Excel.Workbook workbk = xls.ActiveWorkbook;
            Excel.Worksheet sheet = workbk.ActiveSheet as Excel.Worksheet;

            bookName = workbk.FullName;
            bookName = System.IO.Path.GetFileNameWithoutExtension(bookName);

            // Wang Issue reference to value of cell 20190228 Start
            //if (bookName.Length > 9)
            if (this.addinParams.IsSubDAC())
            // Wang Issue reference to value of cell 20190228 End
            {
                bookName = bookName.Substring(0, bookName.Length - 9);
            }

            sheetName = sheet.Name;


            Excel.Range range = (Excel.Range)Application.Selection;
            column = range.Column;
            row = range.Row;

            cellAddress = range.Address;

            // Wang Issue R2-143 20190329 Start
            subDACRefer = "SubDAC(" + bookName + ":" + sheetName + ":" + cellAddress + ")";
            //// Wang Issue reference to value of cell 20190228 Start
            ////subDACRefer = "SubDAC(" + bookName + ":" + sheetName + ":" + cellAddress + ")";
            //if (this.addinParams.IsSubDAC())
            //{
            //    subDACRefer = "SubDAC(" + bookName + ":" + sheetName + ":" + cellAddress + ")";
            //}
            //else
            //{
            //    subDACRefer = "SelfDAC(" + bookName + ":" + sheetName + ":" + cellAddress + ")";
            //}
            //// Wang Issue reference to value of cell 20190228 End
            // Wang Issue R2-143 20190329 End

            System.Windows.Forms.Clipboard.SetText(subDACRefer);
        }

        /// <summary>
        /// Get addin tab from parameter sheet and refresh ribbon
        /// </summary>
        private void Application_WorkbookActivate(Excel.Workbook workbook)
        {
            // Get addin tab from parameter sheet
            addinParams = new AddInParams(workbook);


            bool menuFlag = false;
            Excel.Sheets worksheets = workbook.Worksheets;
            foreach (Excel.Worksheet worksheet in worksheets)
            {
                if (worksheet.Name.Equals("param"))
                {
                    menuFlag = true;
                    break;
                }
            }

            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            //if (menuFlag && addContextMenu == false && addinParams.IsMultiDACEnable())
            //{
            //    Office.MsoControlType menuItem = Office.MsoControlType.msoControlButton;
            //    var MenuText = (Office.CommandBarButton)Application.CommandBars["Cell"].
            //          Controls.Add(menuItem, missing, missing, 1, true);

            //    // 通常セル上での右クリックコンテキストメニューの追加
            //    MenuText.Style = Office.MsoButtonStyle.msoButtonCaption;
            //    MenuText.Caption = "〇SubDAC参照式のコピー";
            //    MenuText.Tag = "0";

            //    MenuText.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(SuDACRefer_Click);


            //    // PivotTable上での右クリックコンテキストメニューの追加
            //    Office.MsoControlType pivotMenuItem = Office.MsoControlType.msoControlButton;

            //    var pivotMenuText = (Office.CommandBarButton)Application.CommandBars["PivotTable Context Menu"].
            //          Controls.Add(pivotMenuItem, missing, missing, 1, true);


            //    pivotMenuText.Style = Office.MsoButtonStyle.msoButtonCaption;
            //    pivotMenuText.Caption = "〇SubDAC参照式のコピー";
            //    pivotMenuText.Tag = "0";

            //    pivotMenuText.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(SuDACRefer_Click);

            //    addContextMenu = true;

            //}            

            // Wang Issue reference to value of cell 20190228 Start
            //if (menuFlag && addContextMenu == false && addinParams.IsSubDAC())
            if (menuFlag && addContextMenu == false && (addinParams.IsSubDAC() || addinParams.IsDAC()))
            // Wang Issue reference to value of cell 20190228 End
            {
                Office.CommandBarControl before = null;
                /*
                try
                {
                    before = Application.CommandBars["Cell"].Controls[Properties.Resources.DAC_MENU_SWITCH_PANE];
                }
                catch (Exception e)
                {

                }
                 * */

                Office.MsoControlType menuItem = Office.MsoControlType.msoControlButton;
                // Wang Issue reference to value of cell 20190228 Start
                //var MenuText = (Office.CommandBarButton)Application.CommandBars["Cell"].
                //    Controls.Add(menuItem, missing, missing, before == null ? 1 : 3, true);

                //// 通常セル上での右クリックコンテキストメニューの追加
                //MenuText.Style = Office.MsoButtonStyle.msoButtonCaption;
                //MenuText.Caption = Properties.Resources.DAC_MENU_COPY_FOMULA;
                //MenuText.Tag = "0";

                //MenuText.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(SuDACRefer_Click);

                m_CopyButton = (Office.CommandBarButton)Application.CommandBars["Cell"].
                    Controls.Add(menuItem, missing, missing, before == null ? 1 : 3, true);

                // 通常セル上での右クリックコンテキストメニューの追加
                m_CopyButton.Style = Office.MsoButtonStyle.msoButtonCaption;
                m_CopyButton.Caption = Properties.Resources.DAC_MENU_COPY_FOMULA;
                m_CopyButton.Tag = "0";

                m_CopyButton.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(SuDACRefer_Click);
                // Wang Issue reference to value of cell 20190228 End

                if (before == null)
                {
                    Application.CommandBars["Cell"].Controls[2].BeginGroup = true;
                }

                // PivotTable上での右クリックコンテキストメニューの追加
                Office.MsoControlType pivotMenuItem = Office.MsoControlType.msoControlButton;

                var pivotMenuText = (Office.CommandBarButton)Application.CommandBars["PivotTable Context Menu"].
                      Controls.Add(pivotMenuItem, missing, missing, 1, true);


                pivotMenuText.Style = Office.MsoButtonStyle.msoButtonCaption;
                pivotMenuText.Caption = Properties.Resources.DAC_MENU_COPY_FOMULA;
                pivotMenuText.Tag = "0";

                pivotMenuText.Click += new Microsoft.Office.Core._CommandBarButtonEvents_ClickEventHandler(SuDACRefer_Click);

                Application.CommandBars["PivotTable Context Menu"].Controls[2].BeginGroup = true;

                addContextMenu = true;

            }
            // Wang Issue NO.687 2018/05/18 End
            
            // Refresh ribbon
            aisRibbon.ribbon.Invalidate();
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            aisRibbon = new AisRibbon(this);
            return aisRibbon;
        }

        #region VSTO で生成されたコード

        /// <summary>
        /// デザイナーのサポートに必要なメソッドです。
        /// このメソッドの内容をコード エディターで変更しないでください。
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion

        public string GetAddinMiscParam(string key)
        {
            if (addinParams.AddinMiscParams.ContainsKey(key))
            {
                return addinParams.AddinMiscParams[key];
            }

            return string.Empty;
        }

        public string GetMiscParam(string key)
        {
            if (addinParams.MiscParams.ContainsKey(key))
            {
                return addinParams.MiscParams[key];
            }

            return string.Empty;
        }
    }
}
