﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AisTemplateAddin
{
    public partial class GraphAlarmSettingDetailForm : Form
    {
        public GraphAlarmSettingDetailForm(string paramName)
        {
            InitializeComponent(paramName);

            this.GraphAlarmSettingDetail.OwnerDetailForm = this;
        }

        private void GraphAlarmSettingDetailForm_Load(object sender, EventArgs e)
        {
        }
    }
}
