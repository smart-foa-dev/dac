﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AisTemplateAddin
{
    class Util
    {
        public static bool isDateTimeOrNull(string a)
        {
            if (string.IsNullOrEmpty(a))
            {
                return true;
            }
            else
            {
                return isDateTime(a);
            }
        }
        public static bool isDateTime(string a)
        {
            string[] datetimeregex = new string[] { @"\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{2}:\d{2}", @"\d{4}/\d{1,2}/\d{1,2} \d{1,2}:\d{2}:\d{2}", 
                                                    @"\d{4}-\d{1,2}-\d{1,2} \d{1,2}:\d{2}",       @"\d{4}/\d{1,2}/\d{1,2} \d{1,2}:\d{2}" };
            if (string.IsNullOrEmpty(a))
            {
                return false;
            }
            foreach (string i in datetimeregex)
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(a, i))
                {
                    return true;
                }
            }

            //else if (System.Text.RegularExpressions.Regex.IsMatch(a, @"\d{4}-\d{2}-\d{2} \d{1,2}:\d{2}:\d{2}"))
            //{
            // YYYY-MM-DD H:MM:SS
            //    return true;
            //}
            return false;
        }

        public static int GetInterval(string a, int defint)
        {
            if (string.IsNullOrEmpty(a))
            {
                return defint;
            }
            string[] reg = new string[] { @"(?<num>\d+?)秒間?", @"(?<num>\d+?)分間?", @"(?<num>\d+?)時間?" };
            int[] multi = new int[] { 1, 60, 3600 };
            int i = 0;
            foreach (string r in reg)
            {
                var m = new System.Text.RegularExpressions.Regex(r);
                var n = m.Match(a);
                while (n.Success)
                {
                    string numstr = n.Groups["num"].Value;
                    int num = int.Parse(numstr) * multi[i];
                    return num;
                }
                i = i + 1;
            }
            return defint;
        }

    }
}
