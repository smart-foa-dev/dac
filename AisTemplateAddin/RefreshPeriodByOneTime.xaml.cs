﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;
using System.ComponentModel;
using FoaCore.Common.Util;
using Microsoft.Office.Interop.Excel;
//Refresh_Period Sunyi 2018/06/27 Start
//仕様変更、期間再設定に時間設定を実装する
using System.Text.RegularExpressions;
//Refresh_Period Sunyi 2018/06/27 End

namespace AisTemplateAddin
{
    /// <summary>
    /// RefreshPeriodByOneTime.xaml の相互作用ロジック
    /// </summary>
    public partial class RefreshPeriodByOneTime : UserControl
    {
        internal RefreshPeriodFormByOneTime OwnerForm { get; set; }

        private const string PARAM_PERIOD_SHEET_NAME = "Period Sheet";
        private const string PARAM_PERIOD_START_DATE_CELL = "Period Start Date Cell";
        private const string PARAM_PERIOD_START_TIME_CELL = "Period Start Time Cell";
        private const string PARAM_PERIOD_END_DATE_CELL = "Period End Date Cell";
        private const string PARAM_PERIOD_END_TIME_CELL = "Period End Time Cell";
        private const string PARAM_PERIOD_START_DATETIME_CELL = "Period Start Date Time Cell";
        private const string PARAM_PERIOD_END_DATETIME_CELL = "Period End Date Time Cell";
        private const string PARAM_Chimney = "ChimneyTab";
        private const string PARAM_FIFO = "FIFOTab";
        private const string PARAM_StockTime = "StockTimeTab";
        private const string PARAM_Online = "OnlineTab";
        //Refresh_Period Sunyi 2018/06/27 Start
        //仕様変更、期間再設定に時間設定を実装する
        private const string PARAM_Stream = "StreamTab";
        //Refresh_Period Sunyi 2018/06/27 End

        public RefreshPeriodByOneTime(bool IsNewPeriod)
        {
            InitializeComponent();

            //ISSUE_NO.810 sunyi 2018/08/02 Start
            //仕様変更、ボタン作成機能を追加する
            if (IsNewPeriod)
            {
                this.Period_Button.IsEnabled = true;
            }
            else
            {
                this.Period_Button.IsEnabled = false;
                this.Period_Button.Foreground = new SolidColorBrush(Colors.Gray);
            }
            //ISSUE_NO.810 sunyi 2018/08/02 End

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            //ISSUE_NO.716 sunyi 2018/06/04 start
            //期間再設定の初期値をセットする
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetMultiStatusMonitorDayList()).GetList();
            //ISSUE_NO.716 sunyi 2018/06/04 end

            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }

            //Refresh_Period Sunyi 2018/06/27 Start
            //仕様変更、期間再設定に時間設定を実装する
            string sheetName = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_SHEET_NAME);
            string startDateCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_DATE_CELL);
            string startTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_TIME_CELL);
            string endDateCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_DATE_CELL);
            string endTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_TIME_CELL);

            string startDateTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_DATETIME_CELL);
            string endDateTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_DATETIME_CELL);

            Worksheet sheet = Globals.ThisAddIn.Application.ActiveWorkbook.Sheets[sheetName];

            if (Globals.ThisAddIn.addinParams.AddInTab.Equals(PARAM_Stream))
            {
                if (!string.IsNullOrEmpty(startTimeCell))
                {
                    this.dateTimePicker_DisplayStart.Value = DateTime.FromOADate(sheet.Range[startTimeCell].Value);
                }
                if (!string.IsNullOrEmpty(endTimeCell))
                {
                    this.dateTimePicker_DisplayEnd.Value = DateTime.FromOADate(sheet.Range[endTimeCell].Value);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(startDateTimeCell))
                {
                    this.dateTimePicker_DisplayStart.Value = sheet.Range[startDateTimeCell].Value ?? DateTime.Now;
                }
                if (!string.IsNullOrEmpty(endDateTimeCell))
                {
                    this.dateTimePicker_DisplayEnd.Value = sheet.Range[endDateTimeCell].Value ?? DateTime.Now;
                }
            }
            //Refresh_Period Sunyi 2018/06/27 End
        }

        //Refresh_Period Sunyi 2018/06/27 Start
        //仕様変更、期間再設定に時間設定を実装する
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        private void Validation_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }
        //Refresh_Period Sunyi 2018/06/27 End

        private bool ValidatePeriodByTemplate(string addinTabId, DateTimePicker end, bool onlineFlg)
        {
            if (onlineFlg && end.Value < DateTime.Now)
            {
                if (addinTabId.Equals(PARAM_Chimney) || addinTabId.Equals(PARAM_FIFO))
                {
                    FoaMessageBox.ShowError("AIS_E_002");
                    return false;
                }

                if (addinTabId.Equals(PARAM_StockTime) || addinTabId.Equals(PARAM_Online))
                {
                    FoaMessageBox.ShowError("AIS_E_024");
                    return false;
                }
            }

            return true;
        }

        private void OnGetTimeClick(object sender, RoutedEventArgs e)
        {
            string sheetName = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_SHEET_NAME);
            string startDateTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_DATETIME_CELL);
            string endDateTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_DATETIME_CELL);

            Worksheet sheet = Globals.ThisAddIn.Application.ActiveWorkbook.Sheets[sheetName];

            if (!string.IsNullOrEmpty(endDateTimeCell))
            {
                this.dateTimePicker_End.Value = sheet.Range[endDateTimeCell].Value;
            }
        }

        private void OnUpdateClick(object sender, RoutedEventArgs e)
        {
            DateTime endData;
            //Refresh_Period Sunyi 2018/06/27 Start
            //仕様変更、期間再設定に時間設定を実装する
            DateTime startTime;
            DateTime endTime;

            //if (GetStartAndEndTime(this.dateTimePicker_End, out endData))
            if (GetStartAndEndTime(this.dateTimePicker_End, this.dateTimePicker_DisplayStart, this.dateTimePicker_DisplayEnd, out endData, out startTime, out endTime))
            //Refresh_Period Sunyi 2018/06/27 End
            {
                if (ValidatePeriodByTemplate(Globals.ThisAddIn.addinParams.AddInTab, this.dateTimePicker_End, Globals.ThisAddIn.addinParams.IsOnline()))
                {
                    try
                    {
                        string sheetName = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_SHEET_NAME);
                        string startDateCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_DATE_CELL);
                        string startTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_TIME_CELL);
                        string endDateCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_DATE_CELL);
                        string endTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_TIME_CELL);
                        string startDateTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_START_DATETIME_CELL);
                        string endDateTimeCell = Globals.ThisAddIn.GetAddinMiscParam(PARAM_PERIOD_END_DATETIME_CELL);

                        Worksheet sheet = Globals.ThisAddIn.Application.ActiveWorkbook.Sheets[sheetName];

                        //Refresh_Period Sunyi 2018/06/27 Start
                        //仕様変更、期間再設定に時間設定を実装する
                        //if (!string.IsNullOrEmpty(startDateCell))
                        //{
                        //    sheet.Range[startDateCell].Value = endData.ToString("yyyy/MM/dd");
                        //}
                        //if (!string.IsNullOrEmpty(startTimeCell))
                        //{
                        //    sheet.Range[startTimeCell].Value = "00:00:00";
                        //}
                        //if (!string.IsNullOrEmpty(endDateCell))
                        //{
                        //    sheet.Range[endDateCell].Value = endData.ToString("yyyy/MM/dd");
                        //}
                        //if (!string.IsNullOrEmpty(endTimeCell))
                        //{
                        //    sheet.Range[endTimeCell].Value = "23:59:59";
                        //}
                        //if (!string.IsNullOrEmpty(startDateTimeCell))
                        //{
                        //   sheet.Range[startDateTimeCell].Value = endData.ToString("yyyy/MM/dd") + " 00:00:00";
                        //}
                        //if (!string.IsNullOrEmpty(endDateTimeCell))
                        //{
                        //   sheet.Range[endDateTimeCell].Value = endData.ToString("yyyy/MM/dd") + " 23:59:59";
                        //}
                        if (Globals.ThisAddIn.addinParams.AddInTab.Equals(PARAM_Stream))
                        {
                            if (!string.IsNullOrEmpty(startDateCell))
                            {
                                sheet.Range[startDateCell].Value = startTime.ToString("yyyy/MM/dd");
                            }
                            if (!string.IsNullOrEmpty(startTimeCell))
                            {
                                sheet.Range[startTimeCell].Value = startTime.ToString("HH:mm:ss");
                            }
                            if (!string.IsNullOrEmpty(endDateCell))
                            {
                                sheet.Range[endDateCell].Value = endTime.ToString("yyyy/MM/dd");
                            }
                            if (!string.IsNullOrEmpty(endTimeCell))
                            {
                                sheet.Range[endTimeCell].Value = endTime.ToString("HH:mm:ss");
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(startDateTimeCell))
                            {
                                sheet.Range[startDateTimeCell].Value = startTime.ToString("yyyy/MM/dd HH:mm:ss");
                            }
                            if (!string.IsNullOrEmpty(endDateTimeCell))
                            {
                                sheet.Range[endDateTimeCell].Value = endTime.ToString("yyyy/MM/dd HH:mm:ss");
                            }
                        }
                        //Refresh_Period Sunyi 2018/06/27 End

                        OwnerForm.DialogResult = System.Windows.Forms.DialogResult.OK;
                        OwnerForm.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }

        private bool IsDac()
        {
            return false;
        }

        //Refresh_Period Sunyi 2018/06/27 Start
        //仕様変更、期間再設定に時間設定を実装する
        //private bool GetStartAndEndTime(DateTimePicker pickerEnd, out DateTime end1)
        private bool GetStartAndEndTime(DateTimePicker pickerEnd, DateTimePicker pickerStartTime, DateTimePicker pickerEndTime, out DateTime end1, out DateTime startTime1, out DateTime endTime1)
        //Refresh_Period Sunyi 2018/06/27 End
        {
            end1 = DateTime.Now;

            //Refresh_Period Sunyi 2018/06/27 Start
            //仕様変更、期間再設定に時間設定を実装する
            startTime1 = DateTime.Now;
            endTime1 = DateTime.Now;

            DateTime? startTime = pickerStartTime.Value;
            DateTime? endTime = pickerEndTime.Value;
            //Refresh_Period Sunyi 2018/06/27 End

            DateTime? end = pickerEnd.Value;

            if (end == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return false;
            }

            end1 = end ?? DateTime.Now;

            end1 = end1.AddMilliseconds(999 - end1.Millisecond);
            //Refresh_Period Sunyi 2018/06/27 Start
            //仕様変更、期間再設定に時間設定を実装する
            if (startTime1 == null || endTime1 == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return false;
            }

            string dayStr = (end ?? DateTime.Now).ToString("yyyy/MM/dd");
            string addOneDayStr = ((end ?? DateTime.Now).AddDays(1)).ToString("yyyy/MM/dd");
            string startStr = (startTime ?? DateTime.Now).ToString("HH:mm:ss");
            string endStr = (endTime ?? DateTime.Now).ToString("HH:mm:ss");

            if (DateTime.Parse(startStr) > DateTime.Parse(endStr))
            {
                startTime1 = DateTime.Parse(dayStr + " " + startStr);
                endTime1 = DateTime.Parse(addOneDayStr + " " + endStr);
            }
            else
            {
                startTime1 = DateTime.Parse(dayStr + " " + startStr);
                endTime1 = DateTime.Parse(dayStr + " " + endStr);
            }
            //Refresh_Period Sunyi 2018/06/27 End

            return true;
        }

        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        private void GreateButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                OwnerForm.IsNew_Period = true;
                OwnerForm.DialogResult = System.Windows.Forms.DialogResult.OK;
                OwnerForm.Close();
            }
            catch (Exception ex)
            {
            }
        }
        //ISSUE_NO.810 sunyi 2018/08/02 End

    }
}
