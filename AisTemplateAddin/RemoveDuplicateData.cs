﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AisTemplateAddin
{
    class RemoveDuplicateData
    {
        private static readonly string START_PREFIX = "S:";
        private static readonly string START_TIME = START_PREFIX + "Time";
        private static readonly string START_CTM = START_PREFIX + "CTM";

        private static readonly string END_PREFIX = "E:";
        private static readonly string END_TIME = END_PREFIX + "Time";
        private static readonly string END_CTM = END_PREFIX + "CTM";

        private static readonly HashSet<string> EXCLUDED_TITLE = new HashSet<string>() {
            START_TIME, END_TIME
        };
        private static readonly HashSet<string> BREAK_SORT_TITLE = new HashSet<string>() {
            START_CTM, END_CTM
        };

        private static string GetNewSheetName(string expect)
        {
            HashSet<string> sheets = new HashSet<string>();
            foreach (Microsoft.Office.Interop.Excel.Worksheet sheet in Globals.ThisAddIn.Application.ActiveWorkbook.Worksheets)
            {
                sheets.Add(sheet.Name);
            }

            int i = 2;
            string newSheetName = expect;

            while (sheets.Contains(newSheetName))
            {
                newSheetName = expect + "(" + i.ToString() + ")";
                i++;
            }

            return newSheetName;
        }

        private static bool ValidateWorksheet(Microsoft.Office.Interop.Excel.Worksheet orgSheet)
        {
            string startTime = (orgSheet.Cells[1, 1].Value == null ? null : orgSheet.Cells[1, 1].Value.ToString());
            if (string.IsNullOrEmpty(startTime) || !startTime.Equals(START_TIME))
            {
                System.Windows.MessageBox.Show("データフォーマットが不正です。期間滞留検索結果シートを選択して再度実行してください。");
                return false;
            }

            return true;
        }

        public static Microsoft.Office.Interop.Excel.Worksheet CreateUniquDataSheet(Microsoft.Office.Interop.Excel.Worksheet orgSheet)
        {
            Microsoft.Office.Interop.Excel.Worksheet unique = null;

            try
            {
                if (!ValidateWorksheet(orgSheet))
                {
                    return null;
                }

                orgSheet.Application.ScreenUpdating = false;

                // copy sheet
                orgSheet.Copy(After: orgSheet);
                unique = (Microsoft.Office.Interop.Excel.Worksheet)Globals.ThisAddIn.Application.ActiveWorkbook.ActiveSheet;
                unique.Name = GetNewSheetName(orgSheet.Name + "(重複無し)");

                // find start keys
                List<object> startKeys = GetKeyColumns(unique, START_PREFIX);

                // find end keys
                List<object> endKeys = GetKeyColumns(unique, END_PREFIX);

                // find end keys
                List<object> times = new List<object>() { 1, 2 };

                // all keys
                IEnumerable<object> allKeys = startKeys.Concat(endKeys);

                // sort by all keys
                SortByKeys(unique, allKeys);

                // remove duplicate data by all keys
                RemoveDuplicateDataByAllKeys(unique, allKeys.ToArray());

                // sort by start keys
                SortByKeys(unique, startKeys);

                // remove duplicate data by start keys
                RemoveDuplicateDataByStartKeys(unique, startKeys.ToArray());

                // sort by end keys
                SortByKeys(unique, endKeys);

                // remove duplicate data by end keys
                RemoveDuplicateDataByEndKeys(unique, endKeys.ToArray());

                // sort by start keys
                SortByKeys(unique, times);
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                orgSheet.Application.ScreenUpdating = true;
            }

            return unique;
        }

        private static List<object> GetKeyColumns(Microsoft.Office.Interop.Excel.Worksheet orgSheet, string prefix)
        {
            List<object> keys = new List<object>();

            for (int i = 3; i < orgSheet.UsedRange.Columns.Count; i++)
            {
                string title = (orgSheet.Cells[1, i].Value == null ? null : orgSheet.Cells[1, i].Value.ToString());
                if (!string.IsNullOrEmpty(title))
                {
                    if (EXCLUDED_TITLE.Contains(title))
                    {
                        continue;
                    }
                    else if (BREAK_SORT_TITLE.Contains(title))
                    {
                        break;
                    }
                    else if (title.StartsWith(prefix))
                    {
                        keys.Add(i);
                    }
                }
                else
                {
                    break;
                }
            }

            return keys;
        }

        private static void RemoveDuplicateDataByStartKeys(Microsoft.Office.Interop.Excel.Worksheet orgSheet, object[] keys)
        {
            RemoveDuplicateDataBySpecifiedKeys(orgSheet, keys, 2);
        }

        private static void RemoveDuplicateDataByEndKeys(Microsoft.Office.Interop.Excel.Worksheet orgSheet, object[] keys)
        {
            RemoveDuplicateDataBySpecifiedKeys(orgSheet, keys, 1);
        }

        private static void RemoveDuplicateDataBySpecifiedKeys(Microsoft.Office.Interop.Excel.Worksheet orgSheet, object[] keys, int timeCol)
        {
            int startRow = 2;
            int endRow = 3;

            while (endRow <= orgSheet.UsedRange.Rows.Count)
            {
                string endTime = (orgSheet.Cells[endRow, timeCol].Value == null ? null : orgSheet.Cells[endRow, timeCol].Value.ToString());
                if (string.IsNullOrEmpty(endTime))
                {
                    endRow++;
                }
                else
                {
                    if (endRow > startRow + 1)
                    {
                        // remove duplicate rows
                        Microsoft.Office.Interop.Excel.Range target = orgSheet.Range[orgSheet.Cells[startRow, 1], orgSheet.Cells[endRow - 1, orgSheet.UsedRange.Columns.Count]];
                        target.RemoveDuplicates(Columns: keys.ToArray(), Header: Microsoft.Office.Interop.Excel.XlYesNoGuess.xlNo);
                    }
                    startRow = endRow;
                    endRow = startRow + 1;
                }
            }

            if (endRow > startRow + 1)
            {
                // remove duplicate rows
                Microsoft.Office.Interop.Excel.Range target = orgSheet.Range[orgSheet.Cells[startRow, 1], orgSheet.Cells[endRow - 1, orgSheet.UsedRange.Columns.Count]];
                target.RemoveDuplicates(Columns: keys.ToArray(), Header: Microsoft.Office.Interop.Excel.XlYesNoGuess.xlNo);
            }
        }

        private static void RemoveDuplicateDataByAllKeys(Microsoft.Office.Interop.Excel.Worksheet orgSheet, object[] keys)
        {
            Microsoft.Office.Interop.Excel.Range target = orgSheet.Range[orgSheet.Cells[2, 1], orgSheet.Cells[orgSheet.UsedRange.Rows.Count, orgSheet.UsedRange.Columns.Count]];

            // remove duplicate rows
            target.RemoveDuplicates(Columns: keys.ToArray(), Header: Microsoft.Office.Interop.Excel.XlYesNoGuess.xlNo);
        }

        private static void SortByKeys(Microsoft.Office.Interop.Excel.Worksheet orgSheet, IEnumerable<object> keys)
        {
            Microsoft.Office.Interop.Excel.Range target = orgSheet.Range[orgSheet.Cells[2, 1], orgSheet.Cells[orgSheet.UsedRange.Rows.Count, orgSheet.UsedRange.Columns.Count]];

            // add sort field
            orgSheet.Sort.SortFields.Clear();
            foreach (int i in keys)
            {
                orgSheet.Sort.SortFields.Add(orgSheet.Range[orgSheet.Cells[1, i], orgSheet.Cells[orgSheet.UsedRange.Rows.Count, i]]);
            }

            // sort
            orgSheet.Sort.SetRange(target);
            orgSheet.Sort.Header = Microsoft.Office.Interop.Excel.XlYesNoGuess.xlNo;
            orgSheet.Sort.MatchCase = true;
            orgSheet.Sort.Orientation = Microsoft.Office.Interop.Excel.XlSortOrientation.xlSortColumns;
            orgSheet.Sort.SortMethod = Microsoft.Office.Interop.Excel.XlSortMethod.xlPinYin;
            orgSheet.Sort.Apply();
        }

    }
}
