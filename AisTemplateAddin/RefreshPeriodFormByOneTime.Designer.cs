﻿namespace AisTemplateAddin
{
    partial class RefreshPeriodFormByOneTime
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        //ISSUE_NO.810 sunyi 2018/08/02 Start
        //仕様変更、ボタン作成機能を追加する
        //private void InitializeComponent()
        private void InitializeComponent(bool IsNewPeriod)
        //ISSUE_NO.810 sunyi 2018/08/02 End
        {
            this.refreshPeriodControlByOneTime = new System.Windows.Forms.Integration.ElementHost();
            //ISSUE_NO.810 sunyi 2018/08/02 Start
            //仕様変更、ボタン作成機能を追加する
            //this.RefreshPeriodByOneTime = new AisTemplateAddin.RefreshPeriodByOneTime();
            this.RefreshPeriodByOneTime = new AisTemplateAddin.RefreshPeriodByOneTime(IsNewPeriod);
            //ISSUE_NO.810 sunyi 2018/08/02 End
            this.SuspendLayout();
            // 
            // refreshPeriodControlByOneTime
            //
            this.refreshPeriodControlByOneTime.Location = new System.Drawing.Point(-1, -7);
            this.refreshPeriodControlByOneTime.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.refreshPeriodControlByOneTime.Name = "refreshPeriodControlByOneTime";
            //Refresh_Period Sunyi 2018/06/27 Start
            //仕様変更、期間再設定に時間設定を実装する
            //this.refreshPeriodControlByOneTime.Size = new System.Drawing.Size(364, 142);
            this.refreshPeriodControlByOneTime.Size = new System.Drawing.Size(462, 202);
            //Refresh_Period Sunyi 2018/06/27 End
            this.refreshPeriodControlByOneTime.TabIndex = 0;
            this.refreshPeriodControlByOneTime.Child = this.RefreshPeriodByOneTime;
            // 
            // RefreshPeriodFormByOneTime
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            //Refresh_Period Sunyi 2018/06/27 Start
            //仕様変更、期間再設定に時間設定を実装する
            //this.ClientSize = new System.Drawing.Size(362, 128);
            this.ClientSize = new System.Drawing.Size(460, 188);
            //Refresh_Period Sunyi 2018/06/27 End
            this.Controls.Add(this.refreshPeriodControlByOneTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RefreshPeriodFormByOneTime";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            //Refresh_Period Sunyi 2018/06/27 Start
            //仕様変更、期間再設定に時間設定を実装する
            //this.Text = "期間再設定V2";
            this.Text = "期間再設定(日)";
            //Refresh_Period Sunyi 2018/06/27 End
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Integration.ElementHost refreshPeriodControlByOneTime;
        private RefreshPeriodByOneTime RefreshPeriodByOneTime;
    }
}