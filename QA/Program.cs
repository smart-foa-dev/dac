﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAC.CtmData;
using System.IO;
using CsvHelper;

namespace QA
{
    class Program
    {
        static void Main(string[] args)
        {
            // Test1();
            //  Test2();
            //Test3();
            Test4();
            return;
        }

        static void Test1()
        {
            StreamReader sr = File.OpenText("test3.json");
            var jsonFile = new MissionResultReader(sr);
            var iterator = jsonFile.GetEnumerator();
            while (iterator.MoveNext())
            {
                var rec = iterator.Current;
                if (rec == null)
                {
                    Console.WriteLine("\nSwitch!");
                    var iterator1 = (MissionResultReader.CtmRecordEnumerator)iterator;
                    Console.WriteLine("id: " + iterator1.CurrentId);
                    Console.WriteLine("name: " + iterator1.CurrentName);
                    Console.WriteLine("num: " + iterator1.CurrentNum);
                    Console.WriteLine();
                }
                else
                {
                    Console.WriteLine("\nRT: " + rec.RT);
                    foreach (var el in rec.EL.Keys)
                    {
                        var val = rec.EL[el].V;
                        Console.WriteLine("el: " + el + ", val: " + val);
                    }
                }
            }
            iterator.Dispose();
        }

        static void Test2()
        {
            StreamReader sr = File.OpenText("test3.json");
            var jsonFile = new MissionResultReader(sr);
            foreach (var rec in jsonFile)
            {
                if (rec == null)
                {
                    Console.WriteLine("\nSwitch!");

                }
                else
                {
                    Console.WriteLine("\nRT: " + rec.RT);
                    foreach (var el in rec.EL.Keys)
                    {
                        var val = rec.EL[el].V;
                        Console.WriteLine("el: " + el + ", val: " + val);
                    }
                }
            }
        }

        static void Test3()
        {
            StreamReader sr = File.OpenText("test3.json");
            var jsonFile = new MissionResultReader(sr);

            var pmList = jsonFile.GetSummary();

            foreach (var pm in pmList)
            {
                Console.WriteLine("id: " + pm.id);
                Console.WriteLine("name: " + pm.name);
                Console.WriteLine("num: " + pm.num);
                Console.WriteLine();
            }
        }

        static void Test4()
        {
            StreamWriter sw = new StreamWriter("writerTest.csv", false, Encoding.UTF8);
            CsvWriter writer = new CsvWriter(sw);

            /***
             * Does not work.
            List<string> rec = new List<string>() { "boo", "foo", "woo" };
            writer.WriteRecord<List<string>>(rec);
            writer.NextRecord();
            ***/
           
        }
    }
}
