﻿using DAC.View;
using FoaCore.Common.Util;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using DAC.AExcel;
using DAC.Model.Util;
using FoaCore.Common.Net;
using System.Threading;
using System.Data;
using DAC.Util;
using DAC.View.Helpers;
using FoaCore.Common;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_ProjectStatusMonitor.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_ProjectStatusMonitor : BaseControl
    {
        private bool isFirstFile;
        private string excelFilePath;

        public bool isDragCtmName;
        public Mission currentSelectedMission;

        private List<CtmObject> ctms = new List<CtmObject>();

        // Input parameter for DAC Excel file..
        private Dictionary<string, Mission> missionDict = new Dictionary<string, Mission>();
        private Dictionary<Mission, List<CtmObject>> missionCtmDict = new Dictionary<Mission, List<CtmObject>>();
        private Dictionary<CtmObject, List<CtmElement>> missionCtmElementDict = new Dictionary<CtmObject, List<CtmElement>>();

        private Mission forCtmDirect;

        private string missionId = string.Empty;

        public UserControl_ProjectStatusMonitor(bool isFirstFile = true, string filePath = "")
        {
            InitializeComponent();

            this.Mode = ControlMode.ProjectStatusMonitor;

            setKeyValuePair();
            setComboBoxItem();

            this.TEMPLATE = "ProjectStatusMonitor.xlsm";

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }

            this.forCtmDirect = new Mission();
            this.forCtmDirect.Id = "ForCtmDirect";
        }

        #region Read Excel File

        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet paramSheet = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);

                if (paramSheet != null)
                {
                    Dictionary<string, string> paramMap = readParam(paramSheet);
                    if (paramMap.ContainsKey("更新周期"))
                    {
                        this.comboBox_Interval.Text = paramMap["更新周期"];
                    }
                    if (paramMap.ContainsKey("取得期間"))
                    {
                        this.comboBox_GetPeriod.Text = paramMap["取得期間"];
                    }

                    if (paramMap.ContainsKey("収集タイプ") &&
                        !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                    {
                        switch (paramMap["収集タイプ"])
                        {
                            case "CTM":
                                this.DataSource = DataSourceType.CTM_DIRECT;
                                break;
                            case "ミッション":
                                this.DataSource = DataSourceType.MISSION;
                                break;
                            case "GRIP":
                                this.DataSource = DataSourceType.GRIP;
                                break;
                            default:
                                break;
                        }
                    }

                    // set SelectedMission
                    if (paramMap.ContainsKey("ミッションID") &&
                        !string.IsNullOrEmpty(paramMap["ミッションID"]))
                    {
                        if (this.DataSource == DataSourceType.MISSION)
                        {
                            string missionId = paramMap["ミッションID"];
                            Mission mission = AisUtil.GetMissionFromId(missionId);
                            this.SelectedMission = mission;
                        }
                        else if (this.DataSource == DataSourceType.GRIP)
                        {
                            string missionId = paramMap["ミッションID"];
                            GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                            this.SelectedGripMission = mission;
                        }
                    }

                    string missionSheetName = "ミッション";
                    SpreadsheetGear.IWorksheet missionSheet = AisUtil.GetSheetFromSheetName_SSG(book, missionSheetName);
                    if (missionSheet != null)
                    {
                        readMissionStructure(missionSheet);
                    }
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private void readMissionStructure(SpreadsheetGear.IWorksheet missionSheet)
        {
            SpreadsheetGear.IRange paramCells = missionSheet.Cells;

            // Currently used cells
            SpreadsheetGear.IRange usedCells = missionSheet.UsedRange.Cells;

            List<MissionViewObject> missionViewListSource = new List<MissionViewObject>();
            List<MissionCtmViewObject> ctmViewListSource = new List<MissionCtmViewObject>();
            List<MissionCtmElementViewObject> ctmElementViewListSource = new List<MissionCtmElementViewObject>();

            for (int i = 0; i < usedCells.Rows.RowCount; i++)
            {
                // Mission-ID
                if (paramCells[i, 0].Value != null)
                {
                    string missionId = paramCells[i, 0].Value.ToString();
                    if (string.IsNullOrEmpty(this.missionId))
                    {
                        this.missionId = missionId;
                        if (!setMission())
                        {
                            return;
                        }
                    }

                    JToken token = this.mainWindow.treeView_Mission_CTM.GetItem(missionId).DataContext as JToken;
                    Mission missionObj = JsonConvert.DeserializeObject<Mission>(token.ToString());
                    missionDict.Add(missionId, missionObj);

                    missionViewListSource.Add(new MissionViewObject()
                    {
                        MissionDisplayName = missionObj.Name,
                        MissionId = missionId,
                        Json = JsonConvert.SerializeObject(missionObj)
                    });
                }

                // CTM-ID
                if (paramCells[i, 3].Value != null)
                {
                    string parentMissionId = string.Empty;
                    if (paramCells[i, 2].Value != null)
                    {
                        parentMissionId = paramCells[i, 2].Value.ToString();
                    }
                    string ctmId = paramCells[i, 3].Value.ToString();
                    string ctmName = paramCells[i, 4].Value.ToString();

                    ctmViewListSource.Add(new MissionCtmViewObject()
                    {
                        CtmDisplayName = ctmName,
                        CtmId = ctmId,
                        ParentMissionId = parentMissionId
                    });
                }

                // Element-ID
                if (paramCells[i, 6].Value != null)
                {
                    string parentCtmId = paramCells[i, 5].Value.ToString();
                    string elementId = paramCells[i, 6].Value.ToString();
                    string elementName = paramCells[i, 7].Value.ToString();
                    string parentMissionId = string.Empty;
                    if (paramCells[i, 8].Value != null)
                    {
                        parentMissionId = paramCells[i, 8].Value.ToString();
                    }

                    ctmElementViewListSource.Add(new MissionCtmElementViewObject()
                    {
                        ElementDisplayName = elementName,
                        ElementId = elementId,
                        ParentCtmId = parentCtmId,
                        ParentMissionId = parentMissionId
                    });
                }
            }

            // Set the ListBox ItemsSource
            setMissionListBoxItemsSource(null, missionViewListSource);
            setCtmListBoxItemsSource(null, ctmViewListSource);
            setElementListBoxItemsSource(null, ctmElementViewListSource);

            // Get the CTM Object, and assign it  to the related dictionary
            List<string> ctmIdList = new List<string>();
            foreach (MissionCtmViewObject ctmViewObj in ctmViewListSource)
            {
                ctmIdList.Add(ctmViewObj.CtmId);
            }

            if (ctmIdList.Count > 0)
            {
                if (string.IsNullOrEmpty(this.missionId))
                {
                    setCtm(ctmIdList);
                }

                // 個別に指定する場合は、既存の設定をサーバから再取得
                CmsHttpClient ctmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                foreach (string ctmId in ctmIdList)
                {
                    ctmGetClient.AddParam("id", ctmId);
                }

                ctmGetClient.CompleteHandler += ctmJson =>
                {
                    List<CtmObject> ctmList = JsonConvert.DeserializeObject<List<CtmObject>>(ctmJson);

                    // Assign mission CTM Dictionary
                    foreach (MissionCtmViewObject ctmViewObj in ctmViewListSource)
                    {
                        Mission missionObj = null;
                        if (missionDict.TryGetValue(ctmViewObj.ParentMissionId, out missionObj))
                        {
                            CtmObject ctmObj = ctmList.Where(x => x.id.ToString() == ctmViewObj.CtmId).FirstOrDefault();

                            List<CtmObject> ctmObjList = new List<CtmObject>();
                            if (missionCtmDict.TryGetValue(missionObj, out ctmObjList))
                            {
                                if (ctmObjList.Contains(ctmObj)) return;

                                ctmObjList.Add(ctmObj);
                                missionCtmDict[missionObj] = ctmObjList;
                            }
                            else
                            {
                                missionCtmDict.Add(missionObj, new List<CtmObject>() { ctmObj });
                            }
                        }
                    }

                    // Assign mission CTM Element Dictionary
                    foreach (MissionCtmElementViewObject ctmElementViewObj in ctmElementViewListSource)
                    {
                        CtmObject ctmObj = ctmList.Where(x => x.id.ToString() == ctmElementViewObj.ParentCtmId).FirstOrDefault();
                        if (ctmObj == null) return;

                        CtmElement elementObj = ctmObj.GetElement(new GUID(ctmElementViewObj.ElementId));
                        if (elementObj != null)
                        {
                            List<CtmElement> ctmElementObjList = new List<CtmElement>();
                            if (missionCtmElementDict.TryGetValue(ctmObj, out ctmElementObjList))
                            {
                                if (ctmElementObjList.Contains(elementObj)) return;

                                ctmElementObjList.Add(elementObj);
                                missionCtmElementDict[ctmObj] = ctmElementObjList;
                            }
                            else
                            {
                                missionCtmElementDict.Add(ctmObj, new List<CtmElement>() { elementObj });
                            }
                        }
                    }
                };
                ctmGetClient.Get(CmsUrlMms.Ctm.List());
            }
        }

        private Dictionary<string, string> readParam(SpreadsheetGear.IWorksheet worksheetCondition)
        {
            var paramMap = new Dictionary<string, string>();

            SpreadsheetGear.IRange paramCells = worksheetCondition.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // 更新周期
                if (paramName == "更新周期")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }

                // 取得期間
                if (paramName == "取得期間")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }
            }

            return paramMap;
        }


        public override void SetMission2()
        {
            if (!this.isFirstFile)
            {
                readExcelFile(this.excelFilePath);
            }
        }

        private bool setMission()
        {
            if (string.IsNullOrEmpty(this.missionId))
            {
                return false;
            }

            this.IsSelectedProgramMission = true;

            // Mission Tree
            TreeViewItem item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(this.missionId);
            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                }
                return false;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_CTM.Focus();
            item.Focus();

            return true;
        }

        override public void SetCtm2()
        {
            if (!this.isFirstFile)
            {
                readExcelFile(this.excelFilePath);
            }
        }

        private void setCtm(List<string> ctmIdList)
        {
            try
            {
                TreeViewItem item = null;
                if (ctmIdList.Count < 1)
                {
                    return;
                }

                this.mainWindow.TabCtrlDS.SelectedIndex = 0;
                foreach (string ctmId in ctmIdList)
                {
                    item = this.mainWindow.treeView_Catalog.GetItem(ctmId);
                    System.Windows.Controls.CheckBox cb = this.mainWindow.treeView_Catalog.GetCheckBox(item);
                    cb.IsChecked = true;
                }

                if (item == null)
                {
                    return;
                }
                item.IsSelected = true;
                List<string> list = this.mainWindow.treeView_Catalog.GetCheckedId(true);
            }
            catch (Exception ex)
            {
                string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                AisMessageBox.DisplayErrorMessageBox(message);
            }
        }

        #endregion

        private void setAllListBoxItemsSourceDefaultView()
        {
            setMissionListBoxItemsSource(this.missionDict);
            setCtmListBoxItemsSource(this.missionCtmDict);
            setElementListBoxItemsSource(this.missionCtmElementDict);
        }

        private void setMissionListBoxItemsSource(Dictionary<string, Mission> missionDict = null, List<MissionViewObject> missionViewListSource = null)
        {
            if (missionViewListSource != null)
            {
                lbMission.ItemsSource = missionViewListSource;
                lbMission.DisplayMemberPath = "MissionDisplayName";
                return;
            }

            List<MissionViewObject> missionViewList = new List<MissionViewObject>();
            foreach (KeyValuePair<string, Mission> kvp in missionDict)
            {
                missionViewList.Add(new MissionViewObject()
                {
                    MissionDisplayName = kvp.Value.Name,
                    MissionId = kvp.Key,
                    Json = JsonConvert.SerializeObject(kvp.Value)
                });
            }
            lbMission.ItemsSource = missionViewList;
            lbMission.DisplayMemberPath = "MissionDisplayName";
        }
        private void setMissionListBoxItemsSource(Dictionary<string, Mission> missionDict)
        {
            List<MissionViewObject> missionViewList = new List<MissionViewObject>();
            foreach (KeyValuePair<string, Mission> kvp in missionDict)
            {
                missionViewList.Add(new MissionViewObject()
                {
                    MissionDisplayName = kvp.Value.Name,
                    MissionId = kvp.Key,
                    Json = JsonConvert.SerializeObject(kvp.Value)
                });
            }
            lbMission.ItemsSource = missionViewList;
            lbMission.DisplayMemberPath = "MissionDisplayName";
        }

        private void setCtmListBoxItemsSource(Dictionary<Mission, List<CtmObject>> missionCtmDict = null, List<MissionCtmViewObject> ctmViewListSource = null)
        {
            if (ctmViewListSource != null)
            {
                lbCtm.ItemsSource = ctmViewListSource;
                lbCtm.DisplayMemberPath = "CtmDisplayName";
                return;
            }

            List<MissionCtmViewObject> ctmViewList = new List<MissionCtmViewObject>();
            foreach (KeyValuePair<Mission, List<CtmObject>> kvp in missionCtmDict)
            {
                foreach (CtmObject ctm in kvp.Value)
                {
                    ctmViewList.Add(new MissionCtmViewObject()
                    {
                        CtmDisplayName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                        CtmId = ctm.id.ToString(),
                        ParentMissionId = kvp.Key.Id
                    });
                }
            }
            lbCtm.ItemsSource = ctmViewList;
            lbCtm.DisplayMemberPath = "CtmDisplayName";
        }
        private void setCtmListBoxItemsSource(Dictionary<Mission, List<CtmObject>> missionCtmDict)
        {
            List<MissionCtmViewObject> ctmViewList = new List<MissionCtmViewObject>();
            foreach (KeyValuePair<Mission, List<CtmObject>> kvp in missionCtmDict)
            {
                foreach (CtmObject ctm in kvp.Value)
                {
                    ctmViewList.Add(new MissionCtmViewObject()
                    {
                        CtmDisplayName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                        CtmId = ctm.id.ToString(),
                        ParentMissionId = kvp.Key.Id
                    });
                }
            }
            lbCtm.ItemsSource = ctmViewList;
            lbCtm.DisplayMemberPath = "CtmDisplayName";
        }
        private void setCtmListBoxItemsSource(List<CtmObject> listCtmObj, string parentMissionId)
        {
            List<MissionCtmViewObject> ctmViewList = new List<MissionCtmViewObject>();
            foreach (CtmObject ctm in listCtmObj)
            {
                ctmViewList.Add(new MissionCtmViewObject()
                {
                    CtmDisplayName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                    CtmId = ctm.id.ToString(),
                    ParentMissionId = parentMissionId
                });
            }
            lbCtm.ItemsSource = ctmViewList;
            lbCtm.DisplayMemberPath = "CtmDisplayName";
        }

        private void setElementListBoxItemsSource(Dictionary<CtmObject, List<CtmElement>> missionCtmElementDict = null, List<MissionCtmElementViewObject> ctmElementViewListSource = null)
        {
            if (ctmElementViewListSource != null)
            {
                lbElement.ItemsSource = ctmElementViewListSource;
                lbElement.DisplayMemberPath = "ElementDisplayName";
                return;
            }

            List<MissionCtmElementViewObject> ctmElementViewList = new List<MissionCtmElementViewObject>();
            foreach (KeyValuePair<CtmObject, List<CtmElement>> kvp in missionCtmElementDict)
            {
                string ctmId = kvp.Key.id.ToString();
                foreach (CtmElement ele in kvp.Value)
                {
                    // Get ParentMissionId
                    string parentMissionId = null;
                    foreach (KeyValuePair<Mission, List<CtmObject>> kvp2 in missionCtmDict)
                    {
                        bool isParentMissionFound = false;
                        foreach (CtmObject ctm in kvp2.Value)
                        {
                            if (ctm.id.ToString() == ctmId)
                            {
                                parentMissionId = kvp2.Key.Id;
                                isParentMissionFound = true;
                                break;
                            }
                        }
                        if (isParentMissionFound) break;
                    }

                    ctmElementViewList.Add(new MissionCtmElementViewObject()
                    {
                        ElementDisplayName = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                        ElementId = ele.id.ToString(),
                        ParentCtmId = ctmId,
                        ParentMissionId = parentMissionId
                    });
                }
            }
            lbElement.ItemsSource = ctmElementViewList;
            lbElement.DisplayMemberPath = "ElementDisplayName";
        }

        private void setElementListBoxItemsSource(List<CtmElement> missionCtmElementList, string parentCtmId, string parentMissionId)
        {
            List<MissionCtmElementViewObject> ctmElementViewList = new List<MissionCtmElementViewObject>();
            foreach (CtmElement ele in missionCtmElementList)
            {
                ctmElementViewList.Add(new MissionCtmElementViewObject()
                {
                    ElementDisplayName = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                    ElementId = ele.id.ToString(),
                    ParentCtmId = parentCtmId,
                    ParentMissionId = parentMissionId
                });
            }
            lbElement.ItemsSource = ctmElementViewList;
            lbElement.DisplayMemberPath = "ElementDisplayName";
        }

        private void setAllListBoxItemsSource(string[] itemArray, bool isCtmDrop)
        {
            string droppedCtmId = itemArray[1];
            CtmObject droppedCtmObj = setCtmObject(droppedCtmId);

            List<CtmObject> listCtmObj = missionCtmElementDict.Keys.ToList();

            if (this.DataSource == DataSourceType.MISSION)      // Mission
            {
                // 1. Re-assign the Mission ListBoxItemsSource
                InsertMissionToListBox(this.currentSelectedMission);
                Mission currentMission = this.currentSelectedMission;
                listCtmObj = missionCtmDict.Where(x => x.Key.Id == currentMission.Id).FirstOrDefault().Value;

                // 2. Check if the dropped CTM object has been added or not.
                if (listCtmObj.Count > 0)
                {
                    bool ctmIsAlreadyExist = false;
                    foreach (CtmObject ctm in listCtmObj)
                    {
                        if (ctm.id.ToString() == droppedCtmId)
                        {
                            ctmIsAlreadyExist = true;
                            break;
                        }
                    }
                    if (!ctmIsAlreadyExist)
                    {
                        listCtmObj.Add(droppedCtmObj);
                    }
                }
                else
                {
                    listCtmObj.Add(droppedCtmObj);
                }

                // 3. Update the CTM Dictionary for that Mission
                missionCtmDict[missionCtmDict.Where(x => x.Key.Id == currentMission.Id).FirstOrDefault().Key] = listCtmObj;

                // 4. Set the CTM ListBox ItemsSource
                setCtmListBoxItemsSource(missionCtmDict);
            }
            else if (this.DataSource == DataSourceType.CTM_DIRECT)  // CTM Direct
            {
                if (listCtmObj.Count > 0)
                {
                    bool ctmIsAlreadyExist = false;
                    foreach (CtmObject ctm in listCtmObj)
                    {
                        if (ctm.id.ToString() == droppedCtmId)
                        {
                            ctmIsAlreadyExist = true;
                            break;
                        }
                    }
                    if (!ctmIsAlreadyExist)
                    {
                        listCtmObj.Add(droppedCtmObj);
                    }
                }
                else
                {
                    listCtmObj.Add(droppedCtmObj);
                }

                List<CtmObject> ctmObjList = new List<CtmObject>();
                if (!this.missionCtmDict.ContainsKey(this.forCtmDirect))
                {
                    missionCtmDict.Add(this.forCtmDirect, ctmObjList);
                }

                missionCtmDict[this.forCtmDirect] = listCtmObj;
                setCtmListBoxItemsSource(listCtmObj, this.forCtmDirect.Id);
            }

            // 5. Check the CTM Element Dictionary & Get the CTM Element data
            List<CtmElement> ctmElementList = null;
            List<string[]> argsData = convertItemArrayToElementData(itemArray);

            // 6. Re-initialize the droppedCtmObj from the list..
            foreach (CtmObject ctm in listCtmObj)
            {
                if (ctm.id.ToString() == droppedCtmId)
                {
                    droppedCtmObj = ctm;
                    break;
                }
            }

            if (missionCtmElementDict.TryGetValue(droppedCtmObj, out ctmElementList) == true)
            {
                foreach (string[] elementData in argsData)
                {
                    CtmElement ele = droppedCtmObj.GetElement(new GUID(elementData[1]));
                    if (!ctmElementList.Contains(ele))
                    {
                        ctmElementList.Add(ele);
                    }
                }
                missionCtmElementDict[droppedCtmObj] = ctmElementList;
            }
            else
            {
                ctmElementList = new List<CtmElement>();
                if (!isCtmDrop) // Element Drop
                {
                    foreach (string[] elementData in argsData)
                    {
                        CtmElement ele = droppedCtmObj.GetElement(new GUID(elementData[1]));
                        ctmElementList.Add(ele);
                    }
                }
                else // CTM Drop
                {
                    foreach (CtmElement ele in droppedCtmObj.GetAllElements())
                    {
                        bool hit = false;
                        foreach (var arg in argsData)
                        {
                            if (ele.id.ToString() == arg[1])
                            {
                                hit = true;
                                break;
                            }
                        }
                        if (hit)
                        {
                            ctmElementList.Add(ele);
                        }
                    }
                }
                missionCtmElementDict.Add(droppedCtmObj, ctmElementList);
            }

            // 7. Set the CTM Element ListBox ItemsSource 
            setElementListBoxItemsSource(missionCtmElementDict);
        }

        public void InsertMissionToListBox(Mission mission)
        {
            Mission missionObj = null;
            if (!this.missionDict.TryGetValue(mission.Id, out missionObj))
            {
                this.missionDict[mission.Id] = mission;
                List<CtmObject> ctmObjList = new List<CtmObject>();
                if (!this.missionCtmDict.TryGetValue(mission, out ctmObjList))
                {
                    this.missionCtmDict.Add(mission, new List<CtmObject>());
                }
            }

            setMissionListBoxItemsSource(this.missionDict);
        }

        private CtmObject setCtmObject(string ctmId)
        {
            CtmObject ctmObj = null;
            if (this.editControl.Ctms != null)
            {
                this.ctms = this.editControl.Ctms;
            }

            foreach (CtmObject ctm in ctms)
            {
                if (ctm.id.ToString() == ctmId)
                {
                    ctmObj = ctm;
                    break;
                }
            }

            return ctmObj;
        }

        private List<string[]> convertItemArrayToElementData(string[] itemArray)
        {
            List<string[]> data = new List<string[]>();

            for (int i = 2; i < itemArray.Length; i += 3)
            {
                string[] ary = new string[]
                {
                    itemArray[i],
                    itemArray[i + 1],
                    itemArray[i + 2]
                };
                data.Add(ary);
            }

            return data;
        }

        // True: Remove CTM & False: Remove Element
        private void removeListBoxItem(bool isCtmItem, object sender)
        {
            if (isCtmItem) // Remove ctm
            {
                MissionCtmViewObject ctmViewObj = lbCtm.Items[lbCtm.SelectedIndex] as MissionCtmViewObject;
                if (ctmViewObj == null) return;

                // 1. Remove CTM from the dictionary
                // **** BEGIN ****
                List<CtmObject> listCtmObj = missionCtmDict.Where
                (x => x.Key.Id == ctmViewObj.ParentMissionId).FirstOrDefault().Value;

                // Get the 'to be removed CtmObject'
                CtmObject ctmObj = listCtmObj.Where(x => x.id.ToString() == ctmViewObj.CtmId).FirstOrDefault();

                listCtmObj.Remove(ctmObj); // Remove from the dict

                if (listCtmObj.Count <= 0)
                {
                    // Remove the Mission object from the dict
                    // **** BEGIN ****
                    this.missionDict.Remove(ctmViewObj.ParentMissionId);
                    // **** END ****

                    // Re-assign the Mission ListBox ItemsSource
                    setMissionListBoxItemsSource(this.missionDict);
                }

                missionCtmDict[missionCtmDict.Where(
                    x => x.Key.Id == ctmViewObj.ParentMissionId).FirstOrDefault().Key] = listCtmObj; // Update the dict for this mission
                // **** END ****

                // 2. Re-assign the CTM ListBox ItemsSource
                setCtmListBoxItemsSource(missionCtmDict);

                // 3. Remove related element from the Dictionary
                missionCtmElementDict.Remove(ctmObj);

                // 4. Re-assign the Element ListBox ItemsSource
                setElementListBoxItemsSource(missionCtmElementDict);
            }
            else // Remove element
            {
                MissionCtmElementViewObject eleViewObj = lbElement.Items[lbElement.SelectedIndex] as MissionCtmElementViewObject;
                if (eleViewObj == null) return;

                // 1. Get the parent CTM object
                CtmObject ctmObj = null;
                List<CtmObject> ctmMissionList = missionCtmDict.Where(x => x.Key.Id == eleViewObj.ParentMissionId).FirstOrDefault().Value;
                foreach (CtmObject ctm in ctmMissionList)
                {
                    if (ctm.id.ToString() == eleViewObj.ParentCtmId)
                    {
                        ctmObj = ctm;
                        break;
                    }
                }
                //CtmObject ctmObj = missionCtmElementDict.Where
                //(x => x.Key.id.ToString() == eleViewObj.parentCtmId).FirstOrDefault().Key;

                // 2. Get the List of related element for the Parent CTM object
                List<CtmElement> ctmElementList = null;
                if (missionCtmElementDict.TryGetValue(ctmObj, out ctmElementList) == true)
                {
                    CtmElement toBeRemovedElement = null;
                    foreach (CtmElement ele in ctmElementList)
                    {
                        if (ele.id.ToString() == eleViewObj.ElementId)
                        {
                            toBeRemovedElement = ele;
                            break;
                        }
                    }
                    if (toBeRemovedElement == null) // there is might another duplicated CTM exists, so the toBeRemovedElement is 'NULL'
                    {
                        foreach (KeyValuePair<CtmObject, List<CtmElement>> kvp in missionCtmElementDict)
                        {
                            if (kvp.Key.id.ToString() == eleViewObj.ParentCtmId)
                            {
                                foreach (CtmElement ele in kvp.Value)
                                {
                                    if (ele.id.ToString() == eleViewObj.ElementId)
                                    {
                                        toBeRemovedElement = ele;
                                        break;
                                    }
                                }
                                if (toBeRemovedElement != null)
                                {
                                    ctmObj = kvp.Key;
                                    ctmElementList = kvp.Value;
                                    if (ctmElementList.Remove(toBeRemovedElement)) break;
                                }
                            }
                        }
                    }
                    else // the toBeRemovedElement is found, proceed to delete from the list..
                    {
                        ctmElementList.Remove(toBeRemovedElement);
                    }

                    if (ctmElementList.Count <= 0) // is empty, remove the CTM object also
                    {
                        // Remove the CTM object from the dict
                        // **** BEGIN ****
                        Mission parentMission = missionCtmDict.Where(x => x.Key.Id == eleViewObj.ParentMissionId).FirstOrDefault().Key;
                        CtmObject toBeRemovedCtm = null;
                        foreach (CtmObject ctm in ctmMissionList)
                        {
                            if (ctm.id.ToString() == ctmObj.id.ToString())
                            {
                                toBeRemovedCtm = ctm;
                                break;
                            }
                        }
                        ctmMissionList.Remove(toBeRemovedCtm); // Remove from the dict

                        if (ctmMissionList.Count <= 0)
                        {
                            // Remove the Mission object from the dict
                            // **** BEGIN ****
                            this.missionDict.Remove(eleViewObj.ParentMissionId);
                            // **** END ****

                            // Re-assign the Mission ListBox ItemsSource
                            setMissionListBoxItemsSource(this.missionDict);
                        }

                        missionCtmDict[parentMission] = ctmMissionList; // Update the dict for this mission
                        // **** END ****

                        // Re-assign the CTM ListBox ItemsSource
                        setCtmListBoxItemsSource(missionCtmDict);
                    }
                    else // there are still left-over elements
                    {
                        missionCtmElementDict[ctmObj] = ctmElementList; // Update the related ctm's List of element dict
                    }
                }

                // 3. Re-assign the Element ListBox ItemsSource
                setElementListBoxItemsSource(missionCtmElementDict);
            }
        }

        private string[] getItemArrayFromDropEvent(object sender, DragEventArgs e)
        {
            var senderObj = sender as System.Windows.Controls.ListBox;
            if (senderObj == null)
            {
                return null;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return null;
            }

            string items = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = items.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 5)
            {
                return null;
            }

            /*
             * 0 CTM名
             * 1 CTMID
             * ----------------
             * 2 エレメント名
             * 3 エレメントID
             * 4 エレメントの型
             * ...
             * ...
             * ...
            */

            return itemArray;
        }

        private void removeMissionListBoxItem()
        {
            MissionViewObject missionViewObj = lbMission.Items[lbMission.SelectedIndex] as MissionViewObject;
            if (missionViewObj == null)
            {
                return;
            }

            // 1. Get the to be removed Mission object
            Mission toBeRemovedMission = missionDict[missionViewObj.MissionId];

            // 2. Get the current selected CtmObject for that mission
            List<CtmObject> listCtmObj = missionCtmDict[toBeRemovedMission];

            // 3. If the ctmObject is exist, continue removing the related element
            foreach (CtmObject ctm in listCtmObj)
            {
                // 4. Remove related element from the Dictionary
                missionCtmElementDict.Remove(ctm);
            }

            // 5. Remove the Ctm from Dictionary
            missionCtmDict.Remove(toBeRemovedMission);

            // 6. Remove the mission from dictionary
            missionDict.Remove(missionViewObj.MissionId);

            // 7. Re-assign the Element ListBox ItemsSource
            setElementListBoxItemsSource(missionCtmElementDict);

            // 8. Re-assign the CTM ListBox ItemsSource
            setCtmListBoxItemsSource(missionCtmDict);

            // 9. Re-assign the Mission ListBox ItemsSource
            List<MissionViewObject> missionViewList = new List<MissionViewObject>();
            foreach (KeyValuePair<string, Mission> kvp in missionDict)
            {
                missionViewList.Add(new MissionViewObject()
                {
                    MissionDisplayName = kvp.Value.Name,
                    MissionId = kvp.Key,
                    Json = JsonConvert.SerializeObject(kvp.Value)
                });
            }
            lbMission.ItemsSource = missionViewList;
            lbMission.DisplayMemberPath = "MissionDisplayName";
        }

        private static bool isTextAllowed(string text)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        public override void DataSourceSelectionChanged(string removingCtmName)
        {
            for (int i = 0; i < this.lbCtm.Items.Count; i++)
            {
                var item = this.lbCtm.Items[i] as MissionCtmViewObject;
                if (item == null)
                {
                    continue;
                }

                if (item.CtmDisplayName != removingCtmName)
                {
                    continue;
                }

                this.lbCtm.SelectedIndex = i;
                removeListBoxItem(true, new object());
                setAllListBoxItemsSourceDefaultView();
                break;
            }
        }

        public override void InitializeDataControl()
        {
            for (int i = 0; i < this.lbCtm.Items.Count; i++)
            {
                this.lbCtm.SelectedIndex = i;
                removeListBoxItem(true, new object());
                setAllListBoxItemsSourceDefaultView();
            }

            for (int i = 0; i < this.lbMission.Items.Count; i++)
            {
                this.lbMission.SelectedIndex = i;
                removeMissionListBoxItem();
                setAllListBoxItemsSourceDefaultView();
            }
        }

        #region Event Handler

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();
            this.editControl.ProjectStatusMonitor = this;

            //言語が英語の場合の表示調整
            string cultureName = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;
            if (cultureName == "en-US")
            {
                this.Border.Width = 212;
                this.label_Interval.Width = 120;
                this.label_GetPeriod.Width = 120;
            }

            // No.549 Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;
        }

        private async void BtnOpenExcel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.missionCtmElementDict.Count < 1)
            {
                FoaMessageBox.ShowError("AIS_E_007");
                return;
            }

            var workFile = createWorkingFile();

            // get mission result
            DateTime end = DateTime.Now;
            float periodHour = 0;
            if (!float.TryParse(this.comboBox_GetPeriod.Text.Trim(), out periodHour))
            {
                FoaMessageBox.ShowError("AIS_E_033", Properties.Resources.TEXT_COLLECTIONPERIOD);
                return;
            }

            // 更新周期の入力チェック
            float periodHour2 = 0;
            if (!float.TryParse(this.comboBox_Interval.Text.Trim(), out periodHour2))
            {
                FoaMessageBox.ShowError("AIS_E_033", Properties.Resources.TEXT_UPDATINGCYCLE);
                return;
            }          

            TimeSpan period = TimeSpan.FromHours(periodHour);
            DateTime start = end - period;

            string resultRootFolder = string.Empty;
            if (this.DataSource == DataSourceType.MISSION)
            {
                string path = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission");
                if (Directory.Exists(path))
                {
                    FoaCore.Util.FileUtil.clearFolder(path);
                }

                string[] missionIds = this.missionDict.Keys.ToArray();
                string resultFolder = await RetrieveCtmDataFromMissionIds(start, end, missionIds);
                DirectoryInfo di = new DirectoryInfo(resultFolder);
                resultRootFolder = di.Parent.FullName;
            }
            else
            {
                this.DownloadCts = new CancellationTokenSource();
                resultRootFolder = await RetrieveCtmData(start, end, this.DownloadCts);
            }


            this.tmpResultFolderPath = resultRootFolder;

            excelOpen(workFile, start, end, resultRootFolder, periodHour);
        }

        private string createWorkingFile()
        {
            // 発行したファイルを使用しない
            string filePathSrc = isFirstFile ? System.Windows.Forms.Application.StartupPath + @"\template\ProjectStatusMonitorTemplate.xlsm" : this.excelFilePath;

            string fileName = string.Format("{0}_{1}.xlsm", "ProjectStatusMonitor", DateTime.Now.ToString("yyyyMMddHHmmss"));
            var filePathDest = System.IO.Path.Combine(resultDir, fileName);
            File.Copy(filePathSrc, filePathDest);
            return filePathDest;
        }

        private void excelOpen(string workfile, DateTime start, DateTime end, string resuloFolderPath, float periodHour)
        {
            this.Bw = new System.ComponentModel.BackgroundWorker();
            this.Bw.WorkerSupportsCancellation = true;

            string excelName = workfile;

            var writer = new InteropExcelWriterProjectStatusMonitor(this.Mode, this.DataSource, this.editControl.Ctms, null, this.Bw, null, this.missionCtmDict, this.missionCtmElementDict);

            var configParams = new Dictionary<ExcelConfigParam, object>();

            DateTime start1;
            DateTime end1;
            bool gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out start1, out end1);
            if (!gotTime)
            {
                return;
            }

            configParams.Add(ExcelConfigParam.START, start);
            configParams.Add(ExcelConfigParam.END, end);
            configParams.Add(ExcelConfigParam.RELOAD_INTERVAL, int.Parse(this.comboBox_Interval.Text));
            configParams.Add(ExcelConfigParam.GET_PERIOD, periodHour);

            if (this.DataSource == DataSourceType.MISSION)
            {
                configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                configParams.Add(ExcelConfigParam.MISSION_NAME, this.SelectedMission.Name);
            }
            else if (this.DataSource == DataSourceType.GRIP)
            {
                configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                configParams.Add(ExcelConfigParam.MISSION_NAME, this.SelectedGripMission.Name);
            }

            if (!isFirstFile)
            {
                configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(this.excelFilePath));
                configParams.Add(ExcelConfigParam.EDIT_MODE, Properties.Resources.TEXT_TEMPLATE_PARAM_EDITMODE);  //BugNo.546 Added for display bug. paramに編集モード項目追加.
            }

            writer.WriteCtmData(excelName, resuloFolderPath, configParams);
        }

        private void lbMission_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbMission.SelectedIndex == -1)
            {
                setAllListBoxItemsSourceDefaultView();
                return;
            }

            MissionViewObject missionViewObj = lbMission.Items[lbMission.SelectedIndex] as MissionViewObject;
            if (missionViewObj == null)
            {
                return;
            }

            // Set the CTM & Element DataGrid
            this.editControl.SetElementDataToDataGrid(missionViewObj.MissionId);

            // Set currentSelectedMission object
            this.currentSelectedMission = JsonConvert.DeserializeObject<Mission>(missionViewObj.Json);

            // Clear the selection on the TreeView
            if (mainWindow.CtrlMt.tree.SelectedItem != null)
            {
                mainWindow.CtrlMt.tree.ClearSelection();
            }

            // Set the CTM & Element ListBox ItemsSource for that selectedMission
            // 1. Selected CTM
            List<CtmObject> listSelectedCtm = missionCtmDict.Where(x => x.Key.Id == currentSelectedMission.Id).FirstOrDefault().Value;
            setCtmListBoxItemsSource(listSelectedCtm, currentSelectedMission.Id);

            // 2. Selected Element
            Dictionary<CtmObject, List<CtmElement>> selectedCtmElementDict = new Dictionary<CtmObject, List<CtmElement>>();
            foreach (CtmObject ctm in listSelectedCtm)
            {
                List<CtmElement> listSelectedElement = new List<CtmElement>();
                foreach (CtmElement ele in missionCtmElementDict.Where(x => x.Key.id.ToString() == ctm.id.ToString()).FirstOrDefault().Value)
                {
                    if (!listSelectedElement.Contains(ele))
                    {
                        listSelectedElement.Add(ele);
                    }
                }
                selectedCtmElementDict[ctm] = listSelectedElement;
            }
            setElementListBoxItemsSource(selectedCtmElementDict);
        }

        private void lbCtm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (lbCtm.SelectedIndex == -1)
            {
                if (lbMission.SelectedIndex == -1)
                {
                    setAllListBoxItemsSourceDefaultView();
                }
                else // A mission is currently selected in the MissionListBox
                {
                    MissionViewObject missionViewObj = lbMission.Items[lbMission.SelectedIndex] as MissionViewObject;
                    if (missionViewObj == null)
                    {
                        return;
                    }

                    List<CtmObject> listSelectedCtm = missionCtmDict.Where(x => x.Key.Id == missionViewObj.MissionId).FirstOrDefault().Value;
                    setCtmListBoxItemsSource(listSelectedCtm, missionViewObj.MissionId);

                    Dictionary<CtmObject, List<CtmElement>> selectedCtmElementDict = new Dictionary<CtmObject, List<CtmElement>>();
                    foreach (CtmObject ctm in listSelectedCtm)
                    {
                        List<CtmElement> listSelectedElement = new List<CtmElement>();
                        foreach (CtmElement ele in missionCtmElementDict.Where(x => x.Key.id.ToString() == ctm.id.ToString()).FirstOrDefault().Value)
                        {
                            if (!listSelectedElement.Contains(ele))
                            {
                                listSelectedElement.Add(ele);
                            }
                        }
                        selectedCtmElementDict[ctm] = listSelectedElement;
                    }
                    setElementListBoxItemsSource(selectedCtmElementDict);
                }

                return;
            }

            MissionCtmViewObject missionCtmViewObj = lbCtm.Items[lbCtm.SelectedIndex] as MissionCtmViewObject;
            if (missionCtmViewObj == null)
            {
                return;
            }

            List<CtmElement> selectedCtmElementList = missionCtmElementDict.Where(x => x.Key.id.ToString() == missionCtmViewObj.CtmId).FirstOrDefault().Value;
            if (selectedCtmElementList != null)
            {
                setElementListBoxItemsSource(selectedCtmElementList, missionCtmViewObj.CtmId, missionCtmViewObj.ParentMissionId);
            }
            else
            {
                this.lbCtm.SelectedIndex = -1;
            }
        }

        private void lbCtm_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lbCtm.SelectedIndex = -1; // clear CtmListBox selection
        }

        private void MenuRemoveCtm_Click(object sender, RoutedEventArgs e)
        {
            if (lbCtm.SelectedIndex == -1)
            {
                return;
            }

            removeListBoxItem(true, sender);
            setAllListBoxItemsSourceDefaultView();
        }

        private void lbElement_Drop(object sender, DragEventArgs e)
        {
            if (isDragCtmName)
            {
                return;
            }

            string[] itemArray = getItemArrayFromDropEvent(sender, e);
            if (itemArray == null)
            {
                return;
            }

            if (this.mainWindow.TabCtrlDS.SelectedIndex == 0)
            {
                this.DataSource = DataSourceType.CTM_DIRECT;
            }
            else
            {
                this.DataSource = DataSourceType.MISSION;
            }

            setAllListBoxItemsSource(itemArray, isDragCtmName);
        }

        private void MenuRemoveElement_Click(object sender, RoutedEventArgs e)
        {
            if (lbElement.SelectedIndex == -1)
            {
                return;
            }

            removeListBoxItem(false, sender);
            setAllListBoxItemsSourceDefaultView();
        }

        private void lbCtm_Drop(object sender, DragEventArgs e)
        {
            if (!isDragCtmName)
            {
                return;
            }

            string[] itemArray = getItemArrayFromDropEvent(sender, e);
            if (itemArray == null) return;

            if (this.mainWindow.TabCtrlDS.SelectedIndex == 0)
            {
                this.DataSource = DataSourceType.CTM_DIRECT;
            }
            else
            {
                this.DataSource = DataSourceType.MISSION;
            }

            setAllListBoxItemsSource(itemArray, isDragCtmName);
        }

        private void lbMission_Drop(object sender, DragEventArgs e)
        {
            var senderObj = sender as System.Windows.Controls.ListBox;
            if (senderObj == null ||
                !e.Data.GetDataPresent(DataFormats.Serializable))
            {
                return;
            }

            JToken tokenItem = (JToken)e.Data.GetData(DataFormats.Serializable);
            Mission droppedMission = JsonConvert.DeserializeObject<Mission>(tokenItem.ToString());

            InsertMissionToListBox(droppedMission);
        }

        private void lbMission_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            lbMission.SelectedIndex = -1; // clear MissionListBox selection
        }

        private void MenuRemoveMission_Click(object sender, RoutedEventArgs e)
        {
            if (lbMission.SelectedIndex == -1)
            {
                return;
            }

            removeMissionListBoxItem();
        }

        private void Validation_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed  // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !isTextAllowed(e.Text);
            if (e.Handled)
            {
                return;
            }
        }

        #endregion

        /// <summary>
        /// 更新周期
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        private List<KeyValuePair<string, TimeSpan>> intervalItems = new List<KeyValuePair<string, TimeSpan>>();

        /// <summary>
        /// 取得期間
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        private List<KeyValuePair<string, TimeSpan>> getPeriodItems = new List<KeyValuePair<string, TimeSpan>>();

        private void setKeyValuePair()
        {
            // 更新周期
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("10", new TimeSpan(0, 0, 10)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("30", new TimeSpan(0, 0, 30)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("60", new TimeSpan(0, 1, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("120", new TimeSpan(0, 2, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("180", new TimeSpan(0, 3, 0)));

            // 取得期間
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("1", new TimeSpan(1, 0, 0)));
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("2", new TimeSpan(2, 0, 0)));
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("3", new TimeSpan(3, 0, 0)));
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("6", new TimeSpan(6, 0, 0)));
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("12", new TimeSpan(12, 0, 0)));
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("24", new TimeSpan(24, 0, 0)));
        }

        private void setComboBoxItem()
        {
            // 更新周期
            foreach (var pair in this.intervalItems)
            {
                this.comboBox_Interval.Items.Add(pair.Key);
            }
            this.comboBox_Interval.SelectedItem = "60";

            // 取得期間
            foreach (var pair in this.getPeriodItems)
            {
                this.comboBox_GetPeriod.Items.Add(pair.Key);
            }
            this.comboBox_GetPeriod.SelectedItem = "1";
        }

        private class MissionViewObject
        {
            public string MissionDisplayName { get; set; }
            public string MissionId { get; set; }
            public string Json { get; set; }
        }

        private class MissionCtmViewObject
        {
            public string CtmDisplayName { get; set; }
            public string CtmId { get; set; }
            public string ParentMissionId { get; set; }
        }

        private class MissionCtmElementViewObject
        {
            public string ElementDisplayName { get; set; }
            public string ElementId { get; set; }
            public string ParentCtmId { get; set; }
            public string ParentMissionId { get; set; }
        }

		// Wang New dac flow 20190308 Start
        //private static readonly ILog logger = LogManager.GetLogger(typeof(UserControl_DACTemplate));
        private static readonly ILog logger = LogManager.GetLogger(typeof(UserControl_ProjectStatusMonitor));
		// Wang New dac flow 20190308 End
    }
}
