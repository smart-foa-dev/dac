﻿using DAC.AExcel;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using FoaCore.Common.Util;
using Microsoft.Office.Interop.Excel;
using SpreadsheetGear;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_LeadTimeTemplate.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_LeadTimeTemplate : BaseControl
    {
        private const string EXCEL_MACRO = "CallBeforeExcelFromAIS";
        private const int PARAM_SHEET_MAX_PARAMS = 100;

        private bool isFirstFile = true;
        private string excelFilePath = string.Empty;

        private List<CtmObject> ctms = new List<CtmObject>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public UserControl_LeadTimeTemplate(bool isFirstFile = true, string filePath = "")
        {
            InitializeComponent();

            this.Mode = ControlMode.LeadTime;
            this.TEMPLATE = "LeadTimeTemplate.xlsm";

            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            // 画像読み込み
            string fileName = "LT2.png";
            if (AisConf.UiLang == "en")
            {
                fileName = "en_LT2.png";
            }

            string uri = string.Format(@"/DAC;component/Resources/Image/{0}", fileName);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(uri, UriKind.RelativeOrAbsolute);
            bi.EndInit();
            this.image_Thumbnail.Source = bi;

            this.image_OpenExcel.Deactivate();

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();

            BitmapImage biCancel = new BitmapImage();
            biCancel.BeginInit();
            biCancel.UriSource = new Uri(@"/DAC;component/Resources/Image/cancel-to-get.png", UriKind.RelativeOrAbsolute);
            biCancel.EndInit();
            this.image_CancelToGet.Source = biCancel;

            this.image_CancelToGet.IsEnabled = false;

            BitmapImage biGage = new BitmapImage();
            biGage.BeginInit();
            biGage.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar-empty.png", UriKind.RelativeOrAbsolute);
            biGage.EndInit();
            this.image_Gage.Source = biGage;

            this.editControl.LeadTime = this;

            // No.479 In the case of Graph template screen, Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;

            if (!this.isFirstFile)
            {
                try
                {
                    readExcelFile(this.excelFilePath);
                    this.image_OpenExcel.Activate();
                }
                catch (Exception ex)
                {
                    string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                    message += string.Format("\n" + ex.StackTrace);
#endif
                    AisMessageBox.DisplayErrorMessageBox(message);
                }
            }
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// 開始時刻エレメントDrag&Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_StartTime_Drop(object sender, DragEventArgs e)
        {
            //LTが空白の場合にみ有効
            if (textBlock_LT.Text == string.Empty)
            {
                TextBlock tb = sender as TextBlock;
                if (tb == null)
                {
                    return;
                }

                if (!e.Data.GetDataPresent(DataFormats.StringFormat))
                {
                    return;
                }

                string item = (string)e.Data.GetData(DataFormats.StringFormat);
                string[] ary = item.Split(',');
                if (ary.Length < 5)
                {
                    return;
                }

                this.textBlock_StartTime.Text = string.Format("{0}・{1} ", ary[0], ary[2]);
            }
        }

        /// <summary>
        /// 終了時刻エレメントDrag&Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_EndTime_Drop(object sender, DragEventArgs e)
        {
            //LTが空白の場合にみ有効
            if (textBlock_LT.Text == string.Empty)
            {
                TextBlock tb = sender as TextBlock;
                if (tb == null)
                {
                    return;
                }

                if (!e.Data.GetDataPresent(DataFormats.StringFormat))
                {
                    return;
                }

                string item = (string)e.Data.GetData(DataFormats.StringFormat);
                string[] ary = item.Split(',');
                if (ary.Length < 5)
                {
                    return;
                }

                this.textBlock_EndTime.Text = string.Format("{0}・{1} ", ary[0], ary[2]);
            }
        }

        /// <summary>
        /// LeadTimeエレメントDrag& Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_LT_Drop(object sender, DragEventArgs e)
        {
            //終了時刻、開始時刻ともに空白の場合のみ有効
            if ((textBlock_StartTime.Text == string.Empty) && (textBlock_EndTime.Text == string.Empty))
            {
                TextBlock tb = sender as TextBlock;
                if (tb == null)
                {
                    return;
                }

                if (!e.Data.GetDataPresent(DataFormats.StringFormat))
                {
                    return;
                }

                string item = (string)e.Data.GetData(DataFormats.StringFormat);
                string[] ary = item.Split(',');
                if (ary.Length < 5)
                {
                    return;
                }

                this.textBlock_LT.Text = string.Format("{0}・{1} ", ary[0], ary[2]);
            }
        }

        /// <summary>
        /// 【詳細】ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //詳細画面表示有無チェック
            if (cnvsHaikei.Visibility == Visibility.Hidden)
            {
                //詳細画面表示
                cnvsHaikei.Visibility = Visibility.Visible;

                cnvsElement1.Visibility = Visibility.Visible;
                cnvsElement2.Visibility = Visibility.Visible;
                cnvsElement3.Visibility = Visibility.Visible;
            }
            else
            {
                //詳細画面非表示
                cnvsHaikei.Visibility = Visibility.Hidden;

                cnvsElement1.Visibility = Visibility.Hidden;
                cnvsElement2.Visibility = Visibility.Hidden;
                cnvsElement3.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// 背景データ削除（指定項目のみ）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bttnDelete_Click(object sender, RoutedEventArgs e)
        {
            //背景データ選択有無チェック(-1:未選択)
            if (lstHaikei.SelectedIndex < 0)
            {
                //背景データ未選択
            }
            else
            {
                //背景データ削除
                lstHaikei.Items.RemoveAt(lstHaikei.SelectedIndex);
            }
        }

        /// <summary>
        /// 有効化/無効化切替
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        /// <summary>
        /// LTエレメント削除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_LT_Click(object sender, MouseButtonEventArgs e)
        {
            textBlock_LT.Text = string.Empty;
        }

        /// <summary>
        /// 開始時刻エレメント削除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_StartTime_Click(object sender, MouseButtonEventArgs e)
        {
            textBlock_StartTime.Text = string.Empty;
        }

        /// <summary>
        /// 終了エレメント削除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_EndTime_Click(object sender, MouseButtonEventArgs e)
        {
            textBlock_EndTime.Text = string.Empty;
        }

        private void lstHaikei_Drop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.lstHaikei.Items.Add(string.Format("{0}・{1}", ary[0], ary[2]));
        }

        /// <summary>
        /// 背景データ手入力追加Drag&Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void txtHaikei_KeyDown(object sender, KeyEventArgs e)
        {
            if ((Keyboard.Modifiers == ModifierKeys.None) && (e.Key == Key.Enter))
            {
                string wrkStr = txtHaikei.Text;
                this.lstHaikei.Items.Add(txtHaikei.Text);
                this.txtHaikei.Text = string.Empty;
            }
        }

        private void bttnAdd_Click(object sender, RoutedEventArgs e)
        {
            if ((txtHaikei.Text != string.Empty) && (txtHaikei.Text != null))
            {
                this.lstHaikei.Items.Add(txtHaikei.Text);
                this.txtHaikei.Text = string.Empty;
            }
        }

        #region イベントハンドラ

        /// <summary>
        /// ミッションからの情報取得（「GET」ボタンクリック時の処理）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button_Get_Click(object sender, RoutedEventArgs e)
        {
            if (this.mainWindow.CtmDtailsCtrl.GetCtmDtailsEmpty() == string.Empty)
            {
                FoaMessageBox.ShowError("AIS_E_009");
                return;
            }

            // 入力値チェック
            if (this.dateTimePicker_Start.Value == null || this.dateTimePicker_End.Value == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return;
            }

            if (this.dateTimePicker_End.Value <= this.dateTimePicker_Start.Value)
            {
                FoaMessageBox.ShowError("AIS_E_019");
                return;
            }

            if ((string.IsNullOrEmpty(this.textBlock_StartTime.Text) ||
                string.IsNullOrEmpty(this.textBlock_EndTime.Text)) &&
                string.IsNullOrEmpty(this.textBlock_LT.Text))
            {
                FoaMessageBox.ShowError("AIS_E_003");
                return;
            }

            DateTime start1;
            DateTime end1;
            bool gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out start1, out end1);
            if (!gotTime)
            {
                return;
            }

            try
            {
                string endElement = this.textBlock_EndTime.Text.Trim();     //終了エレメント
                string startElement = this.textBlock_StartTime.Text.Trim(); //開始エレメント
                string resultElement = this.textBlock_LT.Text.Trim();       //結果エレメント

                string getStart = this.dateTimePicker_Start.Text.Trim();           //取得開始
                string getEnd = this.dateTimePicker_End.Text.Trim();               //取得終了

                string filePathDest = string.Empty;

                preRetrieveCtmData();

                this.DownloadCts = new CancellationTokenSource();
                var dirRetrieve = await RetrieveCtmData(start1, end1, this.DownloadCts);
                if (dirRetrieve == null)
                {
                    image_OpenExcel.Deactivate();
                    return;
                }

                this.Bw = new BackgroundWorker();
                this.Bw.WorkerSupportsCancellation = true;
                // define the event handlers
                Bw.DoWork += delegate(object s, DoWorkEventArgs args)
                {
                    var configParams = new Dictionary<ExcelConfigParam, object>();
                    configParams.Add(ExcelConfigParam.KAISHI_ELEM, startElement);
                    configParams.Add(ExcelConfigParam.SHURYO_ELEM, endElement);
                    configParams.Add(ExcelConfigParam.KEKKA_ELEM, resultElement);
                    configParams.Add(ExcelConfigParam.SHUTOKU_KAISHI, getStart);
                    configParams.Add(ExcelConfigParam.SHUTOKU_SHURYO, getEnd);

                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                        configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetCatalogLang(this.SelectedMission));
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                        configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetGripCatalogLang(this.SelectedGripMission));
                    }

                    if (!isFirstFile)
                    {
                        configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(this.excelFilePath));
                    }


                    filePathDest = writeResultToExcel_X(start1, end1, Bw, this.DataSource, dirRetrieve, configParams);

                    if (string.IsNullOrEmpty(filePathDest))
                    {
                        FoaMessageBox.ShowError("AIS_E_001");
                        return;
                    }
                };
                Bw.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
                {
                    this.Bw = null;

                    bool success = true;
                    if (args.Error != null)  // if an exception occurred during DoWork,
                    {
                        // Do your error handling here
                        AisMessageBox.DisplayErrorMessageBox(args.Error.ToString());
                        success = false;
                    }

                    postRetrieveCtmData(success);

                    this.tmpResultFolderPath = dirRetrieve;

                    //EXCELﾌｧｲﾙ名保存
                    this.excelFilePath = filePathDest;
                };
                Bw.RunWorkerAsync(); // starts the background worker

            }
            catch (Exception ex)
            {
                string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                AisMessageBox.DisplayErrorMessageBox(message);
            }
            finally
            {
                this.button_Get.IsEnabled = true;
                AisUtil.LoadProgressBarImage(this.image_Gage, false);
            }
        }

        #endregion

        #region Excelファイルの作成

        /// <summary>
        /// 開始処理
        /// </summary>
        private void preRetrieveCtmData()
        {
            this.button_Get.IsEnabled = false;
            this.image_CancelToGet.IsEnabled = true;
            this.image_OpenExcel.Deactivate();
            AisUtil.LoadProgressBarImage(this.image_Gage, true);
        }

        /// <summary>
        /// 終了処理
        /// </summary>
        /// <param name="success"></param>
        private void postRetrieveCtmData(bool success)
        {
            if (success)
            {
                this.image_OpenExcel.Activate();
            }

            AisUtil.LoadProgressBarImage(this.image_Gage, false);
            this.image_CancelToGet.IsEnabled = false;
            this.button_Get.IsEnabled = true;
        }

        #endregion

        private string writeResultToExcel_X(DateTime start, DateTime end, BackgroundWorker bWorker, DataSourceType resultType,
            string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            var excelFilenameMain = string.Empty;
            if (resultType == DataSourceType.CTM_DIRECT)
            {
                excelFilenameMain = "CTM_RESULT";
            }
            else if (resultType == DataSourceType.MISSION)
            {
                excelFilenameMain = AisUtil.GetCatalogLang(this.SelectedMission);
            }
            else
            {
                excelFilenameMain = AisUtil.GetGripCatalogLang(this.SelectedGripMission);
            }

            // コピー元ファイルの絶対パス
            string filePathSrc = isFirstFile ? System.IO.Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE) : this.excelFilePath;

            string fileName = string.Format("{0}_{1}.xlsm", excelFilenameMain, DateTime.Now.ToString("yyyyMMddHHmmss"));
            fileName = fileName.Replace("[", string.Empty);
            fileName = fileName.Replace("]", string.Empty);

            string filePathDest = Path.Combine(resultDir, fileName);
            if (!Directory.Exists(this.resultDir))
            {
                Directory.CreateDirectory(this.resultDir);
            }
            File.Copy(filePathSrc, filePathDest);

            var writer = new SSGExcelWriterLeadTime(this.Mode, this.DataSource, this.editControl.Ctms, this.editControl.Dt.Copy(), bWorker, this.SelectedMission);
            writer.WriteCtmData(filePathDest, folderPath, configParams);

            return filePathDest;
        }

        /// <summary>
        /// EXCELマクロをAISより実行：EXCELオープンから起動される。
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="macro"></param>
        /// <param name="saveChange"></param>
        private void getParamsFromExcel(string filepath, string macro, bool saveChange)
        {
            Microsoft.Office.Interop.Excel.Application excel = null;
            Microsoft.Office.Interop.Excel.Workbooks workBooks = null;
            Microsoft.Office.Interop.Excel.Workbook workBook = null;

            try
            {
                //マクロ実行準備
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = false;
                ExcelUtil.MoveExcelOutofScreen(excel);
                workBooks = excel.Workbooks;
                workBook = workBooks.Open(filepath);

                // マクロの実行
                excel.Run(macro);
            }
            catch (Exception ex)
            {
                string message = string.Format("Excelファイルへのアクセスに失敗しました。\n{0}", ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                /*
                System.Windows.Forms.MessageBox.Show(message, "Error!!",
                   System.Windows.Forms.MessageBoxButtons.OK,
                   System.Windows.Forms.MessageBoxIcon.Error);
                 * */
            }
            finally
            {
                if (workBook != null)
                {
                    workBook.Close(saveChange);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workBook);
                    workBook = null;
                }

                if (workBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workBooks);
                    workBooks = null;
                }

                if (excel != null)
                {
                    excel.Quit();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
                    excel = null;
                }
            }
        }

        /// <summary>
        /// エレメント条件Drag&Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtblcElement3_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.txtblcElement3.Text = string.Format("{0}", ary[2]);
        }

        private void txtblcElement2_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.txtblcElement2.Text = string.Format("{0} ", ary[2]);
        }

        private void txtblcElement1_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.txtblcElement1.Text = string.Format("{0}", ary[2]);
        }

        private Dictionary<string, string> conditionDictionary = new Dictionary<string, string>();
        private Dictionary<string, string> paramDictionary = new Dictionary<string, string>();

        /// <summary>
        /// 収集開始日時と収集終了日時を取込みAIS画面に表示
        /// </summary>
        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string conditionSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetCondition = AisUtil.GetSheetFromSheetName_SSG(book, conditionSheetName);
                var conditionDic = readCondition(worksheetCondition);

                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var paramMap = readParam(worksheetParam);

                int firstRowIndex = 0;
                int firstColumnIndex = 3;

                //paramの有効行の算出
                int rowLength = AisUtil.GetRowLength_SSG(worksheetCondition, firstRowIndex, firstColumnIndex);

                //paramの有効列の算出
                int columnLength = AisUtil.GetColumnLength_SSG(worksheetCondition, firstRowIndex, firstColumnIndex);
                System.Data.DataTable dtElementWithId = AisUtil.CreateDataTableFromExcel_SSG(worksheetParam, firstRowIndex, firstColumnIndex, rowLength, columnLength);
                System.Data.DataTable dtElement = AisUtil.DeleteIdRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                System.Data.DataTable dtId = AisUtil.DeleteNameRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                var elementColumnDic = AisUtil.CountElementColumn(dtElementWithId);

                if (paramMap.ContainsKey("収集タイプ") &&
                    !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                {
                    switch (paramMap["収集タイプ"])
                    {
                        case "CTM":
                            this.DataSource = DataSourceType.CTM_DIRECT;
                            break;
                        case "ミッション":
                            this.DataSource = DataSourceType.MISSION;
                            break;
                        case "GRIP":
                            this.DataSource = DataSourceType.GRIP;
                            break;
                        default:
                            break;
                    }
                }

                // set SelectedMission
                if (paramMap.ContainsKey("ミッションID") &&
                    !string.IsNullOrEmpty(paramMap["ミッションID"]))
                {
                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        string missionId = conditionDic["ミッションID"];
                        Mission mission = AisUtil.GetMissionFromId(missionId);
                        this.SelectedMission = mission;
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        string missionId = conditionDic["ミッションID"];
                        GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                        this.SelectedGripMission = mission;
                    }
                }

                this.dtId = dtId;

                this.conditionDictionary = conditionDic;
                this.paramDictionary = paramMap;

                DateTime dtStart = new DateTime();
                if (DateTime.TryParse(conditionDic["取得開始"], out dtStart))
                {
                    this.dateTimePicker_Start.Value = dtStart;
                }

                DateTime dtEnd = new DateTime();
                if (DateTime.TryParse(conditionDic["取得終了"], out dtEnd))
                {
                    this.dateTimePicker_End.Value = dtEnd;
                }

                if (paramMap["終了エレメント"] != "")
                {
                    if (paramMap["終了エレメント"] != null)
                    {
                        this.textBlock_EndTime.Text = (string)paramMap["終了エレメント"];
                    }
                }

                if (paramMap["開始エレメント"] != "")
                {
                    if (paramMap["開始エレメント"] != null)
                    {
                        this.textBlock_StartTime.Text = (string)paramMap["開始エレメント"];
                    }
                }

                if (paramMap["結果エレメント"] != "")
                {
                    if (paramMap["結果エレメント"] != null)
                    {
                        this.textBlock_LT.Text = (string)paramMap["結果エレメント"];
                    }
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private Dictionary<string, string> readParam(SpreadsheetGear.IWorksheet worksheetParam)
        {
            var map = new Dictionary<string, string>();

            SpreadsheetGear.IRange paramCells = worksheetParam.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // サーバIPアドレス
                if (paramName == "サーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // サーバポート番号
                if (paramName == "サーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // CTM NAME
                if (paramName == "CTM名")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // CTMID
                if (paramName == "CTMID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // エレメントID
                if (paramName == "エレメントID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 終了エレメント
                if (paramName == "終了エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // 開始エレメント
                if (paramName == "開始エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // 結果エレメント
                if (paramName == "結果エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバIPアドレス
                if (paramName == "GRIPサーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバポート番号
                if (paramName == "GRIPサーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }
            }

            return map;
        }

        private Dictionary<string, string> readCondition(SpreadsheetGear.IWorksheet worksheetCondition)
        {
            var dic = new Dictionary<string, string>();

            // 収集条件を出力
            SpreadsheetGear.IRange cellsCondition = worksheetCondition.Cells;
            for (int i = 0; i < 30; i++)   // とりあえず10×10のセルを検索
            {
                string paramName = cellsCondition[i, 0].Text;
                string paramValue = cellsCondition[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // ミッション
                if (paramName == "ミッション")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // ミッションID
                if (paramName == "ミッションID")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // 収集開始日時
                if (paramName == "取得開始")
                {
                    string date = cellsCondition[i, 1].Text;
                    string time = cellsCondition[i, 2].Text;
                    string datetime = date;

                    dic.Add(paramName, datetime);
                    continue;
                }

                // 収集終了日時
                if (paramName == "取得終了")
                {
                    string date = cellsCondition[i, 1].Text;
                    string time = cellsCondition[i, 2].Text;
                    string datetime = date;

                    dic.Add(paramName, datetime);
                    continue;
                }
            }


            return dic;
        }

        /// <summary>
        /// ミッションIDからミッション内容を表示
        /// </summary>
        public void SetMission()
        {
            if (!this.conditionDictionary.ContainsKey("ミッションID"))
            {
                return;
            }
            string selectedMissionId = this.conditionDictionary["ミッションID"];

            this.IsSelectedProgramMission = true;

            var item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(selectedMissionId);
            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_CTM.Focus();
            item.Focus();
        }

        public void SetGripMission()
        {
            if (!this.conditionDictionary.ContainsKey("ミッションID"))
            {
                return;
            }
            string selectedMissionId = this.conditionDictionary["ミッションID"];
            var item = this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(selectedMissionId);
            if (item == null)
            {
                if (this.SelectedGripMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(this.SelectedGripMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_Grip.Focus();
            item.Focus();
        }

        private void image_CancelToGet_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.Bw != null)
            {
                this.Bw.CancelAsync();
            }

            this.DownloadCts.Cancel();
            this.DownloadCts = new CancellationTokenSource();
            CancelGripData();
            AisUtil.LoadProgressBarImage(this.image_Gage, false);

            this.button_Get.IsEnabled = true;
            this.image_CancelToGet.IsEnabled = false;
        }

        private void image_OpenExcel_Tap(object sender, RoutedEventArgs e)
        {
            if (this.tmpResultFolderPath != null)
            {
                var reultFiles = Directory.GetFiles(this.tmpResultFolderPath, "*.csv");
                if (reultFiles.Length == 0)
                {
                    AisUtil.LoadProgressBarImage(this.image_Gage, false);
                    FoaMessageBox.ShowError("AIS_E_006");
                    return;
                }
            }


            //マクロ起動処理
            getParamsFromExcel(this.excelFilePath, EXCEL_MACRO, true);

            //EXCELﾌｧｲﾙ表示
            /*
            var processStartInfo = new ProcessStartInfo();
            processStartInfo.FileName = this.excelFilePath;
            Process process = Process.Start(processStartInfo);
             * */
            ExcelUtil.OpenExcelFile(this.excelFilePath);
        }
    }
}
