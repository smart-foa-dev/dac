﻿using FoaCore.Common.Entity;

namespace DAC.Model
{
    public class AgentUser : CmsEntity
    {
        public string AgentId { get; set; }
        public string UserId { get; set; }
    }
}
