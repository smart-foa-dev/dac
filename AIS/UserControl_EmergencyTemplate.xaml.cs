﻿using FoaCore.Common.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DAC.View;
using System.Collections.ObjectModel;
using System.IO;
using DAC.Model.Util;
using FoaCore.Common.Util;
using DAC.Common;
using DAC.View.Helpers;
using System.Net;
using FoaCore.Common.Model;
using System.ComponentModel;
using FoaCore.Common;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_EmergencyTemplate.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_EmergencyTemplate : BaseControl
    {
        private string MailAddress_Value = "";
        private List<string> MailAddress_Value_List;
        private string UserId = "";

        public UserControl_EmergencyTemplate()
        {
            InitializeComponent();

            this.Mode = ControlMode.Emergency;

            this.dateTimePicker_PeriodEnd.Value = DateTime.Now.AddDays(1);
            
            //全てのUSERを取得して、画面のcomboxに返応する
            GetListValue();
        }

        private void GetListValue()
        {
            combobox_cbObjects.SelectedIndex = -1;
            ObservableCollection<SelectableObject<Person>> _list;
            _list = new ObservableCollection<SelectableObject<Person>>();

            foreach (var information in getUserNameList())
            {
                _list.Add(new SelectableObject<Person>(new Person(information.UserName)));
            }
            combobox_cbObjects.ItemsSource = _list;
        }

        private class Person
        {
            public Person(String userName)
            {
                this.userName = userName;
            }
            private String userName;
            public String UserName
            {
                get { return userName; }
                set { userName = value; }
            }
        }

        public class SelectableObject<T> : INotifyPropertyChanged
        {
            private bool _isSelected;
            public bool IsSelected {
                get {
                    return _isSelected;
                }
                set
                {
                    _isSelected = value;
                    NotifyPropertyChanged("IsSelected");
                }
            }
            public T ObjectData { get; set; }

            public SelectableObject(T objectData)
            {
                ObjectData = objectData;
            }

            public SelectableObject(T objectData, bool isSelected)
            {
                IsSelected = isSelected;
                ObjectData = objectData;
            }

            public new event PropertyChangedEventHandler PropertyChanged;

            private void NotifyPropertyChanged(string propertyName)
            {
                if (PropertyChanged != null)
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// チェックしたUSERを画面に表示する
        /// </summary>
        private void OnCbObjectCheckBoxChecked(object sender, RoutedEventArgs e)
        {
            StringBuilder userName = new StringBuilder();
            StringBuilder userId = new StringBuilder();
            StringBuilder address = new StringBuilder();
            // Wang Issue AISEMG-22 20190109 Start
            //MailAddress_Value_List = new List<string>();
            if (MailAddress_Value_List == null)
            {
                MailAddress_Value_List = new List<string>();
            }
            MailAddress_Value_List.Clear();
            // Wang Issue AISEMG-22 20190109 End
            foreach (SelectableObject<Person> cbObject in combobox_cbObjects.Items)
            {
                if (cbObject.IsSelected)
                {
                    userName.AppendFormat("{0},", cbObject.ObjectData.UserName);
                    foreach (var information in getUserNameList())
                    {
                        if (cbObject.ObjectData.UserName.Equals(information.UserName))
                        {
                            userId.AppendFormat("{0},", information.UserId);
                            address.AppendFormat("{0};", information.MailAddress_Value);
                            MailAddress_Value_List.Add(information.MailAddress_Value);
                        }
                    }
                }
            }
            tbObjects.Text = userName.ToString().Trim().TrimEnd(',');
            MailAddress_Value = address.ToString().Trim().TrimEnd(';');
            UserId = userId.ToString().Trim().TrimEnd(',');
        }

        private void OnCbObjectsSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            comboBox.SelectedItem = null;
        }

        /// <summary>
        /// 全てのUSERを取得
        /// </summary>
        /// <returns>userInformation</returns>
        public List<UserInformation> getUserNameList()
        {
            List<UserInformation> userInformation = new List<UserInformation>();
            string url = CmsUrl.GetUserList();

            //using (WebClient client = new WebClient())
            using (WebClient client = AisUtil.getProxyWebClient())
            {
                client.Encoding = Encoding.UTF8;
                string response = client.DownloadString(url);

                var users = JsonConvert.DeserializeObject<UserInfoList>(response);
                foreach (Users user in users.Users)
                {
                    UserInformation information = new UserInformation() { UserId = user.Id, UserName = user.UserName, MailAddress_Value = user.MailAddress };
                    userInformation.Add(information);
                }
            }

            return userInformation;
        }

        public class UserInformation
        {
            public string UserId;
            public string UserName;
            public string MailAddress_Value;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();

            this.radioButton_RecieveCtm.IsChecked = true;
            this.radioButton_AddCtmFalse.IsChecked = true;

            this.editControl.Emergency = this;
            this.button_AndOr.IsEnabled = false;

            ObservableCollection<EmergencyTimer> list = readEt();
            this.timers = list;
            this.listView_Registry.DataContext = this.timers;

            //言語が英語の場合の表示調整
            string cultureName = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;
            int fontvalue = 10;
            if (cultureName == "en-US")
            {
                this.radioButton_RecieveCtm.FontSize = fontvalue;
                this.radioButton_Threshold.FontSize = fontvalue;
            }

            // No.549 Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;
            // Wang Issue AISEMG-32 20190109 Start
            m.MissionTab_GripMissionArea.Visibility = System.Windows.Visibility.Collapsed;
            // Wang Issue AISEMG-32 20190109 End

            //this.startEt();
        }

        private string missionId = string.Empty;
        private string ctmId = string.Empty;
        private string elementId_01 = string.Empty;
        private int elementType_01 = 0;
        private string elementId_02 = string.Empty;
        private int elementType_02 = 0;
        private ObservableCollection<EmergencyTimer> timers = new ObservableCollection<EmergencyTimer>();
        private List<CtmObject> ctms = new List<CtmObject>();

        /// <summary>
        /// 「Test」
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Test_Click(object sender, RoutedEventArgs e)
        {
            // NullOrEmptyCheck
            if (string.IsNullOrEmpty(this.MailAddress_Value))
            {
                FoaMessageBox.ShowError("AIS_E_015");  //メールアドレスが入力されていません
                return;
            }

            // MailAddresValidationCheck
            MailAddressValidationRule validator = new MailAddressValidationRule();
            //ValidationResult res = validator.Validate(this.MailAddress_Value, null);
            //if (!res.IsValid)
            //{
            //    AisMessageBox.DisplayErrorMessageBox(res.ErrorContent.ToString());　　//メールアドレスの形式ではありません
            //    return;
            //}
            // Wang Issue AISEMG-22 20190109 Start
            if (MailAddress_Value_List != null)
            // Wang Issue AISEMG-22 20190109 End
            {
                foreach (string mailAddress in MailAddress_Value_List)
                {
                    // 通報
                    EmergencyTimer et = new EmergencyTimer();
                    et.SendMail(mailAddress, "[Test]" + this.textBox_MailTitle.Text, this.textBox_MailMessage.Text, false);
                }
            }

            FoaMessageBox.ShowInfo("AIS_I_001");  //テストメールを送信しました。
        }

        /// <summary>
        /// 「登録」
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Regist_Click(object sender, RoutedEventArgs e)
        {
            if (!validateInput())
            {
                return;
            }

            int interval = int.Parse(this.textBox_Interval.Text);
            DateTime periodEnd = (DateTime)this.dateTimePicker_PeriodEnd.Value;
            int occ = int.Parse(this.textBox_Occurrences.Text);
            bool threshold = (bool)this.radioButton_Threshold.IsChecked;
            string elementCondition1 = this.comboBox_ElementCondition_01.Items.Count == 0 ? string.Empty : this.comboBox_ElementCondition_01.SelectedItem.ToString();
            string elementConditionValue1 = this.textBox_ElementConditionValue_01.Text;
            string elementCondition2 = this.comboBox_ElementCondition_02.Items.Count == 0 ? string.Empty : this.comboBox_ElementCondition_02.SelectedItem.ToString();
            string elementConditionValue2 = this.textBox_ElementConditionValue_02.Text;
            string andOr = this.button_AndOr.Content == null ? "OR" : this.button_AndOr.Content.ToString();
            string address = this.MailAddress_Value;
            string userId = this.UserId;
            string subject = this.textBox_MailTitle.Text;
            string message = this.textBox_MailMessage.Text;
            bool addCtm = (bool)this.radioButton_AddCtmTrue.IsChecked;

            List<object> args = new List<object>();

            args.Add(interval);
            args.Add(periodEnd);
            args.Add(occ);
            args.Add(threshold);
            args.Add(elementCondition1);
            args.Add(elementConditionValue1);
            args.Add(elementCondition2);
            args.Add(elementConditionValue2);
            args.Add(andOr);
            args.Add(address);
            args.Add(subject);
            args.Add(message);
            args.Add(addCtm);

            CtmObject argsCtm = null;
            foreach (var ctm in this.ctms)
            {
                if (ctm.id.ToString() != this.ctmId)
                {
                    continue;
                }

                argsCtm = ctm;
                break;
            }
            if (argsCtm == null)
            {
                var list = AisUtil.GetCtmsFromCtmIds(new List<string>() { this.ctmId });
                argsCtm = list[0];
            }
            args.Add(argsCtm);

            // 追加
            string missionName = this.textBlock_MissionName.Text;
            string ctmName = this.textBlock_CtmName.Text;
            string elementName_01 = this.textBlock_ElementName_01.Text;
            int elementType_01 = this.elementType_01;
            string elementName_02 = this.textBlock_ElementName_02.Text;
            int elementType_02 = this.elementType_02;
            args.Add(missionName);
            args.Add(ctmName);
            args.Add(elementName_01);
            args.Add(elementType_01);
            args.Add(elementName_02);
            args.Add(elementType_02);
            string id = string.Empty;
            if (this.listView_Registry.SelectedIndex != -1)
            {
                EmergencyTimer target = this.listView_Registry.SelectedItem as EmergencyTimer;
                if (target != null)
                {
                    id = target.Id;
                }
            }
            args.Add(id);
            args.Add(userId);
            string UiLang = LoginInfo.GetInstance().GetUiLang();
            args.Add(UiLang); 
            setTimer(args.ToArray());

            //db処理するので、削除
            //writeEt(this.timers);

            FoaMessageBox.ShowInfo("AIS_I_002");  //登録しました。
        }

        private bool validateInput()
        {
            if (string.IsNullOrEmpty(this.textBlock_MissionName.Text))
            {
                FoaMessageBox.ShowError("AIS_E_008");  //ミッションが選択されていません。
                return false;
            }

            if (string.IsNullOrEmpty(this.textBlock_CtmName.Text))
            {
                FoaMessageBox.ShowError("AIS_E_009");  //CTMが選択されていません。
                return false;
            }

            if (this.dateTimePicker_PeriodEnd.Value == null)
            {
                FoaMessageBox.ShowError("AIS_E_032");  //通報期間が入力されていません。
                return false;
            }

            if (this.dateTimePicker_PeriodEnd.Value <= DateTime.Now)
            {
                FoaMessageBox.ShowError("AIS_E_010");  //通報期間の入力が正しくありません。
                return false;
            }

            if (string.IsNullOrEmpty(this.textBox_Occurrences.Text))
            {
                FoaMessageBox.ShowError("AIS_E_011");  //発生回数が入力されていません。
                return false;
            }

            int occ = 0;
            if (!int.TryParse(this.textBox_Occurrences.Text, out occ) ||
                occ < 1)
            {
                FoaMessageBox.ShowError("AIS_E_012");　//発生回数には、1以上の整数を入力して下さい。
                return false;
            }

            if (string.IsNullOrEmpty(this.textBox_Interval.Text))
            {
                FoaMessageBox.ShowError("AIS_E_013");  //監視幅が入力されていません。
                return false;
            }

            int interval = 0;
            if (!int.TryParse(this.textBox_Interval.Text, out interval) ||
                occ < 1)
            {
                FoaMessageBox.ShowError("AIS_E_014");  //監視幅には、1以上の整数を入力して下さい
                return false;
            }

            if ((bool)this.radioButton_Threshold.IsChecked &&
                string.IsNullOrEmpty(this.textBlock_ElementName_01.Text))
            {
                FoaMessageBox.ShowError("AIS_E_007");  //エレメントが選択されていません。 
                return false;
            }

            if (string.IsNullOrEmpty(this.MailAddress_Value))
            {
                FoaMessageBox.ShowError("AIS_E_015");  //メールアドレスが入力されていません
                return false;
            }

            // MailAddresValidationCheck
            //MailAddressValidationRule validator = new MailAddressValidationRule();
            //ValidationResult res = validator.Validate(MailAddress_Value, null);
            //if (!res.IsValid)
            //{
            //    AisMessageBox.DisplayErrorMessageBox(res.ErrorContent.ToString());  //メールアドレスの形式ではありません
            //    return false;
            //}

            return true;
        }

        private void setTimer(object args)
        {
            object[] obj = (object[])args;

            int interval = (int)obj[0];
            DateTime periodEnd = (DateTime)obj[1];
            int occ = (int)obj[2];
            bool threshold = (bool)obj[3];
            string elementCondition1 = (string)obj[4];
            string elementConditionValue1 = (string)obj[5];
            string elementCondition2 = (string)obj[6];
            string elementConditionValue2 = (string)obj[7];
            string andOr = (string)obj[8];
            string address = (string)obj[9];
            string userId = (string)obj[21];
            string uiLang = (string)obj[22];
            string subject = (string)obj[10];
            string message = (string)obj[11];
            bool addCtm = (bool)obj[12];
            string elementName_01 = (string)obj[16];
            int elementType_01 = (int)obj[17];
            string elementName_02 = (string)obj[18];
            int elementType_02 = (int)obj[19];
            CtmObject ctm = (CtmObject)obj[13];

            EmergencyTimer et = new EmergencyTimer();
            et.NowTimeSpan = new TimeSpan();
            et.OldTimeSpan = new TimeSpan();
            et.StartTime = new DateTime();
            et.IntervalMinute = interval;
            et.MissionId = this.missionId;
            et.CtmId = this.ctmId;
            et.EndTime = periodEnd.ToString("yyyy/MM/dd HH:mm:ss");
            et.MaxCount = occ;
            et.IsDetailCondition = threshold;
            if (threshold)
            {
                et.ElementName_01 = elementName_01;
                et.ElementType_01 = elementType_01;
                et.ElementId_01 = this.elementId_01;
                et.ElementCondition_01 = elementCondition1;
                et.ElementConditionValue_01 = elementConditionValue1;
                if (!string.IsNullOrEmpty(this.elementId_02))
                {
                    et.ElementName_02 = elementName_02;
                    et.ElementType_02 = elementType_02;
                    et.ElementId_02 = this.elementId_02;
                    et.ElementCondition_02 = elementCondition2;
                    et.ElementConditionValue_02 = elementConditionValue2;
                    et.AndOr = andOr;
                }
            }
            et.UserId = userId;
            et.UiLang = uiLang;
            et.MailAddress = address;
            et.MailSubject = subject;
            et.MailMessage = message;
            et.AddFile = addCtm;
            et.Ctm = ctm;

            // 追加
            string missionName = (string)obj[14];
            string ctmName = (string)obj[15];
            string id = (string)obj[20];
            UserInfo user = LoginInfo.GetInstance().GetLoginUserInfo();
            string loginUserId = user.UserId;
            et.MissionName = missionName;
            et.CtmName = ctmName;
            et.LoginUserId = loginUserId;

            if (id != string.Empty)
            {
                et.Id = id;
            }

            // 削除
            foreach (EmergencyTimer timer in this.timers)
            {
                if (timer.Id != et.Id)
                {
                    continue;
                }

                deleteEt(timer, this.timers);
                break;
            }


            string Json = JsonConvert.SerializeObject(et);
            //string ctmJson = JsonConvert.SerializeObject(et.Ctm.id);

            JToken etToken = JToken.Parse(Json);
            //JToken ctmToken = JToken.Parse(ctmJson);
            //etToken["CtmId"] = ctmToken;

            string json = etToken.ToString();

            //Mysqlのテーブルemergency_mailに保存する
            CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
            client.CompleteHandler += resJson =>
            { };
            client.Put(CmsUrl.SetEmergencyMail(), json);
            //et.TimerStart();
            //Console.WriteLine("### {0} - {1} watching start.", et.MissionName, et.CtmName);
            this.timers.Add(et);
        }

        private void button_AndOr_Click(object sender, RoutedEventArgs e)
        {
            if (this.button_AndOr.Content.ToString() == "OR")
            {
                this.button_AndOr.Content = "AND";
            }
            else if (this.button_AndOr.Content.ToString() == "AND")
            {
                this.button_AndOr.Content = "OR";
            }
        }

        public void SetMissionName(string missionName, string missionId)
        {
            this.textBlock_MissionName.Text = missionName;
            this.missionId = missionId;
            this.textBlock_CtmName.Text = string.Empty;
            this.ctmId = string.Empty;
            this.textBlock_ElementName_01.Text = string.Empty;
            this.elementId_01 = string.Empty;
            this.elementType_01 = 0;
            this.textBlock_ElementName_02.Text = string.Empty;
            this.elementId_02 = string.Empty;
            this.elementType_02 = 0;
            this.button_AndOr.Content = "OR";
            this.button_AndOr.IsEnabled = false;
            this.textBox_ElementConditionValue_01.Text = string.Empty;
            this.textBox_ElementConditionValue_02.Text = string.Empty;
            this.comboBox_ElementCondition_01.Items.Clear();
            this.comboBox_ElementCondition_02.Items.Clear();

            getElements(missionId);
        }

        private void getElements(string missionId)
        {
            var pkMap = new Dictionary<string, string>();

            // 設定したCTMをCTMの形(ツリー形式でグループ含む)で表現したい為、
            // CTMの取得(MMSのJson形式)とミッションCTM、ミッションエレメントの取得を行う。
            // 1リクエストで行うのはかなり複雑になるので、2リクエスト。
            // 一つ一つのリクエストはシンプルなのもプラス要素。ただ若干メソッドが長くネストが深い...

            // まずミッションCTMとミッションエレメントを取得
            CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
            client.LastInvokeCompleteHandler = true;
            client.UseMultipleRequest = true;
            client.AddParam("missionId", missionId);
            client.CompleteHandler += resJson =>
            {
                // 選択済みのCTMの情報
                List<MissionCtm> mcList = JsonConvert.DeserializeObject<List<MissionCtm>>(resJson);

                List<string> selectedElementIds = new List<string>();
                mcList.ForEach(mc =>
                {
                    // CTMのIDとミッションCTMのPKを取得
                    //  searchCtmIdJson.Add(CmsUtil.GetIdToken(mc.CtmId));
                    pkMap[mc.CtmId] = mc.Id;
                    mc.Elements.ForEach(mce =>
                    {
                        // エレメントのIDとミッションCTMエレメントのPKを取得
                        selectedElementIds.Add(mce.ElementId);
                        pkMap[mce.ElementId] = mce.Id;
                    });
                });

                // CTMを取得
                {
                    CmsHttpClient ctmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                    ctmGetClient.UseMultipleRequest = true;
                    mcList.ForEach(mc =>
                    {
                        // CTMのIDとミッションCTMのPKを取得
                        ctmGetClient.AddParam("id", mc.CtmId);

                    });

                    ctmGetClient.CompleteHandler += ctmJson =>
                    {
                        this.ctms = JsonConvert.DeserializeObject<List<CtmObject>>(ctmJson);
                    };

                    ctmGetClient.Get(CmsUrlMms.Ctm.List());
                }
            };

            client.Get(CmsUrl.GetMissionCtmUrl());
        }

        private void textBlock_CtmName_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = item.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 4)
            {
                return;
            }

            if (this.textBlock_CtmName.Text != itemArray[0])
            {
                this.textBlock_CtmName.Text = itemArray[0];
                this.textBlock_ElementName_01.Text = string.Empty;
                this.textBlock_ElementName_02.Text = string.Empty;
                this.ctmId = itemArray[1];
                this.button_AndOr.Content = "OR";
                this.button_AndOr.IsEnabled = false;
                this.textBox_ElementConditionValue_01.Text = string.Empty;
                this.textBox_ElementConditionValue_02.Text = string.Empty;
                this.comboBox_ElementCondition_01.Items.Clear();
                this.comboBox_ElementCondition_02.Items.Clear();
            }
        }

        private void textBlock_ElementName_01_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = item.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 3)
            {
                return;
            }

            if (string.IsNullOrEmpty(this.textBlock_CtmName.Text))
            {
                FoaMessageBox.ShowError("AIS_E_016");  //先に「CTM名称」を指定してください。
                return;
            }

            if (this.textBlock_CtmName.Text != itemArray[0])
            {
                FoaMessageBox.ShowError("AIS_E_017");  //別のCTMが指定されています。このエレメントを指定する場合、先に該当する「CTM名称」を指定してください。
                return;
            }

            int dataType = int.Parse(itemArray[4]);
            if (dataType == 100)
            {
                FoaMessageBox.ShowError("AIS_E_018");  //Bulky型には条件を設定できません。
                return;
            }

            this.textBlock_ElementName_01.Text = itemArray[2];
            this.elementId_01 = itemArray[3];
            this.elementType_01 = dataType;
            setConditionType(this.comboBox_ElementCondition_01, dataType);
        }

        private void textBlock_ElementName_02_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = item.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 3)
            {
                return;
            }

            if (string.IsNullOrEmpty(this.textBlock_CtmName.Text))
            {
                FoaMessageBox.ShowError("AIS_E_016");  //先に「CTM名称」を指定してください。
                return;
            }

            if (this.textBlock_CtmName.Text != itemArray[0])
            {
                FoaMessageBox.ShowError("AIS_E_017");  //別のCTMが指定されています。このエレメントを指定する場合、先に該当する「CTM名称」を指定してください。
                return;
            }

            int dataType = int.Parse(itemArray[4]);
            if (dataType == 100)
            {
                FoaMessageBox.ShowError("AIS_E_018");  //Bulky型には条件を設定できません。
                return;
            }

            if (string.IsNullOrEmpty(this.textBlock_ElementName_01.Text))
            {
                this.textBlock_ElementName_01.Text = itemArray[2];
                this.elementId_01 = itemArray[3];
                this.elementType_01 = dataType;
                this.textBlock_ElementName_02.Text = string.Empty;
                this.elementId_02 = string.Empty;
                setConditionType(this.comboBox_ElementCondition_01, dataType);
            }
            else
            {
                this.textBlock_ElementName_02.Text = itemArray[2];
                this.elementId_02 = itemArray[3];
                this.elementType_02 = dataType;
                setConditionType(this.comboBox_ElementCondition_02, dataType);
                this.button_AndOr.IsEnabled = true;
            }
        }

        private void radioButton_RecieveCtm_Checked(object sender, RoutedEventArgs e)
        {
            this.canvas_ElementCondition.Visibility = Visibility.Collapsed;
            this.button_AndOr.IsEnabled = false;
        }

        private void radioButton_Threshold_Checked(object sender, RoutedEventArgs e)
        {
            this.canvas_ElementCondition.Visibility = Visibility.Visible;
            this.button_AndOr.IsEnabled = true;
        }

        private void setConditionType(ComboBox cb, int dataType)
        {
            List<string> items = new List<string>();
            items.Add("==");
            items.Add("!=");
            if (dataType != 1 &&
                dataType != 3)
            {
                items.Add("<");
                items.Add("<=");
                items.Add(">");
                items.Add(">=");
            }

            cb.Items.Clear();
            foreach (string item in items)
            {
                cb.Items.Add(item);
            }

            if (0 < cb.Items.Count)
            {
                cb.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Reset
        /// </summary>
        private void resetControl()
        {
            this.missionId = string.Empty;
            this.textBlock_MissionName.Text = string.Empty;
            this.radioButton_RecieveCtm.IsChecked = true;
            this.ctmId = string.Empty;
            this.textBlock_CtmName.Text = string.Empty;
            this.dateTimePicker_PeriodEnd.Text = string.Empty;
            this.textBox_Occurrences.Text = string.Empty;
            this.textBox_Interval.Text = string.Empty;
            this.button_AndOr.Content = "OR";
            this.button_AndOr.IsEnabled = false;
            this.elementId_01 = string.Empty;
            this.elementType_01 = 0;
            this.textBlock_ElementName_01.Text = string.Empty;
            this.comboBox_ElementCondition_01.SelectedIndex = -1;
            this.comboBox_ElementCondition_01.Items.Clear();
            this.textBox_ElementConditionValue_01.Text = string.Empty;
            this.elementId_02 = string.Empty;
            this.elementType_02 = 0;
            this.textBlock_ElementName_02.Text = string.Empty;
            this.comboBox_ElementCondition_02.SelectedIndex = -1;
            this.comboBox_ElementCondition_02.Items.Clear();
            this.textBox_ElementConditionValue_02.Text = string.Empty;
            this.canvas_ElementCondition.Visibility = Visibility.Collapsed;
            this.tbObjects.Text = string.Empty;
            this.textBox_MailTitle.Text = string.Empty;
            this.textBox_MailMessage.Text = string.Empty;
            this.radioButton_AddCtmFalse.IsChecked = true;
            this.ctms = new List<CtmObject>();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.listView_Registry.SelectedItems.Count < 1)
            {
                TreeViewItem selectedItem = this.mainWindow.treeView_Mission_CTM.SelectedItem as TreeViewItem;
                if (selectedItem != null)
                {
                    selectedItem.IsSelected = false;
                }
                this.editControl.SetInitialDataGrid();

                this.tbObjects.Text = string.Empty;

                foreach (SelectableObject<Person> cbObject in this.combobox_cbObjects.Items)
                {
                    cbObject.IsSelected = false;
                }
                // reset
                resetControl();
                return;
            }

            EmergencyTimer target = this.listView_Registry.SelectedItem as EmergencyTimer;
            if (target == null)
            {
                return;
            }

            this.missionId = target.MissionId;
            var item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(missionId);
            if (item == null)
            {
                return;
            }

            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;

            this.mainWindow.treeView_Mission_CTM.Focus();
            item.Focus();

            this.textBlock_MissionName.Text = target.MissionName;
            if (target.IsDetailCondition)
            {
                this.radioButton_Threshold.IsChecked = true;
                this.button_AndOr.IsEnabled = true;
            }
            else
            {
                this.radioButton_RecieveCtm.IsChecked = true;
                this.button_AndOr.IsEnabled = false;
            }
            this.textBlock_CtmName.Text = target.CtmName;
            this.ctmId = target.CtmId;
            this.dateTimePicker_PeriodEnd.Value = Convert.ToDateTime(target.EndTime);
            this.textBox_Occurrences.Text = target.MaxCount.ToString();
            this.textBox_Interval.Text = target.IntervalMinute.ToString();
            this.textBlock_ElementName_01.Text = target.ElementName_01;
            this.elementId_01 = target.ElementId_01;
            if (target.ElementType_01 != 0)
            {
                setConditionType(this.comboBox_ElementCondition_01, target.ElementType_01);
                this.comboBox_ElementCondition_01.SelectedItem = target.ElementCondition_01;
                this.elementType_01 = target.ElementType_01;
            }
            this.textBox_ElementConditionValue_01.Text = target.ElementConditionValue_01;
            this.textBlock_ElementName_02.Text = target.ElementName_02;
            this.elementId_02 = target.ElementId_02;
            if (target.ElementType_02 != 0)
            {
                setConditionType(this.comboBox_ElementCondition_02, target.ElementType_02);
                this.comboBox_ElementCondition_02.SelectedItem = target.ElementCondition_02;
                this.elementType_02 = target.ElementType_02;
            }
            this.textBox_ElementConditionValue_02.Text = target.ElementConditionValue_02;
            this.button_AndOr.Content = string.IsNullOrEmpty(target.AndOr) ? "OR" : target.AndOr;
            
            foreach (SelectableObject<Person> cbObject in this.combobox_cbObjects.Items)
            {
                cbObject.IsSelected = false;
            }
            StringBuilder sbText = new StringBuilder();
            StringBuilder mailText = new StringBuilder();
            StringBuilder userIdText = new StringBuilder();
            // Wang Issue AISEMG-22 20190109 Start
            if (MailAddress_Value_List == null)
            {
                MailAddress_Value_List = new List<string>();
            }
            MailAddress_Value_List.Clear();
            // Wang Issue AISEMG-22 20190109 End
            foreach (var information in getUserNameList())
            {
                if (target.UserId.Contains(information.UserId))
                {
                    string userName = information.UserName;
                    foreach (SelectableObject<Person> cbObject in this.combobox_cbObjects.Items)
                    {
                        if (userName.Equals(cbObject.ObjectData.UserName))
                        {
                            cbObject.IsSelected = true;
                            sbText.AppendFormat("{0},", cbObject.ObjectData.UserName);
                            mailText.AppendFormat("{0};", information.MailAddress_Value);
                            userIdText.AppendFormat("{0},", information.UserId);

                            // Wang Issue AISEMG-22 20190109 Start
                            MailAddress_Value_List.Add(information.MailAddress_Value);
                            // Wang Issue AISEMG-22 20190109 End
                        }
                    }
                }
            }
            this.tbObjects.Text = sbText.ToString().Trim().TrimEnd(',');
            this.MailAddress_Value = mailText.ToString().Trim().TrimEnd(';');
            this.UserId = userIdText.ToString().Trim().TrimEnd(',');

            this.textBox_MailTitle.Text = target.MailSubject;
            this.textBox_MailMessage.Text = target.MailMessage;
            if (target.AddFile)
            {
                this.radioButton_AddCtmTrue.IsChecked = true;
            }
            else
            {
                this.radioButton_AddCtmFalse.IsChecked = true;
            }
        }

        //private void startEt()
        //{
        //    if (this.timers != null)
        //    {
        //        foreach (EmergencyTimer timer in this.timers)
        //        {
        //            timer.TimerStart();
        //        }
        //    }
        //}

        private ObservableCollection<EmergencyTimer> readEt()
        {
            ObservableCollection<EmergencyTimer> etList = new ObservableCollection<EmergencyTimer>();

            UserInfo user = LoginInfo.GetInstance().GetLoginUserInfo();
            string loginUserId = user.UserId;

            List<string> jsonList = new List<string>();

            CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
            client.LastInvokeCompleteHandler = true;
            client.UseMultipleRequest = true;
            client.AddParam("LoginUserId", loginUserId);
            client.CompleteHandler += resJson =>
            {
                jsonList = JsonConvert.DeserializeObject<List<string>>(resJson);
                foreach (string json in jsonList)
                {
                    EmergencyTimer et = new EmergencyTimer();
                    JToken etToken = JToken.Parse(json);
                    et.Id = etToken["Id"].ToString();
                    et.MissionId = etToken["MissionId"].ToString();
                    et.MissionName = etToken["MissionName"].ToString();
                    et.IsDetailCondition = (bool)etToken["IsDetailCondition"];
                    et.CtmId = etToken["CtmId"].ToString();
                    et.CtmName = etToken["CtmName"].ToString();
                    et.EndTime = etToken["EndTime"].ToString();
                    et.MaxCount = (int)etToken["MaxCount"];
                    et.IntervalMinute = (int)etToken["IntervalMinute"];
                    et.ElementName_01 = etToken["ElementName_01"].ToString();
                    et.ElementId_01 = etToken["ElementId_01"].ToString();
                    et.ElementType_01 = (int)etToken["ElementType_01"];
                    et.ElementCondition_01 = etToken["ElementCondition_01"].ToString();
                    et.ElementConditionValue_01 = etToken["ElementConditionValue_01"].ToString();
                    et.ElementName_02 = etToken["ElementName_02"].ToString();
                    et.ElementId_02 = etToken["ElementId_02"].ToString();
                    et.ElementType_02 = (int)etToken["ElementType_02"];
                    et.ElementCondition_02 = etToken["ElementCondition_02"].ToString();
                    et.ElementConditionValue_02 = etToken["ElementConditionValue_02"].ToString();
                    et.AndOr = etToken["AndOr"].ToString();
                    et.UserId = etToken["UserId"].ToString();
                    et.MailAddress = etToken["MailAddress"].ToString();
                    et.MailSubject = etToken["MailSubject"].ToString();
                    et.MailMessage = etToken["MailMessage"].ToString();
                    et.AddFile = (bool)etToken["AddFile"];

                    var list = AisUtil.GetCtmsFromCtmIds(new List<string>() { et.CtmId });
                    if (list.Count == 0)
                    {
                        continue;
                    }
                    et.Ctm = list[0];

                    etList.Add(et);
                }
            };

            client.Get(CmsUrl.GetInfomation());

            //string fileDirectory = System.IO.Path.Combine(
            //    System.Windows.Forms.Application.StartupPath, "Emergency");
            //string fileName = "EtList.csv";
            //string filePath = System.IO.Path.Combine(fileDirectory, fileName);
            //if (!File.Exists(filePath))
            //{
            //    return etList;
            //}
            //using (StreamReader sr = new StreamReader(filePath, Encoding.Default))
            //{
            //    while (-1 < sr.Peek())
            //    {
            //        string doc = sr.ReadLine();
            //        string[] row = doc.Split(',');
            //        EmergencyTimer et = new EmergencyTimer();
            //        et.Id = row[0];
            //        et.MissionId = row[1];
            //        et.MissionName = row[2];
            //        et.IsDetailCondition = bool.Parse(row[3]);
            //        et.CtmId = row[4];
            //        et.CtmName = row[5];
            //        et.EndTime = DateTime.Parse(row[6]);
            //        et.MaxCount = int.Parse(row[7]);
            //        et.IntervalMinute = int.Parse(row[8]);
            //        et.ElementName_01 = row[9];
            //        et.ElementId_01 = row[10];
            //        et.ElementType_01 = int.Parse(row[11]);
            //        et.ElementCondition_01 = row[12];
            //        et.ElementConditionValue_01 = row[13];
            //        et.ElementName_02 = row[14];
            //        et.ElementId_02 = row[15];
            //        et.ElementType_02 = int.Parse(row[16]);
            //        et.ElementCondition_02 = row[17];
            //        et.ElementConditionValue_02 = row[18];
            //        et.AndOr = row[19];
            //        et.MailAddress = row[20];
            //        et.MailSubject = row[21];
            //        et.MailMessage = row[22];
            //        et.AddFile = bool.Parse(row[23]);

            //        var list = AisUtil.GetCtmsFromCtmIds(new List<string>() { et.CtmId });
            //        et.Ctm = list[0];

            //        etList.Add(et);
            //    }
            //}
            return etList;
        }

        private void writeEt(ObservableCollection<EmergencyTimer> etList)
        {
            string fileDirectory = System.IO.Path.Combine(
                System.Windows.Forms.Application.StartupPath, "Emergency");
            if (!Directory.Exists(fileDirectory))
            {
                Directory.CreateDirectory(fileDirectory);
            }
            string fileName = "EtList.csv";
            string filePath = System.IO.Path.Combine(fileDirectory, fileName);
            using (StreamWriter sw = new StreamWriter(filePath, false, Encoding.Default))
            {
                foreach (var et in etList)
                {
                    string[] row = new string[]
                    {
                        et.Id,              // 0
                        et.MissionId,
                        et.MissionName,
                        et.IsDetailCondition.ToString(),
                        et.CtmId,

                        et.CtmName,         // 5
                        et.EndTime.ToString(),
                        et.MaxCount.ToString(),
                        et.IntervalMinute.ToString(),
                        et.ElementName_01,

                        et.ElementId_01,    // 10
                        et.ElementType_01.ToString(),
                        et.ElementCondition_01,
                        et.ElementConditionValue_01,
                        et.ElementName_02,

                        et.ElementId_02,    // 15
                        et.ElementType_02.ToString(),
                        et.ElementCondition_02,
                        et.ElementConditionValue_02,
                        et.AndOr,
                        
                        et.MailAddress,     // 20
                        et.MailSubject,
                        et.MailMessage,
                        et.AddFile.ToString()   // 22
                    };
                    string doc = string.Join(",", row);
                    sw.WriteLine(doc);
                }
            }
        }

        private void deleteEt(EmergencyTimer target, ObservableCollection<EmergencyTimer> etList)
        {
            //Console.WriteLine("### {0} - {1} Emergency stop.", target.MissionName, target.CtmName);
            //target.TimerStop();
            //target.Dispose();
            etList.Remove(target);
        }

        private void listView_Registry_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.listView_Registry.SelectedItems.Count < 1)
            {
                return;
            }

            EmergencyTimer target = this.listView_Registry.SelectedItem as EmergencyTimer;
            if (target == null)
            {
                return;
            }

            System.Windows.Forms.ContextMenuStrip cMenu = new System.Windows.Forms.ContextMenuStrip();

            // 削除
            System.Windows.Forms.ToolStripMenuItem menuItem_Delete = new System.Windows.Forms.ToolStripMenuItem();
            menuItem_Delete.Text = Properties.Resources.TEXT_DELETE;
            menuItem_Delete.Click += delegate
            {
                //db処理するので、削除

                deleteEt(target, this.timers);
                //writeEt(this.timers);
                CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
                string Json = JsonConvert.SerializeObject(target);
                client.CompleteHandler += resJson =>
                { };
                client.Put(CmsUrl.DeleteEmergencyMail(), Json);
            };
            cMenu.Items.Add(menuItem_Delete);

            cMenu.Show(System.Windows.Forms.Cursor.Position);
        }

        private void listView_Registry_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.listView_Registry.SelectedIndex = -1;
        }
        // No.493 Input control of period setting item. ----------------Start
        //通報期間用
        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        //発生回数・監視幅用(BackSpac,Deleteを許可)
        private void Validation_OnPreviewKeyDown2(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                // || e.Key == System.Windows.Input.Key.Back   // BackSpace
                // || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        private void comboBox_MailAddress_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        // Wang Issue AISEMG-31 20190111 Start
        private void button_Delete_ElementCond1_Click(object sender, RoutedEventArgs e)
        {
            this.textBlock_ElementName_01.Text = string.Empty;
            this.comboBox_ElementCondition_01.Items.Clear();
            this.textBox_ElementConditionValue_01.Text = string.Empty;
        }

        private void button_Delete_ElementCond2_Click(object sender, RoutedEventArgs e)
        {
            this.textBlock_ElementName_02.Text = string.Empty;
            this.comboBox_ElementCondition_02.Items.Clear();
            this.textBox_ElementConditionValue_02.Text = string.Empty;
        }
        // Wang Issue AISEMG-31 20190111 End

        // No.493 Input control of period setting item. ----------------End


    }
}
