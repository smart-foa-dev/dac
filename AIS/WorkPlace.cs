﻿using FoaCore.Common.Entity;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DAC.Model
{
    public class WorkPlace : CmsEntity
    {
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public string WorkPlaceId { get; set; }
        public string WorkPlaceName { get; set; }
        public List<string> Missions { get; set; }
        public List<string> WorkPlaces { get; set; }
    }
}
