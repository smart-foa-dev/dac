﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    /// <summary>
    /// MMS ツリービューノード JSONデータキー
    /// </summary>
    public static class MmsTvJsonKey
    {
        /// <summary>
        /// ID
        /// </summary>
        public const string Id = "id";
        /// <summary>
        /// 名前 (displayNname") / 多言語対応
        /// </summary>
        public const string DisplayName = "displayName";
        /// <summary>
        /// タイプ
        /// </summary>
        public const string Type = "type";
        /// <summary>
        /// 名前 (ツリービュー表示用)
        /// </summary>
        public const string Name = "name";
        /// <summary>
        /// 子ノード
        /// </summary>
        public const string Children = "children";

        /// <summary>
        /// 言語 (DisplayName, Commentの属性)
        /// </summary>
        public const string Lang = "lang";
        /// <summary>
        /// テキスト (DisplayName, Commentの値)
        /// </summary>
        public const string Text = "text";

        /// <summary>
        /// AHS : 管理カタログ内保存テキスト
        /// </summary>
        public const string Ahs = "ahs";
        /// <summary>
        /// MF : 管理カタログ内 AH-MF
        /// </summary>
        public const string Mf = "mf";
        /// <summary>
        /// FE : 管理カタログ内 AH-FE
        /// </summary>
        public const string Fe = "fe";
        /// <summary>
        /// CGS : 管理カタログ内保存テキスト
        /// </summary>
        public const string Cgs = "cgs";
        /// <summary>
        /// CTMS : 管理カタログ内保存テキスト
        /// </summary>
        public const string Ctms = "ctms";
        /// <summary>
        /// ホスト名(IPアドレス) : 管理カタログ内
        /// </summary>
        public const string Hostname = "hostname";
        /// <summary>
        /// ポート番号 : 管理カタログ内 (CG)
        /// </summary>
        public const string Port = "port";
        /// <summary>
        /// ポート番号 : 管理カタログ内 (AH-FE)
        /// </summary>
        public const string FePort = "fePort";
        /// <summary>
        /// ポート番号 : 管理カタログ内 (AH-MF)
        /// </summary>
        public const string MfPort = "mfPort";

        /// <summary>
        /// デバイス・ : 管理カタログ内
        /// </summary>
        public const string BlockNo = "blockNo";

        /// <summary>
        /// デザインコード(CD) : CTM,CTM GROUP, CTM ELEMENT
        /// </summary>
        public const string Cd = "cd";
        /// <summary>
        /// コメント (comment") / 多言語対応 : CTM,CTM GROUP, CTM ELEMENT
        /// </summary>
        public const string Comment = "comment";
        /// <summary>
        /// CTM コメント (CTM comment)
        /// </summary>
        public const string CommentFileVersion = "commentFileVersion";
        /// <summary>
        /// CTM コメント is using word file or not
        /// </summary>
        public const string UseCommentFile = "useCommentFile";
        /// <summary>
        /// ドメインID : CTM TOP
        /// </summary>
        public const string DomainId = "domainId";
        /// <summary>
        /// リビジョン : CTM TOP
        /// </summary>
        public const string Revision = "revision";
        /// <summary>
        /// ステータス : CTM TOP
        /// </summary>
        public const string Status = "status";
        /// <summary>
        /// ステータス2 : CTM TOP
        /// </summary>
        public const string Status2 = "status2";
        /// <summary>
        /// 作業フラグ : CTM TOP
        /// </summary>
        public const string WorkFlag = "workFlag";
        /// <summary>
        /// CTMクラス : CTM TOP
        /// </summary>
        public const string CtmClass = "ctmClass";
        /// <summary>
        /// CTMエレメントクラス : CTM ELEMENT
        /// </summary>
        public const string ElementClass = "elementClass";
        /// <summary>
        /// 単位(unit) / 多言語対応 : CTM ELEMENT
        /// </summary>
        public const string Unit = "unit";
        /// <summary>
        /// データ型 : CTM ELEMENT
        /// </summary>
        public const string Datatype = "datatype";
        /// <summary>
        /// 拡張子 : CTM ELEMENT (Bulky型のみ)
        /// </summary>
        public const string Extention = "extention";


        public const string Bulkysubtype = "bulkysubtype";
    }
}
