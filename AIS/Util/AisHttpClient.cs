﻿using FoaCore.Common.Control;
using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Windows;
using System.Windows.Threading;

namespace DAC.Util
{
    class AisHttpClient
    {
        private static readonly string GET = "GET";
        private static readonly string GET_STREAM = "GET_STREAM";
        private static readonly string PUT = "PUT";
        private static readonly string POST = "POST";
        private static readonly string POST_STREAM = "POST_STREAM";
        private static readonly string DELETE = "DELETE";

        private static readonly string FILE_DOWNLOAD = "FILE_DOWNLOAD";
        private static readonly string FILE_UPLOAD = "FILE_UPLOAD";
        private static readonly string SYNC_GET = "SYNC_GET";
        private static readonly string SYNC_POST = "SYNC_POST";
        private static readonly string SYNC_DELETE = "SYNC_DELETE";
        
        private string methodName;
        private string requestUrl;

        /// <summary>
        /// クッキーコンテナ
        /// </summary>
        private static readonly CookieContainer cookieJar = new CookieContainer();

        /// <summary>
        /// HttpRequestが完了した時のハンドラ
        /// </summary>
        public Action<string> CompleteHandler { get; set; }

        public Action<JObject[], int> CompleteHandlerStream { get; set; }

        private Boolean handleStreamZeroArray = false;

        /// <summary>
        /// HttpRequestで例外が発生した時のハンドラ
        /// </summary>
        public Action<Exception, object[]> ExceptionHandler { get; set; }

        /// <summary>
        /// HttpRequestで例外が発生した時のメッセージ表示フラグ
        /// </summary>
        public bool ShowErrorMessage = true;

        /// <summary>
        /// パラメータ
        /// </summary>
        // private NameValueCollection param;
        private List<KeyValuePair<string, string>> param;

        /// <summary>
        /// 送信データ
        /// </summary>
        private string postData;

        private byte[] binPostData;

        public bool NotShowLoadingImage;

        /// <summary>
        /// 完了時のイベントハンドラを最後に実行する為のフラグ
        /// UI側での処理が完了してからローディングを消す事が適当な為、
        /// CompleteHandlerを実行してから、ローディングの非表示処理を行っています。
        /// その為、連続でHttpRequestを行うと、2回目のRequestの時にローディングが表示されません。
        /// 2回目の表示→1回目の非表示が行われる為。
        /// このフラグをtrueにすると、
        /// ローディングの非表示処理の後にCompleteHandlerを実行するようになります。
        /// </summary>
        public bool LastInvokeCompleteHandler { get; set; }

        /// <summary>
        /// 一つのイベント(ボタンクリック、グリッドクリックなど)に対して、
        /// 複数回のHttpRequestを行う場合、まだ処理中のHttpRequestが存在しても
        /// すでにレスポンスが返ってきたHttpRequestがあるとローディングが消えてしまいます。
        /// このフラグをtrueにすると、ローディングは表示したままで、最後にレスポンスが返ってきた時に
        /// ローディングが非表示になります。
        /// LastInvokeCompleteHandlerも、複数回のHttpRequestを考慮したオプションですが、
        /// HttpRequestの順序を意識するLastInvokeCompleteHandlerに対して、
        /// UseMultipleRequestは、用途の異なるHttpRequestを同時に実行する為のオプションです。
        /// </summary>
        public bool UseMultipleRequest { get; set; }

        /// <summary>
        /// HttpRequest毎のキー
        /// </summary>
        private readonly string requestKey;

        /// <summary>
        /// 実行中のHttpRequestのキーを保持するコレクション
        /// </summary>
        private static HashSet<string> requestKeys = new HashSet<string>();

        public static WebProxy proxy = null;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="userControl"></param>
        public AisHttpClient(System.Windows.Controls.Control userControl)
        {
            param = new List<KeyValuePair<string, string>>();

            // リクエストキー
            requestKey = Membership.GeneratePassword(8, 0);
        }

        /// <summary>
        /// パラメータを追加します。
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddParam(string key, string value)
        {
            param.Add(new KeyValuePair<string, string>(key, value));
        }

        /// <summary>
        /// HTTP GET
        /// </summary>
        /// <param name="url"></param>
        public void Get(string url)
        {
            DoRequest(url, GET);
        }

        /// <summary>
        /// HTTP GETだがデータをStreamのままで処理したい場合
        /// に使う。現在の実装はResponseデータがJson Array
        /// の場合のみ対応。
        /// </summary>
        /// <param name="url"></param>
        /// <param name="handleStreamZeroArray"></param>
        public void GetStream(string url, Boolean handleStreamZeroArray = false)
        {
            this.handleStreamZeroArray = handleStreamZeroArray;
            DoRequest(url, GET_STREAM);
        }

        /// <summary>
        /// HTTP PUT
        /// </summary>
        /// <param name="url"></param>
        /// <param name="json"></param>
        public void Put(string url, string json)
        {
            PutPostDelete(PUT, url, json);
        }

        /// <summary>
        /// HTTP POST
        /// </summary>
        /// <param name="url"></param>
        /// <param name="json"></param>
        public void Post(string url, string json)
        {
            PutPostDelete(POST, url, json);
        }

        public void Post(string url, byte[] binData, Boolean skipAppendQuestionMark = false)
        {
            binPostData = binData;
            DoRequest(url, POST, skipAppendQuestionMark);
        }

        public void PostStream(string url, string json, Boolean handleZeroArray = false)
        {
            postData = json;
            this.handleStreamZeroArray = handleZeroArray;
            DoRequest(url, POST_STREAM);
        }



        /// <summary>
        /// HTTP DELETE
        /// </summary>
        /// <param name="url"></param>
        /// <param name="json"></param>
        public void Delete(string url, string json)
        {
            PutPostDelete(DELETE, url, json);
        }

        /// <summary>
        /// PUT, POST, DELETEの共通メソッド
        /// </summary>
        /// <param name="method"></param>
        /// <param name="url"></param>
        /// <param name="json"></param>
        private void PutPostDelete(string method, string url, string json)
        {
            postData = json;
            DoRequest(url, method);
        }

        /// <summary>
        /// HttpRequestを行います。
        /// </summary>
        /// <param name="url"></param>
        /// <param name="method"></param>
        private void DoRequest(string url, string method, bool skipAppendQuestionMark = false)
        {
            this.methodName = method;
            Start();
            try
            {
                // Query
                string query = GetQueryString();
                if (query.Length > 0)
                {
                    if (!skipAppendQuestionMark)
                    {
                        url = url.Contains("?") ? (url + "&" + query) : (url + "?" + query);
                    }
                }
                this.requestUrl = url;
                // HttpRequest
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Proxy = AisHttpClient.proxy;

                // Cookie
                req.CookieContainer = cookieJar;
                // SetCookie(req.CookieContainer, url);
                // Method
                req.Method = method;
                if (method == GET)
                {
                    // リソースに非同期要求
                    req.BeginGetResponse((result) =>
                    {
                        HandleResponse2(result);
                    }, req);

                }
                else if (method == GET_STREAM)
                {
                    req.Method = GET;
                    // リソースに非同期要求
                    req.BeginGetResponse((result) =>
                    {
                        HandleResponseStream(result);
                    }, req);
                }
                else if (method == POST_STREAM)
                {
                    req.Method = POST;
                    req.BeginGetRequestStream((result) =>
                    {
                        HandleRequestStreamStream(req, result);
                    }, null);
                }
                else {
                    // 非同期ストリームアクセス
                    req.BeginGetRequestStream((result) =>
                    {
                        HandleRequestStream(req, result);
                    }, null);
                }

            }
            catch (Exception e)
            {
                HandleException(e);
            }
        }

        /// <summary>
        /// ストリームを送信する際の非同期アクセスをハンドリングします。
        /// </summary>
        /// <param name="req"></param>
        /// <param name="result"></param>
        private void HandleRequestStream(HttpWebRequest req, IAsyncResult result)
        {
            try
            {
                using (Stream postStream = req.EndGetRequestStream(result))
                {
                    // byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    byte[] byteArray = binPostData != null ? binPostData : Encoding.UTF8.GetBytes(postData);
                    postStream.Write(byteArray, 0, byteArray.Length);
                    // レスポンス取得
                    req.BeginGetResponse((result2) =>
                    {
                        HandleResponse2(result2);
                    }, req);
                }
            }
            catch (Exception e)
            {
                HandleException(e, new object[] { req.Headers.Get(0) });

                if (ExceptionHandler != null)
                {
                    ExceptionHandler(e, new object[] { req.Headers.Get(0) });
                }

            }
        }

        private void HandleRequestStreamStream(HttpWebRequest req, IAsyncResult result)
        {
            try
            {
                using (Stream postStream = req.EndGetRequestStream(result))
                {
                    // byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    byte[] byteArray = binPostData != null ? binPostData : Encoding.UTF8.GetBytes(postData);
                    postStream.Write(byteArray, 0, byteArray.Length);
                    // レスポンス取得
                    req.BeginGetResponse((result2) =>
                    {
                        HandleResponseStream(result2);
                    }, req);
                }
            }
            catch (Exception e)
            {
                HandleException(e, new object[] { req.Headers.Get(0) });

                if (ExceptionHandler != null)
                {
                    ExceptionHandler(e, new object[] { req.Headers.Get(0) });
                }

            }
        }

        /// <summary>
        /// レスポンスを非同期で受信します。
        /// </summary>
        /// <param name="req"></param>
        /****
        private void HandleResponse(HttpWebRequest req)
        {
            string str = "";
            bool hasException = false;
            try
            {
                 using (HttpWebResponse res = (HttpWebResponse)req.GetResponse())
                {
                    // 2014-08-08 yakiyama 204 No-Content も受容されるように変更した。
                    if (res.StatusCode != HttpStatusCode.OK && res.StatusCode != HttpStatusCode.NoContent)
                    {
                        MessageBox.Show(res.StatusDescription);
                        return;
                    }

                    using (StreamReader reader = new StreamReader(res.GetResponseStream(), Encoding.UTF8))
                    {
                        // データ取得
                        str = reader.ReadToEnd();
                        // UIスレッドでハンドラ実行
                        if (LastInvokeCompleteHandler == false)
                        {
                            owner.Dispatcher.BeginInvoke(CompleteHandler, str);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                hasException = true;
                HandleException(e, new object[] { req.Headers.Get(0) });
                if (ExceptionHandler != null)
                {
                    owner.Dispatcher.BeginInvoke(ExceptionHandler, e, new object[] { req.Headers.Get(0) });
                }
            }
            finally
            {
                // Loading End.
                End();
                // 完了ハンドラを最後に実行する場合、例外が発生しなかった場合にのみ実行
                if (hasException == false && LastInvokeCompleteHandler)
                {
                    owner.Dispatcher.BeginInvoke(CompleteHandler, str);
                }
            }
        }
        ***/

        /// <summary>
        /// レスポンスを非同期で受信します。
        /// </summary>
        /// <param name="req"></param>
        private void HandleResponse2(IAsyncResult result)
        {
            string str = "";
            bool hasException = false;
            HttpWebRequest req = (HttpWebRequest)result.AsyncState;
            //req.Proxy = this.proxy;
            try
            {

                using (HttpWebResponse res = (HttpWebResponse)req.EndGetResponse(result))
                {
                    // 2014-08-08 yakiyama 204 No-Content も受容されるように変更した。
                    if (res.StatusCode != HttpStatusCode.OK && res.StatusCode != HttpStatusCode.NoContent)
                    {
                        MessageBox.Show(res.StatusDescription);
                        return;
                    }

                    using (StreamReader reader = new StreamReader(res.GetResponseStream(), Encoding.UTF8))
                    {
                        // データ取得
                        str = reader.ReadToEnd();
                        // UIスレッドでハンドラ実行
                        if (LastInvokeCompleteHandler == false)
                        {
                            if (CompleteHandler != null)
                            {
                                CompleteHandler(str);    
                            }
                            
                        }
                    }
                }
            }
            catch (WebException e)
            {
                hasException = true;
                // var code = new StreamReader(e.Response.GetResponseStream()).ReadToEnd();
                HandleException(e, new object[] { req.Headers.Get(0) });
                if (ExceptionHandler != null)
                {
                    ExceptionHandler(e, new object[] { req.Headers.Get(0) });
                }
            }
            finally
            {
                // Loading End.
                End();
                // 完了ハンドラを最後に実行する場合、例外が発生しなかった場合にのみ実行
                if (hasException == false && LastInvokeCompleteHandler)
                {
                    CompleteHandler(str);
                }
            }
        }

        private const int GET_STREAM_BUF_SIZE = 500;

        private void HandleResponseStream(IAsyncResult ar)
        {
            HttpWebRequest req = (HttpWebRequest)ar.AsyncState;
            //req.Proxy = CmsHttpClient.proxy;
            try
            {
                using (HttpWebResponse res = (HttpWebResponse)req.EndGetResponse(ar))
                {
                    // 2014-08-08 yakiyama 204 No-Content も受容されるように変更した。
                    if (res.StatusCode != HttpStatusCode.OK && res.StatusCode != HttpStatusCode.NoContent)
                    {
                        MessageBox.Show(res.StatusDescription);
                        return;
                    }

                    // 本当はStreamだけコールバックに渡したいが単純にStreamを渡しても読むことができない。
                    // .NETの作法としては Stream.BeginRead()を使って非同期に読むことになっているが、
                    // BeginRead()はバッファ（byte[]）を介してデータを読み出すため、逐次的にJsonTestReader
                    // でJsonをパースするような処理を書くのは困難。妥協案として、Streamの中身がJsonの配列
                    // というケースに絞って実装した。
                    Stream stream = res.GetResponseStream();
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    JsonTextReader jtr = new JsonTextReader(reader);
                    var jsonList = new JObject[GET_STREAM_BUF_SIZE];
                    int count = 0;

                    while (jtr.Read())
                    {
                        if (jtr.TokenType == JsonToken.StartObject)
                        {
                            JObject jObj = JObject.Load(jtr);
                            string key = key = jObj["id"].ToString();

                            jsonList[count] = jObj;
                            count++;

                            if (count == GET_STREAM_BUF_SIZE)
                            {
                                // CompleteHandlerStreamを複数回呼び出す必要があるため
                                // LastInvokeCompleteHandlerのチェックは行わない。
                                CompleteHandlerStream(jsonList, count);
                                jsonList = new JObject[GET_STREAM_BUF_SIZE];
                                count = 0;
                            }
                        }
                    }
                    if ((!this.handleStreamZeroArray && count > 0) || this.handleStreamZeroArray)
                    {
                        CompleteHandlerStream(jsonList, count);
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
            }
            finally
            {
                // Loading End.
                End();
            }
        }

        /// <summary>
        ///  例外をハンドリングします。
        /// </summary>
        /// <param name="e"></param>
        private void HandleException(Exception e, object[] param = null)
        {
            // ローディングの終了
            requestKeys.Clear();
            End();
            // エラーの表示
            if (ShowErrorMessage)
            {
                try
                {
                    if ((((System.Net.WebException)(e)).Response).ResponseUri.AbsolutePath.Contains("/cms/rest/mib/mission/start"))
                    {
                        FoaMessageBox.ShowError(e, param);
                    }
                    else
                    {
                        FoaMessageBox.ShowError2(e, this.methodName, this.requestUrl, param);
                    }

                    if ((((System.Net.WebException)(e)).Response).ResponseUri.AbsolutePath.Contains("/cms/rest/cgCtrl/ctm/stop"))
                    {
                        FoaMessageBox.ShowError(e, param);
                    }
                    else
                    {
                        FoaMessageBox.ShowError2(e, this.methodName, this.requestUrl, param);
                    }
                }
                catch(Exception)
                {
                    Dispatcher.CurrentDispatcher.BeginInvoke(new Action(() =>
                    {
                        FoaMessageBox.ShowError(e, param);
                        //ISSUE_NO.718 sunyi 2018/06/05 start
                        //エラー後、.exeを閉じる
                        Application.Current.Shutdown();
                        //ISSUE_NO.718 sunyi 2018/06/05 end
                    }));
                }
            }
        }

        /// <summary>
        /// クエリ文字列を取得します。
        /// </summary>
        /// <returns></returns>
        private string GetQueryString()
        {
            StringBuilder query = new StringBuilder();

            foreach (KeyValuePair<string, string> kvp in param)
            {
                query.AppendFormat("{0}={1}&",
                    HttpUtility.UrlEncode(kvp.Key),
                    HttpUtility.UrlEncode(kvp.Value));
            }
            return query.ToString();
        }

        /// <summary>
        /// クッキーの設定。
        /// </summary>
        /// <param name="url"></param>
        /// <param name="userInfo"></param>
        public static void SetCookie(string url, FoaCore.Common.Model.UserInfo userInfo)
        {
            Uri uri = new Uri(url);
            Uri trueUri = new Uri(uri.GetLeftPart(UriPartial.Authority));
            string userId = HttpUtility.UrlEncode(userInfo.UserId, Encoding.UTF8);
            cookieJar.Add(trueUri, new Cookie("userId", userId));
        }

        /// <summary>
        /// ローディングの開始
        /// </summary>
        private void Start()
        {
            if (UseMultipleRequest)
            {
                requestKeys.Add(requestKey);
            }

            // ローディング中のキー入力を無効にする為、イベントを登録
            // owner.isEnabled = false だとクリックしたコントロールのフォーカスが取れてしまう
            // -=は予防的に入れたくなるが、End()でちゃんとイベント解除しているし、
            // 仮に重複してイベントが登録された状態になった場合、
            // ここで解除しても-1,+1なので、重複しているという状態は変わらない。
            // 入れてしまうと仮にそういう状態になった時に、問題が分かりづらくなるので。
            //owner.PreviewKeyDown -= OwnerPreviewKeyDown;
        }

        /// <summary>
        /// ローディングの終了
        /// </summary>
        private void End()
        {
            if (UseMultipleRequest)
            {
                if (requestKeys.Contains(requestKey))
                {
                    requestKeys.Remove(requestKey);
                }
                if (requestKeys.Count > 0) return;
            }
        }

        /// <summary>
        /// 呼び出し元のコントロールのキー押下
        /// ローディング中のキー入力を一時的に無効にします。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OwnerPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            e.Handled = true;
        }

        /// <summary>
        /// ファイルダウンロード
        /// </summary>
        /// <param name="url"></param>
        /// <param name="fileName"></param>
        public void FileDownload(string url, string fileName, bool noBI = false)
        {
            this.noBeginInvoke = noBI;

            this.methodName = FILE_DOWNLOAD;
            Start();
            // url
            string query = GetQueryString();
            url = url.Contains("?") ? (url + "&" + query) : (url + "?" + query);
            if (url.Contains("block/test/file"))
            {
                string fixUrl = url.Replace("%3a",":").Replace("%5c","/");
                this.requestUrl = fixUrl;
            }
            else
            {
                this.requestUrl = url;
            }
            // WebClient
            WebClientEx client = new WebClientEx();
            client.Proxy = AisHttpClient.proxy;
            // Cookie
            client.CookieContainer = cookieJar;

            // If downloading the mission result, confirm the file we want to download first..
            if (url.Contains("mib/mission/result/download"))
            {
                System.Net.WebRequest req = System.Net.HttpWebRequest.Create(new Uri(url));
                req.Proxy = AisHttpClient.proxy;
                req.Method = "HEAD";
                try
                {
                    using (System.Net.WebResponse resp = req.GetResponse())
                    {
                        int ContentLength;
                        if (int.TryParse(resp.Headers.Get("Content-Length"), out ContentLength))
                        {
                            if (File.Exists(fileName) == true && IsFileOpen(fileName) == true) return; // Check if the file is currently opened or not..

                            // Download the file only if we can confirm the content.
                            // this is used in order to avoid the client create unnecessary 0kb empty file..
                            // Added by Dida, related with Fix no. 326 (2016/01/22)
                            client.DownloadProgressChanged += HandleDownloadProgress;
                            client.DownloadFileCompleted += HandleDownloadComplete;
                            client.DownloadFileAsync(new Uri(url), fileName);
                        }
                    }
                }
                catch
                {
                    // ローディングの終了
                    requestKeys.Clear();
                    End();
                    // Show error message..
                    FoaMessageBox.ShowError("MIB_E_022");
                }
            }
            else
            {
                // downlaod
                client.DownloadProgressChanged += HandleDownloadProgress;
                client.DownloadFileCompleted += HandleDownloadComplete;
                client.DownloadFileAsync(new Uri(url), fileName);
            }
        }

        private bool IsFileOpen(string path)
        {
            FileStream fs = null;
            try
            {
                fs = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None);
                return false;
            }
            catch (IOException)
            {
                // ローディングの終了
                requestKeys.Clear();
                End();
                // Show error message..
                FoaMessageBox.ShowError("MMS_E_136");
                return true;
            }
            finally
            {
                if (fs != null)
                    fs.Close();
            }
        }

        private volatile bool noBeginInvoke = false;

        /// <summary>
        /// ダウンロード中のイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void HandleDownloadProgress(object sender, DownloadProgressChangedEventArgs e)
        {
        }

        /// <summary>
        /// ダウンロード完了のイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void HandleDownloadComplete(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                HandleException(e.Error);
            }
            else
            {
                End();
                if (!noBeginInvoke)
                {
                    CompleteHandler("no result");
                }
                else
                {
                    CompleteHandler("no result");
                }
            }
        }

        /// <summary>
        /// ファイルアップロード
        /// </summary>
        /// <param name="url"></param>
        /// <param name="fileName"></param>
        public void FileUpload(string url, string fileName)
        {
            this.methodName = FILE_UPLOAD;
            Start();
            // url
            string query = GetQueryString();
            url = url.Contains("?") ? (url + "&" + query) : (url + "?" + query);
            this.requestUrl = url;
            // WebClient
            WebClientEx client = new WebClientEx();
            client.Proxy = AisHttpClient.proxy;
            // Cookie
            client.CookieContainer = cookieJar;

            // upload
            client.UploadProgressChanged += HandleUploadProgress;
            client.UploadFileCompleted += HandleUploadComplete;
            client.UploadFileAsync(new Uri(url), fileName);
        }

        /// <summary>
        /// アップロード中のイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void HandleUploadProgress(object sender, UploadProgressChangedEventArgs e)
        {
        }

        /// <summary>
        /// アップロード完了のイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void HandleUploadComplete(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                HandleException(e.Error);
            }
            else
            {
                End();
                CompleteHandler("no result");
            }
        }

        /// <summary>
        /// HTTP GET(同期アクセス)
        /// </summary>
        /// <param name="url"></param>
        public string SyncGet(string url)
        {
            this.methodName = SYNC_GET;
            Start();
            Stream stream = null;
            StreamReader streamReader = null;
            try
            {
                // Query
                string query = GetQueryString();
                url = url.Contains("?") ? (url + "&" + query) : (url + "?" + query);
                this.requestUrl = url;
                // HttpRequest
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Proxy = AisHttpClient.proxy;
                // Cookie
                req.CookieContainer = cookieJar;
                // リソースに非同期要求
                WebResponse res = req.GetResponse();
                if (res != null)
                {
                    stream = res.GetResponseStream();
                    streamReader = new StreamReader(stream, Encoding.GetEncoding("UTF-8"));
                    return streamReader.ReadToEnd();
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
            finally
            {
                if (streamReader != null)
                {
                    streamReader.Close();
                }
                else
                {
                    if (stream != null) stream.Close();
                }
                End();
            }
        }

        /// <summary>
        /// HTTP POST(同期アクセス)
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        public string SyncPost(string url, string data)
        {
            this.methodName = SYNC_POST;
            Start();
            //Stream stream = null;
            //StreamReader streamReader = null;
            try
            {
                // Query
                string query = GetQueryString();
                url = url.Contains("?") ? (url + "&" + query) : (url + "?" + query);
                this.requestUrl = url;
                postData = data;
                // HttpRequest
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Proxy = AisHttpClient.proxy;
                req.Method = POST;
                // Cookie
                req.CookieContainer = cookieJar;

                //// Create a request using a URL that can receive a post. 
                //WebRequest request = WebRequest.Create("http://www.contoso.com/PostAccepter.aspx ");
                //// Set the Method property of the request to POST.
                //request.Method = "POST";
                //// Create POST data and convert it to a byte array.
                //string postData = "This is a test that posts this string to a Web server.";

                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.
                req.ContentType = "application/x-www-form-urlencoded";
                // Set the ContentLength property of the WebRequest.
                req.ContentLength = byteArray.Length;

                // Get the request stream.
                using (Stream postStream = req.GetRequestStream())
                {
                    // byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    //byte[] byteArray = binPostData != null ? binPostData : Encoding.UTF8.GetBytes(postData);
                    postStream.Write(byteArray, 0, byteArray.Length);

                    //// リソースに非同期要求
                    //WebResponse res = req.GetResponse();
                    //// レスポンス取得
                    //if (res != null)
                    //{
                    //    stream = res.GetResponseStream();
                    //    streamReader = new StreamReader(stream, Encoding.GetEncoding("UTF-8"));
                    //    return streamReader.ReadToEnd();
                    //}
                    //else
                    //{
                    //    return string.Empty;
                    //}
                }
                //// Get the request stream.
                //Stream postStream = req.GetRequestStream();
                //// Write the data to the request stream.
                //postStream.Write(byteArray, 0, byteArray.Length);
                //// Close the Stream object.
                //postStream.Close();

                // Get the response.
                WebResponse response = req.GetResponse();
                // Display the status.
                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                if (response == null)
                {
                    return string.Empty;
                }

                string responseFromServer = string.Empty;
                // Get the stream containing content returned by the server.
                using (Stream dataStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(dataStream, Encoding.GetEncoding("UTF-8"));
                    responseFromServer = reader.ReadToEnd();
                }

                // Get the stream containing content returned by the server.
                //Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                //StreamReader reader = new StreamReader(dataStream, Encoding.GetEncoding("UTF-8"));
                // Read the content.
                //string responseFromServer = reader.ReadToEnd();
                // Display the content.
                //Console.WriteLine(responseFromServer);
                // Clean up the streams.
                //reader.Close();
                //dataStream.Close();
                if (response != null) response.Close();

                //MessageBox.Show("[" + responseFromServer + "]");
                return responseFromServer;

                //using (Stream postStream = req.GetRequestStream())
                //{
                //    // byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                //    //byte[] byteArray = binPostData != null ? binPostData : Encoding.UTF8.GetBytes(postData);
                //    postStream.Write(byteArray, 0, byteArray.Length);
                //    // リソースに非同期要求
                //    WebResponse res = req.GetResponse();
                //    // レスポンス取得
                //    if (res != null)
                //    {
                //        stream = res.GetResponseStream();
                //        streamReader = new StreamReader(stream, Encoding.GetEncoding("UTF-8"));
                //        return streamReader.ReadToEnd();
                //    }
                //    else
                //    {
                //        return string.Empty;
                //    }
                //}

            }
            catch (Exception)
            {
                return string.Empty;
            }
            finally
            {
                //if (streamReader != null)
                //{
                //    streamReader.Close();
                //}
                //else
                //{
                //    if (stream != null) stream.Close();
                //}
                End();
            }
        }

        /// <summary>
        /// HTTP DELETE(同期アクセス)
        /// </summary>
        /// <param name="url"></param>
        /// <param name="data"></param>
        public string SyncDelete(string url, string data)
        {
            this.methodName = SYNC_DELETE;
            Start();
            //Stream stream = null;
            //StreamReader streamReader = null;
            try
            {
                // Query
                string query = GetQueryString();
                url = url.Contains("?") ? (url + "&" + query) : (url + "?" + query);
                this.requestUrl = url;
                postData = data;
                // HttpRequest
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                req.Proxy = AisHttpClient.proxy;
                req.Method = DELETE;
                // Cookie
                req.CookieContainer = cookieJar;

                //// Create a request using a URL that can receive a post. 
                //WebRequest request = WebRequest.Create("http://www.contoso.com/PostAccepter.aspx ");
                //// Set the Method property of the request to POST.
                //request.Method = "POST";
                //// Create POST data and convert it to a byte array.
                //string postData = "This is a test that posts this string to a Web server.";

                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                // Set the ContentType property of the WebRequest.
                req.ContentType = "application/x-www-form-urlencoded";
                // Set the ContentLength property of the WebRequest.
                req.ContentLength = byteArray.Length;

                // Get the request stream.
                using (Stream postStream = req.GetRequestStream())
                {
                    // byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                    //byte[] byteArray = binPostData != null ? binPostData : Encoding.UTF8.GetBytes(postData);
                    postStream.Write(byteArray, 0, byteArray.Length);

                    //// リソースに非同期要求
                    //WebResponse res = req.GetResponse();
                    //// レスポンス取得
                    //if (res != null)
                    //{
                    //    stream = res.GetResponseStream();
                    //    streamReader = new StreamReader(stream, Encoding.GetEncoding("UTF-8"));
                    //    return streamReader.ReadToEnd();
                    //}
                    //else
                    //{
                    //    return string.Empty;
                    //}
                }
                //// Get the request stream.
                //Stream postStream = req.GetRequestStream();
                //// Write the data to the request stream.
                //postStream.Write(byteArray, 0, byteArray.Length);
                //// Close the Stream object.
                //postStream.Close();

                // Get the response.
                WebResponse response = req.GetResponse();
                // Display the status.
                //Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                if (response == null)
                {
                    return string.Empty;
                }

                string responseFromServer = string.Empty;
                // Get the stream containing content returned by the server.
                using (Stream dataStream = response.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(dataStream, Encoding.GetEncoding("UTF-8"));
                    responseFromServer = reader.ReadToEnd();
                }

                // Get the stream containing content returned by the server.
                //Stream dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.
                //StreamReader reader = new StreamReader(dataStream, Encoding.GetEncoding("UTF-8"));
                // Read the content.
                //string responseFromServer = reader.ReadToEnd();
                // Display the content.
                //Console.WriteLine(responseFromServer);
                // Clean up the streams.
                //reader.Close();
                //dataStream.Close();
                if (response != null) response.Close();

                //MessageBox.Show("[" + responseFromServer + "]");
                return responseFromServer;

                //using (Stream postStream = req.GetRequestStream())
                //{
                //    // byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                //    //byte[] byteArray = binPostData != null ? binPostData : Encoding.UTF8.GetBytes(postData);
                //    postStream.Write(byteArray, 0, byteArray.Length);
                //    // リソースに非同期要求
                //    WebResponse res = req.GetResponse();
                //    // レスポンス取得
                //    if (res != null)
                //    {
                //        stream = res.GetResponseStream();
                //        streamReader = new StreamReader(stream, Encoding.GetEncoding("UTF-8"));
                //        return streamReader.ReadToEnd();
                //    }
                //    else
                //    {
                //        return string.Empty;
                //    }
                //}

            }
            catch (Exception)
            {
                return string.Empty;
            }
            finally
            {
                //if (streamReader != null)
                //{
                //    streamReader.Close();
                //}
                //else
                //{
                //    if (stream != null) stream.Close();
                //}
                End();
            }
        }

        /// <summary>
        /// Cookieを使用可能にしたWebClient
        /// </summary>
        /// <see cref="http://so-zou.jp/software/tech/programming/c-sharp/network/web-client.htm"/>
        private class WebClientEx : System.Net.WebClient
        {
            private CookieContainer cookieContainer;

            public CookieContainer CookieContainer
            {
                get
                {
                    return cookieContainer;
                }
                set
                {
                    cookieContainer = value;
                }
            }

            protected override WebRequest GetWebRequest(Uri uri)
            {
                WebRequest webRequest = base.GetWebRequest(uri);

                if (webRequest is HttpWebRequest)
                {
                    HttpWebRequest httpWebRequest = (HttpWebRequest)webRequest;
                    httpWebRequest.CookieContainer = this.cookieContainer;
                }

                return webRequest;
            }
        }
    }
}
