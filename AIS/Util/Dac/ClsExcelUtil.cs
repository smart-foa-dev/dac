﻿using System.Diagnostics;
using ExcelApp = Microsoft.Office.Interop.Excel.Application;
using ExcelWorkbook = Microsoft.Office.Interop.Excel.Workbook;
using System.Runtime.InteropServices;
using System;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;

namespace DAC.Util.Dac
{
    public class ClsExcelUtil
    {
        /*
         * Cellが未設定かを確認
         */
        public static bool isEmpty(Excel.Range cell)
        {
            if (cell == null || cell.Value == null || cell.Value.Equals(""))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /**
         * シートの取得
         *  シート名から該当シートを検索して、シート番号を返す。
         */
        private static int getSheetNoByName(Excel.Sheets sheets, string name)
        {
            int sheetNo = 0;
            foreach (Excel.Worksheet sh in sheets)
            {
                if (name.Equals(sh.Name))
                {
                    return sheetNo + 1;
                }
                sheetNo += 1;
            }
            return -1;
        }

        /**
         * Conf用のro paramから設定値を取得する
         *  引数：キー
         *  戻り値：値
         */
        public static string getDACConfByConfName(Microsoft.Office.Interop.Excel.Workbook book, string sheetName, string confName)
        {
            string confValue = "";

            Excel.Worksheet noParamSheet = null;
            Excel.Range cell = null;

            // 該当シートの取得
            int sheetNo = getSheetNoByName(book.Sheets, sheetName);
            if (sheetNo > 0)
            {
                noParamSheet = book.Sheets[sheetNo];
            }

            int rowPosition = 1;
            while (true)
            {

                cell = noParamSheet.Cells[rowPosition, 1];

                if (isEmpty(cell))
                {
                    confValue = "";
                    break;
                }
                else
                {
                    string cellValue = (string)(cell.Value).ToString();
                    if (confName.Equals(cellValue))
                    {
                        cell = noParamSheet.Cells[rowPosition, 2];

                        if (cell.Value == null)
                        {
                            confValue = "";
                        }
                        else
                        {
                            confValue = (string)(cell.Value).ToString();
                        }

                        break;
                    }

                }

                rowPosition++;
            }


            return confValue;
        }
    }
}
