﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace DAC.Util.Dac
{
    public class ClsReadData
    {
        // AISファイル Paramシート名
        public static readonly string AIS_PARAM_SHEET_NAME = "param";
        public static readonly string AIS_PARAM_TEMP_NAME = "テンプレート名称";
        public static readonly string AIS_PARAM_ONLINE_NAME = "オンライン";

        // AISファイル Ro Paramシート名
        public static readonly string AIS_RO_PARAM_SHEET_NAME = "ro_param";


        // Sub DACシート名
        public static readonly string SUB_DAC_SHEET = "SubDAC";
        // Dacシート コード名
        public static readonly string DAC_SHEET_CODE = "Sheet1";

        // Sub DAC ファイル実行・未実行背景
        public static readonly Color EXCEC_SUB_DAC_COLOR = System.Drawing.Color.FromArgb(135, 206, 250);
        public static readonly Color UNEXCEC_SUB_DAC_COLOR = System.Drawing.Color.FromArgb(140, 140, 140);

        public static Color AFTER_RESULT_COLOR = System.Drawing.Color.FromArgb(204, 0, 0);

        // ActionPanel名
        public static readonly string CAL_ACTION_PANEL = "m_calActionPanel";
        public static readonly string SUB_DAC_ACTION_PANEL = "m_subDACActionPanel";

        public static readonly string RESULT_SHEET_NAME = "result";
        public static readonly string RESULT_SHEET_NAME2 = "演算結果データ";


        public static readonly string RESULT_COPY_SHEET_NAME = "dac_refere";
        public static readonly int RESULT_COPY_HEADER_COUNT = 11;


        public static readonly　string UNEXEC_SUB_DAC_FILE_KEY = "@@@";
    }
}
