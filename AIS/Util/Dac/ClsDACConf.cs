﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Util.Dac
{
    public class ClsDACConf
    {


        private string GRIP_IP_KEY      = "Grip-Server IP";
        private string GRIP_PORT_KEY    = "Grip-Server PORT";
        private string AIS_ENTRY_FOLDER = "登録フォルダ";
        private string GRIP_R2_FOLDER = "Grip-R2 Folder";
        private string DAC_MONITOR = "DAC MONITOR";
        private string TIMEZONE_ID = "TimezoneId";
        private string OFFLINE_MODE = "Offline Mode";
        private string OFFLINE_MACRO = "Offline Macro";

        private string USE_PROXY = "ProxyServer使用";
        private string PROXY_URI = "ProxyServerURI";
        private string ENABLE_MULTI_DAC = "MultiDac有効";

        public ClsDACConf(Microsoft.Office.Interop.Excel.Workbook book)
        {
            // Grip-Server
            GripIp = ClsExcelUtil.getDACConfByConfName(book, ClsReadData.AIS_RO_PARAM_SHEET_NAME, GRIP_IP_KEY);
            GripPort = ClsExcelUtil.getDACConfByConfName(book, ClsReadData.AIS_RO_PARAM_SHEET_NAME, GRIP_PORT_KEY);
            TimezoneId = ClsExcelUtil.getDACConfByConfName(book, ClsReadData.AIS_RO_PARAM_SHEET_NAME, TIMEZONE_ID);
            // Proxy情報
            string useProxyTmp = ClsExcelUtil.getDACConfByConfName(book, ClsReadData.AIS_PARAM_SHEET_NAME, USE_PROXY);
            if ( "true".Equals(useProxyTmp.ToLower()) )
            {
                UseProxy = true;
                ProxyURI = ClsExcelUtil.getDACConfByConfName(book, ClsReadData.AIS_PARAM_SHEET_NAME, PROXY_URI);
            }
            else{
                UseProxy = false;
                ProxyURI = "";
            }
        }

        public string GripIp{get;set;}

        public string GripPort{get;set;}

        public string TimezoneId { get; set; }

        // Use Proxy
        public bool UseProxy { get; set; }
        // Proxy URI
        public string ProxyURI { get; set; }
    }
}
