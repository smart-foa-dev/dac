﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Util.Dac
{
    public class ClsAttribObject
    {
        private string id;
        public string Id {
            get {
                return this.id;
            }
        }

        private string displayName;
        public string DisplayName {
            get {
                return this.displayName;
            }
        }

        private string pid;
        public string Pid
        {
            get
            {
                return this.pid;
            }
        }

        public ClsAttribObject(string pid, string id, string displayName)
        {
            this.id = id;
            this.pid = pid;
            this.displayName = displayName;
        }
    }
}
