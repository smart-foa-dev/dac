﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Model
{
    /// <summary>
    /// エレメント収集条件
    /// </summary>
    class ElementConditionData
    {
        public string Id { get; set; }
        public int Type { get; set; }
        public int Condition { get; set; }
        public string Value { get; set; }
    }
}
