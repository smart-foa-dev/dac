﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using FoaCore.Common.Util;

namespace DAC.Converter
{
    public class ExternalFileInfoConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (!values[0].GetType().FullName.Equals(typeof(string).FullName))
                return null;
            string filepath = (string)values[0];
            long time = (long)values[1];
            string tooltiptext = (string)values[0] + "\n" + UnixTime.ToDateTime2(time);
            return tooltiptext;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
