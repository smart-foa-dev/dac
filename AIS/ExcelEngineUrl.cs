﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Model
{
    static class ExcelEngineUrl
    {
        /// <summary>
        /// EEサーバのAPIのベースURL
        /// </summary>
        /// <returns></returns>
        private static string GetEEBaseUrl()
        {
            return string.Format("http://{0}:{1}/excelengine/rest/", AisConf.Config.CmsHost, AisConf.Config.CmsPort);
        }

        public static class Workplace
        {
            private static string GetWorkplaceBaseUrl()
            {
                return GetEEBaseUrl() + "workplace/";
            }

            public static string GetScopeSearchUrl()
            {
                return GetWorkplaceBaseUrl() + "getscopezip";
            }

            public static string GetExternalCsvFiles()
            {
                return GetWorkplaceBaseUrl() + "getExternalExcelCsvZip";
            }
        }

        public static class ExternalExcel
        {
            public static string GetExternalExcelBaseUrl()
            {
                return GetEEBaseUrl() + "externalfile";
            }

            public static string ListExternalExcelUrl()
            {
                return GetExternalExcelBaseUrl() + "/list";
            }
        }
    }
}
