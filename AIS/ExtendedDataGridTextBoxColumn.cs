﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAC.Model
{
    /// <summary>
    /// DataGridTextBoxColumnを継承してクラスを作成
    /// </summary>
    public class ExtendedDataGridTextBoxColumn : DataGridTextBoxColumn
    {
        //Paintメソッドをオーバーライドする
        protected override void Paint(Graphics g, Rectangle bounds, CurrencyManager source,
            int rowNum, Brush backBrush, Brush foreBrush, bool alignToRight)
        {
            //セルの値を取得する
            object cellValue = this.GetColumnValueAtRow(source, rowNum);
            if (cellValue != null)
            {
                //値が"0"のセルの前景色と背景色を変える
                foreach (string elementName in this.elementNameList)
                {
                    if ((string)cellValue == elementName && cellValue != DBNull.Value)
                    {
                        backBrush = new SolidBrush(Color.GhostWhite);
                    }
                }
            }

            //基本クラスのPaintメソッドを呼び出す
            base.Paint(g, bounds, source, rowNum, backBrush, foreBrush, alignToRight);
        }

        public List<string> elementNameList { get; set; }
    }
}
