﻿using DAC.Properties;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using DAC.Model.Util;
using DAC.AExcel;

namespace DAC.Model
{
    public static class AisConf
    {
        public const string BaseUrl = "http://{0}:{1}/cms/rest/";
        public const string GripServerBaseUrl = "http://{0}:{1}/grip/rest/";
        public const string ExcelEngineBaseUrl = "http://{0}:{1}/excelengine/rest/";
        public static Conf Config { get; set; }
        //public static readonly List<string> CatalogLangList = new List<string> { "ja", "en" };
        //public static readonly List<string> CatalogLangListForDisplay = new List<string> { "Japanese", "English" };
        public static readonly List<string> UiLangList = new List<string> { "ja", "en" };
        public static readonly List<string> UiLangListForDisplay = new List<string> { "Japanese", "English" };
        public static readonly List<string> CatalogLangList = new List<string> { "ja", "en", "de", "fr", "zh-tw", "zh-cn", "ko", "pt", "th", "ms", "in", "vi" };
        public static readonly List<string> CatalogLangListForDisplay = new List<string> { "Japanese", "English", "German", "French", "Traditional Chinese", "Simplified Chinese", "Korean", "Portuguese", "Thai", "Malay", "Indonesian", "Vietnamese" };
        public static readonly List<string> CatalogLangListForData = new List<string> { "Japanese", "English", "German", "French", "TraditionalChinese", "SimplifiedChinese", "Korean", "Portuguese", "Thai", "Malay", "Indonesian", "Vietnamese" };
        //public static readonly string BaseDir = AppDomain.CurrentDomain.BaseDirectory; // これを想定する。 c:\foa\ais\{ドメインID}\ 
        public static string BaseDir = AppDomain.CurrentDomain.BaseDirectory;
        public static string DacBaseDir = AppDomain.CurrentDomain.BaseDirectory;
        public static string DacRegistryFolderPath;
        public static string RegistryFolderPath;
        public static string DomainId;
        public static readonly string TmpFolderPath;
        public static string DacTmpFolderPath;
        public static string DacTemplateDownloadPath;
        public static readonly string ImportFolderPath;
        
        // Wang Issue of quick graph 20180709 Start
        public static readonly string OutputDir;
        public static readonly string DownloadDir;
        // Wang Issue of quick graph 20180709 End

        public static string UserId { get; set; }
        public static string Password { get; set; }
        public static bool IsErrorLogin { get; set; }
        public static string CatalogLang { get; set; }
        public static string UiLang { get; set; }

        public static string UserGuID { get; set; }

        public static bool IsExternal = false;
        public static string PHPRestRoot
        {
            get
            {
                return string.Format("http://{0}/php/webapp/rest", Config.PHPHost);
            }
        }
        static AisConf() {
            string env_FOA_HOME = System.Environment.GetEnvironmentVariable("FOAHOME"); // 入っていない。
            string env_FOADOMAINID = System.Environment.GetEnvironmentVariable("FOADOMAINID");
            if (env_FOA_HOME != null && env_FOADOMAINID != null)
            {                
                DacBaseDir = Path.Combine(env_FOA_HOME, "dac", env_FOADOMAINID);
                DomainId = env_FOADOMAINID;
                if(!Directory.Exists(DacBaseDir))
                {
                    Directory.CreateDirectory(DacBaseDir);
                }
            } else
            {
                DacBaseDir = Path.Combine(BaseDir,"dac");
            }
            logger.Info("[INFO] BaseDir=" + BaseDir + " FOA_HOME=" + env_FOA_HOME + " DOMAINID=" + env_FOADOMAINID);

            //RegistryFolderPath = System.IO.Path.Combine(BaseDir, "registry");
            RegistryFolderPath = System.IO.Path.Combine(BaseDir, "registry");
            DacRegistryFolderPath = System.IO.Path.Combine(DacBaseDir, "MyDacs");
            TmpFolderPath = System.IO.Path.Combine(BaseDir, "tmp");
            DacTmpFolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "dactmp","dacs");
            if(!Directory.Exists(DacTmpFolderPath))
            {
                Directory.CreateDirectory(DacTmpFolderPath);
            }
            DacTemplateDownloadPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "dactmp", "templates");
            if(!Directory.Exists(DacTemplateDownloadPath))
            {
                Directory.CreateDirectory(DacTemplateDownloadPath);
            }            
            ImportFolderPath = System.IO.Path.Combine(TmpFolderPath, "import");
            // Wang Issue of quick graph 20180709 Start
            OutputDir = Path.Combine(TmpFolderPath, "output");
            DownloadDir = Path.Combine(TmpFolderPath, "download");
            // Wang Issue of quick graph 20180709 End
        }

        /// <summary>
        /// Conf/Ais.conf 読み込み
        /// </summary>
        public static void LoadAisConf()
        {
            // var baseDir = AppDomain.CurrentDomain.BaseDirectory;
            var baseDir = BaseDir;
            var confPath = System.IO.Path.Combine(baseDir, @Settings.Default.ConfFile);

            using (StreamReader sr = new StreamReader(confPath))
            {
                string rte = sr.ReadToEnd().Replace("\\", "\\\\");
                Config = JsonConvert.DeserializeObject<Conf>(rte);
            }
            if(string.IsNullOrEmpty(Config.PHPHost))
            {
                Config.PHPHost = Config.CmsHost;
            }
            if (!CatalogLangList.Contains(Config.DefaultCatalogLang))
            {
                Config.DefaultCatalogLang = "ja";
            }

            if (Config.UseProxy == false || String.IsNullOrEmpty(Config.ProxyURI))
            {
                Config.UseProxy = false;
                Config.ProxyURI = "";
                FoaCore.Net.FoaHttpClient.useProxy = false;
            }
            else
            {
                FoaCore.Common.Net.CmsHttpClient.proxy = new System.Net.WebProxy(Config.ProxyURI, true);
                FoaCore.Net.FoaHttpClient.proxy = new System.Net.WebProxy(Config.ProxyURI, true);
                FoaCore.Net.FoaHttpClient.useProxy = true;
            }
            // Wang Issue of quick graph 20180709 Start
            Config.InitializePaths();
            // Wang Issue of quick graph 20180709 End
        }

        public class Conf
        {
            public string CmsHost;
            public string CmsPort = "60000";
            public string DefaultCatalogLang;
            public string DefaultUiLang;
            public int CleanPeriodDaysForEmergencyBulkyFile;
            //public string CmsUiFolderPath = @"C:\foa\cms-ui";
            public string CmsUiFolderPath = BaseDir + @"..\..\cms-ui\" + System.Environment.GetEnvironmentVariable("FOADOMAINID");
            //public string GripR2FolderPath = @"C:\foa\GripClient";
            public string GripR2FolderPath = BaseDir + @"..\..\GripClient\" + System.Environment.GetEnvironmentVariable("FOADOMAINID");
            public string GripServerHost;
            public string GripServerPort = "60000";
            public string PHPHost;
            public string CmsVersion;
            public string LoginAuthentication;
            public bool UpdateCheck = false;
            public string DacFilePath = BaseDir + @"..\..\dll\DAC_Template";
            public string AisFolderPath = BaseDir;// + @"..\..\Ais\" + System.Environment.GetEnvironmentVariable("FOADOMAINID");
            public string GripFolderPath = BaseDir + @"..\..\Grip\" + System.Environment.GetEnvironmentVariable("FOADOMAINID");
            public int Testing = 0;
            public bool UseProxy = false;
            public string ProxyURI;
            //ISSUE_NO.645 sunyi 2018/05/24 start
            //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
            //public int BulkyMaxCount = 100;
            public int BulkyMaxRowsCount = 100;
            public int BulkyMaxColsCount = 100;
            //ISSUE_NO.645 sunyi 2018/05/24 end

            // Wang Issue of quick graph 20180709 Start
            public string GraphTemplateDir;

            public void InitializePaths()
            {
                GraphTemplateDir = Path.Combine(AisUtil.TEMPLATE_DIR_PATH, DAC.Properties.Resources.TEXT_DEFAULT_QUICK_GRAPH);
            }
            // Wang Issue of quick graph 20180709 End
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(AisConf).Name);
    }
}
