﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    public static class FoaDatatype
    {
        public static Dictionary<int, string> name = new Dictionary<int, string>() {
            { 1, "STRING" },
            { 2, "BYTE" },
            { 3, "CHAR" },
            { 4, "SHORT" },
            { 5, "INT" },
            { 6, "FLOAT" },
            { 100, "BULKY" },
            { 200, "BIT" },
            { 201, "SJIS_STRING" },
        };
        public static Dictionary<string, int> key = new Dictionary<string, int>() {
            { "STRING", 1 },
            { "BYTE", 2 },
            { "CHAR", 3 },
            { "SHORT", 4 },
            { "INT", 5 },
            { "FLOAT", 6 },
            { "BULKY", 100 },
            { "BIT", 200 },
            { "SJIS_STRING", 201 }
        };

        //CHU Yimin　linkBulky 2018.4.20　Start
        //Add the sub bulky types 

        public static Dictionary<string, int> bulkysubtypekey = new Dictionary<string, int>() {
            { "NORMAL", 0},
            { "LINKBULKY", 1},
            { "VIDEOFOA", 2}
        };

        public static Dictionary<int, string> bulkysubtypename = new Dictionary<int, string>()
        {
            { 0, "NORMAL"},
            { 1, "LINKBULKY"},
            { 2, "VIDEOFOA"}
        };

        //CHU Yimin　linkBulky 2018.4.20　End

        public const int UNKNOWN = 0;
        public const int STRING = 1;
        public const int BYTE = 2;
        public const int CHAR = 3;
        public const int SHORT = 4;
        public const int INT = 5;
        public const int FLOAT = 6;
        public const int BULKY = 100;
        public const int BIT = 200;
        public const int SJIS_STRING = 201;

        //CHU Yimin　linkBulky 2018.4.20　Start
        //Add the sub bulky types 

        public const int NORMALBULKY = 0;
        public const int LINKBULKY = 1;
        public const int VIDEOFOA = 2;

        public static bool isBulky(int val)
        {
            return val / BULKY * BULKY == BULKY ? true : false;
        }

        public static bool isBulky(string name)
        {
            bool isbulky = false;

            name = name.ToUpper();

            if (bulkysubtypekey.ContainsKey(name))
            {
                isbulky = true;
            }
            else
            {
                int val;
                if (key.TryGetValue(name, out val))
                {
                    if (val == BULKY)
                        isbulky = true;
                }
            }
            return isbulky;
        }

        public static int getBulkySubtype(int val)
        {
            return val % BULKY;
        }
 
        //CHU Yimin　linkBulky 2018.4.20　End

       
        
        public static int Name2Val(string name)
        {
            int val;
            //CHU Yimin　linkBulky 2018.4.20　Start
            //Add the sub bulky types 
            if (bulkysubtypekey.TryGetValue(name, out val))
            {
                return BULKY + val;
            }
            //CHU Yimin　linkBulky 2018.4.20　End

            key.TryGetValue(name, out val);
            return val;
        }

        public static string Val2Name(int val)
        {
            string namex;
            //CHU Yimin　linkBulky 2018.4.20　Start
            //Add the sub bulky types 
            if (isBulky(val))
            {
                if (val != BULKY)
                {
                    bulkysubtypename.TryGetValue(getBulkySubtype(val), out namex);
                    return namex;
                }
            }
            //CHU Yimin　linkBulky 2018.4.20　End
            name.TryGetValue(val, out namex);
            return namex;
        }
    }
   
    public class FoaDeviceTagtype
    {
        public static Dictionary<int, string> name = new Dictionary<int, string>() {
            { 0, "PLC" },
            { 1, "TEXT" },
            { 2, "BULKY" }
        };
        public static Dictionary<string, int> key = new Dictionary<string, int>() {
            { "PLC", 0 },
            { "TEXT", 1 },
            { "BULKY", 2 }
        };

        public const int PLC = 0;
        public const int TEXT = 1;
        public const int BULKY = 2;
    }
    public class FoaDeviceTagtypeName
    {
        public const string PLC = "PLC";
        public const string TEXT = "TEXT";
        public const string BULKY = "BULKY";
    }

    public class FoaDeviceTagCharset
    {
        public static Dictionary<int, string> name = new Dictionary<int, string>() {
            { 0, "UTF8" },
            { 1, "SJIS" }
        };
        public static Dictionary<string, int> key = new Dictionary<string, int>() {
            { "UTF8", 0 },
            { "SJIS", 1 }
        };

        public const int UTF8 = 0;
        public const int SJIS = 1;
    }
    public class FoaDeviceTagCharsetName
    {
        public const string UTF8 = "UTF8";
        public const string SJIS = "SJIS";
    }
}
