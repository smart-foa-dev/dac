﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    /// <summary>
    /// CtmElementType.Typesのアクセス用インデックス
    /// </summary>
    public class CtmElementTypeIndex
    {
        public const int String = 0;
        public const int Byte = 1;
        public const int Char = 2;
        public const int Short = 3;
        public const int Int = 4;
        public const int Float = 5;
        public const int Bulky = 6;
        //public const int LinkBulky = 7;
        //public const int VideoFOA = 8;
    }
    /// <summary>
    /// CtmElementType.Typesの各型の情報インデックス
    /// </summary>
    public class CtmElementTypeInfo
    {
        public const string Name = "name";
        public const string Value = "value";
    }

    /// <summary>
    /// CTM Element のタイプ
    /// </summary>
    public static class CtmElementType
    {
        /// <summary>
        /// タイプ情報リスト
        /// </summary>
        public static List<Dictionary<string, Object>> Types = new List<Dictionary<string, Object>>() {
            // string型 (文字列)
            new Dictionary<string, Object>() {
                {CtmElementTypeInfo.Name, "string"},
                {CtmElementTypeInfo.Value, 1}
            },
            // byte型 (数値)
            new Dictionary<string, Object>() {
                {CtmElementTypeInfo.Name, "byte"},
                {CtmElementTypeInfo.Value, 2}
            },
            // char型 (アスキー文字値)
            new Dictionary<string, Object>() {
                {CtmElementTypeInfo.Name, "char"},
                {CtmElementTypeInfo.Value, 3}
            },
            // short型 (数値)
            new Dictionary<string, Object>() {
                {CtmElementTypeInfo.Name, "short"},
                {CtmElementTypeInfo.Value, 4}
            },
            // int型 (数値)
            new Dictionary<string, Object>() {
                {CtmElementTypeInfo.Name, "int"},
                {CtmElementTypeInfo.Value, 5}
            },
            // float型 (数値)
            new Dictionary<string, Object>() {
                {CtmElementTypeInfo.Name, "float"},
                {CtmElementTypeInfo.Value, 6}
            },
            // bulky型 (文字列:ファイルパス)
            new Dictionary<string, Object>() {
                {CtmElementTypeInfo.Name, "bulky"},
                {CtmElementTypeInfo.Value, 100}
            }
        };


        public static List<Dictionary<string, Object>> BulkySubTimes = new List<Dictionary<string, Object>>() {
            new Dictionary<string, Object>() {
                { CtmElementTypeInfo.Name, "normal"},
                { CtmElementTypeInfo.Value, 0}
            },
            new Dictionary<string, Object>() {
                { CtmElementTypeInfo.Name, "linkbulky"},
                { CtmElementTypeInfo.Value, 1}
            },
            new Dictionary<string, Object>() {
                { CtmElementTypeInfo.Name, "videofoa"},
                { CtmElementTypeInfo.Value, 2}            
            }       
        };


        /// <summary>
        /// エレメントのタイプ値(dataTypeの値)から、Typesリストのインデックスを取得する
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static int GetIndexByValue(int value)
        {
            int index = -1;
            for (int i = 0; i < Types.Count; i++)
            {
                if (Types[i][CtmElementTypeInfo.Value].Equals(value))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        /// <summary>
        /// エレメントのタイプ名("string","byte", ...)から、Typesリストのインデックスを取得する
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int GetIndexByName(string name)
        {
            int index = -1;
            for (int i = 0; i < Types.Count; i++)
            {
                if (Types[i][CtmElementTypeInfo.Name].Equals(name))
                {
                    index = i;
                    break;
                }
            }
            return index;
        }

        /// <summary>
        /// エレメントのタイプ名("string","byte", ...)から、CTM定義JSONに入るdatatype値を取得する
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static int GetValueByName(string name)
        {
            int value = -1;
            for (int i = 0; i < Types.Count; i++)
            {
                if (Types[i][CtmElementTypeInfo.Name].Equals(name))
                {
                    value = (int)Types[i][CtmElementTypeInfo.Value];
                    break;
                }
            }
            return value;
        }

        /// <summary>
        /// エレメントのタイプ値(dataTypeの値)から、エレメントのタイプ名("string","byte", ...)を取得する
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetNameByValue(int value)
        {
            string name = "";
            for (int i = 0; i < Types.Count; i++)
            {
                if (Types[i][CtmElementTypeInfo.Value].Equals(value))
                {
                    name = (string)Types[i][CtmElementTypeInfo.Name];
                    break;
                }
            }
            return name;
        }

        /// <summary>
        /// エレメントのタイプ値(dataTypeの値)が、正しい値かチェックする
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static Boolean IsValidDataTypeValue(int value)
        {
            return GetNameByValue(value).Equals("") ? false : true;
        }
    }
}
