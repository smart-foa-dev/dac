﻿using DAC.View;
using FoaCore.Common.Util;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
//using System.Windows.Shapes;
using System.IO;
using DAC.AExcel;
using DAC.Model.Util;
using FoaCore.Common.Net;
using System.Threading;
using System.Data;
using DAC.Util;
using DAC.View.Helpers;
using FoaCore.Common;
using FoaCore.Common.Control;
using Xceed.Wpf.Toolkit;
using Grip.Net;
using System.Reflection;
using FoaCoreCom.ais.retriever;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_MultiStatusMonitor.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_MultiStatusMonitor : BaseControl
    {
        private bool isFirstFile;
        private string excelFilePath;

        public bool isDragCtmName;
        public Mission currentSelectedMission;

        private List<CtmObject> ctms = new List<CtmObject>();

        // Input parameter for DAC Excel file..
        private Dictionary<string, Mission> missionDict = new Dictionary<string, Mission>();
        private Dictionary<Mission, List<CtmObject>> missionCtmDict = new Dictionary<Mission, List<CtmObject>>();
        private Dictionary<CtmObject, List<CtmElement>> missionCtmElementDict = new Dictionary<CtmObject, List<CtmElement>>();

        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05
        private List<string> missionList = new List<string>();

        private Dictionary<string, int> conditionPatterns = new Dictionary<string, int>();
        private Dictionary<string, List<ElementConditionData>> conditionLists = new Dictionary<string, List<ElementConditionData>>();
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 end
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 start
        //吹き出し追加
        private Dictionary<string, Dictionary<ElementConditionData, string>> elementConditions = new Dictionary<string, Dictionary<ElementConditionData, string>>();
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 end

        //AISMM No.85 sunyi 20181211 start
        //内容修正、GripMission条件表示
        private Dictionary<string, List<ElementConditionData>> conditionLists_Grip = new Dictionary<string, List<ElementConditionData>>();
        private Dictionary<string, Dictionary<ElementConditionData, string>> elementConditions_Grip = new Dictionary<string, Dictionary<ElementConditionData, string>>();
        private Dictionary<string, int> conditionPatterns_Grip = new Dictionary<string, int>();
        private List<string> missionList_Grip = new List<string>();
        List<string> ctmIdList = new List<string>();
        //AISMM No.85 sunyi 20181211 end

        private Mission forCtmDirect;

        private string missionId = string.Empty;

		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
		// 前回選択されたミッションID
        private string preSelectedMissionId = string.Empty;
		// エクセルのオンライングラフID => ファイルID
        private string OnlineId = string.Empty;
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End

        public UserControl_MultiStatusMonitor(bool isFirstFile = true, string filePath = "")
        {
            InitializeComponent();
			// BugFix AIS_MM.99-2 20190611 yakiyama start.
			//AisTvRefDicMission.CallBackDeleteMissionClick = () => { AisTvRefDicElement.JsonItemSource = "[]"; };
			// BugFix AIS_MM.99-2 20190611 yakiyama end.

            this.Mode = ControlMode.MultiStatusMonitor;

            setKeyValuePair();
            setComboBoxItem();

            this.TEMPLATE = "MultiStatusMonitor.xlsm";

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }

            this.forCtmDirect = new Mission();
            this.forCtmDirect.Id = "ForCtmDirect";

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.search_Start };
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.search_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }
        }

        #region Read Excel File

        //AISMM No.85 sunyi 20181214 start
        //内容修正、GripMission条件表示
        private void readExcelFileStep3(JToken workplaceJson)
        {
            // AISを修正処理、画面にデータベースから検索結果を表示する
            CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
            client.CompleteHandler += (resJson) =>
            {
                // ディフォルート選択のMissionId
                var firstFlag = true;

                //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                //ミッションタブのミッションが選択状態になるように修正
                var firstGripMission = false;
                //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end
                var firstMissionId = "";

                JObject tmpJson = JObject.Parse(resJson);
                JObject tempWorkPlaceJson = JObject.Parse(resJson);
                JArray tempMissionJson = (JArray)tempWorkPlaceJson["mission_data"];

                foreach (JObject mission in tempMissionJson)
                {
                    JToken tmpMission = mission.DeepClone();

                    tmpMission["ctms"] = new JArray();
                    tmpMission["elements"] = new JArray();
                    var missionId = mission["id"].ToString();

                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 start
                    //条件付けエレメントのIDをJsonTreeViewに渡す
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 start
                    //吹き出し追加
                    setElementNameWithCondition_Mission(this.conditionLists);
                    setElementNameWithCondition_Grip(this.conditionLists_Grip);
                    Dictionary<string, string> tooltips = new Dictionary<string, string>();
                    //foreach (var condition in this.conditionLists)
                    //{
                    //    foreach (ElementConditionData a in condition.Value)
                    //    {
                    //        tmp.Add(a.Id);
                    //    }
                    //}
                    foreach (var elementCondition in this.elementConditions)
                    {
                        foreach (var element in elementCondition.Value)
                        {
                            foreach (var conditionPattern in conditionPatterns)
                            {
                                if (conditionPattern.Key.Equals(elementCondition.Key))
                                {
                                    tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                                }
                            }
                        }
                    }
                    //AISMM No.85 sunyi 20181211 start
                    //内容修正、GripMission条件表示
                    foreach (var elementCondition in this.elementConditions_Grip)
                    {
                        foreach (var element in elementCondition.Value)
                        {
                            foreach (var conditionPattern in conditionPatterns_Grip)
                            {
                                if (conditionPattern.Key.Equals(elementCondition.Key))
                                {
                                    tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                                }
                            }
                        }
                    }
                    //AISMM No.85 sunyi 20181211 end
                    this.AisTvRefDicElement.conditionLists = tooltips;
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 end
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 end

                    this.AisTvRefDicMission.AisCheckFlag = 2;
                    this.AisTvRefDicMission.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                    this.AisTvRefDicMission.CheckChangedEvent += this.AisTvRefDicMissionCheckChanged;
                    this.AisTvRefDicMission.JsonItemSourceAdd = tmpMission.ToString();
                    // Processing On Service 検証＃4　dn 2018/09/28 start
                    if (firstFlag)
                    {
                        firstMissionId = missionId;
                        //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                        //ミッションタブのミッションが選択状態になるように修正
                        if (int.Parse(tmpMission["missionNewType"].ToString()) == 0)
                        {
                            firstGripMission = true;
                        }
                        //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end
                        firstFlag = false;
                    }
                    // Processing On Service 検証＃4 dn 2018/09/28 end

                    this.AisTvRefDicCtm.AisCheckFlag = 2;
                    this.AisTvRefDicCtm.MissionId = missionId;
                    this.AisTvRefDicCtm.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                    this.AisTvRefDicCtm.CheckChangedEvent += this.AisTvRefDicCtmCheckChanged;
                    this.AisTvRefDicCtm.JsonItemSourceAdd = mission["ctms"].ToString();

                    this.AisTvRefDicElement.AisCheckFlag = 2;
                    this.AisTvRefDicElement.MissionId = missionId;

                    this.AisTvRefDicElement.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                    this.AisTvRefDicElement.CheckChangedEvent += this.AisTvRefDicElementCheckChanged;
                    this.AisTvRefDicElement.JsonItemSourceAdd = mission["elements"].ToString();
                }
                // Processing On Service 検証＃4　dn 2018/09/28 start
                //一番上のミッションを選択した状態にする
                this.AisTvRefDicMission.GetItem(firstMissionId).IsSelected = true;

                //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                //ミッションタブのミッションが選択状態になるように修正
                if (firstGripMission)
                {
                    this.DataSource = DataSourceType.GRIP;
                }
                this.missionId = firstMissionId;
                setMission();
                //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end
                // Processing On Service 検証＃4 dn 2018/09/28 end
            };
            client.Post(CmsUrl.GetAisSelect(), workplaceJson.ToString());
            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 end
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End
        }

        private void readExcelFileStep2(JToken workplaceJson)
        {
            // 個別に指定する場合は、既存の設定をサーバから再取得
            CmsHttpClient ctmGetClient = new CmsHttpClient(Application.Current.MainWindow);
            foreach (string ctmId in ctmIdList)
            {
                ctmGetClient.AddParam("id", ctmId);
                // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
            }

            ctmGetClient.CompleteHandler += ctmJson =>
            {
                // this.missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);
                List<CtmObject> ctmList = JsonConvert.DeserializeObject<List<CtmObject>>(ctmJson);
                foreach (var ctm in ctmList)
                {
                    this.ctms.Add(ctm);
                }
                readExcelFileStep3(workplaceJson);
            };
            ctmGetClient.ExceptionHandler += (e, str) =>
            {
                readExcelFileStep3(workplaceJson);
            };
            ctmGetClient.Get(CmsUrlMms.Ctm.List());
        }

        private void readExcelFileStep1(JToken workplaceJson)
        {
            int count = 0;

            //エレメント条件を取得
            if (missionList_Grip.Count > 0)
            {
                foreach (string id2 in missionList_Grip)
                {
                    // 個別に指定する場合は、既存の設定をサーバから再取得
                    CmsHttpClient missionCtmGetClient2 = new CmsHttpClient(Application.Current.MainWindow);
                    missionCtmGetClient2.AddParam("id", id2);
                    // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
                    missionCtmGetClient2.CompleteHandler += missionCtmJson2 =>
                    {
                        var missionCtms2 = JsonConvert.DeserializeObject<GripMissionCtm>(missionCtmJson2);
                        GetElementConditionDatas_G(missionCtms2);
                        count++;
                        if (count.Equals(missionList_Grip.Count))
                        {
                            readExcelFileStep2(workplaceJson);
                        }
                    };
                    missionCtmGetClient2.ExceptionHandler += (e, str) =>
                    {
                        count++;
                        if (count.Equals(missionList_Grip.Count))
                        {
                            readExcelFileStep2(workplaceJson);
                        }
                    };
                    missionCtmGetClient2.Get(GripUrl.Mission.Base());
                }
            }
            else
            {
                readExcelFileStep2(workplaceJson);
            }
        }
        //AISMM No.85 sunyi 20181214 end

        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string paramSheetName = DAC.Util.Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet paramSheet = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);

                if (paramSheet != null)
                {
                    Dictionary<string, string> paramMap = readParam(paramSheet);
                    if (paramMap.ContainsKey("更新周期"))
                    {
                        // BugFix AIS_MM.140 20191021 yakiyama start
                        this.comboBox_Interval.Text = paramMap["更新周期"];
                        // BugFix AIS_MM.140 20191021 yakiyama end
                    }
                    if (paramMap.ContainsKey("検索タイプ"))
                    {
                        if (paramMap["検索タイプ"].Equals("ON"))
                        {
                            this.radioButton_1.IsChecked = false;
                            this.radioButton_2.IsChecked = true;
                            DateTime dtStart = new DateTime();
                            if (DateTime.TryParse(paramMap["収集開始日時"], out dtStart))
                            {
                                this.search_Start.Value = dtStart;
                            }

                            DateTime dtEnd = new DateTime();
                            if (DateTime.TryParse(paramMap["収集終了日時"], out dtEnd))
                            {
                                this.search_End.Value = dtEnd;
                            }
                        }
                        else
                        {
                            this.radioButton_1.IsChecked = true;
                            this.radioButton_2.IsChecked = false;
                            if (paramMap.ContainsKey("取得期間"))
                            {
                                // Processing On Server 検証＃4　dn 2018/09/28 start
                                // this.comboBox_GetPeriod.Text = paramMap["取得期間"];
                                // workplace指定可能場合、[param]シートの取得期間は0で設定するため
                                if ("0".Equals(paramMap["取得期間"]))
                                {
                                    // BugFix AIS_MM.140 20191021 yakiyama start
                                    this.comboBox_GetPeriod.Text = "1時間前";
                                    // BugFix AIS_MM.140 20191021 yakiyama end
                                }
                                else
                                {
                                    // BugFix AIS_MM.140 20191021 yakiyama start
                                    this.comboBox_GetPeriod.Text = paramMap["取得期間"];
                                    // BugFix AIS_MM.140 20191021 yakiyama end
                                }
                                // Processing On Server　検証＃4　 dn 2018/09/28 end
                            }
                        }
                    }

                    if (paramMap.ContainsKey("収集タイプ") &&
                        !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                    {
                        switch (paramMap["収集タイプ"])
                        {
                            case "CTM":
                                this.DataSource = DataSourceType.CTM_DIRECT;
                                break;
                            case "ミッション":
                                this.DataSource = DataSourceType.MISSION;
                                break;
                            case "GRIP":
                                this.DataSource = DataSourceType.GRIP;
                                break;
                            default:
                                break;
                        }
                    }

                    // set SelectedMission
                    if (paramMap.ContainsKey("ミッションID") &&
                        !string.IsNullOrEmpty(paramMap["ミッションID"]))
                    {
                        if (this.DataSource == DataSourceType.MISSION)
                        {
                            string missionId = paramMap["ミッションID"];
                            Mission mission = AisUtil.GetMissionFromId(missionId);
                            this.SelectedMission = mission;
                        }
                        else if (this.DataSource == DataSourceType.GRIP)
                        {
                            string missionId = paramMap["ミッションID"];
                            GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                            this.SelectedGripMission = mission;
                        }
                    }

                    string missionSheetName = "ミッション";
                    SpreadsheetGear.IWorksheet missionSheet = AisUtil.GetSheetFromSheetName_SSG(book, missionSheetName);
                    if (missionSheet != null)
                    {
                        readMissionStructure(missionSheet);
                    }

					// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
                    string roParamSheetName = DAC.Util.Keywords.RO_PARAM_SHEET;
                    SpreadsheetGear.IWorksheet roParamSheet = AisUtil.GetSheetFromSheetName_SSG(book, roParamSheetName);

                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 start
                    //条件付けエレメントのIDをJsonTreeViewに渡す
                    string missionSheetName_M = "ミッション_MISSION";
                    SpreadsheetGear.IWorksheet missionSheet_M = AisUtil.GetSheetFromSheetName_SSG(book, missionSheetName_M);
                    readMissionStructure_M(missionSheet_M);

                    string missionSheetName_G = "ミッション_GRIP";
                    SpreadsheetGear.IWorksheet missionSheet_G = AisUtil.GetSheetFromSheetName_SSG(book, missionSheetName_G);
                    readMissionStructure_G(missionSheet_G);

                    JToken workplaceJson = new JObject();

                    if (roParamSheet != null)
                    {
                        SpreadsheetGear.IRange paramCells = roParamSheet.Cells;

                        for (int i = 1; i <= 10; i++)   // とりあえず10行ほど検索
                        {
                            // null check
                            if (paramCells[i, 0].Text == null)
                            {
                                continue;
                            }

                            // 更新周期
                            if (paramCells[i, 0].Text == "WorkPlaceID")
                            {
                                workplaceJson["work_place_id"] = paramCells[i, 1].Text;
                                break;
                            }
                        }
                    }

                    //AISMM No.85 sunyi 20181211 start
                    //内容修正、GripMission条件表示
                    ////エレメント条件を取得
                    //foreach(string id in missionList)
                    //{
                    //    // 個別に指定する場合は、既存の設定をサーバから再取得
                    //    CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                    //    missionCtmGetClient.AddParam("missionId", id);
                    //    // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
                    //    missionCtmGetClient.CompleteHandler += missionCtmJson =>
                    //    {
                    //        var missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);
                    //        GetElementConditionDatas(missionCtms);
                    //    };
                    //    missionCtmGetClient.Get(CmsUrl.GetMissionCtmUrl());
                    //}

                    //// AISを修正処理、画面にデータベースから検索結果を表示する
                    //CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
                    //client.CompleteHandler += (resJson) =>
                    //{
                    //    // ディフォルート選択のMissionId
                    //    var firstFlag = true;

                    //    //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                    //    //ミッションタブのミッションが選択状態になるように修正
                    //    var firstGripMission = false;
                    //    //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end
                    //    var firstMissionId = "";

                    //    JObject tmpJson = JObject.Parse(resJson);
                    //    JObject tempWorkPlaceJson = JObject.Parse(resJson);
                    //    JArray tempMissionJson = (JArray)tempWorkPlaceJson["mission_data"];

                    //    foreach (JObject mission in tempMissionJson)
                    //    {
                    //        JToken tmpMission = mission.DeepClone();

                    //        tmpMission["ctms"] = new JArray();
                    //        tmpMission["elements"] = new JArray();
                    //        var missionId = mission["id"].ToString();

                    //        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 start
                    //        //条件付けエレメントのIDをJsonTreeViewに渡す
                    //        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 start
                    //        //吹き出し追加
                    //        setElementNameWithCondition_Mission(this.conditionLists);
                    //        Dictionary<string, string> tooltips = new Dictionary<string, string>();
                    //        //foreach (var condition in this.conditionLists)
                    //        //{
                    //        //    foreach (ElementConditionData a in condition.Value)
                    //        //    {
                    //        //        tmp.Add(a.Id);
                    //        //    }
                    //        //}
                    //        foreach (var elementCondition in this.elementConditions)
                    //        {
                    //            foreach (var element in elementCondition.Value)
                    //            {
                    //                foreach (var conditionPattern in conditionPatterns)
                    //                {
                    //                    if (conditionPattern.Key.Equals(elementCondition.Key))
                    //                    {
                    //                        tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                    //                    }
                    //                }
                    //            }
                    //        }
                    //        this.AisTvRefDicElement.conditionLists = tooltips;
                    //        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 end
                    //        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 end

                    //        this.AisTvRefDicMission.AisCheckFlag = 2;
                    //        this.AisTvRefDicMission.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                    //        this.AisTvRefDicMission.CheckChangedEvent += this.AisTvRefDicMissionCheckChanged;
                    //        this.AisTvRefDicMission.JsonItemSourceAdd = tmpMission.ToString();
                    //        // Processing On Service 検証＃4　dn 2018/09/28 start
                    //        if (firstFlag)
                    //        {
                    //            firstMissionId = missionId;
                    //            //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                    //            //ミッションタブのミッションが選択状態になるように修正
                    //            if (int.Parse(tmpMission["missionNewType"].ToString()) == 0)
                    //            {
                    //                firstGripMission = true;
                    //            }
                    //            //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end
                    //            firstFlag = false;
                    //        }
                    //        // Processing On Service 検証＃4 dn 2018/09/28 end

                    //        this.AisTvRefDicCtm.AisCheckFlag = 2;
                    //        this.AisTvRefDicCtm.MissionId = missionId;
                    //        this.AisTvRefDicCtm.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                    //        this.AisTvRefDicCtm.CheckChangedEvent += this.AisTvRefDicCtmCheckChanged;
                    //        this.AisTvRefDicCtm.JsonItemSourceAdd = mission["ctms"].ToString();

                    //        this.AisTvRefDicElement.AisCheckFlag = 2;
                    //        this.AisTvRefDicElement.MissionId = missionId;

                    //        this.AisTvRefDicElement.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                    //        this.AisTvRefDicElement.CheckChangedEvent += this.AisTvRefDicElementCheckChanged;
                    //        this.AisTvRefDicElement.JsonItemSourceAdd = mission["elements"].ToString();
                    //    }
                    //    // Processing On Service 検証＃4　dn 2018/09/28 start
                    //    //一番上のミッションを選択した状態にする
                    //    this.AisTvRefDicMission.GetItem(firstMissionId).IsSelected = true;

                    //    //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                    //    //ミッションタブのミッションが選択状態になるように修正
                    //    if (firstGripMission)
                    //    {
                    //        this.DataSource = DataSourceType.GRIP;
                    //    }
                    //    this.missionId = firstMissionId;
                    //    setMission();
                    //    //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end

                    //    // Processing On Service 検証＃4 dn 2018/09/28 end
                    //};
                    //client.Post(CmsUrl.GetAisSelect(), workplaceJson.ToString());
                    //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 end
                    //// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End

                    //エレメント条件を取得
                    if (missionList.Count > 0)
                    {
                        int count = 0;
                        foreach (string id in missionList)
                        {
                            // 個別に指定する場合は、既存の設定をサーバから再取得
                            CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                            missionCtmGetClient.AddParam("missionId", id);
                            // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
                            missionCtmGetClient.CompleteHandler += missionCtmJson =>
                            {
                                //AISMM No.85 sunyi 20181211 start
                                //内容修正、GripMission条件表示
                                var missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);
                                GetElementConditionDatas(missionCtms);

                                count++;
                                if (count.Equals(missionList.Count))
                                {
                                    readExcelFileStep1(workplaceJson);
                                }
                            };
                            missionCtmGetClient.ExceptionHandler += (e, str) =>
                            {
                                count++;
                                if (count.Equals(missionList.Count))
                                {
                                    readExcelFileStep1(workplaceJson);
                                }
                            };
                            missionCtmGetClient.Get(CmsUrl.GetMissionCtmUrl());
                        }
                    }
                    else
                    {
                        readExcelFileStep1(workplaceJson);
                    }
                    //AISMM No.85 sunyi 20181214 end
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 start
        //吹き出し追加
        private void setElementNameWithCondition_Mission(Dictionary<string, List<ElementConditionData>> elementConditions)
        {
            this.elementConditions.Clear();
            foreach (var elementCondition in elementConditions)
            {
                foreach (var ctm in this.ctms)
                {
                    if (ctm.id.ToString().Equals(elementCondition.Key))
                    {
                        Dictionary<ElementConditionData, string> tmp = new Dictionary<ElementConditionData, string>();
                        foreach (var elementConditionData in elementCondition.Value)
                        {
                            foreach (var element in ctm.GetAllElements())
                            {
                                if (element.id.ToString() == elementConditionData.Id)
                                {
                                    tmp.Add(elementConditionData, element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                    break;
                                }
                            }
                        }
                        if (tmp.Count > 0)
                        {
                            this.elementConditions.Add(ctm.id.ToString(), tmp);
                        }
                    }
                }
            }
        }

        //AISMM No.85 sunyi 20181211 start
        //内容修正、GripMission条件表示
        private void setElementNameWithCondition_Grip(Dictionary<string, List<ElementConditionData>> elementConditions_Grip)
        {
            this.elementConditions_Grip.Clear();
            foreach (var elementCondition in elementConditions_Grip)
            {
                foreach (var ctm in this.ctms)
                {
                    if (ctm.id.ToString().Equals(elementCondition.Key))
                    {
                        Dictionary<ElementConditionData, string> tmp = new Dictionary<ElementConditionData, string>();
                        foreach (var elementConditionData in elementCondition.Value)
                        {
                            foreach (var element in ctm.GetAllElements())
                            {
                                if (element.id.ToString() == elementConditionData.Id)
                                {
                                    tmp.Add(elementConditionData, element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                    break;
                                }
                            }
                        }
                        if (tmp.Count > 0)
                        {
                            this.elementConditions_Grip.Add(ctm.id.ToString(), tmp);
                        }
                    }
                }
            }
        }
        //AISMM No.85 sunyi 20181211 end

        /// <summary>
        /// 条件式をstring形式で取得
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        private string getConditionString(Dictionary<ElementConditionData, string> condition, int pattern)
        {
            // かっこで囲む必要があるのは、条件が3つ指定してあり、かつANDとORが複合してる時
            bool useBracket = false;
            if (condition.Count == 3 &&
                6 <= pattern)
            {
                useBracket = true;
            }

            int cnt = 0;
            string conditionText = useBracket ? "(" : string.Empty;
            foreach (ElementConditionData ecd in condition.Keys)
            {
                conditionText += condition[ecd];
                conditionText += CmsDataTable.ToStringExp(ecd.Condition);
                conditionText += ecd.Value;

                if (useBracket && cnt == 1)
                {
                    conditionText += ")";
                }
                if (cnt == condition.Count - 1)
                {
                    break;
                }

                if (cnt == 0)
                {
                    conditionText += " ";
                    if (pattern == 2 ||
                        pattern == 4 ||
                        pattern == 6)
                    {
                        conditionText += "AND ";
                    }
                    else
                    {
                        conditionText += "OR ";
                    }
                }
                if (cnt == 1)
                {
                    conditionText += " ";
                    if (pattern == 4 ||
                        pattern == 7)
                    {
                        conditionText += "AND ";
                    }
                    else
                    {
                        conditionText += "OR ";
                    }
                }

                cnt++;
            }

            return conditionText;
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 end

        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 start
        private void readMissionStructure_M(SpreadsheetGear.IWorksheet missionSheet)
        {
            SpreadsheetGear.IRange paramCells = missionSheet.Cells;

            SpreadsheetGear.IRange usedCells = missionSheet.UsedRange.Cells;

            for (int i = 0; i < usedCells.Rows.RowCount; i++)
            {
                // Mission-ID
                if (paramCells[i, 0].Value != null)
                {
                    string missionId = paramCells[i, 0].Value.ToString();
                    this.missionList.Add(missionId);
                }
            }
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 end
        private void readMissionStructure_G(SpreadsheetGear.IWorksheet missionSheet)
        {
            SpreadsheetGear.IRange paramCells = missionSheet.Cells;

            SpreadsheetGear.IRange usedCells = missionSheet.UsedRange.Cells;

            for (int i = 0; i < usedCells.Rows.RowCount; i++)
            {
                // Mission-ID
                if (paramCells[i, 0].Value != null)
                {
                    string missionId = paramCells[i, 0].Value.ToString();
                    this.missionList_Grip.Add(missionId);
                }
            }
        }

        private void readMissionStructure(SpreadsheetGear.IWorksheet missionSheet)
        {
            SpreadsheetGear.IRange paramCells = missionSheet.Cells;

            // Currently used cells
            SpreadsheetGear.IRange usedCells = missionSheet.UsedRange.Cells;

            List<MissionViewObject> missionViewListSource = new List<MissionViewObject>();
            List<MissionCtmViewObject> ctmViewListSource = new List<MissionCtmViewObject>();
            List<MissionCtmElementViewObject> ctmElementViewListSource = new List<MissionCtmElementViewObject>();

            for (int i = 0; i < usedCells.Rows.RowCount; i++)
            {
                // Mission-ID
                if (paramCells[i, 0].Value != null)
                {
                    string missionId = paramCells[i, 0].Value.ToString();
                    if (string.IsNullOrEmpty(this.missionId))
                    {
                        this.missionId = missionId;
                        if (!setMission())
                        {
                            return;
                        }
                    }

                    // Wang Issue NO.727 20180618 Start
                    //JToken token = this.mainWindow.treeView_Mission_CTM.GetItem(missionId).DataContext as JToken;
                    JToken token = this.DataSource.Equals(DataSourceType.GRIP) ?
                        this.mainWindow.treeView_Mission_Grip.GetItem(missionId).DataContext as JToken :
                        this.mainWindow.treeView_Mission_CTM.GetItem(missionId).DataContext as JToken;
                    // Wang Issue NO.727 20180618 End
                    Mission missionObj = JsonConvert.DeserializeObject<Mission>(token.ToString());
                    missionDict.Add(missionId, missionObj);

                    missionViewListSource.Add(new MissionViewObject()
                    {
                        MissionDisplayName = AisUtil.GetCatalogLang(missionObj),
                        MissionId = missionId,
                        Json = JsonConvert.SerializeObject(missionObj)
                    });
                }

                // CTM-ID
                if (paramCells[i, 3].Value != null)
                {
                    string parentMissionId = string.Empty;
                    if (paramCells[i, 2].Value != null)
                    {
                        parentMissionId = paramCells[i, 2].Value.ToString();
                    }
                    string ctmId = paramCells[i, 3].Value.ToString();
                    string ctmName = paramCells[i, 4].Value.ToString();

                    ctmViewListSource.Add(new MissionCtmViewObject()
                    {
                        CtmDisplayName = ctmName,
                        CtmId = ctmId,
                        ParentMissionId = parentMissionId
                    });
                }

                // Element-ID
                if (paramCells[i, 6].Value != null)
                {
                    string parentCtmId = paramCells[i, 5].Value.ToString();
                    string elementId = paramCells[i, 6].Value.ToString();
                    string elementName = paramCells[i, 7].Value.ToString();
                    string parentMissionId = string.Empty;
                    if (paramCells[i, 8].Value != null)
                    {
                        parentMissionId = paramCells[i, 8].Value.ToString();
                    }

                    ctmElementViewListSource.Add(new MissionCtmElementViewObject()
                    {
                        ElementDisplayName = elementName,
                        ElementId = elementId,
                        ParentCtmId = parentCtmId,
                        ParentMissionId = parentMissionId
                    });
                }
            }

            // Set the ListBox ItemsSource
            setMissionListBoxItemsSource(null, missionViewListSource);
            setCtmListBoxItemsSource(null, ctmViewListSource);
            setElementListBoxItemsSource(null, ctmElementViewListSource);

            // Get the CTM Object, and assign it  to the related dictionary
            List<string> ctmIdList = new List<string>();
            foreach (MissionCtmViewObject ctmViewObj in ctmViewListSource)
            {
                ctmIdList.Add(ctmViewObj.CtmId);
            }

            if (ctmIdList.Count > 0)
            {
                if (string.IsNullOrEmpty(this.missionId))
                {
                    setCtm(ctmIdList);
                }

                // 個別に指定する場合は、既存の設定をサーバから再取得
                CmsHttpClient ctmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                foreach (string ctmId in ctmIdList)
                {
                    ctmGetClient.AddParam("id", ctmId);
                }

                ctmGetClient.CompleteHandler += ctmJson =>
                {
                    List<CtmObject> ctmList = JsonConvert.DeserializeObject<List<CtmObject>>(ctmJson);

                    // Assign mission CTM Dictionary
                    foreach (MissionCtmViewObject ctmViewObj in ctmViewListSource)
                    {
                        Mission missionObj = null;
                        if (missionDict.TryGetValue(ctmViewObj.ParentMissionId, out missionObj))
                        {
                            CtmObject ctmObj = ctmList.Where(x => x.id.ToString() == ctmViewObj.CtmId).FirstOrDefault();

                            List<CtmObject> ctmObjList = new List<CtmObject>();
                            if (missionCtmDict.TryGetValue(missionObj, out ctmObjList))
                            {
                                if (ctmObjList.Contains(ctmObj)) return;

                                ctmObjList.Add(ctmObj);
                                missionCtmDict[missionObj] = ctmObjList;
                            }
                            else
                            {
                                missionCtmDict.Add(missionObj, new List<CtmObject>() { ctmObj });
                            }
                        }
                    }

                    // Assign mission CTM Element Dictionary
                    foreach (MissionCtmElementViewObject ctmElementViewObj in ctmElementViewListSource)
                    {
                        CtmObject ctmObj = ctmList.Where(x => x.id.ToString() == ctmElementViewObj.ParentCtmId).FirstOrDefault();
                        if (ctmObj == null) return;

                        CtmElement elementObj = ctmObj.GetElement(new GUID(ctmElementViewObj.ElementId));
                        if (elementObj != null)
                        {
                            List<CtmElement> ctmElementObjList = new List<CtmElement>();
                            if (missionCtmElementDict.TryGetValue(ctmObj, out ctmElementObjList))
                            {
                                if (ctmElementObjList.Contains(elementObj)) return;

                                ctmElementObjList.Add(elementObj);
                                missionCtmElementDict[ctmObj] = ctmElementObjList;
                            }
                            else
                            {
                                missionCtmElementDict.Add(ctmObj, new List<CtmElement>() { elementObj });
                            }
                        }
                    }
                };
                ctmGetClient.Get(CmsUrlMms.Ctm.List());
            }
        }

        private Dictionary<string, string> readParam(SpreadsheetGear.IWorksheet worksheetCondition)
        {
            var paramMap = new Dictionary<string, string>();

            SpreadsheetGear.IRange paramCells = worksheetCondition.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // 更新周期
                if (paramName == "更新周期")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }

                // 取得期間
                if (paramName == "取得期間")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }

                // 検索タイプ
                if (paramName == "検索タイプ")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }

                // 収集開始日時
                if (paramName == "収集開始日時")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }

                // 収集終了日時
                if (paramName == "収集終了日時")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }
            }

            return paramMap;
        }


        public override void SetMission2()
        {
            if (!this.isFirstFile)
            {
                readExcelFile(this.excelFilePath);
            }
        }

        private bool setMission()
        {
            if (string.IsNullOrEmpty(this.missionId))
            {
                return false;
            }

            this.IsSelectedProgramMission = true;

            // Mission Tree
            // Wang Issue NO.727 20180618 Start
            //TreeViewItem item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(this.missionId);
            TreeViewItem item = this.DataSource.Equals(DataSourceType.GRIP) ?
                this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(this.missionId) :
                this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(this.missionId);
            // Wang Issue NO.727 20180618 End
            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                }
                return false;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            // Wang Issue NO.727 20180618 Start
            //this.mainWindow.treeView_Mission_CTM.Focus();
            if (this.DataSource.Equals(DataSourceType.GRIP))
            {
                this.mainWindow.treeView_Mission_Grip.Focus();
            }
            else
            {
                this.mainWindow.treeView_Mission_CTM.Focus();
            }
            // Wang Issue NO.727 20180618 End
            item.Focus();

            return true;
        }
        
        override public void SetCtm2()
        {
            if (!this.isFirstFile)
            {
                readExcelFile(this.excelFilePath);
            }
        }

        private void setCtm(List<string> ctmIdList)
        {
            try
            {
                TreeViewItem item = null;
                if (ctmIdList.Count < 1)
                {
                    return;
                }

                this.mainWindow.TabCtrlDS.SelectedIndex = 0;
                foreach (string ctmId in ctmIdList)
                {
                    item = this.mainWindow.treeView_Catalog.GetItem(ctmId);
                    System.Windows.Controls.CheckBox cb = this.mainWindow.treeView_Catalog.GetCheckBox(item);
                    cb.IsChecked = true;
                }

                if (item == null)
                {
                    return;
                }
                item.IsSelected = true;
                List<string> list = this.mainWindow.treeView_Catalog.GetCheckedId(true);
            }
            catch (Exception ex)
            {
                string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                AisMessageBox.DisplayErrorMessageBox(message);
            }
        }

        #endregion

        private void setAllListBoxItemsSourceDefaultView()
        {
            setMissionListBoxItemsSource(this.missionDict);
            setCtmListBoxItemsSource(this.missionCtmDict);
            setElementListBoxItemsSource(this.missionCtmElementDict);
        }

        private void setMissionListBoxItemsSource(Dictionary<string, Mission> missionDict = null, List<MissionViewObject> missionViewListSource = null)
        {
            if (missionViewListSource != null)
            {
                //lbMission.ItemsSource = missionViewListSource;
                //lbMission.DisplayMemberPath = "MissionDisplayName";
                return;
            }

            List<MissionViewObject> missionViewList = new List<MissionViewObject>();
            foreach (KeyValuePair<string, Mission> kvp in missionDict)
            {
                missionViewList.Add(new MissionViewObject()
                {
                    MissionDisplayName = AisUtil.GetCatalogLang(kvp.Value),
                    MissionId = kvp.Key,
                    Json = JsonConvert.SerializeObject(kvp.Value)
                });
            }
            //lbMission.ItemsSource = missionViewList;
            //lbMission.DisplayMemberPath = "MissionDisplayName";
        }
        private void setMissionListBoxItemsSource(Dictionary<string, Mission> missionDict)
        {
            List<MissionViewObject> missionViewList = new List<MissionViewObject>();
            foreach (KeyValuePair<string, Mission> kvp in missionDict)
            {
                missionViewList.Add(new MissionViewObject()
                {
                    MissionDisplayName = AisUtil.GetCatalogLang(kvp.Value),
                    MissionId = kvp.Key,
                    Json = JsonConvert.SerializeObject(kvp.Value)
                });
            }
            //lbMission.ItemsSource = missionViewList;
            //lbMission.DisplayMemberPath = "MissionDisplayName";
        }

        private void setCtmListBoxItemsSource(Dictionary<Mission, List<CtmObject>> missionCtmDict = null, List<MissionCtmViewObject> ctmViewListSource = null)
        {
            if (ctmViewListSource != null)
            {
                //lbCtm.ItemsSource = ctmViewListSource;
                //lbCtm.DisplayMemberPath = "CtmDisplayName";
                return;
            }

            List<MissionCtmViewObject> ctmViewList = new List<MissionCtmViewObject>();
            foreach (KeyValuePair<Mission, List<CtmObject>> kvp in missionCtmDict)
            {
                foreach (CtmObject ctm in kvp.Value)
                {
                    ctmViewList.Add(new MissionCtmViewObject()
                    {
                        CtmDisplayName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                        CtmId = ctm.id.ToString(),
                        ParentMissionId = kvp.Key.Id
                    });
                }
            }
            //lbCtm.ItemsSource = ctmViewList;
            //lbCtm.DisplayMemberPath = "CtmDisplayName";
        }
        private void setCtmListBoxItemsSource(Dictionary<Mission, List<CtmObject>> missionCtmDict)
        {
            List<MissionCtmViewObject> ctmViewList = new List<MissionCtmViewObject>();
            foreach (KeyValuePair<Mission, List<CtmObject>> kvp in missionCtmDict)
            {
                foreach (CtmObject ctm in kvp.Value)
                {
                    ctmViewList.Add(new MissionCtmViewObject()
                    {
                        CtmDisplayName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                        CtmId = ctm.id.ToString(),
                        ParentMissionId = kvp.Key.Id
                    });
                }
            }
            //lbCtm.ItemsSource = ctmViewList;
            //lbCtm.DisplayMemberPath = "CtmDisplayName";
        }
        private void setCtmListBoxItemsSource(List<CtmObject> listCtmObj, string parentMissionId)
        {
            List<MissionCtmViewObject> ctmViewList = new List<MissionCtmViewObject>();
            foreach (CtmObject ctm in listCtmObj)
            {
                ctmViewList.Add(new MissionCtmViewObject()
                {
                    CtmDisplayName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                    CtmId = ctm.id.ToString(),
                    ParentMissionId = parentMissionId
                });
            }
            //lbCtm.ItemsSource = ctmViewList;
            //lbCtm.DisplayMemberPath = "CtmDisplayName";
        }

        private void setElementListBoxItemsSource(Dictionary<CtmObject, List<CtmElement>> missionCtmElementDict = null, List<MissionCtmElementViewObject> ctmElementViewListSource = null)
        {
            if (ctmElementViewListSource != null)
            {
                //lbElement.ItemsSource = ctmElementViewListSource;
                //lbElement.DisplayMemberPath = "ElementDisplayName";
                return;
            }

            List<MissionCtmElementViewObject> ctmElementViewList = new List<MissionCtmElementViewObject>();
            foreach (KeyValuePair<CtmObject, List<CtmElement>> kvp in missionCtmElementDict)
            {
                string ctmId = kvp.Key.id.ToString();
                foreach (CtmElement ele in kvp.Value)
                {
                    // Get ParentMissionId
                    string parentMissionId = null;
                    foreach (KeyValuePair<Mission, List<CtmObject>> kvp2 in missionCtmDict)
                    {
                        bool isParentMissionFound = false;
                        foreach (CtmObject ctm in kvp2.Value)
                        {
                            if (ctm.id.ToString() == ctmId)
                            {
                                parentMissionId = kvp2.Key.Id;
                                isParentMissionFound = true;
                                break;
                            }
                        }
                        if (isParentMissionFound) break;
                    }

                    ctmElementViewList.Add(new MissionCtmElementViewObject()
                    {
                        ElementDisplayName = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                        ElementId = ele.id.ToString(),
                        ParentCtmId = ctmId,
                        ParentMissionId = parentMissionId
                    });
                }
            }
            //lbElement.ItemsSource = ctmElementViewList;
            //lbElement.DisplayMemberPath = "ElementDisplayName";
        }

        private void setElementListBoxItemsSource(List<CtmElement> missionCtmElementList, string parentCtmId, string parentMissionId)
        {
            List<MissionCtmElementViewObject> ctmElementViewList = new List<MissionCtmElementViewObject>();
            foreach (CtmElement ele in missionCtmElementList)
            {
                ctmElementViewList.Add(new MissionCtmElementViewObject()
                {
                    ElementDisplayName = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                    ElementId = ele.id.ToString(),
                    ParentCtmId = parentCtmId,
                    ParentMissionId = parentMissionId
                });
            }
            //lbElement.ItemsSource = ctmElementViewList;
            //lbElement.DisplayMemberPath = "ElementDisplayName";
        }

        private void setAllListBoxItemsSource(string[] itemArray, bool isCtmDrop)
        {
            string droppedCtmId = itemArray[1];
            CtmObject droppedCtmObj = setCtmObject(droppedCtmId);

            List<CtmObject> listCtmObj = missionCtmElementDict.Keys.ToList();

            if (this.DataSource == DataSourceType.MISSION)      // Mission
            {
                // 1. Re-assign the Mission ListBoxItemsSource
                InsertMissionToListBox(this.currentSelectedMission);
                Mission currentMission = this.currentSelectedMission;
                listCtmObj = missionCtmDict.Where(x => x.Key.Id == currentMission.Id).FirstOrDefault().Value;

                // 2. Check if the dropped CTM object has been added or not.
                if (listCtmObj.Count > 0)
                {
                    bool ctmIsAlreadyExist = false;
                    foreach (CtmObject ctm in listCtmObj)
                    {
                        if (ctm.id.ToString() == droppedCtmId)
                        {
                            ctmIsAlreadyExist = true;
                            break;
                        }
                    }
                    if (!ctmIsAlreadyExist)
                    {
                        listCtmObj.Add(droppedCtmObj);
                    }
                }
                else
                {
                    listCtmObj.Add(droppedCtmObj);
                }

                // 3. Update the CTM Dictionary for that Mission
                missionCtmDict[missionCtmDict.Where(x => x.Key.Id == currentMission.Id).FirstOrDefault().Key] = listCtmObj;

                // 4. Set the CTM ListBox ItemsSource
                setCtmListBoxItemsSource(missionCtmDict);
            }
            else if (this.DataSource == DataSourceType.CTM_DIRECT)  // CTM Direct
            {
                if (listCtmObj.Count > 0)
                {
                    bool ctmIsAlreadyExist = false;
                    foreach (CtmObject ctm in listCtmObj)
                    {
                        if (ctm.id.ToString() == droppedCtmId)
                        {
                            ctmIsAlreadyExist = true;
                            break;
                        }
                    }
                    if (!ctmIsAlreadyExist)
                    {
                        listCtmObj.Add(droppedCtmObj);
                    }
                }
                else
                {
                    listCtmObj.Add(droppedCtmObj);
                }

                List<CtmObject> ctmObjList = new List<CtmObject>();
                if (!this.missionCtmDict.ContainsKey(this.forCtmDirect))
                {
                    missionCtmDict.Add(this.forCtmDirect, ctmObjList);
                }

                missionCtmDict[this.forCtmDirect] = listCtmObj;
                setCtmListBoxItemsSource(listCtmObj, this.forCtmDirect.Id);
            }

            // 5. Check the CTM Element Dictionary & Get the CTM Element data
            List<CtmElement> ctmElementList = null;
            List<string[]> argsData = convertItemArrayToElementData(itemArray);

            // 6. Re-initialize the droppedCtmObj from the list..
            foreach (CtmObject ctm in listCtmObj)
            {
                if (ctm.id.ToString() == droppedCtmId)
                {
                    droppedCtmObj = ctm;
                    break;
                }
            }

            if (missionCtmElementDict.TryGetValue(droppedCtmObj, out ctmElementList) == true)
            {
                foreach (string[] elementData in argsData)
                {
                    CtmElement ele = droppedCtmObj.GetElement(new GUID(elementData[1]));
                    if (!ctmElementList.Contains(ele))
                    {
                        ctmElementList.Add(ele);
                    }
                }
                missionCtmElementDict[droppedCtmObj] = ctmElementList;
            }
            else
            {
                ctmElementList = new List<CtmElement>();
                if (!isCtmDrop) // Element Drop
                {
                    foreach (string[] elementData in argsData)
                    {
                        CtmElement ele = droppedCtmObj.GetElement(new GUID(elementData[1]));
                        ctmElementList.Add(ele);
                    }
                }
                else // CTM Drop
                {
                    foreach (CtmElement ele in droppedCtmObj.GetAllElements())
                    {
                        bool hit = false;
                        foreach (var arg in argsData)
                        {
                            if (ele.id.ToString() == arg[1])
                            {
                                hit = true;
                                break;
                            }
                        }
                        if (hit)
                        {
                            ctmElementList.Add(ele);
                        }
                    }
                }
                missionCtmElementDict.Add(droppedCtmObj, ctmElementList);
            }

            // 7. Set the CTM Element ListBox ItemsSource
            setElementListBoxItemsSource(missionCtmElementDict);
        }

        public void InsertMissionToListBox(Mission mission)
        {
            Mission missionObj = null;
            if (!this.missionDict.TryGetValue(mission.Id, out missionObj))
            {
                this.missionDict[mission.Id] = mission;
                List<CtmObject> ctmObjList = new List<CtmObject>();
                if (!this.missionCtmDict.TryGetValue(mission, out ctmObjList))
                {
                    this.missionCtmDict.Add(mission, new List<CtmObject>());
                }
            }

            setMissionListBoxItemsSource(this.missionDict);
        }

        private CtmObject setCtmObject(string ctmId)
        {
            CtmObject ctmObj = null;
            if (this.editControl.Ctms != null)
            {
                this.ctms = this.editControl.Ctms;
            }

            foreach (CtmObject ctm in ctms)
            {
                if (ctm.id.ToString() == ctmId)
                {
                    ctmObj = ctm;
                    break;
                }
            }

            return ctmObj;
        }

        private List<string[]> convertItemArrayToElementData(string[] itemArray)
        {
            List<string[]> data = new List<string[]>();

            for (int i = 2; i < itemArray.Length; i += 3)
            {
                string[] ary = new string[]
                {
                    itemArray[i],
                    itemArray[i + 1],
                    itemArray[i + 2]
                };
                data.Add(ary);
            }

            return data;
        }

        // True: Remove CTM & False: Remove Element
        private void removeListBoxItem(bool isCtmItem, object sender)
        {
            if (isCtmItem) // Remove ctm
            {
                //MissionCtmViewObject ctmViewObj = lbCtm.Items[lbCtm.SelectedIndex] as MissionCtmViewObject;
                MissionCtmViewObject ctmViewObj = new MissionCtmViewObject();
                if (ctmViewObj == null) return;

                // 1. Remove CTM from the dictionary
                // **** BEGIN ****
                List<CtmObject> listCtmObj = missionCtmDict.Where
                (x => x.Key.Id == ctmViewObj.ParentMissionId).FirstOrDefault().Value;

                // Get the 'to be removed CtmObject'
                CtmObject ctmObj = listCtmObj.Where(x => x.id.ToString() == ctmViewObj.CtmId).FirstOrDefault();

                listCtmObj.Remove(ctmObj); // Remove from the dict

                if (listCtmObj.Count <= 0)
                {
                    // Remove the Mission object from the dict
                    // **** BEGIN ****
                    this.missionDict.Remove(ctmViewObj.ParentMissionId);
                    // **** END ****

                    // Re-assign the Mission ListBox ItemsSource
                    setMissionListBoxItemsSource(this.missionDict);
                }

                missionCtmDict[missionCtmDict.Where(
                    x => x.Key.Id == ctmViewObj.ParentMissionId).FirstOrDefault().Key] = listCtmObj; // Update the dict for this mission
                // **** END ****

                // 2. Re-assign the CTM ListBox ItemsSource
                setCtmListBoxItemsSource(missionCtmDict);

                // 3. Remove related element from the Dictionary
                missionCtmElementDict.Remove(ctmObj);

                // 4. Re-assign the Element ListBox ItemsSource
                setElementListBoxItemsSource(missionCtmElementDict);
            }
            else // Remove element
            {
                //MissionCtmElementViewObject eleViewObj = lbElement.Items[lbElement.SelectedIndex] as MissionCtmElementViewObject;
                MissionCtmElementViewObject eleViewObj = new MissionCtmElementViewObject();
                if (eleViewObj == null) return;

                // 1. Get the parent CTM object
                CtmObject ctmObj = null;
                List<CtmObject> ctmMissionList = missionCtmDict.Where(x => x.Key.Id == eleViewObj.ParentMissionId).FirstOrDefault().Value;
                foreach (CtmObject ctm in ctmMissionList)
                {
                    if (ctm.id.ToString() == eleViewObj.ParentCtmId)
                    {
                        ctmObj = ctm;
                        break;
                    }
                }
                //CtmObject ctmObj = missionCtmElementDict.Where
                //(x => x.Key.id.ToString() == eleViewObj.parentCtmId).FirstOrDefault().Key;

                // 2. Get the List of related element for the Parent CTM object
                List<CtmElement> ctmElementList = null;
                if (missionCtmElementDict.TryGetValue(ctmObj, out ctmElementList) == true)
                {
                    CtmElement toBeRemovedElement = null;
                    foreach (CtmElement ele in ctmElementList)
                    {
                        if (ele.id.ToString() == eleViewObj.ElementId)
                        {
                            toBeRemovedElement = ele;
                            break;
                        }
                    }
                    if (toBeRemovedElement == null) // there is might another duplicated CTM exists, so the toBeRemovedElement is 'NULL'
                    {
                        foreach (KeyValuePair<CtmObject, List<CtmElement>> kvp in missionCtmElementDict)
                        {
                            if (kvp.Key.id.ToString() == eleViewObj.ParentCtmId)
                            {
                                foreach (CtmElement ele in kvp.Value)
                                {
                                    if (ele.id.ToString() == eleViewObj.ElementId)
                                    {
                                        toBeRemovedElement = ele;
                                        break;
                                    }
                                }
                                if (toBeRemovedElement != null)
                                {
                                    ctmObj = kvp.Key;
                                    ctmElementList = kvp.Value;
                                    if (ctmElementList.Remove(toBeRemovedElement)) break;
                                }
                            }
                        }
                    }
                    else // the toBeRemovedElement is found, proceed to delete from the list..
                    {
                        ctmElementList.Remove(toBeRemovedElement);
                    }

                    if (ctmElementList.Count <= 0) // is empty, remove the CTM object also
                    {
                        // Remove the CTM object from the dict
                        // **** BEGIN ****
                        Mission parentMission = missionCtmDict.Where(x => x.Key.Id == eleViewObj.ParentMissionId).FirstOrDefault().Key;
                        CtmObject toBeRemovedCtm = null;
                        foreach (CtmObject ctm in ctmMissionList)
                        {
                            if (ctm.id.ToString() == ctmObj.id.ToString())
                            {
                                toBeRemovedCtm = ctm;
                                break;
                            }
                        }
                        ctmMissionList.Remove(toBeRemovedCtm); // Remove from the dict

                        if (ctmMissionList.Count <= 0)
                        {
                            // Remove the Mission object from the dict
                            // **** BEGIN ****
                            this.missionDict.Remove(eleViewObj.ParentMissionId);
                            // **** END ****

                            // Re-assign the Mission ListBox ItemsSource
                            setMissionListBoxItemsSource(this.missionDict);
                        }

                        missionCtmDict[parentMission] = ctmMissionList; // Update the dict for this mission
                        // **** END ****

                        // Re-assign the CTM ListBox ItemsSource
                        setCtmListBoxItemsSource(missionCtmDict);
                    }
                    else // there are still left-over elements
                    {
                        missionCtmElementDict[ctmObj] = ctmElementList; // Update the related ctm's List of element dict
                    }
                }

                // 3. Re-assign the Element ListBox ItemsSource
                setElementListBoxItemsSource(missionCtmElementDict);
            }
        }

        private string[] getItemArrayFromDropEvent(object sender, DragEventArgs e)
        {
            var senderObj = sender as System.Windows.Controls.ListBox;
            if (senderObj == null)
            {
                return null;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return null;
            }

            string items = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = items.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 5)
            {
                return null;
            }

            /*
             * 0 CTM名
             * 1 CTMID
             * ----------------
             * 2 エレメント名
             * 3 エレメントID
             * 4 エレメントの型
             * ...
             * ...
             * ...
            */

            return itemArray;
        }

        private void removeMissionListBoxItem()
        {
            //MissionViewObject missionViewObj = lbMission.Items[lbMission.SelectedIndex] as MissionViewObject;
            MissionViewObject missionViewObj = null;
            if (missionViewObj == null)
            {
                return;
            }

            // 1. Get the to be removed Mission object
            Mission toBeRemovedMission = missionDict[missionViewObj.MissionId];

            // 2. Get the current selected CtmObject for that mission
            List<CtmObject> listCtmObj = missionCtmDict[toBeRemovedMission];

            // 3. If the ctmObject is exist, continue removing the related element
            foreach (CtmObject ctm in listCtmObj)
            {
                // 4. Remove related element from the Dictionary
                missionCtmElementDict.Remove(ctm);
            }

            // 5. Remove the Ctm from Dictionary
            missionCtmDict.Remove(toBeRemovedMission);

            // 6. Remove the mission from dictionary
            missionDict.Remove(missionViewObj.MissionId);

            // 7. Re-assign the Element ListBox ItemsSource
            setElementListBoxItemsSource(missionCtmElementDict);

            // 8. Re-assign the CTM ListBox ItemsSource
            setCtmListBoxItemsSource(missionCtmDict);

            // 9. Re-assign the Mission ListBox ItemsSource
            List<MissionViewObject> missionViewList = new List<MissionViewObject>();
            foreach (KeyValuePair<string, Mission> kvp in missionDict)
            {
                missionViewList.Add(new MissionViewObject()
                {
                    MissionDisplayName = AisUtil.GetCatalogLang(kvp.Value),
                    MissionId = kvp.Key,
                    Json = JsonConvert.SerializeObject(kvp.Value)
                });
            }

            //lbMission.ItemsSource = missionViewList;
            //lbMission.DisplayMemberPath = "MissionDisplayName";
        }

		// FOA_サーバー化開発 Processing On Server Xj 2018/08/27 Start
        /// <summary>
        /// ミッションツリーを選択した時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AisTvRefDicMission_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
			//AISMM-7 sunyi 20190109 start
            //Nullチェック
            if (this.search_Start.Value == null || this.search_End.Value == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return;
            }
            //AISMM-7 sunyi 20190109 end
            //今選択するのミッション
            JToken currentToken = AisTvRefDicMission.GetSelectedToken();

            List<string> tmpList = new List<string>();

            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
            if (currentToken == null)
            {
                if (this.AisTvRefDicMission.Items.Count == 1 && this.AisTvRefDicCtm.Items.Count > 0)
                {
                    foreach (TreeViewItem treeItem in this.AisTvRefDicCtm.Items)
                    {
                        JToken itemToken = treeItem.DataContext as JToken;

                        if (itemToken["id"].ToString().Contains(this.AisTvRefDicMission.itemList[0].Key))
                        {
                            tmpList.Add(itemToken["id"].ToString());
                        }
                    }
                    this.AisTvRefDicCtm.DeleteMissionCtmElement(tmpList);
                    tmpList.Clear();
                    foreach (TreeViewItem treeItem in this.AisTvRefDicElement.Items)
                    {
                        JToken itemToken = treeItem.DataContext as JToken;
                        if (itemToken["id"].ToString().Contains(this.AisTvRefDicMission.itemList[0].Key))
                        {
                            tmpList.Add(itemToken["id"].ToString());
                        }
                    }
                    this.AisTvRefDicElement.DeleteMissionCtmElement(tmpList);
                    tmpList.Clear();
                }
                if (this.AisTvRefDicMission.itemList.Count > 0)
                {
                    tmpList.Add(this.AisTvRefDicMission.itemList[0].Key);
                    this.AisTvRefDicMission.DeleteMissionCtmElement(tmpList);
                    tmpList.Clear();
                    this.AisTvRefDicMission.IsDeleteFlag = false;
                }
            }
            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
            if (currentToken == null) return;

            //AIS-MM No.89 sunyi 20190109 start
            //先に設定されたものが変更されないようにする
            //if (!string.IsNullOrEmpty(preSelectedMissionId))
            //{
            //    TreeView treeView = (TreeView)sender;
            //    foreach (TreeViewItem treeViewItem in treeView.Items)
            //    {
            //        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
            //        //前回選択したミッションに検索条件を設定
            //        if (preSelectedMissionId == (string)tempChildItemToken[MmsTvJsonKey.Id])
            //        {
            //            setSearchConditions(tempChildItemToken);
            //        }
            //    }

            //    //今選択するのミッション
            //    setCurrentConditions(currentToken);
            //}
            //else
            //{
            //    //今選択するのミッション
            //    setCurrentConditions(currentToken);
            //}
            TreeView treeView = (TreeView)sender;
            foreach (TreeViewItem treeViewItem in treeView.Items)
            {
                JToken tempChildItemToken = treeViewItem.DataContext as JToken;

                //前回選択したミッションに検索条件を設定
                if (string.IsNullOrEmpty(preSelectedMissionId) == false && preSelectedMissionId == (string)tempChildItemToken[MmsTvJsonKey.Id])
                {
					setSearchConditions(tempChildItemToken);
                }
            }
            foreach (TreeViewItem treeViewItem in treeView.Items)
            {
                JToken tempChildItemToken = treeViewItem.DataContext as JToken;

                //今選択しているミッション
				if (string.IsNullOrEmpty(AisTvRefDicMission.SelectedId) == false && AisTvRefDicMission.SelectedId == (string)tempChildItemToken[MmsTvJsonKey.Id])
				{
                    setCurrentConditions(tempChildItemToken, true);
				}
            }

            //AIS-MM No.89 sunyi 20190109 end

            // 前回選択したミッションID
            preSelectedMissionId = AisTvRefDicMission.SelectedId;

            // 色を設定

            foreach (TreeViewItem treeItem in AisTvRefDicMission.Items)
            {
                treeItem.Foreground = new SolidColorBrush(Colors.Black);
                treeItem.Background = new SolidColorBrush(Colors.White);
                JToken itemToken = treeItem.DataContext as JToken;
                string itemSearchType = (string)itemToken["aisSearchType"];
                if (!string.IsNullOrEmpty(itemSearchType))
                {
                    if (int.Parse(itemSearchType) == 1)
                    {
                        Style style = new Style();
                        // 枠色の設定
                        style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(Colors.Red)));
                        style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(2)));
                        // スタイルセット
                        treeItem.Style = style;
                    }
                    else if (int.Parse(itemSearchType) == 2)
                    {
                        Style style = new Style();
                        // 枠色の設定
                        style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(Colors.White)));
                        style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(0)));
                        // スタイルセット
                        treeItem.Style = style;
                    }
                }
            }

            // 選択された行の色を設定
            TreeViewItem thisTreeItem = this.AisTvRefDicMission.GetItem(this.AisTvRefDicMission.SelectedId);

            thisTreeItem.Foreground = new SolidColorBrush(Colors.White);

            thisTreeItem.Background = new SolidColorBrush(Colors.DodgerBlue);
            // Processing On Server 検証＃6　dn 2018/10/05 start
            string curSearchType = (string)currentToken["aisSearchType"];
            if (!string.IsNullOrEmpty(curSearchType))
            {
                // 検索条件設定済みの場合、設定の検索タイプで枠色を設定する
                if (int.Parse(curSearchType) == 1 )
                {
                    Style style = new Style();
                    // 枠色の設定
                    style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(Colors.Red)));
                    style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(2)));
                    // スタイルセット
                    thisTreeItem.Style = style;
                }
                else if (int.Parse(curSearchType) == 2)
                {
                    Style style = new Style();
                    // 枠色の設定
                    style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(Colors.White)));
                    style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(0)));
                    // スタイルセット
                    thisTreeItem.Style = style;
                }
                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 Start
                //選択しない場合、黒にセット
                thisTreeItem.Foreground = new SolidColorBrush(Colors.Black);
                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 End
            }
            else
            {
                // 検索条件が未設定の場合、画面の検索タイプで枠色を設定する
                if (this.radioButton_1.IsChecked == true)
                {
                    Style style = new Style();
                    // 枠色の設定
                    style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(Colors.Red)));
                    style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(2)));
                    // スタイルセット
                    thisTreeItem.Style = style;
                }
                else if (this.radioButton_2.IsChecked == true)
                {
                    Style style = new Style();
                    // 枠色の設定
                    style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(Colors.White)));
                    style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(0)));
                    // スタイルセット
                    thisTreeItem.Style = style;
                }
                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 Start
                //選択しない場合、黒にセット
                thisTreeItem.Foreground = new SolidColorBrush(Colors.Black);
                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 End
            }
            // Processing On Server 検証＃6　dn 2018/10/05 end

            // CTM・Routesを表示・非表示
            bool isFirstCtm1 = false;
            foreach (TreeViewItem treeItem in this.AisTvRefDicCtm.Items)
            {
                treeItem.IsSelected = false;
                JToken itemToken = treeItem.DataContext as JToken;
                if (AisTvRefDicMission.SelectedId == (string)itemToken["missionId"])
                {
                    if (!isFirstCtm1)
                    {
                        isFirstCtm1 = true;

                        treeItem.Foreground = new SolidColorBrush(Colors.White);
                        treeItem.Background = new SolidColorBrush(Colors.DodgerBlue);

                    }
                    treeItem.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    treeItem.Foreground = new SolidColorBrush(Colors.Black);
                    treeItem.Background = new SolidColorBrush(Colors.White);

                    treeItem.Visibility = System.Windows.Visibility.Collapsed;
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                    tmpList.Add(itemToken["id"].ToString());
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                }
            }

            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
            if (this.AisTvRefDicMission.IsDeleteFlag)
            {
                this.AisTvRefDicCtm.DeleteMissionCtmElement(tmpList);
                tmpList.Clear();
            }
            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end

            // Elementsを表示・非表示
            if (getMissionType(currentToken) == 1)
            {
                // CTM Mission
                bool isFirstCtm = false;
                foreach (TreeViewItem treeItem in this.AisTvRefDicElement.Items)
                {
                    JToken itemToken = treeItem.DataContext as JToken;
                    if (AisTvRefDicMission.SelectedId == (string)itemToken["missionId"] && getMissionType(itemToken) == getMissionType(currentToken))
                    {
                        if (!isFirstCtm)
                        {
                            treeItem.Visibility = System.Windows.Visibility.Visible;
                            isFirstCtm = true;
                        }
                        else
                        {
                            treeItem.Visibility = System.Windows.Visibility.Collapsed;
                        }
                    }
                    else
                    {
                        treeItem.Visibility = System.Windows.Visibility.Collapsed;
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                        tmpList.Add(itemToken["id"].ToString());
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                    }
                }
            }
            else //Grip Mission
            {
                foreach (TreeViewItem ctmTreeItem in this.AisTvRefDicCtm.Items)
                {
                    JToken ctmItemToken = ctmTreeItem.DataContext as JToken;
                    if (AisTvRefDicMission.SelectedId == (string)ctmItemToken["missionId"])
                    {
                        string[] ctmNodeArray = Regex.Split(ctmItemToken["nodes"].ToString(), ",", RegexOptions.IgnoreCase);
                        foreach (TreeViewItem treeItem in this.AisTvRefDicElement.Items)
                        {
                            JToken itemToken = treeItem.DataContext as JToken;

                            string[] elementIdArray = Regex.Split(itemToken[MmsTvJsonKey.Id].ToString(), "-", RegexOptions.IgnoreCase);
                            string ctmIdOfElement = elementIdArray[1];

                            if (ctmNodeArray.Contains(ctmIdOfElement)
                                && AisTvRefDicMission.SelectedId == (string)itemToken["missionId"]
                                && currentToken["missionNewType"].ToString() == itemToken["missionNewType"].ToString())
                            {
                                treeItem.Visibility = System.Windows.Visibility.Visible;
                            }
                            else
                            {
                                treeItem.Visibility = System.Windows.Visibility.Collapsed;
                                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                                tmpList.Add(itemToken["id"].ToString());
                                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                            }
                        }
                        break;
                    }
                }
            }
            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
            if (this.AisTvRefDicMission.IsDeleteFlag)
            {
                this.AisTvRefDicElement.DeleteMissionCtmElement(tmpList);
                tmpList.Clear();
            }
            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
        }

        /// <summary>
        /// ミッションツリーをクリックした時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AisTvRefDicMission_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // CTM・Routesを表示・非表示
            foreach (TreeViewItem treeItem in this.AisTvRefDicCtm.Items)
            {
                treeItem.IsSelected = false;
                treeItem.Foreground = new SolidColorBrush(Colors.Black);
                treeItem.Background = new SolidColorBrush(Colors.White);

                treeItem.Visibility = System.Windows.Visibility.Visible;
            }

            // Elementsを表示・非表示
            foreach (TreeViewItem treeItem in this.AisTvRefDicElement.Items)
            {
                treeItem.Visibility = System.Windows.Visibility.Visible;
            }
        }

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 Start
        private string getPeriodTextByValue(string value)
        {
            // 取得期間
            foreach (var pair in this.getPeriodItems)
            {
                if (pair.Value == value)
                {
                    return pair.Key;
                }
            }
            return string.Empty;
        }
        private string getIntervalTextByValue(string value)
        {
            // 更新周期
            foreach (var pair in this.intervalItems)
            {
                if (pair.Value == value)
                {
                    return pair.Key;
                }
            }
            return string.Empty;
        }
        private string getPeriodValue(string text)
        {
            // 取得期間
            foreach (var pair in this.getPeriodItems)
            {
                if (pair.Key == text)
                {
                    return pair.Value;
                }
            }
            return string.Empty;
        }
        private string getIntervalValue(string text)
        {
            // 更新周期
            foreach (var pair in this.intervalItems)
            {
                if (pair.Key == text)
                {
                    return pair.Value;
                }
            }
            return string.Empty;
        }
        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 End

        /// <summary>
        /// 選択されたミッションの検索条件を設定
        /// </summary>
        private void setCurrentConditions(JToken currentToken, bool isMissionSelected)
        {
            if (currentToken == null) return;

            string curSearchType = (string)currentToken["aisSearchType"];
            if (!string.IsNullOrEmpty(curSearchType))
            {
                if (int.Parse(curSearchType) == 1)
                {
                    this.radioButton_1.IsChecked = true;
                    // BugFix AIS_MM.140 20191021 yakiyama start
                    this.comboBox_GetPeriod.SelectedItem = getPeriodTextByValue((string)currentToken["onlineTime"]);
                    this.comboBox_Interval.SelectedItem = getIntervalTextByValue((string)currentToken["onlinePeriod"]);
                    // BugFix AIS_MM.140 20191021 yakiyama end

                    // ComboBoxとDateTimePickerを紐付ける
                    this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.search_Start };
                    this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.search_End };
                    this.comboBox_End.toEnd = true;

                    // ComboBoxのアイテムソースを設定
                    this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
                    this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

                    if (0 < this.comboBox_Start.Items.Count)
                    {
						if (isMissionSelected == true)
						{
							DateTime? dtStart = this.search_Start.Value;
							this.comboBox_Start.SelectedIndex = 0;
							this.search_Start.Value = dtStart;
						}
						else
						{
                            this.comboBox_Start.SelectedIndex = 0;
						}
                    }
                    if (0 < this.comboBox_End.Items.Count)
                    {
						if (isMissionSelected == true)
						{
							DateTime? dtEnd = this.search_End.Value;
                            this.comboBox_End.SelectedIndex = 0;
							this.search_End.Value = dtEnd;
						}
						else
						{
                            this.comboBox_End.SelectedIndex = 0;
						}
                    }
                }
                else if (int.Parse(curSearchType) == 2)
                {
                    this.radioButton_2.IsChecked = true;
                    // BugFix AIS_MM.140 20191021 yakiyama start
                    this.comboBox_GetPeriod.SelectedItem = "1時間前";
                    // BugFix AIS_MM.140 20191021 yakiyama end
                    // Processing On Server dn 2018/09/10 start
                    if (String.IsNullOrEmpty( getIntervalTextByValue((string)currentToken["onlinePeriod"])))
                    {
                        // BugFix AIS_MM.140 20191021 yakiyama start
                        this.comboBox_Interval.SelectedItem = "30秒";
                        // BugFix AIS_MM.140 20191021 yakiyama end
                    } else {
                        // BugFix AIS_MM.140 20191021 yakiyama start
                        this.comboBox_Interval.SelectedItem = getIntervalTextByValue((string)currentToken["onlinePeriod"]);
                        // BugFix AIS_MM.140 20191021 yakiyama end
                    }
                    // Processing On Server dn 2018/09/10 end

                    // BugFix AIS_MM.140 20191021 yakiyama start
                    this.search_Start.Value = getDateTimeByMilliseconds((long)currentToken["offlineStart"]);
                    this.search_End.Value = getDateTimeByMilliseconds((long)currentToken["offlineEnd"]);
                    // BugFix AIS_MM.140 20191021 yakiyama end
                }
            }
            //AIS-MM No.89 sunyi 20190109 start
            //先に設定されたものが変更されないようにする
            else
            {
                setSearchConditions(currentToken);
            }
            //AIS-MM No.89 sunyi 20190109 end
        }

        /// <summary>
        /// 指定ミッションの検索条件を設定
        /// </summary>
        /// <param name="JToken">missionToken</param>
        private void setSearchConditions(JToken missionToken)
        {
            int searchType = 2;
            if (this.radioButton_1.IsChecked == true)
            {
                searchType = 1;
                missionToken["aisSearchType"] = searchType;
                missionToken["onlineTime"] = getPeriodValue(this.comboBox_GetPeriod.Text);
                missionToken["onlinePeriod"] = getIntervalValue(this.comboBox_Interval.Text);
                missionToken["offlineStart"] = null;
                missionToken["offlineEnd"] = null;
            }
            else
            {
                missionToken["aisSearchType"] = searchType;
                missionToken["onlineTime"] = null;
                // Processing On Server dn 2018/09/10 start
                // OFFLINEの場合、更新周期設定
                // missionToken["onlinePeriod"] = null;
                missionToken["onlinePeriod"] = getIntervalValue(this.comboBox_Interval.Text);
                // Processing On Server dn 2018/09/10 end
                missionToken["offlineStart"] = getMillisecondsByDateTime(this.search_Start.Value);
                missionToken["offlineEnd"] = getMillisecondsByDateTime(this.search_End.Value);
            }
        }

        /// <summary>
        /// Convert DateTime to long
        /// </summary>
        /// <param name="DateTime?">value</param>
        private long getMillisecondsByDateTime(DateTime? value)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long timeStamp = (long)((DateTime)value - startTime).TotalMilliseconds;
            return timeStamp;
        }

        /// <summary>
        /// Convert lone to DateTime
        /// </summary>
        /// <param name="long">value</param>
        private DateTime getDateTimeByMilliseconds(long value)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            DateTime dt = startTime.AddMilliseconds(value);
            return dt;
        }
		// FOA_サーバー化開発 Processing On Server Xj 2018/08/27 End

        private static bool isTextAllowed(string text)
        {
            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        public override void DataSourceSelectionChanged(string removingCtmName)
        {
            //for (int i = 0; i < this.lbCtm.Items.Count; i++)
            //{
            //    var item = this.lbCtm.Items[i] as MissionCtmViewObject;
            //    if (item == null)
            //    {
            //        continue;
            //    }

            //    if (item.CtmDisplayName != removingCtmName)
            //    {
            //        continue;
            //    }

            //    this.lbCtm.SelectedIndex = i;
            //    removeListBoxItem(true, new object());
            //    setAllListBoxItemsSourceDefaultView();
            //    break;
            //}
        }

        public override void InitializeDataControl()
        {
            //for (int i = 0; i < this.lbCtm.Items.Count; i++)
            //{
            //    this.lbCtm.SelectedIndex = i;
            //    removeListBoxItem(true, new object());
            //    setAllListBoxItemsSourceDefaultView();
            //}

            //for (int i = 0; i < this.lbMission.Items.Count; i++)
            //{
            //    this.lbMission.SelectedIndex = i;
            //    removeMissionListBoxItem();
            //    setAllListBoxItemsSourceDefaultView();
            //}
            //ISSUE_NO.727 sunyi 2018/06/14 Start
            //grip mission処理ができるように修正にする
            if (this.IsSelectedProgramMission)
            {
                this.MissionListBox_Grid.Visibility = Visibility.Visible;
            }
            else
            {
                // FOA_サーバー化開発 Processing On Server Dcs 2018/08/27 Start
                //for (int i = 0; i < this.lbCtm.Items.Count; i++)
                //{
                //    this.lbCtm.SelectedIndex = i;
                //    removeListBoxItem(true, new object());
                //    setAllListBoxItemsSourceDefaultView();
                //}
                //for (int i = 0; i < this.lbMission.Items.Count; i++)
                //{
                //    this.lbMission.SelectedIndex = i;
                //    removeMissionListBoxItem();
                //    setAllListBoxItemsSourceDefaultView();
                //}
                for (int i = 0; i < this.AisTvRefDicMission.Items.Count; i++)
                {
                    removeMissionListBoxItem();
                    setAllListBoxItemsSourceDefaultView();
                }
                this.MissionListBox_Grid.Visibility = Visibility.Hidden;
                // FOA_サーバー化開発 Processing On Server Dcs 2018/08/27 End
            }
            //ISSUE_NO.727 sunyi 2018/06/14 End
        }

        #region Event Handler

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();
            this.editControl.MultiStatusMonitor = this;

            //言語が英語の場合の表示調整
            string cultureName = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;
            if (cultureName == "en-US")
            {
                this.Border.Width = 212;
                this.label_Interval.Width = 120;
                this.label_GetPeriod.Width = 120;
            }

            // No.549 Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;
        }

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 Start
        /// <summary>
        /// ミッションツリービューを選択された判定
        /// </summary>
        /// <returns>true,false</returns>
        private bool missionHasSelected()
        {
            foreach (TreeViewItem treeViewItem in AisTvRefDicMission.Items)
            {
                Color color = ((SolidColorBrush)treeViewItem.Background).Color;
                if (color == Colors.DodgerBlue)
                {
                    return true;
                }

                if(treeViewItem.IsSelected)
                {
                    return true;
                }
            }
            return false;
        }


//        private async void BtnOpenExcel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        private async void BtnOpenExcel_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            logger.Debug(MethodBase.GetCurrentMethod().Name + "() START.");
            try
            {
                //AISMM-7 sunyi 20190109 start
                //Nullチェック
                if (this.search_Start.Value == null || this.search_End.Value == null)
                {
                    FoaMessageBox.ShowError("AIS_E_021");
                    return;
                }
                //AISMM-7 sunyi 20190109 end
                // Wang Issue of AISMM-21 20181109 Start
                this.mainWindow.LoadinVisibleChange(false); //Loading.Visibility = System.Windows.Visibility.Visible;
                // Wang Issue of AISMM-21 20181109 End

                //画面検索条件チェック
                // 周期の場合
                if (this.radioButton_1.IsChecked == true)
                {
                    float periodHour1 = 0;
                    if (!float.TryParse(getPeriodValue(this.comboBox_GetPeriod.Text.Trim()), out periodHour1))
                    {
                        // Wang Issue of AISMM-21 20181109 Start
                        this.mainWindow.LoadinVisibleChange(true); //Loading.Visibility = System.Windows.Visibility.Hidden;
                        // Wang Issue of AISMM-21 20181109 End
                        FoaMessageBox.ShowError("AIS_E_033", Properties.Resources.TEXT_COLLECTIONPERIOD);
                        return;
                    }

                    // 更新周期の入力チェック
                    float periodHour2 = 0;
                    if (!float.TryParse(getIntervalValue(this.comboBox_Interval.Text.Trim()), out periodHour2))
                    {
                        // Wang Issue of AISMM-21 20181109 Start
                        this.mainWindow.LoadinVisibleChange(true); //Loading.Visibility = System.Windows.Visibility.Hidden;
                        // Wang Issue of AISMM-21 20181109 End
                        FoaMessageBox.ShowError("AIS_E_033", Properties.Resources.TEXT_UPDATINGCYCLE);
                        return;
                    }
                }

                // Wang Issue of AISMM-55 20181109 Start
                if (AisTvRefDicMission.Items.IsEmpty)
                {
                    this.mainWindow.LoadinVisibleChange(true);
                    FoaMessageBox.ShowError("AIS_E_025");
                    return;
                }
                // Wang Issue of AISMM-55 20181109 End

                //Excel出力する際に、「選択ミッション」に検索条件を再設定
                foreach (TreeViewItem treeViewItem in AisTvRefDicMission.Items)
                {
                    JToken missionToken = treeViewItem.DataContext as JToken;

                    // 1.AISを修正場合
                    // 2.ミッションアイテムを選択された場合
                    if (!this.isFirstFile || missionHasSelected())
                    {
                        if (missionToken["aisSearchType"] == null)
                        {
                            setSearchConditions(missionToken);
                        }

                        Color color = ((SolidColorBrush)treeViewItem.Background).Color;
                        if (color == Colors.DodgerBlue)
                        {
                            setSearchConditions(missionToken);
                        }
                    }
                    else
                    {
                        setSearchConditions(missionToken);
                    }
                }

                // CheckBoxTreeViewのデータソースを取得
                // Wang Issue AISTEMP-125 20190123 Start
                //JObject workplaceJson = new JObject();
                //JArray array = new JArray();
                // Wang Issue AISTEMP-125 20190123 End
                JArray jArrayMission = JArray.Parse(AisTvRefDicMission.JsonItemSource);
                JArray jArrayCtm = JArray.Parse(AisTvRefDicCtm.JsonItemSource);
                JArray jArrayElement = JArray.Parse(AisTvRefDicElement.JsonItemSource);
                logger.Debug(MethodBase.GetCurrentMethod().Name + "() jArrayMission." + Environment.NewLine + jArrayMission.ToString());
                //logger.Debug(MethodBase.GetCurrentMethod().Name + "() jArrayCtm." + Environment.NewLine + jArrayCtm.ToString());
                //logger.Debug(MethodBase.GetCurrentMethod().Name + "() jArrayElement." + Environment.NewLine + jArrayElement.ToString());

                // Processing on Server dn 2018/09/07 start
                Boolean ctmHasFlag = false;
                // 期間固定の場合,検索条件チェック
                foreach (JToken missionToken in jArrayMission)
                {

                    if (!getCheckedFlag(missionToken))
                    {
                        continue;
                    }
                    // CTM missionがあるかどうか判定(missionNewType=1:CTM mission選択)

                    if (int.Parse((string)missionToken["missionNewType"]) == 1 && ctmHasFlag == false)
                    {
                        ctmHasFlag = true;
                    }

                    int searchType = int.Parse((string)missionToken["aisSearchType"]);
                    if (searchType == 2)
                    {
                        // 期間チェック：開始日時　> 終了日時、エラーになる
                        long startTime = (long)missionToken["offlineStart"];
                        long endTime = (long)missionToken["offlineEnd"];

                        if (startTime > endTime)
                        {
                            // Wang Issue of AISMM-21 20181109 Start
                            this.mainWindow.LoadinVisibleChange(true);//Loading.Visibility = System.Windows.Visibility.Hidden;
                            // Wang Issue of AISMM-21 20181109 End
                            FoaMessageBox.ShowError("AIS_E_019");
                            return;
                        }

                        // 更新周期の入力チェック
                        float periodHour2 = 0;
                        if (!float.TryParse(getIntervalValue(this.comboBox_Interval.Text.Trim()), out periodHour2))
                        {
                            // Wang Issue of AISMM-21 20181109 Start
                            this.mainWindow.LoadinVisibleChange(true); //Loading.Visibility = System.Windows.Visibility.Hidden;
                            // Wang Issue of AISMM-21 20181109 End
                            FoaMessageBox.ShowError("AIS_E_033", Properties.Resources.TEXT_UPDATINGCYCLE);
                            return;
                        }
                    }
                }

                // Processing on Server dn 2018/09/07 end
                // 選択されたエレメントの存在フラグ
                bool exportFlag = false;

                foreach (JToken elementToken in jArrayElement)
                {
                    //エレメントが存在するの判定
                    if (!exportFlag)
                    {
                        if (getCheckedFlag(elementToken))
                        {
                            exportFlag = true;
                            break;
                        }
                    }
                }

                //ISSUE_NO.727 sunyi 2018/06/14 Start
                //grip mission処理ができるように修正にする
                if (!this.IsSelectedProgramMission)
                {

                    this.DataSource = DataSourceType.GRIP;

                }
                else
                {
                    //ISSUE_NO.727 sunyi 2018/06/14 Start
                    //if (this.missionCtmElementDict.Count < 1)
                    //{
                    //    FoaMessageBox.ShowError("AIS_E_007");
                    //    return;
                    //}
                }

                // Processing On Server dn 2018/09/26 start
                if (ctmHasFlag)
                {
                    this.DataSource = DataSourceType.NONE;
                }
                else
                {
                    this.DataSource = DataSourceType.GRIP;
                }
                // Processing On Server dn 2018/09/26 end

                // 出力するのエレメントがありません
                if (!exportFlag)
                {
                    // Wang Issue of AISMM-21 20181109 Start
                    this.mainWindow.LoadinVisibleChange(true); //Loading.Visibility = System.Windows.Visibility.Hidden;
                    // Wang Issue of AISMM-21 20181109 End
                    FoaMessageBox.ShowError("AIS_E_007");
                    return;
                }

                OpenExcelWorker = new BackgroundWorker();
                OpenExcelWorker.WorkerReportsProgress = true;
                OpenExcelWorker.DoWork += new DoWorkEventHandler(OpenExcel);
                OpenExcelWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OpenExcelCompleted);
                OpenExcelWorker.RunWorkerAsync(new OpenExcelArgument() {
                    jArrayMission = jArrayMission,
                    jArrayCtm = jArrayCtm,
                    jArrayElement = jArrayElement
                });
                // Wang Issue AISTEMP-125 20190123 End
            }
            catch (Exception ex)
            {
                // Wang Issue of AISMM-21 20181109 Start
                this.mainWindow.LoadinVisibleChange(true); //Loading.Visibility = System.Windows.Visibility.Hidden;
                throw ex;
                // Wang Issue of AISMM-21 20181109 End
            }
            logger.Debug(MethodBase.GetCurrentMethod().Name + "() END.");
        }

        // Wang Issue AISTEMP-125 20190121 Start
        class OpenExcelArgument
        {
            public JArray jArrayMission;
            public JArray jArrayCtm;
            public JArray jArrayElement;
        };
        BackgroundWorker OpenExcelWorker;
        private void OpenExcelCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }
        private void OpenExcel(object sender, DoWorkEventArgs e)
        {
            logger.Debug(MethodBase.GetCurrentMethod().Name + "() START.");
            try
            {
                //AISMM-92 sunyi 20190227 start
                //UUIDとMissionIdを関連する
                Dictionary<string, string> UUIDAndMissionIds = new Dictionary<string, string>();
                //AISMM-92 sunyi 20190227 start
                OpenExcelArgument argument = e.Argument as OpenExcelArgument;
                JArray jArrayElement = argument.jArrayElement;
                JArray jArrayMission = argument.jArrayMission;
                JArray jArrayCtm = argument.jArrayCtm;
                //logger.Debug(MethodBase.GetCurrentMethod().Name + "() jArrayElement: " + jArrayElement.ToString());
                //logger.Debug(MethodBase.GetCurrentMethod().Name + "() jArrayMission: " + jArrayMission.ToString());
                //logger.Debug(MethodBase.GetCurrentMethod().Name + "() jArrayCtm: " + jArrayCtm.ToString());

                JArray array = new JArray();
                JObject workplaceJson = new JObject();

                string workFile = createWorkingFile();

                DateTime end = DateTime.Now;
                DateTime start = DateTime.Now;

                // get mission result
                string resultFolder = string.Empty;
                string rootFolder = string.Empty;
                string path = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission");
                if (Directory.Exists(path))
                {
                    FoaCore.Util.FileUtil.clearFolder(path);
                }

                List<int> listInterval = new List<int>();
                int routeIndex = 0;
                int displayOrder = 0;
                JArray externalFileArray = JArray.Parse("[]");
                foreach (JToken missionToken in jArrayMission)
                {
                    if (!getCheckedFlag(missionToken))
                    {
                        continue;
                    }
                    if(missionToken["t"] == null)
                    {
                        externalFileArray.Add(JToken.Parse(missionToken["data"].ToString()));
                        continue;
                    }
                    int searchType = int.Parse((string)missionToken["aisSearchType"]);
                    // Processing On Server dn 2018/09/10 start
                    // 検索タイプと関係ないので、すべての更新周期を追加する。
                    // 2020/10/01 by Chu Yimin  期間再設定のために、Workplaceの設定を外す
                    // if (searchType == 1)
                    // {
                    //     listInterval.Add(int.Parse((string)missionToken["onlinePeriod"]));
                    // }
                    listInterval.Add(int.Parse((string)missionToken["onlinePeriod"]));
                    // Processing On server dn 2018/09/10 end


                    // 開始終了時間を設定
                    convertStartAndEndTime(missionToken, out start, out end);

                    this.DownloadCts = new CancellationTokenSource();

                    string missionTokenId = missionToken[MmsTvJsonKey.Id].ToString();
                    if (getMissionType(missionToken) == 1)
                    { //CTM Mission
                        //AISMM-92 sunyi 20190227 start
                        resultFolder = GetMissionCtm(start, end, missionTokenId, this.DownloadCts,null, UUIDAndMissionIds).GetAwaiter().GetResult();
                        //AISMM-92 sunyi 20190227 end
                        if (resultFolder == null)
                        {
                            continue;
                        }
                        DirectoryInfo di = new DirectoryInfo(resultFolder);
                        rootFolder = di.Parent.FullName;
                    }
                    else
                    { //Grip Mission
                        missionToken["routeIndex"] = routeIndex;
                        //AISMM-92 sunyi 20190227 start
                        resultFolder = GetGripData(start, end, missionToken,UUIDAndMissionIds).GetAwaiter().GetResult();
                        //AISMM-92 sunyi 20190227 end
                        rootFolder = string.Empty;
                        if (resultFolder == null)
                        {
                            logger.Debug("指定した期間「" + start + " ～ " + end + "」で「" + missionTokenId + "」のデータが存在しません。");
                        }
                        else
                        {
                            routeIndex++;
                        }
                    }

                    // 結果フォルダを設定
                    missionToken["resultFolder"] = resultFolder;
                    missionToken["rootFolder"] = rootFolder;
                    // Processing On Server dn 2018/09/13 start
                    // 選択ミッションの表示順
                    missionToken["displayOrder"] = displayOrder;
                    displayOrder++;
                    // Processing On Server dn 2018/09/13 end
                    // Ctmsデータを設定
                    JArray tempCtmChildren = new JArray();
                    foreach (JToken jToken in jArrayCtm)
                    {
                        if (missionTokenId == jToken["missionId"].ToString())
                        {
                            tempCtmChildren.Add(jToken);
                        }
                    }
                    missionToken["ctms"] = (JArray)tempCtmChildren;

                    // Elementsデータを設定
                    JArray tempElementChildren = new JArray();
                    foreach (JToken elementToken in jArrayElement)
                    {
                        if (missionTokenId == elementToken["missionId"].ToString())
                        {
                            tempElementChildren.Add(elementToken);
                        }
                    }
                    missionToken["elements"] = (JArray)tempElementChildren;

                    // Mission Arrayを設定
                    array.Add(missionToken);
                }

                Dictionary<CtmObject, List<CtmElement>> ctmElementDict = new Dictionary<CtmObject, List<CtmElement>>();
                foreach (JToken elementToken in jArrayElement)
                {
                    if (getMissionType(elementToken) == 0)
                    {
                        //continue;
                    }

                    if (getCheckedFlag(elementToken))
                    {
                        string missionId = elementToken["missionId"].ToString();
                        string strElmToken = elementToken.ToString();
                        strElmToken = strElmToken.Replace(missionId + "-", "");
                        JToken tmpToken = JToken.Parse(strElmToken);

                        CtmObject cc = JsonConvert.DeserializeObject<CtmObject>(tmpToken.ToString());
                        List<CtmElement> listElement = new List<CtmElement>();
                        JArray eleGroupArray = JArray.Parse(tmpToken["children"].ToString());
                        foreach (JToken group in eleGroupArray)
                        {
                            //AIS_DAC No.76 sunyi 2018/12/05 start
                            //group["children"]がNullの場合、チェックする
                            if (group["children"] == null)
                            {
                                //AISMM-94 sunyi 2019/03/27 start
                                //Groupがないエレメントをセット
                                if (getCheckedFlag(group))
                                {
                                    CtmElement objElement = new CtmElement();
                                    objElement = JsonConvert.DeserializeObject<CtmElement>(group.ToString());
                                    listElement.Add(objElement);
                                }
                                //AISMM-94 sunyi 2019/03/27 end
                                continue;
                            }
                            //AIS_DAC No.76 sunyi 2018/12/05 end
                            JArray eleArray = JArray.Parse(group["children"].ToString());
                            foreach (JToken ele in eleArray)
                            {
                                if (!getCheckedFlag(ele))
                                {
                                    continue;
                                }

                                CtmElement objElement = new CtmElement();
                                objElement = JsonConvert.DeserializeObject<CtmElement>(ele.ToString());
                                listElement.Add(objElement);
                            }
                        }

                        ctmElementDict.Add(cc, listElement);
                    }
                }

                // MIN Interval.
                int minInterval = 0;
                if (listInterval.Count > 0)
                {
                    minInterval = listInterval.Min();
                }

                // オンライングラフID
                OnlineId = new FoaCore.Common.Util.GUID().ToString();

                workplaceJson["mission_data"] = array;
                workplaceJson["work_place_flag"] = 0;
                workplaceJson["file_type"] = 0;
                workplaceJson["file_id"] = OnlineId;
                workplaceJson["file_name"] = OnlineId;
                workplaceJson["external_file_list"] = externalFileArray.ToString();
                Dispatcher.Invoke(() =>
                {
                    CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
                    client.CompleteHandler += resJson =>
                    {
                        try
                        {
                            string fileName = resJson + ".zip";

                            if (string.IsNullOrEmpty(resJson))
                            {
                                logger.Info("WorkPlaceデータを作成が失敗しました。");
                                return;
                            }
                            logger.Debug(MethodBase.GetCurrentMethod().Name + "() EXCEL-OPEN START.");
                            //excelOpen(workFile, start, end, resultRootFolder, periodHour, searchType, resJson);
                            string DlDir = System.AppDomain.CurrentDomain.BaseDirectory + "temp";
                            if(!Directory.Exists(DlDir))
                            {
                                DirectoryInfo info = Directory.CreateDirectory(DlDir);
                            }
                            if (!Directory.Exists(Path.Combine(DlDir, resJson)))
                            {
                                DirectoryInfo info = Directory.CreateDirectory(Path.Combine(DlDir, resJson));
                            }
                            fileName = Path.Combine(DlDir, resJson, fileName);
                            CmsHttpClient externalClient = new CmsHttpClient(Application.Current.MainWindow);
                            externalClient.CompleteHandler += (response) =>
                            {
                                ZipFileHandler ziphandler = new ZipFileHandler();
                                string resultfolder = ziphandler.unZipFile(fileName);
                                string[] filenames = Directory.GetFiles(resultfolder, "*.csv", SearchOption.TopDirectoryOnly);
                                List<string> csvFileList = new List<string>();
                                foreach (var item in filenames)
                                {
                                    csvFileList.Add(item);
                                }
                                 excelOpen(workFile, array, ctmElementDict, resJson, minInterval, UUIDAndMissionIds, jArrayCtm, csvFileList);
                                logger.Debug(MethodBase.GetCurrentMethod().Name + "() EXCEL-OPEN END.");

                            };
                            externalClient.ExceptionHandler += (ex, objs) =>
                            {
                                logger.Error((ex as Exception).Message);
                            };
                            externalClient.AddParam("workplaceId",resJson);
                            externalClient.FileDownload(ExcelEngineUrl.Workplace.GetExternalCsvFiles(),fileName);
                        }
                        finally
                        {
                            logger.Debug(MethodBase.GetCurrentMethod().Name + "() EXCEL-OPEN FINALIZE.");
                            // Wang Issue of AISMM-21 20181109 Start
                            this.mainWindow.LoadinVisibleChange(true); //Loading.Visibility = System.Windows.Visibility.Hidden;
                            // Wang Issue of AISMM-21 20181109 End
                        }
                    };

                    client.ExceptionHandler += (ex, objs) =>
                    {
                        this.mainWindow.LoadinVisibleChange(true); //Loading.Visibility = System.Windows.Visibility.Hidden;
                        logger.Error("サーバへの接続が失敗しました");
                    };
                    client.Post(CmsUrl.GetAisCreate(), workplaceJson.ToString());
                });
            }
            catch (Exception ex)
            {
                Dispatcher.Invoke(() =>
                {
                    // Wang Issue of AISMM-21 20181109 Start
                    this.mainWindow.LoadinVisibleChange(true); //Loading.Visibility = System.Windows.Visibility.Hidden;
                    // Wang Issue of AISMM-21 20181109 End
                    //throw ex;
                    logger.Error("サーバからのレスポンスが200以外でした exception.message=" + ex.Message);
                    System.Windows.MessageBox.Show("サーバからデータの取得ができませんでした。ミッション、CTM等が消去されている場合がありますので確認してください。");
                });
            }
            logger.Debug(MethodBase.GetCurrentMethod().Name + "() END.");
        }
        // Wang Issue AISTEMP-125 20190121 End

        private void convertStartAndEndTime(JToken missionToken, out DateTime startTime, out DateTime endTime)
        {
            DateTime end = DateTime.Now;
            DateTime start = DateTime.Now;
            int searchType = int.Parse((string)missionToken["aisSearchType"]);
            float periodHour = 0;

            //画面検索条件チェック
            if (searchType == 1)
            {
                periodHour = float.Parse((string)missionToken["onlineTime"]);
                TimeSpan period = TimeSpan.FromHours(periodHour);
                start = end - period;
            }
            else
            {
                start = getDateTimeByMilliseconds((long)missionToken["offlineStart"]);
                end = getDateTimeByMilliseconds((long)missionToken["offlineEnd"]);
            }

            missionToken["startSearchTime"] = start;
            missionToken["endSearchTime"] = end;
            startTime = start;
            endTime = end;
        }
        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 End

        private string createWorkingFile()
        {
            //TODO
            // 発行したファイルを使用しない
            string filePathSrc = isFirstFile ? System.Windows.Forms.Application.StartupPath + @"\template\MultiStatusMonitorTemplate.xlsm" : this.excelFilePath;

            string fileName = string.Format("{0}_{1}.xlsm", "Multi_StatusMonitor", DateTime.Now.ToString("yyyyMMddHHmmss"));
            var filePathDest = System.IO.Path.Combine(resultDir, fileName);
            File.Copy(filePathSrc, filePathDest);
            return filePathDest;
        }

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 Start
        //private void excelOpen(string workfile, DateTime start, DateTime end, string resuloFolderPath, float periodHour, int searchType, string workplaceId)
        private void excelOpen(string workfile, JArray resultArray, Dictionary<CtmObject, List<CtmElement>> ctmElementDic, string workplaceId, int minInterval, Dictionary<string, string> UUIDAndMissionIds = null, JArray jarryCtms = null, List<string> ExternalFileList = null)
        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 End
        {
            logger.Debug(MethodBase.GetCurrentMethod().Name + "() START. workfile:[" + workfile + "]");

            this.Bw = new System.ComponentModel.BackgroundWorker();
            this.Bw.WorkerSupportsCancellation = true;

            string excelName = workfile;

            //ISSUE_NO.727 sunyi 2018/06/14 Start
            //grip mission処理ができるように修正にする
            //var writer = new InteropExcelWriterMultiStatusMonitor(this.Mode, this.DataSource, this.editControl.Ctms, null, this.Bw, null, this.missionCtmDict, this.missionCtmElementDict);
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 Start
            var writer = new InteropExcelWriterMultiStatusMonitor(this.Mode, this.DataSource, this.editControl.Ctms, null, this.Bw, this.currentSelectedMission, this.missionCtmDict, this.missionCtmElementDict, OnlineId, workplaceId, resultArray, ctmElementDic, UUIDAndMissionIds);
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 End
            //ISSUE_NO.727 sunyi 2018/06/14 End

            var configParams = new Dictionary<ExcelConfigParam, object>();

            DateTime start1;
            DateTime end1;
            bool gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out start1, out end1);
            if (!gotTime)
            {
                return;
            }
            //if (searchType == 1) {
            //    configParams.Add(ExcelConfigParam.DISPLAY_START, "");
            //    configParams.Add(ExcelConfigParam.DISPLAY_END, "");
            //}
            //else
            //{
            //  configParams.Add(ExcelConfigParam.DISPLAY_START, start);
            //  configParams.Add(ExcelConfigParam.DISPLAY_END, end);
            //}
            configParams.Add(ExcelConfigParam.DISPLAY_START, start1);
            configParams.Add(ExcelConfigParam.DISPLAY_END, end1);
            //configParams.Add(ExcelConfigParam.START, start);
            //configParams.Add(ExcelConfigParam.END, end);
            //configParams.Add(ExcelConfigParam.RELOAD_INTERVAL, int.Parse(this.comboBox_Interval.Text));

            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 Start
            // 最小更新周期を設定
            configParams.Add(ExcelConfigParam.RELOAD_INTERVAL, minInterval);
			// FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 End

            //configParams.Add(ExcelConfigParam.GET_PERIOD, periodHour);

            if (this.DataSource == DataSourceType.MISSION)
            {
                //configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                //configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetCatalogLang(this.SelectedMission));
            }
            else if (this.DataSource == DataSourceType.GRIP)
            {
                //configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                //ISSUE_NO.727 sunyi 2018/06/14 Start
                //grip mission処理ができるように修正にする
                //configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetCatalogLang(this.SelectedMission));
                //configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetGripCatalogLang(this.SelectedGripMission));
                //ISSUE_NO.727 sunyi 2018/06/14 End
            }

            if (!isFirstFile)
            {
                configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(this.excelFilePath));
                configParams.Add(ExcelConfigParam.EDIT_MODE, Properties.Resources.TEXT_TEMPLATE_PARAM_EDITMODE);  //BugNo.546 Added for display bug. paramに編集モード項目追加.
            }
            List<string> configParamTextList = new List<string>();
            foreach (var key in configParams.Keys)
            {
                configParamTextList.Add(key.ToString() + ":" + configParams[key].ToString());
            }
            logger.Debug(MethodBase.GetCurrentMethod().Name + "(). configParams:[" + string.Join(", ", configParamTextList.ToArray()) + "]");

			// FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 Start
            //writer.WriteCtmData(excelName, resuloFolderPath, configParams);
            writer.WriteCtmDataForAll(excelName, configParams, jarryCtms, ExternalFileList);
			// FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 End

            logger.Debug(MethodBase.GetCurrentMethod().Name + "() END. workfile:[" + workfile + "]");
        }

        private void lbMission_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (lbMission.SelectedIndex == -1)
            //{
            //    setAllListBoxItemsSourceDefaultView();
            //    return;
            //}

            //MissionViewObject missionViewObj = lbMission.Items[lbMission.SelectedIndex] as MissionViewObject;
            //if (missionViewObj == null)
            //{
            //    return;
            //}

            //// Set the CTM & Element DataGrid

            ////TODO start xujing 2018/07/20
            //if (this.Mode != ControlMode.MultiStatusMonitor)
            //{
            //    this.editControl.SetElementDataToDataGrid(missionViewObj.MissionId);
            //}
            ////TODO end xujing 2018/07/20

            //// Set currentSelectedMission object
            //this.currentSelectedMission = JsonConvert.DeserializeObject<Mission>(missionViewObj.Json);

            //// Clear the selection on the TreeView
            //if (mainWindow.CtrlMt.tree.SelectedItem != null)
            //{
            //    mainWindow.CtrlMt.tree.ClearSelection();
            //}

            //// Set the CTM & Element ListBox ItemsSource for that selectedMission
            //// 1. Selected CTM
            //List<CtmObject> listSelectedCtm = missionCtmDict.Where(x => x.Key.Id == currentSelectedMission.Id).FirstOrDefault().Value;
            //setCtmListBoxItemsSource(listSelectedCtm, currentSelectedMission.Id);

            //// 2. Selected Element
            //Dictionary<CtmObject, List<CtmElement>> selectedCtmElementDict = new Dictionary<CtmObject, List<CtmElement>>();
            //foreach (CtmObject ctm in listSelectedCtm)
            //{
            //    List<CtmElement> listSelectedElement = new List<CtmElement>();
            //    foreach (CtmElement ele in missionCtmElementDict.Where(x => x.Key.id.ToString() == ctm.id.ToString()).FirstOrDefault().Value)
            //    {
            //        if (!listSelectedElement.Contains(ele))
            //        {
            //            listSelectedElement.Add(ele);
            //        }
            //    }
            //    selectedCtmElementDict[ctm] = listSelectedElement;
            //}
            //setElementListBoxItemsSource(selectedCtmElementDict);
        }

		// FOA_サーバー化開発 Processing On Server Xj 2018/08/09 Start
        /// <summary>
        /// ミッションツリーにチェックボックスをチェックする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AisTvRefDicElementCheckChanged(object sender, System.EventArgs e)
        {
            commonCheck(sender, e);
        }

        /// <summary>
        /// 「CTM/Routes」ツリーにチェックボックスをチェックする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AisTvRefDicCtmCheckChanged(object sender, System.EventArgs e)
        {
            commonCheck(sender, e);
        }

        /// <summary>
        /// エレメントツリーにチェックボックスをチェックする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AisTvRefDicMissionCheckChanged(object sender, System.EventArgs e)
        {
            commonCheck(sender, e);
        }

        /// <summary>
        /// ツリーアイテムからチェックボックスを取得します。
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private CheckBox GetCheckBox(TreeViewItem item)
        {
            StackPanel st = (StackPanel)item.Header;
            UIElementCollection uiCol = st.Children;
            foreach (UIElement ui in uiCol)
            {
                if (ui is CheckBox)
                {
                    return (CheckBox)ui;
                }
            }
            return null;
        }

        /// <summary>
        /// ctmIdにより、エレメントツリーにチェックボックスを設定
        /// </summary>
        /// <param name="string">ctmId</param>
        /// <param name="CheckBox">checkBox</param>
        private void checkedElementsByCtmId(string ctmId, CheckBox checkBox)
        {
            if (string.IsNullOrEmpty(ctmId))
            {
                return;
            }
            bool isCheck = false;
            if (checkBox.IsChecked == null || checkBox.IsChecked == true)
            {
                isCheck = true;
            }
            else
            {
                isCheck = false;
            }
            this.AisTvRefDicElement.SetCheckState(ctmId, isCheck);
        }

        /// <summary>
        /// ctmIdにより、「Ctm/Routes」ツリーにチェックボックスを設定
        /// </summary>
        /// <param name="string">id</param>
        /// <param name="CheckBox">checkBox</param>
        private void checkedCtm(string id, CheckBox checkBox)
        {
            if (string.IsNullOrEmpty(id))
            {
                return;
            }
            TreeViewItem treeViewItem = this.AisTvRefDicCtm.GetTreeViewItem(id);
            CheckBox tempCheckBox = GetCheckBox(treeViewItem);
            if (tempCheckBox != null)
            {
                if (checkBox.IsChecked == null || checkBox.IsChecked == true)
                {
                    tempCheckBox.IsChecked = true;
                    setCheckedFlag(treeViewItem, true);
                }
                else
                {
                    tempCheckBox.IsChecked = false;
                    setCheckedFlag(treeViewItem, false);
                }
            }
        }

        /// <summary>
        /// ミッションチェックボックスを設定
        /// </summary>
        /// <param name="string">id</param>
        /// <param name="bool">isChecked</param>
        private void checkedMission(string id, bool isChecked)
        {
            if (string.IsNullOrEmpty(id))
            {
                return;
            }
            TreeViewItem treeViewItem = this.AisTvRefDicMission.GetTreeViewItem(id);
            CheckBox tempCheckBox = GetCheckBox(treeViewItem);
            if (tempCheckBox != null)
            {
                if (isChecked == true)
                {
                    tempCheckBox.IsChecked = true;
                }
                else
                {
                    tempCheckBox.IsChecked = false;
                }
                setCheckedFlag(treeViewItem, isChecked);
            }
        }

        /// <summary>
        /// checkedFlagを設定
        /// </summary>
        /// <param name="TreeViewItem">treeViewItem</param>
        /// <param name="object">isChecked</param>
        private void setCheckedFlag(TreeViewItem treeViewItem, object isChecked)
        {
            JToken currentToken = treeViewItem.DataContext as JToken;
            if (isChecked == null)
            {
                currentToken["checkedFlag"] = null;
            }
            else
            {
                currentToken["checkedFlag"] = (bool)isChecked;
            }
        }

        /// <summary>
        /// ミッションタイプを取得します。
        /// </summary>
        /// <param name="token"></param>
        /// <returns>0:Grip Mision, 1:Ctm Mission</returns>
        private int getMissionType(JToken token) {

            // Grip
            int iType = 0;

            string missionType = (string)token["missionNewType"];
            if(!string.IsNullOrEmpty(missionType))
            {
                iType = int.Parse(missionType);
            }
            return iType;
        }

        /// <summary>
        /// チェックフラグを取得します。
        /// </summary>
        /// <param name="token"></param>
        /// <returns>true, false</returns>
        private bool getCheckedFlag(JToken token)
        {

            object ctmCheckedFlag = token["checkedFlag"];
            if (ctmCheckedFlag == null || ctmCheckedFlag.ToString() == "" || Convert.ToBoolean(ctmCheckedFlag) == true)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// （共通）ツリーにチェックボックスをチェックするの処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commonCheck(object sender, System.EventArgs e)
        {
            TreeView treeView = (TreeView)sender;

            EventArgsEx ex = (EventArgsEx)e;

            string missionId = string.Empty;
            bool isMissionCheck = false;

            if (ex.MissionType == 1)
            {
                // CTM場合
                if (treeView.Name == "AisTvRefDicElement")
                {
                    string checkedMissionId = ex.Key.Split('-')[0];

                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 0)
                        {
                            continue;
                        }

                        missionId = (string)tempChildItemToken["missionId"];
                        if (checkedMissionId == missionId)
                        {
                            if (tempCheckBox != null)
                            {
                                if (isMissionCheck == false)
                                {
                                    if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                    {
                                        isMissionCheck = true;
                                    }
                                }

                                checkedCtm((string)tempChildItemToken["id"], tempCheckBox);
                            }
                        }
                    }

                    checkedMission(checkedMissionId, isMissionCheck);

                }
                else if (treeView.Name == "AisTvRefDicCtm")
                {
                    string checkedMissionId = ex.Key.Split('-')[0];

                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 0)
                        {
                            continue;
                        }
                        missionId = (string)tempChildItemToken["missionId"];
                        if (checkedMissionId == missionId)
                        {
                            if (tempCheckBox != null)
                            {
                                if (isMissionCheck == false)
                                {
                                    if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                    {
                                        isMissionCheck = true;
                                    }
                                }
                                if (ex.Key == (string)tempChildItemToken["id"])
                                {
                                    checkedElementsByCtmId((string)tempChildItemToken["id"], tempCheckBox);
                                }
                            }
                        }
                    }

                    checkedMission(checkedMissionId, isMissionCheck);

                }
                else if (treeView.Name == "AisTvRefDicMission")
                {
                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 0)
                        {
                            continue;
                        }
                        if (tempCheckBox != null && ex.Key == (string)tempChildItemToken["id"])
                        {
                            foreach (TreeViewItem ctmItem in this.AisTvRefDicCtm.Items)
                            {
                                CheckBox ctmCheckBox = GetCheckBox(ctmItem);
                                JToken ctmChildItemToken = ctmItem.DataContext as JToken;
                                if (ex.Key == (string)ctmChildItemToken["missionId"])
                                {
                                    if (ctmCheckBox != null)
                                    {
                                        if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                        {
                                            ctmCheckBox.IsChecked = true;
                                            setCheckedFlag(ctmItem, true);
                                        }
                                        else
                                        {
                                            ctmCheckBox.IsChecked = false;
                                            setCheckedFlag(ctmItem, false);
                                        }

                                        checkedElementsByCtmId((string)ctmChildItemToken["id"], tempCheckBox);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (ex.MissionType == 0)
            {
                // Grip場合
                if (treeView.Name == "AisTvRefDicCtm")
                {
                    string checkedMissionId = ex.Key.Split('_')[0];

                    // チェックするのCtmsデータ
                    Dictionary<string, string> checkedDic = new Dictionary<string, string>();
                    // アンチェックするのCtmsデータ
                    Dictionary<string, string> unCheckedDic = new Dictionary<string, string>();

                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 1)
                        {
                            continue;
                        }

                        if (checkedMissionId == (string)tempChildItemToken["missionId"])
                        {
                            if (tempCheckBox != null)
                            {
                                string[] nodesArray = ((string)tempChildItemToken["nodes"]).Split(',');
                                if (nodesArray.Length > 0)
                                {
                                    if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                    {
                                        // チェックするのCtmsデータを作成
                                        foreach (string sttringCtmId in nodesArray)
                                        {
                                            if (checkedDic.ContainsKey(sttringCtmId))
                                            {
                                                continue;
                                            }
                                            checkedDic.Add(sttringCtmId, sttringCtmId);
                                        }
                                    }
                                    else
                                    {
                                        // アンチェックするのCtmsデータを作成
                                        foreach (string sttringCtmId in nodesArray)
                                        {
                                            if (unCheckedDic.ContainsKey(sttringCtmId))
                                            {
                                                continue;
                                            }
                                            unCheckedDic.Add(sttringCtmId, sttringCtmId);
                                        }
                                    }
                                }

                                if (isMissionCheck == false)
                                {
                                    if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                    {
                                        isMissionCheck = true;
                                    }
                                }

                            }
                        }
                    }
                    // Missionチェック処理
                    checkedMission(checkedMissionId, isMissionCheck);

                    // Elementsチェック処理
                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 1)
                        {
                            continue;
                        }

                        missionId = (string)tempChildItemToken["missionId"];
                        if (checkedMissionId == missionId)
                        {
                            if (tempCheckBox != null)
                            {
                                string[] nodesArray = ((string)tempChildItemToken["nodes"]).Split(',');
                                if (nodesArray.Length == 0) continue;
                                foreach (string sttringCtmId in nodesArray)
                                {
                                    if (ex.thisChecked)
                                    {
                                        this.AisTvRefDicElement.SetCheckState(missionId + "-" + sttringCtmId, false);
                                    }
                                    else
                                    {
                                        this.AisTvRefDicElement.SetCheckState(missionId + "-" + sttringCtmId, true);
                                    }
                                }

                                if (ex.thisChecked)
                                {
                                    foreach(KeyValuePair<string, string> item in checkedDic)
                                    {
                                        this.AisTvRefDicElement.SetCheckState(missionId + "-" + item.Key, true);
                                    }
                                }
                                else
                                {
                                    foreach (KeyValuePair<string, string> item in unCheckedDic)
                                    {
                                        this.AisTvRefDicElement.SetCheckState(missionId + "-" + item.Key, false);
                                    }
                                }
                            }
                        }
                    }

                }
                else if (treeView.Name == "AisTvRefDicMission")
                {
                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 1)
                        {
                            continue;
                        }
                        if (tempCheckBox != null && ex.Key == (string)tempChildItemToken["id"])
                        {
                            foreach (TreeViewItem ctmItem in this.AisTvRefDicCtm.Items)
                            {
                                CheckBox ctmCheckBox = GetCheckBox(ctmItem);
                                JToken ctmChildItemToken = ctmItem.DataContext as JToken;
                                if (ex.Key == (string)ctmChildItemToken["missionId"])
                                {
                                    if (ctmCheckBox != null)
                                    {
                                        if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                        {
                                            ctmCheckBox.IsChecked = true;
                                            setCheckedFlag(ctmItem, true);
                                        }
                                        else
                                        {
                                            ctmCheckBox.IsChecked = false;
                                            setCheckedFlag(ctmItem, false);
                                        }

                                        string[] nodesArray = ((string)ctmChildItemToken["nodes"]).Split(',');
                                        if (nodesArray.Length == 0) continue;

                                        foreach(string sttringCtmId in nodesArray)
                                        {
                                            checkedElementsByCtmId(ex.Key + "-" + sttringCtmId, tempCheckBox);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
		// FOA_サーバー化開発 Processing On Server Xj 2018/08/09 End

        private void AisTvRefDicElement_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
        }

        private void AisTvRefDicCtm_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            //AISMM-7 sunyi 20190109 start
            //Nullチェック
            if (this.search_Start.Value == null || this.search_End.Value == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return;
            }
            //AISMM-7 sunyi 20190109 end
            // FOA_サーバー化開発 Processing On Server Xj 2018/08/29 Start
			//今選択するのミッション
            JToken currentToken = this.AisTvRefDicCtm.GetSelectedToken();
            if (currentToken == null) return;

            // 色を設定
            foreach (TreeViewItem treeItem in AisTvRefDicCtm.Items)
            {
                treeItem.Foreground = new SolidColorBrush(Colors.Black);
                treeItem.Background = new SolidColorBrush(Colors.White);
            }

            // 選択された行の色を設定
            TreeViewItem thisTreeItem = this.AisTvRefDicCtm.GetItem(this.AisTvRefDicCtm.SelectedId);
            thisTreeItem.Foreground = new SolidColorBrush(Colors.White);
            thisTreeItem.Background = new SolidColorBrush(Colors.DodgerBlue);

            // 色を設定
            foreach (TreeViewItem treeItem in AisTvRefDicMission.Items)
            {
                treeItem.IsSelected = false;
                treeItem.Foreground = new SolidColorBrush(Colors.Black);
                treeItem.Background = new SolidColorBrush(Colors.White);

                JToken jToken = treeItem.DataContext as JToken;
                if ((string)jToken["id"] == (string)currentToken["missionId"])
                {
                    // BugFix AIS_MM.140 20191021 yakiyama start
                    setCurrentConditions(jToken, true);
                    // BugFix AIS_MM.140 20191021 yakiyama end
                    treeItem.Foreground = new SolidColorBrush(Colors.White);
                    treeItem.Background = new SolidColorBrush(Colors.DodgerBlue);
                }
            }

            //画面のElement項目を選択内容をクッリン
            foreach (TreeViewItem elementTreeViewItem in AisTvRefDicElement.Items)
            {
                JToken elementItemToken = elementTreeViewItem.DataContext as JToken;
                //CTMの場合
                if (getMissionType(currentToken) == 1)
                {
                    if (currentToken[MmsTvJsonKey.Id].ToString() == elementItemToken[MmsTvJsonKey.Id].ToString()
                        && getMissionType(currentToken) == getMissionType(elementItemToken))
                    {
                        elementTreeViewItem.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        elementTreeViewItem.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }
                //Gripの場合
                else if (getMissionType(currentToken) == 0)
                {
                    string[] elementIdArray = Regex.Split(elementItemToken[MmsTvJsonKey.Id].ToString(), "-", RegexOptions.IgnoreCase);
                    string ctmIdOfElement = elementIdArray[1];

                    string[] ctmNodeArray = Regex.Split(currentToken["nodes"].ToString(), ",", RegexOptions.IgnoreCase);

                    if (ctmNodeArray.Contains(ctmIdOfElement) && currentToken["missionId"].ToString() == elementItemToken["missionId"].ToString()
                        && getMissionType(currentToken) == getMissionType(elementItemToken))
                    {
                        elementTreeViewItem.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        elementTreeViewItem.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }
            }

            //if (lbCtm.SelectedIndex == -1)
            //{
            //    if (lbMission.SelectedIndex == -1)
            //    {
            //        setAllListBoxItemsSourceDefaultView();
            //    }
            //    else // A mission is currently selected in the MissionListBox
            //    {
            //        MissionViewObject missionViewObj = lbMission.Items[lbMission.SelectedIndex] as MissionViewObject;
            //        if (missionViewObj == null)
            //        {
            //            return;
            //        }

            //        List<CtmObject> listSelectedCtm = missionCtmDict.Where(x => x.Key.Id == missionViewObj.MissionId).FirstOrDefault().Value;
            //        setCtmListBoxItemsSource(listSelectedCtm, missionViewObj.MissionId);

            //        Dictionary<CtmObject, List<CtmElement>> selectedCtmElementDict = new Dictionary<CtmObject, List<CtmElement>>();
            //        foreach (CtmObject ctm in listSelectedCtm)
            //        {
            //            List<CtmElement> listSelectedElement = new List<CtmElement>();
            //            foreach (CtmElement ele in missionCtmElementDict.Where(x => x.Key.id.ToString() == ctm.id.ToString()).FirstOrDefault().Value)
            //            {
            //                if (!listSelectedElement.Contains(ele))
            //                {
            //                    listSelectedElement.Add(ele);
            //                }
            //            }
            //            selectedCtmElementDict[ctm] = listSelectedElement;
            //        }
            //        setElementListBoxItemsSource(selectedCtmElementDict);
            //    }

            //    return;
            //}

            //MissionCtmViewObject missionCtmViewObj = lbCtm.Items[lbCtm.SelectedIndex] as MissionCtmViewObject;
            //if (missionCtmViewObj == null)
            //{
            //    return;
            //}

            //List<CtmElement> selectedCtmElementList = missionCtmElementDict.Where(x => x.Key.id.ToString() == missionCtmViewObj.CtmId).FirstOrDefault().Value;
            //if (selectedCtmElementList != null)
            //{
            //    setElementListBoxItemsSource(selectedCtmElementList, missionCtmViewObj.CtmId, missionCtmViewObj.ParentMissionId);
            //}
            //else
            //{
            //    this.lbCtm.SelectedIndex = -1;
            //}
			// FOA_サーバー化開発 Processing On Server Xj 2018/08/29 End
        }
        private void lbCtm_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
            {
            //lbCtm.SelectedIndex = -1; // clear CtmListBox selection
            }
        private void MenuRemoveCtm_Click(object sender, RoutedEventArgs e)
        {

            removeListBoxItem(true, sender);
            setAllListBoxItemsSourceDefaultView();
        }

        private void lbElement_Drop(object sender, DragEventArgs e)
        {
            if (isDragCtmName)
            {
                return;
            }

            string[] itemArray = getItemArrayFromDropEvent(sender, e);
            if (itemArray == null)
            {
                return;
            }

            if (this.mainWindow.TabCtrlDS.SelectedIndex == 0)
            {
                this.DataSource = DataSourceType.CTM_DIRECT;
            }
            else
            {
                this.DataSource = DataSourceType.MISSION;
            }

            setAllListBoxItemsSource(itemArray, isDragCtmName);
        }

        private void MenuRemoveElement_Click(object sender, RoutedEventArgs e)
        {
            //if (lbElement.SelectedIndex == -1)
            //{
            //    return;
            //}

            removeListBoxItem(false, sender);
            setAllListBoxItemsSourceDefaultView();
        }

        private void lbCtm_Drop(object sender, DragEventArgs e)
        {
            if (!isDragCtmName)
            {
                return;
            }

            string[] itemArray = getItemArrayFromDropEvent(sender, e);
            if (itemArray == null) return;

            if (this.mainWindow.TabCtrlDS.SelectedIndex == 0)
            {
                this.DataSource = DataSourceType.CTM_DIRECT;
            }
            else
            {
                this.DataSource = DataSourceType.MISSION;
            }

            setAllListBoxItemsSource(itemArray, isDragCtmName);
        }

        private void lbMission_Drop(object sender, DragEventArgs e)
        {
            var senderObj = sender as System.Windows.Controls.ListBox;
            if (senderObj == null ||
                !e.Data.GetDataPresent(DataFormats.Serializable))
            {
                return;
            }

            JToken tokenItem = (JToken)e.Data.GetData(DataFormats.Serializable);
            Mission droppedMission = JsonConvert.DeserializeObject<Mission>(tokenItem.ToString());

            InsertMissionToListBox(droppedMission);
        }

        private void lbMission_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //lbMission.SelectedIndex = -1; // clear MissionListBox selection
        }

        private void MenuRemoveMission_Click(object sender, RoutedEventArgs e)
        {
            //if (lbMission.SelectedIndex == -1)
            //{
            //    return;
            //}

            removeMissionListBoxItem();
        }

        private void Validation_OnPreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed  // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !isTextAllowed(e.Text);
            if (e.Handled)
            {
                return;
            }
        }

        #endregion

        /// <summary>
        /// 更新周期
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        private List<KeyValuePair<string, string>> intervalItems = new List<KeyValuePair<string, string>>();

        /// <summary>
        /// 取得期間
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        private List<KeyValuePair<string, string>> getPeriodItems = new List<KeyValuePair<string, string>>();

        private void setKeyValuePair()
        {
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 Start
            //// 更新周期
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("5秒", new TimeSpan(0, 0, 5)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("10秒", new TimeSpan(0, 0, 10)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("30秒", new TimeSpan(0, 0, 30)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("1分", new TimeSpan(0, 1, 0)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("10分", new TimeSpan(0, 10, 0)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("1時間", new TimeSpan(1, 0, 0)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("4時間", new TimeSpan(4, 0, 0)));

            //// 取得期間
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("1時間前", new TimeSpan(1, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("3時間前", new TimeSpan(3, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("6時間前", new TimeSpan(6, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("12時間前", new TimeSpan(12, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("今日", new TimeSpan(24, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("前日", new TimeSpan(48, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("前々日", new TimeSpan(72, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("今週", new TimeSpan(168, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("先週", new TimeSpan(336, 0, 0)));

			// 更新周期
            this.intervalItems.Add(new KeyValuePair<string, string>("5秒", "5"));
            this.intervalItems.Add(new KeyValuePair<string, string>("10秒", "10"));
            this.intervalItems.Add(new KeyValuePair<string, string>("30秒", "30"));
            this.intervalItems.Add(new KeyValuePair<string, string>("1分", "60"));
            this.intervalItems.Add(new KeyValuePair<string, string>("10分", "600"));
            this.intervalItems.Add(new KeyValuePair<string, string>("1時間", "3600"));
            this.intervalItems.Add(new KeyValuePair<string, string>("4時間", "14400"));

            // 取得期間
            this.getPeriodItems.Add(new KeyValuePair<string, string>("1時間前", "1"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("3時間前", "3"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("6時間前", "6"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("12時間前", "12"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("今日", "24"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("前日", "48"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("前々日", "72"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("今週", "168"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("先週", "336"));
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 End
        }

        private void setComboBoxItem()
        {
            // 更新周期
            foreach (var pair in this.intervalItems)
            {
                this.comboBox_Interval.Items.Add(pair.Key);
            }
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 Start
            //this.comboBox_Interval.SelectedItem = "60";
            this.comboBox_Interval.SelectedItem = "30秒";
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 End

            // 取得期間
            foreach (var pair in this.getPeriodItems)
            {
                this.comboBox_GetPeriod.Items.Add(pair.Key);
            }
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 Start
            //this.comboBox_GetPeriod.SelectedItem = "1";
            this.comboBox_GetPeriod.SelectedItem = "1時間前";
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 End
        }

        private class MissionViewObject
        {
            public string MissionDisplayName { get; set; }
            public string MissionId { get; set; }
            public string Json { get; set; }
        }

        private class MissionCtmViewObject
        {
            public string CtmDisplayName { get; set; }
            public string CtmId { get; set; }
            public string ParentMissionId { get; set; }
        }

        private class MissionCtmElementViewObject
        {
            public string ElementDisplayName { get; set; }
            public string ElementId { get; set; }
            public string ParentCtmId { get; set; }
            public string ParentMissionId { get; set; }
        }

		// Wang New dac flow 20190308 Start
        //private static readonly ILog logger = LogManager.GetLogger(typeof(UserControl_DACTemplate));
        private static readonly ILog logger = LogManager.GetLogger(typeof(UserControl_MultiStatusMonitor));
		// Wang New dac flow 20190308 End

        // Processing On Server 検証＃6 dn 2018/10/05 start
        /// <summary>
        /// 「期間固定」のラジオのクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton_2_Click(object sender, RoutedEventArgs e)
        {
            if (this.AisTvRefDicMission.Items.Count != 0)
            {
                foreach (TreeViewItem treeViewItem in AisTvRefDicMission.Items)
                {
                    JToken missionToken = treeViewItem.DataContext as JToken;

                    // 選択されているMissionの赤枠を消す
                    if (treeViewItem.IsSelected)
                    {
                        Style style = new Style();
                        // 枠色の設定
                        style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(Colors.White)));
                        style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(0)));
                        // スタイルセット
                        treeViewItem.Style = style;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// 「周期設定」のラジオのクリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radioButton_1_Click(object sender, RoutedEventArgs e)
        {
            if (this.AisTvRefDicMission.Items.Count != 0)
            {
                foreach (TreeViewItem treeViewItem in AisTvRefDicMission.Items)
                {
                    JToken missionToken = treeViewItem.DataContext as JToken;

                    // 選択されているMissionは赤枠で囲む
                    if (treeViewItem.IsSelected)
                    {
                        Style style = new Style();
                        // 枠色の設定
                        style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(Colors.Red)));
                        style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(2)));
                        // スタイルセット
                        treeViewItem.Style = style;
                        break;
                    }
                }
            }
        }
        // Processing On Server 検証＃6 dn 2018/10/05 end

        //AISMM No.85 sunyi 20181211 start
        //内容修正、GripMission条件表示
        private void GetElementConditionDatas(List<MissionCtm> missionCtms)
        {
            //this.conditionLists.Clear();
            //this.conditionPatterns.Clear();
            foreach (var missionCtm in missionCtms)
            {
                if (!ctmIdList.Contains(missionCtm.CtmId))
                {
                    ctmIdList.Add(missionCtm.CtmId);
                    if (string.IsNullOrEmpty(missionCtm.ElementCondition) == false)
                    {
                        JToken condJson = JToken.Parse(missionCtm.ElementCondition);
                        JArray condElements = (JArray)condJson["elements"];
                        this.conditionPatterns.Add(missionCtm.CtmId, int.Parse(condJson["pattern"].ToString()));

                        List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
                        ElementConditionDatas = JsonConvert.DeserializeObject<List<ElementConditionData>>(condElements.ToString());
                        this.conditionLists.Add(missionCtm.CtmId, ElementConditionDatas);
                    }
                    else
                    {
                        this.conditionLists.Add(missionCtm.CtmId, new List<ElementConditionData>());
                    }
                }
            }
        }
        //ISSUE_NO.778 Sunyi 2018/07/23 Start
        //G-missionの条件設定の色付く
        private void GetElementConditionDatas_G(GripMissionCtm gripMissionCtms)
        {
            //this.conditionLists_Grip.Clear();
            //this.conditionPatterns_Grip.Clear();
            if (gripMissionCtms != null)
            {
                List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
                //E2NaviUI sunyi 20190305 start
                //MISSION条件対応
                if (gripMissionCtms.StartNodes == null)
                {
                    //start
                    if (gripMissionCtms.StartNode.CondSet != null)
                    {
                        if (!ctmIdList.Contains(gripMissionCtms.StartNode.CtmId))
                        {
                            ctmIdList.Add(gripMissionCtms.StartNode.CtmId);
                            this.conditionPatterns_Grip.Add(gripMissionCtms.StartNode.CtmId, gripMissionCtms.StartNode.CondSet.Pattern);
                            foreach (var Condition in gripMissionCtms.StartNode.CondSet.Conditions)
                            {
                                ElementConditionData ElementConditionData = new ElementConditionData
                                {
                                    Id = Condition.ElementId,
                                    Type = Condition.Datatype,
                                    Condition = Condition.Operator,
                                    Value = Condition.Value
                                };
                                ElementConditionDatas.Add(ElementConditionData);
                            }
                            this.conditionLists_Grip.Add(gripMissionCtms.StartNode.CtmId, ElementConditionDatas);
                        }
                    }
                }
                else
                {
                    //StartNodes
                    foreach (var condSet in gripMissionCtms.StartNodes)
                    {
                        if (!ctmIdList.Contains(condSet.Value.CtmId))
                        {
                            ctmIdList.Add(condSet.Value.CtmId);
                            if (condSet.Value.CondSet != null)
                            {
                                this.conditionPatterns_Grip.Add(condSet.Value.CtmId, condSet.Value.CondSet.Pattern);
                                foreach (var Condition in condSet.Value.CondSet.Conditions)
                                {
                                    ElementConditionData ElementConditionData = new ElementConditionData
                                    {
                                        Id = Condition.ElementId,
                                        Type = Condition.Datatype,
                                        Condition = Condition.Operator,
                                        Value = Condition.Value
                                    };
                                    ElementConditionDatas.Add(ElementConditionData);
                                }
                                this.conditionLists_Grip.Add(condSet.Value.CtmId, ElementConditionDatas);
                            }
                        }
                    }
                }
                //E2NaviUI sunyi 20190305 end

                //end
                foreach (var condSet in gripMissionCtms.EndNodes)
                {
                    if (!ctmIdList.Contains(condSet.CtmId))
                    {
                        ctmIdList.Add(condSet.CtmId);
                        if (condSet.CondSet_End != null)
                        {
                            this.conditionPatterns_Grip.Add(condSet.CtmId, condSet.CondSet_End.Pattern);
                            foreach (var Condition in condSet.CondSet_End.Conditions)
                            {
                                ElementConditionData ElementConditionData = new ElementConditionData
                                {
                                    Id = Condition.ElementId,
                                    Type = Condition.Datatype,
                                    Condition = Condition.Operator,
                                    Value = Condition.Value
                                };
                                ElementConditionDatas.Add(ElementConditionData);
                            }
                            this.conditionLists_Grip.Add(condSet.CtmId, ElementConditionDatas);
                        }
                    }
                }

                //trace
                HashSet<string> branchNodeIds = new HashSet<string>();
                if (gripMissionCtms.BranchNodes != null)
                {
                    foreach (Node node in gripMissionCtms.BranchNodes)
                    {
                        branchNodeIds.Add(node.CtmId);
                    }
                }

                // Wang Issue AISTEMP-122 20181228 Start
                //if (gripMissionCtms.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace && gripMissionCtms.TraceNodes != null)
                if ((gripMissionCtms.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace || gripMissionCtms.SearchType == FoaCore.Model.GripR2.Search.SearchType.Period) && gripMissionCtms.TraceNodes != null)
                // Wang Issue AISTEMP-122 20181228 End
                {
                    foreach (var condSet in gripMissionCtms.TraceNodes)
                    {
                        if (!ctmIdList.Contains(condSet.CtmId))
                        {
                            ctmIdList.Add(condSet.CtmId);
                            if (condSet.CondSet_Trace != null)
                            {
                                this.conditionPatterns_Grip.Add(condSet.CtmId, condSet.CondSet_Trace.Pattern);
                                foreach (var Condition in condSet.CondSet_Trace.Conditions)
                                {
                                    ElementConditionData ElementConditionData = new ElementConditionData
                                    {
                                        Id = Condition.ElementId,
                                        Type = Condition.Datatype,
                                        Condition = Condition.Operator,
                                        Value = Condition.Value
                                    };
                                    ElementConditionDatas.Add(ElementConditionData);
                                }
                                this.conditionLists_Grip.Add(condSet.CtmId, ElementConditionDatas);
                            }
                        }
                    }
                }
            }
        }

        //ISSUE_NO.778 Sunyi 2018/07/23 End
        //AISMM No.85 sunyi 20181211 end
    }
}
