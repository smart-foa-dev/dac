﻿using DAC.AExcel;
using DAC.CtmData;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using FoaCore.Common.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;
using log4net;
using DAC.View.Helpers;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_BulkyEdit.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_BulkyEdit : BaseControl
    {
        private bool isFirstFile = true;
        private string excelFilePath = string.Empty;

        private Dictionary<string, string> conditionDictionary = new Dictionary<string, string>();

        private Dictionary<string, string> paramDictionary = new Dictionary<string, string>();

        public UserControl_BulkyEdit(bool isFirstFile = true, string filePath = "")
        {
            InitializeComponent();

            this.Mode = ControlMode.Bulky;

            this.TEMPLATE = "Bulkyテンプレート.xlsm";

            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }
            this.dataGrid_Bulky.Init(this);
            this.dataGrid_Bulky.GetButton = this.button_Get;
            this.dataGrid_Bulky.OpenImage = this.image_OpenExcel;
            this.dataGrid_Bulky.CancelImage = this.image_CancelToGet;
            this.dataGrid_Bulky.GageImage = this.image_Gage;
            this.dataGrid_Bulky.Start = this.dateTimePicker_Start;
            this.dataGrid_Bulky.End = this.dateTimePicker_End;
        }

        #region イベントハンドラ

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();

            this.button_Get.IsEnabled = false;
            this.image_OpenExcel.Deactivate();

            //言語が英語の場合の表示調整
            string cultureName = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;
            int fontsize = 12;
            if (cultureName == "en-US")
            {
                //RadioButton_Horizontal
                this.radioButton_Horizontal.FontSize = fontsize;

                //RadioButton_Vertical
                this.radioButton_Vertical.FontSize = fontsize;
                Canvas.SetLeft(this.radioButton_Vertical, -55);
                Canvas.SetTop(this.radioButton_Vertical, 106);

                //Button_SelectAllColumn
                Canvas.SetLeft(this.button_SelectAllColumn, 223); 

                //Button_SelectAllRow
                this.button_SelectAllRow.Visibility = Visibility.Hidden;
                this.button_SelectAllRow_en.Visibility = Visibility.Visible;
                Canvas.SetTop(this.button_SelectAllRow_en, 200);
            }

            BitmapImage biCancel = new BitmapImage();
            biCancel.BeginInit();
            biCancel.UriSource = new Uri(@"/DAC;component/Resources/Image/cancel-to-get.png", UriKind.RelativeOrAbsolute);
            biCancel.EndInit();
            this.image_CancelToGet.Source = biCancel;
            this.image_CancelToGet.IsEnabled = false;

            BitmapImage biGage = new BitmapImage();
            biGage.BeginInit();
            biGage.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar-empty.png", UriKind.RelativeOrAbsolute);
            biGage.EndInit();
            this.image_Gage.Source = biGage;

            this.editControl.BulkyEdit = this;

            // No.549 Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;

            if (!this.isFirstFile)
            {
                readExcelFile(this.excelFilePath);
                this.image_OpenExcel.Activate();
            }
        }

        private void button_Get_Click(object sender, RoutedEventArgs e)
        {
            button_Get.IsEnabled = false;
            writeToExcel();
        }

        private void image_OpenExcel_Tap(object sender, RoutedEventArgs e)
        {
            image_OpenExcel.IsEnabled = false;
            openExcelFile();
            image_OpenExcel.IsEnabled = true;
        }

        private void image_CancelToGet_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.Bw != null)
            {
                this.Bw.CancelAsync();                
            }

            DownloadCts.Cancel();
            DownloadCts = new CancellationTokenSource();

            this.button_Get.IsEnabled = true;
            this.image_CancelToGet.IsEnabled = false;

            // 「中止されました」のダイアログ表示中にロード中アニメが動くのを防ぐ
            AisUtil.LoadProgressBarImage(this.image_Gage, false);
            button_Get.IsEnabled = true;
        }

        private void radioButton_Click(object sender, RoutedEventArgs e)
        {
            this.dataGrid_Bulky.GotNewResult = true;
        }

        private void button_SelectAllRow_Click(object sender, RoutedEventArgs e)
        {
            if (this.dataGrid_Bulky.IsEmpty)
            {
                return;
            }
            this.dataGrid_Bulky.SelectAllRow();
            this.dataGrid_Bulky.UpdateCellColorAndGetButton();
        }

        private void button_SelectAllColumn_Click(object sender, RoutedEventArgs e)
        {
            if (this.dataGrid_Bulky.IsEmpty)
            {
                return;
            }
            this.dataGrid_Bulky.SelectAllColumn();
            this.dataGrid_Bulky.UpdateCellColorAndGetButton();
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        #endregion

        #region 起動時のExcel読込み

        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string conditionSheetName = "Bulkyテンプレート";
                SpreadsheetGear.IWorksheet worksheetCondition = AisUtil.GetSheetFromSheetName_SSG(book, conditionSheetName);
                var conditionDic = readCondition(worksheetCondition);

                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var paramMap = readParam(worksheetParam);

                int firstRowIndex = 5;
                int firstColumnIndex = 0;
                int rowLength = AisUtil.GetRowLength_SSG(worksheetCondition, firstRowIndex, firstColumnIndex);
                int columnLength = AisUtil.GetColumnLength_SSG(worksheetCondition, firstRowIndex, firstColumnIndex);
                DataTable dtElement = AisUtil.CreateDataTableFromExcel_SSG(worksheetCondition, firstRowIndex, firstColumnIndex, rowLength, columnLength);
                var elementColumnDic = AisUtil.CountElementColumn(dtElement);

                if (paramMap.ContainsKey("収集タイプ") &&
                    !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                {
                    switch (paramMap["収集タイプ"])
                    {
                        case "CTM":
                            this.DataSource = DataSourceType.CTM_DIRECT;
                            break;
                        case "ミッション":
                            this.DataSource = DataSourceType.MISSION;
                            break;
                        case "GRIP":
                            this.DataSource = DataSourceType.GRIP;
                            break;
                        default:
                            break;
                    }
                }

                // set SelectedMission
                if (paramMap.ContainsKey("ミッションID") &&
                    !string.IsNullOrEmpty(paramMap["ミッションID"]))
                {
                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        string missionId = conditionDic["ミッションID"];
                        Mission mission = AisUtil.GetMissionFromId(missionId);
                        this.SelectedMission = mission;
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        string missionId = conditionDic["ミッションID"];
                        GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                        this.SelectedGripMission = mission;
                    }
                }

                this.conditionDictionary = conditionDic;
                this.paramDictionary = paramMap;

                DateTime dtStart = new DateTime();
                if (DateTime.TryParse(conditionDic["収集開始日時"], out dtStart))
                {
                    this.dateTimePicker_Start.Value = dtStart;
                }

                DateTime dtEnd = new DateTime();
                if (DateTime.TryParse(conditionDic["収集終了日時"], out dtEnd))
                {
                    this.dateTimePicker_End.Value = dtEnd;
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private Dictionary<string, string> readCondition(SpreadsheetGear.IWorksheet worksheetCondition)
        {
            var dic = new Dictionary<string, string>();

            // 収集条件を出力
            SpreadsheetGear.IRange cellsCondition = worksheetCondition.Cells;
            for (int i = 0; i < 10; i++)   // とりあえず10×10のセルを検索
            {
                for (int j = 0; j < 10; j++)
                {
                    // null check
                    if (cellsCondition[i, j].Value == null)
                    {
                        continue;
                    }

                    // ミッション
                    if (cellsCondition[i, j].Text == "ミッション")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // ミッションID
                    if (cellsCondition[i, j].Text == "ミッションID")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // 収集開始日時
                    if (cellsCondition[i, j].Text == "収集開始日時")
                    {
                        string date = cellsCondition[i, j + 1].Text;
                        string time = cellsCondition[i, j + 2].Text;
                        string datetime = date + " " + time;
                        dic.Add(cellsCondition[i, j].Text, datetime);
                        continue;
                    }

                    // 収集終了日時
                    if (cellsCondition[i, j].Text == "収集終了日時")
                    {
                        string date = cellsCondition[i, j + 1].Text;
                        string time = cellsCondition[i, j + 2].Text;
                        string datetime = date + " " + time;
                        dic.Add(cellsCondition[i, j].Text, datetime);
                        continue;
                    }
                }
            }

            return dic;
        }

        private Dictionary<string, string> readParam(SpreadsheetGear.IWorksheet worksheetParam)
        {
            var map = new Dictionary<string, string>();

            SpreadsheetGear.IRange paramCells = worksheetParam.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // サーバIPアドレス
                if (paramName == "サーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // サーバポート番号
                if (paramName == "サーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバIPアドレス
                if (paramName == "GRIPサーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバポート番号
                if (paramName == "GRIPサーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // CTM名
                if (paramName == "CTM名")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // CTMID
                if (paramName == "CTMID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // エレメントID
                if (paramName == "エレメントID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // Bulky選択行
                if (paramName == "Bulky選択行")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // Bulky選択列
                if (paramName == "Bulky選択列")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 連結方向
                if (paramName == "連結方向")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // ソートキー
                if (paramName == "ソートキー")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                    }
                    continue;
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }
            }

            return map;
        }

        #endregion

        #region 内容修正

        public void SetLoadedData(bool isMissionSelected, DataSourceType sourceType)
        {
            if (!this.paramDictionary.ContainsKey("CTMID"))
            {
                // 初回
                return;
            }
            string ctmId = this.paramDictionary["CTMID"];

            string selectedMissionId = null;
            if (this.conditionDictionary.ContainsKey("ミッションID"))
            {
                selectedMissionId = this.conditionDictionary["ミッションID"];
            }

            if (string.IsNullOrEmpty(selectedMissionId))
            {
                // 前回CTMを直接選択
                if (!isMissionSelected)
                {
                    Debug.Assert(false, string.Format("Invalid state: {0}, {1}, {2}", isMissionSelected, selectedMissionId, Enum.GetName(typeof(DataSourceType), sourceType)));
                    return;
                }

                this.mainWindow.TabCtrlDS.SelectedIndex = 0;

                var item = this.mainWindow.treeView_Catalog.GetItem(ctmId);
                if (item == null)
                {
                    return;
                }

                CheckBox cb = this.mainWindow.treeView_Catalog.GetCheckBox(item);
                cb.IsChecked = true;
                item.IsSelected = true;
            }
            else
            {
                // 前回ミッションから選択
                if (isMissionSelected)
                {
                    Debug.Assert(false, string.Format("Invalid state: {0}, {1}, {2}", isMissionSelected, selectedMissionId, Enum.GetName(typeof(DataSourceType), sourceType)));
                    return;
                }

                if (sourceType == DataSourceType.MISSION)
                {
                    var item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(selectedMissionId);
                    if (item == null)
                    {
                        if (this.SelectedMission != null)
                        {
                            this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                        }
                        return;
                    }
                    var parentNode = item.Parent as TreeViewItem;
                    parentNode.IsExpanded = true;
                    item.IsSelected = true;
                    this.mainWindow.treeView_Mission_CTM.Focus();
                    item.Focus();
                }
                else if (sourceType == DataSourceType.GRIP)
                {
                    // Mission Tree
                    var item = this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(selectedMissionId);
                    if (item == null)
                    {
                        if (this.SelectedGripMission != null)
                        {
                            this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(this.SelectedGripMission.Id);
                        }
                        return;
                    }
                    var parentNode = item.Parent as TreeViewItem;
                    parentNode.IsExpanded = true;
                    item.IsSelected = true;
                    this.mainWindow.treeView_Mission_Grip.Focus();
                    item.Focus();
                }
                else
                {
                    Debug.Assert(false, string.Format("Invalid state: {0}, {1}, {2}", isMissionSelected, selectedMissionId, Enum.GetName(typeof(DataSourceType), sourceType)));
                    return;
                }
            }

            if (!this.paramDictionary.ContainsKey("CTM名"))
            {
                return;
            }
            this.dataGrid_Bulky.CtmName = this.paramDictionary["CTM名"];

            if (!this.paramDictionary.ContainsKey("エレメントID"))
            {
                return;
            }
            string elementId = this.paramDictionary["エレメントID"];

            if (this.paramDictionary.ContainsKey("連結方向"))
            {
                string direction = this.paramDictionary["連結方向"];
                setDirection(direction);
            }

            string rows = string.Empty;
            if (!this.paramDictionary.ContainsKey("Bulky選択行"))
            {
                return;
            }
            rows = this.paramDictionary["Bulky選択行"];
            string[] rowsArray = rows.Split(',');

            string columns = string.Empty;
            if (!this.paramDictionary.ContainsKey("Bulky選択列"))
            {
                return;
            }
            columns = this.paramDictionary["Bulky選択列"];
            string[] columnsArray = columns.Split(',');

            setLoadedDataAsync(ctmId, elementId, rowsArray, columnsArray);
        }

        private async void setLoadedDataAsync(string ctmId, string elementId, string[] rowsArray, string[] columnsArray)
        {
            DateTime start1;
            DateTime end1;
            bool gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out start1, out end1);
            if (!gotTime)
            {
                return;
            }

            while (true)
            {
                if (this.CurrentDataSourceType != DataSourceType.NONE)
                {
                    logger.Debug("Waiting for item check event hander.");
                    break;
                }
                await Task.Delay(10);
            }

            try
            {
                await this.dataGrid_Bulky.UpdateBulkyTableX(ctmId, elementId, start1, end1);

                foreach (string row in rowsArray)
                {
                    this.dataGrid_Bulky.SelectedRowNumber.Add(int.Parse(row));
                }

                foreach (string column in columnsArray)
                {
                    this.dataGrid_Bulky.SelectedColumnNumber.Add(int.Parse(column));
                }
                this.dataGrid_Bulky.UpdateCellColorAndGetButton();

                this.dataGrid_Bulky.GotNewResult = false;
            }
            catch (Exception)
            {
                throw new Exception("該当するCTMまたはBulkyエレメントが見つかりませんでした。");
            }
        }

        #endregion

        #region Bulky Gridの設定

        private List<string[]> readCsvData(string filePath)
        {
            List<string[]> data = new List<string[]>();
            using (StreamReader sr = new StreamReader(filePath, AisUtil.Utf8))
            {
                while (-1 < sr.Peek())
                {
                    string src = sr.ReadLine();
                    string dst = AisUtil.ConvertFromUtf8ToSjis(src);
                    string[] row = dst.Split(',');
                    data.Add(row);
                }
            }

            return data;
        }

        private void setDirection(string direction)
        {
            if (direction == "横")
            {
                this.radioButton_Horizontal.IsChecked = true;

            }
            else
            {
                this.radioButton_Vertical.IsChecked = true;
            }
        }

        #endregion

        #region Excelファイルの作成

        private void writeToExcel()
        {
            this.image_CancelToGet.IsEnabled = true;
            AisUtil.LoadProgressBarImage(this.image_Gage, true);

            List<string[]> data = readCsvData(this.dataGrid_Bulky.TempBulkyFilePath);

            string filePathDest = string.Empty;

            bool isHorizontal = (bool)this.radioButton_Horizontal.IsChecked;
            int selectedSortKeyIndex = this.comboBox_SortKey.SelectedIndex;

            this.Bw = new BackgroundWorker();
            this.Bw.WorkerSupportsCancellation = true;

            Bw.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                filePathDest = writeBulkyToExcel(data, this.dataGrid_Bulky.StartR, this.dataGrid_Bulky.EndR, isHorizontal, Bw);
                if (this.Bw.CancellationPending)
                {
                    args.Cancel = true;
                }
            };
            Bw.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                AisUtil.LoadProgressBarImage(this.image_Gage, false);
                this.image_CancelToGet.IsEnabled = false;
                this.image_OpenExcel.Activate();

                this.Bw = null;
                if (args.Cancelled)
                {
                    if (File.Exists(filePathDest))
                    {
                        File.Delete(filePathDest);
                    }
                    return;
                }

                if (args.Error != null)  // if an exception occurred during DoWork,
                {
                    // Do your error handling here
                    AisMessageBox.DisplayErrorMessageBox(args.Error.ToString());
                }

                if (string.IsNullOrEmpty(filePathDest))
                {
                    FoaMessageBox.ShowError("AIS_E_001");
                    return;
                }

                excelFilePath = filePathDest;
                this.image_OpenExcel.Activate();
                this.dataGrid_Bulky.GotNewResult = false;
                button_Get.IsEnabled = true;
            };
            Bw.RunWorkerAsync();
        }

        private void openExcelFile()
        {
            //ExcelUtil.StartExcelProcess(excelFilePath, true);
            ExcelUtil.OpenExcelFile(excelFilePath);

            this.dataGrid_Bulky.GotNewResult = false;
        }

        /// <summary>
        /// エクセル出力準備
        /// </summary>
        /// <returns></returns>
        private string prepareToWriteBulky()
        {
            if (this.dataGrid_Bulky.SelectedColumnNumber.Count == 0)
            {
                this.dataGrid_Bulky.SelectAllColumn();
            }
            if (this.dataGrid_Bulky.SelectedRowNumber.Count == 0)
            {
                this.dataGrid_Bulky.SelectAllRow();
            }

            var filePathSrc = this.isFirstFile ? Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE) : this.excelFilePath;
            string filePathDest = getExcelFilepath(this.DataSource);
            File.Copy(filePathSrc, filePathDest);

            return filePathDest;
        }

        private string writeBulkyToExcel(List<string[]> data, DateTime start, DateTime end,
            bool isHorizontal, BackgroundWorker bWorker)
        {
            CtmObject ctm = CtmUtil.GetCtmObj(editControl.Ctms, this.dataGrid_Bulky.CtmId);

            string filePathDest = prepareToWriteBulky();

            var cparams = new Dictionary<ExcelConfigParam, object>();
            cparams.Add(ExcelConfigParam.TEMPLATE_NAME, "Bulky");
            cparams.Add(ExcelConfigParam.START, start);
            cparams.Add(ExcelConfigParam.END, end);

            cparams.Add(ExcelConfigParam.CTM_ID, this.dataGrid_Bulky.CtmId);
            cparams.Add(ExcelConfigParam.ELEMENT_ID, this.dataGrid_Bulky.ElementId);
            cparams.Add(ExcelConfigParam.CTM_NAME, this.dataGrid_Bulky.CtmName);
            cparams.Add(ExcelConfigParam.B_CONNECTION, isHorizontal ? "横" : "縦");

            if (this.DataSource == DataSourceType.MISSION)
            {
                cparams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                cparams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetCatalogLang(this.SelectedMission));
            }
            else if (this.DataSource == DataSourceType.GRIP)
            {
                cparams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                cparams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetGripCatalogLang(this.SelectedGripMission));
            }

            if (this.isFirstFile)
            {
                var writer = new SSGExcelWriterBulky(ControlMode.Bulky, this.DataSource, ctm, bWorker, this.SelectedMission, this.dataGrid_Bulky.SelectedRowNumber, this.dataGrid_Bulky.SelectedColumnNumber);
                writer.WriteCtmData(filePathDest, this.dataGrid_Bulky.TempBulkyFilePath, cparams);
            }
            else
            {
                var originalFile = System.IO.Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE);
                cparams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(this.excelFilePath));

                var writer = new InteropExcelWriterBulky(ControlMode.Bulky, this.DataSource, ctm, bWorker, this.SelectedMission, this.dataGrid_Bulky.SelectedRowNumber, this.dataGrid_Bulky.SelectedColumnNumber);
                writer.WriteCtmData(filePathDest, this.dataGrid_Bulky.TempBulkyFilePath, cparams);
            }

            return filePathDest;
        }

        #endregion


        // No.493 Input control of period setting item. ----------------Start
        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }
        // No.493 Input control of period setting item. ----------------End

        private static readonly ILog logger = LogManager.GetLogger(typeof(UserControl_BulkyEdit));
    }
}
