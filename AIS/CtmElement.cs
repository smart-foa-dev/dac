﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoaCore.Model;

namespace DAC.Model
{
    public class CtmElement : CtmNode, CtmChildNode
    {
        public int datatype;
        public int bulkysubtype;
        public int size;
        public ForceLoginLangObject unit = new ForceLoginLangObject();

        public int parentNodeIdx4Ddmap;

        public bool IsElement()
        {
            return true;
        }
    }
}
