﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Win32;

namespace DAC.Model
{
    /// <summary>
    /// ミッション投入で取得したJsonデータの処理クラス
    /// メンバーはgetJsonDataメソッドでのみ変更可能
    /// </summary>
    [DataContract]
    public class ProgramMission
    {
        /// <summary>
        /// CTMのID
        /// </summary>
        [DataMember]
        public string id { get; set; }

        /// <summary>
        /// CTM名称
        /// </summary>
        [DataMember]
        public string name { get; set; }

        /// <summary>
        /// CTMのコレクション数
        /// </summary>
        [DataMember]
        public int num { get; set; }

        /// <summary>
        /// CTMの実績データ
        /// </summary>
        [DataMember]
        public List<Ctms> ctms { get; private set; }

        public void SetCtms(List<Ctms> ctms) { this.ctms = ctms; }

        /*
        /// <summary>
        /// プログラムミッションを投入しJsonを変換したCTMデータを受け取る
        /// </summary>
        /// <param name="mid">ミッションのID(msec)</param>
        /// <param name="start">収集開始日時(msec)</param>
        /// <param name="end">収集終了日時(msec)</param>
        /// <returns></returns>
        public static ClsJsonData[] get(string mid, string start, string end)
        {
            //WebClient wc = new WebClient(); //20150824
            WebClient wc = AisUtil.getProxyWebClient();
            string path = string.Empty; //20150827
            try
            {
                //ミッション投入のパラメータを設定する
                string uri = "";
                string hostName = Properties.Settings.Default.PMHostName;
                string portNo = Properties.Settings.Default.PMPortNo;
                string limit = "0";
                string lang = "ja";
                //string jsonStr = "";    //jsonを受け取るオブジェクト

                
                uri = String.Format("http://{0}:{1}/cms/rest/mib/mission/pm?id={2}&start={3}&end={4}&limit={5}&lang={6}",
                           hostName, portNo, mid, start, end, limit, lang);

                // 20150827 ***************************************************************************************
                ClsJsonData[] ctmArray = null;
                string clrVersionRuntime = System.Runtime.InteropServices.RuntimeEnvironment.GetSystemVersion();
                double nowVer = 0;
                using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\"))
                {
                    int releaseKey = Convert.ToInt32(ndpKey.GetValue("Release"));
                    nowVer = CheckFor45DotVersion(releaseKey);
                }

                if (nowVer < 4.5)
                {
                    // ファイルに落とす
                    DateTime dt = DateTime.Now;
                    if (Directory.Exists(Application.StartupPath + @"\tempfiles") == false)
                    {
                        Directory.CreateDirectory(Application.StartupPath + @"\tempfiles");
                    }
                    path = Application.StartupPath + @"\tempfiles\" + dt.ToString("yyyyMMddhhmmssfff") + ".json";
                    wc.DownloadFile(uri, path);

                    StreamReader sr;
                    using (sr = new StreamReader(path, Encoding.UTF8))
                    {
                        JsonSerializer Serialize = new JsonSerializer();
                        ctmArray = (ClsJsonData[])Serialize.Deserialize(sr, typeof(ClsJsonData[]));
                    }
                }
                else if (nowVer >= 4.5)
                {
                    // メモリに取得
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                    using (HttpWebResponse webres = (HttpWebResponse)req.GetResponse())
                    {
                        using (Stream st = webres.GetResponseStream())
                        {
                       
                            System.Runtime.Serialization.Json.DataContractJsonSerializerSettings dataSetting = new DataContractJsonSerializerSettings();
                            dataSetting.UseSimpleDictionaryFormat = true;

                            var serializer = new DataContractJsonSerializer(typeof(List<ClsJsonData>), dataSetting);
                            List<ClsJsonData> getJson = (List<ClsJsonData>)serializer.ReadObject(st);
                            ctmArray = getJson.ToArray();
                        }
                    }
                }
                // 20150827 ***************************************************************************************


                return ctmArray;
            }
            finally
            {
                // 20150827
                wc.Dispose();
                if (File.Exists(path) == true)
                {
                    File.Delete(path);
                }
            }

        }

        // 20150827
        // Checking the version using >= will enable forward compatibility, 
        // however you should always compile your code on newer versions of
        // the framework to ensure your app works the same.
        private static double CheckFor45DotVersion(int releaseKey)
        {
            if (releaseKey >= 393273)
            {
                return 4.6; //"4.6 RC or later";
            }
            if ((releaseKey >= 379893))
            {
                return 4.5;  //"4.5.2 or later";
            }
            if ((releaseKey >= 378675))
            {
                return 4.5;  //"4.5.1 or later";
            }
            if ((releaseKey >= 378389))
            {
                return 4.5; //"4.5 or later";
            }
            // This line should never execute. A non-null release key should mean
            // that 4.5 or later is installed.
            return 4.0;  //"No 4.5 or later version detected";
        }

        public static ClsJsonData[] getBulky(string mid, string start, string end)
        {
            //WebClient wc = new WebClient(); //20150824
            WebClient wc = AisUtil.getProxyWebClient();
            string path = string.Empty; //20150827
            try
            {
                //ミッション投入のパラメータを設定する
                string uri = "";
                string hostName = Properties.Settings.Default.PMHostName;
                string portNo = Properties.Settings.Default.PMPortNo;
                string limit = "0";
                string lang = "ja";                

                uri = String.Format("http://{0}:{1}/cms/rest/mib/mission/pm?id={2}&start={3}&end={4}&limit={5}&lang={6}",
                           hostName, portNo, mid, start, end, limit, lang);

                //WebClient wc = new WebClient();
                //wc.Encoding = Encoding.UTF8;
                //jsonStr = wc.DownloadString(uri);
                //ClsJsonData[] ctmArray = JsonConvert.DeserializeObject<ClsJsonData[]>(jsonStr);

                // 20150827 ***************************************************************************************
                ClsJsonData[] ctmArray = null;
                string clrVersionRuntime = System.Runtime.InteropServices.RuntimeEnvironment.GetSystemVersion();
                double nowVer = 0;
                using (RegistryKey ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey("SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full\\"))
                {
                    int releaseKey = Convert.ToInt32(ndpKey.GetValue("Release"));
                    nowVer = CheckFor45DotVersion(releaseKey);
                }

                if (nowVer < 4.5)
                {
                    // ファイルに落とす
                    DateTime dt = DateTime.Now;
                    if (Directory.Exists(Application.StartupPath + @"\tempfiles") == false)
                    {
                        Directory.CreateDirectory(Application.StartupPath + @"\tempfiles");
                    }
                    path = Application.StartupPath + @"\tempfiles\" + dt.ToString("yyyyMMddhhmmssfff") + ".json";
                    wc.DownloadFile(uri, path);

                    StreamReader sr;
                    using (sr = new StreamReader(path, Encoding.UTF8))
                    {
                        string text = sr.ReadToEnd();
                        ctmArray = JsonConvert.DeserializeObject<ClsJsonData[]>(text);
                    }
                }
                else if (nowVer >= 4.5)
                {
                    // メモリに取得
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                    using (HttpWebResponse webres = (HttpWebResponse)req.GetResponse())
                    {
                        using (Stream st = webres.GetResponseStream())
                        {
                            System.Runtime.Serialization.Json.DataContractJsonSerializerSettings dataSetting = new DataContractJsonSerializerSettings();
                            dataSetting.UseSimpleDictionaryFormat = true;

                            var serializer = new DataContractJsonSerializer(typeof(List<ClsJsonData>), dataSetting);
                            List<ClsJsonData> getJson = (List<ClsJsonData>)serializer.ReadObject(st);
                            ctmArray = getJson.ToArray();
                        }
                    }
                }
                // 20150827 ***************************************************************************************


                //文字コードを整理する
                Encoding sjisEnc = Encoding.GetEncoding("Shift-JIS");
                Encoding utfEnc = Encoding.UTF8;
                foreach (ClsJsonData ctmDatas in ctmArray)
                {
                    foreach (Ctms ctms in ctmDatas.ctms)
                    {
                        //foreach (Ctms.ELcls element in ctms.EL)
                        foreach (Ctms.ELcls element in ctms.EL.Values)
                        {
                            if (element != null)
                            {
                                if (element.T == "100" && element.V != null)
                                {
                                    String str = sjisEnc.GetString(Encoding.Convert(utfEnc, sjisEnc, utfEnc.GetBytes(element.V)));
                                    element.V = str;
                                }
                            }
                        }
                    }
                }
                //Encoding sjisEnc = Encoding.GetEncoding("Shift-JIS");
                //Encoding utfEnc = Encoding.UTF8;
                //byte[] convertBytes;
                //foreach (ClsJsonData ctmDatas in ctmArray)
                //{
                //    convertBytes = sjisEnc.GetBytes(ctmDatas.name);
                //    ctmDatas.name = utfEnc.GetString(convertBytes);
                //    foreach (Ctms ctms in ctmDatas.ctms)
                //    {
                //        //foreach (Ctms.ELcls element in ctms.EL)
                //        foreach (Ctms.ELcls element in ctms.EL.Values)
                //        {
                //            if (element.T != "100" && element.V != null)
                //            {
                //                convertBytes = sjisEnc.GetBytes(element.V);
                //                element.V = utfEnc.GetString(convertBytes);
                //            }
                //        }
                //    }
                //}

                return ctmArray;
            }
            finally
            {
                // 20150827
                wc.Dispose();
                if (File.Exists(path) == true)
                {
                    File.Delete(path);
                }
            }
        }
        */

        // Type created for JSON at <<root>> --> ctms
        /// <summary>
        /// ミッションで取得したCTM
        /// </summary>
        [DataContract]
        public class Ctms
        {
            /// <summary>
            /// エレメントのリスト
            /// </summary>
            [DataMember]
            //public List<ELcls> EL { get; private set; }
            public Dictionary<string, ELcls> EL { get; private set; }
            /// <summary>
            /// CTMが生成された時刻
            /// </summary>
            [DataMember]
            public long RT { get; private set; }

            public void SetEL(Dictionary<string, ELcls> el) { this.EL = el; }
            public void SetRT(long rt) { this.RT = rt; }

            // Type created for JSON at <<root>> --> EL
            /// <summary>
            /// エレメントの内容
            /// </summary>
            [DataContract]
            public class ELcls
            {
                ///// <summary>
                ///// エレメントID
                ///// </summary>
                //[DataMember]
                //public string ID { get; private set; }

                /// <summary>
                /// エレメントの型
                /// </summary>
                [DataMember]
                public string T { get; private set; }

                /// <summary>
                /// エレメントの値
                /// </summary>
                [DataMember]
                public string V { get; set; }

                /// <summary>
                /// エレメントのバルキーリンク
                /// </summary>
                [DataMember]
                public string FN { get; private set; }

                public void SetT(string t) { this.T = t; }
                public void SetFN(string fn) { this.FN = fn; }
            }
        }
    }
}
