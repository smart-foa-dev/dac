﻿using DAC.Model;
using DAC.View;
using FoaCore.Common;
using FoaCore.Common.Control;
using FoaCore.Common.Converter;
using FoaCore.Common.Entity;
using FoaCore.Common.Model;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using Grip.Net;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Grip.Controller.GripMission
{
    /// <summary>
    /// GripMissionツリーコントローラ
    /// </summary>
    public class GripMissionTreeController
    {
        /// <summary>
        /// JsonTreeView
        /// </summary>
        public JsonTreeView tree;

        /// <summary>
        /// Newly added agent item ID
        /// </summary>
        private string newAgentItemId;

        /// <summary>
        /// Temporary new mission ID
        /// </summary>
        public static string newTempMissionId;

        /// <summary>
        /// MibMain Window Object
        /// </summary>
        //private Mib.View.MibMain MibMainWindow;

        /// <summary>
        /// Parent Window & CtmMissionGrid Object
        /// </summary>
        private Window parentWindow;
        //private Mib.View.Component.CtmMissionGrid CtmMissionGridObj;

        /// <summary>
        /// ユーザ毎の表示オブジェクトのコレクション
        /// </summary>
        private Dictionary<string, List<LangObject>> userNames;
        // やむなし。持ちたくないが...
        // サーバ側でユーザ名をひっつける処理をしたくない。
        // MibMainにもったほうが楽だが、コントローラにMibMainの参照は持ってはいけない。

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="viewTree"></param>
        public GripMissionTreeController(JsonTreeView viewTree)
        {
            tree = viewTree;

            // Jsonキーの設定
            tree.IdKeyName = "id";
            tree.LabelKeyName = "name";
            tree.CollectionKeyName = "children";

            //// アイコンの設定(前)
            tree.FrontIconFunction = FrontIconFunction;
            //// アイコンの設定(後)
            //tree.BackIconKeyName = "status";
            //if (isCtmMissionTree)
            //{
            //    tree.BackIconMap = new Dictionary<string, string>(){
            //        {MissionStatus.New.ToString(), "transparent.png"},
            //        {MissionStatus.Repeat.ToString(), "mission-repeat-others.png"},
            //        {MissionStatus.Stopped.ToString(), "mission-stop.png"},
            //        {MissionStatus.Completed.ToString(), "mission-repeat-completed.png"},
            //        {MissionStatus.Making.ToString(), "transparent.png"}
            //    };
            //    // Manually handle the treeViewItem Events
            //    tree.DoNotSetTreeViewItemEventHandlersAutomatically = true;
            //}
            //else
            //{
            //    tree.BackIconMap = new Dictionary<string, string>(){
            //        {MissionStatus.New.ToString(), "transparent.png"},
            //        {MissionStatus.Running.ToString(), "mission-start.png"},
            //        {MissionStatus.Stopped.ToString(), "mission-stop.png"},
            //        {MissionStatus.Completed.ToString(), "mission-complete.png"},
            //        {MissionStatus.Making.ToString(), "mission-start.png"}
            //    };
            //}
            // ラベル
            tree.LabelFunction = LabelFunction;
            // ラベルの色
            tree.ForegroundFunction = LabelForegroundFunction;

            //this.parentWindow = parentWindow;
            //this.CtmMissionGridObj = CtmMissionGridObj;
        }

        /// <summary>
        /// ユーザ名の表示オブジェクトをセットします。
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="names"></param>
        public void AddUserNames(string userId, List<LangObject> names)
        {
            if (userNames == null) userNames = new Dictionary<string, List<LangObject>>();
            userNames[userId] = names;
        }

        /// <summary>
        /// 前のアイコンファンクション
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private string FrontIconFunction(string arg)
        {
            JToken token = JToken.Parse(arg);
            string type = (string)token["t"];
            if (type == NodeType.Mission)
            {
                // ミッション
                // 基本的にはプログラムミッションのみの表示
                // GripミッションはMissionType=0で取得されるので、
                // mission.pngが使用される
                int missionType = (int)token["missionType"];
                if (missionType == MissionType.Mobile)
                {
                    return "mission.png";
                }
                else if (missionType == MissionType.Ontime)
                {
                    return "mission-ontime.png";
                }
                else
                {
                    return "mission-pm.png";
                }
            }
            else if (type == NodeType.Mms)
            {
                return "mms.png";
            }
            else if (type == "myAgent")
            {
                return null;
            }
            else
            {
                // エージェント
                if (type == NodeType.Agent)
                {
                    return "user.png";
                }
                else
                {
                    return "users.png";
                }
            }
        }

        /// <summary>
        /// ツリーのラベルファンクション
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private string LabelFunction(string arg)
        {
            JToken token = JToken.Parse(arg);
            string type = (string)token["t"];
            string label = (string)token["name"];
            string displayName = null;
            string catalogLang = LoginInfo.GetInstance().GetCatalogLang();
            JArray displayNameArray = token["displayName"] == null ? new JArray() : string.IsNullOrEmpty((string)token["displayName"]) ? new JArray() : JArray.Parse((string)token["displayName"]);
            for (int i = 0; i < displayNameArray.Count; i++)
            {
                JToken dnToken = displayNameArray[i];
                if (catalogLang == (string)dnToken["lang"])
                {
                    displayName = (string)dnToken["text"];
                    break;
                }
            }

            if (type != NodeType.Mission && type != NodeType.Mms)
            {
                // エージェントの場合、作成ユーザと共有ユーザの名前を表示
                if (userNames == null) return (displayName != null ? displayName : label);
                // 作成ユーザ
                string createdUserId = (string)token["uc"];

                //if (userNames.ContainsKey(createdUserId))
                //{
                //    label += " " + DisplayNameConverter.Convert2(userNames[createdUserId]);
                //}

                // 共有ユーザ
                var users = token["agentUserList"];
                if (users == null) return (displayName != null ? displayName : label);
                List<AgentUser> agentUserList = JsonConvert.DeserializeObject<List<AgentUser>>(users.ToString());
                if (agentUserList == null) return (displayName != null ? displayName : label);
                agentUserList.ForEach(au =>
                {
                    if (userNames.ContainsKey(au.UserId))
                    {
                        //label += " " + DisplayNameConverter.Convert2(userNames[au.UserId]);
                    }
                });
                return (displayName != null ? displayName : label);
            }
            else if (type == NodeType.Mission)
            {
                return (displayName != null ? displayName : label);
            }
            return label;
        }

        /// <summary>
        /// ツリーのラベルカラーファンクション
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Brush LabelForegroundFunction(string arg)
        {
            JToken token = JToken.Parse(arg);
            string type = (string)token["t"];
            if (type == NodeType.Mission)
            {
                //bool hasError = (bool)token["hasError"];
                //    if (hasError) return Brushes.Red;
            }
            return Brushes.Black;
        }

        public Action LoadCallback = null;
        /// <summary>
        /// ツリーのロード
        /// </summary>
        public void Load(bool onInitWithE)
        {
            CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
            UserInfo user = LoginInfo.GetInstance().GetLoginUserInfo();
            client.AddParam("userId", user.UserId);
            client.AddParam("userRole", user.Role.ToEmunString());
            client.CompleteHandler += resJson =>
            {
                if (user == null)
                {
                    logger.Debug("### user is null.");
                    user = LoginInfo.GetInstance().GetLoginUserInfo();
                }
                else if (user.UserName == null)
                {
                    logger.Debug("### user.UserName is null.");
                    user = LoginInfo.GetInstance().GetLoginUserInfo();
                }
                else if (user.UserId == null)
                {
                    logger.Debug("### user.UserId is null.");
                    user = LoginInfo.GetInstance().GetLoginUserInfo();
                }

                //MessageBox.Show(resJson);
                // 取得したデータをツリー表示
                //string userAgent = user.UserName + " Agent";
                string userAgent = DisplayNameConverter.Convert2(user.DisplayNameObj) + " Agent";
                string userAgentId = user.UserId;

                //Mミッション以外非表示
                List<JToken> tokenList = new List<JToken>();

                JArray mission = JArray.Parse(resJson);
                foreach (JToken missionToken in mission)
                {
                    if (missionToken[MmsTvJsonKey.Children] != null)
                    {
                        JArray missionjarray = (JArray)missionToken[MmsTvJsonKey.Children];

                        foreach (JToken m in missionjarray)
                        {
                            if (m["missionType"].ToString() != "1")
                            {
                               tokenList.Add(m);
                            }

                        }
                    }
                }

                foreach (JToken token in tokenList)
                {
                    token.Remove();
                }
                tokenList.Clear();

                resJson = mission.ToString();

                string jsonAddedUserAgent =
                    "{\"t\":\"mms\",\"name\":\"" + userAgent + "\",\"id\":\"" + userAgentId + "\",\"children\":" + (string.IsNullOrEmpty(resJson) ? "[]" : resJson) + "}";

                tree.JsonItemSource = jsonAddedUserAgent;

                // Expand the user agent item as default
                TreeViewItem userAgentItem = tree.GetItem(userAgentId);
                userAgentItem.IsExpanded = true;
                setToolTip();

                if (!string.IsNullOrEmpty(newAgentItemId))
                {
                    TreeViewItem agentItem = tree.GetItem(newAgentItemId);
                    if (agentItem != null)
                    {
                        agentItem.Focus();
                        agentItem.IsSelected = true;

                        // Re-set to null again
                        newAgentItemId = string.Empty;
                    }
                }

               if (LoadCallback != null)
                {
                    LoadCallback();
                }

               MainWindow mainWindow = Application.Current.MainWindow as MainWindow;

               // Wang Issue NO.687 2018/05/18 Start
               // 1.階層DAC追加(オプションテンプレート表示統一)
               // 2.DAC Excel メニュー整理
               //if (mainWindow.CurrentMode == ControlMode.Dac)
               if (mainWindow.CurrentMode == ControlMode.Dac || mainWindow.CurrentMode == ControlMode.MultiDac)
               // Wang Issue NO.687 2018/05/18 End
               {
                   UserControl_DACTemplate DACTemplateObj = (UserControl_DACTemplate)mainWindow.CurrentEditCtrl;
                   DACTemplateObj.AttachDragDropEventToTreeViewItems();
               }

               if (onInitWithE)
               {
                   if (mainWindow.CurrentMode == ControlMode.Spreadsheet)
                   {
                       mainWindow.CurrentEditCtrl.SetGripMission2();
                   }
                   // Wang Issue NO.687 2018/05/18 Start
                   // 1.階層DAC追加(オプションテンプレート表示統一)
                   // 2.DAC Excel メニュー整理
                   //else if (mainWindow.CurrentMode == ControlMode.Dac)
                   else if (mainWindow.CurrentMode == ControlMode.Dac || mainWindow.CurrentMode == ControlMode.MultiDac)
                   // Wang Issue NO.687 2018/05/18 End
                   {
                       // ここには来ないはず
                       mainWindow.CurrentEditCtrl.SetMission2();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.Online)
                   {
                       mainWindow.CtmDtailsCtrl.Online.SetGripMission();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.Bulky)
                   {
                       // ここには来ないはず → 2017-03-24 yakiyama 変更。
                       mainWindow.CtmDtailsCtrl.BulkyEdit.SetLoadedData(false, DataSourceType.GRIP);
                   }
                   else if (mainWindow.CurrentMode == ControlMode.ContinuousGraph) //連続グラフ(BULKY)
                   {
                       // ここには来ないはず → 2017-03-24 yakiyama 変更。
                       mainWindow.CtmDtailsCtrl.ContinuousGraph.SetLoadedData(false, DataSourceType.GRIP);
                   }
                   else if (mainWindow.CurrentMode == ControlMode.StreamGraph) //トルク波形
                   {
                       mainWindow.CtmDtailsCtrl.StreamGraph.SetLoadedData(false, DataSourceType.GRIP);
                   }
                   else if (mainWindow.CurrentMode == ControlMode.LeadTime)       //リードタイム
                   {
                       mainWindow.CtmDtailsCtrl.LeadTime.SetGripMission();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.StockTime)      //在庫状況
                   {
                       mainWindow.CtmDtailsCtrl.StockTime.SetGripMission();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.ChimneyChart)   //煙突チャート
                   {
                       mainWindow.CtmDtailsCtrl.ChimneyChart.setGripMission();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.Moment)         // レーダーチャート
                   {
                       // ここには来ないはず
                       mainWindow.CtmDtailsCtrl.Moment.SetMission();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.FIFO)           //先入・先出
                   {
                       // ここには来ないはず
                       mainWindow.CtmDtailsCtrl.FIFO.SetGripMission();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.Frequency)      //度数分布
                   {
                       mainWindow.CtmDtailsCtrl.Frequency.SetGripMission();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.Comment)      // コメント
                   {
                       mainWindow.CtmDtailsCtrl.Comment.SetGripMission();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.StatusMonitor)    // ステータスモニター
                   {
                       // ここには来ないはず
                       mainWindow.CtmDtailsCtrl.StatusMonitor.SetMission2();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.MultiStatusMonitor)    // 階層ステータスモニター
                   {
                       // ここには来ないはず
                       mainWindow.CtmDtailsCtrl.MultiStatusMonitor.SetMission2();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.ProjectStatusMonitor)    // プロジェクトステータスモニター
                   {
                       // ここには来ないはず
                       mainWindow.CtmDtailsCtrl.ProjectStatusMonitor.SetMission2();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.Torque)    // トルク
                   {
                       // ここには来ないはず
                       mainWindow.CtmDtailsCtrl.Torque.SetMission2();
                   }
                   else if (mainWindow.CurrentMode == ControlMode.PastStockTime)    // 過去在庫状況
                   {
                       // ここには来ないはず
                       mainWindow.CtmDtailsCtrl.PastStockTime.SetGripMission();
                   }
                   //AIS_WorkPlace
                   else if (mainWindow.CurrentMode == ControlMode.WorkPlace)    // トルク
                   {
                       // ここには来ないはず
                       mainWindow.CtmDtailsCtrl.WorkPlace.SetMission2();
                   }

                   mainWindow.CurrentEditCtrl.Initializing = false;
               }
            };
            client.Get(GripUrl.Agent.WithMission());
        }

        private void setToolTip()
        {
            JArray tvTopArray = JArray.Parse(tree.JsonItemSource);
            JToken token = tvTopArray[0];
            JToken topToken = token;
            JArray children = null;
            if (!string.IsNullOrEmpty(topToken[MmsTvJsonKey.Children].ToString()))
                children = (JArray)topToken[MmsTvJsonKey.Children];
            else
                return;

            if (children.Count > 0)
            {
                foreach (JToken childToken in children)
                {
                    string type = (string)childToken["t"];
                    if (type == NodeType.AgentShare) // Shared Mission Folder
                    {
                        string id = (string)childToken["id"];
                        var users = childToken["agentUserList"];
                        List<AgentUser> agentUserList = JsonConvert.DeserializeObject<List<AgentUser>>(users.ToString());
                        string agentUserLabel = "";
                        int count = agentUserList.Count;
                        int index = 1;
                        agentUserList.ForEach(au =>
                        {
                            if (userNames == null) return;
                            if (userNames.ContainsKey(au.UserId))
                            {
                                if (index != count) agentUserLabel += "\u2022 " + DisplayNameConverter.Convert2(userNames[au.UserId]) + "\n";
                                else agentUserLabel += "\u2022 " + DisplayNameConverter.Convert2(userNames[au.UserId]);
                                index++;
                            }
                        });
                        TreeViewItem tvItem = tree.GetItem(id);
                        ToolTip tooltip = new ToolTip { Content = agentUserLabel };
                        tooltip.FontSize = 14;
                        tooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;
                        tvItem.ToolTip = tooltip;
                    }
                }

            }

        }
        
        #region DELETE NODE
        /// <summary>
        /// ノードの削除
        /// </summary>
       
        private List<string> DeleteMissionIdList;
        private int IndexOfDeleteMissionIdList;
       
        #endregion

        /// <summary>
        /// ミッションが選択されているか判定します。
        /// </summary>
        /// <returns></returns>
        public bool isMissionSelected()
        {
            string type = (string)tree.GetSelectedValueByKey("t");
            if (type == NodeType.Mission)
            {
                return true;
            }
            return false;
        }

        public bool isAgentUserSelected()
        {
            string type = (string)tree.GetSelectedValueByKey("t");
            if (type == NodeType.Mms)
            {
                return true;
            }
            return false;
        }

        public bool isAgentSelected()
        {
            string type = (string)tree.GetSelectedValueByKey("t");
            if (type == NodeType.Agent || type == NodeType.AgentShare)
            {
                return true;
            }
            return false;
        }

        public bool hasAgentItem()
        {
            List<TreeViewItem> tvItemList = tree.GetAllItems();
            foreach (TreeViewItem item in tvItemList)
            {
                JToken token = item.DataContext as JToken;
                if (token["t"] != null)
                {
                    string type = token["t"].ToString();
                    if (type == NodeType.Agent || type == NodeType.AgentShare)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public TreeViewItem firstIndexAgentItem()
        {
            List<TreeViewItem> tvItemList = tree.GetAllItems();
            foreach (TreeViewItem item in tvItemList)
            {
                JToken token = item.DataContext as JToken;
                if (token["t"] != null)
                {
                    string type = token["t"].ToString();
                    if (type == NodeType.Agent || type == NodeType.AgentShare)
                    {
                        return item;
                    }
                }

            }
            return null;
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(GripMissionTreeController));
    }
}
