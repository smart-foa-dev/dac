﻿using DAC.CtmData;
using DAC.Model.Util;
using DAC.View;
using FoaCore;
using FoaCore.Common;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using log4net;
using SpreadsheetGear;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;
using DAC.AExcel;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Text.RegularExpressions;
using FoaCore.Common.Control;
using DAC.Util;
using Grip.Net;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_SpreadsheetTemplate.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_DACTemplate : BaseControl
    {
        public bool isFirstFile;
        public string excelFilePath;

        // private CtmDetails editControl;
        public bool isDragCtmName;
        public Mission currentSelectedMission;

        private List<CtmObject> ctms = new List<CtmObject>();
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 start
        // Multi_Dac 修正
        private string OnlineId = string.Empty;
        private List<string> missionList = new List<string>();
        //吹き出し追加
        private Dictionary<string, Dictionary<ElementConditionData, string>> elementConditions = new Dictionary<string, Dictionary<ElementConditionData, string>>();

        private Dictionary<string, int> conditionPatterns = new Dictionary<string, int>();
        private Dictionary<string, List<ElementConditionData>> conditionLists = new Dictionary<string, List<ElementConditionData>>();
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
        // Input parameter for DAC Excel file..
        private Dictionary<string, Mission> missionDict = new Dictionary<string, Mission>();
        private Dictionary<Mission, List<CtmObject>> missionCtmDict = new Dictionary<Mission,List<CtmObject>>();
        private Dictionary<CtmObject, List<CtmElement>> missionCtmElementDict = new Dictionary<CtmObject, List<CtmElement>>();

        //AISDAC No.81 sunyi 20181214 start
        //AISMM No.85 sunyi 20181211 内容修正、GripMission条件表示
        private Dictionary<string, List<ElementConditionData>> conditionLists_Grip = new Dictionary<string, List<ElementConditionData>>();
        private Dictionary<string, Dictionary<ElementConditionData, string>> elementConditions_Grip = new Dictionary<string, Dictionary<ElementConditionData, string>>();
        private Dictionary<string, int> conditionPatterns_Grip = new Dictionary<string, int>();
        private List<string> missionList_Grip = new List<string>();
        List<string> ctmIdList = new List<string>();
        //AISDAC No.81 sunyi 20181214 end

        private string missionId = string.Empty;

        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/16 start
        //階層Dacサーバ化
        private string preSelectedMissionId = string.Empty;
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/16 end

        // Wang Issue DAC-96 20190320 Start
        private CreateDacParam createDacParam = null;

        public UserControl_DACTemplate(ControlMode mode, CreateDacParam createDacParam, bool isFirstFile = true) : this(mode, isFirstFile, createDacParam.templateFile)
        {
            this.createDacParam = createDacParam;
        }
        // Wang Issue DAC-96 20190320 End

        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        //public UserControl_DACTemplate(bool isFirstFile = true, string filePath = "")
        public UserControl_DACTemplate(ControlMode mode, bool isFirstFile = true, string filePath = "")
        // Wang Issue NO.687 2018/05/18 End
        {
            InitializeComponent();
			// BugFix AIS_MM.99-2 20190611 yakiyama start.
			//AisTvRefDicMission.CallBackDeleteMissionClick = () => { AisTvRefDicElement.JsonItemSource = "[]"; };
			// BugFix AIS_MM.99-2 20190611 yakiyama end.

            Init();
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            //this.Mode = ControlMode.Dac;
            this.Mode = mode;
            // Wang Issue NO.687 2018/05/18 End
            this.TEMPLATE = "DACReport.xlsm";

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            resultDir = Path.Combine(AisConf.BaseDir, "DAC");
            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }
            //Dac-86 sunyi 20181227 start
            OpenDacHandler += OpenDacStatusChangedHandler;
            //Dac-86 sunyi 20181227 end

        }
        public override void SetMission2()
        {
            if (!this.isFirstFile)
            {
                readExcelFile(this.excelFilePath);
            }

        }

        private bool setMission()
        {
            if (string.IsNullOrEmpty(this.missionId))
            {
                return false;
            }

            this.IsSelectedProgramMission = true;

            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 start
            // Multi_Dac 修正
            // Mission Tree
            // Wang Issue NO.727 20180618 Start
            //TreeViewItem item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(this.missionId);
            TreeViewItem item = this.DataSource.Equals(DataSourceType.GRIP) ?
                this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(this.missionId) :
                this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(this.missionId);
            // Wang Issue NO.727 20180618 End
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end

            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                }
                return false;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 start
            // Multi_Dac 修正
            // Wang Issue NO.727 20180618 Start
            //this.mainWindow.treeView_Mission_CTM.Focus();
            if (this.DataSource.Equals(DataSourceType.GRIP))
            {
                this.mainWindow.treeView_Mission_Grip.Focus();
            }
            else
            {
                this.mainWindow.treeView_Mission_CTM.Focus();
            }
            // Wang Issue NO.727 20180618 End
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
            item.Focus();

            return true;
        }

        public void AttachDragDropEventToTreeViewItems()
        {
            foreach (TreeViewItem item in this.mainWindow.treeView_Mission_CTM.GetAllItems())
            {
                JToken token = item.DataContext as JToken;
                if (token["t"] != null && token["t"].ToString() == "m") // mission item only..
                {
                    item.MouseMove += missionItem_MouseMove;
                }
            }
        }

        private void missionItem_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && e.MiddleButton == MouseButtonState.Released && e.RightButton == MouseButtonState.Released)
            {
                TreeViewItem tvItem = sender as TreeViewItem;
                JToken tokenItem = tvItem.DataContext as JToken;
                DataObject dataObj = new DataObject(DataFormats.Serializable, tokenItem);
                DragDrop.DoDragDrop(tvItem, dataObj, DragDropEffects.Copy);
            }
        }


        //public override void DataSourceSelectionChanged(string removingCtmName = null)
        //{
        //    Mission mission = GetSelectedMission();
        //    this.currentSelectedMission = mission;
        //    this.lbMission.SelectedIndex = -1;
        //}



        #region Event Handler

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            //Init();
            this.editControl.DAC = this;

            //言語が英語の場合の表示調整
            string cultureName = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;
            if (cultureName == "en-US")
            {
                this.Border.Width = 170;
                this.label_dateTimePicker_Start.Width = 86;
                this.label_dateTimePicker_End.Width = 86;
            }
			// Wang New dac flow 20190308 Start
            this.dateTimePicker_StartDate.Text = DateTime.Now.ToString("yyyy/MM/dd");
            //this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_StartDate };
            //this.comboBox_Start.SelectedIndex = 1;
			// Wang New dac flow 20190308 End

            // No.549 Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;

            // Wang Issue DAC-96 20190320 Start
            if (this.createDacParam != null && this.createDacParam.start.HasValue)
            {
                this.dateTimePicker_Start.Text = this.createDacParam.start.Value.ToString("HH:mm");
            }

            if (this.createDacParam != null && this.createDacParam.end.HasValue)
            {
                this.dateTimePicker_End.Text = this.createDacParam.end.Value.ToString("HH:mm");
            }
            // Wang Issue DAC-96 20190320 End
            //Dac_Home sunyi 20190530 start
            if (this.createDacParam != null)
            {
                this.DACFileName.Content = Path.GetFileNameWithoutExtension(this.createDacParam.templateFile.ToString());
                this.DACFileName.ToolTip = this.DACFileName.Content;
            }
            else if (!string.IsNullOrEmpty(this.excelFilePath))
            {
                this.DACFileName.Content = AExcel.ExcelUtil.GetToolTipDacFileName(Path.GetFileNameWithoutExtension(this.excelFilePath.ToString()));
                this.DACFileName.ToolTip = this.DACFileName.Content;

                SpreadsheetGear.IWorkbook workbook = null;

                try
                {
                    // ワークブック作成
                    workbook = SpreadsheetGear.Factory.GetWorkbook(this.excelFilePath.ToString());

                    // paramシート作成
                    string paramSheetName = Keywords.PERIOD_TIME_SHEET;
                    SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(workbook, paramSheetName);
                    SpreadsheetGear.IRange cellsParam = worksheetParam.Cells;

                    if (cellsParam["A1"].Entry != null)
                    {
                        this.dateTimePicker_Start.Text = (DateTime.Parse(cellsParam["A1"].Entry.ToString())).ToString("HH:mm");
                    }
                    if (cellsParam["A2"].Entry != null)
                    {
                        this.dateTimePicker_End.Text = (DateTime.Parse(cellsParam["A2"].Entry.ToString())).ToString("HH:mm");
                    }
                }
                finally
                {
                    if (workbook != null)
                    {
                        workbook.Close();
                    }
                }
            }
            //Dac_Home sunyi 20190530 end
        }

        // Wang Issue DAC-72 20181225 Start
        private async void OpenDac(object sender, DoWorkEventArgs e)
        {
            try
            {
                // CheckBoxTreeViewのデータソースを取得
                OpenDacParam param = e.Argument as OpenDacParam;
                JObject workplaceJson = new JObject();
                JArray jArrayMission = param.jArrayMission;
                JArray jArrayCtm = param.jArrayCtm;
                JArray jArrayElement = param.jArrayElement;
                JArray array = new JArray();

                // Processing on Server dn 2018/09/07 start
                Boolean ctmHasFlag = false;
                // 期間固定の場合,検索条件チェック
                foreach (JToken missionToken in jArrayMission)
                {

                    if (!getCheckedFlag(missionToken))
                    {
                        continue;
                    }
                    // CTM missionがあるかどうか判定(missionNewType=1:CTM mission選択)

                    if (int.Parse((string)missionToken["missionNewType"]) == 1 && ctmHasFlag == false)
                    {
                        ctmHasFlag = true;
                    }

                    int searchType = int.Parse((string)missionToken["aisSearchType"]);
                    if (searchType == 2)
                    {
                        // 期間チェック：開始日時　> 終了日時、エラーになる
                        long startTime = (long)missionToken["offlineStart"];
                        long endTime = (long)missionToken["offlineEnd"];

                        if (startTime > endTime)
                        {
                            //AIS_DAC No.74 sunyi 2018/12/05 start
                            //// Wang Issue of AISMM-21 20181109 Start
                            //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                            //ScrollViewerDac.IsEnabled = true;
                            //// Wang Issue of AISMM-21 20181109 End
                            //FoaMessageBox.ShowError("AIS_E_019");
                            //return;
                            DateTime endDate = UnixTime.ToDateTime(endTime);
                            //DateTime endDate = new DateTime(endTime);
                            missionToken["offlineEnd"] = UnixTime.ToLong(endDate.AddDays(1));
                            //AIS_DAC No.74 sunyi 2018/12/05 end
                        }
                    }
                }

                // Processing on Server dn 2018/09/07 end
                // 選択されたエレメントの存在フラグ
                bool exportFlag = false;

                foreach (JToken elementToken in jArrayElement)
                {
                    //エレメントが存在するの判定
                    if (!exportFlag)
                    {
                        if (getCheckedFlag(elementToken))
                        {
                            exportFlag = true;
                            break;
                        }
                    }
                }

                //ISSUE_NO.727 sunyi 2018/06/14 Start
                //grip mission処理ができるように修正にする
                if (!this.IsSelectedProgramMission)
                {
                    this.DataSource = DataSourceType.GRIP;
                }

                // Processing On Server dn 2018/09/26 start
                if (ctmHasFlag)
                {
                    this.DataSource = DataSourceType.NONE;
                }
                else
                {
                    this.DataSource = DataSourceType.GRIP;
                }
                // Processing On Server dn 2018/09/26 end

                // 出力するのエレメントがありません
                if (!exportFlag)
                {
                    //Dac-86 sunyi 20181227 start
                    Dispatcher.Invoke(() =>
                    {
                        //Dac-86 sunyi 20181227 end
                        // Wang Issue of AISMM-21 20181109 Start
                        //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                        // Wang Issue of AISMM-21 20181109 End
                        FoaMessageBox.ShowError("AIS_E_007");
                        //dac home No.15 20190709 sunyi start
                        mainWindow.LoadinVisibleChange(true);
                        //dac home 20190709 sunyi end
                    });
                    //Dac-86 sunyi 20181227 end
                    return;
                }

                var workFile = CreateWorkingFile();

                DateTime end = DateTime.Now;
                DateTime start = DateTime.Now;

                // get mission result
                string resultFolder = string.Empty;
                string rootFolder = string.Empty;
                string path = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission");
                if (Directory.Exists(path))
                {
                    FoaCore.Util.FileUtil.clearFolder(path);
                }

                List<int> listInterval = new List<int>();
                int routeIndex = 0;
                int displayOrder = 0;
                foreach (JToken missionToken in jArrayMission)
                {
                    if (!getCheckedFlag(missionToken))
                    {
                        continue;
                    }

                    int searchType = int.Parse((string)missionToken["aisSearchType"]);
                    // 開始終了時間を設定
                    convertStartAndEndTime(missionToken, out start, out end);

                    this.DownloadCts = new CancellationTokenSource();

                    string missionTokenId = missionToken[MmsTvJsonKey.Id].ToString();
                    if (getMissionType(missionToken) == 1)
                    { //CTM Mission
                        //Dac-86 sunyi 20181227 start
                        //resultFolder = await GetMissionCtm(start, end, missionTokenId, this.DownloadCts);
                        resultFolder = await GetMissionCtm(start, end, missionTokenId, this.DownloadCts, param.handler);
                        if (resultFolder == null)
                        {
                            continue;
                        }
                        else
                        {

                        }
                        //Dac-86 sunyi 20181227 end
                        DirectoryInfo di = new DirectoryInfo(resultFolder);
                        rootFolder = di.Parent.FullName;
                    }
                    else
                    { //Grip Mission
                        missionToken["routeIndex"] = routeIndex;
                        //Dac-86 sunyi 20181227 start
                        //resultFolder = await GetGripData(start, end, missionToken);
                        resultFolder = await GetGripData(start, end, missionToken);
                        //Dac-86 sunyi 20181227 end
                        rootFolder = string.Empty;
                        if (resultFolder == null)
                        {
                            logger.Debug("指定した期間「" + start + " ～ " + end + "」で「" + missionTokenId + "」のデータが存在しません。");
                        }
                        else
                        {
                            routeIndex++;
                        }
                    }

                    // 結果フォルダを設定
                    missionToken["resultFolder"] = resultFolder;
                    missionToken["rootFolder"] = rootFolder;
                    // Processing On Server dn 2018/09/13 start
                    // 選択ミッションの表示順
                    missionToken["displayOrder"] = displayOrder;
                    displayOrder++;
                    // Processing On Server dn 2018/09/13 end
                    // Ctmsデータを設定
                    JArray tempCtmChildren = new JArray();
                    foreach (JToken jToken in jArrayCtm)
                    {
                        if (missionTokenId == jToken["missionId"].ToString())
                        {
                            tempCtmChildren.Add(jToken);
                        }
                    }
                    missionToken["ctms"] = (JArray)tempCtmChildren;

                    // Elementsデータを設定
                    JArray tempElementChildren = new JArray();
                    foreach (JToken elementToken in jArrayElement)
                    {
                        if (missionTokenId == elementToken["missionId"].ToString())
                        {
                            tempElementChildren.Add(elementToken);
                        }
                    }
                    missionToken["elements"] = (JArray)tempElementChildren;

                    // Mission Arrayを設定
                    array.Add(missionToken);
                }

                Dictionary<CtmObject, List<CtmElement>> ctmElementDict = new Dictionary<CtmObject, List<CtmElement>>();
                foreach (JToken elementToken in jArrayElement)
                {
                    if (getMissionType(elementToken) == 0)
                    {
                        continue;
                    }

                    if (getCheckedFlag(elementToken))
                    {
                        string missionId = elementToken["missionId"].ToString();
                        string strElmToken = elementToken.ToString();
                        strElmToken = strElmToken.Replace(missionId + "-", "");
                        JToken tmpToken = JToken.Parse(strElmToken);

                        CtmObject cc = JsonConvert.DeserializeObject<CtmObject>(tmpToken.ToString());
                        List<CtmElement> listElement = new List<CtmElement>();
                        JArray eleGroupArray = JArray.Parse(tmpToken["children"].ToString());
                        foreach (JToken group in eleGroupArray)
                        {
                            //AIS_DAC No.76 sunyi 2018/12/05 start
                            //group["children"]がNullの場合、チェックする
                            if (group["children"] == null)
                            {
                                //AISMM-94 sunyi 2019/03/27 start
                                //Groupがないエレメントをセット
                                if (getCheckedFlag(group))
                                {
                                    CtmElement objElement = new CtmElement();
                                    objElement = JsonConvert.DeserializeObject<CtmElement>(group.ToString());
                                    listElement.Add(objElement);
                                }
                                //AISMM-94 sunyi 2019/03/27 end
                                continue;
                            }
                            //AIS_DAC No.76 sunyi 2018/12/05 end
                            JArray eleArray = JArray.Parse(group["children"].ToString());
                            foreach (JToken ele in eleArray)
                            {
                                if (!getCheckedFlag(ele))
                                {
                                    continue;
                                }

                                CtmElement objElement = new CtmElement();
                                objElement = JsonConvert.DeserializeObject<CtmElement>(ele.ToString());
                                listElement.Add(objElement);
                            }
                        }

                        ctmElementDict.Add(cc, listElement);
                    }
                }

                // MIN Interval.
                int minInterval = 0;
                if (listInterval.Count > 0)
                {
                    minInterval = listInterval.Min();
                }

                // オンライングラフID
                OnlineId = new FoaCore.Common.Util.GUID().ToString();

                workplaceJson["mission_data"] = array;
                workplaceJson["work_place_flag"] = 2;
                workplaceJson["file_type"] = 2;
                workplaceJson["file_id"] = OnlineId;
                workplaceJson["file_name"] = OnlineId;

                //var workFile = CreateWorkingFile();
                //ExcelOpen(workFile);

                AisHttpClient client = new AisHttpClient(null);
                client.ShowErrorMessage = false;
                client.CompleteHandler += resJson =>
                {
                    if (string.IsNullOrEmpty(resJson))
                    {
                        // Wang Issue of AISMM-21 20181109 Start
                        //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                        // Wang Issue of AISMM-21 20181109 End
                        logger.Info("WorkPlaceデータを作成が失敗しました。");
                        return;
                    }
                    //excelOpen(workFile, start, end, resultRootFolder, periodHour, searchType, resJson);
					// Wang New dac flow 20190308 Start
                    //excelOpen(workFile, array, ctmElementDict, minInterval, resJson.ToString(), OnlineId, param.dtStart, param.dtEnd);

                    //Dac No.33 sun 20190726 start
                    //excelOpen(workFile, array, ctmElementDict, minInterval, resJson.ToString(), OnlineId, param.dtStart, param.dtEnd, param.dtStartDate);
                    excelOpen(workFile, array, ctmElementDict, minInterval, resJson.ToString(), OnlineId, param.dtStart, param.dtEnd, param.dtStartDate, param.jArrayCtm);
                    //Dac No.33 sun 20190726 end
                    // Wang New dac flow 20190308 End
                    // Wang Issue of AISMM-21 20181109 Start
                    //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                    Dispatcher.Invoke(() =>
                    {
                        mainWindow.LoadinVisibleChange(true);
						// Wang New dac flow 20190308 Start
                        //ScrollViewerDac.IsEnabled = true;
						// Wang New dac flow 20190308 End
                    });
                    // Wang Issue of AISMM-21 20181109 End
                };
                client.ExceptionHandler = (ex, str) =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        FoaMessageBox.ShowError("C_UNEXPECTED_ERROR", ex);
                        mainWindow.LoadinVisibleChange(true);
						// Wang New dac flow 20190308 Start
                        //ScrollViewerDac.IsEnabled = true;
						// Wang New dac flow 20190308 End
                    });
                };

                client.Post(CmsUrl.GetAisCreate(), workplaceJson.ToString());
            }
#pragma warning disable
            catch (Exception exception)
#pragma warning restore
            {
                Dispatcher.Invoke(() =>
                {
                    mainWindow.LoadinVisibleChange(true);
					// Wang New dac flow 20190308 Start
                    //ScrollViewerDac.IsEnabled = true;
					// Wang New dac flow 20190308 End
                });
            }
        }

        BackgroundWorker OpenDacWorker;
        //Dac-86 sunyi 20181227 start
        EventHandler OpenDacHandler;
        private void OpenDacStatusChangedHandler(object sender, EventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                EventArgsEx ex = e as EventArgsEx;
                FoaMessageBox.ShowError(ex.Key);
            });
        }
        //Dac-86 sunyi 20181227 end
        class OpenDacParam
        {
            public JArray jArrayMission;
            public JArray jArrayCtm;
            public JArray jArrayElement;
            public DateTime? dtStart;
            public DateTime? dtEnd;
			// Wang New dac flow 20190308 Start
            public DateTime? dtStartDate;
			// Wang New dac flow 20190308 End
            //Dac-86 sunyi 20181227 start
            public EventHandler handler;
            //Dac-86 sunyi 20181227 end
        }
        // Wang Issue DAC-72 20181225 End

        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 Start
        // Multi_Dac 修正
        private void BtnOpenDAC_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                //dac No.7 sunyi start
                if (this.dateTimePicker_Start.Value == null || this.dateTimePicker_End.Value == null || this.dateTimePicker_StartDate.Value == null)
                {
                    //収集期間を設定してください。
                    FoaMessageBox.ShowError("AIS_E_073");
                    return;
                }
                //dac No.7 sunyi end
                //AISMM No.55 sunyi 20181217 start
                //DACに展開する
                if (AisTvRefDicMission.Items.IsEmpty)
                {
                    FoaMessageBox.ShowError("AIS_E_025");
                    return;
                }
                //AISMM No.55 sunyi 20181217 end
                // Wang Issue of AISMM-21 20181109 Start
                mainWindow.LoadinVisibleChange(false);
                //LoadingDac.Visibility = System.Windows.Visibility.Visible;
				// Wang New dac flow 20190308 Start
                //ScrollViewerDac.IsEnabled = false;
				// Wang New dac flow 20190308 End

                // Wang Issue DAC-72 20181225 Start
                /*
                //Excel出力する際に、「選択ミッション」に検索条件を再設定
                foreach (TreeViewItem treeViewItem in AisTvRefDicMission.Items)
                {
                    JToken missionToken = treeViewItem.DataContext as JToken;

                    // 1.AISを修正場合
                    // 2.ミッションアイテムを選択された場合
                    if (!this.isFirstFile || missionHasSelected())
                    {
                        if (missionToken["aisSearchType"] == null)
                        {
                            setSearchConditions(missionToken);
                        }

                        System.Windows.Media.Color color = ((SolidColorBrush)treeViewItem.Background).Color;
                        if (color == System.Windows.Media.Colors.DodgerBlue)
                        {
                            setSearchConditions(missionToken);
                        }
                    }
                    else
                    {
                        setSearchConditions(missionToken);
                    }
                }

                // CheckBoxTreeViewのデータソースを取得
                JObject workplaceJson = new JObject();
                JArray jArrayMission = JArray.Parse(AisTvRefDicMission.JsonItemSource);
                JArray jArrayCtm = JArray.Parse(AisTvRefDicCtm.JsonItemSource);
                JArray jArrayElement = JArray.Parse(AisTvRefDicElement.JsonItemSource);
                JArray array = new JArray();

                // Processing on Server dn 2018/09/07 start
                Boolean ctmHasFlag = false;
                // 期間固定の場合,検索条件チェック
                foreach (JToken missionToken in jArrayMission)
                {

                    if (!getCheckedFlag(missionToken))
                    {
                        continue;
                    }
                    // CTM missionがあるかどうか判定(missionNewType=1:CTM mission選択)

                    if (int.Parse((string)missionToken["missionNewType"]) == 1 && ctmHasFlag == false)
                    {
                        ctmHasFlag = true;
                    }

                    int searchType = int.Parse((string)missionToken["aisSearchType"]);
                    if (searchType == 2)
                    {
                        // 期間チェック：開始日時　> 終了日時、エラーになる
                        long startTime = (long)missionToken["offlineStart"];
                        long endTime = (long)missionToken["offlineEnd"];

                        if (startTime > endTime)
                        {
                            //AIS_DAC No.74 sunyi 2018/12/05 start
                            //// Wang Issue of AISMM-21 20181109 Start
                            //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                            //ScrollViewerDac.IsEnabled = true;
                            //// Wang Issue of AISMM-21 20181109 End
                            //FoaMessageBox.ShowError("AIS_E_019");
                            //return;
                            DateTime endDate = UnixTime.ToDateTime(endTime);
                            //DateTime endDate = new DateTime(endTime);
                            missionToken["offlineEnd"] = UnixTime.ToLong(endDate.AddDays(1));
                            //AIS_DAC No.74 sunyi 2018/12/05 end
                        }
                    }
                }

                // Processing on Server dn 2018/09/07 end
                // 選択されたエレメントの存在フラグ
                bool exportFlag = false;

                foreach (JToken elementToken in jArrayElement)
                {
                    //エレメントが存在するの判定
                    if (!exportFlag)
                    {
                        if (getCheckedFlag(elementToken))
                        {
                            exportFlag = true;
                            break;
                        }
                    }
                }

                //ISSUE_NO.727 sunyi 2018/06/14 Start
                //grip mission処理ができるように修正にする
                if (!this.IsSelectedProgramMission)
                {
                    this.DataSource = DataSourceType.GRIP;
                }

                // Processing On Server dn 2018/09/26 start
                if (ctmHasFlag)
                {
                    this.DataSource = DataSourceType.NONE;
                }
                else
                {
                    this.DataSource = DataSourceType.GRIP;
                }
                // Processing On Server dn 2018/09/26 end

                // 出力するのエレメントがありません
                if (!exportFlag)
                {
                    // Wang Issue of AISMM-21 20181109 Start
                    //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                    mainWindow.LoadinVisibleChange(true);

                    ScrollViewerDac.IsEnabled = true;
                    // Wang Issue of AISMM-21 20181109 End
                    FoaMessageBox.ShowError("AIS_E_007");
                    return;
                }

                var workFile = CreateWorkingFile();

                DateTime end = DateTime.Now;
                DateTime start = DateTime.Now;

                // get mission result
                string resultFolder = string.Empty;
                string rootFolder = string.Empty;
                string path = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission");
                if (Directory.Exists(path))
                {
                    FoaCore.Util.FileUtil.clearFolder(path);
                }

                List<int> listInterval = new List<int>();
                int routeIndex = 0;
                int displayOrder = 0;
                foreach (JToken missionToken in jArrayMission)
                {
                    if (!getCheckedFlag(missionToken))
                    {
                        continue;
                    }

                    int searchType = int.Parse((string)missionToken["aisSearchType"]);
                    // 開始終了時間を設定
                    convertStartAndEndTime(missionToken, out start, out end);

                    this.DownloadCts = new CancellationTokenSource();

                    string missionTokenId = missionToken[MmsTvJsonKey.Id].ToString();
                    if (getMissionType(missionToken) == 1)
                    { //CTM Mission
                        resultFolder = await GetMissionCtm(start, end, missionTokenId, this.DownloadCts);
                        DirectoryInfo di = new DirectoryInfo(resultFolder);
                        rootFolder = di.Parent.FullName;
                    }
                    else
                    { //Grip Mission
                        missionToken["routeIndex"] = routeIndex;
                        resultFolder = await GetGripData(start, end, missionToken);
                        rootFolder = string.Empty;
                        if (resultFolder == null)
                        {
                            logger.Debug("指定した期間「" + start + " ～ " + end + "」で「" + missionTokenId + "」のデータが存在しません。");
                        }
                        else
                        {
                            routeIndex++;
                        }
                    }

                    // 結果フォルダを設定
                    missionToken["resultFolder"] = resultFolder;
                    missionToken["rootFolder"] = rootFolder;
                    // Processing On Server dn 2018/09/13 start
                    // 選択ミッションの表示順
                    missionToken["displayOrder"] = displayOrder;
                    displayOrder++;
                    // Processing On Server dn 2018/09/13 end
                    // Ctmsデータを設定
                    JArray tempCtmChildren = new JArray();
                    foreach (JToken jToken in jArrayCtm)
                    {
                        if (missionTokenId == jToken["missionId"].ToString())
                        {
                            tempCtmChildren.Add(jToken);
                        }
                    }
                    missionToken["ctms"] = (JArray)tempCtmChildren;

                    // Elementsデータを設定
                    JArray tempElementChildren = new JArray();
                    foreach (JToken elementToken in jArrayElement)
                    {
                        if (missionTokenId == elementToken["missionId"].ToString())
                        {
                            tempElementChildren.Add(elementToken);
                        }
                    }
                    missionToken["elements"] = (JArray)tempElementChildren;

                    // Mission Arrayを設定
                    array.Add(missionToken);
                }

                Dictionary<CtmObject, List<CtmElement>> ctmElementDict = new Dictionary<CtmObject, List<CtmElement>>();
                foreach (JToken elementToken in jArrayElement)
                {
                    if (getMissionType(elementToken) == 0)
                    {
                        continue;
                    }

                    if (getCheckedFlag(elementToken))
                    {
                        string missionId = elementToken["missionId"].ToString();
                        string strElmToken = elementToken.ToString();
                        strElmToken = strElmToken.Replace(missionId + "-", "");
                        JToken tmpToken = JToken.Parse(strElmToken);

                        CtmObject cc = JsonConvert.DeserializeObject<CtmObject>(tmpToken.ToString());
                        List<CtmElement> listElement = new List<CtmElement>();
                        JArray eleGroupArray = JArray.Parse(tmpToken["children"].ToString());
                        foreach (JToken group in eleGroupArray)
                        {
                            //AIS_DAC No.76 sunyi 2018/12/05 start
                            //group["children"]がNullの場合、チェックする
                            if (group["children"] == null)
                            {
                                continue;
                            }
                            //AIS_DAC No.76 sunyi 2018/12/05 end
                            JArray eleArray = JArray.Parse(group["children"].ToString());
                            foreach (JToken ele in eleArray)
                            {
                                if (!getCheckedFlag(ele))
                                {
                                    continue;
                                }

                                CtmElement objElement = new CtmElement();
                                objElement = JsonConvert.DeserializeObject<CtmElement>(ele.ToString());
                                listElement.Add(objElement);
                            }
                        }

                        ctmElementDict.Add(cc, listElement);
                    }
                }

                // MIN Interval.
                int minInterval = 0;
                if (listInterval.Count > 0)
                {
                    minInterval = listInterval.Min();
                }

                // オンライングラフID
                OnlineId = new FoaCore.Common.Util.GUID().ToString();

                workplaceJson["mission_data"] = array;
                workplaceJson["work_place_flag"] = 2;
                workplaceJson["file_type"] = 2;
                workplaceJson["file_id"] = OnlineId;
                workplaceJson["file_name"] = OnlineId;

                //var workFile = CreateWorkingFile();
                //ExcelOpen(workFile);

                CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
                client.CompleteHandler += resJson =>
                {
                    if (string.IsNullOrEmpty(resJson))
                    {
                        // Wang Issue of AISMM-21 20181109 Start
                        //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                        mainWindow.LoadinVisibleChange(true);
                        ScrollViewerDac.IsEnabled = true;
                        // Wang Issue of AISMM-21 20181109 End
                        logger.Info("WorkPlaceデータを作成が失敗しました。");
                        return;
                    }
                    //excelOpen(workFile, start, end, resultRootFolder, periodHour, searchType, resJson);
                    excelOpen(workFile, array, ctmElementDict, minInterval, resJson.ToString(), OnlineId);
                    // Wang Issue of AISMM-21 20181109 Start
                    //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                    mainWindow.LoadinVisibleChange(true);
                    ScrollViewerDac.IsEnabled = true;
                    // Wang Issue of AISMM-21 20181109 End
                };
                client.Post(CmsUrl.GetAisCreate(), workplaceJson.ToString());
                */
                //Excel出力する際に、「選択ミッション」に検索条件を再設定
                foreach (TreeViewItem treeViewItem in AisTvRefDicMission.Items)
                {
                    JToken missionToken = treeViewItem.DataContext as JToken;

                    // 1.AISを修正場合
                    // 2.ミッションアイテムを選択された場合
                    if (!this.isFirstFile || missionHasSelected())
                    {
                        if (missionToken["aisSearchType"] == null)
                        {
                            setSearchConditions(missionToken);
                        }

                        System.Windows.Media.Color color = ((SolidColorBrush)treeViewItem.Background).Color;
                        if (color == System.Windows.Media.Colors.DodgerBlue)
                        {
                            setSearchConditions(missionToken);
                        }
                    }
                    else
                    {
                        setSearchConditions(missionToken);
                    }
                }

                OpenDacWorker = new BackgroundWorker();
                OpenDacWorker.WorkerReportsProgress = true;
                OpenDacWorker.DoWork += new DoWorkEventHandler(OpenDac);

                OpenDacParam param = new OpenDacParam()
                {
                    jArrayMission = JArray.Parse(AisTvRefDicMission.JsonItemSource),
                    jArrayCtm = JArray.Parse(AisTvRefDicCtm.JsonItemSource),
                    jArrayElement = JArray.Parse(AisTvRefDicElement.JsonItemSource),
                    dtStart = this.dateTimePicker_Start.Value,
                    dtEnd = this.dateTimePicker_End.Value,
					// Wang New dac flow 20190308 Start
                    dtStartDate = this.dateTimePicker_StartDate.Value,
					// Wang New dac flow 20190308 End
                    //Dac-86 sunyi 20181227 start
                    handler = OpenDacHandler
                    //Dac-86 sunyi 20181227 end
                };

                OpenDacWorker.RunWorkerAsync(param);
                // Wang Issue DAC-72 20181225 End
            }
            catch (Exception ex)
            {
                // Wang Issue of AISMM-21 20181109 Start
                //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                mainWindow.LoadinVisibleChange(true);
				// Wang New dac flow 20190308 Start
                //ScrollViewerDac.IsEnabled = true;
				// Wang New dac flow 20190308 End
                throw ex;
                // Wang Issue of AISMM-21 20181109 End
            }
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 end

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 start
        private void convertStartAndEndTime(JToken missionToken, out DateTime startTime, out DateTime endTime)
        {
            DateTime end = DateTime.Now;
            DateTime start = DateTime.Now;
            int searchType = int.Parse((string)missionToken["aisSearchType"]);
            float periodHour = 0;

            //画面検索条件チェック
            if (searchType == 1)
            {
                periodHour = float.Parse((string)missionToken["onlineTime"]);
                TimeSpan period = TimeSpan.FromHours(periodHour);
                start = end - period;
            }
            else
            {
                start = getDateTimeByMilliseconds((long)missionToken["offlineStart"]);
                end = getDateTimeByMilliseconds((long)missionToken["offlineEnd"]);
            }

            missionToken["startSearchTime"] = start;
            missionToken["endSearchTime"] = end;
            startTime = start;
            endTime = end;
        }
        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/14 End


        private string CreateWorkingFile()
        {
			// Wang New dac flow 20190308 Start
            if (isFirstFile) {
                return this.excelFilePath;
            }
			// Wang New dac flow 20190308 End

            //2017.12.05 No.505 インストーラー使用にむけて該当処理を復活
            //DACテンプレートのパス
            //string filePathSrc = isFirstFile ? System.Windows.Forms.Application.StartupPath + @"\DAC_Template\DACReport.xlsm" : this.excelFilePath;
            string filePathSrc = isFirstFile ?  System.IO.Path.Combine(AisConf.Config.DacFilePath,"DACReport.xlsm") : this.excelFilePath;

            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            //string fileName = string.Format("{0}_{1}.xlsm", "DAC", DateTime.Now.ToString("yyyyMMddHHmmss"));
            //dac_home dacFileName修正 名称の階層DACを削除 　影響範囲広い、一応戻します
            /*
            string fileName = string.Format("{0}_{1}.xlsm", this.Mode == ControlMode.MultiDac ? DAC.Properties.Resources.TEXT_STD_MULTIDAC : "DAC", DateTime.Now.ToString("yyyyMMddHHmmss"));
            //string fileName = string.Format("{0}.xlsm", DateTime.Now.ToString("yyyyMMddHHmmss"));
            // Wang Issue NO.687 2018/05/18 End
            var filePathDest = Path.Combine(resultDir, fileName);
            File.Copy(filePathSrc, filePathDest);
            return filePathDest;
            */
            return this.excelFilePath;
        }

        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed(string text)
        {
            System.Text.RegularExpressions.Regex regex =
                new System.Text.RegularExpressions.Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        #endregion


        #region Open exising excel file
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 start
        // Multi_Dac 修正
        //private void readExcelFile(string filePath)
        //{
        //    SpreadsheetGear.IWorkbook book = null;

        //    try
        //    {
        //        // ワークブック
        //        book = SpreadsheetGear.Factory.GetWorkbook(filePath);

        //        string paramSheetName = "ミッション";
        //        SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
        //        if (worksheetParam != null)
        //        {
        //            readParam(worksheetParam);
        //        }
        //        paramSheetName = "業務時間";
        //        worksheetParam = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
        //        if (worksheetParam != null)
        //        {
        //            readStartEndTimeParam(worksheetParam);
        //        }
        //    }
        //    finally
        //    {
        //        if (book != null)
        //        {
        //            book.Close();
        //        }

        //    }
        //}

        //AISDAC No.81 sunyi 20181214 start
        //AISMM No.85 sunyi 20181211 内容修正、GripMission条件表示
        private void readExcelFileStep3(JToken workplaceJson)
        {
            // Wang New dac flow 20190308 Start
            if (workplaceJson == null)
            {
                return;
            }
            // Wang New dac flow 20190308 End

            // AISを修正処理、画面にデータベースから検索結果を表示する
            CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
            client.CompleteHandler += (resJson) =>
            {
                // ディフォルート選択のMissionId
                var firstFlag = true;

                //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                //ミッションタブのミッションが選択状態になるように修正
                var firstGripMission = false;
                //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end
                var firstMissionId = "";

                JObject tmpJson = JObject.Parse(resJson);
                JObject tempWorkPlaceJson = JObject.Parse(resJson);
                JArray tempMissionJson = (JArray)tempWorkPlaceJson["mission_data"];

                foreach (JObject mission in tempMissionJson)
                {
                    JToken tmpMission = mission.DeepClone();

                    tmpMission["ctms"] = new JArray();
                    tmpMission["elements"] = new JArray();
                    var missionId = mission["id"].ToString();

                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 start
                    //条件付けエレメントのIDをJsonTreeViewに渡す
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 start
                    //吹き出し追加
                    setElementNameWithCondition_Mission(this.conditionLists);
                    //AISDAC No.81 sunyi 20181214 start
                    //AISMM No.85 sunyi 20181211 内容修正、GripMission条件表示
                    setElementNameWithCondition_Grip(this.conditionLists_Grip);
                    //AISDAC No.81 sunyi 20181214 end
                    Dictionary<string, string> tooltips = new Dictionary<string, string>();
                    //foreach (var condition in this.conditionLists)
                    //{
                    //    foreach (ElementConditionData a in condition.Value)
                    //    {
                    //        tmp.Add(a.Id);
                    //    }
                    //}
                    foreach (var elementCondition in this.elementConditions)
                    {
                        foreach (var element in elementCondition.Value)
                        {
                            foreach (var conditionPattern in conditionPatterns)
                            {
                                if (conditionPattern.Key.Equals(elementCondition.Key))
                                {
                                    tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                                }
                            }
                        }
                    }
                    //AISDAC No.81 sunyi 20181214 start
                    //AISMM No.85 sunyi 20181211 内容修正、GripMission条件表示
                    foreach (var elementCondition in this.elementConditions_Grip)
                    {
                        foreach (var element in elementCondition.Value)
                        {
                            foreach (var conditionPattern in conditionPatterns_Grip)
                            {
                                if (conditionPattern.Key.Equals(elementCondition.Key))
                                {
                                    tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                                }
                            }
                        }
                    }
                    //AISDAC No.81 sunyi 20181214 end
                    this.AisTvRefDicElement.conditionLists = tooltips;
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 end
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 end

                    this.AisTvRefDicMission.AisCheckFlag = 2;
                    this.AisTvRefDicMission.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                    this.AisTvRefDicMission.CheckChangedEvent += this.AisTvRefDicMissionCheckChanged;
                    this.AisTvRefDicMission.JsonItemSourceAdd = tmpMission.ToString();
                    // Processing On Service 検証＃4　dn 2018/09/28 start
                    if (firstFlag)
                    {
                        firstMissionId = missionId;
                        //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                        //ミッションタブのミッションが選択状態になるように修正
                        if (int.Parse(tmpMission["missionNewType"].ToString()) == 0)
                        {
                            firstGripMission = true;
                        }
                        //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end
                        firstFlag = false;
                    }
                    // Processing On Service 検証＃4 dn 2018/09/28 end

                    this.AisTvRefDicCtm.AisCheckFlag = 2;
                    this.AisTvRefDicCtm.MissionId = missionId;
                    this.AisTvRefDicCtm.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                    this.AisTvRefDicCtm.CheckChangedEvent += this.AisTvRefDicCtmCheckChanged;
                    this.AisTvRefDicCtm.JsonItemSourceAdd = mission["ctms"].ToString();

                    this.AisTvRefDicElement.AisCheckFlag = 2;
                    this.AisTvRefDicElement.MissionId = missionId;

                    this.AisTvRefDicElement.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                    this.AisTvRefDicElement.CheckChangedEvent += this.AisTvRefDicElementCheckChanged;
                    this.AisTvRefDicElement.JsonItemSourceAdd = mission["elements"].ToString();
                }
                // Processing On Service 検証＃4　dn 2018/09/28 start
                //一番上のミッションを選択した状態にする
                this.AisTvRefDicMission.GetItem(firstMissionId).IsSelected = true;

                //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                //ミッションタブのミッションが選択状態になるように修正
                if (firstGripMission)
                {
                    this.DataSource = DataSourceType.GRIP;
                }
                this.missionId = firstMissionId;
                setMission();
                //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end

                // Processing On Service 検証＃4 dn 2018/09/28 end
            };
            client.Post(CmsUrl.GetAisSelect(), workplaceJson.ToString());
        }

        private void readExcelFileStep2(JToken workplaceJson)
        {
            // 個別に指定する場合は、既存の設定をサーバから再取得
            CmsHttpClient ctmGetClient = new CmsHttpClient(Application.Current.MainWindow);
            foreach (string ctmId in ctmIdList)
            {
                ctmGetClient.AddParam("id", ctmId);
                // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
            }

            ctmGetClient.CompleteHandler += ctmJson =>
            {
                // this.missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);
                List<CtmObject> ctmList = JsonConvert.DeserializeObject<List<CtmObject>>(ctmJson);
                foreach (var ctm in ctmList)
                {
                    this.ctms.Add(ctm);
                }
                readExcelFileStep3(workplaceJson);
            };
            ctmGetClient.ExceptionHandler += (e, str) =>
            {
                readExcelFileStep3(workplaceJson);
            };
            ctmGetClient.Get(CmsUrlMms.Ctm.List());
        }

        private void readExcelFileStep1(JToken workplaceJson)
        {
            int count = 0;

            //エレメント条件を取得
            if (missionList_Grip.Count > 0)
            {
                foreach (string id2 in missionList_Grip)
                {
                    // 個別に指定する場合は、既存の設定をサーバから再取得
                    CmsHttpClient missionCtmGetClient2 = new CmsHttpClient(Application.Current.MainWindow);
                    missionCtmGetClient2.AddParam("id", id2);
                    // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
                    missionCtmGetClient2.CompleteHandler += missionCtmJson2 =>
                    {
                        var missionCtms2 = JsonConvert.DeserializeObject<GripMissionCtm>(missionCtmJson2);
                        GetElementConditionDatas_G(missionCtms2);
                        count++;
                        if (count.Equals(missionList_Grip.Count))
                        {
                            readExcelFileStep2(workplaceJson);
                        }
                    };
                    missionCtmGetClient2.ExceptionHandler += (e, str) =>
                    {
                        count++;
                        if (count.Equals(missionList_Grip.Count))
                        {
                            readExcelFileStep2(workplaceJson);
                        }
                    };
                    missionCtmGetClient2.Get(GripUrl.Mission.Base());
                }
            }
            else
            {
                readExcelFileStep2(workplaceJson);
            }
        }
        //AISDAC No.81 sunyi 20181214 end

        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet paramSheet = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);

                if (paramSheet != null)
                {
                    //// set SelectedMission
                    //if (paramMap.ContainsKey("ミッションID") &&
                    //    !string.IsNullOrEmpty(paramMap["ミッションID"]))
                    //{
                    //    if (this.DataSource == DataSourceType.MISSION)
                    //    {
                    //        string missionId = paramMap["ミッションID"];
                    //        Mission mission = AisUtil.GetMissionFromId(missionId);
                    //        this.SelectedMission = mission;
                    //    }
                    //    else if (this.DataSource == DataSourceType.GRIP)
                    //    {
                    //        string missionId = paramMap["ミッションID"];
                    //        GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                    //        this.SelectedGripMission = mission;
                    //    }
                    //}


                    // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
                    string roParamSheetName = Keywords.RO_PARAM_SHEET; ;
                    SpreadsheetGear.IWorksheet roParamSheet = AisUtil.GetSheetFromSheetName_SSG(book, roParamSheetName);

                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 start
                    //条件付けエレメントのIDをJsonTreeViewに渡す
                    string missionSheetName_M = "ミッション_MISSION";
                    SpreadsheetGear.IWorksheet missionSheet_M = AisUtil.GetSheetFromSheetName_SSG(book, missionSheetName_M);
                    readMissionStructure_M(missionSheet_M);

                    string missionSheetName_G = "ミッション_GRIP";
                    SpreadsheetGear.IWorksheet missionSheet_G = AisUtil.GetSheetFromSheetName_SSG(book, missionSheetName_G);
                    //AISDAC No.81 sunyi 20181214 start
                    //AISMM No.85 sunyi 20181211 内容修正、GripMission条件表示
                    //readMissionStructure_M(missionSheet_G);
                    readMissionStructure_G(missionSheet_G);
                    //AISDAC No.81 sunyi 20181214 end

                    //AISDAC No.81 sunyi 20181214 start
                    //AISMM No.85 sunyi 20181211 内容修正、GripMission条件表示
                    ////エレメント条件を取得
                    //foreach (string id in missionList)
                    //{
                    //    // 個別に指定する場合は、既存の設定をサーバから再取得
                    //    CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                    //    missionCtmGetClient.AddParam("missionId", id);
                    //    // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
                    //    missionCtmGetClient.CompleteHandler += missionCtmJson =>
                    //    {
                    //        var missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);

                    //        this.conditionLists.Clear();
                    //        this.conditionPatterns.Clear();
                    //        foreach (var missionCtm in missionCtms)
                    //        {
                    //            if (string.IsNullOrEmpty(missionCtm.ElementCondition) == false)
                    //            {
                    //                JToken condJson = JToken.Parse(missionCtm.ElementCondition);
                    //                JArray condElements = (JArray)condJson["elements"];
                    //                this.conditionPatterns.Add(missionCtm.CtmId, int.Parse(condJson["pattern"].ToString()));

                    //                List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
                    //                ElementConditionDatas = JsonConvert.DeserializeObject<List<ElementConditionData>>(condElements.ToString());
                    //                this.conditionLists.Add(missionCtm.CtmId, ElementConditionDatas);
                    //            }
                    //            else
                    //            {
                    //                this.conditionLists.Add(missionCtm.CtmId, new List<ElementConditionData>());
                    //            }
                    //        }
                    //    };
                    //    missionCtmGetClient.Get(CmsUrl.GetMissionCtmUrl());
                    //}
                    //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 end

                    //// ディフォルート選択のMissionId
                    //var firstFlag = true;

                    ////検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                    ////ミッションタブのミッションが選択状態になるように修正
                    //var firstGripMission = false;
                    ////検証シート_マルチモニター_No.64 sunyi 2018/11/20 end
                    //var firstMissionId = "";
                    //AISDAC No.81 sunyi 20181214 end

                    // Wang New dac flow 20190308 Start
                    //JToken workplaceJson = new JObject();
                    JToken workplaceJson = null;
                    // Wang New dac flow 20190308 End

                    if (roParamSheet != null)
                    {
                        SpreadsheetGear.IRange paramCells = roParamSheet.Cells;

                        for (int i = 1; i <= 10; i++)   // とりあえず10行ほど検索
                        {
                            // null check
                            if (paramCells[i, 0].Text == null)
                            {
                                continue;
                            }

                            // 更新周期
                            if (paramCells[i, 0].Text == "WorkPlaceID")
                            {
                                // Wang New dac flow 20190308 Start
                                //workplaceJson["work_place_id"] = paramCells[i, 1].Text;
                                if (!string.IsNullOrEmpty(paramCells[i, 1].Text))
                                {
                                    workplaceJson = new JObject();
                                    workplaceJson["work_place_id"] = paramCells[i, 1].Text;
                                }
                                // Wang New dac flow 20190308 End
                                break;
                            }
                        }

                        //AISDAC No.81 sunyi 20181214 start
                        //AISMM No.85 sunyi 20181211 内容修正、GripMission条件表示
                        //// AISを修正処理、画面にデータベースから検索結果を表示する
                        //CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
                        //client.CompleteHandler += (resJson) =>
                        //{
                        //    JObject tmpJson = JObject.Parse(resJson);
                        //    JObject tempWorkPlaceJson = JObject.Parse(resJson);
                        //    JArray tempMissionJson = (JArray)tempWorkPlaceJson["mission_data"];

                        //    foreach (JObject mission in tempMissionJson)
                        //    {
                        //        JToken tmpMission = mission.DeepClone();

                        //        tmpMission["ctms"] = new JArray();
                        //        tmpMission["elements"] = new JArray();
                        //        var missionId = mission["id"].ToString();

                        //        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 start
                        //        //条件付けエレメントのIDをJsonTreeViewに渡す
                        //        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 start
                        //        //吹き出し追加
                        //        setElementNameWithCondition_Mission(this.conditionLists);
                        //        Dictionary<string, string> tooltips = new Dictionary<string, string>();
                        //        //foreach (var condition in this.conditionLists)
                        //        //{
                        //        //    foreach (ElementConditionData a in condition.Value)
                        //        //    {
                        //        //        tmp.Add(a.Id);
                        //        //    }
                        //        //}
                        //        foreach (var elementCondition in this.elementConditions)
                        //        {
                        //            foreach (var element in elementCondition.Value)
                        //            {
                        //                foreach (var conditionPattern in conditionPatterns)
                        //                {
                        //                    if (conditionPattern.Key.Equals(elementCondition.Key))
                        //                    {
                        //                        tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                        //                    }
                        //                }
                        //            }
                        //        }
                        //        this.AisTvRefDicElement.conditionLists = tooltips;
                        //        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 end
                        //        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 end

                        //        this.AisTvRefDicMission.AisCheckFlag = 2;
                        //        this.AisTvRefDicMission.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                        //        this.AisTvRefDicMission.CheckChangedEvent += this.AisTvRefDicMissionCheckChanged;
                        //        this.AisTvRefDicMission.JsonItemSourceAdd = tmpMission.ToString();
                        //        // Processing On Service 検証＃4　dn 2018/09/28 start
                        //        if (firstFlag)
                        //        {
                        //            firstMissionId = missionId;
                        //            //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                        //            //ミッションタブのミッションが選択状態になるように修正
                        //            if (int.Parse(tmpMission["missionNewType"].ToString()) == 0)
                        //            {
                        //                firstGripMission = true;
                        //            }
                        //            //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end
                        //            firstFlag = false;
                        //        }
                        //        // Processing On Service 検証＃4 dn 2018/09/28 end

                        //        this.AisTvRefDicCtm.AisCheckFlag = 2;
                        //        this.AisTvRefDicCtm.MissionId = missionId;
                        //        this.AisTvRefDicCtm.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                        //        this.AisTvRefDicCtm.CheckChangedEvent += this.AisTvRefDicCtmCheckChanged;
                        //        this.AisTvRefDicCtm.JsonItemSourceAdd = mission["ctms"].ToString();

                        //        this.AisTvRefDicElement.AisCheckFlag = 2;
                        //        this.AisTvRefDicElement.MissionId = missionId;

                        //        this.AisTvRefDicElement.MissionNewType = int.Parse(tmpMission["missionNewType"].ToString());
                        //        this.AisTvRefDicElement.CheckChangedEvent += this.AisTvRefDicElementCheckChanged;
                        //        this.AisTvRefDicElement.JsonItemSourceAdd = mission["elements"].ToString();
                        //    }
                        //    // Processing On Service 検証＃4　dn 2018/09/28 start
                        //    //一番上のミッションを選択した状態にする
                        //    this.AisTvRefDicMission.GetItem(firstMissionId).IsSelected = true;

                        //    //検証シート_マルチモニター_No.64 sunyi 2018/11/20 start
                        //    //ミッションタブのミッションが選択状態になるように修正
                        //    if (firstGripMission)
                        //    {
                        //        this.DataSource = DataSourceType.GRIP;
                        //    }
                        //    this.missionId = firstMissionId;
                        //    setMission();
                        //    //検証シート_マルチモニター_No.64 sunyi 2018/11/20 end

                        //    // Processing On Service 検証＃4 dn 2018/09/28 end
                        //};
                        //client.Post(CmsUrl.GetAisSelect(), workplaceJson.ToString());
                        //エレメント条件を取得
                        if (missionList.Count > 0)
                        {
/*<<<<<<< HEAD
                            JObject tmpJson = JObject.Parse(resJson);
                            JObject tempWorkPlaceJson = JObject.Parse(resJson);

                            JArray tempMissionJson = (JArray)tempWorkPlaceJson["mission_data"];

                            foreach (JObject mission in tempMissionJson)
=======*/
                            int count = 0;
                            foreach (string id in missionList)
//>>>>>>> 9c225c4694d801da593c0f01c628b73c9db5be33
                            {
                                // 個別に指定する場合は、既存の設定をサーバから再取得
                                CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                                missionCtmGetClient.AddParam("missionId", id);
                                // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
                                missionCtmGetClient.CompleteHandler += missionCtmJson =>
                                {
                                    //AISMM No.85 sunyi 20181211 start
                                    //内容修正、GripMission条件表示
                                    var missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);
                                    GetElementConditionDatas(missionCtms);

                                    count++;
                                    if (count.Equals(missionList.Count))
                                    {
                                        readExcelFileStep1(workplaceJson);
                                    }
                                };
                                missionCtmGetClient.ExceptionHandler += (e, str) =>
                                {
                                    count++;
                                    if (count.Equals(missionList.Count))
                                    {
                                        readExcelFileStep1(workplaceJson);
                                    }
                                };
                                missionCtmGetClient.Get(CmsUrl.GetMissionCtmUrl());
                            }
                        }
                        else
                        {
                            readExcelFileStep1(workplaceJson);
                        }
                        //AISDAC No.81 sunyi 20181214 end

                    }
                    // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private void readMissionStructure_M(SpreadsheetGear.IWorksheet missionSheet)
        {
            SpreadsheetGear.IRange paramCells = missionSheet.Cells;

            SpreadsheetGear.IRange usedCells = missionSheet.UsedRange.Cells;

            for (int i = 0; i < usedCells.Rows.RowCount; i++)
            {
                // Mission-ID
                if (paramCells[i, 0].Value != null)
                {
                    string missionId = paramCells[i, 0].Value.ToString();
                    this.missionList.Add(missionId);
                }
            }
        }

        private void readMissionStructure_G(SpreadsheetGear.IWorksheet missionSheet)
        {
            SpreadsheetGear.IRange paramCells = missionSheet.Cells;

            SpreadsheetGear.IRange usedCells = missionSheet.UsedRange.Cells;

            for (int i = 0; i < usedCells.Rows.RowCount; i++)
            {
                // Mission-ID
                if (paramCells[i, 0].Value != null)
                {
                    string missionId = paramCells[i, 0].Value.ToString();
                    this.missionList_Grip.Add(missionId);
                }
            }
        }

        private void setElementNameWithCondition_Mission(Dictionary<string, List<ElementConditionData>> elementConditions)
        {
            this.elementConditions.Clear();
            foreach (var elementCondition in elementConditions)
            {
                foreach (var ctm in this.ctms)
                {
                    if (ctm.id.ToString().Equals(elementCondition.Key))
                    {
                        Dictionary<ElementConditionData, string> tmp = new Dictionary<ElementConditionData, string>();
                        foreach (var elementConditionData in elementCondition.Value)
                        {
                            foreach (var element in ctm.GetAllElements())
                            {
                                if (element.id.ToString() == elementConditionData.Id)
                                {
                                    tmp.Add(elementConditionData, element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                    break;
                                }
                            }
                        }
                        if (tmp.Count > 0)
                        {
                            this.elementConditions.Add(ctm.id.ToString(), tmp);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 条件式をstring形式で取得
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        private string getConditionString(Dictionary<ElementConditionData, string> condition, int pattern)
        {
            // かっこで囲む必要があるのは、条件が3つ指定してあり、かつANDとORが複合してる時
            bool useBracket = false;
            if (condition.Count == 3 &&
                6 <= pattern)
            {
                useBracket = true;
            }

            int cnt = 0;
            string conditionText = useBracket ? "(" : string.Empty;
            foreach (ElementConditionData ecd in condition.Keys)
            {
                conditionText += condition[ecd];
                conditionText += CmsDataTable.ToStringExp(ecd.Condition);
                conditionText += ecd.Value;

                if (useBracket && cnt == 1)
                {
                    conditionText += ")";
                }
                if (cnt == condition.Count - 1)
                {
                    break;
                }

                if (cnt == 0)
                {
                    conditionText += " ";
                    if (pattern == 2 ||
                        pattern == 4 ||
                        pattern == 6)
                    {
                        conditionText += "AND ";
                    }
                    else
                    {
                        conditionText += "OR ";
                    }
                }
                if (cnt == 1)
                {
                    conditionText += " ";
                    if (pattern == 4 ||
                        pattern == 7)
                    {
                        conditionText += "AND ";
                    }
                    else
                    {
                        conditionText += "OR ";
                    }
                }

                cnt++;
            }

            return conditionText;
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end

        private void readStartEndTimeParam(SpreadsheetGear.IWorksheet worksheetParam)
        {
            SpreadsheetGear.IRange cellsParam = worksheetParam.Cells;

            // 操業開始時刻
            if (cellsParam[0, 0].Value != null)
            {
                double d = double.Parse(cellsParam[0, 0].Value.ToString());
                DateTime conv = DateTime.FromOADate(d);
                dateTimePicker_Start.Value = conv;
            }

            // 操業終了時刻
            if (cellsParam[1, 0].Value != null)
            {
                double d = double.Parse(cellsParam[1, 0].Value.ToString());
                DateTime conv = DateTime.FromOADate(d);
                dateTimePicker_End.Value = conv;
            }
        }

        private void readParam(SpreadsheetGear.IWorksheet worksheetParam)
        {
            SpreadsheetGear.IRange cellsParam = worksheetParam.Cells;

            // Currently used cells
            SpreadsheetGear.IRange usedCells = worksheetParam.UsedRange.Cells;

            List<MissionViewObject> missionViewListSource = new List<MissionViewObject>();
            List<MissionCtmViewObject> ctmViewListSource = new List<MissionCtmViewObject>();
            List<MissionCtmElementViewObject> ctmElementViewListSource = new List<MissionCtmElementViewObject>();

            for (int i = 0; i < usedCells.Rows.RowCount; i++)
            {
                // Mission-ID
                if (cellsParam[i, 0].Value != null)
                {
                    string missionId = cellsParam[i, 0].Text;
                    bool success = false;
                    if (string.IsNullOrEmpty(this.missionId) || !this.missionId.Equals(missionId))
                    {
                        this.missionId = missionId;
                        success = setMission();
                    }
                    if (!success)
                    {
                        return;
                    }

                    JToken token = this.mainWindow.treeView_Mission_CTM.GetItem(missionId).DataContext as JToken;
                    Mission missionObj = JsonConvert.DeserializeObject<Mission>(token.ToString());
                    missionDict.Add(missionId, missionObj);

                    missionViewListSource.Add(new MissionViewObject()
                    {
                        MissionDisplayName = AisUtil.GetCatalogLang(missionObj),
                        MissionId = missionId,
                        Json = JsonConvert.SerializeObject(missionObj)
                    });
                }

                // CTM-ID
                if (cellsParam[i, 3].Value != null)
                {
                    string parentMissionId = cellsParam[i, 2].Text;
                    string ctmId = cellsParam[i, 3].Text;
                    string ctmName = cellsParam[i, 4].Text;

                    ctmViewListSource.Add(new MissionCtmViewObject()
                    {
                        CtmDisplayName = ctmName,
                        CtmId = ctmId,
                        ParentMissionId = parentMissionId
                    });
                }

                // Element-ID
                if (cellsParam[i, 6].Value != null)
                {
                    string parentCtmId = cellsParam[i, 5].Text;
                    string elementId = cellsParam[i, 6].Text;
                    string elementName = cellsParam[i, 7].Text;
                    string parentMissionId = cellsParam[i, 8].Text;

                    ctmElementViewListSource.Add(new MissionCtmElementViewObject()
                    {
                        ElementDisplayName = elementName,
                        ElementId = elementId,
                        ParentCtmId = parentCtmId,
                        ParentMissionId = parentMissionId
                    });
                }
            }

            // Set the ListBox ItemsSource
            //SetMissionListBoxItemsSource(null, missionViewListSource);
            //SetCtmListBoxItemsSource(null, ctmViewListSource);
            //SetElementListBoxItemsSource(null, ctmElementViewListSource);

            // Get the CTM Object, and assign it  to the related dictionary
            List<string> ctmIdList = new List<string>();
            foreach (MissionCtmViewObject ctmViewObj in ctmViewListSource)
            {
                ctmIdList.Add(ctmViewObj.CtmId);
            }

            if (ctmIdList.Count > 0)
            {
                // 個別に指定する場合は、既存の設定をサーバから再取得
                CmsHttpClient ctmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                foreach (string ctmId in ctmIdList)
                {
                    ctmGetClient.AddParam("id", ctmId);
                }

                ctmGetClient.CompleteHandler += ctmJson =>
                {
                    List<CtmObject> ctmList = JsonConvert.DeserializeObject<List<CtmObject>>(ctmJson);

                    // Assign mission CTM Dictionary
                    foreach (MissionCtmViewObject ctmViewObj in ctmViewListSource)
                    {
                        Mission missionObj = null;
                        if (missionDict.TryGetValue(ctmViewObj.ParentMissionId, out missionObj))
                        {
                            CtmObject ctmObj = ctmList.Where(x => x.id.ToString() == ctmViewObj.CtmId).FirstOrDefault();

                            List<CtmObject> ctmObjList = new List<CtmObject>();
                            if (missionCtmDict.TryGetValue(missionObj, out ctmObjList))
                            {
                                if (ctmObjList.Contains(ctmObj))
                                {
                                    //foastudio Dac検証シートNo.39 sunyi 2018/11/05 start
                                    //return;
                                    continue;
                                    //foastudio Dac検証シートNo.39 sunyi 2018/11/05 end
                                }
                                ctmObjList.Add(ctmObj);
                                missionCtmDict[missionObj] = ctmObjList;
                            }
                            else
                            {
                                missionCtmDict.Add(missionObj, new List<CtmObject>() { ctmObj });
                            }
                        }
                    }

                    // Assign mission CTM Element Dictionary
                    foreach (MissionCtmElementViewObject ctmElementViewObj in ctmElementViewListSource)
                    {
                        CtmObject ctmObj = ctmList.Where(x => x.id.ToString() == ctmElementViewObj.ParentCtmId).FirstOrDefault();
                        if (ctmObj == null) return;

                        CtmElement elementObj = ctmObj.GetElement(new GUID(ctmElementViewObj.ElementId));
                        if (elementObj != null)
                        {
                            List<CtmElement> ctmElementObjList = new List<CtmElement>();
                            if (missionCtmElementDict.TryGetValue(ctmObj, out ctmElementObjList))
                            {
                                if (ctmElementObjList.Contains(elementObj))
                                {
                                    //foastudio Dac検証シートNo.39 sunyi 2018/11/05 start
                                    //return;
                                    continue;
                                    //foastudio Dac検証シートNo.39 sunyi 2018/11/05 end
                                }

                                ctmElementObjList.Add(elementObj);
                                missionCtmElementDict[ctmObj] = ctmElementObjList;
                            }
                            else
                            {
                                missionCtmElementDict.Add(ctmObj, new List<CtmElement>() { elementObj });
                            }
                        }
                    }
                };
                ctmGetClient.Get(CmsUrlMms.Ctm.List());
            }
        }

        #endregion

        // FOA_サーバー化開発 Processing On Server Dac sunyi 2018/10/05 Start
        /// <summary>
        /// ミッションツリービューを選択された判定
        /// </summary>
        /// <returns>true,false</returns>
        private bool missionHasSelected()
        {
            foreach (TreeViewItem treeViewItem in AisTvRefDicMission.Items)
            {
                System.Windows.Media.Color color = ((SolidColorBrush)treeViewItem.Background).Color;
                if (color == System.Windows.Media.Colors.DodgerBlue)
                {
                    return true;
                }

                if (treeViewItem.IsSelected)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// チェックフラグを取得します。
        /// </summary>
        /// <param name="token"></param>
        /// <returns>true, false</returns>
        private bool getCheckedFlag(JToken token)
        {

            object ctmCheckedFlag = token["checkedFlag"];
            if (ctmCheckedFlag == null || ctmCheckedFlag.ToString() == "" || Convert.ToBoolean(ctmCheckedFlag) == true)
            {
                return true;
            }

            return false;
        }

        //private void ExcelOpen(string workfile)
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 start
        // Multi_Dac 修正
        //private void excelOpen(string workfile, JArray resultArray, Dictionary<CtmObject, List<CtmElement>> ctmElementDic, int minInterval)
        // Wang Issue DAC-72 20181225 Start
        //private void excelOpen(string workfile, JArray resultArray, Dictionary<CtmObject, List<CtmElement>> ctmElementDic, int minInterval, string workPlaceId, string onlineId)
		// Wang New dac flow 20190308 Start
        //private void excelOpen(string workfile, JArray resultArray, Dictionary<CtmObject, List<CtmElement>> ctmElementDic, int minInterval, string workPlaceId, string onlineId, DateTime? dtStart, DateTime? dtEnd)
        //Dac No.33 sun 20190726 start
        //private void excelOpen(string workfile, JArray resultArray, Dictionary<CtmObject, List<CtmElement>> ctmElementDic, int minInterval, string workPlaceId, string onlineId, DateTime? dtStart, DateTime? dtEnd, DateTime? dtStartDate)
        private void excelOpen(string workfile, JArray resultArray, Dictionary<CtmObject, List<CtmElement>> ctmElementDic, int minInterval, string workPlaceId, string onlineId, DateTime? dtStart, DateTime? dtEnd, DateTime? dtStartDate,JArray jarrayCtms)
        //Dac No.33 sun 20190726 start
            // Wang New dac flow 20190308 End
        // Wang Issue DAC-72 20181225 End
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
        {
            this.Bw = new BackgroundWorker();
            this.Bw.WorkerSupportsCancellation = true;
            string excelName = workfile;
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 start
            // Multi_Dac 修正
            //var writer = new InteropExcelWriterDac(this.Mode, this.DataSource, null, null, this.Bw, null, this.missionCtmDict, this.missionCtmElementDict);
            var writer = new InteropExcelWriterDac(this.Mode, this.DataSource, this.editControl.Ctms, null, this.Bw, this.currentSelectedMission, this.missionCtmDict, this.missionCtmElementDict, resultArray, ctmElementDic, workPlaceId, onlineId);
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
            var configParams = new Dictionary<ExcelConfigParam, object>();

            DateTime start1;
            DateTime end1;

            // Wang Issue DAC-72 20181225 Start
            //bool gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out start1, out end1);
            bool gotTime = GetStartAndEndTime(dtStart, dtEnd, out start1, out end1,true);
            // Wang Issue DAC-72 20181225 End
            if (!gotTime)
            {
                return;
            }


            string missionId = string.Empty;
            foreach (var id in this.missionDict.Keys)
            {
                missionId = id;
            }

            configParams.Add(ExcelConfigParam.START, start1);
            configParams.Add(ExcelConfigParam.END, end1);
            configParams.Add(ExcelConfigParam.MISSION_ID, missionId);
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            //configParams.Add(ExcelConfigParam.ENABLE_MULTI_DAC, LoginInfo.GetInstance().GetEnableMultiDac());
            //configParams.Add(ExcelConfigParam.ENABLE_MULTI_DAC, LoginInfo.GetInstance().GetEnableMultiDac() && this.Mode == ControlMode.MultiDac);
            configParams.Add(ExcelConfigParam.ENABLE_MULTI_DAC, LoginInfo.GetInstance().GetEnableMultiDac());
            // Wang Issue NO.687 2018/05/18 End

            if (!isFirstFile)
            {
                configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(this.excelFilePath));
            }


            //writer.WriteCtmData(excelName, null, configParams);
			// Wang New dac flow 20190308 Start
            //writer.WriteCtmDataForAll(excelName, configParams);
            //Dac No.33 sun 20190726 start
            //writer.WriteCtmDataForAll(excelName, configParams, dtStartDate.Value, dtStart.Value, dtEnd.Value);
            writer.WriteCtmDataForAll(excelName, configParams, dtStartDate.Value, dtStart.Value, dtEnd.Value, jarrayCtms);
            //Dac No.33 sun 20190726 end
			// Wang New dac flow 20190308 End
        }

        public override void InitializeDataControl()
        {
            //for (int i = 0; i < this.lbCtm.Items.Count; i++)
            //{
            //    this.lbCtm.SelectedIndex = i;
            //    removeListBoxItem(true, new object());
            //    setAllListBoxItemsSourceDefaultView();
            //}

            //for (int i = 0; i < this.lbMission.Items.Count; i++)
            //{
            //    this.lbMission.SelectedIndex = i;
            //    removeMissionListBoxItem();
            //    setAllListBoxItemsSourceDefaultView();
            //}
            //ISSUE_NO.727 sunyi 2018/06/14 Start
            //grip mission処理ができるように修正にする
            if (this.IsSelectedProgramMission)
            {
                this.MissionListBox_Grid.Visibility = Visibility.Visible;
            }
            else
            {
                // FOA_サーバー化開発 Processing On Server Dcs 2018/08/27 Start
                //for (int i = 0; i < this.lbCtm.Items.Count; i++)
                //{
                //    this.lbCtm.SelectedIndex = i;
                //    removeListBoxItem(true, new object());
                //    setAllListBoxItemsSourceDefaultView();
                //}
                //for (int i = 0; i < this.lbMission.Items.Count; i++)
                //{
                //    this.lbMission.SelectedIndex = i;
                //    removeMissionListBoxItem();
                //    setAllListBoxItemsSourceDefaultView();
                //}
                for (int i = 0; i < this.AisTvRefDicMission.Items.Count; i++)
                {
                    removeMissionListBoxItem();
                    setAllListBoxItemsSourceDefaultView();
                }
                //this.MissionListBox_Grid.Visibility = Visibility.Hidden;
                // FOA_サーバー化開発 Processing On Server Dcs 2018/08/27 End
            }
            //ISSUE_NO.727 sunyi 2018/06/14 End
        }
        private void setAllListBoxItemsSourceDefaultView()
        {
            setMissionListBoxItemsSource(this.missionDict);
            setCtmListBoxItemsSource(this.missionCtmDict);
            setElementListBoxItemsSource(this.missionCtmElementDict);
        }

        private void setMissionListBoxItemsSource(Dictionary<string, Mission> missionDict)
        {
            List<MissionViewObject> missionViewList = new List<MissionViewObject>();
            foreach (KeyValuePair<string, Mission> kvp in missionDict)
            {
                missionViewList.Add(new MissionViewObject()
                {
                    MissionDisplayName = AisUtil.GetCatalogLang(kvp.Value),
                    MissionId = kvp.Key,
                    Json = JsonConvert.SerializeObject(kvp.Value)
                });
            }
            //lbMission.ItemsSource = missionViewList;
            //lbMission.DisplayMemberPath = "MissionDisplayName";
        }

        private void setCtmListBoxItemsSource(Dictionary<Mission, List<CtmObject>> missionCtmDict = null, List<MissionCtmViewObject> ctmViewListSource = null)
        {
            if (ctmViewListSource != null)
            {
                //lbCtm.ItemsSource = ctmViewListSource;
                //lbCtm.DisplayMemberPath = "CtmDisplayName";
                return;
            }

            List<MissionCtmViewObject> ctmViewList = new List<MissionCtmViewObject>();
            foreach (KeyValuePair<Mission, List<CtmObject>> kvp in missionCtmDict)
            {
                foreach (CtmObject ctm in kvp.Value)
                {
                    ctmViewList.Add(new MissionCtmViewObject()
                    {
                        CtmDisplayName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                        CtmId = ctm.id.ToString(),
                        ParentMissionId = kvp.Key.Id
                    });
                }
            }
            //lbCtm.ItemsSource = ctmViewList;
            //lbCtm.DisplayMemberPath = "CtmDisplayName";
        }
        private void setCtmListBoxItemsSource(Dictionary<Mission, List<CtmObject>> missionCtmDict)
        {
            List<MissionCtmViewObject> ctmViewList = new List<MissionCtmViewObject>();
            foreach (KeyValuePair<Mission, List<CtmObject>> kvp in missionCtmDict)
            {
                foreach (CtmObject ctm in kvp.Value)
                {
                    ctmViewList.Add(new MissionCtmViewObject()
                    {
                        CtmDisplayName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                        CtmId = ctm.id.ToString(),
                        ParentMissionId = kvp.Key.Id
                    });
                }
            }
            //lbCtm.ItemsSource = ctmViewList;
            //lbCtm.DisplayMemberPath = "CtmDisplayName";
        }
        private void setCtmListBoxItemsSource(List<CtmObject> listCtmObj, string parentMissionId)
        {
            List<MissionCtmViewObject> ctmViewList = new List<MissionCtmViewObject>();
            foreach (CtmObject ctm in listCtmObj)
            {
                ctmViewList.Add(new MissionCtmViewObject()
                {
                    CtmDisplayName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                    CtmId = ctm.id.ToString(),
                    ParentMissionId = parentMissionId
                });
            }
            //lbCtm.ItemsSource = ctmViewList;
            //lbCtm.DisplayMemberPath = "CtmDisplayName";
        }

        private void setElementListBoxItemsSource(Dictionary<CtmObject, List<CtmElement>> missionCtmElementDict = null, List<MissionCtmElementViewObject> ctmElementViewListSource = null)
        {
            if (ctmElementViewListSource != null)
            {
                //lbElement.ItemsSource = ctmElementViewListSource;
                //lbElement.DisplayMemberPath = "ElementDisplayName";
                return;
            }

            List<MissionCtmElementViewObject> ctmElementViewList = new List<MissionCtmElementViewObject>();
            foreach (KeyValuePair<CtmObject, List<CtmElement>> kvp in missionCtmElementDict)
            {
                string ctmId = kvp.Key.id.ToString();
                foreach (CtmElement ele in kvp.Value)
                {
                    // Get ParentMissionId
                    string parentMissionId = null;
                    foreach (KeyValuePair<Mission, List<CtmObject>> kvp2 in missionCtmDict)
                    {
                        bool isParentMissionFound = false;
                        foreach (CtmObject ctm in kvp2.Value)
                        {
                            if (ctm.id.ToString() == ctmId)
                            {
                                parentMissionId = kvp2.Key.Id;
                                isParentMissionFound = true;
                                break;
                            }
                        }
                        if (isParentMissionFound) break;
                    }

                    ctmElementViewList.Add(new MissionCtmElementViewObject()
                    {
                        ElementDisplayName = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                        ElementId = ele.id.ToString(),
                        ParentCtmId = ctmId,
                        ParentMissionId = parentMissionId
                    });
                }
            }
            //lbElement.ItemsSource = ctmElementViewList;
            //lbElement.DisplayMemberPath = "ElementDisplayName";
        }

        private void setElementListBoxItemsSource(List<CtmElement> missionCtmElementList, string parentCtmId, string parentMissionId)
        {
            List<MissionCtmElementViewObject> ctmElementViewList = new List<MissionCtmElementViewObject>();
            foreach (CtmElement ele in missionCtmElementList)
            {
                ctmElementViewList.Add(new MissionCtmElementViewObject()
                {
                    ElementDisplayName = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true),
                    ElementId = ele.id.ToString(),
                    ParentCtmId = parentCtmId,
                    ParentMissionId = parentMissionId
                });
            }
            //lbElement.ItemsSource = ctmElementViewList;
            //lbElement.DisplayMemberPath = "ElementDisplayName";
        }

        private string[] getItemArrayFromDropEvent(object sender, DragEventArgs e)
        {
            var senderObj = sender as System.Windows.Controls.ListBox;
            if (senderObj == null)
            {
                return null;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return null;
            }

            string items = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = items.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 5)
            {
                return null;
            }

            /*
             * 0 CTM名
             * 1 CTMID
             * ----------------
             * 2 エレメント名
             * 3 エレメントID
             * 4 エレメントの型
             * ...
             * ...
             * ...
            */

            return itemArray;
        }

        private void removeMissionListBoxItem()
        {
            //MissionViewObject missionViewObj = lbMission.Items[lbMission.SelectedIndex] as MissionViewObject;
            MissionViewObject missionViewObj = null;
            if (missionViewObj == null)
            {
                return;
            }

            // 1. Get the to be removed Mission object
            Mission toBeRemovedMission = missionDict[missionViewObj.MissionId];

            // 2. Get the current selected CtmObject for that mission
            List<CtmObject> listCtmObj = missionCtmDict[toBeRemovedMission];

            // 3. If the ctmObject is exist, continue removing the related element
            foreach (CtmObject ctm in listCtmObj)
            {
                // 4. Remove related element from the Dictionary
                missionCtmElementDict.Remove(ctm);
            }

            // 5. Remove the Ctm from Dictionary
            missionCtmDict.Remove(toBeRemovedMission);

            // 6. Remove the mission from dictionary
            missionDict.Remove(missionViewObj.MissionId);

            // 7. Re-assign the Element ListBox ItemsSource
            setElementListBoxItemsSource(missionCtmElementDict);

            // 8. Re-assign the CTM ListBox ItemsSource
            setCtmListBoxItemsSource(missionCtmDict);

            // 9. Re-assign the Mission ListBox ItemsSource
            List<MissionViewObject> missionViewList = new List<MissionViewObject>();
            foreach (KeyValuePair<string, Mission> kvp in missionDict)
            {
                missionViewList.Add(new MissionViewObject()
                {
                    MissionDisplayName = AisUtil.GetCatalogLang(kvp.Value),
                    MissionId = kvp.Key,
                    Json = JsonConvert.SerializeObject(kvp.Value)
                });
            }

            //lbMission.ItemsSource = missionViewList;
            //lbMission.DisplayMemberPath = "MissionDisplayName";
        }



        private CtmObject GetCtmObject(string ctmId)
        {
            CtmObject ctmObj = null;
            if (this.editControl.Ctms != null)
            {
                this.ctms = this.editControl.Ctms;
            }

            foreach (CtmObject ctm in ctms)
            {
                if (ctm.id.ToString() == ctmId)
                {
                    ctmObj = ctm;
                    break;
                }
            }

            return ctmObj;
        }

        private List<string[]> convertItemArrayToElementData(string[] itemArray)
        {
            List<string[]> data = new List<string[]>();

            for (int i = 2; i < itemArray.Length; i += 3)
            {
                string[] ary = new string[]
                {
                    itemArray[i],
                    itemArray[i + 1],
                    itemArray[i + 2]
                };
                data.Add(ary);
            }

            return data;
        }

        private class MissionViewObject
        {
            public string MissionDisplayName { get; set; }
            public string MissionId { get; set; }
            public string Json { get; set; }
        }
        private class MissionCtmViewObject
        {
            public string CtmDisplayName { get; set; }
            public string CtmId { get; set; }
            public string ParentMissionId { get; set; }
        }
        private class MissionCtmElementViewObject
        {
            public string ElementDisplayName { get; set; }
            public string ElementId { get; set; }
            public string ParentCtmId { get; set; }
            public string ParentMissionId { get; set; }
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(UserControl_DACTemplate));

        private void AisTvRefDicElement_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {

        }

        /// <summary>
        /// ミッションツリーをクリックした時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AisTvRefDicMission_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            // CTM・Routesを表示・非表示
            foreach (TreeViewItem treeItem in this.AisTvRefDicCtm.Items)
            {
                treeItem.IsSelected = false;
                treeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Black);
                treeItem.Background = new SolidColorBrush(System.Windows.Media.Colors.White);

                treeItem.Visibility = System.Windows.Visibility.Visible;
            }

            // Elementsを表示・非表示
            foreach (TreeViewItem treeItem in this.AisTvRefDicElement.Items)
            {
                treeItem.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void AisTvRefDicCtm_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            // FOA_サーバー化開発 Processing On Server Xj 2018/08/29 Start
            //今選択するのミッション
            JToken currentToken = this.AisTvRefDicCtm.GetSelectedToken();
            if (currentToken == null) return;

            // 色を設定
            foreach (TreeViewItem treeItem in AisTvRefDicCtm.Items)
            {
                treeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Black);
                treeItem.Background = new SolidColorBrush(System.Windows.Media.Colors.White);
            }

            // 選択された行の色を設定
            TreeViewItem thisTreeItem = this.AisTvRefDicCtm.GetItem(this.AisTvRefDicCtm.SelectedId);
            thisTreeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.White);
            thisTreeItem.Background = new SolidColorBrush(System.Windows.Media.Colors.DodgerBlue);

            // 色を設定
            foreach (TreeViewItem treeItem in AisTvRefDicMission.Items)
            {
                treeItem.IsSelected = false;
                treeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Black);
                treeItem.Background = new SolidColorBrush(System.Windows.Media.Colors.White);

                JToken jToken = treeItem.DataContext as JToken;
                if ((string)jToken["id"] == (string)currentToken["missionId"])
                {
                    setCurrentConditions(jToken);
                    treeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.White);
                    treeItem.Background = new SolidColorBrush(System.Windows.Media.Colors.DodgerBlue);
                }
            }

            //画面のElement項目を選択内容をクッリン
            foreach (TreeViewItem elementTreeViewItem in AisTvRefDicElement.Items)
            {
                JToken elementItemToken = elementTreeViewItem.DataContext as JToken;
                //CTMの場合
                if (getMissionType(currentToken) == 1) // C-Mission
                {
                    if (currentToken[MmsTvJsonKey.Id].ToString() == elementItemToken[MmsTvJsonKey.Id].ToString()
                        && getMissionType(currentToken) == getMissionType(elementItemToken))
                    {
                        elementTreeViewItem.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        elementTreeViewItem.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }
                //Gripの場合
                else if (getMissionType(currentToken) == 0) // G-Mission
                {
                    string[] elementIdArray = Regex.Split(elementItemToken[MmsTvJsonKey.Id].ToString(), "-", RegexOptions.IgnoreCase);
                    string ctmIdOfElement = elementIdArray[1];

                    string[] ctmNodeArray = Regex.Split(currentToken["nodes"].ToString(), ",", RegexOptions.IgnoreCase);

                    if (ctmNodeArray.Contains(ctmIdOfElement) && currentToken["missionId"].ToString() == elementItemToken["missionId"].ToString()
                        && getMissionType(currentToken) == getMissionType(elementItemToken))
                    {
                        elementTreeViewItem.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        elementTreeViewItem.Visibility = System.Windows.Visibility.Collapsed;
                    }
                }
            }

            //if (lbCtm.SelectedIndex == -1)
            //{
            //    if (lbMission.SelectedIndex == -1)
            //    {
            //        setAllListBoxItemsSourceDefaultView();
            //    }
            //    else // A mission is currently selected in the MissionListBox
            //    {
            //        MissionViewObject missionViewObj = lbMission.Items[lbMission.SelectedIndex] as MissionViewObject;
            //        if (missionViewObj == null)
            //        {
            //            return;
            //        }

            //        List<CtmObject> listSelectedCtm = missionCtmDict.Where(x => x.Key.Id == missionViewObj.MissionId).FirstOrDefault().Value;
            //        setCtmListBoxItemsSource(listSelectedCtm, missionViewObj.MissionId);

            //        Dictionary<CtmObject, List<CtmElement>> selectedCtmElementDict = new Dictionary<CtmObject, List<CtmElement>>();
            //        foreach (CtmObject ctm in listSelectedCtm)
            //        {
            //            List<CtmElement> listSelectedElement = new List<CtmElement>();
            //            foreach (CtmElement ele in missionCtmElementDict.Where(x => x.Key.id.ToString() == ctm.id.ToString()).FirstOrDefault().Value)
            //            {
            //                if (!listSelectedElement.Contains(ele))
            //                {
            //                    listSelectedElement.Add(ele);
            //                }
            //            }
            //            selectedCtmElementDict[ctm] = listSelectedElement;
            //        }
            //        setElementListBoxItemsSource(selectedCtmElementDict);
            //    }

            //    return;
            //}

            //MissionCtmViewObject missionCtmViewObj = lbCtm.Items[lbCtm.SelectedIndex] as MissionCtmViewObject;
            //if (missionCtmViewObj == null)
            //{
            //    return;
            //}

            //List<CtmElement> selectedCtmElementList = missionCtmElementDict.Where(x => x.Key.id.ToString() == missionCtmViewObj.CtmId).FirstOrDefault().Value;
            //if (selectedCtmElementList != null)
            //{
            //    setElementListBoxItemsSource(selectedCtmElementList, missionCtmViewObj.CtmId, missionCtmViewObj.ParentMissionId);
            //}
            //else
            //{
            //    this.lbCtm.SelectedIndex = -1;
            //}
            // FOA_サーバー化開発 Processing On Server Xj 2018/08/29 End
        }

        // FOA_サーバー化開発 Processing On Server Xj 2018/08/27 Start
        /// <summary>
        /// ミッションツリーを選択した時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// TODO: iwai workplaceの３列の左側の部分のイベントハンドラー。中央は、 AisTvRefDicCtm_Sele... AisTvRefDicElement_Selec... となる。ルートとなる場合もCTMとなるので注意
        public void AisTvRefDicMission_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            //今選択するのミッション
            JToken currentToken = AisTvRefDicMission.GetSelectedToken();

            List<string> tmpList = new List<string>();

            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
            if (currentToken == null)
            {
                if (this.AisTvRefDicMission.Items.Count == 1 && this.AisTvRefDicCtm.Items.Count > 0)
                {
                    foreach (TreeViewItem treeItem in this.AisTvRefDicCtm.Items)
                    {
                        JToken itemToken = treeItem.DataContext as JToken;

                        if (itemToken["id"].ToString().Contains(this.AisTvRefDicMission.itemList[0].Key))
                        {
                            tmpList.Add(itemToken["id"].ToString());
                        }
                    }
                    this.AisTvRefDicCtm.DeleteMissionCtmElement(tmpList);
                    tmpList.Clear();
                    foreach (TreeViewItem treeItem in this.AisTvRefDicElement.Items)
                    {
                        JToken itemToken = treeItem.DataContext as JToken;
                        if (itemToken["id"].ToString().Contains(this.AisTvRefDicMission.itemList[0].Key))
                        {
                            tmpList.Add(itemToken["id"].ToString());
                        }
                    }
                    this.AisTvRefDicElement.DeleteMissionCtmElement(tmpList);
                    tmpList.Clear();
                }
                tmpList.Add(this.AisTvRefDicMission.itemList[0].Key);
                this.AisTvRefDicMission.DeleteMissionCtmElement(tmpList);
                tmpList.Clear();
                this.AisTvRefDicMission.IsDeleteFlag = false;
            }
            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
            if (currentToken == null) return;

            if (!string.IsNullOrEmpty(preSelectedMissionId))
            {
                TreeView treeView = (TreeView)sender;
                foreach (TreeViewItem treeViewItem in treeView.Items)
                {
                    JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                    //前回選択したミッションに検索条件を設定
                    if (preSelectedMissionId == (string)tempChildItemToken[MmsTvJsonKey.Id])
                    {
                        setSearchConditions(tempChildItemToken);
                    }
                }

                //今選択するのミッション
                setCurrentConditions(currentToken);
            }
            else
            {
                //今選択するのミッション
                setCurrentConditions(currentToken);
            }
            // 前回選択したミッションID
            preSelectedMissionId = AisTvRefDicMission.SelectedId;

            // 色を設定

            foreach (TreeViewItem treeItem in AisTvRefDicMission.Items)
            {
                treeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Black);
                treeItem.Background = new SolidColorBrush(System.Windows.Media.Colors.White);
                JToken itemToken = treeItem.DataContext as JToken;
                string itemSearchType = (string)itemToken["aisSearchType"];
                if (!string.IsNullOrEmpty(itemSearchType))
                {
                    if (int.Parse(itemSearchType) == 1)
                    {
                        Style style = new Style();
                        // 枠色の設定
                        style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(System.Windows.Media.Colors.Red)));
                        style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(2)));
                        // スタイルセット
                        treeItem.Style = style;
                    }
                    else if (int.Parse(itemSearchType) == 2)
                    {
                        Style style = new Style();
                        // 枠色の設定
                        style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(System.Windows.Media.Colors.White)));
                        style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(0)));
                        // スタイルセット
                        treeItem.Style = style;
                    }
                }
            }

            // 選択された行の色を設定
            TreeViewItem thisTreeItem = this.AisTvRefDicMission.GetItem(this.AisTvRefDicMission.SelectedId);

            thisTreeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.White);

            thisTreeItem.Background = new SolidColorBrush(System.Windows.Media.Colors.DodgerBlue);
            // Processing On Server 検証＃6　dn 2018/10/05 start
            string curSearchType = (string)currentToken["aisSearchType"];
            if (!string.IsNullOrEmpty(curSearchType))
            {
                // 検索条件設定済みの場合、設定の検索タイプで枠色を設定する
                if (int.Parse(curSearchType) == 1)
                {
                    Style style = new Style();
                    // 枠色の設定
                    style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(System.Windows.Media.Colors.Red)));
                    style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(2)));
                    // スタイルセット
                    thisTreeItem.Style = style;
                }
                else if (int.Parse(curSearchType) == 2)
                {
                    Style style = new Style();
                    // 枠色の設定
                    style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(System.Windows.Media.Colors.White)));
                    style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(0)));
                    // スタイルセット
                    thisTreeItem.Style = style;
                }

                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 Start
                //選択しない場合、黒にセット
                thisTreeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Black);
                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 End
            }
            else
            {
                //// 検索条件が未設定の場合、画面の検索タイプで枠色を設定する
                //if (this.radioButton_1.IsChecked == true)
                //{
                //    Style style = new Style();
                //    // 枠色の設定
                //    style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(Colors.Red)));
                //    style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(2)));
                //    // スタイルセット
                //    thisTreeItem.Style = style;
                //}
                //else if (this.radioButton_2.IsChecked == true)
                //{
                //    Style style = new Style();
                //    // 枠色の設定
                //    style.Setters.Add(new Setter(TreeViewItem.BorderBrushProperty, new SolidColorBrush(Colors.White)));
                //    style.Setters.Add(new Setter(TreeViewItem.BorderThicknessProperty, new Thickness(0)));
                //    // スタイルセット
                //    thisTreeItem.Style = style;
                //}

                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 Start
                //選択しない場合、黒にセット
                thisTreeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Black);
                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 End
            }
            // Processing On Server 検証＃6　dn 2018/10/05 end

            // CTM・Routesを表示・非表示
            bool isFirstCtm1 = false;
            foreach (TreeViewItem treeItem in this.AisTvRefDicCtm.Items)
            {
                treeItem.IsSelected = false;
                JToken itemToken = treeItem.DataContext as JToken;
                if (AisTvRefDicMission.SelectedId == (string)itemToken["missionId"])
                {
                    if (!isFirstCtm1)
                    {
                        isFirstCtm1 = true;

                        treeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.White);
                        treeItem.Background = new SolidColorBrush(System.Windows.Media.Colors.DodgerBlue);

                    }
                    treeItem.Visibility = System.Windows.Visibility.Visible;
                }
                else
                {
                    treeItem.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Black);
                    treeItem.Background = new SolidColorBrush(System.Windows.Media.Colors.White);

                    treeItem.Visibility = System.Windows.Visibility.Collapsed;
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                    tmpList.Add(itemToken["id"].ToString());
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                }
            }

            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
            if (this.AisTvRefDicMission.IsDeleteFlag)
            {
                this.AisTvRefDicCtm.DeleteMissionCtmElement(tmpList);
                tmpList.Clear();
            }
            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end

            // Elementsを表示・非表示
            if (getMissionType(currentToken) == 1)
            {
                // CTM Mission
                bool isFirstCtm = false;
                foreach (TreeViewItem treeItem in this.AisTvRefDicElement.Items)
                {
                    JToken itemToken = treeItem.DataContext as JToken;
                    if (AisTvRefDicMission.SelectedId == (string)itemToken["missionId"] && getMissionType(itemToken) == getMissionType(currentToken))
                    {
                        if (!isFirstCtm)
                        {
                            treeItem.Visibility = System.Windows.Visibility.Visible;
                            isFirstCtm = true;
                        }
                        else
                        {
                            treeItem.Visibility = System.Windows.Visibility.Collapsed;
                        }
                    }
                    else
                    {
                        treeItem.Visibility = System.Windows.Visibility.Collapsed;
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                        tmpList.Add(itemToken["id"].ToString());
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                    }
                }
            }
            else //Grip Mission
            {
                foreach (TreeViewItem ctmTreeItem in this.AisTvRefDicCtm.Items)
                {
                    JToken ctmItemToken = ctmTreeItem.DataContext as JToken;
                    if (AisTvRefDicMission.SelectedId == (string)ctmItemToken["missionId"])
                    {
                        string[] ctmNodeArray = Regex.Split(ctmItemToken["nodes"].ToString(), ",", RegexOptions.IgnoreCase);
                        foreach (TreeViewItem treeItem in this.AisTvRefDicElement.Items)
                        {
                            JToken itemToken = treeItem.DataContext as JToken;

                            string[] elementIdArray = Regex.Split(itemToken[MmsTvJsonKey.Id].ToString(), "-", RegexOptions.IgnoreCase);
                            string ctmIdOfElement = elementIdArray[1];

                            if (ctmNodeArray.Contains(ctmIdOfElement)
                                && AisTvRefDicMission.SelectedId == (string)itemToken["missionId"]
                                && currentToken["missionNewType"].ToString() == itemToken["missionNewType"].ToString())
                            {
                                treeItem.Visibility = System.Windows.Visibility.Visible;
                            }
                            else
                            {
                                treeItem.Visibility = System.Windows.Visibility.Collapsed;
                                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                                tmpList.Add(itemToken["id"].ToString());
                                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                            }
                        }
                        break;
                    }
                }
            }
            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
            if (this.AisTvRefDicMission.IsDeleteFlag)
            {
                this.AisTvRefDicElement.DeleteMissionCtmElement(tmpList);
                tmpList.Clear();
            }
            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
        }

        /// <summary>
        /// 指定ミッションの検索条件を設定
        /// </summary>
        /// <param name="JToken">missionToken</param>
        private void setSearchConditions(JToken missionToken)
        {
            int searchType = 2;
            missionToken["aisSearchType"] = searchType;
            missionToken["offlineStart"] = getMillisecondsByDateTime(this.dateTimePicker_Start.Value);
            missionToken["offlineEnd"] = getMillisecondsByDateTime(this.dateTimePicker_End.Value);
        }

        /// <summary>
        /// Convert DateTime to long
        /// </summary>
        /// <param name="DateTime?">value</param>
        private long getMillisecondsByDateTime(DateTime? value)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long timeStamp = (long)((DateTime)value - startTime).TotalMilliseconds;
            return timeStamp;
        }

        /// <summary>
        /// 選択されたミッションの検索条件を設定
        /// </summary>
        private void setCurrentConditions(JToken currentToken)
        {
            if (currentToken == null) return;
            if (currentToken["offlineStart"] != null)
            {
                long startTime = currentToken["offlineStart"].Value<long>();
                dateTimePicker_Start.Value = getDateTimeByMilliseconds(startTime);

            }
            if (currentToken["offlineEnd"] != null)
            {
                long endTime = currentToken["offlineEnd"].Value<long>();
                dateTimePicker_End.Value = getDateTimeByMilliseconds(endTime);
            }

        }

        /// <summary>
        /// Convert lone to DateTime
        /// </summary>
        /// <param name="long">value</param>
        private DateTime getDateTimeByMilliseconds(long value)
        {
            DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            DateTime dt = startTime.AddMilliseconds(value);
            return dt;
        }

        /// <summary>
        /// ミッションタイプを取得します。
        /// </summary>
        /// <param name="token"></param>
        /// <returns>0:Grip Mision, 1:Ctm Mission</returns>
        private int getMissionType(JToken token)
        {

            // Grip
            int iType = 0;

            string missionType = (string)token["missionNewType"];
            if (!string.IsNullOrEmpty(missionType))
            {
                iType = int.Parse(missionType);
            }
            return iType;
        }

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 Start
        private string getPeriodTextByValue(string value)
        {
            // 取得期間
            foreach (var pair in this.getPeriodItems)
            {
                if (pair.Value == value)
                {
                    return pair.Key;
                }
            }
            return string.Empty;
        }
        private string getIntervalTextByValue(string value)
        {
            // 更新周期
            foreach (var pair in this.intervalItems)
            {
                if (pair.Value == value)
                {
                    return pair.Key;
                }
            }
            return string.Empty;
        }
        private string getPeriodValue(string text)
        {
            // 取得期間
            foreach (var pair in this.getPeriodItems)
            {
                if (pair.Key == text)
                {
                    return pair.Value;
                }
            }
            return string.Empty;
        }
        private string getIntervalValue(string text)
        {
            // 更新周期
            foreach (var pair in this.intervalItems)
            {
                if (pair.Key == text)
                {
                    return pair.Value;
                }
            }
            return string.Empty;
        }
        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 End

        /// <summary>
        /// 更新周期
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        private List<KeyValuePair<string, string>> intervalItems = new List<KeyValuePair<string, string>>();

        /// <summary>
        /// 取得期間
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        private List<KeyValuePair<string, string>> getPeriodItems = new List<KeyValuePair<string, string>>();

        private void setKeyValuePair()
        {
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 Start
            //// 更新周期
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("5秒", new TimeSpan(0, 0, 5)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("10秒", new TimeSpan(0, 0, 10)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("30秒", new TimeSpan(0, 0, 30)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("1分", new TimeSpan(0, 1, 0)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("10分", new TimeSpan(0, 10, 0)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("1時間", new TimeSpan(1, 0, 0)));
            //this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("4時間", new TimeSpan(4, 0, 0)));

            //// 取得期間
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("1時間前", new TimeSpan(1, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("3時間前", new TimeSpan(3, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("6時間前", new TimeSpan(6, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("12時間前", new TimeSpan(12, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("今日", new TimeSpan(24, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("前日", new TimeSpan(48, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("前々日", new TimeSpan(72, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("今週", new TimeSpan(168, 0, 0)));
            //this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("先週", new TimeSpan(336, 0, 0)));

            // 更新周期
            this.intervalItems.Add(new KeyValuePair<string, string>("5秒", "5"));
            this.intervalItems.Add(new KeyValuePair<string, string>("10秒", "10"));
            this.intervalItems.Add(new KeyValuePair<string, string>("30秒", "30"));
            this.intervalItems.Add(new KeyValuePair<string, string>("1分", "60"));
            this.intervalItems.Add(new KeyValuePair<string, string>("10分", "600"));
            this.intervalItems.Add(new KeyValuePair<string, string>("1時間", "3600"));
            this.intervalItems.Add(new KeyValuePair<string, string>("4時間", "14400"));

            // 取得期間
            this.getPeriodItems.Add(new KeyValuePair<string, string>("1時間前", "1"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("3時間前", "3"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("6時間前", "6"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("12時間前", "12"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("今日", "24"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("前日", "48"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("前々日", "72"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("今週", "168"));
            this.getPeriodItems.Add(new KeyValuePair<string, string>("先週", "336"));
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 End
        }

        private void setComboBoxItem()
        {
        }

        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 Start
        // Multi_Dac 修正
        /// <summary>
        /// ミッションツリーにチェックボックスをチェックする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AisTvRefDicElementCheckChanged(object sender, System.EventArgs e)
        {
            commonCheck(sender, e);
        }

        /// <summary>
        /// 「CTM/Routes」ツリーにチェックボックスをチェックする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AisTvRefDicCtmCheckChanged(object sender, System.EventArgs e)
        {
            commonCheck(sender, e);
        }

        /// <summary>
        /// エレメントツリーにチェックボックスをチェックする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void AisTvRefDicMissionCheckChanged(object sender, System.EventArgs e)
        {
            commonCheck(sender, e);
        }

        /// <summary>
        /// ツリーアイテムからチェックボックスを取得します。
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        private CheckBox GetCheckBox(TreeViewItem item)
        {
            StackPanel st = (StackPanel)item.Header;
            UIElementCollection uiCol = st.Children;
            foreach (UIElement ui in uiCol)
            {
                if (ui is CheckBox)
                {
                    return (CheckBox)ui;
                }
            }
            return null;
        }

        /// <summary>
        /// ミッションチェックボックスを設定
        /// </summary>
        /// <param name="string">id</param>
        /// <param name="bool">isChecked</param>
        private void checkedMission(string id, bool isChecked)
        {
            if (string.IsNullOrEmpty(id))
            {
                return;
            }
            TreeViewItem treeViewItem = this.AisTvRefDicMission.GetTreeViewItem(id);
            CheckBox tempCheckBox = GetCheckBox(treeViewItem);
            if (tempCheckBox != null)
            {
                if (isChecked == true)
                {
                    tempCheckBox.IsChecked = true;
                }
                else
                {
                    tempCheckBox.IsChecked = false;
                }
                setCheckedFlag(treeViewItem, isChecked);
            }
        }
        /// <summary>
        /// checkedFlagを設定
        /// </summary>
        /// <param name="TreeViewItem">treeViewItem</param>
        /// <param name="object">isChecked</param>
        private void setCheckedFlag(TreeViewItem treeViewItem, object isChecked)
        {
            JToken currentToken = treeViewItem.DataContext as JToken;
            if (isChecked == null)
            {
                currentToken["checkedFlag"] = null;
            }
            else
            {
                currentToken["checkedFlag"] = (bool)isChecked;
            }
        }

        /// <summary>
        /// ctmIdにより、「Ctm/Routes」ツリーにチェックボックスを設定
        /// </summary>
        /// <param name="string">id</param>
        /// <param name="CheckBox">checkBox</param>
        private void checkedCtm(string id, CheckBox checkBox)
        {
            if (string.IsNullOrEmpty(id))
            {
                return;
            }
            TreeViewItem treeViewItem = this.AisTvRefDicCtm.GetTreeViewItem(id);
            CheckBox tempCheckBox = GetCheckBox(treeViewItem);
            if (tempCheckBox != null)
            {
                if (checkBox.IsChecked == null || checkBox.IsChecked == true)
                {
                    tempCheckBox.IsChecked = true;
                    setCheckedFlag(treeViewItem, true);
                }
                else
                {
                    tempCheckBox.IsChecked = false;
                    setCheckedFlag(treeViewItem, false);
                }
            }
        }
        /// <summary>
        /// ctmIdにより、エレメントツリーにチェックボックスを設定
        /// </summary>
        /// <param name="string">ctmId</param>
        /// <param name="CheckBox">checkBox</param>
        private void checkedElementsByCtmId(string ctmId, CheckBox checkBox)
        {
            if (string.IsNullOrEmpty(ctmId))
            {
                return;
            }
            bool isCheck = false;
            if (checkBox.IsChecked == null || checkBox.IsChecked == true)
            {
                isCheck = true;
            }
            else
            {
                isCheck = false;
            }
            this.AisTvRefDicElement.SetCheckState(ctmId, isCheck);
        }

        /// <summary>
        /// （共通）ツリーにチェックボックスをチェックするの処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commonCheck(object sender, System.EventArgs e)
        {
            TreeView treeView = (TreeView)sender;

            EventArgsEx ex = (EventArgsEx)e;

            string missionId = string.Empty;
            bool isMissionCheck = false;

            if (ex.MissionType == 1)
            {
                // CTM場合
                if (treeView.Name == "AisTvRefDicElement")
                {
                    string checkedMissionId = ex.Key.Split('-')[0];

                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 0)
                        {
                            continue;
                        }

                        missionId = (string)tempChildItemToken["missionId"];
                        if (checkedMissionId == missionId)
                        {
                            if (tempCheckBox != null)
                            {
                                if (isMissionCheck == false)
                                {
                                    if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                    {
                                        isMissionCheck = true;
                                    }
                                }

                                checkedCtm((string)tempChildItemToken["id"], tempCheckBox);
                            }
                        }
                    }

                    checkedMission(checkedMissionId, isMissionCheck);

                }
                else if (treeView.Name == "AisTvRefDicCtm")
                {
                    string checkedMissionId = ex.Key.Split('-')[0];

                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 0)
                        {
                            continue;
                        }
                        missionId = (string)tempChildItemToken["missionId"];
                        if (checkedMissionId == missionId)
                        {
                            if (tempCheckBox != null)
                            {
                                if (isMissionCheck == false)
                                {
                                    if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                    {
                                        isMissionCheck = true;
                                    }
                                }
                                if (ex.Key == (string)tempChildItemToken["id"])
                                {
                                    checkedElementsByCtmId((string)tempChildItemToken["id"], tempCheckBox);
                                }
                            }
                        }
                    }

                    checkedMission(checkedMissionId, isMissionCheck);

                }
                else if (treeView.Name == "AisTvRefDicMission")
                {
                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 0)
                        {
                            continue;
                        }
                        if (tempCheckBox != null && ex.Key == (string)tempChildItemToken["id"])
                        {
                            foreach (TreeViewItem ctmItem in this.AisTvRefDicCtm.Items)
                            {
                                CheckBox ctmCheckBox = GetCheckBox(ctmItem);
                                JToken ctmChildItemToken = ctmItem.DataContext as JToken;
                                if (ex.Key == (string)ctmChildItemToken["missionId"])
                                {
                                    if (ctmCheckBox != null)
                                    {
                                        if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                        {
                                            ctmCheckBox.IsChecked = true;
                                            setCheckedFlag(ctmItem, true);
                                        }
                                        else
                                        {
                                            ctmCheckBox.IsChecked = false;
                                            setCheckedFlag(ctmItem, false);
                                        }

                                        checkedElementsByCtmId((string)ctmChildItemToken["id"], tempCheckBox);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else if (ex.MissionType == 0)
            {
                // Grip場合
                if (treeView.Name == "AisTvRefDicCtm")
                {
                    string checkedMissionId = ex.Key.Split('_')[0];

                    // チェックするのCtmsデータ
                    Dictionary<string, string> checkedDic = new Dictionary<string, string>();
                    // アンチェックするのCtmsデータ
                    Dictionary<string, string> unCheckedDic = new Dictionary<string, string>();

                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 1)
                        {
                            continue;
                        }

                        if (checkedMissionId == (string)tempChildItemToken["missionId"])
                        {
                            if (tempCheckBox != null)
                            {
                                string[] nodesArray = ((string)tempChildItemToken["nodes"]).Split(',');
                                if (nodesArray.Length > 0)
                                {
                                    if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                    {
                                        // チェックするのCtmsデータを作成
                                        foreach (string sttringCtmId in nodesArray)
                                        {
                                            if (checkedDic.ContainsKey(sttringCtmId))
                                            {
                                                continue;
                                            }
                                            checkedDic.Add(sttringCtmId, sttringCtmId);
                                        }
                                    }
                                    else
                                    {
                                        // アンチェックするのCtmsデータを作成
                                        foreach (string sttringCtmId in nodesArray)
                                        {
                                            if (unCheckedDic.ContainsKey(sttringCtmId))
                                            {
                                                continue;
                                            }
                                            unCheckedDic.Add(sttringCtmId, sttringCtmId);
                                        }
                                    }
                                }

                                if (isMissionCheck == false)
                                {
                                    if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                    {
                                        isMissionCheck = true;
                                    }
                                }

                            }
                        }
                    }
                    // Missionチェック処理
                    checkedMission(checkedMissionId, isMissionCheck);

                    // Elementsチェック処理
                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 1)
                        {
                            continue;
                        }

                        missionId = (string)tempChildItemToken["missionId"];
                        if (checkedMissionId == missionId)
                        {
                            if (tempCheckBox != null)
                            {
                                string[] nodesArray = ((string)tempChildItemToken["nodes"]).Split(',');
                                if (nodesArray.Length == 0) continue;
                                foreach (string sttringCtmId in nodesArray)
                                {
                                    if (ex.thisChecked)
                                    {
                                        this.AisTvRefDicElement.SetCheckState(missionId + "-" + sttringCtmId, false);
                                    }
                                    else
                                    {
                                        this.AisTvRefDicElement.SetCheckState(missionId + "-" + sttringCtmId, true);
                                    }
                                }

                                if (ex.thisChecked)
                                {
                                    foreach (KeyValuePair<string, string> item in checkedDic)
                                    {
                                        this.AisTvRefDicElement.SetCheckState(missionId + "-" + item.Key, true);
                                    }
                                }
                                else
                                {
                                    foreach (KeyValuePair<string, string> item in unCheckedDic)
                                    {
                                        this.AisTvRefDicElement.SetCheckState(missionId + "-" + item.Key, false);
                                    }
                                }
                            }
                        }
                    }

                }
                else if (treeView.Name == "AisTvRefDicMission")
                {
                    foreach (TreeViewItem treeViewItem in treeView.Items)
                    {
                        CheckBox tempCheckBox = GetCheckBox(treeViewItem);
                        JToken tempChildItemToken = treeViewItem.DataContext as JToken;
                        if (getMissionType(tempChildItemToken) == 1)
                        {
                            continue;
                        }
                        if (tempCheckBox != null && ex.Key == (string)tempChildItemToken["id"])
                        {
                            foreach (TreeViewItem ctmItem in this.AisTvRefDicCtm.Items)
                            {
                                CheckBox ctmCheckBox = GetCheckBox(ctmItem);
                                JToken ctmChildItemToken = ctmItem.DataContext as JToken;
                                if (ex.Key == (string)ctmChildItemToken["missionId"])
                                {
                                    if (ctmCheckBox != null)
                                    {
                                        if (tempCheckBox.IsChecked == null || tempCheckBox.IsChecked == true)
                                        {
                                            ctmCheckBox.IsChecked = true;
                                            setCheckedFlag(ctmItem, true);
                                        }
                                        else
                                        {
                                            ctmCheckBox.IsChecked = false;
                                            setCheckedFlag(ctmItem, false);
                                        }

                                        string[] nodesArray = ((string)ctmChildItemToken["nodes"]).Split(',');
                                        if (nodesArray.Length == 0) continue;

                                        foreach (string sttringCtmId in nodesArray)
                                        {
                                            checkedElementsByCtmId(ex.Key + "-" + sttringCtmId, tempCheckBox);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 end

        //Multi_Dac sunyi 2018/11/30 start
        //アウトプット追加
        private void button_Reload_CTM_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
			// Wang New dac flow 20190308 Start
            //this.ScrollViewerDac.InitItems();
			// Wang New dac flow 20190308 End
        }

        private void button_Reload_CTM_Click(object sender, RoutedEventArgs e)
        {
			// Wang New dac flow 20190308 Start
            //this.ScrollViewerDac.InitItems();
			// Wang New dac flow 20190308 End
        }
        //Multi_Dac sunyi 2018/11/30 end

        //AISDAC No.81 sunyi 20181214 start
        //AISMM No.85 sunyi 20181211 内容修正、GripMission条件表示
        private void setElementNameWithCondition_Grip(Dictionary<string, List<ElementConditionData>> elementConditions_Grip)
        {
            this.elementConditions_Grip.Clear();
            foreach (var elementCondition in elementConditions_Grip)
            {
                foreach (var ctm in this.ctms)
                {
                    if (ctm.id.ToString().Equals(elementCondition.Key))
                    {
                        Dictionary<ElementConditionData, string> tmp = new Dictionary<ElementConditionData, string>();
                        foreach (var elementConditionData in elementCondition.Value)
                        {
                            foreach (var element in ctm.GetAllElements())
                            {
                                if (element.id.ToString() == elementConditionData.Id)
                                {
                                    tmp.Add(elementConditionData, element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                    break;
                                }
                            }
                        }
                        if (tmp.Count > 0)
                        {
                            this.elementConditions_Grip.Add(ctm.id.ToString(), tmp);
                        }
                    }
                }
            }
        }

        private void GetElementConditionDatas(List<MissionCtm> missionCtms)
        {
            //this.conditionLists.Clear();
            //this.conditionPatterns.Clear();
            foreach (var missionCtm in missionCtms)
            {
                if (!ctmIdList.Contains(missionCtm.CtmId))
                {
                    ctmIdList.Add(missionCtm.CtmId);
                    if (string.IsNullOrEmpty(missionCtm.ElementCondition) == false)
                    {
                        JToken condJson = JToken.Parse(missionCtm.ElementCondition);
                        JArray condElements = (JArray)condJson["elements"];
                        this.conditionPatterns.Add(missionCtm.CtmId, int.Parse(condJson["pattern"].ToString()));

                        List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
                        ElementConditionDatas = JsonConvert.DeserializeObject<List<ElementConditionData>>(condElements.ToString());
                        this.conditionLists.Add(missionCtm.CtmId, ElementConditionDatas);
                    }
                    else
                    {
                        this.conditionLists.Add(missionCtm.CtmId, new List<ElementConditionData>());
                    }
                }
            }
        }
        //ISSUE_NO.778 Sunyi 2018/07/23 Start
        //G-missionの条件設定の色付く
        private void GetElementConditionDatas_G(GripMissionCtm gripMissionCtms)
        {
            //this.conditionLists_Grip.Clear();
            //this.conditionPatterns_Grip.Clear();
            if (gripMissionCtms != null)
            {
                List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
                //E2NaviUI sunyi 20190305 start
                //MISSION条件対応
                if (gripMissionCtms.StartNodes == null)
                {
                    //start
                    if (gripMissionCtms.StartNode.CondSet != null)
                    {
                        if (!ctmIdList.Contains(gripMissionCtms.StartNode.CtmId))
                        {
                            ctmIdList.Add(gripMissionCtms.StartNode.CtmId);
                            this.conditionPatterns_Grip.Add(gripMissionCtms.StartNode.CtmId, gripMissionCtms.StartNode.CondSet.Pattern);
                            foreach (var Condition in gripMissionCtms.StartNode.CondSet.Conditions)
                            {
                                ElementConditionData ElementConditionData = new ElementConditionData
                                {
                                    Id = Condition.ElementId,
                                    Type = Condition.Datatype,
                                    Condition = Condition.Operator,
                                    Value = Condition.Value
                                };
                                ElementConditionDatas.Add(ElementConditionData);
                            }
                            this.conditionLists_Grip.Add(gripMissionCtms.StartNode.CtmId, ElementConditionDatas);
                        }
                    }
                }
                else
                {
                    //StartNodes
                    foreach (var condSet in gripMissionCtms.StartNodes)
                    {
                        if (!ctmIdList.Contains(condSet.Value.CtmId))
                        {
                            ctmIdList.Add(condSet.Value.CtmId);
                            if (condSet.Value.CondSet != null)
                            {
                                this.conditionPatterns_Grip.Add(condSet.Value.CtmId, condSet.Value.CondSet.Pattern);
                                foreach (var Condition in condSet.Value.CondSet.Conditions)
                                {
                                    ElementConditionData ElementConditionData = new ElementConditionData
                                    {
                                        Id = Condition.ElementId,
                                        Type = Condition.Datatype,
                                        Condition = Condition.Operator,
                                        Value = Condition.Value
                                    };
                                    ElementConditionDatas.Add(ElementConditionData);
                                }
                                this.conditionLists_Grip.Add(condSet.Value.CtmId, ElementConditionDatas);
                            }
                        }
                    }
                }
                //E2NaviUI sunyi 20190305 end

                //end
                foreach (var condSet in gripMissionCtms.EndNodes)
                {
                    if (!ctmIdList.Contains(condSet.CtmId))
                    {
                        ctmIdList.Add(condSet.CtmId);
                        if (condSet.CondSet_End != null)
                        {
                            this.conditionPatterns_Grip.Add(condSet.CtmId, condSet.CondSet_End.Pattern);
                            foreach (var Condition in condSet.CondSet_End.Conditions)
                            {
                                ElementConditionData ElementConditionData = new ElementConditionData
                                {
                                    Id = Condition.ElementId,
                                    Type = Condition.Datatype,
                                    Condition = Condition.Operator,
                                    Value = Condition.Value
                                };
                                ElementConditionDatas.Add(ElementConditionData);
                            }
                            this.conditionLists_Grip.Add(condSet.CtmId, ElementConditionDatas);
                        }
                    }
                }

                //trace
                HashSet<string> branchNodeIds = new HashSet<string>();
                if (gripMissionCtms.BranchNodes != null)
                {
                    foreach (Node node in gripMissionCtms.BranchNodes)
                    {
                        branchNodeIds.Add(node.CtmId);
                    }
                }

                // Wang Issue AISTEMP-122 20181228 Start
                //if (gripMissionCtms.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace && gripMissionCtms.TraceNodes != null)
                if ((gripMissionCtms.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace || gripMissionCtms.SearchType == FoaCore.Model.GripR2.Search.SearchType.Period) && gripMissionCtms.TraceNodes != null)
                // Wang Issue AISTEMP-122 20181228 End
                {
                    foreach (var condSet in gripMissionCtms.TraceNodes)
                    {
                        if (!ctmIdList.Contains(condSet.CtmId))
                        {
                            ctmIdList.Add(condSet.CtmId);
                            if (condSet.CondSet_Trace != null)
                            {
                                this.conditionPatterns_Grip.Add(condSet.CtmId, condSet.CondSet_Trace.Pattern);
                                foreach (var Condition in condSet.CondSet_Trace.Conditions)
                                {
                                    ElementConditionData ElementConditionData = new ElementConditionData
                                    {
                                        Id = Condition.ElementId,
                                        Type = Condition.Datatype,
                                        Condition = Condition.Operator,
                                        Value = Condition.Value
                                    };
                                    ElementConditionDatas.Add(ElementConditionData);
                                }
                                this.conditionLists_Grip.Add(condSet.CtmId, ElementConditionDatas);
                            }
                        }
                    }
                }
            }
        }
        //ISSUE_NO.778 Sunyi 2018/07/23 End
        //AISDAC No.81 sunyi 20181214 end

		// Wang New dac flow 20190308 Start
        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
        }

        private void BtnOpenDAC_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            try
            {
                //dac No.7 sunyi start
                if (this.dateTimePicker_Start.Value == null || this.dateTimePicker_End.Value == null || this.dateTimePicker_StartDate.Value == null)
                {
                    //収集期間を設定してください。
                    FoaMessageBox.ShowError("AIS_E_073");
                    return;
                }
                //AISMM No.55 sunyi 20181217 start
                //DACに展開する
                if (AisTvRefDicMission.Items.IsEmpty)
                {
                    FoaMessageBox.ShowError("AIS_E_025");
                    return;
                }
                //AISMM No.55 sunyi 20181217 end
                // Wang Issue of AISMM-21 20181109 Start
                mainWindow.LoadinVisibleChange(false);
                //LoadingDac.Visibility = System.Windows.Visibility.Visible;
                //ScrollViewerDac.IsEnabled = false;

                // Wang Issue DAC-72 20181225 Start
                /*
                //Excel出力する際に、「選択ミッション」に検索条件を再設定
                foreach (TreeViewItem treeViewItem in AisTvRefDicMission.Items)
                {
                    JToken missionToken = treeViewItem.DataContext as JToken;

                    // 1.AISを修正場合
                    // 2.ミッションアイテムを選択された場合
                    if (!this.isFirstFile || missionHasSelected())
                    {
                        if (missionToken["aisSearchType"] == null)
                        {
                            setSearchConditions(missionToken);
                        }

                        System.Windows.Media.Color color = ((SolidColorBrush)treeViewItem.Background).Color;
                        if (color == System.Windows.Media.Colors.DodgerBlue)
                        {
                            setSearchConditions(missionToken);
                        }
                    }
                    else
                    {
                        setSearchConditions(missionToken);
                    }
                }

                // CheckBoxTreeViewのデータソースを取得
                JObject workplaceJson = new JObject();
                JArray jArrayMission = JArray.Parse(AisTvRefDicMission.JsonItemSource);
                JArray jArrayCtm = JArray.Parse(AisTvRefDicCtm.JsonItemSource);
                JArray jArrayElement = JArray.Parse(AisTvRefDicElement.JsonItemSource);
                JArray array = new JArray();

                // Processing on Server dn 2018/09/07 start
                Boolean ctmHasFlag = false;
                // 期間固定の場合,検索条件チェック
                foreach (JToken missionToken in jArrayMission)
                {

                    if (!getCheckedFlag(missionToken))
                    {
                        continue;
                    }
                    // CTM missionがあるかどうか判定(missionNewType=1:CTM mission選択)

                    if (int.Parse((string)missionToken["missionNewType"]) == 1 && ctmHasFlag == false)
                    {
                        ctmHasFlag = true;
                    }

                    int searchType = int.Parse((string)missionToken["aisSearchType"]);
                    if (searchType == 2)
                    {
                        // 期間チェック：開始日時　> 終了日時、エラーになる
                        long startTime = (long)missionToken["offlineStart"];
                        long endTime = (long)missionToken["offlineEnd"];

                        if (startTime > endTime)
                        {
                            //AIS_DAC No.74 sunyi 2018/12/05 start
                            //// Wang Issue of AISMM-21 20181109 Start
                            //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                            //ScrollViewerDac.IsEnabled = true;
                            //// Wang Issue of AISMM-21 20181109 End
                            //FoaMessageBox.ShowError("AIS_E_019");
                            //return;
                            DateTime endDate = UnixTime.ToDateTime(endTime);
                            //DateTime endDate = new DateTime(endTime);
                            missionToken["offlineEnd"] = UnixTime.ToLong(endDate.AddDays(1));
                            //AIS_DAC No.74 sunyi 2018/12/05 end
                        }
                    }
                }

                // Processing on Server dn 2018/09/07 end
                // 選択されたエレメントの存在フラグ
                bool exportFlag = false;

                foreach (JToken elementToken in jArrayElement)
                {
                    //エレメントが存在するの判定
                    if (!exportFlag)
                    {
                        if (getCheckedFlag(elementToken))
                        {
                            exportFlag = true;
                            break;
                        }
                    }
                }

                //ISSUE_NO.727 sunyi 2018/06/14 Start
                //grip mission処理ができるように修正にする
                if (!this.IsSelectedProgramMission)
                {
                    this.DataSource = DataSourceType.GRIP;
                }

                // Processing On Server dn 2018/09/26 start
                if (ctmHasFlag)
                {
                    this.DataSource = DataSourceType.NONE;
                }
                else
                {
                    this.DataSource = DataSourceType.GRIP;
                }
                // Processing On Server dn 2018/09/26 end

                // 出力するのエレメントがありません
                if (!exportFlag)
                {
                    // Wang Issue of AISMM-21 20181109 Start
                    //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                    mainWindow.LoadinVisibleChange(true);

                    ScrollViewerDac.IsEnabled = true;
                    // Wang Issue of AISMM-21 20181109 End
                    FoaMessageBox.ShowError("AIS_E_007");
                    return;
                }

                var workFile = CreateWorkingFile();

                DateTime end = DateTime.Now;
                DateTime start = DateTime.Now;

                // get mission result
                string resultFolder = string.Empty;
                string rootFolder = string.Empty;
                string path = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission");
                if (Directory.Exists(path))
                {
                    FoaCore.Util.FileUtil.clearFolder(path);
                }

                List<int> listInterval = new List<int>();
                int routeIndex = 0;
                int displayOrder = 0;
                foreach (JToken missionToken in jArrayMission)
                {
                    if (!getCheckedFlag(missionToken))
                    {
                        continue;
                    }

                    int searchType = int.Parse((string)missionToken["aisSearchType"]);
                    // 開始終了時間を設定
                    convertStartAndEndTime(missionToken, out start, out end);

                    this.DownloadCts = new CancellationTokenSource();

                    string missionTokenId = missionToken[MmsTvJsonKey.Id].ToString();
                    if (getMissionType(missionToken) == 1)
                    { //CTM Mission
                        resultFolder = await GetMissionCtm(start, end, missionTokenId, this.DownloadCts);
                        DirectoryInfo di = new DirectoryInfo(resultFolder);
                        rootFolder = di.Parent.FullName;
                    }
                    else
                    { //Grip Mission
                        missionToken["routeIndex"] = routeIndex;
                        resultFolder = await GetGripData(start, end, missionToken);
                        rootFolder = string.Empty;
                        if (resultFolder == null)
                        {
                            logger.Debug("指定した期間「" + start + " ～ " + end + "」で「" + missionTokenId + "」のデータが存在しません。");
                        }
                        else
                        {
                            routeIndex++;
                        }
                    }

                    // 結果フォルダを設定
                    missionToken["resultFolder"] = resultFolder;
                    missionToken["rootFolder"] = rootFolder;
                    // Processing On Server dn 2018/09/13 start
                    // 選択ミッションの表示順
                    missionToken["displayOrder"] = displayOrder;
                    displayOrder++;
                    // Processing On Server dn 2018/09/13 end
                    // Ctmsデータを設定
                    JArray tempCtmChildren = new JArray();
                    foreach (JToken jToken in jArrayCtm)
                    {
                        if (missionTokenId == jToken["missionId"].ToString())
                        {
                            tempCtmChildren.Add(jToken);
                        }
                    }
                    missionToken["ctms"] = (JArray)tempCtmChildren;

                    // Elementsデータを設定
                    JArray tempElementChildren = new JArray();
                    foreach (JToken elementToken in jArrayElement)
                    {
                        if (missionTokenId == elementToken["missionId"].ToString())
                        {
                            tempElementChildren.Add(elementToken);
                        }
                    }
                    missionToken["elements"] = (JArray)tempElementChildren;

                    // Mission Arrayを設定
                    array.Add(missionToken);
                }

                Dictionary<CtmObject, List<CtmElement>> ctmElementDict = new Dictionary<CtmObject, List<CtmElement>>();
                foreach (JToken elementToken in jArrayElement)
                {
                    if (getMissionType(elementToken) == 0)
                    {
                        continue;
                    }

                    if (getCheckedFlag(elementToken))
                    {
                        string missionId = elementToken["missionId"].ToString();
                        string strElmToken = elementToken.ToString();
                        strElmToken = strElmToken.Replace(missionId + "-", "");
                        JToken tmpToken = JToken.Parse(strElmToken);

                        CtmObject cc = JsonConvert.DeserializeObject<CtmObject>(tmpToken.ToString());
                        List<CtmElement> listElement = new List<CtmElement>();
                        JArray eleGroupArray = JArray.Parse(tmpToken["children"].ToString());
                        foreach (JToken group in eleGroupArray)
                        {
                            //AIS_DAC No.76 sunyi 2018/12/05 start
                            //group["children"]がNullの場合、チェックする
                            if (group["children"] == null)
                            {
                                continue;
                            }
                            //AIS_DAC No.76 sunyi 2018/12/05 end
                            JArray eleArray = JArray.Parse(group["children"].ToString());
                            foreach (JToken ele in eleArray)
                            {
                                if (!getCheckedFlag(ele))
                                {
                                    continue;
                                }

                                CtmElement objElement = new CtmElement();
                                objElement = JsonConvert.DeserializeObject<CtmElement>(ele.ToString());
                                listElement.Add(objElement);
                            }
                        }

                        ctmElementDict.Add(cc, listElement);
                    }
                }

                // MIN Interval.
                int minInterval = 0;
                if (listInterval.Count > 0)
                {
                    minInterval = listInterval.Min();
                }

                // オンライングラフID
                OnlineId = new FoaCore.Common.Util.GUID().ToString();

                workplaceJson["mission_data"] = array;
                workplaceJson["work_place_flag"] = 2;
                workplaceJson["file_type"] = 2;
                workplaceJson["file_id"] = OnlineId;
                workplaceJson["file_name"] = OnlineId;

                //var workFile = CreateWorkingFile();
                //ExcelOpen(workFile);

                CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
                client.CompleteHandler += resJson =>
                {
                    if (string.IsNullOrEmpty(resJson))
                    {
                        // Wang Issue of AISMM-21 20181109 Start
                        //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                        mainWindow.LoadinVisibleChange(true);
                        ScrollViewerDac.IsEnabled = true;
                        // Wang Issue of AISMM-21 20181109 End
                        logger.Info("WorkPlaceデータを作成が失敗しました。");
                        return;
                    }
                    //excelOpen(workFile, start, end, resultRootFolder, periodHour, searchType, resJson);
                    excelOpen(workFile, array, ctmElementDict, minInterval, resJson.ToString(), OnlineId);
                    // Wang Issue of AISMM-21 20181109 Start
                    //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                    mainWindow.LoadinVisibleChange(true);
                    ScrollViewerDac.IsEnabled = true;
                    // Wang Issue of AISMM-21 20181109 End
                };
                client.Post(CmsUrl.GetAisCreate(), workplaceJson.ToString());
                */
                //Excel出力する際に、「選択ミッション」に検索条件を再設定
                foreach (TreeViewItem treeViewItem in AisTvRefDicMission.Items)
                {
                    JToken missionToken = treeViewItem.DataContext as JToken;

                    // 1.AISを修正場合
                    // 2.ミッションアイテムを選択された場合
                    if (!this.isFirstFile || missionHasSelected())
                    {
                        if (missionToken["aisSearchType"] == null)
                        {
                            setSearchConditions(missionToken);
                        }

                        System.Windows.Media.Color color = ((SolidColorBrush)treeViewItem.Background).Color;
                        if (color == System.Windows.Media.Colors.DodgerBlue)
                        {
                            setSearchConditions(missionToken);
                        }
                    }
                    else
                    {
                        setSearchConditions(missionToken);
                    }
                }

                OpenDacWorker = new BackgroundWorker();
                OpenDacWorker.WorkerReportsProgress = true;
                OpenDacWorker.DoWork += new DoWorkEventHandler(OpenDac);

                OpenDacParam param = new OpenDacParam()
                {
                    jArrayMission = JArray.Parse(AisTvRefDicMission.JsonItemSource),
                    jArrayCtm = JArray.Parse(AisTvRefDicCtm.JsonItemSource),
                    jArrayElement = JArray.Parse(AisTvRefDicElement.JsonItemSource),
                    dtStart = this.dateTimePicker_Start.Value,
                    dtEnd = this.dateTimePicker_End.Value,
                    dtStartDate = this.dateTimePicker_StartDate.Value,
                    //Dac-86 sunyi 20181227 start
                    handler = OpenDacHandler
                    //Dac-86 sunyi 20181227 end
                };

                OpenDacWorker.RunWorkerAsync(param);
                // Wang Issue DAC-72 20181225 End
            }
            catch (Exception ex)
            {
                // Wang Issue of AISMM-21 20181109 Start
                //LoadingDac.Visibility = System.Windows.Visibility.Hidden;
                mainWindow.LoadinVisibleChange(true);
                //ScrollViewerDac.IsEnabled = true;
                throw ex;
                // Wang Issue of AISMM-21 20181109 End
            }
        }
		// Wang New dac flow 20190308 End
    }
}
