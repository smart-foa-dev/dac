﻿using DAC.CtmData;
using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using CsvHelper;
using FoaCore;
using FoaCore.Common;
using FoaCore.Common.Util;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;

using DAC.AExcel.Grip;
using Newtonsoft.Json;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using DAC.Util.Dac;
using FoaCoreCom.common.dto;
using System.Net;
using FoaCoreCom.common.main;
using Microsoft.VisualBasic;

namespace DAC.AExcel
{
    class InteropExcelWriterDac : ExcelWriter
    {
		// Wang New dac flow 20190308 Start
        public class IdNameObject {
            public string id;
            public string name;
        }
		// Wang New dac flow 20190308 End

        private Dictionary<Mission, List<CtmObject>> missionCtmDict;
        private Dictionary<CtmObject, List<CtmElement>> missionCtmElementDict;
        private string firstCtmId = string.Empty;

        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 start
        // Multi_Dac 修正
        //grip mission処理ができるように修正にする
        private Mission gripMission;
        //ISSUE_NO.727 sunyi 2018/06/14 End
        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/17 Start
        private Dictionary<CtmObject, List<CtmElement>> ctmElementDic;
        private string onlineId;
        private string workplaceId;
		// 画面データ
        private JArray resultArray;
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/17 End
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end

		// Wang New dac flow 20190308 Start
        /// <summary>
        /// 定数sheetID
        /// </summary>
        private static readonly int varSheetIDRow = 1;
        private static readonly int varSheetIDCol = 600;
        private static readonly string varSheetName = "DataSheet";
        private static readonly string AIS_PARAM_SHEET_NAME = "param";
        private static readonly string PROXY_URI = "ProxyServerURI";
        private static readonly string MISSION_GRIP = "ミッション_GRIP";
        private static readonly string MISSION_M = "ミッション_MISSION";
        private static readonly string MISSION_G = "_Routes";
        private static readonly string DAC_SHEET = "ＤＡＣシート"; // MAX: Column 'BJ'

        private const int TitleRow = 11;
        //dac_home datasheet start
        private const int FormulaRow = 2;
        private const int MaxNum = 26;
        private const int COLUMN_WIDTH = 7;
        //dac_home datasheet end

        /// <summary>
        /// 統計対象ミッション
        /// </summary>
        private Dictionary<string, ClsAttribObject> m_missions = new Dictionary<string, ClsAttribObject>();

        /// <summary>
        /// 統計対象CTM
        /// </summary>
        private Dictionary<string, Dictionary<string, ClsAttribObject>> m_ctms = new Dictionary<string, Dictionary<string, ClsAttribObject>>();

        /// <summary>
        /// 統計対象Element
        /// </summary>
        private Dictionary<string, Dictionary<string, ClsAttribObject>> m_elements = new Dictionary<string, Dictionary<string, ClsAttribObject>>();
		// Wang New dac flow 20190308 End

        public InteropExcelWriterDac(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi = null)
            : base(mode, ds, ctms, dt, bw)
        {

        }

        public InteropExcelWriterDac(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi, Dictionary<Mission, List<CtmObject>> ctmDict,
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
            // Multi_Dac 修正
            //Dictionary<CtmObject, List<CtmElement>> elemDict)
            Dictionary<CtmObject, List<CtmElement>> elemDict, JArray resultArray, Dictionary<CtmObject, List<CtmElement>> ctmElementDic, string workPlaceId, string onlineId)
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
            : base(mode, ds, ctms, dt, bw)
        {
            this.missionCtmDict = ctmDict;
            this.missionCtmElementDict = elemDict;

            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
            // Multi_Dac 修正
            this.workplaceId = workPlaceId;
            this.onlineId = onlineId;
            this.ctmElementDic = ctmElementDic;
            this.resultArray = resultArray;
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
        }


        public override void WriteCtmData(string excelName, string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
            Microsoft.Office.Interop.Excel.Workbooks oWBooks = null;
            Microsoft.Office.Interop.Excel.Workbook oWBook = null;

            try
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                oXls = new Microsoft.Office.Interop.Excel.Application();
                oXls.Visible = false; //確認のためエクセルのウィンドウを表示する

                //エクセルファイルをオープンする
                oWBooks = oXls.Workbooks;
                oWBook = oWBooks.Open(excelName); // オープンするExcelファイル名
                //ISSUE_NO.744 Sunyi 2018/06/19 Start
                //最小化→正常表示
                oXls.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;
                //ISSUE_NO.744 Sunyi 2018/06/19 End

                Microsoft.Office.Interop.Excel.Worksheet wsUserInfo = AisUtil.GetSheetFromSheetName_Interop(oWBook, "USERINFO");
                writeUserInfoSheet(wsUserInfo);

                // 1. Set Mission CTM & Element parameter
                string paramSheetNameM = "ミッション";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamM = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameM);
                writeConfigSheetCtm(worksheetParamM);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 2. Set Start & End Time parameter
                string paramSheetNameC = "業務時間";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamC = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameC);
                writeConfigSheetTime(worksheetParamC, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                string paramSheetName = DAC.Util.Keywords.PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetParam3 = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetName);
                WriteConfigSheetParam(worksheetParam3, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = DAC.Util.Keywords.RO_PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_Interop(oWBook, roParamSheetName);
                WriteRoParamSheet_Interop(worksheetRoParam);
                
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 3. Save the sheet with the parameter
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Save();
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Close();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                //// 4. Re-open again with the newly inputted parameter sheet
                oWBook = (Microsoft.Office.Interop.Excel.Workbook)(oXls.Workbooks.Open(excelName));
                oXls.Visible = true; //確認のためエクセルのウィンドウを表示する
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Activate();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // Wang Issue DAC-44 20181112 Start
                //oXls.Run("MakeMenuBar");
                // Wang Issue DAC-44 20181112 End

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
                //ISSUE_NO.744 Sunyi 2018/06/19 Start
                //最小化→正常表示
                oXls.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlNormal;
                //ISSUE_NO.744 Sunyi 2018/06/19 End
                //EXCEL最前面へ移動
                ExcelUtil.OpenExcelFileForDAC(oXls);
            }
            catch (Exception err)
            {
                // ログ出力
                AisMessageBox.DisplayErrorMessageBox("ExcelOpen Exception Error : \n" + err.ToString());
            }
            finally 
            {
                if (oWBook != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                    oWBook = null;
                }

                if (oWBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBooks);
                    oWBooks = null;
                }

                if (oXls != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                    oXls = null;
                }
            }
        }

        private void writeUserInfoSheet(Microsoft.Office.Interop.Excel.Worksheet ws)
        {
            ws.Cells[1, 1].Value = "'" + LoginInfo.GetInstance().GetLoginUserInfo().UserId;
            /*
            // Host
            Microsoft.Office.Interop.Excel.Range rangeA = AisUtil.GetFindRange_Interop(ws.Cells, "CmsHost");
            if (rangeA != null)
            {
                rangeA[1, 2].Value = AisConf.Config.CmsHost;
            }

            // Port
            Microsoft.Office.Interop.Excel.Range rangeB = AisUtil.GetFindRange_Interop(ws.Cells, "CmsPort");
            if (rangeB != null)
            {
                rangeB[1, 2].Value = AisConf.Config.CmsPort;
            }
            */
        }

        private void writeConfigSheetTime(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            DateTime start = GetDateTimeVal(configParams, ExcelConfigParam.START, DateTime.MinValue);
            DateTime end = GetDateTimeVal(configParams, ExcelConfigParam.END, DateTime.MinValue);

            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;
            start.AddSeconds(-start.Second);
            start.AddMilliseconds(-start.Millisecond);
            // 操業開始時刻
            cellsParam[1, 1].Value = start.ToString("HH:mm");
            end.AddSeconds(-end.Second);
            end.AddMilliseconds(-end.Millisecond);
            // 操業終了時刻
            cellsParam[2, 1].Value = end.ToString("HH:mm");

            // Hide the workSheet
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        private void writeConfigSheetCtm(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();

            // 2. Pass the Mission parameter
            int startRowIndex = 1;
            int eleStartRowIndex = 1;
            foreach (KeyValuePair<Mission, List<CtmObject>> kvpMission in missionCtmDict)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (kvpMission.Value.Count <= 0) continue; // skip mission who does not have selected CTM object

                // Mission ID
                cellsParam[startRowIndex, 1].Value = kvpMission.Key.Id;

                // Mission Name
                cellsParam[startRowIndex, 2].Value = DAC.Model.Util.AisUtil.GetCatalogLang(kvpMission.Key);

                // 3. Pass the CTM parameter
                int ctmStartRowIndex = startRowIndex;
                foreach (CtmObject ctm in kvpMission.Value)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // ParentMission ID
                    cellsParam[ctmStartRowIndex, 3].Value = kvpMission.Key.Id;

                    // CTM ID
                    cellsParam[ctmStartRowIndex, 4].Value = ctm.id.ToString();
                    if (string.IsNullOrEmpty(this.firstCtmId))
                    {
                        this.firstCtmId = ctm.id.ToString();
                    }

                    // CTM Name
                    cellsParam[ctmStartRowIndex, 5].Value = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    // 4. Pass the Element parameter
                    foreach (CtmElement ele in missionCtmElementDict[ctm])
                    {
                        // Cancel Check
                        if (this.bw.CancellationPending)
                        {
                            return;
                        }

                        // ParentCTM ID
                        cellsParam[eleStartRowIndex, 6].Value = ctm.id.ToString();

                        // Element ID
                        cellsParam[eleStartRowIndex, 7].Value = ele.id.ToString();

                        // Element Name
                        cellsParam[eleStartRowIndex, 8].Value = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                        // Additional : ParentMission ID -> For deleting CTM Element in case there is duplicate CTM
                        cellsParam[eleStartRowIndex, 9].Value = kvpMission.Key.Id;

                        ++eleStartRowIndex;
                    }

                    ++ctmStartRowIndex;
                }

                startRowIndex = ctmStartRowIndex;
            }
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        protected virtual void WriteConfigSheetParam(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            string fileName = GetStringVal(configParams, ExcelConfigParam.REGISTERED_FILENAME, string.Empty);
            string missionId = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, null);
            bool enableMultiDac = GetBoolVal(configParams, ExcelConfigParam.ENABLE_MULTI_DAC, false);

            var range = worksheetParam.get_Range("A1", "J100");

            // 登録フォルダ
            var range2 = AisUtil.GetFindRange_Interop(range, "登録フォルダ");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.DacRegistryFolderPath;
            }

            // 登録ファイル
            range2 = AisUtil.GetFindRange_Interop(range, "登録ファイル");
            if (range2 != null)
            {
                range2[1, 2].Value = fileName;
            }

            // サーバIPアドレス
            range2 = AisUtil.GetFindRange_Interop(range, "サーバIPアドレス");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.CmsHost;
            }

            // サーバポート番号
            range2 = AisUtil.GetFindRange_Interop(range, "サーバポート番号");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.CmsPort;
            }

            // GRIPサーバIPアドレス
            range2 = AisUtil.GetFindRange_Interop(range, "GRIPサーバIPアドレス");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.GripServerHost;
            }

            // GRIPサーバポート番号
            range2 = AisUtil.GetFindRange_Interop(range, "GRIPサーバポート番号");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.GripServerPort;
            }

            // ミッションID
            range2 = AisUtil.GetFindRange_Interop(range, "ミッションID");
            if (range2 != null)
            {
                range2[1, 2].Value = missionId;
            }

            // 製作者名
            range2 = AisUtil.GetFindRange_Interop(range, "製作者名");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.UserId;
            }

            // タイムアウト秒数
            range2 = AisUtil.GetFindRange_Interop(range, "タイムアウト秒数");
            if (range2 != null)
            {
                range2[1, 2].Value = "100";
            }

            // for KMEW
            range2 = AisUtil.GetFindRange_Interop(range, "CmsVersion");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.CmsVersion;
            }
            if (AisConf.Config.CmsVersion == "3.5")
            {
                var ahInfo = AisUtil.GetAhInfo(this.firstCtmId);

                range2 = AisUtil.GetFindRange_Interop(range, "MfHost");
                if (range2 != null)
                {
                    range2[1, 2].Value = ahInfo.Mf;
                }
                range2 = AisUtil.GetFindRange_Interop(range, "MfPort");
                if (range2 != null)
                {
                    range2[1, 2].Value = ahInfo.MfPort;
                }
            }

            // ProxyServer使用
            range2 = AisUtil.GetFindRange_Interop(range, "ProxyServer使用");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.UseProxy;
            }

            // ProxyServerURI
            range2 = AisUtil.GetFindRange_Interop(range, "ProxyServerURI");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.ProxyURI;
            }

            // ProxyServerURI
            range2 = AisUtil.GetFindRange_Interop(range, "MultiDac有効");
            if (range2 != null)
            {
                range2[1, 2].Value = enableMultiDac;
            }

            // IsFirstSave
            range2 = AisUtil.GetFindRange_Interop(range, "IsFirstSave");
            if (range2 != null)
            {
                range2[1, 2].Value = "";
            }


            // IsFirstSave
            range2 = AisUtil.GetFindRange_Interop(range, "Domain Id");
            if (range2 != null)
            {
                range2[1, 2].Value = System.Environment.GetEnvironmentVariable("FOADOMAINID");
            }

            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

		// Wang New dac flow 20190308 Start
        public static void InitializeConfigSheetParam(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams, string dacId)
        {
            string fileName = GetStringVal(configParams, ExcelConfigParam.REGISTERED_FILENAME, string.Empty);
            bool enableMultiDac = GetBoolVal(configParams, ExcelConfigParam.ENABLE_MULTI_DAC, false);

            var range = worksheetParam.get_Range("A1", "J100");

            // 登録フォルダ
            var range2 = AisUtil.GetFindRange_Interop(range, "登録フォルダ");
            
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.DacRegistryFolderPath;
            }
            range2 = AisUtil.GetFindRange_Interop(range, "DACID");
            if (range2 != null)
            {
                range2[1, 2].Value = dacId;
            }
            // 登録ファイル
            range2 = AisUtil.GetFindRange_Interop(range, "登録ファイル");
            if (range2 != null)
            {
                range2[1, 2].Value = fileName;
            }

            // 製作者名
            range2 = AisUtil.GetFindRange_Interop(range, "製作者名");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.UserId;
            }

            // MultiDac
            range2 = AisUtil.GetFindRange_Interop(range, "MultiDac有効");
            if (range2 != null)
            {
                range2[1, 2].Value = enableMultiDac;
            }

            // IsFirstSave
            range2 = AisUtil.GetFindRange_Interop(range, "IsFirstSave");
            if (range2 != null)
            {
                range2[1, 2].Value = "";
            }

            //Dac_Home sunyi 20190530 start
            // IsFirstSave
            range2 = AisUtil.GetFindRange_Interop(range, "IsTemplate");
            if (range2 != null)
            {
                range2[1, 2].Value = "";
            }

            //AisFolderPath
            range2 = AisUtil.GetFindRange_Interop(range, "AisFolderPath");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.AisFolderPath;
            }

            // サーバIPアドレス
            range2 = AisUtil.GetFindRange_Interop(range, "サーバIPアドレス");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.CmsHost;
            }

            // サーバポート番号
            range2 = AisUtil.GetFindRange_Interop(range, "サーバポート番号");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.CmsPort;
            }

            // GRIPサーバIPアドレス
            range2 = AisUtil.GetFindRange_Interop(range, "GRIPサーバIPアドレス");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.GripServerHost;
            }

            // GRIPサーバポート番号
            range2 = AisUtil.GetFindRange_Interop(range, "GRIPサーバポート番号");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.GripServerPort;
            }
            //Dac_Home sunyi 20190530 start

            // domain id 
            range2 = AisUtil.GetFindRange_Interop(range, "Domain Id");
            if (range2 != null) 
            {
                range2[1, 2].Value = System.Environment.GetEnvironmentVariable("FOADOMAINID");
            }
            //Dac_Home sunyi 20190530 start

            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }
		// Wang New dac flow 20190308 End

        //Dac_Home sunyi 20190530 start
        public static void InitializeConfigSheetParamTemplate(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams, string dacId)
        {
            string fileName = GetStringVal(configParams, ExcelConfigParam.REGISTERED_FILENAME, string.Empty);
            bool enableMultiDac = GetBoolVal(configParams, ExcelConfigParam.ENABLE_MULTI_DAC, false);

            var range = worksheetParam.get_Range("A1", "J100");

            // 登録フォルダ
            var range2 = AisUtil.GetFindRange_Interop(range, "登録フォルダ");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.DacRegistryFolderPath;
            }

            // 登録ファイル
            range2 = AisUtil.GetFindRange_Interop(range, "登録ファイル");
            if (range2 != null)
            {
                range2[1, 2].Value = fileName;
            }

            //AisFolderPath
            range2 = AisUtil.GetFindRange_Interop(range, "AisFolderPath");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.AisFolderPath;
            }

            // domain id 
            range2 = AisUtil.GetFindRange_Interop(range, "Domain Id");
            if (range2 != null)
            {
                range2[1, 2].Value = System.Environment.GetEnvironmentVariable("FOADOMAINID");
            }
            // dac id
            range2 = AisUtil.GetFindRange_Interop(range, "DACID");
            if (range2 != null)
            {
                range2[1, 2].Value = dacId;
            }
            // IsFirstSave
            range2 = AisUtil.GetFindRange_Interop(range, "IsFirstSave");
            if (range2 != null)
            {
                range2[1, 2].Value = "";
            }

            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }
        //Dac_Home sunyi 20190530 end

        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
        // Multi_Dac 修正
        /// <summary>
        /// CtmObjectの中身を、全てDataRow配列として使用する
        /// </summary>
        /// <param name="missions"></param>
        /// <returns></returns>
        private Dictionary<CtmObject, DataRow> convertToDatRowFromDictionaryNew(Dictionary<CtmObject, List<CtmElement>> ctms)
        {
            Dictionary<CtmObject, DataRow> dicDataRow = new Dictionary<CtmObject, DataRow>();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));

            foreach (var ctm in ctms.Keys)
            {
                DataTable dt2 = new DataTable();
                dt2.Columns.Add("CtmName");
                for (int i = 0; i < ctms[ctm].Count; i++)
                {
                    dt2.Columns.Add(string.Format("Element_{0}", i.ToString("00")));
                }

                DataRow row = dt2.NewRow();
                List<string> els = new List<string>();
                els.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                foreach (var el in ctms[ctm])
                {
                    els.Add(el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                }
                row.ItemArray = els.ToArray();

                dicDataRow.Add(ctm, row);
            }

            return dicDataRow;
        }
        // FOA_サーバー化開発 Processing On Server Xj 2018/08/17 End
        // FOA_サーバー化開発 Processing On Server Xj 2018/08/17 Start
        //重複する名前のCtmのサフィックスを設定
        private string getCtmSuffix(List<string> ctmIdResults, string ctmId)
        {
            string suffix = string.Empty;

            int counts = ctmIdResults.Count(s => s == ctmId);
            if (counts > 1)
            {
                suffix = "(" + counts + ")";
            }

            return suffix;
        }

		// Wang New dac flow 20190308 Start
        //private void writeConfigSheetCtmNew(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        private List<IdNameObject> writeConfigSheetCtmNew(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<string, List<IdNameObject>> ctms, Dictionary<string, List<IdNameObject>> elements)
		// Wang New dac flow 20190308 End
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

			// Wang New dac flow 20190308 Start
            List<IdNameObject> missionIds = new List<IdNameObject>();
			// Wang New dac flow 20190308 End

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();


            Dictionary<CtmObject, List<CtmElement>> ctmElementDict = new Dictionary<CtmObject, List<CtmElement>>();

            // 2. Pass the Mission parameter
            int startRowIndex = 1;
            int eleStartRowIndex = 1;
            List<string> ctmIdResults = new List<string>();
            foreach (JToken missionToken in resultArray)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return missionIds;
                }

                if (int.Parse((string)missionToken["missionNewType"]) == 0)
                {
                    continue;
                }

                string missionId = (string)missionToken["id"];

                // Mission ID
                cellsParam[startRowIndex, 1].Value = missionId;

                // Mission Name
                string missionName = AisUtil.GetCatalogLangNew(missionToken);
                cellsParam[startRowIndex, 2].Value = missionName;

				// Wang New dac flow 20190308 Start
                // add to mission list
                missionIds.Add(new IdNameObject() { id = missionId, name = missionName });
				// Wang New dac flow 20190308 End

                // 3. Pass the CTM parameter
                int ctmStartRowIndex = startRowIndex;
                JArray ctmArray = JArray.Parse(missionToken["ctms"].ToString());
                foreach (JToken ctm in ctmArray)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return missionIds;
                    }

                    object checkedFlag = ctm["checkedFlag"];
                    if (Convert.ToBoolean(checkedFlag) == false)
                    {
                        continue;
                    }

                    string ctmMissionId = ((string)ctm["id"]).Split('-')[0];
                    if (ctmMissionId != missionId)
                    {
                        continue;
                    }
                    string ctmId = ((string)ctm["id"]).Split('-')[1];

                    // ParentMission ID
                    cellsParam[ctmStartRowIndex, 3].Value = missionId;

                    // CTM ID
                    cellsParam[ctmStartRowIndex, 4].Value = ctmId;
                    ctmIdResults.Add(ctmId);

                    // CTM Name
                    string suffix = getCtmSuffix(ctmIdResults, ctmId);
                    string ctmName = AisUtil.GetCatalogLangNew(ctm) + suffix;
                    cellsParam[ctmStartRowIndex, 5].Value = ctmName;

					// Wang New dac flow 20190308 Start
                    // add to ctm list
                    if (!ctms.ContainsKey(missionId)) {
                        List<IdNameObject> ctmList = new List<IdNameObject>();
                        ctms.Add(missionId, ctmList);
                    }
                    ctms[missionId].Add(new IdNameObject() { id = ctmId, name = ctmName });
					// Wang New dac flow 20190308 End

                    ++ctmStartRowIndex;
                }

                startRowIndex = ctmStartRowIndex;

                // 4. Pass the Element parameter
                JArray ctmAndEleArray = JArray.Parse(missionToken["elements"].ToString());
                foreach (JToken eleCtms in ctmAndEleArray)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return missionIds;
                    }
                    string ctmId = ((string)eleCtms["id"]).Split('-')[1];

                    JArray eleGroupArray = JArray.Parse(eleCtms["children"].ToString());
                    foreach (JToken group in eleGroupArray)
                    {
                        //AIS_DAC No.76 sunyi 2018/12/05 start
                        //group["children"]がNullの場合、チェックする
                        if (group["children"] == null)
                        {
                            //AISMM-94 sunyi 2019/03/27 start
                            //Groupがないエレメントをセット
                            object checkedFlag = group["checkedFlag"];
                            if (Convert.ToBoolean(checkedFlag) == false)
                            {
                                continue;
                            }

                            string eleMissionId = ((string)group["id"]).Split('-')[0];
                            if (eleMissionId != missionId)
                            {
                                continue;
                            }
                            string eleId = ((string)group["id"]).Split('-')[1];

                            // ParentCTM ID
                            cellsParam[eleStartRowIndex, 6].Value = ctmId;

                            // Element ID
                            cellsParam[eleStartRowIndex, 7].Value = eleId;

                            // Element Name
                            string eleName = AisUtil.GetCatalogLangNew(group);
                            cellsParam[eleStartRowIndex, 8].Value = eleName;

                            // Additional : ParentMission ID -> For deleting CTM Element in case there is duplicate CTM
                            cellsParam[eleStartRowIndex, 9].Value = missionId;

                            // Wang New dac flow 20190308 Start
                            // add to element list
                            if (!elements.ContainsKey(missionId + ctmId))
                            {
                                List<IdNameObject> elementList = new List<IdNameObject>();
                                elements.Add(missionId + ctmId, elementList);
                            }
                            elements[missionId + ctmId].Add(new IdNameObject() { id = eleId, name = eleName });
                            // Wang New dac flow 20190308 End

                            ++eleStartRowIndex;
                            //AISMM-94 sunyi 2019/03/27 end
                            continue;
                        }
                        //AIS_DAC No.76 sunyi 2018/12/05 end
                        JArray eleArray = JArray.Parse(group["children"].ToString());
                        foreach (JToken ele in eleArray)
                        {
                            object checkedFlag = ele["checkedFlag"];
                            if (Convert.ToBoolean(checkedFlag) == false)
                            {
                                continue;
                            }

                            string eleMissionId = ((string)ele["id"]).Split('-')[0];
                            if (eleMissionId != missionId)
                            {
                                continue;
                            }
                            string eleId = ((string)ele["id"]).Split('-')[1];

                            // ParentCTM ID
                            cellsParam[eleStartRowIndex, 6].Value = ctmId;

                            // Element ID
                            cellsParam[eleStartRowIndex, 7].Value = eleId;

                            // Element Name
                            string eleName = AisUtil.GetCatalogLangNew(ele);
                            cellsParam[eleStartRowIndex, 8].Value = eleName;

                            // Additional : ParentMission ID -> For deleting CTM Element in case there is duplicate CTM
                            cellsParam[eleStartRowIndex, 9].Value = missionId;

							// Wang New dac flow 20190308 Start
                            // add to element list
                            if (!elements.ContainsKey(missionId + ctmId))
                            {
                                List<IdNameObject> elementList = new List<IdNameObject>();
                                elements.Add(missionId + ctmId, elementList);
                            }
                            elements[missionId + ctmId].Add(new IdNameObject() { id = eleId, name = eleName });
							// Wang New dac flow 20190308 End

                            ++eleStartRowIndex;
                        }
                    }

                }
            }

            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;

            return missionIds;
        }

		// Wang New dac flow 20190308 Start
        //private void writeConfigSheetMissionInfoGrip(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        private List<IdNameObject> writeConfigSheetMissionInfoGrip(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<string, List<IdNameObject>> ctms, Dictionary<string, List<IdNameObject>> elements)
		// Wang New dac flow 20190308 End
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

			// Wang New dac flow 20190308 Start
            List<IdNameObject> missionIds = new List<IdNameObject>();
			// Wang New dac flow 20190308 End

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();

            if (resultArray.Count == 0) return missionIds;
            int index = 1;
            foreach (JToken missionToken in resultArray)
            {
                if (0 == int.Parse((string)missionToken["missionNewType"]))
                {
                    string missionId = missionToken["id"].ToString();
                    cellsParam[index, 1].value = missionId;

					// Wang New dac flow 20190308 Start
                    string missionName = AisUtil.GetCatalogLangNew(missionToken);
                    cellsParam[index, 2].value = missionName;

                    missionIds.Add(new IdNameObject() { id = missionId, name = missionName });
					// Wang New dac flow 20190308 End

                    index++;
                }
            }
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
            return missionIds;
        }
        // FOA_サーバー化開発 Processing On Server Xj 2018/08/17 End

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/17 Start
		// Wang New dac flow 20190308 Start
        //public void WriteCtmDataForAll(string excelName, Dictionary<ExcelConfigParam, object> configParams)
        public void WriteCtmDataForAll(string excelName, Dictionary<ExcelConfigParam, object> configParams, DateTime startDate, DateTime start, DateTime end,JArray jarryCtms)
		// Wang New dac flow 20190308 End
        {
            string folderPath = null;

            Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
            Microsoft.Office.Interop.Excel.Workbooks oWBooks = null;
            Microsoft.Office.Interop.Excel.Workbook oWBook = null;

            try
            {
                oXls = new Microsoft.Office.Interop.Excel.Application();
                oXls.Visible = false; //確認のためエクセルのウィンドウを表示する

                //エクセルファイルをオープンする
                oWBooks = oXls.Workbooks;
                oWBook = oWBooks.Open(excelName); // オープンするExcelファイル名
                //AISDAC No.71 sunyi 20181219 start
                //ISSUE_NO.744 Sunyi 2018/06/19 Start
                //最小化→正常表示
                oXls.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlMinimized;
                //ISSUE_NO.744 Sunyi 2018/06/19 End
                //AISDAC No.71 sunyi 20181219 end
                Microsoft.Office.Interop.Excel.Worksheet wsUserInfo = AisUtil.GetSheetFromSheetName_Interop(oWBook, "USERINFO");
                writeUserInfoSheet(wsUserInfo);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

				// Wang New dac flow 20190308 Start
                ClsDACConf dacConf = new ClsDACConf(oWBook);
				// Wang New dac flow 20190308 End

                // 1. Set Mission CTM & Element parameter
                string paramSheetNameM_Mission = "ミッション_MISSION";
                string paramSheetNameM_Grip = "ミッション_GRIP";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamM_Mission = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameM_Mission);
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamM_Grip = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameM_Grip);

				// Wang New dac flow 20190308 Start
                //// CTM MISSION 
                //writeConfigSheetCtmNew(worksheetParamM_Mission);
                //// GRIP MISSION
                //writeConfigSheetMissionInfoGrip(worksheetParamM_Grip);
				
                Dictionary<string, List<IdNameObject>> ctms = new Dictionary<string, List<IdNameObject>>();
                Dictionary<string, List<IdNameObject>> elements = new Dictionary<string, List<IdNameObject>>();
                // CTM MISSION 
                List<IdNameObject> cMissions = writeConfigSheetCtmNew(worksheetParamM_Mission, ctms, elements);
                // GRIP MISSION
                List<IdNameObject> gMissions = writeConfigSheetMissionInfoGrip(worksheetParamM_Grip, ctms, elements);
				// Wang New dac flow 20190308 End

                //AIS_DAC No.80 sunyi 20181210 start
                //// 既存GripMissionに関する内容をクリア
                //// Processing On Server dn 内容修正場合表示データない、Gripに関するシートを削除修正　start
                //foreach (JToken missionToken in resultArray)
                //{
                //    folderPath = (string)missionToken["resultFolder"];
                //    if (0 == int.Parse((string)missionToken["missionNewType"]) && string.IsNullOrEmpty(folderPath))
                //    {
                //        foreach (Microsoft.Office.Interop.Excel.Worksheet gripSheet in oWBook.Sheets)
                //        {
                //            if (gripSheet.Name.IndexOf("Route") != -1)
                //            {
                //                oXls.DisplayAlerts = false;
                //                gripSheet.Delete();
                //                oXls.DisplayAlerts = true;
                //            }

                //        }
                //    }
                //}
                //// Processing On Server dn 内容修正場合表示データない、Gripに関するシートを削除修正　end
                foreach (Microsoft.Office.Interop.Excel.Worksheet gripSheet in oWBook.Sheets)
                {
                    if (gripSheet.Name.IndexOf("_Routes") != -1)
                    {
                        oXls.DisplayAlerts = false;
                        gripSheet.Delete();
                        oXls.DisplayAlerts = true;
                    }
                }
                //AIS_DAC No.80 sunyi 20181210 end

                int routeIndex = 0;
                foreach (JToken missionToken in resultArray)
                {
                    folderPath = (string)missionToken["resultFolder"];

                    if (string.IsNullOrEmpty(folderPath))
                    {
                        continue;
                    }

                    if (0 == int.Parse((string)missionToken["missionNewType"]))
                    {
                        //ISSUE_NO.727 sunyi 2018/06/14 Start
                        //grip mission処理ができるように修正にする
                        routeIndex = int.Parse((string)missionToken["routeIndex"]);
                        string paramSheetNameR = routeIndex + "_Routes";
                        Microsoft.Office.Interop.Excel.Worksheet worksheetParamR = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameR);
                        //Dac No.33 sun 20190726 start
                        //writeConfigSheetRoute(worksheetParamR, folderPath)
                        writeConfigSheetRoute(worksheetParamR, folderPath, jarryCtms);
                        //Dac No.33 sun 20190726 end
                        //ISSUE_NO.727 sunyi 2018/06/14 End
                    }
                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 2. Set Start & End Time parameter
                string paramSheetNameC = "業務時間";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamC = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameC);
                writeConfigSheetTime(worksheetParamC, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                string paramSheetName = DAC.Util.Keywords.PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetParam3 = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetName);
                WriteConfigSheetParam(worksheetParam3, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = DAC.Util.Keywords.RO_PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_Interop(oWBook, roParamSheetName);
                if (this.Mode == ControlMode.MultiDac)
                {
                    MultiWriteRoParamSheet_Interop(worksheetRoParam, this.onlineId, this.workplaceId);
                }
                else
                {
                    WriteRoParamSheet_Interop(worksheetRoParam);
                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

				// Wang New dac flow 20190308 Start
                WriteInitialData(oWBook, startDate, start, end, cMissions, gMissions, ctms, elements, dacConf);
				// Wang New dac flow 20190308 End



                // 3. Save the sheet with the parameter
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Save();
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Close();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                //// 4. Re-open again with the newly inputted parameter sheet
                oWBook = (Microsoft.Office.Interop.Excel.Workbook)(oXls.Workbooks.Open(excelName));
                oXls.Visible = true; //確認のためエクセルのウィンドウを表示する
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Activate();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // Wang Issue DAC-44 20181112 Start
                //oXls.Run("MakeMenuBar");
                // Wang Issue DAC-44 20181112 End

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
                //AISDAC No.71 sunyi 20181219 start
                //ISSUE_NO.744 Sunyi 2018/06/19 Start
                //最小化→正常表示
                oXls.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlNormal;
                //ISSUE_NO.744 Sunyi 2018/06/19 End
                //EXCEL最前面へ移動
                ExcelUtil.OpenExcelFileForDAC(oXls);
                //AISDAC No.71 sunyi 20181219 end
            }
            catch (Exception err)
            {
                // ログ出力
                System.Windows.MessageBox.Show("ExcelOpen Exception Error : \n" + err.ToString());
            }
            finally
            {
                if (oWBook != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                    oWBook = null;
                }

                if (oWBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBooks);
                    oWBooks = null;
                }

                if (oXls != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                    oXls = null;
                }
            }

        }

		// Wang New dac flow 20190308 Start
        private string[] getHostAndPort(Microsoft.Office.Interop.Excel.Workbook oWBook)
        {
            string[] data = new string[2];
            data[0] = "localhost";
            data[1] = "60000";
            var cells = oWBook.Sheets["param"].Cells;
            var rangeA = getFindRange_Interop(cells, "サーバIPアドレス");
            if (rangeA != null)
            {
                data[0] = rangeA[1, 2].Value.ToString();
            }

            var rangeB = getFindRange_Interop(cells, "サーバポート番号");
            if (rangeB != null)
            {
                data[1] = rangeB[1, 2].Value.ToString();
            }

            return data;
        }

        private Microsoft.Office.Interop.Excel.Range getFindRange_Interop(Microsoft.Office.Interop.Excel.Range srchRange, String strWhat)
        {
            return srchRange.Find(strWhat,
                                System.Type.Missing,
                                Microsoft.Office.Interop.Excel.XlFindLookIn.xlValues,
                                Microsoft.Office.Interop.Excel.XlLookAt.xlWhole,
                                Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows,
                                Microsoft.Office.Interop.Excel.XlSearchDirection.xlNext,
                                false,
                                System.Type.Missing, System.Type.Missing);
        }

        private string[] GetColumnValues_G(Microsoft.Office.Interop.Excel.Workbook oWBook, int col)
        {
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
            // Multi_Dac 修正
            //Excel.Worksheet sheet = Globals.ThisWorkbook.Sheets["ミッション"];
            Microsoft.Office.Interop.Excel.Worksheet sheet = oWBook.Sheets[MISSION_GRIP];
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
            if (sheet.UsedRange.Rows.Count > 1)
            {
                Microsoft.Office.Interop.Excel.Range idRange = sheet.UsedRange.Columns[col];
                return ((System.Array)idRange.Cells.Value).OfType<object>().Select(o => o.ToString()).ToArray();
            }
            else // if only 1 element, 1 ctm, and 1 mission is selected, use the below code..
            {
                Microsoft.Office.Interop.Excel.Range cells = sheet.Cells;

                List<string> values = new List<string>();
                for (int rowIndex = 1; rowIndex <= sheet.UsedRange.Rows.Count; rowIndex++)
                {
                    if (cells[rowIndex, col].Value != null)
                        values.Add(cells[rowIndex, col].Value.ToString());
                }
                return values.ToArray();
            }
        }

        public bool SetClipBoardData(DataObject data)
        {
            return App.Current.Dispatcher.Invoke(() =>
            {
                try
                {
                    Clipboard.SetDataObject(data, false);
                    return true;
                }
#pragma warning disable
                catch (Exception e)
#pragma warning restore
                {
                    return false;
                }
            });
        }

        public void WriteInitialData(Microsoft.Office.Interop.Excel.Workbook oWBook, DateTime startDate, DateTime start, DateTime end,
            List<IdNameObject> cMissions, List<IdNameObject> gMissions,
            Dictionary<string, List<IdNameObject>> ctms, Dictionary<string, List<IdNameObject>> eles, ClsDACConf dacConf)
        {
            DateTime now = DateTime.Now;
            try
            {
                this.GetTargetObjects(oWBook);

                /*
                 * 第1階層用の処理
                 */
                // 標準時間からの経過ミリ秒（ミッション取得用）
                string startEpoMilliseconds = "";
                string endEpoMilliseconds = "";

                // 日付（SubDAC計算用）
                DateTime startDateTime;
                DateTime endDateTime;

                DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                startDateTime = startDate.Date;


                //開始時刻作成 **********************************
                TimeSpan startTimeSpan = TimeSpan.MaxValue;
                start.AddSeconds(-start.Second);
                start.AddMilliseconds(-start.Millisecond);
                oWBook.Sheets["業務時間"].Range["A1"].Value = start.ToString("HH:mm");

                startDateTime = startDateTime.AddHours(start.Hour).AddMinutes(start.Minute).AddSeconds(00).AddMilliseconds(000);;
                //startTimeSpan = startDateTime - epoch;

                //終了時刻作成 **********************************
                TimeSpan endTimeSpan = TimeSpan.MaxValue;
                end.AddSeconds(-end.Second);
                end.AddMilliseconds(-end.Millisecond);
                oWBook.Sheets["業務時間"].Range["A2"].Value = end.ToString("HH:mm");

                // 終了時刻が開始時刻より前の場合次の日の時刻にする
                if (start >= end)
                {
                    //endDateTime = startDate.Date.AddDays(1).AddHours(end.Hour).AddMinutes(end.Minute).AddSeconds(59).AddMilliseconds(999);
                    endDateTime = startDate.Date.AddDays(1).AddHours(end.Hour).AddMinutes(end.Minute).AddSeconds(00).AddMilliseconds(000);
                }
                else
                {
                    //endDateTime = startDate.Date.AddHours(end.Hour).AddMinutes(end.Minute).AddSeconds(59).AddMilliseconds(999);
                    endDateTime = startDate.Date.AddHours(end.Hour).AddMinutes(end.Minute).AddSeconds(00).AddMilliseconds(000);
                }
                //endTimeSpan = (endDateTime - epoch);

                startEpoMilliseconds = UnixTime.ToLong(startDateTime).ToString();
                endEpoMilliseconds = UnixTime.ToLong(endDateTime).ToString();

                string[] hostInfo = getHostAndPort(oWBook);
                string hostName = hostInfo[0];
                string portNo = hostInfo[1];
                string limit = "0";
                string lang = "ja";
                int datasheetIndex = 1;

                // ミッション数分だけループ
                #region C-Mission
                foreach (IdNameObject mission in cMissions)
                {
                    // JSONDATAの取得
                    string uri = "";
                    string msg = "";
                    string zipUrl = "";
                    string unZipUrl = "";

                    uri = String.Format("http://{0}:{1}/cms/rest/mib/mission/pmzip?id={2}&start={3}&end={4}&limit={5}&lang={6}&noBulky=true",
                               hostName, portNo, mission.id, startEpoMilliseconds, endEpoMilliseconds, limit, lang);

                    //情報を格納するZIPファイルパスを取得する
                    FoaCoreCom.ais.retriever.CtmDataRetriever ctmDataRetriever = new FoaCoreCom.ais.retriever.CtmDataRetriever();
                    //zipUrl = ctmDataRetriever.GetProgramMissionZipFileAsync(uri, "proxy", out msg);
                    zipUrl = ctmDataRetriever.GetProgramMissionZipFileAsync(uri, dacConf.ProxyURI, out msg);

                    //Zipファイルを解凍する
                    FoaCoreCom.ais.retriever.ZipFileHandler zipFileHandler = new FoaCoreCom.ais.retriever.ZipFileHandler();
                    unZipUrl = zipFileHandler.unZipFile(zipUrl);

                    CtlAisProcess ctlAisProcess = new CtlAisProcess();

                    // ファイル件数を取得
                    Dictionary<string, string> dicResultData = new Dictionary<string, string>();
                    DirectoryInfo folder = new DirectoryInfo(unZipUrl);
                    FileInfo[] fil = folder.GetFiles("*.csv");
                    int fileCnt = 0;
                    if (fil != null)
                    {
                        fileCnt = fil.Length;
                    }

                    // CTMごとに結果シートを作成する
                    int iCounter = 0;
                    foreach (IdNameObject ctm in ctms[mission.id])
                    {
                        /*
                         * 結果シートの追加
                         */
                        // シート名の決定
                        string SheetID = mission.name + "." + ctm.name;
                        //dac_home start
                        //string SheetName = varSheetName + datasheetIndex;
                        if (SheetID.Length > MaxNum)
                        {
                            SheetID = SheetID.Substring(0, MaxNum);
                        }
                        //実行する時、最新のミッションのDataSheetを作るため、旧DataSheetを削除する
                        foreach (Microsoft.Office.Interop.Excel.Worksheet worksheet in oWBook.Sheets)
                        {
                            //dac_home datasheet start
                            int firstdot = worksheet.Name.IndexOf('.');
                            if (firstdot == -1)
                            {
                                continue;
                            }
                            string namewithoutdot = worksheet.Name.Substring(firstdot + 1);
                            //if (worksheet.Name.EndsWith(SheetID))
                            if (namewithoutdot.Equals(SheetID))
                            {
                                //dac_home datasheet start
                                // Wang Issue reference to value of cell 20190228 Start
                                //worksheet.Delete();
                                worksheet.UsedRange.Clear();
                                // Wang Issue reference to value of cell 20190228 End
                            }
                        }
                        string SheetName = datasheetIndex + "." + SheetID;
                        //dac_home end

                        // シートの追加
                        Microsoft.Office.Interop.Excel.Worksheet newWorksheet = null;
                        try
                        {
                            newWorksheet = oWBook.Worksheets[SheetName];
                        }
#pragma warning disable
                        catch (Exception e)
#pragma warning restore
                        {

                        }
                        if (newWorksheet == null)
                        {
                            newWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)oWBook.Worksheets.Add(After: oWBook.Worksheets[oWBook.Worksheets.Count]);
                            newWorksheet.Name = SheetName;
                        }
                        newWorksheet.Select();
                        newWorksheet.Cells[1, 1].Select();
                        datasheetIndex++;

                        newWorksheet.Cells[varSheetIDRow, varSheetIDCol].Value = SheetID;

                        // 結果がない場合
                        if (fileCnt <= iCounter)
                        {
                            //FOR COM高速化 lm  2018/06/22 Modify End
                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            continue;
                        }

                        // 結果が空の場合
                        string result = "";
                        HashSet<string> elements = new HashSet<string>();
                        foreach (IdNameObject element in eles[mission.id + ctm.id])
                        {
                            elements.Add(element.id);
                        }
                        elements.Add("RT");
                        Dictionary<string, string> ctmDataDic = ctlAisProcess.ReadCtmInfoFromCSV(unZipUrl, ctm.id, dacConf.TimezoneId, elements);
                        if (ctmDataDic != null)
                        {
                            result = ctmDataDic[ctm.id];
                            if (string.IsNullOrEmpty(result))
                            {
                                newWorksheet.Cells[1, 1].Value = "データ無し";
                                continue;
                            }
                        }
                        else
                        {
                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            continue;
                        }

                        /*
                         * シート先頭行のエレメント名の挿入
                         */
                        Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)oWBook.Application.Selection;
                        // Wang New dac flow 20190308 Start
                        //rng[1, 1] = "No.";
                        rng[TitleRow, 1] = "No.";
                        // Wang New dac flow 20190308 End
                        //ISSUE_NO.748 Sunyi 2018/08/13 Start
                        //rng[1, 2] = "収集時刻";
                        // Wang New dac flow 20190308 Start
                        //rng[1, 2] = "RECEIVE TIME";
                        rng[TitleRow, 2] = "RECEIVE TIME";
                        // Wang New dac flow 20190308 End
                        //ISSUE_NO.748 Sunyi 2018/08/13 End

                        int iColCount = 3;
                        foreach (IdNameObject element in eles[mission.id + ctm.id])
                        {
                            // Wang New dac flow 20190308 Start
                            //rng[1, iColCount++] = element.name;
                            rng[TitleRow, iColCount++] = element.name;
                            // Wang New dac flow 20190308 End
                        }

                        /*
                         * 結果の貼付け
                         */
                        // コピー先のセルの指定
                        // Wang New dac flow 20190308 Start
                        //Microsoft.Office.Interop.Excel.Range pasteCell = rng.Cells[2, 1];
                        Microsoft.Office.Interop.Excel.Range pasteCell = rng.Cells[TitleRow + 1, 1];
                        // Wang New dac flow 20190308 End

                        // 一旦、メモリーストリームにバイト単位で読み込む。
                        byte[] bs = Encoding.Default.GetBytes(result);
                        MemoryStream ms = new MemoryStream(bs);

                        //クリップボードに文字列をセット
                        DataObject data = new DataObject(DataFormats.CommaSeparatedValue, ms);
                        if (!SetClipBoardData(data))
                        {
                            return;
                        }

                        Thread.Sleep(1000);

                        // コピー先にクリップボードの内容を貼り付ける。
                        newWorksheet.Paste(pasteCell, System.Type.Missing);

                        /*
                         * 演算式の挿入
                         */
                        int rowCount = newWorksheet.UsedRange.Rows.Count + 1;

                        //エレメントの計算値入力                        
                        // Wang New dac flow 20190308 Start
                        //rng[rowCount, 2] = "個数";
                        //rng[rowCount + 1, 2] = "積算";
                        //rng[rowCount + 2, 2] = "平均";
                        //rng[rowCount + 3, 2] = "最大";
                        //rng[rowCount + 4, 2] = "最小";
                        //rng[rowCount + 5, 2] = "種類数";

                        //エレメントの計算値入力
                        //dac_home start
                        rng[1, 1] = mission.name + "." + ctm.name;
                        //dac_home end
                        rng[FormulaRow, 2] = "個数";
                        rng[FormulaRow + 1, 2] = "積算";
                        rng[FormulaRow + 2, 2] = "平均";
                        rng[FormulaRow + 3, 2] = "最大";
                        rng[FormulaRow + 4, 2] = "最小";
                        rng[FormulaRow + 5, 2] = "種類数";
                        // Wang New dac flow 20190308 End

                        for (int i = 3; i < iColCount; i++)
                        {
                            string ColLetterTemp = newWorksheet.Cells[rowCount, i].Address;
                            string[] SplitLetter = ColLetterTemp.Split('$');
                            string ColLetter = SplitLetter[1];
                            // Wang New dac flow 20190308 Start
                            //rng[rowCount, i].Formula = string.Format("=COUNTA({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 1, i].Formula = string.Format("=SUM({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}2:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            //rng[rowCount + 3, i].Formula = string.Format("=MAX({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 4, i].Formula = string.Format("=MIN({0}2:{0}{1})", ColLetter, rowCount - 1);
                            
                            rng[FormulaRow, i].Formula = string.Format("=COUNTA({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 1, i].Formula = string.Format("=SUM({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}12:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            rng[FormulaRow + 3, i].Formula = string.Format("=MAX({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 4, i].Formula = string.Format("=MIN({0}12:{0}{1})", ColLetter, rowCount - 1);
                            // Wang New dac flow 20190308 End

                        }

                        /*
                         * Excelレイアウト調整
                         */
                        //罫線表示処理
                        // Wang New dac flow 20190308 Start
                        //Microsoft.Office.Interop.Excel.Range cell1 = newWorksheet.Cells[1, 1];
                        //Microsoft.Office.Interop.Excel.Range cell2 = newWorksheet.Cells[rowCount - 1, 1];
                        Microsoft.Office.Interop.Excel.Range cell1 = newWorksheet.Cells[FormulaRow, 2];
                        Microsoft.Office.Interop.Excel.Range cell2 = newWorksheet.Cells[FormulaRow + 5, iColCount - 1];
                        // Wang New dac flow 20190308 End
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        // Wang New dac flow 20190308 Start
                        //cell1 = newWorksheet.Cells[1, 2];
                        //cell2 = newWorksheet.Cells[rowCount + 5, iColCount - 1];
                        cell1 = newWorksheet.Cells[TitleRow, 1];
                        cell2 = newWorksheet.Cells[rowCount - 1, iColCount - 1];
                        // Wang New dac flow 20190308 End
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        //セルサイズを自動調整
                        newWorksheet.Columns.AutoFit();
                        //dac_home datasheet start
                        newWorksheet.Cells[1, 1].ColumnWidth = COLUMN_WIDTH;
                        //dac_home datasheet end
                    }
                }
                #endregion C-Mission

                #region G-Mission
                bool IsGripMission = false;
                Dictionary<string, ClsAttribObject> m_missions_tmp = new Dictionary<string, ClsAttribObject>();
                foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions)
                {
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                    // Multi_Dac 修正
                    if (mission.Key.Contains("_Route-"))
                    {
                        IsGripMission = true;
                        m_missions_tmp.Add(mission.Key, mission.Value);
                        continue;
                    }
                }

                if (IsGripMission)
                {
                    string[] gripMissionId;
                    JToken missionToken = JToken.Parse("{}");
                    int routeIndex = 0;
                    string resultFolder = "";
                    gripMissionId = GetColumnValues_G(oWBook, 1);

                    FoaCoreCom.ais.retriever.GripDataRetrieverDac gripDataRetrieverDac = new FoaCoreCom.ais.retriever.GripDataRetrieverDac();
                    ClsSearchInfo clsSearchInfo = new ClsSearchInfo();
                    WebProxy proxy = new WebProxy();

                    List<string> SearchIds = new List<string>();

                    foreach (var missionId in gripMissionId)
                    {
                        //URLのパラメータ
                        clsSearchInfo.MissionId = missionId;
                        clsSearchInfo.GripHostName = hostName;
                        clsSearchInfo.GripPortNo = int.Parse(portNo);
                        clsSearchInfo.SkipSameMainKey = false;
                        clsSearchInfo.SearchByInCondition = "auto";
                        clsSearchInfo.SDateUnixTimeString = startEpoMilliseconds;
                        clsSearchInfo.EDateUnixTimeString = endEpoMilliseconds;
                        clsSearchInfo.onlineId = "";
                        clsSearchInfo.SearchId = new GUID().ToString();

                        SearchIds.Add(clsSearchInfo.SearchId);

                        resultFolder = gripDataRetrieverDac.GetDacGripDatatCsvSync(clsSearchInfo, proxy, routeIndex);
                        //dac No.21 sun start
                        if (string.IsNullOrEmpty(resultFolder))
                        {
                            MessageBox.Show("削除されているミッションがあります。確認ください。");
                        }
                        //dac No.21 sun end
                        routeIndex++;
                    }

                    routeIndex = 0;
                    foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions_tmp)
                    {
                        CtlAisProcess ctlAisProcess = new CtlAisProcess();
                        // CTMごとに結果シートを作成する
                        /*
                         * 結果シートの追加
                         */
                        // シート名の決定
                        string SheetID = mission.Value.DisplayName;
                        //dac_home start
                        //string SheetName = varSheetName + datasheetIndex;
                        if (SheetID.Length > MaxNum)
                        {
                            SheetID = SheetID.Substring(0, MaxNum);
                        }
                        //実行する時、最新のミッションのDataSheetを作るため、旧DataSheetを削除する
                        foreach (Microsoft.Office.Interop.Excel.Worksheet worksheet in oWBook.Sheets)
                        {
                            //dac_home datasheet start
                            int firstdot = worksheet.Name.IndexOf('.');
                            if (firstdot == -1)
                            {
                                continue;
                            }
                            string namewithoutdot = worksheet.Name.Substring(firstdot + 1);
                            //if (worksheet.Name.EndsWith(SheetID))
                            if (namewithoutdot.Equals(SheetID))
                            {
                                //dac_home datasheet start
                                // Wang Issue reference to value of cell 20190228 Start
                                //worksheet.Delete();
                                worksheet.UsedRange.Clear();
                                // Wang Issue reference to value of cell 20190228 End
                            }
                        }
                        string SheetName = datasheetIndex + "." + SheetID;
                        //dac_home end

                        // シートの追加
                        Microsoft.Office.Interop.Excel.Worksheet newWorksheet = null;
                        try
                        {
                            newWorksheet = oWBook.Worksheets[SheetName];
                        }
#pragma warning disable
                        catch (Exception e)
#pragma warning restore
                        {

                        }
                        if (newWorksheet == null)
                        {
                            newWorksheet = (Microsoft.Office.Interop.Excel.Worksheet)oWBook.Worksheets.Add(After: oWBook.Worksheets[oWBook.Worksheets.Count]);
                            newWorksheet.Name = SheetName;
                        }
                        newWorksheet.Select();
                        newWorksheet.Cells[1, 1].Select();
                        datasheetIndex++;

                        newWorksheet.Cells[varSheetIDRow, varSheetIDCol].Value = SheetID;

                        // 結果が空の場合
                        string result = "";
                        string pathString = "";
                        foreach (var searchId in SearchIds)
                        {
                            pathString = gripDataRetrieverDac.DlDir + "\\" + searchId + "\\" + mission.Value.Id + ".csv";
                            if (File.Exists(pathString))
                            {
                                pathString = gripDataRetrieverDac.DlDir + "\\" + searchId;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        Dictionary<string, string> ctmDataDic = ctlAisProcess.ReadCtmInfoFromCSV(pathString, mission.Value.Id, dacConf.TimezoneId, null);
                        if (ctmDataDic != null)
                        {
                            result = ctmDataDic[mission.Value.Id];
                            if (string.IsNullOrEmpty(result))
                            {
                                newWorksheet.Cells[1, 1].Value = "データ無し";
                                continue;
                            }
                        }
                        //FOR COM高速化 lm  2018/06/21 Modify End
                        else
                        {
                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            continue;
                        }

                        /*
                         * シート先頭行のエレメント名の挿入
                         */
                        Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)oWBook.Application.Selection;
                        // Wang New dac flow 20190308 Start
                        //rng[1, 1] = "No.";
                        rng[TitleRow, 1] = "No.";
                        // Wang New dac flow 20190308 End
                        int iColCount = 2;
                        string csvFile = pathString + "\\" + mission.Value.Id + ".csv";
                        using (var reader = new CsvReader(new StreamReader(csvFile, Encoding.GetEncoding("UTF-8"))))
                        {
                            reader.Read();
                            var headers = reader.FieldHeaders;
                            foreach (var header in headers)
                            {
                                // Wang New dac flow 20190308 Start
                                //rng[1, iColCount++] = header;
                                rng[TitleRow, iColCount++] = header;
                                // Wang New dac flow 20190308 End
                            }
                        }
                        /*
                         * 結果の貼付け
                         */
                        // コピー先のセルの指定
                        // Wang New dac flow 20190308 Start
                        //Microsoft.Office.Interop.Excel.Range pasteCell = rng.Cells[2, 1];
                        Microsoft.Office.Interop.Excel.Range pasteCell = rng.Cells[TitleRow + 1, 1];
                        // Wang New dac flow 20190308 End

                        // 一旦、メモリーストリームにバイト単位で読み込む。
                        byte[] bs = Encoding.Default.GetBytes(result);
                        MemoryStream ms = new MemoryStream(bs);

                        //クリップボードに文字列をセット
                        DataObject data = new DataObject(DataFormats.CommaSeparatedValue, ms);
                        if (!SetClipBoardData(data))
                        {
                            return;
                        }

                        Thread.Sleep(1000);

                        // コピー先にクリップボードの内容を貼り付ける。
                        newWorksheet.Paste(pasteCell, System.Type.Missing);

                        /*
                         * 演算式の挿入
                         */
                        // 最終行の設定
                        // Wang New dac flow 20190308 Start
                        //int rowCount = result.Split('\n').Length + 1;

                        ////エレメントの計算値入力                        
                        //rng[rowCount, 1] = "個数";

                        //rng[rowCount + 1, 1] = "積算";
                        //rng[rowCount + 2, 1] = "平均";
                        //rng[rowCount + 3, 1] = "最大";
                        //rng[rowCount + 4, 1] = "最小";
                        //rng[rowCount + 5, 1] = "種類数";
                        
                        int rowCount = newWorksheet.UsedRange.Rows.Count + 1;

                        //エレメントの計算値入力
                        //dac_home start
                        rng[1, 1] = mission.Value.DisplayName;
                        //dac_home end
                        rng[FormulaRow, 1] = "個数";

                        rng[FormulaRow + 1, 1] = "積算";
                        rng[FormulaRow + 2, 1] = "平均";
                        rng[FormulaRow + 3, 1] = "最大";
                        rng[FormulaRow + 4, 1] = "最小";
                        rng[FormulaRow + 5, 1] = "種類数";
                        // Wang New dac flow 20190308 End

                        for (int i = 2; i < iColCount; i++)
                        {
                            string ColLetterTemp = newWorksheet.Cells[rowCount, i].Address;
                            string[] SplitLetter = ColLetterTemp.Split('$');
                            string ColLetter = SplitLetter[1];
                            // Wang New dac flow 20190308 Start
                            //rng[rowCount, i].Formula = string.Format("=COUNTA({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 1, i].Formula = string.Format("=SUM({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}2:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            //rng[rowCount + 3, i].Formula = string.Format("=MAX({0}2:{0}{1})", ColLetter, rowCount - 1);
                            //rng[rowCount + 4, i].Formula = string.Format("=MIN({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow, i].Formula = string.Format("=COUNTA({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 1, i].Formula = string.Format("=SUM({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}12:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            rng[FormulaRow + 3, i].Formula = string.Format("=MAX({0}12:{0}{1})", ColLetter, rowCount - 1);
                            rng[FormulaRow + 4, i].Formula = string.Format("=MIN({0}12:{0}{1})", ColLetter, rowCount - 1);
                            // Wang New dac flow 20190308 End

                        }

                        /*
                         * Excelレイアウト調整
                         */
                        //罫線表示処理
                        // Wang New dac flow 20190308 Start
                        //Microsoft.Office.Interop.Excel.Range cell1 = newWorksheet.Cells[1, 1];
                        //Microsoft.Office.Interop.Excel.Range cell2 = newWorksheet.Cells[rowCount - 1, 1];
                        Microsoft.Office.Interop.Excel.Range cell1 = newWorksheet.Cells[FormulaRow, 2];
                        Microsoft.Office.Interop.Excel.Range cell2 = newWorksheet.Cells[FormulaRow + 5, iColCount - 1];
                        // Wang New dac flow 20190308 End
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        // Wang New dac flow 20190308 Start
                        //cell1 = newWorksheet.Cells[1, 2];
                        //cell2 = newWorksheet.Cells[rowCount + 5, iColCount - 1];
                        cell1 = newWorksheet.Cells[TitleRow, 1];
                        cell2 = newWorksheet.Cells[rowCount - 1, iColCount - 1];
                        // Wang New dac flow 20190308 End
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        //セルサイズを自動調整
                        newWorksheet.Columns.AutoFit();
                        //dac_home datasheet start
                        newWorksheet.Cells[1, 1].ColumnWidth = COLUMN_WIDTH;
                        //dac_home datasheet end
                    }
                }
                #endregion G-Mission

#if false
               if (IsGripMission)
                {
                    string[] gripMissionId;
                    JToken missionToken = JToken.Parse("{}");
                    int routeIndex = 0;
                    string resultFolder = "";
                    gripMissionId = GetColumnValues_G(1);

                    GripDataRetrieverDac gripDataRetrieverDac = new FoaCoreCom.ais.retriever.GripDataRetrieverDac();
                    ClsSearchInfo clsSearchInfo = new ClsSearchInfo();
                    WebProxy proxy = new WebProxy();

                    List<string> SearchIds = new List<string>();

                    foreach (var missionId in gripMissionId)
                    {
                        //URLのパラメータ
                        clsSearchInfo.MissionId = missionId;
                        clsSearchInfo.GripHostName = Globals.Sheet1.m_MissionHostName;
                        clsSearchInfo.GripPortNo = int.Parse(Globals.Sheet1.m_MissionPortNo);
                        clsSearchInfo.SkipSameMainKey = false;
                        clsSearchInfo.SearchByInCondition = "auto";
                        clsSearchInfo.SDateUnixTimeString = startEpoMilliseconds;
                        clsSearchInfo.EDateUnixTimeString = endEpoMilliseconds;
                        clsSearchInfo.onlineId = "";
                        clsSearchInfo.SearchId = new GUID().ToString();

                        SearchIds.Add(clsSearchInfo.SearchId);

                        resultFolder = gripDataRetrieverDac.GetDacGripDatatCsvSync(clsSearchInfo, proxy, routeIndex);

                        routeIndex++;
                    }

                    routeIndex = 0;
                    int datasheetIndex = 1;
                    foreach (KeyValuePair<string, ClsAttribObject> mission in m_missions_tmp)
                    {
                        CtlAisProcess ctlAisProcess = new CtlAisProcess();
                        // CTMごとに結果シートを作成する
                        /*
                         * 結果シートの追加
                         */
                        // シート名の決定
                        string SheetID = mission.Value.DisplayName;
                        string SheetName = varSheetName + datasheetIndex;

                        // シートの追加
                        Excel.Worksheet newWorksheet = null;
                        try
                        {
                            newWorksheet = Globals.ThisWorkbook.Worksheets[SheetName];
                        }
                        catch (Exception e)
                        {

                        }
                        if (newWorksheet == null)
                        {
                            newWorksheet = (Excel.Worksheet)Globals.ThisWorkbook.Worksheets.Add(After: Globals.ThisWorkbook.Worksheets[Globals.ThisWorkbook.Worksheets.Count]);
                            newWorksheet.Name = SheetName;
                        }
                        newWorksheet.Select();
                        newWorksheet.Cells[1, 1].Select();
                        datasheetIndex++;

                        newWorksheet.Cells[varSheetIDRow, varSheetIDCol].Value = SheetID;

                        // 結果が空の場合
                        string result = "";
                        string pathString = "";
                        foreach (var searchId in SearchIds)
                        {
                            pathString = gripDataRetrieverDac.DlDir + "\\" + searchId + "\\" + mission.Value.Id + ".csv";
                            if (File.Exists(pathString))
                            {
                                pathString = gripDataRetrieverDac.DlDir + "\\" + searchId;
                                break;
                            }
                            else
                            {
                                continue;
                            }
                        }
                        Dictionary<string, string> ctmDataDic = ctlAisProcess.ReadCtmInfoFromCSV(pathString, mission.Value.Id, Globals.ThisWorkbook.clsDACConf.TimezoneId, null);
                        if (ctmDataDic != null)
                        {
                            result = ctmDataDic[mission.Value.Id];
                            if (string.IsNullOrEmpty(result))
                            {
                                newWorksheet.Cells[1, 1].Value = "データ無し";
                                continue;
                            }
                        }
                        //FOR COM高速化 lm  2018/06/21 Modify End
                        else
                        {
                            newWorksheet.Cells[1, 1].Value = "データ無し";
                            continue;
                        }

                        /*
                         * シート先頭行のエレメント名の挿入
                         */
                        rng = (Excel.Range)Globals.ThisWorkbook.Application.Selection;
                        DateTime epoch = new DateTime(1970, 1, 1, 9, 0, 0);
                        rng[1, 1] = "No.";
                        int iColCount = 2;
                        string csvFile = pathString + "\\" + mission.Value.Id + ".csv";
                        using (var reader = new CsvReader(new StreamReader(csvFile, Encoding.GetEncoding("UTF-8"))))
                        {
                            reader.Read();
                            var headers = reader.FieldHeaders;
                            foreach (var header in headers)
                            {
                                rng[1, iColCount++] = header;
                            }
                        }
                        /*
                         * 結果の貼付け
                         */
                        // コピー先のセルの指定
                        Excel.Range pasteCell = rng.Cells[2, 1];

                        // 一旦、メモリーストリームにバイト単位で読み込む。
                        byte[] bs = Encoding.Default.GetBytes(result);
                        MemoryStream ms = new MemoryStream(bs);

                        //クリップボードに文字列をセット
                        DataObject data = new DataObject(DataFormats.CommaSeparatedValue, ms);
                        Clipboard.SetDataObject(data, false);

                        Thread.Sleep(1000);

                        // コピー先にクリップボードの内容を貼り付ける。
                        newWorksheet.Paste(pasteCell, System.Type.Missing);

                        /*
                         * 演算式の挿入
                         */
                        // 最終行の設定
                        int rowCount = result.Split('\n').Length + 1;

                        //エレメントの計算値入力                        
                        rng[rowCount, 1] = "個数";

                        rng[rowCount + 1, 1] = "積算";
                        rng[rowCount + 2, 1] = "平均";
                        rng[rowCount + 3, 1] = "最大";
                        rng[rowCount + 4, 1] = "最小";
                        rng[rowCount + 5, 1] = "種類数";

                        for (int i = 2; i < iColCount; i++)
                        {
                            string ColLetterTemp = newWorksheet.Cells[rowCount, i].Address;
                            string[] SplitLetter = ColLetterTemp.Split('$');
                            string ColLetter = SplitLetter[1];
                            rng[rowCount, i].Formula = string.Format("=COUNTA({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 1, i].Formula = string.Format("=SUM({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 2, i].Formula = string.Format("=IFERROR(AVERAGE({0}2:{0}{1}),\"\")", ColLetter, rowCount - 1);
                            rng[rowCount + 3, i].Formula = string.Format("=MAX({0}2:{0}{1})", ColLetter, rowCount - 1);
                            rng[rowCount + 4, i].Formula = string.Format("=MIN({0}2:{0}{1})", ColLetter, rowCount - 1);

                        }

                        /*
                         * Excelレイアウト調整
                         */
                        //罫線表示処理
                        Excel.Range cell1 = newWorksheet.Cells[1, 1];
                        Excel.Range cell2 = newWorksheet.Cells[rowCount - 1, 1];
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        cell1 = newWorksheet.Cells[1, 2];
                        cell2 = newWorksheet.Cells[rowCount + 5, iColCount - 1];
                        rng = newWorksheet.get_Range(cell1, cell2);
                        rng.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                        //セルサイズを自動調整
                        newWorksheet.Columns.AutoFit();

                    }
                }
#endif
                ////////////////////////////////////////////////////////////////////////////////////////


                // 更新処理
                oWBook.Sheets[DAC_SHEET].Select();
                oWBook.Application.Calculation = Microsoft.Office.Interop.Excel.XlCalculation.xlCalculationAutomatic;
                oWBook.Application.Calculate();
                oWBook.Application.ScreenUpdating = true;
                oWBook.Application.DisplayAlerts = true;
#if false
                if (Globals.ThisWorkbook.clsDACConf.MonitorLevel.Equals(ClsMissionLog.LOG_MONITOR_TYPE.SIMPLE))
                {
                    if (logViewerProcess != null && !logViewerProcess.HasExited)
                        logViewerProcess.Kill();
                }
#endif
            }
            //Mi失敗時
#pragma warning disable
            catch (Exception ex)
#pragma warning restore
            {
                oWBook.Sheets[DAC_SHEET].Select();
                oWBook.Application.DisplayAlerts = true;

                // 更新処理
                oWBook.Application.ScreenUpdating = true;
                oWBook.Application.Calculation = Microsoft.Office.Interop.Excel.XlCalculation.xlCalculationAutomatic;
                oWBook.Application.Calculate();
            }
            finally
            {
            }
        }

        /// <summary>
        /// Excelの１列の値を取得する
        /// </summary>
        private string[] GetColumnValues(Microsoft.Office.Interop.Excel.Workbook oWBook, int col)
        {
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
            // Multi_Dac 修正
            //Excel.Worksheet sheet = Globals.ThisWorkbook.Sheets["ミッション"];
            Microsoft.Office.Interop.Excel.Worksheet sheet = oWBook.Sheets[MISSION_M];
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
            if (sheet.UsedRange.Rows.Count > 1)
            {
                Microsoft.Office.Interop.Excel.Range idRange = sheet.UsedRange.Columns[col];
                return ((System.Array)idRange.Cells.Value).OfType<object>().Select(o => o.ToString()).ToArray();
            }
            else // if only 1 element, 1 ctm, and 1 mission is selected, use the below code..
            {
                Microsoft.Office.Interop.Excel.Range cells = sheet.Cells;

                List<string> values = new List<string>();
                for (int rowIndex = 1; rowIndex <= sheet.UsedRange.Rows.Count; rowIndex++)
                {
                    if (cells[rowIndex, col].Value != null)
                        values.Add(cells[rowIndex, col].Value.ToString());
                }
                return values.ToArray();
            }
        }

        /// <summary>
        /// 親子関係表すオブジェクトを作成する
        /// </summary>
        private void BuildDictionary(string[] pids, string[] ids, string[] displayNames, Dictionary<string, Dictionary<string, ClsAttribObject>> dict)
        {
            for (int i = 0; i < ids.Length; i++)
            {
                //親ID
                string pid = (string)pids.GetValue(i);
                //ID
                string id = (string)ids.GetValue(i);
                //表示名
                string displayName = (string)displayNames.GetValue(i);

                ClsAttribObject obj = new ClsAttribObject(pid, id, displayName);
                Dictionary<string, ClsAttribObject> list;

                if (dict.ContainsKey(pid))
                {
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                    // Multi_Dac 修正
                    if (!dict[pid].ContainsKey(id))
                    {
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
                        dict[pid].Add(id, obj);
                    }
                    //親IDは既に存在する場合はオブジェクトリストに追加する
                }
                else
                {
                    //親IDは存在しない場合はオブジェクトリストを作成する
                    list = new Dictionary<string, ClsAttribObject>();
                    list.Add(id, obj);
                    dict.Add(pid, list);
                }
            }
        }

        private void BuildDictionary(string[] pids1, string[] pids2, string[] ids, string[] displayNames, Dictionary<string, Dictionary<string, ClsAttribObject>> dict)
        {
            for (int i = 0; i < ids.Length; i++)
            {
                //親ID
                string pid = (string)pids1.GetValue(i) + (string)pids2.GetValue(i);
                //ID
                string id = (string)ids.GetValue(i);
                //表示名
                string displayName = (string)displayNames.GetValue(i);

                ClsAttribObject obj = new ClsAttribObject(pid, id, displayName);
                Dictionary<string, ClsAttribObject> list;

                if (dict.ContainsKey(pid))
                {
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                    // Multi_Dac 修正
                    if (!dict[pid].ContainsKey(id))
                    {
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 End
                        //親IDは既に存在する場合はオブジェクトリストに追加する
                        dict[pid].Add(id, obj);
                    }
                }
                else
                {
                    //親IDは存在しない場合はオブジェクトリストを作成する
                    list = new Dictionary<string, ClsAttribObject>();
                    list.Add(id, obj);
                    dict.Add(pid, list);
                }
            }
        }

        /// <summary>
        /// 親子関係表すオブジェクトを作成する
        /// </summary>
        private void BuildDictionary_Grip(string pids, string displayNames, Dictionary<string, Dictionary<string, ClsAttribObject>> dict)
        {
            //親ID
            string pid = pids;
            //ID
            string id = displayNames;
            //表示名
            string displayName = displayNames;

            ClsAttribObject obj = new ClsAttribObject(pid, id, displayName);
            Dictionary<string, ClsAttribObject> list;

            if (dict.ContainsKey(pid))
            {
                if (!dict[pid].ContainsKey(id))
                {
                    dict[pid].Add(id, obj);
                }
                //親IDは既に存在する場合はオブジェクトリストに追加する
            }
            else
            {
                //親IDは存在しない場合はオブジェクトリストを作成する
                list = new Dictionary<string, ClsAttribObject>();
                list.Add(id, obj);
                dict.Add(pid, list);
            }
        }

        /// <summary>
        /// 前画面で設定したMission、CTM、ELEMENTを取得する
        /// </summary>
        private void GetTargetObjects(Microsoft.Office.Interop.Excel.Workbook oWBook)
        {
            string[] pids;
            string[] ids;
            string[] displayNames;

            //Missionを読み込む
            ids = GetColumnValues(oWBook, 1);
            displayNames = GetColumnValues(oWBook, 2);
            for (int i = 0; i < ids.Length; i++)
            {
                string id = (string)ids.GetValue(i);
                string displayName = (string)displayNames.GetValue(i);
                m_missions.Add(id, new ClsAttribObject(id, id, displayName));
            }

            //CTMを読み込む
            pids = GetColumnValues(oWBook, 3);
            ids = GetColumnValues(oWBook, 4);
            displayNames = GetColumnValues(oWBook, 5);
            BuildDictionary(pids, ids, displayNames, m_ctms);

            //Elementを読み込む
            string[] pids1;
            string[] pids2;
            pids1 = GetColumnValues(oWBook, 9); // Use MissionID as pids1
            pids2 = GetColumnValues(oWBook, 6); // Use CtmID as pids2
            ids = GetColumnValues(oWBook, 7);
            displayNames = GetColumnValues(oWBook, 8);
            BuildDictionary(pids1, pids2, ids, displayNames, m_elements);
            // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
            // Multi_Dac 修正
            Microsoft.Office.Interop.Excel.Range routesRange = null;
            try
            {
                int routeIndex = 0;
                for (int i = 1; i <= oWBook.Sheets.Count; i++)
                {
                    bool IsGrip = false;
                    Microsoft.Office.Interop.Excel.Worksheet routesSheet = oWBook.Sheets[i];
                    if (routesSheet.Name.Equals(routeIndex.ToString() + MISSION_G))
                    {
                        routesRange = routesSheet.UsedRange;
                        IsGrip = true;
                        routeIndex++;
                    }
                    if (IsGrip)
                    {
                        string routeMissionName = "";
                        string routeCtmName = "";
                        string routeElementName = "";
                        for (int j = 1; j <= routesRange.Rows.Count; j++)
                        {
                            Microsoft.Office.Interop.Excel.Range routesRange1 = routesRange.Cells[j, 1];
                            string gripMission = routesRange1.Value;
                            Microsoft.Office.Interop.Excel.Range routesRange2 = routesRange.Cells[j, 2];
                            string gripCtm = routesRange2.Value;
                            Microsoft.Office.Interop.Excel.Range routesRange3 = routesRange.Cells[j, 3];
                            string gripElement = routesRange3.Value;
                            if (gripMission != null)
                            {
                                routeMissionName = gripMission;
                                m_missions.Add(routeMissionName, new ClsAttribObject(routeMissionName, routeMissionName, routeMissionName));
                                if (gripCtm != null)
                                {
                                    routeCtmName = gripCtm;
                                    BuildDictionary_Grip(routeMissionName, routeCtmName, m_ctms);

                                    if (gripElement != null)
                                    {
                                        routeElementName = gripElement;
                                        BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                    }
                                }
                            }
                            if (gripMission == null && gripCtm != null)
                            {
                                if (gripCtm != null)
                                {
                                    routeCtmName = gripCtm;
                                    BuildDictionary_Grip(routeMissionName, routeCtmName, m_ctms);

                                    if (gripElement != null)
                                    {
                                        routeElementName = gripElement;
                                        BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                    }
                                }
                            }
                            if (gripMission == null && gripCtm != null)
                            {
                                if (gripCtm != null)
                                {
                                    routeCtmName = gripCtm;
                                    BuildDictionary_Grip(routeMissionName, routeCtmName, m_ctms);

                                    if (gripElement != null)
                                    {
                                        routeElementName = gripElement;
                                        BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                    }
                                }
                            }
                            if (gripMission == null && gripCtm == null && gripElement != null)
                            {
                                if (gripElement != null)
                                {
                                    routeElementName = gripElement;
                                    BuildDictionary_Grip(routeCtmName, routeElementName, m_elements);
                                }
                            }
                            //Dac No.33 sun 20190726 start
                            //if (gripMission == null && gripCtm == null && gripElement == null)
                            //{
                            //    break;
                            //}                
                            //Dac No.33 sun 20190726 end

                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
		// Wang New dac flow 20190308 End

        // FOA_サーバー化開発 Processing On Server Xj 2018/08/17 End
        // FOA_サーバー化開発 Processing On Server Xj 2018/08/17 Start
        protected void writeData3(Microsoft.Office.Interop.Excel.Application excel, Microsoft.Office.Interop.Excel.Workbook book, DirectoryInfo root)
        {
            Dictionary<CtmObject, DataRow> dicDataRows = convertToDatRowFromDictionaryNew(this.ctmElementDic);
            List<CtmObject> ctms = convertToListFromDictionary(this.ctmElementDic);

            List<string> ctmIdResults = new List<string>();
            List<XYZ> srcCsvFiles = null;
            try
            {
                srcCsvFiles = new List<XYZ>();
                foreach (var ctm in ctms)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var ctmId = ctm.id.ToString();
                    ctmIdResults.Add(ctmId);

                    var missionId = ctm.missionId;
                    var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    string suffix = getCtmSuffix(ctmIdResults, ctmId);
                    var ctmSheetName = ctmName + suffix;

                    DataRow dataRow = null;
                    foreach (var itemRows in dicDataRows)
                    {
                        if (missionId == itemRows.Key.missionId && ctmName == itemRows.Value.ItemArray[0].ToString())
                        {
                            dataRow = itemRows.Value;
                            break;
                        }
                    }

                    if (dataRow == null)
                    {
                        // CTM自体の選択が無かったので無視
                        continue;
                    }

                    // シート検索 / 作成
                    Microsoft.Office.Interop.Excel.Worksheet sheet = AisUtil.GetSheetFromSheetName_Interop(book, ctmSheetName);
                    sheet.Rows.Clear();

                    CsvFile csvFileObj = null;
                    foreach (DirectoryInfo di in root.GetDirectories())
                    {
                        string srcCsvPath = Path.Combine(di.FullName, ctmId + ".csv");
                        if (File.Exists(srcCsvPath))
                        {
                            csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcCsvPath };
                            var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                            srcCsvFiles.Add(srcCsvFile);
                        }
                    }

                    writeByCtmResultSheet(sheet, csvFileObj, ctm, dataRow, this.bw);
                    /*
                    var srcFilepath = Path.Combine(folderPath, ctmId + ".csv");
                    if (File.Exists(srcFilepath))
                    {
                        csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcFilepath };
                        var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                        srcCsvFiles.Add(srcCsvFile);
                    }

                    writeByCtmResultSheet(sheet, csvFileObj, ctm, dataRow, this.bw);
                    */
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // 次のシートへ
                    sheet.UsedRange.Columns.AutoFit();
                }
            }
            finally
            {
                if (srcCsvFiles != null)
                {
                    foreach (XYZ srcCsvFile in srcCsvFiles)
                    {
                        srcCsvFile.Dispose();
                    }
                }
            }
        }


        // FOA_サーバー化開発 Processing On Server Xj 2018/08/17 End

        //ISSUE_NO.727 sunyi 2018/06/14 Start
        //grip mission処理ができるように修正にする
        private void writeConfigSheetMissionInfo(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();
            cellsParam[1, 1].value = this.gripMission.Id;
            cellsParam[1, 2].value = AisUtil.GetCatalogLang(this.gripMission);
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        //Dac No.33 sun 20190726 start
        //private void writeConfigSheetRoute(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, string folderPath)
        private void writeConfigSheetRoute(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, string folderPath, JArray jarryCtms)
        //Dac No.33 sun 20190726 end
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            var writer = new DAC.AExcel.Grip.GripResultSheetWriter(this.Mode, this.Ctms, this.dtOutput, this.bw);

            // シート検索 / 作成
            worksheetParam.Rows.Clear();

            DirectoryInfo di = new DirectoryInfo(folderPath);

            Dictionary<string, List<string>> routes = new Dictionary<string, List<string>>();

            // 2. Pass the Mission parameter
            int fileIndex = 1;
            int IsCtmIndex = 0;
            int elIndex = 0;
            int RouteIndexRow = 1;
            for (fileIndex = 1; fileIndex <= di.GetFiles().Count(); fileIndex = fileIndex + 2)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                string routeName = System.IO.Path.GetFileNameWithoutExtension(di.GetFiles()[fileIndex].FullName);

                //Dac No.33 sun 20190726 start
                JArray jArrayCtmTmp = new JArray();
                foreach (var ctm in jarryCtms)
                {
                    JToken token = ctm as JToken;
                    if (token["checkedFlag"].ToString().Equals("True"))
                    {
                        jArrayCtmTmp.Add(ctm);
                    }
                }

                bool isChecked = false;
                foreach (var ctmTmp in jArrayCtmTmp)
                {
                    JToken token = ctmTmp as JToken;
                    if (token["name"] == null) continue;
                    string[] rowsArray = token["name"].ToString().Split('_');
                    string name = rowsArray[1] + "_" + rowsArray[2];
                    if (routeName.Contains(name))
                    {
                        isChecked = true;
                        break;
                    }
                }
                if (!isChecked)
                {
                    continue;
                }
                //Dac No.33 sun 20190726 end

                //write routeName
                cellsParam[RouteIndexRow, 1].value = routeName;

                if (di.GetFiles()[fileIndex].Extension.ToUpper() == ".H")
                {
                    var reader = new CsvReader(new StreamReader(di.GetFiles()[fileIndex].FullName, Encoding.GetEncoding("Shift_JIS")));

                    reader.Read();
                    var headersIsCtm = reader.FieldHeaders;

                    List<string> rtPosList = new List<string>();

                    //get ctm name header index 
                    for (IsCtmIndex = 0; IsCtmIndex < headersIsCtm.Count(); IsCtmIndex++)
                    {
                        rtPosList.Add(headersIsCtm[IsCtmIndex]);
                    }

                    var readerAllName = new CsvReader(new StreamReader(di.GetFiles()[fileIndex - 1].FullName, Encoding.GetEncoding("Shift_JIS")));
                    readerAllName.Read();

                    var headerAllName = readerAllName.FieldHeaders;

                    for (elIndex = 0; elIndex < headerAllName.Count(); elIndex++)
                    {

                        if (rtPosList.Contains((elIndex + 1).ToString()))
                        {
                            //dac_home start
                            if (!rtPosList.Contains((elIndex + 3).ToString()))
                            {
                                //write ctm
                                cellsParam[RouteIndexRow + elIndex, 2].value = headerAllName[elIndex];
                                RouteIndexRow = RouteIndexRow - 1;
                            }
                            else
                            {
                                RouteIndexRow = RouteIndexRow - 2;
                            }
                            //dac_home end
                        }
                        else
                        {
                            //DAC-82 sunyi 20181225 start
                            //エレメント欄に表示するのは、CTMのエレメントのみ,受信時刻はエレメントではないので表示しない
                            if (headerAllName[elIndex].Contains("受信時刻_")) continue;
                            //DAC-82 sunyi 20181225 end
                            //write el
                            cellsParam[RouteIndexRow + elIndex, 3].value = headerAllName[elIndex];
                        }
                    }

                    RouteIndexRow = RouteIndexRow - 1 + headerAllName.Count();
                }
            }
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }
        //ISSUE_NO.727 sunyi 2018/06/14 End

        //ISSUE_NO.727 sunyi 2018/06/14 Start
        //grip mission処理ができるように修正にする
        private void writeGripResultSheets(Microsoft.Office.Interop.Excel.Workbook workbook, string folderPath)
        {
            var writer = new DAC.AExcel.Grip.GripResultSheetWriter(this.Mode, this.Ctms, this.dtOutput, this.bw);

            DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (var fi in di.GetFiles())
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (fi.Extension.ToUpper() != ".CSV")
                {
                    continue;
                }

                // シート検索 / 作成
                Microsoft.Office.Interop.Excel.Worksheet worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, AisUtil.RemoveExtention(fi.Name));
                worksheet.Rows.Clear();
                writer.WriteByCtmResultSheetSimple(worksheet, fi.FullName, null, null);

                // 次のシートへ
                worksheet.UsedRange.Columns.AutoFit();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
        }
        //ISSUE_NO.727 sunyi 2018/06/14 End

        private List<CtmObject> convertToListFromDictionary(Dictionary<CtmObject, List<CtmElement>> elementMap)
        {
            List<CtmObject> ctms = new List<CtmObject>();
            foreach (CtmObject ctm in elementMap.Keys)
            {
                ctms.Add(ctm);
            }

            return ctms;
        }
        private static void writeByCtmResultSheet(Microsoft.Office.Interop.Excel.Worksheet worksheet, CsvFile csvFile,
         CtmObject originalCtm, DataRow row, BackgroundWorker bw)
        {
            Dictionary<string, string> id2Name = new Dictionary<string, string>();
            foreach (var element in originalCtm.GetAllElements())
            {
                id2Name.Add(element.id.ToString(), element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
            }

            Microsoft.Office.Interop.Excel.Range cells = worksheet.Cells;

            // Header
            int headCount = 1;
            cells[1, 1].Value = Keywords.RECEIVE_TIME;

            // エレメント名
            int headerIndex = 2;
            bool firstColumn = true;
            foreach (var item in row.ItemArray)
            {
                // Cancel Check
                if (bw.CancellationPending)
                {
                    return;
                }

                if (firstColumn)
                {
                    firstColumn = false;
                    continue;
                }

                var strVal = item as string;
                if (string.IsNullOrEmpty(strVal))
                {
                    continue;
                }

                cells[1, headerIndex].Value = item;
                headerIndex++;
                headCount++;

            }

            cells[1, headerIndex].Value = Keywords.RT;
            cells[1, headerIndex + 1].Value = Keywords.CTM_ID;

            if (csvFile == null)
            {
                return;
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }

            object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headCount + 2];
            string ctmId = System.IO.Path.GetFileNameWithoutExtension(csvFile.Filepath);
            using (var sr = new StreamReader(csvFile.Filepath, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var headers = reader.FieldHeaders;


                var pos2Pos = new Dictionary<int, int>();

                for (int i = 1; i < headers.Length; i++)
                {
                    var id1 = headers[i];

                    if (!id2Name.ContainsKey(id1)) continue;

                    var name1 = id2Name[id1];
                    int pos = GetPositionInDataRow(row, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 1); // RT
                    }
                }


                // Loop over rows
                int rowIndex = 2;
                int blockRowIndex = 0;
                do
                {
                    // Cancel Check
                    if (bw.CancellationPending)
                    {
                        return;
                    }

                    var rec = reader.CurrentRecord;

                    long rt = Int64.Parse(rec[0]);

                    values[blockRowIndex, 0] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);

                    for (int j = 1; j < rec.Length; j++)
                    {
                        int columnIndex;
                        if (pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            string item = rec[j];
                            if (item.StartsWith("=="))
                            {
                                item = "'" + item;
                            }

                            values[blockRowIndex, columnIndex] = item;
                        }
                    }
                    values[blockRowIndex, headCount] = rt.ToString();
                    values[blockRowIndex, headCount + 1] = ctmId;

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                        range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, headCount + 2);
                        range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                        // RT Format
                        Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 1];
                        range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;
                    }
                } while (reader.Read());

                // Cancel Check
                if (bw.CancellationPending)
                {
                    return;
                }

                if (0 != blockRowIndex)
                {
                    Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                    range = range.get_Resize(blockRowIndex, headCount + 2);
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // RT Format
                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 1];
                    range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                    range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                }
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }
            //ISSUE_NO.728 sunyi 2018/06/25 Start
            //仕様変更、resultシートを表示する
            //worksheet.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
            //ISSUE_NO.728 sunyi 2018/06/25 End
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
    }
}
