﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.AExcel
{
    class SSGUtil
    {
        public static SpreadsheetGear.IRange GetFindRange(SpreadsheetGear.IRange srchRange, String strWhat)
        {
            return srchRange.Find(strWhat,
                                null,
                                SpreadsheetGear.FindLookIn.Values,
                                SpreadsheetGear.LookAt.Whole,
                                SpreadsheetGear.SearchOrder.ByRows,
                                SpreadsheetGear.SearchDirection.Next,
                                true);
        }
    }
}
