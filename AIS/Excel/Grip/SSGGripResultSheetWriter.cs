﻿using DAC.Model;
using DAC.Model.Util;
using DAC.View;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;

namespace DAC.AExcel.Grip
{
    class SSGGripResultSheetWriter : ExcelWriter
    {
        public SSGGripResultSheetWriter(ControlMode mode, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw)
            : base(mode, DataSourceType.GRIP, ctms, dt, bw)
        {
        }
        public override void WriteCtmData(string filePathDest, string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            throw new NotSupportedException("Copy function is not supported in this version");
        }
        
        public void WriteByCtmResultSheetSimple(SpreadsheetGear.IWorksheet worksheet, string csvFile,
            CtmObject originalCtm, DataRow row)
        {
             SpreadsheetGear.IRange cells = worksheet.Cells;

             var reader = new CsvReader(new StreamReader(csvFile, Encoding.UTF8));

             reader.Read();
             var headers = reader.FieldHeaders;

             // header
             for (int i = 0; i < headers.Length; i++)
             {
                 cells[0, i].Value = headers[i];
             }

             // 中間データ格納領域
             string[,] values = new string[AisUtil.MAX_BUFF_ROWS, headers.Length];

             // skip type row and unit row
             reader.Read();

             // Loop over rows
             int rowIndex = 1;
             int blockRowIndex = 0;
             while (reader.Read())
             {
                 // Cancel Check
                 if (this.bw.CancellationPending)
                 {
                     return;
                 }

                 var rec = reader.CurrentRecord;

                 values[blockRowIndex, 0] = rec[0];

                 for (int j = 1; j < rec.Length; j++)
                 {
                     values[blockRowIndex, j] = rec[j];
                 }

                 blockRowIndex++;

                 if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                 {
                     int currentRowIndex = rowIndex + blockRowIndex;

                     SpreadsheetGear.IRange range = cells[rowIndex, 0, currentRowIndex - 1, headers.Length - 1];
                     range.Value = values;

                     rowIndex = currentRowIndex;
                     blockRowIndex = 0;
                 }
             }

             if (0 != blockRowIndex)
             {
                 int currentRowIndex = rowIndex + blockRowIndex;
                 SpreadsheetGear.IRange range = cells[rowIndex, 0, currentRowIndex - 1, headers.Length - 1];
                 range.Value = values;
             }
        }



         private bool IsSelected(DataTable tab, string ctmName, string elemName)
         {
             DataRow dataRow = null;
             foreach (var row in tab.Rows)
             {
                 DataRow row1 = row as DataRow;
                 if (row1.ItemArray[0].ToString() == ctmName)
                 {
                     dataRow = row1;
                     break;
                 }
             }
             if (dataRow == null)
             {
                 return false;
             }

             var array = dataRow.ItemArray;
             for (int i = 1; i < array.Length; i++)
             {
                 var val = array[i] as string;
                 if (string.IsNullOrEmpty(val))
                 {
                     continue;
                 }


                 if (val == elemName)
                 {
                     return true;
                 }
             }

             return false;
         }

         private static List<int[]> GetCtmPosList2(string[] headers)
         {
             List<int[]> lst = new List<int[]>();

             if (headers.Length < 2)
             {
                 return lst;
             }

             int startPoint = 2;

             int[] pair = new int[2];
             pair[0] = 0;
             while (startPoint < headers.Length)
             {
                 int idx;
                 for (idx = startPoint; idx < headers.Length; idx++)
                 {
                     var headerX = headers[idx];
                     if (headerX.StartsWith("受信時刻_"))
                     {
                         pair[1] = idx - 1;
                         lst.Add(pair);
                         pair = new int[2];
                         pair[0] = idx - 1;


                         startPoint = idx + 1;
                         break;
                     }
                 }
                 if (idx == headers.Length)
                 {
                     pair[1] = headers.Length;
                     break;
                 }
             }
             lst.Add(pair);
             return lst;
         }
    }
}
