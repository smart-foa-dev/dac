﻿using DAC.Model;
using DAC.Model.Util;
using DAC.View;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using Microsoft.VisualBasic.FileIO;

namespace DAC.AExcel.Grip
{
    class GripResultSheetWriter : ExcelWriter
    {

        public GripResultSheetWriter(ControlMode mode, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw)
            : base(mode, DataSourceType.GRIP, ctms, dt, bw)
        {
        }

        public override void WriteCtmData(string filePathDest, string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            throw new NotSupportedException("Copy function is not supported in this version");
        }


        public void WriteByCtmResultSheetSimple(Microsoft.Office.Interop.Excel.Worksheet worksheet, string csvFile,
           CtmObject originalCtm, DataRow row)
        {
            Microsoft.Office.Interop.Excel.Range cells = worksheet.Cells;


#if SEARCH_FROM_WEB
            var reader = new CsvReader(new StreamReader(csvFile, Encoding.GetEncoding("Shift_JIS")));

            reader.Read();
            var headers = reader.FieldHeaders;
            List<int> rtPosList = new List<int>();

            // header
            string hFile = Path.ChangeExtension(csvFile, "h");
            var hReader = new CsvReader(new StreamReader(hFile, Encoding.UTF8));
            hReader.Read();
            var hHeaders = new List<string>();
            hHeaders.AddRange(hReader.FieldHeaders);
            for (int i = 1; i <= headers.Length; i++)
            {
                cells[1, i].Value = headers[i - 1];
                if (hHeaders.Contains((i - 1).ToString()))
                    rtPosList.Add(i);
            }
#else
            var reader = new CsvReader(new StreamReader(csvFile, Encoding.UTF8));

            reader.Read();
            var headers = reader.FieldHeaders;
            List<int> rtPosList = new List<int>();

            // header
            for (int i = 1; i <= headers.Length; i++)
            {
                cells[1, i].Value = headers[i - 1];
                if (headers[i - 1].StartsWith("受信時刻_"))
                {
                    rtPosList.Add(i);
                }
            }
#endif


            // 中間データ格納領域
            object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headers.Length];



            // skip type row and unit row
            reader.Read();

            // Loop over rows
            int rowIndex = 2;
            int blockRowIndex = 0;
            while (reader.Read())
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                var rec = reader.CurrentRecord;
                /*
                long rt = Int64.Parse(rec[0]);
                */
                // values[blockRowIndex, 0] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);
                //values[blockRowIndex, 0] = rec[0];
                //for (int j = 1; j < rec.Length; j++)
                //{
                //    values[blockRowIndex, j] = rec[j];
                //}
                for (int j = 0; j < rec.Length; j++)
                {
                    //values[blockRowIndex, j] = rec[j];
                    //values[blockRowIndex, j] = "'" + rec[j];
                    if (rec[j].StartsWith("=="))
                        values[blockRowIndex, j] = "'" + rec[j];
                    else
                        values[blockRowIndex, j] = rec[j];
                }

                blockRowIndex++;

                if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                {
                    int currentRowIndex = rowIndex + blockRowIndex;

                    Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                    range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, headers.Length);
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // RT Format
                    foreach (int rtPos in rtPosList)
                    {
                        Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, rtPos];
                        range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                    }

                    rowIndex = currentRowIndex;
                    blockRowIndex = 0;
                }

            }

            if (0 != blockRowIndex)
            {
                Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                range = range.get_Resize(blockRowIndex, headers.Length);
                range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);
                //object[,] values2 = new object[blockRowIndex, headers.Length];
                //for (int tr = 0; tr < blockRowIndex; tr++)
                //{
                //    for (int tc = 0; tc < headers.Length; tc++)
                //    {
                //        values2[tr, tc] = values[tr, tc];
                //    }
                //}
                //range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values2);

                // RT Format
                foreach (int rtPos in rtPosList)
                {
                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, rtPos];
                    range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                    range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                }
            }
        }

        public void WriteByCtmResultSheetSimple(SpreadsheetGear.IWorksheet worksheet, string csvFile, CtmObject originalCtm, DataRow row)
        {
            SpreadsheetGear.IRange cells = worksheet.Cells;


#if SEARCH_FROM_WEB
            var reader = new CsvReader(new StreamReader(csvFile, Encoding.GetEncoding("Shift_JIS")));
            reader.Configuration.IgnoreQuotes = true;
            //var reader = new CsvReader(new StreamReader(csvFile, Encoding.GetEncoding("Shift_JIS")));
            reader.Read();
            var headers = reader.FieldHeaders;
            List<int> rtPosList = new List<int>();

            // header
            string hFile = Path.ChangeExtension(csvFile, "h");
            var hReader = new CsvReader(new StreamReader(hFile, Encoding.UTF8));
            hReader.Read();
            var hHeaders = new List<string>();
            hHeaders.AddRange(hReader.FieldHeaders);
            for (int i = 0; i < headers.Length; i++)
            {
                cells[0, i].Value = headers[i].Trim('\"');
                if (hHeaders.Contains(i.ToString()))
                    rtPosList.Add(i);
            }
#else
            var reader = new CsvReader(new StreamReader(csvFile, Encoding.UTF8));

            reader.Read();
            var headers = reader.FieldHeaders;
            List<int> rtPosList = new List<int>();

            // header
            for (int i = 0; i < headers.Length; i++)
            {
                cells[0, i].Value = headers[i];
                if (headers[i].StartsWith("受信時刻_"))
                {
                    rtPosList.Add(i);
                }
            }
#endif
            // 中間データ格納領域
            object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headers.Length];

            StreamReader strreader = new StreamReader(csvFile,Encoding.UTF8);
            string strcontent = strreader.ReadToEnd();
            TextFieldParser parser = new TextFieldParser(new StringReader(strcontent));

            using (parser)
            {
                parser.TextFieldType = FieldType.Delimited; // 区切り形式とする
                parser.SetDelimiters(","); // 区切り文字はコンマとする
                //parser.CommentTokens = new string[] { "#" }; // #で始まる行はコメントとする
                //parser.HasFieldsEnclosedInQuotes = true; // 引用符付きとする
                //parser.TrimWhiteSpace = true; // 空白文字を取り除く
                parser.HasFieldsEnclosedInQuotes = true;
                parser.TrimWhiteSpace = false;
                int indexRow = 0;
                List<string> ctmNameList = new List<string>();
                List<string> dataTypeList = new List<string>();
                // Skip the Unit and Type line
                parser.ReadFields();
                parser.ReadFields();
                parser.ReadFields();
                int rowIndex = 1;
                int blockRowIndex = 0;
                while (!parser.EndOfData)
                {
                    string[] textrow = parser.ReadFields(); // 1行読み込んでフィールド毎に配列に入れる
                    if (textrow.Length == 0) continue;
                    if (textrow.Length == 1 && string.IsNullOrEmpty(textrow[0].Trim())) continue;
                    List<string> outRow = new List<string>();
                    for (int indexCol = 0; indexCol < textrow.Length; indexCol++)
                    {
                        //ISSUE_NO.768 Sunyi 2018/07/12 Start
                        //5000行を超えるとエラーにならないように修正
                        //values[rowIndex, indexCol] = textrow[indexCol];
                        values[blockRowIndex, indexCol] = textrow[indexCol];
                        //ISSUE_NO.768 Sunyi 2018/07/12 End
                    }
                    indexRow++;

                    //ISSUE_NO.768 Sunyi 2018/07/12 Start
                    //5000行を超えるとエラーにならないように修正
                    blockRowIndex++;
                    //ISSUE_NO.768 Sunyi 2018/07/12 End

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        for (int i = 0; i < AisUtil.MAX_BUFF_ROWS; i++)
                        {
                            for (int j = 0; j < headers.Length; j++)
                            {
                                SpreadsheetGear.IRange range = worksheet.Cells[rowIndex + i, j];
                                range.Value = values[i, j];
                            }
                        }

                        // RT Format
                        foreach (int rtPos in rtPosList)
                        {
                            SpreadsheetGear.IRange range2 = worksheet.Cells[rowIndex, rtPos, rowIndex + AisUtil.MAX_BUFF_ROWS - 1, rtPos];
                            range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                        }
                        rowIndex = currentRowIndex;
                        //ISSUE_NO.768 Sunyi 2018/07/12 Start
                        //5000行を超えるとエラーにならないように修正
                        blockRowIndex = 0;
                        //ISSUE_NO.768 Sunyi 2018/07/12 End
                    }
                }

                if (0 != blockRowIndex)
                {
                    for (int i = 0; i < AisUtil.MAX_BUFF_ROWS; i++)
                    {
                        for (int j = 0; j < headers.Length; j++)
                        {
                            SpreadsheetGear.IRange range = worksheet.Cells[rowIndex + i, j];
                            range.Value = values[i, j];
                        }
                    }

                    // RT Format
                    foreach (int rtPos in rtPosList)
                    {
                        SpreadsheetGear.IRange range2 = worksheet.Cells[rowIndex, rtPos, rowIndex + AisUtil.MAX_BUFF_ROWS - 1, rtPos];
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                    }
                }

            }
        }


        public void WriteByCtmResultSheetWithDtOutput(Microsoft.Office.Interop.Excel.Worksheet worksheet, string csvFile, DataTable tab)
        {
            Microsoft.Office.Interop.Excel.Range cells = worksheet.Cells;


#if SEARCH_FROM_WEB
            // 1. Preare Header list
            var reader = new CsvReader(new StreamReader(csvFile, Encoding.GetEncoding("Shift_JIS")));

            reader.Read();

            var headers = reader.FieldHeaders;

            string hFile = Path.ChangeExtension(csvFile, "h");
            var hReader = new CsvReader(new StreamReader(hFile, Encoding.UTF8));
            hReader.Read();
            var hHeaders = new List<string>();
            hHeaders.AddRange(hReader.FieldHeaders);
            List<int[]> ctmPosList = GetCtmPosList2(headers, hHeaders);
#else
            // 1. Preare Header list
            var reader = new CsvReader(new StreamReader(csvFile, Encoding.UTF8));

            reader.Read();

            var headers = reader.FieldHeaders;

            List<int[]> ctmPosList = GetCtmPosList2(headers);
#endif

            HashSet<int> skipList = new HashSet<int>();

            List<string> headers2 = new List<string>();

            List<int> rtPosList = new List<int>();

            int count = 1;
            foreach (var ctmPos in ctmPosList)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                var ctmName = headers[ctmPos[0]];
                headers2.Add(ctmName);
                headers2.Add(headers[ctmPos[0] + 1]);
                rtPosList.Add(ctmPos[0] + 1);

                for (int i = ctmPos[0] + 2; i < ctmPos[1]; i++)
                {
                    var elemName = ElementId2Name(ctmName, headers[i]);
                    if (!IsSelected(tab, ctmName, elemName))
                    {
                        skipList.Add(i);
                        continue;
                    }

                    if (count > 1)
                    {
                        elemName += "_" + count;
                    }

                    headers2.Add(elemName);
                }
                count++;
            }

            // Write Headers
            int idx = 1;
            foreach (var h in headers2)
            {
                cells[1, idx++].Value = h;
            }


            // 中間データ格納領域
            object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headers2.Count];

            // skip type row and unit row
            reader.Read();

            // Loop over rows
            int rowIndex = 2;
            int blockRowIndex = 0;

            while (reader.Read())
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                var rec = reader.CurrentRecord;

                int blockColIndex = 0;
                for (int j = 0; j < rec.Length; j++)
                {
                    if (skipList.Contains(j))
                    {
                        continue;
                    }
                    //values[blockRowIndex, blockColIndex++] = rec[j];
                    values[blockRowIndex, blockColIndex++] = "'" + rec[j];
                }

                blockRowIndex++;

                if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                {
                    int currentRowIndex = rowIndex + blockRowIndex;

                    Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                    range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, headers2.Count);
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // RT Format
                    foreach (int rtPos in rtPosList)
                    {
                        Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, rtPos];
                        range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                    }

                    rowIndex = currentRowIndex;
                    blockRowIndex = 0;
                }
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }

            if (blockRowIndex > 0)
            {
                Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                range = range.get_Resize(blockRowIndex, headers2.Count);
                range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                // RT Format
                foreach (int rtPos in rtPosList)
                {
                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, rtPos];
                    range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                    range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                }
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }
            //ISSUE_NO.728 sunyi 2018/06/25 Start
            //仕様変更、resultシートを表示する
            //worksheet.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
            //ISSUE_NO.728 sunyi 2018/06/25 End
        }


        private bool IsSelected(DataTable tab, string ctmName, string elemName)
        {
            DataRow dataRow = null;
            foreach (var row in tab.Rows)
            {
                DataRow row1 = row as DataRow;
                if (row1.ItemArray[0].ToString() == ctmName)
                {
                    dataRow = row1;
                    break;
                }
            }
            if (dataRow == null)
            {
                return false;
            }

            var array = dataRow.ItemArray;
            for (int i = 1; i < array.Length; i++)
            {
                var val = array[i] as string;
                if (string.IsNullOrEmpty(val))
                {
                    continue;
                }


                if (val == elemName)
                {
                    return true;
                }
            }

            return false;
        }


#if SEARCH_FROM_WEB
        private static List<int[]> GetCtmPosList2(string[] headers, List<string> hHeaders)
        {

            List<int[]> lst = new List<int[]>();

            if (headers.Length < 1)
            {
                return lst;
            }

            int startPoint = 1;

            int[] pair = new int[2];
            pair[0] = 0;
            while (startPoint < headers.Length)
            {
                int idx;
                for (idx = startPoint; idx < headers.Length; idx++)
                {
                    if (hHeaders.Contains(idx.ToString()))
                    {
                        pair[1] = idx;
                        lst.Add(pair);
                        pair = new int[2];
                        pair[0] = idx;


                        startPoint = idx + 1;
                        break;
                    }
                }
                if (idx == headers.Length)
                {
                    pair[1] = headers.Length;
                    break;
                }
            }
            lst.Add(pair);
            return lst;
        }
#else
        private static List<int[]> GetCtmPosList2(string[] headers)
        {

            List<int[]> lst = new List<int[]>();

            if (headers.Length < 2)
            {
                return lst;
            }

            int startPoint = 2;

            int[] pair = new int[2];
            pair[0] = 0;
            while (startPoint < headers.Length)
            {
                int idx;
                for (idx = startPoint; idx < headers.Length; idx++)
                {
                    var headerX = headers[idx];
                    if (headerX.StartsWith("受信時刻_"))
                    {
                        pair[1] = idx - 1;
                        lst.Add(pair);
                        pair = new int[2];
                        pair[0] = idx - 1;


                        startPoint = idx + 1;
                        break;
                    }
                }
                if (idx == headers.Length)
                {
                    pair[1] = headers.Length;
                    break;
                }
            }
            lst.Add(pair);
            return lst;
        }
#endif


    }
}
