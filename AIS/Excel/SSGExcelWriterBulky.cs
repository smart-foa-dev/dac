﻿using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using CsvHelper;
using SpreadsheetGear;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;

namespace DAC.AExcel
{
    class SSGExcelWriterBulky : SSGExcelWriter
    {
        private List<int> selectedRowNumber;
        private List<int> selectedColumnNumber;

        // Wang FoaStudio Issue AISBUL-50 20181211 Start
        private const int MAX_ROWS = 1048000;
        private const int MAX_COLS = 16100;
        private const int MAX_COLS_MESSAGE = 16000;
        // Wang FoaStudio Issue AISBUL-50 20181211 End

        public SSGExcelWriterBulky(ControlMode mode, DataSourceType ds, CtmObject ctm, BackgroundWorker bw, Mission mi, List<int> rows, List<int> cols)
            : base(mode, ds, null, null, bw, mi)
        {
            this.Ctms = new List<CtmObject>();
            this.Ctms.Add(ctm);
            this.selectedRowNumber = rows;
            this.selectedColumnNumber = cols;
        }

        public override void WriteCtmData(string filePathDest, string srcFile, Dictionary<ExcelConfigParam, object> configParams)
        {
            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                // ワークブック作成
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePathDest);

                // シート作成
                SpreadsheetGear.IWorksheet worksheetCondition = AisUtil.GetSheetFromSheetName_SSG(workbook, GetSummarySheetName());

                WriteSummarySheet(worksheetCondition, this.dtOutput, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // paramシート作成
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(workbook, paramSheetName);
                WriteConfigSheet(worksheetParam, configParams);

                // ro_paramシート作成
                string roParamSheetName = Keywords.RO_PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_SSG(workbook, roParamSheetName);
                WriteRoParamSheet_SSG(worksheetRoParam);

                string dataSheetName = "データ";
                SpreadsheetGear.IWorksheet worksheetData = AisUtil.GetSheetFromSheetName_SSG(workbook, dataSheetName);

                var direction = GetStringVal(configParams, ExcelConfigParam.B_CONNECTION, "横");
                if (direction == "横")
                {
                    WriteBulkySheetHorizontal(worksheetData, srcFile);
                }
                else
                {
                    WriteBulkySheetVertical(worksheetData, srcFile);
                }
                worksheetData.Select();
                worksheetData.Cells[0, 0].Select();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 保存
                workbook.Save();
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close();
                }
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }
        }

        protected override void WriteSummarySheet(IWorksheet worksheetCondition, DataTable dt, Dictionary<ExcelConfigParam, object> configParams)
        {
            DateTime start = GetDateTimeVal(configParams, ExcelConfigParam.START, DateTime.MinValue);
            DateTime end = GetDateTimeVal(configParams, ExcelConfigParam.END, DateTime.MinValue);

            string missionName = GetStringVal(configParams, ExcelConfigParam.MISSION_NAME, null);
            string missionId = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, null);

            if (start == DateTime.MinValue)
            {
                throw new InvalidDataException("start is not specified.");
            }

            if (end == DateTime.MinValue)
            {
                throw new InvalidDataException("end is not specified.");
            }

            // 収集条件を出力
            SpreadsheetGear.IRange cellsCondition = worksheetCondition.Cells;
         
                cellsCondition["B1"].Value = missionName;
                cellsCondition["B2"].Value = missionId;
            
            cellsCondition["B3"].Value = start.ToString("yyyy/MM/dd");
            cellsCondition["C3"].Value = start.ToString("HH:mm:ss");
            cellsCondition["B4"].Value = end.ToString("yyyy/MM/dd");
            cellsCondition["C4"].Value = end.ToString("HH:mm:ss");
        }

        protected override void WriteConfigSheet(IWorksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            string fileName = GetStringVal(configParams, ExcelConfigParam.REGISTERED_FILENAME, string.Empty);
            string ctmId = GetStringVal(configParams, ExcelConfigParam.CTM_ID, "");
            string elementId = GetStringVal(configParams, ExcelConfigParam.ELEMENT_ID, "");
            string ctmName = GetStringVal(configParams, ExcelConfigParam.CTM_NAME, "");
            string connectionB = GetStringVal(configParams, ExcelConfigParam.B_CONNECTION, "横");
            string templateName = GetStringVal(configParams, ExcelConfigParam.TEMPLATE_NAME, "");
            string missionId = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, string.Empty);
            string maxCtmName = GetStringVal(configParams, ExcelConfigParam.MAX_CTM_NAME, "");
            string maxCtmId = GetStringVal(configParams, ExcelConfigParam.MAX_CTM_ID, "");

            SpreadsheetGear.IRange cellsParam = worksheetParam.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                // null check
                if (cellsParam[i, 0].Value == null)
                {
                    continue;
                }

                // 登録フォルダ
                if (cellsParam[i, 0].Value.ToString() == "登録フォルダ")
                {
                    cellsParam[i, 1].Value = AisConf.RegistryFolderPath;
                    continue;
                }

                // 登録ファイル
                if (cellsParam[i, 0].Value.ToString() == "登録ファイル")
                {
                    cellsParam[i, 1].Value = fileName;
                    continue;
                }

                // サーバIPアドレス
                if (cellsParam[i, 0].Value.ToString() == "サーバIPアドレス")
                {
                    cellsParam[i, 1].Value = AisConf.Config.CmsHost;
                    continue;
                }

                // サーバポート番号
                if (cellsParam[i, 0].Value.ToString() == "サーバポート番号")
                {
                    cellsParam[i, 1].Value = AisConf.Config.CmsPort;
                    continue;
                }

                // GRIPサーバIPアドレス
                if (cellsParam[i, 0].Value.ToString() == "GRIPサーバIPアドレス")
                {
                    cellsParam[i, 1].Value = AisConf.Config.GripServerHost;
                    continue;
                }

                // GRIPサーバポート番号
                if (cellsParam[i, 0].Value.ToString() == "GRIPサーバポート番号")
                {
                    cellsParam[i, 1].Value = AisConf.Config.GripServerPort;
                    continue;
                }

                // テンプレート名称
                if (cellsParam[i, 0].Value.ToString() == "テンプレート名称")
                {
                    cellsParam[i, 1].Value = templateName;
                    continue;
                }

                // CTM名
                if (cellsParam[i, 0].Value.ToString() == "CTM名")
                {
                    cellsParam[i, 1].Value = ctmName;
                    continue;
                }

                // CTMID
                if (cellsParam[i, 0].Value.ToString() == "CTMID")
                {
                    cellsParam[i, 1].Value = ctmId;
                    continue;
                }

                // エレメントID
                if (cellsParam[i, 0].Value.ToString() == "エレメントID")
                {
                    cellsParam[i, 1].Value = elementId;
                    continue;
                }

                // Bulky選択行
                if (cellsParam[i, 0].Value.ToString() == "Bulky選択行")
                {
                    // 行の選択情報を出力
                    string numbers = string.Join(",", this.selectedRowNumber);
                    cellsParam[i, 1].Value = numbers;
                    continue;
                }

                // Bulky選択列
                if (cellsParam[i, 0].Value.ToString() == "Bulky選択列")
                {
                    // 列の選択情報を出力
                    string numbers = string.Join(",", this.selectedColumnNumber);
                    cellsParam[i, 1].Value = numbers;
                    continue;
                }

                // 連結方向
                if (cellsParam[i, 0].Value.ToString() == "連結方向")
                {
                    cellsParam[i, 1].Value = connectionB;
                    continue;
                }

                // ミッションID
                if (cellsParam[i, 0].Value.ToString() == "ミッションID")
                {
                    cellsParam[i, 1].Value = missionId;
                    continue;
                }

                // 製作者名
                if (cellsParam[i, 0].Value.ToString() == "製作者名")
                {
                    cellsParam[i, 1].Value = AisConf.UserId;
                    continue;
                }

                // 収集タイプ
                if (cellsParam[i, 0].Value.ToString() == "収集タイプ")
                {
                    switch (this.dataSource)
                    {
                        case DataSourceType.CTM_DIRECT:
                            cellsParam[i, 1].Value = "CTM";
                            break;
                        case DataSourceType.MISSION:
                            cellsParam[i, 1].Value = "ミッション";
                            break;
                        case DataSourceType.GRIP:
                            cellsParam[i, 1].Value = "GRIP";
                            break;
                        default:
                            cellsParam[i, 1].Value = "";
                            break;
                    }
                    continue;
                }

                // for KMEW
                if (cellsParam[i, 0].Value.ToString() == "CmsVersion")
                {
                    cellsParam[i, 1].Value = AisConf.Config.CmsVersion;
                    continue;
                }
                if (AisConf.Config.CmsVersion == "3.5")
                {
                    var ahInfo = AisUtil.GetAhInfo(this.Ctms[0].id.ToString());

                    if (cellsParam[i, 0].Value.ToString() == "MfHost")
                    {
                        cellsParam[i, 1].Value = ahInfo.Mf;
                        continue;
                    }
                    if (cellsParam[i, 0].Value.ToString() == "MfPort")
                    {
                        cellsParam[i, 1].Value = ahInfo.MfPort;
                        continue;
                    }
                }

                // ProxyServer使用
                if (cellsParam[i, 0].Value.ToString() == "ProxyServer使用")
                {
                    cellsParam[i, 1].Value = AisConf.Config.UseProxy;
                    continue;
                }

                // ProxyServerURI
                if (cellsParam[i, 0].Value.ToString() == "ProxyServerURI")
                {
                    cellsParam[i, 1].Value = AisConf.Config.ProxyURI;
                    continue;
                }
                // MaxCtmName使用
                if (cellsParam[i, 0].Value.ToString() == "最大トルク参照CTM名")
                {
                    cellsParam[i, 1].Value = maxCtmName;
                    continue;
                }

                // MaxCtmId使用
                if (cellsParam[i, 0].Value.ToString() == "最大トルク参照CTMID")
                {
                    cellsParam[i, 1].Value = maxCtmId;
                    continue;
                }

                // 最初設定フラグ使用
                if (cellsParam[i, 0].Value.ToString() == "最初設定フラグ")
                {
                    cellsParam[i, 1].Value = "True";
                    continue;
                }

                //ISSUE_NO.710 sunyi 2018/05/25 start
                //パラメータ追加
                // IsFirstSave
                if (cellsParam[i, 0].Value.ToString() == "IsFirstSave")
                {
                    cellsParam[i, 1].Value = "";
                }
                //ISSUE_NO.710 sunyi 2018/05/25 end
            }

            worksheetParam.Visible = SheetVisibility.Hidden;
        }

        public void WriteBulkySheetHorizontal(IWorksheet worksheet, string srcFile)
        {
            SpreadsheetGear.IRange cells = worksheet.Cells;
            cells.Clear();
            cells[0, 0].Value = "No";

            using (var sr = new StreamReader(srcFile, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                bool hasNext = reader.Read();
                if (!hasNext)
                {
                    return;
                }

                int ctmNumber = 0;          // いくつめのCTMか
                string[] header = reader.FieldHeaders;

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 最初のヘッダ
                int excelColNo = 1;
                for (int i = 0; i < header.Length; i++)
                {
                    if (!this.selectedColumnNumber.Contains(i))
                    {
                        continue;
                    }

                    cells[0, excelColNo].Value = string.Format("{0}_{1}", header[i], ctmNumber + 1);
                    excelColNo++;
                }
                ctmNumber++;

                // 各行についてループ
                int rowNumber = 0;          // 行番号
                int excelRowNumber = 1;
                do
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    string[] row = reader.CurrentRecord;

                    // ヘッダーかどうか判定
                    bool isHeader = true;
                    for (int i = 0; i < row.Length; i++)
                    {
                        if (row[i] != header[i])
                        {
                            isHeader = false;
                            break;
                        }
                    }

                    if (isHeader)
                    {
                        // ヘッダー出力
                        int offset = this.selectedColumnNumber.Count * ctmNumber;

                        // Wang FoaStudio Issue AISBUL-50 20181218 Start
                        /*
                        if (offset + this.selectedColumnNumber.Count + 2 > EXCEL_MAX_COLS)
                        {
                            break; // 列が多すぎ
                        }
                        */
                        if (offset + this.selectedColumnNumber.Count + 2 > MAX_COLS)
                        {
                            string message = string.Format(Properties.Message.AIS_W_005, MAX_COLS_MESSAGE);
                            DAC.View.Helpers.AisMessageBox.DisplayWarningMessageBox(message);
                            break;
                        }
                        // Wang FoaStudio Issue AISBUL-50 20181218 End

                        excelColNo = offset + 1;
                        for (int i = 0; i < header.Length; i++)
                        {
                            // Cancel Check
                            if (this.bw.CancellationPending)
                            {
                                return;
                            }

                            if (!this.selectedColumnNumber.Contains(i))
                            {
                                continue;
                            }

                            cells[0, excelColNo].Value = string.Format("{0}_{1}", header[i], ctmNumber + 1);
                            excelColNo++;
                        }

                        rowNumber = 0;  // 行番号をリセット
                        excelRowNumber = 1;
                        ctmNumber++;    // 次のCTMへ
                    }
                    else
                    {
                        //ISSUE_NO.645 sunyi 2018/05/24 start
                        //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                        if (AisUtil.RowClickIsEnable)
                        {
                        //ISSUE_NO.645 sunyi 2018/05/24 end
                            if (!this.selectedRowNumber.Contains(rowNumber))
                            {
                                // 選択された行番号でなければスキップ
                                rowNumber++;
                                continue;
                            }
                        }

                        cells[excelRowNumber, 0].Value = excelRowNumber;

                        int offset = this.selectedColumnNumber.Count * (ctmNumber - 1);
                        excelColNo = offset + 1;
                        for (int i = 0; i < row.Length; i++)
                        {
                            // Cancel Check
                            if (this.bw.CancellationPending)
                            {
                                return;
                            }

                            if (!this.selectedColumnNumber.Contains(i))
                            {
                                // 選択された列番号でなければスキップ
                                continue;
                            }

                            cells[excelRowNumber, excelColNo].Value = row[i];
                            excelColNo++;
                        }

                        rowNumber++;
                        excelRowNumber++;
                    }

                } while (reader.Read());

                // 線
                SpreadsheetGear.IRange borderRange = worksheet.Cells[0, 0, excelRowNumber - 1, (this.selectedColumnNumber.Count * ctmNumber)];
                AisUtil.WriteBorder_SSG(worksheet, borderRange);
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }
        }

        //ISSUE_NO.645 sunyi 2018/05/24 start
        //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
        //public void WriteBulkySheetVertical(IWorksheet worksheet, string srcFile, string IsAllData = null)   
        public void WriteBulkySheetVertical(IWorksheet worksheet, string srcFile)
        //ISSUE_NO.645 sunyi 2018/05/24 end
        {
            SpreadsheetGear.IRange cells = worksheet.Cells;
            cells.Clear();
            cells[0, 0].Value = "No";

            using (var sr = new StreamReader(srcFile, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                bool hasNext = reader.Read();
                if (!hasNext)
                {
                    return;
                }

                int ctmNumber = 0;          // いくつめのCTMか
                string[] header = reader.FieldHeaders;

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 最初のヘッダ
                int excelColNumber = 1;
                for (int i = 0; i < header.Length; i++)
                {
                    if (!this.selectedColumnNumber.Contains(i))
                    {
                        continue;
                    }

                    cells[0, excelColNumber].Value = string.Format("{0}_{1}", header[i], ctmNumber + 1);
                    excelColNumber++;
                }
                ctmNumber++;

                // 各行についてループ
                int rowNumber = 0;          // 行番号
                int excelRowNumber = 1;
                do
                {
                    // Wang FoaStudio Issue AISBUL-50 20181211 Start
                    if (excelRowNumber > MAX_ROWS)
                    {
                        string message = string.Format(Properties.Message.AIS_W_004, MAX_ROWS);
                        DAC.View.Helpers.AisMessageBox.DisplayWarningMessageBox(message);
                        break;
                    }
                    // Wang FoaStudio Issue AISBUL-50 20181211 End

                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    string[] row = reader.CurrentRecord;

                    // ヘッダーかどうか判定
                    bool isHeader = true;
                    for (int i = 0; i < row.Length; i++)
                    {
                        if (row[i] != header[i])
                        {
                            isHeader = false;
                            break;
                        }
                    }

                    if (isHeader)
                    {
                        rowNumber = 0;
                    }
                    else
                    {
                        //ISSUE_NO.645 sunyi 2018/05/24 start
                        //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                        //if (string.IsNullOrEmpty(IsAllData))
                        if (AisUtil.RowClickIsEnable)
                        //ISSUE_NO.645 sunyi 2018/05/24 end
                        {
                            if (!this.selectedRowNumber.Contains(rowNumber))
                            {
                                // 選択された行番号でなければスキップ
                                rowNumber++;
                                continue;
                            }
                        }

                        cells[excelRowNumber, 0].Value = excelRowNumber;

                        excelColNumber = 1;
                        for (int i = 0; i < row.Length; i++)
                        {
                            // Cancel Check
                            if (this.bw.CancellationPending)
                            {
                                return;
                            }

                            if (!this.selectedColumnNumber.Contains(i))
                            {
                                // 選択された列番号でなければスキップ
                                continue;
                            }

                            cells[excelRowNumber, excelColNumber].Value = row[i];
                            excelColNumber++;
                        }

                        rowNumber++;
                        excelRowNumber++;
                    }

                } while (reader.Read());

                // 線
                SpreadsheetGear.IRange borderRange = worksheet.Cells[0, 0, excelRowNumber - 1, this.selectedColumnNumber.Count];
                AisUtil.WriteBorder_SSG(worksheet, borderRange);
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }
        }
        //ISSUE_NO.645 sunyi 2018/05/24 start
        //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
        //public void WriteBulkySheetVerticalStream(IWorksheet worksheet, string srcFile, string IsAllData = null)
        public void WriteBulkySheetVerticalStream(IWorksheet worksheet, string srcFile)
        //ISSUE_NO.645 sunyi 2018/05/24 end
        {
            SpreadsheetGear.IRange cells = worksheet.Cells;
            cells.Clear();
            cells[0, 0].Value = "No";

            using (var sr = new StreamReader(srcFile, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                bool hasNext = reader.Read();
                if (!hasNext)
                {
                    return;
                }

                int ctmNumber = 0;          // いくつめのCTMか
                string[] header = reader.FieldHeaders;

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 最初のヘッダ
                int excelColNumber = 1;
                for (int i = 0; i < header.Length; i++)
                {
                    if (!this.selectedColumnNumber.Contains(i))
                    {
                        continue;
                    }

                    cells[0, excelColNumber].Value = header[i];
                    excelColNumber++;
                }
                ctmNumber++;

                // 各行についてループ
                int rowNumber = 0;          // 行番号
                int excelRowNumber = 1;
                do
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    string[] row = reader.CurrentRecord;

                    // ヘッダーかどうか判定
                    bool isHeader = true;
                    for (int i = 0; i < row.Length; i++)
                    {
                        if (row[i] != header[i])
                        {
                            isHeader = false;
                            break;
                        }
                    }

                    if (isHeader)
                    {
                        rowNumber = 0;
                    }
                    else
                    {
                        //ISSUE_NO.645 sunyi 2018/05/24 start
                        //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                        //if (string.IsNullOrEmpty(IsAllData))
                        if (AisUtil.RowClickIsEnable)
                        //ISSUE_NO.645 sunyi 2018/05/24 end
                        {
                            if (!this.selectedRowNumber.Contains(rowNumber))
                            {
                                // 選択された行番号でなければスキップ
                                rowNumber++;
                                continue;
                            }
                        }

                        cells[excelRowNumber, 0].Value = excelRowNumber;

                        excelColNumber = 1;
                        for (int i = 0; i < row.Length; i++)
                        {
                            // Cancel Check
                            if (this.bw.CancellationPending)
                            {
                                return;
                            }

                            if (!this.selectedColumnNumber.Contains(i))
                            {
                                // 選択された列番号でなければスキップ
                                continue;
                            }

                            cells[excelRowNumber, excelColNumber].Value = row[i];
                            excelColNumber++;
                        }

                        rowNumber++;
                        excelRowNumber++;
                    }

                } while (reader.Read());

                // 線
                SpreadsheetGear.IRange borderRange = worksheet.Cells[0, 0, excelRowNumber - 1, this.selectedColumnNumber.Count];
                AisUtil.WriteBorder_SSG(worksheet, borderRange);
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }
        }
    }
}
