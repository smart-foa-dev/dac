﻿using DAC.AExcel.Grip;
using DAC.CtmData;
using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using CsvHelper;
using FoaCore.Common;
using FoaCore.Common.Util;
using SpreadsheetGear;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;

namespace DAC.AExcel
{
    abstract class ExcelWriterYotobetsu : ExcelWriter
    {
        public ExcelWriterYotobetsu(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi = null)
            : base(mode, ds, ctms, dt, bw)
        {

        }

        protected string folderPath;

        /// <summary>
        /// 下のCTM編集のGridで表示されているものを全て使う。
        /// 個別結果シートのみ。
        /// 上のエレメント編集GridはないのでdtOutputはnull。
        /// </summary>
        /// <param name="filePathDest"></param>
        /// <param name="folderPath"></param>
        /// <param name="configParams"></param>
        public override void WriteCtmData(string filePathDest, string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                // ワークブック作成
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePathDest);

                this.folderPath = folderPath;

                // paramシート作成
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(workbook, paramSheetName);
                WriteSummarySheet(worksheetParam, this.dtOutput);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                WriteConfigSheet(worksheetParam, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = Keywords.RO_PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_SSG(workbook, roParamSheetName);
                WriteRoParamSheet_SSG(worksheetRoParam);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (this.dataSource == DataSourceType.GRIP)
                {
                    WriteGripResultSheets(workbook, folderPath);
                }
                else
                {
                    WriteResultSheets(workbook, folderPath);
                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 保存
                workbook.Save();
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close();
                }
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }
            ////E2-126 sunyi 20190116 start
            ////autofit()
            //AutoFitToSheet(filePathDest, folderPath);
            ////E2-126 sunyi 20190116 end
        }
        ////E2-126 sunyi 20190116 start
        //private void AutoFitToSheet(string filePathDest, string folderPath)
        //{
        //    Microsoft.Office.Interop.Excel.Application excel = null;
        //    Microsoft.Office.Interop.Excel.Workbooks workbooks = null;
        //    Microsoft.Office.Interop.Excel.Workbook workbook = null;
        //    Microsoft.Office.Interop.Excel.Worksheet worksheet = null;
        //    // ワークブック作成
        //    try
        //    {
        //        excel = new Microsoft.Office.Interop.Excel.Application();
        //        excel.Visible = false;
        //        workbooks = excel.Workbooks;
        //        workbook = workbooks.Open(filePathDest);
        //        ExcelUtil.MoveExcelOutofScreen(excel);

        //        if (this.dataSource == DataSourceType.GRIP)
        //        {
        //            DirectoryInfo di = new DirectoryInfo(folderPath);
        //            foreach (var fi in di.GetFiles())
        //            {
        //                worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, AisUtil.RemoveExtention(fi.Name));
        //                worksheet.UsedRange.Columns.AutoFit();
        //            }
        //        }
        //        else
        //        {
        //            foreach (var ctm in this.Ctms)
        //            {
        //                var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
        //                worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, ctmName);
        //                worksheet.UsedRange.Columns.AutoFit();
        //            }
        //        }

        //        // 保存
        //        workbook.Save();

        //        ExcelUtil.RestoreExcelWindow(excel);
        //    }
        //    finally
        //    {
        //        if (workbook != null)
        //        {
        //            workbook.Close(false);
        //            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbook);
        //            workbook = null;
        //        }

        //        if (workbooks != null)
        //        {
        //            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbooks);
        //            workbooks = null;
        //        }

        //        if (excel != null)
        //        {
        //            excel.Quit();
        //            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
        //            excel = null;
        //        }
        //    }
        //}
        ////E2-126 sunyi 20190116 end

        private void WriteResultSheets(SpreadsheetGear.IWorkbook workbook, string folderPath)
        {
            foreach (var ctm in this.Ctms)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                var ctmId = ctm.id.ToString();
                var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                string tmpSheetName = ctmName;
                if (tmpSheetName.Length > 31)
                {
                    int lastindex = tmpSheetName.IndexOf("_Route");
                    if (lastindex != -1)
                    {
                        string firstpart = tmpSheetName.Substring(0, lastindex);
                        string lastpart = tmpSheetName.Substring(lastindex);
                        string cutname = firstpart.Substring(0, 31 - lastpart.Length);
                        tmpSheetName = cutname + lastpart;
                    }
                    else
                    {
                        tmpSheetName = tmpSheetName.Substring(0, 31);
                    }
                }
                SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, tmpSheetName);
                // シート検索 / 作成
                //SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, ctmName);
                worksheet.Cells.Clear();
                //ISSUE_NO.728 sunyi 2018/06/25 Start
                //仕様変更、resultシートを表示する
                //worksheet.Visible = SheetVisibility.Hidden;
                //ISSUE_NO.728 sunyi 2018/06/25 End

                var srcFilepath = Path.Combine(folderPath, ctmId + ".csv");

                CsvFile csvFileObj = null;
                if (File.Exists(srcFilepath))
                {
                    csvFileObj = new CsvFile() { Id = new FoaCore.Suid(ctmId), Name = ctmName, Filepath = srcFilepath };
                }
                else
                {
                    return;
                }

                var elements = ctm.GetAllElements();
                string[,] rowS = new string[elements.Count, 3];
                int i = 0;

                //ISSUE_NO.798 sunyi 2018/07/25 Start
                //選択されたエレメントを出力する
                using (var sr = new StreamReader(csvFileObj.Filepath, Encoding.UTF8))
                using (var reader = new CsvReader(sr))
                {
                    reader.Read();
                    var headers = reader.FieldHeaders;
                    //ISSUE_NO.798 sunyi 2018/07/25 End
                    foreach (var e in elements)
                    {
                        //ISSUE_NO.798 sunyi 2018/07/25 Start
                        //選択されたエレメントを出力する
                        foreach (var header in headers)
                        {
                            if (header.ToString().Equals(e.id.ToString()))
                            {
                                //ISSUE_NO.798 sunyi 2018/07/25 End
                                rowS[i, 0] = e.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                                rowS[i, 1] = FoaDatatype.Val2Name(e.datatype);
                                rowS[i, 2] = e.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                                i++;
                                //ISSUE_NO.798 sunyi 2018/07/25 Start
                                //選択されたエレメントを出力する
                                break;
                                //ISSUE_NO.798 sunyi 2018/07/25 End
                            }
                        }
                    }
                }
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                WriteByCtmResultSheet(worksheet, ctm, rowS, this.bw, csvFileObj);

                // 次のシートへ
                //worksheet.UsedRange.Columns.AutoFit();
                worksheet.UsedRange.ColumnWidth = 16.00;
                ////E2-126 sunyi 20190116 start
                ////autofit()
                ////worksheet.Cells["A:A"].ColumnWidth = 14.40;
                ////E2-126 sunyi 20190116 end
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
        }

        private void WriteGripResultSheets(SpreadsheetGear.IWorkbook workbook, string folderPath)
        {
            var writer = new SSGGripResultSheetWriter(this.Mode, this.Ctms, this.dtOutput, this.bw);

            DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (var fi in di.GetFiles())
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (fi.Extension.ToUpper() != ".CSV")
                {
                    continue;
                }

                // シート検索 / 作成
                string tmpSheetName = AisUtil.RemoveExtention(fi.Name);
                if (tmpSheetName.Length > 31)
                {
                    int lastindex = tmpSheetName.IndexOf("_Route");
                    if (lastindex != -1)
                    {
                        string firstpart = tmpSheetName.Substring(0, lastindex);
                        string lastpart = tmpSheetName.Substring(lastindex);
                        string cutname = firstpart.Substring(0, 31 - lastpart.Length);
                        tmpSheetName = cutname + lastpart;
                    }
                    else
                    {
                        tmpSheetName = tmpSheetName.Substring(0, 31);
                    }
                }
                SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, tmpSheetName);
                //SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, AisUtil.RemoveExtention(fi.Name));
                worksheet.Cells.Clear();

                writer.WriteByCtmResultSheetSimple(worksheet, fi.FullName, null, null);

                // 次のシートへ
                //AISTEMP-125 sunyi 20190329
                //SSG幅調整
                //worksheet.UsedRange.Columns.AutoFit();
                worksheet.UsedRange.ColumnWidth = 16.00;

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
        }

        private void WriteSummarySheet(IWorksheet worksheetCondition, System.Data.DataTable dtWithoutUnit)
        {
            DAC.Model.Util.AisUtil.DeleteUnitRow(dtWithoutUnit);
            System.Data.DataTable dtWithId = addIdToDataTable(dtWithoutUnit);
            int dtRow = dtWithId.Rows.Count;
            int dtColumn = dtWithId.Columns.Count;

            SpreadsheetGear.IRange elementRangeFirstCell = worksheetCondition.Cells["D1"];
            elementRangeFirstCell.CopyFromDataTable(dtWithId, SpreadsheetGear.Data.SetDataFlags.None);
        }

        protected abstract void WriteConfigSheet(IWorksheet worksheetParam, Dictionary<ExcelConfigParam, object> cparams);

        public static void WriteByCtmResultSheet(IWorksheet worksheet, CtmObject originalCtm, string[,] rows, BackgroundWorker bw, CsvFile csvFile)
        {
            Dictionary<string, string> id2Name = new Dictionary<string, string>();
            foreach (var element in originalCtm.GetAllElements())
            {
                id2Name.Add(element.id.ToString(), element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
            }

            string ctmName = originalCtm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

            SpreadsheetGear.IRange cells = worksheet.Cells;

            // Wang Issue AISBUL-10 20181113 Start
            //int rowIndex = 3;
            int rowIndex = 1;
            // Wang Issue AISBUL-10 20181113 End
            int blockRowIndex = 0;

            // Header
            cells["A1"].Value = Keywords.RECEIVE_TIME;
            cells["B1"].Value = Keywords.CTM_NAME;

            int headCount = 2;

            // エレメント名
            for (int i = 0; i < rows.GetLength(0); i++)
            {
                // Cancel Check
                if (bw.CancellationPending)
                {
                    return;
                }

                // エレメント名
                cells[0, headCount].Value = rows[i, 0];
                // Wang Issue AISBUL-10 20181113 Start
                //cells[1, headCount].Value = rows[i, 1];
                //cells[2, headCount].Value = rows[i, 2];
                // Wang Issue AISBUL-10 20181113 End

                headCount++;
            }

            if (csvFile == null)
            {
                return;
            }

            string[,] values = new string[AisUtil.MAX_BUFF_ROWS, headCount];

            using (var sr = new StreamReader(csvFile.Filepath, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var headers = reader.FieldHeaders;

                var pos2Pos = new Dictionary<int, int>();

                for (int i = 1; i < headers.Length; i++)
                {
                    // Cancel Check
                    if (bw.CancellationPending)
                    {
                        return;
                    }

                    var id1 = headers[i];

                    if (!id2Name.ContainsKey(id1)) continue;

                    var name1 = id2Name[id1];
                    int pos = GetPositionInDataRowS(rows, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 2); // RT and CTM_NAME
                    }
                }

                do
                {
                    // Cancel Check
                    if (bw.CancellationPending)
                    {
                        return;
                    }

                    var rec = reader.CurrentRecord;
                    long rt = Int64.Parse(rec[0]);
                    values[blockRowIndex, 0] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);
                    values[blockRowIndex, 1] = ctmName;

                    for (int j = 1; j < rec.Length; j++)
                    {
                        // Cancel Check
                        if (bw.CancellationPending)
                        {
                            return;
                        }

                        int columnIndex;
                        if (pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            values[blockRowIndex, columnIndex] = rec[j];
                        }
                    }

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;
                        SpreadsheetGear.IRange range = cells[rowIndex, 0, currentRowIndex - 1, headCount - 1];
                        range.Value = values;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;

                        // Cancel Check
                        if (bw.CancellationPending)
                        {
                            return;
                        }
                    }
                } while (reader.Read());

                if (0 != blockRowIndex)
                {
                    int currentRowIndex = rowIndex + blockRowIndex;
                    SpreadsheetGear.IRange range = cells[rowIndex, 0, currentRowIndex - 1, headCount - 1];
                    range.Value = values;
                }
            }
        }

        /// <summary>
        /// Range取込
        /// </summary>
        /// <param name="srchRange"></param>
        /// <param name="strWhat"></param>
        /// <returns></returns>
        protected SpreadsheetGear.IRange GetFindRange(SpreadsheetGear.IRange srchRange, String strWhat)
        {
            return srchRange.Find(strWhat,
                                null,
                                SpreadsheetGear.FindLookIn.Values,
                                SpreadsheetGear.LookAt.Whole,
                                SpreadsheetGear.SearchOrder.ByRows,
                                SpreadsheetGear.SearchDirection.Next,
                                true);
        }

    }
}
