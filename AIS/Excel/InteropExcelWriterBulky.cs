﻿using DAC.Model;
using DAC.Model.Util;
using CsvHelper;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Diagnostics;
using DAC.View;
using log4net;
using DAC.Util;

namespace DAC.AExcel
{
    class InteropExcelWriterBulky : InteropExcelWriter
    {
        private List<int> selectedRowNumber;
        private List<int> selectedColumnNumber;

        public InteropExcelWriterBulky(ControlMode mode, DataSourceType ds, CtmObject ctm, BackgroundWorker bw, Mission mi, List<int> rows, List<int> cols)
            : base(mode, ds, null, null, bw, mi)
        {
            this.Ctms = new List<CtmObject>();
            this.Ctms.Add(ctm);
            this.selectedRowNumber = rows;
            this.selectedColumnNumber = cols;
        }

        public override void WriteCtmData(string filePathDest, string srcFile, Dictionary<ExcelConfigParam, object> configParams)
        {
            Microsoft.Office.Interop.Excel.Application excel = null;
            Microsoft.Office.Interop.Excel.Workbooks workbooks = null;
            Microsoft.Office.Interop.Excel.Workbook workbook = null;

            try
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ワークブック作成
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = false;
                workbooks = excel.Workbooks;
                workbook = workbooks.Open(filePathDest);

                // 表紙
                Worksheet worksheetCondition = AisUtil.GetSheetFromSheetName_Interop(workbook, GetSummarySheetName());
                WriteSummarySheet(worksheetCondition, this.dtOutput, configParams);

                // paramシート作成
                string paramSheetName = Keywords.PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetParam = AisUtil.GetSheetFromSheetName_Interop(workbook, paramSheetName);
                WriteConfigSheet(worksheetParam, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = Keywords.RO_PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_Interop(workbook, roParamSheetName);
                WriteRoParamSheet_Interop(worksheetRoParam);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                string dataSheetName = "データ";
                Worksheet worksheetData = AisUtil.GetSheetFromSheetName_Interop(workbook, dataSheetName);

                var direction = GetStringVal(configParams, ExcelConfigParam.B_CONNECTION, "横");
                if (direction == "横")
                {
                    WriteBulkySheetHorizontal(worksheetData, srcFile);
                }
                else
                {
                    WriteBulkySheetVertical(worksheetData, srcFile);
                }
                worksheetData.Select();
                worksheetData.Cells[1, 1].Select();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 保存
                workbook.Save();
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close(false);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbook);
                    workbook = null;
                }

                if (workbooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbooks);
                    workbooks = null;
                }

                if (excel != null)
                {
                    excel.Quit();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
                    excel = null;
                }
            }
        }

        protected override void WriteSummarySheet(Microsoft.Office.Interop.Excel.Worksheet worksheetCondition, System.Data.DataTable dt, Dictionary<ExcelConfigParam, object> configParams)
        {
            DateTime start = GetDateTimeVal(configParams, ExcelConfigParam.START, DateTime.MinValue);
            DateTime end = GetDateTimeVal(configParams, ExcelConfigParam.END, DateTime.MinValue);

            string missionName = GetStringVal(configParams, ExcelConfigParam.MISSION_NAME, null);
            string missionId = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, null);

            // 収集条件を出力
            Microsoft.Office.Interop.Excel.Range cellsCondition = worksheetCondition.Cells;
            for (int i = 1; i <= 10; i++)   // とりあえず10×10のセルを検索
            {
                for (int j = 1; j <= 10; j++)
                {
                    // null check
                    if (cellsCondition[i, j].Value == null)
                    {
                        continue;
                    }

                    // ミッション
                    if (cellsCondition[i, j].Value.ToString() == "ミッション")
                    {
                        cellsCondition[i, j + 1].Value = missionName;
                        continue;
                    }

                    // ミッションID
                    if (cellsCondition[i, j].Value.ToString() == "ミッションID")
                    {
                        cellsCondition[i, j + 1].Value = missionId;
                        continue;
                    }

                    // 収集開始日時
                    if (cellsCondition[i, j].Value.ToString() == "収集開始日時")
                    {
                        cellsCondition[i, j + 1].Value = start.ToString("yyyy/MM/dd");
                        cellsCondition[i, j + 2].Value = start.ToString("HH:mm:ss");
                        continue;
                    }

                    // 収集終了日時
                    if (cellsCondition[i, j].Value.ToString() == "収集終了日時")
                    {
                        cellsCondition[i, j + 1].Value = end.ToString("yyyy/MM/dd");
                        cellsCondition[i, j + 2].Value = end.ToString("HH:mm:ss");
                        continue;
                    }
                }
            }
        }

        protected override void WriteConfigSheet(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            string fileName = GetStringVal(configParams, ExcelConfigParam.REGISTERED_FILENAME, string.Empty);
            string ctmId = GetStringVal(configParams, ExcelConfigParam.CTM_ID, "");
            string elementId = GetStringVal(configParams, ExcelConfigParam.ELEMENT_ID, "");
            string ctmName = GetStringVal(configParams, ExcelConfigParam.CTM_NAME, "");
            string connectionB = GetStringVal(configParams, ExcelConfigParam.B_CONNECTION, "横");
            string missionId = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, null);

            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;
            for (int i = 1; i <= 100; i++)   // とりあえず100行ほど検索
            {
                // null check
                if (cellsParam[i, 1].Value == null)
                {
                    continue;
                }

                // 登録フォルダ
                if (cellsParam[i, 1].Value.ToString() == "登録フォルダ")
                {
                    cellsParam[i, 2].Value = AisConf.RegistryFolderPath;
                    continue;
                }

                // 登録ファイル
                if (cellsParam[i, 1].Value.ToString() == "登録ファイル")
                {
                    cellsParam[i, 2].Value = fileName;
                    continue;
                }

                // サーバIPアドレス
                if (cellsParam[i, 1].Value.ToString() == "サーバIPアドレス")
                {
                    cellsParam[i, 2].Value = AisConf.Config.CmsHost;
                    continue;
                }

                // サーバポート番号
                if (cellsParam[i, 1].Value.ToString() == "サーバポート番号")
                {
                    cellsParam[i, 2].Value = AisConf.Config.CmsPort;
                    continue;
                }

                // GRIPサーバIPアドレス
                if (cellsParam[i, 1].Value.ToString() == "GRIPサーバIPアドレス")
                {
                    cellsParam[i, 2].Value = AisConf.Config.GripServerHost;
                    continue;
                }

                // GRIPサーバポート番号
                if (cellsParam[i, 1].Value.ToString() == "GRIPサーバポート番号")
                {
                    cellsParam[i, 2].Value = AisConf.Config.GripServerPort;
                    continue;
                }

                // テンプレート名称
                if (cellsParam[i, 1].Value.ToString() == "テンプレート名称")
                {
                    cellsParam[i, 2].Value = "Bulky";
                    continue;
                }

                // CTM名
                if (cellsParam[i, 1].Value.ToString() == "CTM名")
                {
                    cellsParam[i, 2].Value = ctmName;
                    continue;
                }

                // CTMID
                if (cellsParam[i, 1].Value.ToString() == "CTMID")
                {
                    cellsParam[i, 2].Value = ctmId;
                    continue;
                }

                // エレメントID
                if (cellsParam[i, 1].Value.ToString() == "エレメントID")
                {
                    cellsParam[i, 2].Value = elementId;
                    continue;
                }

                // Bulky選択行
                if (cellsParam[i, 1].Value.ToString() == "Bulky選択行")
                {
                    // 行の選択情報を出力
                    string numbers = string.Join(",", this.selectedRowNumber);
                    cellsParam[i, 2].Value = numbers;
                    continue;
                }

                // Bulky選択列
                if (cellsParam[i, 1].Value.ToString() == "Bulky選択列")
                {
                    // 列の選択情報を出力
                    string numbers = string.Join(",", this.selectedColumnNumber);
                    cellsParam[i, 2].Value = numbers;
                    continue;
                }

                // 連結方向
                if (cellsParam[i, 1].Value.ToString() == "連結方向")
                {
                    cellsParam[i, 2].Value = connectionB;
                    continue;
                }

                // ミッションID
                if (cellsParam[i, 1].Value.ToString() == "ミッションID")
                {
                    cellsParam[i, 2].Value = missionId;
                    continue;
                }

                // 製作者名
                if (cellsParam[i, 1].Value.ToString() == "製作者名")
                {
                    cellsParam[i, 2].Value = AisConf.UserId;
                    continue;
                }

                // 収集タイプ
                if (cellsParam[i, 1].Value.ToString() == "収集タイプ")
                {
                    switch (this.dataSource)
                    {
                        case DataSourceType.CTM_DIRECT:
                            cellsParam[i, 2].Value = "CTM";
                            break;
                        case DataSourceType.MISSION:
                            cellsParam[i, 2].Value = "ミッション";
                            break;
                        case DataSourceType.GRIP:
                            cellsParam[i, 2].Value = "GRIP";
                            break;
                        default:
                            cellsParam[i, 2].Value = "";
                            break;
                    }
                    continue;
                }

                // for KMEW
                if (cellsParam[i, 1].Value.ToString() == "CmsVersion")
                {
                    cellsParam[i, 2].Value = AisConf.Config.CmsVersion;
                    continue;
                }
                if (AisConf.Config.CmsVersion == "3.5")
                {
                    var ahInfo = AisUtil.GetAhInfo(this.Ctms[0].id.ToString());

                    if (cellsParam[i, 1].Value.ToString() == "MfHost")
                    {
                        cellsParam[i, 2].Value = ahInfo.Mf;
                        continue;
                    }
                    if (cellsParam[i, 1].Value.ToString() == "MfPort")
                    {
                        cellsParam[i, 2].Value = ahInfo.MfPort;
                        continue;
                    }
                }

                // ProxyServer使用
                if (cellsParam[i, 1].Value.ToString() == "ProxyServer使用")
                {
                    cellsParam[i, 2].Value = AisConf.Config.UseProxy;
                    continue;
                }

                // ProxyServerURI
                if (cellsParam[i, 1].Value.ToString() == "ProxyServerURI")
                {
                    cellsParam[i, 2].Value = AisConf.Config.ProxyURI;
                    continue;
                }

                // IsFirstSave
                if (cellsParam[i, 1].Value.ToString() == "IsFirstSave")
                {
                    cellsParam[i, 2].Value = "";
                    continue;
                }
            }

            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        public void WriteBulkySheetHorizontal(Worksheet worksheetData, string srcFile)
        {
            Microsoft.Office.Interop.Excel.Range cells = worksheetData.Cells;
            cells.Clear();
            cells[1, 1].Value = "No";
            bool overFlg = false;

            int ctmBlockYoko = this.selectedColumnNumber.Count;
            int ctmBlockTate = this.selectedRowNumber.Count + 1; // include header
            int numOfCtmBlocks = AisUtil.MAX_BUFF_ROWS / ctmBlockTate;

            for (int i = 1; i < ctmBlockTate; i++)
            {
                cells[i + 1, 1].Value = i;
            }

            Debug.Assert(numOfCtmBlocks > 0, "MAX_BUFF_ROWS is too small.");

            int buffTate = ctmBlockTate;
            int buffYoko = ctmBlockYoko * numOfCtmBlocks;  // not include row number
            if (buffYoko + 1 > EXCEL_MAX_COLS)
            {
                // 列多すぎ
                int buffYokoMax = EXCEL_MAX_COLS - 1;
                int numOfCtmBlocs1 = buffYokoMax / ctmBlockYoko;
                buffYoko = ctmBlockYoko * numOfCtmBlocs1;
                logger.Info(string.Format("ctmBlockYoko is too large: ({0},{1}) Use {2} as buffYoko.", ctmBlockTate, ctmBlockYoko, buffYoko));
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }

            object[,] values = new object[buffTate, buffYoko];

            using (var sr = new StreamReader(srcFile, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                bool hasNext = reader.Read();
                if (!hasNext)
                {
                    return;
                }

                string[] header = reader.FieldHeaders;

                // 最初のヘッダ
                int headerIndex = 0;
                for (int i = 0; i < header.Length; i++)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }
                    if (!this.selectedColumnNumber.Contains(i))
                    {
                        continue;
                    }

                    var val = string.Format("{0}_{1}", header[i], 1);
                    values[0, headerIndex] = val;
                    headerIndex++;
                }

                // 各行についてループ
                int rowNumber = 0;          // 行番号
                int blockRowIndex = 1;
                int ctmIndex = 0;
                int ctmBlockIndex = 0;
                int ctmNumber = 1;
                do
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    string[] row = reader.CurrentRecord;

                    // ヘッダーかどうか判定
                    bool isHeader = true;
                    for (int i = 0; i < row.Length; i++)
                    {
                        if (row[i] != header[i])
                        {
                            isHeader = false;
                            break;
                        }
                    }

                    if (isHeader)
                    {
                        ctmBlockIndex++;

                        if (ctmBlockIndex == numOfCtmBlocks)
                        {
                            int currentCtmIndex = ctmIndex + ctmBlockIndex;

                            int excelStartColIndex = ctmIndex * ctmBlockYoko + 2;

                            if (overFlg) break;
                            if (excelStartColIndex + this.selectedColumnNumber.Count * ctmBlockIndex + 2 > EXCEL_MAX_COLS)
                            {
                                logger.Info(string.Format("Too many records. Stop processing. ctmBlockIndex={0}", ctmBlockIndex));
                                buffYoko = EXCEL_MAX_COLS - ctmIndex * ctmBlockYoko - 1;
                                overFlg = true;
                            }

                            Microsoft.Office.Interop.Excel.Range range = worksheetData.Cells[1, excelStartColIndex];
                            range = range.get_Resize(buffTate, buffYoko);
                            range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                            ctmIndex = currentCtmIndex;
                            ctmBlockIndex = 0;
                        }

                        // ヘッダー出力
                        int blockColIndex = this.selectedColumnNumber.Count * ctmBlockIndex;
                        if (blockColIndex + this.selectedColumnNumber.Count + 2> EXCEL_MAX_COLS)
                        {
                            // 列多すぎ
                            ctmBlockIndex--;
                            logger.Info(string.Format("Too many records. Stop processing. ctmBlockIndex={0}", ctmBlockIndex));
                            break;
                        }

                        for (int i = 0; i < header.Length; i++)
                        {
                            // Cancel Check
                            if (this.bw.CancellationPending)
                            {
                                return;
                            }
                            if (!this.selectedColumnNumber.Contains(i))
                            {
                                continue;
                            }

                            var valH = string.Format("{0}_{1}", header[i], ctmNumber + 1);
                            values[0, blockColIndex] = valH;
                            blockColIndex++;
                        }

                        rowNumber = 0;  // 行番号をリセット
                        blockRowIndex = 1;
                        ctmNumber++;    // 次のCTMへ
                    }
                    else
                    {
                        //ISSUE_NO.645 sunyi 2018/05/24 start
                        //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                        if (AisUtil.RowClickIsEnable)
                        {
                        //ISSUE_NO.645 sunyi 2018/05/24 end
                            if (!this.selectedRowNumber.Contains(rowNumber))
                            {
                                // 選択された行番号でなければスキップ
                                rowNumber++;
                                continue;
                            }
                        }

                        //cells[blockRowIndex + 1, 1].Value = blockRowIndex;
                        int blockColIndex = this.selectedColumnNumber.Count * ctmBlockIndex;

                        for (int i = 0; i < row.Length; i++)
                        {
                            // Cancel Check
                            if (this.bw.CancellationPending)
                            {
                                return;
                            }
                            if (!this.selectedColumnNumber.Contains(i))
                            {
                                // 選択された列番号でなければスキップ
                                continue;
                            }

                            values[blockRowIndex, blockColIndex] = row[i];
                            blockColIndex++;
                        }

                        rowNumber++;
                        blockRowIndex++;
                    }
                } while (reader.Read());

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                int rowLength = 0;
                int columnLength2 = 0;
                if (overFlg)
                {
                    rowLength = this.selectedRowNumber.Count + 1;
                    columnLength2 = EXCEL_MAX_COLS;
                }
                else
                { 
                    int currentCtmIndexF = ctmIndex + ctmBlockIndex;
                    int excelStartColIndexF = ctmIndex * ctmBlockYoko + 2;
                    int excelRangeSizeYokoF = (ctmBlockIndex + 1) * this.selectedColumnNumber.Count;

                    Microsoft.Office.Interop.Excel.Range rangeF = worksheetData.Cells[1, excelStartColIndexF];
                    rangeF = rangeF.get_Resize(buffTate, excelRangeSizeYokoF);
                    rangeF.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // 線
                     rowLength = this.selectedRowNumber.Count + 1;
                     columnLength2 = this.selectedColumnNumber.Count * (currentCtmIndexF + 1) + 1;
                }

                Microsoft.Office.Interop.Excel.Range cellA = worksheetData.Cells[1, 1];
                Microsoft.Office.Interop.Excel.Range cellB = worksheetData.Cells[rowLength, columnLength2];

                Microsoft.Office.Interop.Excel.Range usedRange = worksheetData.Range[cellA, cellB];
                AisUtil.WriteBorder_Interop(worksheetData, usedRange);
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }
        }

        public void WriteBulkySheetVertical(Worksheet worksheetData, string srcFile)
        {
            Microsoft.Office.Interop.Excel.Range cells = worksheetData.Cells;
            cells.Clear();
            cells[1, 1].Value = "No";
            int excelColNumber = 0;

            using (var sr = new StreamReader(srcFile, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                bool hasNext = reader.Read();
                if (!hasNext)
                {
                    return;
                }

                int ctmNumber = 0;          // いくつめのCTMか
                string[] header = reader.FieldHeaders;

                // 最初のヘッダ
                excelColNumber = 2;
                for (int i = 0; i < header.Length; i++)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }
                    if (!this.selectedColumnNumber.Contains(i))
                    {
                        continue;
                    }

                    cells[1, excelColNumber].Value = string.Format("{0}_{1}", header[i], ctmNumber + 1);
                    excelColNumber++;
                }
                ctmNumber++;

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 各行についてループ
                int rowNumber = 0;          // 行番号
                int excelRowNumber = 2;
                object[,] values = new object[AisUtil.MAX_BUFF_ROWS, excelColNumber - 1];
                int rowIndex = 2;
                int blockRowIndex = 0;
                do
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    string[] row = reader.CurrentRecord;

                    // ヘッダーかどうか判定
                    bool isHeader = true;
                    for (int i = 0; i < row.Length; i++)
                    {
                        if (row[i] != header[i])
                        {
                            isHeader = false;
                            break;
                        }
                    }

                    if (isHeader)
                    {
                        rowNumber = 0;
                    }
                    else
                    {
                        //ISSUE_NO.645 sunyi 2018/05/24 start
                        //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                        if (AisUtil.RowClickIsEnable)
                        {
                        //ISSUE_NO.645 sunyi 2018/05/24 end
                            if (!this.selectedRowNumber.Contains(rowNumber))
                            {
                                // 選択された行番号でなければスキップ
                                rowNumber++;
                                continue;
                            }
                        }

                        values[blockRowIndex, 0] = (excelRowNumber - 1).ToString();

                        excelColNumber = 2;
                        for (int i = 0; i < row.Length; i++)
                        {
                            // Cancel Check
                            if (this.bw.CancellationPending)
                            {
                                return;
                            }
                            if (!this.selectedColumnNumber.Contains(i))
                            {
                                // 選択された列番号でなければスキップ
                                continue;
                            }

                            values[blockRowIndex, excelColNumber - 1] = row[i];
                            excelColNumber++;
                        }

                        rowNumber++;
                        excelRowNumber++;
                        blockRowIndex++;

                        if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                        {
                            int currentRowIndex = rowIndex + blockRowIndex;

                            Microsoft.Office.Interop.Excel.Range range = worksheetData.Cells[rowIndex, 1];
                            range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, values.GetLength(1));
                            range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                            rowIndex = currentRowIndex;
                            blockRowIndex = 0;
                        }

                    }
                } while (reader.Read());

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (0 != blockRowIndex)
                {
                    Microsoft.Office.Interop.Excel.Range range = worksheetData.Cells[rowIndex, 1];
                    range = range.get_Resize(blockRowIndex, values.GetLength(1));
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);
                }

                // 線
                int rowLength = excelRowNumber - 1;
                int columnLength = excelColNumber - 1;
                Microsoft.Office.Interop.Excel.Range cellA = worksheetData.Cells[1, 1];
                Microsoft.Office.Interop.Excel.Range cellB = worksheetData.Cells[rowLength, columnLength];

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                Microsoft.Office.Interop.Excel.Range usedRange = worksheetData.Range[cellA, cellB];
                AisUtil.WriteBorder_Interop(worksheetData, usedRange);
            }
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(InteropExcelWriterBulky));
    }
}
