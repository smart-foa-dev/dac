﻿using DAC.Model;
using DAC.Model.Util;
using DAC.View;
using SpreadsheetGear;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;

namespace DAC.AExcel
{
    class SSGExcelWriterPastStockTime : ExcelWriterYotobetsu
    {
        public SSGExcelWriterPastStockTime(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi = null)
            : base(mode, ds, ctms, dt, bw, mi)
        {

        }


        protected override void WriteConfigSheet(IWorksheet worksheetParam, Dictionary<ExcelConfigParam, object> cparams)
        {
            string missionName = GetStringVal(cparams, ExcelConfigParam.MISSION_NAME, null);
            string missionId = GetStringVal(cparams, ExcelConfigParam.MISSION_ID, null);

            string startElement = GetStringVal(cparams, ExcelConfigParam.KAISHI_ELEM, "");
            string endElement = GetStringVal(cparams, ExcelConfigParam.SHURYO_ELEM, "");
            string getStart = GetStringVal(cparams, ExcelConfigParam.SHUTOKU_KAISHI, "");
            string getEnd = GetStringVal(cparams, ExcelConfigParam.SHUTOKU_SHURYO, "");
            string reloadInterval = GetStringVal(cparams, ExcelConfigParam.RELOAD_INTERVAL, "");
            string displayPeriod = GetStringVal(cparams, ExcelConfigParam.DISPLAY_PERIOD, "");
            string displayStart = GetStringVal(cparams, ExcelConfigParam.DISPLAY_START, "");
            string displayEnd = GetStringVal(cparams, ExcelConfigParam.DISPLAY_END, "");
            bool isOnline = GetBoolVal(cparams, ExcelConfigParam.ONLINE_FLAG, false);
            string templateName = GetStringVal(cparams, ExcelConfigParam.TEMPLATE_NAME, "");

            SpreadsheetGear.IRange srchRange;
            SpreadsheetGear.IRange cellsParam = worksheetParam.Cells;

            // ミッション名
            srchRange = GetFindRange(cellsParam, "ミッション");
            if (srchRange != null)
            {
                srchRange.Offset(0, 1).Value = missionName;
            }

            // ミッションID
            srchRange = GetFindRange(cellsParam, "ミッションID");
            if (srchRange != null)
            {
                srchRange.Offset(0, 1).Value = missionId;
            }

            // 登録フォルダ
            srchRange = GetFindRange(cellsParam, "登録フォルダ");
            if (srchRange != null) srchRange.Offset(0, 1).Value = AisConf.RegistryFolderPath;

            // サーバIPアドレス
            srchRange = GetFindRange(cellsParam, "サーバIPアドレス");
            if (srchRange != null) srchRange.Offset(0, 1).Value = AisConf.Config.CmsHost.TrimEnd();

            // サーバポート番号
            srchRange = GetFindRange(cellsParam, "サーバポート番号");
            if (srchRange != null) srchRange.Offset(0, 1).Value = AisConf.Config.CmsPort.TrimEnd();

            // GRIPサーバIPアドレス
            srchRange = GetFindRange(cellsParam, "GRIPサーバIPアドレス");
            if (srchRange != null) srchRange.Offset(0, 1).Value = AisConf.Config.GripServerHost.TrimEnd();

            // GRIPサーバポート番号
            srchRange = GetFindRange(cellsParam, "GRIPサーバポート番号");
            if (srchRange != null) srchRange.Offset(0, 1).Value = AisConf.Config.GripServerPort.TrimEnd();

            // テンプレート名称
            srchRange = GetFindRange(cellsParam, "テンプレート名称");
            if (srchRange != null)
            {
                srchRange.Offset(0, 1).Value = templateName;
            }

            // 終了エレメントDtPickerStart
            srchRange = GetFindRange(cellsParam, "終了エレメント");
            if (srchRange != null) srchRange.Offset(0, 1).Value = endElement;

            // 開始エレメント
            srchRange = GetFindRange(cellsParam, "開始エレメント");
            if (srchRange != null) srchRange.Offset(0, 1).Value = startElement;

            // 取得開始
            srchRange = GetFindRange(cellsParam, "取得開始");
            if (srchRange != null) srchRange.Offset(0, 1).Value = getStart;

            // 取得終了
            srchRange = GetFindRange(cellsParam, "取得終了");
            if (srchRange != null) srchRange.Offset(0, 1).Value = getEnd;

            // 更新周期
            srchRange = GetFindRange(cellsParam, "周期");
            if (srchRange != null) srchRange.Offset(0, 1).Value = reloadInterval;

            // 表示期間
            srchRange = GetFindRange(cellsParam, "表示期間");
            if (srchRange != null) srchRange.Offset(0, 1).Value = displayPeriod;

            // 表示期間
            srchRange = GetFindRange(cellsParam, "表示開始期間");
            if (srchRange != null) srchRange.Offset(0, 1).Value = displayStart;

            // 表示期間
            srchRange = GetFindRange(cellsParam, "表示終了期間");
            if (srchRange != null) srchRange.Offset(0, 1).Value = displayEnd;

            // 前回更新日時
            srchRange = GetFindRange(cellsParam, "前回更新日時");
            if (srchRange != null) srchRange.Offset(0, 1).Value = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

            // 前回更新日時
            srchRange = GetFindRange(cellsParam, "前回更新日時");
            if (srchRange != null) srchRange.Offset(0, 1).Value = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

            // 前回更新日時
            srchRange = GetFindRange(cellsParam, "前回更新日時");
            if (srchRange != null) srchRange.Offset(0, 1).Value = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

            // ミッションID
            srchRange = GetFindRange(cellsParam, "ミッションID");
            if (srchRange != null) srchRange.Offset(0, 1).Value = missionId;

            // 製作者名
            srchRange = GetFindRange(cellsParam, "製作者名");
            if (srchRange != null) srchRange.Offset(0, 1).Value = AisConf.UserId;

            // 収集タイプ
            srchRange = GetFindRange(cellsParam, "収集タイプ");
            if (srchRange != null)
            {
                switch (this.dataSource)
                {
                    case DataSourceType.CTM_DIRECT:
                        srchRange.Offset(0, 1).Value = "CTM";
                        break;
                    case DataSourceType.MISSION:
                        srchRange.Offset(0, 1).Value = "ミッション";
                        break;
                    case DataSourceType.GRIP:
                        srchRange.Offset(0, 1).Value = "GRIP";
                        break;
                    default:
                        srchRange.Offset(0, 1).Value = "";
                        break;
                }
            }

            // Gripの場合のみ出力
            if (this.dataSource == DataSourceType.GRIP)
            {
                // 問合開始日時
                srchRange = GetFindRange(cellsParam, "問合開始日時");
                if (srchRange != null) srchRange.Offset(0, 1).Value = getStart;

                // 問合終了日時
                srchRange = GetFindRange(cellsParam, "問合終了日時");
                if (srchRange != null) srchRange.Offset(0, 1).Value = getEnd;

                // シート一覧
                srchRange = GetFindRange(cellsParam, "シート一覧");
                if (srchRange != null)
                {
                    string[] files = getCsvFileNames(this.folderPath);
                    srchRange.Offset(0, 1).Value = string.Join(",", files);
                }
            }

            if (isOnline)
            {
                srchRange = GetFindRange(cellsParam, "オンライン");
                if (srchRange != null) srchRange.Offset(0, 1).Value = "ONLINE";

                srchRange = GetFindRange(cellsParam, "自動更新");
                if (srchRange != null) srchRange.Offset(0, 1).Value = "ON";
            }
            else
            {
                srchRange = GetFindRange(cellsParam, "オンライン");
                if (srchRange != null) srchRange.Offset(0, 1).Value = "OFFLINE";

                srchRange = GetFindRange(cellsParam, "自動更新");
                if (srchRange != null) srchRange.Offset(0, 1).Value = "OFF";
            }

            // for KMEW
            srchRange = GetFindRange(cellsParam, "CmsVersion");
            if (srchRange != null) srchRange.Offset(0, 1).Value = AisConf.Config.CmsVersion;
            if (AisConf.Config.CmsVersion == "3.5")
            {
                var ahInfo = AisUtil.GetAhInfo(this.Ctms[0].id.ToString());

                srchRange = GetFindRange(cellsParam, "MfHost");
                if (srchRange != null) srchRange.Offset(0, 1).Value = ahInfo.Mf;
                srchRange = GetFindRange(cellsParam, "MfPort");
                if (srchRange != null) srchRange.Offset(0, 1).Value = ahInfo.MfPort;
            }

            // ProxyServer使用
            srchRange = GetFindRange(cellsParam, "ProxyServer使用");
            if (srchRange != null) srchRange.Offset(0, 1).Value = AisConf.Config.UseProxy;

            // ProxyServerURI
            srchRange = GetFindRange(cellsParam, "ProxyServerURI");
            if (srchRange != null) srchRange.Offset(0, 1).Value = AisConf.Config.ProxyURI;

            //ISSUE_NO.710 sunyi 2018/05/25 start
            //パラメータ追加
            // IsFirstSave
            srchRange = GetFindRange(cellsParam, "IsFirstSave");
            if (srchRange != null) srchRange.Offset(0, 1).Value = "";
            //ISSUE_NO.710 sunyi 2018/05/25 end
        }

        private string[] getCsvFileNames(string folderPath)
        {
            if (!Directory.Exists(folderPath))
            {
                return new string[0];
            }

            List<string> files = new List<string>();
            DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (DirectoryInfo subDi in di.GetDirectories())
            {
                if (!subDi.Exists)
                {
                    continue;
                }

                string[] filesInSub = new string[0];
                filesInSub = getCsvFileNames(subDi.FullName);
                files.AddRange(filesInSub);
            }

            foreach (FileInfo fi in di.GetFiles())
            {
                if (!fi.Exists)
                {
                    continue;
                }
                if (fi.Extension.ToUpper() != ".CSV")
                {
                    continue;
                }

                files.Add(AisUtil.RemoveExtention(fi.Name));
            }

            return files.ToArray();
        }
    }
}
