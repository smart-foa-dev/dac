﻿using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using FoaCore.Common;
using FoaCore.Common.Util;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.AExcel
{
    class InteropExcelWriterComment: InteropExcelWriter
    {
        public InteropExcelWriterComment(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, System.Data.DataTable dt, BackgroundWorker bw, Mission mi = null)
            : base(mode, ds, ctms, dt, bw, mi)
        {

        }

        protected override void WriteConfigSheet(Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> cparams)
        {
            string missionName = GetStringVal(cparams, ExcelConfigParam.MISSION_NAME, null);
            string missionId = GetStringVal(cparams, ExcelConfigParam.MISSION_ID, null);

            string startElement = GetStringVal(cparams, ExcelConfigParam.KAISHI_ELEM, string.Empty);
            string endElement = GetStringVal(cparams, ExcelConfigParam.SHURYO_ELEM, string.Empty);

            string reloadInterval = GetStringVal(cparams, ExcelConfigParam.RELOAD_INTERVAL, string.Empty);
            string displayPeriod = GetStringVal(cparams, ExcelConfigParam.DISPLAY_PERIOD, string.Empty);
            string displayStart = GetStringVal(cparams, ExcelConfigParam.DISPLAY_START, string.Empty);
            string displayEnd = GetStringVal(cparams, ExcelConfigParam.DISPLAY_END, string.Empty);

            string start = GetStringVal(cparams, ExcelConfigParam.START, string.Empty);
            string end = GetStringVal(cparams, ExcelConfigParam.END, string.Empty);

            bool isOnline = GetBoolVal(cparams, ExcelConfigParam.ONLINE_FLAG, false);

            string templateName = GetStringVal(cparams, ExcelConfigParam.TEMPLATE_NAME, string.Empty);

            Range srchRange;
            Range cellsParam = worksheetParam.Cells;

            // ミッション名
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "ミッション");
            if (srchRange != null)
            {
                srchRange[1, 2].Value = missionName;
            }

            // ミッションID
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "ミッションID");
            if (srchRange != null)
            {
                srchRange[1, 2].Value = missionId;
            }

            // 登録フォルダ
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "登録フォルダ");
            if (srchRange != null) srchRange[1, 2].Value = AisConf.RegistryFolderPath;

            // サーバIPアドレス
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "サーバIPアドレス");
            if (srchRange != null) srchRange[1, 2].Value = AisConf.Config.CmsHost.TrimEnd();

            // サーバポート番号
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "サーバポート番号");
            if (srchRange != null) srchRange[1, 2].Value = AisConf.Config.CmsPort.TrimEnd();

            // GRIPサーバIPアドレス
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "GRIPサーバIPアドレス");
            if (srchRange != null) srchRange[1, 2].Value = AisConf.Config.GripServerHost.TrimEnd();

            // GRIPサーバポート番号
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "GRIPサーバポート番号");
            if (srchRange != null) srchRange[1, 2].Value = AisConf.Config.GripServerPort.TrimEnd();

            // テンプレート名称
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "テンプレート名称");
            if (srchRange != null)
            {
                srchRange[1, 2].Value = templateName;
            }

            // 終了エレメントDtPickerStart
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "終了エレメント");
            if (srchRange != null) srchRange[1, 2].Value = endElement;

            // 開始エレメント
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "開始エレメント");
            if (srchRange != null) srchRange[1, 2].Value = startElement;

            // 取得開始
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "取得開始");
            if (srchRange != null) srchRange[1, 2].Value = start;

            // 取得終了
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "取得終了");
            if (srchRange != null) srchRange[1, 2].Value = end;

            // 更新周期
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "周期");
            if (srchRange != null) srchRange[1, 2].Value = reloadInterval;

            // 表示期間
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "表示期間");
            if (srchRange != null) srchRange[1, 2].Value = displayPeriod;

            // 表示期間
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "表示開始期間");
            if (srchRange != null) srchRange[1, 2].Value = displayStart;

            // 表示期間
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "表示終了期間");
            if (srchRange != null) srchRange[1, 2].Value = displayEnd;

            // 前回更新日時
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "前回更新日時");
            if (srchRange != null) srchRange[1, 2].Value = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

            // ミッションID
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "ミッションID");
            if (srchRange != null) srchRange[1, 2].Value = missionId;

            // 製作者名
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "製作者名");
            if (srchRange != null) srchRange[1, 2].Value = AisConf.UserId;

            // テンプレート名称
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "テンプレート名称");
            if (srchRange != null) srchRange[1, 2].Value = "コメント";

            // 収集タイプ
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "収集タイプ");
            if (srchRange != null)
            {
                switch (this.dataSource)
                {
                    case DataSourceType.CTM_DIRECT:
                        srchRange[1, 2].Value = "CTM";
                        break;
                    case DataSourceType.MISSION:
                        srchRange[1, 2].Value = "ミッション";
                        break;
                    case DataSourceType.GRIP:
                        srchRange[1, 2].Value = "GRIP";
                        break;
                    default:
                        srchRange[1, 2].Value = "";
                        break;
                }
            }

            // シート一覧(GRIP用)
            if (this.dataSource == DataSourceType.GRIP)
            {
                srchRange = AisUtil.GetFindRange_Interop(cellsParam, "シート一覧");
                if (srchRange != null)
                {
                    string[] files = getCsvFileNames(this.folderPath);
                    srchRange[1, 2].Value = string.Join(",", files);
                }
            }

            if (isOnline)
            {
                srchRange = AisUtil.GetFindRange_Interop(cellsParam, "オンライン");
                if (srchRange != null) srchRange[1, 2].Value = "ONLINE";

                srchRange = AisUtil.GetFindRange_Interop(cellsParam, "自動更新");
                if (srchRange != null) srchRange[1, 2].Value = "ON";
            }
            else
            {
                srchRange = AisUtil.GetFindRange_Interop(cellsParam, "オンライン");
                if (srchRange != null) srchRange[1, 2].Value = "OFFLINE";

                srchRange = AisUtil.GetFindRange_Interop(cellsParam, "自動更新");
                if (srchRange != null) srchRange[1, 2].Value = "OFF";
            }

            // for KMEW
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "CmsVersion");
            if (srchRange != null)
            {
                srchRange[1, 2].Value = AisConf.Config.CmsVersion;
            }
            if (AisConf.Config.CmsVersion == "3.5")
            {
                var ahInfo = AisUtil.GetAhInfo(this.Ctms[0].id.ToString());

                srchRange = AisUtil.GetFindRange_Interop(cellsParam, "MfHost");
                if (srchRange != null)
                {
                    srchRange[1, 2].Value = ahInfo.Mf;
                }
                srchRange = AisUtil.GetFindRange_Interop(cellsParam, "MfPort");
                if (srchRange != null)
                {
                    srchRange[1, 2].Value = ahInfo.MfPort;
                }
            }

            // ProxyServer使用
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "ProxyServer使用");
            if (srchRange != null)
            {
                srchRange[1, 2].Value = AisConf.Config.UseProxy;
            }

            // ProxyServerURI
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "ProxyServerURI");
            if (srchRange != null)
            {
                srchRange[1, 2].Value = AisConf.Config.ProxyURI;
            }

            // IsFirstSave
            srchRange = AisUtil.GetFindRange_Interop(cellsParam, "IsFirstSave");
            if (srchRange != null)
            {
                srchRange[1, 2].Value = "";
            }

        }

        protected override void WriteIntegratedResultSheet(Microsoft.Office.Interop.Excel.Workbook workbook, System.Data.DataTable dtOutput, List<XYZ> srcFiles, long displayStartCurrentBlock = 0)
        {
            //ISSUE_NO.660 sunyi 2018/04/25 start
            //CTM名が変更した場合、エラーがしないように修正する
            if (srcFiles.Count == 0)
            {
                return;
            }
            //ISSUE_NO.660 sunyi 2018/04/25 end
            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }

            Microsoft.Office.Interop.Excel.Worksheet worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, "result");
            worksheet.Rows.Clear();
            Microsoft.Office.Interop.Excel.Range cells = worksheet.Cells;

            // ヘッダー
            cells[1, 1].Value = Keywords.RECEIVE_TIME;
            cells[1, 2].Value = Keywords.CTM_NAME;

            int headerIdx = 3;
            bool firstColumn = true;
            foreach (DataColumn column in dtOutput.Columns)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (firstColumn)
                {
                    firstColumn = false;
                    continue;
                }
                var colName0 = column.ColumnName;
                var colName = colName0;

                cells[1, headerIdx].Value = colName;
                headerIdx++;
            }
            int headCount = headerIdx;

            //
            // レコードを書く前の準備
            //
            foreach (var csvFile in srcFiles)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                csvFile.Init();
                var reader = csvFile.reader;
                reader.Read(); // Read Header and first row

                var pos2Pos = new Dictionary<int, int>();
                var header = reader.FieldHeaders;
                for (int i = 1; i < header.Length; i++)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var id1 = header[i];
                    var el = csvFile.ctmObj.GetElement(new GUID(id1));
                    if (el == null)
                    {
                        continue;
                    }
                    var name1 = el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    int pos = AisUtil.GetPositionInDataRowForAllCtms(csvFile.dtatRow, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 2); // RT + CTM NAME
                    }

                }
                csvFile.pos2Pos = pos2Pos;
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }

            //
            // レコード
            //
            using (var merger = new MergeIterators(srcFiles))
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                int rowIndex = 2;
                int blockRowIndex = 0;

                // string[,]だと明示的に文字列として書き込んでいるようなので、
                // object[,]に変更して、Excelの方に型を判断させるように変更。
                object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headCount];

                do
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    XYZ currentIterator = merger.Current();
                    var rec = currentIterator.Current();
                    long rt = Int64.Parse(rec[0]);
                    if (rt < displayStartCurrentBlock)
                    {
                        continue;
                    }

                    values[blockRowIndex, 0] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);
                    values[blockRowIndex, 1] = currentIterator.Name();

                    for (int j = 1; j < rec.Length; j++)
                    {
                        // Cancel Check
                        if (this.bw.CancellationPending)
                        {
                            return;
                        }

                        int columnIndex;
                        if (currentIterator.pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            string item = rec[j];
                            if (item.StartsWith("=="))
                            {
                                item = "'" + item;
                            }

                            values[blockRowIndex, columnIndex] = item;
                        }
                    }

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                        range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, headCount);
                        range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                        // RT Format
                        Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 1];
                        range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;

                        // Cancel Check
                        if (this.bw.CancellationPending)
                        {
                            return;
                        }
                    }
                } while (merger.MoveNext());

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (0 != blockRowIndex)
                {
                    Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                    range = range.get_Resize(blockRowIndex, headCount);
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // RT Format
                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 1];
                    range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                    range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                }
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }
        }

        private string[] getCsvFileNames(string folderPath)
        {
            if (!Directory.Exists(folderPath))
            {
                return new string[0];
            }

            List<string> files = new List<string>();
            DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (DirectoryInfo subDi in di.GetDirectories())
            {
                if (!subDi.Exists)
                {
                    continue;
                }

                string[] filesInSub = new string[0];
                filesInSub = getCsvFileNames(subDi.FullName);
                files.AddRange(filesInSub);
            }

            foreach (FileInfo fi in di.GetFiles())
            {
                if (!fi.Exists)
                {
                    continue;
                }
                if (fi.Extension.ToUpper() != ".CSV")
                {
                    continue;
                }

                files.Add(AisUtil.RemoveExtention(fi.Name));
            }

            return files.ToArray();
        }
    }
}
