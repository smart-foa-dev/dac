﻿using DAC.CtmData;
using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using CsvHelper;
using FoaCore;
using FoaCore.Common.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using log4net;
using DAC.AExcel.Grip;
using FoaCore.Common;

namespace DAC.AExcel
{
    class InteropExcelWriter : ExcelWriter
    {
        public InteropExcelWriter(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi = null, GripMissionCtm gm = null)
            : base(mode, ds, ctms, dt, bw)
        {
            if (gm != null)
            {
                this.gripMission = gm;
            }
        }

        private GripMissionCtm gripMission;
        protected string folderPath;

        public override void WriteCtmData(string filePathDest, string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            Microsoft.Office.Interop.Excel.Application excel = null;
            Microsoft.Office.Interop.Excel.Workbooks workbooks = null;
            Microsoft.Office.Interop.Excel.Workbook workbook = null;

            try
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ワークブック作成
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = false;
                workbooks = excel.Workbooks;
                workbook = workbooks.Open(filePathDest);
                ExcelUtil.MoveExcelOutofScreen(excel);

                if (this.Mode != ControlMode.Comment)
                {
                    // シート作成
                    Microsoft.Office.Interop.Excel.Worksheet worksheetCondition = AisUtil.GetSheetFromSheetName_Interop(workbook, GetSummarySheetName());
                    WriteSummarySheet(worksheetCondition, this.dtOutput, configParams);
                }

                this.folderPath = folderPath;

                // paramシート作成
                string paramSheetName = Keywords.PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetParam = AisUtil.GetSheetFromSheetName_Interop(workbook, paramSheetName);
                if (this.Mode == ControlMode.Comment)
                {
                    writeSummarySheet(worksheetParam, this.dtOutput);
                }
                WriteConfigSheet(worksheetParam, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = Keywords.RO_PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_Interop(workbook, roParamSheetName);
                WriteRoParamSheet_Interop(worksheetRoParam);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (this.dataSource == DataSourceType.GRIP)
                {
                    writeGripResultSheets(workbook, folderPath);
                    
                }
                else
                {
                    writeResultSheets(workbook, folderPath, configParams);
                }
                //ISSUE_NO.790 sunyi 2018/07/18 Start
                //集計、オンラインテンプレートシートを非表示にする
                Microsoft.Office.Interop.Excel.Worksheet sheet;
                if (Mode.Equals(ControlMode.Spreadsheet))
                {
                    sheet = AisUtil.GetSheetFromSheetName_Interop(workbook, Keywords.SPREAD_SHEET);
                    sheet.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
                }
                if (Mode.Equals(ControlMode.Online))
                {
                    sheet = AisUtil.GetSheetFromSheetName_Interop(workbook, Keywords.ONLINE_SHEET);
                    sheet.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
                }
                //ISSUE_NO.790 sunyi 2018/07/18 End

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 保存
                workbook.Save();

                ExcelUtil.RestoreExcelWindow(excel);

            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close(false);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbook);
                    workbook = null;
                }

                if (workbooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbooks);
                    workbooks = null;
                }

                if (excel != null)
                {
                    excel.Quit();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
                    excel = null;
                }
            }
        }

        private void writeSummarySheet(Microsoft.Office.Interop.Excel.Worksheet sheet, System.Data.DataTable dtWithoutUnit)
        {
            DAC.Model.Util.AisUtil.DeleteUnitRow(dtWithoutUnit);
            System.Data.DataTable dtWithId = addIdToDataTable(dtWithoutUnit);
            int dtRow = dtWithId.Rows.Count;
            int dtColumn = dtWithId.Columns.Count;

            sheet.Range[sheet.Cells[1, 4], sheet.Cells[100, 24]].Delete(SpreadsheetGear.DeleteShiftDirection.Up);
            sheet = AisUtil.WriteDataTableToExcel(sheet, dtWithId, 0, 3);
        }

        private void writeGripResultSheets(Microsoft.Office.Interop.Excel.Workbook workbook, string folderPath)
        {
            var writer = new GripResultSheetWriter(this.Mode, this.Ctms, this.dtOutput, this.bw);

            DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (var fi in di.GetFiles())
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (fi.Extension.ToUpper() != ".CSV")
                {
                    continue;
                }

                // シート検索 / 作成
                Microsoft.Office.Interop.Excel.Worksheet worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, AisUtil.RemoveExtention(fi.Name));
                worksheet.Rows.Clear();
                writer.WriteByCtmResultSheetSimple(worksheet, fi.FullName, null, null);

                // 次のシートへ
                //AISTEMP-125 sunyi 20190329
                //SSG幅調整
                //worksheet.UsedRange.Columns.AutoFit();
                worksheet.UsedRange.ColumnWidth = 16.00;

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
        }

        private void writeResultSheets(Microsoft.Office.Interop.Excel.Workbook workbook, string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            List<XYZ> srcCsvFiles = new List<XYZ>();
            try
            {
                foreach (var ctm in this.Ctms)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var ctmId = ctm.id.ToString();
                    var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    DataRow dataRow = null;
                    foreach (var rowObject in this.dtOutput.Rows)
                    {
                        DataRow row = rowObject as DataRow;
                        if (row.ItemArray[0].ToString() == ctmName)
                        {
                            dataRow = row;
                            break;
                        }
                    }

                    if (dataRow == null)
                    {
                        // CTM自体の選択が無かったので無視
                        continue;
                    }

                    // シート検索 / 作成
                    Microsoft.Office.Interop.Excel.Worksheet worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, ctmName);
                    worksheet.Rows.Clear();

                    var srcFilepath = Path.Combine(folderPath, ctmId + ".csv");
                    CsvFile csvFileObj = null;
                    if (File.Exists(srcFilepath))
                    {
                        csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcFilepath };
                        var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                        srcCsvFiles.Add(srcCsvFile);
                    }

                    writeByCtmResultSheet(worksheet, csvFileObj, ctm, dataRow, this.bw);

                    // 次のシートへ
                    //AISTEMP-125 sunyi 20190329
                    //SSG幅調整
                    //worksheet.UsedRange.Columns.AutoFit();
                    worksheet.UsedRange.ColumnWidth = 16.00;

                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }
                }

                long displayStartCurrentTime = 0;
                DateTime now = DateTime.Now;
                DateTime displayStartCurrentTimeDateTime = GetDateTimeVal(configParams, ExcelConfigParam.DISPLAY_START_CURRENT_TIME, now);
                if (displayStartCurrentTimeDateTime != now)
                {
                    displayStartCurrentTime = UnixTime.ToLong(displayStartCurrentTimeDateTime);
                }

                // 総合結果シート作成
                WriteIntegratedResultSheet(workbook, this.dtOutput, srcCsvFiles, displayStartCurrentTime);
            }
            finally
            {
                if (srcCsvFiles != null)
                {
                    foreach (XYZ srcCsvFile in srcCsvFiles)
                    {
                        srcCsvFile.Dispose();
                    }
                }
            }
        }

        protected virtual void WriteSummarySheet(Microsoft.Office.Interop.Excel.Worksheet worksheetCondition, System.Data.DataTable dt, Dictionary<ExcelConfigParam, object> configParams)
        {
            DateTime start = GetDateTimeVal(configParams, ExcelConfigParam.START, DateTime.MinValue);
            DateTime end = GetDateTimeVal(configParams, ExcelConfigParam.END, DateTime.MinValue);
            string missionName = GetStringVal(configParams, ExcelConfigParam.MISSION_NAME, null);
            string missionId = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, null);

            // 収集条件を出力
            Microsoft.Office.Interop.Excel.Range cellsCondition = worksheetCondition.Cells;
            for (int i = 1; i <= 10; i++)   // とりあえず10×10のセルを検索
            {
                for (int j = 1; j <= 10; j++)
                {
                    // null check
                    if (cellsCondition[i, j].Value == null)
                    {
                        continue;
                    }

                    // ミッション
                    if (cellsCondition[i, j].Value.ToString() == "ミッション")
                    {
                        cellsCondition[i, j + 1].Value = missionName;
                        continue;
                    }

                    // ミッションID
                    if (cellsCondition[i, j].Value.ToString() == "ミッションID")
                    {
                        cellsCondition[i, j + 1].Value = missionId;
                        continue;
                    }

                    // 収集開始日時
                    if (cellsCondition[i, j].Value.ToString() == "収集開始日時")
                    {

                        if (start == DateTime.MinValue)
                        {
                            throw new InvalidDataException("start is not specified.");
                        }
                        cellsCondition[i, j + 1].Value = start.ToString("yyyy/MM/dd");
                        cellsCondition[i, j + 2].Value = start.ToString("HH:mm:ss");
                        continue;
                    }

                    // 収集終了日時
                    if (cellsCondition[i, j].Value.ToString() == "収集終了日時")
                    {
                        if (end == DateTime.MinValue)
                        {
                            throw new InvalidDataException("end is not specified.");
                        }
                        cellsCondition[i, j + 1].Value = end.ToString("yyyy/MM/dd");
                        cellsCondition[i, j + 2].Value = end.ToString("HH:mm:ss");
                        continue;
                    }
                }
            }

            AisUtil.DeleteUnitRow(dt);
            DataTable dtWithId = addIdToDataTable(dt);
            int dtRow = dtWithId.Rows.Count;
            int dtColumn = dtWithId.Columns.Count;

            worksheetCondition.Range[worksheetCondition.Cells[6, 1], worksheetCondition.Cells[26, 121]].Delete(SpreadsheetGear.DeleteShiftDirection.Up);
            worksheetCondition = AisUtil.WriteDataTableToExcel(worksheetCondition, dtWithId, 5, 0);

            Microsoft.Office.Interop.Excel.Range range = cellsCondition.Range[cellsCondition.Cells[6, 1], cellsCondition.Cells[6, dtColumn]];
            range.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.CornflowerBlue);
            range.Font.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
        }

        protected virtual void WriteConfigSheet(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            bool writtenFileNames = false;

            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;
            for (int i = 1; i <= 100; i++)   // とりあえず100行ほど検索
            {
                // null check
                if (cellsParam[i, 1].Value == null)
                {
                    if (this.dataSource != DataSourceType.GRIP ||
                        writtenFileNames)
                    {                                                                               
                        continue;
                    }

                    // GRIPからのデータである場合のみ、シート名一覧を記述する
                    string[] files = getCsvFileNames(this.folderPath);
                    cellsParam[i, 1].Value = "シート一覧";
                    cellsParam[i, 2].Value = string.Join(",", files);
                    writtenFileNames = true;
                    continue;
                }

                // 登録フォルダ
                if (cellsParam[i, 1].Value.ToString() == "登録フォルダ")
                {
                    cellsParam[i, 2].Value = AisConf.RegistryFolderPath;
                    continue;
                }

                // 登録ファイル
                if (cellsParam[i, 1].Value.ToString() == "登録ファイル")
                {
                    string fileName = GetStringVal(configParams, ExcelConfigParam.REGISTERED_FILENAME, string.Empty);
                    cellsParam[i, 2].Value = fileName;
                    continue;
                }

                // サーバIPアドレス
                if (cellsParam[i, 1].Value.ToString() == "サーバIPアドレス")
                {
                    cellsParam[i, 2].Value = AisConf.Config.CmsHost;
                    continue;
                }

                // サーバポート番号
                if (cellsParam[i, 1].Value.ToString() == "サーバポート番号")
                {
                    cellsParam[i, 2].Value = AisConf.Config.CmsPort;
                    continue;
                }

                // GRIPサーバIPアドレス
                if (cellsParam[i, 1].Value.ToString() == "GRIPサーバIPアドレス")
                {
                    cellsParam[i, 2].Value = AisConf.Config.GripServerHost;
                    continue;
                }

                // GRIPサーバポート番号
                if (cellsParam[i, 1].Value.ToString() == "GRIPサーバポート番号")
                {
                    cellsParam[i, 2].Value = AisConf.Config.GripServerPort;
                    continue;
                }

                // テンプレート名称
                if (cellsParam[i, 1].Value.ToString() == "テンプレート名称")
                {
                    cellsParam[i, 2].Value = "集計表";
                    continue;
                }

                // ミッションID
                if (cellsParam[i, 1].Value.ToString() == "ミッションID")
                {
                    cellsParam[i, 2].Value = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, null);
                    continue;
                }

                // 製作者名
                if (cellsParam[i, 1].Value.ToString() == "製作者名")
                {
                    cellsParam[i, 2].Value = AisConf.UserId;
                    continue;
                }

                // 収集タイプ
                if (cellsParam[i, 1].Value.ToString() == "収集タイプ")
                {
                    switch (this.dataSource)
                    {
                        case DataSourceType.CTM_DIRECT:
                            cellsParam[i, 2].Value = "CTM";
                            break;
                        case DataSourceType.MISSION:
                            cellsParam[i, 2].Value = "ミッション";
                            break;
                        case DataSourceType.GRIP:
                            cellsParam[i, 2].Value = "GRIP";
                            break;
                        default:
                            cellsParam[i, 2].Value = "";
                            break;
                    }
                    continue;
                }

                if (this.dataSource == DataSourceType.GRIP)
                {
                    if (cellsParam[i, 1].Value.ToString() == "GRIPサーバIPアドレス")
                    {
                        cellsParam[i, 2].Value = AisConf.Config.GripServerHost;
                        continue;
                    }

                    if (cellsParam[i, 1].Value.ToString() == "GRIPサーバポート番号")
                    {
                        cellsParam[i, 2].Value = AisConf.Config.GripServerPort;
                        continue;
                    }
                }

                // シート一覧(GRIP用)
                if (this.dataSource == DataSourceType.GRIP &&
                    cellsParam[i, 1].Value.ToString() == "シート一覧" &&
                    !writtenFileNames)
                {
                    string[] files = getCsvFileNames(this.folderPath);
                    cellsParam[i, 2].Value = string.Join(",", files);
                    writtenFileNames = true;
                    continue;
                }

                // for KMEW
                if (cellsParam[i, 1].Value.ToString() == "CmsVersion")
                {
                    cellsParam[i, 2].Value = AisConf.Config.CmsVersion;
                    continue;
                }
                if (AisConf.Config.CmsVersion == "3.5")
                {
                    var ahInfo = AisUtil.GetAhInfo(this.Ctms[0].id.ToString());

                    if (cellsParam[i, 1].Value.ToString() == "MfHost")
                    {
                        cellsParam[i, 2].Value = ahInfo.Mf;
                        continue;
                    }
                    if (cellsParam[i, 1].Value.ToString() == "MfPort")
                    {
                        cellsParam[i, 2].Value = ahInfo.MfPort;
                        continue;
                    }
                }

                // ProxyServer使用
                if (cellsParam[i, 1].Value.ToString() == "ProxyServer使用")
                {
                    cellsParam[i, 2].Value = AisConf.Config.UseProxy;
                    continue;
                }

                // ProxyServerURI
                if (cellsParam[i, 1].Value.ToString() == "ProxyServerURI")
                {
                    cellsParam[i, 2].Value = AisConf.Config.ProxyURI;
                    continue;
                }
                // IsFirstSave
                if (cellsParam[i, 1].Value.ToString() == "IsFirstSave")
                {
                    cellsParam[i, 2].Value = "";
                    continue;
                }
            }

            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        private string[] getCsvFileNames(string folderPath)
        {
            if (!Directory.Exists(folderPath))
            {
                return new string[0];
            }

            List<string> files = new List<string>();
            DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (DirectoryInfo subDi in di.GetDirectories())
            {
                if (!subDi.Exists)
                {
                    continue;
                }

                string[] filesInSub = new string[0];
                filesInSub = getCsvFileNames(subDi.FullName);
                files.AddRange(filesInSub);
            }

            foreach (FileInfo fi in di.GetFiles())
            {
                if (!fi.Exists)
                {
                    continue;
                }
                if (fi.Extension.ToUpper() != ".CSV")
                {
                    continue;
                }

                files.Add(AisUtil.RemoveExtention(fi.Name));
            }

            return files.ToArray();
        }

        private static void writeByCtmResultSheet(Microsoft.Office.Interop.Excel.Worksheet worksheet, CsvFile csvFile,
          CtmObject originalCtm, DataRow row, BackgroundWorker bw)
        {
            Dictionary<string, string> id2Name = new Dictionary<string, string>();
            foreach (var element in originalCtm.GetAllElements())
            {
                id2Name.Add(element.id.ToString(), element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
            }

            Microsoft.Office.Interop.Excel.Range cells = worksheet.Cells;

            // Header
            int headCount = 1;
            cells[1, 1].Value = Keywords.RECEIVE_TIME;

            // エレメント名
            int headerIndex = 2;
            bool firstColumn = true;
            foreach (var item in row.ItemArray)
            {
                if (firstColumn)
                {
                    firstColumn = false;
                    continue;
                }

                var strVal = item as string;
                if (string.IsNullOrEmpty(strVal))
                {
                    continue;
                }

                cells[1, headerIndex].Value = item;
                headerIndex++;
                headCount++;

            }

            if (csvFile == null)
            {
                return;
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }

            // 中間データ格納領域
            object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headCount];
            
            using (var sr = new StreamReader(csvFile.Filepath, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var headers = reader.FieldHeaders;
                var pos2Pos = new Dictionary<int, int>();

                for (int i = 1; i < headers.Length; i++)
                {
                    var id1 = headers[i];
                    if (!id2Name.ContainsKey(id1))
                    {
                        continue;
                    }
                    var name1 = id2Name[id1];
                    int pos = GetPositionInDataRow(row, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 1); // RT
                    }
                }

                // Loop over rows
                int rowIndex = 2;
                int blockRowIndex = 0;
                do
                {
                    // Cancel Check
                    if (bw.CancellationPending)
                    {
                        return;
                    }

                    var rec = reader.CurrentRecord;
                    long rt = Int64.Parse(rec[0]);
                    values[blockRowIndex, 0] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);

                    for (int j = 1; j < rec.Length; j++)
                    {
                        int columnIndex;
                        if (pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            string item = rec[j];
                            if (item.StartsWith("=="))
                            {
                                item = "'" + item;
                            }

                            values[blockRowIndex, columnIndex] = item;
                        }
                    }

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                        range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, headCount);
                        range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                        // RT Format
                        Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 1];
                        range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;
                    }

                } while (reader.Read());

                if (0 != blockRowIndex)
                {
                    Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                    range = range.get_Resize(blockRowIndex, headCount);
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // RT Format
                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 1];
                    range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                    range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                }
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }
            //ISSUE_NO.728 sunyi 2018/06/25 Start
            //仕様変更、resultシートを表示する
            //worksheet.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
            //ISSUE_NO.728 sunyi 2018/06/25 End
        }

        protected virtual void WriteIntegratedResultSheet(Microsoft.Office.Interop.Excel.Workbook workbook, DataTable dtOutput, List<XYZ> srcFiles, long displayStartCurrentBlock = 0)
        {
            Microsoft.Office.Interop.Excel.Worksheet worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, "result");
            worksheet.Rows.Clear();
            Microsoft.Office.Interop.Excel.Range cells = worksheet.Cells;

            // ヘッダー
            cells[1, 1].Value = Keywords.CTM_NAME;
            cells[1, 2].Value = Keywords.RECEIVE_TIME;
            int headerIdx = 3;
            bool firstColumn = true;
            foreach (DataColumn column in dtOutput.Columns)
            {
                if (firstColumn)
                {
                    firstColumn = false;
                    continue;
                }
                var colName0 = column.ColumnName;
                var colName = colName0.Substring(0, colName0.Length - 3);

                cells[1, headerIdx].Value = colName;
                headerIdx++;
            }
            int headCount = headerIdx;

            //
            // レコードを書く前の準備
            //
            foreach (var csvFile in srcFiles)
            {
                csvFile.Init();
                var reader = csvFile.reader;
                reader.Read(); // Read Header and first row

                var pos2Pos = new Dictionary<int, int>();
                var header = reader.FieldHeaders;
                for (int i = 1; i < header.Length; i++)
                {
                    var id1 = header[i]; 
                    var el = csvFile.ctmObj.GetElement(new GUID(id1));
                    if (el == null)
                    {
                        continue;
                    }
                    var name1 = el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    int pos = AisUtil.GetPositionInDataRowForAllCtms(csvFile.dtatRow, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 2); // RT + CTM NAME
                    }

                }
                csvFile.pos2Pos = pos2Pos;
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }

            //
            // レコード
            //
            using (var merger = new MergeIterators(srcFiles))
            {
                int rowIndex = 2;
                int blockRowIndex = 0;

                // string[,]だと明示的に文字列として書き込んでいるようなので、
                // object[,]に変更して、Excelの方に型を判断させるように変更。
                object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headCount];

                do
                {
                    XYZ currentIterator = merger.Current();
                    var rec = currentIterator.Current();
                    long rt = Int64.Parse(rec[0]);
                    if (rt < displayStartCurrentBlock)
                    {
                        continue;
                    }

                    values[blockRowIndex, 0] = currentIterator.Name();
                    values[blockRowIndex, 1] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);

                    for (int j = 1; j < rec.Length; j++)
                    {
                        int columnIndex;
                        if (currentIterator.pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            values[blockRowIndex, columnIndex] = rec[j];
                        }
                    }

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                        range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, headCount);
                        range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                        // RT Format
                        Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 2];
                        range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;

                        // Cancel Check
                        if (this.bw.CancellationPending)
                        {
                            return;
                        }
                    }
                } while (merger.MoveNext());

                if (0 != blockRowIndex)
                {
                    Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                    range = range.get_Resize(blockRowIndex, headCount);
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // RT Format
                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 2];
                    range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                    range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                }
            }
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(InteropExcelWriter));
    }
}
