﻿using System.Diagnostics;

using System.IO;


using System.Runtime.InteropServices;
using System;
using System.Text;
using System.Windows.Forms;
using System.Threading;
//ISSUE_NO.799 Sunyi 2018/07/25 Start
//処理中にEXCELを触らないようにする(Dac744)
using System.Collections.Generic;
//ISSUE_NO.799 Sunyi 2018/07/25 End
using ExcelApp = Microsoft.Office.Interop.Excel.Application;

using SpreadsheetGear;
using DAC.Model.Util;
using log4net;

namespace DAC.AExcel
{


    public class ExcelUtil
    {
#if false
        public static void StartExcelProcess(string filepath, bool newProcess)
        {
            if (!filepath.StartsWith("\"") &&
                !filepath.EndsWith("\""))
            {
                filepath = "\"" + filepath + "\"";
            }

            var processStartInfo = new ProcessStartInfo();
            if (newProcess)
            {
                processStartInfo.FileName = "Excel.exe";
                processStartInfo.Arguments = "/x " + filepath;
            }
            else
            {
                processStartInfo.FileName = filepath;
            }

            using (Process proc = new Process())
            {
                proc.StartInfo = processStartInfo;
                proc.Start();
            }
        }
#endif

        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        public static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);
        [DllImport("user32.dll", EntryPoint = "GetWindowRect")]
        public static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);
        public struct Rect
        {
            public int Left { get; set; }
            public int Top { get; set; }
            public int Right { get; set; }
            public int Bottom { get; set; }
        }

        private static readonly int SWP_NOSIZE = 0x0001;
        private static readonly int SWP_NOZORDER = 0x0004;
        private static readonly int SWP_SHOWWINDOW = 0x0040;

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern bool BringWindowToTop(IntPtr hWnd);

        private static readonly ILog logger = LogManager.GetLogger(typeof(ExcelUtil));

        public static void MoveExcelOutofScreen(Microsoft.Office.Interop.Excel.Application app)
        {
            int flag = SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW;

            // Get window handle of excel application
            IntPtr intptr = new IntPtr(app.Hwnd);

            // Move to out of screen
            int outx = Screen.PrimaryScreen.Bounds.Width + 1;
            int outy = Screen.PrimaryScreen.Bounds.Height + 1;
            SetWindowPos(intptr, 0, outx, outy, 0, 0, flag);
        }

        public static void RestoreExcelWindow(Microsoft.Office.Interop.Excel.Application app)
        {
            // Get window handle of excel application
            IntPtr intptr = new IntPtr(app.Hwnd);

            // Get window position
            Rect rectWnd = new Rect();
            GetWindowRect(intptr, ref rectWnd);

            // Restore excel window position
            int flag = SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW;
            SetWindowPos(intptr, 0, rectWnd.Left, rectWnd.Top, 0, 0, flag);

            // Bring excel to front
            BringWindowToTop(intptr);
        }

        //ISSUE_NO.799 Sunyi 2018/07/25 Start
        //処理中にEXCELを触らないようにする(Dac744)
        public static Dictionary<IntPtr, Rect> MoveExcelOutofScreen_Status(Microsoft.Office.Interop.Excel.Application app)
        {
            int flag = SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW;

            Dictionary<IntPtr, Rect> ExcelInfo = new Dictionary<IntPtr, Rect>();
            // Get window handle of excel application
            IntPtr intptr_status = new IntPtr(app.Hwnd);
            Rect rectWnd_status = new Rect();

            // Get window position
            GetWindowRect(intptr_status, ref rectWnd_status);

            // Move to out of screen
            int outx = Screen.PrimaryScreen.Bounds.Width + 1;
            int outy = Screen.PrimaryScreen.Bounds.Height + 1;
            SetWindowPos(intptr_status, 0, outx, outy, 0, 0, flag);

            ExcelInfo.Add(intptr_status, rectWnd_status);
            return ExcelInfo;
        }

        public static void RestoreExcelWindow_Status(Dictionary<IntPtr, Rect> ExcelInfo)
        {
            foreach (var info in ExcelInfo)
            {
                // Restore excel window position
                int flag = SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW;
                SetWindowPos(info.Key, 0, info.Value.Left, info.Value.Top, 0, 0, flag);

                // Bring excel to front
                BringWindowToTop(info.Key);
            }
        }
        //ISSUE_NO.799 Sunyi 2018/07/25 End

        public static void OpenExcelFile(string filepath)
        {
            Microsoft.Office.Interop.Excel.Application xl = null;
            Microsoft.Office.Interop.Excel.Workbooks wbs = null;
            Microsoft.Office.Interop.Excel.Workbook wb = null;

            try
            {
                xl = new Microsoft.Office.Interop.Excel.Application();
                xl.Visible = true;

                IntPtr intptr = new IntPtr(xl.Hwnd);

                // Get window position
                Rect rectWnd = new Rect();
                GetWindowRect(intptr, ref rectWnd);

                // Move excel out of screen
                MoveExcelOutofScreen(xl);

                // Open excel file
                wbs = xl.Workbooks;
                wb = wbs.Open(filepath,2);

                //Dac_Home sunyi 20190530 start
                var range = AisUtil.GetSheetFromSheetName_Interop(wb, DAC.Util.Keywords.PARAM_SHEET).get_Range("A1", "J100");
                //IsOutputFile
                var range2 = AisUtil.GetFindRange_Interop(range, "IsOutputFile");
                var range3 = AisUtil.GetFindRange_Interop(range, "IsTemplate");
                if (range2 != null && range3 != null)
                {
                    range2[1, 2].Value = "True";
                    range3[1, 2].Value = "";
                }
                //Dac_Home sunyi 20190530 start

                // 該当ファイルを開いているプロセスをフォアグラウンドに表示する
                //string fileName = Path.GetFileName(filepath);
                //foreach (System.Diagnostics.Process p
                //    in System.Diagnostics.Process.GetProcesses())
                //{
                //    if (!fileName.Equals("") && !p.MainWindowTitle.Equals("") && 0 <= p.MainWindowTitle.IndexOf(fileName))
                //    {
                        // Wait for macro processing
                        System.Threading.Thread.Sleep(3000);

                        // Restore excel window position
                        int flag = SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW;
                        SetWindowPos(intptr, 0, rectWnd.Left, rectWnd.Top, 0, 0, flag);

                        // Bring excel to front
                        //SetForegroundWindow(new IntPtr(xl.Hwnd));
                        BringWindowToTop(intptr);

                //        break;
                //    }
                //}
            }
            finally
            {
                if (wb != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wb);
                    wb = null;
                }

                if (wbs != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wbs);
                    wbs = null;
                }

                if (xl != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xl);
                    xl = null;
                }

            }

        }
        
        //dac_home start
        public static void CreateExcelFile(string filepath)
        {
            Microsoft.Office.Interop.Excel.Application xl = null;
            Microsoft.Office.Interop.Excel.Workbooks wbs = null;
            Microsoft.Office.Interop.Excel.Workbook wb = null;

            try
            {
                xl = new Microsoft.Office.Interop.Excel.Application();
                xl.Visible = true;

                IntPtr intptr = new IntPtr(xl.Hwnd);

                // Get window position
                Rect rectWnd = new Rect();
                GetWindowRect(intptr, ref rectWnd);

                // Move excel out of screen
                MoveExcelOutofScreen(xl);

                // Open excel file
                wbs = xl.Workbooks;
                wb = wbs.Open(filepath);
                string dacId = AisUtil.GetParentFolderNameFromPath(filepath);
                var configParams = new Dictionary<ExcelConfigParam, object>();
                configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(wb.FullName));
                configParams.Add(ExcelConfigParam.ENABLE_MULTI_DAC, true);
                logger.Debug("REGISTERED_FILE: " + configParams[ExcelConfigParam.REGISTERED_FILENAME]);
                InteropExcelWriterDac.InitializeConfigSheetParam(AisUtil.GetSheetFromSheetName_Interop(wb, DAC.Util.Keywords.PARAM_SHEET), configParams, dacId);
                
                // 該当ファイルを開いているプロセスをフォアグラウンドに表示する
                //string fileName = Path.GetFileName(filepath);
                //foreach (System.Diagnostics.Process p
                //    in System.Diagnostics.Process.GetProcesses())
                //{
                //    if (!fileName.Equals("") && !p.MainWindowTitle.Equals("") && 0 <= p.MainWindowTitle.IndexOf(fileName))
                //    {
                        // Wait for macro processing
                        System.Threading.Thread.Sleep(3000);

                        // Restore excel window position
                        int flag = SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW;
                        SetWindowPos(intptr, 0, rectWnd.Left, rectWnd.Top, 0, 0, flag);

                        // Bring excel to front
                        //SetForegroundWindow(new IntPtr(xl.Hwnd));
                        BringWindowToTop(intptr);

                //        break;
                //    }
                //}
            }
            finally
            {
                if (wb != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wb);
                    wb = null;
                }

                if (wbs != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(wbs);
                    wbs = null;
                }

                if (xl != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(xl);
                    xl = null;
                }

            }

        }
        //dac_home end

        public static void OpenExcelFileForDAC(Microsoft.Office.Interop.Excel.Application app)
        {
            IntPtr intptr = new IntPtr(app.Hwnd);

            // Bring excel to front
            BringWindowToTop(intptr);
        }

        public static void OpenExcelFile(Microsoft.Office.Interop.Excel.Application app)
        {
            IntPtr intptr = new IntPtr(app.Hwnd);

            // Bring excel to front
            BringWindowToTop(intptr);
        }

        private const int MAX_BUFF_ROWS = 5000;
        private const string PARAM_SHEET_NAME = "param";
        private const string CSV_SHEET_NAME = "csvSheet";
        private const string REGISTER_CELL_NAME = "登録フォルダ";
        private static readonly int SKIP_SUB_HEADERS = 2;

        public static List<string> SaveSheetsAsCsv(string excelFile, List<string> sheetNames)
        {
            List<string> exported = new List<string>();

            string dirPath = System.IO.Path.GetDirectoryName(excelFile);

            IWorkbookSet workbookSet = SpreadsheetGear.Factory.GetWorkbookSet();
            IWorkbook workbook = workbookSet.Workbooks.Open(excelFile);

            try
            {
                StringBuilder sb = new StringBuilder();
                SpreadsheetGear.IWorksheet csvsheet = GetSheet(workbook, CSV_SHEET_NAME);

                for (int k = 0; k < sheetNames.Count; k++)
                {
                    string sheetName = sheetNames[k];
                    string excelName = System.IO.Path.GetFileNameWithoutExtension(excelFile);
                    string csvName = excelName + "_" + sheetName + ".csv";
                    string csvPath = System.IO.Path.Combine(dirPath, csvName);
                    StreamWriter sw = new StreamWriter(csvPath, false, Encoding.GetEncoding(932));

                    SpreadsheetGear.IWorksheet worksheet = workbook.Worksheets[sheetName];

                    for (int i = 0; i < worksheet.UsedRange.RowCount; i++)
                    {
                        for (int j = 0; j < worksheet.UsedRange.ColumnCount; j++)
                        {
                            //sb.Append(worksheet.UsedRange[i, j].Value == null ? "" : worksheet.UsedRange[i, j].Value.ToString());
                            string value = null;
                            if (worksheet.UsedRange[i, j].ValueType.Equals(SpreadsheetGear.ValueType.Text))
                            {
                                value = worksheet.UsedRange[i, j].Entry;
                                if (!string.IsNullOrEmpty(value))
                                {
                                    value = value.Replace("\"", "\"\"");
                                }
                            }
                            else if (worksheet.UsedRange[i, j].ValueType.Equals(SpreadsheetGear.ValueType.Number))
                            {
                                if (worksheet.UsedRange[i, j].NumberFormatDefined && SpreadsheetGear.NumberFormatType.DateTime.Equals(worksheet.UsedRange[i, j].NumberFormatType))
                                {
                                    value = (DateTime.Parse(worksheet.UsedRange[i, j].Text)).ToString("yyyy/M/d H:m");
                                }
                                else
                                {
                                    value = worksheet.UsedRange[i, j].Text;
                                }
                            }
                            else
                            {
                                value = worksheet.UsedRange[i, j].Value == null ? "" : worksheet.UsedRange[i, j].Value.ToString();
                            }
                            value = string.IsNullOrEmpty(value) ? "\"\"" : "\"" + value + "\"";
                            sb.Append(value);
                            sb.Append(",");
                        }
                        sb.Append("\r\n");

                        if (i == 0 && csvsheet != null && SKIP_SUB_HEADERS > 0)
                        {
                            IRange range = csvsheet.UsedRange.Find(worksheet.Name, null,
                                SpreadsheetGear.FindLookIn.Values,
                                SpreadsheetGear.LookAt.Whole,
                                SpreadsheetGear.SearchOrder.ByRows,
                                SpreadsheetGear.SearchDirection.Next,
                                true);
                            if (range != null)
                            {
                                for (int l = 0; l < SKIP_SUB_HEADERS; l++)
                                {
                                    for (int j = 0; j < worksheet.UsedRange.ColumnCount; j++)
                                    {
                                        IRange csvRange = csvsheet.Cells[range.Row + l + 1, j];
                                        string value = csvRange.Value == null ? "" : csvRange.Value.ToString();
                                        value = string.IsNullOrEmpty(value) ? "\"\"" : "\"" + value + "\"";
                                        sb.Append(value);
                                        sb.Append(",");
                                    }
                                    sb.Append("\r\n");
                                }
                            }
                        }

                        if (((i + 1) % MAX_BUFF_ROWS) == 0)
                        {
                            sw.Write(sb);
                            sb.Clear();
                        }
                    }

                    if (sb.Length > 0)
                    {
                        sw.Write(sb);
                        sb.Clear();
                    }
                    sw.Close();
                    exported.Add(csvPath);
                }
            }
            finally
            {
                workbook.Close();
            }

            return exported;
        }

        public static void WriteQuickGraphParam(string excelFile, string aisPath)
        {
            IWorkbookSet workbookSet = SpreadsheetGear.Factory.GetWorkbookSet();
            IWorkbook workbook = workbookSet.Workbooks.Open(excelFile);
            SpreadsheetGear.IWorksheet worksheet = workbook.Worksheets[PARAM_SHEET_NAME];

            try
            {
                if (worksheet != null)
                {
                    IRange registerRange = worksheet.UsedRange.Find(REGISTER_CELL_NAME,
                        null,
                        SpreadsheetGear.FindLookIn.Values,
                        SpreadsheetGear.LookAt.Whole,
                        SpreadsheetGear.SearchOrder.ByRows,
                        SpreadsheetGear.SearchDirection.Next,
                        true);

                    if (registerRange != null)
                    {
                        registerRange.Offset(0, 1).Value = aisPath;
                    }

                    workbook.Save();
                }
            }
            finally
            {
                workbook.Close();
            }
        }

        private static SpreadsheetGear.IWorksheet GetSheet(IWorkbook workbook, string sheetName)
        {
            Debug.Assert(!string.IsNullOrEmpty(sheetName));

            SpreadsheetGear.IWorksheet sheet = null;
            try
            {
                sheet = workbook.Worksheets[sheetName];
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }

            return sheet;
        }

        private static SpreadsheetGear.IWorksheet CreateSheet(IWorkbook workbook, string sheetName)
        {
            Debug.Assert(!string.IsNullOrEmpty(sheetName));

            SpreadsheetGear.IWorksheet sheet = GetSheet(workbook, sheetName);

            try
            {
                if (sheet == null)
                {
                    sheet = workbook.Worksheets.Add();
                    sheet.Name = sheetName;
                }
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }

            return sheet;
        }
       
        // 2019/05/30 dongning downloadため add Start
        private static int PROCESS_COUNT = 0;
        private static readonly int MAX_ARRANGE_PROCESSES = 10;
        private static readonly int OFFSET_POSITION = 32;
        public static void StartExcelProcess(string filepath, bool newProcess)
        {
            int flag = SWP_NOSIZE | SWP_NOZORDER | SWP_SHOWWINDOW;
            PROCESS_COUNT = PROCESS_COUNT % MAX_ARRANGE_PROCESSES;
            int offset = PROCESS_COUNT * OFFSET_POSITION;
            PROCESS_COUNT++;

            // Create excel application instance
            ExcelApp app = new ExcelApp();
            IntPtr intptr = new IntPtr(app.Hwnd);

            /*
            // Get window position
            Rect rectWnd = new Rect();
            GetWindowRect(intptr, ref rectWnd);
            */

            // Move to out of screen
            int outx = Screen.PrimaryScreen.Bounds.Width + 1;
            int outy = Screen.PrimaryScreen.Bounds.Height + 1;
            SetWindowPos(intptr, 0, outx, outy, 0, 0, flag);

            // Show excel application
            app.Visible = true;

            // Open excel file
            app.Workbooks.Open(filepath);

            // Reset window position
            //SetWindowPos(intptr, 0, rectWnd.Left, rectWnd.Top, 0, 0, flag);
            SetWindowPos(intptr, 0, offset, offset, 0, 0, flag);

            // Show window on top
            //app.ActiveWindow.Activate();
            //BringWindowToTop(intptr);
            SetForegroundWindow(intptr);
        }
        // 2019/05/30 dongning downloadため add End

        public static string GetToolTipDacFileName(string str)
        {
            string[] name = str.Split('_');
            string tooltipName = "";
            if (name.Length > 1)
            {
                for (int i = 1; i < name.Length; i++)
                {
                    if (!string.IsNullOrEmpty(tooltipName))
                    {
                        tooltipName = tooltipName + "_" + name[i].ToString();
                    }
                    else
                    {
                        tooltipName = name[i].ToString();
                    }
                }
            }
            else
            {
                tooltipName = str;
            }
            return tooltipName;
        }
        
    }
}
