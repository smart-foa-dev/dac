﻿using DAC.AExcel.Grip;
using DAC.CtmData;
using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using CsvHelper;
using FoaCore.Common;
using FoaCore.Common.Util;
using SpreadsheetGear;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.AExcel
{
    class SSGExcelWriterBulkyContinuous : SSGExcelWriterBulky
    {
        //ISSUE_NO.645 sunyi 2018/05/24 start
        //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
        //private const string IS_ALL_DATA = "IsAllData";
        //ISSUE_NO.645 sunyi 2018/05/24 end
        public SSGExcelWriterBulkyContinuous(ControlMode mode, DataSourceType ds, CtmObject ctm, BackgroundWorker bw, Mission mi, List<int> rows, List<int> cols)
            : base(mode, ds, ctm, bw, mi, rows, cols)
        { }

        public void WriteCtmData2(string filePathDest, string srcFile, Dictionary<ExcelConfigParam, object> configParams, string ctmId, string srcFileCtm)
        {
            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                // ワークブック作成
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePathDest);

                // シート作成
                SpreadsheetGear.IWorksheet worksheetCondition = AisUtil.GetSheetFromSheetName_SSG(workbook, GetSummarySheetName());

                WriteSummarySheet(worksheetCondition, this.dtOutput, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // paramシート作成
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(workbook, paramSheetName);
                WriteConfigSheet(worksheetParam, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = Keywords.RO_PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_SSG(workbook, roParamSheetName);
                WriteRoParamSheet_SSG(worksheetRoParam);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // Ctm結果
                WriteResultSheets(workbook, srcFileCtm, ctmId);

                string dataSheetName = "Bulkyソート結果";
                SpreadsheetGear.IWorksheet worksheetData = AisUtil.GetSheetFromSheetName_SSG(workbook, dataSheetName);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                var direction = GetStringVal(configParams, ExcelConfigParam.B_CONNECTION, "横");
                if (direction == "横")
                {
                    WriteBulkySheetHorizontal(worksheetData, srcFile);
                }
                else
                {
                    //ISSUE_NO.645 sunyi 2018/05/24 start
                    //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                    //WriteBulkySheetVertical(worksheetData, srcFile, IS_ALL_DATA);
                    WriteBulkySheetVertical(worksheetData, srcFile);
                    //ISSUE_NO.645 sunyi 2018/05/24 end
                }
                worksheetData.Select();
                worksheetData.Cells[0, 0].Select();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 保存
                workbook.Save();
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close();
                }
            }
        }

        public void WriteCtmData2(string filePathDest, Dictionary<ExcelConfigParam, object> configParams, string maxCtmId, string srcFileMaxCtm)
        {
            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                // ワークブック作成
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePathDest);

                // シート作成
                SpreadsheetGear.IWorksheet worksheetCondition = AisUtil.GetSheetFromSheetName_SSG(workbook, GetSummarySheetName());

                WriteSummarySheet(worksheetCondition, this.dtOutput, configParams);

                // MaxCtm結果
                if (!string.IsNullOrEmpty(srcFileMaxCtm) && !string.IsNullOrEmpty(maxCtmId))
                {
                    WriteResultSheets(workbook, srcFileMaxCtm, maxCtmId);
                }
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 保存
                workbook.Save();
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close();
                }
            }
        }

        public void WriteCtmDataStream(string filePathDest, string srcFile, Dictionary<ExcelConfigParam, object> configParams, string ctmId, string srcFileCtm)
        {
            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                // ワークブック作成
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePathDest);

                // シート作成
                SpreadsheetGear.IWorksheet worksheetCondition = AisUtil.GetSheetFromSheetName_SSG(workbook, GetSummarySheetName());

                WriteSummarySheet(worksheetCondition, this.dtOutput, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // paramシート作成
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(workbook, paramSheetName);
                WriteConfigSheet(worksheetParam, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = Keywords.RO_PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_SSG(workbook, roParamSheetName);
                WriteRoParamSheet_SSG(worksheetRoParam);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // Ctm結果
                WriteResultSheets(workbook, srcFileCtm, ctmId);

                string dataSheetName = "Bulkyソート結果";
                SpreadsheetGear.IWorksheet worksheetData = AisUtil.GetSheetFromSheetName_SSG(workbook, dataSheetName);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                var direction = GetStringVal(configParams, ExcelConfigParam.B_CONNECTION, "横");
                if (direction == "横")
                {
                    WriteBulkySheetHorizontal(worksheetData, srcFile);
                }
                else
                {
                    //ISSUE_NO.645 sunyi 2018/05/24 start
                    //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                    //WriteBulkySheetVerticalStream(worksheetData, srcFile, IS_ALL_DATA);
                    WriteBulkySheetVerticalStream(worksheetData, srcFile);
                    //ISSUE_NO.645 sunyi 2018/05/24 end
                }
                worksheetData.Select();
                worksheetData.Cells[0, 0].Select();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 保存
                workbook.Save();
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close();
                }
            }
        }

        private void WriteResultSheets(SpreadsheetGear.IWorkbook workbook, string srcFilepath, string ctmIdx)
        {
            foreach (var ctm in this.Ctms)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                var ctmId = ctm.id.ToString();
                if (ctmId != ctmIdx)
                {
                    continue;
                }

                var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);


                // シート検索 / 作成
                SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, ctmName);
                worksheet.Cells.Clear();
                //ISSUE_NO.728 sunyi 2018/06/25 Start
                //仕様変更、resultシートを表示する
                //worksheet.Visible = SheetVisibility.Hidden;
                //ISSUE_NO.728 sunyi 2018/06/25 End

                // var srcFilepath = Path.Combine(folderPath, ctmId + ".csv");

                CsvFile csvFileObj = null;
                if (File.Exists(srcFilepath))
                {
                    csvFileObj = new CsvFile() { Id = new FoaCore.Suid(ctmId), Name = ctmName, Filepath = srcFilepath };
                }
                else
                {
                    return;
                }

                var elemes = ctm.GetAllElements();
                string[,] rowS = new string[elemes.Count, 3];
                int i = 0;

                //ISSUE_NO.798 sunyi 2018/07/25 Start
                //選択されたエレメントを出力する
                using (var sr = new StreamReader(csvFileObj.Filepath, Encoding.UTF8))
                using (var reader = new CsvReader(sr))
                {
                    reader.Read();
                    var headers = reader.FieldHeaders;
                    //ISSUE_NO.798 sunyi 2018/07/25 End
                    foreach (var e in elemes)
                    {
                        // Cancel Check
                        if (this.bw.CancellationPending)
                        {
                            return;
                        }
                        //ISSUE_NO.798 sunyi 2018/07/25 Start
                        //選択されたエレメントを出力する
                        foreach (var header in headers)
                        {
                            if (header.ToString().Equals(e.id.ToString()))
                            {
                                //ISSUE_NO.798 sunyi 2018/07/25 End
                                rowS[i, 0] = e.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                                rowS[i, 1] = FoaDatatype.Val2Name(e.datatype);
                                rowS[i, 2] = e.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                                i++;
                                //ISSUE_NO.798 sunyi 2018/07/25 Start
                                //選択されたエレメントを出力する
                                break;
                                //ISSUE_NO.798 sunyi 2018/07/25 End
                            }
                        }
                    }
                }

                ExcelWriterYotobetsu.WriteByCtmResultSheet(worksheet, ctm, rowS, this.bw, csvFileObj);

                // 次のシートへ
                //worksheet.UsedRange.Columns.AutoFit();
                //worksheet.Cells["A:A"].ColumnWidth = 14.40;
                //AISTEMP-125 sunyi 20190329
                //SSG幅調整
                //worksheet.UsedRange.Columns.AutoFit();
                worksheet.UsedRange.ColumnWidth = 16.00;

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
        }
    }
}
