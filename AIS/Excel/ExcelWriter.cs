﻿using DAC.Model;
using DAC.View;
using FoaCore.Common;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Documents;

namespace DAC.AExcel
{
    abstract class ExcelWriter
    {
        protected static readonly int EXCEL_MAX_COLS = 16384;
        protected ControlMode Mode;
        protected List<CtmObject> Ctms;
        protected DataTable dtOutput;
        protected BackgroundWorker bw;
        protected DataSourceType dataSource;
        protected string ONLINEID = "オンライングラフID";
        protected string WORKPLACEID = "WorkPlaceID";

        public ExcelWriter(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw)
        {
            this.Mode = mode;
            this.dataSource = ds;
            this.Ctms = ctms;
            this.dtOutput = dt;
            this.bw = bw;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filePathDest">ターゲットとなるExcelファイルのパス</param>
        /// <param name="folderPath">入力元となるCSVファイルがあるフォルダのパス</param>
        /// <param name="configParams">設定値の一覧</param>
        /// <param name="displayStartCurrentBlock">(オンライン繰り返し移動でのみ使用) resultシートに表示する結果の開始時刻</param>
        public abstract void WriteCtmData(string filePathDest, string folderPath, Dictionary<ExcelConfigParam, object> configParams);

        protected DataTable addIdToDataTable(DataTable dt)
        {
            DataTable dtNew = dt.Clone();
            dtNew.Rows.Clear();
            foreach (var rowObject in dt.Rows)
            {
                DataRow row = rowObject as DataRow;
                List<string> nameList = new List<string>();
                List<string> idList = new List<string>();

                bool addedCtmName = false;
                foreach (var item in row.ItemArray)
                {
                    nameList.Add(item.ToString());
                    if (!addedCtmName)
                    {
                        string ctmId = GetCtmIdFromName(item.ToString());
                        idList.Add(ctmId);
                        addedCtmName = true;
                        continue;
                    }

                    if (string.IsNullOrEmpty(item.ToString()))
                    {
                        idList.Add(string.Empty);
                        continue;
                    }
                    string elementId = GetElementIdFromName(row[0].ToString(), item.ToString());
                    idList.Add(elementId);
                }

                DataRow nameRow = dtNew.NewRow();
                nameRow.ItemArray = nameList.ToArray();
                DataRow idRow = dtNew.NewRow();
                idRow.ItemArray = idList.ToArray();
                dtNew.Rows.Add(nameRow);
                dtNew.Rows.Add(idRow);
            }

            return dtNew;
        }

        protected string GetCtmIdFromName(string ctmName)
        {
            foreach (var ctm in this.Ctms)
            {
                if (ctmName != ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true))
                {
                    continue;
                }

                return ctm.id.ToString();
            }

            return string.Empty;
        }

        protected string GetElementIdFromName(string ctmName, string elementName)
        {
            foreach (var ctm in this.Ctms)
            {
                if (ctmName != ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true))
                {
                    continue;
                }

                foreach (var element in ctm.GetAllElements())
                {
                    if (elementName != element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true))
                    {
                        continue;
                    }

                    return element.id.ToString();
                }

                break;
            }

            return string.Empty;
        }

        protected string ElementId2Name(string ctmName, string elementId)
        {
            foreach (var ctm in this.Ctms)
            {
                if (ctmName != ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true))
                {
                    continue;
                }

                foreach (var element in ctm.GetAllElements())
                {
                    if (element.id.Equals(elementId))
                    {
                        continue;
                    }

                    return element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                }

                break;
            }

            return null;
        }

        protected static int GetPositionInDataRow(DataRow row, string elementName)
        {
            int pos = 0;
            var array = row.ItemArray;
            int idx = 1;

            for (int i = idx; i < array.Length; i++)
            {
                var val = array[i] as string;
                if (string.IsNullOrEmpty(val))
                {
                    continue;
                }

                if (val == elementName)
                {
                    return pos;  // 最初のカラムはCTM名
                }
                pos++;
            }

            return -1;
        }

        protected static int GetPositionInDataRowS(string[] row, string elementName)
        {
            int pos = 0;
            int idx = 1;

            for (int i = idx; i < row.Length; i++)
            {
                var val = row[i];
                if (string.IsNullOrEmpty(val))
                {
                    continue;
                }

                if (val == elementName)
                {
                    return pos;  // 最初のカラムはCTM名
                }
                pos++;
            }

            return -1;
        }

        protected static int GetPositionInDataRowS(string[,] row, string elementName)
        {
            int pos = 0;
            int idx = 0;

            for (int i = idx; i < row.GetLength(0); i++)
            {
                var val = row[i, 0];
                if (string.IsNullOrEmpty(val))
                {
                    continue;
                }

                if (val == elementName)
                {
                    return pos;  // 最初のカラムはCTM名
                }
                pos++;
            }

            return -1;
        }


        protected static string GetStringVal(Dictionary<ExcelConfigParam, object> tab, ExcelConfigParam key, string defaultVal)
        {
            object fileName0;
            string fileName = null;
            if (tab.TryGetValue(key, out fileName0))
            {
                fileName = fileName0 as string;
                if (fileName == null)
                {
                    fileName = defaultVal;
                }
                return fileName;
            }
            else
            {
                return defaultVal;
            }
        }

        protected static int GetIntVal(Dictionary<ExcelConfigParam, object> tab, ExcelConfigParam key, int defaultVal)
        {
            object fileName0;
            int value;
            if (tab.TryGetValue(key, out fileName0))
            {
                try
                {
                    value = (int)fileName0;
                    return value;
                }
                catch (InvalidCastException ice)
                {
                    logger.Error("Invalid type: expected: int, actual: " + fileName0.GetType(), ice);
                    return defaultVal;
                }
            }
            else
            {
                return defaultVal;
            }
        }

        protected static float GetFloatVal(Dictionary<ExcelConfigParam, object> tab, ExcelConfigParam key, float defaultVal)
        {
            object fileName0;
            float value;
            if (tab.TryGetValue(key, out fileName0))
            {
                try
                {
                    value = (float)fileName0;
                    return value;
                }
                catch (InvalidCastException ice)
                {
                    logger.Error("Invalid type: expected: float, actual: " + fileName0.GetType(), ice);
                    return defaultVal;
                }
            }
            else
            {
                return defaultVal;
            }
        }

        protected static bool GetBoolVal(Dictionary<ExcelConfigParam, object> tab, ExcelConfigParam key, bool defaultVal)
        {
            object fileName0;
            bool value;
            if (tab.TryGetValue(key, out fileName0))
            {
                try
                {
                    value = (bool)fileName0;
                    return value;
                }
                catch (InvalidCastException ice)
                {
                    logger.Error("Invalid type: expected: int, actual: " + fileName0.GetType(), ice);
                    return defaultVal;
                }
            }
            else
            {
                return defaultVal;
            }
        }

        protected DateTime GetDateTimeVal(Dictionary<ExcelConfigParam, object> tab, ExcelConfigParam key, DateTime defaultVal)
        {
            object fileName0;
            if (tab.TryGetValue(key, out fileName0))
            {
                try
                {
                    DateTime fileName = (DateTime)fileName0;
                    return fileName;
                }
                catch (InvalidCastException ice)
                {
                    logger.Error("Invalid type: expected: DateTime, actual: " + fileName0.GetType(), ice);
                    return defaultVal;
                }
            }
            else
            {
                return defaultVal;
            }
        }

        protected string GetSummarySheetName()
        {
            switch (this.Mode)
            {
                case ControlMode.Spreadsheet:
                    return "集計テンプレート";
                case ControlMode.Online:
                    return "オンラインテンプレート";
                case ControlMode.Bulky:
                    return "Bulkyテンプレート";
                default:
                    return "Summary";
            }
        }

        protected string[] DataRowToArray(DataRow dataRow)
        {
            var itemArray = dataRow.ItemArray;
            var rowS = new string[itemArray.Length];
            for (int i = 0; i < itemArray.Length; i++)
            {
                rowS[i] = itemArray[i] as string;
            }

            return rowS;
        }

        protected string[] DataRowToArray2(DataRow dataRow)
        {
            var itemArray = dataRow.ItemArray;
            List<string> rowS = new List<string>();
            for (int i = 0; i < itemArray.Length; i++)
            {
                if (itemArray[i] as string == null)
                {
                    continue;
                }

                rowS.Add(itemArray[i] as string);
            }

            return rowS.ToArray();
        }

        protected void WriteRoParamSheet_SSG(SpreadsheetGear.IWorksheet worksheetRoParam)
        {
            SpreadsheetGear.IRange cellsParam = worksheetRoParam.Cells;

            for (int i = 0; i < 10; i++)   // とりあえず10行ほど検索
            {
                // null check
                if (cellsParam[i, 0].Value == null)
                {
                    continue;
                }

                // TimezoneId
                if (cellsParam[i, 0].Value.ToString() == "TimezoneId")
                {
                    cellsParam[i, 1].Value = LoginInfo.GetInstance().GetTimezoneId();
                    continue;
                }

                // オンライングラフID
                if (cellsParam[i, 0].Value.ToString() == ONLINEID)
                {
                    //追加オンラインID
                    string OnlineId = new FoaCore.Common.Util.GUID().ToString();
                    cellsParam[i, 1].Value = OnlineId;
                    continue;
                }
            }

            worksheetRoParam.Visible = SpreadsheetGear.SheetVisibility.Hidden;
        }

        protected virtual void WriteRoParamSheet_Interop(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            for (int i = 1; i <= 10; i++)   // とりあえず10行ほど検索
            {
                // null check
                if (cellsParam[i, 1].Value == null)
                {
                    continue;
                }

                // TimezoneId
                if (cellsParam[i, 1].Value.ToString() == "TimezoneId")
                {
                    cellsParam[i, 2].Value = LoginInfo.GetInstance().GetTimezoneId();
                    continue;
                }

                // オンライングラフID
                if (cellsParam[i, 1].Value.ToString() == ONLINEID)
                {
                    //追加オンラインID
                    string OnlineId = new FoaCore.Common.Util.GUID().ToString();
                    cellsParam[i, 2].Value = OnlineId;
                    continue;
                }

                // Grip-Server IP
                if (cellsParam[i, 1].Value.ToString() == "Grip-Server IP")
                {
                    cellsParam[i, 2].Value = AisConf.Config.GripServerHost;
                    continue;
                }

                // Grip-Server Port
                if (cellsParam[i, 1].Value.ToString() == "Grip-Server PORT")
                {
                    cellsParam[i, 2].Value = AisConf.Config.GripServerPort;
                    continue;
                }

                // Grip-R2 Folder
                if (cellsParam[i, 1].Value.ToString() == "Grip-R2 Folder")
                {
                    cellsParam[i, 2].Value = AisConf.Config.GripR2FolderPath;
                    continue;
                }
            }

            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        protected virtual void MultiWriteRoParamSheet_Interop(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, string onlineId, string workplaceId)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            for (int i = 1; i <= 10; i++)   // とりあえず10行ほど検索
            {
                if (cellsParam[i, 1].Value == null)
                {
                    continue;
                }
                if (cellsParam[i, 1].Value.ToString() == "TimezoneId")
                {
                    cellsParam[i, 2].Value = LoginInfo.GetInstance().GetTimezoneId();
                    continue;
                }
                if (cellsParam[i, 1].Value.ToString() == ONLINEID)
                {
                    cellsParam[i, 2].Value = onlineId;
                    continue;
                }
                if (cellsParam[i, 1].Value.ToString() == WORKPLACEID)
                {
                    cellsParam[i, 2].Value = workplaceId;
                    continue;
                }
                if (cellsParam[i, 1].Value.ToString() == "Grip-Server IP")
                {
                    cellsParam[i, 2].Value = AisConf.Config.GripServerHost;
                    continue;
                }
                if (cellsParam[i, 1].Value.ToString() == "Grip-Server PORT")
                {
                    cellsParam[i, 2].Value = AisConf.Config.GripServerPort;
                    continue;
                }
                if (cellsParam[i, 1].Value.ToString() == "Grip-R2 Folder")
                {
                    cellsParam[i, 2].Value = AisConf.Config.GripR2FolderPath;
                    continue;
                }
            }
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }
        //protected virtual void MultiWriteRoParamSheet_Interop(Microsoft.Office.Interop.Excel.Worksheet worksheetParam,string onlineId,string workplaceId)
        //{
        //    Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

        //    for (int i = 1; i <= 10; i++)   // とりあえず10行ほど検索
        //    {
        //        if (cellsParam[i, 1].Value == null)
        //        {
        //            continue;
        //        }
        //        if (cellsParam[i, 1].Value.ToString() == "TimezoneId")
        //        {
        //            cellsParam[i, 2].Value = LoginInfo.GetInstance().GetTimezoneId();
        //            continue;
        //        }
        //        if (cellsParam[i, 1].Value.ToString() == ONLINEID)
        //        {
        //            cellsParam[i, 2].Value = onlineId;
        //            continue;
        //        }
        //        if (cellsParam[i, 1].Value.ToString() == WORKPLACEID)
        //        {
        //            cellsParam[i, 2].Value = workplaceId;
        //            continue;
        //        }
        //        if (cellsParam[i, 1].Value.ToString() == "Grip-Server IP")
        //        {
        //            cellsParam[i, 2].Value = AisConf.Config.GripServerHost;
        //            continue;
        //        }
        //        if (cellsParam[i, 1].Value.ToString() == "Grip-Server PORT")
        //        {
        //            cellsParam[i, 2].Value = AisConf.Config.GripServerPort;
        //            continue;
        //        }
        //        if (cellsParam[i, 1].Value.ToString() == "Grip-R2 Folder")
        //        {
        //            cellsParam[i, 2].Value = AisConf.Config.GripR2FolderPath;
        //            continue;
        //        }
        //    }
        //    worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        //}
        private static readonly ILog logger = LogManager.GetLogger(typeof(ExcelWriter));
    }
}
