﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.AExcel
{
    enum ExcelConfigParam
    {
        START,
        END,
        REGISTERED_FILENAME,
        ONLINE_KIND,
        RELOAD_INTERVAL,
        DISPLAY_PERIOD,
        GET_PERIOD,
        DISPLAY_START,
        DISPLAY_END,
        CTM_ID,
        ELEMENT_ID,
        CTM_NAME,
        B_CONNECTION,
        B_SELECTED_ROWS,
        B_SELECTED_COLS,
        SHURYO_ELEM,
        KAISHI_ELEM,
        KEKKA_ELEM,
        SHUTOKU_KAISHI,
        SHUTOKU_SHURYO,
        ONLINE_FLAG,
        TEMPLATE_NAME,
        MISSION_NAME,
        MISSION_ID,
        OPERATION,
        DISPLAY_START_CURRENT_TIME,
        EDIT_MODE,
        USE_PROXY,
        PROXY_URI,
        MAX_CTM_NAME,
        MAX_CTM_ID,
        ENABLE_MULTI_DAC
    }
}
