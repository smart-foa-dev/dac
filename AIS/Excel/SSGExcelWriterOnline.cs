﻿using DAC.Model;
using DAC.Model.Util;
using DAC.View;
using SpreadsheetGear;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;

namespace DAC.AExcel
{
    class SSGExcelWriterOnline : SSGExcelWriter
    {
        public SSGExcelWriterOnline(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi = null)
            : base(mode, ds, ctms, dt, bw, mi)
        {

        }

        protected override void WriteSummarySheet(IWorksheet worksheetCondition, DataTable dt, Dictionary<ExcelConfigParam, object> configParams)
        {
            string missionName = GetStringVal(configParams, ExcelConfigParam.MISSION_NAME, null);
            string missionId = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, null);

            string onlineKind = GetStringVal(configParams, ExcelConfigParam.ONLINE_KIND, string.Empty);
            int reloadInterval = GetIntVal(configParams, ExcelConfigParam.RELOAD_INTERVAL, 60);
            int displayPeriod = GetIntVal(configParams, ExcelConfigParam.DISPLAY_PERIOD, 1);
            int getPeriod = GetIntVal(configParams, ExcelConfigParam.GET_PERIOD, 0);

            // 収集条件を出力
            SpreadsheetGear.IRange cellsCondition = worksheetCondition.Cells;

            for (int i = 0; i < 10; i++)   // とりあえず10×10のセルを検索
            {
                for (int j = 0; j < 10; j++)
                {
                    // null check
                    if (cellsCondition[i, j].Value == null)
                    {
                        continue;
                    }

                    // ミッション
                    if (cellsCondition[i, j].Value.ToString() == "ミッション")
                    {
                        cellsCondition[i, j + 1].Value = missionName;
                        continue;
                    }

                    // ミッションID
                    if (cellsCondition[i, j].Value.ToString() == "ミッションID")
                    {
                        cellsCondition[i, j + 1].Value = missionId;
                        continue;
                    }

                    // 収集開始日時
                    if (cellsCondition[i, j].Value.ToString() == "収集開始日時")
                    {
                        cellsCondition[i, j].Font.Bold = true;
                        cellsCondition[i, j].Font.Color = SpreadsheetGear.Colors.White;

                        DateTime start = GetDateTimeVal(configParams, ExcelConfigParam.START, DateTime.MinValue);
                        if (start == DateTime.MinValue)
                        {
                            throw new InvalidDataException("start is not specified.");
                        }
                        cellsCondition[i, j + 1].Value = start.ToString("yyyy/MM/dd");
                        cellsCondition[i, j + 2].Value = start.ToString("HH:mm:ss");
                        continue;
                    }

                    // 収集終了日時
                    if (cellsCondition[i, j].Value.ToString() == "収集終了日時")
                    {
                        DateTime end = GetDateTimeVal(configParams, ExcelConfigParam.END, DateTime.MinValue);
                        if (end == DateTime.MinValue)
                        {
                            throw new InvalidDataException("end is not specified.");
                        }
                        cellsCondition[i, j + 1].Value = end.ToString("yyyy/MM/dd");
                        cellsCondition[i, j + 2].Value = end.ToString("HH:mm:ss");
                        continue;
                    }

                    // オンライン種別
                    if (cellsCondition[i, j].Value.ToString() == "オンライン種別")
                    {
                        cellsCondition[i, j + 1].Value = onlineKind;
                        continue;
                    }

                    // 更新周期
                    if (cellsCondition[i, j].Value.ToString() == "更新周期")
                    {
                        
                        cellsCondition[i, j + 1].Value = reloadInterval;
                        continue;
                    }

                    // 表示期間
                    if (cellsCondition[i, j].Value.ToString() == "表示期間")
                    {
                        cellsCondition[i, j + 1].Value = displayPeriod;
                        if (onlineKind == "毎日定刻繰り返し")
                        {
                            cellsCondition[i, j + 2].Value = "【秒】";
                        }
                        else
                        {
                            cellsCondition[i, j + 2].Value = "【時】";
                        }
                        continue;
                    }

                    // 取得期間
                    if (cellsCondition[i, j].Value.ToString() == "取得期間")
                    {
                       
                        if (getPeriod == 0)
                        {
                            continue;
                        }

                        cellsCondition[i, j].Font.Bold = true;
                        cellsCondition[i, j].Font.Color = SpreadsheetGear.Colors.White;
                        cellsCondition[i, j + 1].Value = getPeriod;
                        cellsCondition[i, j + 2].Value = "【時間】";
                        continue;
                    }
                }
            }

            AisUtil.DeleteUnitRow(dt);
            DataTable dtWithId = addIdToDataTable(dt);
            int dtRow = dtWithId.Rows.Count;
            int dtColumn = dtWithId.Columns.Count;

            SpreadsheetGear.IRange elementRangeFirstCell = worksheetCondition.Cells["A7"];
            elementRangeFirstCell.CopyFromDataTable(dtWithId, SpreadsheetGear.Data.SetDataFlags.None);
            cellsCondition[6, 0, 6, dtColumn - 1].Interior.Color = SpreadsheetGear.Colors.CornflowerBlue;
            cellsCondition[6, 0, 6, dtColumn - 1].Font.Color = SpreadsheetGear.Colors.White;
        }

        protected override void WriteConfigSheet(IWorksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            DateTime displayStart = GetDateTimeVal(configParams, ExcelConfigParam.DISPLAY_START, DateTime.MinValue);
            string missionId = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, null);

            SpreadsheetGear.IRange cellsParam = worksheetParam.Cells;

            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                // null check
                if (cellsParam[i, 0].Value == null)
                {
                    continue;
                }

                // 登録フォルダ
                if (cellsParam[i, 0].Value.ToString() == "登録フォルダ")
                {
                    cellsParam[i, 1].Value = AisConf.RegistryFolderPath;
                    continue;
                }

                // 登録ファイル
                if (cellsParam[i, 0].Value.ToString() == "登録ファイル")
                {
                    cellsParam[i, 1].Value = string.Empty;
                    continue;
                }

                // サーバIPアドレス
                if (cellsParam[i, 0].Value.ToString() == "サーバIPアドレス")
                {
                    cellsParam[i, 1].Value = AisConf.Config.CmsHost;
                    continue;
                }

                // サーバポート番号
                if (cellsParam[i, 0].Value.ToString() == "サーバポート番号")
                {
                    cellsParam[i, 1].Value = AisConf.Config.CmsPort;
                    continue;
                }

                // GRIPサーバIPアドレス
                if (cellsParam[i, 0].Value.ToString() == "GRIPサーバIPアドレス")
                {
                    cellsParam[i, 1].Value = AisConf.Config.GripServerHost;
                    continue;
                }

                // GRIPサーバポート番号
                if (cellsParam[i, 0].Value.ToString() == "GRIPサーバポート番号")
                {
                    cellsParam[i, 1].Value = AisConf.Config.GripServerPort;
                    continue;
                }

                // テンプレート名称
                if (cellsParam[i, 0].Value.ToString() == "テンプレート名称")
                {
                    cellsParam[i, 1].Value = "オンライン";
                    continue;
                }

                // 自動更新
                if (cellsParam[i, 0].Value.ToString() == "自動更新")
                {
                    cellsParam[i, 1].Value = "OFF";
                    continue;
                }

                // 表示開始時刻
                if (cellsParam[i, 0].Value.ToString() == "表示開始時刻")
                {
                    cellsParam[i, 1].Value = displayStart.ToString("yyyy/MM/dd HH:mm:ss");
                    continue;
                }

                // ミッションID
                if (cellsParam[i, 0].Value.ToString() == "ミッションID")
                {
                    cellsParam[i, 1].Value = missionId;
                    continue;
                }

                // 製作者名
                if (cellsParam[i, 0].Value.ToString() == "製作者名")
                {
                    cellsParam[i, 1].Value = AisConf.UserId;
                    continue;
                }

                // 収集タイプ
                if (cellsParam[i, 0].Value.ToString() == "収集タイプ")
                {
                    switch (this.dataSource)
                    {
                        case DataSourceType.CTM_DIRECT:
                            cellsParam[i, 1].Value = "CTM";
                            break;
                        case DataSourceType.MISSION:
                            cellsParam[i, 1].Value = "ミッション";
                            break;
                        case DataSourceType.GRIP:
                            cellsParam[i, 1].Value = "GRIP";
                            break;
                        default:
                            cellsParam[i, 1].Value = "";
                            break;
                    }
                    continue;
                }

                // for KMEW
                if (cellsParam[i, 0].Value.ToString() == "CmsVersion")
                {
                    cellsParam[i, 1].Value = AisConf.Config.CmsVersion;
                    continue;
                }
                if (AisConf.Config.CmsVersion == "3.5")
                {
                    var ahInfo = AisUtil.GetAhInfo(this.Ctms[0].id.ToString());

                    if (cellsParam[i, 0].Value.ToString() == "MfHost")
                    {
                        cellsParam[i, 1].Value = ahInfo.Mf;
                        continue;
                    }
                    if (cellsParam[i, 0].Value.ToString() == "MfPort")
                    {
                        cellsParam[i, 1].Value = ahInfo.MfPort;
                        continue;
                    }
                }

                // ProxyServer使用
                if (cellsParam[i, 0].Value.ToString() == "ProxyServer使用")
                {
                    cellsParam[i, 1].Value = AisConf.Config.UseProxy;
                    continue;
                }

                // ProxyServerURI
                if (cellsParam[i, 0].Value.ToString() == "ProxyServerURI")
                {
                    cellsParam[i, 1].Value = AisConf.Config.ProxyURI;
                    continue;
                }

                //ISSUE_NO.710 sunyi 2018/05/25 start
                //パラメータ追加
                // IsFirstSave
                if (cellsParam[i, 0].Value.ToString() == "IsFirstSave")
                {
                    cellsParam[i, 1].Value = "";
                    continue;
                }
                //ISSUE_NO.710 sunyi 2018/05/25 end
            }

            worksheetParam.Visible = SheetVisibility.Hidden;
        }
    }
}
