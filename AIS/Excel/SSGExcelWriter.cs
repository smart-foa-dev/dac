﻿using DAC.CtmData;
using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.AExcel.Grip;
using DAC.View.Helpers;
using CsvHelper;
using FoaCore.Common;
using FoaCore.Common.Util;
using SpreadsheetGear;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Documents;

namespace DAC.AExcel
{
    class SSGExcelWriter : ExcelWriter
    {
        public SSGExcelWriter(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi = null)
            : base(mode, ds, ctms, dt, bw)
        {

        }

        public override void WriteCtmData(string filePathDest, string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                // ワークブック作成
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePathDest);

                // シート作成
                SpreadsheetGear.IWorksheet worksheetCondition = AisUtil.GetSheetFromSheetName_SSG(workbook, GetSummarySheetName());

                WriteSummarySheet(worksheetCondition, this.dtOutput, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // paramシート作成
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(workbook, paramSheetName);
                WriteConfigSheet(worksheetParam, configParams);

                // ro_paramシート作成
                string roParamSheetName = Keywords.RO_PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_SSG(workbook, roParamSheetName);
                WriteRoParamSheet_SSG(worksheetRoParam);          
                
                if (this.dataSource == DataSourceType.GRIP)
                {
                    writeGripResultSheets(workbook, folderPath);
                    
                }
                else
                {
                    writeResultSheets(workbook, folderPath, configParams);
                }

                //ISSUE_NO.790 sunyi 2018/07/18 Start
                //集計、オンラインテンプレートシートを非表示にする
                SpreadsheetGear.IWorksheet sheet;
                if (Mode.Equals(ControlMode.Spreadsheet))
                {
                    sheet = AisUtil.GetSheetFromSheetName_SSG(workbook, Keywords.SPREAD_SHEET);
                    sheet.Visible = SheetVisibility.Hidden;
                }
                if (Mode.Equals(ControlMode.Online))
                {
                    sheet = AisUtil.GetSheetFromSheetName_SSG(workbook, Keywords.ONLINE_SHEET);
                    sheet.Visible = SheetVisibility.Hidden;
                }
                //ISSUE_NO.790 sunyi 2018/07/18 End

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 保存
                workbook.Save();
            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close();
                }
            }

            ////E2-126 sunyi 20190116 start
            ////autofit()
            //AutoFitToSheet(filePathDest, folderPath);
            ////E2-126 sunyi 20190116 end
        }
        ////E2-126 sunyi 20190116 start
        //private void AutoFitToSheet(string filePathDest, string folderPath)
        //{
        //    Microsoft.Office.Interop.Excel.Application excel = null;
        //    Microsoft.Office.Interop.Excel.Workbooks workbooks = null;
        //    Microsoft.Office.Interop.Excel.Workbook workbook = null;
        //    Microsoft.Office.Interop.Excel.Worksheet worksheet = null;
        //    // ワークブック作成
        //    try
        //    {
        //        excel = new Microsoft.Office.Interop.Excel.Application();
        //        excel.Visible = false;
        //        workbooks = excel.Workbooks;
        //        workbook = workbooks.Open(filePathDest);
        //        ExcelUtil.MoveExcelOutofScreen(excel);

        //        if (this.dataSource == DataSourceType.GRIP)
        //        {
        //            DirectoryInfo di = new DirectoryInfo(folderPath);
        //            foreach (var fi in di.GetFiles())
        //            {
        //                worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, AisUtil.RemoveExtention(fi.Name));
        //                worksheet.UsedRange.Columns.AutoFit();
        //            }
        //        }
        //        else
        //        {
        //            foreach (var ctm in this.Ctms)
        //            {
        //                var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
        //                worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, ctmName);
        //                worksheet.UsedRange.Columns.AutoFit();
        //            }
        //            worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, "result");
        //            worksheet.UsedRange.Columns.AutoFit();
        //        }

        //        // 保存
        //        workbook.Save();

        //        ExcelUtil.RestoreExcelWindow(excel);
        //    }
        //    finally
        //    {
        //        if (workbook != null)
        //        {
        //            workbook.Close(false);
        //            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbook);
        //            workbook = null;
        //        }

        //        if (workbooks != null)
        //        {
        //            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbooks);
        //            workbooks = null;
        //        }

        //        if (excel != null)
        //        {
        //            excel.Quit();
        //            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
        //            excel = null;
        //        }
        //    }
        //}
        ////E2-126 sunyi 20190116 end

        private void writeGripResultSheets(SpreadsheetGear.IWorkbook workbook, string folderPath)
        {
            var writer = new GripResultSheetWriter(this.Mode, this.Ctms, this.dtOutput, this.bw);

            DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (var fi in di.GetFiles())
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (fi.Extension.ToUpper() != ".CSV")
                {
                    continue;
                }

                // シート検索 / 作成
                string tmpSheetName = AisUtil.RemoveExtention(fi.Name);
                if(tmpSheetName.Length > 31)
                {
                    int lastindex = tmpSheetName.IndexOf("_Route");
                    if(lastindex != -1)
                    {
                        string firstpart = tmpSheetName.Substring(0, lastindex);
                        string lastpart = tmpSheetName.Substring(lastindex);
                        string cutname = firstpart.Substring(0, 31 - lastpart.Length);
                        tmpSheetName = cutname + lastpart;
                    }
                    else
                    {
                        tmpSheetName = tmpSheetName.Substring(0, 31);
                    }
                }
                SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, tmpSheetName);
                //SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, AisUtil.RemoveExtention(fi.Name));
                worksheet.Cells.Clear();
                writer.WriteByCtmResultSheetSimple(worksheet, fi.FullName, null, null);



                // 次のシートへ
                //AISTEMP-125 sunyi 20190329
                //SSG幅調整
                //worksheet.UsedRange.Columns.AutoFit();
                worksheet.UsedRange.ColumnWidth = 16.00;

                ////AISMM-75 sunyi 201901010 start
                //// resultシートをRECEIVE TIMEに従いDESCソートする
                //SpreadsheetGear.IRange srchRangeHeader = worksheet.Cells["A1"];
                //SpreadsheetGear.IRange lastRange = srchRangeHeader.EndRight;
                //SpreadsheetGear.IRange workRange = srchRangeHeader.EndDown;
                //workRange = worksheet.Cells[1, 1, workRange.Row, lastRange.Column];

                //SpreadsheetGear.SortKey sortKey1 = new SpreadsheetGear.SortKey(0, SpreadsheetGear.SortOrder.Descending, SpreadsheetGear.SortDataOption.Normal);
                //workRange.Sort(SpreadsheetGear.SortOrientation.Rows, false, sortKey1);
                //workRange.Columns.AutoFit();
                ////AISMM-75 sunyi 201901010 end

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
        }


        private void writeResultSheets(SpreadsheetGear.IWorkbook workbook, string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            List<XYZ> srcCsvFiles = null;
            try
            {
                srcCsvFiles = new List<XYZ>();
                foreach (var ctm in this.Ctms)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var ctmId = ctm.id.ToString();
                    var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    DataRow dataRow = null;
                    foreach (var rowObject in this.dtOutput.Rows)
                    {
                        DataRow row = rowObject as DataRow;
                        if (row.ItemArray[0].ToString() == ctmName)
                        {
                            dataRow = row;
                            break;
                        }
                    }
                    if (dataRow == null)
                    {
                        // CTM自体の選択が無かったので無視
                        continue;
                    }
                    string tmpSheetName = ctmName;
                    if (tmpSheetName.Length > 31)
                    {
                        int lastindex = tmpSheetName.IndexOf("_Route");
                        if (lastindex != -1)
                        {
                            string firstpart = tmpSheetName.Substring(0, lastindex);
                            string lastpart = tmpSheetName.Substring(lastindex);
                            string cutname = firstpart.Substring(0, 31 - lastpart.Length);
                            tmpSheetName = cutname + lastpart;
                        }
                        else
                        {
                            tmpSheetName = tmpSheetName.Substring(0, 31);
                        }
                    }
                    SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, tmpSheetName);
                    // シート検索 / 作成
                    //SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, ctmName);
                    worksheet.Cells.Clear();
                    //ISSUE_NO.728 sunyi 2018/06/25 Start
                    //仕様変更、resultシートを表示する
                    //worksheet.Visible = SheetVisibility.Hidden;
                    //ISSUE_NO.728 sunyi 2018/06/25 End

                    var srcFilepath = Path.Combine(folderPath, ctmId + ".csv");
                    CsvFile csvFileObj = null;
                    if (File.Exists(srcFilepath))
                    {
                        csvFileObj = new CsvFile() { Id = new FoaCore.Suid(ctmId), Name = ctmName, Filepath = srcFilepath };
                        var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                        srcCsvFiles.Add(srcCsvFile);
                    }

                    var rowS = DataRowToArray2(dataRow);
                    writeByCtmResultSheet(worksheet, ctm, rowS, this.bw, csvFileObj);

                    // 次のシートへ
                    //AISTEMP-125 sunyi 20190329
                    //SSG幅調整
                    //worksheet.UsedRange.Columns.AutoFit();
                    worksheet.UsedRange.ColumnWidth = 16.00;
                    //worksheet.UsedRange.Columns.AutoFit();
                    //worksheet.Cells["A:A"].ColumnWidth = 14.40;

                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }
                }

                long displayStartCurrentTime = 0;
                DateTime now = DateTime.Now;
                DateTime displayStartCurrentTimeDateTime = GetDateTimeVal(configParams, ExcelConfigParam.DISPLAY_START_CURRENT_TIME, now);
                if (displayStartCurrentTimeDateTime != now)
                {
                    displayStartCurrentTime = UnixTime.ToLong(displayStartCurrentTimeDateTime);
                }

                // 総合結果シート作成
                writeIntegratedResultSheet(workbook, this.dtOutput, srcCsvFiles, displayStartCurrentTime);
            }
            finally
            {
                if (srcCsvFiles != null)
                {
                    foreach (XYZ srcCsvFile in srcCsvFiles)
                    {
                         srcCsvFile.Dispose();
                    }
                }
            }
        }

        protected virtual void WriteSummarySheet(IWorksheet worksheetCondition, DataTable dt, Dictionary<ExcelConfigParam, object> configParams)
        {
            string missionName = GetStringVal(configParams, ExcelConfigParam.MISSION_NAME, null);
            string missionId = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, null);

            DateTime start = GetDateTimeVal(configParams, ExcelConfigParam.START, DateTime.MinValue);
            if (start == DateTime.MinValue)
            {
                throw new InvalidDataException("start is not specified.");
            }

            DateTime end = GetDateTimeVal(configParams, ExcelConfigParam.END, DateTime.MinValue);
            if (end == DateTime.MinValue)
            {
                throw new InvalidDataException("end is not specified.");
            }

            // 収集条件を出力
            SpreadsheetGear.IRange cellsCondition = worksheetCondition.Cells;
            cellsCondition["B1"].Value = missionName;
            cellsCondition["B2"].Value = missionId;
            
            cellsCondition["B3"].Value = start.ToString("yyyy/MM/dd");
            cellsCondition["C3"].Value = start.ToString("HH:mm:ss");

            cellsCondition["B4"].Value = end.ToString("yyyy/MM/dd");
            cellsCondition["C4"].Value = end.ToString("HH:mm:ss");

            AisUtil.DeleteUnitRow(dt);
            DataTable dtWithId = addIdToDataTable(dt);
            int dtRow = dtWithId.Rows.Count;
            int dtColumn = dtWithId.Columns.Count;

            SpreadsheetGear.IRange elementRangeFirstCell = worksheetCondition.Cells["A6"];
            elementRangeFirstCell.CopyFromDataTable(dtWithId, SpreadsheetGear.Data.SetDataFlags.None);
            cellsCondition[5, 0, 5, dtColumn - 1].Interior.Color = SpreadsheetGear.Colors.CornflowerBlue;
            cellsCondition[5, 0, 5, dtColumn - 1].Font.Color = SpreadsheetGear.Colors.White;
        }

        protected virtual void WriteConfigSheet(IWorksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            string fileName = GetStringVal(configParams, ExcelConfigParam.REGISTERED_FILENAME, string.Empty);

            SpreadsheetGear.IRange cellsParam = worksheetParam.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                // null check
                if (cellsParam[i, 0].Value == null)
                {
                    continue;
                }

                // 登録フォルダ
                if (cellsParam[i, 0].Value.ToString() == "登録フォルダ")
                {
                    cellsParam[i, 1].Value = AisConf.RegistryFolderPath;
                    continue;
                }

                // 登録ファイル
                if (cellsParam[i, 0].Value.ToString() == "登録ファイル")
                {
                    cellsParam[i, 1].Value = fileName;
                    continue;
                }

                // サーバIPアドレス
                if (cellsParam[i, 0].Value.ToString() == "サーバIPアドレス")
                {
                    cellsParam[i, 1].Value = AisConf.Config.CmsHost;
                    continue;
                }

                // サーバポート番号
                if (cellsParam[i, 0].Value.ToString() == "サーバポート番号")
                {
                    cellsParam[i, 1].Value = AisConf.Config.CmsPort;
                    continue;
                }

                // テンプレート名称
                if (cellsParam[i, 0].Value.ToString() == "テンプレート名称")
                {
                    cellsParam[i, 1].Value = "集計表";
                    continue;
                }

                // ミッションID
                if (cellsParam[i, 0].Value.ToString() == "ミッションID")
                {
                    cellsParam[i, 1].Value = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, null);
                    continue;
                }

                // 製作者名
                if (cellsParam[i, 0].Value.ToString() == "製作者名")
                {
                    cellsParam[i, 1].Value = AisConf.UserId;
                    continue;
                }

                // 収集タイプ
                if (cellsParam[i, 0].Value.ToString() == "収集タイプ")
                {
                    switch (this.dataSource)
                    {
                        case DataSourceType.CTM_DIRECT:
                            cellsParam[i, 1].Value = "CTM";
                            break;
                        case DataSourceType.MISSION:
                            cellsParam[i, 1].Value = "ミッション";
                            break;
                        case DataSourceType.GRIP:
                            cellsParam[i, 1].Value = "GRIP";
                            break;
                        default:
                            cellsParam[i, 1].Value = "";
                            break;
                    }
                    continue;
                }

                if (cellsParam[i, 0].Value.ToString() == "GRIPサーバIPアドレス")
                {
                    cellsParam[i, 1].Value = AisConf.Config.GripServerHost;
                    continue;
                }

                if (cellsParam[i, 0].Value.ToString() == "GRIPサーバポート番号")
                {
                    cellsParam[i, 1].Value = AisConf.Config.GripServerPort;
                    continue;
                }

                // for KMEW
                if (cellsParam[i, 0].Value.ToString() == "CmsVersion")
                {
                    cellsParam[i, 1].Value = AisConf.Config.CmsVersion;
                    continue;
                }
                if (AisConf.Config.CmsVersion == "3.5")
                {
                    var ahInfo = AisUtil.GetAhInfo(this.Ctms[0].id.ToString());

                    if (cellsParam[i, 0].Value.ToString() == "MfHost")
                    {
                        cellsParam[i, 1].Value = ahInfo.Mf;
                        continue;
                    }
                    if (cellsParam[i, 0].Value.ToString() == "MfPort")
                    {
                        cellsParam[i, 1].Value = ahInfo.MfPort;
                        continue;
                    }
                }

                // ProxyServer使用
                if (cellsParam[i, 0].Value.ToString() == "ProxyServer使用")
                {
                    cellsParam[i, 1].Value = AisConf.Config.UseProxy;
                    continue;
                }

                // ProxyServerURI
                if (cellsParam[i, 0].Value.ToString() == "ProxyServerURI")
                {
                    cellsParam[i, 1].Value = AisConf.Config.ProxyURI;
                    continue;
                }

                //ISSUE_NO.710 sunyi 2018/05/25 start
                //パラメータ追加
                // IsFirstSave
                if (cellsParam[i, 0].Value.ToString() == "IsFirstSave")
                {
                    cellsParam[i, 1].Value = "";
                }
                //ISSUE_NO.710 sunyi 2018/05/25 end
            }

            worksheetParam.Visible = SheetVisibility.Hidden;
        }

        private static void writeByCtmResultSheet(IWorksheet worksheet, CtmObject originalCtm, DataRow row, BackgroundWorker bw, CsvFile csvFile)
        {
            Dictionary<string, string> id2Name = new Dictionary<string, string>();
            foreach (var element in originalCtm.GetAllElements())
            {
                id2Name.Add(element.id.ToString(), element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
            }

            SpreadsheetGear.IRange cells = worksheet.Cells;

            int rowIndex = 1;

            int blockRowIndex = 0;
            int headCount = 1;

            // Header
            {
                cells["A1"].Value = Keywords.RECEIVE_TIME;

                // エレメント名
                int i = 1;

                bool firstColumn = true;
                foreach (var item in row.ItemArray)
                {
                    if (firstColumn)
                    {
                        firstColumn = false;
                        continue;
                    }

                    // エレメント名
                    cells[0, i].Value = item;
                    i++;
                    headCount++;
                }
            }

            if (csvFile == null)
            {
                return;
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }

            string[,] values = new string[AisUtil.MAX_BUFF_ROWS, headCount];

            using (var sr = new StreamReader(csvFile.Filepath, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var headers = reader.FieldHeaders;


                var pos2Pos = new Dictionary<int, int>();

                for (int i = 1; i < headers.Length; i++)
                {
                    var id1 = headers[i];
                    if (!id2Name.ContainsKey(id1)) continue;
                    var name1 = id2Name[id1];
                    int pos = GetPositionInDataRow(row, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 1); // RT
                    }

                }

                do
                {
                    // Cancel Check
                    if (bw.CancellationPending)
                    {
                        return;
                    }

                    var rec = reader.CurrentRecord;

                    long rt = Int64.Parse(rec[0]);

                    values[blockRowIndex, 0] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);

                    for (int j = 1; j < rec.Length; j++)
                    {
                        int columnIndex;
                        if (pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            values[blockRowIndex, columnIndex] = rec[j];
                        }
                    }

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        SpreadsheetGear.IRange range = cells[rowIndex, 0, currentRowIndex - 1, headCount - 1];
                        range.Value = values;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;

                        // Cancel Check
                        if (bw.CancellationPending)
                        {
                            return;
                        }
                    }
                } while (reader.Read());

                if (0 != blockRowIndex)
                {
                    int currentRowIndex = rowIndex + blockRowIndex;
                    SpreadsheetGear.IRange range = cells[rowIndex, 0, currentRowIndex - 1, headCount - 1];
                    range.Value = values;
                }
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }
        }

        private static void writeByCtmResultSheet(IWorksheet worksheet, CtmObject originalCtm, string[] rows, BackgroundWorker bw, CsvFile csvFile)
        {
            Dictionary<string, string> id2Name = new Dictionary<string, string>();
            foreach (var element in originalCtm.GetAllElements())
            {
                id2Name.Add(element.id.ToString(), element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
            }

            SpreadsheetGear.IRange cells = worksheet.Cells;
            int rowIndex = 1;
            int blockRowIndex = 0;
            int headCount = 1;

            // Header
            {
                cells["A1"].Value = Keywords.RECEIVE_TIME;

                // エレメント名
                int i = 1;
                bool firstColumn = true;
                foreach (var item in rows)
                {
                    if (firstColumn)
                    {
                        firstColumn = false;
                        continue;
                    }

                    // エレメント名
                    cells[0, i].Value = item;
                    i++;
                    headCount++;
                }
            }

            if (csvFile == null)
            {
                return;
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }

            string[,] values = new string[AisUtil.MAX_BUFF_ROWS, headCount];

            using (var sr = new StreamReader(csvFile.Filepath, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var headers = reader.FieldHeaders;
                var pos2Pos = new Dictionary<int, int>();

                for (int i = 1; i < headers.Length; i++)
                {
                    var id1 = headers[i];
                    if (!id2Name.ContainsKey(id1))
                    {
                        //AisMessageBox.DisplayWarningMessageBox("AIS_W_001");
                        //MessageBox.Show("ミッション情報に削除されたエレメントが含まれています。",
                        //    "警告",
                        //    MessageBoxButton.OK,
                        //    MessageBoxImage.Warning);
                        continue;
                    }
                    var name1 = id2Name[id1];
                    int pos = GetPositionInDataRowS(rows, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 1); // RT
                    }
                }

                do
                {
                    // Cancel Check
                    if (bw.CancellationPending)
                    {
                        return;
                    }

                    var rec = reader.CurrentRecord;
                    long rt = Int64.Parse(rec[0]);
                    values[blockRowIndex, 0] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);

                    for (int j = 1; j < rec.Length; j++)
                    {
                        int columnIndex;
                        if (pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            string item = rec[j];
                            if (item.StartsWith("=="))
                            {
                                item = "'" + item;
                            }

                            values[blockRowIndex, columnIndex] = item;
                        }
                    }

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        SpreadsheetGear.IRange range = cells[rowIndex, 0, currentRowIndex - 1, headCount - 1];
                        range.Value = values;
                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;

                        // Cancel Check
                        if (bw.CancellationPending)
                        {
                            return;
                        }
                    }
                } while (reader.Read());

                if (0 != blockRowIndex)
                {
                    int currentRowIndex = rowIndex + blockRowIndex;
                    SpreadsheetGear.IRange range = cells[rowIndex, 0, currentRowIndex - 1, headCount - 1];
                    range.Value = values;
                }
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }
        }

        private void writeIntegratedResultSheet(SpreadsheetGear.IWorkbook workbook, DataTable dtOutput, List<XYZ> srcFiles, long displayStartTime = 0)
        {
            // シート作成
            SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, "result");
            worksheet.Cells.Clear();
            SpreadsheetGear.IRange range = worksheet.Cells;

            // ヘッダー
            range[0, 0].Value = Keywords.CTM_NAME;
            range[0, 1].Value = Keywords.RECEIVE_TIME;

            int headerIdx = 2;
            bool firstColumn = true;
            foreach (DataColumn column in dtOutput.Columns)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (firstColumn)
                {
                    firstColumn = false;
                    continue;
                }
                var colName0 = column.ColumnName;
                var colName = colName0.Substring(0, colName0.Length - 3);

                range[0, headerIdx].Value = colName;
                headerIdx++;
            }
            int headCount = headerIdx;

            //
            // レコードを書く前の準備
            //
            foreach (var csvFile in srcFiles)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                csvFile.Init();
                var reader = csvFile.reader;
                reader.Read(); // Read Header

                var pos2Pos = new Dictionary<int, int>();
                var header = reader.FieldHeaders;
                for (int i = 1; i < header.Length; i++)
                {
                    var id1 = header[i];
                    var el = csvFile.ctmObj.GetElement(new GUID(id1));
                    if (el == null)
                    {
                        continue;
                    }
                    var name1 = el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    int pos = AisUtil.GetPositionInDataRowForAllCtms(csvFile.dtatRow, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 2); // RT + CTM NAME
                    }

                }
                csvFile.pos2Pos = pos2Pos;
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }

            //
            // レコード
            //
            using (var merger = new MergeIterators(srcFiles))
            {
                int rowIndex = 1;
                int blockRowIndex = 0;
                string[,] values = new string[AisUtil.MAX_BUFF_ROWS, headCount];

                do
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    XYZ currentIterator = merger.Current();
                    var rec = currentIterator.Current();
                    long rt = Int64.Parse(rec[0]);
                    if (rt < displayStartTime)
                    {
                        continue;
                    }

                    values[blockRowIndex, 0] = currentIterator.Name();
                    values[blockRowIndex, 1] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);

                    for (int j = 1; j < rec.Length; j++)
                    {
                        int columnIndex;
                        if (currentIterator.pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            string item = rec[j];
                            if (item.StartsWith("=="))
                            {
                                item = "'" + item;
                            }

                            values[blockRowIndex, columnIndex] = item;
                        }
                    }

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        SpreadsheetGear.IRange range2 = range[rowIndex, 0, currentRowIndex - 1, headCount - 1];
                        range2.Value = values;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;


                        // Cancel Check
                        if (this.bw.CancellationPending)
                        {
                            return;
                        }
                    }
                } while (merger.MoveNext());

                if (0 != blockRowIndex)
                {
                    int currentRowIndex = rowIndex + blockRowIndex;
                    SpreadsheetGear.IRange range3 = range[rowIndex, 0, currentRowIndex - 1, headCount - 1];
                    range3.Value = values;
                }
            }
            //AISTEMP-125 sunyi 20190329
            //SSG幅調整
            worksheet.UsedRange.ColumnWidth = 16.00;

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }
        }
    }
}
