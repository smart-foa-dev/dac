﻿using DAC.AExcel.Grip;
using DAC.CtmData;
using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using CsvHelper;
using FoaCore;
using FoaCore.Common;
using FoaCore.Common.Util;
using log4net;
//using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace DAC.AExcel
{
    class InteropExcelWriterMultiStatusMonitor : ExcelWriter
    {
        //ISSUE_NO.727 sunyi 2018/06/14 Start
        //grip mission処理ができるように修正にする
        private Mission gripMission;
        //ISSUE_NO.727 sunyi 2018/06/14 End
        private Dictionary<Mission, List<CtmObject>> missionCtmDict;
        private Dictionary<CtmObject, List<CtmElement>> missionCtmElementDict;

		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/17 Start
        private Dictionary<CtmObject, List<CtmElement>> ctmElementDic;
        private string onlineId;
        private string workplaceId;
		// 画面データ
        private JArray resultArray;
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/17 End

        private Dictionary<string, string> uuidAndMissionIds;

        public InteropExcelWriterMultiStatusMonitor(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi = null)
            : base(mode, ds, ctms, dt, bw)
        {

        }

        public InteropExcelWriterMultiStatusMonitor(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi, Dictionary<Mission, List<CtmObject>> ctmDict,
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/17 Start
            Dictionary<CtmObject, List<CtmElement>> elemDict, string onlineId, string workplaceId, JArray resultArray, Dictionary<CtmObject, List<CtmElement>> ctmElementDic, Dictionary<string, string> UUIDAndMissionIds = null)
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/17 End
            : base(mode, ds, ctms, dt, bw)
        {
            //ISSUE_NO.727 sunyi 2018/06/14 Start
            //grip mission処理ができるように修正にする
            this.gripMission = mi;
            //ISSUE_NO.727 sunyi 2018/06/14 End
            this.missionCtmDict = ctmDict;
            this.missionCtmElementDict = elemDict;
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/17 Start
            this.ctmElementDic = ctmElementDic;
            this.onlineId = onlineId;
            this.workplaceId = workplaceId;
            this.resultArray = resultArray;
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/17 End

            this.uuidAndMissionIds = UUIDAndMissionIds;
        }

        public override void WriteCtmData(string excelName, string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
            Microsoft.Office.Interop.Excel.Workbooks oWBooks = null;
            Microsoft.Office.Interop.Excel.Workbook oWBook = null;

            try
            {
                oXls = new Microsoft.Office.Interop.Excel.Application();

                //ISSUE_NO.799 Sunyi 2018/07/25 Start
                //処理中にEXCELを触らないようにする(Dac744)
                Dictionary<IntPtr, DAC.AExcel.ExcelUtil.Rect> ExcelInfo = ExcelUtil.MoveExcelOutofScreen_Status(oXls);
                //ISSUE_NO.799 Sunyi 2018/07/25 End

                oXls.Visible = false; //確認のためエクセルのウィンドウを表示する

                //エクセルファイルをオープンする
                oWBooks = oXls.Workbooks;
                oWBook = oWBooks.Open(excelName); // オープンするExcelファイル名
                Microsoft.Office.Interop.Excel.Worksheet wsUserInfo = AisUtil.GetSheetFromSheetName_Interop(oWBook, "USERINFO");
                writeUserInfoSheet(wsUserInfo);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }


                // 1. Set Mission CTM & Element parameter
                string paramSheetNameM = "ミッション";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamM = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameM);

                if (this.dataSource == DataSourceType.MISSION)
                {
                    writeConfigSheetCtm(worksheetParamM);
                }
                else if (this.dataSource == DataSourceType.CTM_DIRECT)
                {
                    writeConfigSheetCtm2(worksheetParamM);
                }

                //ISSUE_NO.727 sunyi 2018/06/14 Start
                //grip mission処理ができるように修正にする
                if (this.dataSource == DataSourceType.GRIP)
                {
                    string paramSheetNameR = "Routes";
                    Microsoft.Office.Interop.Excel.Worksheet worksheetParamR = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameR);
                    writeConfigSheetMissionInfo(worksheetParamM);
                    writeConfigSheetRoute(worksheetParamR, folderPath);
                }
                //ISSUE_NO.727 sunyi 2018/06/14 End

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 2. Set Start & End Time parameter
                string paramSheetNameC = "業務時間";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamC = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameC);
                writeConfigSheetTime(worksheetParamC, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                string paramSheetName = DAC.Util.Keywords.PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetParam3 = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetName);
                WriteConfigSheetParam(worksheetParam3, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = DAC.Util.Keywords.RO_PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_Interop(oWBook, roParamSheetName);
                WriteRoParamSheet_Interop(worksheetRoParam);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                //ISSUE_NO.727 sunyi 2018/06/14 Start
                //grip mission処理ができるように修正にする
                //// 初回ミッション結果を出力
                //DirectoryInfo root = new DirectoryInfo(folderPath);
                //if (root.Exists)
                //{
                //    writeData2(oXls, oWBook, root);
                //}
                if (this.dataSource == DataSourceType.MISSION)
                {
                    // 初回ミッション結果を出力
                    DirectoryInfo root = new DirectoryInfo(folderPath);
                    if (root.Exists)
                    {
                        writeData2(oXls, oWBook, root);
                    }
                }
                else if (this.dataSource == DataSourceType.GRIP)
                {
                    writeGripResultSheets(oWBook, folderPath);
                }
                //ISSUE_NO.727 sunyi 2018/06/14 End

                /*
                foreach (DirectoryInfo di in root.GetDirectories())
                {
                    string path = di.FullName;
                    if (!Directory.Exists(path))
                    {
                        continue;
                    }

                    writeData(oXls, oWBook, path);
                }
                */
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 3. Save the sheet with the parameter
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Save();
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Close();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                //// 4. Re-open again with the newly inputted parameter sheet
                oWBook = (Microsoft.Office.Interop.Excel.Workbook)(oXls.Workbooks.Open(excelName));
                oXls.Visible = true; //確認のためエクセルのウィンドウを表示する
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Activate();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                oXls.Run("MakeMenuBar");
                //ISSUE_NO.799 Sunyi 2018/07/25 Start
                //処理中にEXCELを触らないようにする(Dac744)
                ExcelUtil.RestoreExcelWindow_Status(ExcelInfo);
                //ISSUE_NO.799 Sunyi 2018/07/25 End
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
            catch (Exception err)
            {
                // ログ出力
                System.Windows.MessageBox.Show("ExcelOpen Exception Error : \n" + err.ToString());
            }
            finally
            {
                if (oWBook != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                    oWBook = null;
                }

                if (oWBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBooks);
                    oWBooks = null;
                }

                if (oXls != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                    oXls = null;
                }
            }

        }

        protected void writeData(Microsoft.Office.Interop.Excel.Application excel, Microsoft.Office.Interop.Excel.Workbook book, string folderPath)
        {
            DataRow[] rows = convertToDatRowFromDictionary(this.missionCtmElementDict);
            List<CtmObject> ctms = convertToListFromDictionary(this.missionCtmElementDict);

            List<XYZ> srcCsvFiles = null;
            try
            {
                srcCsvFiles = new List<XYZ>();
                foreach (var ctm in ctms)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var ctmId = ctm.id.ToString();
                    var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    DataRow dataRow = null;
                    foreach (var row in rows)
                    {
                        if (row.ItemArray[0].ToString() == ctmName)
                        {
                            dataRow = row;
                            break;
                        }
                    }

                    if (dataRow == null)
                    {
                        // CTM自体の選択が無かったので無視
                        continue;
                    }

                    // シート検索 / 作成
                    Microsoft.Office.Interop.Excel.Worksheet sheet = AisUtil.GetSheetFromSheetName_Interop(book, ctmName);
                    sheet.Rows.Clear();

                    var srcFilepath = Path.Combine(folderPath, ctmId + ".csv");
                    CsvFile csvFileObj = null;
                    if (File.Exists(srcFilepath))
                    {
                        csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcFilepath };
                        var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                        srcCsvFiles.Add(srcCsvFile);
                    }

                    writeByCtmResultSheet(sheet, csvFileObj, ctm, dataRow, this.bw);

                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // 次のシートへ
                    //AISTEMP-125 sunyi 20190329
                    //SSG幅調整
                    //sheet.UsedRange.Columns.AutoFit();
                    sheet.UsedRange.ColumnWidth = 16.00;
                }
            }
            finally
            {
                if (srcCsvFiles != null)
                {
                    foreach (XYZ srcCsvFile in srcCsvFiles)
                    {
                        srcCsvFile.Dispose();
                    }
                }
            }
        }

        //ISSUE_NO.727 sunyi 2018/06/14 Start
        //grip mission処理ができるように修正にする
        private void writeGripResultSheets(Microsoft.Office.Interop.Excel.Workbook workbook, string folderPath,JArray jarryCtms = null)
        {
            var writer = new GripResultSheetWriter(this.Mode, this.Ctms, this.dtOutput, this.bw);

            DirectoryInfo di = new DirectoryInfo(folderPath);
            foreach (var fi in di.GetFiles())
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                //MM_No.118 sun 20190726 start
                JArray jArrayCtmTmp = new JArray();
                if (jarryCtms != null)
                {
                    foreach (var ctm in jarryCtms)
                    {
                        JToken token = ctm as JToken;
                        if (token["checkedFlag"].ToString().Equals("True"))
                        {
                            jArrayCtmTmp.Add(ctm);
                        }
                    }

                    bool isChecked = false;
                    foreach (var ctmTmp in jArrayCtmTmp)
                    {
                        JToken token = ctmTmp as JToken;
                        if (token["name"] == null) continue;
                        string[] rowsArray = token["name"].ToString().Split('_');
                        string name = rowsArray[1] + "_" + rowsArray[2];
                        if (fi.Name.Contains(name))
                        {
                            isChecked = true;
                            break;
                        }
                    }
                    if (!isChecked)
                    {
                        continue;
                    }
                }
                //MM_No.118 sun 20190726 end

                if (fi.Extension.ToUpper() != ".CSV")
                {
                    continue;
                }

                // シート検索 / 作成
                Microsoft.Office.Interop.Excel.Worksheet worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, AisUtil.RemoveExtention(fi.Name));
                worksheet.Rows.Clear();
                writer.WriteByCtmResultSheetSimple(worksheet, fi.FullName, null, null);

                // 次のシートへ
                //worksheet.UsedRange.Columns.AutoFit();
                //AISTEMP-125 sunyi 20190329
                //SSG幅調整
                //worksheet.UsedRange.Columns.AutoFit();
                worksheet.UsedRange.ColumnWidth = 16.00;

                //AISMM-75 sunyi 201901010 start
                // resultシートをRECEIVE TIMEに従いDESCソートする
                Microsoft.Office.Interop.Excel.Range srchRange = worksheet.get_Range("A2");
                Microsoft.Office.Interop.Excel.Range srchRangeHeader = worksheet.get_Range("A1");
                Microsoft.Office.Interop.Excel.Range lastRange = srchRangeHeader.End[Microsoft.Office.Interop.Excel.XlDirection.xlToRight];
                Microsoft.Office.Interop.Excel.Range workRange = srchRange.End[Microsoft.Office.Interop.Excel.XlDirection.xlDown];
                lastRange = worksheet.Cells[workRange.Row, lastRange.Column];
                workRange = worksheet.get_Range(srchRange, lastRange);
                worksheet.Sort.SortFields.Add(worksheet.get_Range("B2"), Microsoft.Office.Interop.Excel.XlSortOn.xlSortOnValues, Microsoft.Office.Interop.Excel.XlSortOrder.xlDescending);
                worksheet.Sort.SetRange(workRange);
                worksheet.Sort.Header = Microsoft.Office.Interop.Excel.XlYesNoGuess.xlNo;
                worksheet.Sort.MatchCase = false;
                worksheet.Sort.Orientation = Microsoft.Office.Interop.Excel.XlSortOrientation.xlSortColumns;
                worksheet.Sort.Apply();
                //AISMM-75 sunyi 201901010 end

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
        }
        //ISSUE_NO.727 sunyi 2018/06/14 End

        protected void writeData2(Microsoft.Office.Interop.Excel.Application excel, Microsoft.Office.Interop.Excel.Workbook book, DirectoryInfo root)
        {
            DataRow[] rows = convertToDatRowFromDictionary(this.missionCtmElementDict);
            List<CtmObject> ctms = convertToListFromDictionary(this.missionCtmElementDict);

            List<XYZ> srcCsvFiles = null;
            try
            {
                srcCsvFiles = new List<XYZ>();
                foreach (var ctm in ctms)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var ctmId = ctm.id.ToString();
                    var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    DataRow dataRow = null;
                    foreach (var row in rows)
                    {
                        if (row.ItemArray[0].ToString() == ctmName)
                        {
                            dataRow = row;
                            break;
                        }
                    }

                    if (dataRow == null)
                    {
                        // CTM自体の選択が無かったので無視
                        continue;
                    }

                    // シート検索 / 作成
                    Microsoft.Office.Interop.Excel.Worksheet sheet = AisUtil.GetSheetFromSheetName_Interop(book, ctmName);
                    sheet.Rows.Clear();

                    CsvFile csvFileObj = null;
                    foreach (DirectoryInfo di in root.GetDirectories())
                    {
                        string srcCsvPath = Path.Combine(di.FullName, ctmId + ".csv");
                        if (File.Exists(srcCsvPath))
                        {
                            csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcCsvPath };
                            var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                            srcCsvFiles.Add(srcCsvFile);
                        }
                    }

                    writeByCtmResultSheet(sheet, csvFileObj, ctm, dataRow, this.bw);
                    /*
                    var srcFilepath = Path.Combine(folderPath, ctmId + ".csv");
                    if (File.Exists(srcFilepath))
                    {
                        csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcFilepath };
                        var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                        srcCsvFiles.Add(srcCsvFile);
                    }

                    writeByCtmResultSheet(sheet, csvFileObj, ctm, dataRow, this.bw);
                    */
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // 次のシートへ
                    //AISTEMP-125 sunyi 20190329
                    //SSG幅調整
                    //sheet.UsedRange.Columns.AutoFit();
                    sheet.UsedRange.ColumnWidth = 16.00;
                }
            }
            finally
            {
                if (srcCsvFiles != null)
                {
                    foreach (XYZ srcCsvFile in srcCsvFiles)
                    {
                        srcCsvFile.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// CtmObjectの中身を、全てDataTableとして使用する
        /// </summary>
        /// <param name="ctms"></param>
        /// <returns></returns>
        private DataTable createDt(List<CtmObject> ctms)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));

            foreach (CtmObject ctm in ctms)
            {
                foreach (var element in ctm.GetAllElements())
                {
                    string elementName = element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    if (!dt.Columns.Contains(elementName))
                    {
                        dt.Columns.Add(new DataColumn(elementName));
                    }
                }

                DataRow row = dt.NewRow();
                row[Keywords.CTM_NAME] = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                foreach (var element in ctm.GetAllElements())
                {
                    string elementName = element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    row[elementName] = elementName;
                }
                dt.Rows.Add(row);
            }

            return dt;
        }

        /// <summary>
        /// CtmObjectの中身を、全てDataTableとして使用する
        /// </summary>
        /// <param name="missions"></param>
        /// <returns></returns>
        private DataTable createDt(Dictionary<Mission, List<CtmObject>> missions, Dictionary<CtmObject, List<CtmElement>> ctms)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));

            foreach (var ctmList in missions.Values)
            {
                foreach (CtmObject ctm in ctmList)
                {
                    bool hit = false;
                    foreach (var rowObj in dt.Rows)
                    {
                        var row = rowObj as DataRow;
                        if (row == null)
                        {
                            continue;
                        }
                        if (row[Keywords.CTM_NAME].ToString() != ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true))
                        {
                            continue;
                        }

                        hit = true;
                        break;
                    }
                    if (hit)
                    {
                        continue;
                    }

                    foreach (var element in ctm.GetAllElements())
                    {
                        string elementName = element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                        if (!dt.Columns.Contains(elementName))
                        {
                            dt.Columns.Add(new DataColumn(elementName));
                        }
                    }

                    DataRow row2 = dt.NewRow();
                    row2[Keywords.CTM_NAME] = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    foreach (var element in ctm.GetAllElements())
                    {
                        string elementName = element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                        row2[elementName] = elementName;
                    }
                    dt.Rows.Add(row2);
                }
            }

            return dt;
        }

        /// <summary>
        /// CtmObjectの中身を、全てDataRow配列として使用する
        /// </summary>
        /// <param name="missions"></param>
        /// <returns></returns>
        private DataRow[] convertToDatRowFromDictionary(Dictionary<CtmObject, List<CtmElement>> ctms)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));

            List<DataRow> rows = new List<DataRow>();
            foreach (var ctm in ctms.Keys)
            {
                DataTable dt2 = new DataTable();
                dt2.Columns.Add("CtmName");
                for (int i = 0; i < ctms[ctm].Count; i++)
                {
                    dt2.Columns.Add(string.Format("Element_{0}", i.ToString("00")));
                }

                DataRow row = dt2.NewRow();
                List<string> els = new List<string>();
                els.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                foreach (var el in ctms[ctm])
                {
                    els.Add(el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                }
                row.ItemArray = els.ToArray();

                rows.Add(row);
            }

            return rows.ToArray();
        }
		// FOA_サーバー化開発 Processing On Server Xj 2018/08/17 Start
        /// <summary>
        /// CtmObjectの中身を、全てDataRow配列として使用する
        /// </summary>
        /// <param name="missions"></param>
        /// <returns></returns>
        private Dictionary<CtmObject, DataRow> convertToDatRowFromDictionaryNew(Dictionary<CtmObject, List<CtmElement>> ctms)
        {
            Dictionary<CtmObject, DataRow> dicDataRow = new Dictionary<CtmObject, DataRow>();
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));

            foreach (var ctm in ctms.Keys)
            {
                DataTable dt2 = new DataTable();
                dt2.Columns.Add("CtmName");
                for (int i = 0; i < ctms[ctm].Count; i++)
                {
                    dt2.Columns.Add(string.Format("Element_{0}", i.ToString("00")));
                }

                DataRow row = dt2.NewRow();
                List<string> els = new List<string>();
                els.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                foreach (var el in ctms[ctm])
                {
                    els.Add(el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                }
                row.ItemArray = els.ToArray();

                dicDataRow.Add(ctm, row);
            }

            return dicDataRow;
        }
		// FOA_サーバー化開発 Processing On Server Xj 2018/08/17 End

        private List<CtmObject> convertToListFromDictionary(Dictionary<CtmObject, List<CtmElement>> elementMap)
        {
            List<CtmObject> ctms = new List<CtmObject>();
            foreach (CtmObject ctm in elementMap.Keys)
            {
                ctms.Add(ctm);
            }

            return ctms;
        }

        private static void writeByCtmResultSheet(Microsoft.Office.Interop.Excel.Worksheet worksheet, CsvFile csvFile,
          CtmObject originalCtm, DataRow row, BackgroundWorker bw)
        {
            Dictionary<string, string> id2Name = new Dictionary<string, string>();
            foreach (var element in originalCtm.GetAllElements())
            {
                id2Name.Add(element.id.ToString(), element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
            }

            Microsoft.Office.Interop.Excel.Range cells = worksheet.Cells;

            // Header
            int headCount = 1;
            cells[1, 1].Value = Keywords.RECEIVE_TIME;

            // エレメント名
            int headerIndex = 2;
            bool firstColumn = true;
            foreach (var item in row.ItemArray)
            {
                // Cancel Check
                if (bw.CancellationPending)
                {
                    return;
                }

                if (firstColumn)
                {
                    firstColumn = false;
                    continue;
                }

                var strVal = item as string;
                if (string.IsNullOrEmpty(strVal))
                {
                    continue;
                }

                cells[1, headerIndex].Value = item;
                headerIndex++;
                headCount++;

            }
            //AISMM-10 sunyi 20180109 start
            //出力しない
            cells[1, headerIndex].Value = Keywords.RT;
            cells[1, headerIndex + 1].Value = Keywords.CTM_ID;
            //AISMM-10 sunyi 20180109 end
            if (csvFile == null)
            {
                return;
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }

            object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headCount +2];
            string ctmId = System.IO.Path.GetFileNameWithoutExtension(csvFile.Filepath);
            using (var sr = new StreamReader(csvFile.Filepath, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var headers = reader.FieldHeaders;


                var pos2Pos = new Dictionary<int, int>();

                for (int i = 1; i < headers.Length; i++)
                {
                    var id1 = headers[i];

                    if (!id2Name.ContainsKey(id1)) continue;

                    var name1 = id2Name[id1];
                    int pos = GetPositionInDataRow(row, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 1); // RT
                    }
                }


                // Loop over rows
                int rowIndex = 2;
                int blockRowIndex = 0;
                do
                {
                    // Cancel Check
                    if (bw.CancellationPending)
                    {
                        return;
                    }

                    var rec = reader.CurrentRecord;

                    long rt = Int64.Parse(rec[0]);

                    values[blockRowIndex, 0] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);

                    for (int j = 1; j < rec.Length; j++)
                    {
                        int columnIndex;
                        if (pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            string item = rec[j];
                            if (item.StartsWith("=="))
                            {
                                item = "'" + item;
                            }

                            values[blockRowIndex, columnIndex] = item;
                        }
                    }
                    //AISMM-10 sunyi 20180109 start
                    //出力しない
                    values[blockRowIndex, headCount] = rt.ToString();
                    values[blockRowIndex, headCount + 1] = ctmId;
                    //AISMM-10 sunyi 20180109 end
                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                        range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, headCount+2);
                        range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                        // RT Format
                        Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 1];
                        range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;
                    }
                } while (reader.Read());

                // Cancel Check
                if (bw.CancellationPending)
                {
                    return;
                }

                if (0 != blockRowIndex)
                {
                    Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                    range = range.get_Resize(blockRowIndex, headCount+2);
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // RT Format
                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 1];
                    range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                    range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                }
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }
            //ISSUE_NO.728 sunyi 2018/06/25 Start
            //仕様変更、resultシートを表示する
            //worksheet.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
            //ISSUE_NO.728 sunyi 2018/06/25 End
        }

        private void writeIntegratedResultSheet(Microsoft.Office.Interop.Excel.Workbook workbook, DataTable dtOutput, List<XYZ> srcFiles, long displayStartCurrentBlock = 0)
        {
            Microsoft.Office.Interop.Excel.Worksheet worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, "result");
            worksheet.Rows.Clear();
            Microsoft.Office.Interop.Excel.Range cells = worksheet.Cells;

            // ヘッダー
            cells[1, 1].Value = Keywords.CTM_NAME;
            cells[1, 2].Value = Keywords.RECEIVE_TIME;

            int headerIdx = 3;
            bool firstColumn = true;
            foreach (DataColumn column in dtOutput.Columns)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (firstColumn)
                {
                    firstColumn = false;
                    continue;
                }
                var colName0 = column.ColumnName;
                var colName = colName0.Substring(0, colName0.Length - 3);

                cells[1, headerIdx].Value = colName;
                headerIdx++;
            }
            int headCount = headerIdx;

            //
            // レコードを書く前の準備
            //
            foreach (var csvFile in srcFiles)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                csvFile.Init();
                var reader = csvFile.reader;
                reader.Read(); // Read Header and first row

                var pos2Pos = new Dictionary<int, int>();
                var header = reader.FieldHeaders;
                for (int i = 1; i < header.Length; i++)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var id1 = header[i];
                    var el = csvFile.ctmObj.GetElement(new GUID(id1));
                    if (el == null)
                    {
                        continue;
                    }
                    var name1 = el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    int pos = AisUtil.GetPositionInDataRowForAllCtms(csvFile.dtatRow, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 2); // RT + CTM NAME
                    }
                }
                csvFile.pos2Pos = pos2Pos;
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }

            //
            // レコード
            //
            using (var merger = new MergeIterators(srcFiles))
            {
                int rowIndex = 2;
                int blockRowIndex = 0;
                object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headCount];

                do
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    XYZ currentIterator = merger.Current();
                    var rec = currentIterator.Current();
                    long rt = Int64.Parse(rec[0]);
                    if (rt < displayStartCurrentBlock)
                    {
                        continue;
                    }

                    values[blockRowIndex, 0] = currentIterator.Name();
                    values[blockRowIndex, 1] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);

                    for (int j = 1; j < rec.Length; j++)
                    {
                        int columnIndex;
                        if (currentIterator.pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            string item = rec[j];
                            if (item.StartsWith("=="))
                            {
                                item = "'" + item;
                            }

                            values[blockRowIndex, columnIndex] = item;
                        }
                    }

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                        range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, headCount);
                        range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                        // RT Format
                        Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 2];
                        range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;

                        // Cancel Check
                        if (this.bw.CancellationPending)
                        {
                            return;
                        }
                    }
                } while (merger.MoveNext());

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (0 != blockRowIndex)
                {
                    Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                    range = range.get_Resize(blockRowIndex, headCount);
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // RT Format
                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 2];
                    range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                    range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                }
            }
        }

        private void writeUserInfoSheet(Microsoft.Office.Interop.Excel.Worksheet ws)
        {
            ws.Cells[1, 1].Value = "'" + LoginInfo.GetInstance().GetLoginUserInfo().UserId;

            // Host
            Microsoft.Office.Interop.Excel.Range rangeA = AisUtil.GetFindRange_Interop(ws.Cells, "CmsHost");
            if (rangeA != null)
            {
                rangeA[1, 2].Value = AisConf.Config.CmsHost;
            }

            // Port
            Microsoft.Office.Interop.Excel.Range rangeB = AisUtil.GetFindRange_Interop(ws.Cells, "CmsPort");
            if (rangeB != null)
            {
                rangeB[1, 2].Value = AisConf.Config.CmsPort;
            }

            ws.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        private void writeConfigSheetTime(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            DateTime start = GetDateTimeVal(configParams, ExcelConfigParam.START, DateTime.MinValue);
            DateTime end = GetDateTimeVal(configParams, ExcelConfigParam.END, DateTime.MinValue);
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 操業開始時刻
            start.AddSeconds(-start.Second);
            start.AddMilliseconds(-start.Millisecond);
            cellsParam[1, 1].Value = start.ToString("HH:mm");

            // 操業終了時刻
            end.AddSeconds(-end.Second);
            end.AddMilliseconds(-end.Millisecond);
            cellsParam[2, 1].Value = end.ToString("HH:mm");

            // Hide the workSheet
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        private void writeConfigSheetCtm(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();

            // 2. Pass the Mission parameter
            int startRowIndex = 1;
            int eleStartRowIndex = 1;
            foreach (KeyValuePair<Mission, List<CtmObject>> kvpMission in missionCtmDict)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (kvpMission.Value.Count <= 0) continue; // skip mission who does not have selected CTM object

                // Mission ID
                cellsParam[startRowIndex, 1].Value = kvpMission.Key.Id;

                // Mission Name
                cellsParam[startRowIndex, 2].Value = AisUtil.GetCatalogLang(kvpMission.Key);

                // 3. Pass the CTM parameter
                int ctmStartRowIndex = startRowIndex;
                foreach (CtmObject ctm in kvpMission.Value)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // ParentMission ID
                    cellsParam[ctmStartRowIndex, 3].Value = kvpMission.Key.Id;

                    // CTM ID
                    cellsParam[ctmStartRowIndex, 4].Value = ctm.id.ToString();

                    // CTM Name
                    cellsParam[ctmStartRowIndex, 5].Value = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    // 4. Pass the Element parameter
                    foreach (CtmElement ele in missionCtmElementDict[ctm])
                    {
                        // Cancel Check
                        if (this.bw.CancellationPending)
                        {
                            return;
                        }

                        // ParentCTM ID
                        cellsParam[eleStartRowIndex, 6].Value = ctm.id.ToString();

                        // Element ID
                        cellsParam[eleStartRowIndex, 7].Value = ele.id.ToString();

                        // Element Name
                        cellsParam[eleStartRowIndex, 8].Value = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                        // Additional : ParentMission ID -> For deleting CTM Element in case there is duplicate CTM
                        cellsParam[eleStartRowIndex, 9].Value = kvpMission.Key.Id;

                        ++eleStartRowIndex;
                    }

                    ++ctmStartRowIndex;
                }

                startRowIndex = ctmStartRowIndex;
            }
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }
		// FOA_サーバー化開発 Processing On Server Xj 2018/08/17 Start
		//重複する名前のCtmのサフィックスを設定
        private string getCtmSuffix(List<string> ctmIdResults, string ctmId)
        {
            string suffix = string.Empty;

            int counts = ctmIdResults.Count(s => s == ctmId);
            if (counts > 1)
            {
                suffix = "(" + counts + ")";
            }

            return suffix;
        }

        private void writeConfigSheetCtmNew(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();


            Dictionary<CtmObject, List<CtmElement>> ctmElementDict = new Dictionary<CtmObject, List<CtmElement>>();

            // 2. Pass the Mission parameter
            int startRowIndex = 1;
            int eleStartRowIndex = 1;
            List<string> ctmIdResults = new List<string>();
            foreach (JToken missionToken in resultArray)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (int.Parse((string)missionToken["missionNewType"]) == 0)
                {
                    continue;
                }

                string missionId = (string)missionToken["id"];

                // Mission ID
                cellsParam[startRowIndex, 1].Value = missionId;

                // Mission Name
                cellsParam[startRowIndex, 2].Value = AisUtil.GetCatalogLangNew(missionToken);

                // 3. Pass the CTM parameter
                int ctmStartRowIndex = startRowIndex;
                JArray ctmArray = JArray.Parse(missionToken["ctms"].ToString());
                foreach (JToken ctm in ctmArray)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    object checkedFlag = ctm["checkedFlag"];
                    if (Convert.ToBoolean(checkedFlag) == false)
                    {
                        continue;
                    }

                    string ctmMissionId = ((string)ctm["id"]).Split('-')[0];
                    if (ctmMissionId != missionId)
                    {
                        continue;
                    }
                    string ctmId = ((string)ctm["id"]).Split('-')[1];
                    // ParentMission ID
                    cellsParam[ctmStartRowIndex, 3].Value = missionId;

                    // CTM ID
                    cellsParam[ctmStartRowIndex, 4].Value = ctmId;
                    ctmIdResults.Add(ctmId);

                    // CTM Name
                    string suffix = getCtmSuffix(ctmIdResults, ctmId);
                    cellsParam[ctmStartRowIndex, 5].Value = AisUtil.GetCatalogLangNew(ctm) + suffix;

                    ++ctmStartRowIndex;
                }

                startRowIndex = ctmStartRowIndex;

                // 4. Pass the Element parameter
                JArray ctmAndEleArray = JArray.Parse(missionToken["elements"].ToString());
                foreach (JToken eleCtms in ctmAndEleArray)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }
                    string cmtId = ((string)eleCtms["id"]).Split('-')[1];

                    JArray eleGroupArray = JArray.Parse(eleCtms["children"].ToString());
                    foreach (JToken group in eleGroupArray)
                    {
                        //AIS_DAC No.76 sunyi 2018/12/05 start
                        //group["children"]がNullの場合、チェックする
                        if (group["children"] == null)
                        {
                            //AISMM-94 sunyi 2019/03/27 start
                            //Groupがないエレメントをセット
                            object checkedFlag = group["checkedFlag"];
                            if (Convert.ToBoolean(checkedFlag) == false)
                            {
                                continue;
                            }

                            string eleMissionId = ((string)group["id"]).Split('-')[0];
                            if (eleMissionId != missionId)
                            {
                                continue;
                            }
                            string eleId = ((string)group["id"]).Split('-')[1];

                            // ParentCTM ID
                            cellsParam[eleStartRowIndex, 6].Value = cmtId;

                            // Element ID
                            cellsParam[eleStartRowIndex, 7].Value = eleId;

                            // Element Name
                            cellsParam[eleStartRowIndex, 8].Value = AisUtil.GetCatalogLangNew(group);

                            // Additional : ParentMission ID -> For deleting CTM Element in case there is duplicate CTM
                            cellsParam[eleStartRowIndex, 9].Value = missionId;

                            ++eleStartRowIndex;
                            //AISMM-94 sunyi 2019/03/27 end
                            continue;
                        }
                        //AIS_DAC No.76 sunyi 2018/12/05 end
                        JArray eleArray = JArray.Parse(group["children"].ToString());
                        foreach (JToken ele in eleArray)
                        {
                            object checkedFlag = ele["checkedFlag"];
                            if (Convert.ToBoolean(checkedFlag) == false)
                            {
                                continue;
                            }

                            string eleMissionId = ((string)ele["id"]).Split('-')[0];
                            if (eleMissionId != missionId)
                            {
                                continue;
                            }
                            string eleId = ((string)ele["id"]).Split('-')[1];

                            // ParentCTM ID
                            cellsParam[eleStartRowIndex, 6].Value = cmtId;

                            // Element ID
                            cellsParam[eleStartRowIndex, 7].Value = eleId;

                            // Element Name
                            cellsParam[eleStartRowIndex, 8].Value = AisUtil.GetCatalogLangNew(ele);

                            // Additional : ParentMission ID -> For deleting CTM Element in case there is duplicate CTM
                            cellsParam[eleStartRowIndex, 9].Value = missionId;

                            ++eleStartRowIndex;
                        }
                    }

                }
            }

            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        private void writeConfigSheetMissionInfoGrip(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();


            if (resultArray.Count == 0) return;
            int index = 1;
            foreach (JToken missionToken in resultArray)
            {
                if (0 == int.Parse((string)missionToken["missionNewType"]))
                {
                    if (string.IsNullOrEmpty((string)missionToken["resultFolder"])) continue;

                    cellsParam[index, 1].value = missionToken["id"];
                    cellsParam[index, 2].value = AisUtil.GetCatalogLangNew(missionToken);
                    //string aisSearchType = (string)missionToken["aisSearchType"];
                    //if (1 == int.Parse(aisSearchType))
                    //{
                    //    cellsParam[index, 3].value = 1;
                    //    cellsParam[index, 4].value = missionToken["onlineTime"];
                    //    cellsParam[index, 5].value = missionToken["onlinePeriod"];
                    //}
                    //else
                    //{
                    //    cellsParam[index, 3].value = 2;
                    //    cellsParam[index, 4].value = missionToken["offlineStart"];
                    //    cellsParam[index, 5].value = missionToken["offlineEnd"];
                    //}
                    index++;
                }
            }

            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }
		// FOA_サーバー化開発 Processing On Server Xj 2018/08/17 End

        //ISSUE_NO.727 sunyi 2018/06/14 Start
        //grip mission処理ができるように修正にする
        private void writeConfigSheetMissionInfo(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();
            cellsParam[1, 1].value = this.gripMission.Id;
            cellsParam[1, 2].value = AisUtil.GetCatalogLang(this.gripMission);
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        private void writeConfigSheetRoute(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, string folderPath, JArray jarryCtms = null)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            var writer = new GripResultSheetWriter(this.Mode, this.Ctms, this.dtOutput, this.bw);

            // シート検索 / 作成
            worksheetParam.Rows.Clear();

            DirectoryInfo di = new DirectoryInfo(folderPath);

            Dictionary<string, List<string>> routes = new Dictionary<string, List<string>>();

            // 2. Pass the Mission parameter
            int fileIndex = 1;
            int IsCtmIndex = 0;
            int elIndex = 0;
            int RouteIndexRow = 1;
            CtmObject ctmObject = null;
            for (fileIndex = 1; fileIndex <= di.GetFiles().Count(); fileIndex = fileIndex + 2)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                string routeName = System.IO.Path.GetFileNameWithoutExtension(di.GetFiles()[fileIndex].FullName);

                //MM_No.118 sun 20190726 start
                JArray jArrayCtmTmp = new JArray();
                if (jarryCtms != null)
                {
					foreach (var ctm in jarryCtms)
                    {
                        JToken token = ctm as JToken;
                        if (token["checkedFlag"].ToString().Equals("True"))
                        {
                            jArrayCtmTmp.Add(ctm);
                        }
                    }

                    bool isChecked = false;
                    foreach (var ctmTmp in jArrayCtmTmp)
                    {
                        JToken token = ctmTmp as JToken;
                        if (token["name"] == null) continue;
                        string[] rowsArray = token["name"].ToString().Split('_');
                        string name = rowsArray[1] + "_" + rowsArray[2];
                        if (routeName.Contains(name))
                        {
                            isChecked = true;
                            break;
                        }
                    }
                    if (!isChecked)
                    {
                        continue;
                    }
                }
                //MM_No.118 sun 20190726 end

                //write routeName
                cellsParam[RouteIndexRow, 1].value = routeName;

                if (di.GetFiles()[fileIndex].Extension.ToUpper() == ".H")
                {
                    var reader = new CsvReader(new StreamReader(di.GetFiles()[fileIndex].FullName, Encoding.GetEncoding("Shift_JIS")));

                    reader.Read();
                    var headersIsCtm = reader.FieldHeaders;

                    List<string> rtPosList = new List<string>();

                    //get ctm name header index
                    for (IsCtmIndex = 0; IsCtmIndex < headersIsCtm.Count(); IsCtmIndex++)
                    {
                        rtPosList.Add(headersIsCtm[IsCtmIndex]);
                    }

                    var readerAllName = new CsvReader(new StreamReader(di.GetFiles()[fileIndex - 1].FullName, Encoding.GetEncoding("Shift_JIS")));
                    readerAllName.Read();

                    var headerAllName = readerAllName.FieldHeaders;

                    for (elIndex = 0; elIndex < headerAllName.Count(); elIndex++)
                    {
                        if (rtPosList.Contains((elIndex+1).ToString()))
                        {
                            //write ctm
                            string ctmName = headerAllName[elIndex];
							string ctmId = string.Empty;
							ctmObject = null;
                            foreach (var tCtmObject in this.ctmElementDic.Keys)
                            {
								if (tCtmObject.displayName.ContainsValue(ctmName))
								{
									ctmId = tCtmObject.id.ToString();
									ctmObject = tCtmObject;
									break;
								}
							}
                            cellsParam[RouteIndexRow + elIndex, 2].value = ctmName;
                            cellsParam[RouteIndexRow + elIndex, 3].value = ctmId;
                            RouteIndexRow = RouteIndexRow - 1;
                        }
                        else
                        {
                            //DAC-82 sunyi 20181225 start
                            //エレメント欄に表示するのは、CTMのエレメントのみ,受信時刻はエレメントではないので表示しない
                            if (headerAllName[elIndex].Contains("受信時刻_")) continue;
                            //DAC-82 sunyi 20181225 end
                            //write el
                            string elementName = headerAllName[elIndex];
							string originalElementName = elementName;
							List<string> elementNameItems = elementName.Split('_').ToList();
							if (elementNameItems.Count > 1)
							{
								string lastElementNameItem = elementNameItems[elementNameItems.Count - 1];
								int lastElementNameIndex = -1;

								if (int.TryParse(lastElementNameItem, out lastElementNameIndex) == true)
								{
									elementNameItems.RemoveAt(elementNameItems.Count - 1);
									originalElementName = string.Join("_", elementNameItems);
								}
							}

							string elementId = string.Empty;
							if (ctmObject != null)
							{
								foreach (CtmElement tElementObject in this.ctmElementDic[ctmObject])
								{
									if (tElementObject.displayName.ContainsValue(originalElementName))
									{
										elementId = tElementObject.id.ToString();
										break;
									}
								}
							}
                            cellsParam[RouteIndexRow + elIndex, 4].value = elementName;
                            cellsParam[RouteIndexRow + elIndex, 5].value = elementId;
                        }
                    }

                    RouteIndexRow = RouteIndexRow - 1 + headerAllName.Count();
                }
            }
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }
        //ISSUE_NO.727 sunyi 2018/06/14 End

        private void writeConfigSheetCtm2(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();

            // 2. Pass the Mission parameter
            int startRowIndex = 1;
            int eleStartRowIndex = 1;

            // 3. Pass the CTM parameter
            int ctmStartRowIndex = startRowIndex;
            foreach (CtmObject ctm in this.missionCtmElementDict.Keys)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // CTM ID
                cellsParam[ctmStartRowIndex, 4].Value = ctm.id.ToString();

                // CTM Name
                cellsParam[ctmStartRowIndex, 5].Value = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                // 4. Pass the Element parameter
                foreach (CtmElement ele in missionCtmElementDict[ctm])
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // ParentCTM ID
                    cellsParam[eleStartRowIndex, 6].Value = ctm.id.ToString();

                    // Element ID
                    cellsParam[eleStartRowIndex, 7].Value = ele.id.ToString();

                    // Element Name
                    cellsParam[eleStartRowIndex, 8].Value = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    ++eleStartRowIndex;
                }

                ++ctmStartRowIndex;
            }

            startRowIndex = ctmStartRowIndex;
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        protected virtual void WriteConfigSheetParam(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            var range = worksheetParam.get_Range("A1", "J100");

            // 登録フォルダ
            var range2 = AisUtil.GetFindRange_Interop(range, "登録フォルダ");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.RegistryFolderPath;
            }

            // 登録ファイル
            range2 = AisUtil.GetFindRange_Interop(range, "登録ファイル");
            if (range2 != null)
            {
                range2[1, 2].Value = GetStringVal(configParams, ExcelConfigParam.REGISTERED_FILENAME, string.Empty);
            }

            // サーバIPアドレス
            range2 = AisUtil.GetFindRange_Interop(range, "サーバIPアドレス");
            {
                range2[1, 2].Value = AisConf.Config.CmsHost;
            }

            // サーバポート番号
            range2 = AisUtil.GetFindRange_Interop(range, "サーバポート番号");
            {
                range2[1, 2].Value = AisConf.Config.CmsPort;
            }

            // GRIPサーバIPアドレス
            range2 = AisUtil.GetFindRange_Interop(range, "GRIPサーバIPアドレス");
            {
                range2[1, 2].Value = AisConf.Config.GripServerHost;
            }

            // サーバポート番号
            range2 = AisUtil.GetFindRange_Interop(range, "GRIPサーバポート番号");
            {
                range2[1, 2].Value = AisConf.Config.GripServerPort;
            }

            // テンプレート名称
            range2 = AisUtil.GetFindRange_Interop(range, "テンプレート名称");
            {
                //foastudi AisAddin sunyi 2018/11/02
                //階層ステータスモニター ⇒ マルチモニタ
                range2[1, 2].Value = "マルチモニタ";
            }

            // 更新周期
            range2 = AisUtil.GetFindRange_Interop(range, "更新周期");
            {
                range2[1, 2].Value = GetIntVal(configParams, ExcelConfigParam.RELOAD_INTERVAL, 60);
            }

            // 取得期間
            range2 = AisUtil.GetFindRange_Interop(range, "取得期間");
            {
                range2[1, 2].Value = GetFloatVal(configParams, ExcelConfigParam.GET_PERIOD, 0);
            }

            // 収集開始日時
            range2 = AisUtil.GetFindRange_Interop(range, "収集開始日時");
            {
                //ISSUE_NO.805 Sunyi 2018/07/27 Start
                //DataTimeからExcelに出力する時、millisecondを出力しない
                //range2[1, 2].Value = GetDateTimeVal(configParams, ExcelConfigParam.DISPLAY_START, DateTime.MinValue);
                range2[1, 2].Value = (GetDateTimeVal(configParams, ExcelConfigParam.DISPLAY_START, DateTime.MinValue)).ToString("yyyy/MM/dd HH:mm:ss");
                //ISSUE_NO.805 Sunyi 2018/07/27 End
            }

            // 収集終了日時
            range2 = AisUtil.GetFindRange_Interop(range, "収集終了日時");
            {
                //ISSUE_NO.805 Sunyi 2018/07/27 Start
                //DataTimeからExcelに出力する時、millisecondを出力しない
                //range2[1, 2].Value = GetDateTimeVal(configParams, ExcelConfigParam.DISPLAY_END, DateTime.MinValue);
                range2[1, 2].Value = GetDateTimeVal(configParams, ExcelConfigParam.DISPLAY_END, DateTime.MinValue).ToString("yyyy/MM/dd HH:mm:ss");
                //ISSUE_NO.805 Sunyi 2018/07/27 End
            }

            // 検索タイプ
            range2 = AisUtil.GetFindRange_Interop(range, "検索タイプ");
            {
                // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 Start
                //range2[1, 2].Value = GetDateTimeVal(configParams, ExcelConfigParam.DISPLAY_END, DateTime.MinValue) > DateTime.MinValue ? "ON" : "OFF";
                //range2[1, 2].Value = GetIntVal(configParams, ExcelConfigParam.RELOAD_INTERVAL, 0) == 0 ? "ON" : "OFF";
                range2[1, 2].Value =  "ON";
                // FOA_サーバー化開発 Processing On Server Dcs 2018/08/31 End
            }

            // ミッションID
            range2 = AisUtil.GetFindRange_Interop(range, "ミッションID");
            if (range2 != null)
            {
                range2[1, 2].Value = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, string.Empty);
            }

            // 製作者名
            range2 = AisUtil.GetFindRange_Interop(range, "製作者名");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.UserId;
            }

            // 収集タイプ
            range2 = AisUtil.GetFindRange_Interop(range, "収集タイプ");
            if (range2 != null)
            {
                switch (this.dataSource)
                {
                    case DataSourceType.CTM_DIRECT:
                        range2[1, 2].Value = "CTM";
                        break;
                    case DataSourceType.MISSION:
                        range2[1, 2].Value = "ミッション";
                        break;
                    case DataSourceType.GRIP:
                        range2[1, 2].Value = "GRIP";
                        break;
                    default:
                        range2[1, 2].Value = "";
                        break;
                }
            }

            // for KMEW
            range2 = AisUtil.GetFindRange_Interop(range, "CmsVersion");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.CmsVersion;
            }
            if (AisConf.Config.CmsVersion == "3.5")
            {
                var ahInfo = AisUtil.GetAhInfo(this.Ctms[0].id.ToString());

                range2 = AisUtil.GetFindRange_Interop(range, "MfHost");
                if (range2 != null)
                {
                    range2[1, 2].Value = ahInfo.Mf;
                }
                range2 = AisUtil.GetFindRange_Interop(range, "MfPort");
                if (range2 != null)
                {
                    range2[1, 2].Value = ahInfo.MfPort;
                }
            }

            //BugNo.546 Added for display bug. paramに編集モード項目追加.
            // 編集モード
            range2 = AisUtil.GetFindRange_Interop(range, "編集モード");
            if (range2 != null)
            {
                range2[1, 2].Value = GetStringVal(configParams, ExcelConfigParam.EDIT_MODE, string.Empty);
            }

            // ProxyServer使用
            range2 = AisUtil.GetFindRange_Interop(range, "ProxyServer使用");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.UseProxy;
            }

            // ProxyServerURI
            range2 = AisUtil.GetFindRange_Interop(range, "ProxyServerURI");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.ProxyURI;
            }

            // IsFirstSave
            range2 = AisUtil.GetFindRange_Interop(range, "IsFirstSave");
            if (range2 != null)
            {
                range2[1, 2].Value = "";
            }


            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/17 Start
        public void WriteCtmDataForAll(string excelName, Dictionary<ExcelConfigParam, object> configParams,JArray jarryCtms = null, List<string>ExternalFileList = null)
        {
            logger.Debug(MethodBase.GetCurrentMethod().Name + "() START. excelName:[" + excelName + "] configParams:" + configParams.Count);

            string folderPath = null;

            Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
            Microsoft.Office.Interop.Excel.Workbooks oWBooks = null;
            Microsoft.Office.Interop.Excel.Workbook oWBook = null;

#if true
            try
            {
                bool failedOpen = false;
                try
                {
                    oXls = new Microsoft.Office.Interop.Excel.Application();
                    oXls.Visible = true; //確認のためエクセルのウィンドウを表示する

                    //エクセルファイルをオープンする
                    oWBooks = oXls.Workbooks;
                    oWBook = oWBooks.Open(excelName); // オープンするExcelファイル名
                }
#pragma warning disable
                catch (Exception err)
#pragma warning restore
                {
                    failedOpen = true;
                    FoaMessageBox.ShowError("AIS_E_074", excelName);
                }
                if (failedOpen == true)
                {
                    logger.Debug(MethodBase.GetCurrentMethod().Name + "() FAILED_OPEN_EXCEL_FILE. excelName:[" + excelName + "] configParams:" + configParams.Count);
                    return;
                }

                Microsoft.Office.Interop.Excel.Worksheet wsUserInfo = AisUtil.GetSheetFromSheetName_Interop(oWBook, "USERINFO");
                writeUserInfoSheet(wsUserInfo);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 1. Set Mission CTM & Element parameter
                string paramSheetNameM_Mission = "ミッション_MISSION";
                string paramSheetNameM_Grip = "ミッション_GRIP";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamM_Mission = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameM_Mission);
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamM_Grip = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameM_Grip);

                // CTM MISSION
                writeConfigSheetCtmNew(worksheetParamM_Mission);
                // GRIP MISSION
                writeConfigSheetMissionInfoGrip(worksheetParamM_Grip);

                //AIS_DAC No.80 sunyi 20181210 start
                //// 既存GripMissionに関する内容をクリア
                //// Processing On Server dn 内容修正場合表示データない、Gripに関するシートを削除修正　start
                //foreach (JToken missionToken in resultArray)
                //{
                //    folderPath = (string)missionToken["resultFolder"];
                //    if (0 == int.Parse((string)missionToken["missionNewType"]) && string.IsNullOrEmpty(folderPath))
                //    {

                //        foreach (Microsoft.Office.Interop.Excel.Worksheet gripSheet in oWBook.Sheets)
                //        {
                //            if (gripSheet.Name.IndexOf("Route") != -1)
                //            {
                //                oXls.DisplayAlerts = false;
                //                gripSheet.Delete();
                //                oXls.DisplayAlerts = true;
                //            }

                //        }
                //    }
                //}
                //// Processing On Server dn 内容修正場合表示データない、Gripに関するシートを削除修正　end
                foreach (Microsoft.Office.Interop.Excel.Worksheet gripSheet in oWBook.Sheets)
                {
                    if (gripSheet.Name.IndexOf("_Routes") != -1)
                    {
                        oXls.DisplayAlerts = false;
                        gripSheet.Delete();
                        oXls.DisplayAlerts = true;
                    }
                }
                //AIS_DAC No.80 sunyi 20181210 end

                int routeIndex = 0;
                foreach (JToken missionToken in resultArray)
                {
                    folderPath = (string)missionToken["resultFolder"];

                    if (string.IsNullOrEmpty(folderPath))
                    {
                        continue;
                    }


                    if (0 == int.Parse((string)missionToken["missionNewType"]))
                    {
                        //ISSUE_NO.727 sunyi 2018/06/14 Start
                        //grip mission処理ができるように修正にする
                        routeIndex = int.Parse((string)missionToken["routeIndex"]);
                        string paramSheetNameR = routeIndex + "_Routes";
                        Microsoft.Office.Interop.Excel.Worksheet worksheetParamR = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameR);
                        writeConfigSheetRoute(worksheetParamR, folderPath, jarryCtms);
                        //ISSUE_NO.727 sunyi 2018/06/14 End
                    }
                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 2. Set Start & End Time parameter
                string paramSheetNameC = "業務時間";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamC = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameC);
                writeConfigSheetTime(worksheetParamC, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                string paramSheetName = DAC.Util.Keywords.PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetParam3 = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetName);
                WriteConfigSheetParam(worksheetParam3, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = DAC.Util.Keywords.RO_PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_Interop(oWBook, roParamSheetName);
                if (this.Mode == ControlMode.MultiStatusMonitor)
                {
                    MultiWriteRoParamSheet_Interop(worksheetRoParam, this.onlineId, this.workplaceId);
                }
                else
                {
                    WriteRoParamSheet_Interop(worksheetRoParam);
                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                bool isFirstWrite = true;
                foreach (JToken missionToken in resultArray)
                {

                    if (1 == int.Parse((string)missionToken["missionNewType"]))
                    {
                        folderPath = (string)missionToken["rootFolder"];
                        if (isFirstWrite)
                        {
                            // 初回ミッション結果を出力
                            DirectoryInfo root = new DirectoryInfo(folderPath);
                            if (root.Exists)
                            {
                                writeData3(oXls, oWBook, root);
                            }
                            isFirstWrite = false;
                        }
                    }
                    else
                    {
                        folderPath = (string)missionToken["resultFolder"];
                        if (string.IsNullOrEmpty(folderPath)) continue;
                        writeGripResultSheets(oWBook, folderPath, jarryCtms);
                    }

                }

                if(ExternalFileList != null)
                {
                    foreach (string filepath in ExternalFileList)
                    {
                        if (File.Exists(filepath))
                        {
                            StreamReader tmpStreamReader = new StreamReader(filepath, Encoding.UTF8);
                            var reader = new CsvReader(tmpStreamReader);
                            reader.Configuration.HasHeaderRecord = false;

                            string sheetname = Path.GetFileNameWithoutExtension(filepath);
                            oWBook.Sheets.Add(Missing.Value,oWBook.Sheets[oWBook.Sheets.Count]);
                            oWBook.Sheets[oWBook.Sheets.Count].name = sheetname;
                            Microsoft.Office.Interop.Excel.Worksheet worksheet = oWBook.Sheets[oWBook.Sheets.Count];
                            Microsoft.Office.Interop.Excel.Range range = worksheet.Cells;
                            int rowindex = 1;
                            while (reader.Read())
                            {
                                var record = reader.CurrentRecord;
                                for (int i = 1; i <= record.Length; i++)
                                {
                                    Microsoft.Office.Interop.Excel.Range cell = worksheet.Cells[rowindex, i];
                                    cell.Value = record[i - 1];
                                }
                                rowindex++;
                            }
                            tmpStreamReader.Close();
                            oWBook.Save();                           
                        }
                    }
                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 3. Save the sheet with the parameter
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Save();
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Close();
                    return;
                }

                //// 4. Re-open again with the newly inputted parameter sheet
                //oWBook = (Microsoft.Office.Interop.Excel.Workbook)(oXls.Workbooks.Open(excelName));
                oXls.Visible = true; //確認のためエクセルのウィンドウを表示する
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Activate();
                ((Microsoft.Office.Interop.Excel.Worksheet)(((Microsoft.Office.Interop.Excel._Workbook)oWBook).Sheets[1])).Select();
                //最小化→正常表示
                oXls.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlNormal;
                //EXCEL最前面へ移動
                ExcelUtil.OpenExcelFile(oXls);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                logger.Debug(MethodBase.GetCurrentMethod().Name + "() start oXls.Run(\"MakeMenuBar\");");
                oXls.Run("MakeMenuBar");
                logger.Debug(MethodBase.GetCurrentMethod().Name + "() end oXls.Run(\"MakeMenuBar\");");

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
            catch (Exception err)
            {
                // ログ出力
                System.Windows.MessageBox.Show("ExcelOpen Exception Error : FileName[" + excelName + "] \n" + err.ToString());
            }
            finally
            {
                if (oWBook != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                    oWBook = null;
                }

                if (oWBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBooks);
                    oWBooks = null;
                }

                if (oXls != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                    oXls = null;
                }
            }
#else
            bool failedOpen = false;
            try
            {
                oXls = new Microsoft.Office.Interop.Excel.Application();
                oXls.Visible = false; //確認のためエクセルのウィンドウを表示する

                //エクセルファイルをオープンする
                oWBooks = oXls.Workbooks;
                oWBook = oWBooks.Open(excelName); // オープンするExcelファイル名
            }
            catch (Exception err)
            {
                failedOpen = true;
                // ログ出力
                System.Windows.MessageBox.Show("ExcelOpen Exception Error : FileName[" + excelName + "] \n" + err.ToString());
                //FoaMessageBox.ShowError("AIS_E_074", excelName);
            }
            finally
            {
                if (oWBook != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                    oWBook = null;
                }

                if (oWBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBooks);
                    oWBooks = null;
                }

                if (oXls != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                    oXls = null;
                }
            }
            if (failedOpen == true)
            {
                logger.Debug(MethodBase.GetCurrentMethod().Name + "() FAILED_OPEN_EXCEL_FILE. excelName:[" + excelName + "] configParams:" + configParams.Count);
                return;
            }

            bool failedWriteUserInfo = false;
            try
            {
                Microsoft.Office.Interop.Excel.Worksheet wsUserInfo = AisUtil.GetSheetFromSheetName_Interop(oWBook, "USERINFO");
                writeUserInfoSheet(wsUserInfo);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
            catch (Exception err)
            {
                failedWriteUserInfo = true;
                // ログ出力
                System.Windows.MessageBox.Show("ExcelOpen Exception Error : FileName[" + excelName + "] \n" + err.ToString());
                //FoaMessageBox.ShowError("AIS_E_075", excelName);
            }
            finally
            {
                if (oWBook != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                    oWBook = null;
                }

                if (oWBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBooks);
                    oWBooks = null;
                }

                if (oXls != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                    oXls = null;
                }
            }
            if (failedWriteUserInfo == true)
            {
                logger.Debug(MethodBase.GetCurrentMethod().Name + "() FAILED_WRITE_USERINFO. excelName:[" + excelName + "] configParams:" + configParams.Count);
                return;
            }


            try
            {
                // 1. Set Mission CTM & Element parameter
                string paramSheetNameM_Mission = "ミッション_MISSION";
                string paramSheetNameM_Grip = "ミッション_GRIP";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamM_Mission = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameM_Mission);
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamM_Grip = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameM_Grip);

                // CTM MISSION
                writeConfigSheetCtmNew(worksheetParamM_Mission);
                // GRIP MISSION
                writeConfigSheetMissionInfoGrip(worksheetParamM_Grip);

                //AIS_DAC No.80 sunyi 20181210 start
                //// 既存GripMissionに関する内容をクリア
                //// Processing On Server dn 内容修正場合表示データない、Gripに関するシートを削除修正　start
                //foreach (JToken missionToken in resultArray)
                //{
                //    folderPath = (string)missionToken["resultFolder"];
                //    if (0 == int.Parse((string)missionToken["missionNewType"]) && string.IsNullOrEmpty(folderPath))
                //    {

                //        foreach (Microsoft.Office.Interop.Excel.Worksheet gripSheet in oWBook.Sheets)
                //        {
                //            if (gripSheet.Name.IndexOf("Route") != -1)
                //            {
                //                oXls.DisplayAlerts = false;
                //                gripSheet.Delete();
                //                oXls.DisplayAlerts = true;
                //            }

                //        }
                //    }
                //}
                //// Processing On Server dn 内容修正場合表示データない、Gripに関するシートを削除修正　end
                foreach (Microsoft.Office.Interop.Excel.Worksheet gripSheet in oWBook.Sheets)
                {
                    if (gripSheet.Name.IndexOf("_Routes") != -1)
                    {
                        oXls.DisplayAlerts = false;
                        gripSheet.Delete();
                        oXls.DisplayAlerts = true;
                    }
                }
                //AIS_DAC No.80 sunyi 20181210 end

                int routeIndex = 0;
                foreach (JToken missionToken in resultArray)
                {
                    folderPath = (string)missionToken["resultFolder"];

                    if (string.IsNullOrEmpty(folderPath))
                    {
                        continue;
                    }


                    if (0 == int.Parse((string)missionToken["missionNewType"]))
                    {
                        //ISSUE_NO.727 sunyi 2018/06/14 Start
                        //grip mission処理ができるように修正にする
                        routeIndex = int.Parse((string)missionToken["routeIndex"]);
                        string paramSheetNameR = routeIndex + "_Routes";
                        Microsoft.Office.Interop.Excel.Worksheet worksheetParamR = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameR);
                        writeConfigSheetRoute(worksheetParamR, folderPath,jarryCtms);
                        //ISSUE_NO.727 sunyi 2018/06/14 End
                    }
                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 2. Set Start & End Time parameter
                string paramSheetNameC = "業務時間";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamC = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameC);
                writeConfigSheetTime(worksheetParamC, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                string paramSheetName = AIS.Util.Keywords.PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetParam3 = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetName);
                WriteConfigSheetParam(worksheetParam3, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = AIS.Util.Keywords.RO_PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_Interop(oWBook, roParamSheetName);
                if (this.Mode == ControlMode.MultiStatusMonitor)
                {
                    MultiWriteRoParamSheet_Interop(worksheetRoParam, this.onlineId, this.workplaceId);
                }
                else
                {
                    WriteRoParamSheet_Interop(worksheetRoParam);
                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                bool isFirstWrite = true;
                foreach (JToken missionToken in resultArray)
                {

                    if (1 == int.Parse((string)missionToken["missionNewType"]))
                    {
                        folderPath = (string)missionToken["rootFolder"];
                        if (isFirstWrite)
                        {
                            // 初回ミッション結果を出力
                            DirectoryInfo root = new DirectoryInfo(folderPath);
                            if (root.Exists)
                            {
                                writeData3(oXls, oWBook, root);
                            }
                            isFirstWrite = false;
                        }
                    }
                    else
                    {
                        folderPath = (string)missionToken["resultFolder"];
                        if (string.IsNullOrEmpty(folderPath)) continue;
                        writeGripResultSheets(oWBook, folderPath, jarryCtms);
                    }

                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 3. Save the sheet with the parameter
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Save();
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Close();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                //// 4. Re-open again with the newly inputted parameter sheet
                oWBook = (Microsoft.Office.Interop.Excel.Workbook)(oXls.Workbooks.Open(excelName));
                oXls.Visible = true; //確認のためエクセルのウィンドウを表示する
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Activate();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                logger.Debug(MethodBase.GetCurrentMethod().Name + "() start oXls.Run(\"MakeMenuBar\");");
                oXls.Run("MakeMenuBar");
                logger.Debug(MethodBase.GetCurrentMethod().Name + "() end oXls.Run(\"MakeMenuBar\");");

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
            catch (Exception err)
            {
                // ログ出力
                System.Windows.MessageBox.Show("ExcelOpen Exception Error : FileName[" + excelName + "] \n" + err.ToString());
            }
            finally
            {
                if (oWBook != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                    oWBook = null;
                }

                if (oWBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBooks);
                    oWBooks = null;
                }

                if (oXls != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                    oXls = null;
                }
            }
#endif
            logger.Debug(MethodBase.GetCurrentMethod().Name + "() END. excelName:[" + excelName + "] configParams:" + configParams.Count);
        }
        // FOA_サーバー化開発 Processing On Server Xj 2018/08/17 End
        // FOA_サーバー化開発 Processing On Server Xj 2018/08/17 Start
        protected void writeData3(Microsoft.Office.Interop.Excel.Application excel, Microsoft.Office.Interop.Excel.Workbook book, DirectoryInfo root)
        {
            Dictionary<CtmObject, DataRow> dicDataRows = convertToDatRowFromDictionaryNew(this.ctmElementDic);
            List<CtmObject> ctms = convertToListFromDictionary(this.ctmElementDic);

            List<string> ctmIdResults = new List<string>();
            List<XYZ> srcCsvFiles = null;
            try
            {
                foreach (var ctm in ctms)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var ctmId = ctm.id.ToString();
                    ctmIdResults.Add(ctmId);

                    var missionId = ctm.missionId;
                    var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    string suffix = getCtmSuffix(ctmIdResults, ctmId);
                    var ctmSheetName = ctmName + suffix;
                    Microsoft.Office.Interop.Excel.Worksheet sheet = AisUtil.GetSheetFromSheetName_Interop(book, ctmSheetName);
                    sheet.Rows.Clear();

                    DataRow dataRow = null;
                    foreach (var itemRows in dicDataRows)
                    {
                        if (missionId == itemRows.Key.missionId && ctmName == itemRows.Value.ItemArray[0].ToString())
                        {
                            dataRow = itemRows.Value;
                            break;
                        }
                    }

                    if (dataRow == null)
                    {
                        // CTM自体の選択が無かったので無視
                        continue;
                    }

                    //AISMM-92 sunyi 20190227 start
                    foreach (var uuidAndMissionId in uuidAndMissionIds)
                    {
                        if(missionId.Equals(uuidAndMissionId.Value))
                        {
                            CsvFile csvFileObj = null;
                            //AISMM-92 sunyi 20190227 end
                            foreach (DirectoryInfo di in root.GetDirectories())
                            {
                                if (di.ToString().Equals(uuidAndMissionId.Key))
                                {
                                    srcCsvFiles = new List<XYZ>();
                                    // シート検索 / 作成
                                    //AISMM-92 sunyi 20190227 start
                                    //違うミッションの同じctmを区別する
                                    //string ctmSheetName = ctmName + (count == 1 ? string.Empty : string.Format("({0})", count));
                                    //count++;
                                    //Microsoft.Office.Interop.Excel.Worksheet sheet = AisUtil.GetSheetFromSheetName_Interop(book, ctmSheetName);
                                    //sheet.Rows.Clear();
                                    //AISMM-92 sunyi 20190227 end
                                    string srcCsvPath = Path.Combine(di.FullName, ctmId + ".csv");
                                    if (File.Exists(srcCsvPath))
                                    {
                                        csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcCsvPath };
                                        var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                                        srcCsvFiles.Add(srcCsvFile);
                                    }
                                    else
                                    {
                                        csvFileObj = null;
                                    }
                                    //AISMM-92 sunyi 20190227 start
                                    writeByCtmResultSheet(sheet, csvFileObj, ctm, dataRow, this.bw);
                                    // 次のシートへ
                                    //sheet.UsedRange.Columns.AutoFit();
                                    sheet.UsedRange.ColumnWidth = 16.00;
                                    //AISMM-92 sunyi 20190227 end
                                }
                            }
                        }
                    }

                    /*
                    var srcFilepath = Path.Combine(folderPath, ctmId + ".csv");
                    if (File.Exists(srcFilepath))
                    {
                        csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcFilepath };
                        var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                        srcCsvFiles.Add(srcCsvFile);
                    }

                    writeByCtmResultSheet(sheet, csvFileObj, ctm, dataRow, this.bw);
                    */
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }
                }
            }
            finally
            {
                if (srcCsvFiles != null)
                {
                    foreach (XYZ srcCsvFile in srcCsvFiles)
                    {
                        srcCsvFile.Dispose();
                    }
                }
            }
        }


        // FOA_サーバー化開発 Processing On Server Xj 2018/08/17 End


        private static readonly ILog logger = LogManager.GetLogger(typeof(InteropExcelWriterMultiStatusMonitor));
    }
}
