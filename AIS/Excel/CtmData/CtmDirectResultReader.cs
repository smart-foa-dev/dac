﻿using AIS.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace AIS.CtmData
{
    class CtmDirectResultReader : IEnumerable<ProgramMission.Ctms>
    {
        private StreamReader streamReader;

        public CtmDirectResultReader(StreamReader sr)
        {
            this.streamReader = sr;
        }

        public IEnumerator<ProgramMission.Ctms> GetEnumerator()
        {
            JsonTextReader jtr = new JsonTextReader(streamReader);
            return new CtmDirectResultEnumerator(jtr);
        }

        public IEnumerator<ProgramMission.Ctms> GetEnumerator(bool forKmew)
        {
            if (forKmew)
            {
                JsonTextReader jtr = new JsonTextReader(streamReader);
                return new CtmDirectResultEnumeratorKmew(jtr);
            }
            else
            {
                return GetEnumerator();
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public class CtmDirectResultEnumerator : IEnumerator<ProgramMission.Ctms>
        {
            private JsonTextReader reader;
            private string currentId;
            private string currentName;
            private int currentNum;
            private ProgramMission.Ctms currentRec;

            public CtmDirectResultEnumerator(JsonTextReader jtr)
            {
                this.reader = jtr;
            }

            public ProgramMission.Ctms Current
            {
                get { return currentRec; }
            }

            object System.Collections.IEnumerator.Current
            {
                get { return currentRec; }
            }

            public bool MoveNext()
            {
                while (reader.Read())
                {
                    if (reader.TokenType == JsonToken.StartObject)
                    {
                        JsonSerializer serializer = new JsonSerializer();
                        currentRec = serializer.Deserialize<ProgramMission.Ctms>(reader);
                        return true;
                    }
                }
                return false;

            }

            public void Reset()
            {
                throw new NotImplementedException();
            }

            private bool disposedValue = false;
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposedValue)
                {
                    if (disposing)
                    {
                        // Dispose of managed resources.
                    }
                    currentRec = null;
                    close();
                }

                this.disposedValue = true;
            }

            private void close()
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }
        }

        public class CtmDirectResultEnumeratorKmew : IEnumerator<ProgramMission.Ctms>
        {
            private JsonTextReader reader;
            private string currentId;
            private string currentName;
            private int currentNum;
            private ProgramMission.Ctms currentRec;

            public CtmDirectResultEnumeratorKmew(JsonTextReader jtr)
            {
                this.reader = jtr;
            }

            public ProgramMission.Ctms Current
            {
                get { return currentRec; }
            }

            object System.Collections.IEnumerator.Current
            {
                get { return currentRec; }
            }

            public bool MoveNext()
            {
                while (reader.Read())
                {
                    if (reader.TokenType == JsonToken.String)
                    {
                        var value = (string)reader.Value;
                        currentRec = JsonConvert.DeserializeObject<ProgramMission.Ctms>(value);
                        return true;
                    }
                }

                return false;
            }

            public void Reset()
            {
                throw new NotImplementedException();
            }

            private bool disposedValue = false;
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposedValue)
                {
                    if (disposing)
                    {
                        // Dispose of managed resources.
                    }
                    currentRec = null;
                    close();
                }

                this.disposedValue = true;
            }

            private void close()
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }
        }
    }
}
