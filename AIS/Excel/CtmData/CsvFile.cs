﻿using AIS.Model;
using CsvHelper;
using FoaCore;
using FoaCore.Common.Util;
using System.Collections.Generic;
using System.IO;
using System.Text;
using AIS.Util;
using FoaCore.Common;

namespace AIS.CtmData
{
    public class CsvFile : InputDataFile
    {
        private CtmObject ctmObj;

        private Dictionary<string, Dictionary<string, string>> getCtmsData(List<CtmObject> ctmList)
        {
            var ctmData = new Dictionary<string, Dictionary<string, string>>();

            foreach (CtmObject ctm in ctmList)
            {
                var elementData = new Dictionary<string, string>();
                foreach (AIS.Model.CtmElement element in ctm.GetAllElements())
                {
                    elementData.Add(element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), element.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                }

                ctmData.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), elementData);
            }

            return ctmData;
        }

        public override Dictionary<string, string> GetElementInfo()
        {
            var elementData = new Dictionary<string, string>();

            foreach (CtmChildNode elem in ctmObj.children)
            {
                var elem1 = (CtmElement) elem;
                string name = elem1.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                string unit = elem1.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                elementData.Add(name, unit);
            }

            return elementData;
        }

        public override CtmObject GetCtmObject()
        {
            if (ctmObj == null)
            {
                ctmObj = createCtmObject();
            }

            return ctmObj;
        }

        private CtmObject createCtmObject()
        {
            using (var _reader = new CsvReader(new StreamReader(Filepath, Encoding.UTF8)))
            {
                _reader.Read();

                var header = _reader.FieldHeaders;
                var types = _reader.CurrentRecord;

                _reader.Read();

                var units = _reader.CurrentRecord;
                CtmObject ctm = new CtmObject();
                ctm.id = new GUID(this.Id.ToString());
                ctm.displayName.Put("ja", this.Name);

                for (int i = 0; i < header.Length; i++)
                {
                    string elemName = header[i];

                    if (elemName == Keywords.CTM_NAME || elemName == Keywords.RECEIVE_TIME)
                    {
                        continue;
                    }

                    string type = types[i];
                    string unit = units[i];

                    CtmElement elem = new CtmElement();
                    elem.id = new GUID(Suid.NewID().ToString());
                    elem.displayName.Put("ja", header[i]);
                    elem.datatype = FoaDatatype.Name2Val(type);
                    elem.unit.Put("ja", unit);

                    ctm.children.Add(elem);
                }

                return ctm;
            }
        }
    }
}
