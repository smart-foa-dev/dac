﻿using AIS.Model;
using AIS.Model.Util;
using AIS.Util;
using AIS.View.Helpers;
using CsvHelper;
using FoaCore;
using FoaCore.Common.Util;
using FoaCore.Common.Util.Json;
using FoaCore.Model.GripR2.Search.WebSearch;
using FoaCore.Net;
using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;


namespace AIS.CtmData
{
    public class BulkyDataRetriever : CtmDataRetriever
    {
        public bool isNoCsvOnly = true;
        public bool isNoBulky = true;

        public async Task<string> GetCtmDirectResultCsvAsync(string ctmId, long start, long end, CancellationToken cToken, string elementId)
        {
            AhInfo ahInfo = null;

            // ctmObjsの要素数が1以上であることは保障されている。
            if (AisConf.Config.CmsVersion == "3.5")
            {
                ahInfo = await AisUtil.GetAhInfoAsync(ctmId);
            }

            var folderPath = System.IO.Path.Combine(AisConf.TmpFolderPath, "ctm", Suid.NewID().ToString());
            Directory.CreateDirectory(folderPath);
            var tmpFilepathB = System.IO.Path.Combine(folderPath, elementId + ".csv");
            string url = string.Format("{0}{1}?testing=0", CmsUrl.GetMibBaseUrl(), "mib/ctm/find");

            // CTMを検索する為にPOSTするJSONを生成
            JToken postToken = JToken.Parse("{}");
            postToken["ctmId"] = ctmId;
            postToken["start"] = start;
            postToken["end"] = end;
            if (AisConf.Config.CmsVersion == "3.5")
            {
                postToken["mfHostName"] = ahInfo.Mf;
                postToken["mfPort"] = ahInfo.MfPort;
            }

            HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(100) };

            bool cancelled = false;
            bool failed = false;
            var req = new HttpRequestMessage(HttpMethod.Post, url);
            req.Content = new StringContent(postToken.ToString());

            var task = client.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cToken).ContinueWith(response0 =>
            {
                var tmpFilepath = System.IO.Path.Combine(folderPath, ctmId + ".csv");

                try
                {
                    var response = response0.Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        logger.Error("Failed to get CTM Direct result: " + ctmId + " StatusCode=" + (int)(response.StatusCode));
                        failed = true;
                    }
                    else
                    {
                        using (StreamWriter sw = new StreamWriter(tmpFilepath, false, new UTF8Encoding(false)))
                        using (CsvWriter writer = new CsvWriter(sw))
                        using (FileStream fs = File.Open(tmpFilepathB, FileMode.Create))
                        using (var stream = response.Content.ReadAsStreamAsync().Result)
                        using (var sr = new StreamReader(stream))
                        using (var iterator = new CtmDirectResultReader(sr).GetEnumerator(AisConf.Config.CmsVersion == "3.5"))
                        {
                            if (!iterator.MoveNext())
                            {
                                logger.Info(string.Format("CTM[{0}] is empty.", ctmId));
                            }
                            else
                            {
                                var rec = iterator.Current;

                                // header
                                List<string> keyOrder = new List<string>(rec.EL.Count);

                                writer.WriteField(Keywords.RECEIVE_TIME);
                                foreach (var el in rec.EL.Keys)
                                {
                                    writer.WriteField(el);
                                    keyOrder.Add(el); // For writing records
                                }

                                writer.NextRecord();
                                do
                                {
                                    rec = iterator.Current;

                                    // Records
                                    writer.WriteField<long>(rec.RT);
                                    foreach (var key in keyOrder)
                                    {
                                        var elem = rec.EL[key];
                                        if (elem.T == "100")
                                        {
                                            writer.WriteField(elem.FN);
                                            if (string.IsNullOrEmpty(elem.FN))
                                            {
                                                continue;
                                            }
                                            else if (!Path.GetExtension(elem.FN).ToLower().Equals(".csv"))
                                            {
                                                this.isNoBulky = false;
                                                continue;
                                            }
                                            else
                                            {
                                                this.isNoBulky = false;
                                                this.isNoCsvOnly = false;
                                            }

                                            if (key == elementId && elem.V != null)
                                            {
                                                byte[] bulky = Convert.FromBase64String(elem.V);
                                                byte[] encodedBulky = bulky;
                                                if (AisUtil.IsBomUtf8(bulky))
                                                {
                                                    encodedBulky = AisUtil.RemoveBom(bulky);
                                                }
                                                else
                                                {
                                                    Encoding encoding = AisUtil.GetCode(bulky);
                                                    if (encoding != null)
                                                    {
                                                        if (!encoding.Equals(Encoding.UTF8))
                                                        {
                                                            encodedBulky = AisUtil.ConvertToWoUtf8Bytes(bulky, encoding);
                                                        }
                                                    }
                                                }
                                                fs.Write(encodedBulky, 0, encodedBulky.Length);
                                            }
                                        }
                                        else
                                        {
                                            writer.WriteField(elem.V);
                                        }
                                    }
                                    writer.NextRecord(); // これなしだと直前のWriteFieldが書き込まれない。
                                } while (iterator.MoveNext());
                            }
                        }
                    }
                }
                catch (AggregateException e)
                {
                    Exception ex = e.InnerException;

                    if (ex is TaskCanceledException)
                    {
                        logger.Info("Cancelled by user.");
                        cancelled = true;

                    }
                    else
                    {
                        logger.Error("Failed to get CTM Direct result: " + ctmId, e);
                        failed = true;
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Failed to get CTM Direct result: " + ctmId, e);
                    failed = true;
                }
            });

            await task;

            if (cancelled || failed)
            {
                ClearDirectory(folderPath);
                folderPath = null;

                if (failed)
                {
                    FoaMessageBox.ShowError("AIS_E_055");
                    //string msg = "CTMの取得に失敗しました。";
                    //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            return folderPath;
        }

        public async Task<string> GetMissionResultCsvAsync(string missionId, long start, long end, CancellationToken cancelToken, string ctmId, string elementId)
        {
            string limit = "0";
            string lang = "ja";
            string url = string.Format("{0}{1}?id={2}&start={3}&end={4}&limit={5}&lang={6}", CmsUrl.GetMmsBaseUrl(), "mib/mission/pm", missionId, start, end, limit, lang);

            var folderPath = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission", Suid.NewID().ToString());
            var tmpFilepathB = System.IO.Path.Combine(folderPath, elementId + ".csv");
            Directory.CreateDirectory(folderPath);
            HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(100) };
            bool failed = false;
            bool cancelled = false;

            var task = client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead, cancelToken).ContinueWith(response0 =>
            {
                try
                {
                    var response = response0.Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        logger.Error("Failed to get Mission result: " + missionId + " StatusCode=" + (int)(response.StatusCode));
                        failed = true;
                    }
                    else
                    {
                        using (var swB = File.Open(tmpFilepathB, FileMode.Create))
                        using (var stream = response.Content.ReadAsStreamAsync().Result)
                        using (var sr = new StreamReader(stream))
                        using (var iterator = new MissionResultReader(sr).GetEnumerator())
                        {
                            var tmpFilepath = System.IO.Path.Combine(folderPath, "tmp.csv");
                            StreamWriter sw = null;
                            CsvWriter writer = null;
                            bool wriitenHeader = false;
                            List<string> keyOrder = null;

                            try
                            {
                                while (iterator.MoveNext())
                                {
                                    var rec = iterator.Current;
                                    if (rec == null)
                                    {
                                        var iterator1 = (MissionResultReader.CtmRecordEnumerator)iterator;
                                        var id = iterator1.CurrentId;

                                        if (wriitenHeader)
                                        {
                                            writer.Dispose();
                                            writer = null;
                                            sw.Close();
                                            sw = null;

                                            if (id == ctmId)
                                            {
                                                var targetFilepath = System.IO.Path.Combine(folderPath, id + ".csv");
                                                File.Move(tmpFilepath, targetFilepath);
                                                break;
                                            }
                                            else
                                            {
                                                File.Delete(tmpFilepath);
                                            }
                                            wriitenHeader = false;
                                        }
                                        else
                                        {
                                            logger.Info(string.Format("Mission[{0}] -> CTM[{1} is empty.", missionId, id));
                                        }
                                    }
                                    else
                                    {
                                        // Header
                                        if (!wriitenHeader)
                                        {
                                            sw = new StreamWriter(tmpFilepath, false, new UTF8Encoding(false));
                                            writer = new CsvWriter(sw);
                                            keyOrder = new List<string>(rec.EL.Count);

                                            writer.WriteField(Keywords.RECEIVE_TIME);
                                            foreach (var el in rec.EL.Keys)
                                            {
                                                writer.WriteField(el);
                                                keyOrder.Add(el); // For writing records
                                            }
                                            writer.NextRecord();
                                            wriitenHeader = true;
                                        }

                                        // Record
                                        writer.WriteField<long>(rec.RT);
                                        foreach (var key in keyOrder)
                                        {
                                            var elem = rec.EL[key];
                                            if (elem.T == "100")
                                            {
                                                writer.WriteField(elem.FN);
                                                if (string.IsNullOrEmpty(elem.FN))
                                                {
                                                    continue;
                                                }
                                                else if (!Path.GetExtension(elem.FN).ToLower().Equals(".csv"))
                                                {
                                                    this.isNoBulky = false;
                                                    continue;
                                                }
                                                else
                                                {
                                                    this.isNoBulky = false;
                                                    this.isNoCsvOnly = false;
                                                }

                                                if (key == elementId && elem.V != null)
                                                {
                                                    byte[] bulky = Convert.FromBase64String(elem.V);
                                                    byte[] encodedBulky = bulky;
                                                    if (AisUtil.IsBomUtf8(bulky))
                                                    {
                                                        encodedBulky = AisUtil.RemoveBom(bulky);
                                                    }
                                                    else
                                                    {
                                                        Encoding encoding = AisUtil.GetCode(bulky);
                                                        if (encoding != null)
                                                        {
                                                            if (!encoding.Equals(Encoding.UTF8))
                                                            {
                                                                encodedBulky = AisUtil.ConvertToWoUtf8Bytes(bulky, encoding);
                                                            }
                                                        }
                                                    }
                                                    swB.Write(encodedBulky, 0, encodedBulky.Length);
                                                }
                                            }
                                            else
                                            {
                                                writer.WriteField(elem.V);
                                            }
                                        }
                                        writer.NextRecord();
                                    }
                                }
                            }
                            finally
                            {
                                if (writer != null)
                                {
                                    writer.Dispose();
                                }
                            }
                        }
                    }
                }
                catch (AggregateException e)
                {
                    Exception ex = e.InnerException;

                    if (ex is TaskCanceledException)
                    {
                        cancelled = true;
                        logger.Info("Cancelled by user.");
                    }
                    else
                    {
                        failed = true;
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Failed to get Mission result: " + missionId, e);
                    failed = true;
                }
            });

            await task;

            if (cancelled || failed)
            {
                ClearDirectory(folderPath);
                folderPath = null;

                if (failed)
                {
                    FoaMessageBox.ShowError("AIS_E_045");
                    //var msg = "ミッション結果の取得に失敗しました。";
                    //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            return folderPath;
        }


        public const int WAITTIME_1 = 1000;
        public const int WAITTIME_2 = 1000;

        public const int ROWS_PER_PAGE = 100;

        public async Task<string> GetGripMissionResultCsvAsync(string missionId, long start, long end, CancellationToken cancelToken, string ctmId, string elementId)
        {
            string searchId = new GUID().ToString();
            string limit = "0";
            string lang = "ja";
            //string url = string.Format("{0}{1}?searchId={2}&missionId={3}&start={4}&end={5}&ctmId={6}&elementId={7}&limit={8}&lang={9}", CmsUrl.GetGripBaseUrl(), "search/ais2/bulkyTemplate/gripMission", searchId, missionId, start, end, ctmId, elementId, limit, lang);
            //logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() url:" + url);

            var folderPath = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission", Suid.NewID().ToString());
            var tmpFilepathB = System.IO.Path.Combine(folderPath, elementId + ".csv");
            Directory.CreateDirectory(folderPath);
            //HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(100) };
            bool failed = false;
            bool cancelled = false;
            logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() folderPath:" + folderPath + " tmpFilepathB:" + tmpFilepathB);

            string textData = null;
            string response1 = null;
            string responseWarning = null;
            await Task.Run(() =>
            {
                try
                {
                    //string url = string.Format(CmsUrl.Search.Mission.CreateCsv(), mission.Id, searchId, start, end, searchByInCondition, skipSameMainKey); logger.Debug(url);
                    string url = string.Format(CmsUrl.Search.Ais2.BulkyTemplate.GripMission.DoSearch(), searchId, missionId, start, end, ctmId, elementId, limit, lang); //logger.Debug(url);
                    logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() url:" + url);
                    var client = new FoaHttpClient();
                    var response = client.Get(url);
                    textData = response.Result;
                }
                catch (SocketException se)
                {
                    logger.Error("Search failed.", se);
                    response1 = se.Message;
                }
            });
            logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() textData:" + textData);
            if (cancelToken.IsCancellationRequested)
            {
                return textData;
            }

            string msg = Properties.Message.AIS_E_046;
            if (response1 != null)
            {
                //string msg = "検索データの取得に失敗しました。 " + response1;
                msg += response1;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (responseWarning != null)
            {
                msg = responseWarning;
                AisMessageBox.DisplayWarningMessageBoxWithCaption("NO DATA", msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "NO DATA", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            if (string.IsNullOrEmpty(textData))
            {
                msg += GripDataResponseType.ErrorNotFound;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorNotFound;
            }



            List<SearchResultPaths> s = null;
            if (string.IsNullOrEmpty(textData) || "[]" == textData)
            {
                msg += GripDataResponseType.ErrorNotFound;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorNotFound;
            }

            try
            {
                s = JsonUtil.Deserialize<List<SearchResultPaths>>(textData);
            }
            catch
            {
                msg += GripDataResponseType.ErrorProcess;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorProcess;
            }
            if (null == s)
            {
                msg += GripDataResponseType.ErrorProcess;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorProcess;
            }

            bool isNoData = true;
            await Task.Run(() =>
            {
                Thread.Sleep(WAITTIME_1);
                for (int i = 0; i < s.Count; i++)
                {
                    SearchResultPaths ps = s[i];
                    if (null == ps.paths || 0 == ps.paths.Count) continue;
                    for (int j = 0; j < ps.paths.Count; j++)
                    {
                        SearchResultPath p = ps.paths[j];
                        if (null == p.nodes || 2 > p.nodes.Count) continue;

                        string status = null;
                        int endId = i + 1;
                        int pathId = j + 1;
                        string url2 = string.Format(CmsUrl.Search.Ais2.BulkyTemplate.GripMission.GetStatus(), searchId, endId, pathId);
                        while (true)
                        {
                            Thread.Sleep(WAITTIME_2); logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2.Get: url2:" + url2);
                            var client2 = new FoaHttpClient();
                            var textData2 = client2.Get(url2); logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2.Get: textData2:" + textData2.Result);
                            var status2 = textData2.Result;
                            if (SearchResultBlock.STATUS_CREATING.Equals(status2)) continue;
                            status = status2;
                            break;
                        }
                        if (SearchResultBlock.STATUS_NORMAL.Equals(status))
                        {
                            p.existsData = true;
                            isNoData = false;
                        }
                    }
                }
            }); logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2.Get: isNoData:" + isNoData);
            if (isNoData)
            {
                return null;
                //return GripDataResponseType.NoFolder;
            }



            ProgramMission resultObject = new ProgramMission();
            Dictionary<long, ProgramMission.Ctms> ctmObjects = new Dictionary<long, ProgramMission.Ctms>();
            await Task.Run(() =>
            {
                int resultNo = 0;
                Thread.Sleep(WAITTIME_1);
                for (int i = 0; i < s.Count; i++)
                {
                    SearchResultPaths ps = s[i];
                    for (int j = 0; j < ps.paths.Count; j++)
                    {
                        SearchResultPath p = ps.paths[j];
                        if (!p.existsData) continue;

                        int endId = i + 1; int pathId = j + 1;
                        string url2 = string.Format(CmsUrl.Search.Ais2.BulkyTemplate.GripMission.DoDownload(), searchId, endId, pathId);

                        Thread.Sleep(WAITTIME_2);
                        WebClient client2 = new WebClient(); logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2D.Get: url2:" + url2);
                        byte[] fileData = client2.DownloadData(new System.Uri(url2)); logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2D.Get: fileData:" + fileData.Length);
                        string stringData = Encoding.GetEncoding("Shift_JIS").GetString(fileData);
                        JToken token = JToken.Parse(stringData);
                        if (string.IsNullOrEmpty(resultObject.id)) resultObject.id = (string)token["id"];
                        if (string.IsNullOrEmpty(resultObject.name)) resultObject.name = (string)token["name"];
                        JArray ctms = (JArray)token["ctms"];
                        foreach (JObject ctm in ctms)
                        {
                            long rt = (long)ctm["RT"];
                            JToken el = (JToken)ctm["EL"];
                            if (el[elementId] != null)
                            {
                                JToken element = el[elementId];
                                int t = (int)element["T"];
                                string fn = element["FN"] == null ? null : (string)element["FN"];
                                string v = element["V"] == null ? null : (string)element["V"];

                                if (!ctmObjects.ContainsKey(rt) && v != null)
                                {
                                    ProgramMission.Ctms ctmObject = new ProgramMission.Ctms();
                                    ctmObject.SetRT(rt);
                                    Dictionary<string, ProgramMission.Ctms.ELcls> elObjects = new Dictionary<string,ProgramMission.Ctms.ELcls>();
                                    ProgramMission.Ctms.ELcls elObject = new ProgramMission.Ctms.ELcls();
                                    elObject.SetT(t.ToString());
                                    elObject.SetFN(fn);
                                    elObject.V = v;
                                    elObjects[elementId] = elObject;
                                    ctmObject.SetEL(elObjects);
                                    ctmObjects[rt] = ctmObject;
                                }
                            }
                        }
                        client2.Dispose();
                        resultNo++;
                    }
                }
            });

            List<long> rts = new List<long>(ctmObjects.Keys);
            rts.Sort();
            rts.Reverse();

            List<ProgramMission.Ctms> ctmList = new List<ProgramMission.Ctms>();
            foreach (long rt in rts)
            {
                ctmList.Add(ctmObjects[rt]);
            }
            resultObject.SetCtms(ctmList);
            resultObject.num = ctmList.Count;
            //return pathString;

            var tmpFilepath = System.IO.Path.Combine(folderPath, "tmp.csv");
            StreamWriter sw = null;
            CsvWriter writer = null;
            bool wriitenHeader = false;
            List<string> keyOrder = null;

            using (var swB = File.Open(tmpFilepathB, FileMode.Create))
            {
                for (int i = 0; i < ctmList.Count; i++)
                {
                    var rec = ctmList[i];

                    if (i == 0)
                    {
                        sw = new StreamWriter(tmpFilepath, false, new UTF8Encoding(false));
                        writer = new CsvWriter(sw);
                        keyOrder = new List<string>(rec.EL.Count);

                        writer.WriteField(Keywords.RECEIVE_TIME);
                        foreach (var el in rec.EL.Keys)
                        {
                            writer.WriteField(el);
                            keyOrder.Add(el); // For writing records
                        }
                        writer.NextRecord();
                        wriitenHeader = true;
                    }

                    // Record
                    writer.WriteField<long>(rec.RT);
                    foreach (var key in keyOrder)
                    {
                        var elem = rec.EL[key];
                        if (elem.T == "100")
                        {
                            writer.WriteField(elem.FN);
                            if (string.IsNullOrEmpty(elem.FN))
                            {
                                continue;
                            }
                            else if (!Path.GetExtension(elem.FN).ToLower().Equals(".csv"))
                            {
                                this.isNoBulky = false;
                                continue;
                            }
                            else
                            {
                                this.isNoBulky = false;
                                this.isNoCsvOnly = false;
                            }

                            if (key == elementId && elem.V != null)
                            {
                                byte[] bulky = Convert.FromBase64String(elem.V);
                                byte[] encodedBulky = bulky;
                                if (AisUtil.IsBomUtf8(bulky))
                                {
                                    encodedBulky = AisUtil.RemoveBom(bulky);
                                }
                                else {
                                    Encoding encoding = AisUtil.GetCode(bulky);
                                    if (encoding != null)
                                    {
                                        if (!encoding.Equals(Encoding.UTF8))
                                        {
                                            encodedBulky = AisUtil.ConvertToWoUtf8Bytes(bulky, encoding);
                                        }
                                    }
                                }
                                swB.Write(encodedBulky, 0, encodedBulky.Length);
                            }
                        }
                        else
                        {
                            writer.WriteField(elem.V);
                        }
                    }
                    writer.NextRecord();
                }

                if (wriitenHeader)
                {
                    writer.Dispose();
                    writer = null;
                    sw.Close();
                    sw = null;

                    var targetFilepath = System.IO.Path.Combine(folderPath, ctmId + ".csv");
                    File.Move(tmpFilepath, targetFilepath);
                    wriitenHeader = false;
                }
                else
                {
                    logger.Info(string.Format("Mission[{0}] -> CTM[{1} is empty.", missionId, ctmId));
                }
            }

            if (cancelled || failed)
            {
                ClearDirectory(folderPath);
                folderPath = null;

                if (failed)
                {
                    //msg = "ミッション結果の取得に失敗しました。";
                    //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                    FoaMessageBox.ShowError("AIS_E_045");
                }
            }

            return folderPath;
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(BulkyDataRetriever));
    }
}
