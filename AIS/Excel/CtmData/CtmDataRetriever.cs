using AIS.Model;
using AIS.Util;
using CsvHelper;
using FoaCore;
using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using AIS.Model.Util;
using FoaCore.Common.Util;
using FoaCore.Common;

namespace AIS.CtmData
{
    public class CtmDataRetriever
    {
        public async Task<string> GetCtmDirectResultCsvAsync(List<CtmObject> ctmObjs, long start, long end, CancellationToken cToken)
        {
            AhInfo ahInfo = null;

            // ctmObjsの要素数が1以上であることは保障されている。
            var firstCtmId = ctmObjs[0].id.ToString();
            if (AisConf.Config.CmsVersion == "3.5")
            {
                ahInfo = await AisUtil.GetAhInfoAsync(firstCtmId);
            }

            var folderPath = System.IO.Path.Combine(AisConf.TmpFolderPath, "ctm", Suid.NewID().ToString());
            Directory.CreateDirectory(folderPath);
            string url = string.Format("{0}{1}?testing={2}&noBulky=true", CmsUrl.GetMibBaseUrl(), "mib/ctm/find", AisConf.Config.Testing);

            // CTMを検索する為にPOSTするJSONを生成
            List<string[]> postJsonList = new List<string[]>(ctmObjs.Count);
            foreach (var ctmObj in ctmObjs)
            {
                JToken postToken = JToken.Parse("{}");
                postToken["ctmId"] = ctmObj.id.ToString();
                postToken["start"] = start;
                postToken["end"] = end;
                if (AisConf.Config.CmsVersion == "3.5")
                {
                    postToken["mfHostName"] = ahInfo.Mf;
                    postToken["mfPort"] = ahInfo.MfPort;
                }
                string[] array = new string[] { postToken.ToString(), ctmObj.id.ToString() };
                postJsonList.Add(array);
            }

            HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(100) };
            bool cancelled = false;
            bool hasContent = false;
            List<string> failedCtmIds = new List<string>();
            List<Task> tasks = new List<Task>(postJsonList.Count);

            foreach (var a1 in postJsonList)
            {
                var req = new HttpRequestMessage(HttpMethod.Post, url);
                req.Content = new StringContent(a1[0]);

                var task = client.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cToken).ContinueWith(response0 =>
                {
                    try
                    {
                        if (cToken.IsCancellationRequested)
                        {
                            throw new TaskCanceledException();
                        }

                        var response = response0.Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            logger.Error("Failed to get CTM Direct result: " + a1[1] + " StatusCode=" + (int)(response.StatusCode));
                            failedCtmIds.Add(a1[1]);
                        }
                        else
                        {
                            var tmpFilepath = System.IO.Path.Combine(folderPath, a1[1] + ".csv");

                            using (StreamWriter sw = new StreamWriter(tmpFilepath, false, new UTF8Encoding(false)))
                            using (CsvWriter writer = new CsvWriter(sw))
                            using (var stream = response.Content.ReadAsStreamAsync().Result)
                            using (var sr = new StreamReader(stream))
                            using (var iterator = new CtmDirectResultReader(sr).GetEnumerator(AisConf.Config.CmsVersion == "3.5"))
                            {
                                if (!iterator.MoveNext())
                                {
                                    logger.Info(string.Format("CTM[{0}] is empty.", a1[1]));
                                }
                                else
                                {
                                    var rec = iterator.Current;

                                    // header
                                    List<string> keyOrder = new List<string>(rec.EL.Count);

                                    writer.WriteField(Keywords.RECEIVE_TIME);
                                    foreach (var el in rec.EL.Keys)
                                    {
                                        writer.WriteField(el);
                                        keyOrder.Add(el); // For writing records
                                    }
                                   
                                    writer.NextRecord();
                                    do
                                    {
                                        if (cToken.IsCancellationRequested)
                                        {
                                            throw new TaskCanceledException();
                                        }

                                        rec = iterator.Current;

                                        // Records
                                        writer.WriteField<long>(rec.RT);
                                        foreach (var key in keyOrder)
                                        {
                                            var elem = rec.EL[key];
                                            if (elem.T == "100")
                                            {
                                                writer.WriteField(elem.FN);
                                            }
                                            else
                                            {
                                                writer.WriteField(elem.V);
                                            }
                                        }
                                        writer.NextRecord(); // これなしだと直前のWriteFieldが書き込まれない。
                                    } while (iterator.MoveNext());

                                    hasContent = true;
                                }
                            }
                        }

                        if (cToken.IsCancellationRequested)
                        {
                            throw new TaskCanceledException();
                        }
                    }
                    catch (AggregateException e)
                    {
                        Exception ex = e.InnerException;

                        if (ex is TaskCanceledException)
                        {
                            logger.Info("Cancelled by user.");
                            cancelled = true;

                        }
                        else
                        {
                            logger.Error("Failed to get CTM Direct result: " + a1[1], e);
                            failedCtmIds.Add(a1[1]);
                        }
                    }
                    catch (TaskCanceledException)
                    {
                        logger.Info("Cancelled by user.");
                        cancelled = true;
                    }
                    catch (Exception e)
                    {
                        logger.Error("Failed to get CTM Direct result: " + a1[1], e);
                        failedCtmIds.Add(a1[1]);
                    }
                });
                tasks.Add(task);
            }
            await Task.WhenAll(tasks);

            if (cancelled)
            {
                ClearDirectory(folderPath);
                folderPath = null;
            }
            else if (failedCtmIds.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var failedId in failedCtmIds)
                {
                    foreach (var ctmObj in ctmObjs)
                    {
                        if (ctmObj.id.ToString() == failedId)
                        {
                            sb.Append(ctmObj.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                            sb.Append(",");
                        }
                    }
                }

                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }

                FoaMessageBox.ShowError("AIS_E_047", sb.ToString());
                //string msg = "CTMの取得に失敗しました。 " + sb.ToString();
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);

                if (!hasContent)
                {
                    ClearDirectory(folderPath);
                    folderPath = null;
                }
            }

            if (folderPath != null)
            {
                foreach (var file1 in Directory.GetFiles(folderPath, "*.csv"))
                {
                    var fileInfo1 = new FileInfo(file1);
                    if (fileInfo1.Length == 0)
                    {
                        fileInfo1.Delete();
                    }
                }
            }

            return folderPath;
        }

        public async Task<string> GetProgramMissionResultCsvAsync(string missionId, long start, long end, CancellationToken cancelToken)
        {
            string limit = "0";
            string lang = "ja";
            // string url = string.Format("{0}{1}?id={2}&start={3}&end={4}&limit={5}&lang={6}&testing={7}&noBulky=true", CmsUrl.GetMmsBaseUrl(), "mib/mission/pm", missionId, start, end, limit, lang, AisConf.Config.Testing);
            string url = string.Format("{0}{1}?id={2}&start={3}&end={4}&limit={5}&lang={6}&testing={7}", CmsUrl.GetMmsBaseUrl(), "mib/mission/pm", missionId, start, end, limit, lang, AisConf.Config.Testing);

            var folderPath = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission", Suid.NewID().ToString());
            Directory.CreateDirectory(folderPath);
            HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(100) };
            bool failed = false;
            bool cancelled = false;

            var task = client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead, cancelToken).ContinueWith(response0 =>
            {
                StreamWriter sw = null;
                CsvWriter writer = null;

                try
                {
                    if (cancelToken.IsCancellationRequested)
                    {
                        throw new TaskCanceledException();
                    }

                    var response = response0.Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        logger.Error("Failed to get Mission result: " + missionId + " StatusCode=" + (int)(response.StatusCode));
                        failed = true;
                    }
                    else
                    {
                        using (var stream = response.Content.ReadAsStreamAsync().Result)
                        using (var sr = new StreamReader(stream))
                        using (var iterator = new MissionResultReader(sr).GetEnumerator())
                        {
                            var tmpFilepath = System.IO.Path.Combine(folderPath, "tmp.csv");;
                            bool wriitenHeader = false;
                            List<string> keyOrder = null;

                            while (iterator.MoveNext())
                            {
                                if (cancelToken.IsCancellationRequested)
                                {
                                    throw new TaskCanceledException();
                                }

                                var rec = iterator.Current;
                                if (rec == null)
                                {
                                    var iterator1 = (MissionResultReader.CtmRecordEnumerator)iterator;
                                    var id = iterator1.CurrentId;

                                    if (wriitenHeader)
                                    {
                                        writer.Dispose();
                                        writer = null;
                                        sw.Close();
                                        sw = null;

                                        var targetFilepath = System.IO.Path.Combine(folderPath, id + ".csv");

                                        File.Move(tmpFilepath, targetFilepath);

                                        wriitenHeader = false;
                                    }
                                    else
                                    {
                                        logger.Info(string.Format("Mission[{0}] -> CTM[{1} is empty.", missionId, id));
                                    }
                                }
                                else
                                {
                                    // Header
                                    if (!wriitenHeader)
                                    {
                                        sw = new StreamWriter(tmpFilepath, false, new UTF8Encoding(false));
                                        writer = new CsvWriter(sw);
                                        keyOrder = new List<string>(rec.EL.Count);

                                        writer.WriteField(Keywords.RECEIVE_TIME);
                                        foreach (var el in rec.EL.Keys)
                                        {
                                            writer.WriteField(el);
                                            keyOrder.Add(el); // For writing records
                                        }
                                        writer.NextRecord();
                                        wriitenHeader = true;
                                    }

                                    // Record
                                    writer.WriteField<long>(rec.RT);
                                    foreach (var key in keyOrder)
                                    {
                                        if (cancelToken.IsCancellationRequested)
                                        {
                                            throw new TaskCanceledException();
                                        }

                                        var elem = rec.EL[key];
                                        if (elem == null)
                                        {
                                            writer.WriteField(string.Empty);
                                        }
                                        else if (elem.T == "100")
                                        {
                                            writer.WriteField(elem.FN);
                                        }
                                        else
                                        {
                                            writer.WriteField(elem.V);
                                        }
                                    }
                                    writer.NextRecord();
                                }
                            }
                        }
                    }

                    if (cancelToken.IsCancellationRequested)
                    {
                        throw new TaskCanceledException();
                    }
                }
                catch (AggregateException e)
                {
                    Exception ex = e.InnerException;

                    if (ex is TaskCanceledException)
                    {
                        cancelled = true;
                        logger.Info("Cancelled by user.");
                    }
                    else
                    {
                        logger.Error("Failed to get Mission result: " + missionId, ex);
                        failed = true;
                    }
                }
                catch (TaskCanceledException)
                {
                    cancelled = true;
                    logger.Info("Cancelled by user.");
                }
                catch (Exception e)
                {
                    logger.Error("Failed to get Mission result: " + missionId, e);
                    failed = true;
                }
                finally
                {
                    if (writer != null)
                    {
                        writer.Dispose();
                    }
                    if (sw != null)
                    {
                        sw.Close();
                    }
                }
            });

            await task;

            if (cancelled || failed)
            {
                ClearDirectory(folderPath);
                folderPath = null;

                if (failed)
                {
                    //var msg = string.Format("ミッション結果の取得に失敗しました。");
                    //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                    FoaMessageBox.ShowError("AIS_E_045");
                }
            }

            return folderPath;
        }

        protected static void ClearDirectory(string dirpath)
        {
            Task.Run(() =>
            {
                if (Directory.Exists(dirpath))
                {
                    Directory.Delete(dirpath, true);
                }
            });
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(CtmDataRetriever));
    }
}
