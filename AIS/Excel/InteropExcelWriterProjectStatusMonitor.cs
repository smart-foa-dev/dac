﻿using DAC.CtmData;
using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using CsvHelper;
using FoaCore;
using FoaCore.Common;
using FoaCore.Common.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.AExcel
{
    class InteropExcelWriterProjectStatusMonitor : ExcelWriter
    {
        private Dictionary<Mission, List<CtmObject>> missionCtmDict;
        private Dictionary<CtmObject, List<CtmElement>> missionCtmElementDict;

        public InteropExcelWriterProjectStatusMonitor(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi = null)
            : base(mode, ds, ctms, dt, bw)
        {

        }

        public InteropExcelWriterProjectStatusMonitor(ControlMode mode, DataSourceType ds, List<CtmObject> ctms, DataTable dt, BackgroundWorker bw, Mission mi, Dictionary<Mission, List<CtmObject>> ctmDict,
            Dictionary<CtmObject, List<CtmElement>> elemDict)
            : base(mode, ds, ctms, dt, bw)
        {
            this.missionCtmDict = ctmDict;
            this.missionCtmElementDict = elemDict;
        }

        public override void WriteCtmData(string excelName, string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
            Microsoft.Office.Interop.Excel.Workbooks oWBooks = null;
            Microsoft.Office.Interop.Excel.Workbook oWBook = null;

            try
            {
                oXls = new Microsoft.Office.Interop.Excel.Application();
                oXls.Visible = false; //確認のためエクセルのウィンドウを表示する

                //エクセルファイルをオープンする
                oWBooks = oXls.Workbooks;
                oWBook = oWBooks.Open(excelName); // オープンするExcelファイル名

                Microsoft.Office.Interop.Excel.Worksheet wsUserInfo = AisUtil.GetSheetFromSheetName_Interop(oWBook, "USERINFO");
                writeUserInfoSheet(wsUserInfo);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 1. Set Mission CTM & Element parameter
                string paramSheetNameM = "ミッション";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamM = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameM);
                if (this.dataSource == DataSourceType.MISSION)
                {
                    writeConfigSheetCtm(worksheetParamM);
                }
                else if (this.dataSource == DataSourceType.CTM_DIRECT)
                {
                    writeConfigSheetCtm2(worksheetParamM);
                }

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 2. Set Start & End Time parameter
                string paramSheetNameC = "業務時間";
                Microsoft.Office.Interop.Excel.Worksheet worksheetParamC = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetNameC);
                writeConfigSheetTime(worksheetParamC, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                string paramSheetName = DAC.Util.Keywords.PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetParam3 = AisUtil.GetSheetFromSheetName_Interop(oWBook, paramSheetName);
                WriteConfigSheetParam(worksheetParam3, configParams);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // ro_paramシート作成
                string roParamSheetName = DAC.Util.Keywords.RO_PARAM_SHEET;
                Microsoft.Office.Interop.Excel.Worksheet worksheetRoParam = AisUtil.GetSheetFromSheetName_Interop(oWBook, roParamSheetName);
                WriteRoParamSheet_Interop(worksheetRoParam);

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 初回ミッション結果を出力
                DirectoryInfo root = new DirectoryInfo(folderPath);
                if (root.Exists)
                {
                    writeData2(oXls, oWBook, root);
                }
                /*
                foreach (DirectoryInfo di in root.GetDirectories())
                {
                    string path = di.FullName;
                    if (!Directory.Exists(path))
                    {
                        continue;
                    }

                    writeData(oXls, oWBook, path);
                }
                */
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // 3. Save the sheet with the parameter
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Save();
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Close();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                //// 4. Re-open again with the newly inputted parameter sheet
                oWBook = (Microsoft.Office.Interop.Excel.Workbook)(oXls.Workbooks.Open(excelName));
                oXls.Visible = true; //確認のためエクセルのウィンドウを表示する
                ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Activate();

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                oXls.Run("MakeMenuBar");
                
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }
            }
            catch (Exception err)
            {
                // ログ出力
                System.Windows.MessageBox.Show("ExcelOpen Exception Error : \n" + err.ToString());
            }
            finally
            {
                if (oWBook != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                    oWBook = null;
                }

                if (oWBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBooks);
                    oWBooks = null;
                }

                if (oXls != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                    oXls = null;
                }
            }

        }

        protected void writeData(Microsoft.Office.Interop.Excel.Application excel, Microsoft.Office.Interop.Excel.Workbook book, string folderPath)
        {
            DataRow[] rows = convertToDatRowFromDictionary(this.missionCtmElementDict);
            List<CtmObject> ctms = convertToListFromDictionary(this.missionCtmElementDict);

            List<XYZ> srcCsvFiles = null;
            try
            {
                srcCsvFiles = new List<XYZ>();
                foreach (var ctm in ctms)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var ctmId = ctm.id.ToString();
                    var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    DataRow dataRow = null;
                    foreach (var row in rows)
                    {
                        if (row.ItemArray[0].ToString() == ctmName)
                        {
                            dataRow = row;
                            break;
                        }
                    }

                    if (dataRow == null)
                    {
                        // CTM自体の選択が無かったので無視
                        continue;
                    }

                    // シート検索 / 作成
                    Microsoft.Office.Interop.Excel.Worksheet sheet = AisUtil.GetSheetFromSheetName_Interop(book, ctmName);
                    sheet.Rows.Clear();

                    var srcFilepath = Path.Combine(folderPath, ctmId + ".csv");
                    CsvFile csvFileObj = null;
                    if (File.Exists(srcFilepath))
                    {
                        csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcFilepath };
                        var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                        srcCsvFiles.Add(srcCsvFile);
                    }

                    writeByCtmResultSheet(sheet, csvFileObj, ctm, dataRow, this.bw);

                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // 次のシートへ
                    sheet.UsedRange.Columns.AutoFit();
                }
            }
            finally
            {
                if (srcCsvFiles != null)
                {
                    foreach (XYZ srcCsvFile in srcCsvFiles)
                    {
                        srcCsvFile.Dispose();
                    }
                }
            }
        }

        protected void writeData2(Microsoft.Office.Interop.Excel.Application excel, Microsoft.Office.Interop.Excel.Workbook book, DirectoryInfo root)
        {
            DataRow[] rows = convertToDatRowFromDictionary(this.missionCtmElementDict);
            List<CtmObject> ctms = convertToListFromDictionary(this.missionCtmElementDict);

            List<XYZ> srcCsvFiles = null;
            try
            {
                srcCsvFiles = new List<XYZ>();
                foreach (var ctm in ctms)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var ctmId = ctm.id.ToString();
                    var ctmName = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    DataRow dataRow = null;
                    foreach (var row in rows)
                    {
                        if (row.ItemArray[0].ToString() == ctmName)
                        {
                            dataRow = row;
                            break;
                        }
                    }

                    if (dataRow == null)
                    {
                        // CTM自体の選択が無かったので無視
                        continue;
                    }

                    // シート検索 / 作成
                    Microsoft.Office.Interop.Excel.Worksheet sheet = AisUtil.GetSheetFromSheetName_Interop(book, ctmName);
                    sheet.Rows.Clear();

                    CsvFile csvFileObj = null;
                    foreach (DirectoryInfo di in root.GetDirectories())
                    {
                        string srcCsvPath = Path.Combine(di.FullName, ctmId + ".csv");
                        if (File.Exists(srcCsvPath))
                        {
                            csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcCsvPath };
                            var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                            srcCsvFiles.Add(srcCsvFile);
                        }
                    }

                    writeByCtmResultSheet(sheet, csvFileObj, ctm, dataRow, this.bw);
                    /*
                    var srcFilepath = Path.Combine(folderPath, ctmId + ".csv");
                    if (File.Exists(srcFilepath))
                    {
                        csvFileObj = new CsvFile() { Id = new Suid(ctmId), Name = ctmName, Filepath = srcFilepath };
                        var srcCsvFile = new XYZ() { id = ctmId, name = ctmName, ctmObj = ctm, dtatRow = dataRow, csvFile = csvFileObj };
                        srcCsvFiles.Add(srcCsvFile);
                    }

                    writeByCtmResultSheet(sheet, csvFileObj, ctm, dataRow, this.bw);
                    */
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // 次のシートへ
                    sheet.UsedRange.Columns.AutoFit();
                }
            }
            finally
            {
                if (srcCsvFiles != null)
                {
                    foreach (XYZ srcCsvFile in srcCsvFiles)
                    {
                        srcCsvFile.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// CtmObjectの中身を、全てDataTableとして使用する
        /// </summary>
        /// <param name="ctms"></param>
        /// <returns></returns>
        private DataTable createDt(List<CtmObject> ctms)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));

            foreach (CtmObject ctm in ctms)
            {
                foreach (var element in ctm.GetAllElements())
                {
                    string elementName = element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    if (!dt.Columns.Contains(elementName))
                    {
                        dt.Columns.Add(new DataColumn(elementName));
                    }
                }

                DataRow row = dt.NewRow();
                row[Keywords.CTM_NAME] = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                foreach (var element in ctm.GetAllElements())
                {
                    string elementName = element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    row[elementName] = elementName;
                }
                dt.Rows.Add(row);
            }

            return dt;
        }

        /// <summary>
        /// CtmObjectの中身を、全てDataTableとして使用する
        /// </summary>
        /// <param name="missions"></param>
        /// <returns></returns>
        private DataTable createDt(Dictionary<Mission, List<CtmObject>> missions, Dictionary<CtmObject, List<CtmElement>> ctms)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));

            foreach (var ctmList in missions.Values)
            {
                foreach (CtmObject ctm in ctmList)
                {
                    bool hit = false;
                    foreach (var rowObj in dt.Rows)
                    {
                        var row = rowObj as DataRow;
                        if (row == null)
                        {
                            continue;
                        }
                        if (row[Keywords.CTM_NAME].ToString() != ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true))
                        {
                            continue;
                        }

                        hit = true;
                        break;
                    }
                    if (hit)
                    {
                        continue;
                    }

                    foreach (var element in ctm.GetAllElements())
                    {
                        string elementName = element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                        if (!dt.Columns.Contains(elementName))
                        {
                            dt.Columns.Add(new DataColumn(elementName));
                        }
                    }

                    DataRow row2 = dt.NewRow();
                    row2[Keywords.CTM_NAME] = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    foreach (var element in ctm.GetAllElements())
                    {
                        string elementName = element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                        row2[elementName] = elementName;
                    }
                    dt.Rows.Add(row2);
                }
            }

            return dt;
        }

        /// <summary>
        /// CtmObjectの中身を、全てDataRow配列として使用する
        /// </summary>
        /// <param name="missions"></param>
        /// <returns></returns>
        private DataRow[] convertToDatRowFromDictionary(Dictionary<CtmObject, List<CtmElement>> ctms)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));

            List<DataRow> rows = new List<DataRow>();
            foreach (var ctm in ctms.Keys)
            {
                DataTable dt2 = new DataTable();
                dt2.Columns.Add("CtmName");
                for (int i = 0; i < ctms[ctm].Count; i++)
                {
                    dt2.Columns.Add(string.Format("Element_{0}", i.ToString("00")));
                }

                DataRow row = dt2.NewRow();
                List<string> els = new List<string>();
                els.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                foreach (var el in ctms[ctm])
                {
                    els.Add(el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                }
                row.ItemArray = els.ToArray();

                rows.Add(row);
            }

            return rows.ToArray();
        }

        private List<CtmObject> convertToListFromDictionary(Dictionary<CtmObject, List<CtmElement>> elementMap)
        {
            List<CtmObject> ctms = new List<CtmObject>();
            foreach (CtmObject ctm in elementMap.Keys)
            {
                ctms.Add(ctm);
            }

            return ctms;
        }

        private static void writeByCtmResultSheet(Microsoft.Office.Interop.Excel.Worksheet worksheet, CsvFile csvFile,
          CtmObject originalCtm, DataRow row, BackgroundWorker bw)
        {
            Dictionary<string, string> id2Name = new Dictionary<string, string>();
            foreach (var element in originalCtm.GetAllElements())
            {
                id2Name.Add(element.id.ToString(), element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
            }

            Microsoft.Office.Interop.Excel.Range cells = worksheet.Cells;

            // Header
            int headCount = 1;
            cells[1, 1].Value = Keywords.RECEIVE_TIME;

            // エレメント名
            int headerIndex = 2;
            bool firstColumn = true;
            foreach (var item in row.ItemArray)
            {
                // Cancel Check
                if (bw.CancellationPending)
                {
                    return;
                }

                if (firstColumn)
                {
                    firstColumn = false;
                    continue;
                }

                var strVal = item as string;
                if (string.IsNullOrEmpty(strVal))
                {
                    continue;
                }

                cells[1, headerIndex].Value = item;
                headerIndex++;
                headCount++;

            }

            if (csvFile == null)
            {
                return;
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }
            
            object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headCount];
            
            using (var sr = new StreamReader(csvFile.Filepath, Encoding.UTF8))
            using (var reader = new CsvReader(sr))
            {
                reader.Read();
                var headers = reader.FieldHeaders;


                var pos2Pos = new Dictionary<int, int>();

                for (int i = 1; i < headers.Length; i++)
                {
                    var id1 = headers[i];
                    if (!id2Name.ContainsKey(id1)) continue;
                    var name1 = id2Name[id1];
                    int pos = GetPositionInDataRow(row, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 1); // RT
                    }
                }


                // Loop over rows
                int rowIndex = 2;
                int blockRowIndex = 0;
                do
                {
                    // Cancel Check
                    if (bw.CancellationPending)
                    {
                        return;
                    }

                    var rec = reader.CurrentRecord;

                    long rt = Int64.Parse(rec[0]);

                    values[blockRowIndex, 0] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);

                    for (int j = 1; j < rec.Length; j++)
                    {
                        int columnIndex;
                        if (pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            string item = rec[j];
                            if (item.StartsWith("=="))
                            {
                                item = "'" + item;
                            }

                            values[blockRowIndex, columnIndex] = item;
                        }
                    }

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                        range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, headCount);
                        range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                        // RT Format
                        Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 1];
                        range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;
                    }
                } while (reader.Read());

                // Cancel Check
                if (bw.CancellationPending)
                {
                    return;
                }

                if (0 != blockRowIndex)
                {
                    Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                    range = range.get_Resize(blockRowIndex, headCount);
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // RT Format
                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 1];
                    range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                    range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                }
            }

            // Cancel Check
            if (bw.CancellationPending)
            {
                return;
            }

            worksheet.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        private void writeIntegratedResultSheet(Microsoft.Office.Interop.Excel.Workbook workbook, DataTable dtOutput, List<XYZ> srcFiles, long displayStartCurrentBlock = 0)
        {
            Microsoft.Office.Interop.Excel.Worksheet worksheet = AisUtil.GetSheetFromSheetName_Interop(workbook, "result");
            worksheet.Rows.Clear();
            Microsoft.Office.Interop.Excel.Range cells = worksheet.Cells;

            // ヘッダー
            cells[1, 1].Value = Keywords.CTM_NAME;
            cells[1, 2].Value = Keywords.RECEIVE_TIME;

            int headerIdx = 3;
            bool firstColumn = true;
            foreach (DataColumn column in dtOutput.Columns)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (firstColumn)
                {
                    firstColumn = false;
                    continue;
                }
                var colName0 = column.ColumnName;
                var colName = colName0.Substring(0, colName0.Length - 3);

                cells[1, headerIdx].Value = colName;
                headerIdx++;
            }
            int headCount = headerIdx;

            //
            // レコードを書く前の準備
            //
            foreach (var csvFile in srcFiles)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                csvFile.Init();
                var reader = csvFile.reader;
                reader.Read(); // Read Header and first row

                var pos2Pos = new Dictionary<int, int>();
                var header = reader.FieldHeaders;
                for (int i = 1; i < header.Length; i++)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    var id1 = header[i];
                    var el = csvFile.ctmObj.GetElement(new GUID(id1));
                    if (el == null)
                    {
                        continue;
                    }
                    var name1 = el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    int pos = AisUtil.GetPositionInDataRowForAllCtms(csvFile.dtatRow, name1);
                    if (pos >= 0)
                    {
                        pos2Pos.Add(i, pos + 2); // RT + CTM NAME
                    }
                }
                csvFile.pos2Pos = pos2Pos;
            }

            // Cancel Check
            if (this.bw.CancellationPending)
            {
                return;
            }

            //
            // レコード
            //
            using (var merger = new MergeIterators(srcFiles))
            {
                int rowIndex = 2;
                int blockRowIndex = 0;
                object[,] values = new object[AisUtil.MAX_BUFF_ROWS, headCount];

                do
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    XYZ currentIterator = merger.Current();
                    var rec = currentIterator.Current();
                    long rt = Int64.Parse(rec[0]);
                    if (rt < displayStartCurrentBlock)
                    {
                        continue;
                    }

                    values[blockRowIndex, 0] = currentIterator.Name();
                    values[blockRowIndex, 1] = UnixTime.ToDateTime(rt).ToString(AisUtil.TIME_FORMAT);

                    for (int j = 1; j < rec.Length; j++)
                    {
                        int columnIndex;
                        if (currentIterator.pos2Pos.TryGetValue(j, out columnIndex))
                        {
                            string item = rec[j];
                            if (item.StartsWith("=="))
                            {
                                item = "'" + item;
                            }

                            values[blockRowIndex, columnIndex] = item;
                        }
                    }

                    blockRowIndex++;

                    if (AisUtil.MAX_BUFF_ROWS == blockRowIndex)
                    {
                        int currentRowIndex = rowIndex + blockRowIndex;

                        Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                        range = range.get_Resize(AisUtil.MAX_BUFF_ROWS, headCount);
                        range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                        // RT Format
                        Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 2];
                        range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                        range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;

                        rowIndex = currentRowIndex;
                        blockRowIndex = 0;

                        // Cancel Check
                        if (this.bw.CancellationPending)
                        {
                            return;
                        }
                    }
                } while (merger.MoveNext());

                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (0 != blockRowIndex)
                {
                    Microsoft.Office.Interop.Excel.Range range = worksheet.Cells[rowIndex, 1];
                    range = range.get_Resize(blockRowIndex, headCount);
                    range.set_Value(Microsoft.Office.Interop.Excel.XlRangeValueDataType.xlRangeValueDefault, values);

                    // RT Format
                    Microsoft.Office.Interop.Excel.Range range2 = worksheet.Cells[rowIndex, 2];
                    range2 = range2.get_Resize(AisUtil.MAX_BUFF_ROWS, 1);
                    range2.NumberFormat = AisUtil.RT_VIEW_FORMAT;
                }
            }
        }

        private void writeUserInfoSheet(Microsoft.Office.Interop.Excel.Worksheet ws)
        {
            ws.Cells[1, 1].Value = "'" + LoginInfo.GetInstance().GetLoginUserInfo().UserId;

            // Host
            Microsoft.Office.Interop.Excel.Range rangeA = AisUtil.GetFindRange_Interop(ws.Cells, "CmsHost");
            if (rangeA != null)
            {
                rangeA[1, 2].Value = AisConf.Config.CmsHost;
            }

            // Port
            Microsoft.Office.Interop.Excel.Range rangeB = AisUtil.GetFindRange_Interop(ws.Cells, "CmsPort");
            if (rangeB != null)
            {
                rangeB[1, 2].Value = AisConf.Config.CmsPort;
            }

            ws.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        private void writeConfigSheetTime(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            DateTime start = GetDateTimeVal(configParams, ExcelConfigParam.START, DateTime.MinValue);
            DateTime end = GetDateTimeVal(configParams, ExcelConfigParam.END, DateTime.MinValue);
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 操業開始時刻
            start.AddSeconds(-start.Second);
            start.AddMilliseconds(-start.Millisecond);
            cellsParam[1, 1].Value = start.ToString("HH:mm");

            // 操業終了時刻
            end.AddSeconds(-end.Second);
            end.AddMilliseconds(-end.Millisecond);
            cellsParam[2, 1].Value = end.ToString("HH:mm");

            // Hide the workSheet
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        private void writeConfigSheetCtm(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();

            // 2. Pass the Mission parameter
            int startRowIndex = 1;
            int eleStartRowIndex = 1;
            foreach (KeyValuePair<Mission, List<CtmObject>> kvpMission in missionCtmDict)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                if (kvpMission.Value.Count <= 0) continue; // skip mission who does not have selected CTM object

                // Mission ID
                cellsParam[startRowIndex, 1].Value = kvpMission.Key.Id;

                // Mission Name
                cellsParam[startRowIndex, 2].Value = kvpMission.Key.Name;

                // 3. Pass the CTM parameter
                int ctmStartRowIndex = startRowIndex;
                foreach (CtmObject ctm in kvpMission.Value)
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // ParentMission ID
                    cellsParam[ctmStartRowIndex, 3].Value = kvpMission.Key.Id;

                    // CTM ID
                    cellsParam[ctmStartRowIndex, 4].Value = ctm.id.ToString();

                    // CTM Name
                    cellsParam[ctmStartRowIndex, 5].Value = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    // 4. Pass the Element parameter
                    foreach (CtmElement ele in missionCtmElementDict[ctm])
                    {
                        // Cancel Check
                        if (this.bw.CancellationPending)
                        {
                            return;
                        }

                        // ParentCTM ID
                        cellsParam[eleStartRowIndex, 6].Value = ctm.id.ToString();

                        // Element ID
                        cellsParam[eleStartRowIndex, 7].Value = ele.id.ToString();

                        // Element Name
                        cellsParam[eleStartRowIndex, 8].Value = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                        // Additional : ParentMission ID -> For deleting CTM Element in case there is duplicate CTM
                        cellsParam[eleStartRowIndex, 9].Value = kvpMission.Key.Id;

                        ++eleStartRowIndex;
                    }

                    ++ctmStartRowIndex;
                }

                startRowIndex = ctmStartRowIndex;
            }
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        private void writeConfigSheetCtm2(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            Microsoft.Office.Interop.Excel.Range cellsParam = worksheetParam.Cells;

            // 1. Clear all previous mission parameter if exists
            worksheetParam.UsedRange.Clear();

            // 2. Pass the Mission parameter
            int startRowIndex = 1;
            int eleStartRowIndex = 1;

            // 3. Pass the CTM parameter
            int ctmStartRowIndex = startRowIndex;
            foreach (CtmObject ctm in this.missionCtmElementDict.Keys)
            {
                // Cancel Check
                if (this.bw.CancellationPending)
                {
                    return;
                }

                // CTM ID
                cellsParam[ctmStartRowIndex, 4].Value = ctm.id.ToString();

                // CTM Name
                cellsParam[ctmStartRowIndex, 5].Value = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                // 4. Pass the Element parameter
                foreach (CtmElement ele in missionCtmElementDict[ctm])
                {
                    // Cancel Check
                    if (this.bw.CancellationPending)
                    {
                        return;
                    }

                    // ParentCTM ID
                    cellsParam[eleStartRowIndex, 6].Value = ctm.id.ToString();

                    // Element ID
                    cellsParam[eleStartRowIndex, 7].Value = ele.id.ToString();

                    // Element Name
                    cellsParam[eleStartRowIndex, 8].Value = ele.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);

                    ++eleStartRowIndex;
                }

                ++ctmStartRowIndex;
            }

            startRowIndex = ctmStartRowIndex;
            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

        protected virtual void WriteConfigSheetParam(Microsoft.Office.Interop.Excel.Worksheet worksheetParam, Dictionary<ExcelConfigParam, object> configParams)
        {
            var range = worksheetParam.get_Range("A1", "J100");

            // 登録フォルダ
            var range2 = AisUtil.GetFindRange_Interop(range, "登録フォルダ");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.RegistryFolderPath;
            }

            // 登録ファイル
            range2 = AisUtil.GetFindRange_Interop(range, "登録ファイル");
            if (range2 != null)
            {
                range2[1, 2].Value = GetStringVal(configParams, ExcelConfigParam.REGISTERED_FILENAME, string.Empty);
            }

            // サーバIPアドレス
            range2 = AisUtil.GetFindRange_Interop(range, "サーバIPアドレス");
            {
                range2[1, 2].Value = AisConf.Config.CmsHost;
            }

            // サーバポート番号
            range2 = AisUtil.GetFindRange_Interop(range, "サーバポート番号");
            {
                range2[1, 2].Value = AisConf.Config.CmsPort;
            }

            // テンプレート名称
            range2 = AisUtil.GetFindRange_Interop(range, "テンプレート名称");
            {
                range2[1, 2].Value = "プロジェクトステータスモニター";
            }

            // 更新周期
            range2 = AisUtil.GetFindRange_Interop(range, "更新周期");
            {
                range2[1, 2].Value = GetIntVal(configParams, ExcelConfigParam.RELOAD_INTERVAL, 60);
            }

            // 取得期間
            range2 = AisUtil.GetFindRange_Interop(range, "取得期間");
            {
                range2[1, 2].Value = GetFloatVal(configParams, ExcelConfigParam.GET_PERIOD, 0);
            }

            // ミッションID
            range2 = AisUtil.GetFindRange_Interop(range, "ミッションID");
            if (range2 != null)
            {
                range2[1, 2].Value = GetStringVal(configParams, ExcelConfigParam.MISSION_ID, string.Empty);
            }

            // 製作者名
            range2 = AisUtil.GetFindRange_Interop(range, "製作者名");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.UserId;
            }

            // 収集タイプ
            range2 = AisUtil.GetFindRange_Interop(range, "収集タイプ");
            if (range2 != null)
            {
                switch (this.dataSource)
                {
                    case DataSourceType.CTM_DIRECT:
                        range2[1, 2].Value = "CTM";
                        break;
                    case DataSourceType.MISSION:
                        range2[1, 2].Value = "ミッション";
                        break;
                    case DataSourceType.GRIP:
                        range2[1, 2].Value = "GRIP";
                        break;
                    default:
                        range2[1, 2].Value = "";
                        break;
                }
            }

            // for KMEW
            range2 = AisUtil.GetFindRange_Interop(range, "CmsVersion");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.CmsVersion;
            }
            if (AisConf.Config.CmsVersion == "3.5")
            {
                var ahInfo = AisUtil.GetAhInfo(this.Ctms[0].id.ToString());

                range2 = AisUtil.GetFindRange_Interop(range, "MfHost");
                if (range2 != null)
                {
                    range2[1, 2].Value = ahInfo.Mf;
                }
                range2 = AisUtil.GetFindRange_Interop(range, "MfPort");
                if (range2 != null)
                {
                    range2[1, 2].Value = ahInfo.MfPort;
                }
            }

            //BugNo.546 Added for display bug. paramに編集モード項目追加.
            // 編集モード
            range2 = AisUtil.GetFindRange_Interop(range, "編集モード");
            if (range2 != null)
            {
                range2[1, 2].Value = GetStringVal(configParams, ExcelConfigParam.EDIT_MODE, string.Empty);
            }

            // ProxyServer使用
            range2 = AisUtil.GetFindRange_Interop(range, "ProxyServer使用");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.UseProxy;
            }

            // ProxyServerURI
            range2 = AisUtil.GetFindRange_Interop(range, "ProxyServerURI");
            if (range2 != null)
            {
                range2[1, 2].Value = AisConf.Config.ProxyURI;
            }

            // IsFirstSave
            range2 = AisUtil.GetFindRange_Interop(range, "IsFirstSave");
            if (range2 != null)
            {
                range2[1, 2].Value = "";
            }

            worksheetParam.Visible = Microsoft.Office.Interop.Excel.XlSheetVisibility.xlSheetHidden;
        }

    }
}
