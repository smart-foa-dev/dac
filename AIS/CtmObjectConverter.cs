﻿using FoaCore.Common;
using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;

namespace DAC.Model
{
    public class CtmObjectConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(CtmObject).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObj = JObject.Load(reader);
            CtmObject ctm = new CtmObject();
            ctm.id = new GUID(jObj[MmsTvJsonKey.Id].ToString());
            ctm.cd = jObj[MmsTvJsonKey.Cd] != null ? jObj[MmsTvJsonKey.Cd].ToString() : null;
            ctm.revision = jObj[MmsTvJsonKey.Revision] != null ? (int)jObj[MmsTvJsonKey.Revision] : 0;
            ctm.lastUpdateTime = jObj["lastUpdateTime"] != null ? (long)jObj["lastUpdateTime"] : 0;
            ctm.updatedBy = jObj["updatedBy"] != null ? jObj["updatedBy"].ToString() : "";
            ctm.systemName = jObj["systemName"] != null ? jObj["systemName"].ToString() : "";

            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
            ctm.missionId = jObj["missionId"] != null ? jObj["missionId"].ToString() : "";
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End
            ctm.status = (int)jObj[MmsTvJsonKey.Status];
            ctm.ctmClass = jObj[MmsTvJsonKey.CtmClass] != null ? jObj[MmsTvJsonKey.CtmClass].ToString() : "";
            ctm.commentFileVersion = jObj[MmsTvJsonKey.CommentFileVersion] != null ? (int)jObj[MmsTvJsonKey.CommentFileVersion] : 0;
            ctm.useCommentFile = jObj[MmsTvJsonKey.UseCommentFile] != null ? (bool)jObj[MmsTvJsonKey.UseCommentFile] : false;

            foreach (JToken dnToken in (JArray)jObj[MmsTvJsonKey.DisplayName])
            {
                ctm.displayName.Put(dnToken[MmsTvJsonKey.Lang].ToString(), dnToken[MmsTvJsonKey.Text].ToString());
            }
            foreach (JToken dnToken in (JArray)jObj[MmsTvJsonKey.Comment])
            {
                ctm.comment.Put(dnToken[MmsTvJsonKey.Lang].ToString(), dnToken[MmsTvJsonKey.Text].ToString());
            }

            if (jObj[MmsTvJsonKey.Children] != null && jObj[MmsTvJsonKey.Children] is JArray)
            {
                foreach (JToken childToken in (JArray)jObj[MmsTvJsonKey.Children])
                {
                    if (jObj["_isParse2JsonUsMember"] != null) childToken["_isParse2JsonUsMember"] = jObj["_isParse2JsonUsMember"];
                    CtmChildNode childNode = JsonConvert.DeserializeObject<CtmChildNode>(childToken.ToString());
                    ctm.children.Add(childNode);
                }
            }

            return ctm;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            CtmObject ctm = (CtmObject)value;

            JObject token = new JObject();
            token[MmsTvJsonKey.Id] = ctm.id.ToString();
            //token[MmsTvJsonKey.Type] = NodeType.Ctm;
            token[MmsTvJsonKey.DisplayName] = JArray.Parse(JsonConvert.SerializeObject(ctm.displayName));
            token[MmsTvJsonKey.Comment] = JArray.Parse(JsonConvert.SerializeObject(ctm.comment));
            if (!string.IsNullOrEmpty(ctm.cd))
            {
                token[MmsTvJsonKey.Cd] = ctm.cd;
            }
            token[MmsTvJsonKey.Revision] = ctm.revision;
            token["lastUpdateTime"] = ctm.lastUpdateTime;
            token["updatedBy"] = ctm.updatedBy;
            token["systemName"] = ctm.systemName;
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
            token["missionId"] = ctm.missionId;
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End
            token[MmsTvJsonKey.Status] = ctm.status;
            // token[MmsTvJsonKey.Status2] = ctm.status2;
            if (!string.IsNullOrEmpty(ctm.ctmClass))
            {
                token[MmsTvJsonKey.CtmClass] = ctm.ctmClass;
            }
            token[MmsTvJsonKey.UseCommentFile] = ctm.useCommentFile;
            token[MmsTvJsonKey.CommentFileVersion] = ctm.commentFileVersion;
            // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
            if (ctm.children != null)
            {
                JArray childrenArray = new JArray();
                foreach (CtmChildNode childNode in ctm.children)
                {
                    JToken childToken = JToken.Parse(JsonConvert.SerializeObject(childNode));
                    childrenArray.Add(childToken);
                }
                if (childrenArray.Count > 0)
                {
                    token[MmsTvJsonKey.Children] = childrenArray;
                }
            }
            writer.WriteRaw(token.ToString());
        }
    }
}
