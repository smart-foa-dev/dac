﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using DAC.Model;

namespace DAC
{
    // メモ: [リファクター] メニューの [名前の変更] コマンドを使用すると、コードと config ファイルの両方で同時にクラス名 "DacService" を変更できます。
    public class DacService : IDacService
    {
        public void CreateDac(string templateFile, DateTime? start, DateTime? end)
        {
            Dac_Home.CreateDac(templateFile, start, end);
        }
        //Dac_Home sunyi 20190530 start
        //登録したテンプレートを編集する
        public void UpdateDac(string templateFile, DateTime? start, DateTime? end)
        {
            Dac_Home.UpdateDac(templateFile, start, end);
        }
        //Dac_Home sunyi 20190530 end
        public DacExecutionResult ValidateDac(string templateFile)
        {
            return Dac_Home.ValidateDac(templateFile);
        }
    }
}
