﻿using FoaCore.Common.Entity;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DAC.Model
{
    public class GripMissionCtm : CmsEntity
    {
        public int MissionType { get; set; }
        public int SearchType { get; set; }

        //ISSUE_NO.778 Sunyi 2018/07/23 Start
        //G-missionの条件設定の色付く
        //public Node StartNode { get; set; }
        //public List<Node> EndNodes { get; set; }
        //public List<Node> TraceNodes { get; set; }

        public StartNodeObject StartNode { get; set; }
        //E2NaviUI sunyi 20190305 start
        //MISSION条件対応
        public Dictionary<string, StartNodeObject> StartNodes { get; set; }
        //E2NaviUI sunyi 20190305 end
        public List<EndNodeObject> EndNodes { get; set; }
        public List<TraceNodeObject> TraceNodes { get; set; }
        //ISSUE_NO.778 Sunyi 2018/07/23 End

        public List<Node> BranchNodes { get; set; }

        public string ElementCondition { get; set; }

        public string LinkDocumentId { get; set; }

        /// <summary>
        /// 表示名
        /// </summary>
        [JsonIgnore]
        public List<LangObject> DisplayNameList { get; set; }

        /// <summary>
        /// 表示名テキスト
        /// </summary>
        public string DisplayName { get; set; }
    }

    public class Node
    {
        public string CtmId { get; set; }
        public List<string> ElementIds { get; set; }
    }

    //ISSUE_NO.778 Sunyi 2018/07/23 Start
    //G-missionの条件設定の色付く
    //public class Node
    //{
    //    public string CtmId { get; set; }
    //    public List<string> ElementIds { get; set; }
    //}

    public class StartNodeObject
    {
        public string CtmId;
        public string CtmClass;

        public List<string> ElementIds;

        public string StartTime;
        public string EndTime;

        public ConditionSet CondSet;

        //E2NaviUI sunyi 20190305 start
        //MISSION条件対応
        public SearchGroupSetting startGroupSet;
        //E2NaviUI sunyi 20190305 end
    }

    public class SearchGroupSetting
    {
        public string id { get; set; }
        public int substringStart { get; set; }
        public int substringLength { get; set; }
    }

    public class EndNodeObject
    {
        public string CtmId;
        public string CtmClass;

        public List<string> ElementIds;

        public ConditionSet CondSet_End;
        //E2NaviUI sunyi 20190305 start
        //MISSION条件対応
        public SearchGroupSetting endGroupSet;
        //E2NaviUI sunyi 20190305 end
    }

    public class TraceNodeObject
    {
        public string CtmId;
        public string CtmClass;

        public List<string> ElementIds;

        public ConditionSet CondSet_Trace;
    }

    public class ConditionSet
    {
        public int Pattern; // LogicalOperatorPattern 0: NONE, 1: ONE, 2: AND, 3: OR
        public List<Condition> Conditions = new List<Condition>();
    }

    public class Condition
    {
        public string ElementId;
        public int Datatype; // FoaDatatype
        public string Value;
        public int Operator;// ElementConditionPattern 1: EQ, 2: NE, 3: LT, 4: LE, 5: GT, 6: GE
    }
    //ISSUE_NO.778 Sunyi 2018/07/23 Start
    //G-missionの条件設定の色付く
}
