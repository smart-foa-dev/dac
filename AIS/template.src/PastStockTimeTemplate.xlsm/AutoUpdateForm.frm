VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} AutoUpdateForm 
   Caption         =   "自動更新状態"
   ClientHeight    =   540
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   5910
   OleObjectBlob   =   "AutoUpdateForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "AutoUpdateForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'*****************
'* 表示位置の調整
'*****************
Private Sub UserForm_Initialize()
  With AutoUpdateForm
    .StartUpPosition = 0
    .Top = ActiveWindow.Top + 10
    .Left = ActiveWindow.Left + ActiveWindow.Width - .Width - 12
  End With
End Sub

'***************************************
'* 画面右上の「×」を押されたときの処理
'***************************************
Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)
    Unload Me
End Sub
