Attribute VB_Name = "Mission_Module"
Option Explicit

'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
Public Sub GetMissionInfoAll()
    Dim ctmList()   As CTMINFO      ' CTM情報
    Dim II          As Integer
    Dim ctmFile     As String
    Dim ctmFiles    As Object
    Dim jsonParse As New FoaCoreCom.jsonParse
    
    Set ctmFiles = GetMissionInfo(ctmList)
    '* シート毎（シート名＝CTM名）へ出力
    If ctmFiles.Count > 0 Then
    
        '* CTM種別でシートに出力
        For II = 0 To UBound(ctmList)
            
            ctmFile = ctmFiles(ctmList(II).ID)
            Call OutputCTMInfoToSheet(ctmList(II).Name, ctmFile)
        
        Next
        
        Call jsonParse.removeCsvFiles(ctmFile)
    
    End If
    
End Sub

'****************************************************************
'* 指定ミッションの情報を取得する
'* ※ParamシートにミッションID、CTMエレメント一覧が必要
'****************************************************************
Private Function GetMissionInfo(ctmList() As CTMINFO) As Object
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim missionID   As String
    Dim startTime   As Double
    Dim endTime     As Double
    Dim SendString  As String
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim dispTerm    As Double
    Dim dispTermUnit As String
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim keyOrder()  As String
    Dim ctmEmptyFiles   As New Dictionary
    Dim jsonParse   As New FoaCoreCom.jsonParse
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    '* メインシートから各種情報を取得
    With Worksheets(ParamSheetName)
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得開始", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(Format(srchRange.Offset(0, 1).Value, "yyyy/m/d"))
            startT = TimeValue(Format(srchRange.Offset(0, 1).Value, "hh:mm:ss"))
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            Set GetMissionInfo = ctmEmptyFiles
            Exit Function
        End If
        startTime = GetUnixTime(startD + startT)
        
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = DateValue(Format(srchRange.Offset(0, 1).Value, "yyyy/m/d"))
            endT = TimeValue(Format(srchRange.Offset(0, 1).Value, "hh:mm:ss"))
        Else
            endD = DateValue("2020/12/31 23:59:59")
            endT = TimeValue("2020/12/31 23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Set GetMissionInfo = ctmEmptyFiles
            Exit Function
        End If
        endTime = GetUnixTime(endD + endT)
        
        '*********************
        '* ミッションIDを取得
        '*********************
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "ミッションID項目が見つからないため処理を中断します。"
            Set GetMissionInfo = ctmEmptyFiles
            Exit Function
        End If
        If missionID = "" Then
            MsgBox "ミッションIDが見つからないため処理を中断します。"
            Set GetMissionInfo = ctmEmptyFiles
            Exit Function
        End If

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If
        If missionID = "" Then
            MsgBox "ミッションIDが見つからないため処理を中断します。"
            Set GetMissionInfo = ctmEmptyFiles
            Exit Function
        End If
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If

    End With
    
    '* CTM情報一覧をシートから取得
    Call GetCTMList(ParamSheetName, ctmList)
    Call GetKeyOrder(ParamSheetName, keyOrder)
        
    '* プログラムミッションに問合せ
    '* 取得開始日時を「現在時刻−表示期間−１分」として算出し問い合わせる
    getEndTime = endTime
    getStartTime = startTime
    SendString = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pm?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & "&limit=" & Format(LimitCTMNumber) & "&lang=ja&noBulky=true"
    
    If useProxy = False Then
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
    Else
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
        httpObj.setProxy 2, proxyUri
    End If
    
    httpObj.Open "GET", SendString, False
    httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
    httpObj.Send
    
    ' ダウンロード待ち
    Do While httpObj.readyState <> 4
        DoEvents
    Loop
    
    '* 取得情報のデコード
    GetString = httpObj.responseText
    Set GetMissionInfo = jsonParse.buildEachCtmCsvFileCtmMission(GetString, GetTimezoneId, False, keyOrder)
    Set httpObj = Nothing
    
  
End Function

'***********************************************
'* キー順を取得する
'***********************************************
Private Sub GetKeyOrder(currSheetName As String, keyOrder() As String)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim ctmRange        As Range
    Dim elmRange        As Range
    Dim currWorksheet   As Worksheet
    Dim ctmIndex        As Integer      ' CTM情報インデックス
    Dim elmIndex        As Integer      ' CTMID,エレメント情報インデックス
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    Dim maxRow          As Integer
    Dim maxCol          As Integer
    
    Set currWorksheet = Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim keyOrder(0)
    
    With currWorksheet
    
        '* CTM名称の項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        With srchRange.SpecialCells(xlCellTypeLastCell)
            maxRow = .Row - srchRange.Row + 1
            maxCol = .Column - srchRange.Column + 1
        End With
        
        ctmIndex = 0
        ReDim keyOrder(maxRow / 2, maxCol)
        
        For II = 0 To maxRow Step 2
            Set ctmRange = startRange.Offset(II, 0)
            elmIndex = 0
            
            If ctmRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            '* CTM情報取得
            keyOrder(ctmIndex, elmIndex) = ctmRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = ctmRange.Offset(0, 1)
            For JJ = 0 To maxCol - 1
                elmIndex = elmIndex + 1
                keyOrder(ctmIndex, elmIndex) = elmRange.Offset(1, JJ).Value
            Next
            ctmIndex = ctmIndex + 1
        Next
    End With
    
End Sub

'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Private Sub OutputCTMInfoToSheet(currSheetName As String, ctmValueFile As String)
    Dim currWorksheet   As Worksheet
    Dim prevWorksheet   As Worksheet
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim delRange        As Range
    Dim dataRange       As Range
    Dim jsonParse As New FoaCoreCom.jsonParse
    
    Set prevWorksheet = ActiveSheet
        
    '* 出力先シートの取得
    Set currWorksheet = Worksheets(currSheetName)
    currWorksheet.Activate
    
    With currWorksheet
        
        '* 書き出し位置の設定
        Set startRange = .Cells(4, 1)
        
        '* 全情報クリア
        Set lastRange = startRange.End(xlDown)
        Set delRange = .Range("4:" & Format(lastRange.Row))
        delRange.Delete
        
        
        Dim FileSize As Long
        FileSize = FileLen(ctmValueFile)

        If FileSize > 0 Then
            '* 書き出し位置の設定
            Set startRange = .Cells(4, 1)
                    
            With ActiveSheet.QueryTables.Add(Connection:="Text;" + ctmValueFile, Destination:=Range("A4"))
                .TextFileCommaDelimiter = True
                .TextFilePlatform = 65001
                .Refresh BackgroundQuery:=False
                .Delete
            End With
            
            Set lastRange = startRange.End(xlDown)
            Set dataRange = .Range("4:" & Format(lastRange.Row))
            dataRange.Sort key1:=Range("A4"), order1:=xlDescending, Header:=xlNo
        End If
        
       
    End With
    
    prevWorksheet.Activate
End Sub

'***********************************************
'* メインシートにあるミッション情報を取得する
'***********************************************
Public Sub GetCTMList(currSheetName As String, ctmList() As CTMINFO)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim ctmRange        As Range
    Dim elmRange        As Range
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim aryIndex        As Integer      ' CTM情報インデックス
    Dim aryIndex2       As Integer      ' エレメント情報インデックス
    Dim iFirstFlag      As Boolean      ' 配列初回フラグ
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    
    iFirstFlag = True
    
    Set currWorksheet = Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim ctmList(0)
    
    With currWorksheet
    
'        .Activate
        
        '* CTM NAMEの項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        
        For II = 0 To MaxCTMNumber Step 2
            Set ctmRange = startRange.Offset(II, 0)
            
            If ctmRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            If iFirstFlag Then
                aryIndex = 0
                ReDim ctmList(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(ctmList) + 1
                ReDim Preserve ctmList(aryIndex)
            End If
            ReDim ctmList(aryIndex).Element(0)
            ctmList(aryIndex).Element(0).Name = ""
            
            '* CTM情報取得
            ctmList(aryIndex).Name = ctmRange.Offset(0, 0).Value
            ctmList(aryIndex).ID = ctmRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = ctmRange.Offset(0, 1)
            For JJ = 0 To MaxELMNumber
                If elmRange.Offset(0, JJ).Value = "" Then
                    Exit For
                End If
                
                aryIndex2 = UBound(ctmList(aryIndex).Element)
                If ctmList(aryIndex).Element(aryIndex2).Name <> "" Then
                    aryIndex2 = aryIndex2 + 1
                    ReDim Preserve ctmList(aryIndex).Element(aryIndex2)
                End If
                ctmList(aryIndex).Element(aryIndex2).Name = elmRange.Offset(0, JJ).Value
                ctmList(aryIndex).Element(aryIndex2).ID = elmRange.Offset(1, JJ).Value
            Next
            
                
        Next
    
    End With
    
End Sub





