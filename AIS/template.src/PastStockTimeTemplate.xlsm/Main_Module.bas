Attribute VB_Name = "Main_Module"
Option Explicit

'****************************************************************************************************
'*******************
'* 自動更新スタート
'*******************
Public Sub AutoUpdateStart()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    
    '* メニュー制御
    Call MenuCtrlEnableOrDisable("グラフ更新", False)
    Call MenuCtrlEnableOrDisable("テスト", False)
    Call MenuCtrlEnableOrDisable("自動更新", False)
    Call MenuCtrlEnableOrDisable("更新停止", True)

    Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "ON"
    
    Call TimerStart

End Sub

'****************************
'* 自動更新を停止する
'****************************
Public Sub AutoUpdateStop()
    Dim currWorksheet   As Worksheet
    Dim mainWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim prevDate        As Date
    Dim prevTime        As Date
    
    Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = "OFF"
    
        '* 予約したスケジュールをキャンセルする
        Call CancelSchedule
    
        '* メニュー制御
        Call MenuCtrlEnableOrDisable("グラフ更新", True)
        Call MenuCtrlEnableOrDisable("テスト", True)
        Call MenuCtrlEnableOrDisable("自動更新", True)
        Call MenuCtrlEnableOrDisable("更新停止", False)
    
        Call UpdateFormStatus
        
    End If
    
End Sub

'*************************************
'* ｢テスト｣の処理
'*************************************
Public Sub OperationTest()
    Dim currWorksheet       As Worksheet
    Dim dataNum             As Integer
    Dim hStepValue          As Variant
    Dim hPrmRange           As Range
    Dim rc                  As Variant
    Dim srchRange           As Range
    Dim sampleType          As String
    Dim wrksTime             As String
    Dim wrkeTime            As String
    
'    rc = MsgBox("テストを開始しますか？", vbYesNo)
'    If rc = vbNo Then
'        Exit Sub
'    End If

    '前回／次回更新日時セット
    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd hh:mm:ss")
    End If
    
'    '* DAC-87 sunyi 20181228 start
'    '* OFFLINEの場合、Testできないようにする
'    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="オンライン", LookAt:=xlWhole)
'    If Not srchRange Is Nothing Then
'        If srchRange.Offset(0, 1).Value = "OFFLINE" Then
'            Exit Sub
'        End If
'    End If
'    '* DAC-87 sunyi 20181228 end
    
    '追加20160606*******************************************
    sampleType = ""
    Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
    '*******************************************************
    '取得開始・取得終了→表示開始期間・表示終了期間にコピー
    Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="取得開始", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        wrksTime = srchRange.Offset(0, 1).Value
    End If
    Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="取得終了", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        wrkeTime = srchRange.Offset(0, 1).Value
    End If
    
    Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="表示開始期間", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = wrksTime
    End If
    Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="表示終了期間", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = wrkeTime
    End If
    '*******************************************************
    
    If sampleType <> "GRIP" Then
        '* ミッションから情報を取得
        Call GetMissionInfoAll
    Else
        Call GetGripMission
    End If
    
    'グラフ更新処理
    'Call UpdateLoalData
    
    Call UpdatePivotTable
    
    Call UpdateAfterGraphEdit
    
    Call DispTermDispInterval

End Sub

'*************************************
'* ｢テスト｣の処理
'*************************************
Public Sub OperationTestXXX()
    Dim currWorksheet       As Worksheet
    Dim dataNum             As Integer
    Dim hStepValue          As Variant
    Dim hPrmRange           As Range
    Dim rc                  As Variant
    
    '* ミッションからの情報取得
    Call GetMissionInfoAll
    
    '* ピボットテーブルの更新
    Call UpdatePivotTable

    '* グラフ編集シートのＸＹ軸の最大／最小／刻み幅を更新する
    Call PutMaxMinStepEditSheet2
    
    '* グラフ編集画面の設定を元にグラフ書式を更新
    Call UpdateAfterGraphEdit

End Sub

'*************************************
'* 最大最小取得の処理
'* ※AISから最初のエクセル起動にコールされるマクロ。
'*************************************
Public Sub CallBeforeExcelFromAIS()
    Dim currWorksheet       As Worksheet
    Dim dataNum             As Integer
    Dim hStepValue          As Variant
    Dim hPrmRange           As Range
    Dim srchRange           As Range
    
    '* グラフテーブルの更新
    '* →paramシートの表示開始／終了期間を用いる
    Call UpdatePivotTable
    
    Call UpdateAfterGraphEdit
    
    Call DispTermDispInterval
    
End Sub

'*************************************
'* グラフ編集シート上の値更新後の処理
'*************************************
Public Sub UpdateLoalData()
    Dim wrkRange            As Range
    Dim currChartObj        As ChartObject
    Dim wrkStr              As String
    Dim Y_Max               As Integer
    Dim Y_Min               As Integer
    Dim Y_Kizami            As Integer

    '　ThisWorkbook名称取込
    Call SetFilenameToVariable

    '****** 刻み時間変更処理
    '* 表示刻みの取込
    Set wrkRange = ThisWorkbook.Worksheets(GraphEditSheetName).Cells.Find(What:="表示刻み[時間]", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        wrkStr = Format(wrkRange.Offset(0, 1).Value, "hh:mm:ss")
    End If
    
    '* 表示刻みのセット
    Set wrkRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="周期", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        wrkRange.Offset(0, 1).Value = Hour(wrkStr) * 60 + Minute(wrkStr)
    End If
    '******
    
    'グラフ再描画
    Call UpdatePivotTable

        '凡例、背景ﾃﾞｰﾀ作成
    Call HaikeiHanreiDataMake
    
    Call GraphReCalculate
    
    
    ''ThisWorkbook.Worksheets(GraphEditSheetName).Range("A1").Select
    
End Sub

'*******************
'* 状態フォーム更新
'*******************
Public Sub UpdateFormStatus()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim flagON          As Boolean
    Dim f               As Variant
    Dim isNotForm       As Boolean
    Dim FinalTime       As Date
    Dim endD            As Date
    Dim endT            As Date
    Dim currShape       As Shape
    Dim btSize          As RECT
    Dim RCAddress       As String
    Dim currRange       As Range
    
    '* 自動更新状態を「pram」シートから取得
    Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "ON" Then
            flagON = True
        Else
            flagON = False
        End If
    End If
    
    '* 収集終了日時を「オンラインテンプレート」シートから取得
    Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="取得終了", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        endD = srchRange.Offset(0, 1).Value
        endT = srchRange.Offset(0, 2).Value
    Else
        endD = CDate("2020/12/31")
        endT = CDate("23:59:59")
    End If
    If Not IsDate(endD + endT) Then
        MsgBox "収集終了日時欄が正しくないため処理を中断します。"
        Exit Sub
    End If
    FinalTime = endD + endT

    '* 表示テキストボックスを確認し、情報を表示する
    Set currWorksheet = Worksheets(FixGraphSheetName)
    isNotForm = True
    For Each currShape In currWorksheet.Shapes
        If currShape.Name = PrefixStrTX & "ONLINE" Then
            isNotForm = False
            If flagON Then
                currShape.TextFrame2.TextRange.Characters.text = "更新中"
                currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 255)
            Else
                currShape.TextFrame2.TextRange.Characters.text = "停止中"
                currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(255, 0, 0)
            End If
        End If
    Next currShape
    '* 表示テキストボックスがなければ追加する
    If isNotForm Then
        '* 設定されている表示範囲のアドレスを取得し、範囲を設定する
        RCAddress = GetDisplayAddress(ActiveSheet)
        If RCAddress <> "" Then
            Set currRange = ActiveSheet.Range(RCAddress)
            '* 更新状況テキストボックスを追加
            btSize.Top = currRange.Top + 1
            btSize.Left = currRange.Left + 25
            btSize.Right = 72
            btSize.Bottom = 24
            Set currShape = PutTextBoxAtSheet(currWorksheet, btSize, RGB(0, 0, 0), RGB(255, 255, 255), PrefixStrTX, "ONLINE", "")
            If flagON Then
                currShape.TextFrame2.TextRange.Characters.text = "更新中"
                currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 255)
            Else
                currShape.TextFrame2.TextRange.Characters.text = "停止中"
                currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(255, 0, 0)
            End If
        End If
    End If

    '* ユーザフォームの有無を確認し、情報を表示する
'    isNotForm = True
'    For Each f In UserForms
'        If TypeOf f Is AutoUpdateForm Then
'            isNotForm = False
'            With AutoUpdateForm
'                .LB_Info.Caption = "取得終了日時：" & FinalTime
'                With .LB_OnOff
'                    If flagON Then
'                        .Caption = "更新中"
'                        .ForeColor = RGB(0, 0, 255)
'                    Else
'                        .Caption = "停止中"
'                        .ForeColor = RGB(255, 0, 0)
'                    End If
'                End With
'            End With
'        End If
'    Next f
'    '* ユーザフォームなかったら再度表示
'    If isNotForm Then
'        Load AutoUpdateForm
'        With AutoUpdateForm
'            .LB_Info.Caption = "取得終了日時：" & FinalTime
'            With .LB_OnOff
'                If flagON Then
'                    .Caption = "更新中"
'                    .ForeColor = RGB(0, 0, 255)
'                Else
'                    .Caption = "停止中"
'                    .ForeColor = RGB(255, 0, 0)
'                End If
'            End With
'        End With
'        AutoUpdateForm.Show vbModeless
'    End If
End Sub

'*************************************
'* 最大最小取得の処理
'* ※AISからコールされるマクロ。【ミッション取得は実行しない】
'*************************************
Public Sub CallMaxMinValueFromAIS()
    Dim currWorksheet       As Worksheet
    Dim dataNum             As Integer
    Dim hStepValue          As Variant
    Dim hPrmRange           As Range
    
    '* ピボットテーブルの更新
    Call UpdatePivotTable
    
'    '* 横軸のステップ幅を取得する
'    Set hPrmRange = Worksheets(PivotSheetName).Cells.Find(What:="横パラメータ", LookAt:=xlWhole)
'    If Not hPrmRange Is Nothing Then
'        hStepValue = hPrmRange.Offset(1, 3).Value
'    Else
'        hStepValue = 10
'    End If
'
'    '* グラフ用データの作成
'    dataNum = goMakeDataForGraph(hStepValue)

    '* グラフ編集シートの最大／最小／刻み幅を更新する
    Call PutMaxMinStepEditSheet
    
    Call UpdateAfterGraphEdit

End Sub

'***************************************************
'* グラフ編集シートから最大／最小／刻み幅を取得する
'***************************************************
Public Sub GetMaxMinStepValue(currSheetName As String, vValue As MAXMINSTEPINFO, hValue As MAXMINSTEPINFO)
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range

    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Set currWorksheet = Worksheets(currSheetName)
    With currWorksheet
        Set srchRange = .Cells.Find(What:=VerticalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            vValue.MaxValue = srchRange.Offset(1, 1).Value
            vValue.MinValue = srchRange.Offset(2, 1).Value
            vValue.StepValue = srchRange.Offset(3, 1).Value
        End If
        
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            hValue.MaxValue = DateValue(srchRange.Offset(1, 1).Value) + TimeValue(srchRange.Offset(1, 1).Value)
            hValue.MinValue = DateValue(srchRange.Offset(2, 1).Value) + TimeValue(srchRange.Offset(2, 1).Value)
            hValue.StepValue = srchRange.Offset(3, 1).Value
        End If
    End With
        

End Sub

'*************************************
'* グラフ編集シート上の値更新後の処理
'*************************************
Public Sub UpdateAfterGraphEdit()
    Dim currWorksheet       As Worksheet
    Dim currChartObj        As ChartObject
    Dim currPivotTable      As PivotTable
    Dim dataNum             As Integer
    Dim hValue              As MAXMINSTEPINFO
    Dim vValue              As MAXMINSTEPINFO
    Dim vValueOld           As MAXMINSTEPINFO
    
    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Call GetMaxMinStepValue(GraphEditSheetName, vValueOld, hValue)

    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set currChartObj = currWorksheet.ChartObjects(HistGraph02Name)
'    Call SetGraphFormat(currChartObj)

    '* グラフの縦軸の最大値／最小値／ステップを再設定
    Call SetGraphParamY(currChartObj, CDbl(vValueOld.MaxValue), CDbl(vValueOld.MinValue), CDbl(vValueOld.StepValue))

    '* グラフの横軸の最大値／最小値／ステップを再設定
    Call SetGraphParamX(currChartObj, CDbl(hValue.MaxValue), CDbl(hValue.MinValue), CDbl(hValue.StepValue))

    Set currWorksheet = Worksheets(FixGraphSheetName)
    Set currChartObj = currWorksheet.ChartObjects(HistGraph02Name)
'    Call SetGraphFormat(currChartObj)

    '* グラフの縦軸の最大値／最小値／ステップを再設定
    Call SetGraphParamY(currChartObj, CDbl(vValueOld.MaxValue), CDbl(vValueOld.MinValue), CDbl(vValueOld.StepValue))

    '* グラフの横軸の最大値／最小値／ステップを再設定
    Call SetGraphParamX(currChartObj, CDbl(hValue.MaxValue), CDbl(hValue.MinValue), CDbl(hValue.StepValue))
        
    '* 基準線を再配置する
    Call GraphReCalculate

End Sub

Public Sub SetGraphFormat(currChartObj As ChartObject)

    With currChartObj.Chart
        .PlotArea.Height = currChartObj.Width - 25
        .PlotArea.Top = 25
        .PlotArea.Width = currChartObj.Width - 75
        .Axes(xlValue).TickLabels.NumberFormatLocal = "G/標準"
    End With

End Sub

'*************************************
'* ピボットテーブル（メイン／サブ）を生成する 在庫では使用しない。
'*************************************
Private Sub goMakePivotTable()
    Dim workTime        As String
    Dim workType        As String
    Dim workStr         As String
    Dim pvtWorksheet    As Worksheet
    Dim currWorksheet   As Worksheet
    Dim lastRange       As Range
    Dim dataRange       As Range
    Dim currRange       As Range
    Dim srchRange       As Range
    Dim hRange          As Range
    Dim hPrmRange       As Range
    Dim workDbl         As Double
    Dim hMaxValue       As Variant
    Dim hMinValue       As Variant
    Dim hStepValue      As Variant
    Dim aveValue        As Variant
    Dim sigValue        As Variant
    Dim divValue        As Variant
    
    Set currWorksheet = Worksheets(ForPivotSheetName)
    Set pvtWorksheet = Worksheets(PivotSheetName)
    
    With currWorksheet
        '* 集計対象時間列項目名
        workTime = currWorksheet.Range(ADDR_WorkTimeStartPos).Value
        '* 集計対象系列列項目名
        workType = currWorksheet.Range(ADDR_SeriesStartPos).Value
    
        '* ピボットテーブルのデータソース範囲の取得
        Set lastRange = .Range(ADDR_SeriesStartPos).End(xlDown)
        Set dataRange = .Range(.Cells(1, 1), .Cells(lastRange.Row, lastRange.Column))
'        dataRange.Name = "ピボットデータ範囲"
    End With
    workStr = ForPivotSheetName & ADDR_MainPivotTable & ":R" & lastRange.Row & "C4"
    
    '* メインピボットテーブル作成
    Call MakePivotTable(PivotMainTableName, PivotSheetName & ADDR_MainPivotTable, workStr, workTime, workType)
    '* サブピボットテーブル作成
    Call MakePivotTableSub(PivotSubTableName, PivotSheetName & ADDR_SubPivotTable, workStr, workTime)
    
    '* 横パラメータの最大値／最小値／横軸刻み幅を取得
    With pvtWorksheet
        Set hPrmRange = .Cells.Find(What:="横パラメータ", LookAt:=xlWhole)
        Set hRange = .Cells.Find(What:="行ラベル", LookAt:=xlWhole)
        If Not hRange Is Nothing Then
            hMaxValue = hRange.Offset(1, 1).Value
            hMinValue = hRange.Offset(1, 2).Value
            aveValue = hRange.Offset(1, 3).Value
            sigValue = hRange.Offset(1, 4).Value
            divValue = hRange.Offset(1, 5).Value
        Else
            hMaxValue = .Range("J18").Value
            hMinValue = .Range("L18").Value
            aveValue = .Range("M18").Value
            sigValue = .Range("N18").Value
            divValue = .Range("O18").Value
        End If
        If Not hPrmRange Is Nothing Then
            hStepValue = hPrmRange.Offset(1, 3).Value
        Else
            hStepValue = .Range("M12").Value
        End If
        '* 横軸：刻み幅を取得
        If hStepValue = 0 Then
            Set currRange = Worksheets(ParamSheetName).Cells.Find(What:="横表示刻み", LookAt:=xlWhole)
            If Not currRange Is Nothing Then
                hStepValue = currRange.Offset(0, 1).Value
                If hStepValue = 0 Then
                    hStepValue = Round((hMaxValue - hMinValue) / 10)
                End If
            Else
                hStepValue = Round((hMaxValue - hMinValue) / 10)
            End If
        End If
        hStepValue = Application.WorksheetFunction.Ceiling(hStepValue, 10)
        '* 横軸：刻み幅をセット
        hPrmRange.Offset(1, 3).Value = hStepValue
    End With
    
    '* 横パラメータの最大／最小／平均値のparamシートに対する更新
    With pvtWorksheet
        hMaxValue = Int(CDbl(hMaxValue) / hStepValue)
        hMaxValue = (hMaxValue + 1) * hStepValue
        hPrmRange.Offset(1, 1).Value = hMaxValue
        hMinValue = Int(CDbl(hMinValue) / hStepValue)
        hMinValue = (hMinValue) * hStepValue
        hPrmRange.Offset(1, 2).Value = hMinValue
    End With
    
    '* 最大／最小／刻み幅および平均／標準偏差／分散のグラフ編集シートに対する更新
    With Worksheets(GraphEditSheetName)
        Set srchRange = .Range("A:L")
        Set currRange = srchRange.Cells.Find(What:="平均", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.Offset(0, 1).Value = aveValue
            currRange.Offset(0, 3).Value = sigValue
            currRange.Offset(0, 5).Value = divValue
        End If
        Set currRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.Offset(1, 1).Value = hMaxValue
            currRange.Offset(2, 1).Value = hMinValue
            currRange.Offset(3, 1).Value = hStepValue
        End If
    End With
    
    '* 平均／標準偏差／分散のグラフシートに対する更新
    With Worksheets(FixGraphSheetName)
        Set srchRange = .Range("A:L")
        Set currRange = srchRange.Cells.Find(What:="平均", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.Offset(0, 1).Value = aveValue
            currRange.Offset(0, 3).Value = sigValue
            currRange.Offset(0, 5).Value = divValue
        End If
    End With
End Sub

'*************************************
'* メインピボットテーブルを生成する
'*************************************
Public Sub MakePivotTable(pvtName As String, pvtPos As String, distData As String, workTime As String, workType As String)
    Dim workStr         As String
    Dim currPivotTable  As PivotTable
    Dim currPivotCache  As PivotCache

    '* ピボットテーブルの準備
    Set currPivotCache = ActiveWorkbook.PivotCaches.Create( _
              SourceType:=xlDatabase, _
              SourceData:=distData, _
              Version:=xlPivotTableVersion14)

    '* ピボットテーブルの配置（A1セル）
    Set currPivotTable = currPivotCache.CreatePivotTable( _
               TableDestination:=pvtPos, _
               TableName:=pvtName, _
               DefaultVersion:=xlPivotTableVersion14)

    '* フィールド設定
    With currPivotTable.PivotFields(workTime)
        .Orientation = xlRowField
        .Position = 1
        .ShowAllItems = True
    End With
    With currPivotTable.PivotFields(workType)
        .Orientation = xlColumnField
        .Position = 1
        .ShowAllItems = True
    End With
    currPivotTable.AddDataField currPivotTable.PivotFields("CTM名"), "データの個数 / CTM名", xlCount
    
    '* 表の項目の設定
    currPivotTable.CompactLayoutRowHeader = workTime
    currPivotTable.CompactLayoutColumnHeader = workType
    
    '* 個数がない場合は０を割り当てる
    currPivotTable.NullString = "0"
    
End Sub

'*************************************
'* サブピボットテーブルを生成する
'*************************************
Public Sub MakePivotTableSub(pvtName As String, pvtPos As String, distData As String, workTime As String)
    Dim workStr         As String
    Dim currPivotTable  As PivotTable
    Dim currPivotCache  As PivotCache

    '* ピボットテーブルの準備
    Set currPivotCache = ActiveWorkbook.PivotCaches.Create( _
              SourceType:=xlDatabase, _
              SourceData:=distData, _
              Version:=xlPivotTableVersion14)

    '* ピボットテーブルの配置（K1セル）
    Set currPivotTable = currPivotCache.CreatePivotTable( _
               TableDestination:=pvtPos, _
               TableName:=pvtName, _
               DefaultVersion:=xlPivotTableVersion14)

    '* フィールド設定
    With currPivotTable.PivotFields("CTM名")
        .Orientation = xlRowField
        .Position = 1
    End With
    
    '* 最大／最小／平均のフィールド設定
    workStr = "データの個数 / " & workTime
    currPivotTable.AddDataField currPivotTable.PivotFields(workTime), workStr, xlCount
    With currPivotTable.PivotFields(workStr)
        .Function = xlMax
        .Caption = "最大値"
    End With
    currPivotTable.AddDataField currPivotTable.PivotFields(workTime), workStr, xlCount
    With currPivotTable.PivotFields(workStr)
        .Function = xlMin
        .Caption = "最小値"
    End With
    currPivotTable.AddDataField currPivotTable.PivotFields(workTime), workStr, xlCount
    With currPivotTable.PivotFields(workStr)
        .Function = xlAverage
        .Caption = "平均"
    End With
    currPivotTable.AddDataField currPivotTable.PivotFields(workTime), workStr, xlCount
    With currPivotTable.PivotFields(workStr)
        .Function = xlStDev
        .Caption = "標準偏差"
    End With
    currPivotTable.AddDataField currPivotTable.PivotFields(workTime), workStr, xlCount
    With currPivotTable.PivotFields(workStr)
        .Function = xlVar
        .Caption = "分散"
    End With
    
End Sub

'*************************************
'* グラフ用のデータを生成する
'*************************************
Public Function goMakeDataForGraph(hStepValue) As Integer
    Dim pvtWorksheet    As Worksheet
    Dim prmWorksheet    As Worksheet
    Dim startRange      As Range
    Dim writeRange      As Range
    Dim dataRange       As Range
    Dim srchRange       As Range
    Dim currRange       As Range
    Dim vRange          As Range
    Dim hRange          As Range
    Dim dataNum         As Integer
    Dim currPivotTable  As PivotTable
    Dim pvtCol          As Integer
    Dim pvtRow          As Integer
    Dim hPrmRange       As Range

    Set pvtWorksheet = Worksheets(PivotSheetName)
    Set prmWorksheet = Worksheets(ParamSheetName)
    
    With pvtWorksheet
    
        Set currPivotTable = .PivotTables(PivotMainTableName)
    
        '* メインピボットテーブルのデータ開始行・列を取得
        pvtRow = currPivotTable.DataBodyRange.Row
        pvtCol = currPivotTable.rowRange.Column
    
        Set startRange = .Cells(pvtRow, pvtCol)
        Set dataRange = .Cells(pvtRow, pvtCol + 1)
        Set currRange = .Cells.Find(What:="グラフ用データ", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            Set writeRange = currRange.Offset(1, 0)
        Else
            Set writeRange = .Range("F2").Value
        End If
    
        '* グラフのためのデータを生成する
        dataNum = MakeDataForGraph(pvtWorksheet, startRange, dataRange, writeRange, hStepValue)
        
        goMakeDataForGraph = dataNum
    
    End With
    
End Function

'*************************************
'* グラフ作成用データ生成
'*************************************
Public Function MakeDataForGraph(currWorksheet As Worksheet, startRange As Range, dataRange As Range, writeRange As Range, hStepValue As Variant) As Integer
    Dim readIndex       As Integer
    Dim writeIndex      As Integer
    Dim tValue          As String
    Dim iValue          As Integer
    Dim workRange       As Range
    Dim currRange       As Range
    Dim hPrmRange       As Range
    Dim vRange          As Range
    Dim vPrmRange       As Range
    Dim MaxValue        As Variant
    Dim MinValue        As Variant
    Dim aveValue        As Variant
    Dim StepValue       As Variant
    
    '* 古いデータ削除
    readIndex = 0
    Set workRange = currWorksheet.Range(writeRange.Offset(0, 0), writeRange.End(xlDown).Offset(0, 1))
    workRange.ClearContents
    
    '* ピボットテーブルからグラフ用のテーブルへ値をコピーする
    readIndex = 0
    If Left(startRange.Offset(readIndex, 0).Value, 1) = "<" Then readIndex = readIndex + 1
    
    writeIndex = 0
    Do While (Left(startRange.Offset(readIndex, 0).Value, 1) <> ">") And _
             (Left(startRange.Offset(readIndex, 0).Value, 1) <> "<") And _
             (startRange.Offset(readIndex, 0).Value <> "総計") And _
             (startRange.Offset(readIndex, 0).Value <> "")
    
        '* ピボットテーブルから値を取得
        tValue = startRange.Offset(readIndex, 0).Value
        iValue = Val(Left(tValue, Len(tValue) - InStr(tValue, "-")))
        
        '* グラフ用表に転記する
        writeRange.Offset(writeIndex, 0).Value = iValue + (hStepValue / 2)
        writeRange.Offset(writeIndex, 0).NumberFormatLocal = "0_);[赤](0)"       ' 表示形式「数値」
        
        '* ピボットテーブルから値を取得
        tValue = dataRange.Offset(readIndex, 0).Value
        iValue = Val(tValue)
        
        '* グラフ用表に転記する
        writeRange.Offset(writeIndex, 1).Value = iValue
        writeRange.Offset(writeIndex, 1).NumberFormatLocal = "0_);[赤](0)"       ' 表示形式「数値」
        
        readIndex = readIndex + 1
        writeIndex = writeIndex + 1
        
        DoEvents
    Loop
    
    '* データ範囲から縦軸の最大値／最小値／平均値を求める
    With currWorksheet
        Set workRange = .Range(dataRange, dataRange.Offset(readIndex - 1, 0))
        MaxValue = Application.WorksheetFunction.Max(workRange)
        MinValue = Application.WorksheetFunction.Min(workRange)
        aveValue = Application.WorksheetFunction.Average(workRange)
    
        '* 縦軸の最大値／最小値／平均値を記載する
        Set vRange = .Cells.Find(What:="縦ラベル", LookAt:=xlWhole)
        If Not vRange Is Nothing Then
            vRange.Offset(1, 1).Value = MaxValue
            vRange.Offset(1, 2).Value = MinValue
            vRange.Offset(1, 3).Value = aveValue
        End If
    
        '* 縦パラメータ欄から刻み値を取得する
        Set vPrmRange = .Cells.Find(What:="縦パラメータ", LookAt:=xlWhole)
        If Not vPrmRange Is Nothing Then
            StepValue = vPrmRange.Offset(1, 3).Value
        Else
            StepValue = .Range("L10").Value
        End If
        '* 縦パラメータに刻み値の指定がない場合は、paramシートから取得し、その値も０ならば新規に算出する
        If StepValue = 0 Then
            Set currRange = Worksheets(ParamSheetName).Cells.Find(What:="縦表示刻み", LookAt:=xlWhole)
            If Not currRange Is Nothing Then
                StepValue = currRange.Offset(0, 1).Value
                If StepValue = 0 Then
                    StepValue = Round((MaxValue - MinValue) / 10)
                End If
            Else
                StepValue = Round((MaxValue - MinValue) / 10)
            End If
            StepValue = Application.WorksheetFunction.Ceiling(StepValue, 10)
            vPrmRange.Offset(1, 3).Value = StepValue
        End If
    
    End With
        
    '* 丸めた最大値／最小値を縦パラメータ欄にセットする
    MaxValue = Int(CDbl(MaxValue) / StepValue)
    MaxValue = (MaxValue + 1) * StepValue
    vPrmRange.Offset(1, 1).Value = MaxValue
    If MinValue <> 0 Then
        MinValue = Int(CDbl(MinValue) / StepValue)
        MinValue = (MinValue - 1) * StepValue
    End If
    If MinValue < 0 Then MinValue = 0
    vPrmRange.Offset(1, 2).Value = MinValue
    
    '* グラフ編集シートにも反映（ただし、元データが存在しない場合のみ）
    With Worksheets(GraphEditSheetName)
        Set currRange = .Cells.Find(What:=VerticalAxisName, LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            If currRange.Offset(1, 1).Value = "" Or currRange.Offset(1, 1).Value = 0 Then
                currRange.Offset(1, 1).Value = MaxValue
            End If
            If currRange.Offset(2, 1).Value = "" Then
                currRange.Offset(2, 1).Value = MinValue
            End If
            If currRange.Offset(3, 1).Value = "" Or currRange.Offset(3, 1).Value = 0 Then
                currRange.Offset(3, 1).Value = StepValue
            End If
        End If
    End With
    
    '* データ数を返す
    MakeDataForGraph = readIndex
    
End Function

'*************************************
'* グラフの設定更新
'*************************************
Public Sub ChangeGraphSetting(currWorksheet As Worksheet)
    Dim pvtWorksheet    As Worksheet
    Dim writeRange      As Range
    Dim workRange       As Range
    Dim lastRange       As Range
    Dim currRange       As Range
    Dim vRange          As Range
    Dim hRange          As Range
    Dim dataNum         As Integer
    Dim currChartObj    As ChartObject
    Dim maxY            As Double
    Dim minY            As Double
    Dim stepY           As Double
    Dim maxX                As Double
    Dim minX                As Double
    Dim stepX               As Double
    Dim workStr         As String
    
    Set pvtWorksheet = Worksheets(PivotSheetName)
    
    Set currChartObj = currWorksheet.ChartObjects(HistGraph02Name)

    With pvtWorksheet
        Set currRange = .Cells.Find(What:="グラフ用データ", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            Set writeRange = currRange.Offset(1, 0)
        Else
            Set writeRange = .Range("F2").Value
        End If
        Set lastRange = writeRange.End(xlDown)
    
        '* グラフの参照データを再設定する
        Set workRange = .Range(writeRange.Offset(0, 0), lastRange.Offset(0, 0))
        workStr = "=" & PivotSheetName & "!" & workRange.Address
        currChartObj.Chart.SeriesCollection(1).XValues = workStr
    
        Set workRange = .Range(writeRange.Offset(0, 1), lastRange.Offset(0, 1))
        workStr = "=" & PivotSheetName & "!" & workRange.Address
        currChartObj.Chart.SeriesCollection(1).Values = workStr
    '    Set workRange = pvtWorksheet.Range(writeRange.Offset(0, 3), lastRange.Offset(0, 3))
    '    workStr = "=" & PivotSheetName & "!" & workRange.Address
    '    currChartObj.Chart.SeriesCollection(2).Values = workStr
    '    Set workRange = pvtWorksheet.Range(writeRange.Offset(0, 2), lastRange.Offset(0, 2))
    '    workStr = "=" & PivotSheetName & "!" & workRange.Address
    '    currChartObj.Chart.SeriesCollection(3).Values = workStr
    
        '* グラフの最大／最小／ステップ値を取得
        Set vRange = .Cells.Find(What:="縦パラメータ", LookAt:=xlWhole)
        Set hRange = .Cells.Find(What:="横パラメータ", LookAt:=xlWhole)
        If Not vRange Is Nothing Then
            maxY = vRange.Offset(1, 1).Value
            minY = vRange.Offset(1, 2).Value
            stepY = vRange.Offset(1, 3).Value
        End If
        If Not hRange Is Nothing Then
            maxX = hRange.Offset(1, 1).Value
            minX = hRange.Offset(1, 2).Value
            stepX = hRange.Offset(1, 3).Value
        End If
    End With
    
    '* グラフの縦軸の最大値／最小値／ステップを設定
    Call SetGraphParamY(currChartObj, maxY, minY, stepY)
    
    '* グラフの縦軸の最大値／最小値／ステップを設定
    Call SetGraphParamX(currChartObj, maxX, minX, stepX)
    
End Sub

'***************************
'* 演算結果シートに転記する
'***************************
Public Sub PostToCalcResultSheet()
    Dim startTimeElmOrg     As String
    Dim endTimeElmOrg       As String
    Dim startTimeElm        As String
    Dim endTimeElm          As String
    Dim rapTimeElm          As String
    Dim typeElm             As String
    Dim ctmList()           As CTMINFO
    Dim srcWorksheet        As Worksheet
    Dim currWorksheet       As Worksheet
    
    Dim motoWorksheet       As Worksheet
    Dim sakiWorksheet       As Worksheet
    Dim psramWorksheet      As Worksheet
    
    Dim II                  As Integer
    Dim colIndex            As Integer
    Dim copyRange           As Range
    Dim destRange           As Range
    Dim srchRange           As Range
    Dim workRange           As Range
    Dim lastRange           As Range
    Dim startRange          As Range
    Dim endRange            As Range
    Dim rowIndex            As Integer
    Dim workLong            As Long
    Dim iFirstFlag          As Boolean
    Dim startCTM            As String
    Dim endCTM              As String
    
    Dim sampleType          As String
    Dim gripSheet           As String
    Dim gripList            As Variant
    Dim gripKaishi          As Variant
    Dim gripSyuryo          As Variant
    Dim rowRange            As Range
    
    '* paramシートから必要な情報を取得する
    With Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="開始エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then startTimeElmOrg = srchRange.Offset(0, 1).Value
        
        Set srchRange = .Cells.Find(What:="終了エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then endTimeElmOrg = srchRange.Offset(0, 1).Value
        
        Set srchRange = .Cells.Find(What:="エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then typeElm = srchRange.Offset(0, 1).Value
        If typeElm = "" Then typeElm = "CTM名"
        
        '追加20160606*******************************************
        sampleType = ""
        Set srchRange = .Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
        '*******************************************************
    End With
    
    '* ピボットテーブル生成用のシートへ取得CTMデータを必要なものだけ転記する
    '* currWorksheet:演算結果データ
    Set currWorksheet = Worksheets(ForPivotSheetName)
    
    '* 以前のデータを削除する
    With currWorksheet
        Set srchRange = currWorksheet.Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
        workRange.ClearContents
    End With
            
    '* CTM情報一覧をシートから取得
    Call GetCTMList(ParamSheetName, ctmList)
    
    '* 全てのワークシートに対して
    If sampleType <> "GRIP" Then
        iFirstFlag = True
        colIndex = 3
        For Each srcWorksheet In Worksheets
                    '* CTM情報一覧に存在するシートのみを処理対象とする
                For II = 0 To UBound(ctmList)
                    If (ctmList(II).Name = srcWorksheet.Name) Then Exit For
                Next
                
                If II <= UBound(ctmList) Then
                    '* RECEIVETIME／CTM名列を複写する
                    If iFirstFlag Then
                        Set copyRange = srcWorksheet.Range("A:B")
                        Set destRange = currWorksheet.Range("A:B")
                        copyRange.Copy Destination:=destRange
                        iFirstFlag = False
                    End If
                    
                    '* 対象種別列を複写する
        '            Set srchRange = srcWorksheet.Range("1:1")       ' １行目を検索範囲にセット
        '            Set workRange = srchRange.Find(What:=typeElm, LookAt:=xlWhole)
        '            If Not workRange Is Nothing Then
        '                Set copyRange = workRange.EntireColumn
        '                Set destRange = currWorksheet.Columns(colIndex)
        '                copyRange.Copy Destination:=destRange
        '                colIndex = colIndex + 1
        '            End If
                    
                    '* 開始時刻列を複写する
                    If startTimeElmOrg <> "" Then
                        startCTM = Left(startTimeElmOrg, InStr(startTimeElmOrg, "・") - 1)
                        If srcWorksheet.Name = startCTM Then
                            startTimeElm = Right(startTimeElmOrg, Len(startTimeElmOrg) - InStr(startTimeElmOrg, "・"))
                            Set srchRange = srcWorksheet.Range("1:1")
                            Set workRange = srchRange.Find(What:=startTimeElm, LookAt:=xlWhole)
                            If Not workRange Is Nothing Then
                                Set copyRange = workRange.EntireColumn
                                Set destRange = currWorksheet.Columns(colIndex)
                                copyRange.Copy Destination:=destRange
                                colIndex = colIndex + 1
                            End If
                        End If
                    End If
                    
                    '* 終了時刻列を複写する
                    If endTimeElmOrg <> "" Then
                        endCTM = Left(endTimeElmOrg, InStr(endTimeElmOrg, "・") - 1)
                        If srcWorksheet.Name = endCTM Then
                            endTimeElm = Right(endTimeElmOrg, Len(endTimeElmOrg) - InStr(endTimeElmOrg, "・"))
                            Set srchRange = srcWorksheet.Range("1:1")
                            Set workRange = srchRange.Find(What:=endTimeElm, LookAt:=xlWhole)
                            If Not workRange Is Nothing Then
                                Set copyRange = workRange.EntireColumn
                                Set destRange = currWorksheet.Columns(colIndex)
                                copyRange.Copy Destination:=destRange
                                colIndex = colIndex + 1
                            End If
                        End If
                    End If
                End If
            
        Next srcWorksheet
        
        '* 開始時刻がないCTMを削除する
        Set currWorksheet = Worksheets(ForPivotSheetName)
        With currWorksheet
            Set srchRange = .Range(.Range("A4"), .Range("A4").End(xlDown)).Offset(0, 2)
            Call DeleteMultiLine("", srchRange)
        End With
    
        '* 2〜3行を削除（型／単位）
        Set workRange = currWorksheet.Range("2:2")
        workRange.ClearContents
        Set workRange = currWorksheet.Range("3:3")
        workRange.Delete
        
    Else
        '追加20160606 GRIPシート名を決定する。**************************************
        Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="シート一覧", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then gripSheet = srchRange.Offset(0, 1).Value
        gripList = Split(gripSheet, ",")
        
        '開始、終了エレメント取込
        gripKaishi = Split(startTimeElmOrg, "・")   ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
        gripSyuryo = Split(endTimeElmOrg, "・")     ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
        
        Set psramWorksheet = Worksheets(ParamSheetName)     'paramシート
        Set motoWorksheet = Worksheets(gripList(0))         'GRIPミッションシート
        Set sakiWorksheet = Worksheets(ForPivotSheetName)   '演算結果データ
        
        '開始CTM位置
        Set srchRange = motoWorksheet.Cells.Find(What:=gripKaishi(0), LookAt:=xlWhole)
        If srchRange Is Nothing Then
            Exit Sub
        End If
                   
        'CTM名称
        Set copyRange = motoWorksheet.Range("A:A")
        Set destRange = sakiWorksheet.Range("B:B")
        copyRange.Copy Destination:=destRange
        
        'CTM受信時間
        Set copyRange = motoWorksheet.Range("B:B")
        Set destRange = sakiWorksheet.Range("A:A")
        copyRange.Copy Destination:=destRange
        
        '開始エレメント位置
        Set rowRange = motoWorksheet.Cells.Find(What:=gripKaishi(1), LookAt:=xlWhole, SearchOrder:=xlByRows, after:=srchRange)
        If rowRange Is Nothing Then
            Exit Sub
        End If
        
        '開始エレメント列コピー
        Set copyRange = rowRange.EntireColumn
        Set destRange = sakiWorksheet.Range("C:C")
        copyRange.Copy Destination:=destRange
        
        '終了CTM位置
        Set srchRange = motoWorksheet.Cells.Find(What:=gripSyuryo(0), LookAt:=xlWhole)
        If srchRange Is Nothing Then
            Exit Sub
        End If
        
        '終了エレメント位置
        Set rowRange = motoWorksheet.Cells.Find(What:=gripSyuryo(1), LookAt:=xlWhole, SearchOrder:=xlByRows, after:=srchRange)
        If rowRange Is Nothing Then
            Exit Sub
        End If
        
        '終了エレメント列コピー
        Set copyRange = rowRange.EntireColumn
        Set destRange = sakiWorksheet.Range("D:D")
        copyRange.Copy Destination:=destRange
        
        '* 開始時刻がないCTMを削除する
        Set currWorksheet = Worksheets(ForPivotSheetName)
        With currWorksheet
            Set srchRange = .Range(.Range("A3"), .Range("A3").End(xlDown)).Offset(0, 0)
            Call DeleteMultiLine("", srchRange)
        End With
    
        '* 2〜3行を削除（型／単位）
        Set workRange = sakiWorksheet.Range("2:2")
        workRange.ClearContents
        
        gripSheet = gripList(0)
        '* AISTEMP No.102 sunyi 2018/11/22 start
'        Call NullDelete(gripSheet)
        '* AISTEMP No.102 sunyi 2018/11/22 end
        '***************************************************************************
    End If
End Sub

'******************************************
'* 指定範囲内にある対象文字列行を削除する
'******************************************
Public Sub DeleteMultiLine(delStr As String, srchRange As Range)
    Dim currWorksheet   As Worksheet
    Dim workRange       As Range
    Dim strAddress      As String
    Dim delRange        As Range

    Set currWorksheet = Worksheets(ForPivotSheetName)
    With currWorksheet
        '* 削除対象セル範囲を取得
        Set workRange = srchRange.Find(What:=delStr, LookAt:=xlWhole)
        If Not workRange Is Nothing Then
            Set delRange = workRange
            strAddress = workRange.Address
            Do While Not workRange Is Nothing

                Set workRange = srchRange.FindNext(workRange)
                If strAddress = workRange.Address Then
                    Exit Do
                End If
                
                '* 対象セルを集める
                Set delRange = Union(delRange, workRange)
            
            Loop
            delRange.EntireRow.Delete
        End If
    End With
    
End Sub

'*************************************
'* 演算結果シートに時刻項目を作成する
'*************************************
Public Sub MakeTimeAxisTitle(writeRange As Range, dtStart As String, dtEnd As String, dtStepSize As Integer)
    Dim TimeArray()             As Date
    Dim dtDiff                  As Variant
    Dim wSec                    As Variant
    Dim wMin                    As Variant
    Dim wHour                   As Variant
    Dim wDay                    As Variant
    Dim axisNum                 As Integer
    Dim II                      As Integer
    Dim lastRange               As Range
    Dim srchRange               As Range
    Dim writeTimeRange          As Range
    Dim currWorksheet           As Worksheet
    
    '* 日時の差分を秒単位で得る
    dtDiff = DateDiff("s", dtStart, dtEnd)
    
    '* 軸の項目数を得る
    axisNum = Int(dtDiff / dtStepSize) + 1
    
    ReDim TimeArray(axisNum)
    
    For II = 0 To UBound(TimeArray) - 1
        TimeArray(II) = DateAdd("s", CDbl(dtStepSize) * CDbl(II), dtStart)
    Next
    
    Set currWorksheet = Worksheets(ForPivotSheetName)
    With currWorksheet
        Set srchRange = .Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set lastRange = srchRange.Range("D1")   '20161109 ADD
        
        Set writeRange = lastRange.Offset(0, 1)
        Set writeTimeRange = .Range(writeRange, lastRange.Offset(0, axisNum))
        
        writeTimeRange = TimeArray
        writeTimeRange.NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
    End With
    
End Sub

'*********************************
'* 演算結果シートに数式を埋め込む
'*********************************
Public Sub MakeMathFormula(writeRange As Range)
    Dim currWorksheet           As Worksheet
    Dim lastRange               As Range
    Dim startRange              As Range
    Dim writeSumRange           As Range
    Dim writeFlagRange          As Range
    Dim lastRow                 As Long
    Dim lastCol                 As Long
    Dim workStr                 As String

    Set currWorksheet = Worksheets(ForPivotSheetName)
    
    With currWorksheet
    
        Set lastRange = writeRange.End(xlToRight)
        Set writeSumRange = .Range(writeRange.Offset(1, 0), lastRange.Offset(1, 0))
        
         If IsEmpty(.Range("A3").Value) Then
           lastRow = 0
        Else
            lastRow = .Range("A3").End(xlDown).Row
            lastCol = lastRange.Column
            
            '* サマリを埋め込む
            workStr = "=SUM(R[" & 1 & "]C:R[" & lastRow - 2 & "]C)"
            writeSumRange.FormulaR1C1 = workStr
            writeSumRange.NumberFormatLocal = "0"
            
            '* 時刻条件フラグ式を埋め込む
            Set writeFlagRange = .Range(writeRange.Offset(2, 0), lastRange.Offset(lastRow - 1, 0))
            workStr = "=IF(AND(RC3<=R1C,RC4=""""),1,IF(AND(RC3<=R1C,RC4>R1C),1,0))"
            writeFlagRange.FormulaR1C1 = workStr
            writeFlagRange.NumberFormatLocal = "0"
        End If
        
        
    End With
    
End Sub

'*************************************
'* グラフのデータソースを変更
'*************************************
Public Sub ChangeChartData(writeRange As Range)
    Dim graphEditWorksheet      As Worksheet
    Dim fixGraphWorksheet       As Worksheet
    Dim graphDataWorksheet      As Worksheet
    Dim currChartObj            As ChartObject
    Dim wrkRange                As Range
    Dim dataXRange              As Range
    Dim dataYRange              As Range
    Dim currSeries              As Series
    Dim updateNG                As Boolean
    
    Dim X_Max                   As Date
    Dim X_Min                   As Date
    Dim X_Kizami                As Date
    Dim Y_Max                   As Integer
    Dim Y_Min                   As Integer
    Dim Y_Kizami                As Integer
    
    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    Set fixGraphWorksheet = Worksheets(FixGraphSheetName)
    
    Set graphDataWorksheet = Worksheets(ForPivotSheetName)
    With graphDataWorksheet
        Set dataXRange = .Range(writeRange.Cells, writeRange.End(xlToRight).Cells)
        Set dataYRange = .Range(writeRange.Offset(1, 0).Cells, writeRange.Offset(1, 0).End(xlToRight).Cells)
    End With
    
    '縦軸　最大・最小値
    DoEvents
    Set wrkRange = writeRange.End(xlToRight).Offset(1, 0)
    Y_Max = Application.Max(Sheets(ForPivotSheetName).Range("E2:" & Replace(wrkRange.Address, "$", "")))
    Y_Min = Application.Min(Sheets(ForPivotSheetName).Range("E2:" & Replace(wrkRange.Address, "$", "")))
    
    
    '20161109
    If Y_Max <= Y_Min Then
        Y_Max = Y_Min + 10
    End If
    
    If Y_Kizami <= 0 Then
        Y_Kizami = 1
    End If
        
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示Max", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        If wrkRange.Offset(0, 1).Value = "" Or wrkRange.Offset(0, 1).Value = 0 Then
            wrkRange.Offset(0, 1).Value = Y_Max
        Else
            Y_Max = wrkRange.Offset(0, 1).Value
        End If
    End If
    
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示Min", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        If wrkRange.Offset(0, 1).Value = "" Or wrkRange.Offset(0, 1).Value = 0 Then
            wrkRange.Offset(0, 1).Value = Y_Min
        Else
            Y_Min = wrkRange.Offset(0, 1).Value
        End If
    End If
    
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示刻み", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        If wrkRange.Offset(0, 1).Value = "" Or wrkRange.Offset(0, 1).Value = 0 Then
            wrkRange.Offset(0, 1).Value = Application.RoundUp(Y_Max / 10, 0)
            Y_Kizami = Application.RoundUp(Y_Max / 10, 0)
            If Y_Kizami <= 0 Then
                DoEvents
                Y_Kizami = 1
            End If
        Else
            Y_Kizami = wrkRange.Offset(0, 1).Value
        End If
    End If
    
    
    '横軸　最大・最小時刻＋刻み時間
    '表示開始日時→グラフ編集シート
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示最新時刻", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        X_Max = wrkRange.Offset(0, 1).Value
    End If
    
    '表示開始日時→グラフ編集シート
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示開始時刻", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        X_Min = wrkRange.Offset(0, 1).Value
    End If

    '表示刻み→グラフ編集シート
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示刻み[時間]", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        X_Kizami = wrkRange.Offset(0, 1).Value
    End If
    
    '* グラフ編集シートのグラフを更新
    Set currChartObj = graphEditWorksheet.ChartObjects(HistGraph02Name)
    '* 縦軸の最大／最小の設定
    currChartObj.Chart.Axes(xlValue).MinimumScale = Y_Min
    currChartObj.Chart.Axes(xlValue).MaximumScale = Y_Max
    currChartObj.Chart.Axes(xlValue).MajorUnit = Y_Kizami
    
    '* 横軸の最大／最小の設定
    currChartObj.Chart.Axes(xlCategory).MinimumScale = DateValue(X_Min) + TimeValue(X_Min)
    currChartObj.Chart.Axes(xlCategory).MaximumScale = DateValue(X_Max) + TimeValue(X_Max)
    currChartObj.Chart.Axes(xlCategory).MajorUnit = TimeValue(X_Kizami)
    
    With currChartObj.Chart
        '* グラフの系統が一つもなければ新規に作成する
        If .SeriesCollection.Count = 0 Then
            Call AddSeriesBaseLine(currChartObj, "系統1", "=" & ForPivotSheetName & "!" & dataXRange.Address, "=" & ForPivotSheetName & "!" & dataYRange.Address, RGB(74, 126, 187))
            Set currSeries = .SeriesCollection("系統1")
            currSeries.MarkerStyle = -4142
            With currSeries.Format.Line
                .Visible = msoTrue
                .Weight = 1.5
            End With
        Else
            updateNG = True
            For Each currSeries In .SeriesCollection
                If currSeries.Name = "系統1" Then
                    .SeriesCollection(1).XValues = "=" & ForPivotSheetName & "!" & dataXRange.Address
                    .SeriesCollection(1).Values = "=" & ForPivotSheetName & "!" & dataYRange.Address
                    updateNG = False
                End If
            Next currSeries
            If updateNG Then
                Call AddSeriesBaseLine(currChartObj, "系統1", "=" & ForPivotSheetName & "!" & dataXRange.Address, "=" & ForPivotSheetName & "!" & dataYRange.Address, RGB(74, 126, 187))
                Set currSeries = .SeriesCollection("系統1")
                currSeries.MarkerStyle = -4142
                With currSeries.Format.Line
                    .Visible = msoTrue
                    .Weight = 1.5
                End With
            End If
        End If
    End With
    
    '* グラフシートのグラフを更新
    Set currChartObj = fixGraphWorksheet.ChartObjects(HistGraph02Name)
    '* 縦軸の最大／最小の設定
    currChartObj.Chart.Axes(xlValue).MinimumScale = Y_Min
    currChartObj.Chart.Axes(xlValue).MaximumScale = Y_Max
    currChartObj.Chart.Axes(xlValue).MajorUnit = Y_Kizami
    
    '* 横軸の最大／最小の設定
    currChartObj.Chart.Axes(xlCategory).MinimumScale = CDbl(X_Min)
    currChartObj.Chart.Axes(xlCategory).MaximumScale = CDbl(X_Max)
    currChartObj.Chart.Axes(xlCategory).MajorUnit = TimeValue(X_Kizami)
    
    With currChartObj.Chart
        '* グラフの系統が一つもなければ新規に作成する
        If .SeriesCollection.Count = 0 Then
            Call AddSeriesBaseLine(currChartObj, "系統1", "=" & ForPivotSheetName & "!" & dataXRange.Address, "=" & ForPivotSheetName & "!" & dataYRange.Address, RGB(74, 126, 187))
            Set currSeries = .SeriesCollection("系統1")
            currSeries.MarkerStyle = -4142
            With currSeries.Format.Line
                .Visible = msoTrue
                .Weight = 1.5
            End With
        Else
            updateNG = True
            For Each currSeries In .SeriesCollection
                If currSeries.Name = "系統1" Then
                    .SeriesCollection(1).XValues = "=" & ForPivotSheetName & "!" & dataXRange.Address
                    .SeriesCollection(1).Values = "=" & ForPivotSheetName & "!" & dataYRange.Address
                    updateNG = False
                End If
            Next currSeries
            If updateNG Then
                Call AddSeriesBaseLine(currChartObj, "系統1", "=" & ForPivotSheetName & "!" & dataXRange.Address, "=" & ForPivotSheetName & "!" & dataYRange.Address, RGB(74, 126, 187))
                Set currSeries = .SeriesCollection("系統1")
                currSeries.MarkerStyle = -4142
                With currSeries.Format.Line
                    .Visible = msoTrue
                    .Weight = 1.5
                End With
            End If
        End If
    End With
    
End Sub

'*****************************************************
'* グラフの最大／最小／刻み幅をグラフ編集シートへ転記
'*****************************************************
Public Sub PutMaxMinStepEditSheet()
    Dim graphEditWorksheet      As Worksheet
    Dim currChartObj            As ChartObject
    Dim maxX                    As Variant
    Dim minX                    As Variant
    Dim stepX                   As Variant
    Dim maxY                    As Variant
    Dim minY                    As Variant
    Dim stepY                   As Variant
    Dim srchRange               As Variant

    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    
    With graphEditWorksheet
        
        Set currChartObj = .ChartObjects(HistGraph02Name)
    
        With currChartObj.Chart
            .Axes(xlValue).MajorUnitIsAuto = True ' 自動設定
            .Axes(xlValue).MinimumScaleIsAuto = True ' 自動設定
            .Axes(xlValue).MaximumScaleIsAuto = True ' 自動設定
            minY = .Axes(xlValue).MinimumScale
            maxY = .Axes(xlValue).MaximumScale
            stepY = .Axes(xlValue).MajorUnit
            .Axes(xlCategory).MajorUnitIsAuto = True ' 自動設定
            .Axes(xlCategory).MinimumScaleIsAuto = True ' 自動設定
            .Axes(xlCategory).MaximumScaleIsAuto = True ' 自動設定
            minX = .Axes(xlCategory).MinimumScale
            maxX = .Axes(xlCategory).MaximumScale
            stepX = .Axes(xlCategory).MajorUnit
        End With
        
        '* グラフ編集シートへ転記
        Set srchRange = .Cells.Find(What:=VerticalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(1, 1).Value = "" Then srchRange.Offset(1, 1).Value = maxY
            If srchRange.Offset(2, 1).Value = "" Then srchRange.Offset(2, 1).Value = minY
            If srchRange.Offset(3, 1).Value = "" Then srchRange.Offset(3, 1).Value = stepY
        End If
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(1, 1).Value = "" Then srchRange.Offset(1, 1).Value = Format(maxX, "yyyy/MM/dd hh:mm:ss")
            If srchRange.Offset(2, 1).Value = "" Then srchRange.Offset(2, 1).Value = Format(minX, "yyyy/MM/dd hh:mm:ss")
            If srchRange.Offset(3, 1).Value = "" Then srchRange.Offset(3, 1).Value = Format(Hour(stepX) & ":" & Minute(stepX) & ":" & Second(stepX), "hh:mm:ss")
        End If
    End With
End Sub

'*****************************************************
'* グラフの最大／最小／刻み幅をグラフ編集シートへ転記
'* ※Ｙ軸のみ更新。Ｘ軸は刻みを15分デフォルトで設定
'*****************************************************
Public Sub PutMaxMinStepEditSheet2()
    Dim graphEditWorksheet      As Worksheet
    Dim currChartObj            As ChartObject
    Dim maxX                    As Variant
    Dim minX                    As Variant
    Dim stepX                   As Variant
    Dim maxY                    As Variant
    Dim minY                    As Variant
    Dim stepY                   As Variant
    Dim srchRange               As Variant

    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    
    With graphEditWorksheet
        
        Set currChartObj = .ChartObjects(HistGraph02Name)
    
        With currChartObj.Chart
            .Axes(xlValue).MajorUnitIsAuto = True ' 自動設定
            .Axes(xlValue).MinimumScaleIsAuto = True ' 自動設定
            .Axes(xlValue).MaximumScaleIsAuto = True ' 自動設定
            minY = .Axes(xlValue).MinimumScale
            maxY = .Axes(xlValue).MaximumScale
            stepY = .Axes(xlValue).MajorUnit
            If stepY < 0.5 Then stepY = 0.5
'            .Axes(xlCategory).MajorUnitIsAuto = True ' 自動設定
'            .Axes(xlCategory).MinimumScaleIsAuto = True ' 自動設定
'            .Axes(xlCategory).MaximumScaleIsAuto = True ' 自動設定
'            minX = .Axes(xlCategory).MinimumScale
'            maxX = .Axes(xlCategory).MaximumScale
'            stepX = .Axes(xlCategory).MajorUnit
            stepX = "00:30:00"
        End With
        
        '* グラフ編集シートへ転記
        Set srchRange = .Cells.Find(What:=VerticalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(1, 1).Value = "" Then srchRange.Offset(1, 1).Value = maxY
            If srchRange.Offset(2, 1).Value = "" Then srchRange.Offset(2, 1).Value = minY
            If srchRange.Offset(3, 1).Value = "" Then srchRange.Offset(3, 1).Value = stepY
        End If
        
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
'            If srchRange.Offset(1, 1).Value = "" Then srchRange.Offset(1, 1).Value = Format(maxX, "yyyy/MM/dd hh:mm:ss")
'            If srchRange.Offset(2, 1).Value = "" Then srchRange.Offset(2, 1).Value = Format(minX, "yyyy/MM/dd hh:mm:ss")
            If srchRange.Offset(3, 1).Value = "" Then srchRange.Offset(3, 1).Value = Format(Hour(stepX) & ":" & Minute(stepX) & ":" & Second(stepX), "hh:mm:ss")
        End If
    End With
End Sub

'*****************************************************
'* グラフのX軸の最大／最小をグラフ編集シートへ転記
'*****************************************************
Public Sub PutMaxMinStepEditSheetNoStepNoY(dtStart As String, dtEnd As String)
    Dim graphEditWorksheet      As Worksheet
    Dim currChartObj            As ChartObject
    Dim maxX                    As Variant
    Dim minX                    As Variant
    Dim srchRange               As Variant

    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    
    With graphEditWorksheet
        
        '* グラフ編集シートへ転記
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 1).Value = dtEnd
            srchRange.Offset(2, 1).Value = dtStart
        End If
    End With
End Sub

'****************************************
'* 演算結果シートUPDATE from paramシート
'****************************************
Public Sub UpdatePivotTable()
    Dim workRange           As Range
    Dim writeRange          As Range
    Dim srchRange           As Range
    Dim dtStart             As String
    Dim dtEnd               As String
    Dim dtNow               As String
    Dim dtStepSize          As Integer
    Dim timeSerial          As Long
    Dim OnOffline           As String
    Dim dtInterval          As Double
    Dim workDate            As Date
    
    Dim wrkTime             As Date
    Dim wrkRange            As Range
    Dim wrkDbl              As Double
    Dim DspTerm             As Integer
    Dim DspDateTime         As Date
    Dim wrkStr              As String

    '* paramシートの情報から開始／終了時刻を算出する
    With Worksheets(ParamSheetName)
        Set srchRange = .Cells.Find(What:="オンライン", LookAt:=xlWhole)
        
        '2016/07/07 コメント
'        If Not srchRange Is Nothing Then
'            OnOffline = srchRange.Offset(0, 1).Value
'        Else
'            OnOffline = "OFFLINE"
'        End If
        '************************************************
        'オンライン処理無の為、全てOFFLINEとする
        OnOffline = "OFFLINE"
    
        '* オンラインとオフラインで開始日時の扱いが異なる
        If OnOffline = "ONLINE" Then
            '*********************
            '* 表示期間を取得
            '*********************
            Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                dtInterval = srchRange.Offset(0, 1).Value
            Else
                dtInterval = 1#
            End If
            
            '*********************
            '* 周期を取得(グラフデータの刻み幅)
            '*********************
            Set srchRange = .Cells.Find(What:="周期", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                dtStepSize = Format(CInt(Val(srchRange.Offset(0, 1).Value) * 60))
            Else
                dtStepSize = "60"
            End If
            
            '* グラフの表示終了日時(現日時)を求める(15分単位でまるめ)
            '*******************************時刻刻み分切上
            dtNow = Format(Now, "yyyy/MM/dd hh:mm:ss")
            
            wrkTime = Application.WorksheetFunction.Ceiling(Now, 1 / 96)
            dtEnd = Format(wrkTime, "yyyy/MM/dd hh:mm:ss")
            '*******************************
            
            '* グラフの表示開始日時を求める(15分単位でまるめ)
            '*******************************時刻刻み切下
            dtInterval = dtInterval * 3600 * (-1)
            workDate = DateAdd("s", dtInterval, CDate(dtEnd))
            
            wrkTime = Application.WorksheetFunction.Floor(workDate, 1 / 96)
            dtStart = Format(wrkTime, "yyyy/MM/dd hh:mm:ss")
            '*******************************
            
            '* グラフ編集シートに最大／最小を転記する
            Call PutMaxMinStepEditSheetNoStepNoY(dtStart, dtEnd)
    
        Else
            '*********************
            '* 表示開始期間を取得
            '*********************
            Set srchRange = .Cells.Find(What:="表示開始期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                dtStart = srchRange.Offset(0, 1).Value
            Else
                dtStart = "2016/1/1 00:00:00"
            End If
            
            If Not IsDate(dtStart) Then
                MsgBox "取得開始日時が正しくないため処理を中断します。"
                Exit Sub
            End If
            
            '*********************
            '* 表示終了期間を取得
            '*********************
            Set srchRange = .Cells.Find(What:="表示終了期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                dtEnd = srchRange.Offset(0, 1).Value
            Else
                dtEnd = "2020/12/31 23:59:59"
            End If
            If Not IsDate(dtEnd) Then
                MsgBox "取得終了時刻が正しくないため処理を中断します。"
                Exit Sub
            End If
            
            '**********************************************************************↓
            'グラフ編集シートに表示開始・終了日時を設定
            Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                DspTerm = srchRange.Offset(0, 1).Value * -1
            Else
                DspTerm = -6
            End If
            DspDateTime = DateAdd("h", DspTerm, CDate(dtEnd))
        
            '表示開始日時→グラフ編集シート
            Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示最新時刻", LookAt:=xlWhole)
            If Not wrkRange Is Nothing Then
                wrkRange.Offset(0, 1).Value = Format(dtEnd, "yyyy/mm/dd hh:mm:ss")
            End If
            
            '表示開始日時→グラフ編集シート
            Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示開始時刻", LookAt:=xlWhole)
            If Not wrkRange Is Nothing Then
                wrkRange.Offset(0, 1).Value = Format(DspDateTime, "yyyy/mm/dd hh:mm:ss")
            End If
            
            '* 初期デフォルト間隔は30分
            dtStepSize = 30 * 60
            '表示開始日時→グラフ編集シート
            Set wrkRange = Worksheets(ParamSheetName).Cells.Find(What:="周期", LookAt:=xlWhole)
            If Not wrkRange Is Nothing Then
                wrkStr = Format(DateAdd("n", wrkRange.Offset(0, 1).Value, "00:00:00"), "hh:mm:ss")
                
                If wrkStr = "" Then
                    wrkStr = "00:30:00"
                    wrkRange.Offset(0, 1).Value = wrkStr
                End If

                dtStepSize = Hour(wrkStr) * 60
                dtStepSize = Minute(wrkStr) + dtStepSize
            Else
                wrkRange.Offset(0, 1).Value = "00:30:00"
                wrkStr = "00:30:00"
                dtStepSize = 30 * 60
            End If
            
            
            Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示刻み[時間]", LookAt:=xlWhole)
            If Not wrkRange Is Nothing Then
                wrkRange.Offset(0, 1).Value = wrkStr
            End If
            '*********************************************************************↑
        End If
    
    End With
        
    '* 演算結果シートに転記
    Call PostToCalcResultSheet
    
    '* 演算結果シートに時刻項目を作成
    Call MakeTimeAxisTitle(writeRange, dtStart, dtEnd, dtStepSize)
    
    '* resultシートに時間内チェックフラグ数式を作成
    Call MakeMathFormula(writeRange)
    
    '* グラフのデータソースを更新する
    Call ChangeChartData(writeRange)
    
End Sub

'*********************************************
'* 演算結果シートUPDATE from グラフ編集シート
'*********************************************
Public Sub UpdatePivotTable2()
    Dim workRange           As Range
    Dim writeRange          As Range
    Dim srchRange           As Range
    Dim dtStart             As String
    Dim dtEnd               As String
    Dim dtNow               As String
    Dim dtStepSize          As Integer
    Dim hValue              As MAXMINSTEPINFO
    Dim vValue              As MAXMINSTEPINFO
    Dim OnOffline           As String
    Dim dtInterval          As Double
    Dim workDate            As Date
    
    Dim wrkTime             As Date
    Dim wrkRange            As Range
    Dim wrkDbl              As Double

    '* paramシートから取得開始／終了日時情報を取得
    With Worksheets(ParamSheetName)
        Set srchRange = .Cells.Find(What:="オンライン", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            OnOffline = srchRange.Offset(0, 1).Value
        Else
            OnOffline = "OFFLINE"
        End If
    
        '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
        Call GetMaxMinStepValue(GraphEditSheetName, vValue, hValue)
            
        '* オンラインとオフラインで開始日時の扱いが異なる
        If OnOffline = "ONLINE" Then
            '*********************
            '* 表示期間を取得
            '*********************
            Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                dtInterval = srchRange.Offset(0, 1).Value
            Else
                dtInterval = 1#
            End If
            
            '*********************
            '* 周期を取得
            '*********************
            Set srchRange = .Cells.Find(What:="周期", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                dtStepSize = Format(CInt(Val(srchRange.Offset(0, 1).Value) * 60))
            Else
                dtStepSize = "60"
            End If
            
            dtNow = Format(Now, "yyyy/MM/dd hh:mm:ss")
            '*******************************時刻刻み切上
            Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="−横軸−", LookAt:=xlWhole)
            wrkDbl = wrkRange.Offset(3, 1).Value
            
            '* 刻み値があればその値自体を30分で丸めるた上で現在時刻をその値で切り上げる
            If wrkDbl > 0 Then
                '* グラフ編集画面に入力された刻み間隔を30分で丸める
                wrkDbl = Application.WorksheetFunction.Ceiling(wrkDbl, 1 / 96)
                wrkTime = Application.WorksheetFunction.Ceiling(Now, wrkDbl)
            '* 刻み値がなければ現在時刻を30分単位で切り上げる
            Else
                wrkTime = Application.WorksheetFunction.Ceiling(Now, 1 / 96)
            End If
            dtEnd = Format(wrkTime, "yyyy/MM/dd hh:mm:ss")
            '**********************
            
            '* 表示期間を秒単位にする
            dtInterval = dtInterval * 3600 * (-1)
            '* 表示最新時刻を取得する
            workDate = wrkRange.Offset(2, 1).Value
            
            '*******************************時刻刻み切下
            If wrkDbl > 0 Then
                wrkTime = Application.WorksheetFunction.Floor(workDate, wrkDbl)
            Else
                wrkTime = Application.WorksheetFunction.Floor(workDate, 1 / 96)
            End If
            dtStart = Format(wrkTime, "yyyy/MM/dd hh:mm:ss")
            '*******************************
        
            '* グラフ編集シートに最大／最小を転記する
            Call PutMaxMinStepEditSheetNoStepNoY(dtStart, dtEnd)
    
        Else
    
            dtStart = hValue.MinValue
            dtEnd = hValue.MaxValue
            dtStepSize = Hour(hValue.StepValue) * 3600 + Minute(hValue.StepValue) * 60 + Second(hValue.StepValue)
        
        End If
    
    End With
    
    '* resultシートに転記
    Call PostToCalcResultSheet
    
    '* resultシートに時刻項目を作成
    Call MakeTimeAxisTitle(writeRange, dtStart, dtEnd, dtStepSize)
    'Call MakeTimeAxisTitle(writeRange, dtStart, dtNow, dtStepSize)
    
    '* resultシートに時間内チェックフラグ数式を作成
    Call MakeMathFormula(writeRange)
    
    '* グラフのデータソースを更新する
    Call ChangeChartData(writeRange)
    
End Sub

'* 指定グラフの縦軸の最大／最大／刻み幅を設定する
Public Sub SetGraphParamY(currChartObj As ChartObject, maxY As Double, minY As Double, stepY As Double)
        
    With currChartObj.Chart
        
        '* 縦軸の最大／最小の設定
        .Axes(xlValue).MinimumScale = minY
        If maxY <> 0 Then .Axes(xlValue).MaximumScale = maxY
        If stepY <> 0 Then .Axes(xlValue).MajorUnit = stepY
'        .Axes(xlValue).MajorUnitIsAuto = True ' 自動設定
'        .Axes(xlValue).MinimumScaleIsAuto = True ' 自動設定
'        .Axes(xlValue).MaximumScaleIsAuto = True ' 自動設定
    End With
    
End Sub

'* 指定グラフの横軸の最大／最大／刻み幅を設定する
Public Sub SetGraphParamX(currChartObj As ChartObject, maxX As Double, minX As Double, stepX As Double)
        
    With currChartObj.Chart
        
        '* 横軸の最大／最小の設定
        .Axes(xlCategory).MinimumScale = minX
        .Axes(xlCategory).MaximumScale = maxX
        If stepX <> 0 Then .Axes(xlCategory).MajorUnit = stepX
'        .Axes(xlCategory).MajorUnitIsAuto = True ' 自動設定
'        .Axes(xlCategory).MinimumScaleIsAuto = True ' 自動設定
'        .Axes(xlCategory).MaximumScaleIsAuto = True ' 自動設定
        
    End With
    
End Sub

'* 背景データをグラフシートへ記載する
Public Sub WriteBackgroundData(graphWorksheet As Worksheet, strData() As String, outFlag As Boolean)
    Dim writeRange          As Range
    Dim srchRange           As Range
    Dim lastRange           As Range
    Dim workRange           As Range
    Dim dataRange           As Range
    Dim workStr             As Variant
    Dim writeIndex          As Integer
    Dim dataValue           As String
    Dim dataUnit            As String
    Dim currWorksheet       As Worksheet
    
    
    '* 取得CTMデータシート
    Set currWorksheet = Worksheets(1)

    With graphWorksheet
    
        Set srchRange = .Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
        
            '* 古いデータを削除
            Set writeRange = srchRange.Offset(2, 0)
            Set lastRange = writeRange.End(xlDown)
            Set workRange = .Range(writeRange, lastRange.Offset(0, 2))
            workRange.Clear
            workRange.Interior.Color = RGB(255, 255, 255)
            
            '* 背景データ出力フラグがONならば出力する
            If outFlag Then
                writeIndex = 0
                '* 着目背景データ文字列を順番に出力する
                For Each workStr In strData
                    '* CTM取得一覧から背景データを検索する
                    If InStr(workStr, "・") Then
                        workStr = Right(workStr, Len(workStr) - InStr(workStr, "・"))
                    End If
                    Set dataRange = currWorksheet.Cells.Find(What:=workStr, LookAt:=xlWhole)
                    If Not dataRange Is Nothing Then
                        dataValue = dataRange.Offset(4, 0).Value
                        dataUnit = dataRange.Offset(2, 0).Value
                    End If
                    
                    writeRange.Offset(writeIndex, 0).Value = workStr
                    writeRange.Offset(writeIndex, 1).Value = dataValue
                    writeRange.Offset(writeIndex, 2).Value = dataUnit
                    Set workRange = .Range(writeRange.Offset(writeIndex, 0), writeRange.Offset(writeIndex, 2))
                    With workRange
                        .Borders.LineStyle = xlContinuous
                        .Borders.Weight = xlThin
                        .Borders(xlEdgeBottom).LineStyle = xlContinuous
                        .Borders(xlEdgeBottom).Weight = xlThin
                        .Borders(xlEdgeLeft).LineStyle = xlContinuous
                        .Borders(xlEdgeLeft).Weight = xlMedium
                        .Borders(xlEdgeRight).LineStyle = xlContinuous
                        .Borders(xlEdgeRight).Weight = xlMedium
                    End With
                    writeIndex = writeIndex + 1
                Next workStr
                Set workRange = .Range(writeRange.Offset(writeIndex, 0), writeRange.Offset(writeIndex, 2))
                workRange.Borders(xlEdgeTop).LineStyle = xlContinuous
                workRange.Borders(xlEdgeTop).Weight = xlMedium
            End If
            
            workRange.EntireColumn.AutoFit
'            writeRange.Offset(0, 0).AutoFit
'            writeRange.Offset(0, 1).AutoFit
'            writeRange.Offset(0, 2).AutoFit
        End If
    
    End With
End Sub

'* 指定ピボットテーブルの最大／最小／ステップ幅を変更する
Public Sub ChangeMaxMinStepPVT(currPivotTable As PivotTable, startValue As Integer, endValue As Integer, StepValue As Integer)
    currPivotTable.rowRange.Cells(2, 1).Group Start:=startValue, End:=endValue, By:=StepValue
End Sub

'* 指定ピボットテーブルのフィールドのデータのないアイテム表示をONにする
Public Sub ToOnDispItem(currPivotField As PivotField)
    currPivotField.ShowAllItems = True
End Sub

'* 指定ピボットテーブルのデータ範囲を変更する
Public Sub ChangePivotFieldTable(currPivotTable As PivotTable, dataRange As String)
    currPivotTable.SourceData = dataRange
    '* ピボットテーブル更新
    currPivotTable.PivotCache.Refresh
End Sub

'***************
'タイマー起動開始
'***************
Public Sub TimerStart()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim endTime         As Double
    Dim prevDate        As Date
    Dim prevTime        As Date
    
    With Worksheets(ParamSheetName)
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value * 60         ' 秒計算
        Else
            updTime = 1
        End If
            
        '*********************
        '* 取得お終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = DateValue(srchRange.Offset(0, 1).Value)
            endT = TimeValue(srchRange.Offset(0, 1).Value)
        Else
            endD = DateValue("2020/12/31 23:59:59")
            endT = TimeValue("2020/12/31 23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'TimerLogic'"
    
    End With

    '前回／次回更新日時セット
'    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
'    If Not srchRange Is Nothing Then
'        srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd hh:mm:ss")
'    End If
    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd hh:mm:ss")
    End If
    
    Call UpdateFormStatus
    

End Sub

'***************
'* タイマー処理
'***************
Public Sub TimerLogic()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim endTime         As Double
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim prevCell        As Range
    Dim paramWorksheet  As Worksheet
    Dim sampleType      As String
    
    Set paramWorksheet = Worksheets(ParamSheetName)
    
    With paramWorksheet
        
        Set prevCell = ActiveCell
    
        '* 自動更新にOFFがセットされていれば処理をやめる
        Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "ON" Then
                Application.CutCopyMode = False
                Exit Sub
            End If
        End If
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value * 60         ' 秒計算
        Else
            updTime = 1
        End If
        updTimeUnit = "【秒】"
            
        '*********************
        '* 取得終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = DateValue(srchRange.Offset(0, 1).Value)
            endT = TimeValue(srchRange.Offset(0, 1).Value)
        Else
            endD = DateValue("2020/12/31 23:59:59")
            endT = TimeValue("2020/12/31 23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'TimerLogic'"
        
    End With
    
'    Application.OnTime Now + TimeValue("00:00:05"), "'TimerLogic'"

    '前回／次回更新日時セット
    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd hh:mm:ss")
    End If
    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd hh:mm:ss")
    End If
    
    
    '***************************
    '***************************
    
    '追加20160606*******************************************
    sampleType = ""
    Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
    '*******************************************************
    
    If sampleType <> "GRIP" Then
        '* ミッションから情報を取得
        Call GetMissionInfoAll
    Else
        Call GetGripMission
    End If
    
    '* ピボットテーブルを更新
    Call UpdatePivotTable2
    
    '* グラフ編集画面の設定を元にグラフを更新
    Call UpdateAfterGraphEdit
    '***************************
    '***************************
        
'    prevCell.Select
    
        '凡例、背景ﾃﾞｰﾀ作成
    Call HaikeiHanreiDataMake
    
    If Application.Visible = True Then
        'EXCEL前面表示用
        Call ExcelUpperDisp
        VBA.AppActivate Excel.Application.Caption
    
        Call ExcelDispSetWin32
        Call ExcelDispFreeWin32
    End If
    
End Sub

'*****************************
'* スケジュール予約キャンセル
'*****************************
Public Sub CancelSchedule()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim prevDate        As Date

    Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)

    With currWorksheet
        '* 予約したスケジュールをキャンセルする
        Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            prevDate = srchRange.Offset(0, 1).Value
            On Error Resume Next
            Application.OnTime prevDate, "'TimerLogic'", , False
        End If
    End With

End Sub

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSub()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
    Dim objFSO          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    Dim IsFirstSave     As String
    
    quitFlag = False
    
    Call SetFilenameToVariable
    
        '表示倍率保存
    Call SaveBairitu
    
    Call CancelSchedule

    Set objFSO = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With ThisWorkbook.Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then IsFirstSave = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
'    currThisFile = ThisWorkbook.FullName
'    Application.DisplayAlerts = False
'    ActiveWorkbook.SaveAs Filename:=currThisFile
'    Application.DisplayAlerts = True

'    wrkInt = InStr(ThisWorkbook.Name, templateName)
'
'    If wrkInt <= 0 Or wrkInt > 4 Then
    If IsFirstSave = "" Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & ThisWorkbook.Name
        Else
            fname = saveFilePath & "\" & templateName & "_" & ThisWorkbook.Name
        End If
        '* テンプレート名／登録フォルダ名を取得する
        With ThisWorkbook.Worksheets(ParamSheetName).Columns(1)
            Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "TRUE"
        End With
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & ThisWorkbook.Name
        Else
            fname = saveFilePath & "\" & ThisWorkbook.Name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
    If Dir(fname) <> "" Then
      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
    End If
    
    '各種バー表示
    Call DispBar
    
    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    Application.DisplayAlerts = False
    
    '* ISSUE_NO.624 Add ↓↓↓ *******************************
    On Error GoTo ErrorHandler
    
    ThisWorkbook.SaveAs Filename:=fname
    
ErrorHandler:
    '-- 例外処理
    If Err.Description <> "" Then
        MsgBox Err.Description, vbCritical & vbOKOnly, "警告"
    End If
    '* ISSUE_NO.624 Add ↑↑↑ *******************************
    
    If Application.Workbooks.Count > 1 Then
        ThisWorkbook.Close
    Else
        Application.Quit
        ThisWorkbook.Close
    End If
    If Err.Description = "" Then
        Application.DisplayAlerts = True
    End If
End Sub

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSubFromX()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
    Dim objFSO          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    
    Debug.Print "In SaveFileSubFromX"
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "ON" Then
            Call CancelSchedule
        End If
    End If

    Set objFSO = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With Workbooks(thisBookName).Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
    currThisFile = Workbooks(thisBookName).FullName
'        Application.DisplayAlerts = False
'        ActiveWorkbook.SaveAs Filename:=currThisFile
'        Application.DisplayAlerts = True

    wrkInt = InStr(Workbooks(thisBookName).Name, templateName)

    If wrkInt <= 0 Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & Workbooks(thisBookName).Name
        Else
            fname = saveFilePath & "\" & templateName & "_" & Workbooks(thisBookName).Name
        End If
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & Workbooks(thisBookName).Name
        Else
            fname = saveFilePath & "\" & Workbooks(thisBookName).Name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
'    If Dir(fname) <> "" Then
'      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
'      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
'    End If
    
    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    Application.DisplayAlerts = False
    Workbooks(thisBookName).SaveAs Filename:=currThisFile
    If Application.Workbooks.Count > 1 Then
        ThisWorkbook.Close
    Else
        Application.DisplayAlerts = False
        ThisWorkbook.Save
        Application.Quit
        ThisWorkbook.Close
    End If
    'Application.DisplayAlerts = True
        
End Sub



