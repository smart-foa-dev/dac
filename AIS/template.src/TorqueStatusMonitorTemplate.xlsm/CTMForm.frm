VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} CTMForm 
   Caption         =   "CTMリスト"
   ClientHeight    =   3435
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   3705
   OleObjectBlob   =   "CTMForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "CTMForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


'*****************
'* キャンセルボタン
'*****************
Private Sub BT_cancel_Click()
   Unload Me
End Sub

'*****************
'* OKボタン
'*****************
Private Sub BT_selectok_Click()

    Dim missionWorksheet  As Worksheet
    Dim paramWorksheet    As Worksheet
    Dim srchRange         As Range
    Dim currRange         As Range
    Dim ipAddr            As String
    Dim portNo            As String
    Dim proxyServer       As String
    Dim proxyServerUI     As String

    Call SetFilenameToVariable
    If IsNull(LB_CTM.Value) Or LB_CTM.Value = "" Then
        MsgBox "CTMを選択ください。"
        Exit Sub
    End If
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With paramWorksheet
        '* サーバIPアドレス
        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            ipAddr = currRange.Offset(0, 1).Value
        End If
        '*サーバポート番号
        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:="サーバポート番号", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            portNo = currRange.Offset(0, 1).Value
        End If
        '* ProxyServer使用
        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            proxyServer = currRange.Offset(0, 1).Value
        End If
        '* ProxyServerURI
        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:="ProxyServerURI", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            proxyServerUI = currRange.Offset(0, 1).Value
        End If
    End With
    
    ActionPaneForm.TB_ReferPass.text = ActionPaneForm.TB_ReferPass.text & " " & ipAddr & " " & portNo & " " & LB_CTM.Value & " " & proxyServer & " " & proxyServerUI
    Unload Me
End Sub

'*****************
'* ツールチップ
'*****************
Private Sub LB_CTM_MouseMove(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)

    Dim intRow As Integer
    Dim dispalyName As String
    Dim iIndex As Integer

    intRow = (Y / 12) + LB_CTM.TopIndex
    If intRow < LB_CTM.listCount Then
        dispalyName = LB_CTM.List(intRow)
        iIndex = InStr(dispalyName, ".xlsm")
        If iIndex > 0 Then
            dispalyName = Left(dispalyName, iIndex - 1)
        End If
        LB_CTM.ControlTipText = dispalyName
    Else
        LB_CTM.ControlTipText = ""
    End If
End Sub

'*****************
'* フォーム初期化
'*****************
Private Sub UserForm_Initialize()

    Dim missionWorksheet    As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim iRowCnt             As Integer

    Call SetFilenameToVariable
    
    Set missionWorksheet = Workbooks(thisBookName).Worksheets(MissionSheetName)
    iRowCnt = missionWorksheet.[D65536].End(3).Row

    With LB_CTM
        .ColumnCount = 2
        .ColumnWidths = "0,70"
        .ColumnHeads = False
        .RowSource = missionWorksheet.Range("D1:E" & iRowCnt).Address(External:=True)
    End With

    LB_CTM.SetFocus
    LB_CTM.ListIndex = 0
    
End Sub


