VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} SearchForm 
   Caption         =   "AISアウトプットのファイル"
   ClientHeight    =   6495
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6015
   OleObjectBlob   =   "SearchForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "SearchForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


'*****************
'* キャンセルボタン
'*****************
Private Sub BT_cancel_Click()
   Unload Me
End Sub

'*****************
'* OKボタン
'*****************
Private Sub BT_selectok_Click()
    Dim paramWorksheet    As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range

    Call SetFilenameToVariable
    If IsNull(LB_RegistryXlsm.Value) Or LB_RegistryXlsm.Value = "" Then
        MsgBox "ファイルを選択ください。"
        Exit Sub
    End If
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With paramWorksheet
        '* 演算式および表示書式を取得
        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            mypath = currRange.Offset(0, 1).Value
        Else
            Exit Sub
        End If
    End With
    
    ActionPaneForm.TB_ReferPass.text = """" & mypath & "\" & LB_RegistryXlsm.Value & ".xlsm" & """"
    Unload Me
End Sub

'*****************
'* フォーム初期化
'*****************
Private Sub UserForm_Initialize()
    Dim paramWorksheet    As Worksheet
    Dim srchRange         As Range
    Dim currRange         As Range
    Dim iIndex            As Integer

    Call SetFilenameToVariable

    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With paramWorksheet
        '* 演算式および表示書式を取得
        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            mypath = currRange.Offset(0, 1).Value
        Else
            Exit Sub
        End If
    End With

    myfile = Dir(mypath & "\*.*")
    iIndex = InStr(myfile, ".xlsm")
    If iIndex > 0 Then
        myfile = Left(myfile, InStr(myfile, ".xlsm") - 1)
    End If
    LB_RegistryXlsm.AddItem myfile
    Do Until Len(myfile) = 0
        myfile = Dir
        iIndex = InStr(myfile, ".xlsm")
        If iIndex > 0 Then
            myfile = Left(myfile, InStr(myfile, ".xlsm") - 1)
        End If
        LB_RegistryXlsm.AddItem myfile
    Loop
    LB_RegistryXlsm.SetFocus
    LB_RegistryXlsm.ListIndex = 0
    
End Sub


