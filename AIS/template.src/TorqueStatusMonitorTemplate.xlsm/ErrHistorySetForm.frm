VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ErrHistorySetForm 
   Caption         =   "異常履歴表示画面設定"
   ClientHeight    =   8370
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7695
   OleObjectBlob   =   "ErrHistorySetForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "ErrHistorySetForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



'*****************
'* フォーム初期化
'*****************
Private Sub UserForm_Initialize()
    Dim missionList()   As MISSIONINFO
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    '* ListViewの初期化
    With LV_Element
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add , "_Element", "エレメント名"
        .ColumnHeaders.Item("_Element").Width = 150
    End With

    With LV_DisplayElement
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add 1, "_Id", "ID", 15
        .ColumnHeaders.Add 2, "_Element", "エレメント", 85
        .ColumnHeaders.Add 3, "_DisplayType", "書式", 40
        .ColumnHeaders.Add 4, "_Ctm", "", 0

    End With
     
    '* 保存したデータを再設定する
    Call GetInfoFromExcel
        
    '* 取得したミッション情報のリストボックスへの出力
    LB_Mission.Clear
    For II = 0 To UBound(missionList)
        LB_Mission.AddItem missionList(II).Name
    Next
    LB_Mission.SetFocus
    LB_Mission.ListIndex = 0        ' リストボックスの最初の項目を選択状態にする
    
    '* 書式は数値がデフォルト
    OB_Numeric.Value = True

End Sub

'***************************************
'* ミッションリストボックス選択時の処理
'***************************************
Private Sub LB_Mission_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer

    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)

    mIndex = -1
    For II = 0 To UBound(missionList)
        If missionList(II).Name = LB_Mission.text Then
            mIndex = II
            Exit For
        End If
    Next

    LB_CTM.Clear
    If mIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM)
            LB_CTM.AddItem missionList(mIndex).CTM(II).Name
        Next
        LB_CTM.SetFocus
        LB_CTM.ListIndex = 0
    End If
End Sub

'***************************************
'* CTMリストボックス選択時の処理
'***************************************
Private Sub LB_CTM_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer
    Dim cIndex  As Integer
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    mIndex = -1
    cIndex = -1
    For II = 0 To UBound(missionList)
        For JJ = 0 To UBound(missionList(II).CTM)
            If missionList(II).CTM(JJ).Name = LB_CTM.text Then
                mIndex = II
                cIndex = JJ
                Exit For
            End If
        Next
        If cIndex > -1 Then Exit For
    Next

    LV_Element.ListItems.Clear
    If mIndex > -1 And cIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM(cIndex).Element)
            LV_Element.ListItems.Add.text = missionList(mIndex).CTM(cIndex).Element(II).Name
        Next
    End If
End Sub

'*****************
'* 設定した情報を取得
'*****************
Public Sub GetInfoFromExcel()

    Dim errHisSheetName    As Worksheet
    Dim iEndRow                 As Integer

    Set errHisSheetName = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    
    'データ表示開始列
    TB_col.text = errHisSheetName.Range("B1").text
    'データ表示開始行
    TB_row.text = errHisSheetName.Range("B2").text
    
    With errHisSheetName
    
        '* A6セルをスタートセルとする
        Set startRange = .Range("A6")
        
        '* 設定した表示用エレメントを取得する
        For II = 0 To 100
            Set msnRange = startRange.Offset(II, 0)
            If msnRange.Offset(0, 0).Value <> "" Then
                Set itm = LV_DisplayElement.ListItems.Add()
                itm.text = II + 1
                itm.SubItems(1) = msnRange.Offset(0, 0).Value
                itm.SubItems(2) = msnRange.Offset(0, 1).Value
                itm.SubItems(3) = msnRange.Offset(0, 2).Value
            Else
                Exit For
            End If
        Next
    End With

    If TB_col.text = "" Then

        '* 「削除」ボタンを非表示
        BT_Delete.Visible = False

        '* 「更新」ボタンを非表示
        BT_Update.Visible = False
    Else

        '* 「新規」ボタンを非表示
        BT_Create.Visible = False
    End If
        
End Sub

'************************
'* 表示エレメントの追加
'************************
Private Sub BT_add_Click()

    Dim sValue              As String
    Dim isFormat            As Integer
    Dim isFormatText        As String
    Dim sCtmValue           As String
        
    '* テキストボックス更新処理
    If OB_Numeric.Value Then
        isFormat = 0
        isFormatText = "数値"
    ElseIf OB_String.Value Then
    
        isFormat = 1
        isFormatText = "文字列"
    ElseIf OB_Date.Value Then
    
        isFormat = 2
        isFormatText = "日付"
    ElseIf OB_Time.Value Then
    
        isFormat = 3
        isFormatText = "時刻"
    ElseIf OB_DateTime.Value Then
    
        isFormat = 4
        isFormatText = "日時"
    End If
    
    '* エレメントを追加する
    For i = LV_Element.ListItems.Count To 1 Step -1
        If LV_Element.ListItems(i).Selected Then
        
            sValue = LV_Element.ListItems(i).text
            bHasFlg = False
            For J = LV_DisplayElement.ListItems.Count To 1 Step -1
                If sValue = LV_DisplayElement.ListItems(J).SubItems(1) Then
                    bHasFlg = True
                    Exit For
                End If
            Next J
            If Not bHasFlg Then
                Set itm = LV_DisplayElement.ListItems.Add()
                itm.text = LV_DisplayElement.ListItems.Count()
                itm.SubItems(1) = sValue
                itm.SubItems(2) = isFormatText
                itm.SubItems(3) = LB_CTM.text
                Exit For
            End If
        End If
    Next i

End Sub

'************************
'* 表示エレメントの行削除
'************************
Private Sub BT_rowdelete_Click()

    For i = LV_DisplayElement.ListItems.Count To 1 Step -1
        If LV_DisplayElement.ListItems(i).Selected Then
            LV_DisplayElement.ListItems.Remove i
            Exit For
        End If
    Next i
    
    For i = LV_DisplayElement.ListItems.Count To 1 Step -1
        LV_DisplayElement.ListItems(i).text = i
    Next i
End Sub

'*******************
'* 閉じるボタン処理
'*******************
Private Sub BT_Close_Click()

    Unload Me

End Sub

'*********************************
'* 作成るボタン処理
'*********************************
Private Sub BT_Create_Click()
       
    If TB_col.text = "" Or TB_row.text = "" Then
        MsgBox "データ表示開始行列が入力されていません。"
        TB_col.SetFocus
        Exit Sub
    End If
    
    If IsNumeric(TB_col.text) Then
        If TB_col.text <= 0 Then
            MsgBox "データ表示開始列は正の整数または英字を入力ください。"
            TB_col.SetFocus
            Exit Sub
        End If
    Else
        Dim iA As Integer
        iA = Asc(TB_col.Value)
        If Not (iA >= 65 And iA <= 90) Or (iA >= 97 And iA <= 122) Then
            MsgBox "データ表示開始列は正の整数または英字を入力ください。"
            TB_col.SetFocus
            Exit Sub
        End If
    End If
    
    If Not IsNumeric(TB_row.text) Then
        MsgBox "データ表示開始行は正の整数を入力ください。"
        TB_row.SetFocus
        Exit Sub
    ElseIf TB_row.text <= 0 Then
        MsgBox "データ表示開始行は正の整数を入力ください。"
        TB_row.SetFocus
        Exit Sub
    End If
        
    If LV_DisplayElement.ListItems.Count = 0 Then
        MsgBox "表示エレメントが選択されていません。"
        Exit Sub
    End If

    Me.Hide

    '* 異常履歴表示画面に入力情報をエクセルに保存する
    Call SaveInfoToExcel
    
    '* ブックを作成する
    Call WriteValueErrHistory
    
    Me.Show vbModeless

    Unload Me

End Sub

'***********
'* 更新処理
'***********
Private Sub BT_Update_Click()
    
    If TB_col.text = "" Then
        MsgBox "データ表示開始列が入力されていません。"
        TB_col.SetFocus
        Exit Sub
    End If
    
    If IsNumeric(TB_col.text) Then
        If TB_col.text <= 0 Then
            MsgBox "データ表示開始列は正の整数または英字を入力ください。"
            TB_col.SetFocus
            Exit Sub
        End If
    Else
        Dim iA As Integer
        iA = Asc(TB_col.Value)
        If Not (iA >= 65 And iA <= 90) Or (iA >= 97 And iA <= 122) Then
            MsgBox "データ表示開始列は正の整数または英字を入力ください。"
            TB_col.SetFocus
            Exit Sub
        End If
    End If
    
    If TB_row.text = "" Then
        MsgBox "データ表示開始行が入力されていません。"
        TB_row.SetFocus
        Exit Sub
    End If
    
    If Not IsNumeric(TB_row.text) Then
        MsgBox "データ表示開始行は正の整数を入力ください。"
        TB_row.SetFocus
        Exit Sub
    ElseIf TB_row.text <= 0 Then
        MsgBox "データ表示開始行は正の整数を入力ください。"
        TB_row.SetFocus
        Exit Sub
    End If

    If LV_DisplayElement.ListItems.Count = 0 Then
        MsgBox "表示エレメントが選択されていません。"
        Exit Sub
    End If

    '* ステータスを削除する
    Call DeleteErrHistory("0")
    
    '* 異常履歴表示画面に入力情報をエクセルに保存する
    Call SaveInfoToExcel
    
    '* ブックを作成する
    Call WriteValueErrHistory
    
    Unload Me
    
End Sub

'***********
'* 削除処理
'***********
Private Sub BT_Delete_Click()
    
    ' ステータスを削除する
    Call DeleteErrHistory("0")
    
    Unload Me
    
End Sub

'***********************
'* ドラッグ完了時の処理
'***********************
Private Sub LV_Element_OLECompleteDrag(Effect As Long)
    Debug.Print "OLECompleteDrag"
    Effect = vbDropEffectCopy
End Sub

'*****************************************************
'* リストボックスからテキストボックスへのドラッグ処理
'*****************************************************
Private Sub LV_Element_OLEStartDrag(data As MSComctlLib.DataObject, AllowedEffects As Long)

    Dim getTextStr      As Variant
    Debug.Print "OLEStartDrag"
    
    '* データオブジェクトの内容をクリア
    data.Clear
    
    '* ドラッグ開始時のリストボックスの選択項目を取得
    getTextStr = LV_Element.SelectedItem.text
    
    '* データオブジェクトにセット
    data.SetData getTextStr, vbCFtext
    
End Sub

'*******************
'* 異常履歴表示画面に入力情報をエクセルに保存する
'*******************
Private Sub SaveInfoToExcel()

    Dim errHisSheetName    As Worksheet
    
    Set errHisSheetName = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    With errHisSheetName
        '* データ表示開始列
        .Range("B1").Value = TB_col.text
        '* データ表示開始行
        .Range("B2").Value = TB_row.text

        '* A5セルをスタートセルとする
        Set startRange = .Range("A5")

        '* 表示エレメントを設定する
        For II = 1 To LV_DisplayElement.ListItems.Count
            Set msnRange = startRange.Offset(II, 0)
            msnRange.Offset(0, 0).Value = LV_DisplayElement.ListItems(II).SubItems(1)
            msnRange.Offset(0, 1).Value = LV_DisplayElement.ListItems(II).SubItems(2)
            msnRange.Offset(0, 2).Value = LV_DisplayElement.ListItems(II).SubItems(3)
        Next
    End With

End Sub


