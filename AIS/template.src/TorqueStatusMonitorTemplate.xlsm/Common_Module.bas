Attribute VB_Name = "Common_Module"
Option Explicit

Dim FinalTime           As Date
Dim TimerStopFlag       As Boolean

'* 本ブック名
Public thisBookName     As String
'* クローズ時の２重処理防止フラグ
Public quitFlag         As Boolean

Public Const MaxCTMNumber       As Integer = 1000       ' 最大CTM数
Public Const MaxELMNumber       As Integer = 1000       ' 最大エレメント数
Public Const LimitCTMNumber     As Long = 0             ' 取得CTM情報最大件数
Public Const MaxCntNumber       As Integer = 300        ' 最大表示件数（プロジェクトステータス用）

'FOR COM高速化 lm 2018/06/15 Add Start
Public Const MaxCntImport       As Integer = 5000       ' 最大書き込み数
'FOR COM高速化 lm 2018/06/15 Add End

Public Const DefaultZoomPercent As Integer = 60
Public Const StepZoomPercent    As Integer = 20
Public Const DataSheetStartRow  As Integer = 2

Public Const MainSheetName      As String = "オンラインテンプレート"
Public Const ResultSheetName    As String = "result"
Public Const GraphEditSheetName As String = "グラフ編集"
Public Const FixGraphSheetName  As String = "グラフ"
Public Const ParamSheetName     As String = "param"
Public Const RoParamSheetName   As String = "ro_param"
Public Const ForPivotSheetName  As String = "演算結果データ"
Public Const MomentSheetName    As String = "モーメント図グラフ値"
Public Const PivotSheetName     As String = "グラフテーブル"
Public Const MissionSheetName   As String = "ミッション"
Public Const StatusSheetName    As String = "ステータス"
Public Const DispDefSheetName   As String = "画面定義"
Public Const DataDispDefSheetName     As String = "生データ画面機能"
Public Const ColorParamSheetName      As String = "color_param"
Public Const LinkSheetName            As String = "LinkSheet"
Public Const ConditiongSheetName      As String = "condition_param"
Public Const LinkParamSheetName       As String = "link_param"
Public Const TorqueSetDataSheetName   As String = "設定画面データ"
Public Const MaxTorqueSheetName       As String = "最大トルクデータ"
Public Const AvgTorqueSheetName       As String = "平均トルクデータ "
Public Const TemplateSheetName          As String = "期間テンプレート"

Public Const PivotMainTableName As String = "PivotTable001"
Public Const PivotSubTableName  As String = "PivotTable002"
Public Const HistGraph01Name    As String = "LeadTime"
Public Const HistGraph02Name    As String = "LeadTime"
Public Const VerticalAxisName   As String = "−縦軸−"
Public Const HorizontalAxisName As String = "−横軸−"
Public Const STEAM_MODULE_NAME  As String = "機種名"

Public Const TateLabelRange   As String = "$G$30"
Public Const YokoLabelRange   As String = "$L$17"

'* リンクボックスのタイプ
Public Const LINKBOX_TYPE_AIS As String = "AIS"
Public Const LINKBOX_TYPE_URL As String = "URL"
Public Const LINKBOX_TYPE_APP As String = "APP"

'* ミッションデータを取得するタイプ
Public Const SEARCH_TYPE_TEST As String = "1"
Public Const SEARCH_TYPE_AUTO As String = "0"
'* 系列先頭セル位置
Public Const ADDR_SeriesStartPos    As String = "C1"
'* 時間先頭セル位置
Public Const ADDR_WorkTimeStartPos  As String = "D1"
'* メインピボットテーブル書き出し位置
Public Const ADDR_MainPivotTable    As String = "!R1C1"
'* サブピボットテーブル書き出し位置
Public Const ADDR_SubPivotTable    As String = "!R17C9"

'* Element情報
Public Type ELMINFO
    Name            As String       ' エレメント名
    ID              As String       ' GUID
    Value           As Variant      ' 値
    Type            As Integer      ' 型
    file            As Variant      ' ファイル名(Bulky指定のみ存在)
End Type

'* CTM情報
Public Type CTMINFO
    Name            As String       ' CTM名
    ID              As String       ' GUID
    Element()       As ELMINFO      ' エレメント情報
    RecvTime        As String       ' 受信時刻
End Type

'* ミッション情報
Public Type MISSIONINFO
    Name            As String       ' CTM名
    ID              As String       ' GUID
    CTM()           As CTMINFO      ' エレメント情報
End Type

'* 基準線情報
Public Type BASEINFO
    Name            As String
    Value           As Variant
End Type

'* 最大／最小／刻み幅情報
Public Type MAXMINSTEPINFO
    maxValue        As Variant
    minValue        As Variant
    stepValue       As Variant
End Type

'*******************************
'* ファイル名を変数へセットする
'*******************************
Public Sub SetFilenameToVariable()
    Dim srchRange       As Range
    Dim workRange       As Range
    
    Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Range("A:A")
    Set workRange = srchRange.Find(What:="マクロファイル名", LookAt:=xlWhole)
    If Not workRange Is Nothing Then
        thisBookName = workRange.Offset(0, 1).Value
    End If

End Sub

'*********************
'* UNIX時間を取得する
'*********************
Public Function GetUnixTime(wDateTime As Date) As Double
    Dim timezoneId  As String
        
    timezoneId = GetTimezoneId()
    
    If timezoneId = "" Then
        GetUnixTime = ((wDateTime - 25569) * 86400 - 32400) * 1000
    Else
        ' 2017.02.22 Change ↓↓↓*************
'        GetUnixTime = CreateObject("FoaCoreCom.FoaCoreCom").GetUnixTime(Year(wDateTime), Month(wDateTime), Day(wDateTime), Hour(wDateTime), Minute(wDateTime), Second(wDateTime), timezoneId)
        GetUnixTime = CreateObject("FoaCoreCom.FoaCoreCom").GetUnixTime_2(Year(wDateTime), Month(wDateTime), Day(wDateTime), Hour(wDateTime), Minute(wDateTime), Second(wDateTime), Strings.Right(Strings.Format(wDateTime, "#0.000"), 3), timezoneId)
        ' 2017.02.22 Change ↑↑↑*************
    End If

End Function

'*********************
'* 日時文字列を取得する
'*********************
Public Function GetDateTime(unixTime As Double) As String
    Dim timezoneId  As String
    
    timezoneId = GetTimezoneId()
    
    If timezoneId = "" Then
        GetDateTime = Format(CDate(((CLng(unixTime / 1000) + 32400) / 86400) + 25569), "yyyy/MM/dd hh:mm:ss")
    Else
        GetDateTime = CreateObject("FoaCoreCom.FoaCoreCom").GetDateTime(unixTime, timezoneId)
    End If
    
End Function

'********************************************************
'* 取得したJSONのオブジェクトから指定されたkeyの値を得る
'********************************************************
Public Function GetJSONValue(objJSON As Object, strKey As String) As Variant
    GetJSONValue = CallByName(objJSON, strKey, VbGet)
End Function

'******************
'* 漢字コード変換
'******************
Public Function convTextEncoding(ByVal text, ByVal fromCharaset As String, Optional ByVal toCharaset As String = "unicode")
    Dim convText As String

    With CreateObject("ADODB.Stream")
        .Open
        .Type = adTypeText
        .Charset = toCharaset
        .WriteText text
        .Position = 0
        .Type = adTypeText
        .Charset = fromCharaset
        
On Error GoTo myLabel
        convText = .ReadText()
        convTextEncoding = Mid(convText, 3, Len(convText))
On Error GoTo 0
    End With
    
    Exit Function
    
myLabel:
    convTextEncoding = StrConv(text, vbUnicode, 1041)

End Function

'******************
'* 更新周期取得
'******************
Public Function GetUpdatePeriod() As Integer
    Dim srchRange       As Range
    Dim period As Integer
        
    With Workbooks(thisBookName).Worksheets(MainSheetName)
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            period = srchRange.Offset(0, 1).Value         ' ミリ秒計算
        Else
            period = 1
        End If
    End With
    
    GetUpdatePeriod = period * 1000
End Function

'******************
'* onlineId取得
'******************
Public Function GetOnlineId() As String
    Dim srchRange       As Range
    Dim onlineId        As Variant
        
    Set srchRange = ThisWorkbook.Worksheets(RoParamSheetName).Range("A:A")
    Set onlineId = srchRange.Find(What:="オンライングラフID", LookAt:=xlWhole)
    
    If onlineId Is Nothing Then
        GetOnlineId = ""
    Else
        GetOnlineId = CStr(onlineId.Offset(0, 1).Value)
    End If
    
End Function


'******************
'* TimezoneId取得
'******************
'FOR COM高速化 lm 2018/06/15 Modify Start
'Private Function GetTimezoneId() As String
Public Function GetTimezoneId() As String
'FOR COM高速化 lm 2018/06/15 Modify End
    Dim srchRange       As Range
    Dim timezoneId      As Variant
        
    Set srchRange = ThisWorkbook.Worksheets(RoParamSheetName).Range("A:A")
    Set timezoneId = srchRange.Find(What:="TimezoneId", LookAt:=xlWhole)
    
    If timezoneId Is Nothing Then
        GetTimezoneId = ""
    Else
        GetTimezoneId = CStr(timezoneId.Offset(0, 1).Value)
    End If
   
End Function


'******************
'* DLLの設定
'******************
Public Function InitDll(Workbook As Workbook) As String
    
    Const GripDataRetrieverName As String = "GripDataRetriever"
    Const FoaCoreComLoadName As String = "FoaCoreCom"
        
    Const GripDataRetrieverTlb As String = "GripDataRetriever.tlb"
    Const FoaCoreComTlb As String = "FoaCoreCom.tlb"
        
    Const AisPath1 As String = "C:\foa\ais\"
    Const AisPath2 As String = "C:\ais\"
        
    Dim isBrokenFlag As Boolean
    isBrokenFlag = False
        
    Dim GripDataRetrieverLoad As Boolean
    Dim FoaCoreComLoad As Boolean
    GripDataRetrieverLoad = False
    FoaCoreComLoad = False
    
    On Error GoTo myError
    
    ' 参照設定のロード、参照不可になっているか確認する。
    Dim Ref
    For Each Ref In Workbook.VBProject.References
        If Ref.Name = GripDataRetrieverName Then
            GripDataRetrieverLoad = True
        ElseIf Ref.Name = FoaCoreComLoadName Then
            FoaCoreComLoad = True
        End If
        
        If Ref.IsBroken = True Then
            isBrokenFlag = True
        End If
    Next Ref
    
    InitDll = ""
    
    
    Dim aisPath As String
    If GripDataRetrieverLoad = False Or FoaCoreComLoad = False Then
        Dim currWorksheet As Worksheet
        Dim srchRange As Range
        Set currWorksheet = Workbook.Worksheets(ParamSheetName)
        Set srchRange = currWorksheet.Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        aisPath = srchRange.Offset(0, 1).Value
        
        Dim n As Integer
        n = InStr(aisPath, "registry")
            
        aisPath = Left(aisPath, n - 1)
        If Dir(aisPath + FoaCoreComTlb) = "" Then
            aisPath = AisPath1
            If Dir(aisPath + FoaCoreComTlb) = "" Then
                aisPath = AisPath2
            End If
        End If

    End If
    
    
    
    ' 必要なモジュールのロード
    If GripDataRetrieverLoad = False Then
        Dim RefFile As String
        RefFile = aisPath + GripDataRetrieverTlb
        ActiveWorkbook.VBProject.References.AddFromFile RefFile
    End If
    
    If FoaCoreComLoad = False Then
        Dim RefFile2 As String
        RefFile2 = aisPath + FoaCoreComTlb
        ActiveWorkbook.VBProject.References.AddFromFile RefFile2
    End If
    
    If isBrokenFlag = True Then
        InitDll = "参照不可になっているライブラリーが存在します。" & vbCrLf
    End If
    
myError:
    InitDll = ""
End Function


