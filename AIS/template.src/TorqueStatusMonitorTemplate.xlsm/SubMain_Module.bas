Attribute VB_Name = "SubMain_Module"
Option Explicit

Private excelUtil As FoaCoreCom.excelUtil

'Wang 外部取込みアプリケーションを起動 2018/04/27 Start
'外部取込みアプリケーションを起動し、異常終了の場合は後続処理を中断する
Private Const COMMAND_TriggerExData = "TriggerExData.bat"
'Wang 外部取込みアプリケーションを起動 2018/04/27 End

'************************************
'* リンクの情報をシートに登録する
'************************************
Public Sub WriteValueLinkSheet(formStatus As String, linkBln As Boolean, commandListPass() As String, _
                                currShapeName As String, commandListType() As Integer, commandListCount As Integer)
    
    Dim linkParamWorksheet  As Worksheet
    Dim workRange           As Range
    Dim srchRange           As Range
    Dim dispRange           As Range
    Dim iRowCnt             As Long
    Dim II                  As Integer
    Dim i                   As Long

    Call SetFilenameToVariable
    Set linkParamWorksheet = Workbooks(thisBookName).Worksheets(LinkSheetName)
    iRowCnt = linkParamWorksheet.UsedRange.Rows.Count
    With linkParamWorksheet
        Set srchRange = .Range("1:1")
        Set workRange = srchRange.Find(What:="パス", LookAt:=xlWhole)
        If Not workRange Is Nothing Then
            '新規作成の場合
            If formStatus = "create" Then
            
                For i = 0 To commandListCount - 1 Step 1
                
                    'AISアウトプットのファイルの場合
                    If commandListType(i) = 1 Then
                    
                        '* 全図形に対して
                        For II = 1 To 1000
                            '定義内容の有無ﾁｪｯｸ
                            If workRange.Offset(II, 1).Value = "" Then
                                '* 記載開始範囲を設定
                                workRange.Offset(II, 0).Value = commandListPass(i)
                                workRange.Offset(II, 1).Value = currShapeName
                                Exit For
                            End If
                        Next
                    End If
                Next i
                
                '更新の場合
            ElseIf formStatus = "update" Then
            
                '既存図形のリンク情報を削除する
                For II = 1 To iRowCnt
                    If .Range("B" & II).Value = "" Then Exit For
                    If .Range("B" & II).Value = currShapeName Then
                        .Rows(II).Delete
                        II = II - 1
                    End If
                Next II

                'リンクがある場合
                If linkBln Then

                    Set srchRange = .Range("B:B")
                    For i = 0 To commandListCount - 1 Step 1
                    
                        'AISアウトプットのファイルの場合
                        If commandListType(i) = 1 Then
                        
                            '* 全図形に対して
                            For II = 1 To 1000
                                '定義内容の有無ﾁｪｯｸ
                                If workRange.Offset(II, 1).Value = "" Then
                                    '* 記載開始範囲を設定
                                    workRange.Offset(II, 0).Value = commandListPass(i)
                                    workRange.Offset(II, 1).Value = currShapeName
                                    Exit For
                                End If
                            Next
                        End If
                    Next i
                End If
            End If
        End If
    End With
End Sub

'*************************************************
'* 画面の情報を元に入力テキストボックスを生成する
'*************************************************
Public Sub WriteValueTextBox(calcString As String, isFormat As Integer, dispColorCond() As String, linkBln As Boolean, elementBln As Boolean, _
                    elementString As String, elementCondList() As String, elementList() As String, elementListCount As Integer, _
                    commandListType() As Integer, commandListName() As String, commandListPass() As String, commandListCount As Integer)
                    
    Dim MyKeyState          As Long
    Dim nowShapeNum         As Integer
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim linkWorksheet       As Worksheet
    Dim condParamsheet      As Worksheet
    Dim linkParamsheet      As Worksheet
    Dim srchRange           As Range
    Dim srchNmRange         As Range
    Dim i                   As Integer
    Dim currShape           As Shape
    Dim currShapeName       As String
    Dim currShapeNum        As Integer
    Dim II                  As Integer
    Dim startRange          As Range
    Dim workRange           As Range
    Dim dispRange           As Range
    Dim itemOpeString       As String
    Dim idArray(100)        As Boolean
    Dim workStr             As String
    Dim splitStr            As Variant
    Dim isCalcString        As Boolean
    Dim iLineWeight         As Integer
    Dim iLineStyle          As Integer
    Dim iEndRow             As Integer
    
    Call SetFilenameToVariable
    
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    
    workStr = "値を表示するテキストボックスを作成" & vbCrLf & vbCrLf & _
                "※セル枠線に合わせる場合はALTキーを押しながらドラッグ"
    
    If MsgBox(workStr, vbYesNo + vbInformation) = vbYes Then
        Application.CommandBars.FindControl(ID:=1111).Execute
    Else
        Exit Sub
    End If
    
    If linkBln Then
        iLineWeight = 5
        iLineStyle = xlDoubleAccounting
    Else
        iLineWeight = 1.5
        iLineStyle = xlContinuous
    End If
    With statusWorksheet
    
        '* 現在の図形数を取得する
        nowShapeNum = .Shapes.Count
        
        '* 図形が増えるまで待機
        Do
            DoEvents
        Loop Until nowShapeNum < .Shapes.Count
        
        '* 現在の該当図形数を取得
        For II = 0 To 99
            idArray(II) = True
        Next
        nowShapeNum = .Shapes.Count
        currShapeNum = 0
        For II = 1 To nowShapeNum
            If Left(.Shapes(II).Name, 6) = "value_" Then
                idArray(CInt(Right(.Shapes(II).Name, 2))) = False
                currShapeNum = currShapeNum + 1
            End If
        Next
        For II = 1 To 99
            If idArray(II) Then Exit For
        Next
        If II > 99 Then
            currShapeNum = 99
        Else
            currShapeNum = II
        End If
        
        '* 図形の属性を設定
        Set currShape = .Shapes(nowShapeNum)
        currShapeName = "value_shape_" & Format(currShapeNum, "00")
        
        With currShape
            .Name = currShapeName
            .OnAction = "ConditionOpen"
            With .TextFrame2
                '* 文字列配置中央
                .VerticalAnchor = msoAnchorMiddle
                With .TextRange
                    .ParagraphFormat.Alignment = msoAlignCenter
                    .Font.Bold = msoTrue
                    .Font.Size = 16
                    .Font.Fill.ForeColor.RGB = RGB(0, 0, 0)
                End With
            End With
            .Line.ForeColor.RGB = RGB(0, 0, 0)
            .Line.Weight = iLineWeight
            .Line.Style = iLineStyle
            .Fill.ForeColor.RGB = RGB(255, 255, 255)
        End With
        
    End With
    
    '*****************************************
    '* 演算式と図形情報を画面定義シートへ記載
    '*****************************************
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet
    
        '* 記載開始範囲を設定
        Set startRange = .Range("A2")
        
        '* 空行を探す
        For II = 0 To 100
            Set workRange = startRange.Offset(II, 0)
            If workRange.Value = "" Then Exit For
        Next
        
        '* 書き出し場所がなければ処理中断
        If II > 100 Then
            currShape.Delete
            Exit Sub
        End If
        
        workRange.Offset(0, 0).Value = currShapeName
        workRange.Offset(0, 1).Value = calcString
        workRange.Offset(0, 3).Value = isFormat
        
        '* link部分の値
        'リンク設定 表示フラグ
        workRange.Offset(0, 20).Value = linkBln
        'リンク種別
        workRange.Offset(0, 21).Value = commandListType(0)
        'リンク先
        If Not linkBln Then
            workRange.Offset(0, 22).Value = ""
        Else
            workRange.Offset(0, 22).Value = commandListPass(0)
        End If
        
        'エレメント値 表示フラグ
        workRange.Offset(0, 23).Value = elementBln
        'エレメント値
        If Not elementBln Then
            elementString = ""
        End If
        workRange.Offset(0, 24).Value = elementString
        
        '* LinkSheetにを保存する
        If linkBln Then
            Call WriteValueLinkSheet("create", linkBln, commandListPass, currShapeName, commandListType, commandListCount)
        End If
        
        '* 表示値の数式を生成する
        itemOpeString = GetItemOperation(calcString)
        
        '* 演算形式を判定する
        isCalcString = False
        If InStr(workRange.Offset(0, 1).Value, ",") > 0 Then
            splitStr = Split(workRange.Offset(0, 1).Value, ",")
            If InStr(splitStr(1), "種類数") Then
              isCalcString = True
            End If
        End If

        With workRange.Offset(0, 2)
        
            '* 書式を設定する
            .NumberFormatLocal = "G/標準"
    
            '* 数式を代入する
            .FormulaLocal = itemOpeString
            
            If IsDate(workRange.Offset(0, 2).Value) Then
                .NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
            ElseIf IsNumeric(.Value) Then
                '* ISSUE_NO.670 sunyi 2018/05/15 start
                '* オーバーフローを修正
                'If .Value = CLng(.Value) Or isCalcString Then
                If .Value = CDbl(.Value) Or isCalcString Then
                '* ISSUE_NO.670 sunyi 2018/05/15 end
                    .NumberFormatLocal = "0"
                Else
                    .NumberFormatLocal = "0.00"
                End If
            End If
        End With
        
        Set dispRange = .Range(workRange.Offset(0, 5), workRange.Offset(0, 19))
        dispRange.NumberFormatLocal = "@"
        dispRange = dispColorCond
    
    End With
    
    '*****************************************
    '* コマンドリスト情報をlinkparamシートへ記載
    '*****************************************
    Set linkParamsheet = Workbooks(thisBookName).Worksheets(LinkParamSheetName)
    With linkParamsheet
        Set srchRange = .Range("A:A")
        Set srchNmRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
        iEndRow = linkParamsheet.UsedRange.Rows.Count

        If srchNmRange.Offset(iEndRow, 0).Value = "" Then

            For i = 0 To commandListCount - 1 Step 1
                srchNmRange.Offset(iEndRow + i, 0).Value = currShapeName
                srchNmRange.Offset(iEndRow + i, 1).Value = commandListType(i)
                srchNmRange.Offset(iEndRow + i, 2).Value = Chr(39) & commandListName(i)
                srchNmRange.Offset(iEndRow + i, 3).Value = Chr(39) & commandListPass(i)
            Next i
        End If
    End With
    
    '*****************************************
    '* 条件を条件画面定義シートへ記載
    '*****************************************
    Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    With condParamsheet
        Set srchRange = .Range("A:A")
        Set srchNmRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
        iEndRow = condParamsheet.UsedRange.Rows.Count
        
        If Not srchNmRange Is Nothing Then
            '* 全図形に対して
            If srchNmRange.Offset(iEndRow, 0).Value = "" Then
                
                For i = 0 To elementListCount - 1 Step 1
                    srchNmRange.Offset(iEndRow + i, 0).Value = currShapeName
                    srchNmRange.Offset(iEndRow + i, 1).Value = elementList(i)
                    srchNmRange.Offset(iEndRow + i, 2).Value = Chr(39) & elementCondList(i)
                Next i
            End If
        End If
    End With
    
    On Error Resume Next
    If elementBln Then
        currShape.TextFrame.Characters.text = elementString
    Else
        Select Case isFormat
            Case 0
                With workRange.Offset(0, 2)
                    '種類数の場合は書式固定
                    '* ISSUE_NO.670 sunyi 2018/05/15 start
                    '* オーバーフローを修正
                    'If .Value = CLng(.Value) Or isCalcString Then
                    If .Value = CDbl(.Value) Or isCalcString Then
                    '* ISSUE_NO.670 sunyi 2018/05/15 end
                        currShape.TextFrame.Characters.text = Format(.Value, "0")
                    Else
                        currShape.TextFrame.Characters.text = Format(.Value, "0.00")
                    End If
                End With
            Case 1
                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
            Case 2
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd")
                End If
            Case 3
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "hh:mm:ss")
                End If
            Case 4
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd hh:mm:ss")
                End If
        End Select
    End If

End Sub

'*************************************************
'* 画面の情報を元に入力テキストボックスを更新する
'*************************************************
Public Sub UpdateValueTextBox(currShapeName As String, calcString As String, isFormat As Integer, dispColorCond() As String, _
                    linkBln As Boolean, elementBln As Boolean, elementString As String, _
                    elementCondList() As String, elementList() As String, elementListCount As Integer, _
                    commandListType() As Integer, commandListName() As String, commandListPass() As String, commandListCount As Integer)
    Dim MyKeyState          As Long
    Dim nowShapeNum         As Integer
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim condParamsheet      As Worksheet
    Dim linkParamsheet      As Worksheet
    Dim srchNmRange         As Range
    Dim scondRange          As Range
    Dim scondNmRange        As Range
    Dim dispCondRange       As Range
    Dim i                   As Integer
    Dim currShape           As Shape
    Dim currShapeNum        As Integer
    Dim II                  As Integer
    Dim srchRange           As Range
    Dim workRange           As Range
    Dim dispRange           As Range
    Dim itemOpeString       As String
    Dim idArray(100)        As Boolean
    Dim workStr             As String
    Dim splitStr            As Variant
    Dim isCalcString        As Boolean
    Dim iLineWeight         As Integer
    Dim iLineStyle          As Integer
    Dim iEndRow             As Integer

    On Error GoTo errlable

    Call SetFilenameToVariable
'    If linkBln Then
'        iLineWeight = 5
'        iLineStyle = xlDoubleAccounting
'    Else
'        iLineWeight = 1.5
'        iLineStyle = xlContinuous
'    End If
    
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    
    Set currShape = statusWorksheet.Shapes(currShapeName)
'    currShape.Line.Style = iLineStyle
'    currShape.Line.Weight = iLineWeight
    
    '*****************************************
    '* 演算式と図形情報を画面定義シートへ記載
    '*****************************************
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet
    
        '* 記載開始範囲を設定
        Set srchRange = .Range("A:A")
        Set workRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
        If workRange Is Nothing Then Exit Sub
        
        workRange.Offset(0, 1).Value = calcString
        workRange.Offset(0, 3).Value = isFormat
        
        '* link部分の値
        'リンク設定 表示フラグ
        workRange.Offset(0, 20).Value = linkBln
        'リンク種別
        workRange.Offset(0, 21).Value = commandListType(0)
        'リンク先
        If Not linkBln Then
            workRange.Offset(0, 22).Value = ""
        Else
            workRange.Offset(0, 22).Value = commandListPass(0)
        End If
        
        'エレメント値 表示フラグ
        workRange.Offset(0, 23).Value = elementBln
        'エレメント値
        If Not elementBln Then
            elementString = ""
        End If
        workRange.Offset(0, 24).Value = elementString
        
        '* LinkSheetにを更新する
        Call WriteValueLinkSheet("update", linkBln, commandListPass, currShapeName, commandListType, commandListCount)
        
        '* 表示値の数式を生成する
        itemOpeString = GetItemOperation(calcString)
        
        '-2017/05/08 No.555 Add.
        '* 演算形式を判定する
        isCalcString = False
        If InStr(workRange.Offset(0, 1).Value, ",") > 0 Then
            splitStr = Split(workRange.Offset(0, 1).Value, ",")
            If InStr(splitStr(1), "種類数") Then
              isCalcString = True
            End If
        End If
        
        With workRange.Offset(0, 2)

            '* 書式を設定する
            .NumberFormatLocal = "G/標準"
    
            '* 数式を代入する
            .FormulaLocal = itemOpeString
        
            If IsDate(.Value) Then
                .NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
            ElseIf IsNumeric(.Value) Then
                '種類数の場合は書式固定
                '* ISSUE_NO.670 sunyi 2018/05/15 start
                '* オーバーフローを修正
                'If .Value = CLng(.Value) Or isCalcString Then
                If .Value = CDbl(.Value) Or isCalcString Then
                '* ISSUE_NO.670 sunyi 2018/05/15 end
                    .NumberFormatLocal = "0"
                Else
                    .NumberFormatLocal = "0.00"
                End If
            End If
        End With
        
        Set dispRange = .Range(workRange.Offset(0, 5), workRange.Offset(0, 19))
        dispRange.NumberFormatLocal = "@"
        dispRange = dispColorCond
    
    End With
        
    '*****************************************
    '* コマンドリスト情報をlinkparamシートへ記載
    '*****************************************
    Set linkParamsheet = Workbooks(thisBookName).Worksheets(LinkParamSheetName)
    With linkParamsheet
        Set srchRange = .Range("A:A")
        Set srchNmRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
        iEndRow = linkParamsheet.UsedRange.Rows.Count

        If Not srchNmRange Is Nothing Then
         '* 更新の場合
            
            '* 全図形に対して
            For II = 1 To iEndRow
                Set dispCondRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
            
                If Not dispCondRange Is Nothing Then
                    ' データを削除する
                    .Rows(dispCondRange.Row).Delete
                Else
                    Exit For
                End If
            Next
            
            If linkBln Then
                '* 全図形に対して
                Set srchNmRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
                For II = 1 To 1000
                    '定義内容の有無ﾁｪｯｸ
                    If srchNmRange.Offset(II, 0).Value = "" Then
                        '* 記載開始範囲を設定
                        For i = 0 To commandListCount - 1 Step 1
                            srchNmRange.Offset(II + i, 0).Value = currShapeName
                            srchNmRange.Offset(II + i, 1).Value = commandListType(i)
                            srchNmRange.Offset(II + i, 2).Value = Chr(39) & commandListName(i)
                            srchNmRange.Offset(II + i, 3).Value = Chr(39) & commandListPass(i)
                        Next i
                        Exit For
                    End If
                Next
            End If
        End If
    End With
    
    '*****************************************
    '* 条件を条件画面定義シートへ記載
    '*****************************************
    Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    With condParamsheet
        Set scondRange = .Range("A:A")
        Set scondNmRange = scondRange.Find(What:="図形名", LookAt:=xlWhole)
        iEndRow = condParamsheet.UsedRange.Rows.Count
        
        If Not scondNmRange Is Nothing Then
            '* 更新の場合
            
            '* 全図形に対して
            For II = 1 To iEndRow
                Set dispCondRange = scondRange.Find(What:=currShapeName, LookAt:=xlWhole)
            
                If Not dispCondRange Is Nothing Then
                    ' データを削除する
                    .Rows(dispCondRange.Row).Delete
                Else
                    Exit For
                End If
            Next
        
            '* 全図形に対して
            iEndRow = condParamsheet.UsedRange.Rows.Count
            If scondNmRange.Offset(iEndRow, 0).Value = "" Then
                
                For i = 0 To elementListCount - 1 Step 1
                    scondNmRange.Offset(iEndRow + i, 0).Value = currShapeName
                    scondNmRange.Offset(iEndRow + i, 1).Value = elementList(i)
                    scondNmRange.Offset(iEndRow + i, 2).Value = Chr(39) & elementCondList(i)
                Next i
            
            End If

        End If
    End With
    On Error Resume Next
    If elementBln Then
        currShape.TextFrame.Characters.text = elementString
    Else
        Select Case isFormat
            Case 0
                With workRange.Offset(0, 2)
                    '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                    'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                    '* ISSUE_NO.670 sunyi 2018/05/15 start
                    '* オーバーフローを修正
                    'If .Value = CLng(.Value) Or isCalcString Then
                    If .Value = CDbl(.Value) Or isCalcString Then
                    '* ISSUE_NO.670 sunyi 2018/05/15 end
                        '-2017/05/08 No.554 Mod. Changed # to 0.
                        currShape.TextFrame.Characters.text = Format(.Value, "0")
                    Else
                        currShape.TextFrame.Characters.text = Format(.Value, "0.00")
                    End If
                End With
            Case 1
                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
            Case 2
                '20161104 追加
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd")
                End If
            Case 3
                '20170213 追加
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "hh:mm:ss")
                End If
            Case 4
                '20170213 追加
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd hh:mm:ss")
                End If
        End Select
    End If
errlable:
    If Err.Number = 1 Then
        MsgBox "該当ボックスが削除されました。"
    End If

End Sub

Public Sub CalcStrTest()
    Call GetItemOperation("""部品2生産実績_部品セリアルＮｏ,個数"" + ""部品2生産実績_部品セリアルＮｏ"" + 10")
End Sub

'*********************************************************
'* 演算式よりセルに埋め込む計算式を生成する　para:定義内容
'*********************************************************
Public Function GetItemOperation(calcStringOrg As String) As String
    Dim calcType        As Variant
    Dim startPos        As Integer
    Dim endPos          As Integer
    Dim splitStr1       As Variant
    Dim splitStr2       As Variant
    Dim splitStr3       As Variant
    Dim splitStr4       As Variant
    Dim splitStr5       As Variant
    Dim splitStr6       As Variant
    Dim currType        As Variant
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim workRange       As Range
    Dim workStr         As String
    Dim workStrCnt      As String
    Dim prefixStr       As String
    Dim workAddr        As String
    Dim dblQuota()      As Integer
    Dim II              As Integer
    Dim JJ              As Integer
    Dim calcStrArray    As Variant
    Dim calcString      As String
    Dim workCond        As Variant
    
    ReDim dblQuota(0)
    dblQuota(0) = -1
    
    GetItemOperation = ""
    
    '* COUNTA/SUM/AVERAGE/MAX/MIN/SUMPRODUCT
    calcType = Array("COUNTA(", "SUM(", "AVERAGE(", "MAX(", "MIN(", "SUMPRODUCT(1/COUNTIF(", "COUNTIF(", "COUNTIFS(")
    
    '* ダブルクォーテーションで囲まれた部分を抽出する
    '* →取得配列の奇数インデックスに入る
    calcStrArray = Split(calcStringOrg, """") '定義内容取込
    
    For II = 1 To UBound(calcStrArray) Step 2
    
        calcString = calcStrArray(II)  '定義内容名
    
        '* 演算式のチェック
        If InStr(calcString, "・") = 0 Then Exit Function
        
        ''''If InStr(calcString, "_") = 0 Then Exit Function
        
        '* カンマがない場合は、取得CTMの最新の値のみを対象とする
        If InStr(calcString, ",") = 0 Then
            
            '* 演算式から " で囲まれた文字列を取得する
            '* "部品1生産実績_部品セリアルＮｏ,個数" / 100 ⇒「=SUM(XX:YY) / 100」
            splitStr2 = Split(calcString, "・")    ' 「部品1生産実績」「部品セリアルＮｏ」
            
            ''''splitStr2 = Split(calcString, "_")    ' 「部品1生産実績」「部品セリアルＮｏ」
            
            'CTMシートから値の取込
            Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr2(0))
            With currWorksheet
                Set srchRange = .Range("1:1")
                Set startRange = srchRange.Find(What:=splitStr2(1), LookAt:=xlWhole)
                If Not startRange Is Nothing Then
                    Set workRange = startRange.Offset(1, 0)             '* 着目エレメントの最新データ範囲
                    '* 数式の生成:CTMシートのセル位置取得
                    workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                    '2017/09/29 No.544 Mod.  CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.
                    'workStr = .Name & "!" & workAddr
                    workStr = "'" & .Name & "'" & "!" & workAddr
                Else
                    Exit Function
                End If
            End With
        
        '* カンマがある場合は、演算形式に応じた式を生成する
        Else
        
            '* 演算式から " で囲まれた文字列を取得する
            '* "部品1生産実績_部品セリアルＮｏ,個数" / 100 ⇒「=SUM(XX:YY) / 100」
            splitStr2 = Split(calcString, ",")      ' 「部品1生産実績_部品セリアルＮｏ」「個数」（「合致条件」）
            splitStr3 = Split(splitStr2(0), "・")    ' 「部品1生産実績」「部品セリアルＮｏ」
            
            ''''splitStr3 = Split(splitStr2(0), "_")    ' 「部品1生産実績」「部品セリアルＮｏ」
                
            If InStr(splitStr2(1), "条件個数") Then
                workCond = splitStr2(2)
            
                Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                With currWorksheet
                    Set srchRange = .Range("1:1")
                    Set startRange = srchRange.Find(What:=splitStr3(1), LookAt:=xlWhole)
                    If Not startRange Is Nothing Then
                        Set lastRange = startRange.Offset(0, 0).End(xlDown)
                        Set workRange = .Range(startRange.Offset(1, 0), lastRange)  '* 着目エレメントのデータ範囲
                        '* データ範囲アドレスの取得
                        workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                        '* 数式の生成
                        '2017/09/29 No.544 Mod. CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.
                        'workStr = "COUNTIF(" & .Name & "!" & workAddr & ", ""=" & workCond & """)"
                        workStr = "COUNTIF(" & "'" & .Name & "'" & "!" & workAddr & ", ""=" & workCond & """)"
                    Else
                        Exit Function
                    End If
                End With
                
            ElseIf InStr(splitStr2(1), "単一条件") Or InStr(splitStr2(1), "複数条件") Then         '個数(単一条件)と個数(複数条件)の時
On Error GoTo errlable
                '* "部品1生産実績_部品セリアルＮｏ,個数" / 100 ⇒「=SUM(XX:YY) / 100」
                splitStr4 = Mid(splitStr2(1), 10, Len(splitStr2(1)) - 10)  ' 「部品1生産実績_部品セリアルＮｏ」「個数」（「合致条件」）
                splitStr5 = Split(splitStr4, ";")                          ' 「部品1生産実績」「部品セリアルＮｏ」
                
                Select Case Left(splitStr2(1), 8)
                    Case "個数(複数条件)"
                        prefixStr = calcType(7)
                    Case "個数(単一条件)"
                        prefixStr = calcType(6)
                End Select
                
                workStrCnt = ""                                                     '初期値がNULLで設定する
                For JJ = 0 To UBound(splitStr5)
                    splitStr6 = Split(splitStr5(JJ), ":")

                    Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                    With currWorksheet
                        Set srchRange = .Range("1:1")
                        Set startRange = srchRange.Find(What:=splitStr6(0), LookAt:=xlWhole)

                        If Not startRange Is Nothing Then
                            Set lastRange = startRange.Offset(0, 0).End(xlDown)
                            Set workRange = .Range(startRange.Offset(1, 0), lastRange)     '* 着目エレメントのデータ範囲
                            '* 数式の生成
                            workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                          
                            If prefixStr = calcType(6) Then                         '個数(単一条件)のみ時
                                workStrCnt = "'" & .Name & "'" & "!" & workAddr & "," & Chr(34) & splitStr6(1) & Chr(34)
                                Exit For
                            Else                                                    '個数(複数条件)のみ時
                                If workStrCnt = "" Then
                                    workStrCnt = "'" & .Name & "'" & "!" & workAddr & "," & Chr(34) & splitStr6(1) & Chr(34)
                                Else
                                    workStrCnt = workStrCnt & "," & "'" & .Name & "'" & "!" & workAddr & "," & Chr(34) & splitStr6(1) & Chr(34)
                                End If
                            End If
                        Else
                            Exit Function
                        End If

                    End With
                Next
                
                workStr = prefixStr & workStrCnt & ")"
                
errlable:
                If Err.Number <> 0 Then
                    MsgBox "演算形式が「個数(単一条件) 」または「個数(複数条件)」の場合、通常のエレメントリストからは選択できません｡エレメント|条件リストから選択してドラッグ＆ドロップ操作してください。"
                End If
            Else
                Select Case splitStr2(1)
                    Case "個数"
                        prefixStr = calcType(0)
                    Case "積算"
                        prefixStr = calcType(1)
                    Case "平均"
                        prefixStr = calcType(2)
                    Case "最大"
                        prefixStr = calcType(3)
                    Case "最小"
                        prefixStr = calcType(4)
                    Case "種類数"
                        prefixStr = calcType(5)
                End Select
            
                Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                With currWorksheet
                    Set srchRange = .Range("1:1")
                    Set startRange = srchRange.Find(What:=splitStr3(1), LookAt:=xlWhole)
                    If Not startRange Is Nothing Then
                        Set lastRange = startRange.Offset(0, 0).End(xlDown)
                        Set workRange = .Range(startRange.Offset(1, 0), lastRange)  '* 着目エレメントのデータ範囲
                        '* 数式の生成
                        workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                        '2017/09/29 No.544 Mod. CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.---------Start
                        If InStr(prefixStr, "SUMPRODUCT") Then
                            'workStr = prefixStr & .Name & "!" & workAddr & "," & .Name & "!" & workAddr & "))"
                            workStr = prefixStr & "'" & .Name & "'" & "!" & workAddr & "," & .Name & "!" & workAddr & "))"
                        Else
                            'workStr = prefixStr & .Name & "!" & workAddr & ")"
                            workStr = prefixStr & "'" & .Name & "'" & "!" & workAddr & ")"
                        End If
                        '2017/09/29 No.544 Mod. CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.---------End
                    Else
                        Exit Function
                    End If
                End With
                
            End If
            
        End If
    
        calcStringOrg = Replace(calcStringOrg, """" & calcString & """", workStr, 1)
    
    Next
    
    GetItemOperation = "=" & calcStringOrg

End Function

'***********************************
'* すべての図形と画面定義を削除する
'***********************************
Public Sub DeleteAllShapeAndDispDef()
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim currShape           As Shape
    Dim startRange          As Range
    Dim lastRange           As Range
    Dim workRange           As Range

    '2017/09/26 No.533 Add.
    Call SetFilenameToVariable

    '*****************
    '* 図形を削除する
    '*****************
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    With statusWorksheet
    
        For Each currShape In .Shapes
            currShape.Delete
        Next currShape
    
    End With
    
    '*********************
    '* 画面定義を削除する
    '*********************
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet
    
        Set startRange = .Range("A1")
        If startRange.Offset(1, 0).Value <> "" Then
            Set lastRange = startRange.Offset(0, 0).End(xlToRight)
            Set lastRange = lastRange.Offset(0, 3)
            Set workRange = .Range(startRange.Offset(1, 0), lastRange.Offset(100, 0))
            workRange.ClearContents
        End If
    
    End With
    
End Sub

'*******************************************
'* 指定のテキストボックスの背景色を変更する
'*******************************************
Public Sub ChangeBackColorTB(currWorksheet As Worksheet, shapeName As String, rgbFill As Long)
    Dim currShape       As Shape
    
    Set currShape = currWorksheet.Shapes(shapeName)
    With currShape
        '* 図形名
'        .Name = currShapeName
        '* 登録マクロ名
'        .OnAction = macroName
        With .TextFrame2
            '* 文字列配置位置
'            .VerticalAnchor = msoAnchorMiddle
            With .TextRange
                '* 文字列配置一
'                .ParagraphFormat.Alignment = msoAlignCenter
                '* 文字列太字設定
'                .Font.Bold = msoTrue
                '* 文字サイズ
'                .Font.Size = 16
                '* 文字色
'                .Font.Fill.ForeColor.RGB = RGB(0, 0, 0)
            End With
        End With
        '* 文字枠色
'        .Line.ForeColor.RGB = RGB(0, 0, 0)
        '* 文字枠太さ
'        .Line.Weight = 1.5
        '* 文字枠塗りつぶし
        .Fill.ForeColor.RGB = rgbFill
    End With
End Sub

'*******************************************
'* 表示範囲の背景色を変更する
'*******************************************
Public Sub ChangeBackColorStatus(currRange As Range, rgbFill As Long)
    
    '* 選択範囲を薄い灰色にする
    currRange.Interior.Color = rgbFill

End Sub

'*******************************************
'* RGBカラー配列取得（Long値）
'*******************************************
Public Sub GetRGBColorArray(rgbColor() As Long)
    Dim paramColWorksheet      As Worksheet
    Dim i                      As Integer
    Dim srchRange              As Range
    Dim currRange              As Range
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    With paramColWorksheet
    
        Set srchRange = .Range("1:1")
        Set currRange = srchRange.Find(What:="操作パネル表示", LookAt:=xlWhole)
        
        If Not currRange Is Nothing Then
            For i = 0 To 14
                If currRange.Offset(i + 1, 1).Value <> "" Then
                    rgbColor(i) = currRange.Offset(i + 1, 1).Value
                Else
                    rgbColor(i) = currRange.Offset(i + 1, 0).Value
                End If
        
            Next
        End If
    End With

End Sub

'*******************************************
'* RGB背景表示カラー配列取得（Long値）
'*******************************************
Public Sub GetRGBColorBackArray(rgbColorBack() As Long)
    Dim paramColWorksheet      As Worksheet
    Dim i                      As Integer
    Dim srchRange              As Range
    Dim currRange              As Range
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    With paramColWorksheet
    
    Set srchRange = .Range("1:1")
    Set currRange = srchRange.Find(What:="背景設定", LookAt:=xlWhole)
    
    If Not currRange Is Nothing Then
        For i = 0 To 3
            If currRange.Offset(i + 1, 1).Value <> "" Then
                rgbColorBack(i) = currRange.Offset(i + 1, 1).Value
            Else
                rgbColorBack(i) = currRange.Offset(i + 1, 0).Value
            End If
    
        Next
    End If
    End With

End Sub

'*******************************************
'* 背景色と全てのテキストボックスの内容更新
'*******************************************
Public Sub UpdateAllBackAndTB()
'2017/06/14 Del No.260.
'Attribute UpdateAllBackAndTB.VB_ProcData.VB_Invoke_Func = "r\n14"
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim paramWorksheet      As Worksheet
    Dim currWorksheet       As Worksheet
    Dim currShape           As Shape
    Dim currRange           As Range
    Dim nowColorRange       As Range
    Dim srchRange           As Range
    Dim dispRange           As Range
    Dim startRange          As Range
    Dim workRange           As Range
    Dim II                  As Integer
    Dim JJ                  As Integer
    Dim formulaStr          As String
    Dim shapeName           As String
    Dim isFormat            As Integer
    Dim rgbColor(15)        As Long
    Dim rgbColorBack(4)     As Long
    Dim splitValue          As Variant
    Dim getValue            As Variant
    Dim getValue1            As Variant
    Dim backColorCond(4)    As Variant
    Dim chgElement          As String
    Dim currValue           As Variant
    Dim dispAddress         As String
    
    Dim shp As Object
    Dim flagExist As Boolean
    
    '* 色配列を取得する
    Call GetRGBColorArray(rgbColor)
    Call GetRGBColorBackArray(rgbColorBack)
    
'    rgbColorBack(0) = rgbColor(13)  ''''''''''''''''''''''20160619
'    rgbColorBack(1) = rgbColor(1)
'    rgbColorBack(2) = rgbColor(4)
''    rgbColorBack(3) = rgbColor(8)
'    '* 緑だけ特別設定
'    rgbColorBack(3) = RGB(0, 176, 80)
    
    '2017/09/26 No.533 Add.
    Call SetFilenameToVariable
    
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    '**************************************************************************************
    '* 画面定義シートにある演算式を更新し、ステータスシート上のテキストボックスを更新する
    '**************************************************************************************
    With dispDefWorksheet
    
        Set srchRange = .Range("1:1")
        Set currRange = srchRange.Find(What:="定義内容", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
        
            '* 全図形に対して
            For II = 1 To 100
                '定義内容の有無ﾁｪｯｸ
                If currRange.Offset(II, 0).Value <> "" Then
                    
                    Set dispRange = srchRange.Find(What:="表示色−白", LookAt:=xlWhole)
                    Set dispRange = .Cells(currRange.Offset(II, 0).Row, dispRange.Column)
                
                    '* 数式を更新する
                    formulaStr = GetItemOperation(currRange.Offset(II, 0).Value)
                    
                    With currRange.Offset(II, 1)
                        .NumberFormatLocal = "G/標準"
                        .FormulaLocal = formulaStr
                        If IsDate(.Value) Then
                            .NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
                        ElseIf IsNumeric(.Value) Then
'                            If InStr(currRange.Offset(II, 0).Value, "個数") Or _
'                               InStr(currRange.Offset(II, 0).Value, "種類数") Then
                           '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                           'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                           '* ISSUE_NO.670 sunyi 2018/05/15 start
                            '* オーバーフローを修正
                            'If .Value = CLng(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                            If .Value = CDbl(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                            '* ISSUE_NO.670 sunyi 2018/05/15 end
                                '-2017/05/08 No.554 Mod. Changed # to 0.
                                .NumberFormatLocal = "0"
                            Else
                                .NumberFormatLocal = "0.00"
                            End If
                        End If
                        
                        isFormat = currRange.Offset(II, 2).Value
                        
                        '* テキストボックスへ数式の値をセットする
                        shapeName = currRange.Offset(II, -1).Value '図形名の取込　value_shape_01
                        
                        '2017/01/13 追加
                        '該当Shape(ﾃｷｽﾄBox)の有無チェック
                        flagExist = False
                        For Each shp In statusWorksheet.Shapes
                            If shp.Name = shapeName Then
                                flagExist = True
                                Exit For
                            End If
                        Next shp
                        
                        '2017/01/13
                        '該当のShapeがあれば、Shapeに値を設定
                        If flagExist = True Then
                            Set currShape = statusWorksheet.Shapes(shapeName)
                            On Error Resume Next
                            If currRange.Offset(II, 22).Value Then
                                currShape.TextFrame.Characters.text = currRange.Offset(II, 23).Value
                                
                                Select Case isFormat
                                    '* 数値処理
                                    Case 0
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If Val(splitValue(0)) <= .Value And .Value <= Val(splitValue(1)) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                Else
                                                    If Val(getValue) = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                End If
                                            End If
                                        Next
                                    '* 文字列処理
                                    Case 1
                                         '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If getValue = .Value Then
                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                    Exit For
                                                Else
                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                End If
                                            End If
                                        Next
                                    '* 日付処理
                                    Case 2
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                End If
                                            End If
                                        Next
                                    '* 時刻処理 2017/02/13 Add
                                    Case 3
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                End If
                                            End If
                                        Next
                                     '* 日時処理 2017/02/13 Add
                                    Case 4
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                End If
                                            End If
                                        Next
                                End Select
                            Else
                            
                                Select Case isFormat
                                    '* 数値処理
                                    Case 0
                                        '* 書式設定
        '                                If InStr(currRange.Offset(II, 0).Value, "個数") Or _
        '                                   InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                        '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                                        'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                                        '* ISSUE_NO.670 sunyi 2018/05/15 start
                                        '* オーバーフローを修正
                                        'If .Value = CLng(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                        If .Value = CDbl(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                        '* ISSUE_NO.670 sunyi 2018/05/15 end
                                            '-2017/05/08 No.554 Mod. Changed # to 0.
                                            currShape.TextFrame.Characters.text = Format(.Value, "0")
                                        Else
                                            currShape.TextFrame.Characters.text = Format(.Value, "0.00")
                                        End If
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If Val(splitValue(0)) <= .Value And .Value <= Val(splitValue(1)) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                Else
                                                    If Val(getValue) = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                End If
                                            End If
                                        Next
                                    '* 文字列処理
                                    Case 1
                                        '* 書式設定
                                        currShape.TextFrame.Characters.text = Format(.Value)
                                         '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If getValue = .Value Then
                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                    Exit For
                                                Else
                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                End If
                                            End If
                                        Next
                                    '* 日付処理
                                    Case 2
                                        '* 書式設定
                                        '20161104 追加
                                        If .Value = 0 Or .Value = "" Then
                                            currShape.TextFrame.Characters.text = ""
                                        Else
                                            '2017/09/26 No.539 Mod. Changed "yyyy/MM/dd hh:mm:ss" to "yyyy/MM/dd".
                                            currShape.TextFrame.Characters.text = Format(.Value, "yyyy/MM/dd")
                                            '* 色の処理
                                            For JJ = 0 To UBound(rgbColor) - 1
                                                getValue = dispRange.Offset(0, JJ).Value
                                                If getValue <> "" Then
                                                    If InStr(getValue, ",") Then
                                                        splitValue = Split(getValue, ",")
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                        End If
                                                    Else
                                                        If getValue = .Value Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If
                                    '* 時刻処理 2017/02/13 Add
                                    Case 3
                                        '* 書式設定
                                        '20161104 追加
                                        If .Value = 0 Or .Value = "" Then
                                            currShape.TextFrame.Characters.text = ""
                                        Else
                                            currShape.TextFrame.Characters.text = Format(.Value, "hh:mm:ss")
                                            '* 色の処理
                                            For JJ = 0 To UBound(rgbColor) - 1
                                                getValue = dispRange.Offset(0, JJ).Value
                                                If getValue <> "" Then
                                                    If InStr(getValue, ",") Then
                                                        splitValue = Split(getValue, ",")
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                        End If
                                                    Else
                                                        If getValue = .Value Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If
                                     '* 日時処理 2017/02/13 Add
                                    Case 4
                                        '* 書式設定
                                        '20161104 追加
                                        If .Value = 0 Or .Value = "" Then
                                            currShape.TextFrame.Characters.text = ""
                                        Else
                                            currShape.TextFrame.Characters.text = Format(.Value, "yyyy/MM/dd hh:mm:ss")
                                            '* 色の処理
                                            For JJ = 0 To UBound(rgbColor) - 1
                                                getValue = dispRange.Offset(0, JJ).Value
                                                If getValue <> "" Then
                                                    If InStr(getValue, ",") Then
                                                        splitValue = Split(getValue, ",")
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                        End If
                                                    Else
                                                        If getValue = .Value Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If
                                End Select
                End If
                        End If
                
                    End With    ' currRange.Offset(II, 1)
    
                End If  ' 図形情報があれば
                
            Next    ' 全図形
            
        End If  ' 「定義内容」項目があれば
        
    End With    ' dispDefWorksheet
    
    '**************************************************************************
    '* paramシートにある表示条件を取得し、ステータスシート上の背景色を更新する
    '**************************************************************************
    With paramWorksheet
    
        '* 表示範囲が設定されていればその範囲で表示する
        dispAddress = GetDisplayAddress(statusWorksheet)
        
        If dispAddress <> "" Then
        
            Set dispRange = statusWorksheet.Range(dispAddress)

            Set srchRange = .Range("A:A")
            Set currRange = srchRange.Find(What:="表示色対象", LookAt:=xlWhole)
            Set nowColorRange = srchRange.Find(What:="現在表示色", LookAt:=xlWhole)
            
        
            If Not currRange Is Nothing Then
            
                '* 表示色対象エレメント名を取得
                chgElement = currRange.Offset(0, 1).Value
                
                '---2016/12/19 Add No.386 [実行時エラー９対策] 表示色対象エレメントが設定されているときのみ処理 IF文追加 -------------Start
                If chgElement <> "" Then
                chgElement = Replace(chgElement, """", "")
            
                splitValue = Split(chgElement, "・")    ' 「部品1生産実績」「部品セリアルＮｏ」
                
                    ''''splitValue = Split(chgElement, "_")    ' 「部品1生産実績」「部品セリアルＮｏ」
                    
                    Set currWorksheet = Workbooks(thisBookName).Worksheets(splitValue(0))
                    With currWorksheet
                        Set srchRange = .Range("1:1")
                        Set startRange = srchRange.Find(What:=splitValue(1), LookAt:=xlWhole)
                        If Not startRange Is Nothing Then
                            Set workRange = startRange.Offset(1, 0)             '* 着目エレメントの最新データ範囲
                            '* 値の取得
                            currValue = workRange.Offset(0, 0).Value
                            
                            '* デフォルトカラーは緑固定
                            '2017/09/27 No.542 Del. Do not set default back color.
                            Call ChangeBackColorStatus(dispRange, RGB(255, 255, 255))
                            'nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(3))
                            
                            For II = 1 To 4
                                '''''''''''''''''''
                                '型の決定(10:数字 11:数字(カンマ区切りの二つの数）,20:文字列)
                                '''''''''''''''''''
                                If IsNumeric(currRange.Offset(II, 1).Value) Then
                                    
                                    splitValue = Split(currRange.Offset(II, 1).Value, ",")
                                    If UBound(splitValue) = 0 Then
                                        isFormat = 10
                                    ElseIf UBound(splitValue) = 1 Then
                                        isFormat = 11
                                    Else
                                        isFormat = 20
                                    End If
                                Else
                                    isFormat = 20
                                End If
                            
                                If isFormat = 10 Then
                                    getValue = CDbl(currRange.Offset(II, 1).Value)
                                    
                                    If currValue = getValue Then
                                    
                                        Call ChangeBackColorStatus(dispRange, rgbColorBack(II - 1))
                                        nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(II - 1))
                                        Exit For
                                    End If
                                ElseIf isFormat = 11 Then
                                    splitValue = Split(currRange.Offset(II, 1).Value, ",")
                                    If UBound(splitValue) = 1 Then
                                        getValue = CDbl(splitValue(0))
                                        getValue1 = CDbl(splitValue(1))
                                        If currValue >= getValue And currValue <= getValue1 Then
                                            Call ChangeBackColorStatus(dispRange, rgbColorBack(II - 1))
                                            nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(II - 1))
                                            Exit For
                                        End If
                                    End If
                                    
                                    
                                ElseIf isFormat = 20 Then
                                    If currValue = currRange.Offset(II, 1).Value Then
                                    
                                        Call ChangeBackColorStatus(dispRange, rgbColorBack(II - 1))
                                        nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(II - 1))
                                        Exit For
                                    End If
                                End If
                                
                                
                            
                                
                                
                            Next
                            '現在運転状態表示 20160619 B21固定   Chr(13) & Chr(10) &
                            currValue = Replace(currValue, "・", Chr(13) & Chr(10) & "")
                            currValue = Replace(currValue, "中", Chr(13) & Chr(10) & "中")
                            
                            Workbooks(thisBookName).Worksheets(ParamSheetName).Range("B21").Value = currValue
                            
                        End If
                    End With
                End If
                '---2016/12/19 Add No.386 [実行時エラー９対策] 表示色対象エレメントが設定されているときのみ処理 IF文追加 -------------End
            End If

        End If
    
    End With
End Sub

Private Function GetRGBHexString(lngRGB As Long) As String
    Dim strR As String
    Dim strG As String
    Dim strB As String
    Dim strHex As String

    '長整数の色表記をR・G・Bに分解
    strR = Right$("00" & Hex$((lngRGB And "&H" & "0000FF")), 2)
    strG = Right$("00" & Hex$((lngRGB And "&H" & "00FF00") / 256), 2)
    strB = Right$("00" & Hex$((lngRGB And "&H" & "FF0000") / 256 ^ 2), 2)
    
    'R・G・Bそれぞれの文字列を結合
    strHex = "#" & strR & strG & strB
    
    '結果をtxt16進表記に代入
    GetRGBHexString = strHex

End Function

'*****************
'* ファイル存在チェック
'*****************
Function IsFileExists(ByVal strFileName As String) As Boolean
    If Dir(strFileName, 16) <> Empty Then
        IsFileExists = True
    Else
        IsFileExists = False
    End If
End Function

'*************************************************
'* 既に開いたExcelを前面に表示
'*************************************************
Private Function BringAppToFront(linkStr As String) As Boolean
    If Applications Is Nothing Then
        Set Applications = CreateObject("Scripting.Dictionary")
    End If

On Error Resume Next
    If Applications.Exists(linkStr) Then
        Applications.Item(linkStr).ActiveWorkbook.Activate
        Call SetForegroundWindow(Applications.Item(linkStr).ActiveWindow.hwnd)
        If Err.Number <> 0 Then
            Call Applications.Remove(linkStr)
            Err.Clear
        Else
            BringAppToFront = True
            Exit Function
        End If
    End If

    BringAppToFront = False
End Function

'*************************************************
'* 既に開いたExcelを前面に表示
'*************************************************
Private Function ActivateWorkbookByPath(linkStr As String) As Boolean
    If excelUtil Is Nothing Then
        Set excelUtil = CreateObject("FoaCoreCom.common.util.ExcelUtil")
    End If
    ActivateWorkbookByPath = excelUtil.ActivateWorkbookByPath(linkStr)
End Function

'*************************************************
'* 既に開いたExcelを前面に表示
'*************************************************
Private Function ActivateWorkbookByApp(xlapp As Excel.Application) As Boolean
    If excelUtil Is Nothing Then
        Set excelUtil = CreateObject("FoaCoreCom.common.util.ExcelUtil")
    End If
    ActivateWorkbookByApp = excelUtil.ActivateWorkbookByApp(xlapp)
End Function

'*************************************************
'* テキストボックスクリック時に呼び出されるマクロ
'*************************************************
Public Sub ConditionOpen()
    Dim currShape           As Shape
    Dim dispDefWorksheet    As Worksheet
    Dim paramWorkhseet      As Worksheet
    Dim condParamsheet      As Worksheet
    Dim linkParamsheet      As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim rowRange            As Range
    Dim colRange            As Range
    Dim calcStr             As String
    Dim isFormat            As Integer
    Dim isLinkChk           As Boolean
    Dim linkStr             As String
    Dim isLinkFormat        As Integer
    Dim isElementChk        As Boolean
    Dim elementStr          As String
    Dim findNo              As Integer
    Dim findNo1             As Integer
    Dim intstr              As Variant
    Dim sSwitch             As String
    Dim sAutoUpdate         As String
    Dim II                  As Integer
    Dim J                   As Integer
    Dim iEndRow             As Integer
    Dim xlapp               As Excel.Application
    Dim fileName            As String
    
    '* 選択図形を取得
    Set currShape = ActiveSheet.Shapes(Application.Caller)
    
    Call SetFilenameToVariable

    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet

        '* 演算式および表示書式を取得
        Set srchRange = .Range("A:A")
        Set rowRange = srchRange.Find(What:=currShape.Name, LookAt:=xlWhole)
        If Not rowRange Is Nothing Then
            calcStr = rowRange.Offset(0, 1).Value
            isFormat = rowRange.Offset(0, 3).Value
            '* リンク情報を取得する
            isLinkChk = rowRange.Offset(0, 20).Value
            isLinkFormat = rowRange.Offset(0, 21).Value
            linkStr = rowRange.Offset(0, 22).Value
            isElementChk = rowRange.Offset(0, 23).Value
            elementStr = rowRange.Offset(0, 24).Value
            
        Else
            Exit Sub
        End If

        '* 表示色条件情報範囲を取得
        Set srchRange = .Range("1:1")
        Set colRange = srchRange.Find(What:="表示色−白", LookAt:=xlWhole)
        If Not colRange Is Nothing Then
            Set currRange = .Cells(rowRange.Row, colRange.Column)
        Else
            Exit Sub
        End If

    End With
    
    Set paramWorkhseet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With paramWorkhseet
        '* 編集モードを取得する
        Set srchRange = .Range("A:A")
        Set rowRange = srchRange.Find(What:="SwitchLink", LookAt:=xlWhole)
        If Not rowRange Is Nothing Then
            sSwitch = rowRange.Offset(0, 1).Value
        End If
        
        '* 自動更新を取得する
        Set rowRange = srchRange.Find(What:="自動更新", LookAt:=xlWhole)
        If Not rowRange Is Nothing Then
            sAutoUpdate = rowRange.Offset(0, 1).Value
        End If
        
'    '* 自動更新の場合、自動更新に停止する
'    If sAutoUpdate = "ON" Then
'        Call ChangeStartStopBT
'    End If
        
    End With

    '* リンクがあり、自動更新の場合、リンク先に遷移先する
    '* リンクがあり、更新停止、編集モードがOFFの場合、リンク先に遷移先する
    '* リンクがあり、更新停止、編集モードがONの場合、編集画面に遷移先する
    'If isLinkChk And (sAutoUpdate = "ON" Or (sAutoUpdate = "OFF" And sSwitch = "OFF")) Then
    
    '* リンクがあり、非編集モードの場合、リンク先に遷移先する
    If sSwitch = "OFF" Then
    
        'コマンドリストを取得する
        Set linkParamsheet = Workbooks(thisBookName).Worksheets(LinkParamSheetName)
        With linkParamsheet
            iEndRow = .UsedRange.Rows.Count
        
            For II = 1 To iEndRow
                
                If .Range("A" & II).Value = currShape.Name Then

                    'リンク先を取得する
                    linkStr = .Range("A" & II).Offset(0, 3)
                    If .Range("A" & II).Offset(0, 1) = 1 Then
                        '既に開いたExcelを前面に表示する
                        linkStr = Replace(linkStr, """", "")
                        fileName = Mid(linkStr, InStrRev(linkStr, "\"), Len(linkStr))
                        fileName = ThisWorkbook.path & fileName
                        
                        If IsFileExists(fileName) = False Then
                            If ActivateWorkbookByPath(linkStr) Then
                                Exit Sub
                            End If
                        Else
                            If ActivateWorkbookByPath(fileName) Then
                                Exit Sub
                            End If
                        End If

On Error GoTo errlable
                        'エクセルを開く
                        Set xlapp = New Excel.Application
                        xlapp.Visible = True
                        
                        If IsFileExists(fileName) = False Then
                            xlapp.Workbooks.Open (linkStr)
                        Else
                            xlapp.Workbooks.Open (fileName)
                        End If
                        
                        Call ActivateWorkbookByApp(xlapp)
                        Set xlapp = Nothing
                        
                        Err.Number = 0
errlable:
                        If Err.Number <> 0 Then
                            MsgBox "申し訳ございません。" & fileName & "が見つかりません。名前が変更されたか、移動や削除が行われた可能性があります。" & vbCrLf & "リンク先を確認し、再度リンク設定をしてください｡ "
                        End If
                    ElseIf .Range("A" & II).Offset(0, 1) = 2 Then
                    
                        'URLを開く
                        ActiveWorkbook.FollowHyperlink Address:=linkStr, NewWindow:=True
                    ElseIf .Range("A" & II).Offset(0, 1) = 3 Then
'Wang 外部取込みアプリケーションを起動 2018/04/27 Start
'外部取込みアプリケーションを起動し、異常終了の場合は後続処理を中断する
'                        'EXEを開く
'                        Shell linkStr, vbNormalFocus
                        
                        Dim parts() As String
                        Dim commandName As String
                        parts = Split(Application.WorksheetFunction.Trim(Replace(linkStr, Chr(9), " ")), " ")
                        commandName = Replace(GetFileName(parts(0)), """", "")

                        If StrComp(commandName, COMMAND_TriggerExData, vbTextCompare) = 0 Then
                            If ExecuteCommand(linkStr) <> 0 Then
                                Exit For
                            End If
                        Else
                            Call ExecuteCommand(linkStr)
                        End If
'Wang 外部取込みアプリケーションを起動 2018/04/27 End
                    End If
                End If
            Next
        End With
        Exit Sub
    End If
    
    Load ActionPaneForm
    With ActionPaneForm
        
        .Caption = "表示テキストボックス編集"
    
        '* 演算式
        .TB_ItemOperation.text = calcStr
        
        '* 演算形式
        If InStr(calcStr, "+") <> 0 Then
            intstr = Split(calcStr, "+")
        
            If InStr(intstr(0), ",") <> 0 Then
                findNo = InStr(intstr(0), ",") + 1
                findNo1 = InStr(intstr(0), "[")
                If findNo1 <> 0 Then
                    .CB_CalcFormat.text = Mid(Trim(intstr(0)), findNo, findNo1 - findNo)
                Else
                    .CB_CalcFormat.text = Mid(Trim(intstr(0)), findNo, Len(Trim(intstr(0))) - findNo)
                End If
            End If
        Else
            If InStr(calcStr, ",") <> 0 Then
                findNo = InStr(calcStr, ",") + 1
                findNo1 = InStr(calcStr, "[")
                
                If findNo1 <> 0 Then
                    .CB_CalcFormat.text = Mid(Trim(calcStr), findNo, findNo1 - findNo)
                Else
                    .CB_CalcFormat.text = Mid(Trim(calcStr), findNo, Len(Trim(calcStr)) - findNo)
                End If
                
            End If
        End If
        
        '* 図形名
        .LBL_ShapeName = currShape.Name
        '* 各条件
        .TB_White.text = currRange.Offset(0, 0).Value
        .TB_Red.text = currRange.Offset(0, 1).Value
        .TB_Lime.text = currRange.Offset(0, 2).Value
        .TB_Blue.text = currRange.Offset(0, 3).Value
        .TB_Yellow.text = currRange.Offset(0, 4).Value
        .TB_Magenta.text = currRange.Offset(0, 5).Value
        .TB_Aqua.text = currRange.Offset(0, 6).Value
        .TB_Maroon.text = currRange.Offset(0, 7).Value
        .TB_Green.text = currRange.Offset(0, 8).Value
        .TB_Navy.text = currRange.Offset(0, 9).Value
        .TB_Olive.text = currRange.Offset(0, 10).Value
        .TB_Purple.text = currRange.Offset(0, 11).Value
        .TB_Teal.text = currRange.Offset(0, 12).Value
        .TB_Silver.text = currRange.Offset(0, 13).Value
        .TB_Gray.text = currRange.Offset(0, 14).Value

        '* 「作成」ボタンを非表示
        .BT_CreateTextBox.Visible = False
        
        '* 書式ラジオボタンの設定
        Select Case isFormat
            Case 0
                .OB_Numeric.Value = True
            Case 1
                .OB_String.Value = True
            Case 2
                .OB_Date.Value = True
            Case 3
                .OB_Time.Value = True
            Case 4
                .OB_DateTime.Value = True
        End Select
        
        '* ISSUE_NO.694 sunyi 2018/05/16 start
        '* 単一、複数条件により、単一、複数条件の画面表示か非表示
        If .CB_CalcFormat.text = "個数(単一条件)" Or .CB_CalcFormat.text = "個数(複数条件)" Then
            .TB_CalcCondition.Visible = True
            .BT_add.Visible = True
            .BT_rowdelete.Visible = True
            .LV_ElementView.Visible = True
            .Label25.Visible = True
            .Label24.Visible = True
            .Label23.Visible = True
            .BT_ConditionUpdate.Visible = True
        Else
            .TB_CalcCondition.Visible = False
            .BT_add.Visible = False
            .BT_rowdelete.Visible = False
            .LV_ElementView.Visible = False
            .Label25.Visible = False
            .Label24.Visible = False
            .Label23.Visible = False
            .BT_ConditionUpdate.Visible = False
        End If
        '* ISSUE_NO.694 sunyi 2018/05/16 end
        
        '単一、複数条件を設定する
        Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
        iEndRow = condParamsheet.UsedRange.Rows.Count
        
        J = 0
        For II = 1 To iEndRow
                        
            If condParamsheet.Range("A" & II).Value = currShape.Name Then
                J = J + 1
                .LV_ElementView.ListItems.Add().text = J
                .LV_ElementView.ListItems(J).SubItems(1) = condParamsheet.Range("A" & II).Offset(0, 1)
                .LV_ElementView.ListItems(J).SubItems(2) = condParamsheet.Range("A" & II).Offset(0, 2)
                If J = 1 Then .TB_CalcCondition = condParamsheet.Range("A" & II).Offset(0, 2)
            End If
        Next
         
        '* Link先
        .CB_Link.Value = isLinkChk
        Select Case isLinkFormat
            Case 1
                .OB_StatusMonitor.Value = True
            Case 2
                .OB_URL.Value = True
            Case 3
                .OB_App.Value = True
        End Select
        
        .TB_ReferPass.text = linkStr
        '* エレメント値 非表示
        .TB_Name.text = elementStr
        .CB_Element.Value = isElementChk
        
        'コマンドリストを設定する
        Set linkParamsheet = Workbooks(thisBookName).Worksheets(LinkParamSheetName)
        iEndRow = linkParamsheet.UsedRange.Rows.Count
        
        J = 0
        For II = 1 To iEndRow
                        
            If linkParamsheet.Range("A" & II).Value = currShape.Name Then
                J = J + 1
                If linkParamsheet.Range("A" & II).Offset(0, 1) = 1 Then
                    .LV_CommandList.ListItems.Add().text = LINKBOX_TYPE_AIS
                ElseIf linkParamsheet.Range("A" & II).Offset(0, 1) = 2 Then
                    .LV_CommandList.ListItems.Add().text = LINKBOX_TYPE_URL
                ElseIf linkParamsheet.Range("A" & II).Offset(0, 1) = 3 Then
                    .LV_CommandList.ListItems.Add().text = LINKBOX_TYPE_APP
                End If
                .LV_CommandList.ListItems(J).SubItems(1) = linkParamsheet.Range("A" & II).Offset(0, 2)
                .LV_CommandList.ListItems(J).SubItems(2) = linkParamsheet.Range("A" & II).Offset(0, 3)
            End If
        Next
         
        .Show vbModeless
    
    End With

End Sub

'Wang 外部取込みアプリケーションを起動 2018/04/27 Start
'外部取込みアプリケーションを起動し、異常終了の場合は後続処理を中断する
Private Function GetFileName(path As String) As String
    Dim fso As Object
    Set fso = CreateObject("Scripting.FileSystemObject")
    GetFileName = fso.GetFileName(path)
    Set fso = Nothing
End Function

Private Function ExecuteCommand(command As String) As Long
    Dim objShell As Object
    
    Set objShell = VBA.CreateObject("WScript.Shell")
    ExecuteCommand = objShell.Run(command, 0, True)
    Set objShell = Nothing
End Function
'Wang 外部取込みアプリケーションを起動 2018/04/27 End

'************************
'* 数字⇒列号　変更する
'************************
Function GetColumnChar(char)
    Dim t
    On Error Resume Next
    If IsNumeric(char) Then
        t = Split(Cells(1, char).Address, "$")(1)
    Else
'        t = Columns(char & ":" & char).Column
        t = char
    End If
    If Err.Number <> 0 Then GetColumnChar = "超える範囲" Else GetColumnChar = t
End Function

'************************
'* 列号⇒数字　変更する
'************************
Function GetColumnNum(char)
    Dim t
    On Error Resume Next
    If IsNumeric(char) Then
'        t = Split(Cells(1, char).Address, "$")(1)
        t = char
    Else
        t = Columns(char & ":" & char).Column
    End If
    If Err.Number <> 0 Then GetColumnNum = "超える範囲" Else GetColumnNum = t
End Function

'*************************************************
'* 画面の情報を元に　異常履歴表示内容を生成する
'*************************************************
Public Sub WriteValueErrHistory()
    
    Dim allData                 As Variant
    Dim errHisWorksheet         As Worksheet
    Dim dataWorksheet           As Worksheet
    Dim statusWorksheet         As Worksheet
    
    Dim startRange              As Range
    Dim msnRange                As Range
    
    Dim iStartCol               As Integer
    Dim iStartRow               As Integer
    Dim sDisplayCnt             As String
    Dim sStartColValue          As String
    Dim iEndRow                 As Integer
    Dim iEndCol                 As Integer
    Dim sEndColValue            As String
    Dim iConditionColNum        As Integer
    Dim sDataSheetName          As String
    
    Dim II                      As Integer
    Dim JJ                      As Integer
    Dim KK                      As Integer
    
    Call SetFilenameToVariable
                
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set errHisWorksheet = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)

    '* 表示用データを取得する
    sDataSheetName = errHisWorksheet.Range("C6").Value
    If sDataSheetName = "" Then Exit Sub
    
    Set dataWorksheet = Workbooks(thisBookName).Worksheets(sDataSheetName)
    With dataWorksheet
        iEndRow = .UsedRange.Rows.Count
        iEndCol = .UsedRange.Columns.Count
    End With
    
'    '* 設定した表示エレメントを取得する
'    With errHisWorksheet
'
'        '* データ表示開始列
'        sStartColValue = .Range("B1").Value
'        iStartCol = GetColumnNum(sStartColValue)
'        '* データ表示開始行
'        iStartRow = .Range("B2").text
'
'        iEndRow = 11
'
'        sEndColValue = GetColumnChar(iEndCol)
'        allData = dataWorksheet.Range("A1:" & sEndColValue & iEndRow)
'
'        '* A6セルをスタートセルとする
'        Set startRange = .Range("A6")
'
'        Dim sCellFormat As String
'        Dim iIndex As Integer
'        iIndex = 0
'        '* 設定した表示用エレメントを取得する
'        For II = 0 To 100
'            Set msnRange = startRange.Offset(II, 0)
'            If msnRange.Offset(0, 0).Value <> "" Then
'
'                For JJ = 1 To iEndCol
'
'                    If msnRange.Offset(0, 0).Value = allData(1, JJ) Then
'                        For KK = 1 To iEndRow
'
'                            '* データ
'                            statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Value = allData(KK, JJ)
'                            '* フォーマット
'                            If msnRange.Offset(0, 1).Value = "数値" Then
'                                sCellFormat = ""
'                            ElseIf msnRange.Offset(0, 1).Value = "文字列" Then
'                                sCellFormat = "@"
'                            ElseIf msnRange.Offset(0, 1).Value = "日付" Then
'                                sCellFormat = "yyyy/MM/dd"
'                            ElseIf msnRange.Offset(0, 1).Value = "時刻" Then
'                                sCellFormat = "hh:mm:ss"
'                            ElseIf msnRange.Offset(0, 1).Value = "日時" Then
'                                sCellFormat = "yyyy/MM/dd hh:mm:ss"
'                            End If
'                            statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).NumberFormatLocal = sCellFormat
'
'                        Next KK
'                        iIndex = iIndex + 1
'                    End If
'                Next JJ
'            Else
'                Exit For
'            End If
'        Next II
'
''        statusWorksheet.Range("A1").Activate
''        statusWorksheet.Columns(GetColumnChar(iStartCol) & ":" & GetColumnChar(iEndCol)).EntireColumn.AutoFit
'
'    End With
    
    '* トルク表示用データを作成する
    If errHisWorksheet.Range("D1").Value = "" Then
        Call TorqueDataCreate
        Call display_all_data
    End If
End Sub

'*************************************************
'* 画面の情報を元に　異常履歴表示内容を削除する
'*************************************************
Public Sub DeleteErrHistory(sFlg As String)
     
    Dim startRange              As Range
    Dim msnRange                As Range
    Dim statusWorksheet         As Worksheet
    Dim errHisWorksheet    As Worksheet
    
    Dim iStartCol               As Integer
    Dim iStartRow               As Integer
    Dim sStartColValue          As String
    
    Dim II                      As Integer
    
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set errHisWorksheet = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    
    With errHisWorksheet

        'データ表示開始列
        sStartColValue = .Range("B1").Value
        If sStartColValue = "" Then Exit Sub
        iStartCol = GetColumnNum(sStartColValue)
        'データ表示開始行
        iStartRow = .Range("B2").Value

    End With

    ' ステータスシートのデータを削除する
    ' データを削除する
    statusWorksheet.Range(Rows(iStartRow), Rows(statusWorksheet.UsedRange.Rows.Count)).ClearContents
    
    If sFlg = "0" Then
    
        '* 異常履歴画面機能シートのデータを削除する
        With errHisWorksheet
    
            '* データ表示開始列
            .Range("B1").Value = ""
    
            '* データ表示開始行
            .Range("B2").Value = ""
        
            '* A６セルをスタートセルとする
            Set startRange = .Range("A6")
    
            '* 設定した表示用エレメントを削除する
            For II = 0 To 100
                Set msnRange = startRange.Offset(II, 0)
                If msnRange.Offset(0, 0).Value <> "" Then
                    msnRange.Offset(0, 0).Value = ""
                    msnRange.Offset(0, 1).Value = ""
                    msnRange.Offset(0, 2).Value = ""
                Else
                    Exit For
                End If
            Next II
        End With
    End If
        
End Sub

'****************************
'* トルク表示用データを作成する
'****************************
Public Sub TorqueDataCreate()

    Dim torqueSetDataWorksheet    As Worksheet
    Dim maxTorWorksheet           As Worksheet
    Dim avgTorWorksheet           As Worksheet
    Dim ctmDataWorksheet          As Worksheet
    
    Dim allData                 As Variant
    Dim iEndRow                 As Long
    Dim iEndCol                 As Long
    Dim sDataSheetName          As String
    Dim iTimeaxisColNum         As Long
    
    Dim II                      As Long
    Dim JJ                      As Long
    Dim KK                      As Long
    Dim countColCnt             As Long
    Dim lIndex                  As Long
    Dim axisNum                 As Integer
    Dim sEndColValue            As String
    Dim rbName                  As String
    
    Dim maxMyArray(366, 15)     As String
    Dim avgMyArray(366, 15)     As String
    
    Set torqueSetDataWorksheet = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    Set maxTorWorksheet = Workbooks(thisBookName).Worksheets(MaxTorqueSheetName)
    Set avgTorWorksheet = Workbooks(thisBookName).Worksheets(AvgTorqueSheetName)

    '* 表示用データを取得する
    sDataSheetName = torqueSetDataWorksheet.Range("C6").Value
    If sDataSheetName = "" Then Exit Sub
    Set ctmDataWorksheet = Workbooks(thisBookName).Worksheets(sDataSheetName)
    With ctmDataWorksheet
        iEndCol = .UsedRange.Columns.Count
    End With
    
    sEndColValue = GetColumnChar(iEndCol)
    iEndRow = 367
    allData = ctmDataWorksheet.Range("A1:" & sEndColValue & iEndRow)
    If allData(2, 1) = "" Then
        '* グラフのテキストバックスの表示非表示を設定
        Call setGraphButtonDidsplay(rbName, sDataSheetName)
        
        '* 表示種別に週を設定する
        torqueSetDataWorksheet.Range("D1").Value = 8
        
        Exit Sub
    End If
    
    '* ロボット名を取得する
    For JJ = 1 To iEndCol
        If InStr(allData(1, JJ), "コントローラ名") > 0 Then
            rbName = allData(2, JJ)
            Exit For
        End If
    Next JJ
    If rbName = "" Then
        '* グラフのテキストバックスの表示非表示を設定
        Call setGraphButtonDidsplay(rbName, sDataSheetName)
        
        '* 表示種別に週を設定する
        torqueSetDataWorksheet.Range("D1").Value = 8
        
        Exit Sub
    End If
    
    lIndex = 0
    '* 時間軸データの設定
    For II = 1 To iEndRow

        If allData(II, 1) = "" Then
            Exit For
        End If
        If II <= 2 Then
            maxMyArray(lIndex, 0) = allData(II, 1)
            avgMyArray(lIndex, 0) = allData(II, 1)
            lIndex = lIndex + 1
        ElseIf FormatDateTime(allData(II, 1), vbLongDate) <> FormatDateTime(allData(II - 1, 1), vbLongDate) Then
            maxMyArray(lIndex, 0) = allData(II, 1)
            avgMyArray(lIndex, 0) = allData(II, 1)
            lIndex = lIndex + 1
        End If
    Next II
    
    countColCnt = 1
    axisNum = 1
    For JJ = 1 To iEndCol

        
        If InStr(allData(1, JJ), "軸番号" & axisNum) > 0 Then
            If allData(2, JJ) <> "" Then
                
                '* ヘッダー(軸)を設定する
                maxMyArray(0, countColCnt) = allData(2, JJ)
                avgMyArray(0, countColCnt) = allData(2, JJ)

                lIndex = 1
                '* データ設定
                For KK = 2 To iEndRow
    
                    If allData(KK, 1) = "" Then
                        Exit For
                    End If
                    If KK <= 2 Then
                        maxMyArray(lIndex, countColCnt) = allData(KK, JJ + 1)
                        avgMyArray(lIndex, countColCnt) = allData(KK, JJ + 2)
                        lIndex = lIndex + 1
                    ElseIf FormatDateTime(allData(KK, 1), vbLongDate) <> FormatDateTime(allData(KK - 1, 1), vbLongDate) Then
                        maxMyArray(lIndex, countColCnt) = allData(KK, JJ + 1)
                        avgMyArray(lIndex, countColCnt) = allData(KK, JJ + 2)
                        lIndex = lIndex + 1
                    End If
    
                Next KK
                 
                countColCnt = countColCnt + 1
            End If
            axisNum = axisNum + 1
        End If
    Next JJ

    With maxTorWorksheet
        .Range("A1:N367").ClearContents
        For II = 0 To 15
            For KK = 0 To 366
                If maxMyArray(KK, II) = "" Then
                    Exit For
                End If
                .Cells(KK + 1, II + 1).Value = maxMyArray(KK, II)
            Next KK
        Next II
    End With
    
    With avgTorWorksheet
        .Range("A1:N367").ClearContents
        For II = 0 To 15
            For KK = 0 To 366
                If maxMyArray(KK, II) = "" Then
                    Exit For
                End If
                .Cells(KK + 1, II + 1).Value = avgMyArray(KK, II)
            Next KK
        Next II
    End With
    
    '* グラフのテキストバックスの表示非表示を設定
    Call setGraphButtonDidsplay(rbName, sDataSheetName)
    
    If torqueSetDataWorksheet.Range("D1").Value = "" Then
        '* 表示種別に週を設定する
        torqueSetDataWorksheet.Range("D1").Value = 8
    End If
    
End Sub

'********************************************
'* グラフのテキストバックスの表示非表示を設定
'********************************************
Public Sub setGraphButtonDidsplay(rbName As String, sDataSheetName As String)

    Dim statusWorksheet     As Worksheet
    Dim maxTorWorksheet     As Worksheet
    Dim avgTorWorksheet     As Worksheet
    
    Dim iBtnCount           As Integer
    
    Dim currShape           As Shape
    Dim II                  As Integer


    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set maxTorWorksheet = Workbooks(thisBookName).Worksheets(MaxTorqueSheetName)
    Set avgTorWorksheet = Workbooks(thisBookName).Worksheets(AvgTorqueSheetName)
    
    iBtnCount = maxTorWorksheet.UsedRange.Columns.Count
    
    With statusWorksheet
    
        For II = 1 To iBtnCount - 1

            Set currShape = .Shapes("M_" & II)
            currShape.TextFrame2.TextRange.text = maxTorWorksheet.Range(GetColumnChar(II + 1) & "1")
            currShape.Visible = msoCTrue
            Set currShape = .Shapes("TM_" & II)
            currShape.TextFrame2.TextRange.text = maxTorWorksheet.Range(GetColumnChar(II + 1) & "2")
            currShape.Visible = msoCTrue
'            Set currShape = .Shapes("P_" & II)
'            currShape.TextFrame2.TextRange.text = avgTorWorksheet.Range(GetColumnChar(II + 1) & "1")
'            currShape.Visible = msoCTrue
            Set currShape = .Shapes("TP_" & II)
            currShape.TextFrame2.TextRange.text = avgTorWorksheet.Range(GetColumnChar(II + 1) & "2")
            currShape.Visible = msoCTrue
        Next
    
        For II = iBtnCount To 15

            Set currShape = .Shapes("M_" & II)
            currShape.Visible = msoFalse
            Set currShape = .Shapes("TM_" & II)
            currShape.Visible = msoFalse
'            Set currShape = .Shapes("P_" & II)
'            currShape.Visible = msoFalse
            Set currShape = .Shapes("TP_" & II)
            currShape.Visible = msoFalse
        Next
    End With
        
    Set currShape = statusWorksheet.Shapes("ALL")
    currShape.Visible = msoCTrue
    currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent6
    Set currShape = statusWorksheet.Shapes("MAXTor")
    currShape.Visible = msoCTrue
    Set currShape = statusWorksheet.Shapes("AVGTor")
    currShape.Visible = msoCTrue
    Set currShape = statusWorksheet.Shapes("display_w")
    currShape.Visible = msoCTrue
    currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent3
    Set currShape = statusWorksheet.Shapes("display_m")
    currShape.Visible = msoCTrue
    currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent2
    Set currShape = statusWorksheet.Shapes("display_y")
    currShape.Visible = msoCTrue
    currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent2
    Set currShape = statusWorksheet.Shapes("Rectangle 1")
    With currShape
        With .TextFrame2
            .TextRange.Characters.text = STEAM_MODULE_NAME & ":" & GetModelName(STEAM_MODULE_NAME, sDataSheetName)
        End With
    End With
    currShape.Visible = msoCTrue
    
    statusWorksheet.ChartObjects("グラフ 1").Activate
    ActiveChart.SetSourceData Source:=maxTorWorksheet.Range("A1:" & GetColumnChar(iBtnCount) & "8")
    ActiveChart.ChartTitle.text = rbName & "最大トルク履歴グラフ"
    ActiveChart.ChartType = xlLine
    statusWorksheet.Shapes("グラフ 1").Height = 241
    statusWorksheet.Shapes("グラフ 1").Width = 322
    
    statusWorksheet.ChartObjects("グラフ 2").Activate
    ActiveChart.SetSourceData Source:=avgTorWorksheet.Range("A1:" & GetColumnChar(iBtnCount) & "8")
    ActiveChart.ChartTitle.text = rbName & "平均トルク履歴グラフ"
    ActiveChart.ChartType = xlLine
    statusWorksheet.Shapes("グラフ 2").Height = 241
    statusWorksheet.Shapes("グラフ 2").Width = 322

End Sub

Public Sub deleteOldData()
    Dim maxSheet As Worksheet
    Dim avgSheet As Worksheet
    Dim missionSheet As Worksheet
    Dim i As Long
    Dim CtmName As String
    
    Set maxSheet = Worksheets(MaxTorqueSheetName)
    Call maxSheet.Cells.Clear
    
    Set avgSheet = Worksheets(AvgTorqueSheetName)
    Call avgSheet.Cells.Clear

    Set missionSheet = Worksheets(MissionSheetName)
    i = 1
    CtmName = missionSheet.Cells(i, 5).Value
    While Not CtmName = ""
        Dim dataSheet As Worksheet
        Set dataSheet = Worksheets(CtmName)
        
        Dim delRows As Long
        Dim delCols As Long
        
        delRows = dataSheet.UsedRange.Rows.Count
        delCols = dataSheet.UsedRange.Columns.Count
        
        If delRows > 1 Then
            Dim dataRange As Range
            Set dataRange = dataSheet.Range(dataSheet.Cells(2, 1), dataSheet.Cells(delRows, delCols))
            Call dataRange.Cells.Clear
        End If
        
        i = i + 1
        CtmName = missionSheet.Cells(i, 5).Value
    Wend

End Sub

'********************************************
'* 履歴表示（週）ボタンの処理
'********************************************
Public Sub refreshData(days As Long)

    Dim paramSheet As Worksheet
    Dim startRange As Range
    Dim srchRange As Range
    
    Set paramSheet = Worksheets(ParamSheetName)
    
    Set startRange = paramSheet.Cells(1, 1).EntireColumn
    Set srchRange = startRange.Find(What:="取得期間", LookAt:=xlPart)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = days * 24
    End If

    '既存データを削除
    Call deleteOldData

    ' データを取得
    Call OperationTest
    
    ' 表示用データを作成
    Call TorqueDataCreate
End Sub

'********************************************
'* 履歴表示（週）ボタンの処理
'********************************************
Public Sub display_w_click()
    Dim torqueWorksheet As Worksheet
    '* 表示種別を設定する
    Set torqueWorksheet = ThisWorkbook.Worksheets(TorqueSetDataSheetName)
    torqueWorksheet.Range("D1").Value = 8
    
    Call refreshData(8)

    '* グラフデータ再設定
    Call set_display_data(8)
    '* 表示ボタン色再設定
    Call setDisplayButtonColor(8)
End Sub

'********************************************
'* 履歴表示（年）ボタンの処理
'********************************************
Public Sub display_m_click()
    Dim torqueWorksheet As Worksheet
    '* 表示種別を設定する
    Set torqueWorksheet = ThisWorkbook.Worksheets(TorqueSetDataSheetName)
    torqueWorksheet.Range("D1").Value = 32
    
    Call refreshData(32)

    '* グラフデータ再設定
    Call set_display_data(32)
    '* 表示ボタン色再設定
    Call setDisplayButtonColor(32)
End Sub

'********************************************
'* 履歴表示（年）ボタンの処理
'********************************************
Public Sub display_y_click()
    Dim torqueWorksheet As Worksheet
    '* 表示種別を設定する
    Set torqueWorksheet = ThisWorkbook.Worksheets(TorqueSetDataSheetName)
    torqueWorksheet.Range("D1").Value = 366
    
    Call refreshData(366)

    '* グラフデータ再設定
    Call set_display_data(366)
    '* 表示ボタン色再設定
    Call setDisplayButtonColor(366)
End Sub

Sub TorqueButtonFunction()

End Sub

Public Sub model_no_Click()
    
End Sub

'****************************
'* 表示ボタン色再設定
'****************************
Sub setDisplayButtonColor(btnNum As Integer)

    Dim statusWorksheet     As Worksheet
    Dim currShape           As Shape
    Dim II                  As Integer

    Call SetFilenameToVariable

    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)

    If btnNum = 8 Then
        Set currShape = statusWorksheet.Shapes("display_w")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent3
        Set currShape = statusWorksheet.Shapes("display_m")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent2
        Set currShape = statusWorksheet.Shapes("display_y")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent2
    ElseIf btnNum = 32 Then
        Set currShape = statusWorksheet.Shapes("display_w")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent2
        Set currShape = statusWorksheet.Shapes("display_m")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent3
        Set currShape = statusWorksheet.Shapes("display_y")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent2
    Else
        Set currShape = statusWorksheet.Shapes("display_w")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent2
        Set currShape = statusWorksheet.Shapes("display_m")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent2
        Set currShape = statusWorksheet.Shapes("display_y")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent3
    End If
    
    '* ボタン色再設定
    Call setButtonColor("M_", 0)
'    Call setButtonColor("P_", 0)

End Sub

'********************
'* グラフデータ再設定
'********************
Public Sub set_display_data(iDisplayCnt As Integer)

    Dim torqueWorksheet     As Worksheet
    Dim statusWorksheet     As Worksheet
    Dim maxTorWorksheet     As Worksheet
    Dim avgTorWorksheet     As Worksheet
    Dim iDataRowCnt         As Integer
    Dim iDataColCnt         As Integer
    
    Call SetFilenameToVariable
    
    Set torqueWorksheet = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set maxTorWorksheet = Workbooks(thisBookName).Worksheets(MaxTorqueSheetName)
    Set avgTorWorksheet = Workbooks(thisBookName).Worksheets(AvgTorqueSheetName)
    iDataRowCnt = maxTorWorksheet.UsedRange.Rows.Count
    iDataColCnt = maxTorWorksheet.UsedRange.Columns.Count
    If iDataRowCnt >= iDisplayCnt And iDisplayCnt > 0 Then
        iDataRowCnt = iDisplayCnt
    End If
    
    statusWorksheet.ChartObjects("グラフ 1").Activate
    ActiveChart.SetSourceData Source:=maxTorWorksheet.Range("A1:" & GetColumnChar(iDataColCnt) & iDataRowCnt)
    
    statusWorksheet.ChartObjects("グラフ 2").Activate
    ActiveChart.SetSourceData Source:=avgTorWorksheet.Range("A1:" & GetColumnChar(iDataColCnt) & iDataRowCnt)
End Sub

'***********************
'* 各軸ボタンの処理
'***********************
Sub M_1_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(2)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(2)
    '* ボタン色再設定
    Call setButtonColor("M_", 1)
End Sub
Sub M_2_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(3)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(3)
    '* ボタン色再設定
    Call setButtonColor("M_", 2)
End Sub
Sub M_3_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(4)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(4)
    '* ボタン色再設定
    Call setButtonColor("M_", 3)
End Sub
Sub M_4_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(5)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(5)
    '* ボタン色再設定
    Call setButtonColor("M_", 4)
End Sub
Sub M_5_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(6)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(6)
    '* ボタン色再設定
    Call setButtonColor("M_", 5)
End Sub
Sub M_6_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(7)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(7)
    '* ボタン色再設定
    Call setButtonColor("M_", 6)
End Sub
Sub M_7_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(8)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(8)
    '* ボタン色再設定
    Call setButtonColor("M_", 7)
End Sub
Sub M_8_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(9)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(9)
    '* ボタン色再設定
    Call setButtonColor("M_", 8)
End Sub
Sub M_9_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(10)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(10)
    '* ボタン色再設定
    Call setButtonColor("M_", 9)
End Sub
Sub M_10_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(11)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(11)
    '* ボタン色再設定
    Call setButtonColor("M_", 10)
End Sub
Sub M_11_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(12)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(12)
    '* ボタン色再設定
    Call setButtonColor("M_", 11)
End Sub
Sub M_12_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(13)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(13)
    '* ボタン色再設定
    Call setButtonColor("M_", 12)
End Sub
Sub M_13_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(14)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(14)
    '* ボタン色再設定
    Call setButtonColor("M_", 13)
End Sub
Sub M_14_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(15)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(15)
    '* ボタン色再設定
    Call setButtonColor("M_", 14)
End Sub
Sub M_15_click()

    '* 最大グラフ単一データ再設定
    Call set_single_display_data(16)
    '* 平均グラフ単一データ再設定
    Call set_single_display_data_p(16)
    '* ボタン色再設定
    Call setButtonColor("M_", 15)
End Sub

'****************************
'* ボタン色再設定
'****************************
Sub setButtonColor(btnType As String, btnNum As Integer)

    Dim statusWorksheet     As Worksheet
    Dim currShape           As Shape
    Dim II                  As Integer

    Call SetFilenameToVariable

    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    With statusWorksheet
        For II = 1 To 15
            Set currShape = .Shapes(btnType & II)
            currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent1
        Next
    End With
    If btnNum = 0 Then
        Set currShape = statusWorksheet.Shapes("ALL")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent6
    Else
        Set currShape = statusWorksheet.Shapes("ALL")
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent1
        
        Set currShape = statusWorksheet.Shapes(btnType & btnNum)
        currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent6
    End If

End Sub

'*************************
'* 最大グラフ単一データ再設定
'*************************
Public Sub set_single_display_data(btnNum As Integer)

    Dim torqueWorksheet     As Worksheet
    Dim statusWorksheet     As Worksheet
    Dim maxTorWorksheet     As Worksheet
    Dim iDataRowCnt         As Integer
    Dim iDataColCnt         As Integer
    Dim iDisplayCnt         As Integer
    
    Call SetFilenameToVariable

    Set torqueWorksheet = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set maxTorWorksheet = Workbooks(thisBookName).Worksheets(MaxTorqueSheetName)
    
    iDataRowCnt = maxTorWorksheet.UsedRange.Rows.Count
    iDataColCnt = maxTorWorksheet.UsedRange.Columns.Count
    iDisplayCnt = torqueWorksheet.Range("D1").Value
    
    If iDataRowCnt >= iDisplayCnt Then
        iDataRowCnt = iDisplayCnt
    End If
    
    statusWorksheet.ChartObjects("グラフ 1").Activate
    ActiveChart.SetSourceData Source:=maxTorWorksheet.Range( _
        "A1:A" & iDataRowCnt & "," & GetColumnChar(btnNum) & "1:" & GetColumnChar(btnNum) & iDataRowCnt)
    
End Sub

''***********************
''* 各軸ボタンの処理
''***********************
'Sub P_1_click()
'
'    Call set_single_display_data_p(2)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 1)
'End Sub
'Sub P_2_click()
'
'    Call set_single_display_data_p(3)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 2)
'End Sub
'Sub P_3_click()
'
'    Call set_single_display_data_p(4)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 3)
'End Sub
'Sub P_4_click()
'
'    Call set_single_display_data_p(5)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 4)
'End Sub
'Sub P_5_click()
'
'    Call set_single_display_data_p(6)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 5)
'End Sub
'Sub P_6_click()
'
'    Call set_single_display_data_p(7)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 6)
'End Sub
'Sub P_7_click()
'
'    Call set_single_display_data_p(8)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 7)
'End Sub
'Sub P_8_click()
'
'    Call set_single_display_data_p(9)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 8)
'End Sub
'Sub P_9_click()
'
'    Call set_single_display_data_p(10)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 9)
'End Sub
'Sub P_10_click()
'
'    Call set_single_display_data_p(11)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 10)
'End Sub
'Sub P_11_click()
'
'    Call set_single_display_data_p(12)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 11)
'End Sub
'Sub P_12_click()
'
'    Call set_single_display_data_p(13)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 12)
'End Sub
'Sub P_13_click()
'
'    Call set_single_display_data_p(14)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 13)
'End Sub
'Sub P_14_click()
'
'    Call set_single_display_data_p(15)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 14)
'End Sub
'Sub P_15_click()
'
'    Call set_single_display_data_p(16)
'    '* ボタン色再設定
'    Call setButtonColor("P_", 15)
'End Sub

'*************************
'* 平均グラフ単一データ再設定
'*************************
Public Sub set_single_display_data_p(btnNum As Integer)

    Dim torqueWorksheet     As Worksheet
    Dim statusWorksheet     As Worksheet
    Dim maxTorWorksheet     As Worksheet
    Dim avgTorWorksheet     As Worksheet
    Dim iDataRowCnt         As Integer
    Dim iDataColCnt         As Integer
    Dim iDisplayCnt         As Integer
    
    Call SetFilenameToVariable

    Set torqueWorksheet = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set avgTorWorksheet = Workbooks(thisBookName).Worksheets(AvgTorqueSheetName)
    
    iDataRowCnt = avgTorWorksheet.UsedRange.Rows.Count
    iDataColCnt = avgTorWorksheet.UsedRange.Columns.Count
    iDisplayCnt = torqueWorksheet.Range("D1").Value
    
    If iDataRowCnt >= iDisplayCnt Then
        iDataRowCnt = iDisplayCnt
    End If
    
    statusWorksheet.ChartObjects("グラフ 2").Activate
    ActiveChart.SetSourceData Source:=avgTorWorksheet.Range( _
        "A1:A" & iDataRowCnt & "," & GetColumnChar(btnNum) & "1:" & GetColumnChar(btnNum) & iDataRowCnt)
'    ActiveChart.ChartTitle.text = "平均トルク履歴グラフ"
End Sub

'*************************
'* ALLボタンの処理
'*************************
Public Sub display_all_data()

    Dim torqueWorksheet     As Worksheet
    Dim statusWorksheet     As Worksheet
    Dim maxTorWorksheet     As Worksheet
    Dim avgTorWorksheet     As Worksheet
    Dim iDataRowCnt         As Integer
    Dim iDataColCnt         As Integer
    Dim iDisplayCnt         As Integer
    
    Call SetFilenameToVariable

    Set torqueWorksheet = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set maxTorWorksheet = Workbooks(thisBookName).Worksheets(MaxTorqueSheetName)
    Set avgTorWorksheet = Workbooks(thisBookName).Worksheets(AvgTorqueSheetName)
    
    iDataRowCnt = maxTorWorksheet.UsedRange.Rows.Count
    iDataColCnt = maxTorWorksheet.UsedRange.Columns.Count
    iDisplayCnt = torqueWorksheet.Range("D1").Value
    
    If iDataRowCnt >= iDisplayCnt And iDisplayCnt > 0 Then
        iDataRowCnt = iDisplayCnt
    End If

    statusWorksheet.ChartObjects("グラフ 1").Activate
    ActiveChart.SetSourceData Source:=maxTorWorksheet.Range("A1:" & GetColumnChar(iDataColCnt) & iDataRowCnt)
    ActiveChart.Axes(xlCategory).TickLabels.NumberFormatLocal = "yyyy/m/d"
    
    statusWorksheet.ChartObjects("グラフ 2").Activate
    ActiveChart.SetSourceData Source:=avgTorWorksheet.Range("A1:" & GetColumnChar(iDataColCnt) & iDataRowCnt)
    ActiveChart.Axes(xlCategory).TickLabels.NumberFormatLocal = "yyyy/m/d"

    '* ボタン色再設定
    Call setButtonColor("M_", 0)
'    Call setButtonColor("P_", 0)
End Sub

'********************************************
'* 初期化全て非表示
'********************************************
Public Sub setAllGraphNotDidsplay()

    Dim statusWorksheet     As Worksheet
    Dim currShape           As Shape
    Dim II                  As Integer

    Call SetFilenameToVariable

    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    With statusWorksheet
    
        For II = 1 To 15
            Set currShape = .Shapes("M_" & II)
            currShape.Visible = msoFalse
            Set currShape = .Shapes("TM_" & II)
            currShape.Visible = msoFalse
'            Set currShape = .Shapes("P_" & II)
'            currShape.Visible = msoFalse
            Set currShape = .Shapes("TP_" & II)
            currShape.Visible = msoFalse
        Next
    End With
    
    Set currShape = statusWorksheet.Shapes("ALL")
    currShape.Visible = msoFalse
    Set currShape = statusWorksheet.Shapes("MAXTor")
    currShape.Visible = msoFalse
    Set currShape = statusWorksheet.Shapes("AVGTor")
    currShape.Visible = msoFalse
    Set currShape = statusWorksheet.Shapes("display_w")
    currShape.Visible = msoFalse
    Set currShape = statusWorksheet.Shapes("display_m")
    currShape.Visible = msoFalse
    Set currShape = statusWorksheet.Shapes("display_y")
    currShape.Visible = msoFalse
    Set currShape = statusWorksheet.Shapes("Rectangle 1")
    currShape.Visible = msoFalse
            
    statusWorksheet.Shapes("グラフ 1").Height = 0
    statusWorksheet.Shapes("グラフ 1").Width = 0
    
    statusWorksheet.Shapes("グラフ 2").Height = 0
    statusWorksheet.Shapes("グラフ 2").Width = 0

End Sub

'********************************************
'* 初期化全て表示
'********************************************
Public Sub setAllGraphDidsplay()

    Dim statusWorksheet     As Worksheet
    Dim currShape           As Shape
    Dim II                  As Integer

    Call SetFilenameToVariable

    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    With statusWorksheet
    
        For II = 1 To 15
            Set currShape = .Shapes("M_" & II)
            currShape.Visible = msoCTrue
            Set currShape = .Shapes("TM_" & II)
            currShape.Visible = msoCTrue
'            Set currShape = .Shapes("P_" & II)
'            currShape.Visible = msoCTrue
            Set currShape = .Shapes("TP_" & II)
            currShape.Visible = msoCTrue
        Next
    End With
    
    Set currShape = statusWorksheet.Shapes("ALL")
    currShape.Visible = msoCTrue
    Set currShape = statusWorksheet.Shapes("MAXTor")
    currShape.Visible = msoCTrue
    Set currShape = statusWorksheet.Shapes("AVGTor")
    currShape.Visible = msoCTrue
    Set currShape = statusWorksheet.Shapes("display_w")
    currShape.Visible = msoCTrue
    Set currShape = statusWorksheet.Shapes("display_m")
    currShape.Visible = msoCTrue
    Set currShape = statusWorksheet.Shapes("display_y")
    currShape.Visible = msoCTrue
    Set currShape = statusWorksheet.Shapes("Rectangle 1")
    currShape.Visible = msoCTrue
            
    statusWorksheet.Shapes("グラフ 1").Height = 241
    statusWorksheet.Shapes("グラフ 1").Width = 322
    
    statusWorksheet.ChartObjects("グラフ 1").Activate
    ActiveChart.ChartTitle.text = "最大トルク履歴グラフ"
    
    statusWorksheet.Shapes("グラフ 2").Height = 241
    statusWorksheet.Shapes("グラフ 2").Width = 322

    statusWorksheet.ChartObjects("グラフ 2").Activate
    ActiveChart.ChartTitle.text = "平均トルク履歴グラフ"
End Sub

Public Function GetModelName(wrkStr As String, CtmName As String)
    Dim ctmDataWorksheet          As Worksheet
    
    Dim allData                 As Variant
    Dim iEndCol                 As Long
    Dim sDataSheetName          As String
    Dim sEndColValue            As String
    
    Dim II                      As Long
    Dim JJ                      As Long
    
    If CtmName = "" Then Exit Function

    Set ctmDataWorksheet = Worksheets(CtmName)
    
    With ctmDataWorksheet
        iEndCol = .UsedRange.Columns.Count
    End With
    
    sEndColValue = GetColumnChar(iEndCol)
    allData = ctmDataWorksheet.Range("A1:" & sEndColValue & DataSheetStartRow)
    If allData(DataSheetStartRow, 1) = "" Then Exit Function

    For JJ = 1 To iEndCol
        If InStr(allData(1, JJ), wrkStr) > 0 Then
            GetModelName = allData(DataSheetStartRow, JJ)
            Exit For
        End If
    Next JJ
End Function
