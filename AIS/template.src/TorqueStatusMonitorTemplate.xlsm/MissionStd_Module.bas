Attribute VB_Name = "MissionStd_Module"
'**********************
'* 収集開始日時の取得
'* →返り値はUNIX TIME
'**********************
Public Function GetCollectStartDTstd(currWorksheet As Worksheet) As Double
    Dim srcWorksheet        As Worksheet
    Dim StartTime           As Double
    Dim startD              As Date
    Dim startT              As Date
    Dim ctmList()           As CTMINFO
    Dim II                  As Integer
    Dim recieveTimeArray()  As Double
    Dim iFirstFlag          As Boolean
    Dim recieveTime         As Variant
    Dim startRange          As Range
    Dim srchRange           As Range
    Dim rtIndex             As Integer
    Dim missionList()       As MISSIONINFO
    
    '**********************************
    '* 全CTMシートの最新時刻を取得する
    '**********************************
    '* CTMリストを取得する
'    Call GetCTMList(MainSheetName, ctmList)
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    '* CTM情報一覧をシートから取得
    iFirstFlag = True
    For Each srcWorksheet In Worksheets
    
        '* CTM情報一覧に存在するシートのみを処理対象とする
        For II = 0 To UBound(missionList(0).CTM)
            If (missionList(0).CTM(II).Name = srcWorksheet.Name) Then Exit For
        Next
        If II <= UBound(missionList(0).CTM) Then

            '* 最上位のRECIEVE TIMEを取得する
            Set startRange = srcWorksheet.Range("A2")
            For II = 0 To 100
                recieveTime = startRange.Offset(II, 0).Value
                If recieveTime <> "" Then
                    Exit For
                End If
            Next
            '* 100行検索しても見つからなければ処理中断＝データ無
            If II > 100 Then
                Exit For
            End If
            
            '* 取得した値が日付形式ならば
            If IsDate(recieveTime) Then
            
                If iFirstFlag Then
                    rtIndex = 0
                    ReDim recieveTimeArray(rtIndex)
                    iFirstFlag = False
                Else
                    rtIndex = UBound(recieveTimeArray) + 1
                    ReDim Preserve recieveTimeArray(rtIndex)
                End If
                
                recieveTimeArray(rtIndex) = GetUnixTime(CDate(recieveTime))
                
            Else
                Exit For
            End If

        End If
    
    Next
        
    If iFirstFlag Then
        recieveTime = 0#
    Else
        '* 取得した時刻群の最小の時刻を取得
        recieveTime = Application.WorksheetFunction.Min(recieveTimeArray)
    End If

    '****************************************
    '* paramシートから収集開始日時を取得する
    '****************************************
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="取得開始", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "" Then
                startD = DateValue(srchRange.Offset(0, 1).Value)
                startT = TimeValue(srchRange.Offset(0, 1).Value)
            Else
                startD = DateValue("2016/1/1 00:00:00")
                startT = TimeValue("2016/1/1 00:00:00")
            End If
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            GetCollectStartDT = -1
            Exit Function
        End If
        StartTime = GetUnixTime(startD + startT)

    End With
    
    GetCollectStartDT = Application.WorksheetFunction.Max(recieveTime, StartTime)

End Function

'****************************************************************
'* 対象ミッションの情報を取得する
'****************************************************************
'FOR COM高速化 lm 2018/06/15 Modify  start
'Public Function GetMissionInfo(missionList() As MISSIONINFO, ctmValue() As CTMINFO) As Integer
Public Function GetMissionInfo(missionList() As MISSIONINFO, ctmValue() As CTMINFO, sType As String) As Integer
'FOR COM高速化 lm 2018/06/15 Modify  end
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim ipAddr      As String
    Dim portNo      As String
    Dim missionID   As String
    Dim ctmID       As String
    Dim StartTime   As Double
    Dim endTime     As Double
    Dim SendUrlStr  As String
    Dim SendString  As Variant
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim iResult     As Integer
    Dim bPmary      As Variant
    Dim aaa         As Variant
    Dim II          As Integer
    Dim currCTM     As CTMINFO
    Dim mIndex      As Integer
    
    Dim updTime     As Variant  ' 2017.02.22 Add
    Dim onlineId    As String   ' 2017.02.22 Add
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
     'FOR COM高速化 lm 2018/06/15 Add  start
    Dim ctmData As FoaCoreCom.CtmDataRetriever
    Dim csvWrite As FoaCoreCom.CsvWriteToExcelHandler
    Dim zipFile As FoaCoreCom.ZipFileHandler
    
    Dim zipUrl As String
    Dim unZipUrl As String
    Dim msg As String
    Dim test As Boolean
    'FOR COM高速化 lm 2018/06/15 Add  end
    
    
    '* メインシートから各種情報を取得
'    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '******************************************************
        '* 収集開始日時を取得
        '* →現在取得済のCTM情報を元に過去最短の時刻を取得する
        '******************************************************
        Dim templateSheet As Worksheet
        Dim startRange1 As Range
        Dim srchRange1 As Range
        Dim wrkStr As String
        
        Set templateSheet = Worksheets(TemplateSheetName)
        
        Set startRange1 = templateSheet.Cells(1, 1).EntireColumn
        Set srchRange1 = startRange1.Find(What:="PeriodFlag", LookAt:=xlPart)
        If Not srchRange1 Is Nothing Then
            wrkStr = srchRange1.Offset(0, 1).Value
        End If
        
'        If wrkStr = "True" Then
'            Set srchRange1 = startRange1.Find(What:="収集開始日時", LookAt:=xlPart)
'            If Not srchRange1 Is Nothing Then
'                StartTime = GetUnixTime(srchRange1.Offset(0, 1).Value + srchRange1.Offset(0, 2).Value)
'            End If
'        Else
'            StartTime = GetCollectStartDT(Workbooks(thisBookName).Worksheets(ParamSheetName))
'        End If
        '*********************
        '* 収集終了日時を取得
        '*********************
'        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            endD = srchRange.Offset(0, 1).Value
'            endT = srchRange.Offset(0, 2).Value
'        Else
'            endD = CDate("2020/12/31")
'            endT = CDate("23:59:59")
'        End If
'        If Not IsDate(endD + endT) Then
'            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
'            Exit Function
'        End If
'        endTime = GetUnixTime(endD + endT)
'        endTime = GetUnixTime(DateAdd("s", 60, Now))
        
         If wrkStr = "True" Then
            Set srchRange1 = startRange1.Find(What:="収集終了日時", LookAt:=xlPart)
            If Not srchRange1 Is Nothing Then
                Dim setting As Date
                setting = srchRange1.Offset(0, 1).Value + srchRange1.Offset(0, 2).Value
                
                endTime = GetUnixTime(DateAdd("d", 1, setting)) - 1
                
                Dim days As Integer
                days = CInt(ThisWorkbook.Worksheets(TorqueSetDataSheetName).Range("D1").Value) - 1
                StartTime = GetUnixTime(DateAdd("d", 1 - days, setting))
            Else
                StartTime = GetCollectStartDT(Workbooks(thisBookName).Worksheets(ParamSheetName))
                endTime = GetUnixTime(DateAdd("s", 60, Now))
            End If
        Else
            StartTime = GetCollectStartDT(Workbooks(thisBookName).Worksheets(ParamSheetName))
            endTime = GetUnixTime(DateAdd("s", 60, Now))
        End If
       
        '*********************
        '* ミッションIDを取得
        '*********************
'        missionID = ""
'        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            missionID = srchRange.Offset(0, 1).Value
'        Else
'            MsgBox "ミッションID項目が見つからないため処理を中断します。"
'            GetMissionInfo = -1
'            Exit Function
'        End If
        
'    End With
    
    '*******************************
    '* IPアドレスおよびPortNoを取得
    '*******************************
    With Workbooks(thisBookName).Worksheets(ParamSheetName)

        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            ipAddr = srchRange.Offset(0, 1).Value
            portNo = srchRange.Offset(1, 1).Value
        Else
            ipAddr = "localhost"
            portNo = "60000"
        End If

        '2017.02.21 Add*****
        '* 更新周期を取得
        '2017.02.21 Add*****
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
    
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
    
    End With
    
    '*******************************
    '* CTM情報一覧をシートから取得
    '*******************************
'    Call GetCTMList(MainSheetName, ctmList)
    
    ' 2017.02.21 Add *********************
    '* オンライングラフIDの取得
    ' 2017.02.21 Add *********************
    onlineId = GetOnlineId()
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    '*******************************
    '* プログラムミッションに問合せ
    '*******************************
    ReDim ctmValue(0)
    ctmValue(0).ID = ""
    For mIndex = 0 To UBound(missionList)
        '* ミッションIDがあれば
        missionID = missionList(mIndex).ID
        If missionID <> "" Then
            getEndTime = endTime
            getStartTime = StartTime
            
            ' 2017.02.22 Change **************************
    '        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pm?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & _
    '                        "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true"
            'FOR COM高速化 lm 2018/06/13 Modify  start
            'SendUrlStr = "http://" & ipAddr & ":" & portNo & "/cms/rest/mib/mission/pm?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & _
                            "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true" _
                            & "&onlineId=" & "&period=" & Format(updTime * 1000) & "&collectEnd=" & Format(getEndTime)
            ' 2017.02.22 Change **************************
            SendUrlStr = "http://" & ipAddr & ":" & portNo & "/cms/rest/mib/mission/pmzip?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & _
                            "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true" _
                             & "&onlineId=" & "&period=" & Format(updTime * 1000) & "&collectEnd=" & Format(getEndTime)
            ' 2017.02.22 Change **************************
        
            
'            If useProxy = False Then
'                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'            Else
'                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'                httpObj.setProxy 2, proxyUri
'            End If
'
'            httpObj.Open "GET", SendUrlStr, False
'            httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
'            httpObj.send
'
'            ' ダウンロード待ち
'            Do While httpObj.readyState <> 4
'                DoEvents
'            Loop
'
'            '* 取得情報のデコード
'            Set sc = CreateObject("ScriptControl")
'            With sc
'                .Language = "JScript"
'
'                '指定したインデックス、名称のデータを取得する
'                .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
'            End With
'            GetString = httpObj.responseText
'            Set objJSON = sc.CodeObject.jsonParse(GetString)
'            Set sc = Nothing
'
''***********************************************************************
''* VBA-JSONを使用する場合
''            GetString = httpObj.responseText
''            Set objJSON = JsonConverter.ParseJson(GetString)
''***********************************************************************
'
'            Set httpObj = Nothing
'
'            '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
'            If InStr(GetString, "MISSION_NOT_FOUND") > 0 Then
'                MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
'
'                '周期起動停止
'                'Call ChangeStartStopBT
'                GetMissionInfo = -1
'                Exit Function
'            End If
'            '**********
'
'            '* 取得情報をCTM情報に展開
'    '        iResult = GetCTMInfo(objJSON, ctmList, ctmValue)
'            iResult = GetCTMInfo(objJSON, missionList(mIndex).CTM, ctmValue)
''            iResult = GetCTMInfoNew(objJSON, missionList(mIndex).CTM, ctmValue)

        ' thisBookNameへカレントExcel対象をセット
        Call SetFilenameToVariable

        Set ctmData = CreateObject("FoaCoreCom.ais.retriever.CtmDataRetriever")
        Set csvWrite = CreateObject("FoaCoreCom.ais.retriever.CsvWriteToExcelHandler")
        Set zipFile = CreateObject("FoaCoreCom.ais.retriever.ZipFileHandler")
        msg = ""
        
        '情報を格納するZIPファイルパスを取得する
        zipUrl = ctmData.GetProgramMissionZipFileAsync(SendUrlStr, proxyUri, msg)
        
        '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
        If InStr(msg, "MISSION_NOT_FOUND") > 0 Then
            MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
            
            '周期起動停止
            'Call ChangeStartStopBT
            'pmzip失敗する場合
            GetMissionInfo = -1
            Exit Function
        End If
        '**********
        
        ' Zipファイルを解凍する
        unZipUrl = zipFile.unZipFile(zipUrl)
              
        ' excelへ書き込む
        Call csvWrite.ReadAllCtmInfoToExcel(Workbooks(thisBookName), unZipUrl, MaxCntImport, sType, GetTimezoneId(), MissionSheetName)
        
        '一時フォルダを削除
        zipFile.deleteFolder (unZipUrl)
        
        'pmzip成功する場合
        iResult = 0
        'FOR COM高速化 lm 2018/06/13 Modify  end
        
        '* ミッションIDがなければ
        Else
        
            ReDim ctmValue(0)
            ctmValue(0).ID = ""
            For II = 0 To UBound(missionList(mIndex).CTM)
        
                currCTM = missionList(mIndex).CTM(II)
                ctmID = currCTM.ID
            
                getEndTime = endTime
                getStartTime = StartTime
                
                SendUrlStr = "http://" & ipAddr & ":" & portNo & "/cms/rest/mib/ctm/find?testing=0"
                
                If useProxy = False Then
                    Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                Else
                    Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                    httpObj.setProxy 2, proxyUri
                End If
    
                httpObj.Open "POST", SendUrlStr, False
                httpObj.setRequestHeader "Content-Type", "text/plain"
                httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***Content-Type: text/plain
                
                SendString = "{""ctmId"":""" & ctmID & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & "}"
                
                bPmary = StrConv(SendString, vbFromUnicode)
            
                Call httpObj.send(SendString)
                
                ' ダウンロード待ち
                Do While httpObj.readyState <> 4
                    DoEvents
                Loop
                
                '* 取得情報のデコード
                Set sc = CreateObject("ScriptControl")
                With sc
                    .Language = "JScript"
            
                    '指定したインデックス、名称のデータを取得する
                    .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
                End With
'                GetString = convTextEncoding(httpObj.responseBody, "UTF-8")
                GetString = httpObj.responseBody
                Set objJSON = sc.CodeObject.jsonParse(GetString)
            
                Set sc = Nothing
                
                Set httpObj = Nothing
            
                '* 取得情報をCTM情報に展開
                iResult = GetCTMInfoForCTM(objJSON, currCTM, ctmValue)
            
            Next
        
        End If
    
    Next
    
    ' MsgBox http.responseText
    GetMissionInfo = iResult
    
End Function







