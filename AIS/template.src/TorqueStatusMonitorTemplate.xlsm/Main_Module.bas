Attribute VB_Name = "Main_Module"
Option Explicit

'--2018/03/06 Add PJNo.39 アドイン連携処理追加----Start
'**************************************
'*アドインメニュー制御(期間再設定)
'**************************************
Public Sub RefreshPeriod()
    Dim addIn As COMAddIn
    Dim automationObject As Object
    Set addIn = Application.COMAddIns("AisTemplateAddin")
    Set automationObject = addIn.Object
    If automationObject.RefreshPeriod Then
        Call RefreshPeriodData
    End If
End Sub
'--2018/03/06 Add PJNo.39 アドイン連携処理追加----End

Public Sub RefreshPeriodData()
    Dim torqueWorksheet     As Worksheet
    Dim workTimeStart       As Integer
    
    Call SetPeriodFlag("True")
    
    Set torqueWorksheet = ThisWorkbook.Worksheets(TorqueSetDataSheetName)
    workTimeStart = Int(torqueWorksheet.Range(ADDR_WorkTimeStartPos).Value)
    
    ' グラフを表示
    Select Case workTimeStart
    Case 8:
        Call display_w_click
    Case 32:
        Call display_m_click
    Case 366:
        Call display_y_click
    End Select
    
End Sub

Public Sub SetPeriodFlag(str As String)
    Dim templateSheet As Worksheet
    Dim startRange As Range
    Dim srchRange As Range
    
    Set templateSheet = Worksheets(TemplateSheetName)
    
    Set startRange = templateSheet.Cells(1, 1).EntireColumn
    Set srchRange = startRange.Find(What:="PeriodFlag", LookAt:=xlPart)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = str
    End If
End Sub

'--2017/04/28 Add No.651 アドイン連携処理追加----Start
'**************************************
'*アドインメニュー制御(活性/非活性切替)
'**************************************
Public Sub InvalidateAddin()
    Dim addIn As COMAddIn
    Dim automationObject As Object
    
    Set addIn = Application.COMAddIns("AisTemplateAddin")
    Set automationObject = addIn.Object
    
    automationObject.Invalidate

End Sub
'--2017/04/28 Add No.651 アドイン連携処理追加----End

'******************************
'* スタートストップボタン処理
'******************************
Public Sub ChangeStartStopBT()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range

    Call SetFilenameToVariable
    
    '* ボタン表示状態を確認
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
    
        '* 更新中なら停止
        If srchRange.Offset(0, 1).Value = "ON" Then
            srchRange.Offset(0, 1).Value = "OFF"

            '* 予約したスケジュールをキャンセルする
            Call CancelSchedule
        
            '* メニュー制御
            Call MenuCtrlEnableOrDisable("テスト", True)
            Call MenuCtrlEnableOrDisable("自動更新", True)
            Call MenuCtrlEnableOrDisable("更新停止", False)
    
        '* 停止中なら開始
        Else
            srchRange.Offset(0, 1).Value = "ON"
            
            '* メニュー制御
            Call MenuCtrlEnableOrDisable("テスト", False)
            Call MenuCtrlEnableOrDisable("自動更新", False)
            Call MenuCtrlEnableOrDisable("更新停止", True)
            
            '* 自動実行開始
            Call TimerStart
        
        End If
        
        Call UpdateFormStatus

        'No.651 Add.
        '* アドインメニュー制御
        Call InvalidateAddin

    End If
    
    '* 編集モードボタン表示状態を確認
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="SwitchLink", LookAt:=xlWhole)

End Sub

'*******************
'* 自動更新スタート
'*******************
Public Sub AutoUpdateStart()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    
    Call SetFilenameToVariable
    
    '* メニュー制御
    Call MenuCtrlEnableOrDisable("テスト", False)
    Call MenuCtrlEnableOrDisable("自動更新", False)
    Call MenuCtrlEnableOrDisable("更新停止", True)

    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)

    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "ON"
    
    Call TimerStart

End Sub

'****************************
'* 自動更新を停止する
'****************************
Public Sub AutoUpdateStop()
    Dim currWorksheet   As Worksheet
    Dim mainWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim prevDate        As Date
    Dim prevTime        As Date
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = "OFF"
    
        '* 予約したスケジュールをキャンセルする
        Call CancelSchedule
    
        '* メニュー制御
        Call MenuCtrlEnableOrDisable("テスト", True)
        Call MenuCtrlEnableOrDisable("自動更新", True)
        Call MenuCtrlEnableOrDisable("更新停止", False)
    
        Call UpdateFormStatus
        
    End If
    
End Sub

'*******************************
'* 背景色設定画面表示
'*******************************
Public Sub SetDisplayBackgroundForm()
    Dim paramWorksheet      As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim chgElement          As Variant
    Dim chgElementID        As Variant  '2017/06/26 No.278 Add.
    Dim colorValue(4)       As Variant
    Dim II                  As Integer
    
    '2017/09/15 No.533 Add.
    Call SetFilenameToVariable
    
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With paramWorksheet
    
        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:="表示色対象", LookAt:=xlWhole)
    
        If Not currRange Is Nothing Then
        
            '* 表示色対象エレメント名を取得
            chgElement = currRange.Offset(0, 1).Value
                    
            '* 表示色対象エレメントIDを取得  '2017/06/26 No.278 Add.
            chgElementID = currRange.Offset(0, 2).Value

            '* 表示色条件を取得
            For II = 0 To 3
                colorValue(II) = currRange.Offset(II + 1, 1).Value
            Next
        
        End If

    End With

    '* 背景色定義画面を開く
    Load ActionPaneBackForm
    With ActionPaneBackForm
    
        .TB_White.text = colorValue(0)
        .TB_Red.text = colorValue(1)
        .TB_Yellow.text = colorValue(2)
        .TB_Green.text = colorValue(3)
        
        .TB_ItemOperation = chgElement
        .TB_ItemElementID = chgElementID  '2017/06/26 No.278 Add.

        .Show vbModeless
    
    End With
End Sub

'*******************************
'* テキストボックス編集画面表示
'*******************************
Public Sub DisplayEditForm()

    '2017/09/15 No.533 Add.
    Call SetFilenameToVariable

    Load ActionPaneForm
    With ActionPaneForm
    
        '* 「作成」ボタンを非表示
        .BT_CreateTextBox.Visible = False
    
        .Show vbModeless
    
    End With
End Sub

'***********************************
'* テキストボックス新規作成画面表示
'***********************************
Public Sub DisplayNewForm()

    '2017/09/15 No.533 Add.
    Call SetFilenameToVariable
    
    Load ActionPaneForm
    With ActionPaneForm
    
        '* 「条件設定」ボタンを非表示
        .BT_Set.Visible = False
        
        '* 「削除」ボタンを非表示
        .BT_Delete.Visible = False
        
        '* 「更新」ボタンを非表示
        .BT_Update.Visible = False
        
        '* 「エレメント追加」ボタンを非表示
        .BT_add.Visible = False
        
        '* 「エレメント削除」ボタンを非表示
        .BT_rowdelete.Visible = False
        
        '* 「条件更新」ボタンを非表示
        .BT_ConditionUpdate.Visible = False
        
        '* 「条件」テキストボックスを非表示
        .TB_CalcCondition.Visible = False
        
        '* 「条件」ラベルを非表示
        .Label23.Visible = False
        
        '* 「エレメント」リストビューを非表示
        .LV_ElementView.Visible = False
        
        '* リストビューのへーだー「エレメント」ラベルを非表示
        .Label25.Visible = False
        
        '* リストビューのへーだー「条件」ラベルを非表示
        .Label24.Visible = False
        
        .Show vbModeless
    
    End With
End Sub


'*******************
'* 状態フォーム更新
'*******************
Public Sub UpdateFormStatus()
    Dim currWorksheet   As Worksheet
    Dim paramWorksheet  As Worksheet
    Dim currRange       As Range
    Dim srchRange       As Range
    Dim flagON          As Boolean
    Dim f               As Variant
    Dim isNotForm       As Boolean
    Dim FinalTime       As Date
    Dim endD            As Date
    Dim endT            As Date
    Dim currShape       As Shape
    Dim RCAddress       As String
    Dim btSize          As RECT
    Dim prevTime        As Variant
    
    Call SetFilenameToVariable

    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    With paramWorksheet
    
        '* 自動更新状態を「pram」シートから取得
        Set srchRange = .Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value = "ON" Then
                flagON = True
            Else
                flagON = False
            End If
        End If
    
        '* 自動更新状態を「pram」シートから取得
        Set srchRange = .Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "" Then
                prevTime = Format(srchRange.Offset(0, 1).Value, "hh:mm:ss")
            Else
                prevTime = "--:--"
            End If
        End If
    
    End With
    
'    '* 全てのワークシートに対して表示テキストボックスを確認し、情報を表示する
''    For Each currWorksheet In Workbooks(thisBookName).Worksheets
'        Set currWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
'        isNotForm = True
'        '* ワークシート上の全ての図形に対して
'        For Each currShape In currWorksheet.Shapes
'            '* 「ONLINE_」文字列を含む図形があれば図形内テキストを更新する
'            If currShape.Name = PrefixStrTX & "ONLINE" Then
'                isNotForm = False
'                If flagON Then
'                    currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & "更新中"
'                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 255)
'                Else
'                    currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & "停止中"
'                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(255, 0, 0)
'                End If
'            End If
'        Next currShape
'        '* 表示テキストボックスがなければ追加する
'        If isNotForm Then
'            '* 設定されている表示範囲のアドレスを取得し、範囲を設定する
'            RCAddress = GetDisplayAddress(currWorksheet)
'            If RCAddress <> "" Then
'                Set currRange = Workbooks(thisBookName).Worksheets(StatusSheetName).Range(RCAddress)
'                '* 更新状況テキストボックスを追加
'                btSize.Top = currRange.Top + 1
'                btSize.Left = currRange.Left + 25
'                btSize.Right = 72
'                btSize.Bottom = 24
'                Set currShape = PutTextBoxAtSheet(currWorksheet, btSize, RGB(0, 0, 0), RGB(255, 255, 255), PrefixStrTX, "ONLINE", "ChangeStartStopBT")
'                If flagON Then
'                    currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & "更新中"
'                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 255)
'                Else
'                    currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & "停止中"
'                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(255, 0, 0)
'                End If
'            End If
'        End If
'    Next currWorksheet
    
End Sub

'**************************
'* 自動更新タイマー起動開始
'**************************
Public Sub TimerStart()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range

    Call SetFilenameToVariable
    
    With Workbooks(thisBookName).Worksheets(ParamSheetName)
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
            
        '*********************
        '* 収集終了日時を取得
        '*********************
'        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            endD = srchRange.Offset(0, 1).Value
'            endT = srchRange.Offset(0, 2).Value
'        Else
'            endD = CDate("2020/12/31")
'            endT = CDate("23:59:59")
'        End If
'        If Not IsDate(endD + endT) Then
'            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
'            Exit Sub
'        End If
'        endTime = GetUnixTime(endD + endT)
            
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'TimerLogic'"
        
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd hh:mm:ss")
        End If
        
    End With
    
    '* ピボットテーブル更新 --2017/09/22 No.537 コメントアウトを解除
    Call RefreshPivotTableData
    
    '* 更新中／停止中ステータス更新
    Call UpdateFormStatus
    
End Sub

'***********************
'* 自動更新タイマー処理
'***********************
Public Sub TimerLogic()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range
    Dim FinalTime       As Date
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    With currWorksheet
    
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "ON" Then
                Application.CutCopyMode = False
                Exit Sub
            End If
        End If
    
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="xxx", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        FinalTime = endD + endT
        If Format(Now, "yyyy/MM/dd hh:mm:ss") >= FinalTime Then         '終了時刻になったら終わる
            MsgBox "終了時刻になりました。" & " " & Format(Now, "hh:mm:ss")
            Call AutoUpdateStop
            Application.CutCopyMode = False
            Exit Sub
        End If
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
            
        '* 次回の処理をスケジュール
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'TimerLogic'"
        
        '前回／次回更新日時セット
        Set srchRange = .Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd hh:mm:ss")
        End If
        Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd hh:mm:ss")
        End If
                
    End With
    
    '* ミッションからの情報取得
    'FOR COM高速化 lm 2018/06/15 Modify  start
    'Call GetMissionInfoAll
    Call GetMissionInfoAll(SEARCH_TYPE_AUTO)
    'FOR COM高速化 lm 2018/06/15 Modify  end
        
    '* ピボットテーブル更新  --2017/09/22 No.537 Mod.コメントアウトを解除
    Call RefreshPivotTableData
    
    '* ステータスシート上の表示更新
    Call UpdateAllBackAndTB

    '* 更新状態表示更新
    Call UpdateFormStatus
    
    If Application.Visible = True Then
        'EXCEL前面表示用
        Call ExcelUpperDisp
        VBA.AppActivate Excel.Application.Caption
    
        Call ExcelDispSetWin32
        Call ExcelDispFreeWin32
    End If
    
End Sub

'****************************
'* ピボットテーブルを更新する
'****************************
Public Sub RefreshPivotTableData()
    Dim currWorksheet   As Worksheet
    Dim srcWorksheet    As Worksheet
    Dim workPVT         As PivotTable
    Dim pvtRange        As Range
    Dim startRange      As Range
    Dim dataRange       As Range
    Dim lastRange       As Range
    Dim dataSrc         As String
    Dim dataNewSrc      As String
    Dim srcSheetName    As String
    Dim rangeValue      As Long '---2016/12/19 Add No.385

    For Each currWorksheet In Workbooks(thisBookName).Worksheets
        For Each workPVT In currWorksheet.PivotTables
            With workPVT
                Set pvtRange = .TableRange1
                
                '* 対象ピボットテーブルのデータソースを取得する
                dataSrc = .SourceData
                '* データソースのシート名を取得する
                srcSheetName = Left(dataSrc, InStr(dataSrc, "!") - 1)
                Set srcWorksheet = Workbooks(thisBookName).Worksheets(srcSheetName)
                With srcWorksheet
                    Set startRange = .Range("A1")
                    Set dataRange = .Range(startRange, startRange.End(xlDown).End(xlToRight))
                    rangeValue = Len(.Range("A2").Value)  '---2016/12/19 Add No.385
                End With
                '---2016/12/19 Add No.385 データが存在するときのみ更新実行 IF文追加 -------------Start
                '* データが存在すればピボットテーブルを更新する
                If rangeValue > 0 Then
                  dataNewSrc = srcSheetName & "!" & dataRange.Address(ReferenceStyle:=xlR1C1, External:=False)
                  .SourceData = dataNewSrc
                  .RefreshTable
                End If
                '---2016/12/19 Add No.385 データが存在するときのみ更新実行 IF文追加 -------------End
            End With
        Next workPVT
    Next currWorksheet

End Sub

'*****************
'* ｢テスト｣の処理
'*****************
Public Sub OperationTest()
    Dim currWorksheet   As Worksheet
    Dim workPVT         As PivotTable
    Dim pvtRange        As Range
    Dim srchRange       As Range

    Call SetFilenameToVariable
    
    '* ミッションからの情報取得
    'FOR COM高速化 lm 2018/06/15 Modify  start
    'Call GetMissionInfoAll
    Call GetMissionInfoAll(SEARCH_TYPE_TEST)
    'FOR COM高速化 lm 2018/06/15 Modify  end
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With currWorksheet
    
        '前回／次回更新日時セット
        Set srchRange = .Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd hh:mm:ss")
        End If
        Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd hh:mm:ss")
        End If
    
    End With
    
    '* ピボットテーブル更新  --2017/09/22 No.537 Add.
    Call RefreshPivotTableData

    '* ステータスシート上の表示更新
    Call UpdateAllBackAndTB
    
    '* 更新状態表示更新
    Call UpdateFormStatus
End Sub

'*****************************
'* スケジュール予約キャンセル
'*****************************
Public Sub CancelSchedule()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim prevDate        As Date
    Dim prevTime        As Date

    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)

    With currWorksheet
        '* 予約したスケジュールをキャンセルする
        Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            prevDate = srchRange.Offset(0, 1).Value
            On Error Resume Next
            Application.OnTime prevDate, "'TimerLogic'", , False
        End If
    End With

End Sub

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSub()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
    Dim objFSO          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    Dim IsFirstSave     As String
    
    Debug.Print "In SaveFileSub"
    
    quitFlag = False
    
    Call SetFilenameToVariable
    
    Call CancelSchedule

    Set objFSO = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With Workbooks(thisBookName).Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then IsFirstSave = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
    currThisFile = Workbooks(thisBookName).FullName
'        Application.DisplayAlerts = False
'        ActiveWorkbook.SaveAs Filename:=currThisFile
'        Application.DisplayAlerts = True

'    wrkInt = InStr(Workbooks(thisBookName).Name, templateName)
'
'    If wrkInt <= 0 Or wrkInt > 4 Then
    If IsFirstSave = "" Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & Workbooks(thisBookName).Name
        Else
            fname = saveFilePath & "\" & templateName & "_" & Workbooks(thisBookName).Name
        End If
        '* テンプレート名／登録フォルダ名を取得する
        With Workbooks(thisBookName).Worksheets(ParamSheetName).Columns(1)
            Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "TRUE"
        End With
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & Workbooks(thisBookName).Name
        Else
            fname = saveFilePath & "\" & Workbooks(thisBookName).Name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
    If Dir(fname) <> "" Then
      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
    End If

    '各種バー表示
    Call DispBar
    
    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    Application.DisplayAlerts = False
    
    '* ISSUE_NO.624 Add ↓↓↓ *******************************
    On Error GoTo ErrorHandler
    
    Workbooks(thisBookName).SaveAs fileName:=fname
    
ErrorHandler:
    '-- 例外処理
    If Err.Description <> "" Then
        MsgBox Err.Description, vbCritical & vbOKOnly, "警告"
    End If
    '* ISSUE_NO.624 Add ↑↑↑ *******************************
    
    
    If Application.Workbooks.Count > 1 Then
        ThisWorkbook.Close
    Else
        Application.Quit
        ThisWorkbook.Close
    End If
    If Err.Description = "" Then
        Application.DisplayAlerts = True
    End If
        
End Sub

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSubFromX()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
    Dim objFSO          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    
    Debug.Print "In SaveFileSubFromX"
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "ON" Then
            Call CancelSchedule
        End If
    End If

    Set objFSO = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With Workbooks(thisBookName).Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
    currThisFile = Workbooks(thisBookName).FullName
'        Application.DisplayAlerts = False
'        ActiveWorkbook.SaveAs Filename:=currThisFile
'        Application.DisplayAlerts = True

    wrkInt = InStr(Workbooks(thisBookName).Name, templateName)

    If wrkInt <= 0 Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & Workbooks(thisBookName).Name
        Else
            fname = saveFilePath & "\" & templateName & "_" & Workbooks(thisBookName).Name
        End If
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & Workbooks(thisBookName).Name
        Else
            fname = saveFilePath & "\" & Workbooks(thisBookName).Name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
'    If Dir(fname) <> "" Then
'      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
'      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
'    End If
    If Workbooks(thisBookName).ReadOnly = True Then
         
        MsgBox Workbooks(thisBookName).Name & " は読み取り専用のため、上書き保存できません。" _
            & Chr(10) & "変更内容を維持するには、新しい名前でブックを保存するか、別の場所に保存する必要があります。", vbExclamation
        Application.Dialogs(xlDialogSaveAs).Show currThisFile
    Else
        '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
        Application.DisplayAlerts = False
        Workbooks(thisBookName).SaveAs fileName:=currThisFile
        If Application.Workbooks.Count > 1 Then
            ThisWorkbook.Close
        Else
            Application.DisplayAlerts = False
        
            ThisWorkbook.Save
        
            Application.Quit
            ThisWorkbook.Close
        End If
        'Application.DisplayAlerts = True
    End If
End Sub


'*******************
'* スイッチオン
'*******************
Public Sub SwitchOn()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="SwitchLink", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = "ON"
    
        '* メニュー制御
        Call MenuCtrlEnableOrDisable("編集モードON", False)
        Call MenuCtrlEnableOrDisable("編集モードOFF", True)
    
    End If

End Sub

'****************************
'* スイッチオフ
'****************************
Public Sub SwitchOff()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="SwitchLink", LookAt:=xlWhole)

    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = "OFF"

        '* メニュー制御
        Call MenuCtrlEnableOrDisable("編集モードON", True)
        Call MenuCtrlEnableOrDisable("編集モードOFF", False)

    End If
    
End Sub

'***********************************
'* 生データ表示画新規作成表示
'***********************************
Public Sub PruduceDataDisplay()

    Call SetFilenameToVariable
    
    Load DataProduceSetForm
    With DataProduceSetForm
            
        .Show vbModeless
    
    End With
End Sub


'***********************************
'* 異常履歴表示画新規作成表示
'***********************************
Public Sub HistoryForm()

    Dim errHisSheetName    As Worksheet
    Dim Value              As String
    
    Call SetFilenameToVariable
    
    Set errHisSheetName = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    Value = errHisSheetName.Range("Z1")

    Load ErrHistorySetForm
'    With ErrHistorySetForm
            
        '.Show vbModeless
    
'    End With
    
'    Me.Hide

    If Value = "" Then
        '* 異常履歴表示画面に入力情報をエクセルに保存する
        Call SaveInfoToExcel
        '* ブックを作成する
        Call WriteValueErrHistory
    Else
        '* ステータスを削除する
        Call DeleteErrHistory("0")
        
        '* 異常履歴表示画面に入力情報をエクセルに保存する
        Call SaveInfoToExcel
        
        '* ブックを作成する
        Call WriteValueErrHistory
    End If
    errHisSheetName.Range("Z1") = "Upate"
'    Me.Show vbModeless

'    Unload Me
    Call display_w_click
End Sub

'*******************
'* 異常履歴表示画面に入力情報をエクセルに保存する
'*******************
Private Sub SaveInfoToExcel()

    Dim errHisSheetName    As Worksheet
    Dim missionSheet   As Worksheet
    Dim startRange         As Range
    Dim msnRange           As Range
    Dim II                 As Integer
    Dim CtmName            As String
    
    Set errHisSheetName = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    Set missionSheet = Workbooks(thisBookName).Worksheets(MissionSheetName)
    
    With errHisSheetName
        '* データ表示開始列
        .Range("B1").Value = 1
        '* データ表示開始行
        .Range("B2").Value = 1

        '* A5セルをスタートセルとする
        Set startRange = .Range("A5")
        
        '* 表示用データを取得する
        CtmName = missionSheet.Range("E1").Value

        '* 表示エレメントを設定する
        Set msnRange = startRange.Offset(1, 0)
        msnRange.Offset(0, 2).Value = CtmName
    End With

End Sub

