Attribute VB_Name = "unitChange_Module"
'工学変換:  温度
'argPtn :C→F, F→C
Public Sub reDrowGraphTemp(argPtn As String, argXy As String)

    Dim currWorksheet         As Worksheet
    Dim currChartObj           As ChartObject
    Dim srchRange               As Range
    Dim pos                          As Integer
    Dim wrkCnt                     As Integer
    Dim vMax, vMin, vStep
    Dim hMax, hMin, hStep
    Dim yokoVal, tateVal

    'グラフ最大・最小値・刻み設定
    Application.ScreenUpdating = False

On Error Resume Next
' ℃→F：9C/5+32
' F→℃：(F-32)*5/9
    Set currWorksheet = Worksheets(PivotSheetName)  'グラフテーブル
    Set srchRange = currWorksheet.Cells.Find(What:="グラフ用データ", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
            ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
            'wrkCnt = srchRange.Offset(1, 0).End(xlDown).Row
            wrkCnt = GetDownToEndRange(srchRange.Offset(1, 0)).Row
            ' Wang FoaStudio Issue AISTEMP-120 20181218 End
            If wrkCnt > 0 Then
                If argPtn = "℃→F" And argXy = "横" Then
                    For pos = 2 To wrkCnt
                        srchRange.Offset(pos - 1, 0).Value = srchRange.Offset(pos - 1, 0).Value * 1.8 + 32
                    Next pos
                ElseIf argPtn = "℃→F" And argXy = "縦" Then
                    For pos = 2 To wrkCnt
                        srchRange.Offset(pos - 1, 1).Value = srchRange.Offset(pos - 1, 1).Value * 1.8 + 32
                    Next pos
                    
                ElseIf argPtn = "F→℃" And argXy = "横" Then
                    For pos = 2 To wrkCnt
                        srchRange.Offset(pos - 1, 0).Value = (srchRange.Offset(pos - 1, 0).Value - 32) * 1.8
                    Next pos
                ElseIf argPtn = "F→℃" And argXy = "縦" Then
                    For pos = 2 To wrkCnt
                        srchRange.Offset(pos - 1, 1).Value = (srchRange.Offset(pos - 1, 1).Value - 32) * 1.8
                    Next pos
                End If
            End If
    End If
    
    Set currWorksheet = Worksheets(PivotSheetName)  'グラフテーブル
    If argXy = "縦" And argPtn = "℃→F" Then
        Set srchRange = currWorksheet.Cells.Find(What:="縦最大値", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 0).Value = srchRange.Offset(1, 0).Value * 1.8 + 32
            srchRange.Offset(1, 1).Value = srchRange.Offset(1, 1).Value * 1.8 + 32
            srchRange.Offset(1, 2).Value = srchRange.Offset(1, 2).Value * 1.8 + 32
        
            vMax = srchRange.Offset(1, 0).Value
            vMin = srchRange.Offset(1, 1).Value
            vStep = srchRange.Offset(1, 2).Value
        End If
    ElseIf argXy = "縦" Then
        Set srchRange = currWorksheet.Cells.Find(What:="縦最大値", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 0).Value = (srchRange.Offset(1, 0).Value - 32) * 1.8
            srchRange.Offset(1, 1).Value = (srchRange.Offset(1, 1).Value - 32) * 1.8
            srchRange.Offset(1, 2).Value = (srchRange.Offset(1, 2).Value - 32) * 1.8
        
            vMax = srchRange.Offset(1, 0).Value
            vMin = srchRange.Offset(1, 1).Value
            vStep = srchRange.Offset(1, 2).Value
        End If
    End If
    
    Set currWorksheet = Worksheets(PivotSheetName)  'グラフテーブル
    '現在値保存
    Set srchRange = currWorksheet.Cells.Find(What:="縦最大値", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        vMax = srchRange.Offset(1, 0).Value
        vMin = srchRange.Offset(1, 1).Value
        vStep = srchRange.Offset(1, 2).Value
    End If
    Set srchRange = currWorksheet.Cells.Find(What:="横最大値", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        hMax = srchRange.Offset(1, 0).Value
        hMin = srchRange.Offset(1, 1).Value
        hStep = srchRange.Offset(1, 2).Value
    End If

    '現在値の変換
    If argXy = "横" And argPtn = "℃→F" Then
        Set srchRange = currWorksheet.Cells.Find(What:="横最大値", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 0).Value = srchRange.Offset(1, 0).Value * 1.8 + 32
            srchRange.Offset(1, 1).Value = srchRange.Offset(1, 1).Value * 1.8 + 32
            srchRange.Offset(1, 2).Value = srchRange.Offset(1, 2).Value * 1.8 + 32
        
            hMax = srchRange.Offset(1, 0).Value
            hMin = srchRange.Offset(1, 1).Value
            hStep = srchRange.Offset(1, 2).Value
        End If
    ElseIf argXy = "横" Then
        Set srchRange = currWorksheet.Cells.Find(What:="横最大値", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 0).Value = (srchRange.Offset(1, 0).Value - 32) * 1.8
            srchRange.Offset(1, 1).Value = (srchRange.Offset(1, 1).Value - 32) * 1.8
            srchRange.Offset(1, 2).Value = (srchRange.Offset(1, 2).Value - 32) * 1.8
        
            hMax = srchRange.Offset(1, 0).Value
            hMin = srchRange.Offset(1, 1).Value
            hStep = srchRange.Offset(1, 2).Value
        End If
    End If
    
    If argXy = "縦" And argPtn = "℃→F" Then
        Set srchRange = currWorksheet.Cells.Find(What:="縦最大値", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 0).Value = srchRange.Offset(1, 0).Value * 1.8 + 32
            srchRange.Offset(1, 1).Value = srchRange.Offset(1, 1).Value * 1.8 + 32
            srchRange.Offset(1, 2).Value = srchRange.Offset(1, 2).Value * 1.8 + 32
        
            hMax = srchRange.Offset(1, 0).Value
            hMin = srchRange.Offset(1, 1).Value
            hStep = srchRange.Offset(1, 2).Value
        End If
    ElseIf argXy = "縦" Then
        Set srchRange = currWorksheet.Cells.Find(What:="縦最大値", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 0).Value = (srchRange.Offset(1, 0).Value - 32) * 1.8
            srchRange.Offset(1, 1).Value = (srchRange.Offset(1, 1).Value - 32) * 1.8
            srchRange.Offset(1, 2).Value = (srchRange.Offset(1, 2).Value - 32) * 1.8
        
            hMax = srchRange.Offset(1, 0).Value
            hMin = srchRange.Offset(1, 1).Value
            hStep = srchRange.Offset(1, 2).Value
        End If
    End If

    'グラフ編集シートの縦軸変更
    Set currWorksheet = Worksheets(GraphEditSheetName)
    If argXy = "縦" Then
        Set srchRange = currWorksheet.Cells.Find(What:="−縦軸−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 1).Value = vMax
            srchRange.Offset(2, 1).Value = vMin
            srchRange.Offset(3, 1).Value = vStep
        End If
    ElseIf argXy = "横" Then
        Set srchRange = currWorksheet.Cells.Find(What:="−横軸−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 1).Value = hMax
            srchRange.Offset(2, 1).Value = hMin
            srchRange.Offset(3, 1).Value = hStep
        End If
    End If
    
    '基準線演算
    Set currWorksheet = Worksheets(GraphEditSheetName)
    If argXy = "横" Then
        Set srchRange = currWorksheet.Cells.Find(What:="−縦軸基準線−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
                ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
                'wrkCnt = srchRange.Offset(1, 0).End(xlDown).Row
                wrkCnt = GetDownToEndRange(srchRange.Offset(1, 0)).Row
                ' Wang FoaStudio Issue AISTEMP-120 20181218 End
                If wrkCnt < 15 And wrkCnt > 0 Then
                    If argPtn = "℃→F" Then
                        For pos = 10 To wrkCnt
                            srchRange.Offset(pos - 9, 1).Value = srchRange.Offset(pos - 9, 1).Value * 1.8 + 32
                        Next pos
                    Else
                        For pos = 10 To wrkCnt
                            srchRange.Offset(pos - 9, 1).Value = (srchRange.Offset(pos - 9, 1).Value - 32) * 1.8
                        Next pos
                    End If
                End If
        End If
    ElseIf argXy = "縦" Then
        Set srchRange = currWorksheet.Cells.Find(What:="−横軸基準線−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
                ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
                'wrkCnt = srchRange.Offset(1, 0).End(xlDown).Row
                wrkCnt = GetDownToEndRange(srchRange.Offset(1, 0)).Row
                ' Wang FoaStudio Issue AISTEMP-120 20181218 End
                If wrkCnt < 15 And wrkCnt > 0 Then
                    If argPtn = "℃→F" Then
                        For pos = 10 To wrkCnt
                            srchRange.Offset(pos - 9, 1).Value = srchRange.Offset(pos - 9, 1).Value * 1.8 + 32
                        Next pos
                    Else
                        For pos = 10 To wrkCnt
                            srchRange.Offset(pos - 9, 1).Value = (srchRange.Offset(pos - 9, 1).Value - 32) * 1.8
                        Next pos
                    End If
                End If
        End If
    End If
    
    'グラフ編集シート
    Set currChartObj = currWorksheet.ChartObjects(HistGraph02Name)
    '* グラフの縦軸の最大値／最小値／ステップを再設定
    Call SetGraphParamY(currChartObj, CDbl(vMax), CDbl(vMin), CDbl(vStep))
    '* グラフの横軸の最大値／最小値／ステップを再設定
    Call SetGraphParamX(currChartObj, CDbl(hMax), CDbl(hMin), CDbl(hStep))
    
    'グラフシート
    Set currChartObj = Worksheets(FixGraphSheetName).ChartObjects(HistGraph02Name)
    '* グラフの縦軸の最大値／最小値／ステップを再設定
    Call SetGraphParamY(currChartObj, CDbl(vMax), CDbl(vMin), CDbl(vStep))
    '* グラフの横軸の最大値／最小値／ステップを再設定
    Call SetGraphParamX(currChartObj, CDbl(hMax), CDbl(hMin), CDbl(hStep))
        
    '* 基準線を再配置する
    Call GraphReCalculate
    
    Application.ScreenUpdating = True
    
End Sub

'工学変換(温度以外)
Public Sub reDrowGraph()
    Dim currWorksheet         As Worksheet
    Dim currChartObj           As ChartObject
    Dim srchRange               As Range
    Dim pos                          As Integer
    Dim wrkCnt                     As Integer
    Dim vMax, vMin, vStep
    Dim hMax, hMin, hStep
    Dim yokoVal, tateVal

    'グラフ最大・最小値・刻み設定
    Application.ScreenUpdating = False
        
    'グラフ用データ設定
    yokoVal = YokoUnitValue
    tateVal = TateUnitValue

On Error Resume Next

    Set currWorksheet = Worksheets(PivotSheetName)  'グラフテーブル
    Set srchRange = currWorksheet.Cells.Find(What:="グラフ用データ", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
            ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
            'wrkCnt = srchRange.Offset(1, 0).End(xlDown).Row
            wrkCnt = GetDownToEndRange(srchRange.Offset(1, 0)).Row
            ' Wang FoaStudio Issue AISTEMP-120 20181218 End
            If wrkCnt > 0 Then
                For pos = 2 To wrkCnt
                    srchRange.Offset(pos - 1, 0).Value = srchRange.Offset(pos - 1, 0).Value * yokoVal
                    srchRange.Offset(pos - 1, 1).Value = srchRange.Offset(pos - 1, 1).Value * tateVal
                Next pos
            End If
    End If
    
    Set srchRange = currWorksheet.Cells.Find(What:="縦最大値", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 0).Value = srchRange.Offset(1, 0).Value * tateVal
        srchRange.Offset(1, 1).Value = srchRange.Offset(1, 1).Value * tateVal
        srchRange.Offset(1, 2).Value = srchRange.Offset(1, 2).Value * tateVal
    
        vMax = srchRange.Offset(1, 0).Value
        vMin = srchRange.Offset(1, 1).Value
        vStep = srchRange.Offset(1, 2).Value
    End If
    Set srchRange = currWorksheet.Cells.Find(What:="横最大値", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 0).Value = srchRange.Offset(1, 0).Value * yokoVal
        srchRange.Offset(1, 1).Value = srchRange.Offset(1, 1).Value * yokoVal
        srchRange.Offset(1, 2).Value = srchRange.Offset(1, 2).Value * yokoVal
    
        hMax = srchRange.Offset(1, 0).Value
        hMin = srchRange.Offset(1, 1).Value
        hStep = srchRange.Offset(1, 2).Value
    End If
    
    'グラフ編集シートの縦軸変更
    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="−縦軸−", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 1).Value = vMax
        srchRange.Offset(2, 1).Value = vMin
        srchRange.Offset(3, 1).Value = vStep
    End If
    
    Set srchRange = currWorksheet.Cells.Find(What:="−横軸−", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 1).Value = hMax
        srchRange.Offset(2, 1).Value = hMin
        srchRange.Offset(3, 1).Value = hStep
    End If
    
    '基準線演算
    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="−縦軸基準線−", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
            ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
            'wrkCnt = srchRange.Offset(0, 0).End(xlDown).Row
            wrkCnt = GetDownToEndRange(srchRange.Offset(0, 0)).Row
            ' Wang FoaStudio Issue AISTEMP-120 20181218 End
            If wrkCnt < 15 And wrkCnt > 0 Then
                For pos = 10 To wrkCnt
                    srchRange.Offset(pos - 9, 1).Value = srchRange.Offset(pos - 9, 1).Value * yokoVal
                Next pos
            End If
    End If
    
    Set srchRange = currWorksheet.Cells.Find(What:="−横軸基準線−", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
            ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
            'wrkCnt = srchRange.Offset(0, 0).End(xlDown).Row
            wrkCnt = GetDownToEndRange(srchRange.Offset(0, 0)).Row
            ' Wang FoaStudio Issue AISTEMP-120 20181218 End
            If wrkCnt < 15 And wrkCnt > 0 Then
                For pos = 10 To wrkCnt
                    srchRange.Offset(pos - 9, 1).Value = srchRange.Offset(pos - 9, 1).Value * tateVal
                Next pos
            End If
    End If
    
    'グラフ編集シート
    Set currChartObj = currWorksheet.ChartObjects(HistGraph02Name)
    '* グラフの縦軸の最大値／最小値／ステップを再設定
    Call SetGraphParamY(currChartObj, CDbl(vMax), CDbl(vMin), CDbl(vStep))
    '* グラフの横軸の最大値／最小値／ステップを再設定
    Call SetGraphParamX(currChartObj, CDbl(hMax), CDbl(hMin), CDbl(hStep))
    
    'グラフシート
    Set currChartObj = Worksheets(FixGraphSheetName).ChartObjects(HistGraph02Name)
    '* グラフの縦軸の最大値／最小値／ステップを再設定
    Call SetGraphParamY(currChartObj, CDbl(vMax), CDbl(vMin), CDbl(vStep))
    '* グラフの横軸の最大値／最小値／ステップを再設定
    Call SetGraphParamX(currChartObj, CDbl(hMax), CDbl(hMin), CDbl(hStep))
        
    '* 基準線を再配置する
    Call GraphReCalculate
    
    Application.ScreenUpdating = True
    
End Sub

'横軸変換値
Public Function YokoUnitValue() As Variant
    Dim wrkRange    As Range
    Dim strFrom     As String
    Dim strTo       As String
    
    Dim valFrom     As Variant
    Dim valTo       As Variant
    Dim valRslt     As Variant
    
    YokoUnitValue = 1
    
    '横軸変換文字検索→変換単位取得
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="横軸変換", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        strFrom = wrkRange.Offset(0, 1).Value
        strTo = wrkRange.Offset(0, 3).Value
        
        '変換文字無のチェック
        If strFrom = "" Or strTo = "" Then
            Exit Function
        End If
    Else
        Exit Function
    End If
    
    '横軸変換前単位の値を単位テーブルから取込む
    Set wrkRange = Worksheets("単位テーブル").Cells.Find(What:=strFrom, LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        valFrom = wrkRange.Offset(0, 1).Value
    Else
        Exit Function
    End If
    
    '横軸変換後単位の値を単位テーブルから取込む
    Set wrkRange = Worksheets("単位テーブル").Cells.Find(What:=strTo, LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        valTo = wrkRange.Offset(0, 1).Value
    Else
        Exit Function
    End If
    
    YokoUnitValue = valTo / valFrom
    
End Function

'縦軸変換値
Public Function TateUnitValue() As Variant
    Dim wrkRange    As Range
    Dim strFrom     As String
    Dim strTo       As String
    
    Dim valFrom     As Variant
    Dim valTo       As Variant
    Dim valRslt     As Variant
    
    TateUnitValue = 1
    
    '縦軸変換文字検索→変換単位取得
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="縦軸変換", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        strFrom = wrkRange.Offset(0, 1).Value
        strTo = wrkRange.Offset(0, 3).Value
          
        '変換文字無のチェック
        If strFrom = "" Or strTo = "" Then
            Exit Function
        End If
    Else
        Exit Function
    End If
    
    '縦軸変換前単位の値を単位テーブルから取込む
    Set wrkRange = Worksheets("単位テーブル").Cells.Find(What:=strFrom, LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        valFrom = wrkRange.Offset(0, 1).Value
    Else
        Exit Function
    End If
    
    '縦軸変換後単位の値を単位テーブルから取込む
    Set wrkRange = Worksheets("単位テーブル").Cells.Find(What:=strTo, LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        valTo = wrkRange.Offset(0, 1).Value
    Else
        Exit Function
    End If
    
    TateUnitValue = valTo / valFrom

End Function

'
Public Sub unititemSelect(argTarget As Range)
    Dim wrkRange    As Range
    Dim wrkCell     As Variant
    Dim wrkStr      As String
    
    '縦軸変換文字検索→変換単位取得
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="−工学値変換−", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        Select Case argTarget.Address
            Case wrkRange.Offset(1, 1).Address
                If wrkRange.Offset(1, 1).Value <> "" Then
                    wrkRange.Offset(1, 1).Value = ""
                    wrkRange.Offset(1, 1).Interior.ColorIndex = 0   ' 背景色
                    Exit Sub
                End If
                
                wrkRange.Offset(1, 1).Value = "変換中"
                wrkRange.Offset(1, 1).Interior.ColorIndex = 15   ' 背景色
                Exit Sub
            Case wrkRange.Offset(1, 3).Address
                If wrkRange.Offset(1, 3).Value <> "" Then
                    wrkRange.Offset(1, 3).Value = ""
                    wrkRange.Offset(1, 3).Interior.ColorIndex = 0   ' 背景色
                    Exit Sub
                End If
            
                wrkRange.Offset(1, 3).Value = "変換中"
                wrkRange.Offset(1, 3).Interior.ColorIndex = 15   ' 背景色
                Exit Sub
            Case wrkRange.Offset(2, 1).Address
                If wrkRange.Offset(2, 1).Value <> "" Then
                    wrkRange.Offset(2, 1).Value = ""
                    wrkRange.Offset(2, 1).Interior.ColorIndex = 0   ' 背景色
                    Exit Sub
                End If
                
                wrkRange.Offset(2, 1).Value = "変換中"
                wrkRange.Offset(2, 1).Interior.ColorIndex = 15   ' 背景色
                Exit Sub
            Case wrkRange.Offset(2, 3).Address
                If wrkRange.Offset(2, 3).Value <> "" Then
                    wrkRange.Offset(2, 3).Value = ""
                    wrkRange.Offset(2, 3).Interior.ColorIndex = 0   ' 背景色
                    Exit Sub
                End If
                wrkRange.Offset(2, 3).Value = "変換中"
                wrkRange.Offset(2, 3).Interior.ColorIndex = 15   ' 背景色
                Exit Sub
        End Select
    Else
        Exit Sub
    End If
    
    'コピー元選択
    'セル結合の場合の対処
    wrkCell = Split(argTarget.Address, ":")
    wrkStr = Worksheets(GraphEditSheetName).Range(wrkCell(0))
    If wrkStr = "" Then
        Exit Sub
    End If
    
    'コピー元の値を変換中セルに書込み
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="変換中", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        wrkRange.Value = wrkStr
        wrkRange.Interior.ColorIndex = 0   ' 背景色
    End If

End Sub
