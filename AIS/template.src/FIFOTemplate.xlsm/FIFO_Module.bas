Attribute VB_Name = "FIFO_Module"
'ISSUE_NO.703 sunyi 2018/06/22 start
Option Explicit
'ISSUE_NO.703 sunyi 2018/06/22 end
'*************************************
'* グラフの描画用：縦横軸算出
'*************************************
Public Sub ChangeChartData()
    
    Dim MaxDateTime     As String
    Dim MinDateTime     As String
    Dim KizamiTime      As String
    
    Dim wrkStr          As String
    Dim wrkPos          As String
    Dim wrkCnt          As String

    ' Wang Issue AISTEMP-101 20181206 Start
    'Dim wrkInt          As Integer
    Dim wrkInt          As Long
    ' Wang Issue AISTEMP-101 20181206 End
    Dim pos             As Integer
    Dim cnt             As Integer
    Dim intTerm         As Double       '* 2016/08/23 Change Integer->Double
    Dim rng_DateTime    As Range
    Dim dataRange       As Range
    Dim srchRange       As Range
    Dim gChartObj       As ChartObject
    
    Dim DateTime        As Date
    Dim DblInt          As Double
    
    Dim setDate         As Date
    
    Call SetFilenameToVariable
    
    'グラフ開始・終了日時設定
    'オンライン、オフライン判断　2017/01/19
    Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="オンライン", LookAt:=xlWhole)
    If srchRange.Offset(0, 1).Value = "OFFLINE" Then
        'オフライン時開始・終了時刻
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="表示開始期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            setDate = srchRange.Offset(0, 1).Value
        Else
            setDate = CDate("2016/1/1 00:00:00")
        End If
        Workbooks(thisBookName).Worksheets(GraphEditSheetName).Range("E4").Value = setDate
        Workbooks(thisBookName).Worksheets(FixGraphSheetName).Range("E4").Value = setDate
         
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="表示終了期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            setDate = srchRange.Offset(0, 1).Value
        Else
            setDate = CDate("2016/1/1 00:00:00")
        End If
        Workbooks(thisBookName).Worksheets(GraphEditSheetName).Range("H4").Value = setDate
        Workbooks(thisBookName).Worksheets(FixGraphSheetName).Range("H4").Value = setDate
    
    Else
        'オンライン時開始・終了時刻
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="取得開始", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            setDate = srchRange.Offset(0, 1).Value
        Else
            setDate = CDate("2016/1/1 00:00:00")
        End If
        Workbooks(thisBookName).Worksheets(GraphEditSheetName).Range("E4").Value = setDate
        Workbooks(thisBookName).Worksheets(FixGraphSheetName).Range("E4").Value = setDate
         
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            setDate = srchRange.Offset(0, 1).Value
        Else
            setDate = CDate("2016/1/1 00:00:00")
        End If
        Workbooks(thisBookName).Worksheets(GraphEditSheetName).Range("H4").Value = setDate
        Workbooks(thisBookName).Worksheets(FixGraphSheetName).Range("H4").Value = setDate
     End If

    ' thisBookName取込
    Call SetFilenameToVariable
    
    Worksheets(ForPivotSheetName).Range("I3").Value = 0
    Worksheets(ForPivotSheetName).Range("J3").Value = 10
    
    'E3: 演算結果テーブルの作業終了日時の最終行検索
    Set dataRange = Worksheets(ForPivotSheetName).Range("D2")  '演算結果テーブル
    
    'ﾃﾞｰﾀ無しの場合、ｸﾞﾗﾌを表示しない。20161028
    wrkStr = dataRange.Value
    If wrkStr = "" Then
        Exit Sub
    End If
    
    '2017/03/10 Mod No.487 Added data check. データ件数１件の対策用
    'wrkStr = Replace(dataRange.End(xlDown).Address, "$", "")
    'wrkInt = dataRange.End(xlDown).Rows.Row
    If Worksheets(ForPivotSheetName).Range("A3").Value = "" Then  '2017/06/09 Changed.
        wrkStr = Replace(dataRange.Address, "$", "")
        '* ISSUE_NO.703 Sunyi 2018/06/20 Start
        '全データを計算する
        'wrkInt = dataRange.Row
        '* ISSUE_NO.703 Sunyi 2018/06/20 End
    Else
        ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
        'wrkStr = Replace(dataRange.End(xlDown).Address, "$", "")
        wrkStr = Replace(GetDownToEndRange(dataRange).Address, "$", "")
        ' Wang FoaStudio Issue AISTEMP-120 20181218 End
        
        '* ISSUE_NO.703 Sunyi 2018/06/20 Start
        '全データを計算する
        'wrkInt = dataRange.End(xlDown).Rows.Row
        '* ISSUE_NO.703 Sunyi 2018/06/20 End
    End If
    
    '* ISSUE_NO.703 Sunyi 2018/06/20 Start
    '全データを計算する
    Set dataRange = Worksheets(ForPivotSheetName).Range("B2")
    
    ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
    'wrkInt = dataRange.End(xlDown).Rows.Row
    wrkInt = GetDownToEndRange(dataRange).Rows.Row
    ' Wang FoaStudio Issue AISTEMP-120 20181218 End
    
    '* ISSUE_NO.703 Sunyi 2018/06/20　End
    
    ' 250以降のデータ削除：グラフの系列数が２５５までなので、２５０系列までの表示とする。
''''    If wrkInt > 252 Then
''''        Worksheets(ForPivotSheetName).Rows("253:" & Trim(CStr(wrkInt))).Delete
''''    End If
    
    'param：表示期間
    Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="表示期間", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        intTerm = srchRange.Offset(0, 1).Value         ' 表示時間
    Else
        intTerm = 6
    End If
    
    'param：自動更新
    'D3:作業開始日時
    Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        If UCase(Trim(srchRange.Offset(0, 1).Value)) = "ON" Then
            MaxDateTime = Format(Now, "YYYY/MM/DD hh:mm:ss")
            MinDateTime = Format(DateAdd("h", intTerm * -1, Now), "YYYY/MM/DD hh:mm:ss")
        Else
            '作業開始日時と作業終了日時から最大・最小値日時の算出
            ' Wang FoaStudio Issue AISTEMP-96 20181219 Start
            'Set rng_DateTime = worksheets(ForPivotSheetName).Range("D2:" & wrkStr)
            'MaxDateTime = Format(WorksheetFunction.Max(rng_DateTime), "YYYY/MM/DD hh:mm:ss")
            'MinDateTime = Format(WorksheetFunction.Min(rng_DateTime), "YYYY/MM/DD hh:mm:ss")
            MaxDateTime = Format(Now, "YYYY/MM/DD hh:mm:ss")
            MinDateTime = Format(DateAdd("h", intTerm * -1, Now), "YYYY/MM/DD hh:mm:ss")
            ' Wang FoaStudio Issue AISTEMP-96 20181219 End
        End If
    End If
    
    '* DAC-87 sunyi 20181228 start
    Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="オンライン", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "OFFLINE" Then
            Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="表示終了期間", LookAt:=xlWhole)
            MaxDateTime = Format(srchRange.Offset(0, 1).Value, "YYYY/MM/DD hh:mm:ss")
            MinDateTime = Format(DateAdd("h", intTerm * -1, srchRange.Offset(0, 1).Value), "YYYY/MM/DD hh:mm:ss")
        End If
    End If
    '* DAC-87 sunyi 20181228 end
    
    '30分桁上げ
    DateTime = Application.WorksheetFunction.Ceiling(CDate(MaxDateTime), 1 / 48)
    MaxDateTime = Format(DateTime, "yyyy/MM/dd hh:mm:ss")
 
    '30分桁下げ
    If intTerm <= 0 Then
        '取得データの最大日時〜最小日時で表示
        DblInt = -60 * 15
        DateTime = DateAdd("s", DblInt, CDate(MinDateTime))
        DateTime = Application.WorksheetFunction.Floor(CDate(DateTime), 1 / 48)
        MinDateTime = Format(DateTime, "yyyy/MM/dd hh:mm:ss")
    Else
        '収集終了日時-表示期間で表示
        DblInt = -3600 * intTerm
        DateTime = DateAdd("s", DblInt, CDate(MaxDateTime))
        DateTime = Application.WorksheetFunction.Floor(CDate(DateTime), 1 / 48)
        MinDateTime = Format(DateTime, "yyyy/MM/dd hh:mm:ss")
    End If
    
    'グラフ編集シートの表示最新時刻・表示開始時刻・表示刻み設定
    Set srchRange = Workbooks(thisBookName).Worksheets(GraphEditSheetName).Cells.Find(What:="表示最新時刻", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = MaxDateTime
    End If
    Set srchRange = Workbooks(thisBookName).Worksheets(GraphEditSheetName).Cells.Find(What:="表示開始時刻", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = MinDateTime
    End If
    
    '表示刻み時間の取込
    Set srchRange = Workbooks(thisBookName).Worksheets(GraphEditSheetName).Cells.Find(What:="表示刻み[時間]", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        KizamiTime = Format(srchRange.Offset(0, 1).Value, "hh:mm:ss")
        If KizamiTime = "" Then
            srchRange.Offset(0, 1).Value = "01:00:00"  'デフォルト1時間
            KizamiTime = "01:00:00"
        End If
    End If
    
    '表題下の開始・終了時刻セット
'     Workbooks(thisBookName).Worksheets(GraphEditSheetName).Range("E4").Value = MinDateTime
'     Workbooks(thisBookName).Worksheets(GraphEditSheetName).Range("H4").Value = MaxDateTime
'     Workbooks(thisBookName).Worksheets(FixGraphSheetName).Range("E4").Value = MinDateTime
'     Workbooks(thisBookName).Worksheets(FixGraphSheetName).Range("H4").Value = MaxDateTime
    
    Application.ScreenUpdating = False
    
    'グラフ編集シート：グラフ描画
    Call GraphDrowing(GraphEditSheetName, MaxDateTime, MinDateTime, KizamiTime, wrkInt)
    
    'グラフシート：グラフ描画
    Call GraphDrowing(FixGraphSheetName, MaxDateTime, MinDateTime, KizamiTime, wrkInt)
    
    Application.ScreenUpdating = True  'No.633 2017/04/25 False→True
    
End Sub

'*************************************
'* グラフの描画
'* 2016.08.02 「アプリケーション定義またはオブジェクト定義のエラーです」
'*************************************
' Wang Issue AISTEMP-101 20181206 Start
'Public Sub GraphDrowing(argSheetNmae As String, argMaxDate As String, argMixDate As String, argKizami As String, argLine As Integer)
Public Sub GraphDrowing(argSheetNmae As String, argMaxDate As String, argMixDate As String, argKizami As String, argLine As Long)
' Wang Issue AISTEMP-101 20181206 End
    Dim wrkStr              As String
    Dim wrkPos              As String
    Dim wrkCnt              As String

    Dim wrkInt              As Integer
    ' Wang Issue AISTEMP-101 20181206 Start
    'Dim pos                   As Integer
    'Dim cnt                   As Integer
    Dim pos                   As Long
    Dim cnt                   As Long
    ' Wang Issue AISTEMP-101 20181206 Start
    Dim gChartObj        As ChartObject
    
    ' Wang Issue AISTEMP-101 20181206 Start
    'Dim fromPos             As Integer
    'Dim toPos                 As Integer
    Dim fromPos             As Long
    Dim toPos                 As Long
    ' Wang Issue AISTEMP-101 20181206 End
    Dim stepVal              As Integer
    Dim sampleType       As String
    Dim srchRange           As Range
    
    'グラフ表示*************************************************************
    'グラフ編集シート：上目盛
    Set gChartObj = Worksheets(argSheetNmae).ChartObjects("上目盛")
    
    '* 横軸の最大／最小の設定
    gChartObj.Chart.Axes(xlCategory).MinimumScale = DateValue(argMixDate) + TimeValue(argMixDate)
    gChartObj.Chart.Axes(xlCategory).MaximumScale = DateValue(argMaxDate) + TimeValue(argMaxDate)
    gChartObj.Chart.Axes(xlCategory).MajorUnit = TimeValue(argKizami)
    
    'グラフ編集シート：FIFOグラフ
    Set gChartObj = Worksheets(argSheetNmae).ChartObjects(HistGraph01Name)
    
    '* 縦軸の最大／最小の設定
    gChartObj.Chart.Axes(xlValue).MinimumScale = 0
    gChartObj.Chart.Axes(xlValue).MaximumScale = 10
    
    '*AISTEMP-121 sunyi 20190117 start
    'グラフの中線が非表示する
    gChartObj.Chart.Axes(xlValue).MajorGridlines.Format.Line.Visible = msoFalse
    '* sunyi 20190117 end
    
    '* 横軸の最大／最小の設定
    gChartObj.Chart.Axes(xlCategory).MinimumScale = DateValue(argMixDate) + TimeValue(argMixDate)
    gChartObj.Chart.Axes(xlCategory).MaximumScale = DateValue(argMaxDate) + TimeValue(argMaxDate)
    gChartObj.Chart.Axes(xlCategory).MajorUnit = TimeValue(argKizami)
    
    '系列の削除
    Call LineDelete(argSheetNmae)
    
    
    sampleType = ""
    Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
    
    If UCase(sampleType) = "GRIP" Then
        fromPos = argLine
        toPos = 2
        stepVal = -1
    Else
        fromPos = 2
        toPos = argLine
        stepVal = 1
    End If
    
    
    cnt = 0
    For pos = fromPos To toPos Step stepVal
'* ISSUE_NO.703 Sunyi 2018/06/20 Start
'全データを計算する
L1:
    If UCase(sampleType) = "GRIP" Then
        If pos < toPos Then
            Exit For
        End If
    Else
        If pos > toPos Then
            Exit For
        End If
    End If
'* ISSUE_NO.703 Sunyi End
        wrkPos = Trim(CStr(pos))
        '演算結果データの開始時刻のチェック
        If Worksheets(ForPivotSheetName).Range("D" & wrkPos).Value = "" Then
        '* ISSUE_NO.703 Sunyi 2018/06/20 Start
        '全データを計算する
            If UCase(sampleType) = "GRIP" Then
                pos = pos - 1
            Else
                pos = pos + 1
            End If
            
            GoTo L1
        '* ISSUE_NO.703 Sunyi 2018/06/20 End
        End If
        
        On Error GoTo ErrorHandler
        If Worksheets(ForPivotSheetName).Range("C" & wrkPos).Value > 0 Then
            cnt = cnt + 1
            If cnt > 250 Then
                Exit For
            End If
            
            wrkCnt = Trim(CStr(cnt))
            '日時を時刻に変更
            '開始日時→開始時刻に変更
            Worksheets(ForPivotSheetName).Range("F" & wrkPos).Value = Format(Worksheets(ForPivotSheetName).Range("D" & wrkPos).Value, "hh:mm")
            '終了日時→終了時刻に変更
            Worksheets(ForPivotSheetName).Range("G" & wrkPos).Value = Format(Worksheets(ForPivotSheetName).Range("E" & wrkPos).Value, "hh:mm")
            '系列名の作成
            Worksheets(ForPivotSheetName).Range("H" & wrkPos).Value = "L" & wrkCnt
            
            '横軸範囲設定
            wrkStr = "$D$" & wrkPos & ":" & "$E$" & wrkPos
            gChartObj.Chart.SeriesCollection.NewSeries
            '* 2016.08.03 Add ↓↓↓**************************
            On Error Resume Next
            '* 2016.08.03 Add ↑↑↑**************************
            '系列名称作成
            gChartObj.Chart.SeriesCollection(cnt).Name = "=" & ForPivotSheetName & "!$H$" & wrkPos
            'gChartObj.Chart.FullSeriesCollection(cnt).Name = "=" & ForPivotSheetName & "!$H$" & wrkPos
            
            '横軸：作業開始時刻・作業終了時刻
            gChartObj.Chart.SeriesCollection(cnt).XValues = "=" & ForPivotSheetName & "!" & wrkStr
            'gChartObj.Chart.FullSeriesCollection(cnt).XValues = "=" & ForPivotSheetName & "!" & wrkStr
            
            '縦軸：最小値・最大値
            gChartObj.Chart.SeriesCollection(cnt).Values = "=" & ForPivotSheetName & "!$I$3:$J$3"
            'gChartObj.Chart.FullSeriesCollection(cnt).Values = "=" & ForPivotSheetName & "!$I$3:$J$3"
            
            '系列の線プロパティ作成
            With gChartObj.Chart.SeriesCollection(cnt).Format.Line
            'With gChartObj.Chart.FullSeriesCollection(cnt).Format.Line
                '.ForeColor.ObjectThemeColor = msoThemeColorText1
                '.ForeColor.TintAndShade = 0
                '.ForeColor.Brightness = 0
                '.Transparency = 0
                .Weight = 0.25
                .Visible = msoTrue
            End With
            
            gChartObj.Chart.SeriesCollection(cnt).MarkerStyle = xlMarkerStyleNone  '-4142
            'gChartObj.Chart.FullSeriesCollection(cnt).MarkerStyle = xlMarkerStyleNone  '-4142
            
            DoEvents
        End If
    Next pos
    
ErrorHandler:

End Sub

'*************************************
'* グラフ系列の削除
'*************************************
Public Sub LineDelete(argSheetName As String)
    Dim pos As Integer
    Dim sum As Integer
    
    sum = Worksheets(argSheetName).ChartObjects(HistGraph01Name).Chart.SeriesCollection.count

    For pos = sum To 1 Step -1
    
        If Worksheets(argSheetName).ChartObjects(HistGraph01Name).Chart.SeriesCollection(pos - 0).Name <> "系列1" Then
        
            Worksheets(argSheetName).ChartObjects(HistGraph01Name).Chart.SeriesCollection(pos - 0).Delete
        
        End If
    Next

End Sub

