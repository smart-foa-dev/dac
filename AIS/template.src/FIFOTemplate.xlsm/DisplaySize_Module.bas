Attribute VB_Name = "DisplaySize_Module"
Public Declare PtrSafe Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
Public Declare PtrSafe Function SetCursorPos Lib "user32" (ByVal X As Long, ByVal Y As Long) As Long

Public Const LOGPIXELSX As Long = &H58&
Public Const LOGPIXELSY As Long = &H5A&

Public Declare PtrSafe Function GetDesktopWindow Lib "user32" () As Long
Public Declare PtrSafe Function GetWindowRect Lib "user32" (ByVal Hwnd As Long, lpRect As RECT) As Long

Public Declare PtrSafe Function GetDeviceCaps Lib "gdi32" ( _
    ByVal hDc As Long, _
    ByVal nIndex As Long _
    ) As Long
Public Declare PtrSafe Function GetDC Lib "user32" ( _
    ByVal Hwnd As Long _
    ) As Long
Public Declare PtrSafe Sub ReleaseDC Lib "user32" ( _
    ByVal Hwnd As Long, _
    ByVal hDc As Long _
    )

Public Type POINTAPI
    X As Long
    Y As Long
End Type

Public Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type

'*******************************************
'* ウインドウをディスプレイの中心に表示する
'*******************************************
Public Sub MoveCenterWindow()
    Dim Ret             As Long
    Dim dskRect         As RECT
    Dim exlRect         As RECT
    Dim Hwnd            As Long
    Dim currWindow      As Window
    Dim workbookName    As String
    Dim exlWidth        As Long
    Dim exlHeight       As Long
    Dim exlTop          As Long
    Dim exlLeft         As Long

    workbookName = ThisWorkbook.Name
    Set currWindow = Windows(workbookName)
    
    '* エクセルのウインドウハンドル
    Hwnd = currWindow.Hwnd
    Call GetWindowRect(Hwnd, exlRect)
    exlWidth = exlRect.Right - exlRect.Left
    exlHeight = exlRect.Bottom - exlRect.Top
    
    '* デスクトップのウインドウハンドル
    Ret = GetDesktopWindow
    Call GetWindowRect(Ret, dskRect)
    
    exlTop = (dskRect.Bottom / 2) - (exlHeight / 2)
    exlLeft = (dskRect.Right / 2) - (exlWidth / 2)
    
    currWindow.WindowState = xlNormal
    currWindow.Left = exlLeft * 72 / 96
    currWindow.Top = exlTop * 72 / 96

    Call GetWindowRect(Hwnd, exlRect)

'    MsgBox "左上のx座標は" & exlRect.Left & vbCrLf & _
'        "左上のy座標は" & exlRect.Top & vbCrLf & _
'        "右下のx座標は" & exlRect.Right & vbCrLf & _
'        "右下のy座標は" & exlRect.Bottom
End Sub

Public Sub Sample()
    Dim Hwnd As Long
    Dim hDc As Long
    Hwnd = Excel.Application.Hwnd
    hDc = GetDC(Hwnd)
    '水平方向DPI
        aaa = GetDeviceCaps(hDc, LOGPIXELSX)
    '垂直方向DPI
        bbb = GetDeviceCaps(hDc, LOGPIXELSY)
    ReleaseDC Hwnd, hDc
End Sub

'* ウィンドウのサイズに合わせて指定範囲をズームする
Sub ZoomSamp1()
    Range("A1:S31").Select     '---ズームしたい範囲を選択
    ActiveWindow.Zoom = True  '---選択範囲をズーム
'    MsgBox Format(ActiveWindow.Zoom / 100, "0%") '---表示サイズを表示
'    ActiveWindow.Zoom = True  '---元にサイズに戻す
End Sub

'****************************************************
'* 対象ワークシートの選択範囲をparamシートに保存する
'****************************************************
Public Sub SetDisplayRange(currWorksheet As Worksheet, currRange As Range)
    Dim paramWorkhseet  As Worksheet
    Dim startRange      As Range
    Dim srchRange       As Range
    Dim prevRange       As Range
    Dim currSheetName   As String
    Dim workStr         As String
    Dim strAddress      As String
    Dim writeOK         As Boolean
    
    Set paramWorkhseet = Worksheets(ParamSheetName)
    writeOK = False
    
    RCAddress = currRange.Address(RowAbsolute:=False, ColumnAbsolute:=False, ReferenceStyle:=xlA1)
    
    '* 選択範囲の保存
    With paramWorkhseet
        Set startRange = .Cells(1, 1).EntireColumn
        Set srchRange = startRange.Find(What:="表示サイズ", LookAt:=xlPart)
        If Not srchRange Is Nothing Then
            workStr = srchRange.Offset(0, 0).Value
            If InStr(workStr, "_") > 0 Then
                '* シート名を取得
                currSheetName = Right(workStr, Len(workStr) - InStr(workStr, "_"))
                If currSheetName = currWorksheet.Name Then
                    srchRange.Offset(0, 1).Value = RCAddress
                Else
                    strAddress = srchRange.Address
                    Do While Not srchRange Is Nothing
                        Set prevRange = srchRange
                        Set srchRange = startRange.FindNext(srchRange)
                        If strAddress = srchRange.Address Then
                            Exit Do
                        End If
                        
                        workStr = srchRange.Offset(0, 0).Value
                        If InStr(workStr, "_") > 0 Then
                            currSheetName = Right(workStr, Len(workStr) - InStr(workStr, "_"))
                            If currSheetName = currWorksheet.Name Then
                                srchRange.Offset(0, 1).Value = RCAddress
                                writeOK = True
                                Exit Do
                            End If
                        End If
                    
                    Loop
                    If Not writeOK Then
                        prevRange.Offset(1, 0).Value = "表示サイズ_" & currWorksheet.Name
                        prevRange.Offset(1, 1).Value = RCAddress
                    End If
                End If
            Else
                srchRange.Offset(0, 0).Value = "表示サイズ_" & currWorksheet.Name
                srchRange.Offset(0, 1).Value = RCAddress
            End If
        End If
        
    End With
End Sub

'*******************************************************
'* 対象ワークシートの選択範囲をparamシートから取得lする
'*******************************************************
Public Function GetDisplayAddress(currWorksheet As Worksheet) As String
    Dim paramWorkhseet  As Worksheet
    Dim startRange      As Range
    Dim srchRange       As Range
    Dim strAddress      As String
    
    Set paramWorkhseet = Worksheets(ParamSheetName)
    
    With paramWorkhseet
        Set startRange = .Cells(1, 1).EntireColumn
        Set srchRange = startRange.Find(What:="表示サイズ_" & currWorksheet.Name)
        If Not srchRange Is Nothing Then
            strAddress = srchRange.Offset(0, 1).Value
        Else
            strAddress = ""
        End If
    End With
    
    GetDisplayAddress = strAddress
End Function

'***************
'* 最大表示処理
'***************
Public Sub ZoomDisplayFull()
    Dim workbookName    As String
    Dim window1         As Window
    Dim RCAddress       As String
    Dim dspRange        As Range
    Dim workRange       As Range
    
    workbookName = ThisWorkbook.Name
    Set window1 = Windows(workbookName)

'新）==========================================
    '* 設定されている表示範囲のアドレスを取得し、範囲を設定する
    RCAddress = GetDisplayAddress(ActiveSheet)
    If RCAddress <> "" Then
        Set dspRange = ActiveSheet.Range(RCAddress)
        dspRange.Select

        '* Windowのサイズを変更する
        window1.WindowState = xlMaximized
        Application.WindowState = xlMaximized
        window1.Zoom = True

        Application.GoTo dspRange

        '* セルの左上を選択する
        Set workRange = dspRange.Resize(1, 1)
        workRange.Select
    Else
        MsgBox "表示範囲が設定されていないため、画面のみフルスクリーン表示します。"

        '* Windowのサイズを変更する
        window1.WindowState = xlMaximized

        Application.WindowState = xlMaximized

        Exit Sub
    End If
'新）==========================================

    
''    '* 設定されている表示範囲のアドレスを取得し、範囲を設定する
''    RCAddress = GetDisplayAddress(ActiveSheet)
''    If RCAddress <> "" Then
''        Set dspRange = ActiveSheet.Range(RCAddress)
''        dspRange.Select
''
''        '* Windowのサイズを変更する
''        window1.WindowState = xlMaximized
''        window1.Zoom = True
''
''        Application.GoTo dspRange
''
''        '* セルの左上を選択する
''        Set workRange = dspRange.Resize(1, 1)
''        workRange.Select
''    Else
''        MsgBox "表示範囲が設定されていないため、画面のみフルスクリーン表示します。"
''
''        '* Windowのサイズを変更する
''        window1.WindowState = xlMaximized
''
''        Exit Sub
''    End If

    
End Sub

'*******************
'* 10％拡大表示処理
'*******************
Public Sub ZoomDisplayPlus()
    Dim zoomValue As Integer
    
    ActiveWindow.DisplayHorizontalScrollBar = False  '水平スクロールバー
    ActiveWindow.DisplayVerticalScrollBar = False    '垂直スクロールバー
    
    zoomValue = ActiveWindow.Zoom + 10
    If zoomValue > 150 Then
        zoomValue = 150
    End If
    
    Call GoZoomDisplay(zoomValue)
    rc = DispBairitu(zoomValue)
End Sub

'*******************
'* 10％縮小表示処理
'*******************
Public Sub ZoomDisplayMinus()
    Dim zoomValue As Integer
    
    zoomValue = ActiveWindow.Zoom - 10
    If zoomValue < 20 Then
        zoomValue = 20
    End If
    
    Call GoZoomDisplay(zoomValue)
    rc = DispBairitu(zoomValue)
End Sub

'*******************
'* 選択範囲保存処理
'*******************
Public Sub ZoomSetDisplayRange()
    Dim workRange       As Range
    
    '* 選択範囲の取得
    Set workRange = Selection

    '* 選択範囲をparamシートに保存
    Call SetDisplayRange(ActiveSheet, workRange)
    
    '--2017/04/27 No.650 オンライン/オフライン別ボタン表示切り替え-----Start
    '    '* ボタンの表示およびウィンドウサイズの変更
    '    Call AddMenuButton

    Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="オンライン", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        'オンラインとオフラインでボタン表示方法が異なる
        If srchRange.Offset(0, 1).Value <> "OFFLINE" Then
          '* ボタンの表示およびウィンドウサイズの変更
           Call AddMenuButton
           '* 更新状態表示
           Call UpdateFormStatus
        Else
          '* ボタンの表示およびウィンドウサイズの変更
           Call OffLineSet
        End If
    End If
    '--2017/04/27 No.650 オンライン/オフライン別ボタン表示切り替え-----End

    '* 諸々バーを非表示
'    Call ScreenDisplayONOFF(False)
    
    Call GoZoomDisplay(ActiveWindow.Zoom)

End Sub

'*******************
'* 選択範囲解除処理
'*******************
Public Sub ZoomUnSetDisplayRange()
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim currShape       As Shape
    Dim paramWorksheet  As Worksheet
    Dim dispArea        As String
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).ActiveSheet
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    '* 選択範囲をparamシートから削除
    dispArea = ""
    With paramWorksheet
    
        Set workRange = .Cells.Find(What:="表示サイズ", LookAt:=xlPart)
        workRange.Offset(0, 0) = "表示サイズ"
        dispArea = workRange.Offset(0, 1).Value
        workRange.Offset(0, 1).ClearContents
    End With
'    currWorksheet.Range(dispArea).Interior.ColorIndex = 0

''    '20161117　dispArea未設定の場合、エラーとなるのでdispAreaの有無ﾁｪｯｸ追加
''    If dispArea <> "" Then
''        currWorksheet.Range(dispArea).Interior.ColorIndex = 0
''    End If
    
    '* 着目シート上のボタン図形を全て削除する
    For Each currShape In currWorksheet.Shapes
        If InStr(currShape.Name, PrefixStrBT) Then
            currShape.Delete
        ElseIf InStr(currShape.Name, PrefixStrTX) Then
            currShape.Delete
        End If
    Next currShape

    '* 諸々バーを表示
    Call ScreenDisplayONOFF(True)
    
'    Call GoZoomDisplay(ActiveWindow.Zoom)

End Sub

'***************
'* 表示設定処理
'***************
Public Sub GoZoomDisplay(zoomSize As Integer)
    Dim RCAddress   As String
    Dim dspRange    As Range
    
'    Application.ScreenUpdating = True
    
    '* 設定されている表示範囲のアドレスを取得する
    RCAddress = GetDisplayAddress(ActiveSheet)
    If RCAddress <> "" Then
        Set dspRange = ActiveSheet.Range(RCAddress)
    Else
        Exit Sub
    End If

'    Application.ScreenUpdating = False
    Call ZoomSelectRange(zoomSize, dspRange)
'    Call MoveCenterWindow
'    ActiveSheet.ScrollArea = ""
'    Application.ScreenUpdating = True
'    MsgBox "表示" & zoomSize & "%"

End Sub

'*****************************************************************************
'* ウィンドウサイズを指定サイズに変更し、内容をそのサイズに合わせてズームする
'*****************************************************************************
Public Sub ZoomSelectRange(zoomValue As Variant, zoomRange As Range)
    Dim window1         As Window
    Dim workbookName    As String
    Dim workRange       As Range
    Const DPI As Long = 96
    Const PPI As Long = 72
    Dim RCAddress       As String
    Dim prevZoom        As Variant
    
    Application.GoTo Reference:=zoomRange, Scroll:=True
    
    ActiveWindow.Zoom = zoomValue
    
'    zoomRange.Select
    RCAddress = zoomRange.Address(ReferenceStyle:=xlR1C1)
    
    RC1Left = ActiveWindow.PointsToScreenPixelsX(zoomRange.Left)
    RC1Top = ActiveWindow.PointsToScreenPixelsY(zoomRange.Top)
    RC2Left = ActiveWindow.PointsToScreenPixelsX(zoomRange.Width * DPI / PPI * (ActiveWindow.Zoom / 100))
    RC2Top = ActiveWindow.PointsToScreenPixelsY(zoomRange.Height * DPI / PPI * (ActiveWindow.Zoom / 100))
    
'    Call SetCursorPos(RC1Left, RC1Top)
'    Call SetCursorPos(RC2Left, RC2Top)
    
    Set workRange = Worksheets("グラフ編集").Range("A1:M31")
    
    workbookName = ThisWorkbook.Name
    Set window1 = Windows(workbookName)

'新）==========================================
'    window1.WindowState = xlMaximized

    '* Windowのサイズを変更する
    '* リボン、メニューその他が表示されている場合のズーム
    If window1.DisplayHeadings Then
        Application.WindowState = xlNormal
        Application.Width = (zoomRange.Width * (ActiveWindow.Zoom / 100)) + (36 * (PPI / DPI))
        Application.Height = (zoomRange.Height * (ActiveWindow.Zoom / 100)) + (210 * (PPI / DPI))
    '* リボン、メニューその他が表示されていない場合のズーム
    Else
        Application.WindowState = xlNormal
        Application.Width = zoomRange.Width * CDbl(ActiveWindow.Zoom / 100)
        Application.Height = (zoomRange.Height + 40) * (ActiveWindow.Zoom / 100)
    End If
'新）==========================================

''    '* Windowのサイズを変更する
''    '* リボン、メニューその他が表示されている場合のズーム
''    If window1.DisplayHeadings Then
''        window1.WindowState = xlNormal
''        window1.Width = (zoomRange.Width * (ActiveWindow.Zoom / 100)) + (36 * (PPI / DPI))
''        window1.Height = (zoomRange.Height * (ActiveWindow.Zoom / 100)) + (210 * (PPI / DPI))
''    '* リボン、メニューその他が表示されていない場合のズーム
''    Else
''        window1.WindowState = xlNormal
''        window1.Width = (zoomRange.Width * (ActiveWindow.Zoom / 100))
''        window1.Height = ((zoomRange.Height + 40) * (ActiveWindow.Zoom / 100))
'''        window1.Width = (RC2Left - RC1Left) * (PPI / DPI)
'''        window1.Height = (RC2Top - RC1Top + 30) * (PPI / DPI)
''    End If
    
    '* 選択範囲を一旦ウィンドウサイズに合わせてズームし、再度設定拡大縮小率に戻す
    '* →選択範囲を左上に表示させるために必要
'    Application.Goto zoomRange
'    ActiveWindow.Zoom = True
'    ActiveWindow.Zoom = zoomValue

    Application.GoTo zoomRange
    
    '* セルの左上を選択する
    Set workRange = zoomRange.Resize(1, 1)
    workRange.Select
    
End Sub

'***********************************************************************
'* 初期表示時の表示倍率の算出
'***********************************************************************
Public Function DispBairitu(argVal As Integer) As Integer
    Dim wrkRange    As Range
    Dim paramRange  As Range
    Dim wrkInt      As Integer
    
    DispBairitu = 40
    Set paramRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="表示倍率", LookAt:=xlWhole)
    If Not paramRange Is Nothing Then
        paramRange.Offset(0, 1).Value = argVal
        wrkInt = Val(paramRange.Offset(0, 1).Value)
        If wrkInt < 40 Then
            DispBairitu = 40
        Else
            DispBairitu = wrkInt
        End If
    End If
    
    Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示倍率", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        wrkRange.Offset(0, 1).Value = DispBairitu
    End If
    
    Set wrkRange = Worksheets(FixGraphSheetName).Cells.Find(What:="表示倍率", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        wrkRange.Offset(0, 1).Value = DispBairitu
    End If

End Function

'***********************************************************************
'* 初期表示時の表示倍率の保存
'***********************************************************************
Public Sub SaveBairitu()
    Dim wrkRange    As Range
    Dim paramRange  As Range
    Dim wrkInt      As Integer
    
    'グラフ編集シートの倍率取込
    wrkInt = 40
    Set wrkRange = Worksheets(FixGraphSheetName).Cells.Find(What:="表示倍率", LookAt:=xlWhole)
    'Set wrkRange = Worksheets(GraphEditSheetName).Cells.Find(What:="表示倍率", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        wrkInt = Val(wrkRange.Offset(0, 1).Value)
    End If
    
    'parameシートへの倍率書込
    Set paramRange = Worksheets(ParamSheetName).Cells.Find(What:="表示倍率", LookAt:=xlWhole)
    If Not paramRange Is Nothing Then
        paramRange.Offset(0, 1).Value = wrkInt
    End If

End Sub

