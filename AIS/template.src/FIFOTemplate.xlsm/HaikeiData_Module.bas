Attribute VB_Name = "HaikeiData_Module"
Option Explicit

'*******************************************************
' 背景ﾃﾞｰﾀ、凡例一覧表の作成
'*******************************************************
Public Sub HaikeiHanreiDataMake()

    ''Call HaikeiDataClear

    ''Call HaikeiDataToGraphSheet
    
    'リストBOX削除
    ''''Call HanreiMake
    
    '''Call GraphLabelCopy
    
    Call DispTermDispInterval

End Sub

Public Sub ListDelete()
    Dim Mycontrol       As Object
    Dim targetSheet     As Worksheet

    'リストボックス作成
    Set targetSheet = Worksheets(FixGraphSheetName)
    
    'リストBox削除
    For Each Mycontrol In targetSheet.OLEObjects
        Mycontrol.Delete
    Next

End Sub

'*******************************************************
' 凡例一覧表の作成
'*******************************************************
Public Sub HanreiMake()
    Dim Mycontrol       As Object
    Dim olelist         As Object
    Dim objlist         As Object
    Dim targetSheet     As Worksheet
    Dim CellSize        As Range
    Dim srchRange       As Range
    
    Dim currChartObj    As ChartObject
    Dim currSeries      As Series
    
    Dim wrkWidth
    Dim wrKTop
    Dim wrkLeft

    'リストボックス作成
    Set targetSheet = Worksheets(FixGraphSheetName)
    
    'リストBox削除
    For Each Mycontrol In targetSheet.OLEObjects
        Mycontrol.Delete
    Next
 
    '背景データ書込み位置
    Set srchRange = targetSheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    Set CellSize = targetSheet.Range(srchRange.Offset(0, 0), srchRange.Offset(0, 2))
    
    'リストコントロールの位置決め
    wrkWidth = srchRange.Offset(0, 0).Width + srchRange.Offset(0, 1).Width + srchRange.Offset(0, 2).Width
    wrKTop = targetSheet.Range("L17").Top
    wrkLeft = targetSheet.Range("L17").Left
    'Set olelist = TargetSheet.OLEObjects.Add(ClassType:="Forms.ListBox.1", Left:=wrkLeft, Top:=wrKTop, Width:=132, Height:=150)
    Set olelist = targetSheet.OLEObjects.Add(ClassType:="Forms.ListBox.1", Left:=wrkLeft, Top:=wrKTop, Width:=wrkWidth, Height:=150)
    
    '作成したリストボックス生成
    Set objlist = targetSheet.OLEObjects(olelist.Name).Object
'    objlist.FontName = "メイリオ"
'    objlist.FontSize = 12

    '凡例をリストBoxに作成
    Set currChartObj = targetSheet.ChartObjects(HistGraph02Name)
    For Each currSeries In currChartObj.Chart.SeriesCollection
        If InStr(currSeries.Name, "系列") > 0 Then
        Else
            objlist.AddItem currSeries.Name
        End If
    Next currSeries
    
    
    'TargetSheet.Select
    
    '終了処理
'    Set objlist = Nothing
'    Set olelist = Nothing
End Sub

'*******************************************************
' 背景データをグラフシートに移動
'*******************************************************
Public Sub HaikeiDataToGraphSheet()
    Dim pos                 As Integer
    Dim cnt                 As Integer
    Dim endRow              As Integer
    
    Dim srcTarget           As Worksheet
    Dim srcRange            As Range
    Dim copyMoto            As Range
    Dim haikeiRange         As Range
    
    Dim targetSheet         As Worksheet
    Dim TargetRange         As Range
    Dim copySaki            As Range

    Dim lineRange           As Range

    
    Set srcTarget = Worksheets(GraphEditSheetName)
    Set targetSheet = Worksheets(FixGraphSheetName)
    
    'グラフ編集シートの背景データ数
    Set srcRange = srcTarget.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    Set haikeiRange = srcRange.Offset(1, 0)
    
    '背景データの有無チェック
    If haikeiRange.Offset(1, 0).Value = "" Or haikeiRange.Offset(1, 0).Value = "-" Or haikeiRange.Offset(1, 0).Value = "−" Or haikeiRange.Offset(1, 0).Value = Null Then
        Exit Sub
    End If
    
    ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
    'Set copyMoto = srcTarget.Range(haikeiRange.Offset(1, 0).Cells, haikeiRange.End(xlDown).Cells)
    Set copyMoto = srcTarget.Range(haikeiRange.Offset(1, 0).Cells, GetDownToEndRange(haikeiRange).Cells)
    ' Wang FoaStudio Issue AISTEMP-120 20181218 End

'    If haikeiRange.End(xlDown).Row > 10 Then
'        Exit Sub
'    End If
'
    ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
    'endRow = haikeiRange.End(xlDown).Cells.Row
    endRow = GetDownToEndRange(haikeiRange).Cells.Row
    ' Wang FoaStudio Issue AISTEMP-120 20181218 End

    'グラフシートの背景データ書込み位置
    Set TargetRange = targetSheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    Set copySaki = TargetRange.Offset(1, 0)

    '背景データコピー
    'copyMoto.Copy Destination:=copySaki
    cnt = 0
    For pos = 6 To endRow
        If haikeiRange.Offset(cnt + 1, 0).Value = "-" Or haikeiRange.Offset(cnt + 1, 0).Value = "−" Then
            Exit For
        End If
    
        copySaki.Offset(cnt, 0).Value = haikeiRange.Offset(cnt + 1, 0).Value
        copySaki.Offset(cnt, 2).Value = haikeiRange.Offset(cnt + 1, 1).Value & haikeiRange.Offset(cnt + 1, 2).Value
        '背景データ名称罫線作成
        Set lineRange = targetSheet.Range(TargetRange.Offset(cnt + 1, 0).Cells, TargetRange.Offset(cnt + 1, 1).Cells)
        With lineRange
            .Borders(xlEdgeTop).LineStyle = xlContinuous
            .Borders(xlEdgeTop).Weight = xlThin
            .Borders(xlEdgeBottom).LineStyle = xlContinuous
            .Borders(xlEdgeBottom).Weight = xlThin
            .Borders(xlEdgeLeft).LineStyle = xlContinuous
            .Borders(xlEdgeLeft).Weight = xlThin
            .Borders(xlEdgeRight).LineStyle = xlContinuous
            .Borders(xlEdgeRight).Weight = xlThin
        End With
        
        '背景データ値+単位罫線作成
        Set lineRange = targetSheet.Range(TargetRange.Offset(cnt + 1, 2).Cells, TargetRange.Offset(cnt + 1, 2).Cells)
        With lineRange
            .Borders(xlEdgeTop).LineStyle = xlContinuous
            .Borders(xlEdgeTop).Weight = xlThin
            .Borders(xlEdgeBottom).LineStyle = xlContinuous
            .Borders(xlEdgeBottom).Weight = xlThin
            .Borders(xlEdgeLeft).LineStyle = xlContinuous
            .Borders(xlEdgeLeft).Weight = xlThin
            .Borders(xlEdgeRight).LineStyle = xlContinuous
            .Borders(xlEdgeRight).Weight = xlThin
        End With
        cnt = cnt + 1
    Next pos
End Sub

'*******************************************************
' グラフシートの背景データクリアー
'*******************************************************
Public Sub HaikeiDataClear()

    Dim targetSheet         As Worksheet
    Dim TargetRange         As Range
    Dim endCell             As Range
    Dim clearCell           As Range

    Set targetSheet = Worksheets(FixGraphSheetName)
    
    'グラフシートの背景データ書込み位置
    Set TargetRange = targetSheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    
        '背景データの有無チェック
    If TargetRange.Offset(1, 0).Value = "" Then
        Exit Sub
    End If
    
    ' Wang FoaStudio Issue AISTEMP-120 20181218 Start
    'Set endCell = TargetRange.End(xlDown).Cells
    Set endCell = GetDownToEndRange(TargetRange).Cells
    ' Wang FoaStudio Issue AISTEMP-120 20181218 End

    Set clearCell = targetSheet.Range(TargetRange.Offset(1, 0), endCell.Offset(0, 2))
'    clearCell.Delete (xlShiftUp)
    With clearCell
        .ClearContents
        .Borders(xlDiagonalDown).LineStyle = xlNone
        .Borders(xlDiagonalUp).LineStyle = xlNone
        .Borders(xlEdgeLeft).LineStyle = xlNone
        .Borders(xlEdgeTop).LineStyle = xlNone
        .Borders(xlEdgeBottom).LineStyle = xlNone
        .Borders(xlEdgeRight).LineStyle = xlNone
        .Borders(xlInsideVertical).LineStyle = xlNone
        .Borders(xlInsideHorizontal).LineStyle = xlNone
    End With
End Sub

'*******************************************************
' グラフラベルコピー
'*******************************************************
Public Sub GraphLabelCopy()
    Dim pvtWorksheet    As Worksheet
    Dim currChartObj    As ChartObject
    Dim actChartObj     As Object
    Dim etcChartObj     As Object
    
    Dim wrkTate         As String
    Dim wrkYoko         As String
        
    'Set pvtWorksheet = Worksheets("グラフ編集")
    'Set currChartObj = pvtWorksheet.ChartObjects("BulkyGraph")
    'Set actChartObj = currChartObj.Chart
    Set actChartObj = Worksheets(GraphEditSheetName).ChartObjects(HistGraph01Name).Chart

    'グラフ編集用ラベル取込
    With actChartObj
'        .HasTitle = True                  '---グラフタイトル表示
'        .ChartTitle.Text = "9月度売上"    '---タイトル文字列設定
        '横ラベル
         With .Axes(xlCategory, xlPrimary) '---主軸項目軸tate
            wrkYoko = .AxisTitle.text
'             .HasTitle = True              '---軸ラベル表示
'             .AxisTitle.Text = "売り場"    '---軸ラベル文字列設定
         End With
         '縦ラベル
         With .Axes(xlValue, xlPrimary)    '---主軸数値軸
            wrkTate = .AxisTitle.text
'             .HasTitle = True              '---軸ラベル表示
'             .AxisTitle.Text = "売上高"    '---軸ラベル文字列設定
         End With
    End With
    
    Set actChartObj = Worksheets(FixGraphSheetName).ChartObjects(HistGraph02Name).Chart
    'グラフ用ラベル書込
    With actChartObj
'        .HasTitle = True                  '---グラフタイトル表示
'        .ChartTitle.Text = "9月度売上"    '---タイトル文字列設定
        '横ラベル
         With .Axes(xlCategory, xlPrimary) '---主軸項目軸tate
             .HasTitle = True              '---軸ラベル表示
             .AxisTitle.text = wrkYoko     '---軸ラベル文字列設定
         End With
         '縦ラベル
         With .Axes(xlValue, xlPrimary)    '---主軸数値軸
             .HasTitle = True              '---軸ラベル表示
             .AxisTitle.text = wrkTate     '---軸ラベル文字列設定
         End With
    End With
End Sub

'*******************************************************
' 表示刻み、表示期間の表示
'*******************************************************
Public Sub DispTermDispInterval()
    Dim targetSheet         As Worksheet
    Dim TargetRange         As Range

    Set targetSheet = Worksheets(ParamSheetName)
    
    Set TargetRange = targetSheet.Cells.Find(What:="オンライン", LookAt:=xlWhole)
    If Not TargetRange Is Nothing Then
        If TargetRange.Offset(0, 1).Value <> "ONLINE" Then
            Worksheets(GraphEditSheetName).Range("L29").Value = ""
            Worksheets(FixGraphSheetName).Range("L29").Value = ""
            Worksheets(GraphEditSheetName).Range("L30").Value = ""
            Worksheets(FixGraphSheetName).Range("L30").Value = ""
            Exit Sub
        End If
    End If
    
    Set TargetRange = targetSheet.Cells.Find(What:="表示期間", LookAt:=xlWhole)
    If Not TargetRange Is Nothing Then
        Worksheets(GraphEditSheetName).Range("L29").Value = "表示期間：" & TargetRange.Offset(0, 1).Value & "時間"
        Worksheets(FixGraphSheetName).Range("L29").Value = "表示期間：" & TargetRange.Offset(0, 1).Value & "時間"
    Else
    
    End If
    
    'グラフシートの背景データ書込み位置
    Set TargetRange = targetSheet.Cells.Find(What:="更新周期", LookAt:=xlWhole)
    If Not TargetRange Is Nothing Then
        Worksheets(GraphEditSheetName).Range("L30").Value = "更新周期：" & TargetRange.Offset(0, 1).Value & "分"
        Worksheets(FixGraphSheetName).Range("L30").Value = "更新周期：" & TargetRange.Offset(0, 1).Value & "分"
    Else
    
    End If

End Sub





