Attribute VB_Name = "BaseLine_Module"
Option Explicit

'*********************
'* 基準線を再配置する
'*********************
Public Sub GraphReCalculate()
    Dim graphWorksheet      As Worksheet
    Dim graphEditWorksheet  As Worksheet
    Dim hBaseList()         As BASEINFO
    Dim vBaseList()         As BASEINFO
    Dim hValue              As MAXMINSTEPINFO
    Dim vValue              As MAXMINSTEPINFO
    Dim currChartObj        As ChartObject
    
    ReDim vBaseList(0)
    ReDim hBaseList(0)
    
    '* グラフ編集のワークシート
    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    '* グラフのワークシート
    Set graphWorksheet = Worksheets(FixGraphSheetName)
    
    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Call GetMaxMinStepValue(GraphEditSheetName, vValue, hValue)
    
    '* グラフ編集シートから縦軸と横軸の基準線情報を取得する
    Call GetBaseLineInfo(GraphEditSheetName, vBaseList, hBaseList)
    
    '* グラフから古い系統を削除する
    Call DeleteOldBaseLineSeries(graphEditWorksheet)
    Call DeleteOldBaseLineSeries(graphWorksheet)
    
    '* グラフテーブルから古い基準線データを削除する（X軸方向のみ）
    Call DeleteOldBaseLineDataOnlyHorizon
    
    '* グラフテーブルに新しい基準線データを生成する（X軸方向のみ）
    Call AddNewBaseLineDataOnlyHorizon
    
    '* グラフに新しい系統を追加する
    Set currChartObj = graphEditWorksheet.ChartObjects(HistGraph02Name)
    Call AddNewBaseLineSeries(currChartObj)
    Set currChartObj = graphWorksheet.ChartObjects(HistGraph02Name)
    Call AddNewBaseLineSeries(currChartObj)
    
    '* グラフ編集シートの基準／上限／下限の水平方向の文字列を合わせる
    Call MoveShapeHorizonInGraphAll(graphEditWorksheet, vBaseList, hBaseList, vValue, hValue)

    '* グラフシートの基準／上限／下限の水平方向の文字列を合わせる
    Call MoveShapeHorizonInGraphAll(graphWorksheet, vBaseList, hBaseList, vValue, hValue)
End Sub

'*******************************************************
'* グラフ編集シートから縦軸と横軸の基準線情報を整理する
'*******************************************************
Public Sub OrganizeList(currSheetName As String)
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range
    Dim vStartRange             As Range
    Dim hStartRange             As Range

    Set currWorksheet = Worksheets(currSheetName)
    
    With currWorksheet

        Set srchRange = .Cells.Find(What:="−縦軸基準線−", LookAt:=xlWhole)
        Set vStartRange = srchRange.Offset(1, 0)
        Set srchRange = .Cells.Find(What:="−横軸基準線−", LookAt:=xlWhole)
        Set hStartRange = srchRange.Offset(1, 0)
    
    End With


End Sub

'*******************************
'* グラフから古い系統を削除する
'*******************************
Public Sub DeleteOldBaseLineSeries(graphWorksheet As Worksheet)
    Dim currChartObj            As ChartObject
    Dim currSeries              As Series
    Dim vBaseList()             As BASEINFO
    Dim hBaseList()             As BASEINFO
    Dim II                      As Integer
    Dim aaa                     As Integer
    
    ReDim vBaseList(0)
    ReDim hBaseList(0)
    
    Set currChartObj = graphWorksheet.ChartObjects(HistGraph02Name)
    
    Call GetOldBaseLineData(vBaseList, hBaseList)
    
'    '* 「系列」の文字を含まない系統を全て削除する
'    For Each currSeries In currChartObj.Chart.SeriesCollection
'            If InStr(currSeries.Name, "系列") = 0 Then
'                currSeries.Delete
'            End If
'    Next currSeries

    For Each currSeries In currChartObj.Chart.SeriesCollection
        If vBaseList(0).Name <> "" Then
            For II = 0 To UBound(vBaseList)
                If currSeries.Name = vBaseList(II).Name Then
                    currSeries.Delete
                    Exit For
                End If
            Next
            If II > UBound(vBaseList) Then
                If hBaseList(0).Name <> "" Then
                    For II = 0 To UBound(hBaseList)
                        If currSeries.Name = hBaseList(II).Name Then
                            currSeries.Delete
                            Exit For
                        End If
                    Next
                End If
            End If
        Else
            If hBaseList(0).Name <> "" Then
                For II = 0 To UBound(hBaseList)
                    If currSeries.Name = hBaseList(II).Name Then
                        currSeries.Delete
                        Exit For
                    End If
                Next
            End If
        End If
    Next currSeries
End Sub

'***********************************************
'* グラフテーブルから古い基準線データを取得する
'***********************************************
Public Sub GetOldBaseLineData(vBaseList() As BASEINFO, hBaseList() As BASEINFO)
    Dim srchRange           As Range
    Dim vStartRange         As Range
    Dim hStartRange         As Range
    Dim lastRange           As Range
    Dim delRange            As Range
    Dim graphDataWorksheet  As Worksheet
    Dim readIndex           As Integer
    Dim iFirstV             As Boolean
    Dim iFirstH             As Boolean
    Dim vIndex              As Integer
    Dim hIndex              As Integer
    
    Set graphDataWorksheet = Worksheets(PivotSheetName)
    
    With graphDataWorksheet
        Set srchRange = .Cells.Find(What:="縦横基準線系統", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            Set vStartRange = srchRange.Offset(1, 0)
            Set hStartRange = srchRange.Offset(4, 0)
        Else
            Exit Sub
        End If
        
        iFirstV = True
        iFirstH = True
        readIndex = 0
        Do
            If vStartRange.Offset(0, readIndex).Value = "" Then Exit Do
            
            If iFirstV Then
                vIndex = 0
                iFirstV = False
            Else
                vIndex = UBound(vBaseList) + 1
                ReDim Preserve vBaseList(vIndex)
            End If
            vBaseList(vIndex).Name = vStartRange.Offset(0, readIndex).Value
            readIndex = readIndex + 2
        Loop
        
        readIndex = 0
        Do
            If hStartRange.Offset(0, readIndex).Value = "" Then Exit Do
            
            If iFirstH Then
                hIndex = 0
                iFirstH = False
            Else
                hIndex = UBound(hBaseList) + 1
                ReDim Preserve hBaseList(hIndex)
            End If
            hBaseList(hIndex).Name = hStartRange.Offset(0, readIndex).Value
            readIndex = readIndex + 2
        Loop
        
    End With
End Sub

'***********************************************
'* グラフテーブルから古い基準線データを削除する
'***********************************************
Public Sub DeleteOldBaseLineData()
    Dim srchRange           As Range
    Dim vStartRange         As Range
    Dim hStartRange         As Range
    Dim lastRange           As Range
    Dim delRange            As Range
    Dim graphDataWorksheet  As Worksheet
    
    Set graphDataWorksheet = Worksheets(PivotSheetName)
    
    With graphDataWorksheet
        Set srchRange = .Cells.Find(What:="縦横基準線系統", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            Set vStartRange = srchRange.Offset(1, 0)
            Set hStartRange = srchRange.Offset(4, 0)
        Else
            Exit Sub
        End If
        Set lastRange = vStartRange.Offset(1, 0).End(xlToRight)
        Set delRange = .Range(vStartRange.Offset(0, 0), lastRange.Offset(1, 0))
        delRange.Clear
        Set lastRange = hStartRange.Offset(1, 0).End(xlToRight)
        Set delRange = .Range(hStartRange.Offset(0, 0), lastRange.Offset(1, 0))
        delRange.Clear
    End With
End Sub

'***********************************************
'* グラフテーブルから古い基準線データを削除する（X軸方向のみ）
'***********************************************
Public Sub DeleteOldBaseLineDataOnlyHorizon()
    Dim srchRange           As Range
    Dim vStartRange         As Range
    Dim hStartRange         As Range
    Dim lastRange           As Range
    Dim delRange            As Range
    Dim graphDataWorksheet  As Worksheet
    
    Set graphDataWorksheet = Worksheets(PivotSheetName)
    
    With graphDataWorksheet
        Set srchRange = .Cells.Find(What:="縦横基準線系統", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            Set hStartRange = srchRange.Offset(4, 0)
        Else
            Exit Sub
        End If
        Set lastRange = hStartRange.Offset(1, 0).End(xlToRight)
        Set delRange = .Range(hStartRange.Offset(0, 0), lastRange.Offset(1, 0))
        delRange.Clear
    End With
End Sub

'***********************************************
'* グラフテーブルに新しい基準線データを生成する
'***********************************************
Public Sub AddNewBaseLineData()
    Dim graphDataWorksheet  As Worksheet
    Dim hBaseList()         As BASEINFO
    Dim vBaseList()         As BASEINFO
    Dim srchRange           As Range
    Dim vStartRange         As Range
    Dim hStartRange         As Range
    Dim II                  As Integer
    Dim hValue              As MAXMINSTEPINFO
    Dim vValue              As MAXMINSTEPINFO
    Dim writeIndex          As Integer
    Dim seriesRange         As Range
    
    ReDim vBaseList(0)
    ReDim hBaseList(0)
    
    Set graphDataWorksheet = Worksheets(PivotSheetName)

    '* グラフ編集シートから縦軸と横軸の基準線情報を取得する
    Call GetBaseLineInfo(GraphEditSheetName, vBaseList, hBaseList)
    
    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Call GetMaxMinStepValue(GraphEditSheetName, vValue, hValue)
    
    With graphDataWorksheet
    
        Set srchRange = .Cells.Find(What:="縦横基準線系統", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            Set vStartRange = srchRange.Offset(1, 0)
            Set hStartRange = srchRange.Offset(4, 0)
        Else
            Exit Sub
        End If
        
        '* 縦軸基準線データ展開
        writeIndex = 0
        If vBaseList(0).Name <> "" Then
            For II = 0 To UBound(vBaseList)
                vStartRange.Offset(0, writeIndex).Value = vBaseList(II).Name
                vStartRange.Offset(1, writeIndex).Value = vBaseList(II).Value
                vStartRange.Offset(2, writeIndex).Value = vBaseList(II).Value
                vStartRange.Offset(1, writeIndex + 1).Value = vValue.MaxValue
                vStartRange.Offset(2, writeIndex + 1).Value = vValue.MinValue
                Set seriesRange = .Range(vStartRange.Offset(0, writeIndex), vStartRange.Offset(2, writeIndex + 1))
                With seriesRange.Borders
                    .LineStyle = xlContinuous
                    .Weight = xlMedium
                End With
                With seriesRange.Borders(xlInsideVertical)
                    .LineStyle = xlContinuous
                    .Weight = xlThin
                End With
                With seriesRange.Borders(xlInsideHorizontal)
                    .LineStyle = xlContinuous
                    .Weight = xlThin
                End With
                Set seriesRange = .Range(vStartRange.Offset(0, writeIndex), vStartRange.Offset(0, writeIndex + 1))
                With seriesRange.Borders(xlEdgeBottom)
                    .LineStyle = xlDouble
                End With
                writeIndex = writeIndex + 2
            Next
        End If
    
        '* 横軸基準線データ展開
        writeIndex = 0
        If hBaseList(0).Name <> "" Then
            For II = 0 To UBound(hBaseList)
                hStartRange.Offset(0, writeIndex).Value = hBaseList(II).Name
                hStartRange.Offset(1, writeIndex).Value = hValue.MaxValue
                hStartRange.Offset(2, writeIndex).Value = hValue.MinValue
                hStartRange.Offset(1, writeIndex + 1).Value = hBaseList(II).Value
                hStartRange.Offset(2, writeIndex + 1).Value = hBaseList(II).Value
                Set seriesRange = .Range(hStartRange.Offset(0, writeIndex), hStartRange.Offset(2, writeIndex + 1))
                With seriesRange.Borders
                    .LineStyle = xlContinuous
                    .Weight = xlMedium
                End With
                With seriesRange.Borders(xlInsideVertical)
                    .LineStyle = xlContinuous
                    .Weight = xlThin
                End With
                With seriesRange.Borders(xlInsideHorizontal)
                    .LineStyle = xlContinuous
                    .Weight = xlThin
                End With
                Set seriesRange = .Range(hStartRange.Offset(0, writeIndex), hStartRange.Offset(0, writeIndex + 1))
                With seriesRange.Borders(xlEdgeBottom)
                    .LineStyle = xlDouble
                End With
                writeIndex = writeIndex + 2
            Next
        End If
    
    End With
    
End Sub

'***********************************************
'* グラフテーブルに新しい基準線データを生成する（X軸方向のみ）
'***********************************************
Public Sub AddNewBaseLineDataOnlyHorizon()
    Dim graphDataWorksheet  As Worksheet
    Dim hBaseList()         As BASEINFO
    Dim vBaseList()         As BASEINFO
    Dim srchRange           As Range
    Dim vStartRange         As Range
    Dim hStartRange         As Range
    Dim II                  As Integer
    Dim hValue              As MAXMINSTEPINFO
    Dim vValue              As MAXMINSTEPINFO
    Dim writeIndex          As Integer
    Dim seriesRange         As Range
    Dim prevDate            As Date
    
    ReDim vBaseList(0)
    ReDim hBaseList(0)
    
    Set graphDataWorksheet = Worksheets(PivotSheetName)

    '* グラフ編集シートから縦軸と横軸の基準線情報を取得する
    Call GetBaseLineInfo(GraphEditSheetName, vBaseList, hBaseList)
    
    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Call GetMaxMinStepValue(GraphEditSheetName, vValue, hValue)
    
    With graphDataWorksheet
    
        Set srchRange = .Cells.Find(What:="縦横基準線系統", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            Set vStartRange = srchRange.Offset(1, 0)
            Set hStartRange = srchRange.Offset(4, 0)
        Else
            Exit Sub
        End If
        
        '* グラフ編集画面のデータで縦軸基準線データを更新する
        writeIndex = 0
        Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            prevDate = srchRange.Offset(0, 1).Value
        Else
            prevDate = CDate("2000/01/01 00:00:00")
        End If
        vStartRange.Offset(0, writeIndex).Value = "現在時刻"
        vStartRange.Offset(1, writeIndex).Value = prevDate
        vStartRange.Offset(2, writeIndex).Value = prevDate
        vStartRange.Offset(1, writeIndex + 1).Value = vValue.MaxValue
        vStartRange.Offset(2, writeIndex + 1).Value = vValue.MinValue
'        Set seriesRange = .Range(vStartRange.Offset(0, writeIndex), vStartRange.Offset(2, writeIndex + 1))
'        With seriesRange.Borders
'            .LineStyle = xlContinuous
'            .Weight = xlMedium
'        End With
'        With seriesRange.Borders(xlInsideVertical)
'            .LineStyle = xlContinuous
'            .Weight = xlThin
'        End With
'        With seriesRange.Borders(xlInsideHorizontal)
'            .LineStyle = xlContinuous
'            .Weight = xlThin
'        End With
'        Set seriesRange = .Range(vStartRange.Offset(0, writeIndex), vStartRange.Offset(0, writeIndex + 1))
'        With seriesRange.Borders(xlEdgeBottom)
'            .LineStyle = xlDouble
'        End With
'        writeIndex = writeIndex + 2
    
        '* 横軸基準線データ展開
        writeIndex = 0
        If hBaseList(0).Name <> "" Then
            For II = 0 To UBound(hBaseList)
                hStartRange.Offset(0, writeIndex).Value = hBaseList(II).Name
                hStartRange.Offset(1, writeIndex).Value = hValue.MaxValue
                hStartRange.Offset(2, writeIndex).Value = hValue.MinValue
                hStartRange.Offset(1, writeIndex + 1).Value = hBaseList(II).Value
                hStartRange.Offset(2, writeIndex + 1).Value = hBaseList(II).Value
                Set seriesRange = .Range(hStartRange.Offset(0, writeIndex), hStartRange.Offset(2, writeIndex + 1))
                With seriesRange.Borders
                    .LineStyle = xlContinuous
                    .Weight = xlMedium
                End With
                With seriesRange.Borders(xlInsideVertical)
                    .LineStyle = xlContinuous
                    .Weight = xlThin
                End With
                With seriesRange.Borders(xlInsideHorizontal)
                    .LineStyle = xlContinuous
                    .Weight = xlThin
                End With
                Set seriesRange = .Range(hStartRange.Offset(0, writeIndex), hStartRange.Offset(0, writeIndex + 1))
                With seriesRange.Borders(xlEdgeBottom)
                    .LineStyle = xlDouble
                End With
                writeIndex = writeIndex + 2
            Next
        End If
    
    End With
    
End Sub

'*******************************
'* グラフに新しい系統を追加する
'*******************************
Public Sub AddNewBaseLineSeries(currChartObj As ChartObject)
    Dim graphDataWorksheet  As Worksheet
    Dim srchRange           As Range
    Dim vStartRange         As Range
    Dim hStartRange         As Range
    Dim seriesName          As String
    Dim xRange              As Range
    Dim yRange              As Range
    Dim xRangeRC            As String
    Dim yRangeRC            As String
    Dim readIndex           As Integer

    Set graphDataWorksheet = Worksheets(PivotSheetName)
    
    With graphDataWorksheet
    
        Set srchRange = .Cells.Find(What:="縦横基準線系統", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            Set vStartRange = srchRange.Offset(1, 0)
            Set hStartRange = srchRange.Offset(4, 0)
        Else
            Exit Sub
        End If
        
        '* 縦軸方向
        readIndex = 0
        Do
            If vStartRange.Offset(0, readIndex).Value = "" Then Exit Do

            seriesName = vStartRange.Offset(0, readIndex).Value
            Set xRange = .Range(vStartRange.Offset(1, readIndex), vStartRange.Offset(2, readIndex))
            Set yRange = .Range(vStartRange.Offset(1, readIndex + 1), vStartRange.Offset(2, readIndex + 1))
            xRangeRC = "=" & PivotSheetName & "!" & xRange.Address
            yRangeRC = "=" & PivotSheetName & "!" & yRange.Address

            Call AddSeriesBaseLine(currChartObj, seriesName, xRangeRC, yRangeRC, RGB(255, 0, 0))
            
            readIndex = readIndex + 2
        Loop
    
        '* 横軸方向
        readIndex = 0
        Do
            If hStartRange.Offset(0, readIndex).Value = "" Then Exit Do

            seriesName = hStartRange.Offset(0, readIndex).Value
            Set xRange = .Range(hStartRange.Offset(1, readIndex), hStartRange.Offset(2, readIndex))
            Set yRange = .Range(hStartRange.Offset(1, readIndex + 1), hStartRange.Offset(2, readIndex + 1))
            xRangeRC = "=" & PivotSheetName & "!" & xRange.Address
            yRangeRC = "=" & PivotSheetName & "!" & yRange.Address

            Call AddSeriesBaseLine(currChartObj, seriesName, xRangeRC, yRangeRC, RGB(255, 0, 0))
            
            readIndex = readIndex + 2
        Loop
    
    End With
        
End Sub

'******************************************
'* 散布図に対し、新たに系統を追加する
'******************************************
Public Sub AddSeriesBaseLine(currChartObj As ChartObject, seriesName As String, xValueRC As String, yValueRC As String, seriesRGB As Long)
    Dim currSeries          As Series
    
    With currChartObj.Chart
        Set currSeries = .SeriesCollection.NewSeries
        With currSeries
            .Name = seriesName
            .XValues = xValueRC
            .Values = yValueRC
            .MarkerStyle = xlMarkerStyleNone
            .Select
            With .Format.Line
                .Visible = msoTrue
                .ForeColor.TintAndShade = 0
                .ForeColor.Brightness = 0
                .ForeColor.RGB = seriesRGB
                .Transparency = 0
                .Weight = 0.75
            End With
        End With
    End With
    
End Sub

'*********************************************
'* 指定シートにある縦横の基準線情報を取得する
'*********************************************
Public Sub GetBaseLineInfo(currSheetName As String, vBaseList() As BASEINFO, hBaseList() As BASEINFO)
    Dim currWorkseet        As Worksheet
    Dim srchRange           As Range
    Dim iFirstV             As Boolean
    Dim iFirstH             As Boolean
    Dim vBaseIndex          As Integer
    Dim hBaseIndex          As Integer
    Dim readIndex           As Integer

    '* グラフのワークシート
    Set currWorkseet = Worksheets(currSheetName)
    
    With currWorkseet
        
        '* 縦軸基準線の情報を取得
        Set srchRange = .Cells.Find(What:="−縦軸基準線−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            iFirstV = True
            readIndex = 1
            Do While (srchRange.Offset(readIndex, 0).Value <> "−") And (srchRange.Offset(readIndex, 0).Value <> "")
                If iFirstV Then
                    vBaseIndex = 0
                    iFirstV = False
                Else
                    vBaseIndex = UBound(vBaseList) + 1
                    ReDim Preserve vBaseList(vBaseIndex)
                End If
                vBaseList(vBaseIndex).Name = srchRange.Offset(readIndex, 0).Value
                vBaseList(vBaseIndex).Value = srchRange.Offset(readIndex, 1).Value
                readIndex = readIndex + 1
            Loop
        End If
        
        '* 縦軸基準線の情報を取得
        Set srchRange = .Cells.Find(What:="−横軸基準線−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            iFirstH = True
            readIndex = 1
            Do While (srchRange.Offset(readIndex, 0).Value <> "−") And (srchRange.Offset(readIndex, 0).Value <> "")
                If iFirstH Then
                    hBaseIndex = 0
                    ReDim hBaseList(hBaseIndex)
                    iFirstH = False
                Else
                    hBaseIndex = UBound(hBaseList) + 1
                    ReDim Preserve hBaseList(hBaseIndex)
                End If
                hBaseList(hBaseIndex).Name = srchRange.Offset(readIndex, 0).Value
                hBaseList(hBaseIndex).Value = srchRange.Offset(readIndex, 1).Value
                readIndex = readIndex + 1
            Loop
        End If
    
    End With

End Sub

'***********************************************
'* グラフ内水平方向の文字列の位置を全て更新する
'***********************************************
Public Sub MoveShapeHorizonInGraphAll(graphWorksheet As Worksheet, _
                                      vBaseList() As BASEINFO, hBaseList() As BASEINFO, _
                                      vValue As MAXMINSTEPINFO, hValue As MAXMINSTEPINFO)
    Dim chartObj            As ChartObject
    Dim currShape           As Shape
    Dim workStr             As String
    Dim hMaxValue           As Variant
    Dim hMinValue           As Variant
    Dim hBaseValue          As Variant
    Dim vMaxValue           As Variant
    Dim vMinValue           As Variant
    Dim vBaseValue          As Variant
    Dim paMax               As Variant
    Dim paMin               As Variant
    Dim paWidth             As Variant
    Dim paPos               As Variant
    Dim II                  As Integer
    Dim prevDate            As Date
    Dim srchRange           As Range
    
    With graphWorksheet
    
        '* 埋め込みグラフオブジェクトの取得
        Set chartObj = .ChartObjects(HistGraph02Name)
        
        '* 中途半端にグラフ上にテキストボックスがあれば一旦全て削除する
        For Each currShape In chartObj.Chart.Shapes
            currShape.Delete
        Next
        
        '* グラフ上にテキストボックスがなければ追加する
        Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            prevDate = srchRange.Offset(0, 1).Value
        Else
            prevDate = CDate("2000/01/01 00:00:00")
        End If
        If chartObj.Chart.Shapes.count = 0 Then
            For II = 0 To UBound(vBaseList)
                '* 縦軸方向の基準線文字列を追加する
                workStr = "vTB_Level" & Format(II, "00") & "InGraph"
                Call AddShapeHorizonInGraph(chartObj, workStr, "現在時刻")
                '* 縦軸方向の基準線文字列の位置を調整する
                Set currShape = chartObj.Chart.Shapes(workStr)
                Call MoveShapeHorizonInGraph(chartObj, currShape, hValue.MaxValue, hValue.MinValue, prevDate)
            Next
            For II = 0 To UBound(hBaseList)
                '* 横軸方向の基準線文字列を追加する
                workStr = "hTB_Level" & Format(II, "00") & "InGraph"
                Call AddShapeHorizonInGraph(chartObj, workStr, hBaseList(II).Name)
                '* 横軸方向の基準線文字列の位置を調整する
                Set currShape = chartObj.Chart.Shapes(workStr)
                Call MoveShapeVerticalInGraph(chartObj, currShape, vValue.MaxValue, vValue.MinValue, hBaseList(II).Value)
            Next
        End If
        
    End With

End Sub

'*************************************
'* 指定グラフの水平方向に上に指定文字列／テキストボックスIDを追加する
'*************************************
Public Sub AddShapeHorizonInGraph(chartObj As ChartObject, idStr As String, textStr As String)
    Dim graphWorksheet      As Worksheet
    Dim currShape           As Shape
    
    With chartObj.Chart.Shapes.AddLabel(msoTextOrientationHorizontal, 10, 10, 72, 72)
        With .TextFrame2
            .VerticalAnchor = msoAnchorMiddle
            .HorizontalAnchor = msoAnchorCenter
            .AutoSize = msoAutoSizeShapeToFitText
            With .TextRange
                .text = textStr
                .Font.Bold = msoTrue
                .Font.Size = 10
            End With
        End With
        .Name = idStr
    End With
        
End Sub

'*************************************
'* 指定グラフの垂直方向に上に指定文字列／テキストボックスIDを追加する
'*************************************
Public Sub AddShapeVerticalInGraph(chartObj As ChartObject, idStr As String, textStr As String)
    Dim graphWorksheet      As Worksheet
    Dim currShape           As Shape
    
    With chartObj.Chart.Shapes.AddLabel(msoTextOrientationHorizontal, 10, 10, 72, 72)
        With .TextFrame2
            .VerticalAnchor = msoAnchorMiddle
            .HorizontalAnchor = msoAnchorNone
            .AutoSize = msoAutoSizeShapeToFitText
            With .TextRange
                .text = textStr
                .Font.Bold = msoTrue
                .Font.Size = 10
            End With
        End With
        .Name = idStr
    End With
        
End Sub

'*************************************
'* グラフ内の水平方向のテキストボックスを移動する
'*************************************
Public Sub MoveShapeHorizonInGraph(chartObj As ChartObject, currShape As Shape, hMaxValue, hMinValue, hBaseValue)
    Dim graphWorksheet      As Worksheet
    Dim graphDataWorksheet  As Worksheet
    Dim workStr             As String
    Dim paMax               As Variant
    Dim paMin               As Variant
    Dim paWidth             As Variant
    Dim paPos               As Variant
    
    '* 基準値があるならば文字列の位置を合わせる
    If hBaseValue <> 0 Then
        currShape.Visible = msoTrue
        paWidth = chartObj.Chart.PlotArea.InsideWidth
        paMin = chartObj.Chart.PlotArea.InsideLeft
        paPos = ((hBaseValue - hMinValue) / (hMaxValue - hMinValue) * paWidth + paMin) - currShape.Width / 2

        '* 文字シェイプを新しい座標にセットする
        currShape.Top = chartObj.Chart.PlotArea.InsideTop - currShape.Height
        currShape.Left = paPos
    '* 基準値がなければ文字列を非表示にする
    Else
        currShape.Visible = msoFalse
    End If


End Sub

'*************************************
'* グラフ内の垂直方向のテキストボックスを移動する
'*************************************
Public Sub MoveShapeVerticalInGraph(chartObj As ChartObject, currShape As Shape, vMaxValue, vMinValue, vBaseValue)
    Dim graphWorksheet      As Worksheet
    Dim graphDataWorksheet  As Worksheet
    Dim workStr             As String
    Dim paMax               As Variant
    Dim paMin               As Variant
    Dim paHeight            As Variant
    Dim paPos               As Variant
    
    '* 基準値があるならば文字列の位置を合わせる
    If vBaseValue <> 0 Then
        currShape.Visible = msoTrue
        paHeight = chartObj.Chart.PlotArea.InsideHeight
        paMin = chartObj.Chart.PlotArea.InsideTop
        
        '原点をゼロにする為、基準線の位置から原点値を減算する。
        paPos = paMin + paHeight - ((vBaseValue - vMinValue) * paHeight) / (vMaxValue - vMinValue) - currShape.Height / 2
        'paPos = paMin + paHeight - (vBaseValue * paHeight) / (vMaxValue - vMinValue) - currShape.Height / 2

        '* 文字シェイプを新しい座標にセットする
        currShape.Top = paPos
        currShape.Left = chartObj.Chart.PlotArea.InsideWidth + chartObj.Chart.PlotArea.InsideLeft + 5
    '* 基準値がなければ文字列を非表示にする
    Else
        currShape.Visible = msoFalse
    End If

End Sub







