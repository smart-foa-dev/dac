VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ActionPaneBackForm 
   Caption         =   "背景色設定"
   ClientHeight    =   7305
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   7665
   OleObjectBlob   =   "ActionPaneBackForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "ActionPaneBackForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False




'*************************************
'* 表示色条件テキストボックスをクリア
'*************************************
Private Sub BT_Clear_Click()
    TB_White.text = ""
    TB_Red.text = ""
    TB_Yellow.text = ""
    TB_Green.text = ""
End Sub

'*********************
'* 表示色条件設定処理
'*********************
Private Sub BT_Set_Click()
    Dim shapeName           As String
    Dim srchRange           As Range
    Dim colRange            As Range
    Dim currRange           As Range
    Dim paramWorksheet    As Worksheet
    
    '2017/06/26 No.278 Add.---------Start
    If TB_ItemOperation.text = "" Then
        MsgBox "設定項目が設定されていません。"
        Exit Sub
    End If
    '2017/06/26 No.278 Add.---------End
    
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    With paramWorksheet

        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:="表示色対象", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.Offset(1, 1).NumberFormatLocal = "@"
            currRange.Offset(1, 1).Value = TB_White.text
            currRange.Offset(2, 1).NumberFormatLocal = "@"
            currRange.Offset(2, 1).Value = TB_Red.text
            currRange.Offset(3, 1).NumberFormatLocal = "@"
            currRange.Offset(3, 1).Value = TB_Yellow.text
            currRange.Offset(4, 1).NumberFormatLocal = "@"
            currRange.Offset(4, 1).Value = TB_Green.text
        End If
        
        currRange.Offset(0, 1).Value = TB_ItemOperation.text
        
        'エレメントIDをセット　'2017/06/26 No.278 Add.
        currRange.Offset(0, 2).Value = TB_ItemElementID.text

    End With
    
    Unload Me

End Sub

'*******************
'* 閉じるボタン処理
'*******************
Private Sub BT_Close_Click()

    Unload Me

End Sub

'***********************************************************************************
'***********************************************************************************
'***********************
'* ドラッグ完了時の処理
'***********************
Private Sub LV_Element_OLECompleteDrag(Effect As Long)
    Dim RowNum As Long  '2017/06/26 No.278 Add.
    Debug.Print "OLECompleteDrag"
    Effect = vbDropEffectCopy

    'エレメントIDをセット '2017/06/26 No.278 Add.
    If LV_Element.ListItems.Count > 0 Then
        RowNum = LV_Element.SelectedItem.Index
    End If
    LV_ElementID.ListItems.Item(RowNum).Selected = True
    TB_ItemElementID.text = ""
    TB_ItemElementID.text = LV_ElementID.SelectedItem.text
    
End Sub

'*****************************************************
'* リストボックスからテキストボックスへのドラッグ処理
'*****************************************************
Private Sub LV_Element_OLEStartDrag(Data As MSComctlLib.DataObject, AllowedEffects As Long)
    Dim workStr         As Variant
    Dim getTextStr      As Variant
    Debug.Print "OLEStartDrag"
    
    '* データオブジェクトの内容をクリア
    Data.Clear
    
    '* ドラッグ開始時のリストボックスの選択項目を取得
    getTextStr = LV_Element.SelectedItem.text
    
    '* 貼り付ける文字列の生成
    workStr = """" & LB_CTM.text & "・" & getTextStr & """"
    
    '''''workStr = """" & LB_CTM.text & "_" & getTextStr & """"
    
    '* データオブジェクトにセット
    Data.SetData workStr, vbCFtext
    
End Sub
'***********************************************************************************
'***********************************************************************************

Private Sub TextBox1_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal x As Single, ByVal y As Single)
    TB_ItemOperation.text = ""
End Sub

'*****************
'* フォーム初期化
'*****************
Private Sub UserForm_Initialize()
    Dim missionList()   As MISSIONINFO
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    Dim rgbColorBack(4) As Long

    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    '* 初期色を取得
    Call GetRGBColorBackArray(rgbColorBack)
    Label11.BackColor = rgbColorBack(0)
    Label9.BackColor = rgbColorBack(1)
    Label12.BackColor = rgbColorBack(2)
    Label16.BackColor = rgbColorBack(3)
    
    '* ListViewの初期化
    With LV_Element
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add , "_Element", "エレメント名"
        .ColumnHeaders.Item("_Element").Width = 150
    End With
    
    '2017/06/26 No.278 Add.---------Start
    '* ListView-ElementIDの初期化
    With LV_ElementID
        .AllowColumnReorder = True
        .Enabled = False
        .Visible = False
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add , "_Element", "ID"
        .ColumnHeaders.Item("_Element").Width = 150
    End With
    '2017/06/26 No.278 Add.---------End
    
    '* 取得したミッション情報のリストボックスへの出力
    LB_Mission.Clear
    For II = 0 To UBound(missionList)
        LB_Mission.AddItem missionList(II).Name
    Next
    LB_Mission.SetFocus
    LB_Mission.ListIndex = 0        ' リストボックスの最初の項目を選択状態にする
    
End Sub

'***************************************
'* ミッションリストボックス選択時の処理
'***************************************
Private Sub LB_Mission_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer

    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)

    mIndex = -1
    For II = 0 To UBound(missionList)
        If missionList(II).Name = LB_Mission.text Then
            mIndex = II
            Exit For
        End If
    Next

    LB_CTM.Clear
    If mIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM)
            LB_CTM.AddItem missionList(mIndex).CTM(II).Name
        Next
        LB_CTM.SetFocus
        LB_CTM.ListIndex = 0
    End If
End Sub

'***************************************
'* CTMリストボックス選択時の処理
'***************************************
Private Sub LB_CTM_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer
    Dim cIndex  As Integer
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    mIndex = -1
    cIndex = -1
    For II = 0 To UBound(missionList)
        For JJ = 0 To UBound(missionList(II).CTM)
            If missionList(II).CTM(JJ).Name = LB_CTM.text Then
                mIndex = II
                cIndex = JJ
                Exit For
            End If
        Next
        If cIndex > -1 Then Exit For
    Next
    
'    LB_Element.Clear
'    If mIndex > -1 And cIndex > -1 Then
'        For II = 0 To UBound(missionList(mIndex).CTM(cIndex).Element)
'            LB_Element.AddItem missionList(mIndex).CTM(cIndex).Element(II).Name
'        Next
'    End If
    LV_Element.ListItems.Clear
    LV_ElementID.ListItems.Clear '2017/06/26 No.278 Add.
    If mIndex > -1 And cIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM(cIndex).Element)
             LV_Element.ListItems.Add.text = missionList(mIndex).CTM(cIndex).Element(II).Name
             LV_ElementID.ListItems.Add.text = missionList(mIndex).CTM(cIndex).Element(II).ID  '2017/06/26 No.278 Add.
        Next
    End If
End Sub

'2017/06/26 No.278 Add.---------Start
'***************************************
'* エレメントボックス選択時の処理
'***************************************
Private Sub LV_Element_Click()
Dim RowNum As Long

If LV_Element.ListItems.Count > 0 Then
    RowNum = LV_Element.SelectedItem.Index
End If

LV_ElementID.ListItems.Item(RowNum).Selected = True

End Sub
'2017/06/26 No.278 Add.---------End

'**********************
'* ボタン11押下時の処理
'**********************
Private Sub Label11_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColorBack(4)        As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorBackArray(rgbColorBack)
    lcolor = rgbColorBack(0)
    retcolor = Label_Click(Label11, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("E2").Value = retcolor
    End If
    
End Sub

Private Sub Label9_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColorBack(4)        As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorBackArray(rgbColorBack)
    lcolor = rgbColorBack(1)
    retcolor = Label_Click(Label9, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("E3").Value = retcolor
    End If
End Sub

Private Sub Label12_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColorBack(4)        As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorBackArray(rgbColorBack)
    lcolor = rgbColorBack(2)
    retcolor = Label_Click(Label12, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("E4").Value = retcolor
    End If
End Sub

Private Sub Label16_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColorBack(4)        As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorBackArray(rgbColorBack)
    lcolor = rgbColorBack(3)
    retcolor = Label_Click(Label16, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("E5").Value = retcolor
    End If
End Sub

'****************************
'* 表示色ラベルボタン設定処理
'****************************
Public Function Label_Click(LableNm As Object, lcolor As Long) As String
    
    'シートから取得したのデータをRGBに変換する
    b = Int(lcolor / 65536)
    g = Int((lcolor Mod 65536) / 256)
    r = (lcolor Mod 65536) Mod 256
    
    '色を選択したの場合
    If Application.Dialogs(xlDialogEditColor).Show(10, r, g, b) = True Then
        Label_Click = ActiveWorkbook.Colors(10)
        LableNm.BackColor = Label_Click
    Else
        Label_Click = ""
    End If
End Function
