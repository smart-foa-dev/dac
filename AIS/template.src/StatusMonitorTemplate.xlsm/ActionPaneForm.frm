VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ActionPaneForm 
   Caption         =   "表示テキストボックス定義"
   ClientHeight    =   11220
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   7665
   OleObjectBlob   =   "ActionPaneForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "ActionPaneForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


'*************************************
'* 表示色条件テキストボックスをクリア
'*************************************
Private Sub BT_Clear_Click()
    TB_White.text = ""
    TB_Red.text = ""
    TB_Lime.text = ""
    TB_Blue.text = ""
    TB_Yellow.text = ""
    TB_Magenta.text = ""
    TB_Aqua.text = ""
    TB_Maroon.text = ""
    TB_Green.text = ""
    TB_Navy.text = ""
    TB_Olive.text = ""
    TB_Purple.text = ""
    TB_Teal.text = ""
    TB_Silver.text = ""
    TB_Gray.text = ""
End Sub

'*********************
'* 表示色条件取得処理
'*********************
Public Sub GetDispColorCond(dispColorCond() As String)
    dispColorCond(0) = TB_White.text
    dispColorCond(1) = TB_Red.text
    dispColorCond(2) = TB_Lime.text
    dispColorCond(3) = TB_Blue.text
    dispColorCond(4) = TB_Yellow.text
    dispColorCond(5) = TB_Magenta.text
    dispColorCond(6) = TB_Aqua.text
    dispColorCond(7) = TB_Maroon.text
    dispColorCond(8) = TB_Green.text
    dispColorCond(9) = TB_Navy.text
    dispColorCond(10) = TB_Olive.text
    dispColorCond(11) = TB_Purple.text
    dispColorCond(12) = TB_Teal.text
    dispColorCond(13) = TB_Silver.text
    dispColorCond(14) = TB_Gray.text
End Sub

'*********************
'* 条件取得処理
'*********************
Public Sub GetElementList(elementList() As String)

    For i = 1 To LV_ElementView.ListItems.Count Step 1
        elementList(i - 1) = LV_ElementView.ListItems(i).SubItems(1)
    Next i
    
End Sub
'*********************
'* 条件取得処理
'*********************
Public Sub GetElementCondList(elementCondList() As String)

    For i = 1 To LV_ElementView.ListItems.Count Step 1
        elementCondList(i - 1) = LV_ElementView.ListItems(i).SubItems(2)
    Next i
    
End Sub

'***********
'* 削除処理
'***********
Private Sub BT_Delete_Click()
    Dim currShape           As Shape
    Dim dispDefWorksheet    As Worksheet
    Dim statusWorksheet     As Worksheet
    Dim linkWorksheet       As Worksheet
    Dim condWorksheet       As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim srchRange1          As Range
    Dim workRange           As Range
    Dim calcStr             As String
    Dim currShapeName       As String
    Dim workStr             As String
    Dim iRowCnt             As Integer
    Dim II                  As Integer

    '* 全図形情報削除
'    Call DeleteAllShapeAndDispDef

    workStr = "着目図形を削除します。よろしいですか？"
    If MsgBox(workStr, vbYesNo + vbQuestion, "削除確認") = vbNo Then Exit Sub
    
    '* 着目図形を削除
    currShapeName = LBL_ShapeName.Caption
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set currShape = statusWorksheet.Shapes(currShapeName)
    currShape.Delete

    '* 着目画面定義情報を削除
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet

        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.EntireRow.ClearContents
        Else
            Exit Sub
        End If

    End With

    '* 着目条件情報を削除
    Set condWorksheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    iRowCnt = condWorksheet.UsedRange.Rows.Count
    With condWorksheet

        For II = iRowCnt To 1 Step -1
            If .Range("A" & II).Value = currShapeName Then
                
                ' データを削除する
                .Rows(II).Delete
            End If
        Next II

    End With
    
    Unload Me
    
End Sub

'*********************
'* 表示色条件設定処理
'*********************
Private Sub BT_Set_Click()
    Dim shapeName           As String
    Dim srchRange           As Range
    Dim colRange            As Range
    Dim rowRange            As Range
    Dim currRange           As Range
    Dim dispDefWorksheet    As Worksheet
    
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    
    shapeName = LBL_ShapeName.Caption
    
    If shapeName <> "" Then
    
        With dispDefWorksheet
    
            Set srchRange = .Range("A:A")
            Set rowRange = srchRange.Find(What:=shapeName, LookAt:=xlWhole)
            If Not rowRange Is Nothing Then
                Set srchRange = .Range("1:1")
                Set colRange = srchRange.Find(What:="表示色−白", LookAt:=xlWhole)
                If Not colRange Is Nothing Then
                    Set currRange = .Cells(rowRange.Row, colRange.Column)
                    Set srchRange = .Range(currRange.Offset(0, 0), currRange.Offset(0, 14))
                    srchRange.NumberFormatLocal = "@"
                    currRange.Offset(0, 0).Value = TB_White.text
                    currRange.Offset(0, 1).Value = TB_Red.text
                    currRange.Offset(0, 2).Value = TB_Lime.text
                    currRange.Offset(0, 3).Value = TB_Blue.text
                    currRange.Offset(0, 4).Value = TB_Yellow.text
                    currRange.Offset(0, 5).Value = TB_Magenta.text
                    currRange.Offset(0, 6).Value = TB_Aqua.text
                    currRange.Offset(0, 7).Value = TB_Maroon.text
                    currRange.Offset(0, 8).Value = TB_Green.text
                    currRange.Offset(0, 9).Value = TB_Navy.text
                    currRange.Offset(0, 10).Value = TB_Olive.text
                    currRange.Offset(0, 11).Value = TB_Purple.text
                    currRange.Offset(0, 12).Value = TB_Teal.text
                    currRange.Offset(0, 13).Value = TB_Silver.text
                    currRange.Offset(0, 14).Value = TB_Gray.text
                End If
            End If
    
        End With
    
    Else
    End If
    
    Unload Me

End Sub

'*******************
'* 補助式ボタン処理
'*******************
Private Sub BT_Assisted_Click()

    If BT_Assisted.Caption = "＋" Then
        BT_Assisted.Caption = StrMinus
        TB_AssistedArea.Visible = True
    Else
        BT_Assisted.Caption = StrPlus
        TB_AssistedArea.Visible = False
    End If

End Sub

'*******************
'* 閉じるボタン処理
'*******************
Private Sub BT_Close_Click()

    Unload Me

End Sub

'*********************************
'* 表示テキストボックスを生成する
'*********************************
Private Sub BT_CreateTextBox_Click()
    Dim isFormat            As Integer
    Dim dispColorCond(15)   As String
    Dim elementCondList(15) As String
    Dim elementList(15)     As String
    Dim elementListCount    As String

    If TB_ItemOperation.text = "" Then
        MsgBox "演算式が入力されていません。"
        Exit Sub
    End If

    '* 表示色条件を取得する
    Call GetDispColorCond(dispColorCond)
    Call GetElementCondList(elementCondList)
    Call GetElementList(elementList)
    elementListCount = LV_ElementView.ListItems.Count
    
    Me.Hide
    
    '* テキストボックス生成処理
    If OB_Numeric.Value Then isFormat = 0
    If OB_String.Value Then isFormat = 1
    If OB_Date.Value Then isFormat = 2
    '* 2017/02/13 Add ↓↓↓↓↓↓
    If OB_Time.Value Then isFormat = 3
    If OB_DateTime.Value Then isFormat = 4
    '* 2017/02/13 Add ↑↑↑↑↑↑
    Call WriteValueTextBox(TB_ItemOperation.text, isFormat, dispColorCond, _
                   elementCondList, elementList, elementListCount)
    
    TB_ItemOperation.text = ""
    Call BT_Clear_Click

    Me.Show vbModeless

End Sub

'***********
'* 更新処理
'***********
Private Sub BT_Update_Click()
    Dim dispColorCond(15)   As String
    Dim isFormat            As Integer
    Dim elementCondList(15) As String
    Dim elementList(15)     As String
    Dim elementListCount    As String
    
    If TB_ItemOperation.text = "" Then
        MsgBox "演算式が入力されていません。"
        Exit Sub
    End If

    '* 表示色条件を取得する
    Call GetDispColorCond(dispColorCond)
    Call GetElementCondList(elementCondList)
    Call GetElementList(elementList)
    elementListCount = LV_ElementView.ListItems.Count
    
    '* テキストボックス更新処理
    If OB_Numeric.Value Then isFormat = 0
    If OB_String.Value Then isFormat = 1
    If OB_Date.Value Then isFormat = 2
    If OB_Time.Value Then isFormat = 3
    If OB_DateTime.Value Then isFormat = 4
    Call UpdateValueTextBox(LBL_ShapeName, TB_ItemOperation.text, isFormat, dispColorCond, elementCondList, elementList, elementListCount)
    
    Unload Me
    
End Sub

'***********************
'* 表示エレメントの追加
'***********************
Private Sub BT_add_Click()

    Dim sValue              As String
    Dim isConditionText     As String
    Dim sCtmValue           As String
        
    '* テキストボックス更新処理
    If TB_CalcCondition.text = "" Then
        MsgBox "演算式条件が入力されていません。"
        Exit Sub
    Else
        isConditionText = TB_CalcCondition.text
    End If
    
    '* エレメントを追加する
    For i = LV_Element.ListItems.Count To 1 Step -1
        If LV_Element.ListItems(i).Selected Then
        
            sValue = LV_Element.ListItems(i).text
            
            '* ISSUE_NO.671 sunyi 2018/05/25 start
            '* 仕様変更
            For l = LV_ElementView.ListItems.Count To 1 Step -1
                If LV_ElementView.ListItems(l).ListSubItems.Item(1).text = sValue Then
                    LV_ElementView.ListItems(l).ListSubItems.Item(1).text = sValue
                    LV_ElementView.ListItems(l).ListSubItems.Item(2).text = isConditionText
                    LV_ElementView.ListItems(l).ListSubItems.Item(3).text = LB_CTM.text
                    Exit Sub
                End If
            Next l
            '* ISSUE_NO.671 sunyi 2018/05/25 end

            '* 個数(単一条件)時、選択の項目で前のデータを替換え
            If CB_CalcFormat.text = "個数(単一条件)" Then
                For J = LV_ElementView.ListItems.Count To 1 Step -1
                    LV_ElementView.ListItems.Remove J
                Next J
            End If

            Set itm = LV_ElementView.ListItems.Add()
            itm.text = LV_ElementView.ListItems.Count()
            itm.SubItems(1) = sValue
            itm.SubItems(2) = isConditionText
            itm.SubItems(3) = LB_CTM.text
            Exit For
        End If
    Next i
    
    '* ISSUE_NO.666 sunyi 2018/05/10 start
    '* 吹き出し追加
    LV_ElementView.ControlTipText = itm.SubItems(1) & ":" & itm.SubItems(2)
    '* ISSUE_NO.666 sunyi 2018/05/10 end

End Sub

'***********************
'* 表示エレメントの行削除
'***********************
Private Sub BT_rowdelete_Click()

    For i = LV_ElementView.ListItems.Count To 1 Step -1
        If LV_ElementView.ListItems(i).Selected Then
            LV_ElementView.ListItems.Remove i
            Exit For
        End If
    Next i
    
    For i = LV_ElementView.ListItems.Count To 1 Step -1
        LV_ElementView.ListItems(i).text = i
    Next i
    '* ISSUE_NO.666 sunyi 2018/05/29 start
    '* 吹き出し追加
    LV_ElementView.ControlTipText = ""
    If LV_ElementView.ListItems.Count > 0 Then
        LV_ElementView.ControlTipText = LV_ElementView.ListItems.Item(1).SubItems(1) & ":" & LV_ElementView.ListItems.Item(1).SubItems(2)
    End If
    '* ISSUE_NO.666 sunyi 2018/05/29 end
End Sub

'**********************
'* エレメント条件式修正
'**********************
Private Sub LV_ElementView_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim getConditionNameStr      As Variant

    If LV_ElementView.SelectedItem <> 0 Then
        TB_CalcCondition.text = LV_ElementView.ListItems(LV_ElementView.SelectedItem.Index).SubItems(2)
                        
    End If
End Sub

'***********************
'* エレメント条件式の更新
'***********************
Private Sub BT_ConditionUpdate_Click()
    For i = LV_ElementView.ListItems.Count To 1 Step -1
        If LV_ElementView.ListItems(i).Selected Then
    LV_ElementView.ListItems(i).SubItems(2) = TB_CalcCondition.text
            Exit For
        End If
    Next i
    '* ISSUE_NO.666 sunyi 2018/05/29 start
    '* 吹き出し追加
    LV_ElementView.ControlTipText = ""
    If LV_ElementView.ListItems.Count > 0 Then
        LV_ElementView.ControlTipText = LV_ElementView.SelectedItem.SubItems(1) & ":" & LV_ElementView.SelectedItem.SubItems(2)
    End If
    '* ISSUE_NO.666 sunyi 2018/05/29 end
End Sub


'***********************
'* ドラッグ完了時の処理
'***********************
Private Sub LV_Element_OLECompleteDrag(Effect As Long)
    Debug.Print "OLECompleteDrag"
    Effect = vbDropEffectCopy
End Sub

'*****************************************************
'* リストボックスからテキストボックスへのドラッグ処理
'*****************************************************
Private Sub LV_Element_OLEStartDrag(Data As MSComctlLib.DataObject, AllowedEffects As Long)
    Dim workStr         As Variant
    Dim getTextStr      As Variant
    Debug.Print "OLEStartDrag"
    
    '* データオブジェクトの内容をクリア
    Data.Clear
    
    '* ISSUE_NO.671 sunyi 2018/05/28 start
    '* 仕様変更
    If CB_CalcFormat.text = "個数(単一条件)" Or CB_CalcFormat.text = "個数(複数条件)" Then
        Exit Sub
    End If
    '* ISSUE_NO.671 sunyi 2018/05/28 end
    
    '* ドラッグ開始時のリストボックスの選択項目を取得
    getTextStr = LV_Element.SelectedItem.text
    
    '* 貼り付ける文字列の生成
    If CB_CalcFormat.text = "" Then
        workStr = """" & LB_CTM.text & "・" & getTextStr & """"
        
        ''''workStr = """" & LB_CTM.text & "_" & getTextStr & """"
    Else
        workStr = """" & LB_CTM.text & "・" & getTextStr & "," & CB_CalcFormat.text & """"
        
        ''''workStr = """" & LB_CTM.text & "_" & getTextStr & "," & CB_CalcFormat.text & """"
    End If
    
    '* データオブジェクトにセット
    Data.SetData workStr, vbCFtext
    
End Sub

'********************************
'* 演算式条件ドラッグ完了時の処理
'********************************
Private Sub LV_ElementView_OLECompleteDrag(Effect As Long)
    Debug.Print "OLECompleteDrag"
    Effect = vbDropEffectCopy
End Sub

'**************************************************************
'* 演算式条件リストボックスからテキストボックスへのドラッグ処理
'**************************************************************
Private Sub LV_ElementView_OLEStartDrag(Data As MSComctlLib.DataObject, AllowedEffects As Long)
    Dim workView            As Variant
    Dim workViewStr         As Variant
'    Dim getViewNameStr      As Variant
    Debug.Print "OLEStartDrag"
    
    '* データオブジェクトの内容をクリア
    Data.Clear
    
'    '* ドラッグ開始時のリストボックスの選択項目を取得
'    getViewNameStr = LV_ElementView.SelectedItem.SubItems(1)
    
    '* 貼り付ける文字列の生成
    If TB_CalcCondition.text = "" Then
        MsgBox "演算式条件が入力されていません。"
        Exit Sub
    End If

    For i = LV_ElementView.ListItems.Count To 1 Step -1
        If workView = "" Then
            workView = LV_ElementView.ListItems(i).SubItems(1) & ":" & LV_ElementView.ListItems(i).SubItems(2)
        Else
            workView = workView & ";" & LV_ElementView.ListItems(i).SubItems(1) & ":" & LV_ElementView.ListItems(i).SubItems(2)
        End If
    Next i
    '* 製品1ライン状態・個数(固定値),個数(XX条件)[エレメント名:条件;エレメント名2:条件2]
    workViewStr = """" & LB_CTM.text & "・" & "個数" & "," & CB_CalcFormat.text & "[" & workView & "]" & """"
    
    '* データオブジェクトにセット
    Data.SetData workViewStr, vbCFtext

End Sub

'********************
'* 演算形式リスト変更
'********************
Private Sub CB_CalcFormat_Change()
    
    If CB_CalcFormat.text = "個数(単一条件)" Then

        Call SetDisplayStatus("1")
        
        '* 複数条件→単一条件に変更時、一番上のデータのみを保留
        For i = LV_ElementView.ListItems.Count To 2 Step -1
            LV_ElementView.ListItems.Remove i
        Next i
    ElseIf CB_CalcFormat.text = "個数(複数条件)" Then

        Call SetDisplayStatus("1")
    Else
    
            Call SetDisplayStatus("0")
    End If
    
End Sub

'****************************************************
'* 演算形式が個数(単一条件)と個数(複数条件)の画面設定
'****************************************************
Private Sub SetDisplayStatus(sFlg As String)

    If sFlg = "1" Then
        
        '* 「エレメント追加」ボタンを表示
        BT_add.Visible = True
        
        '* 「エレメント削除」ボタンを表示
        BT_rowdelete.Visible = True
        
        '* 「条件更新」ボタンを表示
        BT_ConditionUpdate.Visible = True
        
        '* 「条件」テキストボックスを表示
        TB_CalcCondition.Visible = True
        
        '* 「条件」ラベルを表示
        Label23.Visible = True
        
        '* 「エレメント」リストビューを表示
        LV_ElementView.Visible = True
        
        '*ISSUE_NO.671 sunyi 2018/05/29 start
        '*仕様変更
        LV_ElementView.Top = 312
        LV_ElementView.Left = 210
        '*ISSUE_NO.671 sunyi 2018/05/29 end
        
        '* リストビューのへーだー「エレメント」ラベルを表示
        Label25.Visible = True
        
        '* リストビューのへーだー「条件」ラベルを表示
        Label24.Visible = True
    Else
        
        '* 「エレメント追加」ボタンを非表示
        BT_add.Visible = False
        
        '* 「エレメント削除」ボタンを非表示
        BT_rowdelete.Visible = False
        
        '* 「条件更新」ボタンを非表示
        BT_ConditionUpdate.Visible = False
        
        '* 「条件」テキストボックスを非表示
        TB_CalcCondition.Visible = False
        
        '* 「条件」ラベルを非表示
        Label23.Visible = False
        
        '* 「エレメント」リストビューを非表示
        LV_ElementView.Visible = False
        
        '* リストビューのへーだー「エレメント」ラベルを非表示
        Label25.Visible = False
        
        '* リストビューのへーだー「条件」ラベルを非表示
        Label24.Visible = False
    End If
        
End Sub

'* ISSUE_NO.666 sunyi 2018/05/10 start
'* 吹き出し追加
Private Sub TB_CalcCondition_Change()
    TB_CalcCondition.ControlTipText = TB_CalcCondition.text
End Sub

Private Sub TB_ItemOperation_Change()
    TB_ItemOperation.ControlTipText = TB_ItemOperation.text
End Sub

Private Sub LV_ElementView_Click()
    If LV_ElementView.ListItems.Count > 0 Then
        LV_ElementView.ControlTipText = LV_ElementView.SelectedItem.SubItems(1) & ":" & LV_ElementView.SelectedItem.SubItems(2)
    End If
End Sub

Private Sub LV_Element_Click()
    If LV_Element.ListItems.Count > 0 Then
        LV_Element.ControlTipText = LV_Element.SelectedItem.text
    End If
End Sub
'* ISSUE_NO.666 sunyi 2018/05/10 end

'*****************
'* フォーム初期化
'*****************
Private Sub UserForm_Initialize()
    Dim missionList()   As MISSIONINFO
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    Dim rgbColor(15)    As Long
    
    '* 補助式展開のボタン初期表示は"＋"
    BT_Assisted.Caption = StrPlus
    
    '* 補助式エリアは非表示
    TB_AssistedArea.Visible = False
    
    '* 演算形式の初期化
    CB_CalcFormat.Clear
    CB_CalcFormat.AddItem "個数"
    CB_CalcFormat.AddItem "積算"
    CB_CalcFormat.AddItem "平均"
    CB_CalcFormat.AddItem "最大"
    CB_CalcFormat.AddItem "最小"
    CB_CalcFormat.AddItem "種類数"
    CB_CalcFormat.AddItem "個数(単一条件)"
    CB_CalcFormat.AddItem "個数(複数条件)"
    
    '* 初期色を取得
    Call GetRGBColorArray(rgbColor)
    Label11.BackColor = rgbColor(0)
    Label9.BackColor = rgbColor(1)
    Label8.BackColor = rgbColor(2)
    Label10.BackColor = rgbColor(3)
    Label12.BackColor = rgbColor(4)
    Label13.BackColor = rgbColor(5)
    Label14.BackColor = rgbColor(6)
    Label15.BackColor = rgbColor(7)
    Label16.BackColor = rgbColor(8)
    Label17.BackColor = rgbColor(9)
    Label18.BackColor = rgbColor(10)
    Label19.BackColor = rgbColor(11)
    Label20.BackColor = rgbColor(12)
    Label21.BackColor = rgbColor(13)
    Label22.BackColor = rgbColor(14)
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    With LV_Element
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add , "_Element", "エレメント名"
        .ColumnHeaders.Item("_Element").Width = 150
    End With
    
    With LV_ElementView
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add 1, "_Id", "ID", 15
        .ColumnHeaders.Add 2, "_Element", "エレメント", 78
        .ColumnHeaders.Add 3, "_DisplayType", "条件", 34
        .ColumnHeaders.Add 4, "_Ctm", "", 0
        
    End With
    
    '* 取得したミッション情報のリストボックスへの出力
    LB_Mission.Clear
    For II = 0 To UBound(missionList)
        LB_Mission.AddItem missionList(II).Name
    Next
    LB_Mission.SetFocus
    LB_Mission.ListIndex = 0        ' リストボックスの最初の項目を選択状態にする
    
    '* 書式は数値がデフォルト
    OB_Numeric.Value = True
    
    '* ISSUE_NO.666 sunyi 2018/05/10 start
    '* 吹き出し追加
    LV_Element.ControlTipText = LV_Element.ListItems.Item(1).text
    '* ISSUE_NO.666 sunyi 2018/05/10 end
    
'    LB_CTM.Clear
'    For II = 0 To UBound(missionList(0).CTM)
'        LB_CTM.AddItem missionList(0).CTM(II).Name
'    Next
'    LB_CTM.ListIndex = 0            ' リストボックスの最初の項目を選択状態にする
'
'    For II = 0 To UBound(missionList(0).CTM(0).Element)
'        LB_Element.AddItem missionList(0).CTM(0).Element(II).Name
'    Next
    
'    LB_Mission.ListIndex = 0        ' リストボックスの最初の項目を選択状態にする
End Sub

'***************************************
'* ミッションリストボックス選択時の処理
'***************************************
Private Sub LB_Mission_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer

    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)

    mIndex = -1
    For II = 0 To UBound(missionList)
        If missionList(II).Name = LB_Mission.text Then
            mIndex = II
            Exit For
        End If
    Next

    LB_CTM.Clear
    If mIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM)
            LB_CTM.AddItem missionList(mIndex).CTM(II).Name
        Next
        LB_CTM.SetFocus
        LB_CTM.ListIndex = 0
    End If
    
    '* ISSUE_NO.666 sunyi 2018/05/10 start
    '* 吹き出し追加
    LB_Mission.ControlTipText = LB_Mission.text
    '* ISSUE_NO.666 sunyi 2018/05/10 end
    
End Sub

'***************************************
'* CTMリストボックス選択時の処理
'***************************************
Private Sub LB_CTM_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer
    Dim cIndex  As Integer
    
    '*ISSUE_NO.671 sunyi 2018/05/29 start
    '*仕様変更
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim workStr         As String
    '*
    Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="操作バルキ表示Flag", LookAt:=xlWhole)
    If LV_ElementView.ListItems.Count > 0 Then
        If srchRange.Offset(0, 1).Value = "False" Then
            LV_ElementView.ListItems.Clear
            TB_CalcCondition.text = ""
        End If
    End If
    srchRange.Offset(0, 1).Value = "False"
    '*ISSUE_NO.671 sunyi 2018/05/29 end
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    mIndex = -1
    cIndex = -1
    For II = 0 To UBound(missionList)
        For JJ = 0 To UBound(missionList(II).CTM)
            If missionList(II).CTM(JJ).Name = LB_CTM.text Then
                mIndex = II
                cIndex = JJ
                Exit For
            End If
        Next
        If cIndex > -1 Then Exit For
    Next
    
'    LB_Element.Clear
'    If mIndex > -1 And cIndex > -1 Then
'        For II = 0 To UBound(missionList(mIndex).CTM(cIndex).Element)
'            LB_Element.AddItem missionList(mIndex).CTM(cIndex).Element(II).Name
'        Next
'    End If
    LV_Element.ListItems.Clear
    If mIndex > -1 And cIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM(cIndex).Element)
            LV_Element.ListItems.Add.text = missionList(mIndex).CTM(cIndex).Element(II).Name
        Next
    End If
    
    '* ISSUE_NO.666 sunyi 2018/05/10 start
    '* 吹き出し追加
    LB_CTM.ControlTipText = LB_CTM.text
    '* ISSUE_NO.666 sunyi 2018/05/10 end
    
End Sub

'**********************
'* ボタン11押下時の処理
'**********************
Private Sub Label11_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(0)
    retcolor = Label_Click(Label11, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B2").Value = retcolor
    End If
    
End Sub

Private Sub Label9_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(1)
    retcolor = Label_Click(Label9, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B3").Value = retcolor
    End If
End Sub

Private Sub Label8_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(2)
    retcolor = Label_Click(Label8, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B4").Value = retcolor
    End If
End Sub

Private Sub Label10_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(3)
    retcolor = Label_Click(Label10, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B5").Value = retcolor
    End If
End Sub

Private Sub Label12_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(4)
    retcolor = Label_Click(Label12, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B6").Value = retcolor
    End If
End Sub

Private Sub Label13_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(5)
    retcolor = Label_Click(Label13, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B7").Value = retcolor
    End If
End Sub

Private Sub Label14_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(6)
    retcolor = Label_Click(Label14, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B8").Value = retcolor
    End If
End Sub

Private Sub Label15_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(7)
    retcolor = Label_Click(Label15, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B9").Value = retcolor
    End If
End Sub

Private Sub Label16_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(8)
    retcolor = Label_Click(Label16, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B10").Value = retcolor
    End If
End Sub

Private Sub Label17_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(9)
    retcolor = Label_Click(Label17, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B11").Value = retcolor
    End If
End Sub

Private Sub Label18_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(10)
    retcolor = Label_Click(Label18, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B12").Value = retcolor
    End If
End Sub

Private Sub Label19_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
 
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(11)
    retcolor = Label_Click(Label19, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B13").Value = retcolor
    End If
End Sub

Private Sub Label20_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(12)
    retcolor = Label_Click(Label20, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B14").Value = retcolor
    End If

End Sub

Private Sub Label21_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)

    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(13)
    retcolor = Label_Click(Label21, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B15").Value = retcolor
    End If

End Sub

Private Sub Label22_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)

    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(14)
    retcolor = Label_Click(Label22, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B16").Value = retcolor
    End If
End Sub

'****************************
'* 表示色ラベルボタン設定処理
'****************************
Public Function Label_Click(LableNm As Object, lcolor As Long) As String
    
    'シートから取得したのデータをRGBに変換する
    b = Int(lcolor / 65536)
    g = Int((lcolor Mod 65536) / 256)
    r = (lcolor Mod 65536) Mod 256
    
    '色を選択したの場合
    If Application.Dialogs(xlDialogEditColor).Show(10, r, g, b) = True Then
        Label_Click = ActiveWorkbook.Colors(10)
        LableNm.BackColor = Label_Click
    Else
        Label_Click = ""
    End If
End Function



