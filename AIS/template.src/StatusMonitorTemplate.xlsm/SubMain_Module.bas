Attribute VB_Name = "SubMain_Module"
Option Explicit

'*************************************************
'* 画面の情報を元に入力テキストボックスを生成する
'*************************************************
Public Sub WriteValueTextBox(calcString As String, isFormat As Integer, dispColorCond() As String, _
                elementCondList() As String, elementList() As String, elementListCount As String)
    Dim MyKeyState          As Long
    Dim nowShapeNum         As Integer
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim condParamsheet      As Worksheet
    Dim srchRange           As Range
    Dim srchNmRange         As Range
    Dim i                   As Integer
    Dim currShape           As Shape
    Dim currShapeName       As String
    Dim currShapeNum        As Integer
    Dim II                  As Integer
    Dim startRange          As Range
    Dim workRange           As Range
    Dim dispRange           As Range
    Dim itemOpeString       As String
    Dim idArray(100)        As Boolean
    Dim workStr             As String
    Dim splitStr            As Variant 'No.555 Add.
    Dim isCalcString        As Boolean 'No.555 Add.
    Dim iLineWeight         As Integer
    Dim iLineStyle          As Integer
    Dim iEndRow             As Integer
       
    '2017/09/26 No.533 Add.
    Call SetFilenameToVariable
    
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    
    '* 枠線に合わせる、をONにする
'    Application.CommandBars.FindControl(ID:=549).Execute
    
    workStr = "値を表示するテキストボックスを作成" & vbCrLf & vbCrLf & _
                "※セル枠線に合わせる場合はALTキーを押しながらドラッグ"
    
    If MsgBox(workStr, vbYesNo + vbInformation) = vbYes Then
        Application.CommandBars.FindControl(ID:=1111).Execute
    Else
        Exit Sub
    End If
    
'    Call Sleep(200)
    
    With statusWorksheet
    
        '* 現在の図形数を取得する
        nowShapeNum = .Shapes.Count
        
        '* 図形が増えるまで待機
        Do
            DoEvents
        Loop Until nowShapeNum < .Shapes.Count
        
        '* 現在の該当図形数を取得
        For II = 0 To 99
            idArray(II) = True
        Next
        nowShapeNum = .Shapes.Count
        currShapeNum = 0
        For II = 1 To nowShapeNum
            If Left(.Shapes(II).Name, 6) = "value_" Then
                idArray(CInt(Right(.Shapes(II).Name, 2))) = False
                currShapeNum = currShapeNum + 1
            End If
        Next
        For II = 1 To 99
            If idArray(II) Then Exit For
        Next
        If II > 99 Then
            currShapeNum = 99
        Else
            currShapeNum = II
        End If
        
        '* 図形の属性を設定
        Set currShape = .Shapes(nowShapeNum)
        currShapeName = "value_shape_" & Format(currShapeNum, "00")
        
        With currShape
            .Name = currShapeName
            .OnAction = "ConditionOpen"
            With .TextFrame2
                '* 文字列配置中央
                .VerticalAnchor = msoAnchorMiddle
                With .TextRange
                    .ParagraphFormat.Alignment = msoAlignCenter
                    .Font.Bold = msoTrue
                    .Font.Size = 16
                    .Font.Fill.ForeColor.RGB = RGB(0, 0, 0)
                End With
            End With
            .Line.ForeColor.RGB = RGB(0, 0, 0)
            .Line.Weight = 1.5
            .Fill.ForeColor.RGB = RGB(255, 255, 255)
        End With
        
    End With
    
    '*****************************************
    '* 演算式と図形情報を画面定義シートへ記載
    '*****************************************
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet
    
        '* 記載開始範囲を設定
        Set startRange = .Range("A2")
        
        '* 空行を探す
        For II = 0 To 100
            Set workRange = startRange.Offset(II, 0)
            If workRange.Value = "" Then Exit For
        Next
        
        '* 書き出し場所がなければ処理中断
        If II > 100 Then
            currShape.Delete
            Exit Sub
        End If
        
        workRange.Offset(0, 0).Value = currShapeName
        workRange.Offset(0, 1).Value = calcString
        workRange.Offset(0, 3).Value = isFormat
        
        '* 表示値の数式を生成する
        itemOpeString = GetItemOperation(calcString)
        
        '-2017/05/08 No.555 Add.
        '* 演算形式を判定する
        isCalcString = False
        If InStr(workRange.Offset(0, 1).Value, ",") > 0 Then
            splitStr = Split(workRange.Offset(0, 1).Value, ",")
            If InStr(splitStr(1), "種類数") Then
              isCalcString = True
            End If
        End If

        With workRange.Offset(0, 2)
        
            '* 書式を設定する
            .NumberFormatLocal = "G/標準"
    
            '* 数式を代入する
            .FormulaLocal = itemOpeString
            
            If IsDate(workRange.Offset(0, 2).Value) Then
                .NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
            ElseIf IsNumeric(.Value) Then
               '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
               'If .Value = CLng(.Value) Then '20170307 No.473 Changed CInt to CLng.
                '* ISSUE_NO.670 sunyi 2018/05/15 start
                '* オーバーフローを修正
                'If .Value = CLng(.Value) Or isCalcString Then
                If .Value = CDbl(.Value) Or isCalcString Then
                '* ISSUE_NO.670 sunyi 2018/05/15 end
                    '-2017/05/08 No.554 Mod. Changed # to 0.
                    .NumberFormatLocal = "0"
                Else
                    .NumberFormatLocal = "0.00"
                End If
            End If
        End With
        
        Set dispRange = .Range(workRange.Offset(0, 5), workRange.Offset(0, 20))
        dispRange.NumberFormatLocal = "@"
        dispRange = dispColorCond
    
    End With
    
    '*****************************************
    '* 条件を条件画面定義シートへ記載
    '*****************************************
    Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    With condParamsheet
        Set srchRange = .Range("A:A")
        Set srchNmRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
        iEndRow = condParamsheet.UsedRange.Rows.Count
        
        If Not srchNmRange Is Nothing Then
        
            If srchNmRange.Offset(iEndRow, 0).Value = "" Then
                
                For i = 1 To elementListCount Step 1
                    srchNmRange.Offset(iEndRow + i - 1, 0).Value = currShapeName
                    srchNmRange.Offset(iEndRow + i - 1, 1).Value = elementList(i - 1)
                    srchNmRange.Offset(iEndRow + i - 1, 2).Value = Chr(39) & elementCondList(i - 1)
                Next i
'                    Exit For
            End If
        
        End If
    End With
    On Error Resume Next
    Select Case isFormat
        Case 0
            With workRange.Offset(0, 2)
                '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                '* ISSUE_NO.670 sunyi 2018/05/15 start
                '* オーバーフローを修正
                'If .Value = CLng(.Value) Or isCalcString Then
                If .Value = CDbl(.Value) Or isCalcString Then
                '* ISSUE_NO.670 sunyi 2018/05/15 end
                    '-2017/05/08 No.554 Mod. Changed # to 0.
                    currShape.TextFrame.Characters.text = Format(.Value, "0")
                Else
                    currShape.TextFrame.Characters.text = Format(.Value, "0.00")
                End If
            End With
        Case 1
'            '* ISSUE_NO.759 Sunyi 2018/06/25 Start
'            '* 空欄の場合、””に表示する
''            currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
'            If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
'                workRange.Offset(0, 2).Value = ""
'            Else
'                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
'            End If
'            '* ISSUE_NO.759 Sunyi 2018/06/25 End
            'ISSUE_NO.802 sunyi 2018/07/26 Start
            '文字列の場合で、空をそのまま出力する
            currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
            'ISSUE_NO.802 sunyi 2018/07/26 End
        Case 2
            '20161104 追加
            If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                workRange.Offset(0, 2).Value = ""
            Else
                '2017/09/26 No.539 Mod. Changed "yyyy/MM/dd hh:mm:ss" to "yyyy/MM/dd".
                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd")
            End If
        '2017/09/26 No.539 Add. ------- Start
        Case 3
            If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                workRange.Offset(0, 2).Value = ""
            Else
                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "hh:mm:ss")
            End If
        Case 4
            If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                workRange.Offset(0, 2).Value = ""
            Else
                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd hh:mm:ss")
            End If
        '2017/09/26 No.539 Add. ------- End
    End Select
    
End Sub

'*************************************************
'* 画面の情報を元に入力テキストボックスを更新する
'*************************************************
Public Sub UpdateValueTextBox(currShapeName As String, calcString As String, isFormat As Integer, dispColorCond() As String, _
                elementCondList() As String, elementList() As String, elementListCount As String)
    Dim MyKeyState          As Long
    Dim nowShapeNum         As Integer
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim condParamsheet      As Worksheet
    Dim scondRange          As Range
    Dim scondNmRange        As Range
    Dim dispCondRange       As Range
    Dim i                   As Integer
    Dim currShape           As Shape
    Dim currShapeNum        As Integer
    Dim II                  As Integer
    Dim srchRange           As Range
    Dim workRange           As Range
    Dim dispRange           As Range
    Dim itemOpeString       As String
    Dim idArray(100)        As Boolean
    Dim workStr             As String
    Dim splitStr            As Variant 'No.555 Add.
    Dim isCalcString        As Boolean 'No.555 Add.
    Dim iEndRow             As Integer
    
    On Error GoTo errlable
    '2017/09/26 No.533 Add.
    Call SetFilenameToVariable
    
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    
    Set currShape = statusWorksheet.Shapes(currShapeName)
    
    '*****************************************
    '* 演算式と図形情報を画面定義シートへ記載
    '*****************************************
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet
    
        '* 記載開始範囲を設定
        Set srchRange = .Range("A:A")
        Set workRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
        If workRange Is Nothing Then Exit Sub
        
        workRange.Offset(0, 1).Value = calcString
        workRange.Offset(0, 3).Value = isFormat
        
        '* 表示値の数式を生成する
        itemOpeString = GetItemOperation(calcString)
        
        '-2017/05/08 No.555 Add.
        '* 演算形式を判定する
        isCalcString = False
        If InStr(workRange.Offset(0, 1).Value, ",") > 0 Then
            splitStr = Split(workRange.Offset(0, 1).Value, ",")
            If InStr(splitStr(1), "種類数") Then
              isCalcString = True
            End If
        End If
        
        With workRange.Offset(0, 2)

            '* 書式を設定する
            .NumberFormatLocal = "G/標準"
    
            '* 数式を代入する
            .FormulaLocal = itemOpeString
        
            If IsDate(.Value) Then
                .NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
            ElseIf IsNumeric(.Value) Then
                '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                '* ISSUE_NO.670 sunyi 2018/05/15 start
                '* オーバーフローを修正
                'If .Value = CLng(.Value) Or isCalcString Then
                If .Value = CDbl(.Value) Or isCalcString Then
                '* ISSUE_NO.670 sunyi 2018/05/15 end
                    '-2017/05/08 No.554 Mod. Changed # to 0.
                    .NumberFormatLocal = "0"
                Else
                    .NumberFormatLocal = "0.00"
                End If
            End If
        End With
        
        Set dispRange = .Range(workRange.Offset(0, 5), workRange.Offset(0, 20))
        dispRange.NumberFormatLocal = "@"
        dispRange = dispColorCond
    
    End With
    
    '*****************************************
    '* 条件を条件画面定義シートへ記載
    '*****************************************
    Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    With condParamsheet
        Set scondRange = .Range("A:A")
        Set scondNmRange = scondRange.Find(What:="図形名", LookAt:=xlWhole)
        iEndRow = condParamsheet.UsedRange.Rows.Count
        
        If Not scondNmRange Is Nothing Then

            '* 更新の場合
            For II = 1 To iEndRow
                Set dispCondRange = scondRange.Find(What:=currShapeName, LookAt:=xlWhole)
            
                If Not dispCondRange Is Nothing Then
                    ' データを削除する
                    .Rows(dispCondRange.Row).Delete
                Else
                    Exit For
                End If
            Next
        
            '* 全図形に対して
            iEndRow = condParamsheet.UsedRange.Rows.Count
            If scondNmRange.Offset(iEndRow, 0).Value = "" Then
                
                For i = 1 To elementListCount Step 1
                    scondNmRange.Offset(iEndRow + i - 1, 0).Value = currShapeName
                    scondNmRange.Offset(iEndRow + i - 1, 1).Value = elementList(i - 1)
                    scondNmRange.Offset(iEndRow + i - 1, 2).Value = Chr(39) & elementCondList(i - 1)
                Next i
            End If
        
        End If
    End With
    On Error Resume Next
    Select Case isFormat
        Case 0
            With workRange.Offset(0, 2)
                '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                '* ISSUE_NO.670 sunyi 2018/05/15 start
                '* オーバーフローを修正
                'If .Value = CLng(.Value) Or isCalcString Then
                If .Value = CDbl(.Value) Or isCalcString Then
                '* ISSUE_NO.670 sunyi 2018/05/15 end
                    '-2017/05/08 No.554 Mod. Changed # to 0.
                    currShape.TextFrame.Characters.text = Format(.Value, "0")
                Else
                    currShape.TextFrame.Characters.text = Format(.Value, "0.00")
                End If
            End With
        Case 1
'            '* ISSUE_NO.759 Sunyi 2018/06/25 Start
'            '* 空欄の場合、””に表示する
''            currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
'            If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
'                workRange.Offset(0, 2).Value = ""
'            Else
'                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
'            End If
'            '* ISSUE_NO.759 Sunyi 2018/06/25 End
            'ISSUE_NO.802 sunyi 2018/07/26 Start
            '文字列の場合で、空をそのまま出力する
            currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
            'ISSUE_NO.802 sunyi 2018/07/26 End
        Case 2
            '20161104 追加
            If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                workRange.Offset(0, 2).Value = ""
            Else
                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd")
            End If
        Case 3
            '20170213 追加
            If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                workRange.Offset(0, 2).Value = ""
            Else
                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "hh:mm:ss")
            End If
        Case 4
            '20170213 追加
            If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                workRange.Offset(0, 2).Value = ""
            Else
                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd hh:mm:ss")
            End If
    End Select
    
errlable:
    If Err.Number = 1 Then
        MsgBox "該当ボックスが削除されました。"
    End If

    '* 枠線に合わせる、をOFFにする
'    Application.CommandBars.FindControl(ID:=549).Execute
    
'    MsgBox "テキストボックスを作成しました。"

End Sub

Public Sub CalcStrTest()
    Call GetItemOperation("""部品2生産実績_部品セリアルＮｏ,個数"" + ""部品2生産実績_部品セリアルＮｏ"" + 10")
End Sub

'*********************************************************
'* 演算式よりセルに埋め込む計算式を生成する　para:定義内容
'*********************************************************
Public Function GetItemOperation(calcStringOrg As String) As String
    Dim calcType        As Variant
    Dim startPos        As Integer
    Dim endPos          As Integer
    Dim splitStr1       As Variant
    Dim splitStr2       As Variant
    Dim splitStr3       As Variant
    Dim splitStr4       As Variant
    Dim splitStr5       As Variant
    Dim splitStr6       As Variant
    Dim currType        As Variant
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim workRange       As Range
    Dim workStr         As String
    Dim workStrCnt      As String
    Dim prefixStr       As String
    Dim workAddr        As String
    Dim dblQuota()      As Integer
    Dim II              As Integer
    Dim JJ              As Integer
    Dim calcStrArray    As Variant
    Dim calcString      As String
    Dim workCond        As Variant
    
    ReDim dblQuota(0)
    dblQuota(0) = -1
    
    GetItemOperation = ""
    
    '* COUNTA/SUM/AVERAGE/MAX/MIN/SUMPRODUCT
    calcType = Array("COUNTA(", "SUM(", "AVERAGE(", "MAX(", "MIN(", "SUMPRODUCT(1/COUNTIF(", "COUNTIF(", "COUNTIFS(")
    
    '* ダブルクォーテーションで囲まれた部分を抽出する
    '* →取得配列の奇数インデックスに入る
    calcStrArray = Split(calcStringOrg, """") '定義内容取込
    
    For II = 1 To UBound(calcStrArray) Step 2
    
        calcString = calcStrArray(II)  '定義内容名
    
        '* 演算式のチェック
        If InStr(calcString, "・") = 0 Then Exit Function
        
        ''''If InStr(calcString, "_") = 0 Then Exit Function
        
        '* カンマがない場合は、取得CTMの最新の値のみを対象とする
        If InStr(calcString, ",") = 0 Then
            
            '* 演算式から " で囲まれた文字列を取得する
            '* "部品1生産実績_部品セリアルＮｏ,個数" / 100 ⇒「=SUM(XX:YY) / 100」
            splitStr2 = Split(calcString, "・")    ' 「部品1生産実績」「部品セリアルＮｏ」
            
            ''''splitStr2 = Split(calcString, "_")    ' 「部品1生産実績」「部品セリアルＮｏ」
            
            'CTMシートから値の取込
            Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr2(0))
            With currWorksheet
                Set srchRange = .Range("1:1")
                ' Wang Issue NO.727 2018/06/15 Start
                If IsGripType Then
                    Set startRange = srchRange.Find(What:=splitStr2(2), LookAt:=xlWhole)
                Else
                    Set startRange = srchRange.Find(What:=splitStr2(1), LookAt:=xlWhole)
                End If
                ' Wang Issue NO.727 2018/06/15 End
                If Not startRange Is Nothing Then
                    Set workRange = startRange.Offset(1, 0)             '* 着目エレメントの最新データ範囲
                    
                    'ISSUE_NO.802 sunyi 2018/07/26 Start
                    '文字列の場合で、空をそのまま出力する
                    If UBound(calcStrArray) < 3 And workRange.text = "" Then
                        GetItemOperation = ""
                        Exit Function
                    End If
                    'ISSUE_NO.802 sunyi 2018/07/26 End
                    
                    '* 数式の生成:CTMシートのセル位置取得
                    workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                    '2017/09/29 No.544 Mod.  CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.
                    'workStr = .Name & "!" & workAddr
                    workStr = "'" & .Name & "'" & "!" & workAddr
                Else
                    Exit Function
                End If
            End With
        
        '* カンマがある場合は、演算形式に応じた式を生成する
        Else
        
            '* 演算式から " で囲まれた文字列を取得する
            '* "部品1生産実績_部品セリアルＮｏ,個数" / 100 ⇒「=SUM(XX:YY) / 100」
            splitStr2 = Split(calcString, ",")      ' 「部品1生産実績_部品セリアルＮｏ」「個数」（「合致条件」）
            splitStr3 = Split(splitStr2(0), "・")    ' 「部品1生産実績」「部品セリアルＮｏ」
            
            ''''splitStr3 = Split(splitStr2(0), "_")    ' 「部品1生産実績」「部品セリアルＮｏ」
                
            If InStr(splitStr2(1), "条件個数") Then
                workCond = splitStr2(2)
            
                Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                With currWorksheet
                    Set srchRange = .Range("1:1")
                    ' Wang Issue NO.727 2018/06/15 Start
                    If IsGripType Then
                        Set startRange = srchRange.Find(What:=splitStr3(2), LookAt:=xlWhole)
                    Else
                        Set startRange = srchRange.Find(What:=splitStr3(1), LookAt:=xlWhole)
                    End If
                    ' Wang Issue NO.727 2018/06/15 End
                    If Not startRange Is Nothing Then
                        Set lastRange = startRange.Offset(0, 0).End(xlDown)
                        Set workRange = .Range(startRange.Offset(1, 0), lastRange)  '* 着目エレメントのデータ範囲
                        '* データ範囲アドレスの取得
                        workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                        '* 数式の生成
                        '2017/09/29 No.544 Mod. CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.
                        'workStr = "COUNTIF(" & .Name & "!" & workAddr & ", ""=" & workCond & """)"
                        workStr = "COUNTIF(" & "'" & .Name & "'" & "!" & workAddr & ", ""=" & workCond & """)"
                    Else
                        Exit Function
                    End If
                End With
                
            ElseIf InStr(splitStr2(1), "単一条件") Or InStr(splitStr2(1), "複数条件") Then         '個数(単一条件)と個数(複数条件)の時
                '* "部品1生産実績_部品セリアルＮｏ,個数" / 100 ⇒「=SUM(XX:YY) / 100」
                splitStr4 = Mid(splitStr2(1), 10, Len(splitStr2(1)) - 10)  ' 「部品1生産実績_部品セリアルＮｏ」「個数」（「合致条件」）
                splitStr5 = Split(splitStr4, ";")                          ' 「部品1生産実績」「部品セリアルＮｏ」
                
                Select Case Left(splitStr2(1), 8)
                    Case "個数(複数条件)"
                        prefixStr = calcType(7)
                    Case "個数(単一条件)"
                        prefixStr = calcType(6)
                End Select
                
                workStrCnt = ""                                                     '初期値がNULLで設定する
                For JJ = 0 To UBound(splitStr5)
                    splitStr6 = Split(splitStr5(JJ), ":")

                    Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                    With currWorksheet
                        Set srchRange = .Range("1:1")
                        Set startRange = srchRange.Find(What:=splitStr6(0), LookAt:=xlWhole)

                        If Not startRange Is Nothing Then
                            Set lastRange = startRange.Offset(0, 0).End(xlDown)
                            Set workRange = .Range(startRange.Offset(1, 0), lastRange)     '* 着目エレメントのデータ範囲
                            '* 数式の生成
                            workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                          
                            If prefixStr = calcType(6) Then                         '個数(単一条件)のみ時
                                workStrCnt = "'" & .Name & "'" & "!" & workAddr & "," & Chr(34) & splitStr6(1) & Chr(34)
                                Exit For
                            Else                                                    '個数(複数条件)のみ時
                                If workStrCnt = "" Then
                                    workStrCnt = "'" & .Name & "'" & "!" & workAddr & "," & Chr(34) & splitStr6(1) & Chr(34)
                                Else
                                    workStrCnt = workStrCnt & "," & "'" & .Name & "'" & "!" & workAddr & "," & Chr(34) & splitStr6(1) & Chr(34)
                                End If
                            End If
                        Else
                            Exit Function
                        End If

                    End With
                Next
                
                workStr = prefixStr & workStrCnt & ")"
                
            Else
                Select Case splitStr2(1)
                    Case "個数"
                        prefixStr = calcType(0)
                    Case "積算"
                        prefixStr = calcType(1)
                    Case "平均"
                        prefixStr = calcType(2)
                    Case "最大"
                        prefixStr = calcType(3)
                    Case "最小"
                        prefixStr = calcType(4)
                    Case "種類数"
                        prefixStr = calcType(5)
                End Select
            
                Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                With currWorksheet
                    Set srchRange = .Range("1:1")
                    ' Wang Issue NO.727 2018/06/15 Start
                    If IsGripType Then
                        Set startRange = srchRange.Find(What:=splitStr3(2), LookAt:=xlWhole)
                    Else
                        Set startRange = srchRange.Find(What:=splitStr3(1), LookAt:=xlWhole)
                    End If
                    ' Wang Issue NO.727 2018/06/15 End

                    If Not startRange Is Nothing Then
                        Set lastRange = startRange.Offset(0, 0).End(xlDown)
                        Set workRange = .Range(startRange.Offset(1, 0), lastRange)  '* 着目エレメントのデータ範囲
                        '* 数式の生成
                        workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                        '2017/09/29 No.544 Mod. CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.---------Start
                        If InStr(prefixStr, "SUMPRODUCT") Then
                            'workStr = prefixStr & .Name & "!" & workAddr & "," & .Name & "!" & workAddr & "))"
                            workStr = prefixStr & "'" & .Name & "'" & "!" & workAddr & "," & .Name & "!" & workAddr & "))"
                        Else
                            'workStr = prefixStr & .Name & "!" & workAddr & ")"
                            workStr = prefixStr & "'" & .Name & "'" & "!" & workAddr & ")"
                        End If
                        '2017/09/29 No.544 Mod. CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.---------End
                    Else
                        Exit Function
                    End If
                End With
                
            End If
            
        End If
    
        calcStringOrg = Replace(calcStringOrg, """" & calcString & """", workStr, 1)
    
    Next
    
    GetItemOperation = "=" & calcStringOrg

End Function

'***********************************
'* すべての図形と画面定義を削除する
'***********************************
Public Sub DeleteAllShapeAndDispDef()
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim currShape           As Shape
    Dim startRange          As Range
    Dim lastRange           As Range
    Dim workRange           As Range

    '2017/09/26 No.533 Add.
    Call SetFilenameToVariable

    '*****************
    '* 図形を削除する
    '*****************
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    With statusWorksheet
    
        For Each currShape In .Shapes
            currShape.Delete
        Next currShape
    
    End With
    
    '*********************
    '* 画面定義を削除する
    '*********************
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet
    
        Set startRange = .Range("A1")
        If startRange.Offset(1, 0).Value <> "" Then
            Set lastRange = startRange.Offset(0, 0).End(xlToRight)
            Set lastRange = lastRange.Offset(0, 3)
            Set workRange = .Range(startRange.Offset(1, 0), lastRange.Offset(100, 0))
            workRange.ClearContents
        End If
    
    End With
    
End Sub

'*******************************************
'* 指定のテキストボックスの背景色を変更する
'*******************************************
Public Sub ChangeBackColorTB(currWorksheet As Worksheet, shapeName As String, rgbFill As Long)
    Dim currShape       As Shape
    
    Set currShape = currWorksheet.Shapes(shapeName)
    With currShape
        '* 図形名
'        .Name = currShapeName
        '* 登録マクロ名
'        .OnAction = macroName
        With .TextFrame2
            '* 文字列配置位置
'            .VerticalAnchor = msoAnchorMiddle
            With .TextRange
                '* 文字列配置一
'                .ParagraphFormat.Alignment = msoAlignCenter
                '* 文字列太字設定
'                .Font.Bold = msoTrue
                '* 文字サイズ
'                .Font.Size = 16
                '* 文字色
'                .Font.Fill.ForeColor.RGB = RGB(0, 0, 0)
            End With
        End With
        '* 文字枠色
'        .Line.ForeColor.RGB = RGB(0, 0, 0)
        '* 文字枠太さ
'        .Line.Weight = 1.5
        '* 文字枠塗りつぶし
        .Fill.ForeColor.RGB = rgbFill
    End With
End Sub

'*******************************************
'* 表示範囲の背景色を変更する
'*******************************************
Public Sub ChangeBackColorStatus(currRange As Range, rgbFill As Long)
    
    '* 選択範囲を薄い灰色にする
    currRange.Interior.Color = rgbFill

End Sub

'*******************************************
'* RGBカラー配列取得（Long値）
'*******************************************
Public Sub GetRGBColorArray(rgbColor() As Long)
    Dim paramColWorksheet      As Worksheet
    Dim i                      As Integer
    Dim srchRange              As Range
    Dim currRange              As Range
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    With paramColWorksheet
    
    Set srchRange = .Range("1:1")
    Set currRange = srchRange.Find(What:="操作パネル表示", LookAt:=xlWhole)
    
    If Not currRange Is Nothing Then
        For i = 0 To 14
            If currRange.Offset(i + 1, 1).Value <> "" Then
                rgbColor(i) = currRange.Offset(i + 1, 1).Value
            Else
                rgbColor(i) = currRange.Offset(i + 1, 0).Value
            End If
    
        Next
    End If
    End With

End Sub

'*******************************************
'* RGB背景表示カラー配列取得（Long値）
'*******************************************
Public Sub GetRGBColorBackArray(rgbColorBack() As Long)
    Dim paramColWorksheet      As Worksheet
    Dim i                      As Integer
    Dim srchRange              As Range
    Dim currRange              As Range
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    With paramColWorksheet
    
    Set srchRange = .Range("1:1")
    Set currRange = srchRange.Find(What:="背景設定", LookAt:=xlWhole)
    
    If Not currRange Is Nothing Then
        For i = 0 To 3
            If currRange.Offset(i + 1, 1).Value <> "" Then
                rgbColorBack(i) = currRange.Offset(i + 1, 1).Value
            Else
                rgbColorBack(i) = currRange.Offset(i + 1, 0).Value
            End If
    
        Next
    End If
    End With

End Sub

'*******************************************
'* 背景色と全てのテキストボックスの内容更新
'*******************************************
Public Sub UpdateAllBackAndTB()
'2017/06/14 Del No.260.
'Attribute UpdateAllBackAndTB.VB_ProcData.VB_Invoke_Func = "r\n14"
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim paramWorksheet      As Worksheet
    Dim currWorksheet       As Worksheet
    Dim currShape           As Shape
    Dim currRange           As Range
    Dim nowColorRange       As Range
    Dim srchRange           As Range
    Dim dispRange           As Range
    Dim startRange          As Range
    Dim workRange           As Range
    Dim II                  As Integer
    Dim JJ                  As Integer
    Dim formulaStr          As String
    Dim shapeName           As String
    Dim isFormat            As Integer
    Dim rgbColor(15)        As Long
    Dim rgbColorBack(4)     As Long
    Dim splitValue          As Variant
    Dim getValue            As Variant
    Dim getValue1            As Variant
    Dim backColorCond(4)    As Variant
    Dim chgElement          As String
    Dim currValue           As Variant
    Dim dispAddress         As String
    
    Dim shp As Object
    Dim flagExist As Boolean
    
    '* 色配列を取得する
    Call GetRGBColorArray(rgbColor)
    Call GetRGBColorBackArray(rgbColorBack)
    
'    rgbColorBack(0) = rgbColor(13)  ''''''''''''''''''''''20160619
'    rgbColorBack(1) = rgbColor(1)
'    rgbColorBack(2) = rgbColor(4)
''    rgbColorBack(3) = rgbColor(8)
'    '* 緑だけ特別設定
'    rgbColorBack(3) = RGB(0, 176, 80)
    
    '2017/09/26 No.533 Add.
    Call SetFilenameToVariable
    
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    '**************************************************************************************
    '* 画面定義シートにある演算式を更新し、ステータスシート上のテキストボックスを更新する
    '**************************************************************************************
    With dispDefWorksheet
    
        Set srchRange = .Range("1:1")
        Set currRange = srchRange.Find(What:="定義内容", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
        
            '* 全図形に対して
            For II = 1 To 100
                '定義内容の有無ﾁｪｯｸ
                If currRange.Offset(II, 0).Value <> "" Then
                    
                    Set dispRange = srchRange.Find(What:="表示色−白", LookAt:=xlWhole)
                    Set dispRange = .Cells(currRange.Offset(II, 0).Row, dispRange.Column)
                
                    '* 数式を更新する
                    formulaStr = GetItemOperation(currRange.Offset(II, 0).Value)
                    
                    With currRange.Offset(II, 1)
                        .NumberFormatLocal = "G/標準"
                        .FormulaLocal = formulaStr
                        If IsDate(.Value) Then
                            .NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
                        ElseIf IsNumeric(.Value) Then
'                            If InStr(currRange.Offset(II, 0).Value, "個数") Or _
'                               InStr(currRange.Offset(II, 0).Value, "種類数") Then
                           '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                           'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                           '* ISSUE_NO.670 sunyi 2018/05/15 start
                            '* オーバーフローを修正
                            'If .Value = CLng(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                            If .Value = CDbl(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                            '* ISSUE_NO.670 sunyi 2018/05/15 end
                                '-2017/05/08 No.554 Mod. Changed # to 0.
                                .NumberFormatLocal = "0"
                            Else
                                .NumberFormatLocal = "0.00"
                            End If
                        End If
                        
                        isFormat = currRange.Offset(II, 2).Value
                        
                        '* テキストボックスへ数式の値をセットする
                        shapeName = currRange.Offset(II, -1).Value '図形名の取込　value_shape_01
                        
                        '2017/01/13 追加
                        '該当Shape(ﾃｷｽﾄBox)の有無チェック
                        flagExist = False
                        For Each shp In statusWorksheet.Shapes
                            If shp.Name = shapeName Then
                                flagExist = True
                                Exit For
                            End If
                        Next shp
                        
                        '2017/01/13
                        '該当のShapeがあれば、Shapeに値を設定
                        If flagExist = True Then
                            Set currShape = statusWorksheet.Shapes(shapeName)
                            On Error Resume Next
                            Select Case isFormat
                                '* 数値処理
                                Case 0
                                    '* 書式設定
    '                                If InStr(currRange.Offset(II, 0).Value, "個数") Or _
    '                                   InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                    '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                                    'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                                    '* ISSUE_NO.670 sunyi 2018/05/15 start
                                    '* オーバーフローを修正
                                    'If .Value = CLng(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                    If .Value = CDbl(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                    '* ISSUE_NO.670 sunyi 2018/05/15 end
                                        '-2017/05/08 No.554 Mod. Changed # to 0.
                                        currShape.TextFrame.Characters.text = Format(.Value, "0")
                                    Else
                                        currShape.TextFrame.Characters.text = Format(.Value, "0.00")
                                    End If
                                    '* 色の処理
                                    For JJ = 0 To UBound(rgbColor)
                                        getValue = dispRange.Offset(0, JJ).Value
                                        If getValue <> "" Then
                                            If InStr(getValue, ",") Then
                                                splitValue = Split(getValue, ",")
                                                '2017/09/26 No.538 Mod. Changed < to <=.
                                                If Val(splitValue(0)) <= .Value And .Value <= Val(splitValue(1)) Then
                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                    Exit For
                                                Else
                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                End If
                                            Else
                                                If Val(getValue) = .Value Then
                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                    Exit For
                                                Else
                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                End If
                                            End If
                                        End If
                                    Next
                                '* 文字列処理
                                Case 1
'                                    '* ISSUE_NO.759 Sunyi 2018/06/25 Start
'                                    '* 空欄の場合、””に表示する
''                                    '* 書式設定
''                                    currShape.TextFrame.Characters.text = Format(.Value)
'                                    If .Value = 0 Or .Value = "" Then
'                                        .Value = ""
'                                    Else
'                                        currShape.TextFrame.Characters.text = Format(.Value)
'                                    End If
'                                    '* ISSUE_NO.759 Sunyi 2018/06/25 End
                                    'ISSUE_NO.802 sunyi 2018/07/26 Start
                                    '文字列の場合で、空をそのまま出力する
                                    currShape.TextFrame.Characters.text = Format(.Value)
                                    'ISSUE_NO.802 sunyi 2018/07/26 End
                                     '* 色の処理
                                    For JJ = 0 To UBound(rgbColor)
                                        getValue = dispRange.Offset(0, JJ).Value
                                        If getValue <> "" Then
                                            If getValue = .Value Then
                                                Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                Exit For
                                            Else
                                                Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                            End If
                                        End If
                                    Next
                                '* 日付処理
                                Case 2
                                    '* 書式設定
                                    '20161104 追加
                                    If .Value = 0 Or .Value = "" Then
                                        currShape.TextFrame.Characters.text = ""
                                    Else
                                        '2017/09/26 No.539 Mod. Changed "yyyy/MM/dd hh:mm:ss" to "yyyy/MM/dd".
                                        currShape.TextFrame.Characters.text = Format(.Value, "yyyy/MM/dd")
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor)
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                End If
                                            End If
                                        Next
                                    End If
                                '* 時刻処理 2017/02/13 Add
                                Case 3
                                    '* 書式設定
                                    '20161104 追加
                                    If .Value = 0 Or .Value = "" Then
                                        currShape.TextFrame.Characters.text = ""
                                    Else
                                        currShape.TextFrame.Characters.text = Format(.Value, "hh:mm:ss")
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor)
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                End If
                                            End If
                                        Next
                                    End If
                                 '* 日時処理 2017/02/13 Add
                                Case 4
                                    '* 書式設定
                                    '20161104 追加
                                    If .Value = 0 Or .Value = "" Then
                                        currShape.TextFrame.Characters.text = ""
                                    Else
                                        currShape.TextFrame.Characters.text = Format(.Value, "yyyy/MM/dd hh:mm:ss")
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor)
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
                                                    End If
                                                End If
                                            End If
                                        Next
                                    End If
                            End Select
                        End If
                
                    End With    ' currRange.Offset(II, 1)
    
                End If  ' 図形情報があれば
                
            Next    ' 全図形
            
        End If  ' 「定義内容」項目があれば
        
    End With    ' dispDefWorksheet
    
    '**************************************************************************
    '* paramシートにある表示条件を取得し、ステータスシート上の背景色を更新する
    '**************************************************************************
    With paramWorksheet
    
        '* 表示範囲が設定されていればその範囲で表示する
        dispAddress = GetDisplayAddress(statusWorksheet)
        
        If dispAddress <> "" Then
        
            Set dispRange = statusWorksheet.Range(dispAddress)

            Set srchRange = .Range("A:A")
            Set currRange = srchRange.Find(What:="表示色対象", LookAt:=xlWhole)
            Set nowColorRange = srchRange.Find(What:="現在表示色", LookAt:=xlWhole)
            
        
            If Not currRange Is Nothing Then
            
                '* 表示色対象エレメント名を取得
                chgElement = currRange.Offset(0, 1).Value
                
                '---2016/12/19 Add No.386 [実行時エラー９対策] 表示色対象エレメントが設定されているときのみ処理 IF文追加 -------------Start
                If chgElement <> "" Then
                chgElement = Replace(chgElement, """", "")
            
                splitValue = Split(chgElement, "・")    ' 「部品1生産実績」「部品セリアルＮｏ」
                
                    ''''splitValue = Split(chgElement, "_")    ' 「部品1生産実績」「部品セリアルＮｏ」
                    
                    Set currWorksheet = Workbooks(thisBookName).Worksheets(splitValue(0))
                    With currWorksheet
                        Set srchRange = .Range("1:1")
                        ' Wang Issue NO.727 2018/06/15 Start
                        If IsGripType Then
                            Set startRange = srchRange.Find(What:=splitValue(2), LookAt:=xlWhole)
                        Else
                            Set startRange = srchRange.Find(What:=splitValue(1), LookAt:=xlWhole)
                        End If
                        ' Wang Issue NO.727 2018/06/15 End
                        
                        If Not startRange Is Nothing Then
                            Set workRange = startRange.Offset(1, 0)             '* 着目エレメントの最新データ範囲
                            '* 値の取得
                            currValue = workRange.Offset(0, 0).Value
                            
                            '* デフォルトカラーは緑固定
                            '2017/09/27 No.542 Del. Do not set default back color.
                            Call ChangeBackColorStatus(dispRange, RGB(255, 255, 255))
                            'nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(3))
                            
                            For II = 1 To 4
                                '''''''''''''''''''
                                '型の決定(10:数字 11:数字(カンマ区切りの二つの数）,20:文字列)
                                '''''''''''''''''''
                                If IsNumeric(currRange.Offset(II, 1).Value) Then
                                    
                                    splitValue = Split(currRange.Offset(II, 1).Value, ",")
                                    If UBound(splitValue) = 0 Then
                                        isFormat = 10
                                    ElseIf UBound(splitValue) = 1 Then
                                        isFormat = 11
                                    Else
                                        isFormat = 20
                                    End If
                                Else
                                    isFormat = 20
                                End If
                            
                                If isFormat = 10 Then
                                    getValue = CDbl(currRange.Offset(II, 1).Value)
                                    
                                    If currValue = getValue Then
                                    
                                        Call ChangeBackColorStatus(dispRange, rgbColorBack(II - 1))
                                        nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(II - 1))
                                        Exit For
                                    End If
                                ElseIf isFormat = 11 Then
                                    splitValue = Split(currRange.Offset(II, 1).Value, ",")
                                    If UBound(splitValue) = 1 Then
                                        getValue = CDbl(splitValue(0))
                                        getValue1 = CDbl(splitValue(1))
                                        If currValue >= getValue And currValue <= getValue1 Then
                                            Call ChangeBackColorStatus(dispRange, rgbColorBack(II - 1))
                                            nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(II - 1))
                                            Exit For
                                        End If
                                    End If
                                    
                                ElseIf isFormat = 20 Then
                                    If currValue = currRange.Offset(II, 1).Value Then
                                    
                                        Call ChangeBackColorStatus(dispRange, rgbColorBack(II - 1))
                                        nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(II - 1))
                                        Exit For
                                    End If
                                End If
                                
                            Next
                            
'                            '現在運転状態表示 20160619 B21固定   Chr(13) & Chr(10) &
                            currValue = Replace(currValue, "・", Chr(13) & Chr(10) & "")
                            currValue = Replace(currValue, "中", Chr(13) & Chr(10) & "中")
                            
                            Workbooks(thisBookName).Worksheets(ParamSheetName).Range("B21").Value = currValue
                            
                        End If
                    End With
                End If
                '---2016/12/19 Add No.386 [実行時エラー９対策] 表示色対象エレメントが設定されているときのみ処理 IF文追加 -------------End
            End If

        End If
    
    End With
    
End Sub

Private Function GetRGBHexString(lngRGB As Long) As String
    Dim strR As String
    Dim strG As String
    Dim strB As String
    Dim strHex As String

    '長整数の色表記をR・G・Bに分解
    strR = Right$("00" & Hex$((lngRGB And "&H" & "0000FF")), 2)
    strG = Right$("00" & Hex$((lngRGB And "&H" & "00FF00") / 256), 2)
    strB = Right$("00" & Hex$((lngRGB And "&H" & "FF0000") / 256 ^ 2), 2)
    
    'R・G・Bそれぞれの文字列を結合
    strHex = "#" & strR & strG & strB
    
    '結果をtxt16進表記に代入
    GetRGBHexString = strHex

End Function


'*************************************************
'* テキストボックスクリック時に呼び出されるマクロ
'*************************************************
Public Sub ConditionOpen()
    Dim currShape           As Shape
    Dim dispDefWorksheet    As Worksheet
    Dim paramWorkhseet      As Worksheet
    Dim condParamsheet      As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim rowRange            As Range
    Dim colRange            As Range
    Dim calcStr             As String
    Dim isFormat            As Integer
    Dim findNo              As Integer
    Dim findNo1             As Integer
    Dim intstr              As Variant

    Dim II                  As Integer
    Dim J                   As Integer
    Dim iEndRow             As Integer
    
    '* 選択図形を取得
    Set currShape = ActiveSheet.Shapes(Application.Caller)
    
    '2017/09/26 No.533 Add.
    Call SetFilenameToVariable

    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet

        '* 演算式および表示書式を取得
        Set srchRange = .Range("A:A")
        Set rowRange = srchRange.Find(What:=currShape.Name, LookAt:=xlWhole)
        If Not rowRange Is Nothing Then
            calcStr = rowRange.Offset(0, 1).Value
            isFormat = rowRange.Offset(0, 3).Value
        Else
            Exit Sub
        End If

        '* 表示色条件情報範囲を取得
        Set srchRange = .Range("1:1")
        Set colRange = srchRange.Find(What:="表示色−白", LookAt:=xlWhole)
        If Not colRange Is Nothing Then
            Set currRange = .Cells(rowRange.Row, colRange.Column)
        Else
            Exit Sub
        End If

    End With
    
    ' Wang Issue NO.727 2018/06/15 Start
    If IsGripType() Then
        Load GripActionPaneForm
        With GripActionPaneForm
        
            '*ISSUE_NO.671 sunyi 2018/05/29 start
            '*仕様変更
    '        Dim currWorksheet   As Worksheet
    '        '*
    '        Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    '        Set srchRange = currWorksheet.Cells.Find(What:="操作バルキ表示Flag", LookAt:=xlWhole)
    '
    '        srchRange.Offset(0, 1).Value = "True"
                    
            If .CB_CalcFormat.text = "" Then
                '* 「エレメント追加」ボタンを非表示
                .BT_add.Visible = False
                
                '* 「エレメント削除」ボタンを非表示
                .BT_rowdelete.Visible = False
                
                '* 「条件更新」ボタンを非表示
                .BT_ConditionUpdate.Visible = False
                
                '* 「条件」テキストボックスを非表示
                .TB_CalcCondition.Visible = False
                
                '* 「条件」ラベルを非表示
                .Label23.Visible = False
                
                '* 「エレメント」リストビューを非表示
                .LV_ElementView.Visible = False
                
                '* リストビューのへーだー「エレメント」ラベルを非表示
                .Label25.Visible = False
                
                '* リストビューのへーだー「条件」ラベルを非表示
                .Label24.Visible = False
            End If
            '*ISSUE_NO.671 sunyi 2018/05/29 end
            
            .Caption = "表示テキストボックス編集"
            .ShowMissionName
        
            '* 演算式
            .TB_ItemOperation.text = calcStr
            
            '* 演算形式
            If InStr(calcStr, "+") <> 0 Then
                intstr = Split(calcStr, "+")
            
                If InStr(intstr(0), ",") <> 0 Then
                    findNo = InStr(intstr(0), ",") + 1
                    findNo1 = InStr(intstr(0), "[")
                    If findNo1 <> 0 Then
                        .CB_CalcFormat.text = Mid(Trim(intstr(0)), findNo, findNo1 - findNo)
                    Else
                        .CB_CalcFormat.text = Mid(Trim(intstr(0)), findNo, Len(Trim(intstr(0))) - findNo)
                    End If
                End If
            Else
                If InStr(calcStr, ",") <> 0 Then
                    findNo = InStr(calcStr, ",") + 1
                    findNo1 = InStr(calcStr, "[")
                    
                    If findNo1 <> 0 Then
                        .CB_CalcFormat.text = Mid(Trim(calcStr), findNo, findNo1 - findNo)
                    Else
                        .CB_CalcFormat.text = Mid(Trim(calcStr), findNo, Len(Trim(calcStr)) - findNo)
                    End If
                    
                End If
            End If
            
            '* 図形名
            .LBL_ShapeName = currShape.Name
            '* 各条件
            .TB_White.text = currRange.Offset(0, 0).Value
            .TB_Red.text = currRange.Offset(0, 1).Value
            .TB_Lime.text = currRange.Offset(0, 2).Value
            .TB_Blue.text = currRange.Offset(0, 3).Value
            .TB_Yellow.text = currRange.Offset(0, 4).Value
            .TB_Magenta.text = currRange.Offset(0, 5).Value
            .TB_Aqua.text = currRange.Offset(0, 6).Value
            .TB_Maroon.text = currRange.Offset(0, 7).Value
            .TB_Green.text = currRange.Offset(0, 8).Value
            .TB_Navy.text = currRange.Offset(0, 9).Value
            .TB_Olive.text = currRange.Offset(0, 10).Value
            .TB_Purple.text = currRange.Offset(0, 11).Value
            .TB_Teal.text = currRange.Offset(0, 12).Value
            .TB_Silver.text = currRange.Offset(0, 13).Value
            .TB_Gray.text = currRange.Offset(0, 14).Value
    
            '* 「作成」ボタンを非表示
            .BT_CreateTextBox.Visible = False
            
            '* 書式ラジオボタンの設定
            Select Case isFormat
                Case 0
                    .OB_Numeric.Value = True
                Case 1
                    .OB_String.Value = True
                Case 2
                    .OB_Date.Value = True
                Case 3
                    .OB_Time.Value = True
                Case 4
                    .OB_DateTime.Value = True
            End Select
            
            '* ISSUE_NO.694 sunyi 2018/05/16 start
            '* 単一、複数条件により、単一、複数条件の画面表示か非表示
            If .CB_CalcFormat.text = "個数(単一条件)" Or .CB_CalcFormat.text = "個数(複数条件)" Then
                .TB_CalcCondition.Visible = True
                .BT_add.Visible = True
                .BT_rowdelete.Visible = True
                .LV_ElementView.Visible = True
                .Label25.Visible = True
                .Label24.Visible = True
                .Label23.Visible = True
                .BT_ConditionUpdate.Visible = True
            Else
                .TB_CalcCondition.Visible = False
                .BT_add.Visible = False
                .BT_rowdelete.Visible = False
                .LV_ElementView.Visible = False
                .Label25.Visible = False
                .Label24.Visible = False
                .Label23.Visible = False
                .BT_ConditionUpdate.Visible = False
            End If
            '* ISSUE_NO.694 sunyi 2018/05/16 end
        
            Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
            
            iEndRow = condParamsheet.UsedRange.Rows.Count
            
            J = 0
            For II = 1 To iEndRow
                            
                If condParamsheet.Range("A" & II).Value = currShape.Name Then
                    J = J + 1
                    .LV_ElementView.ListItems.Add().text = J
                    .LV_ElementView.ListItems(J).SubItems(1) = condParamsheet.Range("A" & II).Offset(0, 1)
                    .LV_ElementView.ListItems(J).SubItems(2) = condParamsheet.Range("A" & II).Offset(0, 2)
                   
                End If
            Next
            
            '* ISSUE_NO.666 sunyi 2018/05/29 start
            '* 吹き出し追加
            If .LV_ElementView.ListItems.Count > 0 Then
                .LV_ElementView.ControlTipText = .LV_ElementView.ListItems.Item(1).SubItems(1) & ":" & .LV_ElementView.ListItems.Item(1).SubItems(2)
            End If
            '* ISSUE_NO.666 sunyi 2018/05/29 end
        
            .Show vbModeless
        
        End With
        Exit Sub
    End If
    
    Load ActionPaneForm
    With ActionPaneForm
    
        '*ISSUE_NO.671 sunyi 2018/05/29 start
        '*仕様変更
'        Dim currWorksheet   As Worksheet
'        '*
'        Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
'        Set srchRange = currWorksheet.Cells.Find(What:="操作バルキ表示Flag", LookAt:=xlWhole)
'
'        srchRange.Offset(0, 1).Value = "True"
                
        If .CB_CalcFormat.text = "" Then
            '* 「エレメント追加」ボタンを非表示
            .BT_add.Visible = False
            
            '* 「エレメント削除」ボタンを非表示
            .BT_rowdelete.Visible = False
            
            '* 「条件更新」ボタンを非表示
            .BT_ConditionUpdate.Visible = False
            
            '* 「条件」テキストボックスを非表示
            .TB_CalcCondition.Visible = False
            
            '* 「条件」ラベルを非表示
            .Label23.Visible = False
            
            '* 「エレメント」リストビューを非表示
            .LV_ElementView.Visible = False
            
            '* リストビューのへーだー「エレメント」ラベルを非表示
            .Label25.Visible = False
            
            '* リストビューのへーだー「条件」ラベルを非表示
            .Label24.Visible = False
        End If
        '*ISSUE_NO.671 sunyi 2018/05/29 end
        
        .Caption = "表示テキストボックス編集"
    
        '* 演算式
        .TB_ItemOperation.text = calcStr
        
        '* 演算形式
        If InStr(calcStr, "+") <> 0 Then
            intstr = Split(calcStr, "+")
        
            If InStr(intstr(0), ",") <> 0 Then
                findNo = InStr(intstr(0), ",") + 1
                findNo1 = InStr(intstr(0), "[")
                If findNo1 <> 0 Then
                    .CB_CalcFormat.text = Mid(Trim(intstr(0)), findNo, findNo1 - findNo)
                Else
                    .CB_CalcFormat.text = Mid(Trim(intstr(0)), findNo, Len(Trim(intstr(0))) - findNo)
                End If
            End If
        Else
            If InStr(calcStr, ",") <> 0 Then
                findNo = InStr(calcStr, ",") + 1
                findNo1 = InStr(calcStr, "[")
                
                If findNo1 <> 0 Then
                    .CB_CalcFormat.text = Mid(Trim(calcStr), findNo, findNo1 - findNo)
                Else
                    .CB_CalcFormat.text = Mid(Trim(calcStr), findNo, Len(Trim(calcStr)) - findNo)
                End If
                
            End If
        End If
        
        '* 図形名
        .LBL_ShapeName = currShape.Name
        '* 各条件
        .TB_White.text = currRange.Offset(0, 0).Value
        .TB_Red.text = currRange.Offset(0, 1).Value
        .TB_Lime.text = currRange.Offset(0, 2).Value
        .TB_Blue.text = currRange.Offset(0, 3).Value
        .TB_Yellow.text = currRange.Offset(0, 4).Value
        .TB_Magenta.text = currRange.Offset(0, 5).Value
        .TB_Aqua.text = currRange.Offset(0, 6).Value
        .TB_Maroon.text = currRange.Offset(0, 7).Value
        .TB_Green.text = currRange.Offset(0, 8).Value
        .TB_Navy.text = currRange.Offset(0, 9).Value
        .TB_Olive.text = currRange.Offset(0, 10).Value
        .TB_Purple.text = currRange.Offset(0, 11).Value
        .TB_Teal.text = currRange.Offset(0, 12).Value
        .TB_Silver.text = currRange.Offset(0, 13).Value
        .TB_Gray.text = currRange.Offset(0, 14).Value

        '* 「作成」ボタンを非表示
        .BT_CreateTextBox.Visible = False
        
        '* 書式ラジオボタンの設定
        Select Case isFormat
            Case 0
                .OB_Numeric.Value = True
            Case 1
                .OB_String.Value = True
            Case 2
                .OB_Date.Value = True
            Case 3
                .OB_Time.Value = True
            Case 4
                .OB_DateTime.Value = True
        End Select
        
        '* ISSUE_NO.694 sunyi 2018/05/16 start
        '* 単一、複数条件により、単一、複数条件の画面表示か非表示
        If .CB_CalcFormat.text = "個数(単一条件)" Or .CB_CalcFormat.text = "個数(複数条件)" Then
            .TB_CalcCondition.Visible = True
            .BT_add.Visible = True
            .BT_rowdelete.Visible = True
            .LV_ElementView.Visible = True
            .Label25.Visible = True
            .Label24.Visible = True
            .Label23.Visible = True
            .BT_ConditionUpdate.Visible = True
        Else
            .TB_CalcCondition.Visible = False
            .BT_add.Visible = False
            .BT_rowdelete.Visible = False
            .LV_ElementView.Visible = False
            .Label25.Visible = False
            .Label24.Visible = False
            .Label23.Visible = False
            .BT_ConditionUpdate.Visible = False
        End If
        '* ISSUE_NO.694 sunyi 2018/05/16 end
    
        Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
        
        iEndRow = condParamsheet.UsedRange.Rows.Count
        
        J = 0
        For II = 1 To iEndRow
                        
            If condParamsheet.Range("A" & II).Value = currShape.Name Then
                J = J + 1
                .LV_ElementView.ListItems.Add().text = J
                .LV_ElementView.ListItems(J).SubItems(1) = condParamsheet.Range("A" & II).Offset(0, 1)
                .LV_ElementView.ListItems(J).SubItems(2) = condParamsheet.Range("A" & II).Offset(0, 2)
               
            End If
        Next
        
        '* ISSUE_NO.666 sunyi 2018/05/29 start
        '* 吹き出し追加
        If .LV_ElementView.ListItems.Count > 0 Then
            .LV_ElementView.ControlTipText = .LV_ElementView.ListItems.Item(1).SubItems(1) & ":" & .LV_ElementView.ListItems.Item(1).SubItems(2)
        End If
        '* ISSUE_NO.666 sunyi 2018/05/29 end
    
        .Show vbModeless
    
    End With
    
'    Load ConditionForm
'    With ConditionForm
'        '* 演算式
'        .TB_ItemOperation.text = calcStr
'        '* 図形名
'        .LBL_ShapeName = currShape.Name
'        '* 各条件
'        .TB_Green = currRange.Offset(0, 0).Value
'        .TB_Red = currRange.Offset(0, 1).Value
'        .TB_Blue = currRange.Offset(0, 2).Value
'        .TB_White = currRange.Offset(0, 3).Value
'        .Show vbModeless
'    End With

End Sub
