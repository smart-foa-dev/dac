Attribute VB_Name = "MissionStd_Module"
'**********************
'* 収集開始日時の取得
'* →返り値はUNIX TIME
'**********************
Public Function GetCollectStartDTstd(currWorksheet As Worksheet) As Double
    Dim srcWorksheet        As Worksheet
    Dim StartTime           As Double
    Dim startD              As Date
    Dim startT              As Date
    Dim ctmList()           As CTMINFO
    Dim II                  As Integer
    Dim recieveTimeArray()  As Double
    Dim iFirstFlag          As Boolean
    Dim recieveTime         As Variant
    Dim startRange          As Range
    Dim srchRange           As Range
    Dim rtIndex             As Integer
    Dim missionList()       As MISSIONINFO
    
    '**********************************
    '* 全CTMシートの最新時刻を取得する
    '**********************************
    '* CTMリストを取得する
'    Call GetCTMList(MainSheetName, ctmList)
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    '* CTM情報一覧をシートから取得
    iFirstFlag = True
    For Each srcWorksheet In Worksheets
    
        '* CTM情報一覧に存在するシートのみを処理対象とする
        For II = 0 To UBound(missionList(0).CTM)
            If (missionList(0).CTM(II).Name = srcWorksheet.Name) Then Exit For
        Next
        If II <= UBound(missionList(0).CTM) Then

            '* 最上位のRECIEVE TIMEを取得する
            Set startRange = srcWorksheet.Range("A2")
            For II = 0 To 100
                recieveTime = startRange.Offset(II, 0).Value
                If recieveTime <> "" Then
                    Exit For
                End If
            Next
            '* 100行検索しても見つからなければ処理中断＝データ無
            If II > 100 Then
                Exit For
            End If
            
            '* 取得した値が日付形式ならば
            If IsDate(recieveTime) Then
            
                If iFirstFlag Then
                    rtIndex = 0
                    ReDim recieveTimeArray(rtIndex)
                    iFirstFlag = False
                Else
                    rtIndex = UBound(recieveTimeArray) + 1
                    ReDim Preserve recieveTimeArray(rtIndex)
                End If
                
                recieveTimeArray(rtIndex) = GetUnixTime(CDate(recieveTime))
                
            Else
                Exit For
            End If

        End If
    
    Next
        
    If iFirstFlag Then
        recieveTime = 0#
    Else
        '* 取得した時刻群の最小の時刻を取得
        recieveTime = Application.WorksheetFunction.Min(recieveTimeArray)
    End If

    '****************************************
    '* paramシートから収集開始日時を取得する
    '****************************************
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="取得開始", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "" Then
                startD = DateValue(srchRange.Offset(0, 1).Value)
                startT = TimeValue(srchRange.Offset(0, 1).Value)
            Else
                startD = DateValue("2016/1/1 00:00:00")
                startT = TimeValue("2016/1/1 00:00:00")
            End If
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            GetCollectStartDT = -1
            Exit Function
        End If
        StartTime = GetUnixTime(startD + startT)

    End With
    
    GetCollectStartDT = Application.WorksheetFunction.Max(recieveTime, StartTime)

End Function

'****************************************************************
'* 対象ミッションの情報を取得する
'****************************************************************
'* ISSUE_NO.658 sunyi 2018/04/24 start
'* element削除時のデータ更新、性能アップ
'Public Function GetMissionInfo(missionList() As MISSIONINFO, ctmValue() As CTMINFO) As Integer
Public Function GetMissionInfo(missionList() As MISSIONINFO, ctmValue() As CTMINFO, sType As String) As Integer
'* ISSUE_NO.658 sunyi 2018/04/24 end
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim missionID   As String
    Dim ctmID       As String
    Dim StartTime   As Double
    Dim endTime     As Double
    Dim SendUrlStr  As String
    Dim SendString  As Variant
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim iResult     As Integer
    Dim bPmary      As Variant
    Dim aaa         As Variant
    Dim II          As Integer
    Dim currCTM     As CTMINFO
    Dim mIndex      As Integer
    
    Dim updTime     As Variant  ' 2017.02.22 Add
    Dim onlineId    As String   ' 2017.02.22 Add
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    'FOR COM高速化 zhengxiaoxue 2018/06/14 Delete Start
    '* ISSUE_NO.658 sunyi 2018/04/24 start
    '* element削除時のデータ更新、性能アップ
    'Dim sZipFolder     As String
    'Dim obj            As FoaCoreCom.FoaCoreCom
    'Dim bdr            As FoaCoreCom.ReadMissionCtmInfo
    '* ISSUE_NO.658 sunyi 2018/04/24 end
    'FOR COM高速化 zhengxiaoxue 2018/06/14 Delete End

    'FOR COM高速化 zhengxiaoxue 2018/06/14 Add Start
    Dim ctmData As FoaCoreCom.CtmDataRetriever
    Dim csvWrite As FoaCoreCom.CsvWriteToExcelHandler
    Dim zipFile As FoaCoreCom.ZipFileHandler
    
    Dim zipUrl As String
    Dim unZipUrl As String
    Dim msg As String
    'FOR COM高速化 zhengxiaoxue 2018/06/14 Add End
    
    
    '* メインシートから各種情報を取得
'    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '******************************************************
        '* 収集開始日時を取得
        '* →現在取得済のCTM情報を元に過去最短の時刻を取得する
        '******************************************************
        StartTime = GetCollectStartDT(Workbooks(thisBookName).Worksheets(ParamSheetName))

        '*********************
        '* 収集終了日時を取得
        '*********************
'        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            endD = srchRange.Offset(0, 1).Value
'            endT = srchRange.Offset(0, 2).Value
'        Else
'            endD = CDate("2020/12/31")
'            endT = CDate("23:59:59")
'        End If
'        If Not IsDate(endD + endT) Then
'            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
'            Exit Function
'        End If
'        endTime = GetUnixTime(endD + endT)
        endTime = GetUnixTime(DateAdd("s", 60, Now))
        
        '*********************
        '* ミッションIDを取得
        '*********************
'        missionID = ""
'        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            missionID = srchRange.Offset(0, 1).Value
'        Else
'            MsgBox "ミッションID項目が見つからないため処理を中断します。"
'            GetMissionInfo = -1
'            Exit Function
'        End If
        
'    End With
    
    '*******************************
    '* IPアドレスおよびPortNoを取得
    '*******************************
    With Workbooks(thisBookName).Worksheets(ParamSheetName)

        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If

        '2017.02.21 Add*****
        '* 更新周期を取得
        '2017.02.21 Add*****
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
    
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
    
    End With
    
    '*******************************
    '* CTM情報一覧をシートから取得
    '*******************************
'    Call GetCTMList(MainSheetName, ctmList)
    
    ' 2017.02.21 Add *********************
    '* オンライングラフIDの取得
    ' 2017.02.21 Add *********************
    onlineId = GetOnlineId()
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    '*******************************
    '* プログラムミッションに問合せ
    '*******************************
    
    '* ISSUE_NO.658 sunyi 2018/04/24 start
    '* element削除時のデータ更新、性能アップ
    'FOR COM高速化 zhengxiaoxue 2018/06/14 Delete Start
    'Set bdr = CreateObject("FoaCoreCom.ais.retriever.ReadMissionCtmInfo")
    'FOR COM高速化 zhengxiaoxue 2018/06/14 Delete End
    sType = SEARCH_TYPE_AUTO
    '* ISSUE_NO.658 sunyi 2018/04/24 end
    
    ReDim ctmValue(0)
    ctmValue(0).ID = ""
    For mIndex = 0 To UBound(missionList)
        '* ミッションIDがあれば
        missionID = missionList(mIndex).ID
        If missionID <> "" Then
            getEndTime = endTime
            getStartTime = StartTime
            
            '* ISSUE_NO.658 sunyi 2018/04/24 start
            '* element削除時のデータ更新、性能アップ
'            ' 2017.02.22 Change **************************
'    '        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pm?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & _
'    '                        "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true"
'            SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pm?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & _
'                            "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true" _
'                            & "&onlineId=" & "&period=" & Format(updTime * 1000) & "&collectEnd=" & Format(getEndTime)
'            ' 2017.02.22 Change **************************
            SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pmzip?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & _
                            "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true" _
                            & "&onlineId=" & "&period=" & Format(updTime * 1000) & "&collectEnd=" & Format(getEndTime)
            '* ISSUE_NO.658 sunyi 2018/04/24 end
        
            'FOR COM高速化 zhengxiaoxue 2018/06/14 Delete Start
'            If useProxy = False Then
'                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'            Else
'                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'                httpObj.setProxy 2, proxyUri
'            End If
'
'            httpObj.Open "GET", SendUrlStr, False
'            httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
'            httpObj.send
'
'            ' ダウンロード待ち
'            Do While httpObj.readyState <> 4
'                DoEvents
'            Loop
'
'            '* 取得情報のデコード
'            Set sc = CreateObject("ScriptControl")
'            With sc
'                .Language = "JScript"
'
'                '指定したインデックス、名称のデータを取得する
'                .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
'            End With
            'FOR COM高速化 zhengxiaoxue 2018/06/14 Delete End
            
            '* ISSUE_NO.658 sunyi 2018/04/24 start
            '* element削除時のデータ更新、性能アップ
'            GetString = httpObj.responseText
'            Set objJSON = sc.CodeObject.jsonParse(GetString)
'            Set sc = Nothing
'
''***********************************************************************
''* VBA-JSONを使用する場合
''            GetString = httpObj.responseText
''            Set objJSON = JsonConverter.ParseJson(GetString)
''***********************************************************************
'
'            Set httpObj = Nothing
'
'            '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
'            If InStr(GetString, "MISSION_NOT_FOUND") > 0 Then
'                MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
'
'                '周期起動停止
'                'Call ChangeStartStopBT
'                GetMissionInfo = -1
'                Exit Function
'            End If
'            '**********
'
'            '* 取得情報をCTM情報に展開
'    '        iResult = GetCTMInfo(objJSON, ctmList, ctmValue)
'            iResult = GetCTMInfo(objJSON, missionList(mIndex).CTM, ctmValue)
''            iResult = GetCTMInfoNew(objJSON, missionList(mIndex).CTM, ctmValue)
        
            'FOR COM高速化 zhengxiaoxue 2018/06/14 Modify Start
'            sZipFolder = bdr.GetMissionCtmPath()
'
'            If httpObj.Status = 200 Then
'                Set oStream = CreateObject("ADODB.Stream")
'                oStream.Open
'                oStream.Type = 1
'                oStream.Write httpObj.responseBody
'                oStream.SaveToFile sZipFolder & "\data.zip", 2
'                oStream.Close
'            End If
'            UnZipFile sZipFolder & "\data.zip"
'
'            If (bdr.ReadAllCtmInfoToExcel(Workbooks(thisBookName), sZipFolder, MaxCntImport, sType, GetTimezoneId())) Then
'                GetMissionInfo = 1
'            Else
'                GetMissionInfo = -1
'            End If
'
'            If Dir(sZipFolder & "\*.*") <> "" Then Kill sZipFolder & "\*.*"
'            If Dir(sZipFolder) <> "" Then RmDir sZipFolder
            '* ISSUE_NO.658 sunyi 2018/04/24 end
            
            Set ctmData = CreateObject("FoaCoreCom.ais.retriever.CtmDataRetriever")
            Set csvWrite = CreateObject("FoaCoreCom.ais.retriever.CsvWriteToExcelHandler")
            Set zipFile = CreateObject("FoaCoreCom.ais.retriever.ZipFileHandler")
            msg = ""
        
            '情報を格納するZIPファイルパスを取得する
            zipUrl = ctmData.GetProgramMissionZipFileAsync(SendUrlStr, proxyUri, msg)
        
        
            '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
            If InStr(msg, "MISSION_NOT_FOUND") > 0 Then
                MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
                
                '周期起動停止
                'Call ChangeStartStopBT
                'pmzip失敗する場合
                GetMissionInfo = -1
                Exit Function
            End If
            '**********
        
            'Zipファイルを解凍する
            unZipUrl = zipFile.UnZipFile(zipUrl)
        
            'excelへ書き込む
            Call csvWrite.ReadAllCtmInfoToExcel(Workbooks(thisBookName), unZipUrl, MaxCntImport, sType, GetTimezoneId(), MissionSheetName)
        
            '一時フォルダを削除
            zipFile.deleteFolder (unZipUrl)
            
            'pmzip成功する場合
            GetMissionInfo = 0
            'FOR COM高速化 zhengxiaoxue 2018/06/14 Modify End
        
        '* ミッションIDがなければ
        Else
        
            ReDim ctmValue(0)
            ctmValue(0).ID = ""
            For II = 0 To UBound(missionList(mIndex).CTM)
        
                currCTM = missionList(mIndex).CTM(II)
                ctmID = currCTM.ID
            
                getEndTime = endTime
                getStartTime = StartTime
                
                '* ISSUE_NO.658 sunyi 2018/04/24 start
                '* element削除時のデータ更新、性能アップ
'                SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/ctm/find?testing=0"
                SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/ctm/findzip?testing=0"
                '* ISSUE_NO.658 sunyi 2018/04/24 end
                
                If useProxy = False Then
                    Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                Else
                    Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                    httpObj.setProxy 2, proxyUri
                End If
    
                httpObj.Open "POST", SendUrlStr, False
                httpObj.setRequestHeader "Content-Type", "text/plain"
                httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***Content-Type: text/plain
                
                SendString = "{""ctmId"":""" & ctmID & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & "}"
                
                bPmary = StrConv(SendString, vbFromUnicode)
            
                Call httpObj.send(SendString)
                
                ' ダウンロード待ち
                Do While httpObj.readyState <> 4
                    DoEvents
                Loop
                
                '* 取得情報のデコード
                Set sc = CreateObject("ScriptControl")
                With sc
                    .Language = "JScript"
            
                    '指定したインデックス、名称のデータを取得する
                    .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
                End With
                
                '* ISSUE_NO.658 sunyi 2018/04/24 start
                '* element削除時のデータ更新、性能アップ
''                GetString = convTextEncoding(httpObj.responseBody, "UTF-8")
'                GetString = httpObj.responseBody
'                Set objJSON = sc.CodeObject.jsonParse(GetString)
'
'                Set sc = Nothing
'
'                Set httpObj = Nothing
'
'                '* 取得情報をCTM情報に展開
'                iResult = GetCTMInfoForCTM(objJSON, currCTM, ctmValue)
                sZipFolder = bdr.GetMissionCtmPath()
                                
                If httpObj.Status = 200 Then
                    Set oStream = CreateObject("ADODB.Stream")
                    oStream.Open
                    oStream.Type = 1
                    oStream.Write httpObj.responseBody
                    oStream.SaveToFile sZipFolder & "\data.zip", 2
                    oStream.Close
                End If
                UnZipFile sZipFolder & "\data.zip"
    
                If (bdr.ReadAllCtmInfoToExcel(Workbooks(thisBookName), sZipFolder, MaxCntImport, sType, GetTimezoneId())) Then
                    'find成功する場合
                    GetMissionInfo = 1
                Else
                    'find失敗する場合
                    GetMissionInfo = -1
                    Exit For
                End If
                
                If Dir(sZipFolder & "\*.*") <> "" Then Kill sZipFolder & "\*.*"
                If Dir(sZipFolder) <> "" Then RmDir sZipFolder
                '* ISSUE_NO.658 sunyi 2018/04/24 end
            
            Next
        
        End If
    
    Next
    
    'FOR COM高速化 zhengxiaoxue 2018/06/14 Delete Start
    ' MsgBox http.responseText
    'GetMissionInfo = 1
    'FOR COM高速化 zhengxiaoxue 2018/06/14 Delete End
    
End Function


'* ISSUE_NO.658 sunyi 2018/04/24 start
'* element削除時のデータ更新、性能アップ
Public Sub UnZipFile(ByVal SrcPath As Variant, _
                     Optional ByVal DestFolderPath As Variant = "")
'ZIPファイルを解凍
'SrcPath：元ファイル
'DestFolderPath：出力先、指定しない場合は元ファイルと同じ場所
'※出力先に同名ファイルがあった場合はユーザー判断で処理
  With CreateObject("Scripting.FileSystemObject")
    If .FileExists(SrcPath) = False Then Exit Sub
    If LCase(.GetExtensionName(SrcPath)) <> "zip" Then Exit Sub
    If IsFolder(DestFolderPath) = False Then
      DestFolderPath = .GetFile(SrcPath).ParentFolder.Path
    End If
  End With
   
  With CreateObject("Shell.Application")
    .Namespace(DestFolderPath).CopyHere .Namespace(SrcPath).Items
  End With
End Sub
Private Function IsFolder(ByVal SrcPath As String) As Boolean
  IsFolder = CreateObject("Scripting.FileSystemObject").FolderExists(SrcPath)
End Function
'* ISSUE_NO.658 sunyi 2018/04/24 end

