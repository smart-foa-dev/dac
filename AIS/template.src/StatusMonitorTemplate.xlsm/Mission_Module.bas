Attribute VB_Name = "Mission_Module"
Option Explicit

'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
'Public Sub GetMissionInfoAll()
'    Dim ctmList()   As CTMINFO      ' CTM情報
'    Dim ctmValue()  As CTMINFO      ' CTM取得情報
'    Dim iResult     As Integer
'    Dim II          As Integer
'
'    iResult = GetMissionInfo(ctmList, ctmValue)
'
'    '* シート毎（シート名＝CTM名）へ出力
'    If iResult > 0 Then
'
'        '* CTM種別でシートに出力
'        For II = 0 To UBound(ctmList)
'
'            Call OutputCTMInfoToSheet(ctmList(II).Name, 0#, ctmValue)
'
'        Next
'
'    End If
'
'End Sub

'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
'* ISSUE_NO.658 sunyi 2018/04/24 start
'* element削除時のデータ更新、性能アップ
'Public Sub GetMissionInfoAll()
Public Sub GetMissionInfoAll(sType As String)
'* ISSUE_NO.658 sunyi 2018/04/24 end
    Dim ctmList()       As CTMINFO      ' CTM情報
    Dim ctmValue()      As CTMINFO      ' CTM取得情報
    Dim iResult         As Integer
    Dim II              As Integer
    Dim mIndex          As Integer
    Dim srchRange       As Range
    Dim lastRange       As Range
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim selectStr       As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim dispTerm        As Double
    Dim dispTermUnit    As String
    Dim missionList()   As MISSIONINFO
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
'    With currWorksheet
'
'        Set srchRange = .Cells.Find(What:="オンライン種別", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            If IsEmpty(srchRange.Offset(0, 1).Value) Then
'                selectStr = "連続移動"
'            Else
'                selectStr = srchRange.Offset(0, 1).Value
'            End If
'        Else
'                selectStr = "期間固定"
'        End If
'
'    End With
    
    '* ステータスモニターの場合は連続移動型固定
    selectStr = "連続移動"


    '* ISSUE_NO.658 sunyi 2018/04/24 start
    '* element削除時のデータ更新、性能アップ
'    '* ミッション情報から指定期間のCTM情報を取得する
'    iResult = GetMissionInfo(missionList, ctmValue)
    iResult = GetMissionInfo(missionList, ctmValue, sType)
    '* ISSUE_NO.658 sunyi 2018/04/24 end
    '* 表示期間の設定
    Select Case selectStr
        Case "期間固定"
            dispTerm = 0#
        Case "連続移動"
            '*******************************
            '* 取得期間を取得
            '*******************************
            Set srchRange = currWorksheet.Cells.Find(What:="取得期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                If IsEmpty(srchRange.Offset(0, 1).Value) Then
                    dispTerm = 1#
                    dispTermUnit = "【時】"
                Else
                    dispTerm = srchRange.Offset(0, 1).Value
                    dispTermUnit = "【時】"
'                    dispTermUnit = srchRange.Offset(0, 2).Value
                End If
            Else
                dispTerm = 1#
                dispTermUnit = "【時】"
            End If
            Select Case dispTermUnit
                Case "【分】"
                    dispTerm = dispTerm * 60 * (-1)
                Case "【時】"
                    dispTerm = dispTerm * 3600 * (-1)
            End Select
    End Select

    '* resultシート上の以前のデータを削除する
    '* → ステータスモニターはresultシートなし
'    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
'    With currWorksheet
'        Set srchRange = currWorksheet.Range("A2")
'        If srchRange.Value <> "" Then
'            Set lastRange = srchRange.End(xlToRight)
'            Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
'            workRange.Delete
'        End If
'    End With

    For mIndex = 0 To UBound(missionList)

        '* 取得CTM毎に処理
        For II = 0 To UBound(missionList(mIndex).CTM)

            '* 取得データがあれば該当CTMシートへ出力
            If iResult > 0 Then
                Call OutputCTMInfoToSheet(missionList(mIndex).CTM(II).Name, dispTerm, ctmValue, False)
            End If

            '* 連続移動型の場合、表示期間外のデータを削除する
            If dispTerm < 0 Then

                '***********************************
                '* 表示期間開始日時を取得
                '* →現時刻から表示期間を引いた日時
                '***********************************
                prevDate = Now
                prevTime = DateAdd("s", dispTerm, prevDate)

                '* 表示期間外のデータを削除
                Call DeleteOutrangeData(Workbooks(thisBookName).Worksheets(missionList(mIndex).CTM(II).Name), prevTime, "A1")

            End If

            '* resultシートに転記
            '* → ステータスモニターはresultシートなし
    '        Call OutputResultToSheet(ctmList(II).Name)

        Next

    Next

    '* resultシートをRECEIVE TIMEに従いソートする
    '* → ステータスモニターはresultシートなし
'    With currWorksheet
'        Set srchRange = .Range("A1")
'        Set lastRange = srchRange.End(xlToRight)
'        Set workRange = srchRange.End(xlDown)
'        Set lastRange = .Cells(workRange.Row, lastRange.Column)
'        Set workRange = .Range(srchRange, lastRange)
'        workRange.Sort Key1:=.Range("B1"), _
'                     Order1:=xlDescending, _
'                     Header:=xlGuess
'    End With

    '前回更新日時セット
'    Set srchRange = Workbooks(thisBookName).Worksheets(MainSheetName).Cells.Find(What:="前回起動", LookAt:=xlWhole)
'    If Not srchRange Is Nothing Then
'        srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd")
'        srchRange.Offset(0, 2).Value = Format(Now, "hh:mm:ss")
'    End If
End Sub

'***********************************************
'* resultシートから表示期間外のデータを削除する
'***********************************************
Public Sub DeleteOutrangeData(currWorksheet As Worksheet, endDate As Date, colTime As String)
    Dim workRange       As Range
    Dim lastRange       As Range
    Dim srchRange       As Range
    Dim aaa             As Variant
    Dim II              As Integer
    Dim startRow        As Integer
    Dim EndRow          As Integer
    
'    Application.ScreenUpdating = False
    
    startRow = -1
    EndRow = -1
    
    With currWorksheet
        Set srchRange = .Range(colTime)
        If srchRange.Offset(1, 0).Value <> "" Then
            Set lastRange = srchRange.End(xlDown)
            EndRow = lastRange.Row
            For II = 0 To EndRow - 1
                If srchRange.Offset(II, 0).Value < endDate And startRow < 0 Then
                    startRow = srchRange.Offset(II, 0).Row
                    Exit For
                End If
            Next
            
            '* 表示期間内のデータがなければ処理中断
            If startRow < 0 Then Exit Sub
    
            Set workRange = .Range(startRow & ":" & EndRow)
            workRange.Delete
        End If

    End With
    
'    Application.ScreenUpdating = True
    
End Sub

'****************************************************
'* 収集したCTM情報をresultシートに転記する
'****************************************************
Public Sub OutputResultToSheet(currSheetName As String)
    Dim currWorksheet       As Worksheet
    Dim srcWorksheet        As Worksheet
    Dim srchRange           As Range
    Dim lastRange           As Range
    Dim workRange           As Range
    Dim copyRange           As Range
    Dim destRange           As Range
    Dim startRange          As Range
    Dim ctmList()           As CTMINFO
    Dim II                  As Integer
    Dim pasteArray()        As Variant

    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
    
    Set srcWorksheet = Workbooks(thisBookName).Worksheets(currSheetName)

    '* resultシートにデータを複写する
    Set srchRange = srcWorksheet.Range("A2")
    Set destRange = currWorksheet.Range("B2")
    If destRange.Value <> "" Then
        Set destRange = currWorksheet.Range("B1").End(xlDown).Offset(1, 0)
    End If
    Set lastRange = srchRange.Offset(0, 0).End(xlToRight)
    Set copyRange = srcWorksheet.Range(srchRange.Cells, lastRange.Offset(0, 0).End(xlDown).Cells)
    copyRange.Copy Destination:=destRange
    
    '* CTM名を生成する
    II = lastRange.Offset(0, 0).End(xlDown).Row - srchRange.Row
    ReDim pasteArray(II)
    For II = 0 To UBound(pasteArray)
        pasteArray(II) = currSheetName
    Next
    
    '* CTM名を貼り付ける
    Set destRange = destRange.Offset(0, -1)
    Set lastRange = destRange.Offset(UBound(pasteArray), 0)
    Set copyRange = currWorksheet.Range(destRange.Cells, lastRange.Cells)
    copyRange = pasteArray
    
    '* RECEIVE TIME列の書式設定
    With currWorksheet
        Set workRange = currWorksheet.Range(.Range("B2"), .Range("B2").End(xlDown).Cells)
        workRange.NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
        workRange.EntireColumn.AutoFit
    End With
End Sub

'**********************
'* 収集開始日時の取得
'* →返り値はUNIX TIME
'**********************
Public Function GetCollectStartDT(currWorksheet As Worksheet) As Double
    Dim srcWorksheet        As Worksheet
    Dim StartTime           As Double
    Dim startD              As Date
    Dim startT              As Date
    Dim prevDate            As Date
    Dim prevTime            As Date
    Dim ctmList()           As CTMINFO
    Dim II                  As Integer
    Dim recieveTimeArray()  As Double
    Dim iFirstFlag          As Boolean
    Dim recieveTime         As Variant
    Dim startRange          As Range
    Dim srchRange           As Range
    Dim rtIndex             As Integer
    Dim missionList()       As MISSIONINFO
    Dim currDate            As Date
    Dim dateStr             As String
    Dim dispTerm        As Double
    Dim dispTermUnit    As String
    
    '**********************************
    '* 全CTMシートの最新時刻を取得する
    '**********************************
    '* CTMリストを取得する
'    Call GetCTMList(currWorksheet.Name, ctmList)
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    
    '* CTM情報一覧をシートから取得
    iFirstFlag = True
    For Each srcWorksheet In Worksheets
    
        '* CTM情報一覧に存在するシートのみを処理対象とする
        For II = 0 To UBound(missionList(0).CTM)
            If (missionList(0).CTM(II).Name = srcWorksheet.Name) Then Exit For
        Next
        If II <= UBound(missionList(0).CTM) Then

            '* 最上位のRECIEVE TIMEを取得する
            Set startRange = srcWorksheet.Range("A2")
            For II = 0 To 100
                recieveTime = startRange.Offset(II, 0).Value
                If recieveTime <> "" Then
                    Exit For
                End If
            Next
            '* 100行検索しても見つからなければ処理中断＝データ無
            If II > 100 Then
                Exit For
            End If
            
            '* 取得した値が日付形式ならば
'            dateStr = Left(recieveTime, Len(recieveTime) - 4)
            dateStr = Format(recieveTime, "yyyy/MM/dd hh:mm:ss")
            currDate = CDate(dateStr)
            If IsDate(currDate) Then
            
                If iFirstFlag Then
                    rtIndex = 0
                    ReDim recieveTimeArray(rtIndex)
                    iFirstFlag = False
                Else
                    rtIndex = UBound(recieveTimeArray) + 1
                    ReDim Preserve recieveTimeArray(rtIndex)
                End If
                
                recieveTimeArray(rtIndex) = GetUnixTime(currDate)
                
            Else
                Exit For
            End If

        End If
    
    Next
        
    If iFirstFlag Then
        recieveTime = 0#
    Else
        '* 取得した時刻群の最小の時刻を取得
        recieveTime = Application.WorksheetFunction.Min(recieveTimeArray)
    End If

    '************************************************************
    '* paramシートから収集開始日時を現在時刻−取得期間で算出する
    '************************************************************
    With currWorksheet
    
        Set srchRange = currWorksheet.Cells.Find(What:="取得期間", LookAt:=xlWhole)
        dispTermUnit = "【時】"
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                dispTerm = 1#
            Else
                dispTerm = srchRange.Offset(0, 1).Value
            End If
        Else
            dispTerm = 1#
        End If
        Select Case dispTermUnit
            Case "【分】"
                dispTerm = dispTerm * 60 * (-1)
            Case "【時】"
                dispTerm = dispTerm * 3600 * (-1)
        End Select

        prevDate = Now
        prevTime = DateAdd("s", dispTerm, prevDate)
        StartTime = GetUnixTime(prevTime)
    
    End With

'    With currWorksheet
'
'        Set srchRange = .Cells.Find(What:="取得開始", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            If srchRange.Offset(0, 1).Value <> "" Then
'                startD = DateValue(srchRange.Offset(0, 1).Value)
'                startT = TimeValue(srchRange.Offset(0, 1).Value)
'            Else
'                startD = DateValue("2016/1/1 00:00:00")ｓｓ
'                startT = TimeValue("2016/1/1 00:00:00")
'            End If
'        Else
'            startD = DateValue("2016/1/1 00:00:00")
'            startT = TimeValue("2016/1/1 00:00:00")
'        End If
'        If Not IsDate(startD + startT) Then
'            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
'            GetCollectStartDT = -1
'            Exit Function
'        End If
'        StartTime = GetUnixTime(startD + startT)
'
'    End With
    
    GetCollectStartDT = Application.WorksheetFunction.Max(recieveTime, StartTime)

End Function

'*************************************************************************
'* 取得したJSON情報をデコードしCTM情報およびエレメント情報として展開する
'* →ミッションIDによる取得
'* ※VBA-JSON用
'*************************************************************************
Public Function GetCTMInfoNew(objJSON As Object, ctmList() As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim JsonText        As String
    Dim Parsed          As Object
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    Dim LL              As Integer
    Dim ctmIndex        As Integer
    Dim eleIndex        As Integer
    Dim aryIndex        As Integer
    Dim aryIndex2       As Integer
    Dim ctmID           As String
    Dim ctmName         As String
    Dim ctmNum          As String
    Dim receiveTime     As String
    Dim ctmArray        As Variant
    Dim eleArray        As Variant
    Dim keysArray       As Variant
    Dim keysArray2      As Variant
    Dim eleInfo         As Variant
    Dim wValue          As String
    Dim iFirstFlag      As Boolean
    Dim recno           As Integer
    
    iFirstFlag = True
    
'    JsonText = ReadAll("C:\Users\FOA\Documents\00_work_by_fukushima\work\template\VBA-json_test\test.json")
    
    ' Parse json to Dictionary
    ' "values" is parsed as Collection
    ' each item in "values" is parsed as Dictionary
'    Set Parsed = JsonConverter.ParseJson(JsonText)
    
'    Call DumpCollection(Parsed)

'    Debug.Print "CTMの種類数:" & Parsed.Count
    
    '* CTMの種類分ループ
    recno = 0
    For II = 1 To objJSON.Count
    
        '* CTM ID取得
        ctmID = objJSON.Item(II).Item("id")
        '* CTM名称取得
        ctmName = objJSON.Item(II).Item("name")
        '* CTM件数取得
        ctmNum = objJSON.Item(II).Item("num")
        Debug.Print "CTM ID:" & ctmID & ", 名称:" & ctmName & ", 件数:" & ctmNum
        
        '* 対象CTMを確定する
        For ctmIndex = 0 To UBound(ctmList)
            If ctmList(ctmIndex).ID = ctmID Then
                Exit For
            End If
        Next
        
        If ctmIndex <= UBound(ctmList) Then
            
            '* 取得CTMのオブジェクトを取得
            Set ctmArray = objJSON.Item(II).Item("ctms")
            
            '* CTMの件数分ループ
            For JJ = 1 To ctmArray.Count
                
                If ctmValue(0).ID = "" Then
                    aryIndex = 0
                Else
                    aryIndex = UBound(ctmValue) + 1
                    ReDim Preserve ctmValue(aryIndex)
                End If
                ReDim ctmValue(aryIndex).Element(0)
                ctmValue(aryIndex).Element(0).Name = ""
            
                '* 件数カウンタアップ
                recno = recno + 1
                
                '* RECEIVE TIMEの取得
                receiveTime = ctmArray.Item(JJ).Item("RT")
                Debug.Print "RECEIVE TIME:" & receiveTime
                
                '* CTM情報の取得
                ctmValue(aryIndex).Name = ctmList(ctmIndex).Name
                ctmValue(aryIndex).ID = ctmList(ctmIndex).ID
                ctmValue(aryIndex).RecvTime = receiveTime
    
                '* エレメントオブジェクトの取得
                Set eleArray = ctmArray.Item(JJ).Item("EL")
                
                '* エレメントIDリストの取得
                keysArray = eleArray.keys
                For KK = 0 To UBound(keysArray)
    
                    Debug.Print "エレメントID:" & keysArray(KK)
                    
                    '* 対象エレメントを確定する
                    For eleIndex = 0 To UBound(ctmList(ctmIndex).Element)
                        If ctmList(ctmIndex).Element(eleIndex).ID = keysArray(KK) Then
                            Exit For
                        End If
                    Next
                    
                    If eleIndex <= UBound(ctmList(ctmIndex).Element) Then
    
                        aryIndex2 = UBound(ctmValue(aryIndex).Element)
                        If ctmValue(aryIndex).Element(aryIndex2).Name <> "" Then
                            aryIndex2 = aryIndex2 + 1
                            ReDim Preserve ctmValue(aryIndex).Element(aryIndex2)
                        End If
                            
                        '* エレメント情報オブジェクトの取得
                        Set eleInfo = eleArray.Item(keysArray(KK))
        
                        '* エレメント情報(型、値、(Bulky指定ならばファイル名))
                        keysArray2 = eleInfo.keys
                        For LL = 0 To UBound(keysArray2)
        
                            wValue = eleInfo.Item(keysArray2(LL))
                            Debug.Print keysArray2(LL) & ":" & wValue
        
                            '* エレメント情報の取得
                            ctmValue(aryIndex).Element(aryIndex2).Name = ctmList(ctmIndex).Element(eleIndex).Name
                            ctmValue(aryIndex).Element(aryIndex2).ID = ctmList(ctmIndex).Element(eleIndex).ID
                            Select Case keysArray2(LL)
                                Case "T"
                                    ctmValue(aryIndex).Element(aryIndex2).Type = wValue
                                Case "V"
                                    ctmValue(aryIndex).Element(aryIndex2).Value = wValue
                                Case "FN"
                                    ctmValue(aryIndex).Element(aryIndex2).File = wValue
                            End Select
                            '* 取得した値が空白ならば、半角空白をセットしておく
                            '* ISSUE_NO.659 sunyi 2018/04/25 start
                            '* スペースを設定しません
                            'If ctmValue(aryIndex).Element(aryIndex2).Value = "" Then ctmValue(aryIndex).Element(aryIndex2).Value = " "
                            '* ISSUE_NO.659 sunyi 2018/04/25 end
                        Next
                    
                    End If
                    
                Next
            
            Next
        
        End If
        
    Next
    
    GetCTMInfoNew = recno

End Function

'*************************************************************************
'* 取得したJSON情報をデコードしCTM情報およびエレメント情報として展開する
'* →ミッションIDによる取得
'*************************************************************************
Public Function GetCTMInfo(objJSON As Object, ctmList() As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim recno           As Integer
    Dim II              As Integer
    Dim rec             As Object
    Dim rec2            As Object
    Dim rec3            As Object
    Dim rec4            As Object
    Dim aryIndex        As Integer
    Dim aryIndex2       As Integer
    Dim ctmIndex        As Integer
    Dim workElm         As ELMINFO
    Dim iFirstFlag      As Boolean
    
    iFirstFlag = True
    
    ' 件数カウンタ初期化
    recno = 0
    
    '* CTM種別数分ループ
    For Each rec In objJSON
    
        '* 対象CTMを確定する
        For II = 0 To UBound(ctmList)
            If ctmList(II).ID = GetJSONValue(rec, "id") Then
                ctmIndex = II
                Exit For
            End If
        Next
        
        If II <= UBound(ctmList) Then
        
            '* CTM取得件数分ループ
            II = rec.num
            For Each rec2 In rec.ctms
    
                If ctmValue(0).ID = "" Then
                    aryIndex = 0
                Else
                    aryIndex = UBound(ctmValue) + 1
                    ReDim Preserve ctmValue(aryIndex)
                End If
                ReDim ctmValue(aryIndex).Element(0)
                ctmValue(aryIndex).Element(0).Name = ""
                
                '* CTM情報の取得
                ctmValue(aryIndex).Name = ctmList(ctmIndex).Name
                ctmValue(aryIndex).ID = ctmList(ctmIndex).ID
                ctmValue(aryIndex).RecvTime = GetJSONValue(rec2, "RT")
    
                '* 件数カウンタアップ
                recno = recno + 1
                
                '* エレメント数分の値取得
                Set rec3 = CallByName(rec2, "EL", VbGet)
                For II = 0 To UBound(ctmList(ctmIndex).Element)
                
                    workElm = ctmList(ctmIndex).Element(II)
                    
                    Set rec4 = CallByName(rec3, workElm.ID, VbGet)
                    
                    aryIndex2 = UBound(ctmValue(aryIndex).Element)
                    If ctmValue(aryIndex).Element(aryIndex2).Name <> "" Then
                        aryIndex2 = aryIndex2 + 1
                        ReDim Preserve ctmValue(aryIndex).Element(aryIndex2)
                    End If
                    
                    '* エレメント情報の取得
                    ctmValue(aryIndex).Element(aryIndex2).Name = workElm.Name
                    ctmValue(aryIndex).Element(aryIndex2).ID = workElm.ID
                    ctmValue(aryIndex).Element(aryIndex2).Type = CInt(GetJSONValue(rec4, "T"))
                    On Error Resume Next
                    ctmValue(aryIndex).Element(aryIndex2).Value = GetJSONValue(rec4, "V")
                    '* ISSUE_NO.659 sunyi 2018/04/25 start
                    '* スペースを設定しません
                    'If ctmValue(aryIndex).Element(aryIndex2).Value = "" Then ctmValue(aryIndex).Element(aryIndex2).Value = " "
                    '* ISSUE_NO.659 sunyi 2018/04/25 end
                Next
                
            Next
        
        End If
        
    Next
    
    GetCTMInfo = recno
    
End Function

'*************************************************************************
'* 取得したJSON情報をデコードしCTM情報およびエレメント情報として展開する
'* →CTM IDによる取得
'*************************************************************************
Public Function GetCTMInfoForCTM(objJSON As Object, ctmList As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim recno           As Integer
    Dim II              As Integer
    Dim rec             As Object
    Dim rec2            As Object
    Dim rec3            As Object
    Dim rec4            As Object
    Dim aryIndex        As Integer
    Dim aryIndex2       As Integer
    Dim ctmIndex        As Integer
    Dim workElm         As ELMINFO
    Dim iFirstFlag      As Boolean
    
    iFirstFlag = True
    
    ' 件数カウンタ初期化
    recno = 0
    
    '* CTM取得件数分ループ
    For Each rec2 In objJSON

        If ctmValue(0).ID = "" Then
            aryIndex = 0
        Else
            aryIndex = UBound(ctmValue) + 1
            ReDim Preserve ctmValue(aryIndex)
        End If

        ReDim ctmValue(aryIndex).Element(0)
        ctmValue(aryIndex).Element(0).Name = ""
        
        '* CTM情報の取得
        ctmValue(aryIndex).Name = ctmList.Name
        ctmValue(aryIndex).ID = ctmList.ID
        ctmValue(aryIndex).RecvTime = GetJSONValue(rec2, "RT")

        '* 件数カウンタアップ
        recno = recno + 1
        
        '* エレメント数分の値取得
        Set rec3 = CallByName(rec2, "EL", VbGet)
        For II = 0 To UBound(ctmList.Element)
        
            workElm = ctmList.Element(II)
            
            Set rec4 = CallByName(rec3, workElm.ID, VbGet)
            
            aryIndex2 = UBound(ctmValue(aryIndex).Element)
            If ctmValue(aryIndex).Element(aryIndex2).Name <> "" Then
                aryIndex2 = aryIndex2 + 1
                ReDim Preserve ctmValue(aryIndex).Element(aryIndex2)
            End If
            
            '* エレメント情報の取得
            ctmValue(aryIndex).Element(aryIndex2).Name = workElm.Name
            ctmValue(aryIndex).Element(aryIndex2).ID = workElm.ID
            ctmValue(aryIndex).Element(aryIndex2).Type = CInt(GetJSONValue(rec4, "T"))
            On Error Resume Next
            ctmValue(aryIndex).Element(aryIndex2).Value = GetJSONValue(rec4, "V")
            '* ISSUE_NO.659 sunyi 2018/04/25 start
            '* スペースを設定しません
            'If ctmValue(aryIndex).Element(aryIndex2).Value = "" Then ctmValue(aryIndex).Element(aryIndex2).Value = " "
            '* ISSUE_NO.659 sunyi 2018/04/25 end
        Next
        
    Next
        
    GetCTMInfoForCTM = recno
    
End Function

'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Public Sub OutputCTMInfoToSheet(currSheetName As String, dispTerm As Double, ctmValue() As CTMINFO, termFlag As Boolean)
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    Dim currWorksheet   As Worksheet
    Dim writeIndex      As Integer
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim delRange        As Range
    Dim writeRange      As Range
    Dim receiveTime     As Double
    Dim dtRecvL         As Long
    Dim dtRecv          As Date
    Dim ms              As Integer
    Dim strWork         As String
    Dim nowTime         As Double
    Dim endTime         As Double
    Dim writeArray()    As Variant
    Dim writeArray2()   As Variant
    Dim iFirstFlag      As Boolean
    Dim readIndex       As Integer
    Dim latestTime      As Double
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim elementNum      As Integer
    
    '* 出力先シートの取得
    Set currWorksheet = Workbooks(thisBookName).Worksheets(currSheetName)
    
    With currWorksheet
        
        '* 書き出し位置の設定
        Set startRange = .Cells(2, 1)
        
        '* 現在記載されている最新時刻を取得
        latestTime = GetUnixTime(startRange.Offset(0, 0).Value2)
        
        '* 再度書き出し位置の設定
        '* ※全情報を一旦削除したため再設定が必要
        Set startRange = .Cells(2, 1)
        writeIndex = 0
        
'        Application.ScreenUpdating = False
        
        iFirstFlag = True
        elementNum = 0
        For JJ = 0 To UBound(ctmValue)
        
            If currSheetName = ctmValue(JJ).Name Then
            
                receiveTime = CDbl(ctmValue(JJ).RecvTime)
                dtRecvL = CLng(receiveTime / 1000)
                
                '* 処理時刻が記載最新時刻以前ならば処理終了
                ' If (receiveTime <= latestTime + 1) Then Exit For
                If (receiveTime > (latestTime + 1#)) Then

                    ms = CInt(Right(ctmValue(JJ).RecvTime, 3))
                    receiveTime = ((dtRecvL + 32400) / 86400) + 25569
                    dtRecv = CDate(receiveTime)
                    strWork = Format(dtRecv, "yyyy/MM/dd hh:mm:ss") + "." + Format(ms, "000")
                    
                    If iFirstFlag Then
                        readIndex = 0
                        ReDim writeArray(UBound(ctmValue(JJ).Element) + 2, readIndex)
                        iFirstFlag = False
                    Else
                        readIndex = UBound(writeArray, 2) + 1
                        ReDim Preserve writeArray(UBound(ctmValue(JJ).Element) + 2, readIndex)
                    End If
                    
                    '* RECIEVE TIMEをセット
                    writeArray(0, readIndex) = strWork
                    
                    '* CTM NAMEをセット
    '                writeArray(1, readIndex) = ctmValue(JJ).Name
                    
                    elementNum = UBound(ctmValue(JJ).Element)
                
                    For KK = 0 To elementNum
                    
                        writeArray(KK + 1, readIndex) = ctmValue(JJ).Element(KK).Value
                    
                    Next
                    
                    writeIndex = writeIndex + 1
                
                End If
            End If
            
            DoEvents
        
        Next

On Error Resume Next
        
        '* 取得CTMの差分出力
        If Not iFirstFlag Then
        
            '* 書き込み先範囲の取得
            Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, elementNum + 2))
            '* 配列の次元入れ替え
            writeArray2 = WorksheetFunction.Transpose(writeArray)
            '* 指定範囲に書き込み
            writeRange.Insert Shift:=xlShiftDown
            Set startRange = .Cells(2, 1)
            Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, elementNum + 2))
            writeRange = writeArray2
            
            '* RECIVE TIMEの書式を設定
            Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, 0))
            writeRange.NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
            '* カラム幅を調整
            writeRange.EntireColumn.AutoFit
        
        End If
            
'        Application.ScreenUpdating = True
        
    End With
    
End Sub

'***********************************************
'* paramシートにあるミッション情報を取得する
'***********************************************
Public Sub GetCtmList(currSheetName As String, ctmList() As CTMINFO)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim ctmRange        As Range
    Dim elmRange        As Range
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim aryIndex        As Integer      ' CTM情報インデックス
    Dim aryIndex2       As Integer      ' エレメント情報インデックス
    Dim iFirstFlag      As Boolean      ' 配列初回フラグ
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    
    iFirstFlag = True
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim ctmList(0)
    
    With currWorksheet
    
'        .Activate
        
        '* CTM NAMEの項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        
        For II = 0 To MaxCTMNumber Step 2
            Set ctmRange = startRange.Offset(II, 0)
            
            If ctmRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            If iFirstFlag Then
                aryIndex = 0
                ReDim ctmList(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(ctmList) + 1
                ReDim Preserve ctmList(aryIndex)
            End If
            ReDim ctmList(aryIndex).Element(0)
            ctmList(aryIndex).Element(0).Name = ""
            
            '* CTM情報取得
            ctmList(aryIndex).Name = ctmRange.Offset(0, 0).Value
            ctmList(aryIndex).ID = ctmRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = ctmRange.Offset(0, 1)
            For JJ = 0 To MaxELMNumber
                If elmRange.Offset(0, JJ).Value = "" Then
                    Exit For
                End If
                
                aryIndex2 = UBound(ctmList(aryIndex).Element)
                If ctmList(aryIndex).Element(aryIndex2).Name <> "" Then
                    aryIndex2 = aryIndex2 + 1
                    ReDim Preserve ctmList(aryIndex).Element(aryIndex2)
                End If
                ctmList(aryIndex).Element(aryIndex2).Name = elmRange.Offset(0, JJ).Value
                ctmList(aryIndex).Element(aryIndex2).ID = elmRange.Offset(1, JJ).Value
            Next
            
                
        Next
    
    End With
    
End Sub


'***********************************************
'* ミッションシートにあるミッションリストを取得する
'***********************************************
Public Sub GetMissionList(currSheetName As String, missionList() As MISSIONINFO)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim msnRange        As Range
    Dim ctmRange        As Range
    Dim elmRange        As Range
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim aryIndex        As Integer      ' CTM情報インデックス
    Dim aryIndex2       As Integer      ' エレメント情報インデックス
    Dim iFirstFlag      As Boolean      ' 配列初回フラグ
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    Dim KK              As Integer      ' For文用カウンタ
    Dim missionID       As Variant
    Dim ctmID           As Variant
    
    iFirstFlag = True
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim missionList(0)
    
    With currWorksheet
    
'        .Activate
        
        '* ミッションシートのA1セルをスタートセルとする
        Set startRange = .Range("A1")
        
        '* ミッションリストを取得する
        iFirstFlag = True
        For II = 0 To 100
            Set msnRange = startRange.Offset(II, 0)
            
          '2017/09/15 No.533 Mod.
          'If msnRange.Offset(0, 0).Value = "" Then
          '     Exit For
          'End If
            If msnRange.Offset(0, 0).Value <> "" Then
                If iFirstFlag Then
                    aryIndex = 0
                    ReDim missionList(aryIndex)
                    iFirstFlag = False
                Else
                    aryIndex = UBound(missionList) + 1
                    ReDim Preserve missionList(aryIndex)
                End If
                ReDim missionList(aryIndex).CTM(0)
                missionList(aryIndex).CTM(0).ID = ""
                
                '* ミッション情報取得
                missionList(aryIndex).ID = msnRange.Offset(0, 0).Value
                missionList(aryIndex).Name = msnRange.Offset(0, 1).Value
            End If
        Next
            
        '* CTMリストを取得する
        iFirstFlag = True
        For II = 0 To 100
            Set ctmRange = startRange.Offset(II, 2)
            
            '* 所属ミッションIDを取得
            missionID = ctmRange.Offset(0, 0).Value
            If missionID = "" Then
                Exit For
            End If
            
            '* ミッションID検索
            For JJ = 0 To UBound(missionList)
                If missionList(JJ).ID = missionID Then Exit For
            Next
            
            If missionList(JJ).CTM(0).ID = "" Then
                aryIndex = 0
                ReDim missionList(JJ).CTM(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(missionList(JJ).CTM) + 1
                ReDim Preserve missionList(JJ).CTM(aryIndex)
            End If
            ReDim missionList(JJ).CTM(aryIndex).Element(0)
            missionList(JJ).CTM(aryIndex).Element(0).ID = ""
            
            '* ミッション情報取得
            missionList(JJ).CTM(aryIndex).ID = ctmRange.Offset(0, 1).Value
            missionList(JJ).CTM(aryIndex).Name = ctmRange.Offset(0, 2).Value
        
        Next
            
        '* エレメントリストを取得する
        iFirstFlag = True
        '2017/09/29 No.543 Mod. Changed 100 to 5000. 暫定で5000を設定((参考)256Element/1CTM×12CTM=3072)
        'For II = 0 To 100
        For II = 0 To 5000
            Set elmRange = startRange.Offset(II, 5)
            
            '* 所属CTM IDを取得
            ctmID = elmRange.Offset(0, 0).Value
            If ctmID = "" Then
                Exit For
            End If
            
            '* CTM ID検索
            For JJ = 0 To UBound(missionList)
                For KK = 0 To UBound(missionList(JJ).CTM)
                    If missionList(JJ).CTM(KK).ID = ctmID Then Exit For
                Next
                If KK <= UBound(missionList(JJ).CTM) Then Exit For
            Next
            
            If missionList(JJ).CTM(KK).Element(0).ID = "" Then
                aryIndex = 0
                ReDim missionList(JJ).CTM(KK).Element(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(missionList(JJ).CTM(KK).Element) + 1
                ReDim Preserve missionList(JJ).CTM(KK).Element(aryIndex)
            End If
            
            '* ミッション情報取得
            missionList(JJ).CTM(KK).Element(aryIndex).ID = elmRange.Offset(0, 1).Value
            missionList(JJ).CTM(KK).Element(aryIndex).Name = elmRange.Offset(0, 2).Value
        
        Next
            
    End With
    
End Sub

