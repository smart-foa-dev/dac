VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmEngVal 
   Caption         =   "HwlÏ·æĘ"
   ClientHeight    =   4770
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   5520
   OleObjectBlob   =   "frmEngVal.frx":0000
   StartUpPosition =   1  'I[i[ tH[Ė
End
Attribute VB_Name = "frmEngVal"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Dim unitChar As String

Private Sub CommandButton1_Click()
    Dim currWorksheet    As Worksheet
    Dim srchRange           As Range
    Dim rc                        As Boolean
    
    rc = unitCheck(txtYoko1.text, txtYoko2.text)
    If rc = False Then
        MsgBox "ĄēÏ·@ÝčŲíII", vbExclamation
        Exit Sub
    End If
        
    rc = unitCheck(txtTate1.text, txtTate2.text)
        If rc = False Then
        MsgBox "cēÏ·@ÝčŲíII", vbExclamation
        Exit Sub
    End If
    
    'OtÎ·ÉÏ·lðÛķ
    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="|HwlÏ·|", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 1).Value = Trim(txtYoko1.text)
        srchRange.Offset(1, 3).Value = Trim(txtYoko2.text)
        srchRange.Offset(2, 1).Value = Trim(txtTate1.text)
        srchRange.Offset(2, 3).Value = Trim(txtTate2.text)
        
'        srchRange.Offset(1, 1).Value = ""
'        srchRange.Offset(1, 3).Value = ""
'        srchRange.Offset(2, 1).Value = ""
'        srchRange.Offset(2, 3).Value = ""
    End If
    
    If Trim(txtYoko1.text) <> "" And Trim(txtYoko1.text) <> "F" And Trim(txtTate1.text) <> "" And Trim(txtTate1.text) <> "F" Then
        Call reDrowGraph
    Else
        Select Case txtYoko1.text
            Case ""
                Call reDrowGraphTemp("ĻF", "Ą")
            Case "F"
                Call reDrowGraphTemp("FĻ", "Ą")
        End Select
        
        Select Case txtTate1.text
            Case ""
                Call reDrowGraphTemp("ĻF", "c")
            Case "F"
                Call reDrowGraphTemp("FĻ", "c")
        End Select
        
    End If
    
End Sub

Private Function unitCheck(ByVal argUnit1 As String, ByVal argUnit2 As String) As Boolean

    If argUnit1 = "" Then
        unitCheck = True
        Exit Function
    Else
        unitCheck = False
    End If

    unitCheck = False

    Select Case argUnit1
        'Case "ú", "", "Š", "b", "ÐØb"
        Case lblTime1, lblTime2, lblTime3, lblTime4, lblTime5
            Select Case argUnit2
                Case lblTime1, lblTime2, lblTime3, lblTime4, lblTime5
                        unitCheck = True
                Case Else
                        unitCheck = False
            End Select
        Case lblLen1, lblLen2, lblLen3, lblLen4, lblLen5, lblLen6, lblLen7
            Select Case argUnit2
                Case lblLen1, lblLen2, lblLen3, lblLen4, lblLen5, lblLen6, lblLen7
                        unitCheck = True
                Case Else
                        unitCheck = False
            End Select
        Case lblWght1, lblWght2, lblWght3, lblWght4
            Select Case argUnit2
                Case lblWght1, lblWght2, lblWght3, lblWght4
                        unitCheck = True
                Case Else
                        unitCheck = False
            End Select
        Case lblTemp1, lblTemp2
            Select Case argUnit2
                Case lblTemp1, lblTemp2
                        unitCheck = True
                Case Else
                        unitCheck = False
            End Select
        Case lblPrss1, lblPrss2, lblPrss3, lblPrss4
            Select Case argUnit2
                Case lblPrss1, lblPrss2, lblPrss3, lblPrss4
                        unitCheck = True
                Case Else
                        unitCheck = False
            End Select
        Case lblMnt1, lblMnt2, lblMnt3, lblMnt4
            Select Case argUnit2
                Case lblMnt1, lblMnt2, lblMnt3, lblMnt4
                        unitCheck = True
                Case Else
                        unitCheck = False
            End Select
    End Select

End Function

Private Sub lblLen1_Click()
    Call unitSet(lblLen1)
End Sub

Private Sub lblLen2_Click()
    Call unitSet(lblLen2)
End Sub

Private Sub lblLen3_Click()
    Call unitSet(lblLen3)
End Sub

Private Sub lblLen4_Click()
    Call unitSet(lblLen4)
End Sub

Private Sub lblLen5_Click()
    Call unitSet(lblLen5)
End Sub

Private Sub lblLen6_Click()
    Call unitSet(lblLen6)
End Sub

Private Sub lblLen7_Click()
    Call unitSet(lblLen7)
End Sub

Private Sub lblMnt1_Click()
    Call unitSet(lblMnt1)
End Sub

Private Sub lblMnt2_Click()
    Call unitSet(lblMnt2)
End Sub

Private Sub lblMnt3_Click()
    Call unitSet(lblMnt3)
End Sub

Private Sub lblMnt4_Click()
    Call unitSet(lblMnt4)
End Sub

Private Sub lblMnt5_Click()
    Call unitSet(lblMnt5)
End Sub

Private Sub lblMnt6_Click()
    Call unitSet(lblMnt6)
End Sub

Private Sub lblMnt7_Click()
    Call unitSet(lblMnt7)
End Sub

Private Sub lblPrss1_Click()
    Call unitSet(lblPrss1)
End Sub

Private Sub lblPrss2_Click()
    Call unitSet(lblPrss2)
End Sub

Private Sub lblPrss3_Click()
    Call unitSet(lblPrss3)
End Sub

Private Sub lblPrss4_Click()
    Call unitSet(lblPrss4)
End Sub

Private Sub lblPrss5_Click()
    Call unitSet(lblPrss5)
End Sub

Private Sub lblPrss6_Click()
    Call unitSet(lblPrss6)
End Sub

Private Sub lblPrss7_Click()
    Call unitSet(lblPrss7)
End Sub

Private Sub lblTemp1_Click()
    Call unitSet(lblTemp1)
End Sub

Private Sub lblTemp2_Click()
    Call unitSet(lblTemp2)
End Sub

Private Sub lblTemp3_Click()
    Call unitSet(lblTemp3)
End Sub

Private Sub lblTemp4_Click()
    Call unitSet(lblTemp4)
End Sub

Private Sub lblTemp5_Click()
    Call unitSet(lblTemp5)
End Sub

Private Sub lblTemp6_Click()
    Call unitSet(lblTemp6)
End Sub

Private Sub lblTemp7_Click()
    Call unitSet(lblTemp7)
End Sub

Private Sub lblTime1_Click()
    Call unitSet(lblTime1)
End Sub

Private Sub lblTime2_Click()
    Call unitSet(lblTime2)
End Sub

Private Sub lblTime3_Click()
    Call unitSet(lblTime3)
End Sub

Private Sub lblTime4_Click()
    Call unitSet(lblTime4)
End Sub

Private Sub lblTime5_Click()
    Call unitSet(lblTime5)
End Sub

Private Sub lblTime6_Click()
    Call unitSet(lblTime6)
End Sub

Private Sub lblTime7_Click()
    Call unitSet(lblTime7)
End Sub

Private Sub lblWght1_Click()
    Call unitSet(lblWght1)
End Sub

Private Sub lblWght2_Click()
    Call unitSet(lblWght2)
End Sub

Private Sub lblWght3_Click()
    Call unitSet(lblWght3)
End Sub

Private Sub lblWght4_Click()
    Call unitSet(lblWght4)
End Sub

Private Sub lblWght5_Click()
    Call unitSet(lblWght5)
End Sub

Private Sub lblWght6_Click()
    Call unitSet(lblWght6)
End Sub

Private Sub lblWght7_Click()
    Call unitSet(lblWght7)
End Sub

Private Sub lblColorInt()
    unitChar = ""
    lblTime1.BackColor = vbWhite
    lblTime2.BackColor = vbWhite
    lblTime3.BackColor = vbWhite
    lblTime4.BackColor = vbWhite
    lblTime5.BackColor = vbWhite
    lblTime6.BackColor = vbWhite
    
    lblLen1.BackColor = vbWhite
    lblLen2.BackColor = vbWhite
    lblLen3.BackColor = vbWhite
    lblLen4.BackColor = vbWhite
    lblLen5.BackColor = vbWhite
    lblLen6.BackColor = vbWhite
    
    lblWght1.BackColor = vbWhite
    lblWght2.BackColor = vbWhite
    lblWght3.BackColor = vbWhite
    lblWght4.BackColor = vbWhite
    lblWght5.BackColor = vbWhite
    lblWght6.BackColor = vbWhite
    
    lblTemp1.BackColor = vbWhite
    lblTemp2.BackColor = vbWhite
    lblTemp3.BackColor = vbWhite
    lblTemp4.BackColor = vbWhite
    lblTemp5.BackColor = vbWhite
    lblTemp6.BackColor = vbWhite
    
    lblPrss1.BackColor = vbWhite
    lblPrss2.BackColor = vbWhite
    lblPrss3.BackColor = vbWhite
    lblPrss4.BackColor = vbWhite
    lblPrss5.BackColor = vbWhite
    lblPrss6.BackColor = vbWhite
    
    lblMnt1.BackColor = vbWhite
    lblMnt2.BackColor = vbWhite
    lblMnt3.BackColor = vbWhite
    lblMnt4.BackColor = vbWhite
    lblMnt5.BackColor = vbWhite
    lblMnt6.BackColor = vbWhite
    
End Sub

Private Sub txtTate1_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If txtTate1.text <> "" Then
        txtTate1.text = ""
    Else
        txtTate1.text = unitChar
        'SendKeys "{Enter}"
        'txtTate2.SetFocus
    End If
    
    Call lblColorInt
End Sub

Private Sub txtTate2_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If txtTate2.text <> "" Then
        txtTate2.text = ""
    Else
        txtTate2.text = unitChar
        'SendKeys "{Enter}"
        'txtYoko1.SetFocus
    End If
    
    Call lblColorInt
End Sub

Private Sub txtYoko1_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If txtYoko1.text <> "" Then
        txtYoko1.text = ""
    Else
        txtYoko1.text = unitChar
        'SendKeys "{Enter}"
        'txtYoko2.SetFocus
    End If
    
    Call lblColorInt
End Sub

Private Sub txtYoko2_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If txtYoko2.text <> "" Then
        txtYoko2.text = ""
    Else
        txtYoko2.text = unitChar
        'SendKeys "{Enter}"
        'txtTate1.SetFocus
    End If
    
    Call lblColorInt
End Sub

Public Sub unitSet(argUnit As Object)
    Call lblColorInt
    unitChar = argUnit.Caption
    argUnit.BackColor = vbYellow
'    If txtYoko1.Text = "Ï·" Then
'        txtYoko1.Text = argUnit
'    ElseIf txtYoko2.Text = "Ï·" Then
'         txtYoko2.Text = argUnit
'    ElseIf txtTate1.Text = "Ï·" Then
'         txtTate1.Text = argUnit
'    ElseIf txtTate2.Text = "Ï·" Then
'         txtTate2.Text = argUnit
'    End If
'    SendKeys "{Enter}"
End Sub

Private Sub UserForm_Activate()
    Dim currWorksheet     As Worksheet
    Dim srchRange            As Range
    
    unitChar = ""
    
    'ĀļÞŨĖĐįcĄēÅå/ÅŽEÝ
    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="|HwlÏ·|", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
'        txtYoko1.Text = srchRange.Offset(1, 1).Value
'        SendKeys "{Enter}"
'        txtYoko2.Text = srchRange.Offset(1, 3).Value
'        SendKeys "{Enter}"
'        txtTate1.Text = srchRange.Offset(2, 1).Value
'        SendKeys "{Enter}"
'        txtTate2.Text = srchRange.Offset(2, 3).Value
'        SendKeys "{Enter}"
    End If
    
        txtYoko1.text = ""
        txtYoko2.text = ""
        txtTate1.text = ""
        txtTate2.text = ""
End Sub
