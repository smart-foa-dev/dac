Attribute VB_Name = "GripCheck"
Public Function GripInputCheck() As Boolean
    Dim srchRange   As Range
    Dim wrkStr      As String
    Dim wrkStr2     As String

    GripInputCheck = True
    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        'GRIPからのデータ取込：収集タイプの項目取得
        wrkStr = srchRange.Offset(0, 1).Value
        If wrkStr = "GRIP" Then
            '終了エレメント項目取得
            Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="終了エレメント", LookAt:=xlWhole)
            wrkStr = srchRange.Offset(0, 1).Value
            '開始エレメント項目取得
            Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="開始エレメント", LookAt:=xlWhole)
            wrkStr2 = srchRange.Offset(0, 1).Value
            
            If wrkStr = "" Or wrkStr2 = "" Then
                GripInputCheck = False
            Else
                GripInputCheck = True
            End If
            
        Else
            GripInputCheck = True
        End If
        
    Else
        'GRIP以外のデータ取込
        GripInputCheck = True
    End If
        
End Function
