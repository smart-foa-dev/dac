Attribute VB_Name = "Mission_Module"
Option Explicit

'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
Public Sub GetMissionInfoAll()
    Dim ctmList()   As CTMINFO      ' CTM情報
    Dim ctmValue()  As CTMINFO      ' CTM取得情報
    Dim iResult     As Integer
    Dim II          As Integer
    
    iResult = GetMissionInfo(ctmList, ctmValue)
    
    '* シート毎（シート名＝CTM名）へ出力
    If iResult > 0 Then
    
        '* CTM種別でシートに出力
        For II = 0 To UBound(ctmList)
            
            Call OutputCTMInfoToSheet(ctmList(II).Name, 0#, ctmValue)
        
        Next

    End If
    
End Sub

'****************************************************************
'* 指定ミッションの情報を取得する
'* ※ParamシートにミッションID、CTMエレメント一覧が必要
'****************************************************************
Private Function GetMissionInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim MissionID   As String
    Dim startTime   As Double
    Dim endTime     As Double
    Dim SendString  As String
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim dispTerm    As Double
    Dim dispTermUnit As String
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim iResult     As Integer
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    '* メインシートから各種情報を取得
    With Worksheets(ParamSheetName)
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得開始", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(srchRange.Offset(0, 1).Value)
            startT = TimeValue(srchRange.Offset(0, 1).Value)
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If
        startTime = GetUnixTime(startD + startT)
        
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = DateValue(srchRange.Offset(0, 1).Value)
            endT = TimeValue(srchRange.Offset(0, 1).Value)
        Else
            endD = DateValue("2020/12/31 23:59:59")
            endT = TimeValue("2020/12/31 23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If
        endTime = GetUnixTime(endD + endT)
        
        '*********************
        '* ミッションIDを取得
        '*********************
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            MissionID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "ミッションID項目が見つからないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If
        If MissionID = "" Then
            MsgBox "ミッションIDが見つからないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If
        If MissionID = "" Then
            MsgBox "ミッションIDが見つからないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If

        '*******************************
        '* 更新周期を取得
        '*******************************
    
        '*******************************
        '* 表示期間を取得
        '*******************************
        Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                dispTerm = 0#
                dispTermUnit = "【時】"
            Else
                dispTerm = srchRange.Offset(0, 1).Value
                dispTermUnit = srchRange.Offset(0, 2).Value
            End If
        Else
            dispTerm = 0#
            dispTermUnit = "【時】"
        End If
        Select Case dispTermUnit
            Case "【分】"
                dispTerm = dispTerm * 60
            Case "【時】"
                dispTerm = dispTerm * 3600
        End Select
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
    
    End With
    
    '* CTM情報一覧をシートから取得
    Call GetCTMList(ParamSheetName, ctmList)
    
    '* プログラムミッションに問合せ
    '* 取得開始日時を「現在時刻−表示期間−１分」として算出し問い合わせる
    getEndTime = endTime
    getStartTime = startTime
'    getStartTime = GetUnixTime(Now()) - (dispTerm + 60) * 1000
'    MsgBox "開始日時は" & Format(CDate(((getStartTime / 1000 + 32400) / 86400) + 25569), "yyyy/MM/dd hh:mm:ss") & "です。" & vbCrLf & _
'           "終了日時は" & Format(CDate(((getEndTime / 1000 + 32400) / 86400) + 25569), "yyyy/MM/dd hh:mm:ss") & "です。"
    SendString = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pm?id=" & MissionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & "&limit=" & Format(LimitCTMNumber) & "&lang=ja"
    
    If useProxy = False Then
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
    Else
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
        httpObj.setProxy 2, proxyUri
    End If
    
    httpObj.Open "GET", SendString, False
    httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
    httpObj.Send
    
'    Select Case httpObj.Status
'        Case 200
'            With CreateObject("ADODB.Stream")
'                .Type = adTypeText
'                .Open
'                .WriteText httpObj.responseText
'                .SaveToFile "c:\foa\data\test.json", adSaveCreateOverWrite
'                .Close
'            End With
'        Case Else
'            MsgBox "エラーが発生しました。" & vbCrLf & "ステータスコード：" & httpObj.Status, vbCritical + vbSystemModal
'            Exit Function
'    End Select
    
    ' ダウンロード待ち
    Do While httpObj.readyState <> 4
        DoEvents
    Loop
    
    '* 取得情報のデコード
    Set sc = CreateObject("ScriptControl")
    With sc
        .Language = "JScript"

        '指定したインデックス、名称のデータを取得する
        .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
    End With
    GetString = httpObj.responseText
    Set objJSON = sc.CodeObject.jsonParse(GetString)
    
    Set sc = Nothing
    
    Set httpObj = Nothing
    
    '* 取得情報をCTM情報に展開
    iResult = GetCTMInfo(objJSON, ctmList, ctmValue)
    
    ' MsgBox http.responseText
    GetMissionInfo = iResult
    
End Function

'*************************************************************************
'* 取得したJSON情報をデコードしCTM情報およびエレメント情報として展開する
'*************************************************************************
Public Function GetCTMInfo(objJSON As Object, ctmList() As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim recno           As Integer
    Dim II              As Integer
    Dim rec             As Object
    Dim rec2            As Object
    Dim rec3            As Object
    Dim rec4            As Object
    Dim aryIndex        As Integer
    Dim aryIndex2       As Integer
    Dim ctmIndex        As Integer
    Dim workElm         As ELMINFO
    Dim iFirstFlag      As Boolean
    
    iFirstFlag = True
    
    ' 件数カウンタ初期化
    recno = 0
    
    '* CTM種別数分ループ
    For Each rec In objJSON
    
        '* 対象CTMを確定する
        For II = 0 To UBound(ctmList)
            If ctmList(II).ID = GetJSONValue(rec, "id") Then
                ctmIndex = II
                Exit For
            End If
        Next
        
        If II > UBound(ctmList) Then
            MsgBox "ミッションからの取得情報に該当するCTMがありませんでした。" & vbCrLf & "処理を中断します。"
            Exit Function
        End If
        
        '* CTM取得件数分ループ
        II = rec.num
        For Each rec2 In rec.ctms
        
            If iFirstFlag Then
                aryIndex = 0
                ReDim ctmValue(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(ctmValue) + 1
                ReDim Preserve ctmValue(aryIndex)
            End If
            ReDim ctmValue(aryIndex).Element(0)
            ctmValue(aryIndex).Element(0).Name = ""
            
            '* CTM情報の取得
            ctmValue(aryIndex).Name = ctmList(ctmIndex).Name
            ctmValue(aryIndex).ID = ctmList(ctmIndex).ID
            ctmValue(aryIndex).RecvTime = GetJSONValue(rec2, "RT")

            '* 件数カウンタアップ
            recno = recno + 1
            
            '* エレメント数分の値取得
            Set rec3 = CallByName(rec2, "EL", VbGet)
            For II = 0 To UBound(ctmList(ctmIndex).Element)
            
                workElm = ctmList(ctmIndex).Element(II)
                
                Set rec4 = CallByName(rec3, workElm.ID, VbGet)
                
                aryIndex2 = UBound(ctmValue(aryIndex).Element)
                If ctmValue(aryIndex).Element(aryIndex2).Name <> "" Then
                    aryIndex2 = aryIndex2 + 1
                    ReDim Preserve ctmValue(aryIndex).Element(aryIndex2)
                End If
                
                '* エレメント情報の取得
                ctmValue(aryIndex).Element(aryIndex2).Name = workElm.Name
                ctmValue(aryIndex).Element(aryIndex2).ID = workElm.ID
                ctmValue(aryIndex).Element(aryIndex2).Type = CInt(GetJSONValue(rec4, "T"))
                ctmValue(aryIndex).Element(aryIndex2).Value = ""
                On Error Resume Next
                ctmValue(aryIndex).Element(aryIndex2).Value = GetJSONValue(rec4, "V")
                '''If ctmValue(aryIndex).Element(aryIndex2).Value = "" Then ctmValue(aryIndex).Element(aryIndex2).Value = " "
                
            Next
            
        Next
        
    Next
    
    GetCTMInfo = recno
    
End Function

'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Public Sub OutputCTMInfoToSheet(currSheetName As String, dispTerm As Double, ctmValue() As CTMINFO)
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    Dim currWorksheet   As Worksheet
    Dim writeIndex      As Integer
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim delRange        As Range
    Dim writeRange      As Range
    Dim receiveTime     As Double
    Dim dtRecvL         As Long
    Dim dtRecv          As Date
    Dim ms              As Integer
    Dim strWork         As String
    Dim nowTime         As Double
    Dim endTime         As Double
    Dim writeArray()    As Variant
    Dim writeArray2()   As Variant
    Dim iFirstFlag      As Boolean
    Dim readIndex       As Integer
    
    
    '* 出力先シートの取得
    Set currWorksheet = Worksheets(currSheetName)
    
    With currWorksheet
        
        '* 書き出し位置の設定
        Set startRange = .Cells(4, 1)
        
        '* 全情報クリア
        Set lastRange = startRange.End(xlDown)
        Set delRange = .Range("4:" & Format(lastRange.Row))
        delRange.Delete
        
        '* 書き出し位置の設定
        Set startRange = .Cells(4, 1)
        writeIndex = 0
        
'        Application.ScreenUpdating = False
        
        iFirstFlag = True
        For JJ = 0 To UBound(ctmValue)
        
            If currSheetName = ctmValue(JJ).Name Then
            
                receiveTime = CDbl(ctmValue(JJ).RecvTime)
                dtRecvL = CLng(receiveTime / 1000)
                '* 表示期間の処理
                If JJ = 0 Then
                    If dispTerm < 1# Then
                        endTime = dispTerm
                    Else
                        endTime = dtRecvL - dispTerm
                    End If
                End If
                If (endTime < 0 Or endTime > dtRecvL) Then Exit For
                ms = CInt(Right(ctmValue(JJ).RecvTime, 3))
                receiveTime = ((dtRecvL + 32400) / 86400) + 25569
                dtRecv = CDate(receiveTime)
                strWork = Format(dtRecv, "yyyy/MM/dd hh:mm:ss") + "." + Format(ms, "000")
                
                If iFirstFlag Then
                    readIndex = 0
                    ReDim writeArray(UBound(ctmValue(JJ).Element) + 2, readIndex)
                    iFirstFlag = False
                Else
                    readIndex = UBound(writeArray, 2) + 1
                    ReDim Preserve writeArray(UBound(ctmValue(JJ).Element) + 2, readIndex)
                End If
                
                writeArray(0, readIndex) = strWork
                writeArray(1, readIndex) = ctmValue(JJ).Name
                
                For KK = 0 To UBound(ctmValue(JJ).Element)
                
                    writeArray(KK + 2, readIndex) = ctmValue(JJ).Element(KK).Value
                
                Next
                
                writeIndex = writeIndex + 1
            End If
            
            DoEvents
        
        Next
        
        Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, UBound(ctmValue(0).Element) + 2))
        writeArray2 = WorksheetFunction.Transpose(writeArray)
        writeRange = writeArray2
        Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, 0))
        writeRange.NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
        writeRange.EntireColumn.AutoFit
    
'        Application.ScreenUpdating = True
        
    End With
    
End Sub

'***********************************************
'* メインシートにあるミッション情報を取得する
'***********************************************
Public Sub GetCTMList(currSheetName As String, ctmList() As CTMINFO)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim ctmRange        As Range
    Dim elmRange        As Range
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim aryIndex        As Integer      ' CTM情報インデックス
    Dim aryIndex2       As Integer      ' エレメント情報インデックス
    Dim iFirstFlag      As Boolean      ' 配列初回フラグ
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    
    iFirstFlag = True
    
    Set currWorksheet = Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim ctmList(0)
    
    With currWorksheet
    
'        .Activate
        
        '* CTM名称の項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM名称", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        
        For II = 0 To MaxCTMNumber Step 2
            Set ctmRange = startRange.Offset(II, 0)
            
            If ctmRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            If iFirstFlag Then
                aryIndex = 0
                ReDim ctmList(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(ctmList) + 1
                ReDim Preserve ctmList(aryIndex)
            End If
            ReDim ctmList(aryIndex).Element(0)
            ctmList(aryIndex).Element(0).Name = ""
            
            '* CTM情報取得
            ctmList(aryIndex).Name = ctmRange.Offset(0, 0).Value
            ctmList(aryIndex).ID = ctmRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = ctmRange.Offset(0, 1)
            For JJ = 0 To MaxELMNumber
                If elmRange.Offset(0, JJ).Value = "" Then
                    Exit For
                End If
                
                aryIndex2 = UBound(ctmList(aryIndex).Element)
                If ctmList(aryIndex).Element(aryIndex2).Name <> "" Then
                    aryIndex2 = aryIndex2 + 1
                    ReDim Preserve ctmList(aryIndex).Element(aryIndex2)
                End If
                ctmList(aryIndex).Element(aryIndex2).Name = elmRange.Offset(0, JJ).Value
                ctmList(aryIndex).Element(aryIndex2).ID = elmRange.Offset(1, JJ).Value
            Next
            
                
        Next
    
    End With
    
End Sub





