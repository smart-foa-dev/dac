Attribute VB_Name = "Add_Module"

'開始時刻の行削除
Public Sub NullDelete()
    Dim pos         As Integer
    Dim wrkVal
    
    wrkVal = Worksheets(ForPivotSheetName).Range("A4").End(xlDown).Row
    For pos = 3 To wrkVal
        If Worksheets(ForPivotSheetName).Cells(pos, 4).Value = "" Then
            Sheets(ForPivotSheetName).Rows(Trim(CStr(pos)) & ":" & Trim(CStr(pos))).Delete Shift:=xlUp
        End If
    Next pos

End Sub

'*************************************
'Bulkyデータからグラフを作成
'*************************************
Public Sub BulkyDataTOGraph()
    Dim graphDataWorksheet      As Worksheet

    'グラフテーブルにNO設定
    Set graphDataWorksheet = Worksheets(ForPivotSheetName)
    graphDataWorksheet.Range("A1").Value = "NO"
    
    '2016/11/30 データ無しの場合、グラフ処理をしない
    If graphDataWorksheet.Range("A2").Value = "" Or graphDataWorksheet.Range("A2").Value = 0 Then
        Exit Sub
    End If

    '系統グラフ線削除
    Call Line_Delete

    'グラフ作成
    Call MakeGraph
    
    '縦横最大最小値設定
    Call PutMaxMinStepEditSheet
    
    '開始・終了日時設定
    Call StratEndTimeSet
    
    '凡例、背景ﾃﾞｰﾀ作成
''    Call HaikeiHanreiDataMake

End Sub

'*************************************
'* グラフのデータソースを変更
'*************************************
Public Sub MakeGraph()
' Wang FoaStudio Issue AISBUL-51 20181211 Start
On Error GoTo ErrLable
' Wang FoaStudio Issue AISBUL-51 20181211 End

    Dim writeRange              As Range
    Dim wrkCount                As Integer
    Dim pos                     As Integer
    Dim wrkStr                  As String
    
    Dim graphDataWorksheet      As Worksheet
    Dim currChartObj            As ChartObject
    Dim currChartObj2            As ChartObject
    Dim graphEditWorksheet      As Worksheet
    Dim fixGraphWorksheet       As Worksheet

    Dim nameRange               As Range
    Dim dataXRange              As Range
    Dim dataYRange              As Range
    
'*************************************************************PivotSheetName:グラフテーブル
    '''''Call Line_Delete
    
'    'グラフデータALL削除
'    Worksheets(PivotSheetName).Cells.Select
'    Selection.ClearContents
    
    
    Set writeRange = Worksheets(ForPivotSheetName).Range("A1")
    
    Set graphEditWorksheet = Worksheets(GraphEditSheetName) 'グラフ編集シート
    Set fixGraphWorksheet = Worksheets(FixGraphSheetName)   'グラフシート
    
    Set graphDataWorksheet = Worksheets(ForPivotSheetName)
    With graphDataWorksheet
        Set nameRange = .Range(writeRange.Cells, writeRange.End(xlToRight).Cells)
        Set dataXRange = .Range(writeRange.Offset(1, 1).Cells, writeRange.Offset(1, 1).End(xlDown).Cells)
        Set dataYRange = .Range(writeRange.Offset(1, 0).Cells, writeRange.Offset(1, 0).End(xlDown).Cells)
    End With
    
    wrkCount = nameRange.Count
    
    '* グラフ編集シートのグラフを更新
    Set currChartObj = graphEditWorksheet.ChartObjects(HistGraph01Name)
    Set currChartObj2 = fixGraphWorksheet.ChartObjects(HistGraph01Name)
    Set nameRange = graphDataWorksheet.Range(writeRange.Cells, writeRange.Offset(0, 1).Cells)
    
    ' Wang FoaStudio Issue AISBUL-51 20181211 Start
    fixGraphWorksheet.Visible = xlSheetHidden
    ' Wang FoaStudio Issue AISBUL-51 20181211 End
    
    For pos = 1 To wrkCount - 1
        'グラフ系列は255までしか表示しないMarkerStyle =
        If pos > 250 Then
            Exit For
        End If
    
        wrkStr = nameRange.Offset(0, pos).Value2(1, 1)

        ' Wang FoaStudio Issue AISBUL-51 20181211 Start
        If Len(wrkStr) = 0 Then
            Exit For
        End If
        ' Wang FoaStudio Issue AISBUL-51 20181211 End

        With currChartObj.Chart
            .SeriesCollection.NewSeries
            .SeriesCollection(pos).Name = wrkStr
            .SeriesCollection(pos).XValues = "=" & ForPivotSheetName & "!" & dataYRange.Address
            .SeriesCollection(pos).Values = "=" & ForPivotSheetName & "!" & dataXRange.Address
            .SeriesCollection(pos).Format.Line.Weight = 1.25
            .SeriesCollection(pos).Format.Line.Visible = msoTrue
         End With
         ''currChartObj.Chart.ChartType = xlMarkerStyleNone
                                   
        With currChartObj2.Chart
            .SeriesCollection.NewSeries
            .SeriesCollection(pos).Name = wrkStr
            .SeriesCollection(pos).XValues = "=" & ForPivotSheetName & "!" & dataYRange.Address
            .SeriesCollection(pos).Values = "=" & ForPivotSheetName & "!" & dataXRange.Address
            .SeriesCollection(pos).Format.Line.Weight = 1.25
            .SeriesCollection(pos).Format.Line.Visible = msoTrue
        End With
        ''currChartObj2.Chart.ChartType = xlMarkerStyleNone
                                   
            Set dataXRange = graphDataWorksheet.Range(writeRange.Offset(1, pos + 1).Cells, writeRange.Offset(0, pos + 1).End(xlDown).Cells)
            'Set nameRange = graphDataWorksheet.Range(writeRange.Cells, writeRange.Offset(0, pos + 1).Cells)
    Next pos

ErrLable:
    fixGraphWorksheet.Visible = xlSheetVisible

End Sub


'*************************************
'* 開始･終了日時設定
'*************************************
Public Sub StratEndTimeSet()
    Dim graphEditWorksheet      As Worksheet
    Dim srchRange               As Variant
    Dim startDate               As String
    Dim endDate                 As String

    Set graphEditWorksheet = Worksheets("Bulkyテンプレート")
    
    With graphEditWorksheet
        
        '* グラフ編集シートへ転記
        Set srchRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startDate = Format(srchRange.Offset(0, 1).Value, "YYYY/MM/DD") & " " & Format(srchRange.Offset(0, 2).Value, "hh:mm:ss")
            endDate = Format(srchRange.Offset(1, 1).Value, "YYYY/MM/DD") & " " & Format(srchRange.Offset(1, 2).Value, "hh:mm:ss")
        End If
    End With
    
    'グラフ編集シートに開始終了日時を設定
    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    graphEditWorksheet.Range("E4").Value = startDate
    graphEditWorksheet.Range("H4").Value = endDate
    
    'グラフシートに開始終了日時を設定
    Set graphEditWorksheet = Worksheets(FixGraphSheetName)
    graphEditWorksheet.Range("E4").Value = startDate
    graphEditWorksheet.Range("H4").Value = endDate
    
End Sub

'
'件数以外の系列削除
'
Public Sub Line_Delete()
    Dim Gurafu      As ChartObject
    Dim Gurafu2     As ChartObject
    Dim pos         As Integer
    Dim wrkStr      As String
    
    '系列数の取込
    Set Gurafu = Worksheets(GraphEditSheetName).ChartObjects(HistGraph01Name)
    LineCnt = Gurafu.Chart.SeriesCollection.Count
    
    '件数以外の系列を削除
    For pos = LineCnt To 1 Step -1
        wrkStr = Gurafu.Chart.SeriesCollection(pos).Name
        'If "件数" <> wrkStr Then
            Gurafu.Chart.SeriesCollection(pos).Delete
        'End If
    Next pos
    
    '系列数の取込
    Set Gurafu2 = Worksheets(FixGraphSheetName).ChartObjects(HistGraph01Name)
    LineCnt = Gurafu2.Chart.SeriesCollection.Count
    
    '件数以外の系列を削除
    For pos = LineCnt To 1 Step -1
        wrkStr = Gurafu2.Chart.SeriesCollection(pos).Name
        'If "件数" <> wrkStr Then
            Gurafu2.Chart.SeriesCollection(pos).Delete
        'End If
    Next pos

End Sub
