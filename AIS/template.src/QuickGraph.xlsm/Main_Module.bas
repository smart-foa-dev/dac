Attribute VB_Name = "Main_Module"
Option Explicit

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSub()
'    Dim fname           As String
'    Dim iReturn         As Variant
'    Dim srchRange       As Range
'    Dim saveFilePath    As String
'    Dim checkStr        As String
'    Dim currThisFile    As String
'    Dim templateName    As String
'    Dim wrkInt          As Integer
'    Dim msgStr          As String
'    Dim prevDate        As Date
'    Dim prevTime        As Date
'    Dim currWorksheet   As Worksheet
'    Dim IsFirstSave     As String
'
'    '表示倍率保存
'    Call SaveBairitu
'
'    '* テンプレート名／登録フォルダ名を取得する
'    With ThisWorkbook.Worksheets(ParamSheetName).Columns(1)
'        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
'        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
'        Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then IsFirstSave = srchRange.Offset(0, 1).Value
'    End With
'
'    Call MkDir(saveFilePath)
'    checkStr = Right(saveFilePath, 1)
'
'    '* 自ファイルを一旦上書き保存する
'    If IsFirstSave = "" Then
'        '* フォルダ名の最終文字のチェック
'        If checkStr = "\\" Then
'            fname = saveFilePath & templateName & "_" & ThisWorkbook.Name
'        Else
'            fname = saveFilePath & "\" & templateName & "_" & ThisWorkbook.Name
'        End If
'        '* テンプレート名／登録フォルダ名を取得する
'        With ThisWorkbook.Worksheets(ParamSheetName).Columns(1)
'            Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
'            'Wang Issue of quick graph 20180709 Start
''            If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "TRUE"
'            'Wang Issue of quick graph 20180709 End
'        End With
'    Else
'        '* フォルダ名の最終文字のチェック
'        If checkStr = "\\" Then
'            fname = saveFilePath & ThisWorkbook.Name
'        Else
'            fname = saveFilePath & "\" & ThisWorkbook.Name
'        End If
'    End If
'
'    '* 登録フォルダにファイルを上書き複写する
'    If Dir(fname) <> "" Then
'      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
'      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
'    End If
'
'    '各種バー表示
'    Call DispBar
'
'    '* 別名保存する
'    Application.DisplayAlerts = False
'
'    '* ISSUE_NO.624 Add ↓↓↓ *******************************
'    On Error GoTo ErrorHandler
'
'    'Wang Issue of quick graph 20180709 Start
'    If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "TRUE"
'    'Wang Issue of quick graph 20180709 End
'
'    ThisWorkbook.SaveAs Filename:=fname
'
'ErrorHandler:
'    '-- 例外処理
'    If Err.Description <> "" Then
'        MsgBox Err.Description, vbCritical & vbOKOnly, "警告"
'    End If
'    '* ISSUE_NO.624 Add ↑↑↑ *******************************
'
'
'    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
'    If Application.Workbooks.Count > 1 Then
'        ThisWorkbook.Close
'    Else
'        Application.Quit
'        ThisWorkbook.Close
'    End If
'    If Err.Description = "" Then
'        Application.DisplayAlerts = True
'    End If
'

    '表示倍率保存
    Call SaveBairitu

On Error GoTo ErrorHandler
    ThisWorkbook.Save

ErrorHandler:
    '-- 例外処理
    If Err.Description <> "" Then
        MsgBox Err.Description, vbCritical & vbOKOnly, "警告"
    End If

    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    If Application.Workbooks.Count > 1 Then
        ThisWorkbook.Close
    Else
        Application.Quit
        ThisWorkbook.Close
    End If
    If Err.Description = "" Then
        Application.DisplayAlerts = True
    End If

End Sub

Function MkDir(strPath As String)

    Dim fso As New FileSystemObject
    Dim parentPath As String
    
    parentPath = fso.GetParentFolderName(strPath)

    If Not fso.FolderExists(parentPath) Then
        Call MkDir(parentPath)
    End If


    If Not fso.FolderExists(strPath) Then
        fso.CreateFolder strPath
    End If

End Function

