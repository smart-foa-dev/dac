Attribute VB_Name = "MenuBar_Module"
Option Explicit

''******************************************
''　EXCEL終了時(×マーク押下時)の処理
''　　　各種バーの表示
''******************************************
Public Sub DispBar()

    With ActiveWindow
        .DisplayHorizontalScrollBar = True  '水平スクロールバー
        .DisplayVerticalScrollBar = True  '垂直スクロールバー
        .DisplayWorkbookTabs = True  'シート見出し
        .DisplayWorkbookTabs = True
    End With

    With Application
        .DisplayFormulaBar = True  '数式バー
        .DisplayStatusBar = True    'ステータスバー
    End With
    
End Sub


