Attribute VB_Name = "Common_Module"
Option Explicit

Public Const DefaultZoomPercent As Integer = 40
Public Const StepZoomPercent    As Integer = 20

Public Const ParamSheetName     As String = "param"

Public ZoomFactor               As Integer
