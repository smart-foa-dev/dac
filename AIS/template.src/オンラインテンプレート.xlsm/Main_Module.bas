Attribute VB_Name = "Main_Module"
Option Explicit

'--2017/04/28 Add No.651 アドイン連携処理追加----Start
'**************************************
'*アドインメニュー制御(活性/非活性切替)
'**************************************
Public Sub InvalidateAddin()
    Dim addIn As COMAddIn
    Dim automationObject As Object
    
    Set addIn = Application.COMAddIns("AisTemplateAddin")
    Set automationObject = addIn.Object
    
    automationObject.Invalidate

End Sub
'--2017/04/28 Add No.651 アドイン連携処理追加----End

Public Sub GraphAlarmSetting()
    Dim addIn As COMAddIn
    Dim automationObject As Object
    Set addIn = Application.COMAddIns("AisTemplateAddin")
    Set automationObject = addIn.Object
    If automationObject.GraphAlarmSetting Then
        Call OperationTest
    End If
End Sub

'******************************
'* スタートストップボタン処理
'******************************
Public Sub ChangeStartStopBT()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range

    Call SetFilenameToVariable
    
    '* ボタン表示状態を確認
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
    
        '* 更新中なら停止
        If srchRange.Offset(0, 1).Value = "ON" Then
            srchRange.Offset(0, 1).Value = "OFF"

            '* 予約したスケジュールをキャンセルする
            Call CancelSchedule
        
            '* メニュー制御
            Call MenuCtrlEnableOrDisable("テスト", True)
            Call MenuCtrlEnableOrDisable("自動更新", True)
            Call MenuCtrlEnableOrDisable("更新停止", False)
            
            'Wang Issue of UI improvement 2018/06/07 Start
            On Error Resume Next
            ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = False
            ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = True
            On Error GoTo 0
            'Wang Issue of UI improvement 2018/06/07 End
    
        '* 停止中なら開始
        Else
            srchRange.Offset(0, 1).Value = "ON"
            
            '* メニュー制御
            Call MenuCtrlEnableOrDisable("テスト", False)
            Call MenuCtrlEnableOrDisable("自動更新", False)
            Call MenuCtrlEnableOrDisable("更新停止", True)
            
            'Wang Issue of UI improvement 2018/06/07 Start
            On Error Resume Next
            ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = True
            ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = False
            On Error GoTo 0
            'Wang Issue of UI improvement 2018/06/07 End
            
            '* ISSUE_NO.686 sunyi start
            '* オンライン種別の取込
'            '* 自動実行開始
'            Call TimerStart
            Dim wrkStr          As String
            
            Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
            Set srchRange = currWorksheet.Cells.Find(What:="オンライン種別", LookAt:=xlWhole)
        
            If Not srchRange Is Nothing Then
                If IsEmpty(srchRange.Offset(0, 1).Value) Then
                    wrkStr = "期間固定"
                Else
                    wrkStr = srchRange.Offset(0, 1).Value
                End If
            Else
                wrkStr = "期間固定"
            End If
            
            ' オンライン種別毎にタイマー処理起動
            If (wrkStr = "期間固定") Or (wrkStr = "連続移動") Then
                Call TimerStart
            ElseIf wrkStr = "一定時間繰り返し" Then
                '一定時間繰返し処理
                Call Kurikaeshi_TimerStart
            ElseIf wrkStr = "毎日定刻繰り返し" Then
                '毎日定刻繰返し処理
                Call Teikoku_TimerStart
            End If
            '* ISSUE_NO.686 sunyi end
        
        End If
        
        Call UpdateFormStatus
        
        'No.651 Add.
        '* アドインメニュー制御
        Call InvalidateAddin

    End If

End Sub

'***************************************
'* 自動更新スタート
'20160726　改造：一定時間繰返し、毎日定刻繰返し機能追加
'***************************************
Public Sub AutoUpdateStart()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim wrkStr          As String
    
    Call SetFilenameToVariable

    '* メニュー制御
    Call MenuCtrlEnableOrDisable("テスト", False)
    Call MenuCtrlEnableOrDisable("自動更新", False)
    Call MenuCtrlEnableOrDisable("更新停止", True)
    
    'Wang Issue of UI improvement 2018/06/07 Start
    On Error Resume Next
    ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = True
    ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = False
    On Error GoTo 0
    'Wang Issue of UI improvement 2018/06/07 End

    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "ON"
    
    ' 20160726 追加***************************************
    'オンライン種別の取込
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="オンライン種別", LookAt:=xlWhole)

    If Not srchRange Is Nothing Then
        If IsEmpty(srchRange.Offset(0, 1).Value) Then
            wrkStr = "期間固定"
        Else
            wrkStr = srchRange.Offset(0, 1).Value
        End If
    Else
        wrkStr = "期間固定"
    End If
    
    ' オンライン種別毎にタイマー処理起動
    If (wrkStr = "期間固定") Or (wrkStr = "連続移動") Then
        Call TimerStart
    ElseIf wrkStr = "一定時間繰り返し" Then
        '一定時間繰返し処理
        Call Kurikaeshi_TimerStart
    ElseIf wrkStr = "毎日定刻繰り返し" Then
        '毎日定刻繰返し処理
        Call Teikoku_TimerStart
    End If
    '*******************************************************

End Sub

'****************************
'* 自動更新を停止する
'****************************
Public Sub AutoUpdateStop()
    Dim currWorksheet   As Worksheet
    Dim mainWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim prevDate        As Date
    Dim prevTime        As Date
    
    Call SetFilenameToVariable

    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = "OFF"
    
        '* 予約したスケジュールをキャンセルする
        Call CancelSchedule
    
        '* メニュー制御
        Call MenuCtrlEnableOrDisable("テスト", True)
        Call MenuCtrlEnableOrDisable("自動更新", True)
        Call MenuCtrlEnableOrDisable("更新停止", False)
        
        'Wang Issue of UI improvement 2018/06/07 Start
        On Error Resume Next
        ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = False
        ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = True
        On Error GoTo 0
        'Wang Issue of UI improvement 2018/06/07 End
    
        Call UpdateFormStatus
        
    End If
    
End Sub

'*******************
'* 状態フォーム更新
'*******************
Public Sub UpdateFormStatus()
    Dim currWorksheet   As Worksheet
    Dim mainWorksheet   As Worksheet
    Dim currRange       As Range
    Dim srchRange       As Range
    Dim flagON          As Boolean
    Dim f               As Variant
    Dim isNotForm       As Boolean
    Dim FinalTime       As Date
    Dim endD            As Date
    Dim endT            As Date
    Dim currShape       As Shape
    Dim RCAddress       As String
    Dim btSize          As RECT
    Dim prevTime        As Variant
    'Wang Issue of UI improvement 2018/06/07 Start
    Dim UpdataTimeStr   As String
    'Wang Issue of UI improvement 2018/06/07 End
    
    Call SetFilenameToVariable

    '* 自動更新状態を「pram」シートから取得
    Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "ON" Then
            flagON = True
        Else
            flagON = False
        End If
    End If
    
    Set mainWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    With mainWorksheet
        '* 収集終了日時を「オンラインテンプレート」シートから取得
        Set srchRange = Workbooks(thisBookName).Worksheets(MainSheetName).Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        FinalTime = endD + endT
    
        '* 前回起動日時を取得
        Set srchRange = .Cells.Find(What:="前回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 2).Value <> "" Then
                prevTime = Format(srchRange.Offset(0, 2).Value, "hh:mm:ss")
            Else
                prevTime = "--:--"
            End If
        End If
        
         'Wang Issue of UI improvement 2018/06/07 Start
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "" Then
                UpdataTimeStr = srchRange.Offset(0, 1).Value & "sec"
            Else
                UpdataTimeStr = "--秒"
            End If
        End If
        'Wang Issue of UI improvement 2018/06/07 End
    End With

    '* 全てのワークシートに対して表示テキストボックスを確認し、情報を表示する
    For Each currWorksheet In Workbooks(thisBookName).Worksheets
        isNotForm = True
        For Each currShape In currWorksheet.Shapes
            If currShape.name = PrefixStrTX & "ONLINE" Then
                isNotForm = False
                'Wang Issue of UI improvement 2018/06/07 Start
                currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & vbCrLf & vbCrLf & UpdataTimeStr
                'Wang Issue of UI improvement 2018/06/07 End
                If flagON Then
                    'Wang Issue of UI improvement 2018/06/07 Start
                    'currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & "更新中"
                    On Error Resume Next
                    ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = True
                    ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = False
                    On Error GoTo 0
                    'Wang Issue of UI improvement 2018/06/07 End
                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 255)
                Else
                    'Wang Issue of UI improvement 2018/06/07 Start
                    'currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & vbCrLf & vbCrLf & "停止中"
                    On Error Resume Next
                    ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = False
                    ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = True
                    On Error GoTo 0
                    'Wang Issue of UI improvement 2018/06/07 End
                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(255, 0, 0)
                End If
            End If
        Next currShape
        '* 表示テキストボックスがなければ追加する
        If isNotForm Then
            '* 設定されている表示範囲のアドレスを取得し、範囲を設定する
            RCAddress = GetDisplayAddress(currWorksheet)
            If RCAddress <> "" Then
                Set currRange = ActiveSheet.Range(RCAddress)
                '* 更新状況テキストボックスを追加
                btSize.Top = currRange.Top + 1
                btSize.Left = currRange.Left + 25
                btSize.Right = 72
                btSize.Bottom = 24
                Set currShape = PutTextBoxAtSheet(currWorksheet, btSize, RGB(0, 0, 0), RGB(255, 255, 255), PrefixStrTX, "ONLINE", "")
                If flagON Then
                    'Wang Issue of UI improvement 2018/06/07 Start
                    'currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & "更新中"
                    On Error Resume Next
                    ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = True
                    ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = False
                    On Error GoTo 0
                    'Wang Issue of UI improvement 2018/06/07 End
                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 255)
                Else
                    'Wang Issue of UI improvement 2018/06/07 Start
                    'currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & "停止中"
                    On Error Resume Next
                    ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = False
                    ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = True
                    On Error GoTo 0
                    'Wang Issue of UI improvement 2018/06/07 End
                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(255, 0, 0)
                End If
            End If
        End If
    Next currWorksheet
    
End Sub

'**************************
'* 自動更新タイマー起動開始
'**************************
Public Sub TimerStart()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range

    Call SetFilenameToVariable

    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
            
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'TimerLogic'"
        
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
        End If
        
    End With
    
    '* ピボットテーブル更新
    Call RefreshPivotTableData
    
    '* 更新中／停止中ステータス更新
    Call UpdateFormStatus
    
End Sub

'***********************
'* 自動更新タイマー処理
'***********************
Public Sub TimerLogic()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range
    Dim FinalTime       As Date
    Dim sampleType      As String
    
    Call SetFilenameToVariable

    '*Graph_Alarm sunyi 2018/09/14 start
    Call SetUpdateStartTime
    '*Graph_Alarm sunyi 2018/09/14 end

    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    
    With currWorksheet
    
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "ON" Then
                Application.CutCopyMode = False
                Exit Sub
            End If
        End If
    
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        FinalTime = endD + endT
        If Format(Now, "yyyy/MM/dd hh:mm:ss") >= FinalTime Then         '終了時刻になったら終わる
            MsgBox "終了時刻になりました。" & " " & Format(FinalTime, "hh:mm:ss")
            Call AutoUpdateStop
            Application.CutCopyMode = False
            Exit Sub
        End If
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If

        '追加20160606*******************************************
        sampleType = ""
        Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
        '*******************************************************
            
        ' 2016.07.22 Move ↓↓↓ *********************
        Application.ScreenUpdating = False
        
        If sampleType <> "GRIP" Then
            '* ミッションから情報を取得
            'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
            'Call GetMissionInfoAllTimer
            
            ' Wang Foastudio Issue AISTEMP-24 20181207 Start
            'Call GetMissionInfoAllTimer(SEARCH_TYPE_AUTO)
            WriteLog ("Main_Module:TimerLogic() Call GetMissionInfoAllTimer()")
            Call GetMissionInfoAllTimer(GetSearchType())
            ' Wang Foastudio Issue AISTEMP-24 20181207 End
            
            'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
        Else
            Call GetGripMission
        End If
                
        '* ピボットテーブル更新
        Call RefreshPivotTableData
    
        '* 更新状態表示更新
        Call UpdateFormStatus
        
        Application.ScreenUpdating = True
        ' 2016.07.22 Move ↑↑↑ *********************
        
        '* 次回の処理をスケジュール
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'TimerLogic'"
        
        '前回更新日時セット
        Set srchRange = .Cells.Find(What:="前回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevDate, "hh:mm:ss")
        End If
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
        End If
        
    End With
    
    ' 2016.07.22 Delete ↓↓↓ *********************
'    Application.ScreenUpdating = False
'
'    If sampleType <> "GRIP" Then
'        '* ミッションから情報を取得
'        Call GetMissionInfoAll
'    Else
'        Call GetGripMission
'    End If
'
'    '* ピボットテーブル更新
'    Call RefreshPivotTableData
'
'    '* 更新状態表示更新
'    Call UpdateFormStatus
'
'    Application.ScreenUpdating = True

    ' 2016.07.22 Delete ↑↑↑ *********************

    If Application.Visible = True Then
        'EXCEL前面表示用
        Call ExcelUpperDisp
        VBA.AppActivate Excel.Application.Caption
    
        Call ExcelDispSetWin32
        Call ExcelDispFreeWin32
    End If
    
    ' プッシュアラーム機能の呼び出し
    Call UpdateAfterGraphEdit
    'Call checkPushAlarm
    
End Sub

'****************************
'* ピボットテーブルを更新する
'****************************
Public Sub RefreshPivotTableData()
    Dim currWorksheet   As Worksheet
    Dim srcWorksheet    As Worksheet
    Dim workPVT         As PivotTable
    Dim pvtRange        As Range
    Dim startRange      As Range
    Dim dataRange       As Range
    Dim rightRange      As Range
    Dim lastRange       As Range
    Dim dataSrc         As String
    Dim dataNewSrc      As String
    Dim srcSheetName    As String
    Dim rangeValue      As Long '---2016/12/19 Add No.385

    '* 該当ブックの全シートに対して
    For Each currWorksheet In Workbooks(thisBookName).Worksheets
        '* 対象シートの全ピボットテーブルに対して
        For Each workPVT In currWorksheet.PivotTables
            With workPVT
                '* ピボットテーブルの範囲を取得
                Set pvtRange = .TableRange1
                
                '* 対象ピボットテーブルのデータソースを取得する
                dataSrc = .SourceData

                '* データソースのシート名を取得する
                '* 2016.08.03 Add ↓↓↓ シングルクォーテーションを文字列から削除 ******
                dataSrc = Replace(dataSrc, "'", "")
                '* 2016.08.03 Add ↑↑↑ ***********************************************
                srcSheetName = Left(dataSrc, InStr(dataSrc, "!") - 1)
                On Error Resume Next
'                Set srcWorksheet = Workbooks(thisBookName).Worksheets(srcSheetName)
                With Workbooks(thisBookName).Worksheets(srcSheetName)
                    Set startRange = .Range("A1")
                    Set rightRange = startRange.End(xlToRight)       ' 項目行の一番右側を取得
                    Set lastRange = startRange.End(xlDown)           ' データ行の最下行を取得
                    Set lastRange = .Cells(lastRange.Row, rightRange.Column)
                    Set dataRange = .Range(startRange, lastRange)
                    rangeValue = Len(.Range("A2").Value)  '---2016/12/19 Add No.385
                End With
                '---2016/12/19 Add No.385 データが存在するときのみ更新実行 IF文追加 -------------Start
                '* データが存在すればピボットテーブルを更新する
                If rangeValue > 0 Then
                  dataNewSrc = srcSheetName & "!" & dataRange.Address(ReferenceStyle:=xlR1C1, external:=False)
                  .SourceData = dataNewSrc
                  .RefreshTable
                End If
                '---2016/12/19 Add No.385 データが存在するときのみ更新実行 IF文追加 -------------End
            End With
        Next workPVT
    Next currWorksheet

End Sub

'*****************
'* ｢テスト｣の処理
'*****************
Public Sub OperationTest()
    Dim currWorksheet   As Worksheet
    Dim workPVT         As PivotTable
    Dim pvtRange        As Range
    Dim srchRange       As Range
    Dim sampleType      As String
    Dim wrkStr          As String

    Call SetFilenameToVariable

    '追加20160713*******************************************
    sampleType = ""
    Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
    '*******************************************************
    
    Application.ScreenUpdating = False
    
    '* 2016/09/13 Add ↓↓↓**************************************
    'オンライン種別の取込
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="オンライン種別", LookAt:=xlWhole)

    If Not srchRange Is Nothing Then
        If IsEmpty(srchRange.Offset(0, 1).Value) Then
            wrkStr = "期間固定"
        Else
            wrkStr = srchRange.Offset(0, 1).Value
        End If
    Else
        wrkStr = "期間固定"
    End If
    
    ' オンライン種別毎にタイマー処理起動
    If (wrkStr = "期間固定") Or (wrkStr = "連続移動") Then
        If sampleType <> "GRIP" Then
            '* ミッションから情報を取得
            'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
            'Call GetMissionInfoAll
            Call GetMissionInfoAll(SEARCH_TYPE_TEST)
            'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
        Else
            Call GetGripMission
        End If
    ElseIf wrkStr = "一定時間繰り返し" Then
        '一定時間繰返し処理
        If sampleType <> "GRIP" Then
            '* ミッションから情報を取得
            'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
            'Call Kurikaeshi_GetMissionInfoAll
            Call Kurikaeshi_GetMissionInfoAll(SEARCH_TYPE_TEST)
            'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
        Else
            Call GetGripMission
        End If
    ElseIf wrkStr = "毎日定刻繰り返し" Then
        '一定時間繰返し処理
        If sampleType <> "GRIP" Then
            '* ミッションから情報を取得
            'Call Teikoku_GetMissionInfoAll  'No.683 Mod.
            'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
            'Call Teikoku_GetMissionInfoAll_Test
            Call Teikoku_GetMissionInfoAll_Test(SEARCH_TYPE_TEST)
            'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
        Else
            Call GetGripMission
        End If
        '毎日定刻繰返し処理
    End If
    '* 2016/09/13 Add ↑↑↑**************************************
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With currWorksheet
    
        '前回／次回更新日時セット
        Set srchRange = .Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd hh:mm:ss")
        End If
        Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd hh:mm:ss")
        End If
    
    End With
    
    Call RefreshPivotTableData
    
    '* 更新状態表示更新
    Call UpdateFormStatus

    Application.ScreenUpdating = True
    
End Sub

'*****************************
'* スケジュール予約キャンセル
'*****************************
Public Sub CancelSchedule()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim prevDate        As Date
    Dim prevTime        As Date

    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)

    With currWorksheet
        '* 予約したスケジュールをキャンセルする
        Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            prevDate = srchRange.Offset(0, 1).Value
            prevTime = srchRange.Offset(0, 2).Value
            On Error Resume Next
            Application.OnTime prevDate + prevTime, "'TimerLogic'", , False
        End If
    End With

End Sub

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSub()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
    Dim objFso          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    Dim IsFirstSave     As String
    
    quitFlag = False
    
    Call SetFilenameToVariable
    
    Call CancelSchedule

    Set objFso = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With Workbooks(thisBookName).Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then IsFirstSave = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
    currThisFile = Workbooks(thisBookName).FullName
'        Application.DisplayAlerts = False
'        ActiveWorkbook.SaveAs Filename:=currThisFile
'        Application.DisplayAlerts = True

'    wrkInt = InStr(Workbooks(thisBookName).name, templateName)
'
'    If wrkInt <= 0 Or wrkInt > 4 Then

    If IsFirstSave = "" Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & Workbooks(thisBookName).name
        Else
            fname = saveFilePath & "\" & templateName & "_" & Workbooks(thisBookName).name
        End If
        
        '* テンプレート名／登録フォルダ名を取得する
        With Workbooks(thisBookName).Worksheets(ParamSheetName).Columns(1)
            Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "TRUE"
        End With
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & Workbooks(thisBookName).name
        Else
            fname = saveFilePath & "\" & Workbooks(thisBookName).name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
    If Dir(fname) <> "" Then
      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
    End If
    
    'メニューバー表示
    Call DispBar
    
    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    Application.DisplayAlerts = False
    
    '* ISSUE_NO.624 Add ↓↓↓ *******************************
    On Error GoTo ErrorHandler
    
    Workbooks(thisBookName).SaveAs FileName:=fname
    
ErrorHandler:
    '-- 例外処理
    If Err.Description <> "" Then
        MsgBox Err.Description, vbCritical & vbOKOnly, "警告"
    End If
    '* ISSUE_NO.624 Add ↑↑↑ *******************************
    
    If Application.Workbooks.Count > 1 Then
        ThisWorkbook.Close
    Else
        Application.Quit
        ThisWorkbook.Close
    End If
    If Err.Description = "" Then
        Application.DisplayAlerts = True
    End If
End Sub

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSubFromX()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
    Dim objFso          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "ON" Then
            Call CancelSchedule
        End If
    End If

    Set objFso = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With Workbooks(thisBookName).Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
    currThisFile = Workbooks(thisBookName).FullName
'        Application.DisplayAlerts = False
'        ActiveWorkbook.SaveAs Filename:=currThisFile
'        Application.DisplayAlerts = True

    wrkInt = InStr(Workbooks(thisBookName).name, templateName)
'
    If wrkInt <= 0 Or wrkInt > 4 Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & Workbooks(thisBookName).name
        Else
            fname = saveFilePath & "\" & templateName & "_" & Workbooks(thisBookName).name
        End If
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & Workbooks(thisBookName).name
        Else
            fname = saveFilePath & "\" & Workbooks(thisBookName).name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
'    If Dir(fname) <> "" Then
'      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
'      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
'    End If
    
    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    Application.DisplayAlerts = False
    Workbooks(thisBookName).SaveAs FileName:=currThisFile
    If Application.Workbooks.Count > 1 Then
        ThisWorkbook.Close
    Else
        Application.DisplayAlerts = False
    
        ThisWorkbook.Save
    
        Application.Quit
        ThisWorkbook.Close
    End If
    'Application.DisplayAlerts = True
        
End Sub


