Attribute VB_Name = "MissionDiv_Module"
Option Explicit

'****************************************************************
'* 対象ミッションの情報を取得する
'* →プログラムミッションのみ対応
'****************************************************************
Public Function GetDivideMissionInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO, missionID As String, startTime As Double, endTime As Double) As Integer
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim SendUrlStr  As String
    Dim GetString   As String
    Dim srchRange   As Range
    Dim iResult     As Integer
    Dim aaa         As Variant
    Dim II          As Integer
    Dim currCTM     As CTMINFO
    
'    Dim workFolder  As String
'    Dim workFile    As String
'    Dim fileLength  As Long
    Dim divideNum   As Integer  ' 分割取得回数
    Dim divideTime  As Double
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    
    
    divideNum = 0   ' 分割取得する必要なし
    
    With Workbooks(thisBookName).Worksheets(ParamSheetName)

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If
        
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
        
        
    End With
    
    '*******************************
    '* CMSに問合せ
    '*******************************
    SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pm?id=" & missionID & "&start=" & Format(startTime) & "&end=" & Format(endTime) & _
                    "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true"
    
    If useProxy = False Then
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
    Else
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
        httpObj.setProxy 2, proxyUri
    End If
    
    httpObj.Open "GET", SendUrlStr, False
    httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
    httpObj.send
    
    ' ダウンロード待ち
    Do While httpObj.readyState <> 4
        DoEvents
    Loop
    
    ' 2016.07.22 Add ↓↓↓ *****************************
    On Error GoTo ErrorHandler
    ' 2016.07.22 Add ↑↑↑ *****************************
    
    '* 取得情報のデコード
    Set sc = CreateObject("ScriptControl")
    With sc
        .Language = "JScript"

        '指定したインデックス、名称のデータを取得する
        .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
    End With
    GetString = httpObj.responseText
    Set objJSON = sc.CodeObject.jsonParse(GetString)

    Set sc = Nothing
    
    Set httpObj = Nothing

    '* 取得情報をCTM情報に展開
    ReDim ctmValue(0)
    ctmValue(0).ID = ""
    iResult = GetCTMInfo(objJSON, ctmList, ctmValue)

    Set objJSON = Nothing
        
    ' MsgBox http.responseText
    GetDivideMissionInfo = iResult
    
    Exit Function

ErrorHandler:

    Set sc = Nothing
    Set httpObj = Nothing
    Set objJSON = Nothing

    '* エラーが発生したら-1を返す
    GetDivideMissionInfo = -1
    
End Function

'****************************************************************
'* 対象ミッションの情報を取得する
'****************************************************************
Public Function GetDivideCTMInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO, startTime As Double, endTime As Double) As Integer
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim CtmId       As String
    Dim SendUrlStr  As String
    Dim SendString  As Variant
    Dim GetString   As String
    Dim srchRange   As Range
    Dim iResult     As Integer
    Dim bPmary      As Variant
    Dim II          As Integer
    Dim JJ          As Integer
    Dim currCTM     As CTMINFO
    
    Dim cmsVersion  As String
    Dim mfIPAddr    As String
    Dim mfPortNo    As String
    Dim chgStr01    As String
    Dim chgStr02    As String
    
    Dim divideNum   As Integer  ' 分割取得回数
    Dim divideTime  As Double
    Dim wkStartTime As Double
    Dim wkEndTime   As Double
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    
    
    With Workbooks(thisBookName).Worksheets(ParamSheetName)

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If

        ' 2016.07.20 Add *********************
        '* CMSバージョンを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="CmsVersion", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            cmsVersion = srchRange.Offset(0, 1).Value
        Else
            cmsVersion = "V5"
        End If
        
        ' 2016.07.20 Add *********************
        '* 旧バージョン用IPアドレスおよびPortNoを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="MfHost", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            mfIPAddr = srchRange.Offset(0, 1).Value
            mfPortNo = srchRange.Offset(1, 1).Value
        Else
            mfIPAddr = "localhost"
            mfPortNo = "50031"
        End If
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
        
        
    End With
    
    ReDim ctmValue(0)
    ctmValue(0).ID = ""
    For II = 0 To UBound(ctmList)

        currCTM = ctmList(II)
        CtmId = currCTM.ID
    
        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/ctm/find?testing=0"
        
        If useProxy = False Then
            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
        Else
            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
            httpObj.setProxy 2, proxyUri
        End If
        
        httpObj.Open "POST", SendUrlStr, False
        httpObj.setRequestHeader "Content-Type", "text/plain"
        httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***Content-Type: text/plain
        
        ' 2016.07.20 Change ↓↓↓ ***************************
        ' 2016.07.22 Change ↓↓↓ *** POST文字列に「testing=0」を追加
        If cmsVersion = "3.5" Then
            SendString = "{""ctmId"":""" & CtmId & """,""start"":" & Format(startTime) & ",""end"":" & Format(endTime) & _
                           ",""mfHostName"":" & """" & mfIPAddr & """" & ",""mfPort"":" & """" & mfPortNo & """" & "}"
        Else
            SendString = "{""ctmId"":""" & CtmId & """,""start"":" & Format(startTime) & ",""end"":" & Format(endTime) & "}"
        End If
        ' 2016.07.22 Change ↑↑↑ ***************************
        ' 2016.07.20 Change ↑↑↑ ***************************
        
        bPmary = StrConv(SendString, vbFromUnicode)
        
        httpObj.send (SendString)
        
        ' ダウンロード待ち
        Do While httpObj.readyState <> 4
            DoEvents
        Loop
        
        ' 2016.07.22 Add ↓↓↓ *****************************
        On Error GoTo ErrorHandler
        ' 2016.07.22 Add ↑↑↑ *****************************
        '* 取得情報のデコード
        Set sc = CreateObject("ScriptControl")
        With sc
            .Language = "JScript"
    
            '指定したインデックス、名称のデータを取得する
            .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
        End With
        
        GetString = httpObj.responseText
        
        ' 2016.07.20 Add ***************************
        If cmsVersion = "3.5" Then
            chgStr01 = Replace(GetString, "\""", "@@")
            chgStr02 = Replace(chgStr01, """", "")
            GetString = Replace(chgStr02, "@@", """")
        End If
        ' 2016.07.20 Add ***************************
        
        Set objJSON = sc.CodeObject.jsonParse(GetString)
        
        Set sc = Nothing
        
        Set httpObj = Nothing
    
        '* 取得情報をCTM情報に展開
        iResult = GetCTMInfoForCTM(objJSON, currCTM, ctmValue)
        
        Set objJSON = Nothing
    
    Next
    
    ' MsgBox http.responseText
    GetDivideCTMInfo = iResult
    
    Exit Function

ErrorHandler:
    
    Set sc = Nothing
    Set httpObj = Nothing
    Set objJSON = Nothing

    GetDivideCTMInfo = -1
    
    Exit Function
        
End Function



