Attribute VB_Name = "GraphSet_Module"
Option Explicit

'* 最大／最小／刻み幅情報
Public Type GRAPH_VALUE_INFO
    MaxValue        As Integer
    MinValue        As Integer
    StepValue       As Integer
    DataCount       As Integer
End Type

'* 閾値
Public Type THRESHOLD_INFO
    MaxName         As String
    MinName        As String

    MaxValue        As Double
    MinValue        As Double
End Type

'* ミッション名/CMT名/エレメント名
Public Type MISSION_INFO
    MissionName As String
    CtmName As String
    ElementName As String
End Type


Dim graphValueInfo     As GRAPH_VALUE_INFO
Dim thresholdInfo      As THRESHOLD_INFO
Dim missionInfo        As MISSION_INFO

    

'*************************************
'* グラフ再作成処理
'*************************************
Public Sub UpdateAfterGraphEdit(Optional OnServer As String = "False")
    
    Dim test As Worksheet
    
    Dim dataNum As Integer
    
    
    ''''''''''''''''''''''''''''''''
    '* グラフ編集シートから情報取得
    ''''''''''''''''''''''''''''''''
    'ミッション名・CTM名・エレメント名を取得する。
    Call GetMissionInfo2(GraphEditSheetName, missionInfo)
    '縦軸の最大／最小／刻み幅/表示件数を取得する
    Call GetGraphValueInfo(GraphEditSheetName, graphValueInfo)
    
    '* GraphAlarm
'    '上限・下限を取得を取得する
'    Call GetThresholdInfo(GraphAlarmSheetName, thresholdInfo)

    '* GraphAlarm
    
    
    
    'データ無しの場合グラフの修正しない。20161028 ADD
    '****** 2017/02/24 (CInt(hValue.MinValue) > 0---->(CInt(hValue.MinValue) >= 0
    If (CDbl(graphValueInfo.MaxValue) > 0) And (CDbl(graphValueInfo.StepValue) > 0) Then
    Else
        Exit Sub
    End If
    
    '* グラフ用データの作成
    dataNum = goMakeDataForGraph
    
    '* グラフの作成
    Call createGraph(OnServer)
    
End Sub
'    '* GraphAlarm Start
'Public Sub UpdateAfterGraphEdit_Check()
'    Dim dataNum As Integer
'
'    Call OperationTest
'    dataNum = goMakeDataForGraph_Check()
'
'    MsgBox ("GraphAlarm" & Format(Now, "yyyy/MM/dd hh:mm:ss"))
'End Sub
'    '* GraphAlarm End

'***************************************************
'* グラフ編集シートから最大／最小／刻み幅/データ件数を取得する
'***************************************************
Public Sub GetGraphValueInfo(currSheetName As String, graphValueInfo As GRAPH_VALUE_INFO)
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range

    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Set currWorksheet = Worksheets(currSheetName)
    With currWorksheet
        Set srchRange = .Cells.Find(What:="−縦軸−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            graphValueInfo.MaxValue = srchRange.Offset(1, 1).Value
            graphValueInfo.MinValue = srchRange.Offset(2, 1).Value
            graphValueInfo.StepValue = srchRange.Offset(3, 1).Value
        End If
        Set srchRange = .Cells.Find(What:="−データ件数−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            graphValueInfo.DataCount = srchRange.Offset(1, 1).Value
        End If
    End With
        

End Sub


    
    '* GraphAlarm
''***************************************************
''* グラフ編集シートから閾値を取得する
''***************************************************
'Public Sub GetThresholdInfo(currSheetName As String, thresholdInfo As THRESHOLD_INFO)
'    Dim currWorksheet           As Worksheet
'    Dim srchRange               As Range
'
'    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
'    Set currWorksheet = Worksheets(currSheetName)
'    With currWorksheet
'        Set srchRange = .Cells.Find(What:="アラーム_NO", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            thresholdInfo.MaxName = srchRange.Offset(1, 0).Value
'            thresholdInfo.MinName = srchRange.Offset(2, 0).Value
'
'            thresholdInfo.MaxValue = srchRange.Offset(1, 1).Value
'            thresholdInfo.MinValue = srchRange.Offset(2, 1).Value
'        End If
'    End With
'
'
'End Sub
'* GraphAlarm


'***************************************************
'* グラフ編集シートからミッション情報を取得する
'***************************************************
Public Sub GetMissionInfo2(currSheetName As String, missionInfo As MISSION_INFO)
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range

    '* グラフ編集シートからミッション名、CTM名、エレメント名を取得する
    Set currWorksheet = Worksheets(currSheetName)
    With currWorksheet
        Set srchRange = .Cells.Find(What:="−ミッション−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionInfo.MissionName = srchRange.Offset(1, 1).Value
            missionInfo.CtmName = srchRange.Offset(2, 1).Value
            missionInfo.ElementName = srchRange.Offset(3, 1).Value
        End If
    End With
        

End Sub




'*********************
'* 基準線を再配置する
'*********************
Public Sub GraphReCalculate()
    
    Dim graphEditWorksheet  As Worksheet
    
    Dim vBaseList()         As THRESHOLD_INFO
    
    ReDim vBaseList(0)
    
    
    '* グラフ編集のワークシート
    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    
    '* グラフから古い系統を削除する
    Call DeleteOldBaseLineSeries
    
    Call AddBaseLineSeries
    
    
    '* グラフテーブルから古い基準線データを削除する
    'Call DeleteOldBaseLineData
    
    '* グラフテーブルに新しい基準線データを生成する
    'Call AddNewBaseLineData
    
    '* グラフに新しい系統を追加する
    'Set currChartObj = graphEditWorksheet.ChartObjects(HistGraph02Name)
    'Call AddNewBaseLineSeries(currChartObj)
    'Set currChartObj = graphWorksheet.ChartObjects(HistGraph02Name)
    'Call AddNewBaseLineSeries(currChartObj)
    
    '* グラフ編集シートの基準／上限／下限の水平方向の文字列を合わせる
    'Call MoveShapeHorizonInGraphAll(graphEditWorksheet, vBaseList, hBaseList, vValue, hValue)

    '* 基準／上限／下限の水平方向の文字列を合わせる
    'Call MoveShapeHorizonInGraphAll(graphWorksheet, vBaseList, hBaseList, vValue, hValue)
End Sub



'*******************************
'* グラフから古い系統を削除する
'*******************************
Public Sub DeleteOldBaseLineSeries()

    Dim currSeries              As Series
    
    
    Count = 0
    For Each currSeries In graphInfo.GraphObj.Chart.SeriesCollection
        If currSeries.name = thresholdInfo.MaxName Then
        
        ElseIf currSeries.name = thresholdInfo.MinName Then
        
        
        End If
        
    Next currSeries

End Sub



'*******************************
'* グラフに系統を追加する
'*******************************
Public Sub AddBaseLineSeries()

    Dim srs As Series
    Dim graphRange As Range
    
    Dim Chart As Chart
    
    Set Chart = graphInfo.GraphObj.Chart
    Set graphRange = ThisWorkbook.Worksheets(GraphEditSheetName).Range("F2", "G35")
    
    'Set srs = graphInfo.GraphObj.chart.SeriesCollection.Add(Range("グラフ編集!F2", "グラフ編集!G35"))
    Set srs = Chart.SeriesCollection.Add(graphRange)
    srs.name = Sheets("グラフ編集").Cells("F1").Value
    srs.XValues = Range("Sheet1!A2", "Sheet1!A13711")

End Sub



'*************************************
'* グラフ用のデータを生成する
'*************************************
Public Function goMakeDataForGraph() As Integer
    
    Dim dataSheet As Worksheet
    Dim GraphEditSheet As Worksheet
    
    Dim startRange As Range
    Dim endRange As Range
    
    Dim maxMinRange As Range
    
    '* GraphAlarm
    Dim test As String
    '/09/09
    Dim startTime As String
    Dim mainSheet As Worksheet
    Dim GraphAlarmSheet As Worksheet
    Dim currRange As Range
    Dim CTMRange As Range
    
    Dim CtmIdRange As Range
    Dim ElementIdRange As Range
    Dim CtmId As String
    Dim ElementId As String
    Dim II As Integer
    
    Dim writeRange As Range
    Dim IsGraphAlarmRange As Range
    Dim xRange As Range
    Dim yRange As Range
    Dim maxRange As Range
    Dim minRange As Range
    
    Set mainSheet = Sheets(MainSheetName)
    Set GraphAlarmSheet = Sheets(GraphAlarmSheetName)
    '* GraphAlarm
    
    Set GraphEditSheet = Sheets(GraphEditSheetName)
    'Set dataSheet = Sheets(ResultSheetName)
    Set dataSheet = Sheets(missionInfo.CtmName)
    GraphEditSheet.Select
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '貼付け先のデータの削除
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '削除開始セルの取得
    Set startRange = GraphEditSheet.Cells.Find(What:="グラフ用データ", LookAt:=xlWhole)
    Set startRange = startRange.Offset(1, 0)
    '削除終了セルの取得
    Set endRange = startRange.Offset(0, 18)
    Set endRange = endRange.End(xlDown)
    
    Range(startRange, endRange).Clear
    
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    '貼付け先のデータの貼付け(頭から固定件数を貼り付ける）
    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' 受信時刻（RECV_TIME）の貼付け
    Set startRange = dataSheet.Cells.Find(What:="RECEIVE TIME", LookAt:=xlWhole)
    If Not startRange Is Nothing Then
        Set endRange = startRange.Offset(graphValueInfo.DataCount, 0)
        'Set endRange = endRange.End(xlUp)
        Range(startRange, endRange).Copy
        'Range(startRange, startRange.End(xlDown)).Copy
        GraphEditSheet.Range("F2").PasteSpecial
    End If
    '* GraphAlarm
    Set currRange = GraphEditSheet.Cells.Find(What:="グラフ用データ", LookAt:=xlWhole)
    If Not currRange Is Nothing Then
        Set writeRange = currRange.Offset(1, 0)
    Else
        Set writeRange = Sheets(GraphEditSheetName).Range("F2").Value
    End If
    Set xRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 0), writeRange.End(xlDown).Offset(0, 0))
    Set yRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 1), writeRange.End(xlDown).Offset(0, 1))
    
'    Set maxRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 2), writeRange.End(xlDown).Offset(0, 2))
'    Set minRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 3), writeRange.End(xlDown).Offset(0, 3))

'    graphEditSheet.Range(xRange, minRange).Sort Key1:=xRange, Order1:=xlAscending, Header:=xlYes
    GraphEditSheet.Range(xRange, yRange).NumberFormatLocal = "yyyy/m/d h:mm;@"
    '* GraphAlarm
    '* GraphAlarm
    'エレメント検索結果を検索
    Set startRange = dataSheet.Cells.Find(What:=missionInfo.ElementName, LookAt:=xlWhole)
    If Not startRange Is Nothing Then
        Set endRange = startRange.Offset(graphValueInfo.DataCount, 0)
        'Set endRange = endRange.End(xlUp)
        Range(startRange, endRange).Copy
        'Range(startRange, startRange.End(xlDown)).Copy
        GraphEditSheet.Range("G2").PasteSpecial
    End If
    
    '09/14 sort
    Set yRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 2), writeRange.End(xlDown).Offset(0, 2))
    GraphEditSheet.Range(xRange, yRange).Sort Key1:=xRange, Order1:=xlAscending, Header:=xlYes
    
    'データ件数の確認
    Set startRange = GraphEditSheet.Cells.Find(What:="RECEIVE TIME", LookAt:=xlWhole)
    If Not startRange Is Nothing Then
        Set endRange = startRange.Offset(graphValueInfo.DataCount + 10, 0)
        Set endRange = endRange.End(xlUp)
    End If
    goMakeDataForGraph = endRange.Row
    '* GraphAlarm start
'    '最大値、最小値を設定する
'    graphEditSheet.Range("H2:" & "H" & (goMakeDataForGraph)).Select
'    Set maxMinRange = Selection
'    maxMinRange.Value = thresholdInfo.MaxValue
'
'    graphEditSheet.Range("I2").Value = thresholdInfo.MinName
'    graphEditSheet.Range("I2:" & "I" & (goMakeDataForGraph)).Select
'    Set maxMinRange = Selection
'    maxMinRange.Value = thresholdInfo.MinValue

    Set CTMRange = mainSheet.Cells.Find(What:=missionInfo.CtmName, LookAt:=xlWhole)
    
    CtmId = CTMRange.Offset(1, 0).Value
    For II = 1 To 250
        If CTMRange.Offset(0, II).Value = missionInfo.ElementName Then
            ElementId = CTMRange.Offset(1, II).Value
            Exit For
        End If
    Next II
    
    Set CtmIdRange = GraphAlarmSheet.Cells.Find(What:="グラフCTMID", LookAt:=xlWhole)
    CtmIdRange.Offset(0, 1).Value = CtmId
    
    Set ElementIdRange = GraphAlarmSheet.Cells.Find(What:="グラフELEMENTID", LookAt:=xlWhole)
    ElementIdRange.Offset(0, 1).Value = ElementId
    
    '* グラフアラームの判定
    If GraphAlarm_Param_Check() = "True" Then
        Set IsGraphAlarmRange = GraphAlarmSheet.Cells.Find(What:="IsGraphAlarmグラフ", LookAt:=xlWhole)
        IsGraphAlarmRange.Offset(0, 1).Value = "True"
    End If
    
    '* GraphAlarm end
    goMakeDataForGraph = goMakeDataForGraph - 1
End Function

'* GraphAlarm start

Public Sub SetUpdateStartTime()

    Dim dataSheet As Worksheet
    Dim GraphAlarmSheet As Worksheet
    Dim GraphEditSheet  As Worksheet
    Dim startTime As String
    Dim timeRange As Range
    Dim CTMRange As Range
    
    '/09/05
    Set GraphEditSheet = Sheets(GraphEditSheetName)
    Set GraphAlarmSheet = Sheets(GraphAlarmSheetName)
    
    Set CTMRange = GraphEditSheet.Cells.Find(What:="CTM名", LookAt:=xlWhole)
    '* AISTEMP-15 sunyi 2018/11/09 start
    '* グラフアラームが設定しない場合のチェック追加
    If CTMRange.Offset(0, 1).Value <> "" Then
    '* AISTEMP-15 sunyi 2018/11/09 end
        Set dataSheet = Sheets(CTMRange.Offset(0, 1).Value)
        
        Set timeRange = GraphAlarmSheet.Cells.Find(What:="更新開始時刻", LookAt:=xlWhole)
        timeRange.Offset(0, 1).Value = Format(dataSheet.Cells(2, 1), "yyyy/MM/dd hh:mm:ss")
    End If
End Sub

Public Function GraphAlarm_Param_Check() As String
    Dim II  As Integer
    Dim noRange As Range
    Dim GraphAlarmRange As Range
    Dim GraphAlarmSheet As Worksheet
    
    Set GraphAlarmSheet = Sheets(GraphAlarmSheetName)
    
    Set noRange = GraphAlarmSheet.Cells.Find(What:="アラームライン名称", LookAt:=xlWhole)
    Set GraphAlarmRange = GraphAlarmSheet.Cells.Find(What:="GraphAlarm設定", LookAt:=xlWhole)
    For II = 1 To 11
    '/09/05
        If IsEmpty(noRange.Offset(0, II).Value) = False Then
            GraphAlarmRange.Offset(0, 1).Value = "True"
            Exit For
        Else
            GraphAlarmRange.Offset(0, 1).Value = "False"
        End If
    Next II
    
    GraphAlarm_Param_Check = GraphAlarmRange.Offset(0, 1).Value
End Function

'* GraphAlarm start

'Graph_Alarm sunyi 2018/09/07 Strat
'***********************
'* Graph_Alarm_Check
'***********************
Public Sub Graph_Alarm_Check()
    Dim currWorksheet As Worksheet
    Dim srchRange     As Range
    Dim wrkStr As String
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="オンライン種別", LookAt:=xlWhole)
        
    If Not srchRange Is Nothing Then
        If IsEmpty(srchRange.Offset(0, 1).Value) Then
            wrkStr = "期間固定"
        Else
            wrkStr = srchRange.Offset(0, 1).Value
        End If
    Else
        wrkStr = "期間固定"
    End If
    
    ' オンライン種別毎にタイマー処理起動
    If (wrkStr = "期間固定") Or (wrkStr = "連続移動") Then
        Call Graph_Alarm_Check_Timer
    ElseIf wrkStr = "一定時間繰り返し" Then
        '一定時間繰返し処理
        Call Graph_Alarm_Check_Kurikaeshi
    ElseIf wrkStr = "毎日定刻繰り返し" Then
        '毎日定刻繰返し処理
        Call Graph_Alarm_Check_Teikoku
    End If
    
End Sub

'***********************
'* GraphAlarm処理
'***********************
Public Sub Graph_Alarm_Check_Kurikaeshi()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range
    Dim FinalTime       As Date
    Dim sampleType      As String
    
    Call SetFilenameToVariable
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    
    '*Graph_Alarm sunyi 2018/09/14 start
    Call SetUpdateStartTime
    '*Graph_Alarm sunyi 2018/09/14 end
    With currWorksheet
    
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
        
        'オンライン停止判定
        FinalTime = endD + endT
        If Format(Now, "yyyy/MM/dd hh:mm:ss") >= FinalTime Then         '終了時刻になったら終わる
            'MsgBox "終了時刻になりました。" & " " & Format(FinalTime, "hh:mm:ss")
            Call AutoUpdateStop
            Application.CutCopyMode = False
            Exit Sub
        End If
    
        ' 収集タイプの取得
        sampleType = ""
        Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
        
        'CTM,ミッションの取込
        Application.ScreenUpdating = False
            If sampleType <> "GRIP" Then
                '* ミッションから情報を取得
                'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
                'Call Kurikaeshi_GetMissionInfoAll
                'Graph_Alarm sunyi 2018/09/12 start
                
                ' Wang Foastudio Issue AISTEMP-24 20181207 Start
                'Call Kurikaeshi_GetMissionInfoAll(SEARCH_TYPE_AUTO, "True")
                Call Kurikaeshi_GetMissionInfoAll(GetSearchType(), "True")
                ' Wang Foastudio Issue AISTEMP-24 20181207 End
                
                'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
            Else
                Call GetGripMission
            End If

            '* ピボットテーブル更新
            Call RefreshPivotTableData

            '* 更新状態表示更新
'            Call UpdateFormStatus
        Application.ScreenUpdating = True

        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
        
        '* 次回の処理をスケジュール
        prevDate = Now
'        prevTime = DateAdd("s", updTime, prevDate)
'        Application.OnTime prevTime, "'Kurikaeshi_TimerLogic'"
        
        '前回更新日時セット
        Set srchRange = .Cells.Find(What:="前回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevDate, "hh:mm:ss")
        End If
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
        End If
    End With

'    'EXCEL前面表示用
'    If Application.Visible = True Then
'        Call ExcelUpperDisp
'        VBA.AppActivate Excel.Application.Caption
'
'        Call ExcelDispSetWin32
'        Call ExcelDispFreeWin32
'    End If
    
    ' プッシュアラーム機能の呼び出し
    Call UpdateAfterGraphEdit("True")
'    Call checkPushAlarm
    
End Sub


Public Sub Graph_Alarm_Check_Teikoku()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Variant      ' 2016.08.03 Change Integer->Variant
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim startD          As Date
    Dim startT          As Date
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range
    Dim FinalTime       As Date
    Dim sampleType      As String
    Dim dispTerm        As Double      ' 2016.08.03 Change Integer->Double
    Dim dispTermUnit    As String
    
    Call SetFilenameToVariable
    
    '*Graph_Alarm sunyi 2018/09/14 start
    Call SetUpdateStartTime
    '*Graph_Alarm sunyi 2018/09/14 end
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    With currWorksheet
'        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            If srchRange.Offset(0, 1).Value <> "ON" Then
'                Application.CutCopyMode = False
'                Exit Sub
'            End If
'        End If
    
        'オンライン停止チェック
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
        
        ' オンライン停止判定
        FinalTime = endD + endT
        If Format(Now, "yyyy/MM/dd hh:mm:ss") >= FinalTime Then         '終了時刻になったら終わる
'            MsgBox "終了時刻になりました。" & " " & Format(FinalTime, "hh:mm:ss")
            Call AutoUpdateStop
            Application.CutCopyMode = False
            Exit Sub
        End If
        
        '本日分を停止し明日を予約する。
        '*********************
        '* 収集開始日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(srchRange.Offset(0, 1).Value)
            startT = TimeValue(Format(srchRange.Offset(0, 2).Value, "hh:mm:ss"))
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            'MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        '* 2016/09/13 Add ↓↓↓*********************************************
        startD = Date
        '* 2016/09/13 Add ↑↑↑*********************************************
        '*******************************
        '* 表示期間を取得
        '*******************************
        Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                dispTerm = 1#
                dispTermUnit = "【時】"
            Else
                dispTerm = srchRange.Offset(0, 1).Value
                dispTermUnit = srchRange.Offset(0, 2).Value
            End If
        Else
            dispTerm = 1#
            dispTermUnit = "【時】"
        End If
        
        Select Case dispTermUnit
            Case "【分】"
                dispTerm = dispTerm * 60
            Case "【時】"
                dispTerm = dispTerm * 3600
        End Select
        
        prevDate = DateAdd("s", dispTerm, startD + startT)
        If Now > prevDate Then
            prevDate = DateAdd("d", 1, startD + startT)
            '--2017/05/02 Del No.686
            'Call AutoUpdateStop
            'Application.OnTime prevTime, "'Teikoku_TimerLogic'"     '本日起動タイマーセット
            
            '次回更新日時セット
            Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd")
                srchRange.Offset(0, 2).Value = Format(prevDate, "hh:mm:ss")
            End If
            
            Exit Sub
        End If
    
        ' 収集タイプの取得
        sampleType = ""
        Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
        
        'CTM,ミッションの取込
        Application.ScreenUpdating = False
            If sampleType <> "GRIP" Then
                '* ミッションから情報を取得
                'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
                'Call Teikoku_GetMissionInfoAll
                
                ' Wang Foastudio Issue AISTEMP-24 20181207 Start
                'Call Teikoku_GetMissionInfoAll(SEARCH_TYPE_AUTO, "True")
                Call Teikoku_GetMissionInfoAll(GetSearchType(), "True")
                ' Wang Foastudio Issue AISTEMP-24 20181207 End
                
                'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
            Else
                Call GetGripMission
            End If

            '* ピボットテーブル更新
            Call RefreshPivotTableData

'            '* 更新状態表示更新
'            Call UpdateFormStatus
        Application.ScreenUpdating = True

        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
        
        '* 次回の処理をスケジュール
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'Teikoku_TimerLogic'"
        
        '前回更新日時セット
        Set srchRange = .Cells.Find(What:="前回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevDate, "hh:mm:ss")
        End If
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
        End If
    End With

'    'EXCEL前面表示用
'    If Application.Visible = True Then
'        Call ExcelUpperDisp
'        VBA.AppActivate Excel.Application.Caption
'
'        Call ExcelDispSetWin32
'        Call ExcelDispFreeWin32
'    End If
    
    ' プッシュアラーム機能の呼び出し
    Call UpdateAfterGraphEdit("True")
'    Call checkPushAlarm
    
End Sub


'***********************
'* 自動更新タイマー処理
'***********************
Public Sub Graph_Alarm_Check_Timer()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range
    Dim FinalTime       As Date
    Dim sampleType      As String
    
    Call SetFilenameToVariable

    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    
    '*Graph_Alarm sunyi 2018/09/14 start
    Call SetUpdateStartTime
    '*Graph_Alarm sunyi 2018/09/14 end
    
    With currWorksheet
    
'        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            If srchRange.Offset(0, 1).Value <> "ON" Then
'                Application.CutCopyMode = False
'                Exit Sub
'            End If
'        End If
    
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        FinalTime = endD + endT
        If Format(Now, "yyyy/MM/dd hh:mm:ss") >= FinalTime Then         '終了時刻になったら終わる
'            MsgBox "終了時刻になりました。" & " " & Format(FinalTime, "hh:mm:ss")
            Call AutoUpdateStop
            Application.CutCopyMode = False
            Exit Sub
        End If
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If

        '追加20160606*******************************************
        sampleType = ""
        Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
        '*******************************************************
            
        ' 2016.07.22 Move ↓↓↓ *********************
        Application.ScreenUpdating = False
        
        If sampleType <> "GRIP" Then
            '* ミッションから情報を取得
            'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
            'Call GetMissionInfoAllTimer
            
            ' Wang Foastudio Issue AISTEMP-24 20181207 Start
            'Call GetMissionInfoAllTimer(SEARCH_TYPE_AUTO, "True")
            Call GetMissionInfoAllTimer(GetSearchType(), "True")
            ' Wang Foastudio Issue AISTEMP-24 20181207 End
            
            'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
        Else
            Call GetGripMission
        End If
                
        '* ピボットテーブル更新
        Call RefreshPivotTableData
    
'        '* 更新状態表示更新
'        Call UpdateFormStatus
        
        Application.ScreenUpdating = True
        ' 2016.07.22 Move ↑↑↑ *********************
        
        '* 次回の処理をスケジュール
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
'        Application.OnTime prevTime, "'TimerLogic'"
        
        '前回更新日時セット
        Set srchRange = .Cells.Find(What:="前回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevDate, "hh:mm:ss")
        End If
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
        End If
        
    End With
    
    ' 2016.07.22 Delete ↓↓↓ *********************
'    Application.ScreenUpdating = False
'
'    If sampleType <> "GRIP" Then
'        '* ミッションから情報を取得
'        Call GetMissionInfoAll
'    Else
'        Call GetGripMission
'    End If
'
'    '* ピボットテーブル更新
'    Call RefreshPivotTableData
'
'    '* 更新状態表示更新
'    Call UpdateFormStatus
'
'    Application.ScreenUpdating = True

    ' 2016.07.22 Delete ↑↑↑ *********************

'    If Application.Visible = True Then
'        'EXCEL前面表示用
'        Call ExcelUpperDisp
'        VBA.AppActivate Excel.Application.Caption
'
'        Call ExcelDispSetWin32
'        Call ExcelDispFreeWin32
'    End If
    
    ' プッシュアラーム機能の呼び出し
    Call UpdateAfterGraphEdit("True")
'    Call checkPushAlarm
    
End Sub

'Graph_Alarm sunyi 2018/09/07 End


''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Public Sub createGraph(OnServer As String)

    Dim graphSheetFlag As Boolean
    Dim graphSheet As Worksheet
    
    Dim graphChart As Chart
    Dim graphChartObject As ChartObject
    
    Dim currRange As Range
    
    Dim writeRange As Range
    
    Dim xRange As Range
    Dim yRange As Range
    Dim maxRange As Range
    Dim minRange As Range
    
    
    Dim graphWidth As Integer
    Dim graphHeight As Integer
    
    Dim i As Integer
    
    'Graph_Alarm sunyi 2018/09/07 Strat
    Dim graphAlarmStr As String
    Dim startTime As String
    Dim IsGraphAlarmRange As Range
    Dim AlarmInfo As FoaCoreCom.GarphAlarmRetriever
    Dim GraphAlarmSheet As Worksheet
    
    Dim graph_i As Integer
    Dim GraphAlarmValue As String
    Dim GraphParamList As Collection
    Set GraphParamList = New Collection
    
    For graph_i = 1 To 10
        Set currRange = Sheets(GraphAlarmSheetName).Cells.Find(What:="アラーム_NO", LookAt:=xlWhole)
        GraphAlarmValue = currRange.Offset(0, graph_i).Value
        If GraphAlarmValue <> "" Then
            If currRange.Offset(8, graph_i).Value = "True" Then
                If currRange.Offset(8, graph_i).Value = "" Then
                    GraphParamList.Add (currRange.Offset(0, graph_i).Value & "_" & currRange.Offset(1, graph_i).Value & "_" & "upCross")
                Else
                    GraphParamList.Add (currRange.Offset(0, graph_i).Value & "_" & currRange.Offset(1, graph_i).Value & "_" & "upCross_period")
                End If
            Else
                If currRange.Offset(8, graph_i).Value = "" Then
                    GraphParamList.Add (currRange.Offset(0, graph_i).Value & "_" & currRange.Offset(1, graph_i).Value & "_" & "downCross")
                Else
                    GraphParamList.Add (currRange.Offset(0, graph_i).Value & "_" & currRange.Offset(1, graph_i).Value & "_" & "downCross_period")
                End If
            End If
        End If
    Next graph_i
    'Graph_Alarm sunyi 2018/09/07 End
    
    'グラフ編集シートのグラフデータ位置を取得する。
    Set currRange = Sheets(GraphEditSheetName).Cells.Find(What:="グラフ用データ", LookAt:=xlWhole)
    If Not currRange Is Nothing Then
        Set writeRange = currRange.Offset(1, 0)
    Else
        Set writeRange = Sheets(GraphEditSheetName).Range("F2").Value
    End If
    Set xRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 0), writeRange.End(xlDown).Offset(0, 0))
    Set yRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(1, 1), writeRange.End(xlDown).Offset(0, 1))
    '* GraphAlarm
'    Set maxRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 2), writeRange.End(xlDown).Offset(0, 2))
'    Set minRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 3), writeRange.End(xlDown).Offset(0, 3))
    
    
    '* GraphAlarm
'    Range(xRange, minRange).Select
'    Selection.Copy
'    writeRange.Offset(0, 5).Select
'    ActiveSheet.Paste
'
'    Set xRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 5), writeRange.End(xlDown).Offset(0, 5))
'    Set yRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 6), writeRange.End(xlDown).Offset(0, 6))
'    Set maxRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 7), writeRange.End(xlDown).Offset(0, 7))
'    Set minRange = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, 8), writeRange.End(xlDown).Offset(0, 8))
'
'    Range(xRange, minRange).Sort Key1:=xRange, Order1:=xlAscending, Header:=xlYes
'* GraphAlarm
    
    ''''''''''''''''''''''''''''
    '  グラフシートの追加
    '''''''''''''''''''''''''''
    graphSheetFlag = False
    For Each graphSheet In Worksheets
        If graphSheet.name = FixGraphSheetName Then
            graphSheetFlag = True
            Exit For
        End If
    Next graphSheet

    If graphSheetFlag = False Then
        'シートの追加
        Set graphSheet = Worksheets.Add()
        graphSheet.name = FixGraphSheetName
    End If
    graphSheet.Select
    
    ''''''''''''''''''''''''''''
    '  グラフの削除
    '''''''''''''''''''''''''''
    graphWidth = 0
    graphHeight = 0
    
    With graphSheet
        For i = .ChartObjects.Count To 1 Step -1
            graphWidth = .ChartObjects(i).Width
            graphHeight = .ChartObjects(i).Height
            .ChartObjects(i).Delete
        Next i
    End With
    
    ''''''''''''''''''''''''''''
    '  グラフの追加
    '''''''''''''''''''''''''''
    '空のグラフをグラフシートに作成する
    Set graphChart = Charts.Add
    With graphChart
        .Location Where:=xlLocationAsObject, name:=FixGraphSheetName
    End With
    Set graphChartObject = graphSheet.ChartObjects(1)
      
    '新しくデータ系列を作成します
    ActiveChart.SeriesCollection.NewSeries
    
    '作成したデータ系列に対して種類・X軸・Y軸を設定(通常）
    With ActiveChart.SeriesCollection(1)
        'グラフの種類を設定 折線
        'GraphAlarm sunyi 2018/08
        .ChartType = xlLine
        'X軸の項目軸を指定
        .XValues = xRange
        'データの指定
        .Values = yRange
        .name = missionInfo.CtmName & "/" & missionInfo.ElementName
    End With
    
    
      '* GraphAlarm
    For graph_i = 1 To GraphParamList.Count
            
        ActiveChart.SeriesCollection.NewSeries
        
        '作成したデータ系列に対して種類・X軸・Y軸を設定（最大）
        With ActiveChart.SeriesCollection(graph_i + 1)
            'グラフの種類を設定 線グラフ
            .ChartType = xlLine
            'X軸の項目軸を指定
            .XValues = xRange
            'データの指定
            .Values = Sheets(GraphEditSheetName).Range(writeRange.Offset(0, graph_i + 1), writeRange.End(xlDown).Offset(0, graph_i + 1))
            .name = GraphParamList.Item(graph_i)
        End With
    Next graph_i
        
        
'
'    ActiveChart.SeriesCollection.NewSeries
'
'    '作成したデータ系列に対して種類・X軸・Y軸を設定（最大）
'    With ActiveChart.SeriesCollection(2)
'        'グラフの種類を設定 線グラフ
'        .ChartType = xlLine
'        'X軸の項目軸を指定
'        .XValues = xRange
'        'データの指定
'        .Values = maxRange
'        .name = thresholdInfo.MaxName
'    End With
'
'    ActiveChart.SeriesCollection.NewSeries
'
'    '作成したデータ系列に対して種類・X軸・Y軸を設定（最小）
'    With ActiveChart.SeriesCollection(3)
'        'グラフの種類を設定 線グラフ
'        .ChartType = xlLine
'        'X軸の項目軸を指定
'        .XValues = xRange
'        'データの指定
'        .Values = minRange
'        .name = thresholdInfo.MinName
'    End With
         '* GraphAlarm
        
        
    ' グラフ全体の表示の調整
    With ActiveChart
        'グラフの系列を非表示
        .HasLegend = True
        
        ' グラフのX軸(横軸)のタイトルを設定
        .Axes(xlCategory, xlPrimary).HasTitle = False
        
        ' グラフのY軸(縦軸)のタイトルを設定
        .Axes(xlValue, xlPrimary).HasTitle = False
        
    End With

    With graphChartObject
        .Top = Worksheets(FixGraphSheetName).Range("B2").Top
        .Left = Worksheets(FixGraphSheetName).Range("B2").Left
        
        If (graphWidth > 0 And graphHeight > 0) Then
            .Width = graphWidth
            .Height = graphHeight
        Else
            .Width = .Width * 2
            .Height = .Height * 2
        End If
        
    End With

    With graphChartObject.Chart
       .Axes(xlValue).MinimumScale = graphValueInfo.MinValue
       .Axes(xlValue).MaximumScale = graphValueInfo.MaxValue
       .Axes(xlValue).MajorUnit = graphValueInfo.StepValue
       .Axes(xlCategory).TickLabelPosition = xlTickLabelPositionLow
    End With
        
    '* Server化更新の場合、アラームチェックを行う
    Set GraphAlarmSheet = Sheets(GraphAlarmSheetName)
    Set IsGraphAlarmRange = GraphAlarmSheet.Cells.Find(What:="更新開始時刻", LookAt:=xlWhole)
    startTime = IsGraphAlarmRange.Offset(0, 1)
    
    If startTime <> "" Then
        Set AlarmInfo = CreateObject("FoaCoreCom.ais.retriever.GarphAlarmRetriever")
        graphAlarmStr = AlarmInfo.UpdateGraphAlarmInfo(ThisWorkbook, startTime, EncodeFile, OnServer)
        startTime = ""
    End If

End Sub

Public Function ExportChart() As String
    Dim objChrt As ChartObject
    Dim myChart As Chart
    Dim myFileName As String
    Dim FileName As String
    Dim timeStr As String
    Dim GraphAlarmRange As Range
    Dim BeforeMyFileName As String

    Set objChrt = Sheets(FixGraphSheetName).ChartObjects(1)
    Set myChart = objChrt.Chart
    
    Set GraphAlarmRange = Sheets(GraphAlarmSheetName).Cells.Find(What:="BeforeMyFileName", LookAt:=xlWhole)
    If Not GraphAlarmRange Is Nothing Then
        BeforeMyFileName = GraphAlarmRange.Offset(0, 1)
    End If
    
    On Error Resume Next
    Kill ThisWorkbook.Path & "\" & BeforeMyFileName
    On Error GoTo 0
    
    timeStr = GetUnixTime(DateTime.Now)
    myFileName = "myChart_" & timeStr & ".png"

    myChart.Export FileName:=ThisWorkbook.Path & "\" & myFileName, Filtername:="PNG"
    
    GraphAlarmRange.Offset(0, 1).Value = myFileName
    
    ExportChart = ThisWorkbook.Path & "\" & myFileName

End Function

Public Function EncodeFile() As String

    Dim strPicPath
    'Credit to Wikipedia for orange picture
    Const adTypeBinary = 1          ' Binary file is encoded
    
    ' Variables for encoding
    Dim objXML
    Dim objDocElem
    
    ' Variable for reading binary picture
    Dim objStream
    
On Error GoTo ERROR_STEP
    strPicPath = ExportChart
    
    'PNG作成完了待つ
    Application.Wait Now + TimeValue("00:00:03")
    ' Open data stream from picture
    Set objStream = CreateObject("ADODB.Stream")
    objStream.Type = adTypeBinary
    objStream.Open
    objStream.LoadFromFile (strPicPath)

    ' Create XML Document object and root node
    ' that will contain the data
    Set objXML = CreateObject("MSXml2.DOMDocument")
    Set objDocElem = objXML.createElement("Base64Data")
    objDocElem.DataType = "bin.base64"
    
    ' Set binary value
    objDocElem.nodeTypedValue = objStream.Read()
    
    EncodeFile = objDocElem.text
    
    ' Clean all
    Set objXML = Nothing
    Set objDocElem = Nothing
    Set objStream = Nothing
    
    Exit Function
    
ERROR_STEP:     'エラーSTEP追加

    EncodeFile = "ERROR"
    ' Clean all
    Set objXML = Nothing
    Set objDocElem = Nothing
    Set objStream = Nothing

End Function





