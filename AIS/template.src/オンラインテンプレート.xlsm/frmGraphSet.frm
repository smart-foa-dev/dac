VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmGraphSet 
   Caption         =   "グラフ編集画面"
   ClientHeight    =   7140
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   6585
   OleObjectBlob   =   "frmGraphSet.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "frmGraphSet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Option Explicit


'''''''''''''''''''''''''''''''''''''''''''''
' Formの初期化処理（全てを未設定にする。）
'''''''''''''''''''''''''''''''''''''''''''''
Private Sub initForm()

    Dim srchRange           As Range
    
    Dim CTMRange           As Range
    Dim ctmCount             As Integer
    Dim CtmName             As String
    
    
    '''''''''''''''''''''''''''''''''''''''
    '最大・最小・表示刻み幅のクリア
    '''''''''''''''''''''''''''''''''''''''
    txtVmax.text = ""
    txtVmin.text = ""
    txtVstep.text = ""
    
    
    '''''''''''''''''''''''''''''''''''''''
    'データ件数のクリア
    ''''''''''''''''''''''''''''''''''''''
    txtDataCount = ""
    
    '''''''''''''''''''''''''''''''''''''''
    '最大・最小のクリア
    '''''''''''''''''''''''''''''''''''''''
    '* GraphAlarm
'    txtVname1.text = ""
'    txtVval1.text = ""
'    txtVname2.text = ""
'    txtVval2.text = ""
    
    
    '''''''''''''''''''''''''''''''''''''''
    'ミッション名の読み込み
    '''''''''''''''''''''''''''''''''''''''
    ' 自動更新
    Set srchRange = Sheets(MainSheetName).Cells.Find(What:="ミッション", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        lblMissionName.Caption = srchRange.Offset(0, 1).Value
    End If

    '''''''''''''''''''''''''''''''''''''''
    'CTMリストの読み込み
    '''''''''''''''''''''''''''''''''''''''
    ctmCount = 0
    cbCtm.Clear
    Set CTMRange = Sheets(MainSheetName).Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
    If Not CTMRange Is Nothing Then
        
        Set CTMRange = CTMRange.Offset(1, 0)
    
        ctmCount = 0
        Do While Not IsNull(CTMRange.Value) And Not IsEmpty(CTMRange.Value)
        
            'CTM名の取得
            ctmCount = ctmCount + 1
            CtmName = CTMRange.Value
            
            cbCtm.AddItem CtmName
        
            '次のCTMへ移動する。
            Set CTMRange = CTMRange.Offset(2, 0)
        Loop
    
    
    Else
        MsgBox "CTM,エレメント情報の取得に失敗しました"
        Unload frmGraphSet
        Exit Sub
    End If
    

End Sub


'''''''''''''''''''''''''''''''''''''''''''''
' Formの読込
'''''''''''''''''''''''''''''''''''''''''''''
Private Sub setForm()

    Dim graphValueInfo     As GRAPH_VALUE_INFO
    Dim thresholdInfo      As THRESHOLD_INFO
    Dim missionInfo        As MISSION_INFO
    Dim pushAlarmInfo      As PUSH_ALARM_INFO

    ''''''''''''''''''''''''''''''''
    ' グラフ編集シートから情報取得設定する。
    '       表示Max、表示Min,表示刻み幅、上限・下限、ミッション名、CTM名、エレメント名
    ''''''''''''''''''''''''''''''''
    '縦軸の最大／最小／刻み幅を取得する
    Call GetGraphValueInfo(GraphEditSheetName, graphValueInfo)
    If IsNull(graphValueInfo.MaxValue) Or IsEmpty(graphValueInfo.MaxValue) Then
    Else
        txtVmax.text = graphValueInfo.MaxValue
        txtVmin.text = graphValueInfo.MinValue
        txtVstep.text = graphValueInfo.StepValue
        txtDataCount.text = graphValueInfo.DataCount
    End If
    
    '* GraphAlarm
'    '上限・下限を取得を取得する
'    Call GetThresholdInfo(GraphEditSheetName, thresholdInfo)
'    If IsNull(thresholdInfo.MaxValue) Or IsEmpty(thresholdInfo.MaxValue) Then
'        txtVname1.text = ""
'        txtVname2.text = ""
'
'        txtVval1.text = ""
'        txtVval2.text = ""
'    Else
'        txtVname1.text = thresholdInfo.MaxName
'        txtVname2.text = thresholdInfo.MinName
'
'        txtVval1.text = thresholdInfo.MaxValue
'        txtVval2.text = thresholdInfo.MinValue
'    End If
    
    'ミッション情報を取得する
    Call GetMissionInfo2(GraphEditSheetName, missionInfo)
    If missionInfo.CtmName <> "" Then
        cbCtm.Value = missionInfo.CtmName
        cbElement.Value = missionInfo.ElementName
    End If
    
    
    ''''''''''''''''''''''''''''''''
    ' プッシュアラームシートから情報取得設定する。
    '           発生回数、メール、メッセージ、メッセージ名、メッセージコード、判定済み最終データ時刻
    ''''''''''''''''''''''''''''''''
    Call GetPushAlarmInfo(PushAlarmSheetName, pushAlarmInfo)
    '* GraphAlarm
'    If pushAlarmInfo.MailAddress <> "" Then
'        lblPushAlarmState = "設定済み"
'    Else
'        lblPushAlarmState = "未設定"
'    End If
    
    
    
End Sub



'********************************
'　グラフ編集画面
'　　　　キャンセルボタンクリック時
'********************************
Private Sub btnCancel_Click()
    Unload frmGraphSet
End Sub

'********************************
'　グラフ編集画面
'　　　　クリアボタンクリック時
'********************************
Private Sub btnClear_Click()
    initForm
End Sub


Private Sub btnPushAlarm_Click()
    frmPushAlarm.Show
End Sub

'********************************
'　グラフ編集画面
'　　　　更新ボタンクリック時
'********************************
Private Sub btnSet_Click()
    Dim currWorksheet       As Worksheet
    Dim srchRange           As Range
    
    
    ' 自動更新
    Set srchRange = Sheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "ON" Then
            MsgBox "グラフ編集を実施するには、自動更新をOFFにしてください。"
            Unload frmGraphSet
            Exit Sub
        End If
    End If
    
    
    '''''''''''''''''''''''''''''''
    '入力内容のチェック
    '''''''''''''''''''''''''''''''
    ' CTM/エレメントの必須チェック
    If cbCtm.Value = "" Or cbElement.Value = "" Then
        MsgBox "CTMとエレメントは両方を選択してください。"
        Exit Sub
    End If

    
    '.数値チェック（最小・最大・ステップ）
    If Not IsNumeric(txtVmax.text) Or Not IsNumeric(txtVmin.text) Or Not IsNumeric(txtVstep.text) Then
        MsgBox "表示Max・表示Min・表示Step・データ件数には整数を設定してください。"
        Exit Sub
    End If
    
    '* GraphAlarm
'    If Not IsNumeric(txtVval1.text) Or Not IsNumeric(txtVval2.text) Then
'        MsgBox "縦軸基準線には上限・下限には数値を設定してください。"
'        Exit Sub
'    End If
    
    '.整数チェック（最小・最大・ステップ）
    If Int(txtVmax.text) <> CDbl(txtVmax.text) Or Int(txtVmin.text) <> CDbl(txtVmin.text) Or Int(txtVstep.text) <> CDbl(txtVstep.text) Or Int(txtDataCount.text) <> CDbl(txtDataCount.text) Then
        MsgBox "表示Max・表示Min・表示Step・データ件数には整数を設定してください。"
        Exit Sub
    End If
   
   '* GraphAlarm
'   If txtVname1.text = "" Or txtVname2.text = "" Then
'        MsgBox "縦軸基準線の上限・下限の名称を設定してください。"
'        Exit Sub
'   End If
'
   
   
   
   
   
    '.数値の範囲チェック
    'If txtVmax.text <= 0 Then
    '    MsgBox "表示Maxには0より大きい数値を設定してください。"
    '    Exit Sub
    'End If
    
    '
    'If txtVmin.text < 0 Then
    '    MsgBox "表示Minには0以上の数値を設定してください。"
    '    Exit Sub
    'End If
    
    If txtVstep.text <= 0 Then
        MsgBox "表示stepには0より大きい数値を設定してください。"
        Exit Sub
    End If
    
    If txtDataCount.text <= 0 Then
        MsgBox "データ件数には0より大きい数値を設定してください。"
        Exit Sub
    End If
    
    'If txtVval1.text <= 0 Then
    '    MsgBox "縦軸基準線の上限には0より大きい数値を設定してください。"
    '    Exit Sub
    'End If
    
    'If txtVval2.text < 0 Then
    '    MsgBox "縦軸基準線の下限には0以上の数値を設定してください。"
    '    Exit Sub
    'End If
    
    '4.数値の関係
    If CInt(txtVmax.text) <= CInt(txtVmin.text) Then
        MsgBox "表示Maxには表示Minより大きい数値を設定してください。"
        Exit Sub
    End If
    '* GraphAlarm
'    If CDbl(txtVval1.text) <= CDbl(txtVval2.text) Then
'        MsgBox "縦軸基準線の上限には縦軸基準線の下限より大きい数値を設定してください。"
'        Exit Sub
'    End If
    
    '4 Ctm/Elementのチェック
    If Not ((cbCtm.Value <> "") And (cbElement.Value <> "")) Then
        MsgBox "CTM名とエレメント名は共に設定してください。"
        Exit Sub
    End If
    
    '''''''''''''''''''''''''''''''
    'シートへの転記
    '''''''''''''''''''''''''''''''
    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="−ミッション−", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 1).Value = lblMissionName.Caption
        srchRange.Offset(2, 1).Value = cbCtm.Value
        srchRange.Offset(3, 1).Value = cbElement.Value
    End If
    Set srchRange = currWorksheet.Cells.Find(What:="−縦軸−", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 1).Value = txtVmax.text
        srchRange.Offset(2, 1).Value = txtVmin.text
        srchRange.Offset(3, 1).Value = txtVstep.text
    End If
    
    Set srchRange = currWorksheet.Cells.Find(What:="−データ件数−", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 1).Value = txtDataCount.text
    End If
    
    '* GraphAlarm
'    Set srchRange = currWorksheet.Cells.Find(What:="−縦軸基準線−", LookAt:=xlWhole)
'    If Not srchRange Is Nothing Then
'        srchRange.Offset(1, 0).Value = txtVname1.text
'        srchRange.Offset(2, 0).Value = txtVname2.text
'
'        srchRange.Offset(1, 1).Value = txtVval1.text
'        srchRange.Offset(2, 1).Value = txtVval2.text
'    End If


    '''''''''''''''''''''''''''''''
    ' 判定済み最終データ時刻と連続NG回数を空と０に初期化する
    '''''''''''''''''''''''''''''''
    Set currWorksheet = Worksheets(PushAlarmSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="判定済み最終データ時刻", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = ""
        srchRange.Offset(1, 1).Value = "0"
    End If
            
    


    'グラフ更新処理
    Call GraphSet_Module.UpdateAfterGraphEdit
End Sub


'********************************
'　グラフ編集画面　初期化処理
'　　　　アドイン − グラフ編集ボタンクリック時
'********************************
Private Sub UserForm_Activate()
    
    Dim srchRange           As Range
    
    
    '''''''''''''''''''''''''''''''''''''''
    ' 起動チェック
    '''''''''''''''''''''''''''''''''''''''
    ' 自動更新
    Set srchRange = Sheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "ON" Then
            MsgBox "グラフ編集を実施するには、自動更新をOFFにしてください。"
            Unload frmPushAlarm
            Unload frmGraphSet
            Exit Sub
        End If
    End If
    
    '''''''''''''''''''''''''''''''''''''''
    ' 初期化処理
    ''''''''''''''''''''''''''''''''''''''
    Call initForm
    

    '''''''''''''''''''''''''''''''''''''''
    ' 設定シートからの読み込み＋フォームへの設定
    ''''''''''''''''''''''''''''''''''''''
    Call setForm
    
    
    
End Sub

Private Sub cbCtm_Change()

    Dim CTMRange           As Range
    
    Dim loopCount           As Integer
    
    Dim elementRange           As Range
    Dim elementCount        As Integer
    Dim ElementName         As String


    ' リストの初期化
    cbElement.Clear
    If cbCtm.Value = "" Then
        Exit Sub
    End If
    

    ' 選択されたCTMの位置を取得
    Set CTMRange = Sheets(MainSheetName).Cells.Find(What:=cbCtm.Value, LookAt:=xlWhole)


    If Not CTMRange Is Nothing Then
    
    
        'エレメントの数の確認
        'エレメントセルの間が飛ぶため、右端に移動して、左方向へ検索する。）
        Set elementRange = CTMRange.Offset(0, 2000)
        elementCount = elementRange.End(xlToLeft).Column - 1
        
        Set elementRange = CTMRange.Offset(0, 1)
        For loopCount = 0 To elementCount - 1
            ElementName = elementRange.Offset(0, loopCount).Value
            If ElementName <> "" Then
                cbElement.AddItem ElementName
            End If
        Next
    End If
    
End Sub
