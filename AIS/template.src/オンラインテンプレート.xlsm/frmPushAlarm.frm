VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmPushAlarm 
   Caption         =   "プッシュアラーム"
   ClientHeight    =   5685
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   7155
   OleObjectBlob   =   "frmPushAlarm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "frmPushAlarm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False





'''''''''''''''''''''''''''''''''''''''''''''
' Formの初期化処理（全てを未設定にする。）
'''''''''''''''''''''''''''''''''''''''''''''
Private Sub initForm()

    Dim userList As Collection
    Dim user As Variant
    

    ''''''''''''''''''''''''''''''''
    ' プッシュアラーム画面の初期化
    '           発生回数、メールアドレス、メッセージ送信、メッセージ名、メッセージコード
    ''''''''''''''''''''''''''''''''
    
    ' 発生回数
    cbOccurCount.Clear
    For i = 1 To 10
        cbOccurCount.AddItem i
    Next
    
    'メールアドレス
    listMailAddress.Clear
    getUserList userList
    
    For Each user In userList
        listMailAddress.AddItem user
    Next
    

    ' メッセージ名、メッセージコードのクリア
    txtMessageCode.text = ""
    txtMessageName.text = ""
    

    

End Sub


'''''''''''''''''''''''''''''''''''''''''''''
' Formの読込
'''''''''''''''''''''''''''''''''''''''''''''
Private Sub setForm()

    Dim pushAlarmInfo As PUSH_ALARM_INFO
    
    Dim mailArray() As String
    
    ''''''''''''''''''''''''''''''''
    ' プッシュアラームシートから情報取得設定する。
    '           発生回数、メール、メッセージ、メッセージ名、メッセージコード、判定済み最終データ時刻
    ''''''''''''''''''''''''''''''''
    Call GetPushAlarmInfo(PushAlarmSheetName, pushAlarmInfo)
        
    ' 発生回数
    If pushAlarmInfo.OccurCount > 0 Then
        cbOccurCount.Value = pushAlarmInfo.OccurCount
    End If
    
    'メールアドレス
    If pushAlarmInfo.MailAddress <> "" Then
        mailArray = Split(pushAlarmInfo.MailAddress, ",")
        
        With listMailAddress
            For i = 0 To .ListCount - 1
                If UBound(Filter(mailArray, .List(i))) <> -1 Then
                    .Selected(i) = True
                End If
            Next i
        End With
    End If
    
    
    'メッセージコード、メッセージ名
    txtMessageName.text = pushAlarmInfo.MessageName
    txtMessageCode.text = pushAlarmInfo.MessageCode
    
    
End Sub

'********************************
'　プッシュアラーム画面
'　　　　キャンセルボタンクリック時
'********************************
Private Sub btnCancel_Click()
    Unload frmPushAlarm
End Sub

Private Sub btnCancle_Click()
    Unload frmPushAlarm
End Sub

'********************************
'　プッシュアラーム画面
'　　　　クリアボタンクリック時
'********************************
Private Sub btnClear_Click()
    initForm
End Sub


'********************************
'　プッシュアラーム画面面
'　　　　更新ボタンクリック時
'********************************
Private Sub btnSet_Click()
    
    Dim MailAddress As String
    Dim i As Integer
    
    Dim currWorksheet As Worksheet
    
    '''''''''''''''''''''''''''''''''''''
    ' メールアドレスの整形
    '''''''''''''''''''''''''''''''''''''
    MailAddress = ""
    With listMailAddress
        For i = 0 To .ListCount - 1
            If .Selected(i) = True Then
                MailAddress = MailAddress & .List(i) & ","
            End If
        Next i
    End With
    
    If MailAddress <> "" Then
        MailAddress = Left(MailAddress, Len(MailAddress) - 1)
    End If
    
    
    If cbOccurCount.Value = "" And MailAddress = "" And txtMessageName.text = "" And txtMessageCode = "" Then
    
        '''''''''''''''''''''''''''''''
        'シートへの転記
        '''''''''''''''''''''''''''''''
        Set currWorksheet = Worksheets(PushAlarmSheetName)
        Set srchRange = currWorksheet.Cells.Find(What:="−プッシュアラーム−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 1).Value = ""
            srchRange.Offset(2, 1).Value = ""
            srchRange.Offset(3, 1).Value = ""
            srchRange.Offset(4, 1).Value = ""
        End If
        

        
    Else
    
        '''''''''''''''''''''''''''''''''''''''''''
        ' 入力チェック
        '''''''''''''''''''''''''''''''''''''''''''
        '1 発生回数
        If cbOccurCount.Value = "" Then
            MsgBox "発生回数を選択してください。"
            Exit Sub
        End If
        
        '.数値チェック
        If Not IsNumeric(cbOccurCount.Value) Then
            MsgBox "発生回数には整数を設定してください。"
            Exit Sub
        End If

        '.整数チェック
        If Int(cbOccurCount.Value) <> CDbl(cbOccurCount.Value) Then
            MsgBox "発生回数には整数を設定してください。"
            Exit Sub
        End If

        '範囲チェック
        If cbOccurCount.Value <= 0 Then
            MsgBox "発生回数には0より大きい数値を設定してください。"
            Exit Sub
        End If
        
        '2 メールアドレスとメッセージ
        If MailAddress = "" Then
            MsgBox "メールアドレスを選択してください。"
            Exit Sub
        End If
        
        '3 メッセージ名の入力チェック
        If txtMessageName.text = "" Then
            MsgBox "メッセージ名を入力してください。"
            Exit Sub
        End If
        
        '4 メッセージコードの入力チェック
        If txtMessageCode.text = "" Then
            MsgBox "メッセージコードを入力してください。"
            Exit Sub
        End If
        
        '''''''''''''''''''''''''''''''
        'シートへの転記
        '''''''''''''''''''''''''''''''
        Set currWorksheet = Worksheets(PushAlarmSheetName)
        Set srchRange = currWorksheet.Cells.Find(What:="−プッシュアラーム−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 1).Value = cbOccurCount.Value
            srchRange.Offset(2, 1).Value = MailAddress
            srchRange.Offset(3, 1).Value = txtMessageName
            srchRange.Offset(4, 1).Value = txtMessageCode
        End If
    End If
    
    '''''''''''''''''''''''''''''''
    ' 判定済み最終データ時刻と連続NG回数を空と０に初期化する
    '''''''''''''''''''''''''''''''
    Set currWorksheet = Worksheets(PushAlarmSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="判定済み最終データ時刻", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = ""
        srchRange.Offset(1, 1).Value = "0"
    End If
            

End Sub




'********************************
'　プッシュアラーム画面　初期化処理
'　　　　アドイン − グラフ編集ボタンクリック時
'********************************
Private Sub UserForm_Activate()
    
    Dim srchRange           As Range
    
    
    '''''''''''''''''''''''''''''''''''''''
    ' 起動チェック
    '''''''''''''''''''''''''''''''''''''''
    ' 自動更新
    Set srchRange = Sheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "ON" Then
            MsgBox "グラフ編集を実施するには、自動更新をOFFにしてください。"
            Unload frmGraphSet
            Unload frmPushAlarm
            Exit Sub
        End If
    End If
    
    '''''''''''''''''''''''''''''''''''''''
    ' 初期化処理
    ''''''''''''''''''''''''''''''''''''''
    Call initForm
    

    '''''''''''''''''''''''''''''''''''''''
    ' 設定シートからの読み込み＋フォームへの設定
    ''''''''''''''''''''''''''''''''''''''
    Call setForm
    
    
    
End Sub




