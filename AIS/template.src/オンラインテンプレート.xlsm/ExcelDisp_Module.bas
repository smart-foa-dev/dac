Attribute VB_Name = "ExcelDisp_Module"
' Option Explicit
' Public Declare PtrSafe Function SetWindowPos Lib "user32" (ByVal Hwnd As Long, ByVal hWndInsertAfter As Long, _
'        ByVal X As Long, ByVal Y As Long, _
'         ByVal cx As Long, ByVal cy As Long, _
'        ByVal wFlags As Long) As Long
' Public Const walways = -1 '常に手前にセット
' Public Const wreset = -2  '解除
' Public Const wdisp = &H40 '表示する
' Public Const w_SIZE = &H1 'サイズを設定しない
' Public Const w_MOVE = &H2 '位置を設定しない
'
' '==========================================================================
' Public Sub ExcelDispSetWin32()
'    Dim Hwnd As Long
'    Hwnd = Application.Hwnd
'    Call SetWindowPos(Hwnd, walways, 0, 0, 0, 0, wdisp Or w_SIZE Or w_MOVE)
' End Sub
'
' '===========================================================================
' Public Sub ExcelDispFreeWin32()
'    Dim Hwnd As Long
'    Hwnd = Application.Hwnd
'    Call SetWindowPos(Hwnd, wreset, 0, 0, 0, 0, wdisp Or w_SIZE Or w_MOVE)
' End Sub
'
'
'
''*******************************
''* ファイル名を変数へセットする
''*******************************
'Public Sub ExcelUpperDisp()
'    Dim srchRange       As Range
'    Dim workRange       As Range
'    Dim SheetName       As String
'
'    Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Range("A:A")
'    Set workRange = srchRange.Find(What:="マクロファイル名", LookAt:=xlWhole)
'    If Not workRange Is Nothing Then
'        thisBookName = workRange.Offset(0, 1).Value
'    End If
'
'    '**********************
'    AppActivate thisBookName & " - Excel"
'
'    SheetName = ThisWorkbook.ActiveSheet.Name
'
'    ThisWorkbook.Sheets(SheetName).Activate
'    '**********************
'
'End Sub
'
