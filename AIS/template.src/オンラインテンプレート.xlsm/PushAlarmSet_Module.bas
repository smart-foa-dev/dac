Attribute VB_Name = "PushAlarmSet_Module"
Option Explicit

'* プッシュアラーム
Public Type PUSH_ALARM_INFO
    OccurCount As Integer
    MessageName As String
    MessageCode As String
    MailAddress As String
    lastCheckTime As String
    continuousNgTimes As Integer
End Type

Const MAIL_BODY = _
        "関係者各位" & vbCrLf _
    & vbCrLf _
    & "下記のアラームが発生したので､ご連絡します｡" & vbCrLf & vbCrLf _
    & "メッセージ名：" & "<<MESSAGE_NAME>>" & vbCrLf _
    & "メッセージコード：" & "<<MESSAGE_CODE>>" & ".ais" & vbCrLf & vbCrLf _
    & "アラーム発生条件" & vbCrLf _
    & "<<MISSION_NAME>>" & "：" & "<<CTM_NAME>>" & "：" & "<<ELEMENT_NAME>>" & "の計測値が" & vbCrLf _
    & "<<MIN_NAME>>(<<MIN_VALUE>>)" & "から" & "<<MAX_NAME>>(<<MAX_VALUE>>)" & "の範囲を" & "<<OCCUR_COUNT>>" & "回連続で外れました。"

Dim pushAlarmInfo      As PUSH_ALARM_INFO
Dim thresholdInfo As THRESHOLD_INFO
Dim missionInfo        As MISSION_INFO




'***************************************************
'* アラーム判定処理
'***************************************************
Public Sub checkPushAlarm()
    
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range
    
    
    Dim mailTitle As String
    Dim mailBody As String
    Dim toList() As String
    
    Dim continuousNgTimes As Integer
    Dim lastCheckTime As String
    
    '１．グラフ編集シート・プッシュアラームシートから、ミッション情報・閾値情報・プッシュアラーム情報を読込み
    Call GetPushAlarmInfo(PushAlarmSheetName, pushAlarmInfo)
    '* GraphAlarm
'    Call GetThresholdInfo(GraphEditSheetName, thresholdInfo)
'* GraphAlarm
    Call GetMissionInfo2(GraphEditSheetName, missionInfo)
    
    ' 2.グラフの再作成
    Call UpdateAfterGraphEdit
    
    '3.アラーム判定処理
    Call checkAlarm
    
    '4.アラーム送信処理
    If pushAlarmInfo.continuousNgTimes >= pushAlarmInfo.OccurCount Then
    
        'メールタイトル
        mailTitle = pushAlarmInfo.MessageName & ".ais"
    
        'メール本文の作成
        mailBody = MAIL_BODY
        mailBody = Replace(mailBody, "<<MESSAGE_NAME>>", pushAlarmInfo.MessageName)
        mailBody = Replace(mailBody, "<<MESSAGE_CODE>>", pushAlarmInfo.MessageCode)
        mailBody = Replace(mailBody, "<<MISSION_NAME>>", missionInfo.MissionName)
        mailBody = Replace(mailBody, "<<CTM_NAME>>", missionInfo.CtmName)
        mailBody = Replace(mailBody, "<<ELEMENT_NAME>>", missionInfo.ElementName)
        mailBody = Replace(mailBody, "<<MIN_NAME>>", thresholdInfo.MinName)
        mailBody = Replace(mailBody, "<<MIN_VALUE>>", thresholdInfo.MinValue)
        mailBody = Replace(mailBody, "<<MAX_NAME>>", thresholdInfo.MaxName)
        mailBody = Replace(mailBody, "<<MAX_VALUE>>", thresholdInfo.MaxValue)
        mailBody = Replace(mailBody, "<<OCCUR_COUNT>>", pushAlarmInfo.OccurCount)
    
        '宛先を配列に変換
        toList = Split(pushAlarmInfo.MailAddress, ",")
        
        Call sendMail(mailTitle, mailBody, toList)
        
        '判定済み最終データ時刻を更新する
        lastCheckTime = pushAlarmInfo.lastCheckTime
        '連続NG回数を０に初期化する
        continuousNgTimes = 0
        
        
    Else
        '判定済み最終データ時刻を更新する
        lastCheckTime = pushAlarmInfo.lastCheckTime
        
        '連続NG回数を現在地で更新する
        continuousNgTimes = pushAlarmInfo.continuousNgTimes
    
    End If
    
    ' 判定済み最終データ時刻と、連続NG回数をExcleに保存する。
    Set currWorksheet = Worksheets(PushAlarmSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="判定済み最終データ時刻", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = lastCheckTime
        srchRange.Offset(1, 1).Value = continuousNgTimes
    End If
    
    
End Sub

Public Sub checkAlarm()
    
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range
    Dim tmp As String
    
    Dim currRange As Range
    Dim currRowCount As Integer
    
    Dim latestRecvTime As String
    
    Dim checkValue As Double

    '* 日付位置の取得
    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="グラフ用データ", LookAt:=xlWhole)
    If srchRange Is Nothing Then
        GoTo ErrorMissionHandler
    End If
    
    ' チェック開始位置の設定
    currRowCount = 0
    Set currRange = srchRange.Offset(2, 0)
    latestRecvTime = currRange.Value
    checkValue = 0
    
    ' 値がある間はチェックを行う。
    Do While (Not IsNull(currRange.Value) And Not IsEmpty(currRange.Value))
     
        ' 最終チェック日以前のデータが現れたら処理を終了する
        If pushAlarmInfo.lastCheckTime >= currRange.Value Then
            Exit Do
        End If
        
        ' チェックの実施
        checkValue = currRange.Offset(0, 1).Value
        If checkValue >= thresholdInfo.MaxValue Or checkValue <= thresholdInfo.MinValue Then
            pushAlarmInfo.continuousNgTimes = pushAlarmInfo.continuousNgTimes + 1
            Exit Do
        End If
        
        'チェック対象のデータを１つ下にずらす
        currRowCount = currRowCount + 1
        Set currRange = currRange.Offset(currRowCount, 0)
    Loop
    
    ' チェック時刻を更新する。
    pushAlarmInfo.lastCheckTime = latestRecvTime
    
ErrorMissionHandler:
    
    
End Sub

'***************************************************
'* プッシュアラームシートからプッシュアラーム情報を取得する
'***************************************************
Public Sub GetPushAlarmInfo(currSheetName As String, pushAlarmInfo As PUSH_ALARM_INFO)
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range
    Dim tmp As String
    

    '* プッシュアラームシートから、連続発生回数、メールアドレス、メッセージコード、メッセージ名、判定済み昨秋データ時刻、連続NG回数
    Set currWorksheet = Worksheets(currSheetName)
    With currWorksheet
        Set srchRange = .Cells.Find(What:="−プッシュアラーム−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            pushAlarmInfo.OccurCount = srchRange.Offset(1, 1).Value
            pushAlarmInfo.MailAddress = srchRange.Offset(2, 1).Value
            pushAlarmInfo.MessageName = srchRange.Offset(3, 1).Value
            pushAlarmInfo.MessageCode = srchRange.Offset(4, 1).Value
        End If
        
        Set srchRange = .Cells.Find(What:="判定済み最終データ時刻", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            pushAlarmInfo.lastCheckTime = srchRange.Offset(0, 1).Value
            If pushAlarmInfo.lastCheckTime = "" Then
                pushAlarmInfo.lastCheckTime = Now
            End If
            
            tmp = srchRange.Offset(1, 1).Value
            If IsNull(tmp) Or IsEmpty(tmp) Then
                pushAlarmInfo.continuousNgTimes = 0
            ElseIf Not IsNumeric(tmp) Then
                pushAlarmInfo.continuousNgTimes = 0
            Else
                pushAlarmInfo.continuousNgTimes = CInt(tmp)
            End If
        End If
    End With
        

End Sub

'***************************************************
'* プッシュアラームのメール宛先のリストを取得するREST要求を送る。
'***************************************************
Public Sub getUserList(userList As Collection)

    Dim srchRange           As Range

    Dim IPAddr      As String
    Dim PortNo      As String
    Dim SendUrlStr As String

    Dim useProxy As Boolean
    Dim proxyUri As String
    
    Dim httpObj     As Object
    Dim GetString   As String
    
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim objOneJSON As Object
    
    Dim name As String
    Dim mail As String



    ''''''''''''''''''''''''''''''''''''''''
    ' 接続情報の取得
    ''''''''''''''''''''''''''''''''''''''''
    With Sheets(ParamSheetName)
        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
    End With
    
    SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/user"
    
    If useProxy = False Then
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
    Else
        Set httpObj = CreateObject("MSXML2.XMLHTTP")
        httpObj.setProxy 2, proxyUri
    End If
    
    httpObj.Open "GET", SendUrlStr, False
    httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
    httpObj.send
        
    ' ダウンロード待ち
    Do While httpObj.readyState <> 4
        DoEvents
    Loop
    
    On Error GoTo ErrorMissionHandler
    
    GetString = httpObj.responseText
    
    '* 取得情報のデコード
    Set sc = CreateObject("ScriptControl")
    With sc
        .Language = "JScript"
    
        '指定したインデックス、名称のデータを取得する
        .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
    End With
    Set objJSON = sc.CodeObject.jsonParse(GetString)
    
    Set sc = Nothing
    
    Set userList = New Collection
    For Each objOneJSON In objJSON.Users
        name = CallByName(objOneJSON, "UserName", VbGet)
        mail = CallByName(objOneJSON, "MailAddress", VbGet)
        userList.Add Item:=name & "<" & mail & ">"
    Next
    
ErrorMissionHandler:
    
End Sub


