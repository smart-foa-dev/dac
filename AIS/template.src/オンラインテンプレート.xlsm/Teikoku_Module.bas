Attribute VB_Name = "Teikoku_Module"
Option Explicit

'**************************
'* 自動更新タイマー起動開始
'**************************
Public Sub Teikoku_TimerStart()
    Dim wrkStr          As String
    Dim updTime         As Variant      ' 2016.08.03 Change Integer->Variant
    Dim srchRange       As Range
    
    Dim startD          As Date
    Dim startT          As Date
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim startTime       As Date
    Dim endTime         As Double

    Call SetFilenameToVariable

    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '表示期間を過ぎたら、次の日に起動予約する。
        '*********************
        '* 収集開始日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = srchRange.Offset(0, 1).Value
            startT = srchRange.Offset(0, 2).Value
        Else
            startD = CDate("2020/12/31")
            startT = CDate("23:59:59")
        End If
    
        '***********************************************
        '* 表示期間を取得：表示終了時刻〜表示開始時刻
        '***********************************************
        Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value
        Else
            updTime = 3600  '1時間
        End If
        '* 2016.08.03 Change ↓↓↓****************************
'        startTime = GetUnixTime(startD + startT) + updTime
        startTime = DateAdd("s", updTime, startD + startT)
        '* 2016.08.03 Change ↑↑↑****************************
        
        '******************************************************
        '収集期間時刻を超えた場合、翌日起動予約
        prevTime = DateAdd("s", updTime, startD + startT)
        If prevTime < Now Then
            '--2017/05/02 Add No.686
            '表示終了時刻までのデータを取得
            Call OperationTest
            
            prevTime = DateAdd("d", 1, startD + startT)
            Application.OnTime prevTime, "'Teikoku_TimerLogic'"     '翌日起動タイマーセット
            
            '次回更新日時セット
            Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
                srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
            End If
            
            '--2017/05/01 Add No.680
            '* 更新中／停止中ステータス更新
            Call UpdateFormStatus
    
            Exit Sub
        End If
        
        '******************************************************
        '収集期間時刻前の場合、本日起動予約
        prevTime = startD + startT
        If startTime > Now Then
            prevTime = DateAdd("d", 0, Date + startT)
            Application.OnTime prevTime, "'Teikoku_TimerLogic'"     '本日起動タイマーセット
            
            '次回更新日時セット
            Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
                srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
            End If
            Exit Sub
        End If
        
        '******************************************************
        '* 更新周期を取得
        '******************************************************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
            
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'Teikoku_TimerLogic'"     '表示期間中タイマーセット
        
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
        End If
        
    End With
    
    '* ピボットテーブル更新
    Call RefreshPivotTableData
    
    '* 更新中／停止中ステータス更新
    Call UpdateFormStatus
    
End Sub

'***********************
'* 自動更新タイマー処理
'***********************
Public Sub Teikoku_TimerLogic()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Variant      ' 2016.08.03 Change Integer->Variant
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim startD          As Date
    Dim startT          As Date
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range
    Dim FinalTime       As Date
    Dim sampleType      As String
    Dim dispTerm        As Double      ' 2016.08.03 Change Integer->Double
    Dim dispTermUnit    As String
    
    Call SetFilenameToVariable
    
    '*Graph_Alarm sunyi 2018/09/14 start
    Call SetUpdateStartTime
    '*Graph_Alarm sunyi 2018/09/14 end
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    With currWorksheet
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "ON" Then
                Application.CutCopyMode = False
                Exit Sub
            End If
        End If
    
        'オンライン停止チェック
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
        
        ' オンライン停止判定
        FinalTime = endD + endT
        If Format(Now, "yyyy/MM/dd hh:mm:ss") >= FinalTime Then         '終了時刻になったら終わる
            MsgBox "終了時刻になりました。" & " " & Format(FinalTime, "hh:mm:ss")
            Call AutoUpdateStop
            Application.CutCopyMode = False
            Exit Sub
        End If
        
        '本日分を停止し明日を予約する。
        '*********************
        '* 収集開始日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(srchRange.Offset(0, 1).Value)
            startT = TimeValue(Format(srchRange.Offset(0, 2).Value, "hh:mm:ss"))
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        '* 2016/09/13 Add ↓↓↓*********************************************
        startD = Date
        '* 2016/09/13 Add ↑↑↑*********************************************
        '*******************************
        '* 表示期間を取得
        '*******************************
        Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                dispTerm = 1#
                dispTermUnit = "【時】"
            Else
                dispTerm = srchRange.Offset(0, 1).Value
                dispTermUnit = srchRange.Offset(0, 2).Value
            End If
        Else
            dispTerm = 1#
            dispTermUnit = "【時】"
        End If
        
        Select Case dispTermUnit
            Case "【分】"
                dispTerm = dispTerm * 60
            Case "【時】"
                dispTerm = dispTerm * 3600
        End Select
        
        prevDate = DateAdd("s", dispTerm, startD + startT)
        If Now > prevDate Then
            prevDate = DateAdd("d", 1, startD + startT)
            '--2017/05/02 Del No.686
            'Call AutoUpdateStop
            Application.OnTime prevTime, "'Teikoku_TimerLogic'"     '本日起動タイマーセット
            
            '次回更新日時セット
            Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd")
                srchRange.Offset(0, 2).Value = Format(prevDate, "hh:mm:ss")
            End If
            
            Exit Sub
        End If
    
        ' 収集タイプの取得
        sampleType = ""
        Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
        
        'CTM,ミッションの取込
        Application.ScreenUpdating = False
            If sampleType <> "GRIP" Then
                '* ミッションから情報を取得
                'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
                'Call Teikoku_GetMissionInfoAll
                
                ' Wang Foastudio Issue AISTEMP-24 20181207 Start
                'Call Teikoku_GetMissionInfoAll(SEARCH_TYPE_AUTO)
                Call Teikoku_GetMissionInfoAll(GetSearchType())
                ' Wang Foastudio Issue AISTEMP-24 20181207 End
                
                'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
            Else
                Call GetGripMission
            End If

            '* ピボットテーブル更新
            Call RefreshPivotTableData

            '* 更新状態表示更新
            Call UpdateFormStatus
        Application.ScreenUpdating = True

        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
        
        '* 次回の処理をスケジュール
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'Teikoku_TimerLogic'"
        
        '前回更新日時セット
        Set srchRange = .Cells.Find(What:="前回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevDate, "hh:mm:ss")
        End If
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
        End If
    End With

    'EXCEL前面表示用
    If Application.Visible = True Then
        Call ExcelUpperDisp
        VBA.AppActivate Excel.Application.Caption
    
        Call ExcelDispSetWin32
        Call ExcelDispFreeWin32
    End If
    
    ' プッシュアラーム機能の呼び出し
    Call UpdateAfterGraphEdit
    'Call checkPushAlarm
    
End Sub


'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'Public Sub Teikoku_GetMissionInfoAll()
Public Sub Teikoku_GetMissionInfoAll(sType As String, Optional OnServer As String = "False")
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    Dim ctmList()       As CTMINFO      ' CTM情報
    Dim ctmValue()      As CTMINFO      ' CTM取得情報
    Dim II              As Integer
    Dim srchRange       As Range
    Dim lastRange       As Range
    Dim workRange       As Range
    Dim endRange        As Range
    
    Dim currWorksheet   As Worksheet
    Dim selectStr       As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim dispTerm        As Double
    Dim dispTermUnit    As String
    
    Dim actWorksheet    As Worksheet
    Dim rstWorksheet    As Worksheet
    
    Dim CTMRange        As Range
        
    Dim iResult         As Integer
    
    Dim result     As String
    Dim resultSuccess() As String
    
    Dim PathName As String, FileName As String, pos As Long
    Dim endData As Double

    '* CMSからCTM情報を取得する
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
    'result = Teikoku_GetMissionInfo(ctmList, ctmValue)
    result = Teikoku_GetMissionInfo(ctmList, ctmValue, sType, OnServer)
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    
    If result = "ERROR" Then
        Exit Sub
    End If
        
    resultSuccess = Split(result, "@@")
    
    'CTM取得件数設定 20161117 追加
    Set CTMRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="CTM取得件数", LookAt:=xlWhole)
    If Not CTMRange Is Nothing Then
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
        If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS1" Then
'            ' ディレクトリを取得
'            pos = InStrRev(resultSuccess(1), "\")
'            PathName = Left(resultSuccess(1), pos)
'            '* CTM種別でシートに出力
            For II = 0 To UBound(ctmList)
                iResult = iResult + 1
'                FileName = PathName + ctmList(II).ID + ".csv"
'                If Dir(FileName) <> "" Then
'                    iResult = iResult + 1
'                End If
            Next
        'ミッションIDがない場合(find分岐)
        ElseIf UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
            iResult = CInt(resultSuccess(1))
        
        End If
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
        
        CTMRange.Offset(0, 1).Value = iResult
    End If
    
    '20161117 追加
    If iResult <= 0 Then
        '* ISSUE_NO.686 sunyi 2018/05/09 start
        '* データない時、シートをクリアする
        Call DeleteOutrangeData2(Workbooks(thisBookName).Worksheets(ResultSheetName))
        '* ISSUE_NO.686 sunyi 2018/05/09 end
        Exit Sub
    End If

'* ISSUE_NO.625 Delete ↓↓↓ *******************************
'    '* resultシート上の以前のデータを削除する
'    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
'    With currWorksheet
'        Set srchRange = currWorksheet.Range("A2")
'        If srchRange.Value <> "" Then
'            Set srchRange = currWorksheet.Range("A1")
'            Set lastRange = srchRange.End(xlToRight)
'            Set srchRange = currWorksheet.Range("A2").End(xlDown)
'            Set endRange = currWorksheet.Cells(srchRange.Row, lastRange.Column)
'
'            Set workRange = currWorksheet.Range("A2", endRange.Cells)
'            'Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
'            'workRange.Select
'            workRange.ClearContents
'        End If
'    End With
'* ISSUE_NO.625 Delete ↑↑↑ *******************************

    '* 新resultシートがなければ作成する
    Set actWorksheet = Workbooks(thisBookName).ActiveSheet
'    Set rstWorksheet = Workbooks(thisBookName).Worksheets.Add(after:=Worksheets(Worksheets.Count))
'    rstWorksheet.Name = ResultSheetName & "_work"
    actWorksheet.Activate

    '* 取得CTM毎に処理
    For II = 0 To UBound(ctmList)
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'        If resultSuccess(0) = "SUCCESS1" Then
'
'            ' ディレクトリを取得
'            pos = InStrRev(resultSuccess(1), "\")
'            PathName = Left(resultSuccess(1), pos)
'
'            FileName = PathName + ctmList(II).ID + ".csv"
'
'            If Dir(FileName) <> "" Then
'                Call OutputCTMInfoToSheetFromCsv(ctmList(II).name, 0#, FileName)
'            End If
'
'        ElseIf resultSuccess(0) = "SUCCESS2" Then
'
'            '* 取得データがあれば該当CTMシートへ出力
'    ''        If iResult > 0 Then
'
'                'CTMシートの内容削除
'                Set currWorksheet = Workbooks(thisBookName).Worksheets(ctmList(II).name)
'                With currWorksheet
'                    Set srchRange = currWorksheet.Range("A2")
'                    If srchRange.Value <> "" Then
'                        Set lastRange = srchRange.End(xlToRight)
'                        Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
'                        workRange.ClearContents
'                    End If
'                End With
'
'                'CTMシートへの書込
'                Call OutputCTMInfoToSheet(ctmList(II).name, ctmValue, False)
'    ''        End If
'        End If
        'ミッションIDがない場合(find分岐)
        If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
            'CTMシートの内容削除
            Set currWorksheet = Workbooks(thisBookName).Worksheets(ctmList(II).name)
            With currWorksheet
                Set srchRange = currWorksheet.Range("A2")
                If srchRange.Value <> "" Then
                    Set lastRange = srchRange.End(xlToRight)
                    Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
                    workRange.ClearContents
                End If
            End With
                                       
            'CTMシートへの書込
            Call OutputCTMInfoToSheet(ctmList(II).name, ctmValue, False)
        End If
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End

'* ISSUE_NO.625 Delete ↓↓↓ *******************************
'        '* resultシートに転記
'        Call OutputResultToSheet_Colum(ctmList(II), Workbooks(thisBookName).Worksheets(ResultSheetName))
'* ISSUE_NO.625 Delete ↑↑↑ *******************************
        'Call OutputResultToSheet_Colum(ctmList(II), rstWorksheet)
    Next

    '* 旧resultシートを削除し、新resultシートの名称を変更する
''    Application.DisplayAlerts = False
''    Workbooks(thisBookName).Worksheets(ResultSheetName).Delete
''    Application.DisplayAlerts = True
''    rstWorksheet.Name = ResultSheetName
    
'* ISSUE_NO.625 Add ↓↓↓ *******************************
    ' ディレクトリを取得
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
    'ミッションIDがない場合(find分岐)
    If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
        pos = InStrRev(resultSuccess(1), "\")
        PathName = Left(resultSuccess(1), pos)
        
        FileName = PathName + ResultSheetName + ".txt"
        
        Call DeleteOutrangeData2(Workbooks(thisBookName).Worksheets(ResultSheetName))
        If Dir(FileName) <> "" Then
            Call AddCTMInfoToSheetFromCsv1(ResultSheetName, 0#, FileName)
        End If
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
    End If
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
'* ISSUE_NO.625 Add ↑↑↑ *******************************
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
    '* resultシートをRECEIVE TIMEに従いソートする
    With currWorksheet
        Set srchRange = .Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = srchRange.End(xlDown)
        Set lastRange = .Cells(workRange.Row, lastRange.Column)
        Set workRange = .Range(srchRange, lastRange)
        workRange.Sort Key1:=.Range("B1"), _
                     Order1:=xlDescending, _
                     Header:=xlGuess
    End With
                
    '前回更新日時セット
    Set srchRange = Workbooks(thisBookName).Worksheets(MainSheetName).Cells.Find(What:="前回起動", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd")
        srchRange.Offset(0, 2).Value = Format(Now, "hh:mm:ss")
    End If
    
End Sub

'****************************************************************
'* 対象ミッションの情報を取得する
'****************************************************************
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'Graph_Alarm sunyi 2018/09/12 start
Public Function Teikoku_GetMissionInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO, sType As String, Optional OnServer As String = "False") As String
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim missionID   As String
    Dim CtmId       As String
    Dim startTime   As Double
    Dim endTime     As Double
    Dim SendUrlStr  As String
    Dim SendString  As Variant
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim iResult     As Integer
    Dim bPmary      As Variant
    Dim II          As Integer
    Dim JJ          As Integer
    Dim JJJ         As Integer
    Dim KK          As Integer
    Dim currCTM     As CTMINFO
    
    Dim cmsVersion  As String
    Dim mfIPAddr    As String
    Dim mfPortNo    As String
    Dim chgStr01    As String
    Dim chgStr02    As String
    
    Dim workFolder  As String
    Dim workFile    As String
    
    Dim fileLength  As Long
    Dim divideNum   As Integer  ' 分割取得回数
    Dim divideTime  As Double
    Dim wkStartTime As Double
    Dim wkEndTime   As Double
    
    Dim dispTerm        As Double
    Dim dispTermUnit    As String
    Dim pos             As Integer
    
    Dim wrkEnd As String
    Dim period As String
    Dim onlineId As String
    
    Dim jsonParse As New FoaCoreCom.jsonParse
    Dim keyOrder() As String
    
    Dim useProxy As Boolean
    Dim proxyUri As String
      
    'FOR COM高速化 zhengxiaoxue 2018/06/15 Add Start
    Dim ctmData As FoaCoreCom.CtmDataRetriever
    Dim csvWrite As FoaCoreCom.CsvWriteToExcelHandler
    Dim zipFile As FoaCoreCom.ZipFileHandler
    
    Dim zipUrl As String
    Dim unZipUrl As String
    Dim msg As String
    'FOR COM高速化 zhengxiaoxue 2018/06/15 Add End

    
    '* メインシートから各種情報を取得
    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(srchRange.Offset(0, 1).Value)
            startT = TimeValue(Format(srchRange.Offset(0, 2).Value, "hh:mm:ss"))
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            Teikoku_GetMissionInfo = "ERROR"
            Exit Function
        End If
        '* 2016/09/13 Add ↓↓↓*********************************************
        startD = Date
        '* 2016/09/13 Add ↑↑↑*********************************************
        startTime = GetUnixTime(startD + startT)
        
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Teikoku_GetMissionInfo = "ERROR"
            Exit Function
        End If
        endTime = GetUnixTime(endD + endT)
        
        '*******************************
        '* 表示期間を取得
        '*******************************
        Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                dispTerm = 1#
                dispTermUnit = "【時】"
            Else
                dispTerm = srchRange.Offset(0, 1).Value
                dispTermUnit = srchRange.Offset(0, 2).Value
            End If
        Else
            dispTerm = 1#
            dispTermUnit = "【時】"
        End If
        
        Select Case dispTermUnit
            Case "【分】"
                dispTerm = dispTerm * 60
            Case "【時】"
                dispTerm = dispTerm * 3600
        End Select
        endTime = startTime + dispTerm

        startDT = startD + startT
        endDT = DateAdd("s", dispTerm, startDT)
        
        .Range("H3").Value = Format(startDT, "yyyy/mm/dd hh:mm:ss")
        .Range("I3").Value = Format(endDT, "yyyy/mm/dd hh:mm:ss")
        
        startTime = GetUnixTime(startDT)
        endTime = GetUnixTime(endDT)
        
        '*********************
        '* ミッションIDを取得
        '*********************
        missionID = ""
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionID = srchRange.Offset(0, 1).Value
        Else
            'MsgBox "ミッションID項目が見つからないため処理を中断します。"
            Teikoku_GetMissionInfo = "ERROR"
            Exit Function
        End If
        
    End With
    
    With Workbooks(thisBookName).Worksheets(ParamSheetName)

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If

        ' 2016.07.20 Add *********************
        '* CMSバージョンを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="CmsVersion", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            cmsVersion = srchRange.Offset(0, 1).Value
        Else
            cmsVersion = "V5"
        End If
        
        ' 2016.07.20 Add *********************
        '* 旧バージョン用IPアドレスおよびPortNoを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="MfHost", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            mfIPAddr = srchRange.Offset(0, 1).Value
            mfPortNo = srchRange.Offset(1, 1).Value
        Else
            mfIPAddr = "localhost"
            mfPortNo = "50031"
        End If
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
        
        
    End With
    
    '*******************************
    '* CTM情報一覧をシートから取得
    '*******************************
    Call GetCTMList_Colum(MainSheetName, ctmList)
    
    '*******************************
    '* プログラムミッションに問合せ
    '*******************************
    '* ミッションIDがあれば
    If missionID <> "" Then
        getEndTime = endTime
        getStartTime = startTime
        wrkEnd = CStr(GetUnixTime(Now))
        onlineId = GetOnlineId()
        period = CStr(GetUpdatePeriod())
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete Start
'        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pm?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & wrkEnd & _
'                        "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true" & "&onlineId=" & onlineId & "&period=" & period & "&collectEnd=" & endTime
'
'        If useProxy = False Then
'            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'        Else
'            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'            httpObj.setProxy 2, proxyUri
'        End If
'
'        httpObj.Open "GET", SendUrlStr, False
'        httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
'        httpObj.send
'
'        ' ダウンロード待ち
'        Do While httpObj.readyState <> 4
'            DoEvents
'        Loop
'
'        On Error GoTo ErrorMissionHandler
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete End
        
        '* 取得情報のデコード
        'Set sc = CreateObject("ScriptControl")
        'With sc
        '    .Language = "JScript"
        '
        '    '指定したインデックス、名称のデータを取得する
        '    .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
        'End With
        'Set objJSON = sc.CodeObject.jsonParse(GetString)
        '
        'Set sc = Nothing
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete Start
'        Call GetKeyOrder(MainSheetName, keyOrder)
'
'        GetString = httpObj.responseText
'
'        Teikoku_GetMissionInfo = jsonParse.buildCsvFileCtmMission(GetString, GetTimezoneId, keyOrder)
'
'        Set httpObj = Nothing
'
'        Teikoku_GetMissionInfo = "SUCCESS1@@" + Teikoku_GetMissionInfo
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete End
    
        '* 取得情報をCTM情報に展開
        'ReDim ctmValue(0)
        'ctmValue(0).ID = ""
        'iResult = GetCTMInfo(objJSON, ctmList, ctmValue)
        '
        'Set objJSON = Nothing
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pmzip?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & wrkEnd & _
                        "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true" & "&onlineId=" & onlineId & "&period=" & period & "&collectEnd=" & endTime

        Set ctmData = CreateObject("FoaCoreCom.ais.retriever.CtmDataRetriever")
        Set csvWrite = CreateObject("FoaCoreCom.ais.retriever.CsvWriteToExcelHandler")
        Set zipFile = CreateObject("FoaCoreCom.ais.retriever.ZipFileHandler")
        msg = ""
    
        '情報を格納するZIPファイルパスを取得する
        zipUrl = ctmData.GetProgramMissionZipFileAsync(SendUrlStr, proxyUri, msg)
    
        'Graph_Alarm sunyi 2018/09/12 start
        If OnServer <> "True" Then
            '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
            If InStr(msg, "MISSION_NOT_FOUND") > 0 Then
                MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
                
                Teikoku_GetMissionInfo = "ERROR"
                Exit Function
            End If
            '**********
        End If
    
        'Zipファイルを解凍する
        unZipUrl = zipFile.unZipFile(zipUrl)
    
        'excelへ書き込む
        Call csvWrite.ReadAllCtmInfoToExcel(Workbooks(thisBookName), unZipUrl, MaxCntImport, sType, GetTimezoneId(), MainSheetName)
    
        '一時フォルダを削除
        zipFile.deleteFolder (unZipUrl)
        
        Teikoku_GetMissionInfo = "SUCCESS1@@" + "0"
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
    
    '* ミッションIDがなければ
    Else
    
        ReDim ctmValue(0)
        ctmValue(0).ID = ""
        For II = 0 To UBound(ctmList)
    
            currCTM = ctmList(II)
            CtmId = currCTM.ID
        
            getEndTime = endTime
            getStartTime = startTime
            
            SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/ctm/find?testing=0"
            
            If useProxy = False Then
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
            Else
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                httpObj.setProxy 2, proxyUri
            End If
    
            httpObj.Open "POST", SendUrlStr, False
            httpObj.setRequestHeader "Content-Type", "text/plain"
            httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***Content-Type: text/plain
            
            ' 2016.07.20 Change ↓↓↓ ***************************
            ' 2016.07.22 Change ↓↓↓ *** POST文字列に「testing=0」を追加
            If cmsVersion = "3.5" Then
                SendString = "{""ctmId"":""" & CtmId & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & _
                               ",""mfHostName"":" & """" & mfIPAddr & """" & ",""mfPort"":" & """" & mfPortNo & """" & "}"
            Else
                SendString = "{""ctmId"":""" & CtmId & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & "}"
            End If
            ' 2016.07.22 Change ↑↑↑ ***************************
            ' 2016.07.20 Change ↑↑↑ ***************************
            
            bPmary = StrConv(SendString, vbFromUnicode)
            
            httpObj.send (SendString)
                        
            ' ダウンロード待ち
            Do While httpObj.readyState <> 4
                DoEvents
            Loop
            
            On Error GoTo ErrorCTMHandler

            '* 取得情報のデコード
            Set sc = CreateObject("ScriptControl")
            With sc
                .Language = "JScript"
        
                '指定したインデックス、名称のデータを取得する
                .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
            End With
'            GetString = convTextEncoding(httpObj.responseBody, "UTF-8")
            GetString = httpObj.responseText
            
            ' 2016.07.20 Add ***************************
            If cmsVersion = "3.5" Then
                chgStr01 = Replace(GetString, "\""", "@@")
                chgStr02 = Replace(chgStr01, """", "")
                GetString = Replace(chgStr02, "@@", """")
            End If
            ' 2016.07.20 Add ***************************
            
            Set objJSON = sc.CodeObject.jsonParse(GetString)
            
            Set sc = Nothing
            
            Set httpObj = Nothing
        
            '* 取得情報をCTM情報に展開
            iResult = GetCTMInfoForCTM(objJSON, currCTM, ctmValue)
            
            Teikoku_GetMissionInfo = "SUCCESS2@@" + iResult
            
            Set objJSON = Nothing
        
        Next
    
    End If
    '* ISSUE_NO.625 sunyi 2018/04/25 start
'    ' MsgBox http.responseText
'    Teikoku_GetMissionInfo = iResult
    '* ISSUE_NO.625 sunyi 2018/04/25 end
    
    Exit Function

ErrorMissionHandler:

    Set sc = Nothing
    Set httpObj = Nothing
    Set objJSON = Nothing
    
    '* ミッションIDによる取得に失敗した場合
    For JJ = 3 To 10
    
        divideNum = JJ ^ 2
    
        divideTime = CLng((endTime - startTime) / divideNum)
        
        wkStartTime = startTime
        For II = 0 To divideNum - 1
            wkEndTime = wkStartTime + divideTime

            iResult = GetDivideMissionInfo(ctmList, ctmValue, missionID, wkStartTime, wkEndTime)

            '* 取得失敗したらループを抜け分割数を増加させる
            If iResult < 0 Then Exit For
            
            '* 取得CTM毎に処理
            For JJJ = 0 To UBound(ctmList)
        
                '* 取得データがあれば該当CTMシートへ出力
                If iResult > 0 Then
                    Call OutputCTMInfoToSheet(ctmList(JJJ).name, ctmValue, False)
                End If

                DoEvents
            Next
            
            wkStartTime = wkEndTime + 1#
            
            DoEvents
        Next
        
        '* 分割数分処理が終了したら分割処理ループを抜ける
        If II > (divideNum - 1) Then Exit For
        
        DoEvents
    Next
    
    Teikoku_GetMissionInfo = "ERROR"
    
    Exit Function
    
    
ErrorCTMHandler:
    
    Set sc = Nothing
    Set httpObj = Nothing
    Set objJSON = Nothing
    
    '* CTM IDによる取得に失敗した場合
    For JJ = 3 To 10
    
        divideNum = JJ ^ 2
    
        divideTime = CLng((endTime - startTime) / divideNum)
        
        wkStartTime = startTime
        For II = 0 To divideNum - 1
            wkEndTime = wkStartTime + divideTime

            iResult = GetDivideCTMInfo(ctmList, ctmValue, wkStartTime, wkEndTime)

            '* 取得失敗したらループを抜け分割数を増加させる
            If iResult < 0 Then Exit For

            '* 取得CTM毎に処理
            For JJJ = 0 To UBound(ctmList)
        
                '* 取得データがあれば該当CTMシートへ出力
                If iResult > 0 Then
                    Call OutputCTMInfoToSheet(ctmList(JJJ).name, ctmValue, False)
                End If

                DoEvents
            Next
            
            wkStartTime = wkEndTime + 1#
            
            DoEvents
        Next
        
        '* 分割数分処理が終了したら分割処理ループを抜ける
        If II > (divideNum - 1) Then Exit For
        
        DoEvents
    Next
    
    Teikoku_GetMissionInfo = "ERROR"
    
    Exit Function
    
End Function


'--2017/05/02 Add No.683 テスト実行時の処理 ----Start
'*******************************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する（「テスト」用）
'*******************************************************************
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'Public Sub Teikoku_GetMissionInfoAll_Test()
Public Sub Teikoku_GetMissionInfoAll_Test(sType As String)
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    Dim ctmList()       As CTMINFO      ' CTM情報
    Dim ctmValue()      As CTMINFO      ' CTM取得情報
    Dim II              As Integer
    Dim srchRange       As Range
    Dim lastRange       As Range
    Dim workRange       As Range
    Dim endRange        As Range
    
    Dim currWorksheet   As Worksheet
    Dim selectStr       As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim dispTerm        As Double
    Dim dispTermUnit    As String
    
    Dim actWorksheet    As Worksheet
    Dim rstWorksheet    As Worksheet
    
    Dim CTMRange        As Range
        
    Dim iResult         As Integer
    
    Dim result     As String
    Dim resultSuccess() As String
    
    Dim PathName As String, FileName As String, pos As Long
    Dim endData         As Double
    
    '* CMSからCTM情報を取得する
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
    'result = Teikoku_GetMissionInfo_Test(ctmList, ctmValue)
    result = Teikoku_GetMissionInfo_Test(ctmList, ctmValue, sType)
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    
    If result = "ERROR" Then
        Exit Sub
    End If
            
    resultSuccess = Split(result, "@@")

    'CTM取得件数設定 20161117 追加
    Set CTMRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="CTM取得件数", LookAt:=xlWhole)
    If Not CTMRange Is Nothing Then
    
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
        If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS1" Then
'            ' ディレクトリを取得
'            pos = InStrRev(resultSuccess(1), "\")
'            PathName = Left(resultSuccess(1), pos)
'            '* CTM種別でシートに出力
            For II = 0 To UBound(ctmList)
                iResult = iResult + 1
'                FileName = PathName + ctmList(II).ID + ".csv"
'                If Dir(FileName) <> "" Then
'                    iResult = iResult + 1
'                End If
            Next
        ElseIf UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then

            iResult = CInt(resultSuccess(1))
        
        End If
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    
        CTMRange.Offset(0, 1).Value = iResult
    End If
    
    '20161117 追加
    If iResult <= 0 Then
        '* ISSUE_NO.686 sunyi 2018/05/09 start
        '* データない時、シートをクリアする
        Call DeleteOutrangeData2(Workbooks(thisBookName).Worksheets(ResultSheetName))
        '* ISSUE_NO.686 sunyi 2018/05/09 end
        Exit Sub
    End If
'* ISSUE_NO.625 Delete ↓↓↓ *******************************
'    '* resultシート上の以前のデータを削除する
'    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
'    With currWorksheet
'        Set srchRange = currWorksheet.Range("A2")
'        If srchRange.Value <> "" Then
'            Set srchRange = currWorksheet.Range("A1")
'            Set lastRange = srchRange.End(xlToRight)
'            Set srchRange = currWorksheet.Range("A2").End(xlDown)
'            Set endRange = currWorksheet.Cells(srchRange.Row, lastRange.Column)
'
'            Set workRange = currWorksheet.Range("A2", endRange.Cells)
'            'Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
'            'workRange.Select
'            workRange.ClearContents
'        End If
'    End With
'* ISSUE_NO.625 Delete ↑↑↑ *******************************
    '* 新resultシートがなければ作成する
    Set actWorksheet = Workbooks(thisBookName).ActiveSheet
'    Set rstWorksheet = Workbooks(thisBookName).Worksheets.Add(after:=Worksheets(Worksheets.Count))
'    rstWorksheet.Name = ResultSheetName & "_work"
    actWorksheet.Activate

    '* 取得CTM毎に処理
    For II = 0 To UBound(ctmList)
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'        If resultSuccess(0) = "SUCCESS1" Then
'
'            ' ディレクトリを取得
'            pos = InStrRev(resultSuccess(1), "\")
'            PathName = Left(resultSuccess(1), pos)
'
'            FileName = PathName + ctmList(II).ID + ".csv"
'
'            If Dir(FileName) <> "" Then
'                Call OutputCTMInfoToSheetFromCsv(ctmList(II).name, 0#, FileName)
'            End If
'
'        ElseIf resultSuccess(0) = "SUCCESS2" Then
'
'            '* 取得データがあれば該当CTMシートへ出力
'    ''        If iResult > 0 Then
'
'                'CTMシートの内容削除
'                Set currWorksheet = Workbooks(thisBookName).Worksheets(ctmList(II).name)
'                With currWorksheet
'                    Set srchRange = currWorksheet.Range("A2")
'                    If srchRange.Value <> "" Then
'                        Set lastRange = srchRange.End(xlToRight)
'                        Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
'                        workRange.ClearContents
'                    End If
'                End With
'
'                'CTMシートへの書込
'                Call OutputCTMInfoToSheet(ctmList(II).name, ctmValue, False)
'    ''        End If
'        End If
        If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
            'CTMシートの内容削除
            Set currWorksheet = Workbooks(thisBookName).Worksheets(ctmList(II).name)
            With currWorksheet
                Set srchRange = currWorksheet.Range("A2")
                If srchRange.Value <> "" Then
                    Set lastRange = srchRange.End(xlToRight)
                    Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
                    workRange.ClearContents
                End If
            End With
                                   
            'CTMシートへの書込
            Call OutputCTMInfoToSheet(ctmList(II).name, ctmValue, False)
        End If
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
        
'* ISSUE_NO.625 Delete ↓↓↓ *******************************
'        '* resultシートに転記
'        Call OutputResultToSheet_Colum(ctmList(II), Workbooks(thisBookName).Worksheets(ResultSheetName))
'* ISSUE_NO.625 Delete ↑↑↑ *******************************
        'Call OutputResultToSheet_Colum(ctmList(II), rstWorksheet)
    Next

    '* 旧resultシートを削除し、新resultシートの名称を変更する
''    Application.DisplayAlerts = False
''    Workbooks(thisBookName).Worksheets(ResultSheetName).Delete
''    Application.DisplayAlerts = True
''    rstWorksheet.Name = ResultSheetName
    
'* ISSUE_NO.625 Add ↓↓↓ *******************************
    ' ディレクトリを取得
    pos = InStrRev(resultSuccess(1), "\")
    PathName = Left(resultSuccess(1), pos)
    
    FileName = PathName + ResultSheetName + ".txt"
    
    Call DeleteOutrangeData2(Workbooks(thisBookName).Worksheets(ResultSheetName))
    If Dir(FileName) <> "" Then
        Call AddCTMInfoToSheetFromCsv1(ResultSheetName, 0#, FileName)
    End If
'* ISSUE_NO.625 Add ↑↑↑ *******************************

    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
    '* resultシートをRECEIVE TIMEに従いソートする
    With currWorksheet
        Set srchRange = .Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = srchRange.End(xlDown)
        Set lastRange = .Cells(workRange.Row, lastRange.Column)
        Set workRange = .Range(srchRange, lastRange)
        workRange.Sort Key1:=.Range("B1"), _
                     Order1:=xlDescending, _
                     Header:=xlGuess
    End With
                
    '前回更新日時セット
    Set srchRange = Workbooks(thisBookName).Worksheets(MainSheetName).Cells.Find(What:="前回起動", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd")
        srchRange.Offset(0, 2).Value = Format(Now, "hh:mm:ss")
    End If
    
End Sub

'****************************************************************
'* 対象ミッションの情報を取得する（「テスト」用）
'****************************************************************
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'Public Function Teikoku_GetMissionInfo_Test(ctmList() As CTMINFO, ctmValue() As CTMINFO) As String
Public Function Teikoku_GetMissionInfo_Test(ctmList() As CTMINFO, ctmValue() As CTMINFO, sType As String) As String
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim missionID   As String
    Dim CtmId       As String
    Dim startTime   As Double
    Dim endTime     As Double
    Dim SendUrlStr  As String
    Dim SendString  As Variant
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim iResult     As Integer
    Dim bPmary      As Variant
    Dim II          As Integer
    Dim JJ          As Integer
    Dim JJJ         As Integer
    Dim KK          As Integer
    Dim currCTM     As CTMINFO
    
    Dim cmsVersion  As String
    Dim mfIPAddr    As String
    Dim mfPortNo    As String
    Dim chgStr01    As String
    Dim chgStr02    As String
    
    Dim workFolder  As String
    Dim workFile    As String
    
    Dim fileLength  As Long
    Dim divideNum   As Integer  ' 分割取得回数
    Dim divideTime  As Double
    Dim wkStartTime As Double
    Dim wkEndTime   As Double
    
    Dim dispTerm        As Double
    Dim dispTermUnit    As String
    Dim pos             As Integer
    
    Dim wrkEnd As String
    Dim period As String
    Dim onlineId As String
    
    Dim jsonParse As New FoaCoreCom.jsonParse
    Dim keyOrder() As String
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    'FOR COM高速化 zhengxiaoxue 2018/06/15 Add Start
    Dim ctmData As FoaCoreCom.CtmDataRetriever
    Dim csvWrite As FoaCoreCom.CsvWriteToExcelHandler
    Dim zipFile As FoaCoreCom.ZipFileHandler
    
    Dim zipUrl As String
    Dim unZipUrl As String
    Dim msg As String
    'FOR COM高速化 zhengxiaoxue 2018/06/15 Add End
    
    '* メインシートから各種情報を取得
    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(srchRange.Offset(0, 1).Value)
            startT = TimeValue(Format(srchRange.Offset(0, 2).Value, "hh:mm:ss"))
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            Teikoku_GetMissionInfo_Test = "ERROR"
            Exit Function
        End If
        '* 2016/09/13 Add ↓↓↓*********************************************
        startD = Date
        '* 2016/09/13 Add ↑↑↑*********************************************
        startTime = GetUnixTime(startD + startT)
        
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Teikoku_GetMissionInfo_Test = "ERROR"
            Exit Function
        End If
        endTime = GetUnixTime(endD + endT)
        
        '*******************************
        '* 表示期間を取得
        '*******************************
        Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                dispTerm = 1#
                dispTermUnit = "【時】"
            Else
                dispTerm = srchRange.Offset(0, 1).Value
                dispTermUnit = srchRange.Offset(0, 2).Value
            End If
        Else
            dispTerm = 1#
            dispTermUnit = "【時】"
        End If
        
        Select Case dispTermUnit
            Case "【分】"
                dispTerm = dispTerm * 60
            Case "【時】"
                dispTerm = dispTerm * 3600
        End Select
        endTime = startTime + dispTerm

        startDT = startD + startT
        endDT = DateAdd("s", dispTerm, startDT)
        
        .Range("H3").Value = Format(startDT, "yyyy/mm/dd hh:mm:ss")
        .Range("I3").Value = Format(endDT, "yyyy/mm/dd hh:mm:ss")
        
        startTime = GetUnixTime(startDT)
        endTime = GetUnixTime(endDT)
        
        '*********************
        '* ミッションIDを取得
        '*********************
        missionID = ""
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionID = srchRange.Offset(0, 1).Value
        Else
            'MsgBox "ミッションID項目が見つからないため処理を中断します。"
            Teikoku_GetMissionInfo_Test = "ERROR"
            Exit Function
        End If
        
    End With
    
    With Workbooks(thisBookName).Worksheets(ParamSheetName)

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If

        ' 2016.07.20 Add *********************
        '* CMSバージョンを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="CmsVersion", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            cmsVersion = srchRange.Offset(0, 1).Value
        Else
            cmsVersion = "V5"
        End If
        
        ' 2016.07.20 Add *********************
        '* 旧バージョン用IPアドレスおよびPortNoを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="MfHost", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            mfIPAddr = srchRange.Offset(0, 1).Value
            mfPortNo = srchRange.Offset(1, 1).Value
        Else
            mfIPAddr = "localhost"
            mfPortNo = "50031"
        End If
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
        
    End With
    
    '*******************************
    '* CTM情報一覧をシートから取得
    '*******************************
    Call GetCTMList_Colum(MainSheetName, ctmList)
    
    '*******************************
    '* プログラムミッションに問合せ
    '*******************************
    '* ミッションIDがあれば
    If missionID <> "" Then
        getEndTime = endTime
        getStartTime = startTime
        'wrkEnd = CStr(GetUnixTime(Now)) No.683 Mod.
        wrkEnd = CStr(endTime)
        onlineId = ""
        period = CStr(GetUpdatePeriod())
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete Start
'        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pm?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & wrkEnd & _
'                        "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true" & "&onlineId=" & onlineId & "&period=" & period & "&collectEnd=" & endTime
'
'        If useProxy = False Then
'            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'        Else
'            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'            httpObj.setProxy 2, proxyUri
'        End If
'
'        httpObj.Open "GET", SendUrlStr, False
'        httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
'        httpObj.send
'
'        ' ダウンロード待ち
'        Do While httpObj.readyState <> 4
'            DoEvents
'        Loop
'
'        On Error GoTo ErrorMissionHandler
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete End

        
        '* 取得情報のデコード
        'Set sc = CreateObject("ScriptControl")
        'With sc
        '    .Language = "JScript"
        '
        '    '指定したインデックス、名称のデータを取得する
        '    .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
        'End With
        'Set objJSON = sc.CodeObject.jsonParse(GetString)
        '
        'Set sc = Nothing
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete Start
'        Call GetKeyOrder(MainSheetName, keyOrder)
'
'        GetString = httpObj.responseText
'
'        Teikoku_GetMissionInfo_Test = jsonParse.buildCsvFileCtmMission(GetString, GetTimezoneId, keyOrder)
'
'
'        Set httpObj = Nothing
'
'        Teikoku_GetMissionInfo_Test = "SUCCESS1@@" + Teikoku_GetMissionInfo_Test
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete End
    
        '* 取得情報をCTM情報に展開
        'ReDim ctmValue(0)
        'ctmValue(0).ID = ""
        'iResult = GetCTMInfo(objJSON, ctmList, ctmValue)
        '
        'Set objJSON = Nothing
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pmzip?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & wrkEnd & _
                        "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true" & "&onlineId=" & onlineId & "&period=" & period & "&collectEnd=" & endTime
                        
        Set ctmData = CreateObject("FoaCoreCom.ais.retriever.CtmDataRetriever")
        Set csvWrite = CreateObject("FoaCoreCom.ais.retriever.CsvWriteToExcelHandler")
        Set zipFile = CreateObject("FoaCoreCom.ais.retriever.ZipFileHandler")
        msg = ""
    
        '情報を格納するZIPファイルパスを取得する
        zipUrl = ctmData.GetProgramMissionZipFileAsync(SendUrlStr, proxyUri, msg)
        
        On Error GoTo ErrorMissionHandler
        
        '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
        If InStr(msg, "MISSION_NOT_FOUND") > 0 Then
            MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
            
            Teikoku_GetMissionInfo_Test = "ERROR"
            Exit Function
        End If
        '**********
    
        'Zipファイルを解凍する
        unZipUrl = zipFile.unZipFile(zipUrl)
    
        'excelへ書き込む
        Call csvWrite.ReadAllCtmInfoToExcel(Workbooks(thisBookName), unZipUrl, MaxCntImport, sType, GetTimezoneId(), MainSheetName)
    
        '一時フォルダを削除
        zipFile.deleteFolder (unZipUrl)
        
        Teikoku_GetMissionInfo_Test = "SUCCESS1@@" + "0"
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
    
    '* ミッションIDがなければ
    Else
    
        ReDim ctmValue(0)
        ctmValue(0).ID = ""
        For II = 0 To UBound(ctmList)
    
            currCTM = ctmList(II)
            CtmId = currCTM.ID
        
            getEndTime = endTime
            getStartTime = startTime
            
            SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/ctm/find?testing=0"
            
            If useProxy = False Then
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
            Else
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                httpObj.setProxy 2, proxyUri
            End If
                
            httpObj.Open "POST", SendUrlStr, False
            httpObj.setRequestHeader "Content-Type", "text/plain"
            httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***Content-Type: text/plain
            
            ' 2016.07.20 Change ↓↓↓ ***************************
            ' 2016.07.22 Change ↓↓↓ *** POST文字列に「testing=0」を追加
            If cmsVersion = "3.5" Then
                SendString = "{""ctmId"":""" & CtmId & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & _
                               ",""mfHostName"":" & """" & mfIPAddr & """" & ",""mfPort"":" & """" & mfPortNo & """" & "}"
            Else
                SendString = "{""ctmId"":""" & CtmId & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & "}"
            End If
            ' 2016.07.22 Change ↑↑↑ ***************************
            ' 2016.07.20 Change ↑↑↑ ***************************
            
            bPmary = StrConv(SendString, vbFromUnicode)
            
            httpObj.send (SendString)
                        
            ' ダウンロード待ち
            Do While httpObj.readyState <> 4
                DoEvents
            Loop
            
            On Error GoTo ErrorCTMHandler

            '* 取得情報のデコード
            Set sc = CreateObject("ScriptControl")
            With sc
                .Language = "JScript"
        
                '指定したインデックス、名称のデータを取得する
                .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
            End With
'            GetString = convTextEncoding(httpObj.responseBody, "UTF-8")
            GetString = httpObj.responseText
            
            ' 2016.07.20 Add ***************************
            If cmsVersion = "3.5" Then
                chgStr01 = Replace(GetString, "\""", "@@")
                chgStr02 = Replace(chgStr01, """", "")
                GetString = Replace(chgStr02, "@@", """")
            End If
            ' 2016.07.20 Add ***************************
            
            Set objJSON = sc.CodeObject.jsonParse(GetString)
            
            Set sc = Nothing
            
            Set httpObj = Nothing
        
            '* 取得情報をCTM情報に展開
            iResult = GetCTMInfoForCTM(objJSON, currCTM, ctmValue)
            
            Teikoku_GetMissionInfo_Test = "SUCCESS2@@" + iResult
            
            Set objJSON = Nothing
        
        Next
    
    End If
    
    
    Exit Function

ErrorMissionHandler:

    Set sc = Nothing
    Set httpObj = Nothing
    Set objJSON = Nothing
    
    '* ミッションIDによる取得に失敗した場合
    For JJ = 3 To 10
    
        divideNum = JJ ^ 2
    
        divideTime = CLng((endTime - startTime) / divideNum)
        
        wkStartTime = startTime
        For II = 0 To divideNum - 1
            wkEndTime = wkStartTime + divideTime

            iResult = GetDivideMissionInfo(ctmList, ctmValue, missionID, wkStartTime, wkEndTime)

            '* 取得失敗したらループを抜け分割数を増加させる
            If iResult < 0 Then Exit For
            
            '* 取得CTM毎に処理
            For JJJ = 0 To UBound(ctmList)
        
                '* 取得データがあれば該当CTMシートへ出力
                If iResult > 0 Then
                    Call OutputCTMInfoToSheet(ctmList(JJJ).name, ctmValue, False)
                End If

                DoEvents
            Next
            
            wkStartTime = wkEndTime + 1#
            
            DoEvents
        Next
        
        '* 分割数分処理が終了したら分割処理ループを抜ける
        If II > (divideNum - 1) Then Exit For
        
        DoEvents
    Next
    
    Teikoku_GetMissionInfo_Test = "ERROR"
    
    Exit Function
    
    
ErrorCTMHandler:
    
    Set sc = Nothing
    Set httpObj = Nothing
    Set objJSON = Nothing
    
    '* CTM IDによる取得に失敗した場合
    For JJ = 3 To 10
    
        divideNum = JJ ^ 2
    
        divideTime = CLng((endTime - startTime) / divideNum)
        
        wkStartTime = startTime
        For II = 0 To divideNum - 1
            wkEndTime = wkStartTime + divideTime

            iResult = GetDivideCTMInfo(ctmList, ctmValue, wkStartTime, wkEndTime)

            '* 取得失敗したらループを抜け分割数を増加させる
            If iResult < 0 Then Exit For

            '* 取得CTM毎に処理
            For JJJ = 0 To UBound(ctmList)
        
                '* 取得データがあれば該当CTMシートへ出力
                If iResult > 0 Then
                    Call OutputCTMInfoToSheet(ctmList(JJJ).name, ctmValue, False)
                End If

                DoEvents
            Next
            
            wkStartTime = wkEndTime + 1#
            
            DoEvents
        Next
        
        '* 分割数分処理が終了したら分割処理ループを抜ける
        If II > (divideNum - 1) Then Exit For
        
        DoEvents
    Next
    
    Teikoku_GetMissionInfo_Test = "ERROR"
    
    Exit Function
    
End Function
'--2017/05/02 Add No.683 テスト実行時の処理 ----End

