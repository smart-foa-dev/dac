Attribute VB_Name = "Mission_Module"
Option Explicit

'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
'Public Sub GetMissionInfoAll()
'    Dim ctmList()   As CTMINFO      ' CTM情報
'    Dim ctmValue()  As CTMINFO      ' CTM取得情報
'    Dim iResult     As Integer
'    Dim II          As Integer
'
'    iResult = GetMissionInfo(ctmList, ctmValue)
'
'    '* シート毎（シート名＝CTM名）へ出力
'    If iResult > 0 Then
'
'        '* CTM種別でシートに出力
'        For II = 0 To UBound(ctmList)
'
'            Call OutputCTMInfoToSheet(ctmList(II).Name, 0#, ctmValue)
'
'        Next
'
'    End If
'
'End Sub


'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'Public Sub GetMissionInfoAllTimer()
Public Sub GetMissionInfoAllTimer(sType As String, Optional OnServer As String = "False")
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    Dim ctmList()       As CTMINFO      ' CTM情報
    Dim ctmValue()      As CTMINFO      ' CTM取得情報
    Dim II              As Integer
    Dim srchRange       As Range
    Dim lastRange       As Range
    Dim workRange       As Range
    Dim endRange        As Range
    
    Dim currWorksheet   As Worksheet
    Dim selectStr       As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim dispTerm        As Double
    Dim dispTermUnit    As String
    
    Dim actWorksheet    As Worksheet
    Dim rstWorksheet    As Worksheet
    
    Dim CTMRange        As Range
        
    Dim iResult As Integer
    
    Dim result     As String
    Dim resultSuccess() As String
    
    Dim PathName As String, FileName As String, pos As Long
    
    '* ISSUE_NO.664 sunyi 2018/04/27 start
    '* resultシートの重複データを削除する
    'Dim endData As Double
    Dim startTime As Double
    '* ISSUE_NO.664 sunyi 2018/04/27 end
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="オンライン種別", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                selectStr = "期間固定"
            Else
                selectStr = srchRange.Offset(0, 1).Value
            End If
        Else
                selectStr = "期間固定"
        End If
    
    End With
    
    '* ISSUE_NO.664 sunyi 2018/04/27 start
    '* resultシートの重複データを削除する
    '* メインシートから各種情報を取得
    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        startTime = GetCollectStartDT(Workbooks(thisBookName).Worksheets(MainSheetName))
    End With
    '* ISSUE_NO.664 sunyi 2018/04/27 end
        
    '* CMSからCTM情報を取得する
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
    'result = GetMissionInfo(ctmList, ctmValue)
    'Graph_Alarm sunyi 2018/09/12 start
    result = GetMissionInfo(ctmList, ctmValue, sType, OnServer)
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    
    If result = "ERROR" Then
        Exit Sub
    End If
    
   
    resultSuccess = Split(result, "@@")

    
    'CTM取得件数設定 20161117 追加
    Set CTMRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="CTM取得件数", LookAt:=xlWhole)
    If Not CTMRange Is Nothing Then
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
        If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS1" Then
'            ' ディレクトリを取得
'            pos = InStrRev(resultSuccess(1), "\")
'            PathName = Left(resultSuccess(1), pos)
'            '* CTM種別でシートに出力
            For II = 0 To UBound(ctmList)
                iResult = iResult + 1
'                FileName = PathName + ctmList(II).ID + ".csv"
'                If Dir(FileName) <> "" Then
'                    iResult = iResult + 1
'                End If
            Next
        'ミッションIDがない場合(find分岐)
        ElseIf UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
            iResult = CInt(resultSuccess(1))
        
        End If
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
        
        CTMRange.Offset(0, 1).Value = iResult
    End If
    
    '* 表示期間の設定
    Select Case selectStr
        Case "期間固定"
            dispTerm = 0#
        Case "連続移動"
            '*******************************
            '* 表示期間を取得
            '*******************************
            Set srchRange = currWorksheet.Cells.Find(What:="表示期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                If IsEmpty(srchRange.Offset(0, 1).Value) Then
                    dispTerm = 1#
                    dispTermUnit = "【時】"
                Else
                    dispTerm = srchRange.Offset(0, 1).Value
                    dispTermUnit = srchRange.Offset(0, 2).Value
                End If
            Else
                dispTerm = 1#
                dispTermUnit = "【時】"
            End If
            Select Case dispTermUnit
                Case "【分】"
                    dispTerm = dispTerm * 60 * (-1)
                Case "【時】"
                    dispTerm = dispTerm * 3600 * (-1)
            End Select
    End Select
    
'* ISSUE_NO.625 Delete ↓↓↓ *******************************
'    '* resultシート上の以前のデータを削除する
'    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
'    With currWorksheet
'        Set srchRange = currWorksheet.Range("A2")
'        If srchRange.Value <> "" Then
'            Set srchRange = currWorksheet.Range("A1")
'            Set lastRange = srchRange.End(xlToRight)
'            Set srchRange = currWorksheet.Range("A2").End(xlDown)
'            Set endRange = currWorksheet.Cells(srchRange.Row, lastRange.Column)
'
'            Set workRange = currWorksheet.Range("A2", endRange.Cells)
'            'Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
'            'workRange.Select
'            workRange.ClearContents
'        End If
'    End With
'* ISSUE_NO.625 Delete ↑↑↑ *******************************

    '* 2016.07.21 Delete ↑↑↑ *******************************
    
    '* 2016.07.21 Add ↓↓↓ *******************************
    '* 新resultシートがなければ作成する
    Set actWorksheet = Workbooks(thisBookName).ActiveSheet
    '*ISSUE_NO.752 Sunyi 2018/06/25 Start
    '選択シートを戻す
'    actWorksheet.Activate
    '*ISSUE_NO.752 Sunyi 2018/06/25 End
    
    '* 取得CTM毎に処理
    For II = 0 To UBound(ctmList)

        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'        If resultSuccess(0) = "SUCCESS1" Then
'
'            ' ディレクトリを取得
'            pos = InStrRev(resultSuccess(1), "\")
'            PathName = Left(resultSuccess(1), pos)
'
'            FileName = PathName + ctmList(II).ID + ".csv"
'
'            If Dir(FileName) <> "" Then
'                Call AddCTMInfoToSheetFromCsv(ctmList(II).name, 0#, FileName)
'            End If
'
'        ElseIf resultSuccess(0) = "SUCCESS2" Then
'            '* 取得データがあれば該当CTMシートへ出力
'            If iResult > 0 Then
'                Call OutputCTMInfoToSheet(ctmList(II).name, ctmValue, False)
'            End If
'
'        End If
        'ミッションIDがない場合(find分岐)
        If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
            '* 取得データがあれば該当CTMシートへ出力
            If iResult > 0 Then
                Call OutputCTMInfoToSheet(ctmList(II).name, ctmValue, False)
            End If
        End If
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End

        '* 連続移動型の場合、表示期間外のデータを削除する
        If dispTerm < 0 Then

            '***********************************
            '* 表示期間開始日時を取得
            '* →現時刻から表示期間を引いた日時
            '***********************************
            prevDate = Now
            prevTime = DateAdd("s", dispTerm, prevDate)

            '* 表示期間外のデータを削除
            WriteLog ("GetMissionInfoAllTimer() Call DeleteOutrangeData() Sheet:" & ctmList(II).name & " EndDate:" & prevTime & " ColTime:" & "A1")
            Call DeleteOutrangeData(Workbooks(thisBookName).Worksheets(ctmList(II).name), prevTime, "A1")

        End If

'* ISSUE_NO.625 Delete ↓↓↓ *******************************
        '* resultシートに転記
'        Call OutputResultToSheet_Colum(ctmList(II), Workbooks(thisBookName).Worksheets(ResultSheetName))
'* ISSUE_NO.625 Delete ↑↑↑ *******************************
        'Call OutputResultToSheet(ctmList(II).Name, rstWorksheet)

    Next
    
    '* ISSUE_NO.625 sunyi 2018/04/25 start
    '* 連続移動型の場合、表示期間外のデータを削除する
    If dispTerm < 0 Then

        '***********************************
        '* 表示期間開始日時を取得
        '* →現時刻から表示期間を引いた日時
        '***********************************
        prevDate = Now
        prevTime = DateAdd("s", dispTerm, prevDate)

        '* 表示期間外のデータを削除
        WriteLog ("GetMissionInfoAllTimer() Call DeleteOutrangeData() Sheet:" & ResultSheetName & " EndDate:" & prevTime & " ColTime:" & "B1")
        Call DeleteOutrangeData(Workbooks(thisBookName).Worksheets(ResultSheetName), prevTime, "B1")

    End If
    '* ISSUE_NO.625 sunyi 2018/04/25 end
    
    '* 2016.07.21 Add ↓↓↓ *******************************
    '* 旧resultシートを削除し、新resultシートの名称を変更する
''    Application.DisplayAlerts = False
''    Workbooks(thisBookName).Worksheets(ResultSheetName).Delete
''    Application.DisplayAlerts = True
''    rstWorksheet.Name = ResultSheetName
    '* 2016.07.21 Add ↑↑↑ *******************************
    
'* ISSUE_NO.625 Add ↓↓↓ *******************************
'     ディレクトリを取得
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
    'ミッションIDがない場合(find分岐)
    If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
        pos = InStrRev(resultSuccess(1), "\")
        PathName = Left(resultSuccess(1), pos)
    
        FileName = PathName + ResultSheetName + ".txt"
    
        If Dir(FileName) <> "" Then
            '* ISSUE_NO.664 sunyi 2018/04/27 start
            '* resultシートの重複データを削除する
            'endData = GetCollectStartDT(Workbooks(thisBookName).Worksheets(MainSheetName))
            'Call DeleteOutrangeData1(Workbooks(thisBookName).Worksheets(ResultSheetName), endData)
            Call DeleteOutrangeData1(Workbooks(thisBookName).Worksheets(ResultSheetName), startTime)
            '* ISSUE_NO.664 sunyi 2018/04/27 end
            Call AddCTMInfoToSheetFromCsv1(ResultSheetName, 0#, FileName)
        End If
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
    End If
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
'* ISSUE_NO.625 Add ↑↑↑ *******************************
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
    '* resultシートをRECEIVE TIMEに従いソートする
    With currWorksheet
        Set srchRange = .Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = srchRange.End(xlDown)
        Set lastRange = .Cells(workRange.Row, lastRange.Column)
        Set workRange = .Range(srchRange, lastRange)
        workRange.Sort Key1:=.Range("B1"), _
                     Order1:=xlDescending, _
                     Header:=xlGuess
    End With
                
    '前回更新日時セット
    Set srchRange = Workbooks(thisBookName).Worksheets(MainSheetName).Cells.Find(What:="前回起動", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd")
        srchRange.Offset(0, 2).Value = Format(Now, "hh:mm:ss")
    End If
    '*ISSUE_NO.752 Sunyi 2018/06/25 Start
    '選択シートを戻す
    actWorksheet.Activate
    '*ISSUE_NO.752 Sunyi 2018/06/25 End
End Sub

'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'Public Sub GetMissionInfoAll()
Public Sub GetMissionInfoAll(sType As String)
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    Dim ctmList()       As CTMINFO      ' CTM情報
    Dim ctmValue()      As CTMINFO      ' CTM取得情報
    Dim II              As Integer
    Dim srchRange       As Range
    Dim lastRange       As Range
    Dim workRange       As Range
    Dim endRange        As Range
    
    Dim currWorksheet   As Worksheet
    Dim selectStr       As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim dispTerm        As Double
    Dim dispTermUnit    As String
    
    Dim actWorksheet    As Worksheet
    Dim rstWorksheet    As Worksheet
    
    Dim CTMRange        As Range
        
    Dim iResult As Integer
    
    Dim result     As String
    Dim resultSuccess() As String
    
    Dim PathName As String, FileName As String, pos As Long
    
    '* ISSUE_NO.664 sunyi 2018/04/27 start
    '* resultシートの重複データを削除する
    'Dim endData As Double
    Dim startTime As Double
    '* ISSUE_NO.664 sunyi 2018/04/27 end

    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="オンライン種別", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                selectStr = "期間固定"
            Else
                selectStr = srchRange.Offset(0, 1).Value
            End If
        Else
                selectStr = "期間固定"
        End If
    
    End With
    
    '* ISSUE_NO.664 sunyi 2018/04/27 start
    '* resultシートの重複データを削除する
    '* メインシートから各種情報を取得
    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        startTime = GetCollectStartDT(Workbooks(thisBookName).Worksheets(MainSheetName))
    End With
    '* ISSUE_NO.664 sunyi 2018/04/27 end
        
    '* CMSからCTM情報を取得する
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
    'result = GetMissionInfo(ctmList, ctmValue)
    result = GetMissionInfo(ctmList, ctmValue, sType)
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    
    If result = "ERROR" Then
        Exit Sub
    End If
    
   
    resultSuccess = Split(result, "@@")

    
    'CTM取得件数設定 20161117 追加
    Set CTMRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="CTM取得件数", LookAt:=xlWhole)
    If Not CTMRange Is Nothing Then
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
        If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS1" Then
'            ' ディレクトリを取得
'            pos = InStrRev(resultSuccess(1), "\")
'            PathName = Left(resultSuccess(1), pos)
'            '* CTM種別でシートに出力
            For II = 0 To UBound(ctmList)
                iResult = iResult + 1
'                FileName = PathName + ctmList(II).ID + ".csv"
'                If Dir(FileName) <> "" Then
'                    iResult = iResult + 1
'                End If
            Next
        
        'ミッションIDがない場合(find分岐)
        ElseIf UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
            iResult = CInt(resultSuccess(1))
        End If
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End

        CTMRange.Offset(0, 1).Value = iResult
    End If
    
    '* 表示期間の設定
    Select Case selectStr
        Case "期間固定"
            dispTerm = 0#
        Case "連続移動"
            '*******************************
            '* 表示期間を取得
            '*******************************
            Set srchRange = currWorksheet.Cells.Find(What:="表示期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                If IsEmpty(srchRange.Offset(0, 1).Value) Then
                    dispTerm = 1#
                    dispTermUnit = "【時】"
                Else
                    dispTerm = srchRange.Offset(0, 1).Value
                    dispTermUnit = srchRange.Offset(0, 2).Value
                End If
            Else
                dispTerm = 1#
                dispTermUnit = "【時】"
            End If
            Select Case dispTermUnit
                Case "【分】"
                    dispTerm = dispTerm * 60 * (-1)
                Case "【時】"
                    dispTerm = dispTerm * 3600 * (-1)
            End Select
    End Select
    
'* ISSUE_NO.625 Delete ↓↓↓ *******************************
'    '* resultシート上の以前のデータを削除する
'    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
'    With currWorksheet
'        Set srchRange = currWorksheet.Range("A2")
'        If srchRange.Value <> "" Then
'            Set srchRange = currWorksheet.Range("A1")
'            Set lastRange = srchRange.End(xlToRight)
'            Set srchRange = currWorksheet.Range("A2").End(xlDown)
'            Set endRange = currWorksheet.Cells(srchRange.Row, lastRange.Column)
'
'            Set workRange = currWorksheet.Range("A2", endRange.Cells)
'            'Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
'            'workRange.Select
'            workRange.ClearContents
'        End If
'    End With
'* ISSUE_NO.625 Delete ↑↑↑ *******************************

    '* 2016.07.21 Delete ↑↑↑ *******************************
    
    '* 2016.07.21 Add ↓↓↓ *******************************
    '* 新resultシートがなければ作成する
    Set actWorksheet = Workbooks(thisBookName).ActiveSheet
    '*ISSUE_NO.752 Sunyi 2018/06/25 Start
    '選択シートを戻す
'    actWorksheet.Activate
    '*ISSUE_NO.752 Sunyi 2018/06/25 End
    

    '* 取得CTM毎に処理
    For II = 0 To UBound(ctmList)
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'        If resultSuccess(0) = "SUCCESS1" Then
'
'            ' ディレクトリを取得
'            pos = InStrRev(resultSuccess(1), "\")
'            PathName = Left(resultSuccess(1), pos)
'
'            FileName = PathName + ctmList(II).ID + ".csv"
'
'            If Dir(FileName) <> "" Then
'                Call OutputCTMInfoToSheetFromCsv(ctmList(II).name, 0#, FileName)
'            End If
'
'        ElseIf resultSuccess(0) = "SUCCESS2" Then
'            '* 取得データがあれば該当CTMシートへ出力
'            If iResult > 0 Then
'                Call OutputCTMInfoToSheet(ctmList(II).name, ctmValue, False)
'            End If
'
'        End If
        'ミッションIDがない場合(find分岐)
        If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
            '* 取得データがあれば該当CTMシートへ出力
            If iResult > 0 Then
                Call OutputCTMInfoToSheet(ctmList(II).name, ctmValue, False)
            End If
            
        End If
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End

        '* 連続移動型の場合、表示期間外のデータを削除する
        If dispTerm < 0 Then
    
            '***********************************
            '* 表示期間開始日時を取得
            '* →現時刻から表示期間を引いた日時
            '***********************************
            prevDate = Now
            prevTime = DateAdd("s", dispTerm, prevDate)
    
            '* 表示期間外のデータを削除
            WriteLog ("GetMissionInfoAll() Call DeleteOutrangeData() Sheet:" & ctmList(II).name & " EndDate:" & prevTime & " ColTime:" & "A1")
            Call DeleteOutrangeData(Workbooks(thisBookName).Worksheets(ctmList(II).name), prevTime, "A1")
    
        End If
        
'* ISSUE_NO.625 Delete ↓↓↓ *******************************
        '* resultシートに転記
'        Call OutputResultToSheet_Colum(ctmList(II), Workbooks(thisBookName).Worksheets(ResultSheetName))
'* ISSUE_NO.625 Delete ↑↑↑ *******************************
        'Call OutputResultToSheet(ctmList(II).Name, rstWorksheet)
    
    Next
    
    '* ISSUE_NO.625 sunyi 2018/04/25 start
    '* 連続移動型の場合、表示期間外のデータを削除する
        If dispTerm < 0 Then
    
            '***********************************
            '* 表示期間開始日時を取得
            '* →現時刻から表示期間を引いた日時
            '***********************************
            prevDate = Now
            prevTime = DateAdd("s", dispTerm, prevDate)
    
            '* 表示期間外のデータを削除
            WriteLog ("GetMissionInfoAll() Call DeleteOutrangeData() Sheet:" & ResultSheetName & " EndDate:" & prevTime & " ColTime:" & "B1")
            Call DeleteOutrangeData(Workbooks(thisBookName).Worksheets(ResultSheetName), prevTime, "B1")
    
    End If
    '* ISSUE_NO.625 sunyi 2018/04/25 end
    
    '* 2016.07.21 Add ↓↓↓ *******************************
    '* 旧resultシートを削除し、新resultシートの名称を変更する
''    Application.DisplayAlerts = False
''    Workbooks(thisBookName).Worksheets(ResultSheetName).Delete
''    Application.DisplayAlerts = True
''    rstWorksheet.Name = ResultSheetName
    '* 2016.07.21 Add ↑↑↑ *******************************
    
'* ISSUE_NO.625 Add ↓↓↓ *******************************
    ' ディレクトリを取得
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
    If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
    'ミッションIDがない場合(find分岐)
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
        pos = InStrRev(resultSuccess(1), "\")
        PathName = Left(resultSuccess(1), pos)
    
        FileName = PathName + ResultSheetName + ".txt"
    
        If Dir(FileName) <> "" Then
            '* ISSUE_NO.664 sunyi 2018/04/27 start
            '* resultシートの重複データを削除する
            'endData = GetCollectStartDT(Workbooks(thisBookName).Worksheets(MainSheetName))
    
            'Call DeleteOutrangeData1(Workbooks(thisBookName).Worksheets(ResultSheetName), endData)
            Call DeleteOutrangeData1(Workbooks(thisBookName).Worksheets(ResultSheetName), startTime)
            '* ISSUE_NO.664 sunyi 2018/04/27 end
            Call AddCTMInfoToSheetFromCsv1(ResultSheetName, 0#, FileName)
        End If
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
    End If
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
'* ISSUE_NO.625 Add ↑↑↑ *******************************
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
    '* resultシートをRECEIVE TIMEに従いソートする
    With currWorksheet
        Set srchRange = .Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = srchRange.End(xlDown)
        Set lastRange = .Cells(workRange.Row, lastRange.Column)
        Set workRange = .Range(srchRange, lastRange)
        workRange.Sort Key1:=.Range("B1"), _
                     Order1:=xlDescending, _
                     Header:=xlGuess
    End With
                
    '前回更新日時セット
    Set srchRange = Workbooks(thisBookName).Worksheets(MainSheetName).Cells.Find(What:="前回起動", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd")
        srchRange.Offset(0, 2).Value = Format(Now, "hh:mm:ss")
    End If
    '*ISSUE_NO.752 Sunyi 2018/06/25 Start
    '選択シートを戻す
    actWorksheet.Activate
    '*ISSUE_NO.752 Sunyi 2018/06/25 End
End Sub


'* ISSUE_NO.625 Add ↓↓↓ *******************************
'***********************************************
'* resultシートから表示期間外のデータを削除する
'***********************************************
Public Sub DeleteOutrangeData1(currWorksheet As Worksheet, endData As Double)
    Dim workRange       As Range
    Dim lastRange       As Range
    Dim srchRange       As Range
    Dim aaa             As Variant
    Dim II              As Long
    Dim startRow        As Integer
    Dim EndRow          As Long
    Dim startTimeTest   As Double
'    Application.ScreenUpdating = False
    
    startRow = -1
    EndRow = -1
    
    With currWorksheet
        Dim i As Long
        '* ISSUE_NO.686 sunyi 2018/05/09 start
        '* データない時、シートをクリアする
        Set srchRange = currWorksheet.Range("A2")
        If srchRange.Value <> "" Then
        '* ISSUE_NO.686 sunyi 2018/05/09 end
            For i = currWorksheet.Range("B1").End(xlDown).Row To 2 Step -1
                With currWorksheet.Cells(i, "B")
                  If _
                    GetUnixTime(.Value) > endData Then
                    .EntireRow.Delete
                  End If
                End With
            Next i
        '* ISSUE_NO.686 sunyi 2018/05/09 start
        '* データない時、シートをクリアする
        End If
        '* ISSUE_NO.686 sunyi 2018/05/09 end
    End With
    
'    Application.ScreenUpdating = True
    
End Sub
'* ISSUE_NO.625 Add ↑↑↑ *******************************

'* ISSUE_NO.625 Add ↓↓↓ *******************************
'***********************************************
'* resultシートから表示期間外のデータを削除する
'***********************************************
Public Sub DeleteOutrangeData2(currWorksheet As Worksheet)
    Dim workRange       As Range
    Dim lastRange       As Range
    Dim srchRange       As Range
    Dim aaa             As Variant
    Dim II              As Long
    Dim startRow        As Integer
    Dim EndRow          As Long
    Dim startTimeTest   As Double
    Dim startD          As Date
    Dim startDT         As Double
    Dim nowDT         As Double
'    Application.ScreenUpdating = False
    
    startRow = -1
    EndRow = -1
    
    nowDT = GetUnixTime(Format(Now, "yyyy/mm/dd hh:mm:ss"))
    
    With Workbooks(thisBookName).Worksheets(MainSheetName)
        startD = Format(.Range("H3").Value, "yyyy/mm/dd hh:mm:ss")
        startDT = GetUnixTime(startD)
    End With
    
    If nowDT >= startDT Then
        With currWorksheet
            Dim i As Long
            '* ISSUE_NO.686 sunyi 2018/05/09 start
            '* データない時、シートをクリアする
            Set srchRange = currWorksheet.Range("A2")
            If srchRange.Value <> "" Then
            '* ISSUE_NO.686 sunyi 2018/05/09 end
                For i = currWorksheet.Range("B1").End(xlDown).Row To 2 Step -1
                    With currWorksheet.Cells(i, "B")
                        .EntireRow.Delete
                    End With
                Next i
            '* ISSUE_NO.686 sunyi 2018/05/09 start
            '* データない時、シートをクリアする
            End If
            '* ISSUE_NO.686 sunyi 2018/05/09 end
        End With
    End If
    
'    Application.ScreenUpdating = True
    
End Sub
'* ISSUE_NO.625 Add ↑↑↑ *******************************


'* ISSUE_NO.625 Add ↓↓↓ *******************************
'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Public Sub AddCTMInfoToSheetFromCsv1(currSheetName As String, dispTerm As Double, csvFile As String)
    
    Dim currWorksheet   As Worksheet
    Dim oldWorksheet   As Worksheet
    
    Dim startRange      As Range
    Dim lastRange       As Range
    
    Dim pastAddress     As String
    
    Dim buf As String

    Dim lineCount As Integer
    
    Set oldWorksheet = ActiveSheet
    '* 出力先シートの取得
    Set currWorksheet = Worksheets(currSheetName)
    currWorksheet.Activate
    
    With currWorksheet
        
        Dim FileSize As Long
        FileSize = FileLen(csvFile)

        If FileSize > 0 Then
            '* 書き出し位置の設定
            Set startRange = .Cells(2, 1)
            If IsEmpty(startRange.Value) Then
                Set startRange = .Cells(1, 1)
            Else
                Set startRange = startRange.End(xlDown)
            End If
            
            pastAddress = "A" & (startRange.Row + 1)
                    
            With ActiveSheet.QueryTables.Add(Connection:="Text;" + csvFile, Destination:=Range(pastAddress))
                .TextFileCommaDelimiter = True
                .TextFilePlatform = 65001
                .Refresh BackgroundQuery:=False
                .Delete
            End With

            ActiveSheet.UsedRange.Sort Key1:=Range("A2"), Order1:=xlDescending, Header:=xlYes
        End If

        
    End With
    
    oldWorksheet.Activate
    
End Sub
'* ISSUE_NO.625 Add ↑↑↑ *******************************


'***********************************************
'* resultシートから表示期間外のデータを削除する
'***********************************************
Public Sub DeleteOutrangeData(currWorksheet As Worksheet, endDate As Date, colTime As String)
    Dim workRange       As Range
    Dim lastRange       As Range
    Dim srchRange       As Range
    Dim aaa             As Variant
    Dim II              As Long
    Dim startRow        As Integer
    Dim EndRow          As Long
    
    Dim dataBeginRange       As Range
    Dim dataLastRange        As Range
    Dim dataArray As Variant
    Dim dataText As String
    Dim III              As Long
    Dim IIII              As Long
    
'    Application.ScreenUpdating = False
    
    startRow = -1
    EndRow = -1
    
    With currWorksheet
    
        
        Set dataBeginRange = .Range(colTime)
        Set dataLastRange = dataBeginRange.End(xlToRight).End(xlDown)
        WriteLog ("DeleteOutrangeData() Sheet:" & currWorksheet.name & " EndDate:" & endDate & " ColTime:" & colTime & vbTab & " DataRow:" & dataLastRange.Row & " DataColumn:" & dataLastRange.Column)
        WriteLog ("DeleteOutrangeData() Sheet:" & currWorksheet.name & " EndDate:" & endDate & " ColTime:" & colTime & vbTab & " Original Sheet.")
        For II = 0 To dataLastRange.Row - 1
            If dataBeginRange.Offset(II, 0).Value = "" Then
                Exit For
            End If
            WriteLogSimple ((II + 1) & " " & dataBeginRange.Offset(II, 0).Value)
        Next
    
        '* ISSUE_NO.686 sunyi 2018/05/09 start
        '* データない時、シートをクリアする
        Set srchRange = currWorksheet.Range("A2")
        If srchRange.Value <> "" Then
            WriteLog ("DeleteOutrangeData() Sheet:" & currWorksheet.name & " EndDate:" & endDate & " ColTime:" & colTime & vbTab & " [A2]Cell is [" & srchRange.Value & "].")
        '* ISSUE_NO.686 sunyi 2018/05/09 end
            Set srchRange = .Range(colTime)
            Set lastRange = srchRange.End(xlToRight).End(xlDown)
            EndRow = lastRange.Row
            For II = 0 To EndRow - 1
                If srchRange.Offset(II, 0).Value < endDate And startRow < 0 Then
                    startRow = srchRange.Offset(II, 0).Row
                    Exit For
                End If
            Next
            
            WriteLog ("DeleteOutrangeData() Sheet:" & currWorksheet.name & " EndDate:" & endDate & " ColTime:" & colTime & vbTab & " [startRow] is [" & startRow & "].")
            WriteLog ("DeleteOutrangeData() Sheet:" & currWorksheet.name & " EndDate:" & endDate & " ColTime:" & colTime & vbTab & " [EndRow] is [" & EndRow & "].")
            '* 表示期間内のデータがなければ処理中断
            If startRow < 0 Then Exit Sub
    
            Set workRange = .Range(startRow & ":" & EndRow)
            workRange.Delete
            WriteLog ("DeleteOutrangeData() Sheet:" & currWorksheet.name & " EndDate:" & endDate & " ColTime:" & colTime & vbTab & " workRange.Delete[" & startRow & ":" & EndRow & "].")
            
            WriteLog ("DeleteOutrangeData() Sheet:" & currWorksheet.name & " EndDate:" & endDate & " ColTime:" & colTime & vbTab & " Modified Sheet.")
            Set dataBeginRange = .Range(colTime)
            Set dataLastRange = dataBeginRange.End(xlToRight).End(xlDown)
            For II = 0 To dataLastRange.Row - 1
                If dataBeginRange.Offset(II, 0).Value = "" Then
                    Exit For
                End If
                WriteLogSimple ((II + 1) & " " & dataBeginRange.Offset(II, 0).Value)
            Next
            
        '* ISSUE_NO.686 sunyi 2018/05/09 start
        '* データない時、シートをクリアする
        Else
            WriteLog ("DeleteOutrangeData() Sheet:" & currWorksheet.name & " EndDate:" & endDate & " ColTime:" & colTime & vbTab & " [A2]Cell is NULL.")
        End If
        '* ISSUE_NO.686 sunyi 2018/05/09 end

    End With
    
'    Application.ScreenUpdating = True
    
End Sub

'****************************************************
'* 収集したCTM情報をresultシートに転記する
'****************************************************
Public Sub OutputResultToSheet(currSheetName As String, rstWorksheet As Worksheet)
    Dim srcWorksheet        As Worksheet
    Dim prevWorksheet       As Worksheet
    Dim actWorksheet        As Worksheet
    Dim ws                  As Worksheet
    Dim srchRange           As Range
    Dim lastRange           As Range
    Dim workRange           As Range
    Dim copyRange           As Range
    Dim destRange           As Range
    Dim destRangeStart      As Range
    Dim startRange          As Range
    Dim ctmList()           As CTMINFO
    Dim II                  As Long
    Dim pasteArray()        As Variant
    Dim wsFlag              As Boolean

    '* 古いresultシート
    Set prevWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
    '* 該当CTM情報シート
    Set srcWorksheet = Workbooks(thisBookName).Worksheets(currSheetName)
    
    '* 2016.07.21 Add ↓↓↓ *******************************
    '* 旧resultシートからタイトル行をコピーする
    Set copyRange = prevWorksheet.Range("1:1")
    Set destRange = rstWorksheet.Range("1:1")
    copyRange.Copy Destination:=destRange
    '* 2016.07.21 Add ↑↑↑ *******************************

    '* 新resultシートにデータを複写する
    Set srchRange = srcWorksheet.Range("A2")
    '* 複写元にデータがあれば 2016.07.22 Add
    If srchRange.Offset(0, 0).Value <> "" Then
        Set lastRange = srchRange.Offset(0, 0).End(xlToRight)
        Set lastRange = lastRange.Offset(0, 0).End(xlDown)
        Set copyRange = srcWorksheet.Range(srchRange.Cells, lastRange.Cells)
        
        Set destRangeStart = rstWorksheet.Range("A2")
        If destRangeStart.Value <> "" Then
            Set destRangeStart = rstWorksheet.Range("A1").End(xlDown).Offset(1, 0)
        End If
        
        '* 2016.07.21 Change ↓↓↓ *******************************
        '* データ張り込み先の範囲を複写元の範囲に合わせて設定
        Set destRange = rstWorksheet.Range(destRangeStart.Offset(0, 0), destRangeStart.Offset(lastRange.Row, lastRange.Column - 1))
        Set destRange = destRange.Offset(0, 1)
        '* 2016.07.21 Change ↑↑↑ *******************************
        copyRange.Copy Destination:=destRange
        
        '* CTM名を生成する
        II = lastRange.Offset(0, 0).Row - srchRange.Row
        ReDim pasteArray(II)
        For II = 0 To UBound(pasteArray)
            pasteArray(II) = currSheetName
        Next
        
        '* CTM名を貼り付ける
        Set lastRange = destRangeStart.Offset(UBound(pasteArray), 0)
        Set copyRange = rstWorksheet.Range(destRangeStart.Cells, lastRange.Cells)
        copyRange = pasteArray
        
        '* RECEIVE TIME列の書式設定
        With rstWorksheet
            Set workRange = rstWorksheet.Range(.Range("B2"), .Range("B2").End(xlDown).Cells)
            workRange.NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
            workRange.EntireColumn.AutoFit
        End With
    End If
    
End Sub

'**********************
'* 収集開始日時の取得
'* →返り値はUNIX TIME
'**********************
Public Function GetCollectStartDT(currWorksheet As Worksheet) As Double
    Dim srcWorksheet        As Worksheet
    Dim startTime           As Double
    Dim startD              As Date
    Dim startT              As Date
    Dim ctmList()           As CTMINFO
    Dim II                  As Integer
    Dim recieveTimeArray()  As Double
    Dim iFirstFlag          As Boolean
    Dim recieveTime         As Variant
    Dim startRange          As Range
    Dim srchRange           As Range
    Dim rtIndex             As Integer
    Dim currDate            As Date
    Dim dateStr             As String
    
    '**********************************
    '* 全CTMシートの最新時刻を取得する
    '**********************************
    '* CTMリストを取得する
    Call GetCTMList(currWorksheet.name, ctmList)
    
    
    '* CTM情報一覧をシートから取得
    iFirstFlag = True
    For Each srcWorksheet In Worksheets
    
        '* CTM情報一覧に存在するシートのみを処理対象とする
        For II = 0 To UBound(ctmList)
            If (ctmList(II).name = srcWorksheet.name) Then Exit For
        Next
        If II <= UBound(ctmList) Then

            '* 最上位のRECIEVE TIMEを取得する
            Set startRange = srcWorksheet.Range("A2")
            For II = 0 To 100
                recieveTime = startRange.Offset(II, 0).Value
                If recieveTime <> "" Then
                    Exit For
                End If
            Next
            '* 100行検索しても見つからなければ処理中断＝データ無
            If II > 100 Then
                Exit For
            End If
            
            '* 取得した値が日付形式ならば
            dateStr = Format(recieveTime, "yyyy/MM/dd hh:mm:ss")
            currDate = CDate(dateStr)
            If IsDate(currDate) Then
            
                If iFirstFlag Then
                    rtIndex = 0
                    ReDim recieveTimeArray(rtIndex)
                    iFirstFlag = False
                Else
                    rtIndex = UBound(recieveTimeArray) + 1
                    ReDim Preserve recieveTimeArray(rtIndex)
                End If
                
                recieveTimeArray(rtIndex) = GetUnixTime(currDate)
                
            Else
                Exit For
            End If

        End If
    
    Next
        
    If iFirstFlag Then
        recieveTime = 0#
    Else
        '* 取得した時刻群の最小の時刻を取得
        recieveTime = Application.WorksheetFunction.Min(recieveTimeArray)
    End If

    '****************************************
    '* paramシートから収集開始日時を取得する
    '****************************************
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(srchRange.Offset(0, 1).Value)
            startT = TimeValue(Format(srchRange.Offset(0, 2).Value, "hh:mm:ss"))
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            GetCollectStartDT = -1
            Exit Function
        End If
        startTime = GetUnixTime(startD + startT)

    End With
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    With currWorksheet
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "ON" Then
                GetCollectStartDT = startTime
            Else
                GetCollectStartDT = Application.WorksheetFunction.Max(recieveTime + 1000, startTime)
            End If
        End If
    
    End With
    
End Function

''' BugFix AIS_TEMP.130 2019/06/17 yakiyama start.
Public Function GetCollectStartDTForMissionStd(currWorksheet As Worksheet) As Double
    Dim startTime           As Double
    Dim startD              As Date
    Dim startT              As Date
    Dim srchRange           As Range
    
    '****************************************
    '* paramシートから収集開始日時を取得する
    '****************************************
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(srchRange.Offset(0, 1).Value)
            startT = TimeValue(Format(srchRange.Offset(0, 2).Value, "hh:mm:ss"))
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            GetCollectStartDTForMissionStd = -1
            Exit Function
        End If
        startTime = GetUnixTime(startD + startT)

    End With
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    With currWorksheet
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            GetCollectStartDTForMissionStd = startTime
        End If
    
    End With
    
End Function
''' BugFix AIS_TEMP.130 2019/06/17 yakiyama end.

'*************************************************************************
'* 取得したJSON情報をデコードしCTM情報およびエレメント情報として展開する
'* →ミッションIDによる取得
'* ※VBA-JSON用
'*************************************************************************
Public Function GetCTMInfoNew(objJSON As Object, ctmList() As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim JsonText        As String
    Dim Parsed          As Object
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    Dim LL              As Integer
    Dim ctmIndex        As Integer
    Dim eleIndex        As Integer
    Dim aryIndex        As Integer
    Dim aryIndex2       As Integer
    Dim CtmId           As String
    Dim CtmName         As String
    Dim ctmNum          As String
    Dim receiveTime     As String
    Dim ctmArray        As Variant
    Dim eleArray        As Variant
    Dim keysArray       As Variant
    Dim keysArray2      As Variant
    Dim eleInfo         As Variant
    Dim wValue          As String
    Dim iFirstFlag      As Boolean
    Dim recno           As Integer
    
    iFirstFlag = True
    
'    JsonText = ReadAll("C:\Users\FOA\Documents\00_work_by_fukushima\work\template\VBA-json_test\test.json")
    
    ' Parse json to Dictionary
    ' "values" is parsed as Collection
    ' each item in "values" is parsed as Dictionary
'    Set Parsed = JsonConverter.ParseJson(JsonText)
    
'    Call DumpCollection(Parsed)

'    Debug.Print "CTMの種類数:" & Parsed.Count
    
    '* CTMの種類分ループ
    recno = 0
    For II = 1 To objJSON.Count
    
        '* CTM ID取得
        CtmId = objJSON.Item(II).Item("id")
        '* CTM名称取得
        CtmName = objJSON.Item(II).Item("name")
        '* CTM件数取得
        ctmNum = objJSON.Item(II).Item("num")
'        Debug.Print "CTM ID:" & ctmID & ", 名称:" & ctmName & ", 件数:" & ctmNum
        
        '* 対象CTMを確定する
        For ctmIndex = 0 To UBound(ctmList)
            If ctmList(ctmIndex).ID = CtmId Then
                Exit For
            End If
        Next
        
        If ctmIndex <= UBound(ctmList) Then
            
            '* 取得CTMのオブジェクトを取得
            Set ctmArray = objJSON.Item(II).Item("ctms")
            
            '* CTMの件数分ループ
            For JJ = 1 To ctmArray.Count
                
                If ctmValue(0).ID = "" Then
                    aryIndex = 0
                Else
                    aryIndex = UBound(ctmValue) + 1
                    ReDim Preserve ctmValue(aryIndex)
                End If
                ReDim ctmValue(aryIndex).Element(0)
                ctmValue(aryIndex).Element(0).name = ""
            
                '* 件数カウンタアップ
                recno = recno + 1
                
                '* RECEIVE TIMEの取得
                receiveTime = ctmArray.Item(JJ).Item("RT")
'                Debug.Print "RECEIVE TIME:" & receiveTime
                
                '* CTM情報の取得
                ctmValue(aryIndex).name = ctmList(ctmIndex).name
                ctmValue(aryIndex).ID = ctmList(ctmIndex).ID
                ctmValue(aryIndex).RecvTime = receiveTime
    
                '* エレメントオブジェクトの取得
                Set eleArray = ctmArray.Item(JJ).Item("EL")
                
                '* エレメントIDリストの取得
                keysArray = eleArray.Keys
                For KK = 0 To UBound(keysArray)
    
'                    Debug.Print "エレメントID:" & keysArray(KK)
                    
                    '* 対象エレメントを確定する
                    For eleIndex = 0 To UBound(ctmList(ctmIndex).Element)
                        If ctmList(ctmIndex).Element(eleIndex).ID = keysArray(KK) Then
                            Exit For
                        End If
                    Next
                    
                    If eleIndex <= UBound(ctmList(ctmIndex).Element) Then
    
                        aryIndex2 = UBound(ctmValue(aryIndex).Element)
                        If ctmValue(aryIndex).Element(aryIndex2).name <> "" Then
                            aryIndex2 = aryIndex2 + 1
                            ReDim Preserve ctmValue(aryIndex).Element(aryIndex2)
                        End If
                            
                        '* エレメント情報オブジェクトの取得
                        Set eleInfo = eleArray.Item(keysArray(KK))
        
                        '* エレメント情報(型、値、(Bulky指定ならばファイル名))
                        keysArray2 = eleInfo.Keys
                        For LL = 0 To UBound(keysArray2)
        
                            wValue = eleInfo.Item(keysArray2(LL))
'                            Debug.Print keysArray2(LL) & ":" & wValue
        
                            '* エレメント情報の取得
                            ctmValue(aryIndex).Element(aryIndex2).name = ctmList(ctmIndex).Element(eleIndex).name
                            ctmValue(aryIndex).Element(aryIndex2).ID = ctmList(ctmIndex).Element(eleIndex).ID
                            Select Case keysArray2(LL)
                                Case "T"
                                    ctmValue(aryIndex).Element(aryIndex2).Type = wValue
                                Case "V"
                                    ctmValue(aryIndex).Element(aryIndex2).Value = wValue
                                Case "FN"
                                    ctmValue(aryIndex).Element(aryIndex2).File = wValue
                            End Select
                            '* 取得した値が空白ならば、半角空白をセットしておく
                            If ctmValue(aryIndex).Element(aryIndex2).Value = "" Then ctmValue(aryIndex).Element(aryIndex2).Value = " "
                        
                        Next
                    
                    End If
                    
                Next
            
            Next
        
        End If
        
    Next
    
    GetCTMInfoNew = recno

End Function

'*************************************************************************
'* 取得したJSON情報をデコードしCTM情報およびエレメント情報として展開する
'* →ミッションIDによる取得
'*************************************************************************
Public Function GetCTMInfo(objJSON As Object, ctmList() As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim recno           As Integer
    Dim II              As Integer
    Dim rec             As Object
    Dim rec2            As Object
    Dim rec3            As Object
    Dim rec4            As Object
    Dim aryIndex        As Integer
    Dim aryIndex2       As Integer
    Dim ctmIndex        As Integer
    Dim workElm         As ELMINFO
    Dim iFirstFlag      As Boolean
    
    iFirstFlag = True
    
    ' 件数カウンタ初期化
    recno = 0
    
    '* CTM種別数分ループ
    For Each rec In objJSON
    
        '* 対象CTMを確定する
        For II = 0 To UBound(ctmList)
            If ctmList(II).ID = GetJSONValue(rec, "id") Then
                ctmIndex = II
                Exit For
            End If
        Next
        
        If II <= UBound(ctmList) Then
        
            '* CTM取得件数分ループ
            II = rec.num
            For Each rec2 In rec.ctms
    
                If ctmValue(0).ID = "" Then
                    aryIndex = 0
                Else
                    aryIndex = UBound(ctmValue) + 1
                    ReDim Preserve ctmValue(aryIndex)
                End If
                ReDim ctmValue(aryIndex).Element(0)
                ctmValue(aryIndex).Element(0).name = ""
                
                '* CTM情報の取得
                ctmValue(aryIndex).name = ctmList(ctmIndex).name
                ctmValue(aryIndex).ID = ctmList(ctmIndex).ID
                ctmValue(aryIndex).RecvTime = GetJSONValue(rec2, "RT")
                                                                                                                                                                                                                        
                '* 件数カウンタアップ
                recno = recno + 1
                
                '* エレメント数分の値取得
                Set rec3 = CallByName(rec2, "EL", VbGet)
                For II = 0 To UBound(ctmList(ctmIndex).Element)
                
                    workElm = ctmList(ctmIndex).Element(II)
                    
                    Set rec4 = CallByName(rec3, workElm.ID, VbGet)
                    
                    aryIndex2 = UBound(ctmValue(aryIndex).Element)
                    If ctmValue(aryIndex).Element(aryIndex2).name <> "" Then
                        aryIndex2 = aryIndex2 + 1
                        ReDim Preserve ctmValue(aryIndex).Element(aryIndex2)
                    End If
                    
                    '* エレメント情報の取得
                    ctmValue(aryIndex).Element(aryIndex2).name = workElm.name
                    ctmValue(aryIndex).Element(aryIndex2).ID = workElm.ID
                    ctmValue(aryIndex).Element(aryIndex2).Type = CInt(GetJSONValue(rec4, "T"))
                    On Error Resume Next
                    ctmValue(aryIndex).Element(aryIndex2).Value = GetJSONValue(rec4, "V")
                    If ctmValue(aryIndex).Element(aryIndex2).Value = "" Then ctmValue(aryIndex).Element(aryIndex2).Value = " "
                    
                Next
                
            Next
        
        End If
        
    Next
    
    GetCTMInfo = recno
    
End Function

'*************************************************************************
'* 取得したJSON情報をデコードしCTM情報およびエレメント情報として展開する
'* →CTM IDによる取得
'*************************************************************************
Public Function GetCTMInfoForCTM(objJSON As Object, ctmList As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim recno           As Integer
    Dim II              As Integer
    Dim rec             As Object
    Dim rec2            As Object
    Dim rec3            As Object
    Dim rec4            As Object
    Dim aryIndex        As Integer
    Dim aryIndex2       As Integer
    Dim ctmIndex        As Integer
    Dim workElm         As ELMINFO
    Dim iFirstFlag      As Boolean
    
    iFirstFlag = True
    
    ' 件数カウンタ初期化
    recno = 0
    
    '* CTM取得件数分ループ
    For Each rec2 In objJSON

        If ctmValue(0).ID = "" Then
            aryIndex = 0
        Else
            aryIndex = UBound(ctmValue) + 1
            ReDim Preserve ctmValue(aryIndex)
        End If

        ReDim ctmValue(aryIndex).Element(0)
        ctmValue(aryIndex).Element(0).name = ""
        
        '* CTM情報の取得
        ctmValue(aryIndex).name = ctmList.name
        ctmValue(aryIndex).ID = ctmList.ID
        ctmValue(aryIndex).RecvTime = GetJSONValue(rec2, "RT")

        '* 件数カウンタアップ
        recno = recno + 1
        
        '* エレメント数分の値取得
        Set rec3 = CallByName(rec2, "EL", VbGet)
        For II = 0 To UBound(ctmList.Element)
        
            workElm = ctmList.Element(II)
            
            Set rec4 = CallByName(rec3, workElm.ID, VbGet)
            
            aryIndex2 = UBound(ctmValue(aryIndex).Element)
            If ctmValue(aryIndex).Element(aryIndex2).name <> "" Then
                aryIndex2 = aryIndex2 + 1
                ReDim Preserve ctmValue(aryIndex).Element(aryIndex2)
            End If
            
            '* エレメント情報の取得
            ctmValue(aryIndex).Element(aryIndex2).name = workElm.name
            ctmValue(aryIndex).Element(aryIndex2).ID = workElm.ID
            ctmValue(aryIndex).Element(aryIndex2).Type = CInt(GetJSONValue(rec4, "T"))
            On Error Resume Next
            ctmValue(aryIndex).Element(aryIndex2).Value = GetJSONValue(rec4, "V")
            If ctmValue(aryIndex).Element(aryIndex2).Value = "" Then ctmValue(aryIndex).Element(aryIndex2).Value = " "
            
        Next
        
    Next
        
    GetCTMInfoForCTM = recno
    
End Function

'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Public Sub OutputCTMInfoToSheet(currSheetName As String, ctmValue() As CTMINFO, termFlag As Boolean)
    Dim II              As Integer
    Dim JJ              As Long
    Dim KK              As Integer
    Dim currWorksheet   As Worksheet
    Dim writeIndex      As Integer
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim delRange        As Range
    Dim writeRange      As Range
    Dim receiveTime     As Double
    Dim dtRecvL         As Long
    Dim dtRecv          As Date
    Dim ms              As Integer
    Dim strWork         As String
    Dim nowTime         As Double
    Dim endTime         As Double
    Dim writeArray()    As Variant
    Dim writeArray2()   As Variant
    Dim iFirstFlag      As Boolean
    Dim readIndex       As Integer
    Dim latestTime      As Double
    Dim prevDate        As Date
    Dim prevTime        As Date
    
    Dim eleNum          As Integer
    Dim ctmNum          As Integer
    
    '* 出力先シートの取得
    Set currWorksheet = Workbooks(thisBookName).Worksheets(currSheetName)
    
    With currWorksheet
        
        '* 書き出し位置の設定
        Set startRange = .Cells(2, 1)
        
        '* 書き出し位置に現在記載されている最新時刻を取得
        ' 2017.02.21 Change **********************
'        latestTime = GetUnixTime(startRange.Offset(0, 0).Value)
        latestTime = GetUnixTime(startRange.Offset(0, 0).Value2)
        ' 2017.02.21 Change **********************
        
        writeIndex = 0
        
'        Application.ScreenUpdating = False
        
        '* 2016.07.21 Add ↓↓↓ *******************************
        '* 対象CTM件数と対象エレメント数を取得する
        iFirstFlag = True
        ctmNum = 0
        For JJ = 0 To UBound(ctmValue)
        
            If currSheetName = ctmValue(JJ).name Then
                ctmNum = ctmNum + 1
                If iFirstFlag Then
                    eleNum = UBound(ctmValue(JJ).Element)
                    iFirstFlag = False
                End If
            End If
            
        Next
        WriteLog ("OutputCTMInfoToSheet() Sheet:" & currSheetName & " CtmNum:" & ctmNum & " termFlag:" & termFlag)
        
        '* 取得した情報で出力用の配列を確保する
        ReDim writeArray(ctmNum, eleNum + 2)
        '* 2016.07.21 Add ↑↑↑*******************************
        
'        iFirstFlag = True
        For JJ = 0 To UBound(ctmValue)
        
            If currSheetName = ctmValue(JJ).name Then
            
                receiveTime = CDbl(ctmValue(JJ).RecvTime)
                dtRecvL = CLng(receiveTime / 1000)
                
                '* 処理時刻が記載最新時刻以前ならば処理終了
'                Debug.Print "receiveTime:" + Format(receiveTime) + ", latestTime:" + Format(latestTime)
                If (receiveTime > (latestTime + 1#)) Then
'                    Debug.Print "receiveTime:OK!"
    
                    ms = CInt(Right(ctmValue(JJ).RecvTime, 3))
                    strWork = GetDateTime(receiveTime) + "." + Format(ms, "000")
                    
                    '* 2016.07.21 Delete ↓↓↓ *******************************
'                    If iFirstFlag Then
'                        readIndex = 0
'                        ReDim writeArray(UBound(ctmValue(JJ).Element) + 2, readIndex)
'                        iFirstFlag = False
'                    Else
'                        readIndex = UBound(writeArray, 2) + 1
'                        ReDim Preserve writeArray(UBound(ctmValue(JJ).Element) + 2, readIndex)
'                    End If
                    '* 2016.07.21 Delete ↑↑↑ *******************************
                    
                    '* RECIEVE TIMEをセット
                    writeArray(writeIndex, 0) = strWork
                    
                    '* CTM NAMEをセット
    '                writeArray(1, readIndex) = ctmValue(JJ).Name
                    
                    For KK = 0 To UBound(ctmValue(JJ).Element)
                    
                        writeArray(writeIndex, KK + 1) = ctmValue(JJ).Element(KK).Value
                    
                    Next
                    
                    writeIndex = writeIndex + 1
                End If
            End If
            
            DoEvents
        
        Next
        
        '* 取得CTMがあれば取得CTMの差分出力
        If writeIndex <> 0 Then
        
            '* 書き出し位置の設定
            Set startRange = .Cells(2, 1)
        
            '* 書き込み先範囲の取得
            Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, UBound(ctmValue(0).Element) + 2))
            
            '* 配列の次元入れ替え
            '* 2016.07.21 Delete *******************************
            '* 直接配列を確保したため次元入れ替えは不要
            '* 2016.07.20 Change *******************************
'            ReDim writeArray2(UBound(writeArray, 2), UBound(writeArray, 1))
'            For II = 0 To UBound(writeArray, 1)
'                For JJ = 0 To UBound(writeArray, 2)
'                    writeArray2(JJ, II) = writeArray(II, JJ)
'                Next
'            Next
'            writeArray2 = WorksheetFunction.Transpose(writeArray)
            '* 2016.07.20 Change *******************************
            
            '* 指定範囲に書き込み
            writeRange.Insert Shift:=xlShiftDown
            Set startRange = .Cells(2, 1)
            Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, UBound(ctmValue(0).Element) + 2))
            writeRange = writeArray
            
            '* RECIVE TIMEの書式を設定
            Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, 0))
            writeRange.NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
            '* カラム幅を調整
            writeRange.EntireColumn.AutoFit
        
        End If
            
'        Application.ScreenUpdating = True
        
    End With
    
End Sub

'***********************************************
'* メインシートにあるミッション情報を取得する
'***********************************************
Public Sub GetCTMList(currSheetName As String, ctmList() As CTMINFO)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim CTMRange        As Range
    Dim elmRange        As Range
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim aryIndex        As Integer      ' CTM情報インデックス
    Dim aryIndex2       As Integer      ' エレメント情報インデックス
    Dim iFirstFlag      As Boolean      ' 配列初回フラグ
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    
    iFirstFlag = True
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim ctmList(0)
    
    With currWorksheet
    
'        .Activate
        
        '* CTM NAMEの項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        
        For II = 0 To MaxCTMNumber Step 2
            Set CTMRange = startRange.Offset(II, 0)
            
            If CTMRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            If iFirstFlag Then
                aryIndex = 0
                ReDim ctmList(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(ctmList) + 1
                ReDim Preserve ctmList(aryIndex)
            End If
            ReDim ctmList(aryIndex).Element(0)
            ctmList(aryIndex).Element(0).name = ""
            
            '* CTM情報取得
            ctmList(aryIndex).name = CTMRange.Offset(0, 0).Value
            ctmList(aryIndex).ID = CTMRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = CTMRange.Offset(0, 1)
            For JJ = 0 To MaxELMNumber
                If elmRange.Offset(0, JJ).Value = "" Then
                    Exit For
                End If
                
                aryIndex2 = UBound(ctmList(aryIndex).Element)
                If ctmList(aryIndex).Element(aryIndex2).name <> "" Then
                    aryIndex2 = aryIndex2 + 1
                    ReDim Preserve ctmList(aryIndex).Element(aryIndex2)
                End If
                ctmList(aryIndex).Element(aryIndex2).name = elmRange.Offset(0, JJ).Value
                ctmList(aryIndex).Element(aryIndex2).ID = elmRange.Offset(1, JJ).Value
            Next
            
                
        Next
    
    End With
    
End Sub





