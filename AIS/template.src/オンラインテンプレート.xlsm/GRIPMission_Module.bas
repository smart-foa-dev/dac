Attribute VB_Name = "GRIPMission_Module"

'*******************************************
'*　GRIPミッション取込
'*******************************************
Public Function GetGripMission() As Integer

    Dim csvFile As String
    Dim wrkStart As String
    Dim wrkEnd As String
    Dim dspTerm As String
    Dim strtTime As Date
    Dim strFileName As String
    Dim wrkStr      As String
    
    Dim paramSheet  As Worksheets
    Dim workRange   As Range
    Dim firstFlag   As Boolean
    
    Dim mainWorksheet   As Worksheet
    Dim startTime   As Double
    Dim endD        As Date
    Dim endT        As Date
    Dim endTime     As Double
    Dim srchRange   As Range
    Dim missionID   As String
    
    Dim statusGrip  As String
    
    Dim gripMission As GripDataRetriever.GripDataRetriever
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim paramWorksheet  As Worksheet
    Dim CTMRange    As Range
    Dim period As String
    Dim onlineId As String
    
    Dim useProxy As Boolean
    Dim proxyUri As String
        
    GetGripMission = 0
    
    'GRIPミッション設定
    Set gripMission = New GripDataRetriever.GripDataRetriever
    
    '表示期間取得
    Set mainWorksheet = ThisWorkbook.Worksheets(MainSheetName)
    With mainWorksheet

        '*********************
        '* 収集開始日時を取得
        '*********************
        startTime = GetCollectStartDTstd(mainWorksheet)

        '*********************
        '* 収集終了日時を取得
        '*********************
        ' 2017.02.21 Change ↓↓↓**************
'        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            endD = srchRange.Offset(0, 1).Value
'            endT = srchRange.Offset(0, 2).Value
'        Else
'            endD = CDate("2020/12/31")
'            endT = CDate("23:59:59")
'        End If
'        If Not IsDate(endD + endT) Then
'            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
'            Exit Function
'        End If
'        endTime = GetUnixTime(endD + endT)
        endTime = GetCollectEndDTstd(mainWorksheet)
        ' 2017.02.21 Change ↑↑↑**************
                
        '*********************
        '* ミッションIDを取得
        '*********************
        missionID = ""
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "ミッションID項目が見つからないため処理を中断します。"
            GetGripMission = -1
            Exit Function
        End If
    
    End With
        
    ' 2017.02.21 Add ↓↓↓*****************
    wrkStart = CStr(startTime)
    wrkEnd = CStr(GetUnixTime(Now))
    onlineId = GetOnlineId()
    period = CStr(GetUpdatePeriod())
    collectEnd = CStr(endTime)
    ' 2017.02.21 Add ↑↑↑*****************
    
    '* paramシートから各種情報を取得
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With paramWorksheet
    
        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="GRIPサーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If

        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If

    End With
    
'    Set workRange = srchRange.Find(What:="問合開始日時", LookAt:=xlWhole)
'    If Not workRange Is Nothing Then
'        workRange.Offset(0, 1).Value = CStr(Format(strtTime, "YYYY/MM/DD hh:mm:ss"))
'    End If
'
'    Set workRange = srchRange.Find(What:="問合終了日時", LookAt:=xlWhole)
'    If Not workRange Is Nothing Then
'        workRange.Offset(0, 1).Value = CStr(Format(Now, "YYYY/MM/DD hh:mm:ss"))
'    End If

    '* 2016.08.05 Add ↓↓↓ **************************************************
    '* Gripへの状態問合せ
    Do
        statusGrip = gripMission.GetStatus()
        If statusGrip = "STATUS_IDLE" Then Exit Do
        
        Sleep 1000
        
        DoEvents
    Loop
    '* 2016.08.05 Add ↑↑↑ **************************************************
    
    'GRIPミッションの実行
    If useProxy = True Then
        csvFile = gripMission.GetGripDatatCsvOnlineFromProxy(useProxy, proxyUri, IPAddr, PortNo, missionID, wrkStart, wrkEnd, onlineId, period, collectEnd)
    Else
        'GRIPミッションの実行
        '*****************************
        ' 2017.02.21 Change ↓↓↓**************
        'csvFile = gripMission.GetGripDatatCsv(IPAddr, PortNo, missionID, wrkStart, wrkEnd)
        csvFile = gripMission.GetGripDatatCsvOnline_2(IPAddr, PortNo, missionID, wrkStart, wrkEnd, onlineId, period, collectEnd)
        ' 2017.02.21 Change ↑↑↑**************
        '*****************************
    End If
    
    
    'CTM取得件数設定 20161117 追加
    Set CTMRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="GRIP取得件数", LookAt:=xlWhole)
    If Not CTMRange Is Nothing Then
        CTMRange.Offset(0, 1).Value = iResult
    End If
    
    If csvFile = "" Then
        GetGripMission = -1
        
        'MsgBox "Gripミッション結果は有りません。", vbExclamation, "GetGripMission"
        Exit Function
    End If
    
    ' フォルダの存在確認
    If Dir(csvFile, vbDirectory) = "" Then
        GetGripMission = -1
        
        'MsgBox "指定のフォルダは存在しません。", vbExclamation, "GetGripMission"
        Exit Function
    End If

    ' 先頭のファイル名の取得
    strFileName = Dir(csvFile & "\*.*", vbNormal)
    ' ファイルが見つからなくなるまで繰り返す
    firstFlag = True
    Do While strFileName <> ""
        'CSVﾌｧｲﾙをCELLに書込む
        wrkStr = Replace(strFileName, ".csv", "")
        
        If firstFlag = True Then
            firstFlag = False
            Call CsvToCell(csvFile & "\" & strFileName, wrkStr)
        Else
            Call CsvToCellInsert(csvFile & "\" & strFileName, wrkStr)
        End If
        
''        *AISMM-75 sunyi 201901010 start
''        With Sheets(wrkStr)
''            Set srchRange = .Range("A1")
''            Set lastRange = srchRange.End(xlToRight)
''            Set workRange = srchRange.End(xlDown)
''            Set lastRange = .Cells(workRange.Row, lastRange.Column)
''            Set workRange = .Range(srchRange, lastRange)
''            workRange.Sort Key1:=.Range("B1"), _
''                         Order1:=xlDescending, _
''                         Header:=xlGuess
''        End With
''        *AISMM-75 sunyi 201901010 end
        
        '* AISTEMP No.102 sunyi 2018/11/22 start
'        Call NullDelete(wrkStr)
        '* AISTEMP No.102 sunyi 2018/11/22 end
        
        strFileName = Dir()
        
        GetGripMission = GetGripMission + 1
    Loop
    
End Function

Public Sub CsvToCell(argFileName As String, argSheetName As String) '
    
    Sheets(argSheetName).Cells.Clear

    With Sheets(argSheetName).QueryTables.Add(Connection:="TEXT;" & argFileName, Destination:=Sheets(argSheetName).Range("$A$1"))
'    With ActiveSheet.QueryTables.Add(Connection:= _
'        "TEXT;C:\Users\HelpMe\Desktop\GripClient_0612_2\grip_temp\2016-06-14-17-04-12-346\0_製品1生産実績_ルート1.csv", Destination:=Range("$A$1"))
        .name = "temp"
        .FieldNames = True
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .RefreshOnFileOpen = False
        .RefreshStyle = xlOverwriteCells    'xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
        .AdjustColumnWidth = False  'True
        .RefreshPeriod = 0

        .TextFilePromptOnRefresh = False
        .TextFilePlatform = 65001 'UTF8  SHIF-JIS 932
        .TextFileStartRow = 1
        .TextFileParseType = xlDelimited
        .TextFileTextQualifier = xlTextQualifierDoubleQuote
        .TextFileConsecutiveDelimiter = False
        .TextFileTabDelimiter = False
        .TextFileSemicolonDelimiter = False
        .TextFileCommaDelimiter = True
        .TextFileSpaceDelimiter = False
        '.TextFileColumnDataTypes = Array(1, 1, 1)
        .TextFileTrailingMinusNumbers = True
        .Refresh BackgroundQuery:=False
        .Delete
    End With
    
    '* 2016.08.03 Change ↓↓↓ **********************
    Sheets(argSheetName).Cells.Range("2:3").Delete
'    Sheets(argSheetName).Cells.Range("2:2").Clear
'    Sheets(argSheetName).Cells.Range("3:3").Delete
    '* 2016.08.03 Change ↑↑↑ **********************
    
End Sub


Public Sub CsvToCellInsert(argFileName As String, argSheetName As String)
    Dim intRow     As Integer
    Dim strRow        As String
    
    intRow = Sheets(argSheetName).Range("A3").End(xlDown).Row
    strRow = "A" & Trim(CStr(intRow + 1))
    
    With Sheets(argSheetName).QueryTables.Add(Connection:="TEXT;" & argFileName, Destination:=Sheets(argSheetName).Range(strRow))
'    With ActiveSheet.QueryTables.Add(Connection:= _
'        "TEXT;C:\Users\HelpMe\Desktop\GripClient_0612_2\grip_temp\2016-06-14-17-04-12-346\0_製品1生産実績_ルート1.csv", Destination:=Range("$A$1"))
        .name = "temp"
        .FieldNames = True
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .RefreshOnFileOpen = False
        .RefreshStyle = xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
        .AdjustColumnWidth = False  'True
        .RefreshPeriod = 0

        .TextFilePromptOnRefresh = False
        .TextFilePlatform = 65001 'UTF8  SHIF-JIS 932
        .TextFileStartRow = 1
        .TextFileParseType = xlDelimited
        .TextFileTextQualifier = xlTextQualifierDoubleQuote
        .TextFileConsecutiveDelimiter = False
        .TextFileTabDelimiter = False
        .TextFileSemicolonDelimiter = False
        .TextFileCommaDelimiter = True
        .TextFileSpaceDelimiter = False
        '.TextFileColumnDataTypes = Array(1, 1, 1)
        .TextFileTrailingMinusNumbers = True
        .Refresh BackgroundQuery:=False
        .Delete
    End With
    
    Sheets(argSheetName).Rows(intRow + 3).Delete
    Sheets(argSheetName).Rows(intRow + 2).Delete
    Sheets(argSheetName).Rows(intRow + 1).Delete
    
End Sub



'開始時刻の行削除
Public Sub NullDelete(argSheetName As String)
    Dim pos         As Integer
    Dim wrkVal
    
    wrkVal = Worksheets(argSheetName).Range("A65536").End(xlUp).Row
    For pos = wrkVal To 3 Step -1
        If Worksheets(argSheetName).Cells(pos, 2).Value = "" Then
            Worksheets(argSheetName).Rows(pos).Delete

        End If
    Next pos

End Sub
