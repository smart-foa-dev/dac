Attribute VB_Name = "Mail_Module"
Option Explicit

Public Sub testMail()
    Dim mailTile As String
    Dim mailBody As String
    Dim mailAddresses(2) As String
    
    mailTile = "title"
    mailBody = "body"
    mailAddresses(1) = "fwka1605.foa@gmail.com"
    mailAddresses(2) = "fwka1605.foa@gmail.com"
    
    Call sendMail("test", "test", mailAddresses)
    
End Sub


'**************************************
'*メール送信
'**************************************
Public Sub sendMail(mailTitle As String, mailBody As String, mailAddresses() As String)
    
    
    Dim srchRange As Range
    
    Dim iCounter As Integer
    
    Dim IPAddr As String
    Dim PortNo As String
    
    Dim SendUrlStr As String
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    Dim httpObj As Object
    
    Dim JsonObject As Object
    
    Dim GetString As String
    
    With Sheets(ParamSheetName)

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If

        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
        
    End With
        
        
    SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mail"
       
    If useProxy = False Then
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
    Else
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
        httpObj.setProxy 2, proxyUri
    End If
    
    
    Set JsonObject = New Dictionary
    
    JsonObject.Add "subject", mailTitle
    JsonObject.Add "message", mailBody
    
     JsonObject.Add "to", New Collection

    For iCounter = LBound(mailAddresses) To UBound(mailAddresses)
        JsonObject("to").Add mailAddresses(iCounter)
    Next
    
    httpObj.Open "POST", SendUrlStr, False
    httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
    httpObj.send JsonConverter.ConvertToJson(JsonObject)
    
    ' レスポンスコード（正常）
    'If httpObj.Status = 200 Then
    '    MsgBox "正常に終了しました"
    'End If

    

End Sub
