Attribute VB_Name = "Common_Module"
Option Explicit

Public Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Dim FinalTime           As Date
Dim TimerStopFlag       As Boolean

'* 本ブック名
Public thisBookName     As String
'* クローズ時の２重処理防止フラグ
Public quitFlag         As Boolean

Public Const MaxCTMNumber       As Integer = 1000       ' 最大CTM数
Public Const MaxELMNumber       As Integer = 1000       ' 最大エレメント数
Public Const LimitCTMNumber     As Long = 0             ' 取得CTM情報最大件数
Public Const DivideLimitLen     As Long = 200000000     ' 2016.07.22 Add 分割取得閾値 デフォルト200MB

'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
Public Const MaxCntImport       As Integer = 5000       ' 最大書き込み数
'* ミッションデータを取得するタイプ
Public Const SEARCH_TYPE_TEST As String = "1"
Public Const SEARCH_TYPE_AUTO As String = "0"
'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End

Public Const DefaultZoomPercent As Integer = 60
Public Const StepZoomPercent    As Integer = 20

Public Const MainSheetName      As String = "オンラインテンプレート"
Public Const ResultSheetName    As String = "result"
Public Const GraphEditSheetName As String = "グラフ編集"
Public Const FixGraphSheetName  As String = "グラフ"
Public Const ParamSheetName     As String = "param"
Public Const RoParamSheetName   As String = "ro_param"
Public Const ForPivotSheetName  As String = "演算結果データ"
Public Const PivotSheetName     As String = "グラフテーブル"
Public Const VerticalAxisName   As String = "−縦軸−"
Public Const PushAlarmSheetName As String = "プッシュアラーム"
Public Const GraphAlarmSheetName As String = "GraphAlarm_Param"

Public Const TateLabelRange   As String = "$G$30"
Public Const YokoLabelRange   As String = "$L$17"

'* 系列先頭セル位置
Public Const ADDR_SeriesStartPos    As String = "C1"
'* 時間先頭セル位置
Public Const ADDR_WorkTimeStartPos  As String = "D1"
'* メインピボットテーブル書き出し位置
Public Const ADDR_MainPivotTable    As String = "!R1C1"
'* サブピボットテーブル書き出し位置
Public Const ADDR_SubPivotTable    As String = "!R17C9"

'* Element情報
Public Type ELMINFO
    name            As String       ' エレメント名
    ID              As String       ' GUID
    Value           As Variant      ' 値
    Type            As Integer      ' 型
    File            As Variant      ' ファイル名(Bulky指定のみ存在)
    colum_no        As Integer      ' 列番号
End Type

'* CTM情報
Public Type CTMINFO
    name            As String       ' CTM名
    ID              As String       ' GUID
    Element()       As ELMINFO      ' エレメント情報
    RecvTime        As String       ' 受信時刻
End Type

'* ミッション情報
Public Type missionInfo
    name            As String       ' CTM名
    ID              As String       ' GUID
    CTM()           As CTMINFO      ' エレメント情報
End Type

Public Const WriteLogFlag As Boolean = False ' ログ出力フラグ




'*******************************
'* ファイル名を変数へセットする
'*******************************
Public Sub SetFilenameToVariable()
    Dim srchRange       As Range
    Dim workRange       As Range
    
    Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Range("A:A")
    Set workRange = srchRange.Find(What:="マクロファイル名", LookAt:=xlWhole)
    If Not workRange Is Nothing Then
        thisBookName = workRange.Offset(0, 1).Value
    End If

End Sub

'*********************
'* UNIX時間を取得する
'*********************
Public Function GetUnixTime(wDateTime As Date) As Double
    Dim timezoneId  As String
        
    timezoneId = GetTimezoneId()
    
    If timezoneId = "" Then
        GetUnixTime = ((wDateTime - 25569) * 86400 - 32400) * 1000
    Else
        ' 2017.02.22 Change ↓↓↓*************
'        GetUnixTime = CreateObject("FoaCoreCom.FoaCoreCom").GetUnixTime(Year(wDateTime), Month(wDateTime), Day(wDateTime), Hour(wDateTime), Minute(wDateTime), Second(wDateTime), timezoneId)
        GetUnixTime = CreateObject("FoaCoreCom.FoaCoreCom").GetUnixTime_2(Year(wDateTime), Month(wDateTime), Day(wDateTime), Hour(wDateTime), Minute(wDateTime), Second(wDateTime), Strings.Right(Strings.Format(wDateTime, "#0.000"), 3), timezoneId)
        ' 2017.02.22 Change ↑↑↑*************
    End If

End Function

'*********************
'* 日時文字列を取得する
'*********************
Public Function GetDateTime(unixTime As Double) As String
    Dim timezoneId  As String
    
    timezoneId = GetTimezoneId()
    
    If timezoneId = "" Then
        GetDateTime = Format(CDate(((CLng(unixTime / 1000) + 32400) / 86400) + 25569), "yyyy/MM/dd hh:mm:ss")
    Else
        GetDateTime = CreateObject("FoaCoreCom.FoaCoreCom").GetDateTime(unixTime, timezoneId)
    End If
    
End Function

'********************************************************
'* 取得したJSONのオブジェクトから指定されたkeyの値を得る
'********************************************************
Public Function GetJSONValue(objJSON As Object, strKey As String) As Variant
    GetJSONValue = CallByName(objJSON, strKey, VbGet)
End Function

'******************
'* 漢字コード変換
'******************
Public Function convTextEncoding(ByVal text, ByVal fromCharaset As String, Optional ByVal toCharaset As String = "unicode")
    Dim convText As String

    With CreateObject("ADODB.Stream")
        .Open
        .Type = adTypeText
        .Charset = toCharaset
        .WriteText text
        .Position = 0
        .Type = adTypeText
        .Charset = fromCharaset
        
On Error GoTo myLabel
        convText = .ReadText()
        convTextEncoding = Mid(convText, 3, Len(convText))
On Error GoTo 0
    End With
    
    Exit Function
    
myLabel:
    convTextEncoding = StrConv(text, vbUnicode, 1041)

End Function

'******************
'* 更新周期取得
'******************
Public Function GetUpdatePeriod() As Long 'No.603 2017/04/25 Integer→Long
    Dim srchRange       As Range
    Dim period As Long 'No.603 2017/04/25 Integer→Long
        
    With Workbooks(thisBookName).Worksheets(MainSheetName)
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            period = srchRange.Offset(0, 1).Value         ' ミリ秒計算
        Else
            period = 1
        End If
    End With
    
    GetUpdatePeriod = period * 1000
End Function

'******************
'* onlineId取得
'******************
Public Function GetOnlineId() As String
    Dim srchRange       As Range
    Dim onlineId        As Variant
        
    Set srchRange = ThisWorkbook.Worksheets(RoParamSheetName).Range("A:A")
    Set onlineId = srchRange.Find(What:="オンライングラフID", LookAt:=xlWhole)
    
    If onlineId Is Nothing Then
        GetOnlineId = ""
    Else
        GetOnlineId = CStr(onlineId.Offset(0, 1).Value)
    End If
    
End Function


'******************
'* TimezoneId取得
'******************
Public Function GetTimezoneId() As String
    Dim srchRange       As Range
    Dim timezoneId      As Variant
        
    Set srchRange = ThisWorkbook.Worksheets(RoParamSheetName).Range("A:A")
    Set timezoneId = srchRange.Find(What:="TimezoneId", LookAt:=xlWhole)
    
    If timezoneId Is Nothing Then
        GetTimezoneId = ""
    Else
        GetTimezoneId = CStr(timezoneId.Offset(0, 1).Value)
    End If
   
End Function

'***********************************************
'* メインシートにあるミッション情報を取得する
'***********************************************

Public Sub GetKeyOrder(currSheetName As String, keyOrder() As String)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim CTMRange        As Range
    Dim elmRange        As Range
    Dim currWorksheet   As Worksheet
    Dim ctmIndex        As Integer      ' CTM情報インデックス
    Dim elmIndex        As Integer      ' CTMID,エレメント情報インデックス
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    Dim maxRow          As Integer
    Dim maxCol          As Integer
    
    Set currWorksheet = Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim keyOrder(0)
    
    With currWorksheet
    
        '* CTM名称の項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        With srchRange.SpecialCells(xlCellTypeLastCell)
            maxRow = .Row - srchRange.Row + 1
            maxCol = .Column - srchRange.Column + 1
        End With
        
        ctmIndex = 0
        ReDim keyOrder(maxRow / 2, maxCol)
        
        For II = 0 To maxRow Step 2
            Set CTMRange = startRange.Offset(II, 0)
            elmIndex = 0
            
            If CTMRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            '* CTM情報取得
            keyOrder(ctmIndex, elmIndex) = CTMRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = CTMRange.Offset(0, 1)
            For JJ = 0 To maxCol - 1
                elmIndex = elmIndex + 1
                keyOrder(ctmIndex, elmIndex) = elmRange.Offset(1, JJ).Value
            Next
            ctmIndex = ctmIndex + 1
        Next
    End With
    
End Sub



'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Public Sub OutputCTMInfoToSheetFromCsv(currSheetName As String, dispTerm As Double, csvFile As String)
    
    Dim currWorksheet   As Worksheet
    
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim delRange        As Range
    
    Dim buf As String

    Dim lineCount As Integer
        
    '* 出力先シートの取得
    Set currWorksheet = Worksheets(currSheetName)
    currWorksheet.Activate
    
    With currWorksheet
        
        
        '* 全情報クリア
        Set startRange = .Cells(2, 1)
        Set lastRange = startRange.End(xlDown)
        Set delRange = .Range("2:" & Format(lastRange.Row))
        delRange.Delete
        
        
        Dim FileSize As Long
        FileSize = FileLen(csvFile)

        If FileSize > 0 Then
            '* 書き出し位置の設定
            Set startRange = .Cells(2, 1)
            
            Columns("A").Insert
                    
            With ActiveSheet.QueryTables.Add(Connection:="Text;" + csvFile, Destination:=Range("A2"))
                .TextFileCommaDelimiter = True
                .TextFilePlatform = 65001
                .Refresh BackgroundQuery:=False
                .Delete
            End With
            
            Columns("A").Delete

            ActiveSheet.UsedRange.Sort Key1:=Range("A2"), Order1:=xlDescending, Header:=xlYes
        End If

        
    End With
    
End Sub

'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Public Sub AddCTMInfoToSheetFromCsv(currSheetName As String, dispTerm As Double, csvFile As String)

    
    Dim currWorksheet   As Worksheet
    
    Dim startRange      As Range
    Dim lastRange       As Range
    
    Dim pastAddress     As String
    
    Dim buf As String

    Dim lineCount As Integer
    
    
    '* 出力先シートの取得
    Set currWorksheet = Worksheets(currSheetName)
    currWorksheet.Activate
    
    With currWorksheet
        
        Dim FileSize As Long
        FileSize = FileLen(csvFile)

        If FileSize > 0 Then
            '* 書き出し位置の設定
            Set startRange = .Cells(2, 1)
            If IsEmpty(startRange.Value) Then
                Set startRange = .Cells(1, 1)
            Else
                Set startRange = startRange.End(xlDown)
            End If
            
            Columns("A").Insert
            
            pastAddress = "A" & (startRange.Row + 1)
                    
            With ActiveSheet.QueryTables.Add(Connection:="Text;" + csvFile, Destination:=Range(pastAddress))
                .TextFileCommaDelimiter = True
                .TextFilePlatform = 65001
                .Refresh BackgroundQuery:=False
                .Delete
            End With
            
            Columns("A").Delete

            ActiveSheet.UsedRange.Sort Key1:=Range("A2"), Order1:=xlDescending, Header:=xlYes
        End If

        
    End With
    
    
    
    
End Sub


'******************
'* DLLの設定
'******************
Public Function InitDll(Workbook As Workbook) As String
    
    Const GripDataRetrieverName As String = "GripDataRetriever"
    Const FoaCoreComLoadName As String = "FoaCoreCom"
        
    Const GripDataRetrieverTlb As String = "GripDataRetriever.tlb"
    Const FoaCoreComTlb As String = "FoaCoreCom.tlb"
        
    Const AisPath1 As String = "C:\foa\ais\"
    Const AisPath2 As String = "C:\ais\"
        
    Dim isBrokenFlag As Boolean
    isBrokenFlag = False
        
    Dim GripDataRetrieverLoad As Boolean
    Dim FoaCoreComLoad As Boolean
    GripDataRetrieverLoad = False
    FoaCoreComLoad = False
    
    On Error GoTo myError
    
    ' 参照設定のロード、参照不可になっているか確認する。
    Dim Ref
    For Each Ref In Workbook.VBProject.References
        If Ref.name = GripDataRetrieverName Then
            GripDataRetrieverLoad = True
        ElseIf Ref.name = FoaCoreComLoadName Then
            FoaCoreComLoad = True
        End If
        
        If Ref.IsBroken = True Then
            isBrokenFlag = True
        End If
    Next Ref
    
    InitDll = ""
    
    
    Dim aisPath As String
    If GripDataRetrieverLoad = False Or FoaCoreComLoad = False Then
        Dim currWorksheet As Worksheet
        Dim srchRange As Range
        Set currWorksheet = Workbook.Worksheets(ParamSheetName)
        Set srchRange = currWorksheet.Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        aisPath = srchRange.Offset(0, 1).Value
        
        Dim n As Integer
        n = InStr(aisPath, "registry")
            
        aisPath = Left(aisPath, n - 1)
        If Dir(aisPath + FoaCoreComTlb) = "" Then
            aisPath = AisPath1
            If Dir(aisPath + FoaCoreComTlb) = "" Then
                aisPath = AisPath2
            End If
        End If

    End If
    
    
    
    ' 必要なモジュールのロード
    If GripDataRetrieverLoad = False Then
        Dim RefFile As String
        RefFile = aisPath + GripDataRetrieverTlb
        ActiveWorkbook.VBProject.References.AddFromFile RefFile
    End If
    
    If FoaCoreComLoad = False Then
        Dim RefFile2 As String
        RefFile2 = aisPath + FoaCoreComTlb
        ActiveWorkbook.VBProject.References.AddFromFile RefFile2
    End If
    
    If isBrokenFlag = True Then
        InitDll = "参照不可になっているライブラリーが存在します。" & vbCrLf
    End If
    
myError:
    InitDll = ""
End Function

' Wang Foastudio Issue AISTEMP-24 20181207 Start
Public Function GetSearchType() As String
    Dim sampleType As String
    Dim srchRange As Object
    Dim currWorksheet As Object
    Dim wrkStr As String
    
    Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
    
    'オンライン種別の取込
    Set currWorksheet = ThisWorkbook.Worksheets(MainSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="オンライン種別", LookAt:=xlWhole)

    If Not srchRange Is Nothing Then
        If IsEmpty(srchRange.Offset(0, 1).Value) Then
            wrkStr = "期間固定"
        Else
            wrkStr = srchRange.Offset(0, 1).Value
        End If
    Else
        wrkStr = "期間固定"
    End If
    
    ' オンライン種別毎にタイマー処理起動
    GetSearchType = SEARCH_TYPE_AUTO
    
    If (wrkStr = "期間固定") Then
        GetSearchType = SEARCH_TYPE_AUTO '既存データをクリアしない
    ElseIf (wrkStr = "連続移動") Then
        GetSearchType = SEARCH_TYPE_TEST  '既存データをクリアする
    ElseIf wrkStr = "一定時間繰り返し" Then
        GetSearchType = SEARCH_TYPE_TEST  '既存データをクリアする
    ElseIf wrkStr = "毎日定刻繰り返し" Then
        GetSearchType = SEARCH_TYPE_TEST  '既存データをクリアする
    End If
End Function
' Wang Foastudio Issue AISTEMP-24 20181207 End


Public Sub WriteLog(ByVal strMsg As String)
    Dim objFso As Object
    Set objFso = CreateObject("Scripting.FileSystemObject")
     
    Dim strPath As String
    strPath = ThisWorkbook.Path & "\log-OnlineTemplateXlsm.txt"
     
    If WriteLogFlag = True Then
        With objFso
            If Not .FileExists(strPath) Then
                .CreateTextFile (strPath)
            End If
            With .OpenTextFile(strPath, 8) '8:ForAppending
                .WriteLine Now & vbTab & strMsg
                .Close
            End With
        End With
         
        Set objFso = Nothing
    End If
     
End Sub

Public Sub WriteLogSimple(ByVal strMsg As String)
    Dim objFso As Object
    Set objFso = CreateObject("Scripting.FileSystemObject")
     
    Dim strPath As String
    strPath = ThisWorkbook.Path & "\log-OnlineTemplateXlsm.txt"
     
    If WriteLogFlag = True Then
        With objFso
            If Not .FileExists(strPath) Then
                .CreateTextFile (strPath)
            End If
            With .OpenTextFile(strPath, 8) '8:ForAppending
                .WriteLine strMsg
                .Close
            End With
        End With
         
        Set objFso = Nothing
    End If
     
End Sub






