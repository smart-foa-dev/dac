Attribute VB_Name = "MissionStd_Module"
'**********************
'* 収集開始日時の取得
'* →返り値はUNIX TIME
'**********************
Public Function GetCollectStartDTstd(currWorksheet As Worksheet) As Double
    Dim srchRange           As Range
    Dim startRange          As Range
    Dim selectStr           As String
    Dim dispTerm            As Double
    Dim dispTermUnit        As String
    Dim prevDate            As Date
    Dim prevTime            As Date
    Dim startTime           As Double
    Dim startD              As Date
    Dim startT              As Date
    
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="オンライン種別", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "" Then
                selectStr = srchRange.Offset(0, 1).Value
            Else
                selectStr = "期間固定"
            End If
        Else
                selectStr = "期間固定"
        End If
    
        Set startRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not startRange Is Nothing Then
            startD = startRange.Offset(0, 1).Value
            startT = startRange.Offset(0, 2).Value
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            GetCollectStartDTstd = -1
            Exit Function
        End If
        startTime = GetUnixTime(startD + startT)
    End With
    
    '* 表示期間の設定
    Select Case selectStr
        Case "期間固定"
            '* 期間固定の場合、GRIPミッションへの問い合わせ収集開始時刻は
            '* オンラインテンプレートシートの「収集開始日時」が相当する
            dispTerm = 0#
        Case "連続移動"
            '* 連続移動の場合、GRIPミッションへの問い合わせ収集開始時刻は
            '* 現在時刻から表示期間を遡った時刻が「収集開始日時」になる
            '*******************************
            '* 表示期間を取得
            '*******************************
            Set srchRange = currWorksheet.Cells.Find(What:="表示期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                If IsEmpty(srchRange.Offset(0, 1).Value) Then
                    dispTerm = 1#
                    dispTermUnit = "【時】"
                Else
                    dispTerm = srchRange.Offset(0, 1).Value
                    dispTermUnit = srchRange.Offset(0, 2).Value
                End If
            Else
                dispTerm = 1#
                dispTermUnit = "【時】"
            End If
            Select Case dispTermUnit
                Case "【分】"
                    dispTerm = dispTerm * 60 * (-1)
                Case "【時】"
                    dispTerm = dispTerm * 3600 * (-1)
            End Select
            prevDate = Now
            prevTime = DateAdd("s", dispTerm, prevDate)
            startTime = GetUnixTime(prevTime)
            startRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            startRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
    End Select
    
    GetCollectStartDTstd = startTime

End Function

' 2017.02.22 Add ↓↓↓
'*********************
'* 収集終了日時を取得
'*********************
Public Function GetCollectEndDTstd(currWorksheet As Worksheet) As Double
    Dim srchRange           As Range
    Dim endTime             As Double
    Dim endD                As Date
    Dim endT                As Date
    
    With currWorksheet
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Function
        End If
        endTime = GetUnixTime(endD + endT)
    End With
    
    GetCollectEndDTstd = endTime
End Function

'****************************************************************
'* 対象ミッションの情報を取得する
'****************************************************************
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'Public Function GetMissionInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO) As String
'Graph_Alarm sunyi 2018/09/12 start
Public Function GetMissionInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO, sType As String, Optional OnServer As String = "False") As String
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim missionID   As String
    Dim CtmId       As String
    Dim startTime   As Double
    Dim endTime     As Double
    Dim SendUrlStr  As String
    Dim SendString  As Variant
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim iResult     As Integer
    Dim bPmary      As Variant
    Dim II          As Integer
    Dim JJ          As Integer
    Dim JJJ         As Integer
    Dim KK          As Integer
    Dim currCTM     As CTMINFO
    
    Dim cmsVersion  As String
    Dim mfIPAddr    As String
    Dim mfPortNo    As String
    Dim chgStr01    As String
    Dim chgStr02    As String
    
    Dim workFolder  As String
    Dim workFile    As String
    
    Dim fileLength  As Long
    Dim divideNum   As Integer  ' 分割取得回数
    Dim divideTime  As Double
    Dim wkStartTime As Double
    Dim wkEndTime   As Double
    
    Dim updTime     As Variant  ' 2017.02.21 Add
    Dim onlineId    As String   ' 2017.02.21 Add
    
    Dim jsonParse As New FoaCoreCom.jsonParse
    Dim keyOrder() As String
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    'FOR COM高速化 zhengxiaoxue 2018/06/15 Add Start
    Dim ctmData As FoaCoreCom.CtmDataRetriever
    Dim csvWrite As FoaCoreCom.CsvWriteToExcelHandler
    Dim zipFile As FoaCoreCom.ZipFileHandler
    
    Dim zipUrl As String
    Dim unZipUrl As String
    Dim msg As String
    'FOR COM高速化 zhengxiaoxue 2018/06/15 Add End
    
    '* メインシートから各種情報を取得
    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        ''' BugFix AIS_TEMP.130 2019/06/17 yakiyama start.
        'startTime = GetCollectStartDT(Workbooks(thisBookName).Worksheets(MainSheetName))
        startTime = GetCollectStartDTForMissionStd(Workbooks(thisBookName).Worksheets(MainSheetName))
        ''' BugFix AIS_TEMP.130 2019/06/17 yakiyama end.
        
'        Debug.Print "startTime:" + GetUnixTime2Date(startTime) + "(" + Format(startTime) + ")"

        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Function
        End If
        endTime = GetUnixTime(endD + endT)
        
        '*********************
        '* ミッションIDを取得
        '*********************
        missionID = ""
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "ミッションID項目が見つからないため処理を中断します。"
            GetMissionInfo = "ERROR"
            Exit Function
        End If
        
        '2017.02.21 Add*****
        '* 更新周期を取得
        '2017.02.21 Add*****
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
    
    End With
    
    With Workbooks(thisBookName).Worksheets(ParamSheetName)

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If

        ' 2016.07.20 Add *********************
        '* CMSバージョンを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="CmsVersion", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            cmsVersion = srchRange.Offset(0, 1).Value
        Else
            cmsVersion = "V5"
        End If
        
        ' 2016.07.20 Add *********************
        '* 旧バージョン用IPアドレスおよびPortNoを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="MfHost", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            mfIPAddr = srchRange.Offset(0, 1).Value
            mfPortNo = srchRange.Offset(1, 1).Value
        Else
            mfIPAddr = "localhost"
            mfPortNo = "50031"
        End If
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
        
    End With
    
    ' 2017.02.21 Add *********************
    '* オンライングラフIDの取得
    ' 2017.02.21 Add *********************
    onlineId = GetOnlineId()
    
    '*******************************
    '* CTM情報一覧をシートから取得
    '*******************************
    
    Call GetCTMList_Colum(MainSheetName, ctmList)
    'Call GetCTMList(MainSheetName, ctmList)
    
    '*******************************
    '* プログラムミッションに問合せ
    '*******************************
On Error GoTo ERROR_STEP

    '* ミッションIDがあれば
    If missionID <> "" Then
        getEndTime = endTime
        getStartTime = startTime
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pmzip?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & _
                        "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true" _
                        & "&onlineId=" & "&period=" & Format(updTime * 1000) & "&collectEnd=" & Format(getEndTime)
        WriteLog ("GetMissionInfo() MissionID: " & missionID & " SendUrlStr: " & SendUrlStr)
                        
        Set ctmData = CreateObject("FoaCoreCom.ais.retriever.CtmDataRetriever")
        Set csvWrite = CreateObject("FoaCoreCom.ais.retriever.CsvWriteToExcelHandler")
        Set zipFile = CreateObject("FoaCoreCom.ais.retriever.ZipFileHandler")
        msg = ""
    
        '情報を格納するZIPファイルパスを取得する
        zipUrl = ctmData.GetProgramMissionZipFileAsync(SendUrlStr, proxyUri, msg)
        
        On Error GoTo ErrorMissionHandler
    
    
        'Graph_Alarm sunyi 2018/09/12 start
        If OnServer <> "True" Then
            '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
            If InStr(msg, "MISSION_NOT_FOUND") > 0 Then
                MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
                
                GetMissionInfo = "ERROR"
                Exit Function
            End If
        '**********
        End If
    
        'Zipファイルを解凍する
        unZipUrl = zipFile.unZipFile(zipUrl)
    
        'excelへ書き込む
        Call csvWrite.ReadAllCtmInfoToExcel(Workbooks(thisBookName), unZipUrl, MaxCntImport, sType, GetTimezoneId(), MainSheetName)
    
        '一時フォルダを削除
        zipFile.deleteFolder (unZipUrl)
        
        GetMissionInfo = "SUCCESS1@@" + "0"
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
    
    Else    '* ミッションIDがなければ
    
        ReDim ctmValue(0)
        ctmValue(0).ID = ""
        For II = 0 To UBound(ctmList)
    
            currCTM = ctmList(II)
            CtmId = currCTM.ID
        
            getEndTime = endTime
            getStartTime = startTime
            
            SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/ctm/find?testing=0"
            
            If useProxy = False Then
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
            Else
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                httpObj.setProxy 2, proxyUri
            End If
            
            httpObj.Open "POST", SendUrlStr, False
            httpObj.setRequestHeader "Content-Type", "text/plain"
            httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***Content-Type: text/plain
            
            ' 2016.07.20 Change ↓↓↓ ***************************
            ' 2016.07.22 Change ↓↓↓ *** POST文字列に「testing=0」を追加
            If cmsVersion = "3.5" Then
                SendString = "{""ctmId"":""" & CtmId & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & _
                               ",""mfHostName"":" & """" & mfIPAddr & """" & ",""mfPort"":" & """" & mfPortNo & """" & "}"
            Else
                SendString = "{""ctmId"":""" & CtmId & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & "}"
            End If
            ' 2016.07.22 Change ↑↑↑ ***************************
            ' 2016.07.20 Change ↑↑↑ ***************************
            WriteLog ("GetMissionInfo() NoMission:  SendUrlStr: " & SendUrlStr & " Post SendString: " & SendString)
            
            bPmary = StrConv(SendString, vbFromUnicode)
            
            httpObj.send (SendString)
                        
            ' ダウンロード待ち
            Do While httpObj.readyState <> 4
                DoEvents
            Loop
            
            ' 2016.07.22 Add ↓↓↓ *****************************
            On Error GoTo ErrorCTMHandler
            ' 2016.07.22 Add ↑↑↑ *****************************
            '* 取得情報のデコード
            Set sc = CreateObject("ScriptControl")
            With sc
                .Language = "JScript"
        
                '指定したインデックス、名称のデータを取得する
                .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
            End With
'            GetString = convTextEncoding(httpObj.responseBody, "UTF-8")
            GetString = httpObj.responseText
            
            ' 2016.07.20 Add ***************************
            If cmsVersion = "3.5" Then
                chgStr01 = Replace(GetString, "\""", "@@")
                chgStr02 = Replace(chgStr01, """", "")
                GetString = Replace(chgStr02, "@@", """")
            End If
            ' 2016.07.20 Add ***************************
            
            Set objJSON = sc.CodeObject.jsonParse(GetString)
            
            Set sc = Nothing
            
            Set httpObj = Nothing
        
            '* 取得情報をCTM情報に展開
            iResult = GetCTMInfoForCTM(objJSON, currCTM, ctmValue)
            
            GetMissionInfo = "SUCCESS2@@" + iResult
            
            Set objJSON = Nothing
        
        Next
    
    End If
    

    Exit Function

ERROR_STEP: 'エラーSTEP追加

    GetMissionInfo = "ERROR"
    Exit Function


ErrorMissionHandler:

    Set sc = Nothing
    Set httpObj = Nothing
    Set objJSON = Nothing
    
    '* ミッションIDによる取得に失敗した場合
    For JJ = 3 To 10
    
        divideNum = JJ ^ 2
    
        divideTime = CLng((endTime - startTime) / divideNum)
        
        wkStartTime = startTime
        For II = 0 To divideNum - 1
            wkEndTime = wkStartTime + divideTime
            
            iResult = GetDivideMissionInfo(ctmList, ctmValue, missionID, wkStartTime, wkEndTime)

            '* 取得失敗したらループを抜け分割数を増加させる
            If iResult < 0 Then Exit For

            '* 取得CTM毎に処理
            For JJJ = 0 To UBound(ctmList)
        
                '* 取得データがあれば該当CTMシートへ出力
                If iResult > 0 Then
                    Call OutputCTMInfoToSheet(ctmList(JJJ).name, ctmValue, False)
                End If

                DoEvents
            Next
            
            wkStartTime = wkEndTime + 1#
            
            DoEvents
        Next
        
        '* 分割数分処理が終了したら分割処理ループを抜ける
        If II > (divideNum - 1) Then Exit For
        
        DoEvents
    Next

    
    GetMissionInfo = "ERROR"
    
    Exit Function
    
    
ErrorCTMHandler:
    
    Set sc = Nothing
    Set httpObj = Nothing
    Set objJSON = Nothing
    
    '* CTM IDによる取得に失敗した場合
    For JJ = 3 To 10
    
        divideNum = JJ ^ 2
    
        divideTime = CLng((endTime - startTime) / divideNum)
        
        wkStartTime = startTime
        For II = 0 To divideNum - 1
            wkEndTime = wkStartTime + divideTime
            
'Debug.Print "divideNum=" & Format(divideNum) & ", II=" & Format(II)

            iResult = GetDivideCTMInfo(ctmList, ctmValue, wkStartTime, wkEndTime)

            '* 取得失敗したらループを抜け分割数を増加させる
            If iResult < 0 Then Exit For

'Debug.Print "wkStartTime=" & Format(wkStartTime) & ", wkEndTime=" & Format(wkEndTime)

            '* 取得CTM毎に処理
            For JJJ = 0 To UBound(ctmList)
        
                '* 取得データがあれば該当CTMシートへ出力
                If iResult > 0 Then
                    Call OutputCTMInfoToSheet(ctmList(JJJ).name, ctmValue, False)
                End If

                DoEvents
            Next
            
            wkStartTime = wkEndTime + 1#
            
            DoEvents
        Next
        
        '* 分割数分処理が終了したら分割処理ループを抜ける
        If II > (divideNum - 1) Then Exit For
        
        DoEvents
    Next
    
    GetMissionInfo = "ERROR"
    
    Exit Function
    
End Function
