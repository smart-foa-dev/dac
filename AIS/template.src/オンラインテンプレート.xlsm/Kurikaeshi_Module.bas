Attribute VB_Name = "Kurikaeshi_Module"
Option Explicit

'**************************
'* 自動更新タイマー起動開始
'**************************
Public Sub Kurikaeshi_TimerStart()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range

    Call SetFilenameToVariable

    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
        
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'Kurikaeshi_TimerLogic'"
        
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
        End If
        
    End With
    
    '* ピボットテーブル更新
    Call RefreshPivotTableData
    
    '* 更新中／停止中ステータス更新
    Call UpdateFormStatus
    
End Sub

'***********************
'* 自動更新タイマー処理
'***********************
Public Sub Kurikaeshi_TimerLogic()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range
    Dim FinalTime       As Date
    Dim sampleType      As String
    
    '*Graph_Alarm sunyi 2018/09/14 start
    Call SetUpdateStartTime
    '*Graph_Alarm sunyi 2018/09/14 end
    
    Call SetFilenameToVariable
    Set currWorksheet = Workbooks(thisBookName).Worksheets(MainSheetName)
    With currWorksheet
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "ON" Then
                Application.CutCopyMode = False
                Exit Sub
            End If
        End If
    
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
        
        'オンライン停止判定
        FinalTime = endD + endT
        If Format(Now, "yyyy/MM/dd hh:mm:ss") >= FinalTime Then         '終了時刻になったら終わる
            MsgBox "終了時刻になりました。" & " " & Format(FinalTime, "hh:mm:ss")
            Call AutoUpdateStop
            Application.CutCopyMode = False
            Exit Sub
        End If
    
        ' 収集タイプの取得
        sampleType = ""
        Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
        
        'CTM,ミッションの取込
        Application.ScreenUpdating = False
            If sampleType <> "GRIP" Then
                '* ミッションから情報を取得
                'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
                'Call Kurikaeshi_GetMissionInfoAll
                
                ' Wang Foastudio Issue AISTEMP-24 20181207 Start
                'Call Kurikaeshi_GetMissionInfoAll(SEARCH_TYPE_AUTO)
                Call Kurikaeshi_GetMissionInfoAll(GetSearchType())
                ' Wang Foastudio Issue AISTEMP-24 20181207 End
                
                'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
            Else
                Call GetGripMission
            End If

            '* ピボットテーブル更新
            Call RefreshPivotTableData

            '* 更新状態表示更新
            Call UpdateFormStatus
        Application.ScreenUpdating = True

        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
        
        '* 次回の処理をスケジュール
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'Kurikaeshi_TimerLogic'"
        
        '前回更新日時セット
        Set srchRange = .Cells.Find(What:="前回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevDate, "hh:mm:ss")
        End If
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回起動", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            srchRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
        End If
    End With

    'EXCEL前面表示用
    If Application.Visible = True Then
        Call ExcelUpperDisp
        VBA.AppActivate Excel.Application.Caption
    
        Call ExcelDispSetWin32
        Call ExcelDispFreeWin32
    End If
    
    ' プッシュアラーム機能の呼び出し
    Call UpdateAfterGraphEdit
    'Call checkPushAlarm
    
End Sub


'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'Public Sub Kurikaeshi_GetMissionInfoAll()
'Graph_Alarm sunyi 2018/09/12 start
Public Sub Kurikaeshi_GetMissionInfoAll(sType As String, Optional OnServer As String = "False")
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    Dim ctmList()       As CTMINFO      ' CTM情報
    Dim ctmValue()      As CTMINFO      ' CTM取得情報
    Dim II              As Integer
    Dim srchRange       As Range
    Dim lastRange       As Range
    Dim workRange       As Range
    Dim endRange        As Range
    
    Dim currWorksheet   As Worksheet
    Dim selectStr       As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim dispTerm        As Double
    Dim dispTermUnit    As String
    
    Dim actWorksheet    As Worksheet
    Dim rstWorksheet    As Worksheet
    
    Dim CTMRange        As Range
        
    Dim iResult         As Integer
    
    Dim result     As String
    Dim resultSuccess() As String
    
    Dim PathName As String, FileName As String, pos As Long
    Dim endData         As Double
    
    
    '* CMSからCTM情報を取得する
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
    'result = Kurikaeshi_GetMissionInfo(ctmList, ctmValue)
    'Graph_Alarm sunyi 2018/09/12 start
    result = Kurikaeshi_GetMissionInfo(ctmList, ctmValue, sType, OnServer)
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    
    If result = "ERROR" Then
        Exit Sub
    End If
   
    resultSuccess = Split(result, "@@")
    
    
    'CTM取得件数設定 20161117 追加
    Set CTMRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="CTM取得件数", LookAt:=xlWhole)
    If Not CTMRange Is Nothing Then
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
        If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS1" Then
'            ' ディレクトリを取得
'            pos = InStrRev(resultSuccess(1), "\")
'            PathName = Left(resultSuccess(1), pos)
'            '* CTM種別でシートに出力
             For II = 0 To UBound(ctmList)
                iResult = iResult + 1
                 
'                FileName = PathName + ctmList(II).ID + ".csv"
'                If Dir(FileName) <> "" Then
'                    iResult = iResult + 1
'                End If
             Next
            
        'ミッションIDがない場合(find分岐)
        ElseIf UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
            iResult = CInt(resultSuccess(1))
        
        End If
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
        
        CTMRange.Offset(0, 1).Value = iResult
    End If
    
    '20161117　追加
    If iResult <= 0 Then
        '* ISSUE_NO.686 sunyi 2018/05/09 start
        '* データない時、シートをクリアする
        Call DeleteOutrangeData2(Workbooks(thisBookName).Worksheets(ResultSheetName))
        '* ISSUE_NO.686 sunyi 2018/05/09 end
        Exit Sub
    End If
    
'* ISSUE_NO.625 Delete ↓↓↓ *******************************
'    '* resultシート上の以前のデータを削除する
'    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
'    With currWorksheet
'        Set srchRange = currWorksheet.Range("A2")
'        If srchRange.Value <> "" Then
'            Set srchRange = currWorksheet.Range("A1")
'            Set lastRange = srchRange.End(xlToRight)
'            Set srchRange = currWorksheet.Range("A2").End(xlDown)
'            Set endRange = currWorksheet.Cells(srchRange.Row, lastRange.Column)
'
'            Set workRange = currWorksheet.Range("A2", endRange.Cells)
'            'Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
'            'workRange.Select
'            workRange.ClearContents
'        End If
'    End With
'* ISSUE_NO.625 Delete ↑↑↑ *******************************

    '* 新resultシートがなければ作成する
    Set actWorksheet = Workbooks(thisBookName).ActiveSheet
    '*ISSUE_NO.752 Sunyi 2018/06/25 Start
    '選択シートを戻す
'    actWorksheet.Activate
    '*ISSUE_NO.752 Sunyi 2018/06/25 End
    
'    Set rstWorksheet = Workbooks(thisBookName).Worksheets.Add(after:=Worksheets(Worksheets.Count))
'    rstWorksheet.Name = ResultSheetName & "_work"


    '* 取得CTM毎に処理
    For II = 0 To UBound(ctmList)
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'        If resultSuccess(0) = "SUCCESS1" Then
'
'            ' ディレクトリを取得
'            pos = InStrRev(resultSuccess(1), "\")
'            PathName = Left(resultSuccess(1), pos)
'
'            FileName = PathName + ctmList(II).ID + ".csv"
'
'            If Dir(FileName) <> "" Then
'                Call OutputCTMInfoToSheetFromCsv(ctmList(II).name, 0#, FileName)
'            End If
'
'        ElseIf resultSuccess(0) = "SUCCESS2" Then
'            '* 取得データがあれば該当CTMシートへ出力
'''          If iResult > 0 Then
'
'                'CTMシートの内容削除
'                Set currWorksheet = Workbooks(thisBookName).Worksheets(ctmList(II).name)
'                With currWorksheet
'                    Set srchRange = currWorksheet.Range("A2")
'                    If srchRange.Value <> "" Then
'                        Set lastRange = srchRange.End(xlToRight)
'                        Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
'                        workRange.ClearContents
'                    End If
'                End With
'
'                'CTMシートへの書込
'                Call OutputCTMInfoToSheet(ctmList(II).name, ctmValue, False)
'''          End If
'        End If

        'ミッションIDがない場合(find分岐)
        If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
            'CTMシートの内容削除
            Set currWorksheet = Workbooks(thisBookName).Worksheets(ctmList(II).name)
            With currWorksheet
                Set srchRange = currWorksheet.Range("A2")
                If srchRange.Value <> "" Then
                    Set lastRange = srchRange.End(xlToRight)
                    Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
                    workRange.ClearContents
                End If
            End With
                                   
            'CTMシートへの書込
            Call OutputCTMInfoToSheet(ctmList(II).name, ctmValue, False)
        End If
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    
    
'* ISSUE_NO.625 Delete ↓↓↓ *******************************
'        '* resultシートに転記
'        Call OutputResultToSheet_Colum(ctmList(II), Workbooks(thisBookName).Worksheets(ResultSheetName))
'* ISSUE_NO.625 Delete ↑↑↑ *******************************
        'Call OutputResultToSheet_Colum(ctmList(II), rstWorksheet)
    Next

    '* 旧resultシートを削除し、新resultシートの名称を変更する
''    Application.DisplayAlerts = False
''    Workbooks(thisBookName).Worksheets(ResultSheetName).Delete
''    Application.DisplayAlerts = True
''    rstWorksheet.Name = ResultSheetName

'* ISSUE_NO.625 Add ↓↓↓ *******************************
    ' ディレクトリを取得
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
    'ミッションIDがない場合(find分岐)
    If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
        pos = InStrRev(resultSuccess(1), "\")
        PathName = Left(resultSuccess(1), pos)
        
        FileName = PathName + ResultSheetName + ".txt"
        
        Call DeleteOutrangeData2(Workbooks(thisBookName).Worksheets(ResultSheetName))
        If Dir(FileName) <> "" Then
            Call AddCTMInfoToSheetFromCsv1(ResultSheetName, 0#, FileName)
        End If
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add Start
    End If
    'FOR COM共通化 zhengxiaoxue 2018/06/15 Add End
'* ISSUE_NO.625 Add ↑↑↑ *******************************
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
    '* resultシートをRECEIVE TIMEに従いソートする
    With currWorksheet
        Set srchRange = .Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = srchRange.End(xlDown)
        Set lastRange = .Cells(workRange.Row, lastRange.Column)
        Set workRange = .Range(srchRange, lastRange)
        workRange.Sort Key1:=.Range("B1"), _
                     Order1:=xlDescending, _
                     Header:=xlGuess
    End With
                
    '前回更新日時セット
    Set srchRange = Workbooks(thisBookName).Worksheets(MainSheetName).Cells.Find(What:="前回起動", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd")
        srchRange.Offset(0, 2).Value = Format(Now, "hh:mm:ss")
    End If
    '*ISSUE_NO.752 Sunyi 2018/06/25 Start
    '選択シートを戻す
    actWorksheet.Activate
    '*ISSUE_NO.752 Sunyi 2018/06/25 End
End Sub

'****************************************************************
'* 対象ミッションの情報を取得する
'****************************************************************
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify Start
'Public Function Kurikaeshi_GetMissionInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO) As String
'Graph_Alarm sunyi 2018/09/12 start
Public Function Kurikaeshi_GetMissionInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO, sType As String, Optional OnServer As String = "False") As String
'FOR COM共通化 zhengxiaoxue 2018/06/15 Modify End
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim missionID   As String
    Dim CtmId       As String
    Dim startTime   As Double
    Dim endTime     As Double
    Dim SendUrlStr  As String
    Dim SendString  As Variant
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim iResult     As Integer
    Dim bPmary      As Variant
    Dim II          As Integer
    Dim JJ          As Integer
    Dim JJJ         As Integer
    Dim KK          As Integer
    Dim currCTM     As CTMINFO
    
    Dim cmsVersion  As String
    Dim mfIPAddr    As String
    Dim mfPortNo    As String
    Dim chgStr01    As String
    Dim chgStr02    As String
    
    Dim workFolder  As String
    Dim workFile    As String
    
    Dim fileLength  As Long
    Dim divideNum   As Integer  ' 分割取得回数
    Dim divideTime  As Double
    Dim wkStartTime As Double
    Dim wkEndTime   As Double
    
    Dim dispTerm        As Double
    Dim dispTermUnit    As String
    Dim pos             As Integer
    
    Dim jsonParse As New FoaCoreCom.jsonParse
    Dim keyOrder() As String

    Dim useProxy As Boolean
    Dim proxyUri As String
    
    'FOR COM高速化 zhengxiaoxue 2018/06/15 Add Start
    Dim ctmData As FoaCoreCom.CtmDataRetriever
    Dim csvWrite As FoaCoreCom.CsvWriteToExcelHandler
    Dim zipFile As FoaCoreCom.ZipFileHandler
    
    Dim zipUrl As String
    Dim unZipUrl As String
    Dim msg As String
    'FOR COM高速化 zhengxiaoxue 2018/06/15 Add End

    
    '* オンラインテンプレートシートから各種情報を取得
    With Workbooks(thisBookName).Worksheets(MainSheetName)
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        'startTime = GetCollectStartDT(Workbooks(thisBookName).Worksheets(MainSheetName))
        
        Set srchRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(srchRange.Offset(0, 1).Value)
            startT = TimeValue(Format(srchRange.Offset(0, 2).Value, "hh:mm:ss"))
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            Kurikaeshi_GetMissionInfo = "ERROR"
            Exit Function
        End If
        startTime = GetUnixTime(startD + startT)
        
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Kurikaeshi_GetMissionInfo = "ERROR"
            Exit Function
        End If
        endTime = GetUnixTime(endD + endT)
        
        '*******************************
        '* 表示期間を取得
        '*******************************
        Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                dispTerm = 1#
                dispTermUnit = "【時】"
            Else
                dispTerm = srchRange.Offset(0, 1).Value
                dispTermUnit = srchRange.Offset(0, 2).Value
            End If
        Else
            dispTerm = 1#
            dispTermUnit = "【時】"
        End If
        Select Case dispTermUnit
            Case "【分】"
                dispTerm = dispTerm * 60
            Case "【時】"
                dispTerm = dispTerm * 3600
        End Select
        
        '**********************************
        '* 一定時間繰返し処理
        '* ==>収集開始時刻算出
        '**********************************
        startDT = startD + startT
        For pos = 1 To 100
            endDT = DateAdd("s", dispTerm * pos, startD + startT)
            If Now > endDT Then
                startDT = endDT
            Else
                Exit For
            End If
        Next pos
        .Range("H3").Value = Format(startDT, "yyyy/mm/dd hh:mm:ss")
        .Range("I3").Value = Format(endDT, "yyyy/mm/dd hh:mm:ss")
        startTime = GetUnixTime(startDT)
        endTime = GetUnixTime(endDT)
        
        '*********************
        '* ミッションIDを取得
        '*********************
        missionID = ""
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionID = srchRange.Offset(0, 1).Value
        Else
            'MsgBox "ミッションID項目が見つからないため処理を中断します。"
            Kurikaeshi_GetMissionInfo = "ERROR"
            Exit Function
        End If
        
    End With
    
    ' Paramシートからの取込
    With Workbooks(thisBookName).Worksheets(ParamSheetName)

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If

        ' 2016.07.20 Add *********************
        '* CMSバージョンを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="CmsVersion", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            cmsVersion = srchRange.Offset(0, 1).Value
        Else
            cmsVersion = "V5"
        End If
        
        ' 2016.07.20 Add *********************
        '* 旧バージョン用IPアドレスおよびPortNoを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="MfHost", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            mfIPAddr = srchRange.Offset(0, 1).Value
            mfPortNo = srchRange.Offset(1, 1).Value
        Else
            mfIPAddr = "localhost"
            mfPortNo = "50031"
        End If
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
        
        
    End With
    
    '*******************************
    '* CTM情報一覧をシートから取得
    '*******************************
    Call GetCTMList_Colum(MainSheetName, ctmList)
    
    '*******************************
    '* プログラムミッションに問合せ
    '*******************************
    '* ミッションIDがあれば
    If missionID <> "" Then
        getEndTime = endTime
        getStartTime = startTime
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete Start
'        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pm?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & _
'                        "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true"
'
'        If useProxy = False Then
'            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'        Else
'            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'            httpObj.setProxy 2, proxyUri
'        End If
'
'        httpObj.Open "GET", SendUrlStr, False
'        httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
'        httpObj.send
'
'        ' ダウンロード待ち
'        Do While httpObj.readyState <> 4
'            DoEvents
'        Loop
'
'        On Error GoTo ErrorMissionHandler
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete End
        
        '* 取得情報のデコード
        'Set sc = CreateObject("ScriptControl")
        'With sc
        '    .Language = "JScript"
        '
        '    '指定したインデックス、名称のデータを取得する
        '    .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
        'End With
        'Set objJSON = sc.CodeObject.jsonParse(GetString)
        '
        'Set sc = Nothing
        
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete Start
'        Call GetKeyOrder(MainSheetName, keyOrder)
'        GetString = httpObj.responseText
'
'        Kurikaeshi_GetMissionInfo = jsonParse.buildCsvFileCtmMission(GetString, GetTimezoneId, keyOrder)
'
'        Set httpObj = Nothing
'
'        Kurikaeshi_GetMissionInfo = "SUCCESS1@@" + Kurikaeshi_GetMissionInfo
        'FOR COM共通化 zhengxiaoxue 2018/06/15 Delete End
        
        '* 取得情報をCTM情報に展開
        'ReDim ctmValue(0)
        'ctmValue(0).ID = ""
        'iResult = GetCTMInfo(objJSON, ctmList, ctmValue)
        '
        'Set objJSON = Nothing
        
        'FOR COM高速化 zhengxiaoxue 2018/06/15 Add Start
        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pmzip?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & _
                       "&limit=" & Format(LimitCTMNumber) & "&lang=ja" & "&noBulky=true"
                       
        Set ctmData = CreateObject("FoaCoreCom.ais.retriever.CtmDataRetriever")
        Set csvWrite = CreateObject("FoaCoreCom.ais.retriever.CsvWriteToExcelHandler")
        Set zipFile = CreateObject("FoaCoreCom.ais.retriever.ZipFileHandler")
        msg = ""
    
        '情報を格納するZIPファイルパスを取得する
        zipUrl = ctmData.GetProgramMissionZipFileAsync(SendUrlStr, proxyUri, msg)
        
        On Error GoTo ErrorMissionHandler
        
        'Graph_Alarm sunyi 2018/09/12 start
        If OnServer <> "True" Then
            '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
            If InStr(msg, "MISSION_NOT_FOUND") > 0 Then
                MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
                
                Kurikaeshi_GetMissionInfo = "ERROR"
                Exit Function
            End If
            '**********
        End If
    
        'Zipファイルを解凍する
        unZipUrl = zipFile.unZipFile(zipUrl)
    
        'excelへ書き込む
        Call csvWrite.ReadAllCtmInfoToExcel(Workbooks(thisBookName), unZipUrl, MaxCntImport, sType, GetTimezoneId(), MainSheetName)
    
        '一時フォルダを削除
        zipFile.deleteFolder (unZipUrl)
        
        Kurikaeshi_GetMissionInfo = "SUCCESS1@@" + "0"
        'FOR COM高速化 zhengxiaoxue 2018/06/15 Add End
    
    '* ミッションIDがなければ
    Else
    
        ReDim ctmValue(0)
        ctmValue(0).ID = ""
        For II = 0 To UBound(ctmList)
    
            currCTM = ctmList(II)
            CtmId = currCTM.ID
        
            getEndTime = endTime
            getStartTime = startTime
            
            SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/ctm/find?testing=0"
            
            If useProxy = False Then
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
            Else
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                httpObj.setProxy 2, proxyUri
            End If
            
            httpObj.Open "POST", SendUrlStr, False
            httpObj.setRequestHeader "Content-Type", "text/plain"
            httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***Content-Type: text/plain
            
            ' 2016.07.20 Change ↓↓↓ ***************************
            ' 2016.07.22 Change ↓↓↓ *** POST文字列に「testing=0」を追加
            If cmsVersion = "3.5" Then
                SendString = "{""ctmId"":""" & CtmId & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & _
                               ",""mfHostName"":" & """" & mfIPAddr & """" & ",""mfPort"":" & """" & mfPortNo & """" & "}"
            Else
                SendString = "{""ctmId"":""" & CtmId & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & "}"
            End If
            ' 2016.07.22 Change ↑↑↑ ***************************
            ' 2016.07.20 Change ↑↑↑ ***************************
            
            bPmary = StrConv(SendString, vbFromUnicode)
            
            httpObj.send (SendString)
                        
            ' ダウンロード待ち
            Do While httpObj.readyState <> 4
                DoEvents
            Loop
            
            ' 2016.07.22 Add ↓↓↓ *****************************
            On Error GoTo ErrorCTMHandler
            ' 2016.07.22 Add ↑↑↑ *****************************
            '* 取得情報のデコード
            Set sc = CreateObject("ScriptControl")
            With sc
                .Language = "JScript"
        
                '指定したインデックス、名称のデータを取得する
                .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
            End With
'            GetString = convTextEncoding(httpObj.responseBody, "UTF-8")
            GetString = httpObj.responseText
            
            ' 2016.07.20 Add ***************************
            If cmsVersion = "3.5" Then
                chgStr01 = Replace(GetString, "\""", "@@")
                chgStr02 = Replace(chgStr01, """", "")
                GetString = Replace(chgStr02, "@@", """")
            End If
            ' 2016.07.20 Add ***************************
            
            Set objJSON = sc.CodeObject.jsonParse(GetString)
            
            Set sc = Nothing
            
            Set httpObj = Nothing
        
            '* 取得情報をCTM情報に展開
            iResult = GetCTMInfoForCTM(objJSON, currCTM, ctmValue)
            
            Kurikaeshi_GetMissionInfo = "SUCCESS2@@" + iResult

            Set objJSON = Nothing
        
        Next
    
    End If
    
    
    Exit Function

ErrorMissionHandler:

    Set sc = Nothing
    Set httpObj = Nothing
    Set objJSON = Nothing
    
    '* ミッションIDによる取得に失敗した場合
    For JJ = 3 To 10
    
        divideNum = JJ ^ 2
    
        divideTime = CLng((endTime - startTime) / divideNum)
        
        wkStartTime = startTime
        For II = 0 To divideNum - 1
            wkEndTime = wkStartTime + divideTime
            
'Debug.Print "divideNum=" & Format(divideNum) & ", II=" & Format(II)

            iResult = GetDivideMissionInfo(ctmList, ctmValue, missionID, wkStartTime, wkEndTime)

            '* 取得失敗したらループを抜け分割数を増加させる
            If iResult < 0 Then Exit For

'Debug.Print "wkStartTime=" & Format(wkStartTime) & ", wkEndTime=" & Format(wkEndTime)

            '* 取得CTM毎に処理
            For JJJ = 0 To UBound(ctmList)
        
                '* 取得データがあれば該当CTMシートへ出力
                If iResult > 0 Then
                    Call OutputCTMInfoToSheet(ctmList(JJJ).name, ctmValue, False)
                End If

                DoEvents
            Next
            
            wkStartTime = wkEndTime + 1#
            
            DoEvents
        Next
        
        '* 分割数分処理が終了したら分割処理ループを抜ける
        If II > (divideNum - 1) Then Exit For
        
        DoEvents
    Next
    
    Kurikaeshi_GetMissionInfo = "ERROR"
    
    Exit Function
    
    
ErrorCTMHandler:
    
    Set sc = Nothing
    Set httpObj = Nothing
    Set objJSON = Nothing
    
    '* CTM IDによる取得に失敗した場合
    For JJ = 3 To 10
    
        divideNum = JJ ^ 2
    
        divideTime = CLng((endTime - startTime) / divideNum)
        
        wkStartTime = startTime
        For II = 0 To divideNum - 1
            wkEndTime = wkStartTime + divideTime
            
'Debug.Print "divideNum=" & Format(divideNum) & ", II=" & Format(II)

            iResult = GetDivideCTMInfo(ctmList, ctmValue, wkStartTime, wkEndTime)

            '* 取得失敗したらループを抜け分割数を増加させる
            If iResult < 0 Then Exit For

'Debug.Print "wkStartTime=" & Format(wkStartTime) & ", wkEndTime=" & Format(wkEndTime)

            '* 取得CTM毎に処理
            For JJJ = 0 To UBound(ctmList)
        
                '* 取得データがあれば該当CTMシートへ出力
                If iResult > 0 Then
                    Call OutputCTMInfoToSheet(ctmList(JJJ).name, ctmValue, False)
                End If

                DoEvents
            Next
            
            wkStartTime = wkEndTime + 1#
            
            DoEvents
        Next
        
        '* 分割数分処理が終了したら分割処理ループを抜ける
        If II > (divideNum - 1) Then Exit For
        
        DoEvents
    Next

    Kurikaeshi_GetMissionInfo = "ERROR"
    
    Exit Function
    
End Function


'***********************************************
'* メインシートにあるミッション情報を取得する
'***********************************************
Public Sub GetCTMList_Colum(currSheetName As String, ctmList() As CTMINFO)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim CTMRange        As Range
    Dim elmRange        As Range
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim aryIndex        As Integer      ' CTM情報インデックス
    Dim aryIndex2       As Integer      ' エレメント情報インデックス
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    Dim elmCnt          As Integer
    
    'オンラインテンプレートシートの参照
    Set currWorksheet = Workbooks(thisBookName).Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim ctmList(0)
    
    With currWorksheet
        
        '* CTM NAMEの項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        
        For II = 0 To MaxCTMNumber Step 2
            Set CTMRange = startRange.Offset(II, 0)
            
            If CTMRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            If II = 0 Then
                aryIndex = 0
                ReDim ctmList(aryIndex)
            Else
                aryIndex = UBound(ctmList) + 1
                ReDim Preserve ctmList(aryIndex)
            End If
            ReDim ctmList(aryIndex).Element(0)
            ctmList(aryIndex).Element(0).name = ""
            
            '* CTM情報取得
            ctmList(aryIndex).name = CTMRange.Offset(0, 0).Value
            ctmList(aryIndex).ID = CTMRange.Offset(1, 0).Value
            
            'エレメント最終列
            elmCnt = CTMRange.Cells(1, Columns.Count).End(xlToLeft).Column
            
            '* エレメント情報取得
            Set elmRange = CTMRange.Offset(0, 1)
            For JJ = 0 To elmCnt
                If elmRange.Offset(0, JJ).Value <> "" Then
                    aryIndex2 = UBound(ctmList(aryIndex).Element)
                    If ctmList(aryIndex).Element(aryIndex2).name <> "" Then
                        aryIndex2 = aryIndex2 + 1
                        ReDim Preserve ctmList(aryIndex).Element(aryIndex2)
                    End If
                    ctmList(aryIndex).Element(aryIndex2).name = elmRange.Offset(0, JJ).Value
                    ctmList(aryIndex).Element(aryIndex2).ID = elmRange.Offset(1, JJ).Value
                    ctmList(aryIndex).Element(aryIndex2).colum_no = elmRange.Offset(0, JJ).Column
                
                End If
            Next
        Next
    
    End With
    
End Sub

'****************************************************
'* 収集したCTM情報をresultシートに転記する
'****************************************************
Public Sub OutputResultToSheet_Colum(ctmList As CTMINFO, rstWorksheet As Worksheet)
    Dim srcWorksheet        As Worksheet
    Dim prevWorksheet       As Worksheet
    Dim srchRange           As Range
    Dim lastRange           As Range
    Dim lastRangeR          As Range        '* 2016/09/13 Add
    Dim lastRangeC          As Range        '* 2016/09/13 Add
    Dim workRange           As Range
    Dim copyRange           As Range
    Dim destRange           As Range
    Dim destRangeStart      As Range
    Dim startRange          As Range
    Dim II                  As Long
    Dim pasteArray()        As Variant
    Dim pos                 As Integer
    Dim wrkCol
    Dim wrkStr

    '* 古いresultシート
    Set prevWorksheet = Workbooks(thisBookName).Worksheets(ResultSheetName)
    '* CTMシート
    Set srcWorksheet = Workbooks(thisBookName).Worksheets(ctmList.name)
    
    '* 旧resultシートからタイトル行をコピーする
'    Set copyRange = prevWorksheet.Range("1:1")
'    Set destRange = rstWorksheet.Range("1:1")
'    copyRange.Copy Destination:=destRange

    '* 新resultシートにデータを複写する
    ' CTMシートのﾃﾞｰﾀ有無ﾁｪｯｸ
'    Set srchRange = srcWorksheet.Range("A2")
    Set srchRange = srcWorksheet.Range("A1")
    If srchRange.Offset(1, 0).Value <> "" Then
        '* 2016/09/13 Change ↓↓↓********************************************
        Set lastRangeC = srchRange.Offset(0, 0).End(xlToRight)   '最終列
        Set lastRangeR = srchRange.Offset(0, 0).End(xlDown)      '最終行
        '* 2016/09/13 Change ↑↑↑********************************************
        wrkStr = lastRangeC.Column
        
        ' CTMシートのデータ範囲設定
        'Set copyRange = srcWorksheet.Range(srchRange.Cells, lastRange.Cells)
        
        'コピー先データ有無ﾁｪｯｸ=>最終行の検索
        Set destRangeStart = rstWorksheet.Range("A2")
        If destRangeStart.Offset(0, 0).Value <> "" Then
            '* 2016/09/13 Change ↓↓↓********************************************
            Set destRangeStart = destRangeStart.Offset(-1, 0).End(xlDown).Offset(1, 0)
'            Set destRangeStart = destRangeStart.Offset(0, 0).End(xlDown).Offset(1, 0)
            '* 2016/09/13 Change ↑↑↑********************************************
            wrkStr = destRangeStart.Row
        End If

        'コピー先開始位置決定
        'エレメント毎にデータの貼付け
        For pos = 0 To UBound(ctmList.Element)
            wrkCol = ctmList.Element(pos).colum_no
            wrkStr = ctmList.Element(pos).name
            
            Set startRange = srchRange.Offset(1, pos + 1)               '開始位置
            '* 2016/09/13 Change ↓↓↓********************************************
'            Set lastRange = srchRange.Offset(0, pos + 1).End(xlDown)    '最終行位置
            Set lastRange = srcWorksheet.Cells(lastRangeR.Row, startRange.Column)
            '* 2016/09/13 Change ↑↑↑********************************************
            Set copyRange = srcWorksheet.Range(startRange.Cells, lastRange.Cells)
            
            Set destRange = destRangeStart.Offset(0, wrkCol)
            copyRange.Copy Destination:=destRange
        Next pos
        
        'RECIEVE TIMEの設定
        Set startRange = srchRange.Offset(1, 0)             '開始位置
        '* 2016/09/13 Change ↓↓↓********************************************
'        Set lastRange = srchRange.Offset(0, 0).End(xlDown)  '最終行位置
        Set lastRange = srcWorksheet.Cells(lastRangeR.Row, startRange.Column)
        '* 2016/09/13 Change ↑↑↑********************************************
        Set copyRange = srcWorksheet.Range(startRange.Cells, lastRange.Cells)
            
        Set destRange = destRangeStart.Offset(0, 1)
        copyRange.Copy Destination:=destRange
        
        
'        Set destRange = rstWorksheet.Range(destRangeStart.Offset(0, 0), destRangeStart.Offset(lastRange.Row, lastRange.Column - 1))
'        Set destRange = destRange.Offset(0, 1)
'        copyRange.Copy Destination:=destRange
        
        '* CTM名を生成する
        '* 2016/09/13 Change ↓↓↓********************************************
'        II = lastRange.Offset(0, 0).Row - srchRange.Row
        II = lastRangeR.Offset(0, 0).Row - srchRange.Row - 1
        '* 2016/09/13 Change ↑↑↑********************************************
        ReDim pasteArray(II)
        For II = 0 To UBound(pasteArray)
            pasteArray(II) = ctmList.name
        Next
        
        '* CTM名を貼り付ける
        Set lastRange = destRangeStart.Offset(UBound(pasteArray), 0)
        Set copyRange = rstWorksheet.Range(destRangeStart.Cells, lastRange.Cells)
        copyRange = pasteArray
        
        '* RECEIVE TIME列の書式設定
        With rstWorksheet
            Set workRange = rstWorksheet.Range(.Range("B2"), .Range("B2").End(xlDown).Cells)
            workRange.NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
            workRange.EntireColumn.AutoFit
        End With
    End If
    
End Sub
