VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmBackData 
   Caption         =   "データアタッチ"
   ClientHeight    =   4935
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   5325
   OleObjectBlob   =   "frmBackData.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "frmBackData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Option Explicit
Dim linePos As Integer
'設定処理
Private Sub CommandButton1_Click()
    Dim pos                         As Integer
    Dim dataCnt                  As Integer
    Dim currWorksheet       As Worksheet
    Dim srchRange              As Range
    Dim cellRange              As Range
    Dim endCell                    As String
    
    'Application.Visible = False
    Application.ScreenUpdating = False
    
    '背景データセット
    dataCnt = 0
    Set currWorksheet = Worksheets("背景データ")
    Set srchRange = currWorksheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        For pos = 1 To 10
            If Trim(frmBackData.Controls("txtName" & pos).Value) <> "" Then
                srchRange.Offset(1 + pos, 0).Value = Trim(frmBackData.Controls("txtName" & pos).Value)
                srchRange.Offset(1 + pos, 1).Value = Trim(frmBackData.Controls("txtVal" & pos).Value)
                srchRange.Offset(1 + pos, 2).Value = Trim(frmBackData.Controls("txtUnit" & pos).Value)
                'データ名称セル設定
                'Call DataNameCellSet(pos)
                '値・単位セル設定
                'Call ValueUnitCellSet(pos)
                dataCnt = pos
            Else
                srchRange.Offset(1 + pos, 0).Value = ""
                srchRange.Offset(1 + pos, 1).Value = ""
                srchRange.Offset(1 + pos, 2).Value = ""
            End If
        Next pos
    End If
    
    '文字列幅設定
    Set srchRange = currWorksheet.Columns("C:E")
    srchRange.EntireColumn.AutoFit
        If currWorksheet.Columns("C").ColumnWidth < 20 Then
        currWorksheet.Columns("C").ColumnWidth = 20
    End If
        If currWorksheet.Columns("D").ColumnWidth < 25 Then
        currWorksheet.Columns("D").ColumnWidth = 25
    End If
        If currWorksheet.Columns("E").ColumnWidth < 10 Then
        currWorksheet.Columns("E").ColumnWidth = 10
    End If
    '背景データセルの自動整形
    Call cellMojiAlignment
    
    '背景データのコピー
    endCell = "E" & CStr(dataCnt + 4)
    currWorksheet.Range("C4:" & endCell).Copy
    
    '貼付け位置の決定
    Set currWorksheet = ActiveSheet
    currWorksheet.Select
    Set cellRange = ActiveCell
    cellRange.Select
    
    '背景データ　ペースト
    ActiveSheet.Pictures.Paste.Select
'    ActiveSheet.Pictures.Paste(link:=True).Select
'    ActiveCell.Select
'    ActiveSheet.Paste
    
End Sub

'ｷｬﾝｾﾙ処理
Private Sub CommandButton2_Click()
    Dim pos                     As Integer
    Dim txtPos                 As Integer
    Dim wrkName(10)      As String
    Dim wrkVal(10)           As String
    Dim wrkUnit(10)          As String
    Dim rc
    
    If linePos <= 0 Then
        Exit Sub
    End If
    
    '１行目の処理
    'If linePos <= 1 Then
        If frmBackData.Controls("txtName" & linePos).Value = "" And _
           frmBackData.Controls("txtVal" & linePos).Value = "" And _
           frmBackData.Controls("txtUnit" & linePos).Value = "" Then
           Exit Sub
        End If
    'End If

    rc = MsgBox("次の背景データを削除しますか？" & vbLf & _
           "データ名称：" & frmBackData.Controls("txtName" & linePos).Value & vbLf & _
           "値：" & frmBackData.Controls("txtVal" & linePos).Value & vbLf & _
           "単位：" & frmBackData.Controls("txtUnit" & linePos).Value, vbYesNo)
           
    If rc <> vbYes Then
        Exit Sub
    End If
    
    '選択行の削除
    frmBackData.Controls("txtName" & linePos).Value = ""
    frmBackData.Controls("txtVal" & linePos).Value = ""
    frmBackData.Controls("txtUnit" & linePos).Value = ""
    'linePos = 0
        
    '現状値保存
    For pos = 1 To 10
        wrkName(pos) = frmBackData.Controls("txtName" & pos).Value
        wrkVal(pos) = frmBackData.Controls("txtVal" & pos).Value
         wrkUnit(pos) = frmBackData.Controls("txtUnit" & pos).Value
    
        frmBackData.Controls("txtName" & pos).Value = ""
        frmBackData.Controls("txtVal" & pos).Value = ""
        frmBackData.Controls("txtUnit" & pos).Value = ""
    Next pos
    
    '空白項目前詰め
    txtPos = 0
    For pos = 1 To 10
        If wrkName(pos) <> "" Then
            txtPos = txtPos + 1
            frmBackData.Controls("txtName" & txtPos).Value = wrkName(pos)
            frmBackData.Controls("txtVal" & txtPos).Value = wrkVal(pos)
            frmBackData.Controls("txtUnit" & txtPos).Value = wrkUnit(pos)
        End If
    Next pos
    
    '空欄部分を非表示
    For pos = 1 To 10
        If frmBackData.Controls("txtName" & pos).Value = "" Then
            frmBackData.Controls("txtName" & pos).Visible = False
            frmBackData.Controls("txtVal" & pos).Visible = False
            frmBackData.Controls("txtUnit" & pos).Visible = False
        End If
    Next pos
    
    '追加行の表示
    If txtPos < 10 Then
        frmBackData.Controls("txtName" & txtPos + 1).Visible = True
        frmBackData.Controls("txtVal" & txtPos + 1).Visible = True
        frmBackData.Controls("txtUnit" & txtPos + 1).Visible = True
    End If
    
       '再登録
    Call CommandButton1_Click
End Sub

'GetFocus***
Private Sub txtName1_Enter()
    linePos = getFocus(txtName1)
End Sub
Private Sub txtVal1_Enter()
    linePos = getFocus(txtVal1)
End Sub
Private Sub txtUnit1_Enter()
    linePos = getFocus(txtUnit1)
End Sub
Private Sub txtName2_Enter()
    linePos = getFocus(txtName2)
End Sub
Private Sub txtVal2_Enter()
    linePos = getFocus(txtVal2)
End Sub
Private Sub txtUnit2_Enter()
    linePos = getFocus(txtUnit2)
End Sub
Private Sub txtName3_Enter()
    linePos = getFocus(txtName3)
End Sub
Private Sub txtVal3_Enter()
    linePos = getFocus(txtVal3)
End Sub
Private Sub txtUnit3_Enter()
    linePos = getFocus(txtUnit3)
End Sub
Private Sub txtName4_Enter()
    linePos = getFocus(txtName4)
End Sub
Private Sub txtVal4_Enter()
    linePos = getFocus(txtVal4)
End Sub
Private Sub txtUnit4_Enter()
    linePos = getFocus(txtUnit4)
End Sub

Private Sub txtName5_Enter()
    linePos = getFocus(txtName5)
End Sub
Private Sub txtVal5_Enter()
    linePos = getFocus(txtVal5)
End Sub
Private Sub txtUnit5_Enter()
    linePos = getFocus(txtUnit5)
End Sub
Private Sub txtName6_Enter()
    linePos = getFocus(txtName6)
End Sub
Private Sub txtVal6_Enter()
    linePos = getFocus(txtVal6)
End Sub
Private Sub txtUnit6_Enter()
    linePos = getFocus(txtUnit6)
End Sub
Private Sub txtName7_Enter()
    linePos = getFocus(txtName7)
End Sub
Private Sub txtVal7_Enter()
    linePos = getFocus(txtVal7)
End Sub
Private Sub txtUnit7_Enter()
    linePos = getFocus(txtUnit7)
End Sub
Private Sub txtName8_Enter()
    linePos = getFocus(txtName8)
End Sub
Private Sub txtVal8_Enter()
    linePos = getFocus(txtVal8)
End Sub
Private Sub txtUnit8_Enter()
    linePos = getFocus(txtUnit8)
End Sub
Private Sub txtName9_Enter()
    linePos = getFocus(txtName9)
End Sub
Private Sub txtVal9_Enter()
    linePos = getFocus(txtVal9)
End Sub
Private Sub txtUnit9_Enter()
    linePos = getFocus(txtUnit9)
End Sub
Private Sub txtName10_Enter()
    linePos = getFocus(txtName10)
End Sub
Private Sub txtVal10_Enter()
    linePos = getFocus(txtVal10)
End Sub
Private Sub txtUnit10_Enter()
    linePos = getFocus(txtUnit10)
End Sub

Private Sub txtName1_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtName1.text = ActiveCell.Value
    txtVal1.SetFocus
End Sub
Private Sub txtVal1_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtVal1.text = ActiveCell.Value
    txtUnit1.SetFocus
End Sub
Private Sub txtUnit1_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
        
    txtUnit1.text = ActiveCell.Value
    txtName1.SetFocus
End Sub

Private Sub txtName2_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtName2.text = ActiveCell.Value
    txtVal2.SetFocus
End Sub

Private Sub txtVal2_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtVal2.text = ActiveCell.Value
    txtUnit2.SetFocus
End Sub
Private Sub txtUnit2_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtUnit2.text = ActiveCell.Value
    txtName2.SetFocus
End Sub

Private Sub txtName3_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtName3.text = ActiveCell.Value
    txtVal3.SetFocus
End Sub
Private Sub txtVal3_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtVal3.text = ActiveCell.Value
    txtUnit3.SetFocus
End Sub
Private Sub txtUnit3_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtUnit3.text = ActiveCell.Value
    txtName3.SetFocus
End Sub
Private Sub txtName4_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtName4.text = ActiveCell.Value
    txtVal4.SetFocus
End Sub
Private Sub txtVal4_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtVal4.text = ActiveCell.Value
    txtUnit4.SetFocus
End Sub
Private Sub txtUnit4_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtUnit4.text = ActiveCell.Value
    txtName4.SetFocus
End Sub
Private Sub txtName5_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtName5.text = ActiveCell.Value
    txtVal5.SetFocus
End Sub
Private Sub txtVal5_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtVal5.text = ActiveCell.Value
    txtUnit5.SetFocus
End Sub
Private Sub txtUnit5_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtUnit5.text = ActiveCell.Value
    txtName5.SetFocus
End Sub
Private Sub txtName6_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtName6.text = ActiveCell.Value
    txtVal6.SetFocus
End Sub
Private Sub txtVal6_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtVal6.text = ActiveCell.Value
    txtUnit6.SetFocus
End Sub
Private Sub txtUnit6_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtUnit6.text = ActiveCell.Value
    txtName6.SetFocus
End Sub
Private Sub txtName7_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtName7.text = ActiveCell.Value
    txtVal7.SetFocus
End Sub
Private Sub txtVal7_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtVal7.text = ActiveCell.Value
    txtUnit7.SetFocus
End Sub
Private Sub txtUnit7_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtUnit7.text = ActiveCell.Value
    txtName7.SetFocus
End Sub
Private Sub txtName8_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtName8.text = ActiveCell.Value
    txtVal8.SetFocus
End Sub
Private Sub txtVal8_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtVal8.text = ActiveCell.Value
    txtUnit8.SetFocus
End Sub
Private Sub txtUnit8_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtUnit8.text = ActiveCell.Value
    txtName8.SetFocus
End Sub
Private Sub txtName9_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtName9.text = ActiveCell.Value
    txtVal9.SetFocus
End Sub
Private Sub txtVal9_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtVal9.text = ActiveCell.Value
    txtUnit9.SetFocus
End Sub
Private Sub txtUnit9_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtUnit9.text = ActiveCell.Value
    txtName9.SetFocus
End Sub
Private Sub txtName10_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtName10.text = ActiveCell.Value
    txtVal10.SetFocus
End Sub
Private Sub txtVal10_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtVal10.text = ActiveCell.Value
    txtUnit10.SetFocus
End Sub
Private Sub txtUnit10_DblClick(ByVal Cancel As MSForms.ReturnBoolean)
    If ActiveCell.Value = "" Then Exit Sub
    
    txtUnit10.text = ActiveCell.Value
    txtName10.SetFocus
End Sub

Private Sub txtName1_AfterUpdate()
    Call backDataSetClear(txtName1, txtName1.text)
End Sub
Private Sub txtName2_AfterUpdate()
    Call backDataSetClear(txtName2, txtName2.text)
End Sub
Private Sub txtName3_AfterUpdate()
    Call backDataSetClear(txtName3, txtName3.text)
End Sub
Private Sub txtName4_AfterUpdate()
    Call backDataSetClear(txtName4, txtName4.text)
End Sub
Private Sub txtName5_AfterUpdate()
    Call backDataSetClear(txtName5, txtName5.text)
End Sub
Private Sub txtName6_AfterUpdate()
    Call backDataSetClear(txtName6, txtName6.text)
End Sub
Private Sub txtName7_AfterUpdate()
    Call backDataSetClear(txtName7, txtName7.text)
End Sub
Private Sub txtName8_AfterUpdate()
    Call backDataSetClear(txtName8, txtName8.text)
End Sub
Private Sub txtName9_AfterUpdate()
    Call backDataSetClear(txtName9, txtName9.text)
End Sub
Private Sub txtName10_AfterUpdate()
    Call backDataSetClear(txtName10, txtName10.text)
End Sub

Private Sub UserForm_Activate()
    Dim pos                          As Integer
    Dim txtPos                      As Integer
    Dim currWorksheet        As Worksheet
    Dim srchRange              As Range
    
    linePos = 0
    'テキストBOX表示
    For pos = 2 To 10
        frmBackData.Controls("txtName" & pos).Visible = False
        frmBackData.Controls("txtVal" & pos).Visible = False
        frmBackData.Controls("txtUnit" & pos).Visible = False
    Next pos

    txtPos = 0
    Set currWorksheet = Worksheets("背景データ")
    Set srchRange = currWorksheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        For pos = 1 To 10
            If srchRange.Offset(1 + pos, 0).Value <> "" Then
                frmBackData.Controls("txtName" & pos).Value = srchRange.Offset(1 + pos, 0).Value
                frmBackData.Controls("txtVal" & pos).Value = srchRange.Offset(1 + pos, 1).Value
                frmBackData.Controls("txtUnit" & pos).Value = srchRange.Offset(1 + pos, 2).Value
                frmBackData.Controls("txtName" & pos).Visible = True
                frmBackData.Controls("txtVal" & pos).Visible = True
                frmBackData.Controls("txtUnit" & pos).Visible = True
                txtPos = txtPos + 1
            Else
                frmBackData.Controls("txtName" & pos).Value = ""
                frmBackData.Controls("txtVal" & pos).Value = ""
                frmBackData.Controls("txtUnit" & pos).Value = ""
            End If
        Next pos
    End If
    
    '空白行の追加
    If txtPos < 10 Then
        frmBackData.Controls("txtName" & txtPos + 1).Visible = True
        frmBackData.Controls("txtVal" & txtPos + 1).Visible = True
        frmBackData.Controls("txtUnit" & txtPos + 1).Visible = True
    End If
    
End Sub

'背景データ選択位置
Private Function getFocus(argTxt As Object) As Integer
    Dim pos As Integer
    getFocus = 0
    
    '選択テキストチェック
    For pos = 1 To 10
        If argTxt.Name = frmBackData.Controls("txtName" & pos).Name Or _
           argTxt.Name = frmBackData.Controls("txtVal" & pos).Name Or _
           argTxt.Name = frmBackData.Controls("txtUnit" & pos).Name Then
           getFocus = pos
           Exit For
        End If
    Next pos
    
End Function

Public Sub backDataSetClear(argTxtBox As Object, argText As String)
    Dim wrkStr      As String
    
        If argText <> "" Then
            Select Case argTxtBox.Name
                Case "txtName1"
                        If txtName2.Visible = True Then
                            Exit Sub
                        End If
                        txtName2.text = ""
                        txtVal2.text = ""
                        txtUnit2.text = ""
                        txtName2.Visible = True
                        txtVal2.Visible = True
                        txtUnit2.Visible = True
                Case "txtName2"
                        If txtName3.Visible = True Then
                            Exit Sub
                        End If
                        txtName3.text = ""
                        txtVal3.text = ""
                        txtUnit3.text = ""
                        txtName3.Visible = True
                        txtVal3.Visible = True
                        txtUnit3.Visible = True
                Case "txtName3"
                        If txtName4.Visible = True Then
                            Exit Sub
                        End If
                        txtName4.text = ""
                        txtVal4.text = ""
                        txtUnit4.text = ""
                        txtName4.Visible = True
                        txtVal4.Visible = True
                        txtUnit4.Visible = True
                Case "txtName4"
                        If txtName5.Visible = True Then
                            Exit Sub
                        End If
                        txtName5.text = ""
                        txtVal5.text = ""
                        txtUnit5.text = ""
                        txtName5.Visible = True
                        txtVal5.Visible = True
                        txtUnit5.Visible = True
                Case "txtName5"
                        If txtName6.Visible = True Then
                            Exit Sub
                        End If
                        txtName6.text = ""
                        txtVal6.text = ""
                        txtUnit6.text = ""
                        txtName6.Visible = True
                        txtVal6.Visible = True
                        txtUnit6.Visible = True
                Case "txtName6"
                        If txtName7.Visible = True Then
                            Exit Sub
                        End If
                        txtName7.text = ""
                        txtVal7.text = ""
                        txtUnit7.text = ""
                        txtName7.Visible = True
                        txtVal7.Visible = True
                        txtUnit7.Visible = True
                Case "txtName7"
                        If txtName8.Visible = True Then
                            Exit Sub
                        End If
                        txtName8.text = ""
                        txtVal8.text = ""
                        txtUnit8.text = ""
                        txtName8.Visible = True
                        txtVal8.Visible = True
                        txtUnit8.Visible = True
                Case "txtName8"
                        If txtName9.Visible = True Then
                            Exit Sub
                        End If
                        txtName9.text = ""
                        txtVal9.text = ""
                        txtUnit9.text = ""
                        txtName9.Visible = True
                        txtVal9.Visible = True
                        txtUnit9.Visible = True
                Case "txtName9"
                        If txtName10.Visible = True Then
                            Exit Sub
                        End If
                        txtName10.text = ""
                        txtVal10.text = ""
                        txtUnit10.text = ""
                        txtName10.Visible = True
                        txtVal10.Visible = True
                        txtUnit10.Visible = True
                Case "txtName10"
            End Select
            Exit Sub
        End If
End Sub

Sub cellMojiAlignment()
    Dim currWorksheet        As Worksheet
    Dim srchRange              As Range

    Set currWorksheet = Worksheets("背景データ")
    Set srchRange = currWorksheet.Range("C5:C14")
    With srchRange
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    With srchRange
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With

    Set currWorksheet = Worksheets("背景データ")
    Set srchRange = currWorksheet.Range("D5:D14")
    With srchRange
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    With srchRange
        .HorizontalAlignment = xlRight
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With

    Set currWorksheet = Worksheets("背景データ")
    Set srchRange = currWorksheet.Range("E5:E14")
    With srchRange
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    With srchRange
        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    With srchRange
        .HorizontalAlignment = xlLeft
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
    With srchRange
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .IndentLevel = 0
        .ShrinkToFit = False
        .ReadingOrder = xlContext
        .MergeCells = False
    End With
End Sub

