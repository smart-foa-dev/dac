Attribute VB_Name = "Mission_Module"
Option Explicit
'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
'FOR COM高速化 zhengxiaoxue  2018/06/07 Modify Start
'Public Sub GetMissionInfoAll()
Public Sub GetMissionInfoAll(sType As String)
'FOR COM高速化 zhengxiaoxue  2018/06/07 Modify End
    Dim ctmList()   As CTMINFO      ' CTM情報
    Dim ctmValue()  As CTMINFO      ' CTM取得情報
    
    
    Dim II          As Integer
    
    Dim result     As String
    Dim resultSuccess() As String
    
    Dim iResult As Integer
    
    'FOR COM高速化 dn 2018/06/13 Modify Start
    result = GetMissionInfo(ctmList, ctmValue, sType)
    
    
    If result = "ERROR" Then
        Call ChangeStartStopBT
        MsgBox thisBookName & "　：　ミッションデータ取込失敗！！", vbCritical
        End
    End If
    
    resultSuccess = Split(result, "@@")
    
'    If resultSuccess(0) = "SUCCESS1" Then
'
'        Dim PathName As String, FileName As String, pos As Long
'
'        ' ディレクトリを取得
'        pos = InStrRev(resultSuccess(1), "\")
'        PathName = Left(resultSuccess(1), pos)
'
'
'        '* CTM種別でシートに出力
'        For II = 0 To UBound(ctmList)
'            FileName = PathName + ctmList(II).ID + ".csv"
'
'            Call OutputCTMInfoToSheetFromCsv(ctmList(II).Name, 0#, FileName)
'        Next
'    Else

    ' ミッションIDがない場合(find分岐)
    'If resultSuccess(0) = "SUCCESS2" Then
    If UBound(resultSuccess) > 0 And resultSuccess(0) = "SUCCESS2" Then

        iResult = CInt(resultSuccess(1))
        '* シート毎（シート名＝CTM名）へ出力
        If iResult > 0 Then
            '* CTM種別でシートに出力
            For II = 0 To UBound(ctmList)
                
                Call OutputCTMInfoToSheet(ctmList(II).Name, 0#, ctmValue)
            
            Next
        End If
    
    End If
    'FOR COM高速化 dn 2018/06/13 Modify End
    
End Sub


'****************************************************************
'* 指定ミッションの情報を取得する
'* ※ParamシートにミッションID、CTMエレメント一覧が必要
'****************************************************************

'FOR COM高速化 zhengxiaoxue  2018/06/07 Modify Start
'Private Function GetMissionInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO) As String
Private Function GetMissionInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO, sType As String) As String
'FOR COM高速化 zhengxiaoxue  2018/06/07 Modify End

    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim missionID   As String
    Dim ctmID       As String
    Dim startTime   As Double
    Dim endTime     As Double
    Dim SendUrlStr  As String
    Dim SendString  As String
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim dispTerm    As Double
    Dim dispTermUnit As String
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim iResult     As Integer
    Dim bPmary      As Variant
    Dim II          As Integer
    Dim currCTM     As CTMINFO
    
    Dim cmsVersion  As String
    Dim mfIPAddr    As String
    Dim mfPortNo    As String
    Dim chgStr01    As String
    Dim chgStr02    As String
    
    Dim jsonParse As New FoaCoreCom.jsonParse
    Dim keyOrder() As String
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    'FOR COM高速化 zhengxiaoxue  2018/06/07 Add Start
    Dim ctmData As FoaCoreCom.CtmDataRetriever
    Dim csvWrite As FoaCoreCom.CsvWriteToExcelHandler
    Dim zipFile As FoaCoreCom.ZipFileHandler
    
    Dim zipUrl As String
    Dim unZipUrl As String
    Dim msg As String
    'FOR COM高速化 zhengxiaoxue  2018/06/07 Add End
    
    '* メインシートから各種情報を取得
    With Worksheets(ParamSheetName)
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得開始", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(srchRange.Offset(0, 1).Value)
            startT = TimeValue(srchRange.Offset(0, 1).Value)
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If
        startTime = GetUnixTime(startD + startT)
        
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = DateValue(srchRange.Offset(0, 1).Value)
            endT = TimeValue(srchRange.Offset(0, 1).Value)
        Else
            endD = DateValue("2020/12/31 23:59:59")
            endT = TimeValue("2020/12/31 23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If
        endTime = GetUnixTime(endD + endT)
        
        '*********************
        '* ミッションIDを取得
        '*********************
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "ミッションID項目が見つからないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If
'        If missionID = "" Then
'            MsgBox "ミッションIDが見つからないため処理を中断します。"
'            GetMissionInfo = -1
'            Exit Function
'        End If

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If

        ' 2016.07.20 Add *********************
        '* CMSバージョンを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="CmsVersion", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            cmsVersion = srchRange.Offset(0, 1).Value
        Else
            cmsVersion = "V5"
        End If
        
        ' 2016.07.20 Add *********************
        '* 旧バージョン用IPアドレスおよびPortNoを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="MfHost", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            mfIPAddr = srchRange.Offset(0, 1).Value
            mfPortNo = srchRange.Offset(1, 1).Value
        Else
            mfIPAddr = "localhost"
            mfPortNo = "50031"
        End If
        
        '*******************************
        '* 表示期間を取得
        '*******************************
        Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                dispTerm = 0#
                dispTermUnit = "【時】"
            Else
                dispTerm = srchRange.Offset(0, 1).Value
                dispTermUnit = srchRange.Offset(0, 2).Value
            End If
        Else
            dispTerm = 0#
            dispTermUnit = "【時】"
        End If
        Select Case dispTermUnit
            Case "【分】"
                dispTerm = dispTerm * 60
            Case "【時】"
                dispTerm = dispTerm * 3600
        End Select
    
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
    
    
    End With
    
    '* CTM情報一覧をシートから取得
    Call GetCTMList(ParamSheetName, ctmList)
    
    '*******************************
    '* プログラムミッションに問合せ
    '*******************************
On Error GoTo ERROR_STEP

    '* ミッションIDがあれば
    If missionID <> "" Then
        getEndTime = endTime
        getStartTime = startTime
        
        '* For Com高速化 dn 2018/06/13 Modify Start
        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pmzip?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & "&limit=" & Format(LimitCTMNumber) & "&lang=ja"
        

'        If useProxy = False Then
'            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'        Else
'            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'            httpObj.setProxy 2, proxyUri
'        End If
'
'        httpObj.Open "GET", SendUrlStr, False
'        httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
'        httpObj.send
'
'        ' ダウンロード待ち
'        Do While httpObj.readyState <> 4
'            DoEvents
'        Loop
'
'        '* 取得情報のデコード
'        Set sc = CreateObject("ScriptControl")
'        With sc
'            .Language = "JScript"
'
'            '指定したインデックス、名称のデータを取得する
'            .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
'        End With
'        GetString = httpObj.responseText


        ' thisBookNameへカレントExcel対象をセット
        Call SetFilenameToVariable

        Set ctmData = CreateObject("FoaCoreCom.ais.retriever.CtmDataRetriever")
        Set csvWrite = CreateObject("FoaCoreCom.ais.retriever.CsvWriteToExcelHandler")
        Set zipFile = CreateObject("FoaCoreCom.ais.retriever.ZipFileHandler")
        msg = ""
        
        '情報を格納するZIPファイルパスを取得する
        zipUrl = ctmData.GetProgramMissionZipFileAsync(SendUrlStr, proxyUri, msg)
        
        
        '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
        If InStr(msg, "MISSION_NOT_FOUND") > 0 Then
            MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
            
            '周期起動停止
            'Call ChangeStartStopBT
            GetMissionInfo = -1
            Exit Function
        End If
        '**********
        
        ' Zipファイルを解凍する
        unZipUrl = zipFile.unZipFile(zipUrl)
        
        ' excelへ書き込む
        Call csvWrite.ReadAllCtmInfoToExcel(Workbooks(thisBookName), unZipUrl, MaxCntImport, sType, GetTimezoneId(), ParamSheetName)
        
        '一時フォルダを削除
        zipFile.deleteFolder (unZipUrl)
        
        GetMissionInfo = 1
        'Set objJSON = sc.CodeObject.jsonParse(GetString)
        'Set sc = Nothing
        'Set httpObj = Nothing
    
        ''* 取得情報をCTM情報に展開
        'ReDim ctmValue(0)
        'ctmValue(0).ID = ""
        'iResult = GetCTMInfo(objJSON, ctmList, ctmValue)
        
        '* 取得情報のデコード
'        GetString = httpObj.responseText
'
'
'        GetMissionInfo = "SUCCESS1@@" + jsonParse.buildCsvFileCtmMissionRtCtm(GetString, GetTimezoneId, keyOrder)
'
'        Set httpObj = Nothing
'
        
    '* ミッションIDがなければ
    Else
    
        ReDim ctmValue(0)
        ctmValue(0).ID = ""
        For II = 0 To UBound(ctmList)
    
            currCTM = ctmList(II)
            ctmID = currCTM.ID
        
            getEndTime = endTime
            getStartTime = startTime
            
            SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/ctm/find?testing=0"
            
            If useProxy = False Then
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
            Else
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                httpObj.setProxy 2, proxyUri
            End If
            
            httpObj.Open "POST", SendUrlStr, False
            httpObj.setRequestHeader "Content-Type", "text/plain"
            httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***Content-Type: text/plain
            
            ' 2016.07.20 Change ***************************
            If cmsVersion = "3.5" Then
                SendString = "{""ctmId"":""" & ctmID & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & ",""mfHostName"":" & """" & mfIPAddr & """" & ",""mfPort"":" & """" & mfPortNo & """" & "}"
            Else
                SendString = "{""ctmId"":""" & ctmID & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & "}"
            End If
            ' 2016.07.20 Change ***************************
            
            bPmary = StrConv(SendString, vbFromUnicode)
        
            ' 2016.07.20 Change *********************
'            Call httpObj.send(SendString)
            httpObj.send (SendString)
            
            ' ダウンロード待ち
            Do While httpObj.readyState <> 4
                DoEvents
            Loop
            
            '* 取得情報のデコード
            Set sc = CreateObject("ScriptControl")
            With sc
                .Language = "JScript"
        
                '指定したインデックス、名称のデータを取得する
                .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
            End With
'            GetString = convTextEncoding(httpObj.responseBody, "UTF-8")
            GetString = httpObj.responseText
            
            ' 2016.07.20 Add ***************************
            If cmsVersion = "3.5" Then
                chgStr01 = Replace(GetString, "\""", "@@")
                chgStr02 = Replace(chgStr01, """", "")
                GetString = Replace(chgStr02, "@@", """")
            End If
            ' 2016.07.20 Add ***************************
            
            Set objJSON = sc.CodeObject.jsonParse(GetString)
        
            Set sc = Nothing
            
            Set httpObj = Nothing
        
            '* 取得情報をCTM情報に展開
            iResult = GetCTMInfoForCTM(objJSON, currCTM, ctmValue)
    
            ' MsgBox http.responseText
            GetMissionInfo = "SUCCESS2@@" + iResult
            
            Set objJSON = Nothing
        
        Next
    
    End If
    
    Exit Function

ERROR_STEP: 'エラーSTEP追加

    GetMissionInfo = "ERROR"
    Exit Function
    
End Function

'*************************************************************************
'* 取得したJSON情報をデコードしCTM情報およびエレメント情報として展開する
'* →CTM IDによる取得
'*************************************************************************
Public Function GetCTMInfoForCTM(objJSON As Object, ctmList As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim recno           As Integer
    Dim II              As Variant
    Dim rec             As Object
    Dim rec2            As Object
    Dim rec3            As Object
    Dim rec4            As Object
    Dim aryIndex        As Integer
    Dim aryIndex2       As Integer
    Dim ctmIndex        As Integer
    Dim workElm         As ELMINFO
    Dim iFirstFlag      As Boolean
    
    iFirstFlag = True
    
    ' 件数カウンタ初期化
    recno = 0
    
    '* CTM取得件数分ループ
    For Each rec2 In objJSON

        If ctmValue(0).ID = "" Then
            aryIndex = 0
        Else
            aryIndex = UBound(ctmValue) + 1
            ReDim Preserve ctmValue(aryIndex)
        End If

        ReDim ctmValue(aryIndex).Element(0)
        ctmValue(aryIndex).Element(0).Name = ""
        
        '* CTM情報の取得
        ctmValue(aryIndex).Name = ctmList.Name
        ctmValue(aryIndex).ID = ctmList.ID
        ctmValue(aryIndex).RecvTime = GetJSONValue(rec2, "RT")

        '* 件数カウンタアップ
        recno = recno + 1
        
        '* エレメント数分の値取得
        Set rec3 = CallByName(rec2, "EL", VbGet)
        For II = 0 To UBound(ctmList.Element)
        
            workElm = ctmList.Element(II)
            
            Set rec4 = CallByName(rec3, workElm.ID, VbGet)
            
            aryIndex2 = UBound(ctmValue(aryIndex).Element)
            If ctmValue(aryIndex).Element(aryIndex2).Name <> "" Then
                aryIndex2 = aryIndex2 + 1
                ReDim Preserve ctmValue(aryIndex).Element(aryIndex2)
            End If
            
            '* エレメント情報の取得
            ctmValue(aryIndex).Element(aryIndex2).Name = workElm.Name
            ctmValue(aryIndex).Element(aryIndex2).ID = workElm.ID
            ctmValue(aryIndex).Element(aryIndex2).Type = CInt(GetJSONValue(rec4, "T"))
            On Error Resume Next
            ctmValue(aryIndex).Element(aryIndex2).Value = GetJSONValue(rec4, "V")
            If ctmValue(aryIndex).Element(aryIndex2).Value = "" Then ctmValue(aryIndex).Element(aryIndex2).Value = " "
            
        Next
        
    Next
        
    GetCTMInfoForCTM = recno
    
End Function

'***********************************************
'* メインシートにあるミッション情報を取得する
'***********************************************
Public Sub GetKeyOrder(currSheetName As String, keyOrder() As String)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim ctmRange        As Range
    Dim elmRange        As Range
    Dim currWorksheet   As Worksheet
    Dim ctmIndex        As Integer      ' CTM情報インデックス
    Dim elmIndex        As Integer      ' CTMID,エレメント情報インデックス
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    Dim maxRow          As Integer
    Dim maxCol          As Integer
    
    Set currWorksheet = Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim keyOrder(0)
    
    With currWorksheet
    
        '* CTM名称の項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        With srchRange.SpecialCells(xlCellTypeLastCell)
            maxRow = .Row - srchRange.Row + 1
            maxCol = .Column - srchRange.Column + 1
        End With
        
        ctmIndex = 0
        ReDim keyOrder(maxRow / 2, maxCol)
        
        For II = 0 To maxRow Step 2
            Set ctmRange = startRange.Offset(II, 0)
            elmIndex = 0
            
            If ctmRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            '* CTM情報取得
            keyOrder(ctmIndex, elmIndex) = ctmRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = ctmRange.Offset(0, 1)
            For JJ = 0 To maxCol - 1
                elmIndex = elmIndex + 1
                keyOrder(ctmIndex, elmIndex) = elmRange.Offset(1, JJ).Value
            Next
            ctmIndex = ctmIndex + 1
        Next
    End With
    
End Sub

'*************************************************************************
'* 取得したJSON情報をデコードしCTM情報およびエレメント情報として展開する
'*************************************************************************
Public Function GetCTMInfo(objJSON As Object, ctmList() As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim recno           As Integer
    Dim II              As Integer
    Dim rec             As Object
    Dim rec2            As Object
    Dim rec3            As Object
    Dim rec4            As Object
    Dim aryIndex        As Variant
    Dim aryIndex2       As Variant
    Dim ctmIndex        As Variant
    Dim workElm         As ELMINFO
    Dim iFirstFlag      As Boolean
    
    iFirstFlag = True
    
    ' 件数カウンタ初期化
    recno = 0
    
    '* CTM種別数分ループ
    For Each rec In objJSON
    
        '* 対象CTMを確定する
        For II = 0 To UBound(ctmList)
            If ctmList(II).ID = GetJSONValue(rec, "id") Then
                ctmIndex = II
                Exit For
            End If
        Next
        
        If II > UBound(ctmList) Then
            MsgBox "ミッションからの取得情報に該当するCTMがありませんでした。" & vbCrLf & "処理を中断します。"
            Exit Function
        End If
        
        '* CTM取得件数分ループ
        For Each rec2 In rec.ctms

            If iFirstFlag Then
                aryIndex = 0
                ReDim ctmValue(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(ctmValue) + 1
                ReDim Preserve ctmValue(aryIndex)
            End If
            ReDim ctmValue(aryIndex).Element(0)
            ctmValue(aryIndex).Element(0).Name = ""
            
            '* CTM情報の取得
            ctmValue(aryIndex).Name = ctmList(ctmIndex).Name
            ctmValue(aryIndex).ID = ctmList(ctmIndex).ID
            ctmValue(aryIndex).RecvTime = GetJSONValue(rec2, "RT")

            '* 件数カウンタアップ
            recno = recno + 1
            
            '* エレメント数分の値取得
            Set rec3 = CallByName(rec2, "EL", VbGet)
            For II = 0 To UBound(ctmList(ctmIndex).Element)
            
                workElm = ctmList(ctmIndex).Element(II)
                
                Set rec4 = CallByName(rec3, workElm.ID, VbGet)
                
                aryIndex2 = UBound(ctmValue(aryIndex).Element)
                If ctmValue(aryIndex).Element(aryIndex2).Name <> "" Then
                    aryIndex2 = aryIndex2 + 1
                    ReDim Preserve ctmValue(aryIndex).Element(aryIndex2)
                End If
                
                '* エレメント情報の取得
                ctmValue(aryIndex).Element(aryIndex2).Name = workElm.Name
                ctmValue(aryIndex).Element(aryIndex2).ID = workElm.ID
                ctmValue(aryIndex).Element(aryIndex2).Type = CInt(GetJSONValue(rec4, "T"))
                On Error Resume Next
                ctmValue(aryIndex).Element(aryIndex2).Value = GetJSONValue(rec4, "V")
                If ctmValue(aryIndex).Element(aryIndex2).Value = "" Then ctmValue(aryIndex).Element(aryIndex2).Value = " "
                
            Next
            
        Next
        
    Next
    
    GetCTMInfo = recno
    
End Function


'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Public Sub OutputCTMInfoToSheetFromCsv(currSheetName As String, dispTerm As Double, csvFile As String)
    
    Dim currWorksheet   As Worksheet
    
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim delRange        As Range
    Dim sortRange        As Range
    
    Dim buf As String

    Dim lineCount As Integer
    
    
    '* 出力先シートの取得
    Set currWorksheet = Worksheets(currSheetName)
    currWorksheet.Activate
    
    With currWorksheet
        
        
        '* 全情報クリア
        Set startRange = .Cells(4, 1)
        Set lastRange = startRange.End(xlDown)
        Set delRange = .Range("4:" & Format(lastRange.Row))
        delRange.Delete
        
        
        ' ファイルが存在するときのみサイズチェックを実施
        Dim FileSize As Long
        If Dir(csvFile) <> "" Then
            FileSize = FileLen(csvFile)
        Else
            FileSize = 0
        End If
                
        If FileSize > 0 Then
            '* 書き出し位置の設定
            Set startRange = .Cells(4, 1)
                    
            With ActiveSheet.QueryTables.Add(Connection:="Text;" + csvFile, Destination:=Range("A4"))
                .TextFileCommaDelimiter = True
                .TextFilePlatform = 65001
                .Refresh BackgroundQuery:=False
                .Delete
            End With
            
            
            Set startRange = .Cells(4, 1)
            Set lastRange = startRange.End(xlDown)
            Set sortRange = .Range("4:" & Format(lastRange.Row))
            
            sortRange.Sort key1:=Range("B4"), order1:=xlDescending, Header:=xlYes
        End If

        
    End With
    
    
    
    
End Sub

'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Public Sub OutputCTMInfoToSheet(currSheetName As String, dispTerm As Double, ctmValue() As CTMINFO)
    Dim II              As Variant
    Dim JJ              As Variant
    Dim KK              As Integer
    Dim currWorksheet   As Worksheet
    Dim writeIndex      As Variant
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim delRange        As Range
    Dim writeRange      As Range
    Dim receiveTime     As Double
    Dim dtRecvL         As Long
    Dim dtRecv          As Date
    Dim ms              As Integer
    Dim strWork         As String
    Dim nowTime         As Double
    Dim endTime         As Double
    Dim writeArray()    As Variant
    Dim writeArray2()   As Variant
    Dim iFirstFlag      As Boolean
    Dim readIndex       As Variant
    
    
    '* 出力先シートの取得
    Set currWorksheet = Worksheets(currSheetName)
    
    With currWorksheet
        
        '* 書き出し位置の設定
        Set startRange = .Cells(4, 1)
        
        '* 全情報クリア
        Set lastRange = startRange.End(xlDown)
        Set delRange = .Range("4:" & Format(lastRange.Row))
        delRange.Delete
        
        '* 書き出し位置の設定
        Set startRange = .Cells(4, 1)
        writeIndex = 0
        
'        Application.ScreenUpdating = False
        
        iFirstFlag = True
        For JJ = 0 To UBound(ctmValue)
        
            If currSheetName = ctmValue(JJ).Name Then
            
                receiveTime = CDbl(ctmValue(JJ).RecvTime)
                dtRecvL = CLng(receiveTime / 1000)
                '* 表示期間の処理
                If JJ = 0 Then
                    If dispTerm < 1# Then
                        endTime = dispTerm
                    Else
                        endTime = dtRecvL - dispTerm
                    End If
                End If
                If (endTime < 0 Or endTime > dtRecvL) Then Exit For
                ms = CInt(Right(ctmValue(JJ).RecvTime, 3))
                receiveTime = ((dtRecvL + 32400) / 86400) + 25569
                dtRecv = CDate(receiveTime)
                strWork = Format(dtRecv, "yyyy/MM/dd hh:mm:ss") + "." + Format(ms, "000")
                
                If iFirstFlag Then
                    readIndex = 0
                    ReDim writeArray(UBound(ctmValue(JJ).Element) + 2, readIndex)
                    iFirstFlag = False
                Else
                    readIndex = UBound(writeArray, 2) + 1
                    ReDim Preserve writeArray(UBound(ctmValue(JJ).Element) + 2, readIndex)
                End If
                
                writeArray(0, readIndex) = strWork
                writeArray(1, readIndex) = ctmValue(JJ).Name
                
                For KK = 0 To UBound(ctmValue(JJ).Element)
                
                    writeArray(KK + 2, readIndex) = ctmValue(JJ).Element(KK).Value
                
                Next
                
                writeIndex = writeIndex + 1
            End If
            
            DoEvents
        
        Next
        
        Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, UBound(ctmValue(0).Element) + 2))
        ReDim writeArray2(UBound(writeArray, 2), UBound(writeArray, 1))
        For II = 0 To UBound(writeArray, 1)
            For JJ = 0 To UBound(writeArray, 2)
                writeArray2(JJ, II) = writeArray(II, JJ)
            Next
        Next
'        writeArray2 = WorksheetFunction.Transpose(writeArray)
        writeRange = writeArray2
        Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, 0))
        writeRange.NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
        writeRange.EntireColumn.AutoFit
    
'        Application.ScreenUpdating = True
        
    End With
    
End Sub

'***********************************************
'* メインシートにあるミッション情報を取得する
'***********************************************
Public Sub GetCTMList(currSheetName As String, ctmList() As CTMINFO)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim ctmRange        As Range
    Dim elmRange        As Range
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim aryIndex        As Integer      ' CTM情報インデックス
    Dim aryIndex2       As Integer      ' エレメント情報インデックス
    Dim iFirstFlag      As Boolean      ' 配列初回フラグ
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    
    iFirstFlag = True
    
    Set currWorksheet = Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim ctmList(0)
    
    With currWorksheet
    
'        .Activate
        
        '* CTM NAMEの項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        
        For II = 0 To MaxCTMNumber Step 2
            Set ctmRange = startRange.Offset(II, 0)
            
            If ctmRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            If iFirstFlag Then
                aryIndex = 0
                ReDim ctmList(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(ctmList) + 1
                ReDim Preserve ctmList(aryIndex)
            End If
            ReDim ctmList(aryIndex).Element(0)
            ctmList(aryIndex).Element(0).Name = ""
            
            '* CTM情報取得
            ctmList(aryIndex).Name = ctmRange.Offset(0, 0).Value
            ctmList(aryIndex).ID = ctmRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = ctmRange.Offset(0, 1)
            For JJ = 0 To MaxELMNumber
                If elmRange.Offset(0, JJ).Value = "" Then
                    Exit For
                End If
                
                aryIndex2 = UBound(ctmList(aryIndex).Element)
                If ctmList(aryIndex).Element(aryIndex2).Name <> "" Then
                    aryIndex2 = aryIndex2 + 1
                    ReDim Preserve ctmList(aryIndex).Element(aryIndex2)
                End If
                ctmList(aryIndex).Element(aryIndex2).Name = elmRange.Offset(0, JJ).Value
                ctmList(aryIndex).Element(aryIndex2).ID = elmRange.Offset(1, JJ).Value
            Next
            
                
        Next
    
    End With
    
End Sub





