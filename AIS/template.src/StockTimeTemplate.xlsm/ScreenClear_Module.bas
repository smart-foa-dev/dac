Attribute VB_Name = "ScreenClear_Module"
Public Const StrPlus            As String = "＋"
Public Const StrMinus           As String = "−"
Public Const StrScrnChg         As String = "⇔"
Public Const StrSave            As String = "Ｓ"
Public Const PrefixStrBT        As String = "BT_shape_"
Public Const PrefixStrTX        As String = "TX_shape_"
Public Const dfW                As Integer = 24
Public Const dfH                As Integer = 24

'*********************************************
'* 着目ワークシートにメニューボタンを生成する
'*********************************************
'Wang Issue of zoom display area while window size is changed 2018/06/08 Start
'Public Sub AddMenuButton()
Public Sub AddMenuButton(Optional isOpen As Boolean = False)
    Dim createButton            As Boolean
'Wang Issue of zoom display area while window size is changed 2018/06/08 End
    Dim currWorksheet           As Worksheet
    Dim currShape               As Shape
    Dim btSize                  As RECT
    Dim RCAddress               As String
    Dim currRange               As Range
    
    Dim workStr                 As String

    Set currWorksheet = ActiveSheet
    
    '* 着目シート上のボタン図形を全て削除する
    For Each currShape In currWorksheet.Shapes
        If InStr(currShape.Name, PrefixStrBT) Then
            'Wang Issue of zoom display area while window size is changed 2018/06/08 Start
            If isOpen And Not createButton Then
                createButton = (currShape.Name = (PrefixStrBT & StrScrnChg))
            End If
            'Wang Issue of zoom display area while window size is changed 2018/06/08 End
            currShape.Delete
        ElseIf InStr(currShape.Name, PrefixStrTX) Then
            currShape.Delete
        End If
    Next currShape

    '* 設定されている表示範囲のアドレスを取得し、範囲を設定する
    RCAddress = GetDisplayAddress(ActiveSheet)
    If RCAddress <> "" Then
        Set currRange = ActiveSheet.Range(RCAddress)
    
        '* ボタンサイズ設定
        'Wang Issue of UI improvement 2018/06/07 Start
        'btSize.Top = currRange.Top + dfH + dfH / 2 + 2
        btSize.Top = currRange.Top + dfH * 3 + 2
        'Wang Issue of UI improvement 2018/06/07 End
        btSize.Right = dfW
        btSize.Bottom = dfH
        'Wang Issue of zoom display area while window size is changed 2018/06/06 Start
        'The following buttons are not needed any more
        'ISSUE_NO.789 sunyi 2018/07/17 Start
        'Excelバージョンが2010以前の場合、拡大縮小のボタンを作る
        If Application.Version <= 14 Then
            '* 拡大ボタンを追加
            btSize.Left = currRange.Left + 1
            Set currShape = PutButtonAtSheet(currWorksheet, btSize, RGB(0, 0, 255), RGB(0, 102, 255), PrefixStrBT, StrPlus, "ZoomDisplayPlus")
    
            '* 縮小ボタンを追加
            btSize.Left = currRange.Left + (dfW + 2) + 1
            Set currShape = PutButtonAtSheet(currWorksheet, btSize, RGB(0, 102, 0), RGB(0, 153, 0), PrefixStrBT, StrMinus, "ZoomDisplayMinus")
    
            '* 画面切り替えボタンを追加
            btSize.Left = currRange.Left + (dfW + 2) * 2 + 1
            'Wang Issue PAT-136 20190326 Start
'            Set currShape = PutButtonAtSheet(currWorksheet, btSize, RGB(255, 102, 0), RGB(255, 153, 0), PrefixStrBT, StrScrnChg, "ScreenDisplayChange")
            'Wang Issue PAT-136 20190326 End
        Else
        'ISSUE_NO.789 sunyi 2018/07/17 End
        
             'Wang Issue of zoom display area while window size is changed 2018/06/08 Start
        '        If MsgBox("「Ctrl+D」でツールバーの表示・非表示を切り替えることができます。" & Chr(13) & Chr(10) & "切り替えボタンも表示しますか。", vbYesNo) = vbYes Then
        '            btSize.Left = currRange.Left + (dfW + 2) * 2 + 1
        '            Set currShape = PutButtonAtSheet(currWorksheet, btSize, RGB(255, 102, 0), RGB(255, 153, 0), PrefixStrBT, StrScrnChg, "ScreenDisplayChange")
        '        End If
            If Not isOpen Then
                'Wang Issue PAT-136 20190326 Start
'                createButton = (MsgBox("「Ctrl+D」でツールバーの表示・非表示を切り替えることができます。" & Chr(13) & Chr(10) & "切り替えボタンも表示しますか。", vbYesNo) = vbYes)
                createButton = True
                'Wang Issue PAT-136 20190326 End
            End If
            If createButton Then
                btSize.Left = currRange.Left + (dfW + 2) * 2 + 1
                'Wang Issue PAT-136 20190326 Start
'                Set currShape = PutButtonAtSheet(currWorksheet, btSize, RGB(255, 102, 0), RGB(255, 153, 0), PrefixStrBT, StrScrnChg, "ScreenDisplayChange")
                'Wang Issue PAT-136 20190326 End
            End If
        'ISSUE_NO.789 sunyi 2018/07/17 Start
        'Excel 2010の場合、拡大縮小のボタンを作る
        End If
        'ISSUE_NO.789 sunyi 2018/07/17 End
        'Wang Issue of zoom display area while window size is changed 2018/06/08 End
        'Wang Issue of zoom display area while window size is changed 2018/06/06 End
        '* 更新状態テキストボックスを追加
        Set currShape = AddStatusTextBox(currWorksheet, currRange)
        'Wang Issue of UI improvement 2018/06/07 Start
        Call AddStatusIcon(currShape, ActiveSheet)
        currRange.Select
        'Wang Issue of UI improvement 2018/06/07 End
    
    End If


'    currWorksheet.Shapes(PrefixStrBT & "ONLINE").TextFrame2.TextRange.Characters.Text = "更新中"
'    currWorksheet.Shapes(PrefixStrBT & "ONLINE").TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 255)
'
'    MsgBox "xxx"
'
'    currWorksheet.Shapes(PrefixStrBT & "ONLINE").TextFrame2.TextRange.Characters.Text = "停止中"
'    currWorksheet.Shapes(PrefixStrBT & "ONLINE").TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(255, 0, 0)
End Sub

'Wang Issue of UI improvement 2018/06/07 Start
Private Sub AddStatusIcon(statusTextBox As Shape, targetSheet As Worksheet)
    Dim paramSheet      As Worksheet
    Dim graph           As ShapeRange

    Set paramSheet = ThisWorkbook.Sheets(ParamSheetName)
    
    ' paste refresh graph
    Set graph = paramSheet.Shapes.Range(Array("BT_shape_refreshgraph"))
    
    paramSheet.Activate
    graph.Select
    Selection.Copy
    
    targetSheet.Activate
    targetSheet.Paste
    
    Selection.Left = statusTextBox.Left + (statusTextBox.Width - graph.Width) / 2
    Selection.Top = statusTextBox.Top + (statusTextBox.Height - graph.Height) / 2
    
    Selection.OnAction = statusTextBox.OnAction
    
    ' paste stop graph
    Set graph = paramSheet.Shapes.Range(Array("BT_shape_stopgraph"))
    
    paramSheet.Activate
    graph.Select
    Selection.Copy
    
    targetSheet.Activate
    targetSheet.Paste

    Selection.Left = statusTextBox.Left + (statusTextBox.Width - graph.Width) / 2
    Selection.Top = statusTextBox.Top + (statusTextBox.Height - graph.Height) / 2
    
    Selection.OnAction = statusTextBox.OnAction
End Sub
'Wang Issue of UI improvement 2018/06/07 End

'*****************************
'* テキストボックスを追加する
'*****************************
Public Function AddStatusTextBox(currWorksheet As Worksheet, currRange As Range) As Shape
    Dim btSize  As RECT

    '* 更新状態テキストボックスを追加
    btSize.Top = currRange.Top + 1
    btSize.Left = currRange.Left + 1
    btSize.Right = (dfW + 2) * 3
    'Wang Issue of UI improvement 2018/06/07 Start
    'btSize.Bottom = dfH + dfh/2
    btSize.Bottom = dfH * 3
    'Wang Issue of UI improvement 2018/06/07 End
    Set AddStatusTextBox = PutTextBoxAtSheet(currWorksheet, btSize, RGB(0, 0, 0), RGB(255, 255, 255), PrefixStrTX, "ONLINE", "ChangeStartStopBT")
End Function

'*************************
'* メニューボタン生成処理
'*************************
Public Function PutButtonAtSheet(currWorksheet As Worksheet, _
                            btSize As RECT, rgbLine As Long, rgbFill As Long, _
                            prefixStr As String, btName As String, macroName As String) As Shape
    Dim currShape       As Shape
    Dim workStr         As String

    '* ボタンを追加
    Set currShape = currWorksheet.Shapes.AddShape(msoShapeRoundedRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        workStr = prefixStr & btName
        .Name = workStr
        .OnAction = macroName
        With .TextFrame2
            '* 文字列配置中央
            .VerticalAnchor = msoAnchorMiddle
            .TextRange.ParagraphFormat.Alignment = msoAlignCenter
            .TextRange.Characters.text = btName
            .TextRange.Font.Bold = msoTrue
            .TextRange.Font.Size = 20
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With

    Set PutButtonAtSheet = currShape

End Function

'*************************
'* テキストボックス生成処理
'*************************
Public Function PutTextBoxAtSheet(currWorksheet As Worksheet, _
                            btSize As RECT, rgbLine As Long, rgbFill As Long, _
                            prefixStr As String, btName As String, macroName As String) As Shape
    Dim currShape       As Shape
    Dim workStr         As String

    '* ボタンを追加
    Set currShape = currWorksheet.Shapes.AddShape(msoShapeRoundedRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        workStr = prefixStr & btName
        .Name = workStr
        .OnAction = macroName
        With .TextFrame2
            '* 文字列配置中央
            .MarginLeft = 0
            .MarginRight = 0
            .MarginTop = 0
            .MarginBottom = 0
            .VerticalAnchor = msoAnchorMiddle
            With .TextRange
                .ParagraphFormat.Alignment = msoAlignCenter
'                .Characters.Text = btName
                With .Font
                    .Bold = msoTrue
                    .Size = 12
                    .Fill.ForeColor.RGB = RGB(0, 0, 0)
                End With
            End With
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With
    
    Set PutTextBoxAtSheet = currShape

End Function

'***********************************
'* 画面表示不要部分表示／非表示処理
'***********************************
Public Sub ScreenDisplayONOFF(dispFlag As Boolean)
    Dim workStr         As String

    workStr = "SHOW.TOOLBAR(""Ribbon""," & dispFlag & ")"

    ' リボンを非表示にする
    Application.ExecuteExcel4Macro workStr
    '* 数式バーを非表示
    Application.DisplayFormulaBar = dispFlag
    '* スクロールバーを非表示
    Application.DisplayScrollBars = dispFlag
    '* ステータスバーを非表示
    Application.DisplayStatusBar = dispFlag
    '* 行番号を非表示
    ActiveWindow.DisplayHeadings = dispFlag
    '* シートタブを非表示
    ActiveWindow.DisplayWorkbookTabs = dispFlag

End Sub

'**************************
'* スクリーン不要情報ONOFF
'**************************
Public Sub ScreenDisplayChange()
    Dim flagDIsp    As Boolean
    
    'Wang Issue of zoom display area while window size is changed 2018/06/06 Start
    ThisWorkbook.ForbidCalcSize = True
    'Wang Issue of zoom display area while window size is changed 2018/06/06 End
    
    flagDIsp = Not ActiveWindow.DisplayHeadings

    '* 諸々バーを非表示
    Call ScreenDisplayONOFF(flagDIsp)
    
'    Call GoZoomDisplay(ActiveWindow.Zoom)

    'Wang Issue of zoom display area while window size is changed 2018/06/06 Start
    'Call GoZoomDisplay(ActiveWindow.Zoom)
    '*ISSUE_NO.811 Sunyi 2018/08/03 Start
    '*Excel2010の場合、サイズ自動調整をしないように修正
    If Application.Version <= 14 Then
        Call GoZoomDisplay(ActiveWindow.Zoom)
    Else
        Call ThisWorkbook.displayAreaObject.ResizeWindow(ActiveWindow, True)
    End If
    'ISSUE_NO.811 Sunyi 2018/08/03 End
    ThisWorkbook.ForbidCalcSize = False
    'Wang Issue of zoom display area while window size is changed 2018/06/06 End

End Sub

'**********************************************
'* スクリーン不要情報ONOFF(ファイルオープン時)
'**********************************************
Public Sub ScreenDisplayChangeForOpen()
    
    'Wang Issue of zoom display area while window size is changed 2018/06/06 Start
    ThisWorkbook.ForbidCalcSize = True
    'Wang Issue of zoom display area while window size is changed 2018/06/06 End

    '* メニューボタンを追加
    'Wang Issue of zoom display area while window size is changed 2018/06/08 Start
    'Call AddMenuButton
    Call AddMenuButton(True)
    'Wang Issue of zoom display area while window size is changed 2018/06/08 End

    '* 諸々バーを非表示
    Call ScreenDisplayONOFF(False)
    
    'Wang Issue of zoom display area while window size is changed 2018/06/06 Start
    'ISSUE_NO.789 sunyi 2018/07/17 Start
    'Excelバージョンが2010以前の場合、拡大縮小のボタンを作る
    If Application.Version <= 14 Then
        Call GoZoomDisplay(ActiveWindow.Zoom)
    Else
    'ISSUE_NO.789 sunyi 2018/07/17 End
        Call ThisWorkbook.displayAreaObject.ResizeWindow(ActiveWindow, True, True)
        If TypeName(Selection) = "Range" Then
            Call Selection.Resize(1, 1).Select
        End If
        ThisWorkbook.ForbidCalcSize = False
    End If
    'Wang Issue of zoom display area while window size is changed 2018/06/06 End

End Sub

Public Sub ScreenRescue()
    '* 諸々バーを非表示
    Call ScreenDisplayONOFF(False)
End Sub


