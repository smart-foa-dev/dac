Attribute VB_Name = "Main_Module"
Option Explicit

'*************************************
'* ｢テスト｣の処理
'*************************************
Private Sub OperationTest()
    Dim currWorksheet       As Worksheet
    Dim dataNum             As Integer
    Dim hStepValue          As Variant
    Dim hPrmRange           As Range
    Dim sampleType          As String
    Dim srchRange           As Range

    '追加20160606*******************************************
    sampleType = ""
    Set srchRange = Worksheets(ParamSheetName).Columns(1).Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
    '*******************************************************
    
'    '* DAC-87 sunyi 20181228 start
'    '* OFFLINEの場合、Testできないようにする
'    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="オンライン", LookAt:=xlWhole)
'    If Not srchRange Is Nothing Then
'        If srchRange.Offset(0, 1).Value = "OFFLINE" Then
'            Exit Sub
'        End If
'    Else
'        Exit Sub
'    End If
'    '* DAC-87 sunyi 20181228 end
    
    If sampleType <> "GRIP" Then
        '* ミッションから情報を取得
        Call GetMissionInfoAll
    Else
        Call GetGripMission
    End If

    Call CallBeforeExcelFromAIS
    
'    '* ピボットテーブルの更新
'    If UpdatePivotTable = False Then
'        Exit Sub
'    End If
'
'    '* 横軸のステップ幅を取得する
'    Set hPrmRange = Worksheets(PivotSheetName).Cells.Find(What:="横パラメータ", LookAt:=xlWhole)
'    If Not hPrmRange Is Nothing Then
'        hStepValue = hPrmRange.Offset(1, 3).Value
'    Else
'        hStepValue = 10
'    End If
'
'    '* グラフ用データの作成
'    dataNum = goMakeDataForGraph(hStepValue)
'
'    Call UpdateAfterGraphEdit

End Sub

'*************************************
'* 最大最小取得の処理
'* ※AISからコールされるマクロ。【ミッション取得は実行しない】
'*************************************
Private Sub CallMaxMinValueFromAIS()
    Dim currWorksheet       As Worksheet
    Dim dataNum             As Integer
    Dim hStepValue          As Variant
    Dim hPrmRange           As Range
    
    '* ピボットテーブルの更新
    If UpdatePivotTable = False Then
        Exit Sub
    End If
    
    '* 横軸のステップ幅を取得する
    Set hPrmRange = Worksheets(PivotSheetName).Cells.Find(What:="横パラメータ", LookAt:=xlWhole)
    If Not hPrmRange Is Nothing Then
        hStepValue = hPrmRange.Offset(1, 3).Value
    Else
        hStepValue = 10
    End If
    
    '* グラフ用データの作成
    dataNum = goMakeDataForGraph(hStepValue)

    Call UpdateAfterGraphEdit

End Sub

'*************************************
'* 最大最小取得の処理
'* ※AISからエクセル起動にコールされるマクロ。【ミッション取得は実行しない】
'*************************************
Private Sub CallBeforeExcelFromAIS()
    Dim currWorksheet       As Worksheet
    Dim dataNum             As Integer
    Dim hStepValue          As Variant
    Dim hPrmRange           As Range
    
    '* ピボットテーブルの更新
    If UpdatePivotTable = False Then
        Exit Sub
    End If
    
    '* 横軸のステップ幅を取得する
    Set hPrmRange = Worksheets(PivotSheetName).Cells.Find(What:="横パラメータ", LookAt:=xlWhole)
    If Not hPrmRange Is Nothing Then
        hStepValue = hPrmRange.Offset(1, 3).Value
    Else
        hStepValue = 10
    End If
    
    '* グラフ用データの作成
    dataNum = goMakeDataForGraph(hStepValue)

    Call UpdateAfterGraphEdit
    
End Sub

'***************************************************
'* グラフ編集シートから最大／最小／刻み幅を取得する
'***************************************************
Public Sub GetMaxMinStepValue(currSheetName As String, vValue As MAXMINSTEPINFO, hValue As MAXMINSTEPINFO)
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range

    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Set currWorksheet = Worksheets(currSheetName)
    With currWorksheet
        Set srchRange = .Cells.Find(What:=VerticalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            vValue.MaxValue = srchRange.Offset(1, 1).Value
            vValue.MinValue = srchRange.Offset(2, 1).Value
            vValue.StepValue = srchRange.Offset(3, 1).Value
        End If
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            hValue.MaxValue = srchRange.Offset(1, 1).Value
            hValue.MinValue = srchRange.Offset(2, 1).Value
            hValue.StepValue = srchRange.Offset(3, 1).Value
        End If
    End With
        

End Sub

'*************************************
'* グラフ編集シート上の値更新後の処理
'*************************************
Public Sub UpdateAfterGraphEdit()
    Dim currWorksheet       As Worksheet
    Dim currChartObj        As ChartObject
    Dim currPivotTable      As PivotTable
    Dim srchRange               As Range
    Dim dataNum             As Integer
    Dim hValue              As MAXMINSTEPINFO
    Dim vValue              As MAXMINSTEPINFO
    Dim vValueOld           As MAXMINSTEPINFO
    
    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Call GetMaxMinStepValue(GraphEditSheetName, vValueOld, hValue)
    
    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set currChartObj = currWorksheet.ChartObjects(HistGraph02Name)
    Set currPivotTable = Worksheets(PivotSheetName).PivotTables(PivotMainTableName)
    
    'データ無しの場合グラフの修正しない。20161028 ADD
    '****** 2017/02/24 (CInt(hValue.MinValue) > 0---->(CInt(hValue.MinValue) >= 0
    If (CInt(hValue.MinValue) >= 0) And (CInt(hValue.MaxValue) > 0) And (CInt(hValue.StepValue) > 0) Then
    Else
        Exit Sub
    End If
    
    '* ピボットテーブルの最大値／最小値／ステップを再設定
    Call ChangeMaxMinStepPVT(currPivotTable, CInt(hValue.MinValue), CInt(hValue.MaxValue), CInt(hValue.StepValue))
    
    '* グラフ用データの作成
    dataNum = goMakeDataForGraph(hValue.StepValue)
    
    '* 再度グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Call GetMaxMinStepValue(GraphEditSheetName, vValue, hValue)
    
    '* グラフの縦軸の最大値／最小値／ステップを再設定
    Call SetGraphParamY(currChartObj, CDbl(vValueOld.MaxValue), CDbl(vValueOld.MinValue), CDbl(vValueOld.StepValue))
    
    '* グラフの横軸の最大値／最小値／ステップを再設定
    Call SetGraphParamX(currChartObj, CDbl(hValue.MaxValue), CDbl(hValue.MinValue), CDbl(hValue.StepValue))
    
    Set currChartObj = Worksheets(FixGraphSheetName).ChartObjects(HistGraph02Name)
    
    '* グラフの縦軸の最大値／最小値／ステップを再設定
    Call SetGraphParamY(currChartObj, CDbl(vValueOld.MaxValue), CDbl(vValueOld.MinValue), CDbl(vValueOld.StepValue))
    
    '* グラフの横軸の最大値／最小値／ステップを再設定
    Call SetGraphParamX(currChartObj, CDbl(hValue.MaxValue), CDbl(hValue.MinValue), CDbl(hValue.StepValue))
    
        '最大値、最小値、刻み設定　2017/04/13
    Set currWorksheet = Worksheets(PivotSheetName)  'グラフテーブル
    Set srchRange = currWorksheet.Cells.Find(What:="縦最大値", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 0).Value = CDbl(vValueOld.MaxValue)
        srchRange.Offset(1, 1).Value = CDbl(vValueOld.MinValue)
        srchRange.Offset(1, 2).Value = CDbl(vValueOld.StepValue)
    End If
    Set srchRange = currWorksheet.Cells.Find(What:="横最大値", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 0).Value = CDbl(hValue.MaxValue)
        srchRange.Offset(1, 1).Value = CDbl(hValue.MinValue)
        srchRange.Offset(1, 2).Value = CDbl(hValue.StepValue)
    End If
        
    '* 基準線を再配置する
    Call GraphReCalculate
    
    '凡例、背景ﾃﾞｰﾀ作成
''    Call HaikeiHanreiDataMake

End Sub

'*************************************
'* ピボットテーブル（メイン／サブ）を生成する
'*************************************
Private Sub goMakePivotTable()
    Dim workTime        As String
    Dim workType        As String
    Dim workStr         As String
    Dim pvtWorksheet    As Worksheet
    Dim currWorksheet   As Worksheet
    Dim lastRange       As Range
    Dim dataRange       As Range
    Dim currRange       As Range
    Dim srchRange       As Range
    Dim hRange          As Range
    Dim hPrmRange       As Range
    Dim workDbl         As Double
    Dim hMaxValue       As Variant
    Dim hMinValue       As Variant
    Dim hStepValue      As Variant
    Dim aveValue        As Variant
    Dim sigValue        As Variant
    Dim divValue        As Variant
    
    Set currWorksheet = Worksheets(ForPivotSheetName)
    Set pvtWorksheet = Worksheets(PivotSheetName)
    
    With currWorksheet
        '* 集計対象時間列項目名
        workTime = currWorksheet.Range(ADDR_WorkTimeStartPos).Value
        '* 集計対象系列列項目名
        workType = currWorksheet.Range(ADDR_SeriesStartPos).Value
    
        '* ピボットテーブルのデータソース範囲の取得
        Set lastRange = .Range(ADDR_SeriesStartPos).End(xlDown)
        Set dataRange = .Range(.Cells(1, 1), .Cells(lastRange.Row, lastRange.Column))
        dataRange.Name = "ピボットデータ範囲"
    End With
    workStr = ForPivotSheetName & ADDR_MainPivotTable & ":R" & lastRange.Row & ADDR_DataCell  '"C3"
    
    '* メインピボットテーブル作成
    Call MakePivotTable(PivotMainTableName, PivotSheetName & ADDR_MainPivotTable, workStr, workTime, workType)
    '* サブピボットテーブル作成
    Call MakePivotTableSub(PivotSubTableName, PivotSheetName & ADDR_SubPivotTable, workStr, workTime)
    
    '* 横パラメータの最大値／最小値／横軸刻み幅を取得
    With pvtWorksheet
        Set hPrmRange = .Cells.Find(What:="横パラメータ", LookAt:=xlWhole)
        Set hRange = .Cells.Find(What:="行ラベル", LookAt:=xlWhole)
        If Not hRange Is Nothing Then
            hMaxValue = hRange.Offset(1, 1).Value
            hMinValue = hRange.Offset(1, 2).Value
            aveValue = hRange.Offset(1, 3).Value
            sigValue = hRange.Offset(1, 4).Value
            divValue = hRange.Offset(1, 5).Value
        Else
            hMaxValue = .Range("I18").Value
            hMinValue = .Range("J18").Value
            aveValue = .Range("K18").Value
            sigValue = .Range("L18").Value
            divValue = .Range("M18").Value
        End If
        If Not hPrmRange Is Nothing Then
            hStepValue = hPrmRange.Offset(1, 3).Value
        Else
            hStepValue = .Range("M12").Value
        End If
        
        '* 横軸：刻み幅を取得
        If hStepValue = 0 Then
            Set currRange = Worksheets(ParamSheetName).Cells.Find(What:="横表示刻み", LookAt:=xlWhole)
            If Not currRange Is Nothing Then
                hStepValue = currRange.Offset(0, 1).Value
                If hStepValue = 0 Then
                    hStepValue = Round((hMaxValue - hMinValue) / 10)
                End If
            Else
                hStepValue = Round((hMaxValue - hMinValue) / 10)
            End If
        End If
        hStepValue = Application.WorksheetFunction.Ceiling(hStepValue, 10)
        If hStepValue <= 0 Then
            hStepValue = hMaxValue - hMinValue
        End If
        '* 横軸：刻み幅をセット
        hPrmRange.Offset(1, 3).Value = hStepValue
    End With
    
    '* 横パラメータの最大／最小／平均値のparamシートに対する更新
    With pvtWorksheet
        If hStepValue <= 0 Then
            hStepValue = 1
        End If
    
        hMaxValue = Int(CDbl(hMaxValue) / hStepValue)
        hMaxValue = (hMaxValue + 1) * hStepValue
        hPrmRange.Offset(1, 1).Value = hMaxValue
        hMinValue = Int(CDbl(hMinValue) / hStepValue)
        hMinValue = (hMinValue) * hStepValue
        hPrmRange.Offset(1, 2).Value = hMinValue
    End With
    
    '* 最大／最小／刻み幅および平均／標準偏差／分散のグラフ編集シートに対する更新
    With Worksheets(GraphEditSheetName)
        Set srchRange = .Range("A:L")
        Set currRange = srchRange.Cells.Find(What:="平均", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.Offset(0, 1).Value = aveValue
            currRange.Offset(0, 3).Value = sigValue
            currRange.Offset(0, 5).Value = divValue
        End If
        Set currRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.Offset(1, 1).Value = hMaxValue
            currRange.Offset(2, 1).Value = hMinValue
            currRange.Offset(3, 1).Value = hStepValue
        End If
    End With
    
    '* 平均／標準偏差／分散のグラフシートに対する更新
    With Worksheets(FixGraphSheetName)
        Set srchRange = .Range("A:L")
        Set currRange = srchRange.Cells.Find(What:="平均", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.Offset(0, 1).Value = aveValue
            currRange.Offset(0, 3).Value = sigValue
            currRange.Offset(0, 5).Value = divValue
        End If
    End With
End Sub

'*************************************
'* メインピボットテーブルを生成する
'*************************************
Public Sub MakePivotTable(pvtName As String, pvtPos As String, distData As String, workTime As String, workType As String)
    Dim workStr             As String
    Dim currPivotTable      As PivotTable
    Dim currPivotCache      As PivotCache
    Dim paraEnzan           As String
    Dim rsltEnzan           As String
    Dim srchRange           As Range
    Dim wrkStr              As String
    
    '* paramシートから必要な情報を取得する
    With Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="演算", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then paraEnzan = srchRange.Offset(0, 1).Value
    End With

    '* ピボットテーブルの準備
    Set currPivotCache = ActiveWorkbook.PivotCaches.Create( _
              SourceType:=xlDatabase, _
              SourceData:=distData, _
              Version:=xlPivotTableVersion14)

    '* ピボットテーブルの配置（A1セル）
    Set currPivotTable = currPivotCache.CreatePivotTable( _
               TableDestination:=pvtPos, _
               TableName:=pvtName, _
               DefaultVersion:=xlPivotTableVersion14)

    '* フィールド設定　1行目
'    With currPivotTable.PivotFields(workTime)
'        .Orientation = xlRowField
'        .Position = 1
'        .ShowAllItems = True
'    End With
''    With currPivotTable.PivotFields(workType)
''        .Orientation = xlColumnField
''        .Position = 1
''        .ShowAllItems = True
''    End With
    
    wrkStr = ""
    
    Select Case paraEnzan
        Case "個数"
            currPivotTable.AddDataField currPivotTable.PivotFields(workType), "データの個数 / " & workType, xlCount

        Case "合計"
            currPivotTable.AddDataField currPivotTable.PivotFields(workType), "合計 / " & workType, xlSum
            
        Case "平均"
            currPivotTable.AddDataField currPivotTable.PivotFields(workType), "平均 / " & workType, xlAverage
            
        Case "最大"
            currPivotTable.AddDataField currPivotTable.PivotFields(workType), "最大値 / " & workType, xlMax
            
        Case "最小"
            currPivotTable.AddDataField currPivotTable.PivotFields(workType), "最小値 / " & workType, xlMin
            
        Case Else
            currPivotTable.AddDataField currPivotTable.PivotFields(workType), "データの個数 / " & workType, xlCount
            
    End Select
    
    With currPivotTable.PivotFields(workTime)
        .Orientation = xlRowField
        .Position = 1
    End With
    'currPivotTable.AddDataField currPivotTable.PivotFields("【実績値】総横幅"), "データの個数 / 【実績値】総横幅", xlCount
    
    '* 表の項目の設定
    currPivotTable.CompactLayoutRowHeader = workTime
    currPivotTable.CompactLayoutColumnHeader = workType
    
    '* 個数がない場合は０を割り当てる
    currPivotTable.NullString = "0"
    
End Sub

'*************************************
'* サブピボットテーブルを生成する
'*************************************
Public Sub MakePivotTableSub(pvtName As String, pvtPos As String, distData As String, workTime As String)
    Dim workStr         As String
    Dim currPivotTable  As PivotTable
    Dim currPivotCache  As PivotCache

    '* ピボットテーブルの準備
    Set currPivotCache = ActiveWorkbook.PivotCaches.Create( _
              SourceType:=xlDatabase, _
              SourceData:=distData, _
              Version:=xlPivotTableVersion14)

    '* ピボットテーブルの配置（K1セル）
    Set currPivotTable = currPivotCache.CreatePivotTable( _
               TableDestination:=pvtPos, _
               TableName:=pvtName, _
               DefaultVersion:=xlPivotTableVersion14)

    '* フィールド設定
    With currPivotTable.PivotFields(workTime)
    'With currPivotTable.PivotFields("CTM NAME")
        .Orientation = xlRowField
        .Position = 1
    End With
    
    '* 最大／最小／平均のフィールド設定
    workStr = "データの個数 / " & workTime
    currPivotTable.AddDataField currPivotTable.PivotFields(workTime), workStr, xlCount
    With currPivotTable.PivotFields(workStr)
        .Function = xlMax
        .Caption = "最大値"
    End With
    currPivotTable.AddDataField currPivotTable.PivotFields(workTime), workStr, xlCount
    With currPivotTable.PivotFields(workStr)
        .Function = xlMin
        .Caption = "最小値"
    End With
    currPivotTable.AddDataField currPivotTable.PivotFields(workTime), workStr, xlCount
    With currPivotTable.PivotFields(workStr)
        .Function = xlAverage
        .Caption = "平均"
    End With
    currPivotTable.AddDataField currPivotTable.PivotFields(workTime), workStr, xlCount
    With currPivotTable.PivotFields(workStr)
        .Function = xlStDev
        .Caption = "標準偏差"
    End With
    currPivotTable.AddDataField currPivotTable.PivotFields(workTime), workStr, xlCount
    With currPivotTable.PivotFields(workStr)
        .Function = xlVar
        .Caption = "分散"
    End With
    
End Sub

'*************************************
'* グラフ用のデータを生成する
'*************************************
Public Function goMakeDataForGraph(hStepValue) As Integer
    Dim pvtWorksheet    As Worksheet
    Dim prmWorksheet    As Worksheet
    Dim startRange      As Range
    Dim writeRange      As Range
    Dim dataRange       As Range
    Dim srchRange       As Range
    Dim currRange       As Range
    Dim vRange          As Range
    Dim hRange          As Range
    Dim dataNum         As Integer
    Dim currPivotTable  As PivotTable
    Dim pvtCol          As Integer
    Dim pvtRow          As Integer
    Dim hPrmRange       As Range

    Set pvtWorksheet = Worksheets(PivotSheetName)
    Set prmWorksheet = Worksheets(ParamSheetName)
    
    With pvtWorksheet
    
        Set currPivotTable = .PivotTables(PivotMainTableName)
    
        '* メインピボットテーブルのデータ開始行・列を取得
        pvtRow = currPivotTable.DataBodyRange.Row
        pvtCol = currPivotTable.rowRange.Column
    
        Set startRange = .Cells(pvtRow, pvtCol)
        Set dataRange = .Cells(pvtRow, pvtCol + 1)
        Set currRange = .Cells.Find(What:="グラフ用データ", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            Set writeRange = currRange.Offset(1, 0)
        Else
            Set writeRange = .Range("F2").Value
        End If
    
        '* グラフのためのデータを生成する
        dataNum = MakeDataForGraph(pvtWorksheet, startRange, dataRange, writeRange, hStepValue)
        
        goMakeDataForGraph = dataNum
    
    End With
    
End Function

'*************************************
'* グラフ作成用データ生成
'*************************************
Public Function MakeDataForGraph(currWorksheet As Worksheet, startRange As Range, dataRange As Range, writeRange As Range, hStepValue As Variant) As Integer
    Dim readIndex       As Integer
    Dim writeIndex      As Integer
    Dim tValue          As String
    Dim iValue          As Long
    Dim workRange       As Range
    Dim currRange       As Range
    Dim hPrmRange       As Range
    Dim vRange          As Range
    Dim vPrmRange       As Range
    Dim MaxValue        As Variant
    Dim MinValue        As Variant
    Dim aveValue        As Variant
    Dim StepValue       As Variant
    
    '* 古いデータ削除
    readIndex = 0
    Set workRange = currWorksheet.Range(writeRange.Offset(0, 0), writeRange.End(xlDown).Offset(0, 1))
    workRange.ClearContents
    
    '* ピボットテーブルからグラフ用のテーブルへ値をコピーする
    readIndex = 0
    If Left(startRange.Offset(readIndex, 0).Value, 1) = "<" Then readIndex = readIndex + 1
    
    writeIndex = 0
    Do While (Left(startRange.Offset(readIndex, 0).Value, 1) <> ">") And _
             (Left(startRange.Offset(readIndex, 0).Value, 1) <> "<") And _
             (startRange.Offset(readIndex, 0).Value <> "総計") And _
             (startRange.Offset(readIndex, 0).Value <> "")
    
        '* ピボットテーブルから値を取得
        tValue = startRange.Offset(readIndex, 0).Value
        iValue = Val(Left(tValue, Len(tValue) - InStr(tValue, "-")))
        
        '* グラフ用表に転記する
        writeRange.Offset(writeIndex, 0).Value = iValue + (hStepValue / 2)
        writeRange.Offset(writeIndex, 0).NumberFormatLocal = "0_);[赤](0)"       ' 表示形式「数値」
        
        '* ピボットテーブルから値を取得
        tValue = dataRange.Offset(readIndex, 0).Value
        iValue = Val(tValue)
        
        '* グラフ用表に転記する
        writeRange.Offset(writeIndex, 1).Value = iValue
        writeRange.Offset(writeIndex, 1).NumberFormatLocal = "0_);[赤](0)"       ' 表示形式「数値」
        
        readIndex = readIndex + 1
        writeIndex = writeIndex + 1
        
        DoEvents
    Loop
    
    '* データ範囲から縦軸の最大値／最小値／平均値を求める
    With currWorksheet
        Set workRange = .Range(dataRange, dataRange.Offset(readIndex - 1, 0))
        MaxValue = Application.WorksheetFunction.Max(workRange)
        MinValue = Application.WorksheetFunction.Min(workRange)
        aveValue = Application.WorksheetFunction.Average(workRange)
    
        '* 縦軸の最大値／最小値／平均値を記載する
        Set vRange = .Cells.Find(What:="縦ラベル", LookAt:=xlWhole)
        If Not vRange Is Nothing Then
            vRange.Offset(1, 1).Value = MaxValue
            vRange.Offset(1, 2).Value = MinValue
            vRange.Offset(1, 3).Value = aveValue
        End If
    
        '* 縦パラメータ欄から刻み値を取得する
        Set vPrmRange = .Cells.Find(What:="縦パラメータ", LookAt:=xlWhole)
        If Not vPrmRange Is Nothing Then
            StepValue = vPrmRange.Offset(1, 3).Value
        Else
            StepValue = .Range("L10").Value
        End If
        '* 縦パラメータに刻み値の指定がない場合は、paramシートから取得し、その値も０ならば新規に算出する
        If StepValue = 0 Then
            Set currRange = Worksheets(ParamSheetName).Cells.Find(What:="縦表示刻み", LookAt:=xlWhole)
            If Not currRange Is Nothing Then
                StepValue = currRange.Offset(0, 1).Value
                If StepValue = 0 Then
                    StepValue = Round((MaxValue - MinValue) / 10)
                End If
            Else
                StepValue = Round((MaxValue - MinValue) / 10)
            End If
            StepValue = Application.WorksheetFunction.Ceiling(StepValue, 10)
            vPrmRange.Offset(1, 3).Value = StepValue
        End If
    
    End With
        
    '* 丸めた最大値／最小値を縦パラメータ欄にセットする
    If StepValue <= 0 Then
        StepValue = 1
    End If
    MaxValue = Int(CDbl(MaxValue) / StepValue)
    MaxValue = (MaxValue + 1) * StepValue
    vPrmRange.Offset(1, 1).Value = MaxValue
    If MinValue <> 0 Then
        MinValue = Int(CDbl(MinValue) / StepValue)
        MinValue = (MinValue - 1) * StepValue
    End If
    If MinValue < 0 Then MinValue = 0
    vPrmRange.Offset(1, 2).Value = MinValue
    
    '* グラフ編集シートにも反映（ただし、元データが存在しない場合のみ）
    With Worksheets(GraphEditSheetName)
        Set currRange = .Cells.Find(What:=VerticalAxisName, LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            If currRange.Offset(1, 1).Value = "" Or currRange.Offset(1, 1).Value = 0 Then
                currRange.Offset(1, 1).Value = MaxValue
            End If
            If currRange.Offset(2, 1).Value = "" Then
                currRange.Offset(2, 1).Value = MinValue
            End If
            If currRange.Offset(3, 1).Value = "" Or currRange.Offset(3, 1).Value = 0 Then
                currRange.Offset(3, 1).Value = StepValue
            End If
        End If
    End With
    
    '* データ数を返す
    MakeDataForGraph = readIndex
    
End Function

'*************************************
'* グラフの設定更新
'*************************************
Public Sub ChangeGraphSetting(currWorksheet As Worksheet)
    Dim pvtWorksheet    As Worksheet
    Dim writeRange      As Range
    Dim workRange       As Range
    Dim lastRange       As Range
    Dim currRange       As Range
    Dim vRange          As Range
    Dim hRange          As Range
    Dim dataNum         As Integer
    Dim currChartObj    As ChartObject
    Dim maxY            As Double
    Dim minY            As Double
    Dim stepY           As Double
    Dim maxX                As Double
    Dim minX                As Double
    Dim stepX               As Double
    Dim workStr         As String
    
    Set pvtWorksheet = Worksheets(PivotSheetName)
    
    Set currChartObj = currWorksheet.ChartObjects(HistGraph02Name)

    With pvtWorksheet
        Set currRange = .Cells.Find(What:="グラフ用データ", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            Set writeRange = currRange.Offset(1, 0)
        Else
            Set writeRange = .Range("F2").Value
        End If
        Set lastRange = writeRange.End(xlDown)
    
        '* グラフの参照データを再設定する
        Set workRange = .Range(writeRange.Offset(0, 0), lastRange.Offset(0, 0))
        workStr = "=" & PivotSheetName & "!" & workRange.Address
        currChartObj.Chart.SeriesCollection(1).XValues = workStr
    
        Set workRange = .Range(writeRange.Offset(0, 1), lastRange.Offset(0, 1))
        workStr = "=" & PivotSheetName & "!" & workRange.Address
        currChartObj.Chart.SeriesCollection(1).Values = workStr
    '    Set workRange = pvtWorksheet.Range(writeRange.Offset(0, 3), lastRange.Offset(0, 3))
    '    workStr = "=" & PivotSheetName & "!" & workRange.Address
    '    currChartObj.Chart.SeriesCollection(2).Values = workStr
    '    Set workRange = pvtWorksheet.Range(writeRange.Offset(0, 2), lastRange.Offset(0, 2))
    '    workStr = "=" & PivotSheetName & "!" & workRange.Address
    '    currChartObj.Chart.SeriesCollection(3).Values = workStr
    
        '* グラフの最大／最小／ステップ値を取得
        Set vRange = .Cells.Find(What:="縦パラメータ", LookAt:=xlWhole)
        Set hRange = .Cells.Find(What:="横パラメータ", LookAt:=xlWhole)
        If Not vRange Is Nothing Then
            maxY = vRange.Offset(1, 1).Value
            minY = vRange.Offset(1, 2).Value
            stepY = vRange.Offset(1, 3).Value
        End If
        If Not hRange Is Nothing Then
            maxX = hRange.Offset(1, 1).Value
            minX = hRange.Offset(1, 2).Value
            stepX = hRange.Offset(1, 3).Value
        End If
    End With
    
    '* グラフの縦軸の最大値／最小値／ステップを設定
    Call SetGraphParamY(currChartObj, maxY, minY, stepY)
    
    '* グラフの縦軸の最大値／最小値／ステップを設定
    Call SetGraphParamX(currChartObj, maxX, minX, stepX)
    
End Sub

'* ピボットテーブルアップデート
'*******************************
'* 複写対象は、
'* �@CTM名
'* �A対象種別(＝凡例)
'* �B開始時刻
'* �C終了時刻
'* �D時間
'*******************************
Public Function UpdatePivotTable() As Boolean

    Dim startTimeElm        As String
    Dim endTimeElm          As String
    Dim rapTimeElm          As String
    Dim typeElm             As String
    Dim enzan               As String
    
    Dim hMaxValue           As Integer
    Dim hMinValue           As Integer
    Dim hStepValue          As Integer
    Dim workStr             As String
    Dim srchRange           As Range
    Dim workRange           As Range
    Dim copyRange           As Range
    Dim distRange           As Range
    Dim startRange          As Range
    Dim endRange            As Range
    Dim lastRange           As Range
    Dim dataRange           As Range
    Dim currWorksheet       As Worksheet
    Dim srcWorksheet        As Worksheet
    Dim pvtWorksheet        As Worksheet
    Dim colIndex            As Integer
    Dim rowIndex            As Integer
    Dim allNum              As Integer
    Dim workLong            As Long
    Dim currPivotTable      As PivotTable
    Dim currPivotField      As PivotField
    Dim firstAddress        As String
    Dim strDataArray()      As String
    Dim strDataIndex        As Integer
    Dim iFirstFlag          As Boolean
    Dim pvtStr              As String
    Dim workTime            As String
    Dim ctmList()           As CTMINFO      ' CTM情報
    Dim II                  As Integer
    
    Dim sampleType          As String
    Dim gripSheet           As String
    Dim gripList            As Variant
    Dim gripKaishi          As Variant
    Dim gripSyuryo          As Variant
    Dim gripRslt            As Variant
    Dim rowRange            As Range
    
    Dim motoWorksheet       As Worksheet
    Dim sakiWorksheet       As Worksheet
    Dim psramWorksheet      As Worksheet
    Dim wrkStr              As Variant
    
    UpdatePivotTable = True
    
    '******* アラート停止を追加 20160712 追加
    Application.DisplayAlerts = False
    '*******
    
    Worksheets(GraphEditSheetName).Activate
    
    '* paramシートから必要な情報を取得する
    With Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="開始エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then startTimeElm = srchRange.Offset(0, 1).Value

        Set srchRange = .Cells.Find(What:="終了エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then endTimeElm = srchRange.Offset(0, 1).Value

        Set srchRange = .Cells.Find(What:="結果エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then rapTimeElm = srchRange.Offset(0, 1).Value

        Set srchRange = .Cells.Find(What:="エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then typeElm = srchRange.Offset(0, 1).Value
        If typeElm = "" Then typeElm = "CTM NAME"
        
        
        Set srchRange = .Cells.Find(What:="演算", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then enzan = srchRange.Offset(0, 1).Value
        
        '追加20160606*******************************************
        sampleType = ""
        Set srchRange = .Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
        '*******************************************************
    End With
    
    '* ピボットテーブル生成用のシートへ取得CTMデータを必要なものだけ転記する
    Set currWorksheet = Worksheets(ForPivotSheetName)
    '* 以前のデータを削除する
    With currWorksheet
        Set srchRange = currWorksheet.Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
        workRange.ClearContents
    End With
            
    '* CTM情報一覧をシートから取得
    'Call GetCTMList(ParamSheetName, ctmList)
    
    If sampleType <> "GRIP" Then
            '開始時刻設定
        gripKaishi = Split(rapTimeElm, "・")   ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
        
        Set psramWorksheet = Worksheets(ParamSheetName)     'paramシート
        Set motoWorksheet = Worksheets(gripKaishi(0))       'GRIPミッションシート
        Set sakiWorksheet = Worksheets(ForPivotSheetName)   '演算結果データ
        
        '開始CTM位置
        Set srchRange = motoWorksheet.Cells.Find(What:=gripKaishi(1), LookAt:=xlWhole)
        If srchRange Is Nothing Then
            MsgBox startTimeElm & "：開始CTMもしくはｴﾚﾒﾝﾄのﾃﾞｰﾀがありません!!", vbExclamation
            UpdatePivotTable = False

            'Application.DisplayAlerts = False     '---確認メッセージ非表示
            'Application.Quit                      '---Excelを終了します
            Exit Function
        Else
            '開始CTM名称(1)
            Set copyRange = motoWorksheet.Range("A:A")
            Set distRange = sakiWorksheet.Range("A:A")
            copyRange.Copy Destination:=distRange
            
            'CTM受信時間
            Set copyRange = motoWorksheet.Range("B:B")
            Set distRange = sakiWorksheet.Range("B:B")
            copyRange.Copy Destination:=distRange

            '開始時刻コピー
            wrkStr = srchRange.Row
            wrkStr = srchRange.Rows
            wrkStr = Split(srchRange.Address, "$")
            
            Set copyRange = motoWorksheet.Range(wrkStr(1) & ":" & wrkStr(1))
            Set distRange = sakiWorksheet.Range("C:C")
            copyRange.Copy Destination:=distRange
        End If
        
        '* 2〜3行を削除（型／単位）
        Set workRange = currWorksheet.Range("2:3")
        workRange.Delete
        
        'エレメントの数字チェック 20160727 M.Ito
        If IsNumeric(Worksheets(ForPivotSheetName).Range("C2").Value) Then
        Else
            MsgBox "選択エレメントが数値ではありません！！"
            UpdatePivotTable = False

'            Application.DisplayAlerts = False     '---確認メッセージ非表示
'            Application.Quit                      '---Excelを終了します
            Exit Function
        End If
    Else
        '追加20160606 GRIPシート名を決定する。**************************************
        Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="シート一覧", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then gripSheet = srchRange.Offset(0, 1).Value
        gripList = Split(gripSheet, ",")
        
        '開始、終了エレメント取込
        gripKaishi = Split(startTimeElm, "・")   ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
        gripSyuryo = Split(endTimeElm, "・")     ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
        gripRslt = Split(rapTimeElm, "・")     ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
        
        Set psramWorksheet = Worksheets(ParamSheetName)     'paramシート
        Set motoWorksheet = Worksheets(gripList(0))         'GRIPミッションシート
        Set sakiWorksheet = Worksheets(ForPivotSheetName)   '演算結果データ
        
        '開始CTM位置
        Set srchRange = motoWorksheet.Cells.Find(What:=gripRslt(0), LookAt:=xlWhole)
        If srchRange Is Nothing Then
            MsgBox "開始CTMもしくはｴﾚﾒﾝﾄのﾃﾞｰﾀがありません!!", vbExclamation
            UpdatePivotTable = False

'            Application.DisplayAlerts = False     '---確認メッセージ非表示
'            Application.Quit                      '---Excelを終了します
            Exit Function
        End If
                   
        'CTM名称(1)
        Set copyRange = motoWorksheet.Range("A:A")
        Set distRange = sakiWorksheet.Range("B:B")
        copyRange.Copy Destination:=distRange
        
        'CTM受信時間
        Set copyRange = motoWorksheet.Range("B:B")
        Set distRange = sakiWorksheet.Range("A:A")
        copyRange.Copy Destination:=distRange
        
        '開始エレメント位置
        Set rowRange = motoWorksheet.Cells.Find(What:=gripRslt(1), LookAt:=xlWhole, SearchOrder:=xlByRows, after:=srchRange)
        If rowRange Is Nothing Then
            MsgBox "開始CTMもしくはｴﾚﾒﾝﾄのﾃﾞｰﾀがありません!!", vbExclamation
            UpdatePivotTable = False

'            Application.DisplayAlerts = False     '---確認メッセージ非表示
'            Application.Quit                      '---Excelを終了します
            Exit Function
        End If
        
        '結果エレメント列コピー
        Set copyRange = rowRange.EntireColumn
        Set distRange = sakiWorksheet.Range("C:C")
        copyRange.Copy Destination:=distRange
                
        '* 受信時刻がないCTMを削除する
        Set currWorksheet = Worksheets(ForPivotSheetName)
        With currWorksheet
            Set srchRange = .Range(.Range("A3"), .Range("A3").End(xlDown)).Offset(0, 0)
            Call DeleteMultiLine("", srchRange)
        End With
            
        '* 2〜3行を削除（型／単位）
        Set workRange = sakiWorksheet.Range("2:2")
        workRange.Delete
        'workRange.ClearContents
        
        Call NullDelete(ForPivotSheetName)
        
'        '秒算出式の追加
'        Set currWorksheet = Worksheets(ForPivotSheetName)
'        With currWorksheet
'            Set srchRange = .Range(.Range("E2"), .Range("E2").End(xlDown)).Offset(0, 0)
'            workStr = Replace(srchRange.Address, "E", "D")
'            .Range(workStr).Formula = "=ROUND((F2-E2)*86400, 0)"    '終了日時-開始日時*24*60*60
'        End With
        
        currWorksheet.Cells(1, 1).Value = "RECEIVE TIME"
        currWorksheet.Cells(1, 2).Value = "CTM NAME"
        currWorksheet.Cells(1, 3).Value = gripRslt(1)
        
        '***************************************************************************
    End If
    
    '* ピボットテーブルのデータ範囲取得
    With currWorksheet
        Set lastRange = .Cells(1, 3).End(xlDown)
        Set dataRange = .Range(.Cells(1, 1), lastRange.Cells)
        dataRange.Name = "ピボットデータ範囲"
    End With
    workStr = ForPivotSheetName & "!R1C1:R" & lastRange.Row & ADDR_DataCell '"C3"
    
    '* ピボットテーブルの削除
    Set pvtWorksheet = Worksheets(PivotSheetName)
    With pvtWorksheet
        For Each currPivotTable In pvtWorksheet.PivotTables
            currPivotTable.TableRange1.ClearContents
        Next currPivotTable
    End With
    
    '* ピボットテーブルの作成
    Call goMakePivotTable
    
    '* メインピボットテーブルのデータ範囲更新
    Set pvtWorksheet = Worksheets(PivotSheetName)
    With pvtWorksheet
        Set currPivotTable = .PivotTables(PivotMainTableName)
        With currPivotTable
            .SourceData = workStr
            .RefreshTable
        End With
        Set srchRange = .Cells.Find(What:="横パラメータ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            hMaxValue = srchRange.Offset(1, 1).Value
            hMinValue = srchRange.Offset(1, 2).Value
            hStepValue = srchRange.Offset(1, 3).Value
        End If
    End With
    
    'データ無しの場合グラフの修正しない。20161028 ADD
    '****** 2017/02/24 (CInt(hMinValue) > 0---->(CInt(hMinValue) >= 0
    If (CInt(hMinValue) >= 0) And (CInt(hMaxValue) > 0) And (CInt(hStepValue) > 0) Then
    Else
        UpdatePivotTable = False
        Exit Function
    End If

    '* データの最大値／最小値／ステップを設定
    Call ChangeMaxMinStepPVT(currPivotTable, hMinValue, hMaxValue, hStepValue)
   
End Function

'* ピボットテーブルアップデート
'*******************************
'* 複写対象は、
'* �@CTM名
'* �A対象種別(＝凡例)
'* �B開始時刻
'* �C終了時刻
'* �D時間
'*******************************
Public Sub UpdatePivotTable_NG()
    Dim startTimeElm        As String
    Dim endTimeElm          As String
    Dim rapTimeElm          As String
    Dim typeElm             As String
    Dim enzan               As String
    
    Dim hMaxValue           As Integer
    Dim hMinValue           As Integer
    Dim hStepValue          As Integer
    Dim workStr             As String
    Dim srchRange           As Range
    Dim workRange           As Range
    Dim copyRange           As Range
    Dim distRange           As Range
    Dim startRange          As Range
    Dim endRange            As Range
    Dim lastRange           As Range
    Dim dataRange           As Range
    Dim currWorksheet       As Worksheet
    Dim srcWorksheet        As Worksheet
    Dim pvtWorksheet        As Worksheet
    Dim colIndex            As Integer
    Dim rowIndex            As Integer
    Dim allNum              As Integer
    Dim workLong            As Long
    Dim currPivotTable      As PivotTable
    Dim currPivotField      As PivotField
    Dim firstAddress        As String
    Dim strDataArray()      As String
    Dim strDataIndex        As Integer
    Dim iFirstFlag          As Boolean
    Dim pvtStr              As String
    Dim workTime            As String
    Dim ctmList()           As CTMINFO      ' CTM情報
    Dim II                  As Integer
    
    Dim sampleType          As String
    Dim gripSheet           As String
    Dim gripList            As Variant
    Dim gripKaishi          As Variant
    Dim gripSyuryo          As Variant
    Dim gripRslt            As Variant
    Dim rowRange            As Range
    
    Dim motoWorksheet       As Worksheet
    Dim sakiWorksheet       As Worksheet
    Dim psramWorksheet      As Worksheet
    
    
    '******* アラート停止を追加 20160712 追加
    Application.DisplayAlerts = False
    '*******
    
    Worksheets(GraphEditSheetName).Activate
    
    '* paramシートから必要な情報を取得する
    With Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="開始エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then startTimeElm = srchRange.Offset(0, 1).Value

        Set srchRange = .Cells.Find(What:="終了エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then endTimeElm = srchRange.Offset(0, 1).Value

        Set srchRange = .Cells.Find(What:="結果エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then rapTimeElm = srchRange.Offset(0, 1).Value

        Set srchRange = .Cells.Find(What:="エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then typeElm = srchRange.Offset(0, 1).Value
        If typeElm = "" Then typeElm = "CTM NAME"
        
        
        Set srchRange = .Cells.Find(What:="演算", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then enzan = srchRange.Offset(0, 1).Value
        
        '追加20160606*******************************************
        sampleType = ""
        Set srchRange = .Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then sampleType = srchRange.Offset(0, 1).Value
        '*******************************************************
    End With
    
    '* ピボットテーブル生成用のシートへ取得CTMデータを必要なものだけ転記する
    Set currWorksheet = Worksheets(ForPivotSheetName)
    '* 以前のデータを削除する
    With currWorksheet
        Set srchRange = currWorksheet.Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
        workRange.ClearContents
    End With
            
    '* CTM情報一覧をシートから取得
    Call GetCTMList(ParamSheetName, ctmList)
    
    If sampleType <> "GRIP" Then
        For Each srcWorksheet In Worksheets
        
            For II = 0 To UBound(ctmList)
                If (ctmList(II).Name = srcWorksheet.Name) Then Exit For
            Next
            If II <= UBound(ctmList) Then
            
                colIndex = 3
                
                '* ピボットテーブル用の表へRECEIVETIME／CTM名列を複写する
                Set copyRange = srcWorksheet.Range("A:B")
                Set distRange = currWorksheet.Range("A:B")
                copyRange.Copy Destination:=distRange
                
    '            '* ピボットテーブル用の表へ対象種別列を複写する
    '            Set srchRange = srcWorksheet.Range("1:1")       ' １行目を検索範囲にセット
    '            Set workRange = srchRange.Find(What:=typeElm, LookAt:=xlWhole)
    '            If Not workRange Is Nothing Then
    '                Set copyRange = workRange.EntireColumn
    '                Set distRange = currWorksheet.Columns(colIndex)
    '                copyRange.Copy Destination:=distRange
    '                colIndex = colIndex + 1
    '            End If
                
                '* 結果エレメント：時間列の指定があれば、ピボットテーブル用の表へ時間列を複写する
                If rapTimeElm <> "" Then
                    rapTimeElm = Right(rapTimeElm, Len(rapTimeElm) - InStr(rapTimeElm, "・"))
                    Set srchRange = srcWorksheet.Range("1:1")
                    Set workRange = srchRange.Find(What:=rapTimeElm, LookAt:=xlWhole)
                    If Not workRange Is Nothing Then
                        Set copyRange = workRange.EntireColumn
                        Set distRange = currWorksheet.Columns(colIndex)
                        copyRange.Copy Destination:=distRange
                    End If
                End If
                colIndex = colIndex + 1
                
    '''            '* ピボットテーブル用の表へ開始時刻列を複写する
    '''            If startTimeElm <> "" Then
    '''                startTimeElm = Right(startTimeElm, Len(startTimeElm) - InStr(startTimeElm, "・"))
    '''                Set srchRange = srcWorksheet.Range("1:1")
    '''                Set workRange = srchRange.Find(What:=startTimeElm, LookAt:=xlWhole)
    '''                If Not workRange Is Nothing Then
    '''                    Set copyRange = workRange.EntireColumn
    '''                    Set distRange = currWorksheet.Columns(colIndex)
    '''                    copyRange.Copy Destination:=distRange
    '''                    colIndex = colIndex + 1
    '''                End If
    '''            End If
    '''
    '''            '* ピボットテーブル用の表へ終了時刻列を複写する
    '''            If endTimeElm <> "" Then
    '''                endTimeElm = Right(endTimeElm, Len(endTimeElm) - InStr(endTimeElm, "・"))
    '''                Set srchRange = srcWorksheet.Range("1:1")
    '''                Set workRange = srchRange.Find(What:=endTimeElm, LookAt:=xlWhole)
    '''                If Not workRange Is Nothing Then
    '''                    Set copyRange = workRange.EntireColumn
    '''                    Set distRange = currWorksheet.Columns(colIndex)
    '''                    copyRange.Copy Destination:=distRange
    '''                    colIndex = colIndex + 1
    '''                End If
    '''            End If
    '''
    '''            '* 終了時刻がないCTMを削除する
    '''            With currWorksheet
    '''                Set srchRange = .Range(.Range("A4"), .Range("A4").End(xlDown)).Offset(0, 5)
    '''                Call DeleteMultiLine("", srchRange)
    '''            End With
                
                '* 2〜3行を削除（型／単位）
                Set workRange = currWorksheet.Range("2:3")
                workRange.Delete
                
    '''            '* RECEIVE TIMEの書式設定
    '''            With currWorksheet
    '''                Set workRange = currWorksheet.Range(.Range("A2"), .Range("A2").End(xlDown).Cells)
    '''                workRange.NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
    '''            End With
    '''
    '''            '* 時間列の指定がなければ、ピボットテーブル用の表へ時間列を計算する
    '''            If rapTimeElm = "" Then
    '''                rapTimeElm = "経過時間(秒)"
    '''                currWorksheet.Cells(1, 4).Value = rapTimeElm
    '''                Set distRange = currWorksheet.Cells(2, 4)
    '''                Set startRange = currWorksheet.Cells(2, 5)
    '''                Set lastRange = currWorksheet.Cells(2, 1).End(xlDown)
    '''                Set endRange = currWorksheet.Cells(2, 6)
    '''                rowIndex = 0
    '''                Do While startRange.Offset(rowIndex, 0).Row <= lastRange.Row
    '''                    workLong = GetUnixTime(endRange.Offset(rowIndex, 0).Value) - GetUnixTime(startRange.Offset(rowIndex, 0))
    '''                    distRange.Offset(rowIndex, 0).Value = workLong / 1000
    '''                    rowIndex = rowIndex + 1
    '''                    DoEvents
    '''                Loop
    '''            End If
                
                Exit For
            
            End If
        Next srcWorksheet
    Else
        '追加20160606 GRIPシート名を決定する。**************************************
        Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="シート一覧", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then gripSheet = srchRange.Offset(0, 1).Value
        gripList = Split(gripSheet, ",")
        
        '開始、終了エレメント取込
        gripKaishi = Split(startTimeElm, "・")   ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
        gripSyuryo = Split(endTimeElm, "・")     ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
        gripRslt = Split(rapTimeElm, "・")     ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
        
        Set psramWorksheet = Worksheets(ParamSheetName)     'paramシート
        Set motoWorksheet = Worksheets(gripList(0))         'GRIPミッションシート
        Set sakiWorksheet = Worksheets(ForPivotSheetName)   '演算結果データ
        
        '開始CTM位置
        Set srchRange = motoWorksheet.Cells.Find(What:=gripRslt(0), LookAt:=xlWhole)
        If srchRange Is Nothing Then
            MsgBox "開始CTMもしくはｴﾚﾒﾝﾄのﾃﾞｰﾀがありません!!", vbExclamation
'            Application.DisplayAlerts = False     '---確認メッセージ非表示
'            Application.Quit                      '---Excelを終了します
            End
            Exit Sub
        End If
                   
        'CTM名称(1)
        Set copyRange = motoWorksheet.Range("A:A")
        Set distRange = sakiWorksheet.Range("B:B")
        copyRange.Copy Destination:=distRange
        
        'CTM受信時間
        Set copyRange = motoWorksheet.Range("B:B")
        Set distRange = sakiWorksheet.Range("A:A")
        copyRange.Copy Destination:=distRange
        
        '開始エレメント位置
        Set rowRange = motoWorksheet.Cells.Find(What:=gripRslt(1), LookAt:=xlWhole, SearchOrder:=xlByRows, after:=srchRange)
        If rowRange Is Nothing Then
            MsgBox "開始CTMもしくはｴﾚﾒﾝﾄのﾃﾞｰﾀがありません!!", vbExclamation
'            Application.DisplayAlerts = False     '---確認メッセージ非表示
'            Application.Quit                      '---Excelを終了します
            End
            Exit Sub
        End If
        
        '結果エレメント列コピー
        Set copyRange = rowRange.EntireColumn
        Set distRange = sakiWorksheet.Range("C:C")
        copyRange.Copy Destination:=distRange
                
        '* 受信時刻がないCTMを削除する
        Set currWorksheet = Worksheets(ForPivotSheetName)
        With currWorksheet
            Set srchRange = .Range(.Range("A3"), .Range("A3").End(xlDown)).Offset(0, 0)
            Call DeleteMultiLine("", srchRange)
        End With
            
        '* 2〜3行を削除（型／単位）
        Set workRange = sakiWorksheet.Range("2:2")
        workRange.Delete
        'workRange.ClearContents
        Call NullDelete(ForPivotSheetName)
        
'        '秒算出式の追加
'        Set currWorksheet = Worksheets(ForPivotSheetName)
'        With currWorksheet
'            Set srchRange = .Range(.Range("E2"), .Range("E2").End(xlDown)).Offset(0, 0)
'            workStr = Replace(srchRange.Address, "E", "D")
'            .Range(workStr).Formula = "=ROUND((F2-E2)*86400, 0)"    '終了日時-開始日時*24*60*60
'        End With
        
        currWorksheet.Cells(1, 1).Value = "RECEIVE TIME"
        currWorksheet.Cells(1, 2).Value = "CTM NAME"
        currWorksheet.Cells(1, 3).Value = gripRslt(1)
        
        '***************************************************************************
    End If
    
    '* ピボットテーブルのデータ範囲取得
    With currWorksheet
        Set lastRange = .Cells(1, 3).End(xlDown)
        Set dataRange = .Range(.Cells(1, 1), lastRange.Cells)
        dataRange.Name = "ピボットデータ範囲"
    End With
    workStr = ForPivotSheetName & "!R1C1:R" & lastRange.Row & ADDR_DataCell '"C3"
    
    '* ピボットテーブルの削除
    Set pvtWorksheet = Worksheets(PivotSheetName)
    With pvtWorksheet
        For Each currPivotTable In pvtWorksheet.PivotTables
            currPivotTable.TableRange1.ClearContents
        Next currPivotTable
    End With
    
    '* ピボットテーブルの作成
    Call goMakePivotTable
    
    '* メインピボットテーブルのデータ範囲更新
    Set pvtWorksheet = Worksheets(PivotSheetName)
    With pvtWorksheet
        Set currPivotTable = .PivotTables(PivotMainTableName)
        With currPivotTable
            .SourceData = workStr
            .RefreshTable
        End With
        Set srchRange = .Cells.Find(What:="横パラメータ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            hMaxValue = srchRange.Offset(1, 1).Value
            hMinValue = srchRange.Offset(1, 2).Value
            hStepValue = srchRange.Offset(1, 3).Value
        End If
    End With
    
    'データ無しの場合グラフの修正しない。20161028 ADD
    If (CInt(hMinValue) > 0) And (CInt(hMaxValue) > 0) And (CInt(hStepValue) > 0) Then
    Else
        Exit Sub
    End If

    '* データの最大値／最小値／ステップを設定
    Call ChangeMaxMinStepPVT(currPivotTable, hMinValue, hMaxValue, hStepValue)
   
End Sub

'******************************************
'* 指定範囲内にある対象文字列行を削除する
'******************************************
Public Sub DeleteMultiLine(delStr As String, srchRange As Range)
    Dim currWorksheet   As Worksheet
    Dim workRange       As Range
    Dim strAddress      As String
    Dim delRange        As Range

    Set currWorksheet = Worksheets(ForPivotSheetName)
    With currWorksheet
        '* 削除対象セル範囲を取得
        Set workRange = srchRange.Find(What:=delStr, LookAt:=xlWhole)
        If Not workRange Is Nothing Then
            Set delRange = workRange
            strAddress = workRange.Address
            Do While Not workRange Is Nothing

                Set workRange = srchRange.FindNext(workRange)
                If strAddress = workRange.Address Then
                    Exit Do
                End If
                
                '* 対象セルを集める
                Set delRange = Union(delRange, workRange)
            
            Loop
            delRange.EntireRow.Delete
        End If
    End With
    
End Sub

'* 指定グラフの縦軸の最大／最大／刻み幅を設定する
Public Sub SetGraphParamY(currChartObj As ChartObject, maxY As Double, minY As Double, stepY As Double)
        
    With currChartObj.Chart
        
        '* 縦軸の最大／最小の設定
        .Axes(xlValue).MinimumScale = minY
        .Axes(xlValue).MaximumScale = maxY
        .Axes(xlValue).MajorUnit = stepY
'        .Axes(xlValue).MajorUnitIsAuto = True ' 自動設定
'        .Axes(xlValue).MinimumScaleIsAuto = True ' 自動設定
'        .Axes(xlValue).MaximumScaleIsAuto = True ' 自動設定
    End With
    
End Sub

'* 指定グラフの横軸の最大／最大／刻み幅を設定する
Public Sub SetGraphParamX(currChartObj As ChartObject, maxX As Double, minX As Double, stepX As Double)
        
    With currChartObj.Chart
        
        '* 横軸の最大／最小の設定
        .Axes(xlCategory).MinimumScale = minX
        .Axes(xlCategory).MaximumScale = maxX
        .Axes(xlCategory).MajorUnit = stepX
'        .Axes(xlCategory).MajorUnitIsAuto = True ' 自動設定
'        .Axes(xlCategory).MinimumScaleIsAuto = True ' 自動設定
'        .Axes(xlCategory).MaximumScaleIsAuto = True ' 自動設定
        
    End With
    
End Sub

'* 背景データをグラフシートへ記載する
Public Sub WriteBackgroundData(graphWorksheet As Worksheet, strData() As String, outFlag As Boolean)
    Dim writeRange          As Range
    Dim srchRange           As Range
    Dim lastRange           As Range
    Dim workRange           As Range
    Dim dataRange           As Range
    Dim workStr             As Variant
    Dim writeIndex          As Integer
    Dim dataValue           As String
    Dim dataUnit            As String
    Dim currWorksheet       As Worksheet
    
    
    '* 取得CTMデータシート
    Set currWorksheet = Worksheets(1)

    With graphWorksheet
    
        Set srchRange = .Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
        
            '* 古いデータを削除
            Set writeRange = srchRange.Offset(2, 0)
            Set lastRange = writeRange.End(xlDown)
            Set workRange = .Range(writeRange, lastRange.Offset(0, 2))
            workRange.Clear
            workRange.Interior.Color = RGB(255, 255, 255)
            
            '* 背景データ出力フラグがONならば出力する
            If outFlag Then
                writeIndex = 0
                '* 着目背景データ文字列を順番に出力する
                For Each workStr In strData
                    '* CTM取得一覧から背景データを検索する
                    If InStr(workStr, "・") Then
                        workStr = Right(workStr, Len(workStr) - InStr(workStr, "・"))
                    End If
                    Set dataRange = currWorksheet.Cells.Find(What:=workStr, LookAt:=xlWhole)
                    If Not dataRange Is Nothing Then
                        dataValue = dataRange.Offset(4, 0).Value
                        dataUnit = dataRange.Offset(2, 0).Value
                    End If
                    
                    writeRange.Offset(writeIndex, 0).Value = workStr
                    writeRange.Offset(writeIndex, 1).Value = dataValue
                    writeRange.Offset(writeIndex, 2).Value = dataUnit
                    Set workRange = .Range(writeRange.Offset(writeIndex, 0), writeRange.Offset(writeIndex, 2))
                    With workRange
                        .Borders.LineStyle = xlContinuous
                        .Borders.Weight = xlThin
                        .Borders(xlEdgeBottom).LineStyle = xlContinuous
                        .Borders(xlEdgeBottom).Weight = xlThin
                        .Borders(xlEdgeLeft).LineStyle = xlContinuous
                        .Borders(xlEdgeLeft).Weight = xlMedium
                        .Borders(xlEdgeRight).LineStyle = xlContinuous
                        .Borders(xlEdgeRight).Weight = xlMedium
                    End With
                    writeIndex = writeIndex + 1
                Next workStr
                Set workRange = .Range(writeRange.Offset(writeIndex, 0), writeRange.Offset(writeIndex, 2))
                workRange.Borders(xlEdgeTop).LineStyle = xlContinuous
                workRange.Borders(xlEdgeTop).Weight = xlMedium
            End If
            
            workRange.EntireColumn.AutoFit
'            writeRange.Offset(0, 0).AutoFit
'            writeRange.Offset(0, 1).AutoFit
'            writeRange.Offset(0, 2).AutoFit
        End If
    
    End With
End Sub

'* 指定ピボットテーブルの最大／最小／ステップ幅を変更する
Public Sub ChangeMaxMinStepPVT(currPivotTable As PivotTable, startValue As Integer, endValue As Integer, StepValue As Integer)
    currPivotTable.rowRange.Cells(2, 1).Group Start:=startValue, End:=endValue, By:=StepValue
End Sub

'* 指定ピボットテーブルのフィールドのデータのないアイテム表示をONにする
Public Sub ToOnDispItem(currPivotField As PivotField)
    currPivotField.ShowAllItems = True
End Sub

'* 指定ピボットテーブルのデータ範囲を変更する
Public Sub ChangePivotFieldTable(currPivotTable As PivotTable, dataRange As String)
    currPivotTable.SourceData = dataRange
    '* ピボットテーブル更新
    currPivotTable.PivotCache.Refresh
End Sub

'***************
'タイマー起動開始
'***************
Public Sub TimerStart()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim endTime         As Double
    Dim TimerStopFlag   As Boolean
    Dim FinalTime       As Date

    TimerStopFlag = True
    
    Call GetMissionInfoAll
    
    With Worksheets(ParamSheetName)
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value
            updTimeUnit = srchRange.Offset(0, 2).Value
        Else
            updTime = 5
            updTimeUnit = "【秒】"
        End If
            
        '*********************
        '* 取得開始日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        FinalTime = endD + endT
        
        Select Case updTimeUnit
            Case "【秒】"
                Application.OnTime Now + TimeValue("00:00:" & Format(updTime, "00")), "'TimerLogic'"
            
            Case "【分】"
                Application.OnTime Now + TimeValue("00:" & Format(updTime, "00") & ":00"), "'TimerLogic'"
        
        End Select
    
    End With
    
    'Application.OnTime TimeValue(Format(FinalTime, "yyyy/MM/dd hh:mm:ss")), "TimerLogic", , False
    'Application.OnTime TimeValue(Format(FinalTime, "hh:mm:ss")), "TimerLogic", , False

    '前回更新日時セット
'    Set srchRange = .Cells.Find(What:="前回起動", LookAt:=xlWhole)
'    If Not srchRange Is Nothing Then
'        srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd")
'        srchRange.Offset(0, 2).Value = Format(Now, "hh:mm:ss")
'    End If
    
End Sub

'***************
'* タイマー処理
'***************
Public Sub TimerLogic()
    Dim wrkStr As String
    Dim wrkTime As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim endTime         As Double
    Dim FinalTime       As Date
    
    With Worksheets(ParamSheetName)
    
        If .Range("C1").Value <> "" Then
            MsgBox "自動更新手動停止！！"
            Application.CutCopyMode = False
            Exit Sub
        End If
    
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        FinalTime = endD + endT
        If Format(Now, "yyyy/MM/dd hh:mm:ss") >= FinalTime Then         '終了時刻になったら終わる
            MsgBox "終了時刻になりました。" & " " & Format(Now, "hh:mm:ss")
            Application.CutCopyMode = False
            Exit Sub
        End If
    
        '* ミッションから情報を取得
        Call GetMissionInfoAll
        
        '* ピボットテーブルを更新
        If UpdatePivotTable = False Then
            Exit Sub
        End If
        
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value
            updTimeUnit = srchRange.Offset(0, 2).Value
        Else
            updTime = 5
            updTimeUnit = "【秒】"
        End If
            
        Select Case updTimeUnit
            Case "【秒】"
                Application.OnTime Now + TimeValue("00:00:" & Format(updTime, "00")), "'TimerLogic'"
            
            Case "【分】"
                wrkTime = Format(Val(Sheets("オンラインテンプレート").Range("E2").Value), "00")
                Application.OnTime Now + TimeValue("00:" & Format(updTime, "00") & ":00"), "'TimerLogic'"
        
        End Select
        
    End With
    
'    Application.OnTime Now + TimeValue("00:00:05"), "'TimerLogic'"

    '前回更新日時セット
'    Set srchRange = Worksheets(MainSheetName).Cells.Find(What:="前回起動", LookAt:=xlWhole)
'    If Not srchRange Is Nothing Then
'        srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd")
'        srchRange.Offset(0, 2).Value = Format(Now, "hh:mm:ss")
'    End If
    
    
End Sub

'* ｢自動｣の処理
Sub BT_Auto_Click()
    Call TimerStart
End Sub

'* ファイルの登録
'* →指定フォルダへ保存するだけ
Public Sub SaveFileSub()
    Dim wrkInt          As Integer

    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
    Dim objFSO          As Object
    Dim msgStr          As String
    Dim IsFirstSave     As String
    
'    iReturn = MsgBox("EXCELファイル：" & ActiveWorkbook.Name & " を登録します。よろしいですか？", vbYesNo, "登録")
'    If iReturn = vbYes Then
    
        '表示倍率保存
        Call SaveBairitu
    
        Set objFSO = CreateObject("Scripting.FileSystemObject")
    
        '* テンプレート名／登録フォルダ名を取得する
        With Worksheets(ParamSheetName).Columns(1)
            Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
            Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
            Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then IsFirstSave = srchRange.Offset(0, 1).Value
        End With
        
'            wrkInt = 0
'        wrkInt = InStr(ThisWorkbook.Name, templateName)
'
'        If wrkInt <= 0 Or wrkInt > 4 Then
        If IsFirstSave = "" Then
            '* フォルダ名の最終文字のチェック
            checkStr = Right(saveFilePath, 1)
            If checkStr = "\\" Then
                fname = saveFilePath & templateName & "_" & ThisWorkbook.Name
            Else
                fname = saveFilePath & "\" & templateName & "_" & ThisWorkbook.Name
            End If
            
            '* テンプレート名／登録フォルダ名を取得する
            With Worksheets(ParamSheetName).Columns(1)
                Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
                If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "TRUE"
            End With
        Else
            '* フォルダ名の最終文字のチェック
            checkStr = Right(saveFilePath, 1)
            If checkStr = "\\" Then
                fname = saveFilePath & ThisWorkbook.Name
            Else
                fname = saveFilePath & "\" & ThisWorkbook.Name
            End If
        End If
        
        '* 登録フォルダにファイルを上書き複写する
        '        objFSO.CopyFile currThisFile, fname
        If Dir(fname) <> "" Then
          msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
          If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
        End If
        
        '各種バー表示
        Call DispBar
        
        '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
        Application.DisplayAlerts = False
        
        '* ISSUE_NO.624 Add ↓↓↓ *******************************
        On Error GoTo ErrorHandler
        
        ThisWorkbook.SaveAs Filename:=fname
        
ErrorHandler:
        '-- 例外処理
        If Err.Description <> "" Then
            MsgBox Err.Description, vbCritical & vbOKOnly, "警告"
        End If
        '* ISSUE_NO.624 Add ↑↑↑ *******************************
        
        If Application.Workbooks.Count > 1 Then
            ThisWorkbook.Close
        Else
            Application.Quit
            ThisWorkbook.Close
        End If
        
        If Err.Description = "" Then
            Application.DisplayAlerts = True
        End If
    
End Sub


