Attribute VB_Name = "MenuBar_Module"
''********************************
''　EXCEL起動時の処理
''　　　　メニューバーの作成
''********************************
Public Sub MakeMenuBar()
    Dim objCB           As CommandBar
    Dim objCBCtrl       As CommandBarControl
    Dim objCBCtrlSub    As CommandBarControl
    
    Exit Sub
    
    On Error Resume Next
    Application.CommandBars("AIS_LeadTime_Bar").Delete
    ThisWorkbook.Application.CommandBars.Add Name:="AIS_LeadTime_Bar", Position:=msoBarTop, MenuBar:=True
    Set objCB = ThisWorkbook.Application.CommandBars("AIS_LeadTime_Bar")
    objCB.Visible = True
    
''            '* 「リフレッシュ」を追加
''    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
''    With objCBCtrl
''        .Style = msoButtonIconAndCaption
''        .Caption = "最前面表示"   'メニュー名称
''        .OnAction = "OnTIme_Module.OnTimeStart"   '起動プログラム
''        .BeginGroup = False          '直前に区切り線有り
''        .FaceId = 1667                   'アイコン番号
''    End With
''
''        '* 「リフレッシュ」を追加
''    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
''    With objCBCtrl
''        .Style = msoButtonIconAndCaption
''        .Caption = "最前面表示停止"   'メニュー名称
''        .OnAction = "OnTIme_Module.OnTimeCancel"   '起動プログラム
''        .BeginGroup = False          '直前に区切り線有り
''        .FaceId = 1668               'アイコン番号
''    End With
    
    '* 「最大表示」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "最大表示"   'メニュー名称
        .OnAction = "DisplaySize_Module.ZoomDisplayFull"   '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 178               'アイコン番号
    End With
    
    '* 「表示（＋）」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIcon
        .Caption = "＋１０％）"   'メニュー名称
        .OnAction = "DisplaySize_Module.ZoomDisplayPlus"   '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 444               'アイコン番号
    End With
    
    '* 「表示（−）」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIcon
        .Caption = "−１０％"   'メニュー名称
        .OnAction = "DisplaySize_Module.ZoomDisplayMinus"   '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 445               'アイコン番号
    End With
    
    '* 「表示範囲登録」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "表示範囲設定"   'メニュー名称
        .OnAction = "DisplaySize_Module.ZoomSetDisplayRange"   '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 4359               'アイコン番号
    End With
    
    '* 「範囲設定解除」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "範囲設定解除"   'メニュー名称
        .OnAction = "DisplaySize_Module.ZoomUnSetDisplayRange"   '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 4272               'アイコン番号
    End With
    
    ''「グラフ更新」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "グラフ更新"   'メニュー名称
'        .OnAction = "Main_Module.UpdateAfterGraphEdit"      '起動ﾌﾟﾛｸﾞﾗﾑ
        .OnAction = "subScreen.frmGraphSetDisp"      '起動ﾌﾟﾛｸﾞﾗﾑ
        .BeginGroup = False         '直前に区切り線無し
        .FaceId = 435                 'アイコン番号
    End With
    
        ''「工学変換グラフ更新」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "工学値変換"   'メニュー名称
        .OnAction = "subScreen.frmEngValDisp"      '起動ﾌﾟﾛｸﾞﾗﾑ"
        .BeginGroup = False         '直前に区切り線無し
        .FaceId = 1081                 'アイコン番号
    End With
    
            ''「データアタッチ」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "データアタッチ"   'メニュー名称
        .OnAction = "subScreen.frmBackDataDisp"   '起動プログラム
        .BeginGroup = True          '直前に区切り線有り
        .FaceId = 2059               'アイコン番号
    End With

    ''「登録」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "登録"   'メニュー名称
        .OnAction = "Main_Module.SaveFileSub"   '起動プログラム
        .BeginGroup = True          '直前に区切り線有り
        .FaceId = 271               'アイコン番号
    End With
    
     
End Sub


''******************************************
''　EXCEL終了時の処理
''　　　　メニューバーの削除
''　削除しないと、他EXCELでも表示する為
''******************************************
Public Sub Auto_close()

On Error Resume Next
    
    '各種バー表示
    Call DispBar

    Call OnTimeCancel

''    Application.CommandBars("AIS_LeadTime_Bar").Delete

    'Call ScreenDisplayONOFF(True)
    
    Application.DisplayAlerts = False
    
    ThisWorkbook.Save
    Application.Quit

End Sub

''******************************************
''　EXCEL終了時(×マーク押下時)の処理
''　　　各種バーの表示
''******************************************
Public Sub DispBar()

    With ActiveWindow
        .DisplayHorizontalScrollBar = True  '水平スクロールバー
        .DisplayVerticalScrollBar = True  '垂直スクロールバー
        .DisplayWorkbookTabs = True  'シート見出し
'''        .DisplayFormulas = True  'セルが数式表示となる
        .DisplayWorkbookTabs = True
    End With

    With Application
        .DisplayFormulaBar = True  '数式バー
        .DisplayStatusBar = True    'ステータスバー
    End With
    
End Sub

