VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmGraphSet 
   Caption         =   "OtŌWæĘ"
   ClientHeight    =   4185
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   6195
   OleObjectBlob   =   "frmGraphSet.frx":0000
   StartUpPosition =   1  'I[i[ tH[Ė
End
Attribute VB_Name = "frmGraphSet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub CommandButton1_Click()
    Dim currWorksheet       As Worksheet
    Dim srchRange           As Range
    
    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="|cē|", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 1).Value = txtVmax.text
        srchRange.Offset(2, 1).Value = txtVmin.text
        srchRange.Offset(3, 1).Value = txtVstep.text
    End If
    
    Set srchRange = currWorksheet.Cells.Find(What:="|Ąē|", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 1).Value = txtHmax.text
        srchRange.Offset(2, 1).Value = txtHmin.text
        srchRange.Offset(3, 1).Value = txtHstep.text
    End If
    
    Set srchRange = currWorksheet.Cells.Find(What:="|cēîü|", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 0).Value = txtVname1.text
        srchRange.Offset(2, 0).Value = txtVname2.text
        srchRange.Offset(3, 0).Value = txtVname3.text
        srchRange.Offset(4, 0).Value = txtVname4.text
        srchRange.Offset(5, 0).Value = txtVname5.text

        srchRange.Offset(1, 1).Value = txtVval1.text
        srchRange.Offset(2, 1).Value = txtVval2.text
        srchRange.Offset(3, 1).Value = txtVval3.text
        srchRange.Offset(4, 1).Value = txtVval4.text
        srchRange.Offset(5, 1).Value = txtVval5.text
    End If
    
    Set srchRange = currWorksheet.Cells.Find(What:="|Ąēîü|", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(1, 0).Value = txtHname1.text
        srchRange.Offset(2, 0).Value = txtHname2.text
        srchRange.Offset(3, 0).Value = txtHname3.text
        srchRange.Offset(4, 0).Value = txtHname4.text
        srchRange.Offset(5, 0).Value = txtHname5.text
        
        srchRange.Offset(1, 1).Value = txtHval1.text
        srchRange.Offset(2, 1).Value = txtHval2.text
        srchRange.Offset(3, 1).Value = txtHval3.text
        srchRange.Offset(4, 1).Value = txtHval4.text
        srchRange.Offset(5, 1).Value = txtHval5.text
    End If


    'OtXV
    Call Main_Module.UpdateAfterGraphEdit
End Sub

Private Sub txtVname1_AfterUpdate()

    Call VLineVisible(1)
    
End Sub

Private Sub txtVname2_AfterUpdate()

    Call VLineVisible(2)
    
End Sub

Private Sub txtVname3_AfterUpdate()

    Call VLineVisible(3)
    
End Sub

Private Sub txtVname4_AfterUpdate()

    Call VLineVisible(4)
    
End Sub

Private Sub txtVname5_AfterUpdate()

    Call VLineVisible(5)
    
End Sub

Private Sub txtHname1_AfterUpdate()

    Call HLineVisible(1)
    
End Sub

Private Sub txtHname2_AfterUpdate()

    Call HLineVisible(2)
    
End Sub

Private Sub txtHname3_AfterUpdate()

    Call HLineVisible(3)
    
End Sub

Private Sub txtHname4_AfterUpdate()

    Call HLineVisible(4)
    
End Sub

Private Sub txtHname5_AfterUpdate()

    Call HLineVisible(5)
    
End Sub

Public Sub VLineVisible(argPos As Integer)

'    Dim wrkStr  As String
'
'    Select Case argPos
'        Case 1
'            wrkStr = txtVname1.Text
'        Case 2
'            wrkStr = txtVname2.Text
'        Case 3
'            wrkStr = txtVname3.Text
'        Case 4
'            wrkStr = txtVname4.Text
'        Case 5
'            wrkStr = txtVname5.Text
'    End Select
'
'    If wrkStr <> "" Then
'        Select Case argPos
'            Case 1
'                txtVname2.Visible = True
'                txtVname2.Text = ""
'                txtVval2.Visible = True
'                txtVval2.Text = ""
'                chkV2.Visible = True
'                chkV2.Value = False
'            Case 2
'                txtVname3.Visible = True
'                txtVname3.Text = ""
'                txtVval3.Visible = True
'                txtVval3.Text = ""
'                chkV3.Visible = True
'                chkV3.Value = False
'            Case 3
'                txtVname4.Visible = True
'                txtVname4.Text = ""
'                txtVval4.Visible = True
'                txtVval4.Text = ""
'                chkV4.Visible = True
'                chkV4.Value = False
'            Case 4
'                txtVname5.Visible = True
'                txtVname5.Text = ""
'                txtVval5.Visible = True
'                txtVval5.Text = ""
'                chkV5.Visible = True
'                chkV5.Value = False
'            Case 5
'        End Select
'    Else
'        Select Case argPos
'            Case 1
'
'            Case 2
'                If txtVname3.Visible = False Then
'                    txtVname2.Visible = False
'                    txtVname2.Text = ""
'                    txtVval2.Visible = False
'                    txtVval2.Text = ""
'                    chkV2.Visible = False
'                    chkV2.Value = False
'                End If
'
'                If txtVname3.Text = "" Then
'                    txtVname3.Visible = False
'                    txtVname3.Text = ""
'                    txtVval3.Visible = False
'                    txtVval3.Text = ""
'                    chkV3.Visible = False
'                    chkV3.Value = False
'                End If
'            Case 3
'                If txtVname4.Visible = False Or txtVname4.Text = "" Then
'                    txtVname3.Visible = False
'                    txtVname3.Text = ""
'                    txtVval3.Visible = False
'                    txtVval3.Text = ""
'                    chkV3.Visible = False
'                    chkV3.Value = False
'                End If
'
'                If txtVname4.Text = "" Then
'                    txtVname4.Visible = False
'                    txtVname4.Text = ""
'                    txtVval4.Visible = False
'                    txtVval4.Text = ""
'                    chkV4.Visible = False
'                    chkV4.Value = False
'                End If
'            Case 4
'                If txtVname5.Visible = False Or txtVname5.Text = "" Then
'                    txtVname4.Visible = False
'                    txtVname4.Text = ""
'                    txtVval4.Visible = False
'                    txtVval4.Text = ""
'                    chkV4.Visible = False
'                    chkV4.Value = False
'                End If
'
'                If txtVname5.Text = "" Then
'                    txtVname5.Visible = False
'                    txtVname5.Text = ""
'                    txtVval5.Visible = False
'                    txtVval5.Text = ""
'                    chkV5.Visible = False
'                    chkV5.Value = False
'                End If
'            Case 5
'                txtVname5.Visible = False
'                txtVname5.Text = ""
'                txtVval5.Visible = False
'                txtVval5.Text = ""
'                chkV5.Visible = False
'                chkV5.Value = False
'        End Select
'    End If

End Sub

Public Sub HLineVisible(argPos As Integer)

'    Dim wrkStr  As String
'
'    Select Case argPos
'        Case 1
'            wrkStr = txtHname1.Text
'        Case 2
'            wrkStr = txtHname2.Text
'        Case 3
'            wrkStr = txtHname3.Text
'        Case 4
'            wrkStr = txtHname4.Text
'        Case 5
'            wrkStr = txtHname5.Text
'    End Select
'
'    If wrkStr <> "" Then
'        Select Case argPos
'            Case 1
'                txtHname2.Visible = True
'                txtHname2.Text = ""
'                txtHval2.Visible = True
'                txtHval2.Text = ""
'                chkH2.Visible = True
'                chkH2.Value = False
'            Case 2
'                txtHname3.Visible = True
'                txtHname3.Text = ""
'                txtHval3.Visible = True
'                txtHval3.Text = ""
'                chkH3.Visible = True
'                chkH3.Value = False
'            Case 3
'                txtHname4.Visible = True
'                txtHname4.Text = ""
'                txtHval4.Visible = True
'                txtHval4.Text = ""
'                chkH4.Visible = True
'                chkH4.Value = False
'            Case 4
'                txtHname5.Visible = True
'                txtHname5.Text = ""
'                txtHval5.Visible = True
'                txtHval5.Text = ""
'                chkH5.Visible = True
'                chkH5.Value = False
'            Case 5
'        End Select
'    Else
'        Select Case argPos
'            Case 1
'
'            Case 2
'                If txtHname3.Visible = False Then
'                    txtHname2.Visible = False
'                    txtHname2.Text = ""
'                    txtHval2.Visible = False
'                    txtHval2.Text = ""
'                    chkH2.Visible = False
'                    chkH2.Value = False
'                End If
'            Case 3
'                If txtHname4.Visible = False Then
'                    txtHname3.Visible = False
'                    txtHname3.Text = ""
'                    txtHval3.Visible = False
'                    txtHval3.Text = ""
'                    chkH3.Visible = False
'                    chkH3.Value = False
'                End If
'            Case 4
'                If txtHname5.Visible = False Then
'                    txtHname4.Visible = False
'                    txtHname4.Text = ""
'                    txtHval4.Visible = False
'                    txtHval4.Text = ""
'                    chkH4.Visible = False
'                    chkH4.Value = False
'                End If
'            Case 5
'                txtHname5.Visible = False
'                txtHname5.Text = ""
'                txtHval5.Visible = False
'                txtHval5.Text = ""
'                chkH5.Visible = False
'                chkH5.Value = False
'        End Select
'    End If

End Sub

Private Sub UserForm_Activate()
    Dim currChartObj        As ChartObject
    Dim currWorksheet       As Worksheet
    Dim srchRange           As Range
    
    'ĀļÞŨĖĐįcĄēÅå/ÅŽEÝ
    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set currChartObj = currWorksheet.ChartObjects(HistGraph02Name)
    
    With currChartObj.Chart
        '* cēĖÅå^ÅŽĖÝč
        txtVmax.text = .Axes(xlValue).MaximumScale
        txtVmin.text = .Axes(xlValue).MinimumScale
        txtVstep.text = .Axes(xlValue).MajorUnit
        
        '* ĄēĖÅå^ÅŽĖÝč
        txtHmax.text = .Axes(xlCategory).MaximumScale
        txtHmin.text = .Axes(xlCategory).MinimumScale
        txtHstep.text = .Axes(xlCategory).MajorUnit
    End With
    
    Set srchRange = currWorksheet.Cells.Find(What:="|cē|", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        txtVmax.text = srchRange.Offset(1, 1).Value
        txtVmin.text = srchRange.Offset(2, 1).Value
        txtVstep.text = srchRange.Offset(3, 1).Value
    End If
    
    Set srchRange = currWorksheet.Cells.Find(What:="|Ąē|", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        txtHmax.text = srchRange.Offset(1, 1).Value
        txtHmin.text = srchRange.Offset(2, 1).Value
        txtHstep.text = srchRange.Offset(3, 1).Value
    End If
    
    'cĄüæ
    Set srchRange = currWorksheet.Cells.Find(What:="|cēîü|", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        txtVname1.text = srchRange.Offset(1, 0).Value
        txtVname2.text = srchRange.Offset(2, 0).Value
        txtVname3.text = srchRange.Offset(3, 0).Value
        txtVname4.text = srchRange.Offset(4, 0).Value
        txtVname5.text = srchRange.Offset(5, 0).Value
        
        txtVval1.text = srchRange.Offset(1, 1).Value
        txtVval2.text = srchRange.Offset(2, 1).Value
        txtVval3.text = srchRange.Offset(3, 1).Value
        txtVval4.text = srchRange.Offset(4, 1).Value
        txtVval5.text = srchRange.Offset(5, 1).Value
    End If
    
    Set srchRange = currWorksheet.Cells.Find(What:="|Ąēîü|", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        txtHname1.text = srchRange.Offset(1, 0).Value
        txtHname2.text = srchRange.Offset(2, 0).Value
        txtHname3.text = srchRange.Offset(3, 0).Value
        txtHname4.text = srchRange.Offset(4, 0).Value
        txtHname5.text = srchRange.Offset(5, 0).Value
        
        txtHval1.text = srchRange.Offset(1, 1).Value
        txtHval2.text = srchRange.Offset(2, 1).Value
        txtHval3.text = srchRange.Offset(3, 1).Value
        txtHval4.text = srchRange.Offset(4, 1).Value
        txtHval5.text = srchRange.Offset(5, 1).Value
    End If
    
End Sub
