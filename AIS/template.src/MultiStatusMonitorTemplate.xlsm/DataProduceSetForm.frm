VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} DataProduceSetForm 
   Caption         =   "生データ表示画面設定"
   ClientHeight    =   11895
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7695
   OleObjectBlob   =   "DataProduceSetForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "DataProduceSetForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


'*************************************
'* 表示色条件テキストボックスをクリア
'*************************************
Private Sub BT_Clear_Click()
    TB_condition.text = ""
    TB_1.text = ""
    TB_2.text = ""
    TB_3.text = ""
    TB_4.text = ""
    TB_5.text = ""
    TB_6.text = ""
    TB_7.text = ""
End Sub

'*********************
'* 表示色条件取得処理
'*********************
Public Sub GetDispColorCond(dispColorCond() As String)
    dispColorCond(0) = TB_1.text
    dispColorCond(1) = TB_2.text
    dispColorCond(2) = TB_3.text
    dispColorCond(3) = TB_4.text
    dispColorCond(4) = TB_5.text
    dispColorCond(5) = TB_6.text
    dispColorCond(6) = TB_7.text
End Sub

'*******************************************
'* RGBカラー配列取得（Long値）
'*******************************************
Public Sub GetRGBColorArray(rgbColor() As Long)
    Dim paramColWorksheet      As Worksheet
    Dim srchRange              As Range
    Dim currRange              As Range
    Dim i                      As Integer
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    With paramColWorksheet
    
        Set srchRange = .Range("1:1")
        Set currRange = srchRange.Find(What:="生データ表示画面", LookAt:=xlWhole)
        
        If Not currRange Is Nothing Then
            For i = 0 To 6
                If currRange.Offset(i + 1, 1).Value <> "" Then
                    rgbColor(i) = currRange.Offset(i + 1, 1).Value
                Else
                    rgbColor(i) = currRange.Offset(i + 1, 0).Value
                End If
            Next
        End If
    End With

End Sub


'*******************************************
'* レベル色を設定
'*******************************************
Private Sub LB_1_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(7)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(0)
    retcolor = Label_Click(LB_1, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("H2").Value = retcolor
    End If
End Sub

'*******************************************
'* レベル色を設定
'*******************************************
Private Sub LB_2_Click()

    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(7)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(0)
    retcolor = Label_Click(LB_2, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("H3").Value = retcolor
    End If
End Sub

'*******************************************
'* レベル色を設定
'*******************************************
Private Sub LB_3_Click()

    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(7)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(0)
    retcolor = Label_Click(LB_3, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("H4").Value = retcolor
    End If
End Sub

'*******************************************
'* レベル色を設定
'*******************************************
Private Sub LB_4_Click()

    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(7)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(0)
    retcolor = Label_Click(LB_4, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("H5").Value = retcolor
    End If
End Sub

'*******************************************
'* レベル色を設定
'*******************************************
Private Sub LB_5_Click()

    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(7)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(0)
    retcolor = Label_Click(LB_5, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("H6").Value = retcolor
    End If
End Sub

'*******************************************
'* レベル色を設定
'*******************************************
Private Sub LB_6_Click()

    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(7)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(0)
    retcolor = Label_Click(LB_6, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("H7").Value = retcolor
    End If
End Sub

'*******************************************
'* レベル色を設定
'*******************************************
Private Sub LB_7_Click()

    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(7)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(0)
    retcolor = Label_Click(LB_7, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("H8").Value = retcolor
    End If
End Sub

'****************************
'* 表示色ラベルボタン設定処理
'****************************
Public Function Label_Click(LableNm As Object, lcolor As Long) As String
    
    'シートから取得したのデータをRGBに変換する
    b = Int(lcolor / 65536)
    g = Int((lcolor Mod 65536) / 256)
    r = (lcolor Mod 65536) Mod 256
    
    '色を選択したの場合
    If Application.Dialogs(xlDialogEditColor).Show(10, r, g, b) = True Then
        Label_Click = ActiveWorkbook.Colors(10)
        LableNm.BackColor = Label_Click
    Else
        Label_Click = ""
    End If
End Function

'*****************
'* フォーム初期化
'*****************
Private Sub UserForm_Initialize()
    Dim missionList()   As MISSIONINFO
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    '* ListViewの初期化
    With LV_Element
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add , "_Element", "エレメント名"
        .ColumnHeaders.Item("_Element").Width = 150
    End With

    With LV_DisplayElement
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add 1, "_Id", "ID", 15
        .ColumnHeaders.Add 2, "_Element", "エレメント", 85
        .ColumnHeaders.Add 3, "_DisplayType", "書式", 40
        .ColumnHeaders.Add 4, "_Ctm", "", 0

    End With
     
    '保存したデータを再設定する
    Call GetInfoFromExcel
        
    '* 取得したミッション情報のリストボックスへの出力
    LB_Mission.Clear
    For II = 0 To UBound(missionList)
        LB_Mission.AddItem missionList(II).Name
    Next
    LB_Mission.SetFocus
    LB_Mission.ListIndex = 0        ' リストボックスの最初の項目を選択状態にする
    
    '* 書式は数値がデフォルト
    OB_Numeric.Value = True
    
End Sub

'***************************************
'* ミッションリストボックス選択時の処理
'***************************************
Private Sub LB_Mission_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer

    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)

    mIndex = -1
    For II = 0 To UBound(missionList)
        If missionList(II).Name = LB_Mission.text Then
            mIndex = II
            Exit For
        End If
    Next

    LB_CTM.Clear
    If mIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM)
            LB_CTM.AddItem missionList(mIndex).CTM(II).Name
        Next
        LB_CTM.SetFocus
        LB_CTM.ListIndex = 0
    End If
End Sub

'***************************************
'* CTMリストボックス選択時の処理
'***************************************
Private Sub LB_CTM_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer
    Dim cIndex  As Integer
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    mIndex = -1
    cIndex = -1
    For II = 0 To UBound(missionList)
        For JJ = 0 To UBound(missionList(II).CTM)
            If missionList(II).CTM(JJ).Name = LB_CTM.text Then
                mIndex = II
                cIndex = JJ
                Exit For
            End If
        Next
        If cIndex > -1 Then Exit For
    Next

    LV_Element.ListItems.Clear
    If mIndex > -1 And cIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM(cIndex).Element)
            LV_Element.ListItems.Add.text = missionList(mIndex).CTM(cIndex).Element(II).Name
        Next
    End If
End Sub

'*****************
'* 設定した情報を取得
'*****************
Public Sub GetInfoFromExcel()
    Dim rgbColor(7)             As Long
    Dim dataProduceWorksheet    As Worksheet
    Dim iEndRow                 As Integer
            
    '設定した表示文字色を取得する
    Call GetRGBColorArray(rgbColor)
    LB_1.BackColor = rgbColor(0)
    LB_2.BackColor = rgbColor(1)
    LB_3.BackColor = rgbColor(2)
    LB_4.BackColor = rgbColor(3)
    LB_5.BackColor = rgbColor(4)
    LB_6.BackColor = rgbColor(5)
    LB_7.BackColor = rgbColor(6)
    
    Set dataProduceWorksheet = Workbooks(thisBookName).Worksheets(DataDispDefSheetName)
    
    'データ表示開始列
    TB_col.text = dataProduceWorksheet.Range("B1").text
    'データ表示開始行
    TB_row.text = dataProduceWorksheet.Range("B2").text
    'データ表示件数
    TB_cnt.text = dataProduceWorksheet.Range("B3").text
    'ウィンドウ枠を固定
    CB_FrameFixed.Value = dataProduceWorksheet.Range("D1").text
    
    CheckBox1.Value = dataProduceWorksheet.Range("D2").text
    
    '表示文字色条件：エレメント
    TB_condition.text = dataProduceWorksheet.Range("A6").text
    
    '表示条件
    TB_1.text = dataProduceWorksheet.Range("B6").text
    TB_2.text = dataProduceWorksheet.Range("C6").text
    TB_3.text = dataProduceWorksheet.Range("D6").text
    TB_4.text = dataProduceWorksheet.Range("E6").text
    TB_5.text = dataProduceWorksheet.Range("F6").text
    TB_6.text = dataProduceWorksheet.Range("G6").text
    TB_7.text = dataProduceWorksheet.Range("H6").text
    
    With dataProduceWorksheet
    
        '* A10セルをスタートセルとする
        Set startRange = .Range("A10")
        
        '* 設定した表示用エレメントを取得する
        For II = 0 To 100
            Set msnRange = startRange.Offset(II, 0)
            If msnRange.Offset(0, 0).Value <> "" Then
                Set itm = LV_DisplayElement.ListItems.Add()
                itm.text = II + 1
                itm.SubItems(1) = msnRange.Offset(0, 0).Value
                itm.SubItems(2) = msnRange.Offset(0, 1).Value
                itm.SubItems(3) = msnRange.Offset(0, 2).Value
            Else
                Exit For
            End If
        Next
    End With

    If TB_col.text = "" Then

        '* 「削除」ボタンを非表示
        BT_Delete.Visible = False

        '* 「更新」ボタンを非表示
        BT_Update.Visible = False
    Else

        '* 「新規」ボタンを非表示
        BT_Create.Visible = False
    End If
        
End Sub

'*****************
'* 表示エレメントの追加
'*****************
Private Sub BT_add_Click()

    Dim sValue              As String
    Dim isFormat            As Integer
    Dim isFormatText        As String
    Dim sCtmValue           As String
        
    '* テキストボックス更新処理
    If OB_Numeric.Value Then
        isFormat = 0
        isFormatText = "数値"
    ElseIf OB_String.Value Then
    
        isFormat = 1
        isFormatText = "文字列"
    ElseIf OB_Date.Value Then
    
        isFormat = 2
        isFormatText = "日付"
    ElseIf OB_Time.Value Then
    
        isFormat = 3
        isFormatText = "時刻"
    ElseIf OB_DateTime.Value Then
    
        isFormat = 4
        isFormatText = "日時"
    End If
    
    '* エレメントを追加する
    For i = LV_Element.ListItems.Count To 1 Step -1
        If LV_Element.ListItems(i).Selected Then
        
            sValue = LV_Element.ListItems(i).text
            bHasFlg = False
            For J = LV_DisplayElement.ListItems.Count To 1 Step -1
                If sValue = LV_DisplayElement.ListItems(J).SubItems(1) Then
                    bHasFlg = True
                    Exit For
                End If
            Next J
            If Not bHasFlg Then
                Set itm = LV_DisplayElement.ListItems.Add()
                itm.text = LV_DisplayElement.ListItems.Count()
                itm.SubItems(1) = sValue
                itm.SubItems(2) = isFormatText
                itm.SubItems(3) = LB_CTM.text
                Exit For
            End If
        End If
    Next i

End Sub

'*****************
'* 表示エレメントの行削除
'*****************
Private Sub BT_rowdelete_Click()

    For i = LV_DisplayElement.ListItems.Count To 1 Step -1
        If LV_DisplayElement.ListItems(i).Selected Then
            LV_DisplayElement.ListItems.Remove i
            Exit For
        End If
    Next i
    
    For i = LV_DisplayElement.ListItems.Count To 1 Step -1
        LV_DisplayElement.ListItems(i).text = i
    Next i
End Sub

'*******************
'* 閉じるボタン処理
'*******************
Private Sub BT_Close_Click()

    Unload Me

End Sub

'*********************************
'* 作成るボタン処理
'*********************************
Private Sub BT_Create_Click()
    
    If TB_cnt.text <> "" Then
        If Not IsNumeric(TB_cnt.text) Then
            MsgBox "データ表示件数は正の整数を入力ください。"
            TB_cnt.SetFocus
            Exit Sub
        ElseIf TB_cnt.text <= 0 Then
            MsgBox "データ表示件数は正の整数を入力ください。"
            TB_cnt.SetFocus
            Exit Sub
        End If
    End If
    
    If TB_col.text = "" Or TB_row.text = "" Then
        MsgBox "データ表示開始行列が入力されていません。"
        TB_col.SetFocus
        Exit Sub
    End If
    
    If IsNumeric(TB_col.text) Then
        If TB_col.text <= 0 Then
            MsgBox "データ表示開始列は正の整数または英字を入力ください。"
            TB_col.SetFocus
            Exit Sub
        End If
    Else
        Dim iA As Integer
        iA = Asc(TB_col.Value)
        If Not (iA >= 65 And iA <= 90) Or (iA >= 97 And iA <= 122) Then
            MsgBox "データ表示開始列は正の整数または英字を入力ください。"
            TB_col.SetFocus
            Exit Sub
        End If
    End If
    
    If Not IsNumeric(TB_row.text) Then
        MsgBox "データ表示開始行は正の整数を入力ください。"
        TB_row.SetFocus
        Exit Sub
    ElseIf TB_row.text <= 0 Then
        MsgBox "データ表示開始行は正の整数を入力ください。"
        TB_row.SetFocus
        Exit Sub
    End If
    
    If LV_DisplayElement.ListItems.Count = 0 Then
        MsgBox "表示エレメントが選択されていません。"
        Exit Sub
    End If

    Me.Hide

    '* 生データ表示画面に入力情報をエクセルに保存する
    Call SaveInfoToExcel
    
    '* ブックを作成する
    Call WriteValueProduceData
    
    Me.Show vbModeless

    Unload Me

End Sub

'***********
'* 更新処理
'***********
Private Sub BT_Update_Click()
    
    If TB_cnt.text <> "" Then
        If Not IsNumeric(TB_cnt.text) Then
            MsgBox "データ表示件数は正の整数を入力ください。"
            TB_cnt.SetFocus
            Exit Sub
        ElseIf TB_cnt.text <= 0 Then
            MsgBox "データ表示件数は正の整数を入力ください。"
            TB_cnt.SetFocus
            Exit Sub
        End If
    End If
    
    If TB_col.text = "" Then
        MsgBox "データ表示開始列が入力されていません。"
        TB_col.SetFocus
        Exit Sub
    End If
    
    If IsNumeric(TB_col.text) Then
        If TB_col.text <= 0 Then
            MsgBox "データ表示開始列は正の整数または英字を入力ください。"
            TB_col.SetFocus
            Exit Sub
        End If
    Else
        Dim iA As Integer
        iA = Asc(TB_col.Value)
        If Not (iA >= 65 And iA <= 90) Or (iA >= 97 And iA <= 122) Then
            MsgBox "データ表示開始列は正の整数または英字を入力ください。"
            TB_col.SetFocus
            Exit Sub
        End If
    End If
    
    If TB_row.text = "" Then
        MsgBox "データ表示開始行が入力されていません。"
        TB_row.SetFocus
        Exit Sub
    End If
    
    If Not IsNumeric(TB_row.text) Then
        MsgBox "データ表示開始行は正の整数を入力ください。"
        TB_row.SetFocus
        Exit Sub
    ElseIf TB_row.text <= 0 Then
        MsgBox "データ表示開始行は正の整数を入力ください。"
        TB_row.SetFocus
        Exit Sub
    End If

    If LV_DisplayElement.ListItems.Count = 0 Then
        MsgBox "表示エレメントが選択されていません。"
        Exit Sub
    End If

    ' ステータスを削除する
    Call DeleteProduceData("0", CB_FrameFixed.Value)
    
    '* 生データ表示画面に入力情報をエクセルに保存する
    Call SaveInfoToExcel
    
    '* ブックを作成する
    Call WriteValueProduceData
    
    Unload Me
    
End Sub

'***********
'* 削除処理
'***********
Private Sub BT_Delete_Click()
    
    ' ステータスを削除する
    Call DeleteProduceData("0", True)
    
    Unload Me
    
End Sub

'***********************
'* ドラッグ完了時の処理
'***********************
Private Sub LV_Element_OLECompleteDrag(Effect As Long)
    Debug.Print "OLECompleteDrag"
    Effect = vbDropEffectCopy
End Sub

'*****************************************************
'* リストボックスからテキストボックスへのドラッグ処理
'*****************************************************
Private Sub LV_Element_OLEStartDrag(data As MSComctlLib.DataObject, AllowedEffects As Long)

    Dim getTextStr      As Variant
    Debug.Print "OLEStartDrag"
    
    '* データオブジェクトの内容をクリア
    data.Clear
    
    '* ドラッグ開始時のリストボックスの選択項目を取得
    getTextStr = LV_Element.SelectedItem.text
    
    '* データオブジェクトにセット
    data.SetData getTextStr, vbCFtext
    
End Sub

'*******************
'* 生データ表示画面に入力情報をエクセルに保存する
'*******************
Private Sub SaveInfoToExcel()

    Dim dataProduceWorksheet    As Worksheet
    
    Set dataProduceWorksheet = Workbooks(thisBookName).Worksheets(DataDispDefSheetName)
    With dataProduceWorksheet
        'データ表示開始列
        .Range("B1").Value = TB_col.text
        'データ表示開始行
        .Range("B2").Value = TB_row.text
        'データ表示件数
        .Range("B3").Value = TB_cnt.text
        'ウィンドウ枠を固定
        .Range("D1").Value = CB_FrameFixed.Value
        
        .Range("D2").Value = CheckBox1.Value
        
        '表示文字色条件：エレメント
        .Range("A6").Value = TB_condition.text
        '表示条件
        .Range("B6").Value = TB_1.text
        .Range("C6").Value = TB_2.text
        .Range("D6").Value = TB_3.text
        .Range("E6").Value = TB_4.text
        .Range("F6").Value = TB_5.text
        .Range("G6").Value = TB_6.text
        .Range("H6").Value = TB_7.text

        '* A10セルをスタートセルとする
        Set startRange = .Range("A9")

        '* 表示エレメントを設定する
        For II = 1 To LV_DisplayElement.ListItems.Count
            Set msnRange = startRange.Offset(II, 0)
            msnRange.Offset(0, 0).Value = LV_DisplayElement.ListItems(II).SubItems(1)
            msnRange.Offset(0, 1).Value = LV_DisplayElement.ListItems(II).SubItems(2)
            msnRange.Offset(0, 2).Value = LV_DisplayElement.ListItems(II).SubItems(3)
        Next
        
        Set msnRange = startRange.Offset(II, 0)
        '* ISSUE_NO.677 sunyi 2018/05/07 start
        '* チェックボックスを表示か非表示の処理追加
        If dataProduceWorksheet.Range("D2").Value = "True" Then
        '* ISSUE_NO.677 sunyi 2018/05/07 end
            msnRange.Offset(0, 0).Value = ElementCheckVale
        '* ISSUE_NO.677 sunyi 2018/05/07 start
        '* チェックボックスを表示か非表示の処理追加
        End If
        '* ISSUE_NO.677 sunyi 2018/05/07 end
        
    End With

End Sub

