VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} GripActionPaneBackForm 
   Caption         =   "背景色設定"
   ClientHeight    =   7305
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   7665
   OleObjectBlob   =   "GripActionPaneBackForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "GripActionPaneBackForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private GripEntityMap As New clsGripEntityMap



'*************************************
'* 表示色条件テキストボックスをクリア
'*************************************
Private Sub BT_Clear_Click()
    TB_White.text = ""
    TB_Red.text = ""
    TB_Yellow.text = ""
    TB_Green.text = ""
End Sub

'*********************
'* 表示色条件設定処理
'*********************
Private Sub BT_Set_Click()
    Dim shapeName           As String
    Dim srchRange           As Range
    Dim colRange            As Range
    Dim currRange           As Range
    Dim paramWorksheet    As Worksheet
    
    '2017/06/26 No.278 Add.---------Start
    If TB_ItemOperation.text = "" Then
        MsgBox "設定項目が設定されていません。"
        Exit Sub
    End If
    '2017/06/26 No.278 Add.---------End
    
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    With paramWorksheet

        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:="表示色対象", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.Offset(1, 1).NumberFormatLocal = "@"
            currRange.Offset(1, 1).Value = TB_White.text
            currRange.Offset(2, 1).NumberFormatLocal = "@"
            currRange.Offset(2, 1).Value = TB_Red.text
            currRange.Offset(3, 1).NumberFormatLocal = "@"
            currRange.Offset(3, 1).Value = TB_Yellow.text
            currRange.Offset(4, 1).NumberFormatLocal = "@"
            currRange.Offset(4, 1).Value = TB_Green.text
        End If
        
        currRange.Offset(0, 1).Value = TB_ItemOperation.text
        
        'エレメントIDをセット　'2017/06/26 No.278 Add.
        currRange.Offset(0, 2).Value = LB_Route.text & "・" & LB_CTM.text & "・" & LV_Element.SelectedItem.text

    End With
    
    Unload Me

End Sub

'*******************
'* 閉じるボタン処理
'*******************
Private Sub BT_Close_Click()

    Unload Me

End Sub

Private Sub LB_Route_Change()
    Dim II      As Integer

    '* ミッションリストの取得
    Dim ctms As Object
    Set ctms = GripEntityMap.GetCtmList(LB_Route.ListIndex)

    LB_CTM.Clear
    For II = 0 To ctms.Count - 1
        Call LB_CTM.AddItem(ctms.Item(II).Name)
    Next
    LB_CTM.SetFocus
    LB_CTM.ListIndex = 0
    
    '* ISSUE_NO.666 sunyi 2018/05/10 start
    '* 吹き出し追加
    LB_Route.ControlTipText = LB_Route.text
    '* ISSUE_NO.666 sunyi 2018/05/10 end
End Sub

'***********************************************************************************
'***********************************************************************************
'***********************
'* ドラッグ完了時の処理
'***********************
Private Sub LV_Element_OLECompleteDrag(Effect As Long)
    Dim RowNum As Long  '2017/06/26 No.278 Add.
    Debug.Print "OLECompleteDrag"
    Effect = vbDropEffectCopy

    'エレメントIDをセット '2017/06/26 No.278 Add.
    If LV_Element.ListItems.Count > 0 Then
        RowNum = LV_Element.SelectedItem.Index
    End If
End Sub

'*****************************************************
'* リストボックスからテキストボックスへのドラッグ処理
'*****************************************************
Private Sub LV_Element_OLEStartDrag(data As MSComctlLib.DataObject, AllowedEffects As Long)
    Dim workStr         As Variant
    Dim getTextStr      As Variant
    Debug.Print "OLEStartDrag"
    
    '* データオブジェクトの内容をクリア
    data.Clear
    
    '* ドラッグ開始時のリストボックスの選択項目を取得
    getTextStr = LV_Element.SelectedItem.text
    
    '* 貼り付ける文字列の生成
    workStr = """" & LB_Route.text & "・" & LB_CTM.text & "・" & getTextStr & """"
    
    '''''workStr = """" & LB_CTM.text & "_" & getTextStr & """"
    
    '* データオブジェクトにセット
    data.SetData workStr, vbCFtext
    
End Sub
'***********************************************************************************
'***********************************************************************************

Private Sub TextBox1_MouseDown(ByVal Button As Integer, ByVal Shift As Integer, ByVal X As Single, ByVal Y As Single)
    TB_ItemOperation.text = ""
End Sub

'*****************
'* フォーム初期化
'*****************
Private Sub UserForm_Initialize()
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    Dim rgbColorBack(4) As Long

    '* 初期色を取得
    Call GetRGBColorBackArray(rgbColorBack)
    Label11.BackColor = rgbColorBack(0)
    Label9.BackColor = rgbColorBack(1)
    Label12.BackColor = rgbColorBack(2)
    Label16.BackColor = rgbColorBack(3)
    
    '* ListViewの初期化
    With LV_Element
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add , "_Element", "エレメント名"
        .ColumnHeaders.Item("_Element").Width = 150
    End With
    
    Call ShowMissionName
    
    '* 取得したミッション情報のリストボックスへの出力
    LB_Route.Clear
    Dim routes As Object
    Set routes = GripEntityMap.GetRouteList
    For II = 0 To routes.Count - 1
        LB_Route.AddItem (routes.Item(II).Name)
    Next
    LB_Route.SetFocus
    LB_Route.ListIndex = 0        ' リストボックスの最初の項目を選択状態にする
    
End Sub


'***************************************
'* CTMリストボックス選択時の処理
'***************************************
Private Sub LB_CTM_Change()
    Dim II      As Integer

    If LB_CTM.ListIndex < 0 Then
        Exit Sub
    End If

    '* ミッションリストの取得
    Dim elements As Object
    Set elements = GripEntityMap.GetElementList(LB_Route.ListIndex, LB_CTM.ListIndex)

    LV_Element.ListItems.Clear
    For II = 0 To elements.Count - 1
        LV_Element.ListItems.Add.text = elements.Item(II).Name
    Next
    
    '* ISSUE_NO.666 sunyi 2018/05/10 start
    '* 吹き出し追加
    LB_CTM.ControlTipText = LB_CTM.text
    '* ISSUE_NO.666 sunyi 2018/05/10 end
End Sub

'2017/06/26 No.278 Add.---------Start
'***************************************
'* エレメントボックス選択時の処理
'***************************************
Private Sub LV_Element_Click()
Dim RowNum As Long

If LV_Element.ListItems.Count > 0 Then
    RowNum = LV_Element.SelectedItem.Index
End If
End Sub
'2017/06/26 No.278 Add.---------End

'**********************
'* ボタン11押下時の処理
'**********************
Private Sub Label11_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColorBack(4)        As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorBackArray(rgbColorBack)
    lcolor = rgbColorBack(0)
    retcolor = Label_Click(Label11, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("E2").Value = retcolor
    End If
    
End Sub

Private Sub Label9_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColorBack(4)        As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorBackArray(rgbColorBack)
    lcolor = rgbColorBack(1)
    retcolor = Label_Click(Label9, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("E3").Value = retcolor
    End If
End Sub

Private Sub Label12_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColorBack(4)        As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorBackArray(rgbColorBack)
    lcolor = rgbColorBack(2)
    retcolor = Label_Click(Label12, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("E4").Value = retcolor
    End If
End Sub

Private Sub Label16_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColorBack(4)        As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorBackArray(rgbColorBack)
    lcolor = rgbColorBack(3)
    retcolor = Label_Click(Label16, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("E5").Value = retcolor
    End If
End Sub

'****************************
'* 表示色ラベルボタン設定処理
'****************************
Public Function Label_Click(LableNm As Object, lcolor As Long) As String
    
    'シートから取得したのデータをRGBに変換する
    b = Int(lcolor / 65536)
    g = Int((lcolor Mod 65536) / 256)
    r = (lcolor Mod 65536) Mod 256
    
    '色を選択したの場合
    If Application.Dialogs(xlDialogEditColor).Show(10, r, g, b) = True Then
        Label_Click = ActiveWorkbook.Colors(10)
        LableNm.BackColor = Label_Click
    Else
        Label_Click = ""
    End If
End Function


Public Sub ShowMissionName()
    '* ミッション名の取得
    Dim missionName As String
    missionName = GetMissionName(MissionSheetName)
    If missionName <> "" Then
        Me.Caption = Me.Caption + "　■ミッション_" + missionName
    End If
End Sub

Private Function GetMissionName(currSheetName As String) As String
    Dim startRange      As Range
    Dim msnRange        As Range
    Dim currWorksheet   As Worksheet
    Dim II              As Integer      ' For文用カウンタ
    
    Set currWorksheet = ThisWorkbook.Worksheets(currSheetName)
    
    With currWorksheet
        '* ミッションシートのA1セルをスタートセルとする
        Set startRange = .Range("B1")
        
        '* ミッションリストを取得する
        For II = 0 To 100
            Set msnRange = startRange.Offset(II, 0)
            
            If msnRange.Offset(0, 0).Value <> "" Then
                GetMissionName = msnRange.Offset(0, 0).Value
                Exit For
            End If
        Next
    End With
    
End Function

