VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsGripEntityMap"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Const ROUTE_SHEET_NAME As String = "Routes"

Private routes As Object
Private routesDataList As Object

Private Sub Class_Initialize()
    Call InitializeEntityMap
End Sub

Private Sub InitializeEntityMap()
    Dim routesSheet As Worksheet
    Dim valueRange As Range
    Dim routeEntity As clsGripEntity
    Dim ctmEntity As clsGripEntity
    Dim elementEntity As clsGripEntity
    Dim i As Long
    Dim intPos As Long
    Dim worksheetIndex As Long
    
    ' processing on server dn 2018/09/07 start
    'Set routes = CreateObject("System.Collections.ArrayList")
    Set routesDataList = CreateObject("Scripting.Dictionary")
  
    For worksheetIndex = 1 To ThisWorkbook.Worksheets.Count
        intPos = InStr(ThisWorkbook.Worksheets(worksheetIndex).Name, ROUTE_SHEET_NAME)
        If intPos <> 0 Then
            Set routesSheet = ThisWorkbook.Worksheets(worksheetIndex)
            Set valueRange = routesSheet.UsedRange
        
            For i = 1 To valueRange.Rows.Count
                ' route
                Dim routeName As String
                routeName = routesSheet.Cells(i, 1).Value
                If routeName <> "" Then
                    Set routeEntity = New clsGripEntity
                    routeEntity.Name = routeName
                    Set routeEntity.SubEntities = CreateObject("Scripting.Dictionary")
                    routesDataList.Add routeName, routeEntity
                    
                    'Set routeEntity.SubEntities = CreateObject("System.Collections.ArrayList")
            
                    'Call routes.Add(routeEntity)
                End If
            
                ' ctm
                Dim ctmName As String
                ctmName = routesSheet.Cells(i, 2).Value
                If ctmName <> "" Then
                    Set ctmEntity = New clsGripEntity
                    ctmEntity.Name = ctmName
                    Set ctmEntity.SubEntities = CreateObject("Scripting.Dictionary")
                    
                    routeEntity.SubEntities.Add ctmName, ctmEntity
                    
                    'Set ctmEntity.SubEntities = CreateObject("System.Collections.ArrayList")
            
                    'Call routeEntity.SubEntities.Add(ctmEntity)
                End If
            
                ' element
                Dim elementName As String
                elementName = routesSheet.Cells(i, 3).Value
                If elementName <> "" Then
                    Set elementEntity = New clsGripEntity
                    elementEntity.Name = elementName
                    Set elementEntity.SubEntities = CreateObject("Scripting.Dictionary")
                    
                    ctmEntity.SubEntities.Add elementName, elementEntity
                    
                    'Set elementEntity.SubEntities = CreateObject("System.Collections.ArrayList")
            
                    'Call ctmEntity.SubEntities.Add(elementEntity)
                End If
            Next
        End If
    Next
    ' processing on server dn 2018/09/07 End
   
End Sub

Public Function GetRouteList() As Object
    ' processing on server dn 2018/09/07 Start
    'Set GetRouteList = routes
    Set GetRouteList = routesDataList
    ' processing on server dn 2018/09/07 End
End Function

Public Function GetCtmList(routeIndex As Long) As Object
    Set GetCtmList = routes.Item(routeIndex).SubEntities
End Function

Public Function GetElementList(routeIndex As Long, ctmIndex As Long) As Object
    Set GetElementList = GetCtmList(routeIndex).Item(ctmIndex).SubEntities
End Function


