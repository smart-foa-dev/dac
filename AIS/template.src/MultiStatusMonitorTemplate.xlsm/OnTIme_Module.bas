Attribute VB_Name = "OnTIme_Module"
 Option Explicit
 Public Declare PtrSafe Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, _
        ByVal X As Long, ByVal Y As Long, _
         ByVal cx As Long, ByVal cy As Long, _
        ByVal wFlags As Long) As Long
 Public Const walways = -1 '常に手前にセット
 Public Const wreset = -2  '解除
 Public Const wdisp = &H40 '表示する
 Public Const w_SIZE = &H1 'サイズを設定しない
 Public Const w_MOVE = &H2 '位置を設定しない
 
 '==========================================================================
 Public Sub ExcelDispSetWin32()
    Dim hwnd As Long
    hwnd = Application.hwnd
    Call SetWindowPos(hwnd, walways, 0, 0, 0, 0, wdisp Or w_SIZE Or w_MOVE)
 End Sub
 
 '===========================================================================
 Public Sub ExcelDispFreeWin32()
    Dim hwnd As Long
    hwnd = Application.hwnd
    Call SetWindowPos(hwnd, wreset, 0, 0, 0, 0, wdisp Or w_SIZE Or w_MOVE)
 End Sub
 
'*******************************
'* ファイル名を変数へセットする
'*******************************
Public Sub ExcelUpperDisp()
'    Dim srchRange       As Range
'    Dim workRange       As Range
'    Dim SheetName       As String
'
'    Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Range("A:A")
'    Set workRange = srchRange.Find(What:="マクロファイル名", LookAt:=xlWhole)
'    If Not workRange Is Nothing Then
'        thisBookName = workRange.Offset(0, 1).Value
'    End If
'
'    '**********************
'    AppActivate thisBookName & " - Excel"
'
'    SheetName = ThisWorkbook.ActiveSheet.Name
'
'    ThisWorkbook.Sheets(SheetName).Activate
    '**********************
End Sub


Public Sub OnTimeStart()
    Dim dw          As Date
    Dim wrkRange    As Range
    
    dw = DateAdd("S", 3, Now)
    
    Set wrkRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="次回表示日時", LookAt:=xlWhole)
    
    If Not wrkRange Is Nothing Then
        If wrkRange.Offset(0, 1).Value = "" Then
            Application.OnTime dw, "ExcelDisp"
            wrkRange.Offset(0, 1).Value = Format(dw, "yyyy/mm/dd hh:mm:ss")
        End If
    End If
     
    
 End Sub
 
Public Sub OnTimeContinue()
    Dim dw          As Date
    Dim wrkRange    As Range
    
    dw = DateAdd("S", 3, Now)
    
    Set wrkRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="次回表示日時", LookAt:=xlWhole)
    
    If Not wrkRange Is Nothing Then
        wrkRange.Offset(0, 1).Value = Format(dw, "yyyy/mm/dd hh:mm:ss")
    End If
     
    Application.OnTime dw, "ExcelDisp"
    
 End Sub
 
'更新を止める
Public Sub OnTimeCancel()
    Dim dw          As Date
    Dim wrkRange    As Range
    
    On Error Resume Next
    Set wrkRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="次回表示日時", LookAt:=xlWhole)
    If Not wrkRange Is Nothing Then
        If wrkRange.Offset(0, 1).Value <> "" Then
            dw = CDate(wrkRange.Offset(0, 1).Value)
            Application.OnTime dw, Procedure:="ExcelDisp", Schedule:=False
            wrkRange.Offset(0, 1).Value = ""
        End If
    End If
     
 
 End Sub

'Excel再表示
Private Sub ExcelDisp()
    If Application.Visible = True Then
        'EXCEL前面表示用
        Call ExcelUpperDisp
        VBA.AppActivate Excel.Application.Caption
    
        Call ExcelDispSetWin32
        Call ExcelDispFreeWin32
        
        Call OnTimeContinue
    End If
End Sub

