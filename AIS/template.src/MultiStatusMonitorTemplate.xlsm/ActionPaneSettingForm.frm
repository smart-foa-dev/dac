VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ActionPaneSettingForm 
   Caption         =   "条件設定"
   ClientHeight    =   5235
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   6945
   OleObjectBlob   =   "ActionPaneSettingForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "ActionPaneSettingForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub ComboBox1_Change()
    If ComboBox1.SelText = "＝" Then
        ComboBox2.Enabled = False
        Setting_2.Enabled = False
        ComboBox2.ListIndex = -1
        Setting_2.text = ""
    Else
        ComboBox2.Enabled = True
        Setting_2.Enabled = True
    End If
End Sub

Private Sub CancelButton_Click()
    Unload ActionPaneSettingForm
End Sub

Private Sub ClearButton_Click()
    ComboBox1.ListIndex = -1
    ComboBox2.ListIndex = -1
    Setting_1.text = ""
    Setting_2.text = ""
    Setting_3.text = ""
    Setting_4.text = ""
End Sub

'Private Sub Setting_1_Change()
'    If ComboBox1.SelText <> "＝" Then
'        If Not IsNumeric(Setting_1.text) And Setting_1.text <> "" Then
'            MsgBox ("数字を入力してください。")
'        End If
'    End If
'End Sub
'
'Private Sub Setting_2_Change()
'    If ComboBox1.text = "＝" Then
'        Exit Sub
'    ElseIf Not IsNumeric(Setting_2.text) And Setting_2.text <> "" Then
'        MsgBox ("数字を入力してください。")
'    End If
'End Sub

Private Sub Setting_3_Change()
    If Setting_3 = "" Then
        Setting_4.Enabled = False
    Else
        Setting_4.Enabled = True
    End If
End Sub

Private Sub SetButton_Click()

    Dim str             As String
    Dim str2            As String
    Dim keisanStr       As String
    
    Dim TextValue1      As String
    Dim TextValue2      As String
    Dim TooltipText     As String
    
    Dim paramWorkhseet  As Worksheet
    Dim srchRange       As Range
    Dim rowRange        As Range
    Dim sSwitch         As String
    
    Set paramWorkhseet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With paramWorkhseet
        '* 編集モードを取得する
        Set srchRange = .Range("A:A")
        Set rowRange = srchRange.Find(What:="SwitchLink", LookAt:=xlWhole)
        If Not rowRange Is Nothing Then
            sSwitch = rowRange.Offset(0, 1).Value
        End If
    End With
    
    With ActionPaneForm
        If sSwitch = "OFF" Then
            ''' 20190625 yakiyama modified start.
            If Not Label4.BackColor = &HFFFFFF And ComboBox1.text = "" And ComboBox2.text = "" Then
            ''' 20190625 yakiyama modified end.
                MsgBox ("条件を入力してください。")
                Exit Sub
            ElseIf ComboBox1.text <> "" And Setting_1 = "" Then
                MsgBox ("条件を入力してください。")
                Exit Sub
            ElseIf ComboBox1.text = "" And Setting_1 <> "" Then
                MsgBox ("条件を入力してください。")
                Exit Sub
            ElseIf ComboBox2.text <> "" And Setting_2 = "" Then
                MsgBox ("条件を入力してください。")
                Exit Sub
            ElseIf ComboBox2.text = "" And Setting_2 <> "" Then
                MsgBox ("条件を入力してください。")
                Exit Sub
            End If
        End If
        
        If Cb_IsNumericFormat.Value = True Then
            If Not IsNumericEx(Setting_1.text) And Setting_1.text <> "" Then
                MsgBox ("条件は数字を入力してください。")
                Exit Sub
            End If
        End If
        
        If ComboBox1.SelText <> "＝" Then
            If Not IsNumericEx(Setting_1.text) And Setting_1.text <> "" Then
                MsgBox ("条件は数字を入力してください。")
                Exit Sub
            End If
        End If
        
        If Not IsNumericEx(Setting_2.text) And Setting_2.text <> "" Then
            MsgBox ("条件は数字を入力してください。")
            Exit Sub
        End If
        
        If Len(Setting_3.text) > 15 Then
            MsgBox ("一行目の表示文字は15文字までを入力してください。")
            Exit Sub
        End If
        If Len(Setting_4.text) > 15 Then
            MsgBox ("二行目の表示文字は15文字までを入力してください。")
            Exit Sub
        End If
        
'        If (ComboBox1.text <> "" Or ComboBox2.text <> "") And Setting_3 = "" Then
'            Setting_4.text = ""
'            MsgBox ("表示文字を入力してください。")
'            Exit Sub
'        End If
        
        '* ActionPaneに値を書き込み
        If ComboBox1.text <> "" And ComboBox2.text <> "" Then
            keisanStr = Setting_1.Value & ComboBox1.text & Label_AND.Caption & ComboBox2.text & Setting_2.Value
        ElseIf ComboBox1.text <> "" And Not ComboBox2.text <> "" Then
            keisanStr = Setting_1.Value & ComboBox1.text & Label_AND.Caption
        ElseIf Not ComboBox1.text <> "" And ComboBox2.text <> "" Then
            keisanStr = Label_AND.Caption & ComboBox2.text & Setting_2.Value
        Else
            keisanStr = ""
        End If
        
        If Setting_3.Value <> "" And Setting_4.Value <> "" Then
            TextValue1 = Setting_3.Value
            TextValue2 = Setting_4.Value
            TooltipText = "[1] " & Setting_3.Value & " [2] " & Setting_4.Value
        ElseIf Setting_3.Value <> "" Then
            TextValue1 = Setting_3.Value
            TextValue2 = ""
            TooltipText = "[1] " & Setting_3.Value & " [2]"
        ElseIf Setting_4.Value <> "" Then
            TextValue1 = ""
            TextValue2 = Setting_4.Value
            TooltipText = "[1] " & " [2] " & Setting_4.Value
        Else
            TextValue1 = ""
            TextValue2 = ""
            TooltipText = ""
        End If
        
        
        Select Case True
        
        Case Label6.Caption = "TB_White"
            
            .TB_White.Caption = keisanStr
            .TB_White_1.Caption = TextValue1
            .TB_White_2.Caption = TextValue2
            .TB_White_1.ControlTipText = TooltipText
        
        Case Label6.Caption = "TB_Red"
        
            .TB_Red.Caption = keisanStr
            .TB_Red_1.Caption = TextValue1
            .TB_Red_2.Caption = TextValue2
            .TB_Red_1.ControlTipText = TooltipText
            
        Case Label6.Caption = "TB_Lime"
        
            .TB_Lime.Caption = keisanStr
            .TB_Lime_1.Caption = TextValue1
            .TB_Lime_2.Caption = TextValue2
            .TB_Lime_1.ControlTipText = TooltipText
            
        Case Label6.Caption = "TB_Blue"
        
            .TB_Blue.Caption = keisanStr
            .TB_Blue_1.Caption = TextValue1
            .TB_Blue_2.Caption = TextValue2
            .TB_Blue_1.ControlTipText = TooltipText
        
        Case Label6.Caption = "TB_Yellow"
        
            .TB_Yellow.Caption = keisanStr
            .TB_Yellow_1.Caption = TextValue1
            .TB_Yellow_2.Caption = TextValue2
            .TB_Yellow_1.ControlTipText = TooltipText
            
        Case Label6.Caption = "TB_Magenta"
        
            .TB_Magenta.Caption = keisanStr
            .TB_Magenta_1.Caption = TextValue1
            .TB_Magenta_2.Caption = TextValue2
            .TB_Magenta_1.ControlTipText = TooltipText
            
        Case Label6.Caption = "TB_Aqua"
        
            .TB_Aqua.Caption = keisanStr
            .TB_Aqua_1.Caption = TextValue1
            .TB_Aqua_2.Caption = TextValue2
            .TB_Aqua_1.ControlTipText = TooltipText
        
        Case Label6.Caption = "TB_Maroon"
        
            .TB_Maroon.Caption = keisanStr
            .TB_Maroon_1.Caption = TextValue1
            .TB_Maroon_2.Caption = TextValue2
            .TB_Maroon_1.ControlTipText = TooltipText
        
        End Select
        
    End With
    
    Unload ActionPaneSettingForm
    
End Sub



