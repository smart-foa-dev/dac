Attribute VB_Name = "SubMain_Module"
Option Explicit

'* foastudio AisAddin sunyi 2018/11/02
Private excelUtilObj As FoaCoreCom.excelUtil
'Processing on Server dn 2018/09/07 start
Private GripEntityMap As New clsGripEntityMap
'Processing on Server dn 2018/09/07 end

'Wang 外部取込みアプリケーションを起動 2018/04/27 Start
'外部取込みアプリケーションを起動し、異常終了の場合は後続処理を中断する
Private Const COMMAND_CTMRefineUI = "CTMRefineUI.exe"
Private Const COMMAND_TriggerExData = "TriggerExData.bat"
'Wang 外部取込みアプリケーションを起動 2018/04/27 End

Private Function getLastRow(WS As Worksheet, Optional CheckCol As Long = 1) As Long
  getLastRow = WS.Cells(1, CheckCol).End(xlDown).Row
End Function

'************************************
'* リンクの情報をシートに登録する
'************************************
Public Sub WriteValueLinkSheet(formStatus As String, linkBln As Boolean, commandListPass() As String, _
                                currShapeName As String, commandListType() As Integer, commandListCount As Integer)
    
    Dim linkParamWorksheet  As Worksheet
    Dim workRange           As Range
    Dim srchRange           As Range
    Dim dispRange           As Range
    Dim iRowCnt             As Long
    Dim II                  As Integer
    Dim i                   As Long

    Call SetFilenameToVariable
    Set linkParamWorksheet = Workbooks(thisBookName).Worksheets(LinkSheetName)
    iRowCnt = linkParamWorksheet.UsedRange.Rows.Count
    With linkParamWorksheet
        Set srchRange = .Range("1:1")
        Set workRange = srchRange.Find(What:="パス", LookAt:=xlWhole)
        If Not workRange Is Nothing Then
            '新規作成の場合
            If formStatus = "create" Then
            
                For i = 0 To commandListCount - 1 Step 1
                
                    'AISアウトプットのファイルの場合
                    If commandListType(i) = 1 Then
                    
                        '* 全図形に対して
                        For II = 1 To 1000
                            '定義内容の有無ﾁｪｯｸ
                            If workRange.Offset(II, 1).Value = "" Then
                                '* 記載開始範囲を設定
                                workRange.Offset(II, 0).Value = commandListPass(i)
                                workRange.Offset(II, 1).Value = currShapeName
                                Exit For
                            End If
                        Next
                    End If
                Next i
                
                '更新の場合
            ElseIf formStatus = "update" Then
            
                '既存図形のリンク情報を削除する
                For II = 1 To iRowCnt
                    If .Range("B" & II).Value = "" Then Exit For
                    If .Range("B" & II).Value = currShapeName Then
                        .Rows(II).Delete
                        II = II - 1
                    End If
                Next II

                'リンクがある場合
                If linkBln Then

                    Set srchRange = .Range("B:B")
                    For i = 0 To commandListCount - 1 Step 1
                    
                        'AISアウトプットのファイルの場合
                        If commandListType(i) = 1 Then
                        
                            '* 全図形に対して
                            For II = 1 To 1000
                                '定義内容の有無ﾁｪｯｸ
                                If workRange.Offset(II, 1).Value = "" Then
                                    '* 記載開始範囲を設定
                                    workRange.Offset(II, 0).Value = commandListPass(i)
                                    workRange.Offset(II, 1).Value = currShapeName
                                    Exit For
                                End If
                            Next
                        End If
                    Next i
                End If
            End If
        End If
    End With
End Sub

'*************************************************
'* 画面の情報を元に入力テキストボックスを生成する
'*************************************************
'AISMM No.82 sunyi 20181211 start
'Public Sub WriteValueTextBox(calcString As String, isFormat As Integer, dispColorCond() As String, linkBln As Boolean, elementBln As Boolean, _
'                    elementString As String, elementCondList() As String, elementList() As String, elementListCount As Integer, _
'                    commandListType() As Integer, commandListName() As String, commandListPass() As String, commandListCount As Integer, dispTextCond() As String)
Public Function WriteValueTextBox(calcString As String, isFormat As Integer, dispColorCond() As String, linkBln As Boolean, elementBln As Boolean, _
                    elementString As String, elementCondList() As String, elementList() As String, elementListCount As Integer, _
                    commandListType() As Integer, commandListName() As String, commandListPass() As String, commandListCount As Integer, dispTextCond() As String)
'AISMM No.82 sunyi 20181211 end
    Dim MyKeyState          As Long
    Dim nowShapeNum         As Integer
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim linkWorksheet       As Worksheet
    Dim condParamsheet      As Worksheet
    Dim linkParamsheet      As Worksheet
    Dim srchRange           As Range
    Dim srchNmRange         As Range
    Dim i                   As Integer
    Dim currShape           As Shape
    Dim currShapeName       As String
    Dim currShapeNum        As Integer
    Dim II                  As Integer
    Dim startRange          As Range
    Dim workRange           As Range
    Dim dispRange           As Range
    Dim itemOpeString       As String
    Dim idArray(100)        As Boolean
    Dim workStr             As String
    Dim splitStr            As Variant
    Dim isCalcString        As Boolean
    Dim iLineWeight         As Integer
    Dim iLineStyle          As Integer
    Dim iEndRow             As Integer
    On Error GoTo ErrorHandler1

    Call SetFilenameToVariable
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    
    'AISMM No.82 sunyi 20181211 start
    If ActiveSheet.Name <> StatusSheetName Then
        workStr = "「ステータス」シートを選択した上で、テキストボックスを作成してください。" & vbCrLf & vbCrLf & _
                "※「はい」の場合は、「ステータス」シートに切り替えます。" & vbCrLf & vbCrLf & _
                "※「いいえ」の場合は、テキストボックスを作成しません。"
                
        If MsgBox(workStr, vbYesNo + vbInformation) = vbYes Then
            statusWorksheet.Activate
            WriteValueTextBox = 0
        Else
            WriteValueTextBox = 1
            Exit Function
        End If
    End If
    'AISMM No.82 sunyi 20181211 end
    
    'AISMM No.95,96 yakiyama 20190607 start
    Application.CommandBars.FindControl(ID:=1111).Execute
    'workStr = "値を表示するテキストボックスを作成" & vbCrLf & vbCrLf & _
    '            "※セル枠線に合わせる場合はALTキーを押しながらドラッグ"
    '
    'If MsgBox(workStr, vbYesNo + vbInformation) = vbYes Then
    '    Application.CommandBars.FindControl(ID:=1111).Execute
    'Else
    '    'AISMM No.82 sunyi 20181211 start
    '    WriteValueTextBox = 0
    '    'AISMM No.82 sunyi 20181211 end
    '    Exit Function
    'End If
    'AISMM No.95.96 yakiyama 20190607 end
    
    If linkBln Then
        iLineWeight = 5
        iLineStyle = xlDoubleAccounting
    Else
        iLineWeight = 1.5
        iLineStyle = xlContinuous
    End If
    With statusWorksheet
    
        '* 現在の図形数を取得する
        nowShapeNum = .Shapes.Count
        
        '* 図形が増えるまで待機
        Do
            DoEvents
        Loop Until nowShapeNum < .Shapes.Count
        
        '* 現在の該当図形数を取得
        For II = 0 To 99
            idArray(II) = True
        Next
        
        '**入力規則カウント抜く
        For II = .Shapes.Count To 1 Step -1
            If .Shapes(II).AutoShapeType = msoShapeRectangle Then
                nowShapeNum = II
                Exit For
            End If
        Next II
        'nowShapeNum = .Shapes.Count
        currShapeNum = 0
        For II = 1 To nowShapeNum
            If Left(.Shapes(II).Name, 6) = "value_" Then
                idArray(CInt(Right(.Shapes(II).Name, 2))) = False
                currShapeNum = currShapeNum + 1
            End If
        Next
        For II = 1 To 99
            If idArray(II) Then Exit For
        Next
        If II > 99 Then
            currShapeNum = 99
        Else
            currShapeNum = II
        End If
        
        '* 図形の属性を設定
        Set currShape = .Shapes(nowShapeNum)
        currShapeName = "value_shape_" & Format(currShapeNum, "00")
        
        With currShape
            .Name = currShapeName
            .OnAction = "ConditionOpen"
            With .TextFrame2
                '* 文字列配置中央
                .VerticalAnchor = msoAnchorMiddle
                With .TextRange
                    .ParagraphFormat.Alignment = msoAlignCenter
                    .Font.Bold = msoTrue
                    .Font.Size = 16
                    .Font.Fill.ForeColor.RGB = RGB(0, 0, 0)
                End With
            End With
            .Line.ForeColor.RGB = RGB(0, 0, 0)
            .Line.Weight = iLineWeight
            .Line.Style = iLineStyle
            .Fill.ForeColor.RGB = RGB(255, 255, 255)
        End With
        
    End With
    
    '*****************************************
    '* 演算式と図形情報を画面定義シートへ記載
    '*****************************************
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet
    
        '* 記載開始範囲を設定
        Set startRange = .Range("A2")
        
        '* 空行を探す
        For II = 0 To 100
            Set workRange = startRange.Offset(II, 0)
            If workRange.Value = "" Then Exit For
        Next
        
        '* 書き出し場所がなければ処理中断
        If II > 100 Then
            currShape.Delete
            'AISMM No.82 sunyi 20181211 start
            WriteValueTextBox = 0
            'AISMM No.82 sunyi 20181211 end
            Exit Function
        End If
        
        workRange.Offset(0, 0).Value = currShapeName
        workRange.Offset(0, 1).Value = calcString
        workRange.Offset(0, 3).Value = isFormat
        
        '* link部分の値
        'リンク設定 表示フラグ
        workRange.Offset(0, 20).Value = linkBln
        'リンク種別
        workRange.Offset(0, 21).Value = commandListType(0)
        'リンク先
        If Not linkBln Then
            workRange.Offset(0, 22).Value = ""
        Else
            workRange.Offset(0, 22).Value = commandListPass(0)
        End If
        
        'エレメント値 表示フラグ
        workRange.Offset(0, 23).Value = elementBln
        'エレメント値
        If Not elementBln Then
            elementString = ""
        End If
        workRange.Offset(0, 24).Value = elementString
        
        '* LinkSheetにを保存する
        If linkBln Then
            Call WriteValueLinkSheet("create", linkBln, commandListPass, currShapeName, commandListType, commandListCount)
        End If
        
        '* 表示値の数式を生成する
        itemOpeString = GetItemOperation(calcString)
        
        '* 演算形式を判定する
        isCalcString = False
        If InStr(workRange.Offset(0, 1).Value, ",") > 0 Then
            splitStr = Split(workRange.Offset(0, 1).Value, ",")
            If InStr(splitStr(1), "種類数") Then
              isCalcString = True
            End If
        End If

        With workRange.Offset(0, 2)
        
            '* 書式を設定する
            .NumberFormatLocal = "G/標準"
    
            '* 数式を代入する
            WriteLog ("MultiStatusMonitorTemplate.xlsm WriteValueTextBox()  currShapeName:[" & currShapeName & "] currShapeNum:[" & currShapeNum & "] isFormat:[" & isFormat & "] calcString:[" & calcString & "] itemOpeString[" & itemOpeString & "] isCalcString[" & isCalcString & "]")
            .FormulaLocal = itemOpeString
            
            If IsDate(workRange.Offset(0, 2).Value) Then
                .NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
            ElseIf IsNumeric(.Value) Then
                '* ISSUE_NO.670 sunyi 2018/05/15 start
                '* オーバーフローを修正
                'If .Value = CLng(.Value) Or isCalcString Then
                'If .Value = CDbl(.Value) Or isCalcString Then
                'If .Value Mod 1 = 0 Or isCalcString Then
                If InStr(.text, ".") <= 0 Or isCalcString Then
                '* ISSUE_NO.670 sunyi 2018/05/15 end
                    .NumberFormatLocal = "0"
                Else
                    .NumberFormatLocal = "0.00"
                End If
            End If
        End With
        
        Set dispRange = .Range(workRange.Offset(0, 5), workRange.Offset(0, 19))
        dispRange.NumberFormatLocal = "@"
        dispRange = dispColorCond
        
        '* foastudio sunyi 2018/10/23 start
        Set dispRange = .Range(workRange.Offset(0, 25), workRange.Offset(0, 41))
        dispRange = dispTextCond
        '* foastudio sunyi 2018/10/23 end
    End With
ErrorHandler1:
    If Err.Number > 0 Then
        MsgBox "式にエラーがあります。確認してください"
        Err.Clear
        GoTo last
    End If
        
    
    '*****************************************
    '* コマンドリスト情報をlinkparamシートへ記載
    '*****************************************
    Set linkParamsheet = Workbooks(thisBookName).Worksheets(LinkParamSheetName)
    With linkParamsheet
        Set srchRange = .Range("A:A")
        Set srchNmRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
        iEndRow = linkParamsheet.UsedRange.Rows.Count

        If srchNmRange.Offset(iEndRow, 0).Value = "" Then

            For i = 0 To commandListCount - 1 Step 1
                srchNmRange.Offset(iEndRow + i, 0).Value = currShapeName
                srchNmRange.Offset(iEndRow + i, 1).Value = commandListType(i)
                srchNmRange.Offset(iEndRow + i, 2).Value = Chr(39) & commandListName(i)
                srchNmRange.Offset(iEndRow + i, 3).Value = Chr(39) & commandListPass(i)
            Next i
        End If
    End With
    
    '*****************************************
    '* 条件を条件画面定義シートへ記載
    '*****************************************
    Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    With condParamsheet
        Set srchRange = .Range("A:A")
        Set srchNmRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
        iEndRow = condParamsheet.UsedRange.Rows.Count
        
        If Not srchNmRange Is Nothing Then
            '* 全図形に対して
            If srchNmRange.Offset(iEndRow, 0).Value = "" Then
                
                For i = 0 To elementListCount - 1 Step 1
                    srchNmRange.Offset(iEndRow + i, 0).Value = currShapeName
                    srchNmRange.Offset(iEndRow + i, 1).Value = elementList(i)
                    srchNmRange.Offset(iEndRow + i, 2).Value = Chr(39) & elementCondList(i)
                Next i
            End If
        End If
    End With
    
    On Error Resume Next
    If elementBln Then
        currShape.TextFrame.Characters.text = elementString
    Else
        Select Case isFormat
            Case 0
                With workRange.Offset(0, 2)
                    '種類数の場合は書式固定
                    '* ISSUE_NO.670 sunyi 2018/05/15 start
                    '* オーバーフローを修正
                    'If .Value = CLng(.Value) Or isCalcString Then
                    'If .Value = CDbl(.Value) Or isCalcString Then
                    'If .Value Mod 1 = 0 Or isCalcString Then
                    If InStr(.text, ".") <= 0 Or isCalcString Then
                    '* ISSUE_NO.670 sunyi 2018/05/15 end
                        currShape.TextFrame.Characters.text = Format(.Value, "0")
                    Else
                        currShape.TextFrame.Characters.text = Format(.Value, "0.00")
                    End If
                End With
            Case 1
'                '* ISSUE_NO.759 Sunyi 2018/06/25 Start
'                '* 空欄の場合、””に表示する
'    '            currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
'                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
'                    workRange.Offset(0, 2).Value = ""
'                Else
'                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
'                End If
'                '* ISSUE_NO.759 Sunyi 2018/06/25 End
                'ISSUE_NO.802 sunyi 2018/07/26 Start
                '文字列の場合で、空をそのまま出力する
                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
                'ISSUE_NO.802 sunyi 2018/07/26 End
            Case 2
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd")
                End If
            Case 3
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "hh:mm:ss")
                End If
            Case 4
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd hh:mm:ss")
                End If
        End Select
    End If
last:
  
    'AISMM No.82 sunyi 20181211 start
    WriteValueTextBox = 0
    'AISMM No.82 sunyi 20181211 end
End Function

'*************************************************
'* 画面の情報を元に入力テキストボックスを更新する
'*************************************************
Public Sub UpdateValueTextBox(currShapeName As String, calcString As String, isFormat As Integer, dispColorCond() As String, _
                    linkBln As Boolean, elementBln As Boolean, elementString As String, _
                    elementCondList() As String, elementList() As String, elementListCount As Integer, _
                    commandListType() As Integer, commandListName() As String, commandListPass() As String, commandListCount As Integer, dispTextCond() As String)
    Dim MyKeyState          As Long
    Dim nowShapeNum         As Integer
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim condParamsheet      As Worksheet
    Dim linkParamsheet      As Worksheet
    Dim srchNmRange         As Range
    Dim scondRange          As Range
    Dim scondNmRange        As Range
    Dim dispCondRange       As Range
    Dim i                   As Integer
    Dim currShape           As Shape
    Dim currShapeNum        As Integer
    Dim II                  As Integer
    Dim srchRange           As Range
    Dim workRange           As Range
    Dim dispRange           As Range
    Dim itemOpeString       As String
    Dim idArray(100)        As Boolean
    Dim workStr             As String
    Dim splitStr            As Variant
    Dim isCalcString        As Boolean
    Dim iLineWeight         As Integer
    Dim iLineStyle          As Integer
    Dim iEndRow             As Integer

    On Error GoTo errlable

    Call SetFilenameToVariable
    '* ISSUE_NO.667 sunyi 2018/05/09 start
    '* テキストボックス作成後に編集画面でリンク設定をした場合、ボックスの枠が二重線にセット
    If linkBln Then
        iLineWeight = 5
        iLineStyle = xlDoubleAccounting
    Else
        iLineWeight = 1.5
        iLineStyle = xlContinuous
    End If
    '* ISSUE_NO.667 sunyi 2018/05/09 end
    
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    
    Set currShape = statusWorksheet.Shapes(currShapeName)
    
    '* ISSUE_NO.667 sunyi 2018/05/09 start
    '* テキストボックス作成後に編集画面でリンク設定をした場合、ボックスの枠が二重線にセット
    currShape.Line.Style = iLineStyle
    currShape.Line.Weight = iLineWeight
    '* ISSUE_NO.667 sunyi 2018/05/09 end
    
    '*****************************************
    '* 演算式と図形情報を画面定義シートへ記載
    '*****************************************
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet
    
        '* 記載開始範囲を設定
        Set srchRange = .Range("A:A")
        Set workRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
        If workRange Is Nothing Then Exit Sub
        
        workRange.Offset(0, 1).Value = calcString
        workRange.Offset(0, 3).Value = isFormat
        
        '* link部分の値
        'リンク設定 表示フラグ
        workRange.Offset(0, 20).Value = linkBln
        'リンク種別
        workRange.Offset(0, 21).Value = commandListType(0)
        'リンク先
        If Not linkBln Then
            workRange.Offset(0, 22).Value = ""
        Else
            workRange.Offset(0, 22).Value = commandListPass(0)
        End If
        
        'エレメント値 表示フラグ
        workRange.Offset(0, 23).Value = elementBln
        'エレメント値
        If Not elementBln Then
            elementString = ""
        End If
        workRange.Offset(0, 24).Value = elementString
        
        '* LinkSheetにを更新する
        Call WriteValueLinkSheet("update", linkBln, commandListPass, currShapeName, commandListType, commandListCount)
        
        '* 表示値の数式を生成する
        itemOpeString = GetItemOperation(calcString)
        
        '-2017/05/08 No.555 Add.
        '* 演算形式を判定する
        isCalcString = False
        If InStr(workRange.Offset(0, 1).Value, ",") > 0 Then
            splitStr = Split(workRange.Offset(0, 1).Value, ",")
            If InStr(splitStr(1), "種類数") Then
              isCalcString = True
            End If
        End If
        
        With workRange.Offset(0, 2)

            '* 書式を設定する
            .NumberFormatLocal = "G/標準"
    
            '* 数式を代入する
            .FormulaLocal = itemOpeString
        
            If IsDate(.Value) Then
                .NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
            ElseIf IsNumeric(.Value) Then
                '種類数の場合は書式固定
                '* ISSUE_NO.670 sunyi 2018/05/15 start
                '* オーバーフローを修正
                'If .Value = CLng(.Value) Or isCalcString Then
                'If .Value = CDbl(.Value) Or isCalcString Then
                'If .Value Mod 1 = 0 Or isCalcString Then
                If InStr(.text, ".") <= 0 Or isCalcString Then
                '* ISSUE_NO.670 sunyi 2018/05/15 end
                    .NumberFormatLocal = "0"
                Else
                    .NumberFormatLocal = "0.00"
                End If
            End If
        End With
        
        Set dispRange = .Range(workRange.Offset(0, 5), workRange.Offset(0, 19))
        dispRange.NumberFormatLocal = "@"
        dispRange = dispColorCond
        
        '* foastudio sunyi 2018/10/23 start
        Set dispRange = .Range(workRange.Offset(0, 25), workRange.Offset(0, 41))
        dispRange = dispTextCond
        '* foastudio sunyi 2018/10/23 end
    
    End With
        
    '*****************************************
    '* コマンドリスト情報をlinkparamシートへ記載
    '*****************************************
    Set linkParamsheet = Workbooks(thisBookName).Worksheets(LinkParamSheetName)
    With linkParamsheet
        Set srchRange = .Range("A:A")
        Set srchNmRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
        iEndRow = linkParamsheet.UsedRange.Rows.Count

        If Not srchNmRange Is Nothing Then
         '* 更新の場合
            
            '* 全図形に対して
            For II = 1 To iEndRow
                Set dispCondRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
            
                If Not dispCondRange Is Nothing Then
                    ' データを削除する
                    .Rows(dispCondRange.Row).Delete
                Else
                    Exit For
                End If
            Next
            
            If linkBln Then
                '* 全図形に対して
                Set srchNmRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
                For II = 1 To 1000
                    '定義内容の有無ﾁｪｯｸ
                    If srchNmRange.Offset(II, 0).Value = "" Then
                        '* 記載開始範囲を設定
                        For i = 0 To commandListCount - 1 Step 1
                            srchNmRange.Offset(II + i, 0).Value = currShapeName
                            srchNmRange.Offset(II + i, 1).Value = commandListType(i)
                            srchNmRange.Offset(II + i, 2).Value = Chr(39) & commandListName(i)
                            srchNmRange.Offset(II + i, 3).Value = Chr(39) & commandListPass(i)
                        Next i
                        Exit For
                    End If
                Next
            End If
        End If
    End With
    
    '*****************************************
    '* 条件を条件画面定義シートへ記載
    '*****************************************
    Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    With condParamsheet
        Set scondRange = .Range("A:A")
        Set scondNmRange = scondRange.Find(What:="図形名", LookAt:=xlWhole)
        iEndRow = condParamsheet.UsedRange.Rows.Count
        
        If Not scondNmRange Is Nothing Then
            '* 更新の場合
            
            '* 全図形に対して
            For II = 1 To iEndRow
                Set dispCondRange = scondRange.Find(What:=currShapeName, LookAt:=xlWhole)
            
                If Not dispCondRange Is Nothing Then
                    ' データを削除する
                    .Rows(dispCondRange.Row).Delete
                Else
                    Exit For
                End If
            Next
        
            '* 全図形に対して
            iEndRow = condParamsheet.UsedRange.Rows.Count
            If scondNmRange.Offset(iEndRow, 0).Value = "" Then
                
                For i = 0 To elementListCount - 1 Step 1
                    scondNmRange.Offset(iEndRow + i, 0).Value = currShapeName
                    scondNmRange.Offset(iEndRow + i, 1).Value = elementList(i)
                    scondNmRange.Offset(iEndRow + i, 2).Value = Chr(39) & elementCondList(i)
                Next i
            
            End If

        End If
    End With
errlable:
    If Err.Number > 0 Then
        Err.Clear
        MsgBox "式にエラーがあります。確認してください。"
        GoTo last
    End If
        
    WriteLog ("MultiStatusMonitorTemplate.xlsm UpdateValueTextBox()  currShapeName:[" & currShapeName & "]")
    If elementBln Then
        currShape.TextFrame.Characters.text = elementString
        WriteLog ("MultiStatusMonitorTemplate.xlsm UpdateValueTextBox()  currShapeName:[" & currShapeName & "] currShape.TextFrame.Characters.text = elementString [" & elementString & "]")
    Else
        Select Case isFormat
            Case 0
                With workRange.Offset(0, 2)
                    WriteLog ("MultiStatusMonitorTemplate.xlsm UpdateValueTextBox()  currShapeName:[" & currShapeName & "] isFormat:[0] .Value[" & .Value & "] isCalcString[" & isCalcString & "] (.Value Mod 1)[" & (.Value Mod 1) & "] (InStr(.text, '.'))[" & (InStr(.text, ".")) & "]")
                    '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                    'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                    '* ISSUE_NO.670 sunyi 2018/05/15 start
                    '* オーバーフローを修正
                    'If .Value = CLng(.Value) Or isCalcString Then
                    'If .Value = CDbl(.Value) Or isCalcString Then
                    'If .Value Mod 1 = 0 Or isCalcString Then
                    If InStr(.text, ".") <= 0 Or isCalcString Then
                    '* ISSUE_NO.670 sunyi 2018/05/15 end
                    
                        '-2017/05/08 No.554 Mod. Changed # to 0.
                        currShape.TextFrame.Characters.text = Format(.Value, "0")
                    Else
                        currShape.TextFrame.Characters.text = Format(.Value, "0.00")
                    End If
                End With
            Case 1
'                '* ISSUE_NO.759 Sunyi 2018/06/25 Start
'                '* 空欄の場合、””に表示する
'    '            currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
'                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
'                    workRange.Offset(0, 2).Value = ""
'                Else
'                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
'                End If
'                '* ISSUE_NO.759 Sunyi 2018/06/25 End
                'ISSUE_NO.802 sunyi 2018/07/26 Start
                '文字列の場合で、空をそのまま出力する
                currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value)
                'ISSUE_NO.802 sunyi 2018/07/26 End
            Case 2
                '20161104 追加
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd")
                End If
            Case 3
                '20170213 追加
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "hh:mm:ss")
                End If
            Case 4
                '20170213 追加
                If workRange.Offset(0, 2).Value = 0 Or workRange.Offset(0, 2).Value = "" Then
                    workRange.Offset(0, 2).Value = ""
                Else
                    currShape.TextFrame.Characters.text = Format(workRange.Offset(0, 2).Value, "yyyy/MM/dd hh:mm:ss")
                End If
        End Select
    End If
'errlable:
'    If Err.Number = 1 Then
'        MsgBox "該当ボックスが削除されました。"
'    End If
last:
End Sub

Public Sub CalcStrTest()
    Call GetItemOperation("""部品2生産実績_部品セリアルＮｏ,個数"" + ""部品2生産実績_部品セリアルＮｏ"" + 10")
End Sub

'*********************************************************
'* 演算式よりセルに埋め込む計算式を生成する　para:定義内容
'*********************************************************
Public Function GetItemOperation(calcStringOrg As String) As String
    Dim calcType        As Variant
    Dim startPos        As Integer
    Dim endPos          As Integer
    Dim splitStr1       As Variant
    Dim splitStr2       As Variant
    Dim splitStr3       As Variant
    Dim splitStr4       As Variant
    Dim splitStr5       As Variant
    Dim splitStr6       As Variant
    Dim currType        As Variant
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim maxRow1 As Long
    Dim lastRange       As Range
    Dim workRange       As Range
    Dim workStr         As String
    Dim workStrCnt      As String
    Dim prefixStr       As String
    Dim workAddr        As String
    Dim dblQuota()      As Integer
    Dim II              As Integer
    Dim JJ              As Integer
    Dim calcStrArray    As Variant
    Dim calcString      As String
    Dim workCond        As Variant
    
    ReDim dblQuota(0)
    dblQuota(0) = -1
    
    GetItemOperation = ""
    
    ' processing on server dn 2018/09/10 start
    Set routesObj = GripEntityMap.GetRouteList
    ' processing on server dn 2018/09/10 end
    
    '* COUNTA/SUM/AVERAGE/MAX/MIN/SUMPRODUCT
    calcType = Array("COUNTA(", "SUM(", "AVERAGE(", "MAX(", "MIN(", "SUMPRODUCT(1/COUNTIF(", "COUNTIF(", "COUNTIFS(")
    
    '* ダブルクォーテーションで囲まれた部分を抽出する
    '* →取得配列の奇数インデックスに入る
    calcStrArray = Split(calcStringOrg, """") '定義内容取込

    
    For II = 1 To UBound(calcStrArray) Step 2
    
        calcString = calcStrArray(II)  '定義内容名
    
        '* 演算式のチェック
        'マルチモニター No.58 sunyi 2018/11/20
        ' "・"⇒"｜"
        'If InStr(calcString, "・") = 0 Then Exit Function
        If InStr(calcString, "｜") = 0 Then Exit Function
        
        ''''If InStr(calcString, "_") = 0 Then Exit Function
        
        '* カンマがない場合は、取得CTMの最新の値のみを対象とする
        If InStr(calcString, ",") = 0 Then
            
            '* 演算式から " で囲まれた文字列を取得する
            '* "部品1生産実績_部品セリアルＮｏ,個数" / 100 ⇒「=SUM(XX:YY) / 100」
            'マルチモニター No.58 sunyi 2018/11/20
            ' "・"⇒"｜"
            'splitStr2 = Split(calcString, "・")    ' 「部品1生産実績」「部品セリアルＮｏ」
            splitStr2 = Split(calcString, "｜")    ' 「部品1生産実績」「部品セリアルＮｏ」
            
            ''''splitStr2 = Split(calcString, "_")    ' 「部品1生産実績」「部品セリアルＮｏ」
            
            ' processing on server dn 2018/09/10 start
            'CTMシートから値の取込
            'Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr2(0))
            
            ' GripのMissionリスト取得
            If routesObj.Exists(splitStr2(0)) Then
                'Gripシートから値の取込
                Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr2(0))
            ElseIf InStr(splitStr2(0), "Route") = 0 Then
                'CTMシートから値の取込
                Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr2(1))
            Else
                Exit Function
            End If
            ' processing on server dn 2018/09/10 end
            With currWorksheet
                Set srchRange = .Range("1:1")
                ' processing on server dn 2018/09/10 start
                ' Wang Issue NO.727 2018/06/15 Start
'                If IsGripType Then
'                    Set startRange = srchRange.Find(What:=splitStr2(2), LookAt:=xlWhole)
'                Else
'                    Set startRange = srchRange.Find(What:=splitStr2(1), LookAt:=xlWhole)
'                End If
                Set startRange = srchRange.Find(What:=splitStr2(2), LookAt:=xlWhole)
                ' Wang Issue NO.727 2018/06/15 End
                ' processing on server dn 2018/09/10 end
                If Not startRange Is Nothing Then
                    Set workRange = startRange.Offset(1, 0)             '* 着目エレメントの最新データ範囲
                    
                    'ISSUE_NO.802 sunyi 2018/07/26 Start
                    '文字列の場合で、空をそのまま出力する
                    If UBound(calcStrArray) < 3 And workRange.text = "" Then
                        GetItemOperation = ""
                        Exit Function
                    End If
                    'ISSUE_NO.802 sunyi 2018/07/26 End
                    
                    '* 数式の生成:CTMシートのセル位置取得
                    workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                    '2017/09/29 No.544 Mod.  CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.
                    'workStr = .Name & "!" & workAddr
                    workStr = "'" & .Name & "'" & "!" & workAddr
                Else
                    Exit Function
                End If
            End With
        
        '* カンマがある場合は、演算形式に応じた式を生成する
        Else
        
            '* 演算式から " で囲まれた文字列を取得する
            '* "部品1生産実績_部品セリアルＮｏ,個数" / 100 ⇒「=SUM(XX:YY) / 100」
            splitStr2 = Split(calcString, ",")      ' 「部品1生産実績_部品セリアルＮｏ」「個数」（「合致条件」）
            'マルチモニター No.58 sunyi 2018/11/20
             ' "・"⇒"｜"
            'splitStr3 = Split(splitStr2(0), "・")    ' 「部品1生産実績」「部品セリアルＮｏ」
            splitStr3 = Split(splitStr2(0), "｜")    ' 「部品1生産実績」「部品セリアルＮｏ」
            
            ''''splitStr3 = Split(splitStr2(0), "_")    ' 「部品1生産実績」「部品セリアルＮｏ」
                
            If InStr(splitStr2(1), "条件個数") Then
                workCond = splitStr2(2)
                
                ' processing on server dn 2018/09/10 start
                'Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                ' GripのMissionリストに存在するかどうか
                If routesObj.Exists(splitStr2(0)) Then
                    'Gripシートから値の取込
                    Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                ElseIf InStr(splitStr2(0), "Route") = 0 Then
                    'CTMシートから値の取込
                    Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(1))
                Else
                    Exit Function
                End If
                ' processing on server dn 2018/09/10 end
                With currWorksheet
                    Set srchRange = .Range("1:1")
                    ' processing on server dn 2018/09/10 start
                    ' Wang Issue NO.727 2018/06/15 Start
'                    If IsGripType Then
'                        Set startRange = srchRange.Find(What:=splitStr3(2), LookAt:=xlWhole)
'                    Else
'                        Set startRange = srchRange.Find(What:=splitStr3(1), LookAt:=xlWhole)
'                    End If
                    Set startRange = srchRange.Find(What:=splitStr3(2), LookAt:=xlWhole)
                    ' Wang Issue NO.727 2018/06/15 End
                    ' processing on server dn 2018/09/10 end
                    If Not startRange Is Nothing Then
                        Set lastRange = startRange.Offset(0, 0).End(xlDown)
                        maxRow1 = getLastRow(currWorksheet)
                        Set workRange = .Range(startRange.Offset(1, 0), startRange.Offset(maxRow1, 0)) '* 着目エレメントのデータ範囲

                        'Set lastRange = startRange.Offset(0, 0).End(xlDown)
                        'Set workRange = .Range(startRange.Offset(1, 0), lastRange)  '* 着目エレメントのデータ範囲
                        '* データ範囲アドレスの取得
                        workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                        '* 数式の生成
                        '2017/09/29 No.544 Mod. CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.
                        'workStr = "COUNTIF(" & .Name & "!" & workAddr & ", ""=" & workCond & """)"
                        workStr = "COUNTIF(" & "'" & .Name & "'" & "!" & workAddr & ", ""=" & workCond & """)"
                    Else
                        Exit Function
                    End If
                End With
                
            ElseIf InStr(splitStr2(1), "単一条件") Or InStr(splitStr2(1), "複数条件") Then         '個数(単一条件)と個数(複数条件)の時
On Error GoTo errlable
                '* "部品1生産実績_部品セリアルＮｏ,個数" / 100 ⇒「=SUM(XX:YY) / 100」
                splitStr4 = Mid(splitStr2(1), 10, Len(splitStr2(1)) - 10)  ' 「部品1生産実績_部品セリアルＮｏ」「個数」（「合致条件」）
                splitStr5 = Split(splitStr4, ";")                          ' 「部品1生産実績」「部品セリアルＮｏ」
                
                Select Case Left(splitStr2(1), 8)
                    Case "個数(複数条件)"
                        prefixStr = calcType(7)
                    Case "個数(単一条件)"
                        prefixStr = calcType(6)
                End Select
                
                workStrCnt = ""                                                     '初期値がNULLで設定する
                For JJ = 0 To UBound(splitStr5)
                    splitStr6 = Split(splitStr5(JJ), ":")

                    ' processing on server dn 2018/09/10 start
                    'Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                    ' GripのMissionリストに存在するかどうか
                    If routesObj.Exists(splitStr3(0)) Then
                        'Gripシートから値の取込
                        Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                    ElseIf InStr(splitStr2(0), "Route") = 0 Then
                        'CTMシートから値の取込
                        Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(1))
                    Else
                        Exit Function
                    End If
                    ' processing on server dn 2018/09/10 end
                    With currWorksheet
                        Set srchRange = .Range("1:1")
                        Set startRange = srchRange.Find(What:=splitStr6(0), LookAt:=xlWhole)

                        If Not startRange Is Nothing Then
                        
                             Set lastRange = startRange.Offset(0, 0).End(xlDown)
                             maxRow1 = getLastRow(currWorksheet)
                             Set workRange = .Range(startRange.Offset(1, 0), startRange.Offset(maxRow1, 0)) '* 着目エレメントのデータ範囲
                            
'                            Set lastRange = startRange.Offset(0, 0).End(xlDown)
'                            Set workRange = .Range(startRange.Offset(1, 0), lastRange)     '* 着目エレメントのデータ範囲
                            '* 数式の生成
                            workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                          
                            If prefixStr = calcType(6) Then                         '個数(単一条件)のみ時
                                workStrCnt = "'" & .Name & "'" & "!" & workAddr & "," & Chr(34) & splitStr6(1) & Chr(34)
                                Exit For
                            Else                                                    '個数(複数条件)のみ時
                                If workStrCnt = "" Then
                                    workStrCnt = "'" & .Name & "'" & "!" & workAddr & "," & Chr(34) & splitStr6(1) & Chr(34)
                                Else
                                    workStrCnt = workStrCnt & "," & "'" & .Name & "'" & "!" & workAddr & "," & Chr(34) & splitStr6(1) & Chr(34)
                                End If
                            End If
                        Else
                            Exit Function
                        End If

                    End With
                Next
                
                workStr = prefixStr & workStrCnt & ")"
                
errlable:
                If Err.Number <> 0 Then
                    MsgBox "演算形式が「個数(単一条件) 」または「個数(複数条件)」の場合、通常のエレメントリストからは選択できません｡エレメント|条件リストから選択してドラッグ＆ドロップ操作してください。"
                End If
            Else
                Select Case splitStr2(1)
                    Case "個数"
                        prefixStr = calcType(0)
                    Case "積算"
                        prefixStr = calcType(1)
                    Case "平均"
                        prefixStr = calcType(2)
                    Case "最大"
                        prefixStr = calcType(3)
                    Case "最小"
                        prefixStr = calcType(4)
                    Case "種類数"
                        prefixStr = calcType(5)
                End Select
            
                ' processing on server dn 2018/09/10 start
                'Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                ' GripのMissionリストに存在するかどうか
                If routesObj.Exists(splitStr3(0)) Then
                    'Gripシートから値の取込
                    Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(0))
                ElseIf InStr(splitStr2(0), "Route") = 0 Then
                    'CTMシートから値の取込
                    Set currWorksheet = Workbooks(thisBookName).Worksheets(splitStr3(1))
                Else
                    Exit Function
                End If

                ' processing on server dn 2018/09/10 end
                With currWorksheet
                    ' TODO dn isGripType 処理　2018/09/07
                    Set srchRange = .Range("1:1")
                    ' processing on server dn 2018/09/10 start
                    ' Wang Issue NO.727 2018/06/15 Start
'                    If IsGripType Then
'                        Set startRange = srchRange.Find(What:=splitStr3(2), LookAt:=xlWhole)
'                    Else
'                        Set startRange = srchRange.Find(What:=splitStr3(1), LookAt:=xlWhole)
'                    End If
                    Set startRange = srchRange.Find(What:=splitStr3(2), LookAt:=xlWhole)
                    ' Wang Issue NO.727 2018/06/15 End
                    ' processing on server dn 2018/09/10 end
                    If Not startRange Is Nothing Then
'                        Set lastRange = startRange.Offset(0, 0).End(xlDown)
'                        Set workRange = .Range(startRange.Offset(1, 0), lastRange)  '* 着目エレメントのデータ範囲
                        Set lastRange = startRange.Offset(0, 0).End(xlDown)
                        maxRow1 = getLastRow(currWorksheet)
                        Set workRange = .Range(startRange.Offset(1, 0), startRange.Offset(maxRow1, 0)) '* 着目エレメントのデータ範囲

                        '* 数式の生成
                        workAddr = workRange.AddressLocal(RowAbsolute:=False, ColumnAbsolute:=False)
                        '2017/09/29 No.544 Mod. CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.---------Start
                        If InStr(prefixStr, "SUMPRODUCT") Then
                            'workStr = prefixStr & .Name & "!" & workAddr & "," & .Name & "!" & workAddr & "))"
                            ' BugFix AIS_MM.108 2019/07/30 yakiyama start.
                            'workStr = prefixStr & "'" & .Name & "'" & "!" & workAddr & "," & .Name & "!" & workAddr & "))"
                            workStr = "IF(COUNTIF(" & "'" & .Name & "'" & "!" & workAddr & "," & "'" & .Name & "'" & "!" & workAddr & ")=0,0," & "(" & prefixStr & "'" & .Name & "'" & "!" & workAddr & "," & "'" & .Name & "'" & "!" & workAddr & "))" & ")" & ")"
                            ' BugFix AIS_MM.108 2019/07/30 yakiyama end.
                        Else
                            'workStr = prefixStr & .Name & "!" & workAddr & ")"
                            workStr = prefixStr & "'" & .Name & "'" & "!" & workAddr & ")"
                        End If
                        '2017/09/29 No.544 Mod. CTM名にカッコがある場合の対策：シート名(CTM名)を文字列へ変更.---------End
                    Else
                        Exit Function
                    End If
                End With
                
            End If
            
        End If
    
        calcStringOrg = Replace(calcStringOrg, """" & calcString & """", workStr, 1)
    
    Next
    
    If calcStringOrg <> "" Then
        GetItemOperation = "=" & calcStringOrg
    Else
        GetItemOperation = ""
    End If

End Function

'***********************************
'* すべての図形と画面定義を削除する
'***********************************
Public Sub DeleteAllShapeAndDispDef()
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim currShape           As Shape
    Dim startRange          As Range
    Dim lastRange           As Range
    Dim workRange           As Range

    '2017/09/26 No.533 Add.
    Call SetFilenameToVariable

    '*****************
    '* 図形を削除する
    '*****************
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    With statusWorksheet
    
        For Each currShape In .Shapes
            currShape.Delete
        Next currShape
    
    End With
    
    '*********************
    '* 画面定義を削除する
    '*********************
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet
    
        Set startRange = .Range("A1")
        If startRange.Offset(1, 0).Value <> "" Then
            Set lastRange = startRange.Offset(0, 0).End(xlToRight)
            Set lastRange = lastRange.Offset(0, 3)
            Set workRange = .Range(startRange.Offset(1, 0), lastRange.Offset(100, 0))
            workRange.ClearContents
        End If
    
    End With
    
End Sub

'*******************************************
'* 指定のテキストボックスの背景色を変更する
'*******************************************
Public Sub ChangeBackColorAndTextTB(currWorksheet As Worksheet, shapeName As String, rgbFill As Long, textValue As String)
    Dim currShape       As Shape
    
    Set currShape = currWorksheet.Shapes(shapeName)
    With currShape
        '* 図形名
'        .Name = shapeName
        '* 登録マクロ名
'        .OnAction = macroName
        With .TextFrame2
            '* 文字列配置位置
'            .VerticalAnchor = msoAnchorMiddle
            With .TextRange
                .text = textValue
                '* 文字列配置一
'                .ParagraphFormat.Alignment = msoAlignCenter
                '* 文字列太字設定
'                .Font.Bold = msoTrue
                '* 文字サイズ
'                .Font.Size = 16
                '* 文字色
'                .Font.Fill.ForeColor.RGB = RGB(0, 0, 0)
            End With
        End With
        '* 文字枠色
'        .Line.ForeColor.RGB = RGB(0, 0, 0)
        '* 文字枠太さ
'        .Line.Weight = 1.5
        '* 文字枠塗りつぶし
        .Fill.ForeColor.RGB = rgbFill
    End With
End Sub

'*******************************************
'* 指定のテキストボックスの背景色を変更する
'*******************************************
Public Sub ChangeBackColorTB(currWorksheet As Worksheet, shapeName As String, rgbFill As Long)
    Dim currShape       As Shape
    
    Set currShape = currWorksheet.Shapes(shapeName)
    With currShape
        '* 図形名
'        .Name = textValue
        '* 登録マクロ名
'        .OnAction = macroName
        With .TextFrame2
            '* 文字列配置位置
'            .VerticalAnchor = msoAnchorMiddle
            With .TextRange
                '* 文字列配置一
'                .ParagraphFormat.Alignment = msoAlignCenter
                '* 文字列太字設定
'                .Font.Bold = msoTrue
                '* 文字サイズ
'                .Font.Size = 16
                '* 文字色
'                .Font.Fill.ForeColor.RGB = RGB(0, 0, 0)
            End With
        End With
        '* 文字枠色
'        .Line.ForeColor.RGB = RGB(0, 0, 0)
        '* 文字枠太さ
'        .Line.Weight = 1.5
        '* 文字枠塗りつぶし
        .Fill.ForeColor.RGB = rgbFill
    End With
End Sub

'*******************************************
'* 表示範囲の背景色を変更する
'*******************************************
Public Sub ChangeBackColorStatus(currRange As Range, rgbFill As Long)
    
    '* 選択範囲を薄い灰色にする
    currRange.Interior.Color = rgbFill

End Sub

'*******************************************
'* 指定のテキストボックスのフォント色を変更する
'*******************************************
Public Sub ChangeFontColor(currWorksheet As Worksheet, shapeName As String, rgbFont As Long)
    Dim currShape       As Shape
    
    Set currShape = currWorksheet.Shapes(shapeName)
    With currShape
        '* 図形名
'        .Name = textValue
        '* 登録マクロ名
'        .OnAction = macroName
        With .TextFrame2
            '* 文字列配置位置
'            .VerticalAnchor = msoAnchorMiddle
            With .TextRange
                '* 文字列配置一
'                .ParagraphFormat.Alignment = msoAlignCenter
                '* 文字列太字設定
'                .Font.Bold = msoTrue
                '* 文字サイズ
'                .Font.Size = 16
                '* 文字色
                .Font.Fill.ForeColor.RGB = rgbFont
            End With
        End With
        '* 文字枠色
'        .Line.ForeColor.RGB = RGB(0, 0, 0)
        '* 文字枠太さ
'        .Line.Weight = 1.5
        '* 文字枠塗りつぶし
'        .Fill.ForeColor.RGB = rgbFill
    End With
End Sub

'*******************************************
'* 指定のテキストボックスのテキストフォーマット(数値)を変更する
'*******************************************
Public Sub ChangeTextBoxTextNumberFormat(currWorksheet As Worksheet, shapeName As String, currRange As Range, numericFormat As String)
    Dim currShape       As Shape
    Set currShape = currWorksheet.Shapes(shapeName)
    currShape.TextFrame.Characters.text = Format(currRange.Value, numericFormat)
    
    'If currRange.Value Mod 1 = 0 Or isCalcString Then
    ''* ISSUE_NO.670 sunyi 2018/05/15 end
    '
    '    '-2017/05/08 No.554 Mod. Changed # to 0.
    '    currShape.TextFrame.Characters.text = Format(currRange.Value, "0")
    'Else
    '    currShape.TextFrame.Characters.text = Format(currRange.Value, "0.00")
    'End If
End Sub

'*******************************************
'* RGBカラー配列取得（Long値）
'*******************************************
Public Sub GetRGBColorArray(rgbColor() As Long)
    Dim paramColWorksheet      As Worksheet
    Dim i                      As Integer
    Dim srchRange              As Range
    Dim currRange              As Range
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    With paramColWorksheet
    
        Set srchRange = .Range("1:1")
        Set currRange = srchRange.Find(What:="操作パネル表示", LookAt:=xlWhole)
        
        If Not currRange Is Nothing Then
            For i = 0 To 14
                If currRange.Offset(i + 1, 1).Value <> "" Then
                    rgbColor(i) = currRange.Offset(i + 1, 1).Value
                Else
                    rgbColor(i) = currRange.Offset(i + 1, 0).Value
                End If
        
            Next
        End If
    End With

End Sub

'*******************************************
'* RGB背景表示カラー配列取得（Long値）
'*******************************************
Public Sub GetRGBColorBackArray(rgbColorBack() As Long)
    Dim paramColWorksheet      As Worksheet
    Dim i                      As Integer
    Dim srchRange              As Range
    Dim currRange              As Range
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    With paramColWorksheet
    
    Set srchRange = .Range("1:1")
    Set currRange = srchRange.Find(What:="背景設定", LookAt:=xlWhole)
    
    If Not currRange Is Nothing Then
        For i = 0 To 3
            If currRange.Offset(i + 1, 1).Value <> "" Then
                rgbColorBack(i) = currRange.Offset(i + 1, 1).Value
            Else
                rgbColorBack(i) = currRange.Offset(i + 1, 0).Value
            End If
    
        Next
    End If
    End With

End Sub

'*******************************************
'* 背景色と全てのテキストボックスの内容更新
'*******************************************
Public Sub UpdateAllBackAndTB()
'2017/06/14 Del No.260.
'Attribute UpdateAllBackAndTB.VB_ProcData.VB_Invoke_Func = "r\n14"
    Dim statusWorksheet     As Worksheet
    Dim dispDefWorksheet    As Worksheet
    Dim paramWorksheet      As Worksheet
    Dim currWorksheet       As Worksheet
    Dim currShape           As Shape
    Dim currRange           As Range
    Dim nowColorRange       As Range
    Dim srchRange           As Range
    Dim dispRange           As Range
    Dim startRange          As Range
    Dim workRange           As Range
    Dim II                  As Integer
    Dim JJ                  As Integer
    Dim formulaStr          As String
    Dim shapeName           As String
    Dim isFormat            As Integer
    Dim rgbColor(15)        As Long
    Dim rgbColorBack(4)     As Long
    Dim splitValue          As Variant
    Dim getValue            As Variant
    
    Dim getText1Value       As String
    Dim getText2Value       As String
    Dim getTextValue        As String
    Dim getDataValue        As String
    '* sunyi 2018/11/16 start
    Dim arr                 As Variant
    Dim arr1                 As Variant
    Dim arr2                 As Variant
    Dim comboboxValue1           As String
    Dim comboboxValue2           As String
    Dim settingValue1           As String
    Dim settingValue2           As String
    '* sunyi 2018/11/16 end
    
    Dim getValue1            As Variant
    Dim backColorCond(4)    As Variant
    Dim chgElement          As String
    Dim currValue           As Variant
    Dim dispAddress         As String
    
    '*AISMM-81 sunyi 20190109 start
    '条件なしの場合、白にする
    Dim defaultColor     As Long
    '*AISMM-81 sunyi 20190109 end
    Dim shp As Object
    Dim flagExist As Boolean
    
    ' AISMM-100 yakiyama 20190626 start
    Dim hasNullConditionDefaultText As Boolean
    Dim isSetTextValue As Boolean
    Dim nullConditionDefaultText As String
    ' AISMM-100 yakiyama 20190626 end
    
    ' AISMM-103 yakiyama 20190725 start
    Dim numericFormat As String
    ' AISMM-103 yakiyama 20190725 end
        
    '* 色配列を取得する
    Call GetRGBColorArray(rgbColor)
    Call GetRGBColorBackArray(rgbColorBack)
    
'    rgbColorBack(0) = rgbColor(13)  ''''''''''''''''''''''20160619
'    rgbColorBack(1) = rgbColor(1)
'    rgbColorBack(2) = rgbColor(4)
''    rgbColorBack(3) = rgbColor(8)
'    '* 緑だけ特別設定
'    rgbColorBack(3) = RGB(0, 176, 80)
    
    '2017/09/26 No.533 Add.
    Call SetFilenameToVariable
    
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    '**************************************************************************************
    '* 画面定義シートにある演算式を更新し、ステータスシート上のテキストボックスを更新する
    '**************************************************************************************
    With dispDefWorksheet
    
        Set srchRange = .Range("1:1")
        Set currRange = srchRange.Find(What:="定義内容", LookAt:=xlWhole)
        If Not currRange Is Nothing Then
        
            '* 全図形に対して
            For II = 1 To 100
                '定義内容の有無ﾁｪｯｸ
                If currRange.Offset(II, 0).Value <> "" Then
                    WriteLog ("MultiStatusMonitorTemplate.xlsm  II:[" & II & "]  [If currRange.Offset(II, 0).Value <> '']")
                    
                    Set dispRange = srchRange.Find(What:="表示色−白", LookAt:=xlWhole)
                    Set dispRange = .Cells(currRange.Offset(II, 0).Row, dispRange.Column)
                
                    '* 数式を更新する
                    formulaStr = GetItemOperation(currRange.Offset(II, 0).Value)
                    
                    With currRange.Offset(II, 1)
                        .NumberFormatLocal = "G/標準"
                        numericFormat = "G/標準"
                        .FormulaLocal = formulaStr
                        If IsDate(.Value) Then
                            .NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
                        ElseIf IsNumeric(.Value) Then
'                            If InStr(currRange.Offset(II, 0).Value, "個数") Or _
'                               InStr(currRange.Offset(II, 0).Value, "種類数") Then
                           '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                           'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                           '* ISSUE_NO.670 sunyi 2018/05/15 start
                            '* オーバーフローを修正
                            'If .Value = CLng(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                            'If .Value = CDbl(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                            WriteLog ("MultiStatusMonitorTemplate.xlsm UpdateAllBackAndTB()  II:[" & II & "] IsNumeric:[True] .Value[" & .Value & "] InStr(currRange.Offset(II, 0).Value, '種類数')[" & InStr(currRange.Offset(II, 0).Value, "種類数") & "] (.Value Mod 1)[" & (.Value Mod 1) & "] (InStr(.text, '.'))[" & (InStr(.text, ".")) & "]")
                            'If .Value Mod 1 = 0 Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                            If InStr(.text, ".") <= 0 Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                            '* ISSUE_NO.670 sunyi 2018/05/15 end
                                '-2017/05/08 No.554 Mod. Changed # to 0.
                                .NumberFormatLocal = "0"
                                numericFormat = "0"
                            Else
                                .NumberFormatLocal = "0.00"
                                numericFormat = "0.00"
                            End If
                        End If
                        
                        isFormat = currRange.Offset(II, 2).Value
                        
                        '* テキストボックスへ数式の値をセットする
                        shapeName = currRange.Offset(II, -1).Value '図形名の取込　value_shape_01
                        
                        '2017/01/13 追加
                        '該当Shape(ﾃｷｽﾄBox)の有無チェック
                        flagExist = False
                        For Each shp In statusWorksheet.Shapes
                            If shp.Name = shapeName Then
                                flagExist = True
                                Exit For
                            End If
                        Next shp
                        WriteLog ("MultiStatusMonitorTemplate.xlsm  II:[" & II & "]  Shape.Name:[" & shapeName & "]  isFormat:[" & isFormat & "]  flagExist:[" & flagExist & "]  .Value:[" & .Value & "]  .Value2:[" & .Value2 & "]  .WrapText:[" & .WrapText & "]")
                        
                        '2017/01/13
                        '該当のShapeがあれば、Shapeに値を設定
                        If flagExist = True Then
                            Set currShape = statusWorksheet.Shapes(shapeName)
                            On Error Resume Next
                            'Call ChangeFontColor(statusWorksheet, currShape.Name, RGB(0, 0, 0))
                            
                            '*AISMM-81 sunyi 20190109 start
                            '条件なしの場合、白にする
                            'defaultColor = 16777215
                            ' 先頭の色にする。
                            defaultColor = rgbColor(0)
                            '*AISMM-81 sunyi 20190109 end
                            ' AISMM-100 yakiyama 20190626 start
                            hasNullConditionDefaultText = False
                            isSetTextValue = False
                            nullConditionDefaultText = ""
                            ' AISMM-100 yakiyama 20190626 end
                            
                            If currRange.Offset(II, 22).Value Then
                                currShape.TextFrame.Characters.text = currRange.Offset(II, 23).Value
                                WriteLog ("MultiStatusMonitorTemplate IF(currRange.Offset(II, 22).Value)")
                                
                                Select Case isFormat
                                    '* 数値処理
                                    Case 0
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            '* foastudio sunyi 2018/10/23 start
                                            getText1Value = dispRange.Offset(0, JJ * 2 + 20).Value
                                            getText2Value = dispRange.Offset(0, JJ * 2 + 21).Value
                                            '* sunyi 2018/11/16 start
                                            If getText1Value <> "" And getText2Value <> "" Then
                                                getTextValue = getText1Value & vbCrLf & getText2Value
                                            Else
                                            '* sunyi 2018/11/16 end
                                                getTextValue = getText1Value & getText2Value
                                            End If
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  JJ:  " & JJ & "  rgbColor:  " & rgbColor(JJ) & "  getValue:[" & getValue & "] getTextValue:[" & getTextValue & "]")
                                            
                                            ' AISMM-100 yakiyama 20190626 start
                                            If rgbColor(JJ) = defaultColor Then
                                                nullConditionDefaultText = getTextValue
                                                If nullConditionDefaultText <> "" Then
                                                    hasNullConditionDefaultText = True
                                                End If
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                            End If
                                            ' AISMM-100 yakiyama 20190626 end
                                            
                                            getDataValue = currShape.TextFrame.Characters.text
                                            If getTextValue = "" Then
                                                getTextValue = getDataValue
                                            End If
                                            '* foastudio sunyi 2018/10/23 end
                                            If getValue <> "" Then
                                            '* sunyi 2018/11/16 start
                                                If InStr(getValue, "値") And InStr(getValue, "＝") = 0 Then
                                                    arr = Split(getValue, "値")
                                                    
                                                    If arr(0) <> "" And arr(1) = "" Then
                                                        arr1 = Split(arr(0), Right(arr(0), 1))
                                                        settingValue1 = arr1(0)
                                                        comboboxValue1 = Right(arr(0), 1)
                                                        
                                                        If comboboxValue1 = "＜" Then
                                                            If .Value > Val(settingValue1) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        ElseIf comboboxValue1 = "≦" Then
                                                            If .Value >= Val(settingValue1) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                        
                                                        
                                                    ElseIf arr(0) = "" And arr(1) <> "" Then
                                                        arr2 = Split(arr(1), Left(arr(1), 1))
                                                        settingValue2 = arr2(1)
                                                        comboboxValue2 = Left(arr(1), 1)
                                                        
                                                        If comboboxValue2 = "＜" Then
                                                            If .Value < Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        ElseIf comboboxValue2 = "≦" Then
                                                            If .Value <= Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                        
                                                        
                                                    ElseIf arr(0) <> "" And arr(1) <> "" Then
                                                        arr1 = Split(arr(0), Right(arr(0), 1))
                                                        arr2 = Split(arr(1), Left(arr(1), 1))
                                                        
                                                        comboboxValue1 = Right(arr(0), 1)
                                                        comboboxValue2 = Left(arr(1), 1)
                                                        settingValue1 = arr1(0)
                                                        settingValue2 = arr2(1)
                                                        
                                                        '* 条件１：<の場合、2パタン
                                                        If comboboxValue1 = "＜" And comboboxValue2 = "＜" Then
                                                            If .Value > Val(settingValue1) And .Value < Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                        If comboboxValue1 = "＜" And comboboxValue2 = "≦" Then
                                                            If .Value > Val(settingValue1) And .Value <= Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                        
                                                        '* 条件１：<=の場合、2パタン
                                                        If comboboxValue1 = "≦" And comboboxValue2 = "＜" Then
                                                            If .Value >= Val(settingValue1) And .Value < Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                        If comboboxValue1 = "≦" And comboboxValue2 = "≦" Then
                                                            If .Value >= Val(settingValue1) And .Value <= Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                    Else
                                                        ' AISMM-100 yakiyama 20190626 start
                                                        If rgbColor(JJ) = defaultColor Then
                                                            If nullConditionDefaultText <> "" Then
                                                                hasNullConditionDefaultText = True
                                                            End If
                                                        End If
                                                        ' AISMM-100 yakiyama 20190626 end
                                                    End If
                                                 Else
                                                    arr = Split(getValue, "値")
                                                    arr1 = Split(arr(0), Right(arr(0), 1))
                                                    settingValue1 = arr1(0)
                                                    comboboxValue1 = Right(arr(0), 1)
                                                    If IsNumeric(settingValue1) Then
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        'If Val(splitValue(0)) <= .Value And .Value <= Val(splitValue(1)) Then
                                                        If Val(settingValue1) = .Value Then
                                                            isSetTextValue = True
                                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                        End If
                                                    Else
                                                        If Val(getValue) = .Value Then
                                                            isSetTextValue = True
                                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                            Exit For
                                                        Else
                                                            ' AISMM-100 yakiyama 20190626 start
                                                            If rgbColor(JJ) = defaultColor Then
                                                                If nullConditionDefaultText <> "" Then
                                                                    hasNullConditionDefaultText = True
                                                                End If
                                                            End If
                                                            ' AISMM-100 yakiyama 20190626 end
                                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                        End If
                                                    End If
                                                End If
                                                '* sunyi 2018/11/16 end
                                            Else
                                                ' AISMM-100 yakiyama 20190626 start
                                                If rgbColor(JJ) = defaultColor Then
                                                    If nullConditionDefaultText <> "" Then
                                                        hasNullConditionDefaultText = True
                                                    End If
                                                End If
                                                ' AISMM-100 yakiyama 20190626 end
                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                            End If
                                        Next
                                        
                                        ' AISMM-100 yakiyama 20190626 start
                                        WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                        If isSetTextValue = False Then
                                            If hasNullConditionDefaultText = True Then
                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, nullConditionDefaultText)
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "] NullConditionDefaultText")
                                            Else
                                                Call ChangeTextBoxTextNumberFormat(statusWorksheet, currShape.Name, currRange.Offset(II, 1), numericFormat)
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  ChangeTextBoxTextNumberFormat:  " & numericFormat & "  .Value:[" & .Value & "]")
                                            End If
                                        End If
                                        ' AISMM-100 yakiyama 20190626 end
                                    '* 文字列処理
                                    Case 1
                                         '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                        '* AIS_MM.100 yakiyama 2019/06/12 start
                                            getValue = dispRange.Offset(0, JJ).Value
                                            '* AIS_MM.100 foastudio yakiyama 2019/06/12 start
                                            getText1Value = dispRange.Offset(0, JJ * 2 + 20).Value
                                            getText2Value = dispRange.Offset(0, JJ * 2 + 21).Value
                                            '* yakiyama 2019/06/12 start
                                            If getText1Value <> "" And getText2Value <> "" Then
                                                getTextValue = getText1Value & vbCrLf & getText2Value
                                            Else
                                            '* yakiyama 2019/06/12 end
                                                getTextValue = getText1Value & getText2Value
                                            End If
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  JJ:  " & JJ & "  rgbColor:  " & rgbColor(JJ) & "  getValue:[" & getValue & "] getTextValue:[" & getTextValue & "]")
                                            
                                            ' AISMM-100 yakiyama 20190626 start
                                            If rgbColor(JJ) = defaultColor Then
                                                nullConditionDefaultText = getTextValue
                                                If nullConditionDefaultText <> "" Then
                                                    hasNullConditionDefaultText = True
                                                End If
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                            End If
                                            ' AISMM-100 yakiyama 20190626 end
                                            
                                            getDataValue = currShape.TextFrame.Characters.text
                                            If getTextValue = "" Then
                                                getTextValue = getDataValue
                                            End If
                                            '* AIS_MM.100 foastudio yakiyama 2019/06/12 end
                                            If getValue <> "" Then
                                                arr = Split(getValue, "値")
                                                arr1 = Split(arr(0), Right(arr(0), 1))
                                                settingValue1 = arr1(0)
                                                'If settingValue1 = .Value Then
                                                '    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                '    Exit For
                                                'End If
                                                If settingValue1 = .Value Then
                                                    isSetTextValue = True
                                                    Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                    Exit For
                                                Else
                                                    ' AISMM-100 yakiyama 20190626 start
                                                    If rgbColor(JJ) = defaultColor Then
                                                        If nullConditionDefaultText <> "" Then
                                                            hasNullConditionDefaultText = True
                                                        End If
                                                    End If
                                                    ' AISMM-100 yakiyama 20190626 end
                                                    Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                End If
                                            Else
                                                ' AISMM-100 yakiyama 20190626 start
                                                If rgbColor(JJ) = defaultColor Then
                                                    If nullConditionDefaultText <> "" Then
                                                        hasNullConditionDefaultText = True
                                                    End If
                                                End If
                                                ' AISMM-100 yakiyama 20190626 end
                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                            End If
                                            '* AIS_MM.100 yakiyama 2019/06/12 end
                                        Next
                                        
                                        ' AISMM-100 yakiyama 20190626 start
                                        WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                        If hasNullConditionDefaultText = True And isSetTextValue = False Then
                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, nullConditionDefaultText)
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "] NullCondtionDefaultText")
                                        End If
                                        ' AISMM-100 yakiyama 20190626 end
                                    '* 日付処理
                                    Case 2
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                End If
                                            End If
                                        Next
                                    '* 時刻処理 2017/02/13 Add
                                    Case 3
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                End If
                                            End If
                                        Next
                                     '* 日時処理 2017/02/13 Add
                                    Case 4
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                End If
                                            End If
                                        Next
                                End Select
                            Else
                                WriteLog ("MultiStatusMonitorTemplate !IF(currRange.Offset(II, 22).Value)")
                            
                                Select Case isFormat
                                    '* 数値処理
                                    Case 0
                                        '* 書式設定
        '                                If InStr(currRange.Offset(II, 0).Value, "個数") Or _
        '                                   InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                        '-2017/05/08 No.555 Mod. 種類数の場合は書式固定
                                        'If .Value = CLng(.Value) Then  '20170307 No.473 Changed CInt to CLng.
                                        '* ISSUE_NO.670 sunyi 2018/05/15 start
                                        '* オーバーフローを修正
                                        'If .Value = CLng(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                        'If .Value = CDbl(.Value) Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                        numericFormat = "G/標準"
                                        'If .Value Mod 1 = 0 Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                        If InStr(.text, ".") <= 0 Or InStr(currRange.Offset(II, 0).Value, "種類数") Then
                                        '* ISSUE_NO.670 sunyi 2018/05/15 end
                                            '-2017/05/08 No.554 Mod. Changed # to 0.
                                            currShape.TextFrame.Characters.text = Format(.Value, "0")
                                            numericFormat = "0"
                                        Else
                                            currShape.TextFrame.Characters.text = Format(.Value, "0.00")
                                            numericFormat = "0.00"
                                        End If
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            '* foastudio sunyi 2018/10/23 start
                                            getText1Value = dispRange.Offset(0, JJ * 2 + 20).Value
                                            getText2Value = dispRange.Offset(0, JJ * 2 + 21).Value
                                            '* sunyi 2018/11/16 start
                                            If getText1Value <> "" And getText2Value <> "" Then
                                                getTextValue = getText1Value & vbCrLf & getText2Value
                                            Else
                                            '* sunyi 2018/11/16 end
                                                getTextValue = getText1Value & getText2Value
                                            End If
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  JJ:  " & JJ & "  rgbColor:  " & rgbColor(JJ) & "  getValue:[" & getValue & "] getTextValue:[" & getTextValue & "]")
                                            
                                            ' AISMM-100 yakiyama 20190626 start
                                            If rgbColor(JJ) = defaultColor Then
                                                nullConditionDefaultText = getTextValue
                                                If nullConditionDefaultText <> "" Then
                                                    hasNullConditionDefaultText = True
                                                End If
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                            End If
                                            ' AISMM-100 yakiyama 20190626 end
                                            
                                            getDataValue = currShape.TextFrame.Characters.text
                                            If getTextValue = "" Then
                                                getTextValue = getDataValue
                                            End If
                                            '* foastudio sunyi 2018/10/23 end
                                            If getValue <> "" Then
                                            '* sunyi 2018/11/16 start
                                                If InStr(getValue, "値") And InStr(getValue, "＝") = 0 Then
                                                    arr = Split(getValue, "値")
                                                    
                                                    If arr(0) <> "" And arr(1) = "" Then
                                                        arr1 = Split(arr(0), Right(arr(0), 1))
                                                        settingValue1 = arr1(0)
                                                        comboboxValue1 = Right(arr(0), 1)
                                                        
                                                        If comboboxValue1 = "＜" Then
                                                            If .Value > Val(settingValue1) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        ElseIf comboboxValue1 = "≦" Then
                                                            If .Value >= Val(settingValue1) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                        
                                                        
                                                    ElseIf arr(0) = "" And arr(1) <> "" Then
                                                        arr2 = Split(arr(1), Left(arr(1), 1))
                                                        settingValue2 = arr2(1)
                                                        comboboxValue2 = Left(arr(1), 1)
                                                        
                                                        If comboboxValue2 = "＜" Then
                                                            If .Value < Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        ElseIf comboboxValue2 = "≦" Then
                                                            If .Value <= Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                        
                                                        
                                                    ElseIf arr(0) <> "" And arr(1) <> "" Then
                                                        arr1 = Split(arr(0), Right(arr(0), 1))
                                                        arr2 = Split(arr(1), Left(arr(1), 1))
                                                        
                                                        comboboxValue1 = Right(arr(0), 1)
                                                        comboboxValue2 = Left(arr(1), 1)
                                                        settingValue1 = arr1(0)
                                                        settingValue2 = arr2(1)
                                                        
                                                        '* 条件１：<の場合、2パタン
                                                        If comboboxValue1 = "＜" And comboboxValue2 = "＜" Then
                                                            If .Value > Val(settingValue1) And .Value < Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                        If comboboxValue1 = "＜" And comboboxValue2 = "≦" Then
                                                            If .Value > Val(settingValue1) And .Value <= Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                        
                                                        '* 条件１：<=の場合、2パタン
                                                        If comboboxValue1 = "≦" And comboboxValue2 = "＜" Then
                                                            If .Value >= Val(settingValue1) And .Value < Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                        If comboboxValue1 = "≦" And comboboxValue2 = "≦" Then
                                                            If .Value >= Val(settingValue1) And .Value <= Val(settingValue2) Then
                                                                isSetTextValue = True
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                                Exit For
                                                            Else
                                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                            End If
                                                        End If
                                                    Else
                                                        ' AISMM-100 yakiyama 20190626 start
                                                        If rgbColor(JJ) = defaultColor Then
                                                            If nullConditionDefaultText <> "" Then
                                                                hasNullConditionDefaultText = True
                                                            End If
                                                        End If
                                                        ' AISMM-100 yakiyama 20190626 end
                                                    End If
                                                 Else
                                                    arr = Split(getValue, "値")
                                                    arr1 = Split(arr(0), Right(arr(0), 1))
                                                    settingValue1 = arr1(0)
                                                    comboboxValue1 = Right(arr(0), 1)
                                                    If IsNumeric(settingValue1) Then
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        'If Val(splitValue(0)) <= .Value And .Value <= Val(splitValue(1)) Then
                                                        If Val(settingValue1) = .Value Then
                                                            isSetTextValue = True
                                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                        End If
                                                    Else
                                                        If Val(getValue) = .Value Then
                                                            isSetTextValue = True
                                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                            Exit For
                                                        Else
                                                            ' AISMM-100 yakiyama 20190626 start
                                                            If rgbColor(JJ) = defaultColor Then
                                                                If nullConditionDefaultText <> "" Then
                                                                    hasNullConditionDefaultText = True
                                                                End If
                                                            End If
                                                            ' AISMM-100 yakiyama 20190626 end
                                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                        End If
                                                    End If
                                                End If
                                                '* sunyi 2018/11/16 end
                                            Else
                                                ' AISMM-100 yakiyama 20190626 start
                                                If rgbColor(JJ) = defaultColor Then
                                                    If nullConditionDefaultText <> "" Then
                                                        hasNullConditionDefaultText = True
                                                    End If
                                                End If
                                                ' AISMM-100 yakiyama 20190626 end
                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                            End If
                                        Next
                                        
                                        ' AISMM-100 yakiyama 20190626 start
                                        WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                        If isSetTextValue = False Then
                                            If hasNullConditionDefaultText = True Then
                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, nullConditionDefaultText)
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "] NullConditionDefaultText")
                                            Else
                                                Call ChangeTextBoxTextNumberFormat(statusWorksheet, currShape.Name, currRange.Offset(II, 1), numericFormat)
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  ChangeTextBoxTextNumberFormat:  " & numericFormat & "  .Value:[" & .Value & "]")
                                            End If
                                        End If
                                        ' AISMM-100 yakiyama 20190626 end
                                    '* 文字列処理
                                    Case 1
'                                        '* ISSUE_NO.759 Sunyi 2018/06/25 Start
'                                        '* 空欄の場合、””に表示する
'    '                                    '* 書式設定
'    '                                    currShape.TextFrame.Characters.text = Format(.Value)
'                                        If .Value = 0 Or .Value = "" Then
'                                            .Value = ""
'                                        Else
'                                            currShape.TextFrame.Characters.text = Format(.Value)
'                                        End If
'                                        '* ISSUE_NO.759 Sunyi 2018/06/25 End
                                        'ISSUE_NO.802 sunyi 2018/07/26 Start
                                        '文字列の場合で、空をそのまま出力する
                                        currShape.TextFrame.Characters.text = Format(.Value)
                                        'ISSUE_NO.802 sunyi 2018/07/26 End
                                         '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                        '* AIS_MM.100 yakiyama 2019/06/12 start
'                                            getValue = dispRange.Offset(0, JJ).Value
'                                            If getValue <> "" Then
'                                                If getValue = .Value Then
'                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
'                                                    Exit For
'                                                Else
'                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
'                                                End If
'                                            End If
                                            getValue = dispRange.Offset(0, JJ).Value
                                            '* AIS_MM.100 foastudio yakiyama 2019/06/12 start
                                            getText1Value = dispRange.Offset(0, JJ * 2 + 20).Value
                                            getText2Value = dispRange.Offset(0, JJ * 2 + 21).Value
                                            '* yakiyama 2019/06/12 start
                                            If getText1Value <> "" And getText2Value <> "" Then
                                                getTextValue = getText1Value & vbCrLf & getText2Value
                                            Else
                                            '* yakiyama 2019/06/12 end
                                                getTextValue = getText1Value & getText2Value
                                            End If
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  JJ:  " & JJ & "  rgbColor:  " & rgbColor(JJ) & "  getValue:[" & getValue & "] getTextValue:[" & getTextValue & "]")
                                            
                                            ' AISMM-100 yakiyama 20190626 start
                                            If rgbColor(JJ) = defaultColor Then
                                                nullConditionDefaultText = getTextValue
                                                If nullConditionDefaultText <> "" Then
                                                    hasNullConditionDefaultText = True
                                                End If
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                            End If
                                            ' AISMM-100 yakiyama 20190626 end
                                            
                                            getDataValue = currShape.TextFrame.Characters.text
                                            If getTextValue = "" Then
                                                getTextValue = getDataValue
                                            End If
                                            '* AIS_MM.100 foastudio yakiyama 2019/06/12 end
                                            If getValue <> "" Then
                                                arr = Split(getValue, "値")
                                                arr1 = Split(arr(0), Right(arr(0), 1))
                                                settingValue1 = arr1(0)
                                                'If settingValue1 = .Value Then
                                                '    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                '    Exit For
                                                'End If
                                                If settingValue1 = .Value Then
                                                    isSetTextValue = True
                                                    Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, rgbColor(JJ), getTextValue)
                                                    Exit For
                                                Else
                                                    ' AISMM-100 yakiyama 20190626 start
                                                    If rgbColor(JJ) = defaultColor Then
                                                        If nullConditionDefaultText <> "" Then
                                                            hasNullConditionDefaultText = True
                                                        End If
                                                    End If
                                                    ' AISMM-100 yakiyama 20190626 end
                                                    Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                                End If
                                            Else
                                                ' AISMM-100 yakiyama 20190626 start
                                                If rgbColor(JJ) = defaultColor Then
                                                    If nullConditionDefaultText <> "" Then
                                                        hasNullConditionDefaultText = True
                                                    End If
                                                End If
                                                ' AISMM-100 yakiyama 20190626 end
                                                Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                            End If
                                        '* AIS_MM.100 yakiyama 2019/06/12 end
                                        Next
                                        
                                        ' AISMM-100 yakiyama 20190626 start
                                        WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                        If hasNullConditionDefaultText = True And isSetTextValue = False Then
                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, nullConditionDefaultText)
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "] NullConditionDefaultText")
                                        End If
                                        ' AISMM-100 yakiyama 20190626 end
                                    '* 日付処理
                                    Case 2
                                        '* 書式設定
                                        '20161104 追加
                                        If .Value = 0 Or .Value = "" Then
                                            currShape.TextFrame.Characters.text = ""
                                        Else
                                            '2017/09/26 No.539 Mod. Changed "yyyy/MM/dd hh:mm:ss" to "yyyy/MM/dd".
                                            currShape.TextFrame.Characters.text = Format(.Value, "yyyy/MM/dd")
                                            '* 色の処理
                                            For JJ = 0 To UBound(rgbColor) - 1
                                                getValue = dispRange.Offset(0, JJ).Value
                                                If getValue <> "" Then
                                                    If InStr(getValue, ",") Then
                                                        splitValue = Split(getValue, ",")
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    Else
                                                        If getValue = .Value Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If
                                    '* 時刻処理 2017/02/13 Add
                                    Case 3
                                        '* 書式設定
                                        '20161104 追加
                                        If .Value = 0 Or .Value = "" Then
                                            currShape.TextFrame.Characters.text = ""
                                        Else
                                            currShape.TextFrame.Characters.text = Format(.Value, "hh:mm:ss")
                                            '* 色の処理
                                            For JJ = 0 To UBound(rgbColor) - 1
                                                getValue = dispRange.Offset(0, JJ).Value
                                                If getValue <> "" Then
                                                    If InStr(getValue, ",") Then
                                                        splitValue = Split(getValue, ",")
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    Else
                                                        If getValue = .Value Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If
                                     '* 日時処理 2017/02/13 Add
                                    Case 4
                                        '* 書式設定
                                        '20161104 追加
                                        If .Value = 0 Or .Value = "" Then
                                            currShape.TextFrame.Characters.text = ""
                                        Else
                                            currShape.TextFrame.Characters.text = Format(.Value, "yyyy/MM/dd hh:mm:ss")
                                            '* 色の処理
                                            For JJ = 0 To UBound(rgbColor) - 1
                                                getValue = dispRange.Offset(0, JJ).Value
                                                If getValue <> "" Then
                                                    If InStr(getValue, ",") Then
                                                        splitValue = Split(getValue, ",")
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    Else
                                                        If getValue = .Value Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If
                                End Select
                            End If
                        End If
                
                    End With    ' currRange.Offset(II, 1)
                
                ' 2019/07/09 yakiyama AIS_MM.100-0 start
                Else
                    WriteLog ("MultiStatusMonitorTemplate.xlsm  II:[" & II & "]  ![If currRange.Offset(II, 0).Value <> '']")
                    
                    Set dispRange = srchRange.Find(What:="表示色−白", LookAt:=xlWhole)
                    Set dispRange = .Cells(currRange.Offset(II, 0).Row, dispRange.Column)
                
                    With currRange.Offset(II, 1)
                        .NumberFormatLocal = "G/標準"
                        
                        isFormat = currRange.Offset(II, 2).Value
                        
                        '* テキストボックスへ数式の値をセットする
                        shapeName = currRange.Offset(II, -1).Value '図形名の取込　value_shape_01
                        
                        '2017/01/13 追加
                        '該当Shape(ﾃｷｽﾄBox)の有無チェック
                        flagExist = False
                        For Each shp In statusWorksheet.Shapes
                            If shp.Name = shapeName Then
                                flagExist = True
                                Exit For
                            End If
                        Next shp
                        WriteLog ("MultiStatusMonitorTemplate.xlsm  II:[" & II & "]  Shape.Name:[" & shapeName & "]  isFormat:[" & isFormat & "]  flagExist:[" & flagExist & "]  .Value:[" & .Value & "]  .Value2:[" & .Value2 & "]  .WrapText:[" & .WrapText & "]")
                        
                        '2017/01/13
                        '該当のShapeがあれば、Shapeに値を設定
                        If flagExist = True Then
                            Set currShape = statusWorksheet.Shapes(shapeName)
                            On Error Resume Next
                            'Call ChangeFontColor(statusWorksheet, currShape.Name, RGB(0, 0, 0))
                            
                            '*AISMM-81 sunyi 20190109 start
                            '条件なしの場合、白にする
                            defaultColor = 16777215
                            '*AISMM-81 sunyi 20190109 end
                            ' AISMM-100 yakiyama 20190626 start
                            hasNullConditionDefaultText = False
                            isSetTextValue = False
                            nullConditionDefaultText = ""
                            ' AISMM-100 yakiyama 20190626 end
                            
                            If currRange.Offset(II, 22).Value Then
                                currShape.TextFrame.Characters.text = currRange.Offset(II, 23).Value
                                WriteLog ("MultiStatusMonitorTemplate IF(currRange.Offset(II, 22).Value)")
                                
                                Select Case isFormat
                                    '* 数値処理
                                    Case 0
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            '* foastudio sunyi 2018/10/23 start
                                            getText1Value = dispRange.Offset(0, JJ * 2 + 20).Value
                                            getText2Value = dispRange.Offset(0, JJ * 2 + 21).Value
                                            '* sunyi 2018/11/16 start
                                            If getText1Value <> "" And getText2Value <> "" Then
                                                getTextValue = getText1Value & vbCrLf & getText2Value
                                            Else
                                            '* sunyi 2018/11/16 end
                                                getTextValue = getText1Value & getText2Value
                                            End If
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  JJ:  " & JJ & "  rgbColor:  " & rgbColor(JJ) & "  getTextValue:[" & getTextValue & "]")
                                            
                                            ' AISMM-100 yakiyama 20190626 start
                                            If rgbColor(JJ) = defaultColor Then
                                                nullConditionDefaultText = getTextValue
                                                If nullConditionDefaultText <> "" Then
                                                    hasNullConditionDefaultText = True
                                                End If
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                            End If
                                            ' AISMM-100 yakiyama 20190626 end
                                            
                                            getDataValue = currShape.TextFrame.Characters.text
                                            If getTextValue = "" Then
                                                getTextValue = getDataValue
                                            End If
                                            '* foastudio sunyi 2018/10/23 end
                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                        Next
                                        
                                        ' AISMM-100 yakiyama 20190626 start
                                        WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                        If hasNullConditionDefaultText = True And isSetTextValue = False Then
                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, nullConditionDefaultText)
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "] NullConditionDefaultText")
                                        End If
                                        ' AISMM-100 yakiyama 20190626 end
                                    '* 文字列処理
                                    Case 1
                                         '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                        '* AIS_MM.100 yakiyama 2019/06/12 start
                                            '* AIS_MM.100 foastudio yakiyama 2019/06/12 start
                                            getText1Value = dispRange.Offset(0, JJ * 2 + 20).Value
                                            getText2Value = dispRange.Offset(0, JJ * 2 + 21).Value
                                            '* yakiyama 2019/06/12 start
                                            If getText1Value <> "" And getText2Value <> "" Then
                                                getTextValue = getText1Value & vbCrLf & getText2Value
                                            Else
                                            '* yakiyama 2019/06/12 end
                                                getTextValue = getText1Value & getText2Value
                                            End If
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  JJ:  " & JJ & "  rgbColor:  " & rgbColor(JJ) & "  getValue:[" & getValue & "] getTextValue:[" & getTextValue & "]")
                                            
                                            ' AISMM-100 yakiyama 20190626 start
                                            If rgbColor(JJ) = defaultColor Then
                                                nullConditionDefaultText = getTextValue
                                                If nullConditionDefaultText <> "" Then
                                                    hasNullConditionDefaultText = True
                                                End If
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                            End If
                                            ' AISMM-100 yakiyama 20190626 end
                                            
                                            getDataValue = currShape.TextFrame.Characters.text
                                            If getTextValue = "" Then
                                                getTextValue = getDataValue
                                            End If
                                            '* AIS_MM.100 foastudio yakiyama 2019/06/12 end
                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                        Next
                                        
                                        ' AISMM-100 yakiyama 20190626 start
                                        WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                        If hasNullConditionDefaultText = True And isSetTextValue = False Then
                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, nullConditionDefaultText)
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "] NullCondtionDefaultText")
                                        End If
                                        ' AISMM-100 yakiyama 20190626 end
                                    '* 日付処理
                                    Case 2
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                End If
                                            End If
                                        Next
                                    '* 時刻処理 2017/02/13 Add
                                    Case 3
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                End If
                                            End If
                                        Next
                                     '* 日時処理 2017/02/13 Add
                                    Case 4
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            getValue = dispRange.Offset(0, JJ).Value
                                            If getValue <> "" Then
                                                If InStr(getValue, ",") Then
                                                    splitValue = Split(getValue, ",")
                                                    '2017/09/26 No.538 Mod. Changed < to <=.
                                                    If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                Else
                                                    If getValue = .Value Then
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                        Exit For
                                                    Else
                                                        Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                    End If
                                                End If
                                            End If
                                        Next
                                End Select
                            Else
                                WriteLog ("MultiStatusMonitorTemplate !IF(currRange.Offset(II, 22).Value)")
                            
                                Select Case isFormat
                                    '* 数値処理
                                    Case 0
                                        '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                            '* foastudio sunyi 2018/10/23 start
                                            getText1Value = dispRange.Offset(0, JJ * 2 + 20).Value
                                            getText2Value = dispRange.Offset(0, JJ * 2 + 21).Value
                                            '* sunyi 2018/11/16 start
                                            If getText1Value <> "" And getText2Value <> "" Then
                                                getTextValue = getText1Value & vbCrLf & getText2Value
                                            Else
                                            '* sunyi 2018/11/16 end
                                                getTextValue = getText1Value & getText2Value
                                            End If
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  JJ:  " & JJ & "  rgbColor:  " & rgbColor(JJ) & "  getValue:[" & getValue & "] getTextValue:[" & getTextValue & "]")
                                            
                                            ' AISMM-100 yakiyama 20190626 start
                                            If rgbColor(JJ) = defaultColor Then
                                                nullConditionDefaultText = getTextValue
                                                If nullConditionDefaultText <> "" Then
                                                    hasNullConditionDefaultText = True
                                                End If
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                            End If
                                            ' AISMM-100 yakiyama 20190626 end
                                            
                                            getDataValue = currShape.TextFrame.Characters.text
                                            If getTextValue = "" Then
                                                getTextValue = getDataValue
                                            End If
                                            '* foastudio sunyi 2018/10/23 end
                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                        Next
                                        
                                        ' AISMM-100 yakiyama 20190626 start
                                        WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                        If hasNullConditionDefaultText = True And isSetTextValue = False Then
                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, nullConditionDefaultText)
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "] NullConditionDefaultText")
                                        End If
                                        ' AISMM-100 yakiyama 20190626 end
                                    '* 文字列処理
                                    Case 1
'                                        '* ISSUE_NO.759 Sunyi 2018/06/25 Start
'                                        '* 空欄の場合、””に表示する
'    '                                    '* 書式設定
'    '                                    currShape.TextFrame.Characters.text = Format(.Value)
'                                        If .Value = 0 Or .Value = "" Then
'                                            .Value = ""
'                                        Else
'                                            currShape.TextFrame.Characters.text = Format(.Value)
'                                        End If
'                                        '* ISSUE_NO.759 Sunyi 2018/06/25 End
                                        'ISSUE_NO.802 sunyi 2018/07/26 Start
                                        '文字列の場合で、空をそのまま出力する
                                        currShape.TextFrame.Characters.text = Format(.Value)
                                        'ISSUE_NO.802 sunyi 2018/07/26 End
                                         '* 色の処理
                                        For JJ = 0 To UBound(rgbColor) - 1
                                        '* AIS_MM.100 yakiyama 2019/06/12 start
'                                            getValue = dispRange.Offset(0, JJ).Value
'                                            If getValue <> "" Then
'                                                If getValue = .Value Then
'                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
'                                                    Exit For
'                                                Else
'                                                    Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(0))
'                                                End If
'                                            End If
                                            '* AIS_MM.100 foastudio yakiyama 2019/06/12 start
                                            getText1Value = dispRange.Offset(0, JJ * 2 + 20).Value
                                            getText2Value = dispRange.Offset(0, JJ * 2 + 21).Value
                                            '* yakiyama 2019/06/12 start
                                            If getText1Value <> "" And getText2Value <> "" Then
                                                getTextValue = getText1Value & vbCrLf & getText2Value
                                            Else
                                            '* yakiyama 2019/06/12 end
                                                getTextValue = getText1Value & getText2Value
                                            End If
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  JJ:  " & JJ & "  rgbColor:  " & rgbColor(JJ) & "  getValue:[" & getValue & "] getTextValue:[" & getTextValue & "]")
                                            
                                            ' AISMM-100 yakiyama 20190626 start
                                            If rgbColor(JJ) = defaultColor Then
                                                nullConditionDefaultText = getTextValue
                                                If nullConditionDefaultText <> "" Then
                                                    hasNullConditionDefaultText = True
                                                End If
                                                WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                            End If
                                            ' AISMM-100 yakiyama 20190626 end
                                            
                                            getDataValue = currShape.TextFrame.Characters.text
                                            If getTextValue = "" Then
                                                getTextValue = getDataValue
                                            End If
                                            '* AIS_MM.100 foastudio yakiyama 2019/06/12 end
                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, getDataValue)
                                        '* AIS_MM.100 yakiyama 2019/06/12 end
                                        Next
                                        
                                        ' AISMM-100 yakiyama 20190626 start
                                        WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "]")
                                        If hasNullConditionDefaultText = True And isSetTextValue = False Then
                                            Call ChangeBackColorAndTextTB(statusWorksheet, currShape.Name, defaultColor, nullConditionDefaultText)
                                            WriteLog ("MultiStatusMonitor Shape.Name:  " & shapeName & "  hasNullConditionDefaultText:  " & hasNullConditionDefaultText & "  isSetTextValue:  " & isSetTextValue & "  nullConditionDefaultText:[" & nullConditionDefaultText & "] NullConditionDefaultText")
                                        End If
                                        ' AISMM-100 yakiyama 20190626 end
                                    '* 日付処理
                                    Case 2
                                        '* 書式設定
                                        '20161104 追加
                                        If .Value = 0 Or .Value = "" Then
                                            currShape.TextFrame.Characters.text = ""
                                        Else
                                            '2017/09/26 No.539 Mod. Changed "yyyy/MM/dd hh:mm:ss" to "yyyy/MM/dd".
                                            currShape.TextFrame.Characters.text = Format(.Value, "yyyy/MM/dd")
                                            '* 色の処理
                                            For JJ = 0 To UBound(rgbColor) - 1
                                                getValue = dispRange.Offset(0, JJ).Value
                                                If getValue <> "" Then
                                                    If InStr(getValue, ",") Then
                                                        splitValue = Split(getValue, ",")
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    Else
                                                        If getValue = .Value Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If
                                    '* 時刻処理 2017/02/13 Add
                                    Case 3
                                        '* 書式設定
                                        '20161104 追加
                                        If .Value = 0 Or .Value = "" Then
                                            currShape.TextFrame.Characters.text = ""
                                        Else
                                            currShape.TextFrame.Characters.text = Format(.Value, "hh:mm:ss")
                                            '* 色の処理
                                            For JJ = 0 To UBound(rgbColor) - 1
                                                getValue = dispRange.Offset(0, JJ).Value
                                                If getValue <> "" Then
                                                    If InStr(getValue, ",") Then
                                                        splitValue = Split(getValue, ",")
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    Else
                                                        If getValue = .Value Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If
                                     '* 日時処理 2017/02/13 Add
                                    Case 4
                                        '* 書式設定
                                        '20161104 追加
                                        If .Value = 0 Or .Value = "" Then
                                            currShape.TextFrame.Characters.text = ""
                                        Else
                                            currShape.TextFrame.Characters.text = Format(.Value, "yyyy/MM/dd hh:mm:ss")
                                            '* 色の処理
                                            For JJ = 0 To UBound(rgbColor) - 1
                                                getValue = dispRange.Offset(0, JJ).Value
                                                If getValue <> "" Then
                                                    If InStr(getValue, ",") Then
                                                        splitValue = Split(getValue, ",")
                                                        '2017/09/26 No.538 Mod. Changed < to <=.
                                                        If splitValue(0) <= .Value And .Value <= splitValue(1) Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    Else
                                                        If getValue = .Value Then
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, rgbColor(JJ))
                                                            Exit For
                                                        Else
                                                            Call ChangeBackColorTB(statusWorksheet, currShape.Name, defaultColor)
                                                        End If
                                                    End If
                                                End If
                                            Next
                                        End If
                                End Select
                            End If
                        End If
                
                    End With    ' currRange.Offset(II, 1)
                
                
                
                ' 2019/07/09 yakiyama AIS_MM.100-0 end
                End If  ' 図形情報があれば
                
            Next    ' 全図形
            
        End If  ' 「定義内容」項目があれば
        
    End With    ' dispDefWorksheet
    
    '**************************************************************************
    '* paramシートにある表示条件を取得し、ステータスシート上の背景色を更新する
    '**************************************************************************
    With paramWorksheet
    
        '* 表示範囲が設定されていればその範囲で表示する
        dispAddress = GetDisplayAddress(statusWorksheet)
        
        If dispAddress <> "" Then
        
            Set dispRange = statusWorksheet.Range(dispAddress)

            Set srchRange = .Range("A:A")
            Set currRange = srchRange.Find(What:="表示色対象", LookAt:=xlWhole)
            Set nowColorRange = srchRange.Find(What:="現在表示色", LookAt:=xlWhole)
            
        
            If Not currRange Is Nothing Then
            
                '* 表示色対象エレメント名を取得
                chgElement = currRange.Offset(0, 1).Value
                
                '---2016/12/19 Add No.386 [実行時エラー９対策] 表示色対象エレメントが設定されているときのみ処理 IF文追加 -------------Start
                If chgElement <> "" Then
                chgElement = Replace(chgElement, """", "")
                'マルチモニター No.58 sunyi 2018/11/20
                ' "・"⇒"｜"
                'splitValue = Split(chgElement, "・")    ' 「部品1生産実績」「部品セリアルＮｏ」
                splitValue = Split(chgElement, "｜")    ' 「部品1生産実績」「部品セリアルＮｏ」
                
                    ''''splitValue = Split(chgElement, "_")    ' 「部品1生産実績」「部品セリアルＮｏ」
                    
                    Set currWorksheet = Workbooks(thisBookName).Worksheets(splitValue(0))
                    With currWorksheet
                        Set srchRange = .Range("1:1")
                         ' TODO dn isGripType 処理　2018/09/07
                        ' Wang Issue NO.727 2018/06/15 Start
                        If IsGripType Then
                            Set startRange = srchRange.Find(What:=splitValue(2), LookAt:=xlWhole)
                        Else
                            Set startRange = srchRange.Find(What:=splitValue(1), LookAt:=xlWhole)
                        End If
                        ' Wang Issue NO.727 2018/06/15 End
                        
                        If Not startRange Is Nothing Then
                            Set workRange = startRange.Offset(1, 0)             '* 着目エレメントの最新データ範囲
                            '* 値の取得
                            currValue = workRange.Offset(0, 0).Value
                            
                            '* デフォルトカラーは緑固定
                            '2017/09/27 No.542 Del. Do not set default back color.
                            Call ChangeBackColorStatus(dispRange, RGB(255, 255, 255))
                            'nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(3))
                            
                            For II = 1 To 4
                                '''''''''''''''''''
                                '型の決定(10:数字 11:数字(カンマ区切りの二つの数）,20:文字列)
                                '''''''''''''''''''
                                If IsNumeric(currRange.Offset(II, 1).Value) Then
                                    
                                    splitValue = Split(currRange.Offset(II, 1).Value, ",")
                                    If UBound(splitValue) = 0 Then
                                        isFormat = 10
                                    ElseIf UBound(splitValue) = 1 Then
                                        isFormat = 11
                                    Else
                                        isFormat = 20
                                    End If
                                Else
                                    isFormat = 20
                                End If
                            
                                If isFormat = 10 Then
                                    getValue = CDbl(currRange.Offset(II, 1).Value)
                                    
                                    If currValue = getValue Then
                                    
                                        Call ChangeBackColorStatus(dispRange, rgbColorBack(II - 1))
                                        nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(II - 1))
                                        Exit For
                                    End If
                                ElseIf isFormat = 11 Then
                                    splitValue = Split(currRange.Offset(II, 1).Value, ",")
                                    If UBound(splitValue) = 1 Then
                                        getValue = CDbl(splitValue(0))
                                        getValue1 = CDbl(splitValue(1))
                                        If currValue >= getValue And currValue <= getValue1 Then
                                            Call ChangeBackColorStatus(dispRange, rgbColorBack(II - 1))
                                            nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(II - 1))
                                            Exit For
                                        End If
                                    End If
                                    
                                    
                                ElseIf isFormat = 20 Then
                                    If currValue = currRange.Offset(II, 1).Value Then
                                    
                                        Call ChangeBackColorStatus(dispRange, rgbColorBack(II - 1))
                                        nowColorRange.Offset(0, 1).Value = GetRGBHexString(rgbColorBack(II - 1))
                                        Exit For
                                    End If
                                End If
                                
                            Next
                            
'                            '現在運転状態表示 20160619 B21固定   Chr(13) & Chr(10) &
                            'マルチモニター No.58 sunyi 2018/11/20
                            ' "・"⇒"｜"
                            'currValue = Replace(currValue, "・", Chr(13) & Chr(10) & "")
                            currValue = Replace(currValue, "｜", Chr(13) & Chr(10) & "")
                            currValue = Replace(currValue, "中", Chr(13) & Chr(10) & "中")
                            '* ISSUE_NO.713 sunyi 2018/06/05 start
                            ' param取得位置を修正
                            'Workbooks(thisBookName).Worksheets(ParamSheetName).Range("B21").Value = currValue
                            Workbooks(thisBookName).Worksheets(ParamSheetName).Range("B22").Value = currValue
                            '* ISSUE_NO.713 sunyi 2018/06/05 end
                            
                        End If
                    End With
                End If
                '---2016/12/19 Add No.386 [実行時エラー９対策] 表示色対象エレメントが設定されているときのみ処理 IF文追加 -------------End
            End If

        End If
    
    End With
    Call DeleteProduceData("1", False)
    Call WriteValueProduceData
End Sub

Private Function GetRGBHexString(lngRGB As Long) As String
    Dim strR As String
    Dim strG As String
    Dim strB As String
    Dim strHex As String

    '長整数の色表記をR・G・Bに分解
    strR = Right$("00" & Hex$((lngRGB And "&H" & "0000FF")), 2)
    strG = Right$("00" & Hex$((lngRGB And "&H" & "00FF00") / 256), 2)
    strB = Right$("00" & Hex$((lngRGB And "&H" & "FF0000") / 256 ^ 2), 2)
    
    'R・G・Bそれぞれの文字列を結合
    strHex = "#" & strR & strG & strB
    
    '結果をtxt16進表記に代入
    GetRGBHexString = strHex

End Function

'*****************
'* ファイル存在チェック
'*****************
Function IsFileExists(ByVal strFileName As String) As Boolean
    If Dir(strFileName, 16) <> Empty Then
        IsFileExists = True
    Else
        IsFileExists = False
    End If
End Function

'*************************************************
'* 既に開いたExcelを前面に表示
'*************************************************
Private Function ActivateWorkbookByPath(linkStr As String) As Boolean
'* foastudio AisAddin sunyi 2018/11/02
    If excelUtilObj Is Nothing Then
        Set excelUtilObj = CreateObject("FoaCoreCom.common.util.ExcelUtil")
    End If
    ActivateWorkbookByPath = excelUtilObj.ActivateWorkbookByPath(linkStr)
End Function

'*************************************************
'* 既に開いたExcelを前面に表示
'*************************************************
Private Function ActivateWorkbookByApp(xlapp As Excel.Application) As Boolean
    If excelUtilObj Is Nothing Then
        Set excelUtilObj = CreateObject("FoaCoreCom.common.util.ExcelUtil")
    End If
    ActivateWorkbookByApp = excelUtilObj.ActivateWorkbookByApp(xlapp)
End Function

'*************************************************
'* テキストボックスクリック時に呼び出されるマクロ
'*************************************************
Public Sub ConditionOpen()
    Dim currShape           As Shape
    Dim dispDefWorksheet    As Worksheet
    Dim paramWorkhseet      As Worksheet
    Dim condParamsheet      As Worksheet
    Dim linkParamsheet      As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim rowRange            As Range
    Dim colRange            As Range
    Dim calcStr             As String
    Dim calcStr2             As String
    Dim isFormat            As Integer
    Dim isLinkChk           As Boolean
    Dim linkStr             As String
    Dim isLinkFormat        As Integer
    Dim isElementChk        As Boolean
    Dim elementStr          As String
    Dim findNo              As Integer
    Dim findNo1             As Integer
    Dim intstr              As Variant
    Dim intstr2              As Variant
    Dim sSwitch             As String
    Dim sAutoUpdate         As String
    Dim II                  As Integer
    Dim J                   As Integer
    Dim iEndRow             As Integer
    Dim xlapp               As Excel.Application
    Dim fileName            As String
    
    '* 選択図形を取得
    Set currShape = ActiveSheet.Shapes(Application.Caller)
    
    Call SetFilenameToVariable

    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet

        '* 演算式および表示書式を取得
        Set srchRange = .Range("A:A")
        Set rowRange = srchRange.Find(What:=currShape.Name, LookAt:=xlWhole)
        If Not rowRange Is Nothing Then
            calcStr = rowRange.Offset(0, 1).Value
            isFormat = rowRange.Offset(0, 3).Value
            '* リンク情報を取得する
            isLinkChk = rowRange.Offset(0, 20).Value
            isLinkFormat = rowRange.Offset(0, 21).Value
            linkStr = rowRange.Offset(0, 22).Value
            isElementChk = rowRange.Offset(0, 23).Value
            elementStr = rowRange.Offset(0, 24).Value
            
        Else
            Exit Sub
        End If

        '* 表示色条件情報範囲を取得
        Set srchRange = .Range("1:1")
        Set colRange = srchRange.Find(What:="表示色−白", LookAt:=xlWhole)
        If Not colRange Is Nothing Then
            Set currRange = .Cells(rowRange.Row, colRange.Column)
        Else
            Exit Sub
        End If

    End With
    
    Set paramWorkhseet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With paramWorkhseet
        '* 編集モードを取得する
        Set srchRange = .Range("A:A")
        Set rowRange = srchRange.Find(What:="SwitchLink", LookAt:=xlWhole)
        If Not rowRange Is Nothing Then
            sSwitch = rowRange.Offset(0, 1).Value
        End If
        
        '* 自動更新を取得する
        Set rowRange = srchRange.Find(What:="自動更新", LookAt:=xlWhole)
        If Not rowRange Is Nothing Then
            sAutoUpdate = rowRange.Offset(0, 1).Value
        End If
        
    End With
    
    '*AISMM No.86 sunyi 2018/12/18 start
    '*仕様変更
    If sAutoUpdate = "ON" And sSwitch = "ON" Then
        MsgBox ("動作中は編集できません。" & vbCrLf & _
                "停止してから、編集してください。")
        Exit Sub
    End If
    '*AISMM No.86 sunyi 2018/12/18 end
    
    '* リンクがなし、非編集モードの場合、処理なし
    If Not isLinkChk And sSwitch = "OFF" Then
        Exit Sub
    End If
    
    '*AISMM No.86 sunyi 2018/12/18 start
    '*仕様変更
''    * 自動更新の場合、自動更新に停止する
'    If sAutoUpdate = "ON" Then
'        Call ChangeStartStopBT
'    End If
    '*AISMM No.86 sunyi 2018/12/18 end

    '* リンクがあり、非編集モードの場合、リンク先に遷移先する
    If isLinkChk And sSwitch = "OFF" Then
    
        'コマンドリストを取得する
        Set linkParamsheet = Workbooks(thisBookName).Worksheets(LinkParamSheetName)
        With linkParamsheet
            iEndRow = .UsedRange.Rows.Count
        
            For II = 1 To iEndRow
                
                If .Range("A" & II).Value = currShape.Name Then

                    'リンク先を取得する
                    linkStr = .Range("A" & II).Offset(0, 3)
                    If .Range("A" & II).Offset(0, 1) = 1 Then
                        '既に開いたExcelを前面に表示する
                        linkStr = Replace(linkStr, """", "")
                        fileName = Mid(linkStr, InStrRev(linkStr, "\"), Len(linkStr))
                        fileName = ThisWorkbook.path & fileName
                        
                        If IsFileExists(fileName) = False Then
                            If ActivateWorkbookByPath(linkStr) Then
                                Exit Sub
                            End If
                        Else
                            If ActivateWorkbookByPath(fileName) Then
                                Exit Sub
                            End If
                        End If

On Error GoTo errlable
                        'エクセルを開く
                        Set xlapp = New Excel.Application
                        xlapp.Visible = True
                        
                        If IsFileExists(fileName) = False Then
                            xlapp.Workbooks.Open (linkStr)
                        Else
                            xlapp.Workbooks.Open (fileName)
                        End If
                        
                        Call ActivateWorkbookByApp(xlapp)
                        Set xlapp = Nothing
                        
                        Err.Number = 0
errlable:
                        If Err.Number <> 0 Then
                            MsgBox "申し訳ございません。" & fileName & "が見つかりません。名前が変更されたか、移動や削除が行われた可能性があります。" & vbCrLf & "リンク先を確認し、再度リンク設定をしてください｡ "
                        End If
                    ElseIf .Range("A" & II).Offset(0, 1) = 2 Then
                    
                        'URLを開く
                        ActiveWorkbook.FollowHyperlink Address:=linkStr, NewWindow:=True
                    ElseIf .Range("A" & II).Offset(0, 1) = 3 Then
'Wang 外部取込みアプリケーションを起動 2018/04/27 Start
'外部取込みアプリケーションを起動し、異常終了の場合は後続処理を中断する
'                        Dim specifyPath As String
'
'                        specifyPath = CreateCtmRefineParamFile
'                        If specifyPath <> "" Then
'                            linkStr = linkStr & Chr(32) & "-specify" & Chr(32) & specifyPath
'                        End If
'
'                        'EXEを開く
'                        Shell linkStr, vbNormalFocus
                        
                        Dim parts() As String
                        Dim commandName As String
                        parts = Split(Application.WorksheetFunction.Trim(Replace(linkStr, Chr(9), " ")), " ")
                        commandName = Replace(GetFileName(parts(0)), """", "")
                        
                        If StrComp(commandName, COMMAND_CTMRefineUI, vbTextCompare) = 0 Then
                            Dim specifyPath As String
                            specifyPath = CreateCtmRefineParamFile
                            If specifyPath <> "" Then
                                linkStr = linkStr & Chr(32) & "-specify" & Chr(32) & specifyPath
                            End If
                            Call shell(linkStr, vbNormalFocus)
                        ElseIf StrComp(commandName, COMMAND_TriggerExData, vbTextCompare) = 0 Then
                            If ExecuteCommand(linkStr) <> 0 Then
                                Exit For
                            End If
                        Else
                            Call ExecuteCommand(linkStr)
                        End If
'Wang 外部取込みアプリケーションを起動 2018/04/27 End
                    End If
                End If
            Next
        End With
        Exit Sub
    End If
    
    ' Processing On Server dn 2018/09/07 start
    ' Wang Issue NO.727 2018/06/15 Start
'    If IsGripType() Then
'        Load GripActionPaneForm
'        With GripActionPaneForm
'
'            .Caption = "表示テキストボックス編集"
'            Call .ShowMissionName
'
'            '*ISSUE_NO.671 sunyi 2018/05/29 start
'            '*仕様変更
'    '        Dim currWorksheet   As Worksheet
'    '        '*
'    '        Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
'    '        Set srchRange = currWorksheet.Cells.Find(What:="操作バルキ表示Flag", LookAt:=xlWhole)
'    '
'    '        srchRange.Offset(0, 1).Value = "True"
'
'            If .CB_CalcFormat.text = "" Then
'                '* 「エレメント追加」ボタンを非表示
'                .BT_add.Visible = False
'
'                '* 「エレメント削除」ボタンを非表示
'                .BT_rowdelete.Visible = False
'
'                '* 「条件更新」ボタンを非表示
'                .BT_ConditionUpdate.Visible = False
'
'                '* 「条件」テキストボックスを非表示
'                .TB_CalcCondition.Visible = False
'
'                '* 「条件」ラベルを非表示
'                .Label23.Visible = False
'
'                '* 「エレメント」リストビューを非表示
'                .LV_ElementView.Visible = False
'
'                '* リストビューのへーだー「エレメント」ラベルを非表示
'                .Label25.Visible = False
'
'                '* リストビューのへーだー「条件」ラベルを非表示
'                .Label24.Visible = False
'            End If
'            '*ISSUE_NO.671 sunyi 2018/05/29 end
'
'            '* 演算式
'            .TB_ItemOperation.text = calcStr
'
'            '* 演算形式
'            If InStr(calcStr, "+") <> 0 Then
'                intstr = Split(calcStr, "+")
'
'                If InStr(intstr(0), ",") <> 0 Then
'                    findNo = InStr(intstr(0), ",") + 1
'                    findNo1 = InStr(intstr(0), "[")
'                    If findNo1 <> 0 Then
'                        .CB_CalcFormat.text = Mid(Trim(intstr(0)), findNo, findNo1 - findNo)
'                    Else
'                        .CB_CalcFormat.text = Mid(Trim(intstr(0)), findNo, Len(Trim(intstr(0))) - findNo)
'                    End If
'                End If
'            Else
'                If InStr(calcStr, ",") <> 0 Then
'                    findNo = InStr(calcStr, ",") + 1
'                    findNo1 = InStr(calcStr, "[")
'
'                    If findNo1 <> 0 Then
'                        .CB_CalcFormat.text = Mid(Trim(calcStr), findNo, findNo1 - findNo)
'                    Else
'                        .CB_CalcFormat.text = Mid(Trim(calcStr), findNo, Len(Trim(calcStr)) - findNo)
'                    End If
'
'                End If
'            End If
'
'            '* 図形名
'            .LBL_ShapeName = currShape.Name
'            '* 各条件
'            .TB_White.text = currRange.Offset(0, 0).Value
'            .TB_Red.text = currRange.Offset(0, 1).Value
'            .TB_Lime.text = currRange.Offset(0, 2).Value
'            .TB_Blue.text = currRange.Offset(0, 3).Value
'            .TB_Yellow.text = currRange.Offset(0, 4).Value
'            .TB_Magenta.text = currRange.Offset(0, 5).Value
'            .TB_Aqua.text = currRange.Offset(0, 6).Value
'            .TB_Maroon.text = currRange.Offset(0, 7).Value
'            .TB_Green.text = currRange.Offset(0, 8).Value
'            .TB_Navy.text = currRange.Offset(0, 9).Value
'            .TB_Olive.text = currRange.Offset(0, 10).Value
'            .TB_Purple.text = currRange.Offset(0, 11).Value
'            .TB_Teal.text = currRange.Offset(0, 12).Value
'            .TB_Silver.text = currRange.Offset(0, 13).Value
'            .TB_Gray.text = currRange.Offset(0, 14).Value
'
'            '* 「作成」ボタンを非表示
'            .BT_CreateTextBox.Visible = False
'
'            '* 書式ラジオボタンの設定
'            Select Case isFormat
'                Case 0
'                    .OB_Numeric.Value = True
'                Case 1
'                    .OB_String.Value = True
'                Case 2
'                    .OB_Date.Value = True
'                Case 3
'                    .OB_Time.Value = True
'                Case 4
'                    .OB_DateTime.Value = True
'            End Select
'
'            '* ISSUE_NO.694 sunyi 2018/05/16 start
'            '* 単一、複数条件により、単一、複数条件の画面表示か非表示
'            If .CB_CalcFormat.text = "個数(単一条件)" Or .CB_CalcFormat.text = "個数(複数条件)" Then
'                .TB_CalcCondition.Visible = True
'                .BT_add.Visible = True
'                .BT_rowdelete.Visible = True
'                .LV_ElementView.Visible = True
'                .Label25.Visible = True
'                .Label24.Visible = True
'                .Label23.Visible = True
'                .BT_ConditionUpdate.Visible = True
'            Else
'                .TB_CalcCondition.Visible = False
'                .BT_add.Visible = False
'                .BT_rowdelete.Visible = False
'                .LV_ElementView.Visible = False
'                .Label25.Visible = False
'                .Label24.Visible = False
'                .Label23.Visible = False
'                .BT_ConditionUpdate.Visible = False
'            End If
'            '* ISSUE_NO.694 sunyi 2018/05/16 end
'
'            '単一、複数条件を設定する
'            Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
'            iEndRow = condParamsheet.UsedRange.Rows.Count
'
'            J = 0
'            For II = 1 To iEndRow
'
'                If condParamsheet.Range("A" & II).Value = currShape.Name Then
'                    J = J + 1
'                    .LV_ElementView.ListItems.Add().text = J
'                    .LV_ElementView.ListItems(J).SubItems(1) = condParamsheet.Range("A" & II).Offset(0, 1)
'                    .LV_ElementView.ListItems(J).SubItems(2) = condParamsheet.Range("A" & II).Offset(0, 2)
'                    If J = 1 Then .TB_CalcCondition = condParamsheet.Range("A" & II).Offset(0, 2)
'                End If
'            Next
'
'            '* Link先
'            .CB_Link.Value = isLinkChk
'            Select Case isLinkFormat
'                Case 1
'                    .OB_StatusMonitor.Value = True
'                Case 2
'                    .OB_URL.Value = True
'                Case 3
'                    .OB_App.Value = True
'            End Select
'
'            .TB_ReferPass.text = linkStr
'            '* エレメント値 非表示
'            .TB_Name.text = elementStr
'            .CB_Element.Value = isElementChk
'
'            'コマンドリストを設定する
'            Set linkParamsheet = Workbooks(thisBookName).Worksheets(LinkParamSheetName)
'            iEndRow = linkParamsheet.UsedRange.Rows.Count
'
'            J = 0
'            For II = 1 To iEndRow
'
'                If linkParamsheet.Range("A" & II).Value = currShape.Name Then
'                    J = J + 1
'                    If linkParamsheet.Range("A" & II).Offset(0, 1) = 1 Then
'                        .LV_CommandList.ListItems.Add().text = LINKBOX_TYPE_AIS
'                    ElseIf linkParamsheet.Range("A" & II).Offset(0, 1) = 2 Then
'                        .LV_CommandList.ListItems.Add().text = LINKBOX_TYPE_URL
'                    ElseIf linkParamsheet.Range("A" & II).Offset(0, 1) = 3 Then
'                        .LV_CommandList.ListItems.Add().text = LINKBOX_TYPE_APP
'                    End If
'                    .LV_CommandList.ListItems(J).SubItems(1) = linkParamsheet.Range("A" & II).Offset(0, 2)
'                    .LV_CommandList.ListItems(J).SubItems(2) = linkParamsheet.Range("A" & II).Offset(0, 3)
'                End If
'            Next
'
'            '* ISSUE_NO.666 sunyi 2018/05/29 start
'            '* 吹き出し追加
'            If .LV_ElementView.ListItems.Count > 0 Then
'                .LV_ElementView.ControlTipText = .LV_ElementView.ListItems.Item(1).SubItems(1) & ":" & .LV_ElementView.ListItems.Item(1).SubItems(2)
'            End If
'            If .LV_CommandList.ListItems.Count > 0 Then
'                .LV_CommandList.ControlTipText = .LV_CommandList.ListItems.Item(1).text & ":" & .LV_CommandList.ListItems.Item(1).SubItems(1)
'            End If
'            '* ISSUE_NO.666 sunyi 2018/05/29 end
'
'            .Show vbModeless
'
'        End With
'
'        Exit Sub
'    End If
    ' Wang Issue NO.727 2018/06/15 End
    ' Processing On Server dn 2018/09/07 end
    
    Load ActionPaneForm
    With ActionPaneForm
        
        .Caption = "表示テキストボックス編集"
        
        '*ISSUE_NO.671 sunyi 2018/05/29 start
        '*仕様変更
'        Dim currWorksheet   As Worksheet
'        '*
'        Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
'        Set srchRange = currWorksheet.Cells.Find(What:="操作バルキ表示Flag", LookAt:=xlWhole)
'
'        srchRange.Offset(0, 1).Value = "True"
                
        If .CB_CalcFormat.text = "" Then
            '* 「エレメント追加」ボタンを非表示
            .BT_add.Visible = False
            
            '* 「エレメント削除」ボタンを非表示
            .BT_rowdelete.Visible = False
            
            '* 「条件更新」ボタンを非表示
            .BT_ConditionUpdate.Visible = False
            
            '* 「条件」テキストボックスを非表示
            .TB_CalcCondition.Visible = False
            
            '* 「条件」ラベルを非表示
            .Label23.Visible = False
            
            '* 「エレメント」リストビューを非表示
            .LV_ElementView.Top = 0
            .LV_ElementView.Left = 0
            .LV_ElementView.Visible = False
            .Frame5.Visible = False
            
            '* リストビューのへーだー「エレメント」ラベルを非表示
            .Label25.Visible = False
            
            '* リストビューのへーだー「条件」ラベルを非表示
            .Label24.Visible = False
        End If
        '*ISSUE_NO.671 sunyi 2018/05/29 end
    
        '* 演算式
        .TB_ItemOperation.text = calcStr
        
        '* 演算形式
        'WriteLog ("MultiStatusMonitorTemplate.xlsm SubMain_Module ConditionOpen()  calcStr:[" & calcStr & "] InStr(calcStr, '+'):[" & InStr(calcStr, "+") & "]")
        If InStr(calcStr, "+") <> 0 Then
            intstr = Split(calcStr, "+")
        
            'WriteLog ("MultiStatusMonitorTemplate.xlsm SubMain_Module ConditionOpen()  calcStr:[" & calcStr & "]  (If InStr(calcStr, '+') <> 0 Then) InStr(intstr(0), ','):[" & InStr(intstr(0), ",") & "]")
            If InStr(intstr(0), ",") <> 0 Then
                findNo = InStr(intstr(0), ",") + 1
                findNo1 = InStr(intstr(0), "[")
                'WriteLog ("MultiStatusMonitorTemplate.xlsm SubMain_Module ConditionOpen()  calcStr:[" & calcStr & "]  (If InStr(calcStr, '+') <> 0 Then) (findNo = InStr(intstr(0), ',') + 1):[" & findNo & "] (findNo1 = InStr(intstr(0), '[')):[" & findNo1 & "]")
                If findNo1 <> 0 Then
                    .CB_CalcFormat.text = Mid(Trim(intstr(0)), findNo, findNo1 - findNo)
                Else
                    calcStr2 = Mid(Trim(intstr(0)), findNo, Len(Trim(intstr(0))) - findNo)
                    If InStr(calcStr2, """") <> 0 Then
                        intstr2 = Split(calcStr2, """")
                        .CB_CalcFormat.text = intstr2(0)
                    Else
                        .CB_CalcFormat.text = Mid(Trim(intstr(0)), findNo, Len(Trim(intstr(0))) - findNo)
                    End If
                End If
            End If
        Else
            'WriteLog ("MultiStatusMonitorTemplate.xlsm SubMain_Module ConditionOpen()  calcStr:[" & calcStr & "] !(If InStr(calcStr, '+') <> 0 Then) InStr(calcStr, ','):[" & InStr(calcStr, ",") & "]")
            If InStr(calcStr, ",") <> 0 Then
                findNo = InStr(calcStr, ",") + 1
                findNo1 = InStr(calcStr, "[")
                'WriteLog ("MultiStatusMonitorTemplate.xlsm SubMain_Module ConditionOpen()  calcStr:[" & calcStr & "] !(If InStr(calcStr, '+') <> 0 Then) (findNo = InStr(calcStr, ',') + 1):[" & findNo & "] (findNo1 = InStr(calcStr, '[')):[" & findNo1 & "]")
                
                If findNo1 <> 0 Then
                    .CB_CalcFormat.text = Mid(Trim(calcStr), findNo, findNo1 - findNo)
                Else
                    calcStr2 = Mid(Trim(calcStr), findNo, Len(Trim(calcStr)) - findNo)
                    If InStr(calcStr2, """") <> 0 Then
                        intstr2 = Split(calcStr2, """")
                        .CB_CalcFormat.text = intstr2(0)
                    Else
                        .CB_CalcFormat.text = Mid(Trim(calcStr), findNo, Len(Trim(calcStr)) - findNo)
                    End If
                End If
                
            End If
        End If
        'WriteLog ("MultiStatusMonitorTemplate.xlsm SubMain_Module ConditionOpen()  .CB_CalcFormat.text:[" & .CB_CalcFormat.text & "]")
        
        '* 図形名
        .LBL_ShapeName = currShape.Name
        '* 各条件
        '* foastudio sunyi 2018/10/23 start
        .TB_White.Caption = currRange.Offset(0, 0).Value
        .TB_Red.Caption = currRange.Offset(0, 1).Value
        .TB_Lime.Caption = currRange.Offset(0, 2).Value
        .TB_Blue.Caption = currRange.Offset(0, 3).Value
        .TB_Yellow.Caption = currRange.Offset(0, 4).Value
        .TB_Magenta.Caption = currRange.Offset(0, 5).Value
        .TB_Aqua.Caption = currRange.Offset(0, 6).Value
        .TB_Maroon.Caption = currRange.Offset(0, 7).Value
        
        .TB_White_1.Caption = currRange.Offset(0, 20).Value
        .TB_White_2.Caption = currRange.Offset(0, 21).Value
        .TB_Red_1.Caption = currRange.Offset(0, 22).Value
        .TB_Red_2.Caption = currRange.Offset(0, 23).Value
        .TB_Lime_1.Caption = currRange.Offset(0, 24).Value
        .TB_Lime_2.Caption = currRange.Offset(0, 25).Value
        .TB_Blue_1.Caption = currRange.Offset(0, 26).Value
        .TB_Blue_2.Caption = currRange.Offset(0, 27).Value
        .TB_Yellow_1.Caption = currRange.Offset(0, 28).Value
        .TB_Yellow_2.Caption = currRange.Offset(0, 29).Value
        .TB_Magenta_1.Caption = currRange.Offset(0, 30).Value
        .TB_Magenta_2.Caption = currRange.Offset(0, 31).Value
        .TB_Aqua_1.Caption = currRange.Offset(0, 32).Value
        .TB_Aqua_2.Caption = currRange.Offset(0, 33).Value
        .TB_Maroon_1.Caption = currRange.Offset(0, 34).Value
        .TB_Maroon_2.Caption = currRange.Offset(0, 35).Value
        
        '* sunyi 2018/11/16 start
        .TB_White_1.ControlTipText = "[1] " & currRange.Offset(0, 20).Value & " [2] " & currRange.Offset(0, 21).Value
        .TB_Red_1.ControlTipText = "[1] " & currRange.Offset(0, 22).Value & " [2] " & currRange.Offset(0, 23).Value
        .TB_Lime_1.ControlTipText = "[1] " & currRange.Offset(0, 24).Value & " [2] " & currRange.Offset(0, 25).Value
        .TB_Blue_1.ControlTipText = "[1] " & currRange.Offset(0, 26).Value & " [2] " & currRange.Offset(0, 27).Value
        .TB_Yellow_1.ControlTipText = "[1] " & currRange.Offset(0, 28).Value & " [2] " & currRange.Offset(0, 29).Value
        .TB_Magenta_1.ControlTipText = "[1] " & currRange.Offset(0, 30).Value & " [2] " & currRange.Offset(0, 31).Value
        .TB_Aqua_1.ControlTipText = "[1] " & currRange.Offset(0, 32).Value & " [2] " & currRange.Offset(0, 33).Value
        .TB_Maroon_1.ControlTipText = "[1] " & currRange.Offset(0, 34).Value & " [2] " & currRange.Offset(0, 35).Value
        '* sunyi 2018/11/16 end
        
'        .TB_Green.text = currRange.Offset(0, 8).Value
'        .TB_Navy.text = currRange.Offset(0, 9).Value
'        .TB_Olive.text = currRange.Offset(0, 10).Value
'        .TB_Purple.text = currRange.Offset(0, 11).Value
'        .TB_Teal.text = currRange.Offset(0, 12).Value
'        .TB_Silver.text = currRange.Offset(0, 13).Value
'        .TB_Gray.text = currRange.Offset(0, 14).Value
        '* foastudio sunyi 2018/10/23 end

        '* 「作成」ボタンを非表示
        .BT_CreateTextBox.Visible = False
        
        '* 書式ラジオボタンの設定
        Select Case isFormat
            Case 0
                .OB_Numeric.Value = True
            Case 1
                .OB_String.Value = True
            Case 2
                .OB_Date.Value = True
            Case 3
                .OB_Time.Value = True
            Case 4
                .OB_DateTime.Value = True
        End Select
        
        '* ISSUE_NO.694 sunyi 2018/05/16 start
        '* 単一、複数条件により、単一、複数条件の画面表示か非表示
        If .CB_CalcFormat.text = "個数(単一条件)" Or .CB_CalcFormat.text = "個数(複数条件)" Then
            .TB_CalcCondition.Visible = True
            .BT_add.Visible = True
            .BT_rowdelete.Visible = True
            .Frame5.Visible = True
            .LV_ElementView.Top = 0
            .LV_ElementView.Left = 0
            .LV_ElementView.Visible = True
            .LV_ElementView.Top = 0
            .LV_ElementView.Left = 0
            .Label25.Visible = True
            .Label24.Visible = True
            .Label23.Visible = True
            .BT_ConditionUpdate.Visible = True
        Else
            .TB_CalcCondition.Visible = False
            .BT_add.Visible = False
            .BT_rowdelete.Visible = False
            .LV_ElementView.Visible = False
            .LV_ElementView.Top = 0
            .LV_ElementView.Left = 0
            .Frame5.Visible = False
            .Label25.Visible = False
            .Label24.Visible = False
            .Label23.Visible = False
            .BT_ConditionUpdate.Visible = False
        End If
        '* ISSUE_NO.694 sunyi 2018/05/16 end
        
        '単一、複数条件を設定する
        Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
        iEndRow = condParamsheet.UsedRange.Rows.Count
        
        J = 0
        For II = 1 To iEndRow
                        
            If condParamsheet.Range("A" & II).Value = currShape.Name Then
                J = J + 1
                .LV_ElementView.ListItems.Add().text = J
                .LV_ElementView.ListItems(J).SubItems(1) = condParamsheet.Range("A" & II).Offset(0, 1)
                .LV_ElementView.ListItems(J).SubItems(2) = condParamsheet.Range("A" & II).Offset(0, 2)
                If J = 1 Then .TB_CalcCondition = condParamsheet.Range("A" & II).Offset(0, 2)
            End If
        Next
         
        '* Link先
        .CB_Link.Value = isLinkChk
        Select Case isLinkFormat
            Case 1
                .OB_StatusMonitor.Value = True
            Case 2
                .OB_URL.Value = True
            Case 3
                .OB_App.Value = True
        End Select
        
        .TB_ReferPass.text = linkStr
        '* エレメント値 非表示
        .TB_Name.text = elementStr
        .CB_Element.Value = isElementChk
        
        'コマンドリストを設定する
        Set linkParamsheet = Workbooks(thisBookName).Worksheets(LinkParamSheetName)
        iEndRow = linkParamsheet.UsedRange.Rows.Count
        
        J = 0
        For II = 1 To iEndRow
                        
            If linkParamsheet.Range("A" & II).Value = currShape.Name Then
                J = J + 1
                If linkParamsheet.Range("A" & II).Offset(0, 1) = 1 Then
                    .LV_CommandList.ListItems.Add().text = LINKBOX_TYPE_AIS
                ElseIf linkParamsheet.Range("A" & II).Offset(0, 1) = 2 Then
                    .LV_CommandList.ListItems.Add().text = LINKBOX_TYPE_URL
                ElseIf linkParamsheet.Range("A" & II).Offset(0, 1) = 3 Then
                    .LV_CommandList.ListItems.Add().text = LINKBOX_TYPE_APP
                End If
                .LV_CommandList.ListItems(J).SubItems(1) = linkParamsheet.Range("A" & II).Offset(0, 2)
                .LV_CommandList.ListItems(J).SubItems(2) = linkParamsheet.Range("A" & II).Offset(0, 3)
            End If
        Next
        
        '* ISSUE_NO.666 sunyi 2018/05/29 start
        '* 吹き出し追加
        If .LV_ElementView.ListItems.Count > 0 Then
            .LV_ElementView.ControlTipText = .LV_ElementView.ListItems.Item(1).SubItems(1) & ":" & .LV_ElementView.ListItems.Item(1).SubItems(2)
        End If
        If .LV_CommandList.ListItems.Count > 0 Then
            .LV_CommandList.ControlTipText = .LV_CommandList.ListItems.Item(1).text & ":" & .LV_CommandList.ListItems.Item(1).SubItems(1)
        End If
        '* ISSUE_NO.666 sunyi 2018/05/29 end
             
        .Show vbModeless
    
    End With

End Sub

'Wang 外部取込みアプリケーションを起動 2018/04/27 Start
'外部取込みアプリケーションを起動し、異常終了の場合は後続処理を中断する
Private Function GetFileName(path As String) As String
    Dim fso As Object
    Set fso = CreateObject("Scripting.FileSystemObject")
    GetFileName = fso.GetFileName(path)
    Set fso = Nothing
End Function

Private Function ExecuteCommand(command As String) As Long
    Dim objShell As Object
    
    Set objShell = VBA.CreateObject("WScript.Shell")
    ExecuteCommand = objShell.Run(command, 1, True)
    Set objShell = Nothing
End Function
'Wang 外部取込みアプリケーションを起動 2018/04/27 End

'*************************************************
'* 画面の情報を元に○○工程　ステータスを生成する
'*************************************************
Public Sub WriteValueProduceData()
    
    Dim allData                 As Variant
    Dim rgbColor(7)             As Long
    Dim dataProduceWorksheet    As Worksheet
    Dim dataWorksheet           As Worksheet
    Dim statusWorksheet         As Worksheet
    
    Dim sDisplayCondition       As String
    Dim sCon1                   As String
    Dim sCon2                   As String
    Dim sCon3                   As String
    Dim sCon4                   As String
    Dim sCon5                   As String
    Dim sCon6                   As String
    Dim sCon7                   As String
    
    Dim startRange              As Range
    Dim msnRange                As Range
    
    Dim iStartCol               As Integer
    Dim iStartRow               As Integer
    Dim sDisplayCnt             As String
    Dim sStartColValue          As String
    Dim iEndRow                 As Integer
    Dim iEndCol                 As Integer
    Dim sEndColValue            As String
    Dim iConditionColNum        As Integer
    Dim sDataSheetName          As String
    Dim bFrameFixed             As Boolean
    
    Dim II                      As Integer
    Dim JJ                      As Integer
    Dim KK                      As Integer
    Dim LL                      As Integer
    
    Dim checkFlag               As Boolean
    
    '設定した表示文字色を取得する
    Call GetProduceDataColorArray(rgbColor)

    Call SetFilenameToVariable
                
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set dataProduceWorksheet = Workbooks(thisBookName).Worksheets(DataDispDefSheetName)

    '* 表示用データを取得する
    sDataSheetName = dataProduceWorksheet.Range("C10").Value
    If sDataSheetName = "" Then Exit Sub
    Set dataWorksheet = Workbooks(thisBookName).Worksheets(sDataSheetName)
    With dataWorksheet
        iEndRow = .UsedRange.Rows.Count
        iEndCol = .UsedRange.Columns.Count
    End With
    
    '設定した表示エレメントを取得する
    With dataProduceWorksheet

        'データ表示開始列
        sStartColValue = .Range("B1").Value
        iStartCol = GetColumnNum(sStartColValue)
        'データ表示開始行
        iStartRow = .Range("B2").text
        'データ表示件数
        sDisplayCnt = .Range("B3").text
        'ウィンドウ枠を固定
        bFrameFixed = .Range("D1").text
        
        If sDisplayCnt <> "" Then
            If iEndRow > sDisplayCnt Then
                iEndRow = sDisplayCnt + 1
            End If
        End If
        
        sEndColValue = GetColumnChar(iEndCol)
        allData = dataWorksheet.Range("A1:" & sEndColValue & iEndRow)
        
        '表示文字色条件：エレメント
        sDisplayCondition = .Range("A6").text
        iConditionColNum = 0
        For JJ = 1 To iEndCol - 1
            If sDisplayCondition = allData(1, JJ) Then
               iConditionColNum = JJ
               Exit For
            End If
        Next JJ
        
        '表示条件
        sCon1 = .Range("B6").text
        sCon2 = .Range("C6").text
        sCon3 = .Range("D6").text
        sCon4 = .Range("E6").text
        sCon5 = .Range("F6").text
        sCon6 = .Range("G6").text
        sCon7 = .Range("H6").text
  
        '* A10セルをスタートセルとする
        Set startRange = .Range("A10")
        
        Dim sCellFormat As String
        Dim iIndex As Integer
        Dim HideIndex As Integer
        iIndex = 0
        '* 設定した表示用エレメントを取得する
        For II = 0 To 100
            Set msnRange = startRange.Offset(II, 0)
            If msnRange.Offset(0, 0).Value <> "" Then
                
                For JJ = 1 To iEndCol
                
                    If msnRange.Offset(0, 0).Value = allData(1, JJ) Then
                        For KK = 1 To iEndRow
                        
                            ' データ
                            statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Value = allData(KK, JJ)
                            ' フォーマット
                            If msnRange.Offset(0, 1).Value = "数値" Then
                                sCellFormat = ""
                            ElseIf msnRange.Offset(0, 1).Value = "文字列" Then
                                sCellFormat = "@"
                            ElseIf msnRange.Offset(0, 1).Value = "日付" Then
                                sCellFormat = "yyyy/MM/dd"
                            ElseIf msnRange.Offset(0, 1).Value = "時刻" Then
                                sCellFormat = "hh:mm:ss"
                            ElseIf msnRange.Offset(0, 1).Value = "日時" Then
                                sCellFormat = "yyyy/MM/dd hh:mm:ss"
                            End If
                            statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).NumberFormatLocal = sCellFormat
                            '* ISSUE_NO.672 sunyi 2018/05/09 start
                            '* デフォルト値は黒にセット
                            statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Font.Color = rgbColor(0)
                            '* ISSUE_NO.672 sunyi 2018/05/09 end
                            ' データフォント色
                            If iConditionColNum > 0 Then
                                If sCon1 = allData(KK, iConditionColNum) Then
                                     statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Font.Color = rgbColor(0)
                                ElseIf sCon2 = allData(KK, iConditionColNum) Then
                                     statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Font.Color = rgbColor(1)
                                ElseIf sCon3 = allData(KK, iConditionColNum) Then
                                     statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Font.Color = rgbColor(2)
                                ElseIf sCon4 = allData(KK, iConditionColNum) Then
                                     statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Font.Color = rgbColor(3)
                                ElseIf sCon5 = allData(KK, iConditionColNum) Then
                                     statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Font.Color = rgbColor(4)
                                ElseIf sCon6 = allData(KK, iConditionColNum) Then
                                     statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Font.Color = rgbColor(5)
                                ElseIf sCon7 = allData(KK, iConditionColNum) Then
                                     statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Font.Color = rgbColor(6)
                                End If
                            Else
                                statusWorksheet.Cells(KK + iStartRow - 1, iIndex + iStartCol).Font.Color = 0
                            End If
                        Next KK
                        iIndex = iIndex + 1
                    End If
                    
                    '* ISSUE_NO.677 sunyi 2018/05/07 start
                    '* チェックボックスを表示か非表示の処理追加
                    If dataProduceWorksheet.Range("D2").Value = "True" Then
                    '* ISSUE_NO.677 sunyi 2018/05/07 end
                        checkFlag = False
                        If msnRange.Offset(0, 0).Value = ElementCheckVale Then
                            
                            For LL = 1 To iEndRow
                            
                                HideIndex = iIndex + iStartCol + MaxRtTmpIndex
                                If LL = 1 Then
                                    statusWorksheet.Range(Cells(iStartRow, HideIndex), Cells(iStartRow, HideIndex)).Value = allData(LL, iEndCol - 1)
                                    statusWorksheet.Range(Cells(iStartRow, HideIndex + 1), Cells(iStartRow, HideIndex + 1)).Value = allData(LL, iEndCol)
                                Else
                                    If CStr(allData(LL, iEndCol - 1)) = "" Then
                                        Exit For
                                    End If
                                    
                                    With statusWorksheet.Range(Cells(LL + iStartRow - 1, iIndex + iStartCol), Cells(LL + iStartRow - 1, iIndex + iStartCol)).Validation
                                    .Delete
                                    .Add Type:=xlValidateList, AlertStyle:=xlValidAlertStop, Operator:= _
                                    xlBetween, Formula1:=CheckValue
                                    .IgnoreBlank = True
                                    .InCellDropdown = True
                                    .InputTitle = ""
                                    .ErrorTitle = ""
                                    .InputMessage = ""
                                    .ErrorMessage = ""
                                    .IMEMode = xlIMEModeNoControl
                                    .ShowInput = True
                                    .ShowError = True
                                    End With
                                    
                                    '*Rt、CTMIDを設定
                                    statusWorksheet.Range(Cells(LL + iStartRow - 1, iIndex + iStartCol), Cells(LL + iStartRow - 1, iIndex + iStartCol)).Value = CheckValueDefault
                                    statusWorksheet.Range(Cells(LL + iStartRow - 1, HideIndex), Cells(LL + iStartRow - 1, HideIndex)).Value = str(allData(LL, iEndCol - 1))
                                    statusWorksheet.Range(Cells(LL + iStartRow - 1, HideIndex + 1), Cells(LL + iStartRow - 1, HideIndex + 1)).Value = allData(LL, iEndCol)
                                End If
                            Next LL
                            iIndex = iIndex + 1
                            checkFlag = True
                        End If
                        
                        If checkFlag = True Then
                            msnRange.Offset(0, 0).Value = ""
                        End If
                    '* ISSUE_NO.677 sunyi 2018/05/07 start
                    '* チェックボックスを表示か非表示の処理追加
                    End If
                    '* ISSUE_NO.677 sunyi 2018/05/07 end
                Next JJ
            Else
                Exit For
            End If
        Next II

        statusWorksheet.Range("A1").Activate
        statusWorksheet.Columns(GetColumnChar(iStartCol) & ":" & GetColumnChar(iEndCol)).EntireColumn.AutoFit
        
        If bFrameFixed Then
            ' セルを選択
            statusWorksheet.Cells(iStartRow + 1, iStartCol).Select
            
            ' ウィンドウ枠を固定
            ActiveWindow.FreezePanes = True
        End If
        
'        statusWorksheet.Columns(HideIndex).Select
'        Selection.EntireColumn.Hidden = True
'        statusWorksheet.Columns(HideIndex + 1).Select
'        Selection.EntireColumn.Hidden = True
        
    End With

End Sub

'*******************************************
'* RGBカラー配列取得（Long値）生データ表示画面用
'*******************************************
Public Sub GetProduceDataColorArray(rgbColor() As Long)

    Dim paramColWorksheet      As Worksheet
    Dim srchRange              As Range
    Dim currRange              As Range
    Dim i                      As Integer
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    With paramColWorksheet
    
        Set srchRange = .Range("1:1")
        Set currRange = srchRange.Find(What:="生データ表示画面", LookAt:=xlWhole)
        
        If Not currRange Is Nothing Then
            For i = 0 To 6
                If currRange.Offset(i + 1, 1).Value <> "" Then
                    rgbColor(i) = currRange.Offset(i + 1, 1).Value
                Else
                    rgbColor(i) = currRange.Offset(i + 1, 0).Value
                End If
            Next
        End If
    End With

End Sub

'*************************************************
'* 画面の情報を元に○○工程　ステータスを削除する
'*************************************************
Public Sub DeleteProduceData(sFlg As String, bNewdFrameFixed As Boolean)
     
    Dim startRange              As Range
    Dim msnRange                As Range
    Dim statusWorksheet         As Worksheet
    Dim dataProduceWorksheet    As Worksheet
    
    Dim iStartCol               As Integer
    Dim iStartRow               As Integer
    Dim sStartColValue          As String
    Dim bOldFrameFixed          As Boolean
    
    Dim II                      As Integer
    
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set dataProduceWorksheet = Workbooks(thisBookName).Worksheets(DataDispDefSheetName)
    
    With dataProduceWorksheet

        'データ表示開始列
        sStartColValue = .Range("B1").Value
        If sStartColValue = "" Then Exit Sub
        iStartCol = GetColumnNum(sStartColValue)
        'データ表示開始行
        iStartRow = .Range("B2").Value
        'ウィンドウ枠を固定
        bOldFrameFixed = .Range("D1").text
    End With

    ' ステータスシートのデータを削除する
    ' データを削除する
    statusWorksheet.Range(Rows(iStartRow), Rows(statusWorksheet.UsedRange.Rows.Count)).ClearContents
    
    With statusWorksheet.Range(Rows(iStartRow), Rows(statusWorksheet.UsedRange.Rows.Count)).Validation
        .Delete
        .Add Type:=xlValidateInputOnly, AlertStyle:=xlValidAlertStop, Operator _
        :=xlBetween
        .IgnoreBlank = True
        .InCellDropdown = True
        .IMEMode = xlIMEModeNoControl
        .ShowInput = True
        .ShowError = True
    End With
       
    If sFlg = "0" Then
    
        If bOldFrameFixed Then
            ' セルを選択する
            statusWorksheet.Cells(iStartRow + 1, iStartCol).Select
        
            ' ウィンドウ枠を解除する
            ActiveWindow.FreezePanes = False
            
        ElseIf bNewdFrameFixed Then
            ' セルを選択する
            statusWorksheet.Cells(iStartRow + 1, iStartCol).Select
        
            ' ウィンドウ枠を解除する
            ActiveWindow.FreezePanes = False
        End If
        
        ' 生データ画面機能シートのデータを削除する
        With dataProduceWorksheet
    
            'データ表示開始列
            .Range("B1").Value = ""
    
            'データ表示開始行
            .Range("B2").Value = ""
    
            'データ表示件数
            .Range("B3").Value = ""
    
            '表示文字色条件：エレメント
            .Range("A6").Value = ""
    
            '表示条件
            .Range("B6").Value = ""
            .Range("C6").Value = ""
            .Range("D6").Value = ""
            .Range("E6").Value = ""
            .Range("F6").Value = ""
            .Range("G6").Value = ""
            .Range("H6").Value = ""
    
             '* A10セルをスタートセルとする
            Set startRange = .Range("A10")
    
            '* 設定した表示用エレメントを削除する
            For II = 0 To 100
                Set msnRange = startRange.Offset(II, 0)
                If msnRange.Offset(0, 0).Value <> "" Then
                    msnRange.Offset(0, 0).Value = ""
                    msnRange.Offset(0, 1).Value = ""
                    msnRange.Offset(0, 2).Value = ""
                Else
                    Exit For
                End If
            Next II
        End With
    End If
        
End Sub

'************************
'* 数字⇒列号　変更する
'************************
Function GetColumnChar(char)
    Dim t
    On Error Resume Next
    If IsNumeric(char) Then
        t = Split(Cells(1, char).Address, "$")(1)
    Else
'        t = Columns(char & ":" & char).Column
        t = char
    End If
    If Err.Number <> 0 Then GetColumnChar = "超える範囲" Else GetColumnChar = t
End Function

'************************
'* 列号⇒数字　変更する
'************************
Function GetColumnNum(char)
    Dim t
    On Error Resume Next
    If IsNumeric(char) Then
'        t = Split(Cells(1, char).Address, "$")(1)
        t = char
    Else
        t = Columns(char & ":" & char).Column
    End If
    If Err.Number <> 0 Then GetColumnNum = "超える範囲" Else GetColumnNum = t
End Function


'************************
'************************
Function CreateCtmRefineParamFile() As String
    
    Dim dataProduceWorksheet    As Worksheet
    Dim dataWorksheet           As Worksheet
    Dim statusWorksheet         As Worksheet
    Dim sDataSheetName          As Worksheet
    
    Dim startRange              As Range
    Dim dataRange               As Range
    
    Dim startCol                As Integer
    Dim startRow                As Integer
    Dim dataIndexRow            As String
    Dim checkBoxCol             As Integer
    Dim checkMaxBoxRow             As Integer
    
    Dim elementIndex            As Integer
    Dim II                      As Integer
    
    Dim FolderPath              As String
    Dim obj                     As FoaCoreCom.FoaCoreCom
    Dim bdr                     As FoaCoreCom.StatusDataRetriever

    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set dataProduceWorksheet = Workbooks(thisBookName).Worksheets(DataDispDefSheetName)
    
    startCol = dataProduceWorksheet.Range("B1").Value
    startRow = dataProduceWorksheet.Range("B2").Value
    
'    dataIndexRow = dataProduceWorksheet.Range("B3").Value
'    If dataIndexRow <> "" Then
'        checkMaxBoxRow = dataIndexRow + startRow
'    End If
'
    With dataProduceWorksheet
        '* A10セルをスタートセルとする
        Set startRange = .Range("A10")
        
        '* 設定した表示用エレメントを取得する
        For elementIndex = 0 To 100
            Set dataRange = startRange.Offset(elementIndex, 0)
            If dataRange.Offset(0, 0).Value = "" Then
                elementIndex = elementIndex
                Exit For
            End If
        Next elementIndex
            
    End With
    
    checkBoxCol = startCol + elementIndex
    
    dataIndexRow = dataProduceWorksheet.Range("B3").Value
    If dataIndexRow <> "" Then
        checkMaxBoxRow = dataIndexRow + startRow + 1
    Else
        For II = startRow + 1 To 5000
            If statusWorksheet.Range(Cells(II, startCol + elementIndex + MaxRtTmpIndex), Cells(II, startCol + elementIndex + MaxRtTmpIndex)).Value = "" Then
                Exit For
            End If
        Next II
        checkMaxBoxRow = II
    End If
    
    Set bdr = CreateObject("FoaCoreCom.ais.retriever.StatusDataRetriever")
    
    CreateCtmRefineParamFile = bdr.GetStatusDataFromExcel(Workbooks(thisBookName), MaxRtTmpIndex, checkBoxCol, checkMaxBoxRow, startRow)
    
End Function

'************************
'チェックボックス用セルを設定
'************************
Public Sub SetCheckFalgInfoToExcel()

    Dim dataProduceWorksheet    As Worksheet
    Dim startRange              As Range
    Dim msnRange              As Range
    Dim II                      As Integer
    
    Set dataProduceWorksheet = Workbooks(thisBookName).Worksheets(DataDispDefSheetName)
    If dataProduceWorksheet.Range("B1").Value <> "" Then
        With dataProduceWorksheet
            
            '* A10セルをスタートセルとする
            Set startRange = .Range("A9")
    
            '* 表示エレメントを設定する
            For II = 1 To 100
                Set msnRange = startRange.Offset(II, 0)
                If msnRange.Offset(0, 0).Value = "" Then
                    Exit For
                End If
            Next
            
            Set msnRange = startRange.Offset(II, 0)
            '* ISSUE_NO.677 sunyi 2018/05/07 start
            '* チェックボックスを表示か非表示の処理追加
            If dataProduceWorksheet.Range("D2").Value = "True" Then
            '* ISSUE_NO.677 sunyi 2018/05/07 end
                msnRange.Offset(0, 0).Value = ElementCheckVale
            '* ISSUE_NO.677 sunyi 2018/05/07 start
            '* チェックボックスを表示か非表示の処理追加
            End If
            '* ISSUE_NO.677 sunyi 2018/05/07 end
        End With
    End If

End Sub

'************************
'自動更新の時､チェックボックスを非活性にする
'************************
Public Sub SetCheckBoxFromStatus()
    
    Dim dataProduceWorksheet    As Worksheet
    Dim dataWorksheet           As Worksheet
    Dim statusWorksheet         As Worksheet
    Dim sDataSheetName          As Worksheet
    
    Dim startRange              As Range
    Dim dataRange               As Range
    
    Dim startCol                As Integer
    Dim startRow                As Integer
    Dim dataIndexRow            As String
    Dim checkBoxCol             As Integer
    Dim checkMaxBoxRow             As Integer
    
    Dim elementIndex            As Integer
    Dim II                      As Integer
    Dim LL                      As Integer
     
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set dataProduceWorksheet = Workbooks(thisBookName).Worksheets(DataDispDefSheetName)
    '* ISSUE_NO.677 sunyi 2018/05/07 start
    '* チェックボックスを表示か非表示の処理追加
    If dataProduceWorksheet.Range("D2").Value = "True" Then
    '* ISSUE_NO.677 sunyi 2018/05/07 end
        startCol = dataProduceWorksheet.Range("B1").Value
        startRow = dataProduceWorksheet.Range("B2").Value
        
    '    dataIndexRow = dataProduceWorksheet.Range("B3").Value
    '    If dataIndexRow <> "" Then
    '        checkMaxBoxRow = dataIndexRow + startRow
    '    End If
        
        With dataProduceWorksheet
            '* A10セルをスタートセルとする
            Set startRange = .Range("A10")
            
            '* 設定した表示用エレメントを取得する
            For elementIndex = 0 To 100
                Set dataRange = startRange.Offset(elementIndex, 0)
                If dataRange.Offset(0, 0).Value = "" Then
                    elementIndex = elementIndex
                    Exit For
                End If
            Next elementIndex
                
        End With
        
        checkBoxCol = startCol + elementIndex
        
        dataIndexRow = dataProduceWorksheet.Range("B3").Value
            If dataIndexRow <> "" Then
                checkMaxBoxRow = dataIndexRow + startRow + 1
            Else
                For LL = startRow + 1 To 5000
                    If statusWorksheet.Range(Cells(LL, startCol + elementIndex + MaxRtTmpIndex), Cells(LL, startCol + elementIndex + MaxRtTmpIndex)).Value = "" Then
                        Exit For
                    End If
                Next LL
                checkMaxBoxRow = LL
            End If
        
        If checkBoxCol <> 0 Then
            For II = startRow + 1 To checkMaxBoxRow - 1
                statusWorksheet.Range(Cells(II, checkBoxCol), Cells(II, checkBoxCol)).Value = CheckValueDefault
            Next II
        End If
    '* ISSUE_NO.677 sunyi 2018/05/07 start
    '* チェックボックスを表示か非表示の処理追加
    End If
    '* ISSUE_NO.677 sunyi 2018/05/07 end
End Sub

