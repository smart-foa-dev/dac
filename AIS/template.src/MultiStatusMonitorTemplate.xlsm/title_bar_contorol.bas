Attribute VB_Name = "title_bar_contorol"
Private Declare PtrSafe Function FindWindow Lib "user32" Alias "FindWindowA" _
    (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare PtrSafe Function SetWindowLong Lib "user32" Alias "SetWindowLongA" _
    (ByVal hwnd As Long, ByVal nIndex As Long, ByVal dwNewLong As Long) As Long
Private Declare PtrSafe Function GetWindowLong Lib "user32" Alias "GetWindowLongA" _
    (ByVal hwnd As Long, ByVal nIndex As Long) As Long
  
Private Const GWL_STYLE = (-16)
Private Const WS_CAPTION = &HC00000
Private Const WS_MAXIMIZEBOX = &H10000
Private Const WS_MINIMIZEBOX = &H20000
Private Const WS_SYSMENU = &H80000
 
Private Declare PtrSafe Function SetWindowPos Lib "user32" _
    (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, _
    ByVal X As Long, ByVal Y As Long, ByVal cx As Long, _
    ByVal cy As Long, ByVal wFlags As Long) As Long
 
Private Enum ESetWindowPosStyles
    SWP_SHOWWINDOW = &H40
    SWP_HIDEWINDOW = &H80
    SWP_FRAMECHANGED = &H20
    SWP_NOACTIVATE = &H10
    SWP_NOCOPYBITS = &H100
    SWP_NOMOVE = &H2
    SWP_NOOWNERZORDER = &H200
    SWP_NOREDRAW = &H8
    SWP_NOREPOSITION = SWP_NOOWNERZORDER
    SWP_NOSIZE = &H1
    SWP_NOZORDER = &H4
    SWP_DRAWFRAME = SWP_FRAMECHANGED
    HWND_NOTOPMOST = -2
End Enum
 
Private Declare PtrSafe Function GetWindowRect Lib "user32" _
    (ByVal hwnd As Long, lpRect As RECT) As Long
 
Private Type RECT
    Left As Long
    Top As Long
    Right As Long
    Bottom As Long
End Type
 
Sub Title_Show()
    ShowTitleBar True
End Sub
 
Sub Title_Hide()
    ShowTitleBar False
End Sub
 
Sub ShowTitleBar(bShow As Boolean)
    Dim lStyle As Long
    Dim tRect As RECT
    Dim sWndTitle As String
    Dim xlhnd
    '// Untested should perhaps look for the class ?!
    sWndTitle = "Microsoft Excel - " & ActiveWindow.Caption
    xlhnd = FindWindow(vbNullString, sWndTitle)
    '// Get the window's position:
    GetWindowRect xlhnd, tRect
    '// Show the Title bar ?
    If Not bShow Then
        lStyle = GetWindowLong(xlhnd, GWL_STYLE)
        lStyle = lStyle And Not WS_SYSMENU
        lStyle = lStyle And Not WS_MAXIMIZEBOX
        lStyle = lStyle And Not WS_MINIMIZEBOX
        lStyle = lStyle And Not WS_CAPTION
    Else
        lStyle = GetWindowLong(xlhnd, GWL_STYLE)
        lStyle = lStyle Or WS_SYSMENU
        lStyle = lStyle Or WS_MAXIMIZEBOX
        lStyle = lStyle Or WS_MINIMIZEBOX
        lStyle = lStyle Or WS_CAPTION
    End If
    SetWindowLong xlhnd, GWL_STYLE, lStyle
    Application.DisplayFullScreen = Not bShow
    '// Ensure the style is set and makes the xlwindow the
    '// same size, regardless of the title bar.
    SetWindowPos xlhnd, 0, tRect.Left, tRect.Top, tRect.Right - tRect.Left, _
        tRect.Bottom - tRect.Top, SWP_NOREPOSITION Or SWP_NOZORDER Or SWP_FRAMECHANGED
End Sub

Sub test()
    Dim wrkStr As String
    
    wrkStr = ActiveWorkbook.Name
    wrkStr = ActiveWindow.Caption
    
    Application.Caption = ""
    ActiveWindow.Caption = ActiveWorkbook.Name

End Sub

