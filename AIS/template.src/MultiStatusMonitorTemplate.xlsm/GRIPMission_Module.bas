Attribute VB_Name = "GRIPMission_Module"
'* ISSUE_NO727 sunyi 2018/06/15 Start
'* grip mission処理ができるように修正にする
Option Explicit
'*******************************************
'*　GRIPミッション取込
'*******************************************
Public Function GetGripMission() As Integer

    Dim csvFile     As String
    Dim wrkStart    As String
    Dim wrkEnd      As String
    Dim dspTerm     As String
    Dim missionID   As String
    Dim strtTime    As Date
    Dim endTime     As Date
    Dim strFileName As String
    Dim wrkStr      As String
    
    Dim paramSheet  As Worksheet
    Dim srchRange   As Range
    Dim workRange   As Range
    Dim firstFlag    As Boolean
    
    Dim gripMission As GripDataRetriever.GripDataRetriever
    Dim IPAddr      As String
    Dim PortNo      As String
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    Dim dispTerm As Double
    Dim dispTermUnit As String
    
    '20161027 Add
    firstFlag = True
    
    'GRIPミッション設定
    Set gripMission = New GripDataRetriever.GripDataRetriever
    
    With Workbooks(thisBookName).Worksheets(ParamSheetName)
        
        Set srchRange = .Cells.Find(What:="取得期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                dispTerm = 1#
                dispTermUnit = "【時】"
            Else
                dispTerm = srchRange.Offset(0, 1).Value
                dispTermUnit = "【時】"
            End If
        Else
            dispTerm = 1#
            dispTermUnit = "【時】"
        End If
        Select Case dispTermUnit
            Case "【分】"
                dispTerm = dispTerm * 60 * (-1)
            Case "【時】"
                dispTerm = dispTerm * 3600 * (-1)
        End Select
        
        If dispTerm = 0 Then
            '* 収集開始日時と終了日時を取得
            Set srchRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                strtTime = CDate(srchRange.Offset(0, 1).Value)
            End If
            Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                endTime = CDate(srchRange.Offset(0, 1).Value)
            End If
            wrkStart = GetUnixTime(strtTime)
            wrkEnd = GetUnixTime(endTime)
        Else
            endTime = Now
            strtTime = DateAdd("s", dispTerm, Now)
            wrkStart = GetUnixTime(strtTime)
            wrkEnd = GetUnixTime(endTime)
        End If
    
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
        
        '*********************
        '* ミッションIDを取得
        '*********************
        missionID = ""
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "ミッションID項目が見つからないため処理を中断します。"
            GetGripMission = -1
            Exit Function
        End If
    
        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="GRIPサーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If

    End With
    
    'GRIPミッションの実行
    If useProxy = True Then
        csvFile = gripMission.GetGripDatatCsvFromProxy(useProxy, proxyUri, IPAddr, PortNo, missionID, wrkStart, wrkEnd)
    Else
        'GRIPミッションの実行
        '*****************************
        csvFile = gripMission.GetGripDatatCsv(IPAddr, PortNo, missionID, wrkStart, wrkEnd)
        '*****************************
    End If
    
    If csvFile = "" Then
        MsgBox "Gripミッション結果は有りません。", vbExclamation, "GetGripMission"
        GetGripMission = -1
        Exit Function
    End If
    
    ' フォルダの存在確認
    If Dir(csvFile, vbDirectory) = "" Then
        MsgBox "指定のフォルダは存在しません。", vbExclamation, "GetGripMission"
        GetGripMission = -1
        Exit Function
    End If

    ' 先頭のファイル名の取得
    strFileName = Dir(csvFile & "\*.*", vbNormal)
    firstFlag = True
    ' ファイルが見つからなくなるまで繰り返す
    Do While strFileName <> ""
        'CSVﾌｧｲﾙをCELLに書込む
        wrkStr = Replace(strFileName, ".csv", "")
        
'        If firstFlag = True Then
'            firstFlag = False
            Call CsvToCell(csvFile & "\" & strFileName, wrkStr)
'        Else
'            Call CsvToCellInsert(csvFile & "\" & strFileName, wrkStr)
'        End If
        '* AISTEMP No.102 sunyi 2018/11/22 start
'        Call NullDelete(wrkStr)
        '* AISTEMP No.102 sunyi 2018/11/22 end
        
        strFileName = Dir()
    Loop
    
    GetGripMission = 0
    
End Function

Public Sub CsvToCell(argFileName As String, argSheetName As String) '
    Dim srchRange As Range
    Dim lastRange As Range
    Dim workRange As Range
    
    Sheets(argSheetName).Cells.Clear

    With Sheets(argSheetName).QueryTables.Add(Connection:="TEXT;" & argFileName, Destination:=Sheets(argSheetName).Range("$A$1"))
'    With ActiveSheet.QueryTables.Add(Connection:= _
'        "TEXT;C:\Users\HelpMe\Desktop\GripClient_0612_2\grip_temp\2016-06-14-17-04-12-346\0_製品1生産実績_ルート1.csv", Destination:=Range("$A$1"))
        .Name = "temp"
        .FieldNames = True
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .RefreshOnFileOpen = False
        .RefreshStyle = xlOverwriteCells    'xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
        .AdjustColumnWidth = False  'True
        .RefreshPeriod = 0

        .TextFilePromptOnRefresh = False
        .TextFilePlatform = 65001 'UTF8  SHIF-JIS 932
        .TextFileStartRow = 1
        .TextFileParseType = xlDelimited
        .TextFileTextQualifier = xlTextQualifierDoubleQuote
        .TextFileConsecutiveDelimiter = False
        .TextFileTabDelimiter = False
        .TextFileSemicolonDelimiter = False
        .TextFileCommaDelimiter = True
        .TextFileSpaceDelimiter = False
        '.TextFileColumnDataTypes = Array(1, 1, 1)
        .TextFileTrailingMinusNumbers = True
        .Refresh BackgroundQuery:=False
        .Delete
    End With
    
    Sheets(argSheetName).Cells.Range("2:3").Delete
    
    '* resultシートをRECEIVE TIMEに従いソートする
    With Sheets(argSheetName)
        Set srchRange = .Range("A2")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = srchRange.End(xlDown)
        Set lastRange = .Cells(workRange.Row, lastRange.Column)
        Set workRange = .Range(srchRange, lastRange)
        workRange.Sort Key1:=.Range("B1"), _
                     Order1:=xlDescending, _
                     Header:=xlGuess
    End With
    
End Sub


Public Sub CsvToCellInsert(argFileName As String, argSheetName As String)
    Dim intRow      As Variant
    Dim strRow      As String
    
    intRow = Sheets(argSheetName).Range("A3").End(xlDown).Row
    strRow = "A" & Trim(CStr(intRow + 1))
    
    With Sheets(argSheetName).QueryTables.Add(Connection:="TEXT;" & argFileName, Destination:=Sheets(argSheetName).Range(strRow))
'    With ActiveSheet.QueryTables.Add(Connection:= _
'        "TEXT;C:\Users\HelpMe\Desktop\GripClient_0612_2\grip_temp\2016-06-14-17-04-12-346\0_製品1生産実績_ルート1.csv", Destination:=Range("$A$1"))
        .Name = "temp"
        .FieldNames = True
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .RefreshOnFileOpen = False
        .RefreshStyle = xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
        .AdjustColumnWidth = False  'True
        .RefreshPeriod = 0

        .TextFilePromptOnRefresh = False
        .TextFilePlatform = 65001 'UTF8  SHIF-JIS 932
        .TextFileStartRow = 1
        .TextFileParseType = xlDelimited
        .TextFileTextQualifier = xlTextQualifierDoubleQuote
        .TextFileConsecutiveDelimiter = False
        .TextFileTabDelimiter = False
        .TextFileSemicolonDelimiter = False
        .TextFileCommaDelimiter = True
        .TextFileSpaceDelimiter = False
        '.TextFileColumnDataTypes = Array(1, 1, 1)
        .TextFileTrailingMinusNumbers = True
        .Refresh BackgroundQuery:=False
        .Delete
    End With
    
    Sheets(argSheetName).Rows(intRow + 3).Delete
    Sheets(argSheetName).Rows(intRow + 2).Delete
    Sheets(argSheetName).Rows(intRow + 1).Delete
    
End Sub



'開始時刻の行削除
Public Sub NullDelete(argSheetName As String)
    Dim pos         As Variant
    Dim wrkVal      As Variant
    
    wrkVal = Worksheets(argSheetName).Range("A65536").End(xlUp).Row
    For pos = wrkVal To 3 Step -1
        If Worksheets(argSheetName).Cells(pos, 2).Value = "" Then
            Worksheets(argSheetName).Rows(pos).Delete

        End If
    Next pos

End Sub
'* ISSUE_NO727 sunyi 2018/06/15 End
