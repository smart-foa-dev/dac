Attribute VB_Name = "Main_Module"
Option Explicit

'--2018/03/06 Add PJNo.39 アドイン連携処理追加----Start
'**************************************
'*アドインメニュー制御(期間再設定)
'**************************************
Public Sub RefreshPeriod()
    Dim addIn As COMAddIn
    Dim automationObject As Object
    Set addIn = Application.COMAddIns("AisTemplateAddin")
    Set automationObject = addIn.Object
    If automationObject.RefreshPeriod Then
        Call OperationTest
    End If
End Sub
'--2018/03/06 Add PJNo.39 アドイン連携処理追加----End

'* foastudio AisAddin sunyi 2018/10/26 start
Public Sub SetIsPressed(IsPressed As Boolean)
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="SwitchLink", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        If IsPressed Then
            srchRange.Offset(0, 1).Value = "ON"
        Else
            srchRange.Offset(0, 1).Value = "OFF"
        End If
    End If
End Sub

'* ON/OFFを返す関数。上記の関数の状態を返す。
Public Function IsPressed() As String
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="SwitchLink", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        IsPressed = srchRange.Offset(0, 1).Value
    Else
        IsPressed = ""
    End If
End Function

Public Sub SetIsUpdated(isUpdated As Boolean)
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim mainWorksheet   As Worksheet
    Dim prevDate        As Date
    Dim prevTime        As Date
    
    Call SetFilenameToVariable
    
    If isUpdated Then
        '* メニュー制御
        Call MenuCtrlEnableOrDisable("テストデータ", False)
        
        'Wang Issue of UI improvement 2018/06/07 Start
        On Error Resume Next
        ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = True
        ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = False
        On Error GoTo 0
        'Wang Issue of UI improvement 2018/06/07 End
    
        Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
        Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
        
        If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "ON"
        
        Call TimerStart
    Else
        Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
        Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
        
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = "OFF"
        
            '* 予約したスケジュールをキャンセルする
            Call CancelSchedule
        
            '* メニュー制御
            Call MenuCtrlEnableOrDisable("テストデータ", True)
            'Wang Issue of UI improvement 2018/06/07 Start
            On Error Resume Next
            ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = False
            ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = True
            On Error GoTo 0
            'Wang Issue of UI improvement 2018/06/07 End
            Call UpdateFormStatus
            
        End If
    End If

End Sub
'* foastudio AisAddin sunyi 2018/10/26 end

'* ISSUE_NO.810 sunyi 2018/08/02 Start
'* 仕様変更、ボタン作成機能を追加する
Public Sub RefreshPeriod_Common()
    Dim addIn As COMAddIn
    Dim automationObject As Object
    Set addIn = Application.COMAddIns("AisTemplateAddin")
    Set automationObject = addIn.Object
    If automationObject.RefreshPeriod_Common Then
        Call OperationTest
    End If
End Sub

Public Sub RefreshPeriod_ByOneTime()
    Dim addIn As COMAddIn
    Dim automationObject As Object
    Set addIn = Application.COMAddIns("AisTemplateAddin")
    Set automationObject = addIn.Object
    If automationObject.RefreshPeriod_ByOneTime Then
        Call OperationTest
    End If
End Sub
'* ISSUE_NO.810 sunyi 2018/08/02 End

'* ISSUE_NO.810 sunyi 2018/08/02 Start
'* 仕様変更、ボタン作成機能を追加する
Public Sub WriteValueTextBoxPeriod(str As String)

    Dim nowShapeNum         As Integer
    Dim statusWorksheet     As Worksheet
    Dim currShape           As Shape
    Dim currShapeName       As String
    Dim currShapeNum        As Integer
    Dim II                  As Integer
    Dim idArray(100)        As Boolean
    Dim workStr             As String
    Dim iLineWeight         As Integer
    Dim iLineStyle          As Integer

    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    
    workStr = "期間再設定ボタンを作成" & vbCrLf & vbCrLf & _
                "※セル枠線に合わせる場合はALTキーを押しながらドラッグ"
    
    If MsgBox(workStr, vbYesNo + vbInformation) = vbYes Then
        Application.CommandBars.FindControl(ID:=1111).Execute
    Else
        Exit Sub
    End If
    
    iLineWeight = 1.5
    iLineStyle = xlContinuous
        
    With statusWorksheet
    
        '* 現在の図形数を取得する
        nowShapeNum = .Shapes.Count
        
        '* 図形が増えるまで待機
        Do
            DoEvents
        Loop Until nowShapeNum < .Shapes.Count
        
        '* 現在の該当図形数を取得
        For II = 0 To 99
            idArray(II) = True
        Next
        
        '**入力規則カウント抜く
        For II = .Shapes.Count To 1 Step -1
            If .Shapes(II).AutoShapeType = msoShapeRectangle Then
                nowShapeNum = II
                Exit For
            End If
        Next II
        'nowShapeNum = .Shapes.Count
        currShapeNum = 0
        For II = 1 To nowShapeNum
            If Left(.Shapes(II).Name, 6) = "value_" Then
                idArray(CInt(Right(.Shapes(II).Name, 2))) = False
                currShapeNum = currShapeNum + 1
            End If
        Next
        For II = 1 To 99
            If idArray(II) Then Exit For
        Next
        If II > 99 Then
            currShapeNum = 99
        Else
            currShapeNum = II
        End If
        
        '* 図形の属性を設定
        Set currShape = .Shapes(nowShapeNum)
        currShapeName = "value_shape_" & Format(currShapeNum, "00")
        
        With currShape
        
            With .ThreeD
                .BevelTopType = msoBevelCircle
                .BevelTopInset = 9
                .BevelTopDepth = 9
            End With
            
            .Name = currShapeName
            If str = "Common" Then
                .TextFrame.Characters.text = "期間再設定"
            Else
                .TextFrame.Characters.text = "期間再設定（日）"
            End If
            .OnAction = "Main_Module.RefreshPeriod_" & str
            With .TextFrame2
                '* 文字列配置中央
                .VerticalAnchor = msoAnchorMiddle
                With .TextRange
                    .ParagraphFormat.Alignment = msoAlignCenter
                    .Font.Bold = msoTrue
                    .Font.Size = 16
                    .Font.Fill.ForeColor.RGB = RGB(0, 0, 0)
                End With
            End With
            .Line.ForeColor.RGB = RGB(0, 0, 0)
            .Line.Weight = iLineWeight
            .Line.Style = iLineStyle
            .Fill.ForeColor.RGB = RGB(255, 255, 255)
        End With
        
    End With

End Sub
'* ISSUE_NO.810 sunyi 2018/08/02 End

'--2017/04/28 Add No.651 アドイン連携処理追加----Start
'**************************************
'*アドインメニュー制御(活性/非活性切替)
'**************************************
Public Sub InvalidateAddin()
    Dim addIn As COMAddIn
    Dim automationObject As Object
    
    Set addIn = Application.COMAddIns("AisTemplateAddin")
    Set automationObject = addIn.Object
    
    automationObject.Invalidate

End Sub
'--2017/04/28 Add No.651 アドイン連携処理追加----End

'******************************
'* スタートストップボタン処理
'******************************
Public Sub ChangeStartStopBT()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range

    Call SetFilenameToVariable
    
    '* ボタン表示状態を確認
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
    
        '* 更新中なら停止
        If srchRange.Offset(0, 1).Value = "ON" Then
            srchRange.Offset(0, 1).Value = "OFF"

            '* 予約したスケジュールをキャンセルする
            Call CancelSchedule
        
            '* メニュー制御
            Call MenuCtrlEnableOrDisable("テスト", True)
            Call MenuCtrlEnableOrDisable("自動更新", True)
            Call MenuCtrlEnableOrDisable("更新停止", False)
            
            'Wang Issue of UI improvement 2018/06/07 Start
            On Error Resume Next
            ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = False
            ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = True
            On Error GoTo 0
            'Wang Issue of UI improvement 2018/06/07 End
    
        '* 停止中なら開始
        Else
            srchRange.Offset(0, 1).Value = "ON"
            
            '* メニュー制御
            Call MenuCtrlEnableOrDisable("テスト", False)
            Call MenuCtrlEnableOrDisable("自動更新", False)
            Call MenuCtrlEnableOrDisable("更新停止", True)
            
            'Wang Issue of UI improvement 2018/06/07 Start
            On Error Resume Next
            ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = True
            ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = False
            On Error GoTo 0
            'Wang Issue of UI improvement 2018/06/07 End
            
            '* 自動実行開始
            Call TimerStart
        
        End If
        
        Call UpdateFormStatus

        'No.651 Add.
        '* アドインメニュー制御
        Call InvalidateAddin

    End If
    
    '* 編集モードボタン表示状態を確認
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="SwitchLink", LookAt:=xlWhole)

End Sub

'*******************
'* 自動更新スタート
'*******************
Public Sub AutoUpdateStart()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    
    Call SetFilenameToVariable
    
    '* メニュー制御
    Call MenuCtrlEnableOrDisable("テスト", False)
    Call MenuCtrlEnableOrDisable("自動更新", False)
    Call MenuCtrlEnableOrDisable("更新停止", True)
    
    'Wang Issue of UI improvement 2018/06/07 Start
    On Error Resume Next
    ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = True
    ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = False
    On Error GoTo 0
    'Wang Issue of UI improvement 2018/06/07 End

    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)

    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "ON"
    
    Call TimerStart

End Sub

'****************************
'* 自動更新を停止する
'****************************
Public Sub AutoUpdateStop()
    Dim currWorksheet   As Worksheet
    Dim mainWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim prevDate        As Date
    Dim prevTime        As Date
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = "OFF"
    
        '* 予約したスケジュールをキャンセルする
        Call CancelSchedule
    
        '* メニュー制御
        Call MenuCtrlEnableOrDisable("テスト", True)
        Call MenuCtrlEnableOrDisable("自動更新", True)
        Call MenuCtrlEnableOrDisable("更新停止", False)
        'Wang Issue of UI improvement 2018/06/07 Start
        On Error Resume Next
        ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = False
        ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = True
        On Error GoTo 0
        'Wang Issue of UI improvement 2018/06/07 End
        Call UpdateFormStatus
        
    End If
    
End Sub

'*******************************
'* 背景色設定画面表示
'*******************************
Public Sub SetDisplayBackgroundForm()
    Dim paramWorksheet      As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim chgElement          As Variant
    Dim chgElementID        As Variant  '2017/06/26 No.278 Add.
    Dim colorValue(4)       As Variant
    Dim II                  As Integer
    Dim expitem As Variant
    Dim iditem As Variant
    
    '2017/09/15 No.533 Add.
    Call SetFilenameToVariable
    
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With paramWorksheet
    
        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:="表示色対象", LookAt:=xlWhole)
    
        If Not currRange Is Nothing Then
        
            '* 表示色対象エレメント名を取得
            chgElement = currRange.Offset(0, 1).Value
                    
            '* 表示色対象エレメントIDを取得  '2017/06/26 No.278 Add.
            chgElementID = currRange.Offset(0, 2).Value

            '* 表示色条件を取得
            For II = 0 To 3
                colorValue(II) = currRange.Offset(II + 1, 1).Value
            Next
        
        End If

    End With

    ' Processing On Server dn 2019/09/07 start
    ' Grip とCTMを一緒に対応しているため
    '* 背景色定義画面を開く
    ' Wang Issue NO.727 2018/06/15 Start
'    If IsGripType() Then
'        Load GripActionPaneBackForm
'        With GripActionPaneBackForm
'
'            .TB_White.text = colorValue(0)
'            .TB_Red.text = colorValue(1)
'            .TB_Yellow.text = colorValue(2)
'            .TB_Green.text = colorValue(3)
'
'            .TB_ItemOperation = chgElement
'
'            .Show vbModeless
'
'        End With
'        Exit Sub
'    End If
    ' Wang Issue NO.727 2018/06/15 End
    ' Processing On Server dn 2019/09/07 end
    
    Load ActionPaneBackForm
    If colorValue(0) = "" And colorValue(1) = "" And colorValue(2) = "" And colorValue(3) = "" Then
      expitem = ""
      iditem = ""
    Else
      expitem = chgElement
      iditem = chgElementID
    End If
    With ActionPaneBackForm
    
        .TB_White.text = colorValue(0)
        .TB_Red.text = colorValue(1)
        .TB_Yellow.text = colorValue(2)
        .TB_Green.text = colorValue(3)
        
        .TB_ItemOperation = expitem
        .TB_ItemElementID = iditem  '2017/06/26 No.278 Add.

        .Show vbModeless
    
    End With
End Sub

'*******************************
'* テキストボックス編集画面表示
'*******************************
Public Sub DisplayEditForm()

    '2017/09/15 No.533 Add.
    Call SetFilenameToVariable

    Load ActionPaneForm
    With ActionPaneForm
    
        '* 「作成」ボタンを非表示
        .BT_CreateTextBox.Visible = False
    
        .Show vbModeless
    
    End With
End Sub

'***********************************
'* テキストボックス新規作成画面表示
'***********************************
Public Sub DisplayNewForm()

    '*ISSUE_NO.671 sunyi 2018/05/29 start
    '*仕様変更
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    '*
    
    Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)
    '*AISMM No.86 sunyi 2018/12/18 start
    '*仕様変更
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If srchRange.Offset(0, 1).Value = "ON" Then
        MsgBox ("動作中は編集できません。" & vbCrLf & _
                "停止してから、編集してください。")
        Exit Sub
    End If
    '*AISMM No.86 sunyi 2018/12/18 end
    
    Set srchRange = currWorksheet.Cells.Find(What:="操作バルキ表示Flag", LookAt:=xlWhole)
    
    srchRange.Offset(0, 1).Value = "True"
    '*ISSUE_NO.671 sunyi 2018/05/29 end

    '2017/09/15 No.533 Add.
    Call SetFilenameToVariable
    
    ' Processing On Server dn 2019/09/07 start
    ' Grip とCTMを一緒に対応しているため
    ' Wang Issue NO.727 2018/06/15 Start
'    If IsGripType() Then
'        Load GripActionPaneForm
'        With GripActionPaneForm
'
'            '* 「条件設定」ボタンを非表示
'            .BT_Set.Visible = False
'
'            '* 「削除」ボタンを非表示
'            .BT_Delete.Visible = False
'
'            '* 「更新」ボタンを非表示
'            .BT_Update.Visible = False
'
'            '* 「エレメント追加」ボタンを非表示
'            .BT_add.Visible = False
'
'            '* 「エレメント削除」ボタンを非表示
'            .BT_rowdelete.Visible = False
'
'            '* 「条件更新」ボタンを非表示
'            .BT_ConditionUpdate.Visible = False
'
'            '* 「条件」テキストボックスを非表示
'            .TB_CalcCondition.Visible = False
'
'            '* 「条件」ラベルを非表示
'            .Label23.Visible = False
'
'            '* 「エレメント」リストビューを非表示
'            .LV_ElementView.Visible = False
'
'            '* リストビューのへーだー「エレメント」ラベルを非表示
'            .Label25.Visible = False
'
'            '* リストビューのへーだー「条件」ラベルを非表示
'            .Label24.Visible = False
'
'            .Show vbModeless
'
'        End With
'        Exit Sub
'    End If
    ' Wang Issue NO.727 2018/06/15 End
    ' Processing On Server dn 2019/09/07 end

    Load ActionPaneForm
    With ActionPaneForm
    
        '* 「条件設定」ボタンを非表示
        .BT_Set.Visible = False
        
        '* 「削除」ボタンを非表示
        .BT_Delete.Visible = False
        
        '* 「更新」ボタンを非表示
        .BT_Update.Visible = False
        
        '* 「エレメント追加」ボタンを非表示
        .BT_add.Visible = False
        
        '* 「エレメント削除」ボタンを非表示
        .BT_rowdelete.Visible = False
        
        '* 「条件更新」ボタンを非表示
        .BT_ConditionUpdate.Visible = False
        
        '* 「条件」テキストボックスを非表示
        .TB_CalcCondition.Visible = False
        
        '* 「条件」ラベルを非表示
        .Label23.Visible = False
        
        '* 「エレメント」リストビューを非表示
        .LV_ElementView.Top = 0
        .LV_ElementView.Left = 0
        .LV_ElementView.Visible = False
        .Frame5.Visible = False
        
        '* リストビューのへーだー「エレメント」ラベルを非表示
        .Label25.Visible = False
        
        '* リストビューのへーだー「条件」ラベルを非表示
        .Label24.Visible = False
        
        .Show vbModeless
    
    End With
End Sub


'*******************
'* 状態フォーム更新
'*******************
Public Sub UpdateFormStatus()
    Dim currWorksheet   As Worksheet
    Dim paramWorksheet  As Worksheet
    Dim currRange       As Range
    Dim srchRange       As Range
    Dim flagON          As Boolean
    Dim f               As Variant
    Dim isNotForm       As Boolean
    Dim FinalTime       As Date
    Dim endD            As Date
    Dim endT            As Date
    Dim currShape       As Shape
    Dim RCAddress       As String
    Dim btSize          As RECT
    Dim prevTime        As Variant
    
    'Wang Issue of UI improvement 2018/06/07 Start
    Dim UpdataTimeStr   As String
    'Wang Issue of UI improvement 2018/06/07 End
    
    Call SetFilenameToVariable

    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    With paramWorksheet
    
        '* 自動更新状態を「pram」シートから取得
        Set srchRange = .Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value = "ON" Then
                flagON = True
                Call SetCheckBoxFromStatus
            Else
                flagON = False
                Call SetCheckBoxFromStatus
            End If
        End If
    
        '* 自動更新状態を「pram」シートから取得
        Set srchRange = .Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "" Then
                prevTime = Format(srchRange.Offset(0, 1).Value, "hh:mm:ss")
            Else
                prevTime = "--:--"
            End If
        End If
        
        'Wang Issue of UI improvement 2018/06/07 Start
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "" Then
                UpdataTimeStr = srchRange.Offset(0, 1).Value & "sec"
            Else
                UpdataTimeStr = "--秒"
            End If
        End If
        'Wang Issue of UI improvement 2018/06/07 End
    
    End With
    
    '* 全てのワークシートに対して表示テキストボックスを確認し、情報を表示する
'    For Each currWorksheet In Workbooks(thisBookName).Worksheets
        Set currWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
        isNotForm = True
        '* ワークシート上の全ての図形に対して
        For Each currShape In currWorksheet.Shapes
            '* 「ONLINE_」文字列を含む図形があれば図形内テキストを更新する
            If currShape.Name = PrefixStrTX & "ONLINE" Then
                isNotForm = False
                'Wang Issue of UI improvement 2018/06/07 Start
                currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & vbCrLf & vbCrLf & UpdataTimeStr
                'Wang Issue of UI improvement 2018/06/07 End
                If flagON Then
                    'Wang Issue of UI improvement 2018/06/07 Start
                    'currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & "更新中"
                    On Error Resume Next
                    ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = True
                    ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = False
                    On Error GoTo 0
                    'Wang Issue of UI improvement 2018/06/07 End
                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 255)
                Else
                    'Wang Issue of UI improvement 2018/06/07 Start
                    'currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & vbCrLf & vbCrLf & "停止中"
                    On Error Resume Next
                    ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = False
                    ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = True
                    On Error GoTo 0
                    'Wang Issue of UI improvement 2018/06/07 End
                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(255, 0, 0)
                End If
            End If
        Next currShape
        '* 表示テキストボックスがなければ追加する
        If isNotForm Then
            '* 設定されている表示範囲のアドレスを取得し、範囲を設定する
            RCAddress = GetDisplayAddress(currWorksheet)
            If RCAddress <> "" Then
                Set currRange = Workbooks(thisBookName).Worksheets(StatusSheetName).Range(RCAddress)
                '* 更新状況テキストボックスを追加
                btSize.Top = currRange.Top + 1
                btSize.Left = currRange.Left + 25
                btSize.Right = 72
                btSize.Bottom = 24
                Set currShape = PutTextBoxAtSheet(currWorksheet, btSize, RGB(0, 0, 0), RGB(255, 255, 255), PrefixStrTX, "ONLINE", "ChangeStartStopBT")
                If flagON Then
                    'Wang Issue of UI improvement 2018/06/07 Start
                    'currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & "更新中"
                    On Error Resume Next
                    ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = True
                    ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = False
                    On Error GoTo 0
                    'Wang Issue of UI improvement 2018/06/07 End
                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 255)
                Else
                    'Wang Issue of UI improvement 2018/06/07 Start
                    'currShape.TextFrame2.TextRange.Characters.text = prevTime & vbCrLf & "停止中"
                    On Error Resume Next
                    ActiveSheet.Shapes.Range(Array("BT_shape_refreshgraph")).Visible = False
                    ActiveSheet.Shapes.Range(Array("BT_shape_stopgraph")).Visible = True
                    On Error GoTo 0
                    'Wang Issue of UI improvement 2018/06/07 End
                    currShape.TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(255, 0, 0)
                End If
            End If
        End If
'    Next currWorksheet
    
End Sub

'**************************
'* 自動更新タイマー起動開始
'**************************
Public Sub TimerStart()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range

    Call SetFilenameToVariable
    
    With Workbooks(thisBookName).Worksheets(ParamSheetName)
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
            
        '*********************
        '* 収集終了日時を取得
        '*********************
'        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
'        If Not srchRange Is Nothing Then
'            endD = srchRange.Offset(0, 1).Value
'            endT = srchRange.Offset(0, 2).Value
'        Else
'            endD = CDate("2020/12/31")
'            endT = CDate("23:59:59")
'        End If
'        If Not IsDate(endD + endT) Then
'            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
'            Exit Sub
'        End If
'        endTime = GetUnixTime(endD + endT)
            
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'TimerLogic'"
        
        '次回更新日時セット
        Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd hh:mm:ss")
        End If
        
    End With
    
    '* ピボットテーブル更新 --2017/09/22 No.537 コメントアウトを解除
    Call RefreshPivotTableData
    
    '* 更新中／停止中ステータス更新
    Call UpdateFormStatus
    
End Sub

'***********************
'* 自動更新タイマー処理
'***********************
Public Sub TimerLogic()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim endTime         As Double
    Dim workPVT         As PivotTable
    Dim currWorksheet   As Worksheet
    Dim pvtRange        As Range
    Dim FinalTime       As Date
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    With currWorksheet
    
        Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "ON" Then
                Application.CutCopyMode = False
                Exit Sub
            End If
        End If
    
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="xxx", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = srchRange.Offset(0, 1).Value
            endT = srchRange.Offset(0, 2).Value
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        FinalTime = endD + endT
        If Format(Now, "yyyy/MM/dd hh:mm:ss") >= FinalTime Then         '終了時刻になったら終わる
            MsgBox "終了時刻になりました。" & " " & Format(Now, "hh:mm:ss")
            Call AutoUpdateStop
            Application.CutCopyMode = False
            Exit Sub
        End If
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="更新周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value         ' 秒計算
        Else
            updTime = 1
        End If
            
        '* 次回の処理をスケジュール
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'TimerLogic'"
        
        '前回／次回更新日時セット
        Set srchRange = .Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd hh:mm:ss")
        End If
        Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd hh:mm:ss")
        End If
                
    End With
    
    '* Processing On Server Dn 2018/08/24 Start
    ' Grip とCTMを一緒に対応しているため
    If isWorkplaceType Then
        Call GetMissionInfoAllAuto(SEARCH_TYPE_AUTO)
    Else
        '* ISSUE_NO.727 sunyi 2018/07/17 start
        If IsGripType Then
            Call GetGripMission
        Else
            '* ミッションからの情報取得
            Call GetMissionInfoAll(SEARCH_TYPE_AUTO)
        End If
        '* ISSUE_NO.727 sunyi 2018/07/17 End
    End If
    '* Processing On Server Dn 2018/08/24 End
        
    '* ピボットテーブル更新  --2017/09/22 No.537 Mod.コメントアウトを解除
    Call RefreshPivotTableData
    
    '* ステータスシート上の表示更新
    Call UpdateAllBackAndTB

    '* 更新状態表示更新
    Call UpdateFormStatus
    
    If Application.Visible = True Then
        'EXCEL前面表示用
        Call ExcelUpperDisp
        VBA.AppActivate Excel.Application.Caption
    
        Call ExcelDispSetWin32
        Call ExcelDispFreeWin32
    End If
    
End Sub


'* ISSUE_NO.724,725 Sunyi 2018/06/20 Start
'* 724.シングルクォーテーションを文字列から削除
'* 725.全項目、全データをセットする

''****************************
''* ピボットテーブルを更新する
''****************************
'Public Sub RefreshPivotTableData()
'    Dim currWorksheet   As Worksheet
'    Dim srcWorksheet    As Worksheet
'    Dim workPVT         As PivotTable
'    Dim pvtRange        As Range
'    Dim startRange      As Range
'    Dim dataRange       As Range
'    Dim lastRange       As Range
'    Dim dataSrc         As String
'    Dim dataNewSrc      As String
'    Dim srcSheetName    As String
'    Dim rangeValue      As Long '---2016/12/19 Add No.385
'
'    For Each currWorksheet In Workbooks(thisBookName).Worksheets
'        For Each workPVT In currWorksheet.PivotTables
'            With workPVT
'                Set pvtRange = .TableRange1
'
'                '* 対象ピボットテーブルのデータソースを取得する
'                dataSrc = .SourceData
'                '* データソースのシート名を取得する
'                srcSheetName = Left(dataSrc, InStr(dataSrc, "!") - 1)
'                Set srcWorksheet = Workbooks(thisBookName).Worksheets(srcSheetName)
'                With srcWorksheet
'                    Set startRange = .Range("A1")
'                    Set dataRange = .Range(startRange, startRange.End(xlDown).End(xlToRight))
'                    rangeValue = Len(.Range("A2").Value)  '---2016/12/19 Add No.385
'                End With
'                '---2016/12/19 Add No.385 データが存在するときのみ更新実行 IF文追加 -------------Start
'                '* データが存在すればピボットテーブルを更新する
'                If rangeValue > 0 Then
'                  dataNewSrc = srcSheetName & "!" & dataRange.Address(ReferenceStyle:=xlR1C1, External:=False)
'                  .SourceData = dataNewSrc
'                  .RefreshTable
'                End If
'                '---2016/12/19 Add No.385 データが存在するときのみ更新実行 IF文追加 -------------End
'            End With
'        Next workPVT
'    Next currWorksheet
'
'End Sub
'****************************
'* ピボットテーブルを更新する
'****************************
Public Sub RefreshPivotTableData()
    Dim currWorksheet   As Worksheet
    Dim srcWorksheet    As Worksheet
    Dim workPVT         As PivotTable
    Dim pvtRange        As Range
    Dim startRange      As Range
    Dim dataRange       As Range
    Dim rightRange      As Range
    Dim lastRange       As Range
    Dim dataSrc         As String
    Dim dataNewSrc      As String
    Dim srcSheetName    As String
    Dim rangeValue      As Long '---2016/12/19 Add No.385
    
    ' Wang Issue AISMM-91 20190124 Start
    Dim compositePvt() As String
    ReDim compositePvt(0)
    ' Wang Issue AISMM-91 20190124 End

    '* 該当ブックの全シートに対して
    For Each currWorksheet In Workbooks(thisBookName).Worksheets
        '* 対象シートの全ピボットテーブルに対して
        For Each workPVT In currWorksheet.PivotTables
            With workPVT
                '* ピボットテーブルの範囲を取得
                Set pvtRange = .TableRange1
                
                '* 対象ピボットテーブルのデータソースを取得する
                ' Wang Issue AISMM-91 20190124 Start
                If IsArray(.SourceData) Then
                    ReDim Preserve compositePvt(UBound(compositePvt) + 1)
                    compositePvt(UBound(compositePvt)) = currWorksheet.Name & "!" & workPVT.Name
                ' Wang Issue AISMM-91 20190124 End
                Else
                    dataSrc = .SourceData
                    
                    '* データソースのシート名を取得する
                    '* 2016.08.03 Add ↓↓↓ シングルクォーテーションを文字列から削除 ******
                    dataSrc = Replace(dataSrc, "'", "")
                    '* 2016.08.03 Add ↑↑↑ ***********************************************
                    srcSheetName = Left(dataSrc, InStr(dataSrc, "!") - 1)
                    On Error Resume Next
    '                Set srcWorksheet = Workbooks(thisBookName).Worksheets(srcSheetName)
                    With Workbooks(thisBookName).Worksheets(srcSheetName)
                        Set startRange = .Range("A1")
                        Set rightRange = startRange.End(xlToRight)       ' 項目行の一番右側を取得
                        Set lastRange = startRange.End(xlDown)           ' データ行の最下行を取得
                        Set lastRange = .Cells(lastRange.Row, rightRange.Column)
                        Set dataRange = .Range(startRange, lastRange)
                        rangeValue = Len(.Range("A2").Value)  '---2016/12/19 Add No.385
                    End With
                    '---2016/12/19 Add No.385 データが存在するときのみ更新実行 IF文追加 -------------Start
                    '* データが存在すればピボットテーブルを更新する
                    If rangeValue > 0 Then
                      dataNewSrc = srcSheetName & "!" & dataRange.Address(ReferenceStyle:=xlR1C1, external:=False)
                      .SourceData = dataNewSrc
                      .RefreshTable
                    End If
                    '---2016/12/19 Add No.385 データが存在するときのみ更新実行 IF文追加 -------------End
                End If
            End With
        Next workPVT
    Next currWorksheet

    ' Wang Issue AISMM-91 20190124 Start
    If (UBound(compositePvt) > 0) Then
        Dim strMessage As String
        Dim i As Integer
        strMessage = "下記のピボットテーブルは複数シートを使用していますので、更新できません。"
        For i = 1 To UBound(compositePvt)
            strMessage = strMessage & Chr(10) & Chr(13) & compositePvt(i)
        Next i
        MsgBox strMessage, vbCritical
    End If
    ' Wang Issue AISMM-91 20190124 End

End Sub

'* ISSUE_NO.724,725 Sunyi 2018/06/20 End

'*****************
'* ｢テスト｣の処理
'*****************
Public Sub OperationTest()
    Dim currWorksheet   As Worksheet
    Dim workPVT         As PivotTable
    Dim pvtRange        As Range
    Dim srchRange       As Range
    Dim searchType      As String
    
    On Error GoTo Label1
    
    Call SetFilenameToVariable
    
    '* Processing On Server Dn 2018/08/31 Start
    ' Grip とCTMを一緒に対応しているため
    If isWorkplaceType Then
        Call GetMissionInfoAllAuto(SEARCH_TYPE_TEST)
    Else
        '* ISSUE_NO.727 sunyi 2018/07/17 start
    '    '* ミッションからの情報取得
    '    Call GetMissionInfoAll(SEARCH_TYPE_AUTO)
        If IsGripType Then
            Call GetGripMission
        Else
            '* ミッションからの情報取得
            Call GetMissionInfoAll(SEARCH_TYPE_TEST)
        End If
        '* ISSUE_NO.727 sunyi 2018/07/17 End
    End If
    '* Processing On Server Dn 2018/08/31 End
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="検索タイプ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            searchType = srchRange.Offset(0, 1).Value
        End If
        
        If searchType = "OFF" Then
            '前回／次回更新日時セット
            Set srchRange = .Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd hh:mm:ss")
            End If
            Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd hh:mm:ss")
            End If
        End If
    End With
    
    '* ピボットテーブル更新  --2017/09/22 No.537 Add.
    Call RefreshPivotTableData

    '* ステータスシート上の表示更新
    Call UpdateAllBackAndTB
    

    
    '* 更新状態表示更新
    If searchType = "OFF" Then
        Call UpdateFormStatus
    End If
    Exit Sub
Label1:
    MsgBox ("エラーがありました。")
    
End Sub

'*****************************
'* スケジュール予約キャンセル
'*****************************
Public Sub CancelSchedule()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim prevDate        As Date
    Dim prevTime        As Date

    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)

    With currWorksheet
        '* 予約したスケジュールをキャンセルする
        Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            prevDate = srchRange.Offset(0, 1).Value
            On Error Resume Next
            Application.OnTime prevDate, "'TimerLogic'", , False
        End If
    End With

End Sub

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSub()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
    Dim objFso          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    Dim IsFirstSave     As String
    
    Debug.Print "In SaveFileSub"
    
    quitFlag = False
    
    Call SetFilenameToVariable
    
    Call CancelSchedule

    '* ボックス編集モードをOFFにする。
    Call SetIsPressed(False)
    
    Set objFso = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With Workbooks(thisBookName).Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then IsFirstSave = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
    currThisFile = Workbooks(thisBookName).FullName
'        Application.DisplayAlerts = False
'        ActiveWorkbook.SaveAs Filename:=currThisFile
'        Application.DisplayAlerts = True

'    wrkInt = InStr(Workbooks(thisBookName).Name, templateName)
'
'    If wrkInt <= 0 Or wrkInt > 4 Then
    If IsFirstSave = "" Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & Workbooks(thisBookName).Name
        Else
            fname = saveFilePath & "\" & templateName & "_" & Workbooks(thisBookName).Name
        End If
        '* テンプレート名／登録フォルダ名を取得する
        With Workbooks(thisBookName).Worksheets(ParamSheetName).Columns(1)
            Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "TRUE"
        End With
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & Workbooks(thisBookName).Name
        Else
            fname = saveFilePath & "\" & Workbooks(thisBookName).Name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
    If Dir(fname) <> "" Then
      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
    End If

    '各種バー表示
    Call DispBar
    
    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    Application.DisplayAlerts = False
    
    '* ISSUE_NO.624 Add ↓↓↓ *******************************
    On Error GoTo ErrorHandler
    
    Workbooks(thisBookName).SaveAs fileName:=fname
    
ErrorHandler:
    '-- 例外処理
    If Err.Description <> "" Then
        MsgBox Err.Description, vbCritical & vbOKOnly, "警告"
    End If
    '* ISSUE_NO.624 Add ↑↑↑ *******************************
    
    
    
    If Application.Workbooks.Count > 1 Then
        ThisWorkbook.Close
    Else
        Application.Quit
        ThisWorkbook.Close
    End If
    If Err.Description = "" Then
        Application.DisplayAlerts = True
    End If
End Sub

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSubFromX()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
    Dim objFso          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    
    Debug.Print "In SaveFileSubFromX"
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "ON" Then
            Call CancelSchedule
        End If
    End If

    Set objFso = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With Workbooks(thisBookName).Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
    currThisFile = Workbooks(thisBookName).FullName
'        Application.DisplayAlerts = False
'        ActiveWorkbook.SaveAs Filename:=currThisFile
'        Application.DisplayAlerts = True

    wrkInt = InStr(Workbooks(thisBookName).Name, templateName)

    If wrkInt <= 0 Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & Workbooks(thisBookName).Name
        Else
            fname = saveFilePath & "\" & templateName & "_" & Workbooks(thisBookName).Name
        End If
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & Workbooks(thisBookName).Name
        Else
            fname = saveFilePath & "\" & Workbooks(thisBookName).Name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
'    If Dir(fname) <> "" Then
'      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
'      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
'    End If
    
    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    If Workbooks(thisBookName).ReadOnly = True Then
         
        MsgBox Workbooks(thisBookName).Name & " は読み取り専用のため、上書き保存できません。" _
            & Chr(10) & "変更内容を維持するには、新しい名前でブックを保存するか、別の場所に保存する必要があります。", vbExclamation
        Application.Dialogs(xlDialogSaveAs).Show currThisFile
    Else
        
        Application.DisplayAlerts = False
        Workbooks(thisBookName).SaveAs fileName:=currThisFile
        If Application.Workbooks.Count > 1 Then
            ThisWorkbook.Close
        Else
            Application.DisplayAlerts = False
        
            ThisWorkbook.Save
        
            Application.Quit
            ThisWorkbook.Close
        End If
        'Application.DisplayAlerts = True
    
    End If
End Sub


'*******************
'* スイッチオン
'*******************
Public Sub SwitchOn()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="SwitchLink", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = "ON"
    
        '* メニュー制御
        Call MenuCtrlEnableOrDisable("編集モードON", False)
        Call MenuCtrlEnableOrDisable("編集モードOFF", True)
    
    End If

End Sub

'****************************
'* スイッチオフ
'****************************
Public Sub SwitchOff()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    
    Call SetFilenameToVariable
    
    Set currWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="SwitchLink", LookAt:=xlWhole)

    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = "OFF"

        '* メニュー制御
        Call MenuCtrlEnableOrDisable("編集モードON", True)
        Call MenuCtrlEnableOrDisable("編集モードOFF", False)

    End If
    
End Sub

'***********************************
'* 生データ表示画新規作成表示
'***********************************
Public Sub PruduceDataDisplay()

    Call SetFilenameToVariable
    
    Load DataProduceSetForm
    With DataProduceSetForm
            
        .Show vbModeless
    
    End With
End Sub

'* Processing On Server Dn 2018/08/31 Start
''****************************
''* Workplace場合、データ取得
''****************************
Public Sub GetMissionInfoAllAuto(sType As String)
    Dim ctmRtTime    As Double
    Dim workPlaceID  As String
    Dim srchRange    As Range
    Dim useProxy     As Boolean
    Dim proxyUri     As String
    Dim SendUrlStr   As String
    Dim zipURL       As String
    Dim unZipUrl     As String
    Dim IPAddr       As String
    Dim PortNo       As String
    Dim operation    As String
    Dim IsMultiStatus As Boolean
    
    
    Dim workplaceData As FoaCoreCom.WorkplaceDataRetriever
    Dim csvWrite As FoaCoreCom.CsvWriteToExcelHandler
    Set workplaceData = CreateObject("FoaCoreCom.ais.retriever.WorkplaceDataRetriever")
    Set csvWrite = CreateObject("FoaCoreCom.ais.retriever.CsvWriteToExcelHandler")
    
    Dim zipFileTemp As FoaCoreCom.ZipFileHandler
    Set zipFileTemp = CreateObject("FoaCoreCom.ais.retriever.ZipFileHandler")
    
    'CTMのRT時間取得
    ctmRtTime = GetCtmRtTime()
    
    'WorkPlaceID
    Set srchRange = Workbooks(thisBookName).Worksheets(RoParamSheetName).Cells.Find(What:="WorkPlaceID", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        workPlaceID = srchRange.Offset(0, 1).Value
    End If
    
    '*******************************
    '*******************************
    '* Proxyの使用可否とProxyのURIを取得
    Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        useProxy = srchRange.Offset(0, 1).Value
        proxyUri = srchRange.Offset(1, 1).Value
    Else
        useProxy = False
        proxyUri = ""
    End If
    Set srchRange = Workbooks(thisBookName).Worksheets(ParamSheetName).Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        IPAddr = srchRange.Offset(0, 1).Value
        PortNo = srchRange.Offset(1, 1).Value
    Else
        IPAddr = "localhost"
        PortNo = "60000"
    End If
    
    'ExcelEngineからWorkplaceDataZipパスを取得
    If sType = SEARCH_TYPE_TEST Then
        operation = "test"
    Else
        operation = "auto"
    End If
    SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/excelengine/rest/workplace/getzip?workplaceId=" & workPlaceID & "&ctmRtTime=" & Format(ctmRtTime) & "&operation=" & operation

    Dim msg As String
    msg = ""
    zipURL = workplaceData.GetWorkplaceMissionZipFileAsync(SendUrlStr, proxyUri, msg)
    If msg <> "" Then
        MsgBox ("データ取得失敗")
        Exit Sub
    End If

    ' Zipファイルを解凍する
    unZipUrl = zipFileTemp.UnZipFile(zipURL)
    
  ' excelへ書き込む
   IsMultiStatus = True
   Call csvWrite.ReadAllMissionToExcel(Workbooks(thisBookName), unZipUrl, MaxCntImport, sType, GetTimezoneId(), MissionSheetName, IsMultiStatus)
    
    '一時フォルダを削除
    zipFileTemp.deleteFolder (unZipUrl)
    
    Call SetCheckFalgInfoToExcel
    
End Sub
'* Processing On Server Dn 2018/08/31 End



