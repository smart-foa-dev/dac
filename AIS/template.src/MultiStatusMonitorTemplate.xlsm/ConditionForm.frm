VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ConditionForm 
   Caption         =   "条件入力"
   ClientHeight    =   3480
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   5415
   OleObjectBlob   =   "ConditionForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "ConditionForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'*******************
'* 閉じるボタン処理
'*******************
Private Sub BT_Close_Click()
    Unload Me
End Sub

'***********
'* 削除処理
'***********
Private Sub BT_Delete_Click()
    Dim currShape           As Shape
    Dim dispDefWorksheet    As Worksheet
    Dim statusWorksheet     As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim calcStr             As String
    Dim currShapeName       As String
    Dim workStr             As String

    '* 全図形情報削除
'    Call DeleteAllShapeAndDispDef

    workStr = "着目図形を削除します。よろしいですか？"
    If MsgBox(workStr, vbYesNo + vbQuestion, "削除確認") = vbNo Then Exit Sub
    
    '* 着目図形を削除
    currShapeName = LBL_ShapeName.Caption
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set currShape = statusWorksheet.Shapes(currShapeName)
    currShape.Delete

    '* 着目画面定義情報を削除
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet

        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.EntireRow.ClearContents
        Else
            Exit Sub
        End If

    End With
    
    Unload Me
    
End Sub

'*************
'* 式編集処理
'*************
Private Sub BT_Edit_Click()
    Dim dispDefWorksheet    As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim calcStr             As String
    Dim currShapeName       As String

    currShapeName = LBL_ShapeName.Caption
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet

        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            calcStr = currRange.Offset(0, 1).Value
        Else
            Exit Sub
        End If

    End With
    
    MsgBox currShapeName & ":" & calcStr
    
End Sub

'***************
'* 条件設定処理
'***************
Private Sub BT_Set_Click()
    Dim shapeName           As String
    Dim srchRange           As Range
    Dim colRange            As Range
    Dim rowRange            As Range
    Dim currRange           As Range
    Dim dispDefWorksheet    As Worksheet
    
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    
    shapeName = LBL_ShapeName.Caption
    
    With dispDefWorksheet

        Set srchRange = .Range("A:A")
        Set rowRange = srchRange.Find(What:=shapeName, LookAt:=xlWhole)
        If Not rowRange Is Nothing Then
            Set srchRange = .Range("1:1")
            Set colRange = srchRange.Find(What:="表示色−緑", LookAt:=xlWhole)
            If Not colRange Is Nothing Then
                Set currRange = .Cells(rowRange.Row, colRange.Column)
                Set srchRange = .Range(currRange.Offset(0, 0), currRange.Offset(0, 3))
                srchRange.NumberFormatLocal = "@"
                currRange.Offset(0, 0).Value = TB_Green.text
                currRange.Offset(0, 1).Value = TB_Red.text
                currRange.Offset(0, 2).Value = TB_Blue.text
                currRange.Offset(0, 3).Value = TB_White.text
            End If
        End If

    End With
    
    Unload Me

End Sub

Private Sub UserForm_Click()

End Sub
