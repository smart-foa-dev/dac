VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ActionPaneForm 
   Caption         =   "パネル項目設定"
   ClientHeight    =   10785
   ClientLeft      =   240
   ClientTop       =   390
   ClientWidth     =   15930
   OleObjectBlob   =   "ActionPaneForm.frx":0000
End
Attribute VB_Name = "ActionPaneForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'Processing on Server dn 2018/09/07 start
Private GripEntityMap As New clsGripEntityMap
'Processing on Server dn 2018/09/07 end

'*************************************
'* 表示色条件テキストボックスをクリア
'*************************************
Private Sub BT_Clear_Click()
'* foastudio sunyi 2018/10/23 start
    If MsgBox("BOX表示色＆文字設定の内容をすべてクリアしますか？", vbYesNo + vbInformation) = vbNo Then Exit Sub
    TB_White.Caption = ""
    TB_Red.Caption = ""
    TB_Lime.Caption = ""
    TB_Blue.Caption = ""
    TB_Yellow.Caption = ""
    TB_Magenta.Caption = ""
    TB_Aqua.Caption = ""
    TB_Maroon.Caption = ""
    TB_White_1.Caption = ""
    TB_Red_1.Caption = ""
    TB_Lime_1.Caption = ""
    TB_Blue_1.Caption = ""
    TB_Yellow_1.Caption = ""
    TB_Magenta_1.Caption = ""
    TB_Aqua_1.Caption = ""
    TB_Maroon_1.Caption = ""
    TB_White_2.Caption = ""
    TB_Red_2.Caption = ""
    TB_Lime_2.Caption = ""
    TB_Blue_2.Caption = ""
    TB_Yellow_2.Caption = ""
    TB_Magenta_2.Caption = ""
    TB_Aqua_2.Caption = ""
    TB_Maroon_2.Caption = ""
'    TB_Green.text = ""
'    TB_Navy.text = ""
'    TB_Olive.text = ""
'    TB_Purple.text = ""
'    TB_Teal.text = ""
'    TB_Silver.text = ""
'    TB_Gray.text = ""
'* foastudio sunyi 2018/10/23 end
End Sub

'*********************
'* 表示色条件取得処理
'*********************
Public Sub GetDispColorCond(dispColorCond() As String)
'* foastudio sunyi 2018/10/23 start
    dispColorCond(0) = TB_White.Caption
    dispColorCond(1) = TB_Red.Caption
    dispColorCond(2) = TB_Lime.Caption
    dispColorCond(3) = TB_Blue.Caption
    dispColorCond(4) = TB_Yellow.Caption
    dispColorCond(5) = TB_Magenta.Caption
    dispColorCond(6) = TB_Aqua.Caption
    dispColorCond(7) = TB_Maroon.Caption
'    dispColorCond(8) = TB_Green.text
'    dispColorCond(9) = TB_Navy.text
'    dispColorCond(10) = TB_Olive.text
'    dispColorCond(11) = TB_Purple.text
'    dispColorCond(12) = TB_Teal.text
'    dispColorCond(13) = TB_Silver.text
'    dispColorCond(14) = TB_Gray.text
'* foastudio sunyi 2018/10/23 start
End Sub


'* foastudio sunyi 2018/10/23 start
'*********************
'* 表示文字取得処理
'*********************
Public Sub GetDispTextCond(dispTextCond() As String)
    dispTextCond(0) = Left(TB_White_1.Caption, 15)
    dispTextCond(1) = Left(TB_White_2.Caption, 15)
    dispTextCond(2) = Left(TB_Red_1.Caption, 15)
    dispTextCond(3) = Left(TB_Red_2.Caption, 15)
    dispTextCond(4) = Left(TB_Lime_1.Caption, 15)
    dispTextCond(5) = Left(TB_Lime_2.Caption, 15)
    dispTextCond(6) = Left(TB_Blue_1.Caption, 15)
    dispTextCond(7) = Left(TB_Blue_2.Caption, 15)
    dispTextCond(8) = Left(TB_Yellow_1.Caption, 15)
    dispTextCond(9) = Left(TB_Yellow_2.Caption, 15)
    dispTextCond(10) = Left(TB_Magenta_1.Caption, 15)
    dispTextCond(11) = Left(TB_Magenta_2.Caption, 15)
    dispTextCond(12) = Left(TB_Aqua_1.Caption, 15)
    dispTextCond(13) = Left(TB_Aqua_2.Caption, 15)
    dispTextCond(14) = Left(TB_Maroon_1.Caption, 15)
    dispTextCond(15) = Left(TB_Maroon_2.Caption, 15)
End Sub
'* foastudio sunyi 2018/10/23 end

'*********************
'* 条件取得処理
'*********************
Public Sub GetElementList(elementList() As String)

    For i = 1 To LV_ElementView.ListItems.Count Step 1
        elementList(i - 1) = LV_ElementView.ListItems(i).SubItems(1)
    Next i
    
End Sub
'*********************
'* 条件取得処理
'*********************
Public Sub GetElementCondList(elementCondList() As String)

    For i = 1 To LV_ElementView.ListItems.Count Step 1
        elementCondList(i - 1) = LV_ElementView.ListItems(i).SubItems(2)
    Next i
    
End Sub

'*************************************************
'* コマンドリストの情報のタイプを取得する
'*************************************************
Public Sub GetCommandListType(commandListType() As Integer)

    For i = 1 To LV_CommandList.ListItems.Count Step 1
        If LV_CommandList.ListItems(i).text = LINKBOX_TYPE_AIS Then
            commandListType(i - 1) = 1
        ElseIf LV_CommandList.ListItems(i).text = LINKBOX_TYPE_URL Then
            commandListType(i - 1) = 2
        ElseIf LV_CommandList.ListItems(i).text = LINKBOX_TYPE_APP Then
            commandListType(i - 1) = 3
        End If
    Next i
    
End Sub

'*************************************************
'* コマンドリストの情報の表示名を取得する
'*************************************************
Public Sub GetCommandListName(commandListName() As String)

    For i = 1 To LV_CommandList.ListItems.Count Step 1
        commandListName(i - 1) = LV_CommandList.ListItems(i).SubItems(1)
    Next i
    
End Sub

'*************************************************
'* コマンドリストの情報の全てパスを取得する
'*************************************************
Public Sub GetCommandListPass(commandListPass() As String)

    For i = 1 To LV_CommandList.ListItems.Count Step 1
        commandListPass(i - 1) = LV_CommandList.ListItems(i).SubItems(2)
    Next i
    
End Sub

'***********
'* 削除処理
'***********
Private Sub BT_Delete_Click()
    Dim currShape           As Shape
    Dim dispDefWorksheet    As Worksheet
    Dim statusWorksheet     As Worksheet
    Dim linkWorksheet       As Worksheet
    Dim condWorksheet       As Worksheet
    Dim linkParamWorksheet  As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim srchRange1          As Range
    Dim workRange           As Range
    Dim calcStr             As String
    Dim currShapeName       As String
    Dim workStr             As String
    Dim iRowCnt             As Integer
    Dim II                  As Integer

    '* 全図形情報削除
'    Call DeleteAllShapeAndDispDef

    workStr = "着目図形を削除します。よろしいですか？"
    If MsgBox(workStr, vbYesNo + vbQuestion, "削除確認") = vbNo Then Exit Sub
    
    '* 着目図形を削除
    currShapeName = LBL_ShapeName.Caption
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set currShape = statusWorksheet.Shapes(currShapeName)
    currShape.Delete

    '* 着目画面定義情報を削除
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet

        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.EntireRow.ClearContents
        Else
            Exit Sub
        End If

    End With
    
    '* 着目リンクシート情報を削除
    Set linkWorksheet = Workbooks(thisBookName).Worksheets(LinkSheetName)
    iRowCnt = linkWorksheet.UsedRange.Rows.Count
    With linkWorksheet
        For II = iRowCnt To 1 Step -1
            If .Range("B" & II).Value = currShapeName Then
                
                ' データを削除する
                .Rows(II).Delete
            End If
        Next II
    End With
    
    '* 着目条件情報を削除
    Set condWorksheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    iRowCnt = condWorksheet.UsedRange.Rows.Count
    With condWorksheet

        For II = iRowCnt To 1 Step -1
            If .Range("A" & II).Value = currShapeName Then
                
                ' データを削除する
                .Rows(II).Delete
            End If
        Next II

    End With
    
    '* 着目コマンドリスト情報を削除
    Set linkParamWorksheet = Workbooks(thisBookName).Worksheets(LinkParamSheetName)
    iRowCnt = linkParamWorksheet.UsedRange.Rows.Count
    With linkParamWorksheet

        For II = iRowCnt To 1 Step -1
            If .Range("A" & II).Value = currShapeName Then
                
                ' データを削除する
                .Rows(II).Delete
            End If
        Next II

    End With
    Unload Me
    
End Sub

'*********************
'* 表示色条件設定処理
'*********************
Private Sub BT_Set_Click()
    Dim shapeName           As String
    Dim srchRange           As Range
    Dim colRange            As Range
    Dim rowRange            As Range
    Dim currRange           As Range
    Dim dispDefWorksheet    As Worksheet
    
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    
    shapeName = LBL_ShapeName.Caption
    
    If shapeName <> "" Then
    
        With dispDefWorksheet
    
            Set srchRange = .Range("A:A")
            Set rowRange = srchRange.Find(What:=shapeName, LookAt:=xlWhole)
            If Not rowRange Is Nothing Then
                Set srchRange = .Range("1:1")
                Set colRange = srchRange.Find(What:="表示色−白", LookAt:=xlWhole)
                If Not colRange Is Nothing Then
                    Set currRange = .Cells(rowRange.Row, colRange.Column)
                    Set srchRange = .Range(currRange.Offset(0, 0), currRange.Offset(0, 14))
                    srchRange.NumberFormatLocal = "@"
                    '* foastudio sunyi 2018/10/23 start
                    currRange.Offset(0, 0).Value = TB_White.Caption
                    currRange.Offset(0, 1).Value = TB_Red.Caption
                    currRange.Offset(0, 2).Value = TB_Lime.Caption
                    currRange.Offset(0, 3).Value = TB_Blue.Caption
                    currRange.Offset(0, 4).Value = TB_Yellow.Caption
                    currRange.Offset(0, 5).Value = TB_Magenta.Caption
                    currRange.Offset(0, 6).Value = TB_Aqua.Caption
                    currRange.Offset(0, 7).Value = TB_Maroon.Caption
'                    currRange.Offset(0, 8).Value = TB_Green.text
'                    currRange.Offset(0, 9).Value = TB_Navy.text
'                    currRange.Offset(0, 10).Value = TB_Olive.text
'                    currRange.Offset(0, 11).Value = TB_Purple.text
'                    currRange.Offset(0, 12).Value = TB_Teal.text
'                    currRange.Offset(0, 13).Value = TB_Silver.text
'                    currRange.Offset(0, 14).Value = TB_Gray.text
                    '* foastudio sunyi 2018/10/23 end
                End If
                '* foastudio sunyi 2018/10/23 start
                Set colRange = srchRange.Find(What:="表示文字1-1", LookAt:=xlWhole)
                If Not colRange Is Nothing Then
                    Set currRange = .Cells(rowRange.Row, colRange.Column)
                    Set srchRange = .Range(currRange.Offset(0, 0), currRange.Offset(0, 15))
                    srchRange.NumberFormatLocal = "@"
                    currRange.Offset(0, 0).Value = Left(TB_White_1.Caption, 15)
                    currRange.Offset(0, 1).Value = Left(TB_White_2.Caption, 15)
                    currRange.Offset(0, 2).Value = Left(TB_Red_1.Caption, 15)
                    currRange.Offset(0, 3).Value = Left(TB_Red_2.Caption, 15)
                    currRange.Offset(0, 4).Value = Left(TB_Lime_1.Caption, 15)
                    currRange.Offset(0, 5).Value = Left(TB_Lime_2.Caption, 15)
                    currRange.Offset(0, 6).Value = Left(TB_Blue_1.Caption, 15)
                    currRange.Offset(0, 7).Value = Left(TB_Blue_2.Caption, 15)
                    currRange.Offset(0, 8).Value = Left(TB_Yellow_1.Caption, 15)
                    currRange.Offset(0, 9).Value = Left(TB_Yellow_2.Caption, 15)
                    currRange.Offset(0, 10).Value = Left(TB_Magenta_1.Caption, 15)
                    currRange.Offset(0, 11).Value = Left(TB_Magenta_2.Caption, 15)
                    currRange.Offset(0, 12).Value = Left(TB_Aqua_1.Caption, 15)
                    currRange.Offset(0, 13).Value = Left(TB_Aqua_2.Caption, 15)
                    currRange.Offset(0, 14).Value = Left(TB_Maroon_1.Caption, 15)
                    currRange.Offset(0, 15).Value = Left(TB_Maroon_2.Caption, 15)
'                    currRange.Offset(0, 8).Value = TB_Green.text
'                    currRange.Offset(0, 9).Value = TB_Navy.text
'                    currRange.Offset(0, 10).Value = TB_Olive.text
'                    currRange.Offset(0, 11).Value = TB_Purple.text
'                    currRange.Offset(0, 12).Value = TB_Teal.text
'                    currRange.Offset(0, 13).Value = TB_Silver.text
'                    currRange.Offset(0, 14).Value = TB_Gray.text
                End If
                '* foastudio sunyi 2018/10/23 end
            End If
    
        End With
    
    Else
    End If
    
    Unload Me

End Sub

'*******************
'* 補助式ボタン処理
'*******************
Private Sub BT_Assisted_Click()

    If BT_Assisted.Caption = "＋" Then
        BT_Assisted.Caption = StrMinus
        'TB_AssistedArea.Visible = True
    Else
        BT_Assisted.Caption = StrPlus
        'TB_AssistedArea.Visible = False
    End If

End Sub

'*******************
'* 閉じるボタン処理
'*******************
Private Sub BT_Close_Click()

    Unload Me

End Sub

'*********************************
'* 表示テキストボックスを生成する
'*********************************
Private Sub BT_CreateTextBox_Click()

    Dim isFormat            As Integer
    Dim dispColorCond(15)   As String
    '* foastudio sunyi 2018/10/23 start
    Dim dispTextCond(16)   As String
    '* foastudio sunyi 2018/10/23 end
    Dim elementCondList(50) As String
    Dim elementList(50)     As String
    Dim commandListType(50) As Integer
    Dim commandListName(50) As String
    Dim commandListPass(50) As String
    'AISMM No.82 sunyi 20181211 start
    Dim IsNotCreateBox         As Integer
    'AISMM No.82 sunyi 20181211 end

    If Not CB_Link.Value And TB_ItemOperation.text = "" Then
        MsgBox "演算式が入力されていません。"
        Exit Sub
    End If

    '* 表示色条件を取得する
    Call GetDispColorCond(dispColorCond)
    '* foastudio sunyi 2018/10/23 start
    Call GetDispTextCond(dispTextCond)
    '* foastudio sunyi 2018/10/23 end
    Call GetElementCondList(elementCondList)
    Call GetElementList(elementList)
    Call GetCommandListType(commandListType)
    Call GetCommandListName(commandListName)
    Call GetCommandListPass(commandListPass)

    '* テキストボックス生成処理
    If OB_Numeric.Value Then isFormat = 0
    If OB_String.Value Then isFormat = 1
    If OB_Date.Value Then isFormat = 2
    If OB_Time.Value Then isFormat = 3
    If OB_DateTime.Value Then isFormat = 4
    
    If CB_Link.Value Then
        If LV_CommandList.ListItems.Count = 0 Then
            MsgBox "コマンドリストが入力されていません。"
            Exit Sub
        End If
    End If
    
    '* 単一複数条件のフォーマットチェック
    If Not ConditonCheck Then
        MsgBox "演算形式が「個数(単一条件) 」または「個数(複数条件)」の場合、通常のエレメントリストからは選択できません｡エレメント|条件リストから選択してドラッグ＆ドロップ操作してください。 "
        Exit Sub
    End If
    
    Me.Hide
    
    '* 画面の情報を元に入力テキストボックスを生成する
    'AISMM No.82 sunyi 20181211 start
'    Call WriteValueTextBox(TB_ItemOperation.text, isFormat, dispColorCond, CB_Link.Value, _
'                            CB_Element.Value, TB_Name.text, elementCondList, elementList, LV_ElementView.ListItems.Count, _
'                            commandListType, commandListName, commandListPass, LV_CommandList.ListItems.Count, dispTextCond)
    IsNotCreateBox = WriteValueTextBox(TB_ItemOperation.text, isFormat, dispColorCond, CB_Link.Value, _
                            CB_Element.Value, TB_Name.text, elementCondList, elementList, LV_ElementView.ListItems.Count, _
                            commandListType, commandListName, commandListPass, LV_CommandList.ListItems.Count, dispTextCond)
    'AISMM No.82 sunyi 20181211 end
                                
    TB_ItemOperation.text = ""
    'AISMM No.107 yakiyama 20190730 start
    '* 演算形式の初期化
    CB_CalcFormat.Clear
    CB_CalcFormat.AddItem "個数"
    CB_CalcFormat.AddItem "積算"
    CB_CalcFormat.AddItem "平均"
    CB_CalcFormat.AddItem "最大"
    CB_CalcFormat.AddItem "最小"
    CB_CalcFormat.AddItem "種類数"
    CB_CalcFormat.AddItem "個数(単一条件)"
    CB_CalcFormat.AddItem "個数(複数条件)"
    TB_CalcCondition.text = ""
    'AISMM No.107 yakiyama 20190730 end
    TB_ReferPass.text = ""
    TB_Name.text = ""
    '* foastudio sunyi 2018/10/23 start
    '* Call BT_Clear_Click
    TB_White.Caption = ""
    TB_Red.Caption = ""
    TB_Lime.Caption = ""
    TB_Blue.Caption = ""
    TB_Yellow.Caption = ""
    TB_Magenta.Caption = ""
    TB_Aqua.Caption = ""
    TB_Maroon.Caption = ""
    TB_White_1.Caption = ""
    TB_Red_1.Caption = ""
    TB_Lime_1.Caption = ""
    TB_Blue_1.Caption = ""
    TB_Yellow_1.Caption = ""
    TB_Magenta_1.Caption = ""
    TB_Aqua_1.Caption = ""
    TB_Maroon_1.Caption = ""
    TB_White_2.Caption = ""
    TB_Red_2.Caption = ""
    TB_Lime_2.Caption = ""
    TB_Blue_2.Caption = ""
    TB_Yellow_2.Caption = ""
    TB_Magenta_2.Caption = ""
    TB_Aqua_2.Caption = ""
    TB_Maroon_2.Caption = ""
    LV_ElementView.ListItems.Clear
    LV_CommandList.ListItems.Clear
    '* foastudio sunyi 2018/10/23 end
    'AISMM No.82 sunyi 20181211 start
'    Me.Show vbModeless
    If IsNotCreateBox = 0 Then
        Me.Show vbModeless
    Else
        Unload Me
    End If
    'AISMM No.82 sunyi 20181211 end
End Sub

'***********
'* 更新処理
'***********
Private Sub BT_Update_Click()
    Dim dispColorCond(15)   As String
    '* foastudio sunyi 2018/10/23 start
    Dim dispTextCond(16)   As String
    '* foastudio sunyi 2018/10/23 end
    Dim isFormat            As Integer
    Dim elementCondList(50) As String
    Dim elementList(50)     As String
    Dim commandListType(50) As Integer
    Dim commandListName(50) As String
    Dim commandListPass(50) As String
    
    If Not CB_Link.Value And TB_ItemOperation.text = "" Then
        MsgBox "演算式が入力されていません。"
        Exit Sub
    End If

    '* 表示色条件を取得する
    Call GetDispColorCond(dispColorCond)
    '* foastudio sunyi 2018/10/23 start
    Call GetDispTextCond(dispTextCond)
    '* foastudio sunyi 2018/10/23 end
    Call GetElementCondList(elementCondList)
    Call GetElementList(elementList)
    Call GetCommandListType(commandListType)
    Call GetCommandListName(commandListName)
    Call GetCommandListPass(commandListPass)
    
    '* テキストボックス更新処理
    If OB_Numeric.Value Then isFormat = 0
    If OB_String.Value Then isFormat = 1
    If OB_Date.Value Then isFormat = 2
    If OB_Time.Value Then isFormat = 3
    If OB_DateTime.Value Then isFormat = 4
     
    If CB_Link.Value Then
        If LV_CommandList.ListItems.Count = 0 Then
            MsgBox "コマンドリストが入力されていません。"
            Exit Sub
        End If
    End If
    
    '* 単一複数条件のフォーマットチェック
    If Not ConditonCheck Then
        MsgBox "演算形式が「個数(単一条件) 」または「個数(複数条件)」の場合、通常のエレメントリストからは選択できません｡エレメント|条件リストから選択してドラッグ＆ドロップ操作してください。 "
        ActionPaneForm.Show
        Exit Sub
    End If
    
    '* 画面の情報を元に入力テキストボックスを更新する
    Call UpdateValueTextBox(LBL_ShapeName, TB_ItemOperation.text, isFormat, dispColorCond, CB_Link.Value, _
                            CB_Element.Value, TB_Name.text, elementCondList, elementList, LV_ElementView.ListItems.Count, _
                            commandListType, commandListName, commandListPass, LV_CommandList.ListItems.Count, dispTextCond)
    
    Unload Me
    
End Sub

'********************************************
'* 単一複数条件のフォーマットチェック
'********************************************
Function ConditonCheck()
    
    Dim itemOperation  As String
    Dim iType          As Integer
    
    ConditonCheck = True
    
    iType = 0
    itemOperation = TB_ItemOperation.Value
    
    If InStr(itemOperation, "単一条件") > 0 Then
        iType = 1
    ElseIf InStr(itemOperation, "複数条件") > 0 Then
        iType = 2
    End If
    If iType <> 0 Then
        '* AIS-MM No.90 sunyi 20190109 start
        '* ・⇒｜
        'If InStr(itemOperation, "・個数,個数(") = 0 Then
        If InStr(itemOperation, "｜個数,個数(") = 0 Then
        '* AIS-MM No.90 sunyi 20190109 end
            ConditonCheck = False
        End If
    End If
    
End Function

'*************************************************
'* リンクの情報をシートに登録する
'*************************************************
Public Sub WriteValueConditionSheet(formStatus As String, listCount As Integer, currShapeName As String)
    Dim condParamsheet      As Worksheet
    Dim workRange           As Range
    Dim srchRange           As Range
    Dim srchNmRange         As Range
    Dim dispRange           As Range
    Dim II                  As Integer
    
    Call SetFilenameToVariable
    
    Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    With condParamsheet
        Set srchRange = .Range("A:A")
        Set workRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
    
        If Not workRange Is Nothing Then
            '* 更新の場合
            If formStatus = "update" Then
            
              '* 全図形に対して
                For II = 1 To 100
                    Set srchNmRange = .Range("A:A")
                    Set dispRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
                  
                    If Not dispRange Is Nothing Then
                        ' データを削除する
                        Rows(workRange.Row).Delete
                    Else
                        Exit For
                    End If
                Next
            End If
            

            '* 全図形に対して
            For II = 1 To 15000
                '定義内容の有無ﾁｪｯｸ
                If workRange.Offset(II, 0).Value = "" Then
                    For i = 1 To listCount Step -1
                        
                        workRange.Offset(II + i - 1, 0).Value = currShapeName
                        workRange.Offset(II + i - 1, 1).Value = LV_ElementView.ListItems(i).SubItems(1)
                        workRange.Offset(II + i - 1, 2).Value = LV_ElementView.ListItems(i).SubItems(2)
                    Next i
                End If
            Next
        
        End If
    End With
End Sub

'***********************
'* 表示エレメントの追加
'***********************
Private Sub BT_add_Click()

    Dim sValue              As String
    Dim isConditionText     As String
        
    '* テキストボックス更新処理
    If TB_CalcCondition.text = "" Then
        MsgBox "演算式条件が入力されていません。"
        Exit Sub
    Else
        isConditionText = TB_CalcCondition.text
    End If
    
    '* エレメントを追加する
    For i = LV_Element.ListItems.Count To 1 Step -1
        If LV_Element.ListItems(i).Selected Then
        
            sValue = LV_Element.ListItems(i).text
            
            '* ISSUE_NO.671 sunyi 2018/05/25 start
            '* 仕様変更
            For l = LV_ElementView.ListItems.Count To 1 Step -1
                If LV_ElementView.ListItems(l).ListSubItems.Item(1).text = sValue Then
                    LV_ElementView.ListItems(l).ListSubItems.Item(1).text = sValue
                    LV_ElementView.ListItems(l).ListSubItems.Item(2).text = isConditionText
                    LV_ElementView.ListItems(l).ListSubItems.Item(3).text = LB_CTM.text
                    Exit Sub
                End If
            Next l
            '* ISSUE_NO.671 sunyi 2018/05/25 end

            '* 個数(単一条件)時、選択の項目で前のデータを替換え
            If CB_CalcFormat.text = "個数(単一条件)" Then
                For J = LV_ElementView.ListItems.Count To 1 Step -1
                    LV_ElementView.ListItems.Remove J
                Next J
            End If

            Set itm = LV_ElementView.ListItems.Add()
            itm.text = LV_ElementView.ListItems.Count()
            itm.SubItems(1) = sValue
            itm.SubItems(2) = isConditionText
            itm.SubItems(3) = LB_CTM.text
            Exit For
        End If
    Next i
    '* ISSUE_NO.666 sunyi 2018/05/10 start
    '* 吹き出し追加
    LV_ElementView.ControlTipText = itm.SubItems(1) & ":" & itm.SubItems(2)
    '* ISSUE_NO.666 sunyi 2018/05/10 end

End Sub

'***********************
'* 表示エレメントの行削除
'***********************
Private Sub BT_rowdelete_Click()

    For i = LV_ElementView.ListItems.Count To 1 Step -1
        If LV_ElementView.ListItems(i).Selected Then
            LV_ElementView.ListItems.Remove i
            Exit For
        End If
    Next i
    
    For i = LV_ElementView.ListItems.Count To 1 Step -1
        LV_ElementView.ListItems(i).text = i
    Next i
    
    '* ISSUE_NO.666 sunyi 2018/05/29 start
    '* 吹き出し追加
    LV_ElementView.ControlTipText = ""
    If LV_ElementView.ListItems.Count > 0 Then
        LV_ElementView.ControlTipText = LV_ElementView.ListItems.Item(1).SubItems(1) & ":" & LV_ElementView.ListItems.Item(1).SubItems(2)
    End If
    '* ISSUE_NO.666 sunyi 2018/05/29 end
End Sub

'**********************
'* エレメント条件式修正
'**********************
Private Sub LV_ElementView_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim getConditionNameStr      As Variant

    If LV_ElementView.SelectedItem <> 0 Then
        TB_CalcCondition.text = LV_ElementView.ListItems(LV_ElementView.SelectedItem.Index).SubItems(2)
                        
    End If
End Sub

'***********************
'* エレメント条件式の更新
'***********************
Private Sub BT_ConditionUpdate_Click()
    For i = LV_ElementView.ListItems.Count To 1 Step -1
        If LV_ElementView.ListItems(i).Selected Then

            LV_ElementView.ListItems(i).SubItems(2) = TB_CalcCondition.text
            Exit For
        End If
    Next i
    '* ISSUE_NO.666 sunyi 2018/05/29 start
    '* 吹き出し追加
    LV_ElementView.ControlTipText = ""
    If LV_ElementView.ListItems.Count > 0 Then
        LV_ElementView.ControlTipText = LV_ElementView.SelectedItem.SubItems(1) & ":" & LV_ElementView.SelectedItem.SubItems(2)
    End If
    '* ISSUE_NO.666 sunyi 2018/05/29 end
End Sub

'*****************
'* ファイル存在チェック
'*****************
Function IsFileExists(ByVal strFileName As String) As Boolean
    If Dir(strFileName, 16) <> Empty Then
        IsFileExists = True
    Else
        IsFileExists = False
    End If
End Function

'************************
'* コマンドリストを追加
'************************
Private Sub BT_ComListAdd_Click()
    
    Dim splitStr            As Variant
    Dim displayValue        As String
    Dim fileName            As String
    Dim fileFullName        As String
    Dim linkSypeValue       As String
    Dim ofsFileSys          As Object
    Dim file                As Object
    
    displayValue = TB_ReferPass.text
    
    If displayValue = "" Then
        MsgBox "リンク先が入力されていません。"
        Exit Sub
    End If
    
    bHasFlg = False
    For i = LV_CommandList.ListItems.Count To 1 Step -1
        If displayValue = LV_CommandList.ListItems(i).SubItems(2) Then
            MsgBox "重複な内容が入力されています。"
            bHasFlg = True
            Exit For
        End If
    Next i

On Error GoTo errlable
    '* コマンドリストを追加する
    If Not bHasFlg Then
        '* Link種類を指定
        If OB_StatusMonitor.Value Then
    
            linkSypeValue = LINKBOX_TYPE_AIS
            'ダブルクォーテーションがある場合
            If InStr(displayValue, """") > 0 Then
                splitStr = Split(displayValue, """")
                fileFullName = splitStr(1)
            Else
                fileFullName = displayValue
            End If

            Set ofsFileSys = CreateObject("Scripting.FileSystemObject")
            Set file = ofsFileSys.GetFile(fileFullName)
            fileName = file.Name

        ElseIf OB_URL.Value Then
    
            linkSypeValue = LINKBOX_TYPE_URL
            fileName = displayValue
            fileFullName = displayValue
            
        ElseIf OB_App.Value Then
        
            linkSypeValue = LINKBOX_TYPE_APP
            'ダブルクォーテーションがある場合
            If InStr(displayValue, """") > 0 Then
                splitStr = Split(displayValue, """")
                fileFullName = splitStr(1)
            Else
                splitStr = Split(displayValue, Chr(9))
                splitStr = Split(splitStr(0), Chr(32))
                fileFullName = splitStr(0)
            End If

            Set ofsFileSys = CreateObject("Scripting.FileSystemObject")
            Set file = ofsFileSys.GetFile(fileFullName)
            fileName = file.Name
        End If
        
        Set itm = LV_CommandList.ListItems.Add()
        itm.text = linkSypeValue
        itm.SubItems(1) = fileName
        itm.SubItems(2) = displayValue
        TB_ReferPass.text = ""
        
        '* ISSUE_NO.666 sunyi 2018/05/10 start
        '* 吹き出し追加
        LV_CommandList.ControlTipText = itm.text & ":" & itm.SubItems(1)
        '* ISSUE_NO.666 sunyi 2018/05/10 end
        '* コマンドリストを追加する
    End If
    
errlable:
        If Err.Number <> 0 Then
            MsgBox "申し訳ございません。" & fileFullName & "が見つかりません。"
        End If
End Sub

'*************************
'* コマンドリストトの更新
'*************************
Private Sub BT_ComListUpd_Click()

    Dim splitStr            As Variant
    Dim displayValue        As String
    Dim fileName            As String
    Dim fileFullName        As String
    Dim linkSypeValue       As String
    Dim ofsFileSys          As Object
    Dim file                As Object
    
    displayValue = TB_ReferPass.text
    
    If displayValue = "" Then
        MsgBox "リンク先が入力されていません。"
        Exit Sub
    End If
    
    bHasFlg = False
    For i = LV_CommandList.ListItems.Count To 1 Step -1
        If displayValue = LV_CommandList.ListItems(i).SubItems(2) Then
            MsgBox "重複な内容が入力されています。"
            bHasFlg = True
            Exit For
        End If
    Next i
    
    
    
On Error GoTo errlable
    '* コマンドリストを追加する
    If Not bHasFlg Then
    
        For i = 1 To LV_CommandList.ListItems.Count
            If LV_CommandList.ListItems(i).Selected Then
            
                '* Link種類を指定
                If OB_StatusMonitor.Value Then
            
                    linkSypeValue = LINKBOX_TYPE_AIS
                    'ダブルクォーテーションがある場合
                    If InStr(displayValue, """") > 0 Then
                        splitStr = Split(displayValue, """")
                        fileFullName = splitStr(1)
                    Else
                        fileFullName = displayValue
                    End If
        
                    Set ofsFileSys = CreateObject("Scripting.FileSystemObject")
                    Set file = ofsFileSys.GetFile(fileFullName)
                    fileName = file.Name
        
                ElseIf OB_URL.Value Then
            
                    linkSypeValue = LINKBOX_TYPE_URL
                    fileName = displayValue
                    fileFullName = displayValue
                    
                ElseIf OB_App.Value Then
                
                    linkSypeValue = LINKBOX_TYPE_APP
                    'ダブルクォーテーションがある場合
                    If InStr(displayValue, """") > 0 Then
                        splitStr = Split(displayValue, """")
                        fileFullName = splitStr(1)
                    Else
                        splitStr = Split(displayValue, Chr(9))
                        splitStr = Split(splitStr(0), Chr(32))
                        fileFullName = splitStr(0)
                    End If
        
                    Set ofsFileSys = CreateObject("Scripting.FileSystemObject")
                    Set file = ofsFileSys.GetFile(fileFullName)
                    fileName = file.Name
                End If
        
                '* コマンドリストを更新する
                LV_CommandList.ListItems(i).text = linkSypeValue
                LV_CommandList.ListItems(i).SubItems(1) = fileName
                LV_CommandList.ListItems(i).SubItems(2) = displayValue
                TB_ReferPass.text = ""
                
                '* ISSUE_NO.666 sunyi 2018/05/29 start
                '* 吹き出し追加
                LV_CommandList.ControlTipText = LV_CommandList.ListItems(i).text & ":" & LV_CommandList.ListItems(i).SubItems(1)
                '* ISSUE_NO.666 sunyi 2018/05/29 end
                '* コマンドリストを追加する
            End If
        Next i
    End If
    
errlable:
        If Err.Number <> 0 Then
            MsgBox "申し訳ございません。" & fileFullName & "が見つかりません。"
        End If
End Sub

'***************************
'* コマンドリストトの行削除
'***************************
Private Sub BT_ComListDel_Click()

    For i = LV_CommandList.ListItems.Count To 1 Step -1
        If LV_CommandList.ListItems(i).Selected Then
            LV_CommandList.ListItems.Remove i
            Exit For
        End If
    Next i
    
    '* ISSUE_NO.666 sunyi 2018/05/29 start
    '* 吹き出し追加
    LV_CommandList.ControlTipText = ""
    If LV_CommandList.ListItems.Count > 0 Then
        LV_CommandList.ControlTipText = LV_CommandList.ListItems.Item(1).text & ":" & LV_CommandList.ListItems.Item(1).SubItems(1)
    End If
    '* ISSUE_NO.666 sunyi 2018/05/29 end
End Sub

'***************************
'* コマンドリストトのクリック
'***************************
Private Sub LV_CommandList_DblClick()

    For i = 1 To LV_CommandList.ListItems.Count
        If LV_CommandList.ListItems(i).Selected Then
        
            If LV_CommandList.ListItems(i).text = LINKBOX_TYPE_AIS Then OB_StatusMonitor.Value = True
            If LV_CommandList.ListItems(i).text = LINKBOX_TYPE_URL Then OB_URL.Value = True
            If LV_CommandList.ListItems(i).text = LINKBOX_TYPE_APP Then OB_App.Value = True
            
            TB_ReferPass.text = LV_CommandList.ListItems(i).SubItems(2)
            Exit For
        End If
    Next i
End Sub


Private Sub CB_Element_Click()
    If CB_Element.Value = False Then
        TB_Name.Enabled = False
    Else
        TB_Name.Enabled = True
    End If
End Sub

Private Sub CB_Link_Click()
    If CB_Link.Value = False Then
        OB_StatusMonitor.Enabled = False
        OB_URL.Enabled = False
        OB_App.Enabled = False
        TB_ReferPass.Enabled = False
        CB_Refer.Enabled = False
        BT_ComListUpd.Enabled = False
        BT_ComListAdd.Enabled = False
        BT_ComListDel.Enabled = False
        LV_CommandList.Enabled = False
    Else
        OB_StatusMonitor.Enabled = True
        OB_URL.Enabled = True
        OB_App.Enabled = True
        TB_ReferPass.Enabled = True
        CB_Refer.Enabled = True
        BT_ComListUpd.Enabled = True
        BT_ComListAdd.Enabled = True
        BT_ComListDel.Enabled = True
        LV_CommandList.Enabled = True
    End If
End Sub

Private Sub CB_Refer_Click()

    Dim missionWorksheet  As Worksheet
    Dim paramWorksheet    As Worksheet
    Dim srchRange         As Range
    Dim currRange         As Range
    Dim proxyServer       As String
    Dim proxyServerUI     As String
    Dim IPAddr            As String
    Dim PortNo            As String
    Dim fileName          As Variant
        
    If OB_App.Value Then
        fileName = Application.GetOpenFilename("実行ファイル ,*.exe") '2018/03/06 UPD_No.35.文件→実行ファイル
        If fileName <> False Then
            If InStr(fileName, "CTMRefineUI.exe") > 0 Then
                'CTMForm.Show
                Set missionWorksheet = Workbooks(thisBookName).Worksheets(MissionSheetName)
                Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
                With paramWorksheet
                    '* サーバIPアドレス
                    Set srchRange = .Range("A:A")
                    Set currRange = srchRange.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
                    If Not currRange Is Nothing Then
                        IPAddr = currRange.Offset(0, 1).Value
                    End If
                    '*サーバポート番号
                    Set srchRange = .Range("A:A")
                    Set currRange = srchRange.Find(What:="サーバポート番号", LookAt:=xlWhole)
                    If Not currRange Is Nothing Then
                        PortNo = currRange.Offset(0, 1).Value
                    End If
                    '* ProxyServer使用
                    Set srchRange = .Range("A:A")
                    Set currRange = srchRange.Find(What:="ProxyServer使用", LookAt:=xlWhole)
                    If Not currRange Is Nothing Then
                        proxyServer = currRange.Offset(0, 1).Value
                    End If
                    '* ProxyServerURI
                    Set srchRange = .Range("A:A")
                    Set currRange = srchRange.Find(What:="ProxyServerURI", LookAt:=xlWhole)
                    If Not currRange Is Nothing Then
                        proxyServerUI = currRange.Offset(0, 1).Value
                    End If
                End With
                                
                'TB_ReferPass.text = """" & fileName & """" & Chr(32) & ipAddr & Chr(32) & portNo & Chr(32) & missionWorksheet.Range("D1") & Chr(32) & proxyServer & Chr(32) & proxyServerUI
                TB_ReferPass.text = """" & fileName & """" & Chr(32) & "-h" & Chr(32) & IPAddr & Chr(32) & "-p" & Chr(32) & PortNo & Chr(32) & "-ctm" & Chr(32) & missionWorksheet.Range("D1") & Chr(32) & "-proxy" & Chr(32) & proxyServer & Chr(32) & "-uri" & Chr(32) & proxyServerUI
            Else
                TB_ReferPass.text = """" & fileName & """"
            End If
        End If
    ElseIf OB_StatusMonitor.Value Then
        SearchForm.Show
    End If
End Sub

'***********************
'* ドラッグ完了時の処理
'***********************
Private Sub LV_Element_OLECompleteDrag(Effect As Long)
    Debug.Print "OLECompleteDrag"
    Effect = vbDropEffectCopy
End Sub

'*****************************************************
'* リストボックスからテキストボックスへのドラッグ処理
'*****************************************************
Private Sub LV_Element_OLEStartDrag(data As MSComctlLib.DataObject, AllowedEffects As Long)
    Dim workStr         As Variant
    Dim getTextStr      As Variant
    Debug.Print "OLEStartDrag"
    
    '* データオブジェクトの内容をクリア
    data.Clear
    
    '* ISSUE_NO.671 sunyi 2018/05/28 start
    '* 仕様変更
    If CB_CalcFormat.text = "個数(単一条件)" Or CB_CalcFormat.text = "個数(複数条件)" Then
        Exit Sub
    End If
    '* ISSUE_NO.671 sunyi 2018/05/28 end
    
    '* ドラッグ開始時のリストボックスの選択項目を取得
    getTextStr = LV_Element.SelectedItem.text
    
    ' Processing On Server dn 2018/09/10 start
    ' Grip場合、シート名も取得できるため修正
    '* 貼り付ける文字列の生成
    If CB_CalcFormat.text = "" Then
        ' workStr = """"  & LB_CTM.text & "・" & getTextStr & """"
        'マルチモニター No.58 sunyi 2018/11/20
        ' "・"⇒"｜"
        'workStr = """" & LB_Mission.text & "・" & LB_CTM.text & "・" & getTextStr & """"
        workStr = """" & LB_Mission.text & "｜" & LB_CTM.text & "｜" & getTextStr & """"
        
        ''''workStr = """" & LB_CTM.text & "_" & getTextStr & """"
    Else
        'workStr = """"  & LB_CTM.text & "・" & getTextStr & "," & CB_CalcFormat.text & """"
        'マルチモニター No.58 sunyi 2018/11/20
        ' "・"⇒"｜"
        'workStr = """" & LB_Mission.text & "・" & LB_CTM.text & "・" & getTextStr & "," & CB_CalcFormat.text & """"
        workStr = """" & LB_Mission.text & "｜" & LB_CTM.text & "｜" & getTextStr & "," & CB_CalcFormat.text & """"
        
        ''''workStr = """" & LB_CTM.text & "_" & getTextStr & "," & CB_CalcFormat.text & """"
    End If
    ' Processing On Server dn 2018/09/10 end
    
    '* データオブジェクトにセット
    data.SetData workStr, vbCFtext
    
End Sub

'********************************
'* 演算式条件ドラッグ完了時の処理
'********************************
Private Sub LV_ElementView_OLECompleteDrag(Effect As Long)
    Debug.Print "OLECompleteDrag"
    Effect = vbDropEffectCopy
End Sub

'**************************************************************
'* 演算式条件リストボックスからテキストボックスへのドラッグ処理
'**************************************************************
Private Sub LV_ElementView_OLEStartDrag(data As MSComctlLib.DataObject, AllowedEffects As Long)
    Dim workView            As Variant
    Dim workViewStr         As Variant
    Dim getViewNameStr      As Variant

    Debug.Print "OLEStartDrag"
    
    '* データオブジェクトの内容をクリア
    data.Clear
    
    '* ドラッグ開始時のリストボックスの選択項目を取得
    getViewNameStr = LV_ElementView.SelectedItem.SubItems(1)

    '* 貼り付ける文字列の生成
    If TB_CalcCondition.text = "" Then
        MsgBox "演算式条件が入力されていません。"
        Exit Sub
    End If

    For i = LV_ElementView.ListItems.Count To 1 Step -1
        If workView = "" Then
            workView = LV_ElementView.ListItems(i).SubItems(1) & ":" & LV_ElementView.ListItems(i).SubItems(2)
        Else
            workView = workView & ";" & LV_ElementView.ListItems(i).SubItems(1) & ":" & LV_ElementView.ListItems(i).SubItems(2)
        End If
    Next i
    ' Processing On Server dn 2018/09/10 start
    '* 製品1ライン状態・個数(固定値),個数(XX条件)[エレメント名:条件;エレメント名2:条件2]
    'workViewStr = """" & LB_CTM.text & "・" & "個数" & "," & CB_CalcFormat.text & "[" & workView & "]" & """"

    ' Grip場合、シート名も取得できるため修正
    'マルチモニター No.58 sunyi 2018/11/20
        ' "・"⇒"｜"
    'workViewStr = """" & LB_Mission.text & "・" & LB_CTM.text & "・" & "個数" & "," & CB_CalcFormat.text & "[" & workView & "]" & """"
    workViewStr = """" & LB_Mission.text & "｜" & LB_CTM.text & "｜" & "個数" & "," & CB_CalcFormat.text & "[" & workView & "]" & """"
    
    ' Processing On Server dn 2018/09/10 end
    
    '* データオブジェクトにセット
    data.SetData workViewStr, vbCFtext

End Sub

'********************
'* 演算形式リスト変更
'********************
Private Sub CB_CalcFormat_Change()
    LV_ElementView.Top = 84
    LV_ElementView.Left = 222
    If CB_CalcFormat.text = "個数(単一条件)" Then

        Call SetDisplayStatus("1")
        
        '* 複数条件→単一条件に変更時、一番上のデータのみを保留
        For i = LV_ElementView.ListItems.Count To 2 Step -1
            LV_ElementView.ListItems.Remove i
        Next i
        Call SetDisplayStatus("1")
        
    ElseIf CB_CalcFormat.text = "個数(複数条件)" Then
        Call SetDisplayStatus("1")
    Else
    
            Call SetDisplayStatus("0")
    End If
    
End Sub

'****************************************************
'* 演算形式が個数(単一条件)と個数(複数条件)の画面設定
'****************************************************
Private Sub SetDisplayStatus(sFlg As String)

    If sFlg = "1" Then
        
        '* 「エレメント追加」ボタンを表示
        BT_add.Visible = True
        
        '* 「エレメント削除」ボタンを表示
        BT_rowdelete.Visible = True
        
        '* 「条件更新」ボタンを表示
        BT_ConditionUpdate.Visible = True
        
        '* 「条件」テキストボックスを表示
        TB_CalcCondition.Visible = True
        
        '* 「条件」ラベルを表示
        Label23.Visible = True
        
        '*ISSUE_NO.671 sunyi 2018/05/29 start
        '*仕様変更
        '* foastudio sunyi 2018/10/23 start
        '* LV_ElementView.Top = 294
        Frame5.Visible = True
        'LV_ElementView.Top = 84
        '* foastudio sunyi 2018/10/23 start
        'LV_ElementView.Left = 222
        '*ISSUE_NO.671 sunyi 2018/05/29 end
        
        '* 「エレメント」リストビューを表示
        LV_ElementView.Top = 0
        LV_ElementView.Left = 0
        LV_ElementView.Visible = True
        
        '* リストビューのへーだー「エレメント」ラベルを表示
        Label25.Visible = True
        
        '* リストビューのへーだー「条件」ラベルを表示
        Label24.Visible = True
    Else
        
        '* 「エレメント追加」ボタンを非表示
        BT_add.Visible = False
        
        '* 「エレメント削除」ボタンを非表示
        BT_rowdelete.Visible = False
        
        '* 「条件更新」ボタンを非表示
        BT_ConditionUpdate.Visible = False
        
        '* 「条件」テキストボックスを非表示
        TB_CalcCondition.Visible = False
        
        '* 「条件」ラベルを非表示
        Label23.Visible = False
        
        '* 「エレメント」リストビューを非表示
        LV_ElementView.Top = 0
        LV_ElementView.Left = 0
        LV_ElementView.Visible = False
        Frame5.Visible = False
        
        '* リストビューのへーだー「エレメント」ラベルを非表示
        Label25.Visible = False
        
        '* リストビューのへーだー「条件」ラベルを非表示
        Label24.Visible = False
    End If
    ActionPaneForm.Frame3.Repaint
End Sub

'* ISSUE_NO.666 sunyi 2018/05/10 start
'* 吹き出し追加
Private Sub TB_CalcCondition_Change()
    TB_CalcCondition.ControlTipText = TB_CalcCondition.text
End Sub

Private Sub TB_ItemOperation_Change()
    TB_ItemOperation.ControlTipText = TB_ItemOperation.text
End Sub

Private Sub TB_ReferPass_Change()
    TB_ReferPass.ControlTipText = TB_ReferPass.text
End Sub

Private Sub LV_ElementView_Click()
    If LV_ElementView.ListItems.Count > 0 Then
        LV_ElementView.ControlTipText = LV_ElementView.SelectedItem.SubItems(1) & ":" & LV_ElementView.SelectedItem.SubItems(2)
    End If
End Sub

Private Sub LV_Element_Click()
    If LV_Element.ListItems.Count > 0 Then
        LV_Element.ControlTipText = LV_Element.SelectedItem.text
    End If
End Sub

Private Sub LV_CommandList_Click()
    If LV_CommandList.ListItems.Count > 0 Then
        LV_CommandList.ControlTipText = LV_CommandList.SelectedItem.text & ":" & LV_CommandList.SelectedItem.SubItems(1)
    End If
End Sub
'* ISSUE_NO.666 sunyi 2018/05/10 end

'* foastudio sunyi 2018/10/23 start
Private Sub TB_White_B_1_Click()
    TB_White_B_1.Visible = False
    TB_White_B_2.Visible = True
    TB_White_1.Visible = False
    TB_White_2.Visible = True
    Call TB_White_B_2.SetFocus
End Sub

Private Sub TB_White_B_2_Click()
    TB_White_B_1.Visible = True
    TB_White_B_2.Visible = False
    TB_White_1.Visible = True
    TB_White_2.Visible = False
    Call TB_White_B_1.SetFocus
End Sub

Private Sub TB_Yellow_B_1_Click()
    TB_Yellow_B_1.Visible = False
    TB_Yellow_B_2.Visible = True
    TB_Yellow_1.Visible = False
    TB_Yellow_2.Visible = True
    Call TB_Yellow_B_2.SetFocus
End Sub

Private Sub TB_Yellow_B_2_Click()
    TB_Yellow_B_1.Visible = True
    TB_Yellow_B_2.Visible = False
    TB_Yellow_1.Visible = True
    TB_Yellow_2.Visible = False
    Call TB_Yellow_B_1.SetFocus
End Sub

Private Sub TB_Lime_B_1_Click()
    TB_Lime_B_1.Visible = False
    TB_Lime_B_2.Visible = True
    TB_Lime_1.Visible = False
    TB_Lime_2.Visible = True
    Call TB_Lime_B_2.SetFocus
End Sub

Private Sub TB_Lime_B_2_Click()
    TB_Lime_B_1.Visible = True
    TB_Lime_B_2.Visible = False
    TB_Lime_1.Visible = True
    TB_Lime_2.Visible = False
    Call TB_Lime_B_1.SetFocus
End Sub

Private Sub TB_Magenta_B_1_Click()
    TB_Magenta_B_1.Visible = False
    TB_Magenta_B_2.Visible = True
    TB_Magenta_1.Visible = False
    TB_Magenta_2.Visible = True
    Call TB_Magenta_B_2.SetFocus
End Sub

Private Sub TB_Magenta_B_2_Click()
    TB_Magenta_B_1.Visible = True
    TB_Magenta_B_2.Visible = False
    TB_Magenta_1.Visible = True
    TB_Magenta_2.Visible = False
    Call TB_Magenta_B_1.SetFocus
End Sub

Private Sub TB_Maroon_B_1_Click()
    TB_Maroon_B_1.Visible = False
    TB_Maroon_B_2.Visible = True
    TB_Maroon_1.Visible = False
    TB_Maroon_2.Visible = True
    Call TB_Maroon_B_2.SetFocus
End Sub

Private Sub TB_Maroon_B_2_Click()
    TB_Maroon_B_1.Visible = True
    TB_Maroon_B_2.Visible = False
    TB_Maroon_1.Visible = True
    TB_Maroon_2.Visible = False
    Call TB_Maroon_B_1.SetFocus
End Sub

Private Sub TB_Red_B_1_Click()
    TB_Red_B_1.Visible = False
    TB_Red_B_2.Visible = True
    TB_Red_1.Visible = False
    TB_Red_2.Visible = True
    Call TB_Red_B_2.SetFocus
End Sub

Private Sub TB_Red_B_2_Click()
    TB_Red_B_1.Visible = True
    TB_Red_B_2.Visible = False
    TB_Red_1.Visible = True
    TB_Red_2.Visible = False
    Call TB_Red_B_1.SetFocus
End Sub

Private Sub TB_Aqua_B_1_Click()
    TB_Aqua_B_1.Visible = False
    TB_Aqua_B_2.Visible = True
    TB_Aqua_1.Visible = False
    TB_Aqua_2.Visible = True
    Call TB_Aqua_B_2.SetFocus
End Sub

Private Sub TB_Aqua_B_2_Click()
    TB_Aqua_B_1.Visible = True
    TB_Aqua_B_2.Visible = False
    TB_Aqua_1.Visible = True
    TB_Aqua_2.Visible = False
    Call TB_Aqua_B_1.SetFocus
End Sub

Private Sub TB_Blue_B_1_Click()
    TB_Blue_B_1.Visible = False
    TB_Blue_B_2.Visible = True
    TB_Blue_1.Visible = False
    TB_Blue_2.Visible = True
    Call TB_Blue_B_2.SetFocus
End Sub

Private Sub TB_Blue_B_2_Click()
    TB_Blue_B_1.Visible = True
    TB_Blue_B_2.Visible = False
    TB_Blue_1.Visible = True
    TB_Blue_2.Visible = False
    Call TB_Blue_B_1.SetFocus
End Sub

Private Sub TB_White_1_Change()
    If Len(TB_White_1.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If
End Sub

Private Sub TB_Lime_1_Change()
    If Len(TB_Lime_1.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If
End Sub

Private Sub TB_Magenta_1_Change()
    If Len(TB_Magenta_1.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If
End Sub

Private Sub TB_Maroon_1_Change()
    If Len(TB_Maroon_1.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If

End Sub

Private Sub TB_Red_1_Change()
    If Len(TB_Red_1.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If

End Sub

Private Sub TB_Aqua_1_Change()
    If Len(TB_Aqua_1.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If

End Sub

Private Sub TB_Blue_1_Change()
    If Len(TB_Blue_1.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If

End Sub

Private Sub TB_Yellow_1_Change()
    If Len(TB_Yellow_1.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If
End Sub

Private Sub TB_White_2_Change()
    If Len(TB_White_2.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If
End Sub

Private Sub TB_Lime_2_Change()
    If Len(TB_Lime_2.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If
End Sub

Private Sub TB_Magenta_2_Change()
    If Len(TB_Magenta_2.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If
End Sub

Private Sub TB_Maroon_2_Change()
    If Len(TB_Maroon_2.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If

End Sub

Private Sub TB_Red_2_Change()
    If Len(TB_Red_2.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If

End Sub

Private Sub TB_Aqua_2_Change()
    If Len(TB_Aqua_2.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If

End Sub

Private Sub TB_Blue_2_Change()
    If Len(TB_Blue_2.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If

End Sub

Private Sub TB_Yellow_2_Change()
    If Len(TB_Yellow_2.text) > 15 Then
        MsgBox ("15文字までを入力してください。")
    End If
End Sub
'* foastudio sunyi 2018/10/23 end

Private Sub TB_White_2_Click()

End Sub

'* sunyi 2018/11/16 start
Private Sub TB_White_Click()
    
    Dim arr As Variant
    
    Load ActionPaneSettingForm
    With ActionPaneSettingForm
        
        .Label6.Caption = "TB_White"
        
        Call Combobox_Initialize
        
        .Label4.BackColor = Label11.BackColor
        .Label4.BackColor = Label11.BackColor
                                
        If TB_White.Caption <> "" Then
            arr = Split(TB_White.Caption, .Label_AND.Caption)
            arr1 = Split(arr(0), Right(arr(0), 1))
            arr2 = Split(arr(1), Left(arr(1), 1))
            If (arr(0) <> "") Then
                .ComboBox1.text = Right(arr(0), 1)
                .Setting_1.Value = arr1(0)
            End If
            If arr(1) <> "" Then
                .ComboBox2.text = Left(arr(1), 1)
                .Setting_2.Value = arr2(1)
            End If
        End If
        
        If TB_White_1 <> "" And TB_White_2 <> "" Then
            .Setting_3.Value = TB_White_1.Caption
            .Setting_4.Value = TB_White_2.Caption
        ElseIf TB_White_1 <> "" Then
            .Setting_3.Value = TB_White_1.Caption
        End If
        
        If OB_Numeric.Value Then
            .Cb_IsNumericFormat.Value = True
        Else
            .Cb_IsNumericFormat.Value = False
        End If
        
        .Show vbModeless
    End With
    
End Sub

Private Sub TB_Red_Click()
    Dim arr As Variant
    Dim arr1 As Variant
    Dim arr2 As Variant
    
    Load ActionPaneSettingForm
    With ActionPaneSettingForm
        
        .Label6.Caption = "TB_Red"
        
        Call Combobox_Initialize
        
        .Label4.BackColor = Label9.BackColor
        .Label4.BackColor = Label9.BackColor
                                
        If TB_Red.Caption <> "" Then
            arr = Split(TB_Red.Caption, .Label_AND.Caption)
            arr1 = Split(arr(0), Right(arr(0), 1))
            arr2 = Split(arr(1), Left(arr(1), 1))
            If (arr(0) <> "") Then
                .ComboBox1.text = Right(arr(0), 1)
                .Setting_1.Value = arr1(0)
            End If
            If arr(1) <> "" Then
                .ComboBox2.text = Left(arr(1), 1)
                .Setting_2.Value = arr2(1)
            End If
        End If
        
        If TB_Red_1 <> "" And TB_Red_2 <> "" Then
            .Setting_3.Value = TB_Red_1.Caption
            .Setting_4.Value = TB_Red_2.Caption
        ElseIf TB_Red_1 <> "" Then
            .Setting_3.Value = TB_Red_1.Caption
        End If
        
        If OB_Numeric.Value Then
            .Cb_IsNumericFormat.Value = True
        Else
            .Cb_IsNumericFormat.Value = False
        End If
        
        .Show vbModeless
    End With
    
End Sub

Private Sub TB_Lime_Click()
    
    Dim arr As Variant
    
    Load ActionPaneSettingForm
    With ActionPaneSettingForm
        
        .Label6.Caption = "TB_Lime"
        
        Call Combobox_Initialize
        
        .Label4.BackColor = Label8.BackColor
        .Label4.BackColor = Label8.BackColor
                                
        If TB_Lime.Caption <> "" Then
            arr = Split(TB_Lime.Caption, .Label_AND.Caption)
            arr1 = Split(arr(0), Right(arr(0), 1))
            arr2 = Split(arr(1), Left(arr(1), 1))
            If (arr(0) <> "") Then
                .ComboBox1.text = Right(arr(0), 1)
                .Setting_1.Value = arr1(0)
            End If
            If arr(1) <> "" Then
                .ComboBox2.text = Left(arr(1), 1)
                .Setting_2.Value = arr2(1)
            End If
        End If
        
        If TB_Lime_1 <> "" And TB_Lime_2 <> "" Then
            .Setting_3.Value = TB_Lime_1.Caption
            .Setting_4.Value = TB_Lime_2.Caption
        ElseIf TB_Lime_1 <> "" Then
            .Setting_3.Value = TB_Lime_1.Caption
        End If
        
        If OB_Numeric.Value Then
            .Cb_IsNumericFormat.Value = True
        Else
            .Cb_IsNumericFormat.Value = False
        End If
        
        .Show vbModeless
    End With
    
End Sub

Private Sub TB_Blue_Click()
    
    Dim arr As Variant
    
    Load ActionPaneSettingForm
    With ActionPaneSettingForm
        
        .Label6.Caption = "TB_Blue"
        
        Call Combobox_Initialize
        
        .Label4.BackColor = Label10.BackColor
        .Label4.BackColor = Label10.BackColor
                                
        If TB_Blue.Caption <> "" Then
            arr = Split(TB_Blue.Caption, .Label_AND.Caption)
            arr1 = Split(arr(0), Right(arr(0), 1))
            arr2 = Split(arr(1), Left(arr(1), 1))
            If (arr(0) <> "") Then
                .ComboBox1.text = Right(arr(0), 1)
                .Setting_1.Value = arr1(0)
            End If
            If arr(1) <> "" Then
                .ComboBox2.text = Left(arr(1), 1)
                .Setting_2.Value = arr2(1)
            End If
        End If
        
        If TB_Blue_1 <> "" And TB_Blue_2 <> "" Then
            .Setting_3.Value = TB_Blue_1.Caption
            .Setting_4.Value = TB_Blue_2.Caption
        ElseIf TB_Blue_1 <> "" Then
            .Setting_3.Value = TB_Blue_1.Caption
        End If
        
        If OB_Numeric.Value Then
            .Cb_IsNumericFormat.Value = True
        Else
            .Cb_IsNumericFormat.Value = False
        End If
        
        .Show vbModeless
    End With
    
End Sub

Private Sub TB_Yellow_Click()
    
    Dim arr As Variant
    
    Load ActionPaneSettingForm
    With ActionPaneSettingForm
        
        .Label6.Caption = "TB_Yellow"
        
        Call Combobox_Initialize
        
        .Label4.BackColor = Label12.BackColor
        .Label4.BackColor = Label12.BackColor
                                
        If TB_Yellow.Caption <> "" Then
            arr = Split(TB_Yellow.Caption, .Label_AND.Caption)
            arr1 = Split(arr(0), Right(arr(0), 1))
            arr2 = Split(arr(1), Left(arr(1), 1))
            If (arr(0) <> "") Then
                .ComboBox1.text = Right(arr(0), 1)
                .Setting_1.Value = arr1(0)
            End If
            If arr(1) <> "" Then
                .ComboBox2.text = Left(arr(1), 1)
                .Setting_2.Value = arr2(1)
            End If
        End If
        
        If TB_Yellow_1 <> "" And TB_Yellow_2 <> "" Then
            .Setting_3.Value = TB_Yellow_1.Caption
            .Setting_4.Value = TB_Yellow_2.Caption
        ElseIf TB_Yellow_1 <> "" Then
            .Setting_3.Value = TB_Yellow_1.Caption
        End If
        
        If OB_Numeric.Value Then
            .Cb_IsNumericFormat.Value = True
        Else
            .Cb_IsNumericFormat.Value = False
        End If
        
        .Show vbModeless
    End With
    
End Sub

Private Sub TB_Magenta_Click()
    
    Dim arr As Variant
    
    Load ActionPaneSettingForm
    With ActionPaneSettingForm
        
        .Label6.Caption = "TB_Magenta"
        
        Call Combobox_Initialize
        
        .Label4.BackColor = Label13.BackColor
        .Label4.BackColor = Label13.BackColor
                                
        If TB_Magenta.Caption <> "" Then
            arr = Split(TB_Magenta.Caption, .Label_AND.Caption)
            arr1 = Split(arr(0), Right(arr(0), 1))
            arr2 = Split(arr(1), Left(arr(1), 1))
            If (arr(0) <> "") Then
                .ComboBox1.text = Right(arr(0), 1)
                .Setting_1.Value = arr1(0)
            End If
            If arr(1) <> "" Then
                .ComboBox2.text = Left(arr(1), 1)
                .Setting_2.Value = arr2(1)
            End If
        End If
        
        If TB_Magenta_1 <> "" And TB_Magenta_2 <> "" Then
            .Setting_3.Value = TB_Magenta_1.Caption
            .Setting_4.Value = TB_Magenta_2.Caption
        ElseIf TB_Magenta_1 <> "" Then
            .Setting_3.Value = TB_Magenta_1.Caption
        End If
        
        If OB_Numeric.Value Then
            .Cb_IsNumericFormat.Value = True
        Else
            .Cb_IsNumericFormat.Value = False
        End If
        
        .Show vbModeless
    End With
    
End Sub

Private Sub TB_Aqua_Click()
    
    Dim arr As Variant
    
    Load ActionPaneSettingForm
    With ActionPaneSettingForm
        
        .Label6.Caption = "TB_Aqua"
        
        Call Combobox_Initialize
        
        .Label4.BackColor = Label14.BackColor
        .Label4.BackColor = Label14.BackColor
                                
        If TB_Aqua.Caption <> "" Then
            arr = Split(TB_Aqua.Caption, .Label_AND.Caption)
            arr1 = Split(arr(0), Right(arr(0), 1))
            arr2 = Split(arr(1), Left(arr(1), 1))
            If (arr(0) <> "") Then
                .ComboBox1.text = Right(arr(0), 1)
                .Setting_1.Value = arr1(0)
            End If
            If arr(1) <> "" Then
                .ComboBox2.text = Left(arr(1), 1)
                .Setting_2.Value = arr2(1)
            End If
        End If
        
        If TB_Aqua_1 <> "" And TB_Aqua_2 <> "" Then
            .Setting_3.Value = TB_Aqua_1.Caption
            .Setting_4.Value = TB_Aqua_2.Caption
        ElseIf TB_Aqua_1 <> "" Then
            .Setting_3.Value = TB_Aqua_1.Caption
        End If
        
        If OB_Numeric.Value Then
            .Cb_IsNumericFormat.Value = True
        Else
            .Cb_IsNumericFormat.Value = False
        End If
        
        .Show vbModeless
    End With
    
End Sub

Private Sub TB_Maroon_Click()
    
    Dim arr As Variant
    
    Load ActionPaneSettingForm
    With ActionPaneSettingForm
        
        .Label6.Caption = "TB_Maroon"
        
        Call Combobox_Initialize
        
        .Label4.BackColor = Label15.BackColor
        .Label4.BackColor = Label15.BackColor
                                
        If TB_Maroon.Caption <> "" Then
            arr = Split(TB_Maroon.Caption, .Label_AND.Caption)
            arr1 = Split(arr(0), Right(arr(0), 1))
            arr2 = Split(arr(1), Left(arr(1), 1))
            If (arr(0) <> "") Then
                .ComboBox1.text = Right(arr(0), 1)
                .Setting_1.Value = arr1(0)
            End If
            If arr(1) <> "" Then
                .ComboBox2.text = Left(arr(1), 1)
                .Setting_2.Value = arr2(1)
            End If
        End If
        
        If TB_Maroon_1 <> "" And TB_Maroon_2 <> "" Then
            .Setting_3.Value = TB_Maroon_1.Caption
            .Setting_4.Value = TB_Maroon_2.Caption
        ElseIf TB_Maroon_1 <> "" Then
            .Setting_3.Value = TB_Maroon_1.Caption
        End If
        
        If OB_Numeric.Value Then
            .Cb_IsNumericFormat.Value = True
        Else
            .Cb_IsNumericFormat.Value = False
        End If
        
        .Show vbModeless
    End With
    
End Sub

Private Sub TB_White_1_Click()
    Call TB_White_Click
End Sub


Private Sub TB_Red_1_Click()
    Call TB_Red_Click
End Sub

Private Sub TB_Lime_1_Click()
    Call TB_Lime_Click
End Sub

Private Sub TB_Blue_1_Click()
    Call TB_Blue_Click
End Sub

Private Sub TB_Yellow_1_Click()
    Call TB_Yellow_Click
End Sub

Private Sub TB_Magenta_1_Click()
    Call TB_Magenta_Click
End Sub

Private Sub TB_Aqua_1_Click()
    Call TB_Aqua_Click
End Sub

Private Sub TB_Maroon_1_Click()
    Call TB_Maroon_Click
End Sub

Private Sub Combobox_Initialize()
    With ActionPaneSettingForm
        .ComboBox1.AddItem "＝"
        .ComboBox1.AddItem "＜"
        .ComboBox1.AddItem "≦"
        .ComboBox2.AddItem "＜"
        .ComboBox2.AddItem "≦"
        
        .Setting_4.Enabled = False
    End With
End Sub
'* sunyi 2018/11/16 end

'*****************
'* フォーム初期化
'*****************
Private Sub UserForm_Initialize()
    Dim missionList()   As MISSIONINFO
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    Dim rgbColor(15)    As Long
    
    '* 補助式展開のボタン初期表示は"＋"
    BT_Assisted.Caption = StrPlus
    
    '* 補助式エリアは非表示
    'TB_AssistedArea.Visible = False
    
    '* ボタンの名前定義
    CB_Refer.Caption = "一覧"
    '* 演算形式の初期化
    CB_CalcFormat.Clear
    CB_CalcFormat.AddItem "個数"
    CB_CalcFormat.AddItem "積算"
    CB_CalcFormat.AddItem "平均"
    CB_CalcFormat.AddItem "最大"
    CB_CalcFormat.AddItem "最小"
    CB_CalcFormat.AddItem "種類数"
    CB_CalcFormat.AddItem "個数(単一条件)"
    CB_CalcFormat.AddItem "個数(複数条件)"

    '* 初期色を取得
    Call GetRGBColorArray(rgbColor)
    Label11.BackColor = rgbColor(0)
    Label9.BackColor = rgbColor(1)
    Label8.BackColor = rgbColor(2)
    Label10.BackColor = rgbColor(3)
    Label12.BackColor = rgbColor(4)
    Label13.BackColor = rgbColor(5)
    Label14.BackColor = rgbColor(6)
    Label15.BackColor = rgbColor(7)
'    Label16.BackColor = rgbColor(8)
'    Label17.BackColor = rgbColor(9)
'    Label18.BackColor = rgbColor(10)
'    Label19.BackColor = rgbColor(11)
'    Label20.BackColor = rgbColor(12)
'    Label21.BackColor = rgbColor(13)
'    Label22.BackColor = rgbColor(14)
    Label12.BackColor = rgbColor(4)
    
    '* チェックボックスの初期値がfalseを設定
    CB_Element.Value = False
    CB_Link.Value = False
    '* 書式は数値がデフォルト
    OB_StatusMonitor.Value = True
    Call CB_Element_Click
    Call CB_Link_Click
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    With LV_Element
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add , "_Element", "エレメント名"
        .ColumnHeaders.Item("_Element").Width = 150
    End With
    
    With LV_ElementView
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add 1, "_Id", "ID", 15
        .ColumnHeaders.Add 2, "_Element", "エレメント", 78
        .ColumnHeaders.Add 3, "_DisplayType", "条件", 34
        .ColumnHeaders.Add 4, "_Ctm", "", 0
        
    End With
    LV_ElementView.ListItems.Clear
    
    With LV_CommandList
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add 1, "_Type", "タイプ", 40
        .ColumnHeaders.Add 2, "_Command", "コマンド", 305
        .ColumnHeaders.Add 3, "_Pass", "パス", 0
    End With
    LV_CommandList.ListItems.Clear
    
    '* 取得したミッション情報のリストボックスへの出力
    LB_Mission.Clear
    For II = 0 To UBound(missionList)
        If missionList(II).Name <> "" Then
                LB_Mission.AddItem missionList(II).Name
        End If
    Next
    ' processing on server dn 2018/09/07 start
    ' Gripの場合
    Set routesObj = GripEntityMap.GetRouteList
    For Each Element In routesObj.keys
         LB_Mission.AddItem (Element)
    Next
    
    'LB_Mission.SetFocus
    'LB_Mission.ListIndex = 0        ' リストボックスの最初の項目を選択状態にする
    If LB_Mission.listCount <> 0 Then
        LB_Mission.SetFocus
        LB_Mission.ListIndex = 0        ' リストボックスの最初の項目を選択状態にする
    End If
    ' processing on server dn 2018/09/07 end
    
    '* ISSUE_NO.666 sunyi 2018/05/10 start
    '* 吹き出し追加
    If LV_Element.ListItems.Count <> 0 Then
        LV_Element.ControlTipText = LV_Element.ListItems.Item(1).text
    End If
    '* ISSUE_NO.666 sunyi 2018/05/10 end
    '* 書式は数値がデフォルト
    OB_Numeric.Value = True

End Sub

'***************************************
'* ミッションリストボックス選択時の処理
'***************************************
Private Sub LB_Mission_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer

    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)

    mIndex = -1
    For II = 0 To UBound(missionList)
        If missionList(II).Name = LB_Mission.text And missionList(II).Name <> "" Then
            mIndex = II
            Exit For
        End If
    Next
        
    'Processing On Server dn 2018/09/07 start
    Dim gripCtmDic As Object
    
    If mIndex = -1 And LB_Mission.text <> "" Then
        Set gripCtmDic = routesObj(LB_Mission.text)
        mIndex = -2
    End If
    'Processing On Server dn 2018/09/07 end

    LB_CTM.Clear
    If mIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM)
            LB_CTM.AddItem missionList(mIndex).CTM(II).Name
        Next
        LB_CTM.SetFocus
        LB_CTM.ListIndex = 0
        
    'Processing On Server dn 2018/09/07 start
    ElseIf mIndex = -2 Then
        For Each Element In gripCtmDic.SubEntities
            LB_CTM.AddItem Element
        Next
        LB_CTM.SetFocus
        LB_CTM.ListIndex = 0
    End If
    'Processing On Server dn 2018/09/07 end
    
    '* ISSUE_NO.666 sunyi 2018/05/10 start
    '* 吹き出し追加
    LB_Mission.ControlTipText = LB_Mission.text
    '* ISSUE_NO.666 sunyi 2018/05/10 end
End Sub

'***************************************
'* CTMリストボックス選択時の処理
'***************************************
Private Sub LB_CTM_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer
    Dim cIndex  As Integer
    
    '*ISSUE_NO.671 sunyi 2018/05/29 start
    '*仕様変更
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim workStr         As String
    '*
    Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="操作バルキ表示Flag", LookAt:=xlWhole)
    If LV_ElementView.ListItems.Count > 0 Then
        If srchRange.Offset(0, 1).Value = "False" Then
            TB_CalcCondition.text = ""
            LV_ElementView.ListItems.Clear
        End If
    End If
    srchRange.Offset(0, 1).Value = "False"
    '*ISSUE_NO.671 sunyi 2018/05/29 end
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    mIndex = -1
    cIndex = -1
    For II = 0 To UBound(missionList)
        If missionList(II).Name <> "" Then
            For JJ = 0 To UBound(missionList(II).CTM)
                If missionList(II).CTM(JJ).Name = LB_CTM.text Then
                    mIndex = II
                    cIndex = JJ
                    Exit For
                End If
            Next
        End If
        If cIndex > -1 Then Exit For
    Next
    
    ' processing on server dn 2018/09/07 start
    Dim gripElementDic As Object
    
    If mIndex = -1 And LB_CTM.text <> "" Then
        Dim gripCtmDic As Object
        Set gripCtmDic = routesObj(LB_Mission.text)
        Set gripElementDic = gripCtmDic.SubEntities(LB_CTM.text)
        mIndex = -2
        cIndex = -2
    End If
    ' processing on server dn 2018/09/07 end
    
    LV_Element.ListItems.Clear
    If mIndex > -1 And cIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM(cIndex).Element)
            LV_Element.ListItems.Add.text = missionList(mIndex).CTM(cIndex).Element(II).Name
        Next
    ' processing on server dn 2018/09/07 start
    ElseIf mIndex = -2 And cIndex = -2 Then
        For Each Element In gripElementDic.SubEntities
            LV_Element.ListItems.Add.text = Element
        Next
    
    End If
    ' processing on server dn 2018/09/07 end
    '* ISSUE_NO.666 sunyi 2018/05/10 start
    '* 吹き出し追加
    LB_CTM.ControlTipText = LB_CTM.text
    '* ISSUE_NO.666 sunyi 2018/05/10 end
End Sub

'**********************
'* ボタン11押下時の処理
'**********************
Private Sub Label11_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(0)
    retcolor = Label_Click(Label11, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B2").Value = retcolor
    End If
    
End Sub

Private Sub Label9_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(1)
    retcolor = Label_Click(Label9, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B3").Value = retcolor
    End If
End Sub

Private Sub Label8_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(2)
    retcolor = Label_Click(Label8, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B4").Value = retcolor
    End If
End Sub

Private Sub Label10_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(3)
    retcolor = Label_Click(Label10, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B5").Value = retcolor
    End If
End Sub

Private Sub Label12_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(4)
    retcolor = Label_Click(Label12, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B6").Value = retcolor
    End If
End Sub

Private Sub Label13_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(5)
    retcolor = Label_Click(Label13, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B7").Value = retcolor
    End If
End Sub

Private Sub Label14_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(6)
    retcolor = Label_Click(Label14, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B8").Value = retcolor
    End If
End Sub

Private Sub Label15_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(7)
    retcolor = Label_Click(Label15, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B9").Value = retcolor
    End If
End Sub

Private Sub Label16_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(8)
    retcolor = Label_Click(Label16, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B10").Value = retcolor
    End If
End Sub

Private Sub Label17_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(9)
    retcolor = Label_Click(Label17, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B11").Value = retcolor
    End If
End Sub

Private Sub Label18_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(10)
    retcolor = Label_Click(Label18, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B12").Value = retcolor
    End If
End Sub

Private Sub Label19_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
 
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(11)
    retcolor = Label_Click(Label19, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B13").Value = retcolor
    End If
End Sub

Private Sub Label20_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(12)
    retcolor = Label_Click(Label20, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B14").Value = retcolor
    End If

End Sub

Private Sub Label21_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)

    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(13)
    retcolor = Label_Click(Label21, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B15").Value = retcolor
    End If

End Sub

Private Sub Label22_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)

    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(14)
    retcolor = Label_Click(Label22, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B16").Value = retcolor
    End If
End Sub

'****************************
'* 表示色ラベルボタン設定処理
'****************************
Public Function Label_Click(LableNm As Object, lcolor As Long) As String
    
    'シートから取得したのデータをRGBに変換する
    b = Int(lcolor / 65536)
    g = Int((lcolor Mod 65536) / 256)
    r = (lcolor Mod 65536) Mod 256
    
    '色を選択したの場合
    If Application.Dialogs(xlDialogEditColor).Show(10, r, g, b) = True Then
        Label_Click = ActiveWorkbook.Colors(10)
        LableNm.BackColor = Label_Click
    Else
        Label_Click = ""
    End If
End Function








