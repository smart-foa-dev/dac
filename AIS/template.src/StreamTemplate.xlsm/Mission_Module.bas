Attribute VB_Name = "Mission_Module"
Option Explicit

'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
Public Sub GetMissionInfoAll()
    Dim ctmList()   As CTMINFO      ' CTM情報
    Dim ctmValue()  As CTMINFO      ' CTM取得情報
    Dim iResult     As Integer
    Dim II          As Integer
    
    iResult = GetMissionInfo(ctmList, ctmValue)
    
    '* シート毎（シート名＝CTM名）へ出力
    If iResult > 0 Then
    
        '* CTM種別でシートに出力
        For II = 0 To UBound(ctmList)
            
            Call OutputCTMInfoToSheet(ctmList(II).Name, 0#, ctmValue)
        
        Next

    End If
    
End Sub


'****************************************************
'* ミッションから情報を取得しバルキ毎のシートへ出力する
'****************************************************
Public Function GetMissionBulkyInfoAll()
    Dim ctmList()   As CTMINFO      ' CTM情報
    Dim ctmValue()  As CTMINFO      ' CTM取得情報
    Dim iResult     As Integer
    Dim II          As Integer
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim ipAddr      As String
    Dim portNo      As String
    Dim MissionID   As String
    Dim CtmID   As String
    Dim ElementID   As String
    Dim startTime   As Double
    Dim endTime     As Double
    Dim SendString  As String
    Dim GetString   As String
    Dim srchRange   As Range
    Dim systemData      As Date
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim dispTerm    As Double
    Dim dispTermUnit As String
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim bulkySelectedCols  As String
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    Dim Worksheet As Worksheet
    
    '* メインシートから各種情報を取得
    With Worksheets(ParamSheetName)

        '*********************
        '* ミッションIDを取得
        '*********************
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            MissionID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "ミッションID項目が見つからないため処理を中断します。"
            GetMissionBulkyInfoAll = -1
            Exit Function
        End If
        If MissionID = "" Then
            MsgBox "ミッションIDが見つからないため処理を中断します。"
            GetMissionBulkyInfoAll = -1
            Exit Function
        End If

        '*********************
        '* CTMIDを取得
        '*********************
        Set srchRange = .Cells.Find(What:="CTMID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            CtmID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "CTMID項目が見つからないため処理を中断します。"
            GetMissionBulkyInfoAll = -1
            Exit Function
        End If
        If CtmID = "" Then
            MsgBox "CTMID項目が見つからないため処理を中断します。"
            GetMissionBulkyInfoAll = -1
            Exit Function
        End If


        '*********************
        '* エレメントIDを取得
        '*********************
        Set srchRange = .Cells.Find(What:="エレメントID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            ElementID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "エレメントID項目が見つからないため処理を中断します。"
            GetMissionBulkyInfoAll = -1
            Exit Function
        End If
        If ElementID = "" Then
            MsgBox "エレメントID項目が見つからないため処理を中断します。"
            GetMissionBulkyInfoAll = -1
            Exit Function
        End If



        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            ipAddr = srchRange.Offset(0, 1).Value
            portNo = srchRange.Offset(1, 1).Value
        Else
            ipAddr = "localhost"
            portNo = "60000"
        End If
        If MissionID = "" Then
            MsgBox "ミッションIDが見つからないため処理を中断します。"
            GetMissionBulkyInfoAll = -1
            Exit Function
        End If

        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
        
        '*******************************
        '* Bulky選択列を取得
        '*******************************
        Set srchRange = .Cells.Find(What:="Bulky選択列", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            bulkySelectedCols = srchRange.Offset(0, 1).Value
        Else
            bulkySelectedCols = ""
        End If

    End With

    Set Worksheet = Sheets(TemplateSheetName)
    startD = DateValue(Worksheet.Range("B3").Value)
    startT = TimeValue(startD + Worksheet.Range("C3").Value)
    endD = DateValue(Worksheet.Range("B4").Value)
    endT = TimeValue(startD + Worksheet.Range("C4").Value)

    startTime = GetUnixTime(startD + startT)
    endTime = GetUnixTime(endD + endT)
    
    If startTime <> GetUnixTime(Now) And endTime <> (GetUnixTime(Now)) Then
        '* バルキデータを取得
        Call getBulkyData(MissionID, CtmID, ElementID, ipAddr, portNo, useProxy, proxyUri, startTime, endTime, bulkySelectedCols)
    End If
    
End Function

'*********************
' バタン再作成
'*********************
Sub UpdateButtonsOfBulky(maxCtmName As String, CtmName As String)

    Dim fixGraphWorksheet       As Worksheet
    Dim graphDataWorksheet      As Worksheet
    Dim currShape               As Shape
    Dim writeRange              As Range
    Dim buttonRange             As Range
    Dim wrkCount                As Integer
    Dim pos                     As Integer
    Dim wrkStr                  As String
    Dim nameRange               As Range
    Dim btSize                  As RECT
    
    Set writeRange = Worksheets(ForPivotSheetName).Range("A1")
    Set graphDataWorksheet = Worksheets(ForPivotSheetName)
    With graphDataWorksheet
        Set nameRange = .Range(writeRange.Cells, writeRange.End(xlToRight).Cells)
    End With
    
    Set fixGraphWorksheet = Worksheets(FixGraphSheetName)   'グラフシート
        
    wrkCount = nameRange.count
        
    'Labelを作成
    Call SetButtonTitle(CtmName)
    
    '*バタン再作成
    For pos = 1 To MaxButtonsNum
        Dim strRange As String
        Dim buttonRow As Long
        
        buttonRow = pos + ButtonStartRow - 1
        strRange = "A" & buttonRow & ":B" & buttonRow
    
        Set buttonRange = Worksheets(FixGraphSheetName).Range(strRange)
    
        wrkStr = nameRange.Offset(0, pos).Value2(1, 1)
        
        If pos < wrkCount Then
            btSize.Top = buttonRange.Top
            btSize.Left = buttonRange.Left + 1
            btSize.Right = buttonRange.Width - 2
            btSize.Bottom = buttonRange.Height
            Set currShape = PutButtonAtSheetOfMap(fixGraphWorksheet, btSize, RGB(255, 255, 255), RGB(0, 0, 255), PrefixButton, CStr(pos), "B_" + CStr(pos) + "_click", wrkStr)
        End If
        If pos = 1 Then
            currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent6
        End If
    Next pos
    
    '*最大値を取得
    Call GetMaxTorqueData("C", nameRange, wrkCount, maxCtmName)
    '*平均値を取得
    Call GetMaxTorqueData("D", nameRange, wrkCount, maxCtmName)
    
End Sub

'*********************
' バルキデータを取得
'*********************
Sub getBulkyData(MissionID As String, CtmID As String, ElementID As String, ipAddr As String, portNo As String, useProxy As Boolean, proxyUri As String, startTime As Double, endTime As Double, bulkySelectedCols As String)

    Dim obj As FoaCoreCom.FoaCoreCom
    Dim bdr As FoaCoreCom.BulkyDataRetriever
    'CsvWriteToExcel→CsvWriteToExcelHandler
    '高速化merge 2018/07/09 sonyi
    Dim bdrCsv As FoaCoreCom.CsvWriteToExcelHandler
    Dim param As FoaCoreCom.ClsGetBulkyParam
    Dim cvsPath As String
    Dim maxCvsPath As String
    Dim Worksheet As Worksheet
    Dim ctmDataSheet As Worksheet
    Dim File_Size As Long
    Dim srchRange       As Range
    Dim paramRng        As Range
    Dim CtmName As String
    Dim maxCtmName As String
    Dim maxCtmId As String
    Dim MAX_BUFF_ROWS As Long
    Dim rowIndex As Long
    Dim blockRowIndex As Long
    Dim SettingFlag   As String
    Dim fixGraphWorksheet       As Worksheet
    Dim currShape               As Shape
    
    With Worksheets(ParamSheetName)
        '* CTM名を「pram」シートから取得
        Set srchRange = .Cells.Find(What:="CTM名", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            CtmName = srchRange.Offset(0, 1).Value
        End If
        '* Max_CTM名、Max_CTMIDを「pram」シートから取得
        Set srchRange = .Cells.Find(What:="最大トルク参照CTM名", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            maxCtmName = srchRange.Offset(0, 1).Value
            maxCtmId = srchRange.Offset(1, 1).Value
        End If
    End With
    
    Set paramRng = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="最初設定フラグ", LookAt:=xlWhole)
    SettingFlag = paramRng.Offset(0, 1).Value
    '最初起動の場合は、当日に設定しない
    If SettingFlag = "False" Then
        Set param = CreateObject("FoaCoreCom.common.dto.ClsGetBulkyParam")
        param.MissionID = MissionID
        param.CtmID = CtmID
        param.ElementID = ElementID
        param.useProxy = useProxy
        param.proxyUri = proxyUri
        param.CmsHostName = ipAddr
        param.CmsPortNo = portNo
        param.startTime = startTime
        param.endTime = endTime
        param.MAX_BUFF_ROWS = 5000
        param.CtmSheetName = CtmName
    
        Set Worksheet = Sheets(param.CtmSheetName)
        If Worksheet.UsedRange.Rows.count >= DataSheetStartRow Then
            Worksheet.Range(Worksheet.Cells(DataSheetStartRow, 1), Worksheet.Cells(Worksheet.UsedRange.Rows.count, Worksheet.UsedRange.Columns.count)).Clear
        End If
        Set bdr = CreateObject("FoaCoreCom.ais.retriever.BulkyDataRetriever")
        cvsPath = bdr.GetMissionResultCsvAsync(param, ThisWorkbook)
        
        If maxCtmName <> "" Then
             Set Worksheet = Sheets(maxCtmName)
             If Worksheet.UsedRange.Rows.count >= DataSheetStartRow Then
                Worksheet.Range(Worksheet.Cells(DataSheetStartRow, 1), Worksheet.Cells(Worksheet.UsedRange.Rows.count, Worksheet.UsedRange.Columns.count)).Clear
             End If
             '*最大、平均シート更新
             param.CtmID = maxCtmId
             param.CtmSheetName = maxCtmName
             maxCvsPath = bdr.GetMissionResultCsvAsync(param, ThisWorkbook)
        End If
        
        'ファイルのサイズを取得
        File_Size = FileLen(cvsPath)
        
        Set Worksheet = Sheets(ForPivotSheetName)
        Worksheet.Cells.Clear
        
        Set fixGraphWorksheet = Worksheets(FixGraphSheetName)   'グラフシート
            
        For Each currShape In fixGraphWorksheet.Shapes
            If InStr(currShape.Name, "B_") Then
            currShape.Delete
            End If
        Next currShape
        
        '開始・終了日時設定
        Call StratEndTimeSet
        
        If Not File_Size = 0 Then
            '* CSVからbulky結果シートに値をセットする
            Dim result As Boolean
            
            Set bdrCsv = CreateObject("FoaCoreCom.ais.retriever.CsvWriteToExcelHandler")
            result = bdrCsv.ReadCsvWriteToExcel(Worksheets(ForPivotSheetName), cvsPath, MAX_CSV_BUFF_ROWS, START_ROW, START_COL, bulkySelectedCols)
    '        Call CSVWriteToExcel(cvsPath, Worksheet, bulkySelectedCols)
            
            'グラフ作成
            Call BulkyDataTOGraph
            
            'データ変更時、バタンを再作成
            Call UpdateButtonsOfBulky(maxCtmName, CtmName)
            
            Call MakeGraph2(1)
        Else
            MsgBox "データがないためもう一度、再設定をしてください。"
        End If
    Else
        'グラフ作成
        Call BulkyDataTOGraph
        'データ変更時、バタンを再作成
        Call UpdateButtonsOfBulky(maxCtmName, CtmName)
        
        Call MakeGraph2(1)
    End If
    
    paramRng.Offset(0, 1).Value = "False"
End Sub

Private Sub WriteRowData(rowData As Variant, row As Long, bulkySelectedCols As String)
    Dim countMax As Integer
    Dim temp2 As Variant
    Dim count As Integer
    
    Dim arrWsNames() As String
    Dim II          As Integer
    Dim bool As Boolean
    
    arrWsNames = Split(bulkySelectedCols, ",")
    
    temp2 = Split(rowData, ",")
    countMax = UBound(temp2) - LBound(temp2) + 1
    
    For II = 0 To UBound(arrWsNames)
        For count = 0 To countMax - 1
            If count = 0 Then
                If row = 1 Then
                    Cells(row, 1).Value = temp2(count)
                Else
                    Cells(row, 1).Value = Val(temp2(count))
                End If
            Else
                If Int(arrWsNames(II)) = count - 1 Then
                    If row = 1 Then
                        Cells(row, II + 2).Value = temp2(count)
                    Else
                        Cells(row, II + 2).Value = Val(temp2(count))
                    End If
                End If
            End If
        Next
    Next
End Sub

'*********************
'CSVからbulky結果シートに値をセットする
'*********************
Public Sub CSVWriteToExcel(cvsPath As String, Worksheet As Worksheet, bulkySelectedCols As String)
    Dim buf As String, Target As String, i As Long
    Dim tmp1 As Variant
    Dim MaxRow   As Long
    Dim MaxCol   As Long
    
    Target = cvsPath
    With CreateObject("ADODB.Stream")
        .Charset = "UTF-8"
        .Open
        .LoadFromFile Target
        buf = .ReadText
        .Close
        tmp1 = Split(buf, vbCrLf)
        
        Worksheet.Select
        For i = 0 To UBound(tmp1)
            Call WriteRowData(tmp1(i), i + 1, bulkySelectedCols)
        Next i
        
        '* 罫線付く
        MaxRow = Range("A1").End(xlDown).row
        MaxCol = Range("A1").End(xlToRight).Column
        Range(Cells(1, 1), Cells(MaxRow, MaxCol)).Select
        
        Selection.Borders(xlDiagonalDown).LineStyle = xlNone
        Selection.Borders(xlDiagonalUp).LineStyle = xlNone
        With Selection.Borders(xlEdgeLeft)
            .LineStyle = xlContinuous
            .ColorIndex = 0
            .TintAndShade = 0
            .Weight = xlThin
        End With
        With Selection.Borders(xlEdgeTop)
            .LineStyle = xlContinuous
            .ColorIndex = 0
            .TintAndShade = 0
            .Weight = xlThin
        End With
        With Selection.Borders(xlEdgeBottom)
            .LineStyle = xlContinuous
            .ColorIndex = 0
            .TintAndShade = 0
            .Weight = xlThin
        End With
        With Selection.Borders(xlEdgeRight)
            .LineStyle = xlContinuous
            .ColorIndex = 0
            .TintAndShade = 0
            .Weight = xlThin
        End With
        With Selection.Borders(xlInsideVertical)
            .LineStyle = xlContinuous
            .ColorIndex = 0
            .TintAndShade = 0
            .Weight = xlThin
        End With
        With Selection.Borders(xlInsideHorizontal)
            .LineStyle = xlContinuous
            .ColorIndex = 0
            .TintAndShade = 0
            .Weight = xlThin
        End With
    End With
    
End Sub
    

'****************************************************************
'* 指定ミッションの情報を取得する
'* ※ParamシートにミッションID、CTMエレメント一覧が必要
'****************************************************************
Private Function GetMissionInfo(ctmList() As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim ipAddr      As String
    Dim portNo      As String
    Dim MissionID   As String
    Dim startTime   As Double
    Dim endTime     As Double
    Dim SendString  As String
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim dispTerm    As Double
    Dim dispTermUnit As String
    Dim getStartTime As Double
    Dim getEndTime  As Double
    Dim iResult     As Integer
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    '* メインシートから各種情報を取得
    With Worksheets(ParamSheetName)
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得開始", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(srchRange.Offset(0, 1).Value)
            startT = TimeValue(srchRange.Offset(0, 1).Value)
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If
        startTime = GetUnixTime(startD + startT)
        
        '*********************
        '* 収集終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = DateValue(srchRange.Offset(0, 1).Value)
            endT = TimeValue(srchRange.Offset(0, 1).Value)
        Else
            endD = DateValue("2020/12/31 23:59:59")
            endT = TimeValue("2020/12/31 23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If
        endTime = GetUnixTime(endD + endT)
        
        '*********************
        '* ミッションIDを取得
        '*********************
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            MissionID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "ミッションID項目が見つからないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If
        If MissionID = "" Then
            MsgBox "ミッションIDが見つからないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            ipAddr = srchRange.Offset(0, 1).Value
            portNo = srchRange.Offset(1, 1).Value
        Else
            ipAddr = "localhost"
            portNo = "60000"
        End If
        If MissionID = "" Then
            MsgBox "ミッションIDが見つからないため処理を中断します。"
            GetMissionInfo = -1
            Exit Function
        End If

        '*******************************
        '* 更新周期を取得
        '*******************************
    
        '*******************************
        '* 表示期間を取得
        '*******************************
        Set srchRange = .Cells.Find(What:="表示期間", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If IsEmpty(srchRange.Offset(0, 1).Value) Then
                dispTerm = 0#
                dispTermUnit = "【時】"
            Else
                dispTerm = srchRange.Offset(0, 1).Value
                dispTermUnit = srchRange.Offset(0, 2).Value
            End If
        Else
            dispTerm = 0#
            dispTermUnit = "【時】"
        End If
        Select Case dispTermUnit
            Case "【分】"
                dispTerm = dispTerm * 60
            Case "【時】"
                dispTerm = dispTerm * 3600
        End Select
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
    
    End With
    
    '* CTM情報一覧をシートから取得
    Call GetCTMList(ParamSheetName, ctmList)
    
    '* プログラムミッションに問合せ
    '* 取得開始日時を「現在時刻−表示期間−１分」として算出し問い合わせる
    getEndTime = endTime
    getStartTime = startTime
'    getStartTime = GetUnixTime(Now()) - (dispTerm + 60) * 1000
'    MsgBox "開始日時は" & Format(CDate(((getStartTime / 1000 + 32400) / 86400) + 25569), "yyyy/MM/dd hh:mm:ss") & "です。" & vbCrLf & _
'           "終了日時は" & Format(CDate(((getEndTime / 1000 + 32400) / 86400) + 25569), "yyyy/MM/dd hh:mm:ss") & "です。"
    SendString = "http://" & ipAddr & ":" & portNo & "/cms/rest/mib/mission/pm?id=" & MissionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & "&limit=" & Format(LimitCTMNumber) & "&lang=ja"
    
    If useProxy = False Then
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
    Else
        Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
        httpObj.setProxy 2, proxyUri
    End If
    
    httpObj.Open "GET", SendString, False
    httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
    httpObj.Send
    
'    Select Case httpObj.Status
'        Case 200
'            With CreateObject("ADODB.Stream")
'                .Type = adTypeText
'                .Open
'                .WriteText httpObj.responseText
'                .SaveToFile "c:\foa\data\test.json", adSaveCreateOverWrite
'                .Close
'            End With
'        Case Else
'            MsgBox "エラーが発生しました。" & vbCrLf & "ステータスコード：" & httpObj.Status, vbCritical + vbSystemModal
'            Exit Function
'    End Select
    
    ' ダウンロード待ち
    Do While httpObj.readyState <> 4
        DoEvents
    Loop
    
    '* 取得情報のデコード
    Set sc = CreateObject("ScriptControl")
    With sc
        .Language = "JScript"

        '指定したインデックス、名称のデータを取得する
        .AddCode "function jsonParse(s) { return eval('(' + s + ')'); }"
    End With
    GetString = httpObj.responseText
    Set objJSON = sc.CodeObject.jsonParse(GetString)
    
    Set sc = Nothing
    
    Set httpObj = Nothing
    
    '* 取得情報をCTM情報に展開
    iResult = GetCTMInfo(objJSON, ctmList, ctmValue)
    
    ' MsgBox http.responseText
    GetMissionInfo = iResult
    
End Function

'*************************************************************************
'* 取得したJSON情報をデコードしCTM情報およびエレメント情報として展開する
'*************************************************************************
Public Function GetCTMInfo(objJSON As Object, ctmList() As CTMINFO, ctmValue() As CTMINFO) As Integer
    Dim recno           As Integer
    Dim II              As Integer
    Dim rec             As Object
    Dim rec2            As Object
    Dim rec3            As Object
    Dim rec4            As Object
    Dim aryIndex        As Integer
    Dim aryIndex2       As Integer
    Dim ctmIndex        As Integer
    Dim workElm         As ELMINFO
    Dim iFirstFlag      As Boolean
    
    iFirstFlag = True
    
    ' 件数カウンタ初期化
    recno = 0
    
    '* CTM種別数分ループ
    For Each rec In objJSON
    
        '* 対象CTMを確定する
        For II = 0 To UBound(ctmList)
            If ctmList(II).ID = GetJSONValue(rec, "id") Then
                ctmIndex = II
                Exit For
            End If
        Next
        
        If II > UBound(ctmList) Then
            MsgBox "ミッションからの取得情報に該当するCTMがありませんでした。" & vbCrLf & "処理を中断します。"
            Exit Function
        End If
        
        '* CTM取得件数分ループ
        II = rec.num
        For Each rec2 In rec.ctms
        
            If iFirstFlag Then
                aryIndex = 0
                ReDim ctmValue(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(ctmValue) + 1
                ReDim Preserve ctmValue(aryIndex)
            End If
            ReDim ctmValue(aryIndex).Element(0)
            ctmValue(aryIndex).Element(0).Name = ""
            
            '* CTM情報の取得
            ctmValue(aryIndex).Name = ctmList(ctmIndex).Name
            ctmValue(aryIndex).ID = ctmList(ctmIndex).ID
            ctmValue(aryIndex).RecvTime = GetJSONValue(rec2, "RT")

            '* 件数カウンタアップ
            recno = recno + 1
            
            '* エレメント数分の値取得
            Set rec3 = CallByName(rec2, "EL", VbGet)
            For II = 0 To UBound(ctmList(ctmIndex).Element)
            
                workElm = ctmList(ctmIndex).Element(II)
                
                Set rec4 = CallByName(rec3, workElm.ID, VbGet)
                
                aryIndex2 = UBound(ctmValue(aryIndex).Element)
                If ctmValue(aryIndex).Element(aryIndex2).Name <> "" Then
                    aryIndex2 = aryIndex2 + 1
                    ReDim Preserve ctmValue(aryIndex).Element(aryIndex2)
                End If
                
                '* エレメント情報の取得
                ctmValue(aryIndex).Element(aryIndex2).Name = workElm.Name
                ctmValue(aryIndex).Element(aryIndex2).ID = workElm.ID
                ctmValue(aryIndex).Element(aryIndex2).Type = CInt(GetJSONValue(rec4, "T"))
                ctmValue(aryIndex).Element(aryIndex2).Value = ""
                On Error Resume Next
                ctmValue(aryIndex).Element(aryIndex2).Value = GetJSONValue(rec4, "V")
                '''If ctmValue(aryIndex).Element(aryIndex2).Value = "" Then ctmValue(aryIndex).Element(aryIndex2).Value = " "
                
            Next
            
        Next
        
    Next
    
    GetCTMInfo = recno
    
End Function

'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Public Sub OutputCTMInfoToSheet(currSheetName As String, dispTerm As Double, ctmValue() As CTMINFO)
    Dim II              As Integer
    Dim JJ              As Integer
    Dim KK              As Integer
    Dim currWorksheet   As Worksheet
    Dim writeIndex      As Integer
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim delRange        As Range
    Dim writeRange      As Range
    Dim receiveTime     As Double
    Dim dtRecvL         As Long
    Dim dtRecv          As Date
    Dim ms              As Integer
    Dim strWork         As String
    Dim nowTime         As Double
    Dim endTime         As Double
    Dim writeArray()    As Variant
    Dim writeArray2()   As Variant
    Dim iFirstFlag      As Boolean
    Dim readIndex       As Integer
    
    
    '* 出力先シートの取得
    Set currWorksheet = Worksheets(currSheetName)
    
    With currWorksheet
        
        '* 書き出し位置の設定
        Set startRange = .Cells(4, 1)
        
        '* 全情報クリア
        Set lastRange = startRange.End(xlDown)
        Set delRange = .Range("4:" & Format(lastRange.row))
        delRange.Delete
        
        '* 書き出し位置の設定
        Set startRange = .Cells(4, 1)
        writeIndex = 0
        
'        Application.ScreenUpdating = False
        
        iFirstFlag = True
        For JJ = 0 To UBound(ctmValue)
        
            If currSheetName = ctmValue(JJ).Name Then
            
                receiveTime = CDbl(ctmValue(JJ).RecvTime)
                dtRecvL = CLng(receiveTime / 1000)
                '* 表示期間の処理
                If JJ = 0 Then
                    If dispTerm < 1# Then
                        endTime = dispTerm
                    Else
                        endTime = dtRecvL - dispTerm
                    End If
                End If
                If (endTime < 0 Or endTime > dtRecvL) Then Exit For
                ms = CInt(Right(ctmValue(JJ).RecvTime, 3))
                receiveTime = ((dtRecvL + 32400) / 86400) + 25569
                dtRecv = CDate(receiveTime)
                strWork = Format(dtRecv, "yyyy/MM/dd hh:mm:ss") + "." + Format(ms, "000")
                
                If iFirstFlag Then
                    readIndex = 0
                    ReDim writeArray(UBound(ctmValue(JJ).Element) + 2, readIndex)
                    iFirstFlag = False
                Else
                    readIndex = UBound(writeArray, 2) + 1
                    ReDim Preserve writeArray(UBound(ctmValue(JJ).Element) + 2, readIndex)
                End If
                
                writeArray(0, readIndex) = strWork
                writeArray(1, readIndex) = ctmValue(JJ).Name
                
                For KK = 0 To UBound(ctmValue(JJ).Element)
                
                    writeArray(KK + 2, readIndex) = ctmValue(JJ).Element(KK).Value
                
                Next
                
                writeIndex = writeIndex + 1
            End If
            
            DoEvents
        
        Next
        
        Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, UBound(ctmValue(0).Element) + 2))
        writeArray2 = WorksheetFunction.Transpose(writeArray)
        writeRange = writeArray2
        Set writeRange = .Range(startRange.Offset(0, 0), startRange.Offset(writeIndex - 1, 0))
        writeRange.NumberFormatLocal = "yyyy/MM/dd hh:mm:ss"
        writeRange.EntireColumn.AutoFit
    
'        Application.ScreenUpdating = True
        
    End With
    
End Sub

'***********************************************
'* メインシートにあるミッション情報を取得する
'***********************************************
Public Sub GetCTMList(currSheetName As String, ctmList() As CTMINFO)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim ctmRange        As Range
    Dim elmRange        As Range
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim aryIndex        As Integer      ' CTM情報インデックス
    Dim aryIndex2       As Integer      ' エレメント情報インデックス
    Dim iFirstFlag      As Boolean      ' 配列初回フラグ
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    
    iFirstFlag = True
    
    Set currWorksheet = Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim ctmList(0)
    
    With currWorksheet
    
'        .Activate
        
        '* CTM名称の項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM名称", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        
        For II = 0 To MaxCTMNumber Step 2
            Set ctmRange = startRange.Offset(II, 0)
            
            If ctmRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            If iFirstFlag Then
                aryIndex = 0
                ReDim ctmList(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(ctmList) + 1
                ReDim Preserve ctmList(aryIndex)
            End If
            ReDim ctmList(aryIndex).Element(0)
            ctmList(aryIndex).Element(0).Name = ""
            
            '* CTM情報取得
            ctmList(aryIndex).Name = ctmRange.Offset(0, 0).Value
            ctmList(aryIndex).ID = ctmRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = ctmRange.Offset(0, 1)
            For JJ = 0 To MaxELMNumber
                If elmRange.Offset(0, JJ).Value = "" Then
                    Exit For
                End If
                
                aryIndex2 = UBound(ctmList(aryIndex).Element)
                If ctmList(aryIndex).Element(aryIndex2).Name <> "" Then
                    aryIndex2 = aryIndex2 + 1
                    ReDim Preserve ctmList(aryIndex).Element(aryIndex2)
                End If
                ctmList(aryIndex).Element(aryIndex2).Name = elmRange.Offset(0, JJ).Value
                ctmList(aryIndex).Element(aryIndex2).ID = elmRange.Offset(1, JJ).Value
            Next
            
                
        Next
    
    End With
    
End Sub





