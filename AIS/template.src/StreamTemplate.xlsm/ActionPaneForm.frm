VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} ActionPaneForm 
   Caption         =   "表示テキストボックス定義"
   ClientHeight    =   2610
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   7665
   OleObjectBlob   =   "ActionPaneForm.frx":0000
End
Attribute VB_Name = "ActionPaneForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False




'*************************************
'* 表示色条件テキストボックスをクリア
'*************************************
Private Sub BT_Clear_Click()
    TB_White.text = ""
    TB_Red.text = ""
    TB_Lime.text = ""
    TB_Blue.text = ""
    TB_Yellow.text = ""
    TB_Magenta.text = ""
    TB_Aqua.text = ""
    TB_Maroon.text = ""
    TB_Green.text = ""
    TB_Navy.text = ""
    TB_Olive.text = ""
    TB_Purple.text = ""
    TB_Teal.text = ""
    TB_Silver.text = ""
    TB_Gray.text = ""
End Sub

'*********************
'* 表示色条件取得処理
'*********************
Public Sub GetDispColorCond(dispColorCond() As String)
    dispColorCond(0) = ""
    dispColorCond(1) = ""
    dispColorCond(2) = ""
    dispColorCond(3) = ""
    dispColorCond(4) = ""
    dispColorCond(5) = ""
    dispColorCond(6) = ""
    dispColorCond(7) = ""
    dispColorCond(8) = ""
    dispColorCond(9) = ""
    dispColorCond(10) = ""
    dispColorCond(11) = ""
    dispColorCond(12) = ""
    dispColorCond(13) = ""
    dispColorCond(14) = ""
End Sub

'*********************
'* 条件取得処理
'*********************
Public Sub GetElementList(elementList() As String)

'    For i = 1 To LV_ElementView.ListItems.Count Step 1
'        elementList(i - 1) = LV_ElementView.ListItems(i).SubItems(1)
'    Next i
    
End Sub
'*********************
'* 条件取得処理
'*********************
Public Sub GetElementCondList(elementCondList() As String)

'    For i = 1 To LV_ElementView.ListItems.Count Step 1
'        elementCondList(i - 1) = LV_ElementView.ListItems(i).SubItems(2)
'    Next i
    
End Sub
'***********
'* 削除処理
'***********
Private Sub BT_Delete_Click()
    Dim currShape           As Shape
    Dim dispDefWorksheet    As Worksheet
    Dim statusWorksheet     As Worksheet
    Dim linkWorksheet       As Worksheet
    Dim condWorksheet       As Worksheet
    Dim srchRange           As Range
    Dim currRange           As Range
    Dim srchRange1          As Range
    Dim workRange           As Range
    Dim calcStr             As String
    Dim currShapeName       As String
    Dim workStr             As String
    Dim iRowCnt             As Integer
    Dim II                  As Integer

    '* 全図形情報削除
'    Call DeleteAllShapeAndDispDef

    workStr = "着目図形を削除します。よろしいですか？"
    If MsgBox(workStr, vbYesNo + vbQuestion, "削除確認") = vbNo Then Exit Sub
    
    '* 着目図形を削除
    currShapeName = LBL_ShapeName.Caption
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set currShape = statusWorksheet.Shapes(currShapeName)
    currShape.Delete

    '* 着目画面定義情報を削除
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    With dispDefWorksheet

        Set srchRange = .Range("A:A")
        Set currRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
        If Not currRange Is Nothing Then
            currRange.EntireRow.ClearContents
        Else
            Exit Sub
        End If

    End With
    
    '* 着目リンクシート情報を削除
    Set linkWorksheet = Workbooks(thisBookName).Worksheets(LinkSheetName)
    With linkWorksheet
        Set srchRange1 = .Range("B:B")
        Set workRange = srchRange1.Find(What:=currShapeName, LookAt:=xlWhole)
        
        If Not workRange Is Nothing Then
            ' データを削除する
            .Rows(workRange.row).Delete
        End If

    End With
    
    '* 着目条件情報を削除
    Set condWorksheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    iRowCnt = condWorksheet.UsedRange.Rows.count
    With condWorksheet

        For II = iRowCnt To 1 Step -1
            If .Range("A" & II).Value = currShapeName Then
                
                ' データを削除する
                .Rows(II).Delete
            End If
        Next II

    End With
    
    Unload Me
    
End Sub

'*********************
'* 表示色条件設定処理
'*********************
Private Sub BT_Set_Click()
    Dim shapeName           As String
    Dim srchRange           As Range
    Dim colRange            As Range
    Dim rowRange            As Range
    Dim currRange           As Range
    Dim dispDefWorksheet    As Worksheet
    
    Set dispDefWorksheet = Workbooks(thisBookName).Worksheets(DispDefSheetName)
    
    shapeName = LBL_ShapeName.Caption
    
    If shapeName <> "" Then
    
        With dispDefWorksheet
    
            Set srchRange = .Range("A:A")
            Set rowRange = srchRange.Find(What:=shapeName, LookAt:=xlWhole)
            If Not rowRange Is Nothing Then
                Set srchRange = .Range("1:1")
                Set colRange = srchRange.Find(What:="表示色−白", LookAt:=xlWhole)
                If Not colRange Is Nothing Then
                    Set currRange = .Cells(rowRange.row, colRange.Column)
                    Set srchRange = .Range(currRange.Offset(0, 0), currRange.Offset(0, 14))
                    srchRange.NumberFormatLocal = "@"
                    currRange.Offset(0, 0).Value = TB_White.text
                    currRange.Offset(0, 1).Value = TB_Red.text
                    currRange.Offset(0, 2).Value = TB_Lime.text
                    currRange.Offset(0, 3).Value = TB_Blue.text
                    currRange.Offset(0, 4).Value = TB_Yellow.text
                    currRange.Offset(0, 5).Value = TB_Magenta.text
                    currRange.Offset(0, 6).Value = TB_Aqua.text
                    currRange.Offset(0, 7).Value = TB_Maroon.text
                    currRange.Offset(0, 8).Value = TB_Green.text
                    currRange.Offset(0, 9).Value = TB_Navy.text
                    currRange.Offset(0, 10).Value = TB_Olive.text
                    currRange.Offset(0, 11).Value = TB_Purple.text
                    currRange.Offset(0, 12).Value = TB_Teal.text
                    currRange.Offset(0, 13).Value = TB_Silver.text
                    currRange.Offset(0, 14).Value = TB_Gray.text
                End If
            End If
    
        End With
    
    Else
    End If
    
    Unload Me

End Sub

'*******************
'* 補助式ボタン処理
'*******************
Private Sub BT_Assisted_Click()

    If BT_Assisted.Caption = "＋" Then
        BT_Assisted.Caption = StrMinus
        TB_AssistedArea.Visible = True
    Else
        BT_Assisted.Caption = StrPlus
        TB_AssistedArea.Visible = False
    End If

End Sub

'*******************
'* 閉じるボタン処理
'*******************
Private Sub BT_Close_Click()

    Unload Me

End Sub

'*********************************
'* 表示テキストボックスを生成する
'*********************************
Private Sub BT_CreateTextBox_Click()
    Dim isFormat            As Integer
    Dim isLinkFormat        As Integer
    Dim dispColorCond(15)   As String
    Dim elementCondList(15) As String
    Dim elementList(15)     As String
    Dim elementListCount    As String
    

'    If TB_ItemOperation.text = "" Then
'        MsgBox "演算式が入力されていません。"
'        Exit Sub
'    End If

    '* 表示色条件を取得する
    Call GetDispColorCond(dispColorCond)
    Call GetElementCondList(elementCondList)
    Call GetElementList(elementList)
    elementListCount = 0
    
    Me.Hide
    
'    '* テキストボックス生成処理
'    If OB_Numeric.Value Then isFormat = 0
'    If OB_String.Value Then isFormat = 1
'    If OB_Date.Value Then isFormat = 2
'    '* 2017/02/13 Add ↓↓↓↓↓↓
'    If OB_Time.Value Then isFormat = 3
'    If OB_DateTime.Value Then isFormat = 4
'    '* 2017/02/13 Add ↑↑↑↑↑↑
'
'    '* Link種類を指定
'    If OB_StatusMonitor.Value Then isLinkFormat = 1
'    If OB_URL.Value Then isLinkFormat = 2
'    If OB_App.Value Then isLinkFormat = 3

    If CB_Link.Value Then
        If TB_ReferPass.text = "" Then
            MsgBox "リンク先が入力されていません。"
            Exit Sub
        End If
    End If
    
    Call WriteValueTextBox
'    TB_ItemOperation.Text, isFormat, dispColorCond, CB_Link.Value, isLinkFormat,
'                            TB_ReferPass.Text, CB_Element.Value, TB_Name.Text, elementCondList, elementList, elementListCount)
                            
    
'    TB_ItemOperation.Text = ""
'    TB_ReferPass.Text = ""
'    TB_Name.Text = ""
    
'    Call BT_Clear_Click

    Me.Show vbModeless

End Sub

'***********
'* 更新処理
'***********
Private Sub BT_Update_Click()
    Dim dispColorCond(15)   As String
    Dim isFormat            As Integer
    Dim isLinkFormat        As Integer
    Dim elementCondList(15) As String
    Dim elementList(15)     As String
    Dim elementListCount    As String
    
    If TB_ItemOperation.text = "" Then
        MsgBox "演算式が入力されていません。"
        Exit Sub
    End If

    '* 表示色条件を取得する
    Call GetDispColorCond(dispColorCond)
    Call GetElementCondList(elementCondList)
    Call GetElementList(elementList)
    elementListCount = LV_ElementView.ListItems.count
    
    '* テキストボックス更新処理
    If OB_Numeric.Value Then isFormat = 0
    If OB_String.Value Then isFormat = 1
    If OB_Date.Value Then isFormat = 2
    If OB_Time.Value Then isFormat = 3
    If OB_DateTime.Value Then isFormat = 4
    
    '* Link種類を指定
    If OB_StatusMonitor.Value Then isLinkFormat = 1
    If OB_URL.Value Then isLinkFormat = 2
    If OB_App.Value Then isLinkFormat = 3
    
    If CB_Link.Value Then
        If TB_ReferPass.text = "" Then
            MsgBox "リンク先が入力されていません。"
            Exit Sub
        End If
    End If
    
    Call UpdateValueTextBox(LBL_ShapeName, TB_ItemOperation.text, isFormat, dispColorCond, CB_Link.Value, _
                            isLinkFormat, TB_ReferPass.text, CB_Element.Value, TB_Name.text, elementCondList, elementList, elementListCount)
    
    Unload Me
    
End Sub
'*************************************************
'* リンクの情報をシートに入力する
'*************************************************
Public Sub WriteValueConditionSheet(formStatus As String, listCount As Integer, currShapeName As String)
    Dim condParamsheet      As Worksheet
    Dim workRange           As Range
    Dim srchRange           As Range
    Dim srchNmRange         As Range
    Dim dispRange           As Range
    Dim II                  As Integer
    
    Call SetFilenameToVariable
    
    Set condParamsheet = Workbooks(thisBookName).Worksheets(ConditiongSheetName)
    With condParamsheet
        Set srchRange = .Range("A:A")
        Set workRange = srchRange.Find(What:="図形名", LookAt:=xlWhole)
    
        If Not workRange Is Nothing Then
            '* 更新の場合
            If formStatus = "update" Then
            
              '* 全図形に対して
                For II = 1 To 100
                    Set srchNmRange = .Range("A:A")
                    Set dispRange = srchRange.Find(What:=currShapeName, LookAt:=xlWhole)
                  
                    If Not dispRange Is Nothing Then
                        ' データを削除する
                        Rows(workRange.row).Delete
                    Else
                        Exit For
                    End If
                Next
            End If
            

            '* 全図形に対して
            For II = 1 To 15000
                '定義内容の有無ﾁｪｯｸ
                If workRange.Offset(II, 0).Value = "" Then
                    For i = 1 To listCount Step -1
                        
                        workRange.Offset(II + i - 1, 0).Value = currShapeName
                        workRange.Offset(II + i - 1, 1).Value = LV_ElementView.ListItems(i).SubItems(1)
                        workRange.Offset(II + i - 1, 2).Value = LV_ElementView.ListItems(i).SubItems(2)
                    Next i
                End If
            Next
        
        End If
    End With
End Sub

'***********************
'* 表示エレメントの追加
'***********************
Private Sub BT_add_Click()

    Dim sValue              As String
    Dim isConditionText     As String
    Dim sCtmValue           As String
        
    '* テキストボックス更新処理
    If TB_CalcCondition.text = "" Then
        MsgBox "演算式条件が入力されていません。"
        Exit Sub
    Else
        isConditionText = TB_CalcCondition.text
    End If
    
    '* エレメントを追加する
    For i = LV_Element.ListItems.count To 1 Step -1
        If LV_Element.ListItems(i).Selected Then
        
            sValue = LV_Element.ListItems(i).text

            '* 個数(単一条件)時、選択の項目で前のデータを替換え
            If CB_CalcFormat.text = "個数(単一条件)" Then
                For j = LV_ElementView.ListItems.count To 1 Step -1
                    LV_ElementView.ListItems.Remove j
                Next j
            End If

            Set itm = LV_ElementView.ListItems.Add()
            itm.text = LV_ElementView.ListItems.count()
            itm.SubItems(1) = sValue
            itm.SubItems(2) = isConditionText
            itm.SubItems(3) = LB_CTM.text
            Exit For
        End If
    Next i

End Sub

'***********************
'* 表示エレメントの行削除
'***********************
Private Sub BT_rowdelete_Click()

    For i = LV_ElementView.ListItems.count To 1 Step -1
        If LV_ElementView.ListItems(i).Selected Then
            LV_ElementView.ListItems.Remove i
            Exit For
        End If
    Next i
    
    For i = LV_ElementView.ListItems.count To 1 Step -1
        LV_ElementView.ListItems(i).text = i
    Next i
End Sub

'**********************
'* エレメント条件式修正
'**********************
Private Sub LV_ElementView_ItemClick(ByVal Item As MSComctlLib.ListItem)
    Dim getConditionNameStr      As Variant

    If LV_ElementView.SelectedItem <> 0 Then
        TB_CalcCondition.text = LV_ElementView.ListItems(LV_ElementView.SelectedItem.Index).SubItems(2)
                        
    End If
End Sub

'***********************
'* エレメント条件式の更新
'***********************
Private Sub BT_ConditionUpdate_Click()
    For i = LV_ElementView.ListItems.count To 1 Step -1
        If LV_ElementView.ListItems(i).Selected Then

            LV_ElementView.ListItems(i).SubItems(2) = TB_CalcCondition.text
            Exit For
        End If
    Next i
End Sub


Private Sub CB_Element_Click()
    If CB_Element.Value = False Then
        TB_Name.Enabled = False
    Else
        TB_Name.Enabled = True
    End If
End Sub

Private Sub CB_Link_Click()
    If CB_Link.Value = False Then
        OB_StatusMonitor.Enabled = False
        OB_URL.Enabled = False
        OB_App.Enabled = False
        TB_ReferPass.Enabled = False
        CB_Refer.Enabled = False
    Else
        OB_StatusMonitor.Enabled = True
        OB_URL.Enabled = True
        OB_App.Enabled = True
        TB_ReferPass.Enabled = True
        CB_Refer.Enabled = True
    End If
End Sub

Private Sub CB_Refer_Click()

    Dim paramWorksheet    As Worksheet
    Dim srchRange         As Range
    Dim currRange         As Range
    Dim proxyServer       As String
    Dim proxyServerUI     As String
    Dim ipAddr            As String
    Dim portNo            As String
    Dim Filename          As Variant
    
    If OB_App.Value Then
        Filename = Application.GetOpenFilename("文件 ,*.exe")
        If Filename <> False Then
            ActionPaneForm.TB_ReferPass.text = Filename
            If InStr(Filename, "CTMRefineUI.exe") > 0 Then
                CTMForm.Show
            End If
        End If
    ElseIf OB_StatusMonitor.Value Then
        SearchForm.Show
    End If
End Sub

'***********************
'* ドラッグ完了時の処理
'***********************
Private Sub LV_Element_OLECompleteDrag(Effect As Long)
    Debug.Print "OLECompleteDrag"
    Effect = vbDropEffectCopy
End Sub

'*****************************************************
'* リストボックスからテキストボックスへのドラッグ処理
'*****************************************************
Private Sub LV_Element_OLEStartDrag(data As MSComctlLib.DataObject, AllowedEffects As Long)
    Dim workStr         As Variant
    Dim getTextStr      As Variant
    Debug.Print "OLEStartDrag"
    
    '* データオブジェクトの内容をクリア
    data.Clear
    
    '* ドラッグ開始時のリストボックスの選択項目を取得
    getTextStr = LV_Element.SelectedItem.text
    
    '* 貼り付ける文字列の生成
    If CB_CalcFormat.text = "" Then
        workStr = """" & LB_CTM.text & "・" & getTextStr & """"
        
        ''''workStr = """" & LB_CTM.text & "_" & getTextStr & """"
    Else
        workStr = """" & LB_CTM.text & "・" & getTextStr & "," & CB_CalcFormat.text & """"
        
        ''''workStr = """" & LB_CTM.text & "_" & getTextStr & "," & CB_CalcFormat.text & """"
    End If
    
    '* データオブジェクトにセット
    data.SetData workStr, vbCFtext
    
End Sub

'********************************
'* 演算式条件ドラッグ完了時の処理
'********************************
Private Sub LV_ElementView_OLECompleteDrag(Effect As Long)
    Debug.Print "OLECompleteDrag"
    Effect = vbDropEffectCopy
End Sub

'**************************************************************
'* 演算式条件リストボックスからテキストボックスへのドラッグ処理
'**************************************************************
Private Sub LV_ElementView_OLEStartDrag(data As MSComctlLib.DataObject, AllowedEffects As Long)
    Dim workView            As Variant
    Dim workViewStr         As Variant
    Dim getViewNameStr      As Variant

    Debug.Print "OLEStartDrag"
    
    '* データオブジェクトの内容をクリア
    data.Clear
    
    '* ドラッグ開始時のリストボックスの選択項目を取得
    getViewNameStr = LV_ElementView.SelectedItem.SubItems(1)

    '* 貼り付ける文字列の生成
    If TB_CalcCondition.text = "" Then
        MsgBox "演算式条件が入力されていません。"
        Exit Sub
    End If

    For i = LV_ElementView.ListItems.count To 1 Step -1
        If workView = "" Then
            workView = LV_ElementView.ListItems(i).SubItems(1) & ":" & LV_ElementView.ListItems(i).SubItems(2)
        Else
            workView = workView & ";" & LV_ElementView.ListItems(i).SubItems(1) & ":" & LV_ElementView.ListItems(i).SubItems(2)
        End If
    Next i
    '* 製品1ライン状態・個数(固定値),個数(XX条件)[エレメント名:条件;エレメント名2:条件2]
    workViewStr = """" & LB_CTM.text & "・" & "個数" & "," & CB_CalcFormat.text & "[" & workView & "]" & """"
    
    '* データオブジェクトにセット
    data.SetData workViewStr, vbCFtext

End Sub

'********************
'* 演算形式リスト変更
'********************
Private Sub CB_CalcFormat_Change()
    
    If CB_CalcFormat.text = "個数(単一条件)" Then

        Call SetDisplayStatus("1")
        
        '* 複数条件→単一条件に変更時、一番上のデータのみを保留
        For i = LV_ElementView.ListItems.count To 2 Step -1
            LV_ElementView.ListItems.Remove i
        Next i
    ElseIf CB_CalcFormat.text = "個数(複数条件)" Then

        Call SetDisplayStatus("1")
    Else
    
            Call SetDisplayStatus("0")
    End If
    
End Sub

'****************************************************
'* 演算形式が個数(単一条件)と個数(複数条件)の画面設定
'****************************************************
Private Sub SetDisplayStatus(sFlg As String)

    If sFlg = "1" Then
        
        '* 「エレメント追加」ボタンを表示
        BT_Add.Visible = True
        
        '* 「エレメント削除」ボタンを表示
        BT_rowdelete.Visible = True
        
        '* 「条件更新」ボタンを表示
        BT_ConditionUpdate.Visible = True
        
        '* 「条件」テキストボックスを表示
        TB_CalcCondition.Visible = True
        
        '* 「条件」ラベルを表示
        Label23.Visible = True
        
        '* 「エレメント」リストビューを表示
        LV_ElementView.Visible = True
        
        '* リストビューのへーだー「エレメント」ラベルを表示
        Label25.Visible = True
        
        '* リストビューのへーだー「条件」ラベルを表示
        Label24.Visible = True
    Else
        
        '* 「エレメント追加」ボタンを非表示
        BT_Add.Visible = False
        
        '* 「エレメント削除」ボタンを非表示
        BT_rowdelete.Visible = False
        
        '* 「条件更新」ボタンを非表示
        BT_ConditionUpdate.Visible = False
        
        '* 「条件」テキストボックスを非表示
        TB_CalcCondition.Visible = False
        
        '* 「条件」ラベルを非表示
        Label23.Visible = False
        
        '* 「エレメント」リストビューを非表示
        LV_ElementView.Visible = False
        
        '* リストビューのへーだー「エレメント」ラベルを非表示
        Label25.Visible = False
        
        '* リストビューのへーだー「条件」ラベルを非表示
        Label24.Visible = False
    End If
        
End Sub

'*****************
'* フォーム初期化
'*****************
Private Sub UserForm_Initialize()
'    Dim missionList()   As MISSIONINFO
'    Dim II              As Integer
'    Dim JJ              As Integer
'    Dim KK              As Integer
'    Dim rgbColor(15)    As Long
'
'    '* 補助式展開のボタン初期表示は"＋"
'    BT_Assisted.Caption = StrPlus
'
'    '* 補助式エリアは非表示
'    TB_AssistedArea.Visible = False
'
'    '* ボタンの名前定義
'    CB_Refer.Caption = "..."
'    '* 演算形式の初期化
'    CB_CalcFormat.Clear
'    CB_CalcFormat.AddItem "個数"
'    CB_CalcFormat.AddItem "積算"
'    CB_CalcFormat.AddItem "平均"
'    CB_CalcFormat.AddItem "最大"
'    CB_CalcFormat.AddItem "最小"
'    CB_CalcFormat.AddItem "種類数"
'    CB_CalcFormat.AddItem "個数(単一条件)"
'    CB_CalcFormat.AddItem "個数(複数条件)"
'
'    '* 初期色を取得
'    Call GetRGBColorArray(rgbColor)
'    Label11.BackColor = rgbColor(0)
'    Label9.BackColor = rgbColor(1)
'    Label8.BackColor = rgbColor(2)
'    Label10.BackColor = rgbColor(3)
'    Label12.BackColor = rgbColor(4)
'    Label13.BackColor = rgbColor(5)
'    Label14.BackColor = rgbColor(6)
'    Label15.BackColor = rgbColor(7)
'    Label16.BackColor = rgbColor(8)
'    Label17.BackColor = rgbColor(9)
'    Label18.BackColor = rgbColor(10)
'    Label19.BackColor = rgbColor(11)
'    Label20.BackColor = rgbColor(12)
'    Label21.BackColor = rgbColor(13)
'    Label22.BackColor = rgbColor(14)
'
'    '* チェックボックスの初期値がfalseを設定
'    CB_Element.Value = False
'    CB_Link.Value = False
'    '* 書式は数値がデフォルト
'    OB_StatusMonitor.Value = True
'    Call CB_Element_Click
'    Call CB_Link_Click
'
'    '* ミッションリストの取得
'    Call GetMissionList(MissionSheetName, missionList)
'
'    With LV_Element
'        .AllowColumnReorder = True
'        .Enabled = True
'        .LabelEdit = lvwManual
'        .FullRowSelect = True
'        .ColumnHeaders.Add , "_Element", "エレメント名"
'        .ColumnHeaders.Item("_Element").Width = 150
'    End With
'
'    With LV_ElementView
'        .AllowColumnReorder = True
'        .Enabled = True
'        .LabelEdit = lvwManual
'        .FullRowSelect = True
'        .ColumnHeaders.Add 1, "_Id", "ID", 15
'        .ColumnHeaders.Add 2, "_Element", "エレメント", 78
'        .ColumnHeaders.Add 3, "_DisplayType", "条件", 34
'        .ColumnHeaders.Add 4, "_Ctm", "", 0
'
'    End With
'
'    '* 取得したミッション情報のリストボックスへの出力
'    LB_Mission.Clear
'    For II = 0 To UBound(missionList)
'        LB_Mission.AddItem missionList(II).Name
'    Next
'    LB_Mission.SetFocus
'    LB_Mission.ListIndex = 0        ' リストボックスの最初の項目を選択状態にする
'
'    '* 書式は数値がデフォルト
'    OB_Numeric.Value = True

End Sub

'***************************************
'* ミッションリストボックス選択時の処理
'***************************************
Private Sub LB_Mission_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer

    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)

    mIndex = -1
    For II = 0 To UBound(missionList)
        If missionList(II).Name = LB_Mission.text Then
            mIndex = II
            Exit For
        End If
    Next

    LB_CTM.Clear
    If mIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM)
            LB_CTM.AddItem missionList(mIndex).CTM(II).Name
        Next
        LB_CTM.SetFocus
        LB_CTM.ListIndex = 0
    End If
End Sub

'***************************************
'* CTMリストボックス選択時の処理
'***************************************
Private Sub LB_CTM_Change()
    Dim missionList()   As MISSIONINFO
    Dim II      As Integer
    Dim JJ      As Integer
    Dim mIndex  As Integer
    Dim cIndex  As Integer
    
    '* ミッションリストの取得
    Call GetMissionList(MissionSheetName, missionList)
    
    mIndex = -1
    cIndex = -1
    For II = 0 To UBound(missionList)
        For JJ = 0 To UBound(missionList(II).CTM)
            If missionList(II).CTM(JJ).Name = LB_CTM.text Then
                mIndex = II
                cIndex = JJ
                Exit For
            End If
        Next
        If cIndex > -1 Then Exit For
    Next
    
'    LB_Element.Clear
'    If mIndex > -1 And cIndex > -1 Then
'        For II = 0 To UBound(missionList(mIndex).CTM(cIndex).Element)
'            LB_Element.AddItem missionList(mIndex).CTM(cIndex).Element(II).Name
'        Next
'    End If
    LV_Element.ListItems.Clear
    If mIndex > -1 And cIndex > -1 Then
        For II = 0 To UBound(missionList(mIndex).CTM(cIndex).Element)
            LV_Element.ListItems.Add.text = missionList(mIndex).CTM(cIndex).Element(II).Name
        Next
    End If
End Sub

'**********************
'* ボタン11押下時の処理
'**********************
Private Sub Label11_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(0)
    retcolor = Label_Click(Label11, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B2").Value = retcolor
    End If
    
End Sub

Private Sub Label9_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(1)
    retcolor = Label_Click(Label9, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B3").Value = retcolor
    End If
End Sub

Private Sub Label8_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(2)
    retcolor = Label_Click(Label8, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B4").Value = retcolor
    End If
End Sub

Private Sub Label10_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(3)
    retcolor = Label_Click(Label10, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B5").Value = retcolor
    End If
End Sub

Private Sub Label12_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(4)
    retcolor = Label_Click(Label12, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B6").Value = retcolor
    End If
End Sub

Private Sub Label13_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(5)
    retcolor = Label_Click(Label13, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B7").Value = retcolor
    End If
End Sub

Private Sub Label14_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(6)
    retcolor = Label_Click(Label14, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B8").Value = retcolor
    End If
End Sub

Private Sub Label15_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(7)
    retcolor = Label_Click(Label15, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B9").Value = retcolor
    End If
End Sub

Private Sub Label16_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(8)
    retcolor = Label_Click(Label16, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B10").Value = retcolor
    End If
End Sub

Private Sub Label17_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(9)
    retcolor = Label_Click(Label17, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B11").Value = retcolor
    End If
End Sub

Private Sub Label18_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(10)
    retcolor = Label_Click(Label18, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B12").Value = retcolor
    End If
End Sub

Private Sub Label19_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
 
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(11)
    retcolor = Label_Click(Label19, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B13").Value = retcolor
    End If
End Sub

Private Sub Label20_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)
    
    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(12)
    retcolor = Label_Click(Label20, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B14").Value = retcolor
    End If

End Sub

Private Sub Label21_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)

    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(13)
    retcolor = Label_Click(Label21, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B15").Value = retcolor
    End If

End Sub

Private Sub Label22_Click()
    Dim paramColWorksheet      As Worksheet
    Dim lcolor                 As Long
    Dim rgbColor(15)           As Long
    Dim retcolor               As String
    
    Call SetFilenameToVariable
    Set paramColWorksheet = Workbooks(thisBookName).Worksheets(ColorParamSheetName)

    Call GetRGBColorArray(rgbColor)
    lcolor = rgbColor(14)
    retcolor = Label_Click(Label22, lcolor)
    
    If retcolor <> "" Then
        paramColWorksheet.Range("B16").Value = retcolor
    End If
End Sub

'****************************
'* 表示色ラベルボタン設定処理
'****************************
Public Function Label_Click(LableNm As Object, lcolor As Long) As String
    
    'シートから取得したのデータをRGBに変換する
    b = Int(lcolor / 65536)
    g = Int((lcolor Mod 65536) / 256)
    r = (lcolor Mod 65536) Mod 256
    
    '色を選択したの場合
    If Application.Dialogs(xlDialogEditColor).Show(10, r, g, b) = True Then
        Label_Click = ActiveWorkbook.Colors(10)
        LableNm.BackColor = Label_Click
    Else
        Label_Click = ""
    End If
End Function



