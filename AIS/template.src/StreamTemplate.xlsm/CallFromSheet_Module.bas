Attribute VB_Name = "CallFromSheet_Module"
Option Explicit

'***********************************************
'* ダブルクリックでコールされる処理
'***********************************************
Public Sub CellDoubleClick(ByVal Target As Range)
    Dim currWorksheet       As Worksheet
    Dim checkRange          As Range
    Dim srchRange           As Range
    Dim lastRange           As Range
    Dim startRange          As Range
    Dim readIndex           As Integer

    Set currWorksheet = Worksheets(GraphEditSheetName)
    
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            Set startRange = srchRange.Offset(2, 0)
            Set lastRange = srchRange.End(xlDown).Offset(1, 0)
            Set checkRange = .Range(srchRange.Offset(0, 0), lastRange.Offset(0, 2))
        
            Set srchRange = Application.Intersect(Target, checkRange)
            
            If Not srchRange Is Nothing Then
                '* 背景データ編集画面を開く
                Load BackDataForm
                readIndex = 0
                Do While Not IsEmpty(startRange.Offset(readIndex, 0).Value)
                    If "−" <> startRange.Offset(readIndex, 0).Value Then  '**** 20160530
                        With BackDataForm.LV_BackDataList.ListItems.Add
                            .text = startRange.Offset(readIndex, 0).Value
                            .SubItems(1) = startRange.Offset(readIndex, 1).Value
                            .SubItems(2) = startRange.Offset(readIndex, 2).Value
                        End With
                    End If
                    readIndex = readIndex + 1
                Loop
                BackDataForm.Show vbModeless
                'Worksheets(1).Activate
            End If
        End If
    End With

End Sub

'***********************************************
'* 基準値セルに変化があった時にコールされる処理
'***********************************************
Public Sub CellValueChange(ByVal Target As Range)
    Dim srchRange           As Range
    Dim currWorksheet       As Worksheet
    Dim vCheckRange         As Range
    Dim hCheckRange         As Range
    Dim vStartRange         As Range
    Dim hStartRange         As Range
    Dim vBaseList()         As BASEINFO
    Dim hBaseList()         As BASEINFO
    Dim vListIndex          As Integer
    Dim hListIndex          As Integer
    Dim diffRow             As Integer
    Dim addRange            As Range
    Dim titleRange          As Range
    Dim workStr             As String
    
    ReDim vBaseList(0)
    ReDim hBaseList(0)
    
    If Target.Offset(0, 0).text = Null Then Exit Sub
    workStr = Format(Target.Offset(0, 0).text)
    If workStr = "−" Then Exit Sub
    
    Set currWorksheet = Worksheets(GraphEditSheetName)
    
    With currWorksheet

        Set srchRange = .Cells.Find(What:="−横軸基準線−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
'            Set vStartRange = srchRange.Offset(1, 0)
'            Set vCheckRange = .Range(srchRange.Offset(1, 0), srchRange.Offset(25, 0))
'            Set srchRange = .Cells.Find(What:="−横軸基準線−", LookAt:=xlWhole)
            Set hStartRange = srchRange.Offset(1, 0)
            Set hCheckRange = .Range(srchRange.Offset(1, 0), srchRange.Offset(25, 0))
    
            '* 現在設定されている基準線リストを取得する
            Call GetBaseLineInfo(GraphEditSheetName, vBaseList, hBaseList)
                
'            '* 縦範囲チェック
'            Set srchRange = Application.Intersect(Target, vCheckRange)
'            If Not srchRange Is Nothing Then
'
'                If vBaseList(0).Name <> "" Then
'
'                    '* 現在設定されている基準線の数を取得する
'                    vListIndex = UBound(vBaseList)
'
'                    diffRow = Target.Row - vStartRange.Row
'
'                    '* 編集セルが最下行であれば
'                    If diffRow = vListIndex Then
'
'                        '* 空白行を追加し書式設定する
'                        Call SetCellFormat(Target)
'
'                    End If
'                End If
'            End If
'
            '* 横範囲チェック
            If Target Is Nothing Then Exit Sub
            Set srchRange = Application.Intersect(Target, hCheckRange)
            If Not srchRange Is Nothing Then
                
                If hBaseList(0).Name <> "" Then
                    
                    '* 現在設定されている基準線の数を取得する
                    hListIndex = UBound(hBaseList)
                    
                    diffRow = Target.row - hStartRange.row
                    
                    '* 編集セルが最下行であれば
                    If diffRow = hListIndex Then
                        
                        '* 空白行を追加し書式設定する
                        'Call SetCellFormat(Target)
                    
                    End If
                End If
            End If
        End If
    
    End With
    
End Sub

'*********************
'* 基準値セル書式設定
'*********************
Public Sub SetCellFormat(Target As Range)
    Dim addRange        As Range
    Dim titleRange      As Range
    Dim currWorksheet   As Worksheet
    
    Set currWorksheet = ActiveSheet
    
    Set addRange = currWorksheet.Range(Target.Offset(1, 0), Target.Offset(1, 1))
    Set titleRange = Target.Offset(1, 0)
                    
    '* 罫線の処理
    With addRange
    
        .Borders(xlInsideVertical).LineStyle = xlDouble
        .Borders(xlEdgeTop).LineStyle = xlDot
        .Borders(xlEdgeTop).Weight = xlThin
        .Borders(xlEdgeBottom).LineStyle = xlContinuous 'xlDot
        .Borders(xlEdgeBottom).Weight = xlMedium
        .Borders(xlEdgeLeft).LineStyle = xlContinuous
        .Borders(xlEdgeLeft).Weight = xlMedium
        .Borders(xlEdgeRight).LineStyle = xlContinuous
        .Borders(xlEdgeRight).Weight = xlMedium
    
'        .Borders(xlInsideVertical).LineStyle = xlDouble
'        .Borders(xlEdgeTop).LineStyle = xlDot
'        .Borders(xlEdgeTop).Weight = xlThin
'        .Borders(xlEdgeBottom).LineStyle = xlDot
'        .Borders(xlEdgeBottom).Weight = xlThin
'        .Borders(xlEdgeLeft).LineStyle = xlContinuous
'        .Borders(xlEdgeLeft).Weight = xlMedium
'        .Borders(xlEdgeRight).LineStyle = xlContinuous
'        .Borders(xlEdgeRight).Weight = xlMedium
        With .Font
            .Bold = False
            .Color = RGB(0, 0, 0)
            .Name = "メイリオ"
            .Size = 11
        End With
    End With
    
    '* セル背景／フォント処理
    With titleRange
        .Interior.Color = RGB(0, 176, 80)
        .Value = "−"
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        With .Font
            .Bold = True
            .Color = RGB(255, 255, 255)
        End With
    End With
    
    'データ入力規則：数値
    titleRange.Offset(0, 1).Validation.Delete
    titleRange.Offset(0, 1).Validation.Add Type:=xlValidateDecimal, AlertStyle:=xlValidAlertStop, _
                            Operator:=xlBetween, Formula1:="-2147483648", Formula2:="2147483647"

End Sub


'***********************************************
'* 背景データの手入力時、罫線作成
'***********************************************
Public Sub HaikeiDataInput(ByVal Target As Range)
    Dim wrkStr          As String
    Dim wrkTarget       As Range
    Dim currTarget      As Range
    Dim srchRange       As Range
    Dim titleRange      As Range
    
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim currWorksheet   As Worksheet
    Dim wrkVal
            
    If ActiveSheet.Name <> GraphEditSheetName Then
        Exit Sub
    End If
    
    '行チェック：
    wrkVal = Target.Offset(0, 0).row
    If wrkVal < 6 Then
        Exit Sub
    End If
    
    If Target.Offset(0, 0).text = Null Or Target.Offset(0, 0).text = "" Or Target.Offset(0, 0).text = "-" Or Target.Offset(0, 0).text = "−" Then
        Exit Sub
    End If
    
    '列チェック：背景データの列での変化か？
    Set currWorksheet = ActiveSheet
    Set srchRange = currWorksheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    wrkVal = srchRange.Column
    If wrkVal <> Target.Column Then
        Exit Sub
    End If
    
    '*********************************************** 20160530
    Set currTarget = currWorksheet.Range(Target.Offset(0, 0), Target.Offset(0, 0))
    With currTarget
        .Interior.Color = RGB(0, 112, 192)
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        With .Font
            .Bold = True
            .Color = RGB(255, 255, 255)
            .Name = "メイリオ"
            .Size = 11
        End With
    End With
    
    Set currTarget = currWorksheet.Range(Target.Offset(0, 0), Target.Offset(0, 2))
    With currTarget
        .Borders.LineStyle = xlContinuous
        .Borders.Weight = xlThin
        .Borders(xlEdgeTop).LineStyle = xlContinuous  'xlDot
        .Borders(xlEdgeTop).Weight = xlThin
        .Borders(xlEdgeBottom).LineStyle = xlContinuous
        .Borders(xlEdgeBottom).Weight = xlThin  'xlMedium
        .Borders(xlEdgeLeft).LineStyle = xlContinuous
        .Borders(xlEdgeLeft).Weight = xlThin  'xlMedium
        .Borders(xlEdgeRight).LineStyle = xlContinuous
        .Borders(xlEdgeRight).Weight = xlThin  'xlMedium
    End With
    '***********************************************
    
    ' 次行の罫線作成
    Set wrkTarget = currWorksheet.Range(Target.Offset(1, 0), Target.Offset(1, 2))
    With wrkTarget
        .Borders.LineStyle = xlContinuous
        .Borders.Weight = xlThin
        .Borders(xlEdgeTop).LineStyle = xlContinuous ' xlDot
        .Borders(xlEdgeTop).Weight = xlThin
        .Borders(xlEdgeBottom).LineStyle = xlContinuous
        .Borders(xlEdgeBottom).Weight = xlThin  'xlMedium
        .Borders(xlEdgeLeft).LineStyle = xlContinuous
        .Borders(xlEdgeLeft).Weight = xlThin  'xlMedium
        .Borders(xlEdgeRight).LineStyle = xlContinuous
        .Borders(xlEdgeRight).Weight = xlThin  'xlMedium

        With .Font
            .Bold = False
            .Color = RGB(0, 0, 0)
            .Name = "メイリオ"
            .Size = 11
        End With
    End With
    
    '* セル背景／フォント処理
    Set titleRange = Target.Offset(1, 0)
    With titleRange
        .Interior.Color = RGB(0, 112, 192)
        .Value = "−"
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        With .Font
            .Bold = True
            .Color = RGB(255, 255, 255)
        End With
    End With
        
    '罫線再書き込み
    '*********************************************** 20160530
    Set lastRange = srchRange.End(xlDown)
    Set startRange = currWorksheet.Range(srchRange.Offset(1, 0), lastRange.Offset(0, 2))
    'startRange.Select
    
    With startRange
        .Borders(xlEdgeLeft).LineStyle = xlContinuous
        .Borders(xlEdgeLeft).Weight = xlMedium
        
        .Borders(xlEdgeTop).LineStyle = xlContinuous
        .Borders(xlEdgeTop).Weight = xlMedium

        .Borders(xlEdgeBottom).LineStyle = xlContinuous
        .Borders(xlEdgeBottom).Weight = xlMedium

        .Borders(xlEdgeRight).LineStyle = xlContinuous
        .Borders(xlEdgeRight).Weight = xlMedium
        
        .Borders(xlInsideHorizontal).LineStyle = xlDot
        .Borders(xlInsideHorizontal).Weight = xlThin
    End With
    '*********************************************** 20160530
    
    Target.Offset(1, 0).Select

End Sub


