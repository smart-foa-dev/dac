Attribute VB_Name = "ScreenClear_Module"
Public Const StrPlus            As String = "＋"
Public Const StrMinus           As String = "−"
Public Const StrScrnChg         As String = "⇔"
Public Const StrSave            As String = "Ｓ"
Public Const PrefixStrBT        As String = "BT_shape_"
Public Const PrefixStrTX        As String = "TX_shape_"
Public Const PrefixButton        As String = "B_"
Public Const dfW                As Integer = 24
Public Const dfH                As Integer = 24

'*********************************************
'* 着目ワークシートにメニューボタンを生成する
'*********************************************
'Wang Issue of zoom display area while window size is changed 2018/06/08 Start
'Public Sub AddMenuButton()
Public Sub AddMenuButton(Optional isOpen As Boolean = False)
    Dim createButton            As Boolean
'Wang Issue of zoom display area while window size is changed 2018/06/08 End
    Dim currWorksheet           As Worksheet
    Dim currShape               As Shape
    Dim btSize                  As RECT
    Dim RCAddress               As String
    Dim currRange               As Range
    
    Dim workStr                 As String

    Set currWorksheet = ActiveSheet
    
    '* 着目シート上のボタン図形を全て削除する
    For Each currShape In currWorksheet.Shapes
        If InStr(currShape.Name, PrefixStrBT) Then
            'Wang Issue of zoom display area while window size is changed 2018/06/08 Start
            If isOpen And Not createButton Then
                createButton = (currShape.Name = (PrefixStrBT & StrScrnChg))
            End If
            'Wang Issue of zoom display area while window size is changed 2018/06/08 End
            currShape.Delete
        ElseIf InStr(currShape.Name, PrefixStrTX) Then
            currShape.Delete
        End If
    Next currShape

    '* 設定されている表示範囲のアドレスを取得し、範囲を設定する
    RCAddress = GetDisplayAddress(ActiveSheet)
    If RCAddress <> "" Then
        Set currRange = ActiveSheet.Range(RCAddress)
    
        '* ボタンサイズ設定
'        btSize.Top = currRange.Top + dfH + dfH / 2 + 2
        btSize.Top = currRange.Top + 1
        btSize.Right = dfW
        btSize.Bottom = dfH
        
        'Wang Issue of zoom display area while window size is changed 2018/06/06 Start
        'The following buttons are not needed any more
    
        'ISSUE_NO.789 sunyi 2018/07/17 Start
        'Excelバージョンが2010以前の場合、拡大縮小のボタンを作る
        If Application.Version <= 14 Then
            '* 拡大ボタンを追加
            btSize.Left = currRange.Left + 1
            Set currShape = PutButtonAtSheet(currWorksheet, btSize, RGB(0, 0, 255), RGB(0, 102, 255), PrefixStrBT, StrPlus, "ZoomDisplayPlus")
    
            '* 縮小ボタンを追加
            btSize.Left = currRange.Left + (dfW + 2) + 1
            Set currShape = PutButtonAtSheet(currWorksheet, btSize, RGB(0, 102, 0), RGB(0, 153, 0), PrefixStrBT, StrMinus, "ZoomDisplayMinus")
    
            '* 画面切り替えボタンを追加
            btSize.Left = currRange.Left + (dfW + 2) * 2 + 1
            'Wang Issue PAT-136 20190326 Start
'            Set currShape = PutButtonAtSheet(currWorksheet, btSize, RGB(255, 102, 0), RGB(255, 153, 0), PrefixStrBT, StrScrnChg, "ScreenDisplayChange")
            'Wang Issue PAT-136 20190326 End
        Else
        'ISSUE_NO.789 sunyi 2018/07/17 End
        
             'Wang Issue of zoom display area while window size is changed 2018/06/08 Start
        '        If MsgBox("「Ctrl+D」でツールバーの表示・非表示を切り替えることができます。" & Chr(13) & Chr(10) & "切り替えボタンも表示しますか。", vbYesNo) = vbYes Then
        '            btSize.Left = currRange.Left + (dfW + 2) * 2 + 1
        '            Set currShape = PutButtonAtSheet(currWorksheet, btSize, RGB(255, 102, 0), RGB(255, 153, 0), PrefixStrBT, StrScrnChg, "ScreenDisplayChange")
        '        End If
            If Not isOpen Then
                'Wang Issue PAT-136 20190326 Start
'                createButton = (MsgBox("「Ctrl+D」でツールバーの表示・非表示を切り替えることができます。" & Chr(13) & Chr(10) & "切り替えボタンも表示しますか。", vbYesNo) = vbYes)
                createButton = True
                'Wang Issue PAT-136 20190326 End
            End If
            If createButton Then
                btSize.Left = currRange.Left + (dfW + 2) * 2 + 1
                'Wang Issue PAT-136 20190326 Start
'                Set currShape = PutButtonAtSheet(currWorksheet, btSize, RGB(255, 102, 0), RGB(255, 153, 0), PrefixStrBT, StrScrnChg, "ScreenDisplayChange")
                'Wang Issue PAT-136 20190326 End
            End If
        'ISSUE_NO.789 sunyi 2018/07/17 Start
        'Excel 2010の場合、拡大縮小のボタンを作る
        End If
        'ISSUE_NO.789 sunyi 2018/07/17 End
        'Wang Issue of zoom display area while window size is changed 2018/06/08 End
        'Wang Issue of zoom display area while window size is changed 2018/06/06 End
    
        '* 更新状態テキストボックスを追加
'        btSize.Top = currRange.Top
'        btSize.Left = currRange.Left + 25
'        btSize.Right = 72
'        Set currShape = PutTextBoxAtSheet(currWorksheet, btSize, RGB(0, 0, 0), RGB(255, 255, 255), PrefixStrTX, "ONLINE", "")
    
    End If


'    currWorksheet.Shapes(PrefixStrBT & "ONLINE").TextFrame2.TextRange.Characters.Text = "更新中"
'    currWorksheet.Shapes(PrefixStrBT & "ONLINE").TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 255)
'
'    MsgBox "xxx"
'
'    currWorksheet.Shapes(PrefixStrBT & "ONLINE").TextFrame2.TextRange.Characters.Text = "停止中"
'    currWorksheet.Shapes(PrefixStrBT & "ONLINE").TextFrame2.TextRange.Font.Fill.ForeColor.RGB = RGB(255, 0, 0)
End Sub

'*************************
'* メニューボタン生成処理
'*************************
Public Function PutButtonAtSheet(currWorksheet As Worksheet, _
                            btSize As RECT, rgbLine As Long, rgbFill As Long, _
                            prefixStr As String, btName As String, macroName As String) As Shape
    Dim currShape       As Shape
    Dim workStr         As String

    '* ボタンを追加
    Set currShape = currWorksheet.Shapes.AddShape(msoShapeRoundedRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        workStr = prefixStr & btName
        .Name = workStr
        .OnAction = macroName
        With .TextFrame2
            '* 文字列配置中央
            .VerticalAnchor = msoAnchorMiddle
            .TextRange.ParagraphFormat.Alignment = msoAlignCenter
            .TextRange.Characters.text = btName
            .TextRange.Font.Bold = msoTrue
            .TextRange.Font.Size = 20
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With

    Set PutButtonAtSheet = currShape

End Function

'*************************
'* BulkyDataボタン生成処理
'*************************
Public Function PutButtonAtSheetOfMap(currWorksheet As Worksheet, _
                            btSize As RECT, rgbLine As Long, rgbFill As Long, _
                            prefixStr As String, btName As String, macroName As String, strName As String) As Shape
    Dim currShape       As Shape
    Dim workStr         As String

    '* ボタンを追加
    Set currShape = currWorksheet.Shapes.AddShape(msoShapeRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        workStr = prefixStr & btName
        .Name = workStr
        .OnAction = macroName
        With .TextFrame2
            '* 文字列配置中央
            .VerticalAnchor = msoAnchorMiddle
            .TextRange.ParagraphFormat.Alignment = msoAlignCenter
            .TextRange.Characters.text = strName
            .TextRange.Font.Bold = msoTrue
            .TextRange.Font.Size = 12
        End With
        With .ThreeD
            .BevelTopType = msoBevelCircle
            .BevelTopInset = 6
            .BevelTopDepth = 6
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With

    Set PutButtonAtSheetOfMap = currShape

End Function

'*************************
'* BulkyDataテキスト生成処理
'*************************
Public Function PutMaxTextAtSheetOfMap(currWorksheet As Worksheet, _
                            btSize As RECT, rgbLine As Long, rgbFill As Long, _
                            prefixStr As String, btName As String, strName As String) As Shape
    Dim currShape       As Shape
    Dim workStr         As String

    '* ボタンを追加
    Set currShape = currWorksheet.Shapes.AddShape(msoShapeRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        workStr = prefixStr & btName
        .Name = workStr
        With .TextFrame2
            '* 文字列配置中央
            .VerticalAnchor = msoAnchorMiddle
            .TextRange.ParagraphFormat.Alignment = msoAlignCenter
            .TextRange.Characters.text = strName
            .TextRange.Font.Bold = msoTrue
            .TextRange.Font.Size = 12
            .TextRange.Font.Fill.ForeColor.RGB = RGB(0, 0, 0)
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With

    Set PutMaxTextAtSheetOfMap = currShape

End Function

Public Sub SetButtonTitle(CtmName As String)

    Dim fixGraphWorksheet       As Worksheet
    Dim currShape               As Shape
    Dim writeRange              As Range
    Dim buttonRange             As Range
    Dim wrkCount                As Integer
    Dim pos                     As Integer
    Dim wrkStr                  As String
    Dim nameRange               As Range
    Dim btSize                  As RECT
    Dim strRange As String
    Dim buttonRow As Long
    Dim workStr         As String
    Dim rgbLine As Long
    Dim rgbFill As Long
    
    rgbLine = RGB(255, 255, 255)
    rgbFill = RGB(0, 0, 255)
'*
    buttonRow = ButtonStartRow - 4
    strRange = "A" & buttonRow & ":D" & buttonRow

    Set buttonRange = Worksheets(FixGraphSheetName).Range(strRange)
    
    btSize.Top = buttonRange.Top
    btSize.Left = buttonRange.Left + 1
    btSize.Right = buttonRange.Width - 1
    btSize.Bottom = buttonRange.Height * 2
    '* ボタンを追加
    Set currShape = Worksheets(FixGraphSheetName).Shapes.AddShape(msoShapeRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        .Name = PrefixButton + STEAM_MODULE_NAME
        With .TextFrame2
            '* 文字列配置中央
            .VerticalAnchor = msoAnchorMiddle
            .TextRange.ParagraphFormat.Alignment = msoAlignCenter
            .TextRange.Characters.text = STEAM_MODULE_NAME + ":" + GetModelName(STEAM_MODULE_NAME, CtmName)
            .TextRange.Font.Bold = msoTrue
            .TextRange.Font.Size = 12
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With
'*
    buttonRow = ButtonStartRow - 2
    strRange = "E" & buttonRow & ":M" & buttonRow

    Set buttonRange = Worksheets(FixGraphSheetName).Range(strRange)
    
    btSize.Top = buttonRange.Top
    btSize.Left = buttonRange.Left + 1
    btSize.Right = buttonRange.Width - 2
    btSize.Bottom = buttonRange.Height * 2
    '* ボタンを追加
    Set currShape = Worksheets(FixGraphSheetName).Shapes.AddShape(msoShapeRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        .Name = PrefixButton + TORQUE_STEAM
        With .TextFrame2
            '* 文字列配置中央
            .VerticalAnchor = msoAnchorMiddle
            .TextRange.ParagraphFormat.Alignment = msoAlignCenter
'            .TextRange.Characters.Text = GetModelName("コントローラ名") + Space(10) + "軸" + Space(10) + TORQUE_STEAM
            .TextRange.Font.Bold = msoTrue
            .TextRange.Font.Size = 12
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With
'*
    buttonRow = ButtonStartRow - 2
    strRange = "A" & buttonRow & ":B" & buttonRow

    Set buttonRange = Worksheets(FixGraphSheetName).Range(strRange)
    
    btSize.Top = buttonRange.Top
    btSize.Left = buttonRange.Left + 1
    btSize.Right = buttonRange.Width - 2
    btSize.Bottom = buttonRange.Height * 2
    '* ボタンを追加
    Set currShape = Worksheets(FixGraphSheetName).Shapes.AddShape(msoShapeRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        .Name = PrefixButton + "軸"
        With .TextFrame2
            '* 文字列配置中央
            .VerticalAnchor = msoAnchorMiddle
            .TextRange.ParagraphFormat.Alignment = msoAlignCenter
            .TextRange.Characters.text = "軸"
            .TextRange.Font.Bold = msoTrue
            .TextRange.Font.Size = 10
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With
'*
    buttonRow = ButtonStartRow - 2
    strRange = "C" & buttonRow & ":C" & buttonRow

    Set buttonRange = Worksheets(FixGraphSheetName).Range(strRange)
    
    btSize.Top = buttonRange.Top
    btSize.Left = buttonRange.Left + 1
    btSize.Right = buttonRange.Width - 2
    btSize.Bottom = buttonRange.Height * 2
    '* ボタンを追加
    Set currShape = Worksheets(FixGraphSheetName).Shapes.AddShape(msoShapeRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        .Name = PrefixButton + "最大トルク"
        With .TextFrame2
            '* 文字列配置中央
            .VerticalAnchor = msoAnchorMiddle
            .TextRange.ParagraphFormat.Alignment = msoAlignCenter
            .TextRange.Characters.text = "最大" & Chr(10) & "トルク"
            .TextRange.Font.Bold = msoTrue
            .TextRange.Font.Size = 10
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With
'*
    buttonRow = ButtonStartRow - 2
    strRange = "D" & buttonRow & ":D" & buttonRow

    Set buttonRange = Worksheets(FixGraphSheetName).Range(strRange)
    
    btSize.Top = buttonRange.Top
    btSize.Left = buttonRange.Left + 1
    btSize.Right = buttonRange.Width - 2
    btSize.Bottom = buttonRange.Height * 2
    '* ボタンを追加
    Set currShape = Worksheets(FixGraphSheetName).Shapes.AddShape(msoShapeRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        .Name = PrefixButton + "平均トルク"
        With .TextFrame2
            '* 文字列配置中央
            .VerticalAnchor = msoAnchorMiddle
            .TextRange.ParagraphFormat.Alignment = msoAlignCenter
            .TextRange.Characters.text = "平均" & Chr(10) & "トルク"
            .TextRange.Font.Bold = msoTrue
            .TextRange.Font.Size = 10
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With
    
End Sub

Public Function GetModelName(wrkStr As String, CtmName As String)
    Dim ctmDataWorksheet          As Worksheet
    
    Dim allData                 As Variant
    Dim iEndCol                 As Long
    Dim sDataSheetName          As String
    Dim sEndColValue            As String
    
    Dim II                      As Long
    Dim JJ                      As Long
    
    If CtmName = "" Then Exit Function

    Set ctmDataWorksheet = Worksheets(CtmName)
    
    With ctmDataWorksheet
        iEndCol = .UsedRange.Columns.count
    End With
    
    sEndColValue = GetColumnChar(iEndCol)
    allData = ctmDataWorksheet.Range("A1:" & sEndColValue & DataSheetStartRow)
    If allData(DataSheetStartRow, 1) = "" Then Exit Function

    For JJ = 1 To iEndCol
        If InStr(allData(1, JJ), wrkStr) > 0 Then
            GetModelName = allData(DataSheetStartRow, JJ)
            Exit For
        End If
    Next JJ
End Function


'*************************
'* テキストボックス生成処理
'*************************
Public Function PutTextBoxAtSheet(currWorksheet As Worksheet, _
                            btSize As RECT, rgbLine As Long, rgbFill As Long, _
                            prefixStr As String, btName As String, macroName As String) As Shape
    Dim currShape       As Shape
    Dim workStr         As String

    '* ボタンを追加
    Set currShape = currWorksheet.Shapes.AddShape(msoShapeRoundedRectangle, btSize.Left, btSize.Top, btSize.Right, btSize.Bottom)
    
    With currShape
        workStr = prefixStr & btName
        .Name = workStr
        .OnAction = macroName
        With .TextFrame2
            '* 文字列配置中央
            .MarginLeft = 0
            .MarginRight = 0
            .VerticalAnchor = msoAnchorMiddle
            With .TextRange
                .ParagraphFormat.Alignment = msoAlignCenter
'                .Characters.Text = btName
                With .Font
                    .Bold = msoTrue
                    .Size = 14
                    .Fill.ForeColor.RGB = RGB(0, 0, 0)
                End With
            End With
        End With
        .Line.ForeColor.RGB = rgbLine
        .Fill.ForeColor.RGB = rgbFill
    End With
    
    Set PutTextBoxAtSheet = currShape

End Function

'***********************************
'* 画面表示不要部分表示／非表示処理
'***********************************
Public Sub ScreenDisplayONOFF(dispFlag As Boolean)
    Dim workStr         As String

    workStr = "SHOW.TOOLBAR(""Ribbon""," & dispFlag & ")"

    ' リボンを非表示にする
    Application.ExecuteExcel4Macro workStr
    '* 数式バーを非表示
    Application.DisplayFormulaBar = dispFlag
    '* スクロールバーを非表示
    Application.DisplayScrollBars = dispFlag
    '* ステータスバーを非表示
    Application.DisplayStatusBar = dispFlag
    '* 行番号を非表示
    ActiveWindow.DisplayHeadings = dispFlag
    '* シートタブを非表示
    ActiveWindow.DisplayWorkbookTabs = dispFlag

End Sub

'**************************
'* スクリーン不要情報ONOFF
'**************************
Public Sub ScreenDisplayChange()
    Dim flagDIsp    As Boolean
    
    'Wang Issue of zoom display area while window size is changed 2018/06/06 Start
    ThisWorkbook.ForbidCalcSize = True
    'Wang Issue of zoom display area while window size is changed 2018/06/06 End
    
    flagDIsp = Not ActiveWindow.DisplayHeadings

    '* 諸々バーを非表示
    Call ScreenDisplayONOFF(flagDIsp)
    
    'Call GoZoomDisplay(ActiveWindow.Zoom)
    
    'Wang Issue of zoom display area while window size is changed 2018/06/06 Start
    'Call GoZoomDisplay(ActiveWindow.Zoom)
    '*ISSUE_NO.811 Sunyi 2018/08/03 Start
    '*Excel2010の場合、サイズ自動調整をしないように修正
    If Application.Version <= 14 Then
        Call GoZoomDisplay(ActiveWindow.Zoom)
    Else
        Call ThisWorkbook.displayAreaObject.ResizeWindow(ActiveWindow, True)
    End If
    'ISSUE_NO.811 Sunyi 2018/08/03 End
    ThisWorkbook.ForbidCalcSize = False
    'Wang Issue of zoom display area while window size is changed 2018/06/06 End


End Sub

'**********************************************
'* スクリーン不要情報ONOFF(ファイルオープン時)
'**********************************************
Public Sub ScreenDisplayChangeForOpen()
    'Wang Issue of zoom display area while window size is changed 2018/06/06 Start
    ThisWorkbook.ForbidCalcSize = True
    'Wang Issue of zoom display area while window size is changed 2018/06/06 End
    
    '* メニューボタンを追加
    'Wang Issue of zoom display area while window size is changed 2018/06/08 Start
    'Call AddMenuButton
    Call AddMenuButton(True)
    'Wang Issue of zoom display area while window size is changed 2018/06/08 End

    '* 諸々バーを非表示
    Call ScreenDisplayONOFF(False)
    
    'Wang Issue of zoom display area while window size is changed 2018/06/06 Start
    'ISSUE_NO.789 sunyi 2018/07/17 Start
    'Excelバージョンが2010以前の場合、拡大縮小のボタンを作る
    If Application.Version <= 14 Then
        Call GoZoomDisplay(ActiveWindow.Zoom)
    Else
        Call ThisWorkbook.displayAreaObject.ResizeWindow(ActiveWindow, True, True)
        If TypeName(Selection) = "Range" Then
            Call Selection.Resize(1, 1).Select
        End If
        ThisWorkbook.ForbidCalcSize = False
    End If
    'Wang Issue of zoom display area while window size is changed 2018/06/06 End
End Sub


