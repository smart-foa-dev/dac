Attribute VB_Name = "subScreen"
Option Explicit

Public Sub frmGraphSetDisp()
        frmGraphSet.Show vbModeless
End Sub

Public Sub frmBackDataDisp()
   frmBackData.Show vbModeless '
End Sub

Public Sub set_single_display_data(btnNum As Integer)

    Dim torqueWorksheet     As Worksheet
    Dim statusWorksheet     As Worksheet
    Dim maxTorWorksheet     As Worksheet
    Dim iDataRowCnt         As Integer
    Dim iDataColCnt         As Integer
    Dim iDisplayCnt         As Integer
    
    Call SetFilenameToVariable

    Set torqueWorksheet = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
    Set maxTorWorksheet = Workbooks(thisBookName).Worksheets(MaxTorqueSheetName)
    
    iDataRowCnt = maxTorWorksheet.UsedRange.Rows.count
    iDataColCnt = maxTorWorksheet.UsedRange.Columns.count
    iDisplayCnt = torqueWorksheet.Range("D1").Value
    
    If iDataRowCnt >= iDisplayCnt Then
        iDataRowCnt = iDisplayCnt
    End If
    
    statusWorksheet.ChartObjects("グラフ 1").Activate
    ActiveChart.SetSourceData Source:=maxTorWorksheet.Range( _
        "A1:A" & iDataRowCnt & "," & GetColumnChar(btnNum) & "1:" & GetColumnChar(btnNum) & iDataRowCnt)
    
End Sub

'*************************
'* 平均グラフ単一データ再設定
'*************************
Public Sub set_single_display_data_p(btnNum As Integer)

    Dim GraphWorksheet     As Worksheet
    Dim ForPivotWorksheet     As Worksheet
    Dim iDataRowCnt         As Integer
    Dim iDataColCnt         As Integer
    Dim iDisplayCnt         As Integer
    
    'Call SetFilenameToVariable

    Set GraphWorksheet = Workbooks(thisBookName).Worksheets(GraphEditSheetName)
    Set ForPivotWorksheet = Workbooks(thisBookName).Worksheets(ForPivotSheetName)
    
    iDataRowCnt = ForPivotWorksheet.UsedRange.Rows.count
    iDataColCnt = ForPivotWorksheet.UsedRange.Columns.count
    'iDisplayCnt = torqueWorksheet.Range("D1").Value
    
    'If iDataRowCnt >= iDisplayCnt Then
    '    iDataRowCnt = iDisplayCnt
    'End If
    
    statusWorksheet.ChartObjects("グラフ 2").Activate
    ActiveChart.SetSourceData Source:=avgTorWorksheet.Range( _
        "A1:A" & iDataRowCnt & "," & GetColumnChar(btnNum) & "1:" & GetColumnChar(btnNum) & iDataRowCnt)
'    ActiveChart.ChartTitle.text = "平均トルク履歴グラフ"
End Sub


'***********************
'* 各軸ボタンの処理
'***********************
Sub B_1_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(2)
    '* ボタン色再設定
    Call setButtonColor("B_", 1)
    Call MakeGraph2(1)
End Sub
Sub B_2_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(3)
    '* ボタン色再設定
    Call setButtonColor("B_", 2)
    Call MakeGraph2(2)
End Sub
Sub B_3_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(4)
    '* ボタン色再設定
    Call setButtonColor("B_", 3)
    Call MakeGraph2(3)
End Sub
Sub B_4_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(5)
    '* ボタン色再設定
    Call setButtonColor("B_", 4)
    Call MakeGraph2(4)
End Sub
Sub B_5_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(6)
    '* ボタン色再設定
    Call setButtonColor("B_", 5)
    Call MakeGraph2(5)
End Sub
Sub B_6_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(7)
    '* ボタン色再設定
    Call setButtonColor("B_", 6)
    Call MakeGraph2(6)
End Sub
Sub B_7_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(8)
    '* ボタン色再設定
    Call setButtonColor("B_", 7)
    Call MakeGraph2(7)
End Sub
Sub B_8_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(9)
    '* ボタン色再設定
    Call setButtonColor("B_", 8)
    Call MakeGraph2(8)
End Sub
Sub B_9_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(10)
    '* ボタン色再設定
    Call setButtonColor("B_", 9)
    Call MakeGraph2(9)
End Sub
Sub B_10_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(11)
    '* ボタン色再設定
    Call setButtonColor("B_", 10)
    Call MakeGraph2(10)
End Sub
Sub B_11_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(12)
    '* ボタン色再設定
    Call setButtonColor("B_", 11)
    Call MakeGraph2(11)
End Sub
Sub B_12_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(13)
    '* ボタン色再設定
    Call setButtonColor("B_", 12)
    Call MakeGraph2(12)
End Sub
Sub B_13_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(14)
    '* ボタン色再設定
    Call setButtonColor("B_", 13)
    Call MakeGraph2(13)
End Sub
Sub B_14_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(15)
    '* ボタン色再設定
    Call setButtonColor("B_", 14)
    Call MakeGraph2(14)
End Sub
Sub B_15_click()

    '* 最大グラフ単一データ再設定
    'Call set_single_display_data(16)
    '* ボタン色再設定
    Call setButtonColor("B_", 15)
    Call MakeGraph2(15)
End Sub

'****************************
'* ボタン色再設定
'****************************
Sub setButtonColor(btnType As String, btnNum As Integer)

    Dim statusWorksheet     As Worksheet
    Dim dataWorksheet       As Worksheet
    Dim currShape           As Shape
    Dim II                  As Integer
    Dim MaxCount            As Integer

'    Call SetFilenameToVariable

    Set statusWorksheet = Worksheets(FixGraphSheetName)
    
    Set dataWorksheet = Worksheets(ForPivotSheetName)
    MaxCount = dataWorksheet.UsedRange.Columns.count
    
    With statusWorksheet
        For II = 1 To MaxButtonsNum
            If II < MaxCount Then
                Set currShape = .Shapes(btnType & II)
                currShape.Line.ForeColor.RGB = RGB(255, 255, 255)
                currShape.Fill.ForeColor.RGB = RGB(0, 0, 255)
            End If
        Next
    End With
    
    Set currShape = statusWorksheet.Shapes(btnType & btnNum)
    currShape.Fill.ForeColor.ObjectThemeColor = msoThemeColorAccent6
   
End Sub


'****************************
'ボタンにより、データを取得
'****************************
Public Sub GetMaxTorqueData(strCol As String, nameRange As Range, wrkCount As Integer, maxCtmName As String)
    
    Dim fixGraphWorksheet       As Worksheet
    Dim pos                     As Integer
    Dim buttonRow               As Integer
    Dim strRange                As String
    Dim buttonRange             As Range
    Dim wrkStr                  As String
    Dim btSize                  As RECT
    Dim currShape               As Shape
    
    Set fixGraphWorksheet = Worksheets(FixGraphSheetName)   'グラフシート

    For pos = 1 To MaxButtonsNum
    
        buttonRow = pos + ButtonStartRow - 1
        strRange = strCol & buttonRow
    
        Set buttonRange = Worksheets(FixGraphSheetName).Range(strRange)
    
        wrkStr = nameRange.Offset(0, pos).Value2(1, 1)
        
        If pos < wrkCount Then
            btSize.Top = buttonRange.Top
            btSize.Left = buttonRange.Left + 1
            btSize.Right = buttonRange.Width - 2
            btSize.Bottom = buttonRange.Height
            wrkStr = MaxTorqueDataCreate(pos, wrkStr, strCol, maxCtmName)
            '*最大トルクシートがない、若しくは値がない時、"-"を出力します。
            If wrkStr = "" Then
                wrkStr = "-"
            End If
            Set currShape = PutMaxTextAtSheetOfMap(fixGraphWorksheet, btSize, RGB(255, 255, 255), RGB(192, 192, 192), PrefixButton, CStr(pos), wrkStr)
        End If
    Next pos
End Sub
'****************************
'* トルク表示用データを作成する
'****************************
Public Function MaxTorqueDataCreate(pos As Integer, strName As String, isMax As String, maxCtmName As String) As String

    Dim ctmDataWorksheet          As Worksheet
    
    Dim allData                 As Variant
    Dim iEndCol                 As Long
    Dim sDataSheetName          As String
    Dim sEndColValue            As String
    
    Dim II                      As Long
    Dim JJ                      As Long
    
    If isMax = "C" Then
        II = 1
    End If
    If isMax = "D" Then
        II = 2
    End If
    
    If maxCtmName = "" Then Exit Function

    Set ctmDataWorksheet = Worksheets(maxCtmName)
    
    With ctmDataWorksheet
        iEndCol = .UsedRange.Columns.count
    End With
    
    sEndColValue = GetColumnChar(iEndCol)
    allData = ctmDataWorksheet.Range("A1:" & sEndColValue & DataSheetStartRow)
    If allData(DataSheetStartRow, 1) = "" Then Exit Function

    For JJ = 1 To iEndCol - 2
        If InStr(allData(1, JJ), "軸番号") > 0 And InStr(allData(1, JJ + 1), "最大トルク") > 0 And InStr(allData(1, JJ + 2), "平均トルク") > 0 Then
            If allData(4, JJ) = strName Then
                MaxTorqueDataCreate = allData(DataSheetStartRow, JJ + II)
                Exit For
            End If
        End If
    Next JJ
    
End Function

'************************
'* 数字⇒列号　変更する
'************************
Function GetColumnChar(char)
    Dim t
    On Error Resume Next
    If IsNumeric(char) Then
        t = Split(Cells(1, char).Address, "$")(1)
    Else
'        t = Columns(char & ":" & char).Column
        t = char
    End If
    If Err.Number <> 0 Then GetColumnChar = "超える範囲" Else GetColumnChar = t
End Function

'*************************
'* グラフ単一データ再設定
'*************************
'Public Sub set_single_display_data(btnNum As Integer)

'    Dim torqueWorksheet     As Worksheet
'    Dim statusWorksheet     As Worksheet
'    Dim maxTorWorksheet     As Worksheet
'    Dim iDataRowCnt         As Integer
'    Dim iDataColCnt         As Integer
'    Dim iDisplayCnt         As Integer
'
'    Call SetFilenameToVariable
'
'    Set torqueWorksheet = Workbooks(thisBookName).Worksheets(TorqueSetDataSheetName)
'    Set statusWorksheet = Workbooks(thisBookName).Worksheets(StatusSheetName)
'    Set maxTorWorksheet = Workbooks(thisBookName).Worksheets(MaxTorqueSheetName)
'
'    iDataRowCnt = maxTorWorksheet.UsedRange.Rows.Count
'    iDataColCnt = maxTorWorksheet.UsedRange.Columns.Count
'    iDisplayCnt = torqueWorksheet.Range("D1").Value
'
'    If iDataRowCnt >= iDisplayCnt Then
'        iDataRowCnt = iDisplayCnt
'    End If
'
'    statusWorksheet.ChartObjects("グラフ 1").Activate
'    ActiveChart.SetSourceData Source:=maxTorWorksheet.Range( _
'        "A1:A" & iDataRowCnt & "," & GetColumnChar(btnNum) & "1:" & GetColumnChar(btnNum) & iDataRowCnt)
    
'End Sub

