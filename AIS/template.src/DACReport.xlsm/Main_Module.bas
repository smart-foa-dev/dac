Attribute VB_Name = "Main_Module"
Option Explicit

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSub()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim srchMulti       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
'    Dim objFSO          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    Dim IsFirstSave     As String
    
'    Call CancelSchedule

'    Set objFSO = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With ThisWorkbook.Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        ' Wang Issue NO.687 2018/05/18 Start
        ' 1.階層DAC追加(オプションテンプレート表示統一)
        ' 2.DAC Excel メニュー整理
        'If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
        Set srchMulti = .Cells.Find(What:="MultiDac有効", LookAt:=xlWhole)
        If Not srchMulti Is Nothing Then
            If UCase(CStr(srchMulti.Offset(0, 1).Value)) = "TRUE" Then
                templateName = "階層DAC"
            End If
        End If
        If Len(templateName) = 0 Then
            If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
        End If
        ' Wang Issue NO.687 2018/05/18 End
        Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then IsFirstSave = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
'    currThisFile = ThisWorkbook.FullName
'    Application.DisplayAlerts = False
'    ActiveWorkbook.SaveAs Filename:=currThisFile
'    Application.DisplayAlerts = True
'
'    wrkInt = InStr(ThisWorkbook.Name, templateName)
'
'    If wrkInt <= 0 Then
    If IsFirstSave = "" Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & ThisWorkbook.Name
        Else
            fname = saveFilePath & "\" & templateName & "_" & ThisWorkbook.Name
        End If
        
        '* テンプレート名／登録フォルダ名を取得する
        With ThisWorkbook.Worksheets(ParamSheetName).Columns(1)
            Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "TRUE"
        End With
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & ThisWorkbook.Name
        Else
            fname = saveFilePath & "\" & ThisWorkbook.Name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
    If Dir(fname) <> "" Then
      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
    End If
    
    '* 別名保存する
    Application.DisplayAlerts = False
    
    '* ISSUE_NO.624 Add ↓↓↓ *******************************
    On Error GoTo ErrorHandler
    
    ThisWorkbook.SaveAs Filename:=fname
    
ErrorHandler:
    '-- 例外処理
    If Err.Description <> "" Then
        MsgBox Err.Description, vbCritical & vbOKOnly, "警告"
    End If
    '* ISSUE_NO.624 Add ↑↑↑ *******************************
    
    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    If Application.Workbooks.Count > 1 Then
        ThisWorkbook.Close
    Else
        Application.Quit
        ThisWorkbook.Close
    End If
    
    If Err.Description = "" Then
        Application.DisplayAlerts = True
    End If
End Sub




