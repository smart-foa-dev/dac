Attribute VB_Name = "Main_Module"
Option Explicit

'****************
'* ｢テスト｣の処理
'****************
Public Sub OperationTest()
    Dim currWorksheet   As Worksheet
    Dim workPVT         As PivotTable
    Dim pvtRange        As Range
    Dim srchRange       As Range
    Dim sampleType      As String
    Dim wrkStr          As String
    
    Application.ScreenUpdating = False
    
    '収集タイプ
    Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        sampleType = srchRange.Offset(0, 1).Value
    End If
    
    
    '一定時間繰返し処理
    If sampleType <> "GRIP" Then
        '* ミッションから情報を取得
        Call GetMissionInfoAll
    Else
        Call GetGripMission
        
        'dac home 参照式のコピーのため、dac_refereを作成 start
        Dim flag As Boolean
        Dim var As Variant
        
        flag = False
        For Each var In ThisWorkbook.Worksheets
            If var.Name = "dac_refere" Then
                flag = True
                Exit For
            End If
        Next var
        If flag = False Then
            ThisWorkbook.Worksheets.Add.Name = "dac_refere"
            ThisWorkbook.Worksheets("dac_refere").Visible = False
        End If
        'dac home 参照式のコピーのため、dac_refereを作成 end
        
    End If
    
    Call RefreshPivotTableData

    Application.ScreenUpdating = True
    
    ThisWorkbook.Save
    
End Sub

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSub()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
'    Dim objFSO          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    Dim IsFirstSave     As String
    
        '表示倍率保存
    Call SaveBairitu
    
'    Call CancelSchedule

'    Set objFSO = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With ThisWorkbook.Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then IsFirstSave = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
'    currThisFile = ThisWorkbook.FullName
'    Application.DisplayAlerts = False
'    ActiveWorkbook.SaveAs Filename:=currThisFile
'    Application.DisplayAlerts = True

'    wrkInt = InStr(ThisWorkbook.Name, templateName)
'
'    If wrkInt <= 0 Or wrkInt > 4 Then
    If IsFirstSave = "" Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & ThisWorkbook.Name
        Else
            fname = saveFilePath & "\" & templateName & "_" & ThisWorkbook.Name
        End If
        '* テンプレート名／登録フォルダ名を取得する
        With ThisWorkbook.Worksheets(ParamSheetName).Columns(1)
            Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "TRUE"
        End With
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & ThisWorkbook.Name
        Else
            fname = saveFilePath & "\" & ThisWorkbook.Name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
    If Dir(fname) <> "" Then
      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
    End If
    
    '各種バー表示
    Call DispBar
    
    '* 別名保存する
    Application.DisplayAlerts = False
    
    '* ISSUE_NO.624 Add ↓↓↓ *******************************
    On Error GoTo ErrorHandler
    
    ThisWorkbook.SaveAs Filename:=fname
    
ErrorHandler:
    '-- 例外処理
    If Err.Description <> "" Then
        MsgBox Err.Description, vbCritical & vbOKOnly, "警告"
    End If
    '* ISSUE_NO.624 Add ↑↑↑ *******************************
    
    
    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    If Application.Workbooks.Count > 1 Then
        ThisWorkbook.Close
    Else
        Application.Quit
        ThisWorkbook.Close
    End If
    If Err.Description = "" Then
        Application.DisplayAlerts = True
    End If
        
End Sub

'---2016/12/16 Add No.383 アドインにグラフ更新(ピボット更新)追加 ----------------------------Start
'---以下、Bulkyテンプレートから移植
'****************************
'* ピボットテーブルを更新する
'****************************
Public Sub RefreshPivotTableData()
    Dim currWorksheet   As Worksheet
    Dim srcWorksheet    As Worksheet
    Dim workPVT         As PivotTable
    Dim pvtRange        As Range
    Dim startRange      As Range
    Dim dataRange       As Range
    Dim lastRange       As Range
    Dim dataSrc         As String
    Dim dataNewSrc      As String
    Dim srcSheetName    As String
    Dim rangeValue      As Long '---2016/12/19 Add No.385
    Dim srchRange       As Range   '---2017/04/27 Add No.647
    Dim sampleType      As String  '---2017/04/27 Add No.647
    
    Dim originalColumn As String
    Dim newColumn As String


    For Each currWorksheet In ThisWorkbook.Worksheets
        For Each workPVT In currWorksheet.PivotTables
            With workPVT
                Set pvtRange = .TableRange1
                
                '* 対象ピボットテーブルのデータソースを取得する
                dataSrc = .SourceData
                
                originalColumn = GetLastNumber(dataSrc)
                
                
                '* データソースのシート名を取得する
                srcSheetName = Left(dataSrc, InStr(dataSrc, "!") - 1)
                
                '---2017/04/27 Add No.647 収集タイプ判定・シート名編集追加 ----Start
                '* GripMissionの場合はシート名を編集する
                Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)
                Set srchRange = currWorksheet.Cells.Find(What:="収集タイプ", LookAt:=xlWhole)
                If Not srchRange Is Nothing Then
                    sampleType = srchRange.Offset(0, 1).Value
                End If
                
'                If sampleType = "GRIP" Then
                    srcSheetName = Replace(srcSheetName, "'", "")
'                End If
               '---2017/04/27 Add No.647 収集タイプ判定・シート名編集追加 ----End
                
                Set srcWorksheet = ThisWorkbook.Worksheets(srcSheetName)
                With srcWorksheet
                    Set startRange = .Range("A1")
                    Set dataRange = .Range(startRange, startRange.End(xlDown).End(xlToRight))
                    rangeValue = Len(.Range("A2").Value)  '---2016/12/19 Add No.385
                End With
                '---2016/12/19 Add No.385 データが存在するときのみ更新実行 IF文追加 -------------Start
                '* データが存在すればピボットテーブルを更新する
                If rangeValue > 0 Then
                
                  dataNewSrc = srcSheetName & "!" & dataRange.Address(ReferenceStyle:=xlR1C1, external:=False)
                  
                  newColumn = GetLastNumber(dataNewSrc)
                  
                  If originalColumn <> newColumn Then
                      dataNewSrc = Left(dataNewSrc, Len(dataNewSrc) - Len(newColumn))
                      dataNewSrc = dataNewSrc + originalColumn
                  End If
                  
                  .SourceData = dataNewSrc
                  .RefreshTable
                End If
                '---2016/12/19 Add No.385 データが存在するときのみ更新実行 IF文追加 -------------End
            End With
        Next workPVT
    Next currWorksheet

End Sub
'---2016/12/16 Add No.383 アドインにグラフ更新(ピボット更新)追加 ----------------------------End

