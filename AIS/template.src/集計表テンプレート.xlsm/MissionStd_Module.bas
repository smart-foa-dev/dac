Attribute VB_Name = "MissionStd_Module"
'**********************
'* 収集開始日時の取得
'* →返り値はUNIX TIME
'**********************
Public Function GetCollectStartDTstd(currWorksheet As Worksheet) As Double
    Dim srchRange           As Range
    Dim startRange          As Range
    Dim selectStr           As String
    Dim dispTerm            As Double
    Dim dispTermUnit        As String
    Dim prevDate            As Date
    Dim prevTime            As Date
    Dim startTime           As Double
    Dim startD              As Date
    Dim startT              As Date
    
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="オンライン種別", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "" Then
                selectStr = srchRange.Offset(0, 1).Value
            Else
                selectStr = "期間固定"
            End If
        Else
                selectStr = "期間固定"
        End If
    
        Set startRange = .Cells.Find(What:="収集開始日時", LookAt:=xlWhole)
        If Not startRange Is Nothing Then
            startD = DateValue(Format(startRange.Offset(0, 1).Value, "yyyy/m/d"))
            startT = TimeValue(Format(startRange.Offset(0, 2).Value, "hh:mm:ss"))
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            GetCollectStartDTstd = -1
            Exit Function
        End If
        startTime = GetUnixTime(startD + startT)
    End With
    
    '* 表示期間の設定
    Select Case selectStr
        Case "期間固定"
            '* 期間固定の場合、GRIPミッションへの問い合わせ収集開始時刻は
            '* オンラインテンプレートシートの「収集開始日時」が相当する
            dispTerm = 0#
        Case "連続移動"
            '* 連続移動の場合、GRIPミッションへの問い合わせ収集開始時刻は
            '* 現在時刻から表示期間を遡った時刻が「収集開始日時」になる
            '*******************************
            '* 表示期間を取得
            '*******************************
            Set srchRange = currWorksheet.Cells.Find(What:="表示期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                If IsEmpty(srchRange.Offset(0, 1).Value) Then
                    dispTerm = 1#
                    dispTermUnit = "【時】"
                Else
                    dispTerm = srchRange.Offset(0, 1).Value
                    dispTermUnit = srchRange.Offset(0, 2).Value
                End If
            Else
                dispTerm = 1#
                dispTermUnit = "【時】"
            End If
            Select Case dispTermUnit
                Case "【分】"
                    dispTerm = dispTerm * 60 * (-1)
                Case "【時】"
                    dispTerm = dispTerm * 3600 * (-1)
            End Select
            prevDate = Now
            prevTime = DateAdd("s", dispTerm, prevDate)
            startTime = GetUnixTime(prevTime)
            startRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd")
            startRange.Offset(0, 2).Value = Format(prevTime, "hh:mm:ss")
    End Select
    
    GetCollectStartDTstd = startTime

End Function

' 2017.02.22 Add ↓↓↓
'*********************
'* 収集終了日時を取得
'*********************
Public Function GetCollectEndDTstd(currWorksheet As Worksheet) As Double
    Dim srchRange           As Range
    Dim endTime             As Double
    Dim endD                As Date
    Dim endT                As Date
    
    With currWorksheet
        Set srchRange = .Cells.Find(What:="収集終了日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = DateValue(Format(srchRange.Offset(0, 1).Value, "yyyy/m/d"))
            endT = TimeValue(Format(srchRange.Offset(0, 2).Value, "hh:mm:ss"))
        Else
            endD = CDate("2020/12/31")
            endT = CDate("23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Function
        End If
        endTime = GetUnixTime(endD + endT)
    End With
    
    GetCollectEndDTstd = endTime
End Function

