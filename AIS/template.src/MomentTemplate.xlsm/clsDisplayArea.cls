VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsDisplayArea"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'Wang Issue of zoom display area while window size is changed 2018/06/06 Start

Option Explicit

Private DEFAULT_ZOOM As Double
Private DisplayAreaSheet As Worksheet
Private DisplayAreaRange As Range
Private DisplayMagRange As Range
Private DisplayMagValue As Double

Private Sub InitializeDefaultZoom()
    Set DisplayMagRange = GetDisplayMagRange
    
    If Not DisplayMagRange Is Nothing Then
        DEFAULT_ZOOM = DisplayMagRange.Value
    Else
        DEFAULT_ZOOM = 100
    End If
End Sub

Private Function GetDisplaySizeRange() As Range
    Dim paramSheet As Worksheet
    Dim startRange As Range
    
    Set paramSheet = ThisWorkbook.Worksheets(ParamSheetName)
    Set startRange = paramSheet.Cells(1, 1).EntireColumn
    Set GetDisplaySizeRange = startRange.Find(What:="表示サイズ", LookAt:=xlPart)
End Function

Private Function GetDisplayMagRange() As Range
    Dim paramSheet As Worksheet
    Dim startRange As Range
    Dim titleRange As Range
    
    Set paramSheet = ThisWorkbook.Worksheets(ParamSheetName)
    Set startRange = paramSheet.Cells(1, 1).EntireColumn
    Set titleRange = startRange.Find(What:="表示倍率", LookAt:=xlPart)
    If Not titleRange Is Nothing Then
        Set GetDisplayMagRange = titleRange.Offset(0, 1)
    End If
End Function

Private Sub GetDisplayInfo()
    Set DisplayAreaSheet = Nothing
    Set DisplayAreaRange = Nothing

    Dim displaySizeRange As Range
    
    Set displaySizeRange = GetDisplaySizeRange()
    If displaySizeRange Is Nothing Then
        Exit Sub
    End If
    
    Dim rngArray() As String
    rngArray = Split(displaySizeRange.text, "_")
    If UBound(rngArray) < 1 Then
        Exit Sub
    End If

    Set DisplayAreaSheet = ThisWorkbook.Worksheets(rngArray(1))
    Set DisplayAreaRange = DisplayAreaSheet.Range(displaySizeRange.Offset(0, 1).Value)
    Set DisplayMagRange = GetDisplayMagRange
    
    If Not DisplayMagRange Is Nothing Then
        DisplayMagValue = DisplayMagRange.Value
    Else
        DisplayMagValue = DEFAULT_ZOOM
    End If
    
End Sub

Private Sub Class_Initialize()
    Set DisplayAreaSheet = Nothing
    Set DisplayAreaRange = Nothing
    
    Call InitializeDefaultZoom
End Sub

Private Sub SaveCurrentZoom()
    If Not DisplayMagRange Is Nothing Then
        DisplayMagRange.Value = ActiveWindow.Zoom
    End If
End Sub

Public Sub ResizeWindow(ByVal Wn As Window, Optional restore As Boolean = False, Optional useStoredZoom As Boolean = False)
    If ActiveWindow.DisplayHeadings Then
        Exit Sub
    End If

    Call GetDisplayInfo

    If DisplayAreaRange Is Nothing Then
        Exit Sub
    End If

    ' adjust zoom only on target sheet
    If Not ActiveSheet Is DisplayAreaSheet Then
        Exit Sub
    End If
    
    Dim windowFrameWidth As Double
    Dim windowFrameHeight As Double
    Dim zoomScale As Double
    
    windowFrameWidth = Wn.Width - Application.UsableWidth
    windowFrameHeight = Wn.Height - Application.UsableHeight
    
    ' calculate scale
    If Not restore Then
        zoomScale = WorksheetFunction.Min(Application.UsableHeight / DisplayAreaRange.Height, Application.UsableWidth / DisplayAreaRange.Width) * 100
    Else
        If useStoredZoom Then
            zoomScale = DisplayMagValue
        Else
            zoomScale = DEFAULT_ZOOM
        End If
    End If
    
    ' Zoom content
    ActiveWindow.Zoom = WorksheetFunction.Max(WorksheetFunction.Min(zoomScale, 400), 10)
    Call SaveCurrentZoom
    
    ' Reset position
    ActiveCell.Activate
    ActiveWindow.ScrollColumn = DisplayAreaRange.Column
    ActiveWindow.ScrollRow = DisplayAreaRange.Row
    
    ' Window Fit process
    Dim scaleWidth As Double
    Dim scaleHeight As Double
    '* ISSUE_NO.746 Sunyi 2018/06/21 Start
    '開くサイズ調整
    scaleWidth = DisplayAreaRange.Width * (ActiveWindow.Zoom / 100) + windowFrameWidth
'    scaleHeight = DisplayAreaRange.Height * (ActiveWindow.Zoom / 100) + windowFrameHeight
    scaleHeight = DisplayAreaRange.Height * (ActiveWindow.Zoom / 90) + windowFrameHeight
    '* ISSUE_NO.746 Sunyi 2018/06/21 End
    
    'Debug.Print Wn.Width & "x" & Wn.Height & " U- " & calcWnWidth & "x" & calcWnHeight
    If Wn.WindowState = xlMaximized Then
        Wn.WindowState = xlNormal
        Wn.Top = 0
        Wn.Left = 0
    End If
    
    If Not scaleWidth = Wn.Width Then
        Wn.Width = scaleWidth
    End If
    If Not scaleHeight = Wn.Height Then
        Wn.Height = scaleHeight
    End If
End Sub

Public Sub ResizeWindowBySheet(ByVal sh As Object)
    Call GetDisplayInfo
    
    If sh Is DisplayAreaSheet Then
        Call ResizeWindow(ActiveWindow)
    End If
End Sub
'Wang Issue of zoom display area while window size is changed 2018/06/06 End


