Attribute VB_Name = "Main_Module"
Option Explicit

'****************************************************************************************************
'*******************
'* 自動更新スタート
'*******************
Public Sub AutoUpdateStart()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    
    '* メニュー制御
    Call MenuCtrlEnableOrDisable("グラフ更新", False)
    Call MenuCtrlEnableOrDisable("テスト", False)
    Call MenuCtrlEnableOrDisable("自動更新", False)
    Call MenuCtrlEnableOrDisable("更新停止", True)

    Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "ON"
    
    Call TimerStart

End Sub

'****************************
'* 自動更新を停止する
'****************************
Public Sub AutoUpdateStop()
    Dim currWorksheet   As Worksheet
    Dim mainWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim prevDate        As Date
    Dim prevTime        As Date
    
    Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="自動更新", LookAt:=xlWhole)
    
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = "OFF"
    
        '* 予約したスケジュールをキャンセルする
        Call CancelSchedule
    
        '* メニュー制御
        Call MenuCtrlEnableOrDisable("グラフ更新", True)
        Call MenuCtrlEnableOrDisable("テスト", True)
        Call MenuCtrlEnableOrDisable("自動更新", True)
        Call MenuCtrlEnableOrDisable("更新停止", False)
    
        Call UpdateFormStatus
        
    End If
    
End Sub

'*************************************
'* ｢テスト｣の処理
'*************************************
Public Sub OperationTest()
    Dim currWorksheet       As Worksheet
    Dim dataNum             As Integer
    Dim hStepValue          As Variant
    Dim hPrmRange           As Range
    Dim rc                  As Variant
    Dim srchRange           As Range
    
'    rc = MsgBox("テストを開始しますか？", vbYesNo)
'    If rc = vbNo Then
'        Exit Sub
'    End If

    '前回／次回更新日時セット
    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(Now, "yyyy/MM/dd hh:mm:ss")
    End If
    
    '* ミッションからの情報取得
    Call GetMissionInfoAll
    
    '* ピボットテーブルの更新
    Call UpdateGraphDataSecond

    '* グラフ編集画面の設定を元にグラフを更新
    Call UpdateAfterGraphEdit

End Sub

'*************************************
'* ｢テスト｣の処理
'*************************************
Public Sub OperationTestXXX()
    Dim currWorksheet       As Worksheet
    Dim dataNum             As Integer
    Dim hStepValue          As Variant
    Dim hPrmRange           As Range
    Dim rc                  As Variant
    
    '* ミッションからの情報取得
    Call GetMissionInfoAll
    
    '* ピボットテーブルの更新
    Call UpdateGraphDataFirst

    '* グラフ編集シートのＸＹ軸の最大／最小／刻み幅を更新する
    Call PutMaxMinStepEditSheet
    
    '* グラフ編集画面の設定を元にグラフを更新
'    Call UpdateAfterGraphEdit

End Sub

'*************************************
'* 最大最小取得の処理
'* ※AISから最初のエクセル起動にコールされるマクロ。
'*************************************
Public Sub CallBeforeExcelFromAIS()
    Dim currWorksheet       As Worksheet
    Dim dataNum             As Integer
    Dim hStepValue          As Variant
    Dim hPrmRange           As Range
    
    '* グラフテーブルの更新
    '* →paramシートの表示開始／終了期間を用いる
    Call UpdateGraphDataFirst

    '* グラフ編集シートのＸＹ軸の最大／最小／刻み幅を
    '* 自動で設定したグラフの軸の値で更新する
    Call PutMaxMinStepEditSheet3
    
    Call UpdateAfterGraphEdit
    
End Sub

'*************************************
'* グラフ編集シート上の値更新後の処理
'*************************************
Public Sub UpdateLoalData()
    '* グラフテーブルの更新
    '* →グラフ編集シートの表示開始／終了期間を用いる
    Call UpdateGraphDataSecond
    
    '* グラフ編集シートのＸＹ軸の最大／最小／刻み幅を
    '* 自動で設定したグラフの軸の値で更新する
    Call PutMaxMinStepEditSheet4
    
    Call UpdateAfterGraphEdit

    ' 背景ﾃﾞｰﾀ、凡例一覧表の作成
    Call HaikeiHanreiDataMake
    
End Sub

'*******************
'* 状態フォーム更新
'*******************
Public Sub UpdateFormStatus()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim flagON          As Boolean
    Dim f               As Variant
    Dim isNotForm       As Boolean
    Dim FinalTime       As Date
    Dim endD            As Date
    Dim endT            As Date
    
    '* 自動更新状態を「pram」シートから取得
    Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        If srchRange.Offset(0, 1).Value = "ON" Then
            flagON = True
        Else
            flagON = False
        End If
    End If
    
    '* 収集終了日時を「オンラインテンプレート」シートから取得
    Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="取得終了", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        endD = srchRange.Offset(0, 1).Value
        endT = srchRange.Offset(0, 2).Value
    Else
        endD = CDate("2020/12/31")
        endT = CDate("23:59:59")
    End If
    
    If Not IsDate(endD + endT) Then
        MsgBox "収集終了日時欄が正しくないため処理を中断します。"
        Exit Sub
    End If
    FinalTime = endD + endT

    '* ユーザフォームの有無を確認し、情報を表示する
    isNotForm = True
    For Each f In UserForms
        If TypeOf f Is AutoUpdateForm Then
            isNotForm = False
            With AutoUpdateForm
                .LB_Info.Caption = "取得終了日時：" & FinalTime
                With .LB_OnOff
                    If flagON Then
                        .Caption = "更新中"
                        .ForeColor = RGB(0, 0, 255)
                    Else
                        .Caption = "停止中"
                        .ForeColor = RGB(255, 0, 0)
                    End If
                End With
            End With
        End If
    Next f
    
    '* ユーザフォームなかったら再度表示
    If isNotForm Then
        Load AutoUpdateForm
        With AutoUpdateForm
            .LB_Info.Caption = "取得終了日時：" & FinalTime
            With .LB_OnOff
                If flagON Then
                    .Caption = "更新中"
                    .ForeColor = RGB(0, 0, 255)
                Else
                    .Caption = "停止中"
                    .ForeColor = RGB(255, 0, 0)
                End If
            End With
        End With
        AutoUpdateForm.Show vbModeless
    End If
End Sub

'***************************************************
'* グラフ編集シートから最大／最小／刻み幅を取得する
'***************************************************
Public Sub GetMaxMinStepValue(currSheetName As String, vValue As MAXMINSTEPINFO, hValue As MAXMINSTEPINFO)
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range

    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Set currWorksheet = Worksheets(currSheetName)
    With currWorksheet
        Set srchRange = .Cells.Find(What:=VerticalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            vValue.maxValue = srchRange.Offset(1, 1).Value
            vValue.minValue = srchRange.Offset(2, 1).Value
            vValue.stepValue = srchRange.Offset(3, 1).Value
        End If
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            hValue.maxValue = DateValue(srchRange.Offset(1, 1).Value) + TimeValue(srchRange.Offset(1, 1).Value)
            hValue.minValue = DateValue(srchRange.Offset(2, 1).Value) + TimeValue(srchRange.Offset(2, 1).Value)
            hValue.stepValue = srchRange.Offset(3, 1).Value
        End If
    End With
        
End Sub

'***************************************************
'* グラフ編集シートから最大／最小／刻み幅を取得する
'***************************************************
Public Sub GetMaxMinStepValue2(currSheetName As String, hValue As MAXMINSTEPINFO)
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range

    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Set currWorksheet = Worksheets(currSheetName)
    With currWorksheet
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            hValue.maxValue = srchRange.Offset(1, 1).Value
            hValue.minValue = srchRange.Offset(2, 1).Value
            hValue.stepValue = srchRange.Offset(3, 1).Value
        End If
    End With
        
End Sub

'*************************************
'* グラフ編集シート上の値更新後の処理
'*************************************
Public Sub UpdateAfterGraphEdit()
    Dim currWorksheet       As Worksheet
    Dim currChartObj        As ChartObject
    Dim currPivotTable      As PivotTable
    Dim dataNum             As Integer
    Dim hValue              As MAXMINSTEPINFO
    Dim vValue              As MAXMINSTEPINFO
    
    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    '* ※縦軸はないため、横軸のみデータを取得
    Call GetMaxMinStepValue2(GraphEditSheetName, hValue)
    
    Set currWorksheet = Worksheets(GraphEditSheetName)
    Set currChartObj = currWorksheet.ChartObjects(HistGraph02Name)
    
    '* グラフの縦軸の最大値／最小値／ステップを再設定
    Call SetGraphParamY(currChartObj, CDbl(hValue.maxValue), CDbl(hValue.maxValue * (-1)), CDbl(hValue.stepValue))
    
    '* グラフの横軸の最大値／最小値／ステップを再設定
    Call SetGraphParamX(currChartObj, CDbl(hValue.maxValue), CDbl(hValue.maxValue * (-1)), CDbl(hValue.stepValue))
    
    Set currChartObj = Worksheets(FixGraphSheetName).ChartObjects(HistGraph02Name)
    
    '* グラフの縦軸の最大値／最小値／ステップを再設定
    Call SetGraphParamY(currChartObj, CDbl(hValue.maxValue), CDbl(hValue.maxValue * (-1)), CDbl(hValue.stepValue))
    
    '* グラフの横軸の最大値／最小値／ステップを再設定
    Call SetGraphParamX(currChartObj, CDbl(hValue.maxValue), CDbl(hValue.maxValue * (-1)), CDbl(hValue.stepValue))
        
    '* 基準線を再配置する
    Call GraphReCalculate

End Sub


'***************************
'* 演算結果シートに転記する
'***************************
Public Sub PostToCalcResultSheet()
    Dim startTimeElmOrg     As String
    Dim endTimeElmOrg       As String
    Dim rsltTimeElmOrg      As String
    
    Dim startTimeElm        As String
    Dim endTimeElm          As String
    Dim rsltTimeElm         As String
    
    Dim rapTimeElm          As String
    Dim typeElm             As String
    Dim ctmList()           As CTMINFO
    Dim srcWorksheet        As Worksheet
    Dim currWorksheet       As Worksheet
    Dim II                  As Integer
    Dim colIndex            As Integer
    Dim copyRange           As Range
    Dim destRange           As Range
    Dim srchRange           As Range
    Dim workRange           As Range
    Dim lastRange           As Range
    Dim startRange          As Range
    Dim endRange            As Range
    Dim writeFlagRange      As Range
    Dim distRange           As Range
    
    Dim rowIndex            As Integer
    Dim workLong            As Long
    Dim iFirstFlag          As Boolean
    Dim startCTM            As String
    Dim endCTM              As String
    
    Dim rsltCTM             As String
    Dim wrkStr              As Variant
    Dim gripKaishi          As Variant
    Dim gripSyuryo          As Variant
    
    Dim psramWorksheet      As Worksheet
    Dim motoWorksheet       As Worksheet
    Dim sakiWorksheet       As Worksheet
    
    '* paramシートから必要な情報を取得する
    With Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="エレメントモーメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then startTimeElmOrg = srchRange.Offset(0, 1).Value
        
        Set srchRange = .Cells.Find(What:="エレメント角度", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then endTimeElmOrg = srchRange.Offset(0, 1).Value
        
        Set srchRange = .Cells.Find(What:="エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then typeElm = srchRange.Offset(0, 1).Value
        If typeElm = "" Then typeElm = "CTM NAME"
    End With
    
    '* ピボットテーブル生成用のシートへ取得CTMデータを必要なものだけ転記する
    '* currWorksheet:演算結果データ
    Set currWorksheet = Worksheets(ForPivotSheetName)
    
    '* 以前のデータを削除する
    With currWorksheet
        Set srchRange = currWorksheet.Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
        workRange.Delete
    End With
            
    '************************************************
    '開始時刻設定
    gripKaishi = Split(startTimeElmOrg, "・")       ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
    
    Set psramWorksheet = Worksheets(ParamSheetName)     'paramシート
    Set motoWorksheet = Worksheets(gripKaishi(0))               'GRIPミッションシート
    Set sakiWorksheet = Worksheets(ForPivotSheetName)       '演算結果データ
    
    '開始CTM位置
    Set srchRange = motoWorksheet.Cells.Find(What:=gripKaishi(1), LookAt:=xlWhole)
    If srchRange Is Nothing Then
        MsgBox startTimeElm & "：開始CTMもしくはｴﾚﾒﾝﾄのﾃﾞｰﾀがありません!!", vbExclamation
        'Application.DisplayAlerts = False     '---確認メッセージ非表示
        'Application.Quit                      '---Excelを終了します
        End
        Exit Sub
    Else
        '開始CTM名称(1)
        Set copyRange = motoWorksheet.Range("A:A")
        Set distRange = sakiWorksheet.Range("A:A")
        copyRange.Copy Destination:=distRange
        
        'CTM受信時間
        Set copyRange = motoWorksheet.Range("B:B")
        Set distRange = sakiWorksheet.Range("B:B")
        copyRange.Copy Destination:=distRange

        '開始時刻コピー
        wrkStr = srchRange.Row
        wrkStr = srchRange.Rows
        wrkStr = Split(srchRange.Address, "$")
        
        Set copyRange = motoWorksheet.Range(wrkStr(1) & ":" & wrkStr(1))
        Set distRange = sakiWorksheet.Range("C:C")
        copyRange.Copy Destination:=distRange
    End If
    
    '終了時刻設定
    gripSyuryo = Split(endTimeElmOrg, "・")                ' 0:CTM名,1:ｴﾚﾒﾝﾄ名
    Set motoWorksheet = Worksheets(gripSyuryo(0))       ' データシート
    
    '終了CTM位置
    Set srchRange = motoWorksheet.Cells.Find(What:=gripSyuryo(1), LookAt:=xlWhole)
    If srchRange Is Nothing Then
        MsgBox endTimeElm & "：終了CTMもしくはｴﾚﾒﾝﾄのﾃﾞｰﾀがありません!!", vbExclamation
        'Application.DisplayAlerts = False     '---確認メッセージ非表示
        'Application.Quit                      '---Excelを終了します
        End
        Exit Sub
    Else

        '終了時刻コピー
        wrkStr = Split(srchRange.Address, "$")
        
        Set copyRange = motoWorksheet.Range(wrkStr(1) & ":" & wrkStr(1))
        Set distRange = sakiWorksheet.Range("D:D")
        copyRange.Copy Destination:=distRange
    End If
'***************************************
    
    '* 2〜3行を削除（型／単位）
    Set workRange = currWorksheet.Range("2:3")
    workRange.Delete
    
End Sub

'***************************
'* 演算結果シートに転記する
'***************************
Public Sub PostToCalcResultSheet_NG()
    Dim startTimeElmOrg     As String
    Dim endTimeElmOrg       As String
    Dim rsltTimeElmOrg      As String
    
    Dim startTimeElm        As String
    Dim endTimeElm          As String
    Dim rsltTimeElm         As String
    
    Dim rapTimeElm          As String
    Dim typeElm             As String
    Dim ctmList()           As CTMINFO
    Dim srcWorksheet        As Worksheet
    Dim currWorksheet       As Worksheet
    Dim II                  As Integer
    Dim colIndex            As Integer
    Dim copyRange           As Range
    Dim destRange           As Range
    Dim srchRange           As Range
    Dim workRange           As Range
    Dim lastRange           As Range
    Dim startRange          As Range
    Dim endRange            As Range
    Dim writeFlagRange      As Range
    
    Dim rowIndex            As Integer
    Dim workLong            As Long
    Dim iFirstFlag          As Boolean
    Dim startCTM            As String
    Dim endCTM              As String
    
    Dim rsltCTM             As String
    Dim wrkStr              As String

    '* paramシートから必要な情報を取得する
    With Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="エレメントモーメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then startTimeElmOrg = srchRange.Offset(0, 1).Value
        
        Set srchRange = .Cells.Find(What:="エレメント角度", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then endTimeElmOrg = srchRange.Offset(0, 1).Value
        
        Set srchRange = .Cells.Find(What:="エレメント", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then typeElm = srchRange.Offset(0, 1).Value
        If typeElm = "" Then typeElm = "CTM NAME"
    End With
    
    '* ピボットテーブル生成用のシートへ取得CTMデータを必要なものだけ転記する
    '* currWorksheet:演算結果データ
    Set currWorksheet = Worksheets(ForPivotSheetName)
    
    '* 以前のデータを削除する
    With currWorksheet
        Set srchRange = currWorksheet.Range("A1")
        Set lastRange = srchRange.End(xlToRight)
        Set workRange = currWorksheet.Range(srchRange.Cells, lastRange.End(xlDown).Cells)
        workRange.Delete
    End With
            
    '* CTM情報一覧をシートから取得
    Call GetCTMList(ParamSheetName, ctmList)
    
    '* 全てのワークシートに対して
    iFirstFlag = True
    colIndex = 3
    For Each srcWorksheet In Worksheets
    
        '* CTM情報一覧に存在するシートのみを処理対象とする
        For II = 0 To UBound(ctmList)
            If (ctmList(II).Name = srcWorksheet.Name) Then Exit For
        Next
        If II <= UBound(ctmList) Then
        
            
            '* RECEIVETIME／CTM名列を複写する
            If iFirstFlag Then
                Set copyRange = srcWorksheet.Range("A:B")
                Set destRange = currWorksheet.Range("A:B")
                copyRange.Copy Destination:=destRange
                iFirstFlag = False
            End If
            
            '* 対象種別列を複写する
'            Set srchRange = srcWorksheet.Range("1:1")       ' １行目を検索範囲にセット
'            Set workRange = srchRange.Find(What:=typeElm, LookAt:=xlWhole)
'            If Not workRange Is Nothing Then
'                Set copyRange = workRange.EntireColumn
'                Set destRange = currWorksheet.Columns(colIndex)
'                copyRange.Copy Destination:=destRange
'                colIndex = colIndex + 1
'            End If
            
            '* エレメントモーメントを複写する
            If startTimeElmOrg <> "" Then
                startCTM = Left(startTimeElmOrg, InStr(startTimeElmOrg, "・") - 1)
                If srcWorksheet.Name = startCTM Then
                    startTimeElm = Right(startTimeElmOrg, Len(startTimeElmOrg) - InStr(startTimeElmOrg, "・"))
                    Set srchRange = srcWorksheet.Range("1:1")
                    Set workRange = srchRange.Find(What:=startTimeElm, LookAt:=xlWhole)
                    If Not workRange Is Nothing Then
                        Set copyRange = workRange.EntireColumn
                        Set destRange = currWorksheet.Columns(colIndex)
                        copyRange.Copy Destination:=destRange
                        colIndex = colIndex + 1
                    End If
                End If
            End If
            
            '* エレメント角度を複写する
            If endTimeElmOrg <> "" Then
                endCTM = Left(endTimeElmOrg, InStr(endTimeElmOrg, "・") - 1)
                If srcWorksheet.Name = endCTM Then
                    endTimeElm = Right(endTimeElmOrg, Len(endTimeElmOrg) - InStr(endTimeElmOrg, "・"))
                    Set srchRange = srcWorksheet.Range("1:1")
                    Set workRange = srchRange.Find(What:=endTimeElm, LookAt:=xlWhole)
                    If Not workRange Is Nothing Then
                        Set copyRange = workRange.EntireColumn
                        Set destRange = currWorksheet.Columns(colIndex)
                        copyRange.Copy Destination:=destRange
                        colIndex = colIndex + 1
                    End If
                End If
            End If
        End If
    Next srcWorksheet

    '* 開始時間のNULL行削除　2016/05/13
'    Set currWorksheet = Worksheets(ForPivotSheetName)
'    With currWorksheet
'        Set srchRange = .Range(.Range("A4"), .Range("A4").End(xlDown)).Offset(0, 3)
'        Call DeleteMultiLine("", srchRange)
'    End With
    
    '* 2〜3行を削除（型／単位）
    Set workRange = currWorksheet.Range("2:3")
    workRange.Delete
    
End Sub

'******************************************
'* 指定範囲内にある対象文字列行を削除する
'******************************************
Public Sub DeleteMultiLine(delStr As String, srchRange As Range)
    Dim currWorksheet   As Worksheet
    Dim workRange       As Range
    Dim strAddress      As String
    Dim delRange        As Range

    Set currWorksheet = Worksheets(ForPivotSheetName)
    With currWorksheet
        '* 削除対象セル範囲を取得
        Set workRange = srchRange.Find(What:=delStr, LookAt:=xlWhole)
        If Not workRange Is Nothing Then
            Set delRange = workRange
            strAddress = workRange.Address
            Do While Not workRange Is Nothing

                Set workRange = srchRange.FindNext(workRange)
                If strAddress = workRange.Address Then
                    Exit Do
                End If
                
                '* 対象セルを集める
                Set delRange = Union(delRange, workRange)
            
            Loop
            delRange.EntireRow.Delete
        End If
    End With
    
End Sub

'*************************************
'* グラフのデータソースを変更
'*************************************
Public Sub ChangeChartData(writeRange As Range)
    Dim graphEditWorksheet      As Worksheet
    Dim fixGraphWorksheet       As Worksheet
    Dim graphDataWorksheet      As Worksheet
    Dim currWorksheet           As Worksheet
    Dim currChartObj            As ChartObject
    Dim lastRange               As Range
    Dim dataXRange              As Range
    Dim dataYRange              As Range
    Dim currSeries              As Series
    Dim updateNG                As Boolean
    
    '* グラフ編集シート
    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    '* グラフシート
    Set fixGraphWorksheet = Worksheets(FixGraphSheetName)
    
    Set graphDataWorksheet = Worksheets(ForPivotSheetName)
    With graphDataWorksheet
        Set dataXRange = .Range(writeRange.Cells, writeRange.End(xlDown).Cells)
        Set dataYRange = .Range(writeRange.Offset(0, 1).Cells, writeRange.Offset(0, 1).End(xlDown).Cells)
    End With
    
'    Application.ScreenUpdating = False
    
    Set currWorksheet = ActiveSheet
    
    '* グラフ編集シートのグラフを更新
    graphEditWorksheet.Activate
    Set currChartObj = graphEditWorksheet.ChartObjects(HistGraph02Name)
    With currChartObj.Chart
        '* グラフの系統が一つもなければ新規に作成する
        If .SeriesCollection.Count = 0 Then
            Call AddSeriesBaseLine(currChartObj, "PALL", "=" & ForPivotSheetName & "!" & dataXRange.Address, "=" & ForPivotSheetName & "!" & dataYRange.Address, RGB(74, 126, 187))
            Set currSeries = .SeriesCollection("PALL")
            With currSeries
                .MarkerStyle = 8
                .MarkerSize = 5
                With .Format.Line
                    .Visible = msoFalse
                End With
                With .Format.Fill
                    .Visible = msoTrue
                    .ForeColor.ObjectThemeColor = msoThemeColorAccent1
                    .ForeColor.TintAndShade = 0
                    .Solid
                End With
            End With
        Else
            updateNG = True
            For Each currSeries In .SeriesCollection
                If currSeries.Name = "PALL" Then
                    currSeries.XValues = "=" & ForPivotSheetName & "!" & dataXRange.Address
                    currSeries.Values = "=" & ForPivotSheetName & "!" & dataYRange.Address
                    updateNG = False
                End If
            Next currSeries
            If updateNG Then
                Call AddSeriesBaseLine(currChartObj, "PALL", "=" & ForPivotSheetName & "!" & dataXRange.Address, "=" & ForPivotSheetName & "!" & dataYRange.Address, RGB(74, 126, 187))
                Set currSeries = .SeriesCollection("PALL")
                With currSeries
                    .MarkerStyle = 8
                    .MarkerSize = 5
                    With .Format.Line
                        .Visible = msoFalse
                    End With
                    With .Format.Fill
                        .Visible = msoTrue
                        .ForeColor.ObjectThemeColor = msoThemeColorAccent1
                        .ForeColor.TintAndShade = 0
                        .Solid
                    End With
                End With
            End If
        End If
    End With
    
    '* グラフシートのグラフを更新
    fixGraphWorksheet.Activate
    Set currChartObj = fixGraphWorksheet.ChartObjects(HistGraph02Name)
    With currChartObj.Chart
        '* グラフの系統が一つもなければ新規に作成する
        If .SeriesCollection.Count = 0 Then
            Call AddSeriesBaseLine(currChartObj, "PALL", "=" & ForPivotSheetName & "!" & dataXRange.Address, "=" & ForPivotSheetName & "!" & dataYRange.Address, RGB(74, 126, 187))
            Set currSeries = .SeriesCollection("PALL")
            With currSeries
                .MarkerStyle = 8
                .MarkerSize = 5
                With .Format.Line
                    .Visible = msoFalse
                End With
                With .Format.Fill
                    .Visible = msoTrue
                    .ForeColor.ObjectThemeColor = msoThemeColorAccent1
                    .ForeColor.TintAndShade = 0
                    .Solid
                End With
            End With
        Else
            updateNG = True
            For Each currSeries In .SeriesCollection
                If currSeries.Name = "PALL" Then
                    currSeries.XValues = "=" & ForPivotSheetName & "!" & dataXRange.Address
                    currSeries.Values = "=" & ForPivotSheetName & "!" & dataYRange.Address
                    updateNG = False
                End If
            Next currSeries
            If updateNG Then
                Call AddSeriesBaseLine(currChartObj, "PALL", "=" & ForPivotSheetName & "!" & dataXRange.Address, "=" & ForPivotSheetName & "!" & dataYRange.Address, RGB(74, 126, 187))
                Set currSeries = .SeriesCollection("PALL")
                With currSeries
                    .MarkerStyle = 8
                    .MarkerSize = 5
                    With .Format.Line
                        .Visible = msoFalse
                    End With
                    With .Format.Fill
                        .Visible = msoTrue
                        .ForeColor.ObjectThemeColor = msoThemeColorAccent1
                        .ForeColor.TintAndShade = 0
                        .Solid
                    End With
                End With
            End If
        End If
    End With
    
    currWorksheet.Activate
    
'    Application.ScreenUpdating = True
    
End Sub

'*****************************************************
'* グラフの最大／最小／刻み幅をグラフ編集シートへ転記
'*****************************************************
Public Sub PutMaxMinStepEditSheet()
    Dim graphEditWorksheet      As Worksheet
    Dim currChartObj            As ChartObject
    Dim maxX                    As Variant
    Dim minX                    As Variant
    Dim stepX                   As Variant
    Dim maxY                    As Variant
    Dim minY                    As Variant
    Dim stepY                   As Variant
    Dim srchRange               As Variant

    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    
    With graphEditWorksheet
        
        Set currChartObj = .ChartObjects(HistGraph02Name)
    
        With currChartObj.Chart
            .Axes(xlValue).MajorUnitIsAuto = True ' 自動設定
            .Axes(xlValue).MinimumScaleIsAuto = True ' 自動設定
            .Axes(xlValue).MaximumScaleIsAuto = True ' 自動設定
            minY = .Axes(xlValue).MinimumScale
            maxY = .Axes(xlValue).MaximumScale
            stepY = .Axes(xlValue).MajorUnit
            .Axes(xlCategory).MajorUnitIsAuto = True ' 自動設定
            .Axes(xlCategory).MinimumScaleIsAuto = True ' 自動設定
            .Axes(xlCategory).MaximumScaleIsAuto = True ' 自動設定
            minX = .Axes(xlCategory).MinimumScale
            maxX = .Axes(xlCategory).MaximumScale
            stepX = .Axes(xlCategory).MajorUnit
        End With
        
        '* グラフ編集シートへ転記
        Set srchRange = .Cells.Find(What:=VerticalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(1, 1).Value = "" Then srchRange.Offset(1, 1).Value = maxY
            If srchRange.Offset(2, 1).Value = "" Then srchRange.Offset(2, 1).Value = minY
            If srchRange.Offset(3, 1).Value = "" Then srchRange.Offset(3, 1).Value = stepY
        End If
        
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(1, 1).Value = "" Then srchRange.Offset(1, 1).Value = Format(maxX, "yyyy/MM/dd hh:mm:ss")
            If srchRange.Offset(2, 1).Value = "" Then srchRange.Offset(2, 1).Value = Format(minX, "yyyy/MM/dd hh:mm:ss")
            If srchRange.Offset(3, 1).Value = "" Then srchRange.Offset(3, 1).Value = Format(Hour(stepX) & ":" & Minute(stepX) & ":" & Second(stepX), "hh:mm:ss")
        End If
    End With
End Sub

'*****************************************************
'* 初回のグラフの最大／最小／刻み幅をグラフ編集シートへ転記
'* ※Ｙ軸のみ更新。Ｘ軸は刻みを15分デフォルトで設定
'*****************************************************
Public Sub PutMaxMinStepEditSheet2()
    Dim graphEditWorksheet      As Worksheet
    Dim currChartObj            As ChartObject
    Dim maxX                    As Variant
    Dim minX                    As Variant
    Dim stepX                   As Variant
    Dim maxY                    As Variant
    Dim minY                    As Variant
    Dim stepY                   As Variant
    Dim srchRange               As Variant

    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    
    With graphEditWorksheet
        
        Set currChartObj = .ChartObjects(HistGraph02Name)
    
        With currChartObj.Chart
            .Axes(xlValue).MajorUnitIsAuto = True ' 自動設定
            .Axes(xlValue).MinimumScaleIsAuto = True ' 自動設定
            .Axes(xlValue).MaximumScaleIsAuto = True ' 自動設定
            minY = .Axes(xlValue).MinimumScale
            maxY = .Axes(xlValue).MaximumScale
            stepY = .Axes(xlValue).MajorUnit
            If stepY < 0.5 Then stepY = 0.5
'            .Axes(xlCategory).MajorUnitIsAuto = True ' 自動設定
'            .Axes(xlCategory).MinimumScaleIsAuto = True ' 自動設定
'            .Axes(xlCategory).MaximumScaleIsAuto = True ' 自動設定
'            minX = .Axes(xlCategory).MinimumScale
'            maxX = .Axes(xlCategory).MaximumScale
'            stepX = .Axes(xlCategory).MajorUnit
            stepX = "00:30:00"
        End With
        
        '* グラフ編集シートへ転記
        Set srchRange = .Cells.Find(What:=VerticalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(1, 1).Value = "" Then srchRange.Offset(1, 1).Value = maxY
            If srchRange.Offset(2, 1).Value = "" Then srchRange.Offset(2, 1).Value = minY
            If srchRange.Offset(3, 1).Value = "" Then srchRange.Offset(3, 1).Value = stepY
        End If
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
'            If srchRange.Offset(1, 1).Value = "" Then srchRange.Offset(1, 1).Value = Format(maxX, "yyyy/MM/dd hh:mm:ss")
'            If srchRange.Offset(2, 1).Value = "" Then srchRange.Offset(2, 1).Value = Format(minX, "yyyy/MM/dd hh:mm:ss")
            If srchRange.Offset(3, 1).Value = "" Then srchRange.Offset(3, 1).Value = Format(Hour(stepX) & ":" & Minute(stepX) & ":" & Second(stepX), "hh:mm:ss")
        End If
    End With
End Sub

'*****************************************************
'* 初回のグラフの最大／最小／刻み幅をグラフ編集シートへ転記
'*****************************************************
Public Sub PutMaxMinStepEditSheet3()
    Dim graphEditWorksheet      As Worksheet
    Dim resultEditWorksheet     As Worksheet
    Dim momentWorksheet         As Worksheet
    Dim currChartObj            As ChartObject
    Dim maxValue                As Variant
    Dim minValue                As Variant
    Dim stepValue               As Variant
    Dim srchRange               As Range
    Dim writeRange              As Range
    Dim workRange               As Range
    Dim II                      As Integer

    '* 基準丸め値**************************************
    Const divValue              As Integer = 10
    Const celValue              As Integer = 1
    '* 基準丸め値**************************************

    Set resultEditWorksheet = Worksheets(ForPivotSheetName)
    With resultEditWorksheet

        '* データ開始位置の取得
        Set writeRange = .Range("C2")
        
        '* データの最大値を取得する
        Set workRange = writeRange.End(xlToRight)
        Set workRange = workRange.End(xlDown)
        Set srchRange = .Range(writeRange, workRange)
        
        maxValue = Application.WorksheetFunction.Max(srchRange)
        minValue = Application.WorksheetFunction.Min(srchRange)
        maxValue = Application.WorksheetFunction.Max(maxValue, Abs(minValue))
        minValue = 0#
        stepValue = maxValue / divValue
        stepValue = Application.WorksheetFunction.Ceiling(stepValue, celValue)
        maxValue = Application.WorksheetFunction.Ceiling(maxValue, stepValue)
        
    End With
        
    '* 補助線のデータを更新する
    Set momentWorksheet = Worksheets(MomentSheetName)
    With momentWorksheet
        Set srchRange = .Range("C1")
        For II = 0 To 9
            srchRange.Offset(0, II * 2).Value = (II + 1) * stepValue
        Next
    End With
    
    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    With graphEditWorksheet
        
        '* グラフ編集シートへ転記
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(1, 1).Value = "" Then srchRange.Offset(1, 1).Value = maxValue
            If srchRange.Offset(2, 1).Value = "" Then srchRange.Offset(2, 1).Value = minValue
            If srchRange.Offset(3, 1).Value = "" Then srchRange.Offset(3, 1).Value = stepValue
        End If

    End With
End Sub

'*****************************************************
'* 2回目以降のグラフの最大／最小／刻み幅をグラフ編集シートへ転記
'*****************************************************
Public Sub PutMaxMinStepEditSheet4()
    Dim graphEditWorksheet      As Worksheet
    Dim resultEditWorksheet     As Worksheet
    Dim momentWorksheet         As Worksheet
    Dim currChartObj            As ChartObject
    Dim maxValue                As Variant
    Dim minValue                As Variant
    Dim stepValue               As Variant
    Dim srchRange               As Range
    Dim writeRange              As Range
    Dim workRange               As Range
    Dim II                      As Integer

    '* 基準丸め値**************************************
    Const divValue              As Integer = 10
    Const celValue              As Integer = 1
    '* 基準丸め値**************************************

    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    With graphEditWorksheet
        
        '* グラフ編集シートへ転記
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            maxValue = srchRange.Offset(1, 1).Value
            minValue = srchRange.Offset(2, 1).Value
            stepValue = srchRange.Offset(3, 1).Value
        End If

    End With
    
    '* 補助線のデータを更新する
    Set momentWorksheet = Worksheets(MomentSheetName)
    With momentWorksheet
        Set srchRange = .Range("C1")
        For II = 0 To 9
            srchRange.Offset(0, II * 2).Value = (II + 1) * stepValue
        Next
    End With
    
End Sub

'*****************************************************
'* グラフのX軸の最大／最小をグラフ編集シートへ転記
'*****************************************************
Public Sub PutMaxMinStepEditSheetNoStepNoY(dtStart As String, dtEnd As String)
    Dim graphEditWorksheet      As Worksheet
    Dim currChartObj            As ChartObject
    Dim maxX                    As Variant
    Dim minX                    As Variant
    Dim srchRange               As Variant

    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    
    With graphEditWorksheet
        
        '* グラフ編集シートへ転記
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 1).Value = dtEnd
            srchRange.Offset(2, 1).Value = dtStart
        End If
    End With
End Sub

'*****************************************************
'* グラフのX軸の最大／最小をグラフ編集シートへ転記
'*****************************************************
Public Sub PutMaxMinStepEditSheetNoY(dtStart As String, dtEnd As String, dtStep As String)
    Dim graphEditWorksheet      As Worksheet
    Dim currChartObj            As ChartObject
    Dim maxX                    As Variant
    Dim minX                    As Variant
    Dim srchRange               As Variant

    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    
    With graphEditWorksheet
        
        '* グラフ編集シートへ転記
        Set srchRange = .Cells.Find(What:=HorizontalAxisName, LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            srchRange.Offset(1, 1).Value = dtEnd
            srchRange.Offset(2, 1).Value = dtStart
            srchRange.Offset(3, 1).Value = dtStep
        End If
    End With
End Sub

'****************************************
'* 演算結果シートUPDATE from paramシート
'****************************************
Public Sub UpdateGraphDataFirst()
    Dim workRange           As Range
    Dim writeRange          As Range
    Dim srchRange           As Range
    Dim dtStart             As String
    Dim dtEnd               As String
    Dim dtStepSize          As Integer
    Dim timeSerial          As Long
    Dim dtInterval          As Double
    Dim workDate            As Date
    Dim currWorksheet       As Worksheet
    
    '* resultシートに転記
    Call PostToCalcResultSheet

    Set currWorksheet = Worksheets(ForPivotSheetName)
    With currWorksheet

        '* データ開始位置の取得
        Set writeRange = .Range("C2")
        
    End With
        
    '* グラフのデータソースを更新する
    Call ChangeChartData(writeRange)
    
End Sub

'*********************************************
'* 演算結果シートUPDATE from グラフ編集シート
'*********************************************
Public Sub UpdateGraphDataSecond()
    Dim workRange           As Range
    Dim writeRange          As Range
    Dim srchRange           As Range
    Dim dtStart             As String
    Dim dtEnd               As String
    Dim dtStepSize          As Integer
    Dim timeSerial          As Long
    Dim dtInterval          As Double
    Dim workDate            As Date
    Dim currWorksheet       As Worksheet
    
    '* resultシートに転記
    Call PostToCalcResultSheet

    Set currWorksheet = Worksheets(ForPivotSheetName)
    With currWorksheet

        '* データ開始位置の取得
        Set writeRange = .Range("C2")
        
    End With
        
    '* グラフのデータソースを更新する
    Call ChangeChartData(writeRange)
    
End Sub

'*************************************************
'* 指定グラフの縦軸の最大／最大／刻み幅を設定する
'*************************************************
Public Sub SetGraphParamY(currChartObj As ChartObject, maxY As Double, minY As Double, stepY As Double)
        
    With currChartObj.Chart
        
        '* 縦軸の最大／最小の設定
        .Axes(xlValue).MinimumScale = minY
        If maxY <> 0 Then .Axes(xlValue).MaximumScale = maxY
        If stepY <> 0 Then .Axes(xlValue).MajorUnit = stepY
    
    End With
    
End Sub

'*************************************************
'* 指定グラフの横軸の最大／最大／刻み幅を設定する
'*************************************************
Public Sub SetGraphParamX(currChartObj As ChartObject, maxX As Double, minX As Double, stepX As Double)
        
    With currChartObj.Chart
        
        '* 横軸の最大／最小の設定
        .Axes(xlCategory).MinimumScale = minX
        .Axes(xlCategory).MaximumScale = maxX
        If stepX <> 0 Then .Axes(xlCategory).MajorUnit = stepX
        
    End With
    
End Sub

'*************************************
'* 背景データをグラフシートへ記載する
'*************************************
Public Sub WriteBackgroundData(graphWorksheet As Worksheet, strData() As String, outFlag As Boolean)
    Dim writeRange          As Range
    Dim srchRange           As Range
    Dim lastRange           As Range
    Dim workRange           As Range
    Dim dataRange           As Range
    Dim workStr             As Variant
    Dim writeIndex          As Integer
    Dim dataValue           As String
    Dim dataUnit            As String
    Dim currWorksheet       As Worksheet
    
    
    '* 取得CTMデータシート
    Set currWorksheet = Worksheets(1)

    With graphWorksheet
    
        Set srchRange = .Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
        
            '* 古いデータを削除
            Set writeRange = srchRange.Offset(2, 0)
            Set lastRange = writeRange.End(xlDown)
            Set workRange = .Range(writeRange, lastRange.Offset(0, 2))
            workRange.Clear
            workRange.Interior.Color = RGB(255, 255, 255)
            
            '* 背景データ出力フラグがONならば出力する
            If outFlag Then
                writeIndex = 0
                '* 着目背景データ文字列を順番に出力する
                For Each workStr In strData
                    '* CTM取得一覧から背景データを検索する
                    If InStr(workStr, "・") Then
                        workStr = Right(workStr, Len(workStr) - InStr(workStr, "・"))
                    End If
                    Set dataRange = currWorksheet.Cells.Find(What:=workStr, LookAt:=xlWhole)
                    If Not dataRange Is Nothing Then
                        dataValue = dataRange.Offset(4, 0).Value
                        dataUnit = dataRange.Offset(2, 0).Value
                    End If
                    
                    writeRange.Offset(writeIndex, 0).Value = workStr
                    writeRange.Offset(writeIndex, 1).Value = dataValue
                    writeRange.Offset(writeIndex, 2).Value = dataUnit
                    Set workRange = .Range(writeRange.Offset(writeIndex, 0), writeRange.Offset(writeIndex, 2))
                    With workRange
                        .Borders.LineStyle = xlContinuous
                        .Borders.Weight = xlThin
                        .Borders(xlEdgeBottom).LineStyle = xlContinuous
                        .Borders(xlEdgeBottom).Weight = xlThin
                        .Borders(xlEdgeLeft).LineStyle = xlContinuous
                        .Borders(xlEdgeLeft).Weight = xlMedium
                        .Borders(xlEdgeRight).LineStyle = xlContinuous
                        .Borders(xlEdgeRight).Weight = xlMedium
                    End With
                    writeIndex = writeIndex + 1
                Next workStr
                Set workRange = .Range(writeRange.Offset(writeIndex, 0), writeRange.Offset(writeIndex, 2))
                workRange.Borders(xlEdgeTop).LineStyle = xlContinuous
                workRange.Borders(xlEdgeTop).Weight = xlMedium
            End If
            
            workRange.EntireColumn.AutoFit
'            writeRange.Offset(0, 0).AutoFit
'            writeRange.Offset(0, 1).AutoFit
'            writeRange.Offset(0, 2).AutoFit
        End If
    
    End With
End Sub

'*******************************
'* ファイルの登録
'* →指定フォルダへ保存するだけ
'*******************************
Public Sub SaveFileSub()
    Dim fname           As String
    Dim iReturn         As Variant
    Dim srchRange       As Range
    Dim saveFilePath    As String
    Dim checkStr        As String
    Dim currThisFile    As String
    Dim templateName    As String
    Dim objFSO          As Object
    Dim wrkInt          As Integer
    Dim msgStr          As String
    Dim prevDate        As Date
    Dim prevTime        As Date
    Dim currWorksheet   As Worksheet
    Dim IsFirstSave     As String
        
    '表示倍率保存
    Call SaveBairitu
    
    Call CancelSchedule

    Set objFSO = CreateObject("Scripting.FileSystemObject")

    '* テンプレート名／登録フォルダ名を取得する
    With ThisWorkbook.Worksheets(ParamSheetName).Columns(1)
        Set srchRange = .Cells.Find(What:="登録フォルダ", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then saveFilePath = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="テンプレート名称", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then templateName = srchRange.Offset(0, 1).Value
        Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then IsFirstSave = srchRange.Offset(0, 1).Value
    End With
    
    '* 自ファイルを一旦上書き保存する
'    currThisFile = ThisWorkbook.FullName
'    Application.DisplayAlerts = False
'    ActiveWorkbook.SaveAs Filename:=currThisFile
'    Application.DisplayAlerts = True

'    wrkInt = InStr(ThisWorkbook.Name, templateName)
'
'    If wrkInt <= 0 Or wrkInt > 4 Then
    If IsFirstSave = "" Then
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & templateName & "_" & ThisWorkbook.Name
        Else
            fname = saveFilePath & "\" & templateName & "_" & ThisWorkbook.Name
        End If
        
        '* テンプレート名／登録フォルダ名を取得する
        With ThisWorkbook.Worksheets(ParamSheetName).Columns(1)
            Set srchRange = .Cells.Find(What:="IsFirstSave", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then srchRange.Offset(0, 1).Value = "TRUE"
        End With
    Else
        '* フォルダ名の最終文字のチェック
        checkStr = Right(saveFilePath, 1)
        If checkStr = "\\" Then
            fname = saveFilePath & ThisWorkbook.Name
        Else
            fname = saveFilePath & "\" & ThisWorkbook.Name
        End If
    End If
    
    '* 登録フォルダにファイルを上書き複写する
'        objFSO.CopyFile currThisFile, fname
    If Dir(fname) <> "" Then
      msgStr = "同じ名前のブックが登録フォルダに存在します。上書きしますか？"
      If MsgBox(msgStr, vbYesNo) = vbNo Then Exit Sub
    End If
    
    '各種バー表示
    Call DispBar
    
    '* 開いているエクセルブックが１つだけならエクセルも登録時に終了させる
    Application.DisplayAlerts = False
    
    '* ISSUE_NO.624 Add ↓↓↓ *******************************
    On Error GoTo ErrorHandler
    
    ThisWorkbook.SaveAs Filename:=fname
    
ErrorHandler:
    '-- 例外処理
    If Err.Description <> "" Then
        MsgBox Err.Description, vbCritical & vbOKOnly, "警告"
    End If
    '* ISSUE_NO.624 Add ↑↑↑ *******************************
    
    If Application.Workbooks.Count > 1 Then
        ThisWorkbook.Close
    Else
        Application.Quit
        ThisWorkbook.Close
    End If
    If Err.Description = "" Then
        Application.DisplayAlerts = True
    End If
        
End Sub

'****************************************************************************************************************
'****************************************************************************************************************
'****************************************************************************************************************
'****************************************************************************************************************

'***************
'タイマー起動開始
'***************
Public Sub TimerStart()
    Dim wrkStr          As String
    Dim wrkTime         As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim endTime         As Double
    Dim prevDate        As Date
    Dim prevTime        As Date
    
    With Worksheets(ParamSheetName)
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value * 60         ' 秒計算
        Else
            updTime = 1
        End If
            
        '*********************
        '* 取得終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = DateValue(srchRange.Offset(0, 1).Value)
            endT = TimeValue(srchRange.Offset(0, 1).Value)
        Else
            endD = DateValue("2020/12/31 23:59:59")
            endT = TimeValue("2020/12/31 23:59:59")
        End If
        
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'TimerLogic'"
    
    End With
    
    'Application.OnTime TimeValue(Format(FinalTime, "yyyy/MM/dd hh:mm:ss")), "TimerLogic", , False
    'Application.OnTime TimeValue(Format(FinalTime, "hh:mm:ss")), "TimerLogic", , False

    '前回更新日時セット
'    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
'    If Not srchRange Is Nothing Then
'        srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd hh:mm:ss")
'    End If
    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd hh:mm:ss")
    End If
    
    Call UpdateFormStatus
End Sub

'***************
'* タイマー処理
'***************
Public Sub TimerLogic()
    Dim wrkStr As String
    Dim wrkTime As String
    Dim updTime         As Integer
    Dim updTimeUnit     As String
    Dim srchRange       As Range
    Dim endD            As Date
    Dim endT            As Date
    Dim endTime         As Double
    Dim prevDate        As Date
    Dim prevTime        As Date
    
    Dim wrkSerial       As Double
    Dim wrkNow          As Double
    
    With Worksheets(ParamSheetName)
    
    
        ' タイマー停止処理************************************
        wrkSerial = 0
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            wrkSerial = DateValue(srchRange.Offset(0, 1).Value) + TimeValue(srchRange.Offset(0, 1).Value)
        End If
        
        '終了時刻と現在時刻を比較し、終了時刻<現在時刻⇒タイマー処理
        wrkNow = DateValue(Now) + TimeValue(Now)
        If wrkSerial < wrkNow Then
            Call AutoUpdateStop
            
            '前回更新日時セット
            Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd hh:mm:ss")
            End If
        
        End If
        '*****************************************************
    
    
        '* 自動更新にOFFがセットされていれば処理をやめる
        Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Cells.Find(What:="自動更新", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            If srchRange.Offset(0, 1).Value <> "ON" Then
                Application.CutCopyMode = False
                Exit Sub
            End If
        End If
    
        '*********************
        '* 更新周期を取得
        '*********************
        Set srchRange = .Cells.Find(What:="周期", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            updTime = srchRange.Offset(0, 1).Value * 60         ' 秒計算
        Else
            updTime = 1
        End If
        updTimeUnit = "【秒】"
            
        '*********************
        '* 取得終了日時を取得
        '*********************
        Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            endD = DateValue(srchRange.Offset(0, 1).Value)
            endT = TimeValue(srchRange.Offset(0, 1).Value)
        Else
            endD = DateValue("2020/12/31 23:59:59")
            endT = TimeValue("2020/12/31 23:59:59")
        End If
        If Not IsDate(endD + endT) Then
            MsgBox "収集終了日時欄が正しくないため処理を中断します。"
            Exit Sub
        End If
        endTime = GetUnixTime(endD + endT)
            
        prevDate = Now
        prevTime = DateAdd("s", updTime, prevDate)
        Application.OnTime prevTime, "'TimerLogic'"
        
    End With
    
'    Application.OnTime Now + TimeValue("00:00:05"), "'TimerLogic'"

    '前回更新日時セット
    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(prevDate, "yyyy/MM/dd hh:mm:ss")
    End If
    Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        srchRange.Offset(0, 1).Value = Format(prevTime, "yyyy/MM/dd hh:mm:ss")
    End If
    
    '***************************
    '***************************
    '* ミッションから情報を取得
    Call GetMissionInfoAll
    
    '* ピボットテーブルを更新
    Call UpdateGraphDataSecond
    
    '* グラフ編集画面の設定を元にグラフを更新
    Call UpdateAfterGraphEdit
    '***************************
    '***************************
    
    ActiveSheet.Cells(1, 1).Select
    
    Call HaikeiHanreiDataMake
    
End Sub

'*****************************
'* スケジュール予約キャンセル
'*****************************
Public Sub CancelSchedule()
    Dim currWorksheet   As Worksheet
    Dim srchRange       As Range
    Dim prevDate        As Date

    Set currWorksheet = ThisWorkbook.Worksheets(ParamSheetName)

    With currWorksheet
        '* 予約したスケジュールをキャンセルする
        Set srchRange = .Cells.Find(What:="次回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            prevDate = srchRange.Offset(0, 1).Value
            On Error Resume Next
            Application.OnTime prevDate, "'TimerLogic'", , False
        End If
    End With

End Sub






