Attribute VB_Name = "BaseLine_Module"
Option Explicit

'*********************
'* 基準線を再配置する
'*********************
Public Sub GraphReCalculate()
    Dim graphWorksheet      As Worksheet
    Dim graphEditWorksheet  As Worksheet
    Dim currWorksheet       As Worksheet
    Dim hBaseList()         As BASEINFO
    Dim vBaseList()         As BASEINFO
    Dim hValue              As MAXMINSTEPINFO
    Dim vValue              As MAXMINSTEPINFO
    Dim currChartObj        As ChartObject
    
    ReDim vBaseList(0)
    ReDim hBaseList(0)
    
    '* グラフ編集のワークシート
    Set graphEditWorksheet = Worksheets(GraphEditSheetName)
    '* グラフのワークシート
    Set graphWorksheet = Worksheets(FixGraphSheetName)
    
    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    '* ※縦軸はないため、横軸のみデータを取得
    Call GetMaxMinStepValue2(GraphEditSheetName, hValue)
    
    '* グラフ編集シートから縦軸と横軸の基準線情報を取得する
    '* ※縦軸はないため、横軸のみデータを取得
    Call GetBaseLineInfo(GraphEditSheetName, hBaseList)
    
    '* グラフから古い系統を削除する
    Set currChartObj = graphEditWorksheet.ChartObjects(HistGraph02Name)
    Call DeleteOldBaseLineSeries(currChartObj)
    Set currChartObj = graphWorksheet.ChartObjects(HistGraph02Name)
    Call DeleteOldBaseLineSeries(currChartObj)
    
    '* グラフテーブルから古い基準線データを削除する（X軸方向のみ）
    Call DeleteOldBaseLineDataOnlyHorizon
    
    '* グラフテーブルに新しい基準線データを生成する（X軸方向のみ）
    Call AddNewBaseLineDataOnlyHorizon

    '* グラフに新しい系統を追加する
    
'    Application.ScreenUpdating = False
    
    Set currWorksheet = ActiveSheet
    
    graphEditWorksheet.Activate
    Set currChartObj = graphEditWorksheet.ChartObjects(HistGraph02Name)
    Call AddNewBaseLineSeries(currChartObj)
    graphWorksheet.Activate
    Set currChartObj = graphWorksheet.ChartObjects(HistGraph02Name)
    Call AddNewBaseLineSeries(currChartObj)
    
    currWorksheet.Activate
    
'    Application.ScreenUpdating = True
    
'    '* グラフ編集シートの基準／上限／下限の水平方向の文字列を合わせる
'    Call MoveShapeHorizonInGraphAll(graphEditWorksheet, vBaseList, hBaseList, vValue, hValue)
'
'    '* グラフシートの基準／上限／下限の水平方向の文字列を合わせる
'    Call MoveShapeHorizonInGraphAll(graphWorksheet, vBaseList, hBaseList, vValue, hValue)
End Sub

'*******************************************************
'* グラフ編集シートから縦軸と横軸の基準線情報を整理する
'*******************************************************
Public Sub OrganizeList(currSheetName As String)
    Dim currWorksheet           As Worksheet
    Dim srchRange               As Range
    Dim vStartRange             As Range
    Dim hStartRange             As Range

    Set currWorksheet = Worksheets(currSheetName)
    
    With currWorksheet

        Set srchRange = .Cells.Find(What:="−縦軸基準線−", LookAt:=xlWhole)
        Set vStartRange = srchRange.Offset(1, 0)
        Set srchRange = .Cells.Find(What:="−横軸基準線−", LookAt:=xlWhole)
        Set hStartRange = srchRange.Offset(1, 0)
    
    End With


End Sub

'*******************************
'* 指定グラフから古い系統を削除する
'*******************************
Public Sub DeleteOldBaseLineSeries(currChartObj As ChartObject)
    Dim currSeries              As Series
    Dim vBaseList()             As BASEINFO
    Dim hBaseList()             As BASEINFO
    Dim II                      As Integer
    
    ReDim hBaseList(0)
    
    Call GetOldBaseLineData(hBaseList)
    
    For Each currSeries In currChartObj.Chart.SeriesCollection
        If hBaseList(0).Name <> "" Then
            For II = 0 To UBound(hBaseList)
                If currSeries.Name = hBaseList(II).Name Then
                    currSeries.Delete
                    Exit For
                End If
            Next
        End If
    Next currSeries
End Sub

'***********************************************
'* モーメント図グラフ値シートから古い基準線データを取得する
'***********************************************
Public Sub GetOldBaseLineData(hBaseList() As BASEINFO)
    Dim srchRange           As Range
    Dim hStartRange         As Range
    Dim lastRange           As Range
    Dim delRange            As Range
    Dim graphDataWorksheet  As Worksheet
    Dim readIndex           As Integer
    Dim iFirstH             As Boolean
    Dim hIndex              As Integer
    
    Set graphDataWorksheet = Worksheets(MomentSheetName)
    
    With graphDataWorksheet
        Set hStartRange = .Range("W1")
        
        iFirstH = True
        readIndex = 0
        Do
            If hStartRange.Offset(0, readIndex).Value = "" Then Exit Do
            
            If iFirstH Then
                hIndex = 0
                iFirstH = False
            Else
                hIndex = UBound(hBaseList) + 1
                ReDim Preserve hBaseList(hIndex)
            End If
            hBaseList(hIndex).Name = hStartRange.Offset(0, readIndex).Value
            hBaseList(hIndex).Value = hStartRange.Offset(0, readIndex + 1).Value
            readIndex = readIndex + 2
        Loop
        
    End With
End Sub

'***********************************************
'* グラフテーブルから古い基準線データを削除する
'***********************************************
Public Sub DeleteOldBaseLineData()
    Dim srchRange           As Range
    Dim vStartRange         As Range
    Dim hStartRange         As Range
    Dim lastRange           As Range
    Dim delRange            As Range
    Dim graphDataWorksheet  As Worksheet
    
    Set graphDataWorksheet = Worksheets(PivotSheetName)
    
    With graphDataWorksheet
        Set srchRange = .Cells.Find(What:="縦横基準線系統", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            Set vStartRange = srchRange.Offset(1, 0)
            Set hStartRange = srchRange.Offset(4, 0)
        Else
            Exit Sub
        End If
        Set lastRange = vStartRange.Offset(1, 0).End(xlToRight)
        Set delRange = .Range(vStartRange.Offset(0, 0), lastRange.Offset(1, 0))
        delRange.Clear
        Set lastRange = hStartRange.Offset(1, 0).End(xlToRight)
        Set delRange = .Range(hStartRange.Offset(0, 0), lastRange.Offset(1, 0))
        delRange.Clear
    End With
End Sub

'***********************************************
'* グラフテーブルから古い基準線データを削除する（X軸方向のみ）
'***********************************************
Public Sub DeleteOldBaseLineDataOnlyHorizon()
    Dim srchRange           As Range
    Dim hStartRange         As Range
    Dim lastRange           As Range
    Dim delRange            As Range
    Dim graphDataWorksheet  As Worksheet
    
    Set graphDataWorksheet = Worksheets(MomentSheetName)
    
    With graphDataWorksheet
        Set hStartRange = .Range("W1")
        Set lastRange = hStartRange.Offset(0, 0).End(xlToRight)
        Set delRange = .Range(hStartRange.EntireColumn, lastRange.EntireColumn)
        delRange.Delete
    End With
End Sub

'***********************************************
'* グラフテーブルに新しい基準線データを生成する
'***********************************************
Public Sub AddNewBaseLineData()
    Dim graphDataWorksheet  As Worksheet
    Dim hBaseList()         As BASEINFO
    Dim vBaseList()         As BASEINFO
    Dim srchRange           As Range
    Dim vStartRange         As Range
    Dim hStartRange         As Range
    Dim II                  As Integer
    Dim hValue              As MAXMINSTEPINFO
    Dim vValue              As MAXMINSTEPINFO
    Dim writeIndex          As Integer
    Dim seriesRange         As Range
    
    ReDim vBaseList(0)
    ReDim hBaseList(0)
    
    Set graphDataWorksheet = Worksheets(PivotSheetName)

    '* グラフ編集シートから縦軸と横軸の基準線情報を取得する
    Call GetBaseLineInfo(GraphEditSheetName, hBaseList)
    
    '* グラフ編集シートから縦軸／横軸のそれぞれの最大／最小／刻み幅を取得する
    Call GetMaxMinStepValue(GraphEditSheetName, vValue, hValue)
    
    With graphDataWorksheet
    
        Set srchRange = .Cells.Find(What:="縦横基準線系統", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            Set vStartRange = srchRange.Offset(1, 0)
            Set hStartRange = srchRange.Offset(4, 0)
        Else
            Exit Sub
        End If
        
        '* 縦軸基準線データ展開
        writeIndex = 0
        If vBaseList(0).Name <> "" Then
            For II = 0 To UBound(vBaseList)
                vStartRange.Offset(0, writeIndex).Value = vBaseList(II).Name
                vStartRange.Offset(1, writeIndex).Value = vBaseList(II).Value
                vStartRange.Offset(2, writeIndex).Value = vBaseList(II).Value
                vStartRange.Offset(1, writeIndex + 1).Value = vValue.maxValue
                vStartRange.Offset(2, writeIndex + 1).Value = vValue.minValue
                Set seriesRange = .Range(vStartRange.Offset(0, writeIndex), vStartRange.Offset(2, writeIndex + 1))
                With seriesRange.Borders
                    .LineStyle = xlContinuous
                    .Weight = xlMedium
                End With
                With seriesRange.Borders(xlInsideVertical)
                    .LineStyle = xlContinuous
                    .Weight = xlThin
                End With
                With seriesRange.Borders(xlInsideHorizontal)
                    .LineStyle = xlContinuous
                    .Weight = xlThin
                End With
                Set seriesRange = .Range(vStartRange.Offset(0, writeIndex), vStartRange.Offset(0, writeIndex + 1))
                With seriesRange.Borders(xlEdgeBottom)
                    .LineStyle = xlDouble
                End With
                writeIndex = writeIndex + 2
            Next
        End If
    
        '* 横軸基準線データ展開
        writeIndex = 0
        If hBaseList(0).Name <> "" Then
            For II = 0 To UBound(hBaseList)
                hStartRange.Offset(0, writeIndex).Value = hBaseList(II).Name
                hStartRange.Offset(1, writeIndex).Value = hValue.maxValue
                hStartRange.Offset(2, writeIndex).Value = hValue.minValue
                hStartRange.Offset(1, writeIndex + 1).Value = hBaseList(II).Value
                hStartRange.Offset(2, writeIndex + 1).Value = hBaseList(II).Value
                Set seriesRange = .Range(hStartRange.Offset(0, writeIndex), hStartRange.Offset(2, writeIndex + 1))
                With seriesRange.Borders
                    .LineStyle = xlContinuous
                    .Weight = xlMedium
                End With
                With seriesRange.Borders(xlInsideVertical)
                    .LineStyle = xlContinuous
                    .Weight = xlThin
                End With
                With seriesRange.Borders(xlInsideHorizontal)
                    .LineStyle = xlContinuous
                    .Weight = xlThin
                End With
                Set seriesRange = .Range(hStartRange.Offset(0, writeIndex), hStartRange.Offset(0, writeIndex + 1))
                With seriesRange.Borders(xlEdgeBottom)
                    .LineStyle = xlDouble
                End With
                writeIndex = writeIndex + 2
            Next
        End If
    
    End With
    
End Sub

'***********************************************
'* グラフテーブルに新しい基準線データを生成する（X軸方向のみ）
'***********************************************
Public Sub AddNewBaseLineDataOnlyHorizon()
    Dim momentWorksheet     As Worksheet
    Dim baseList()          As BASEINFO
    Dim srchRange           As Range
    Dim hStartRange         As Range
    Dim baseRange           As Range
    Dim destRange           As Range
    Dim seriesRange         As Range
    Dim II                  As Integer
    Dim hValue              As MAXMINSTEPINFO
    Dim writeIndex          As Integer
    Dim prevDate            As Date
    
    ReDim baseList(0)
    
    Set momentWorksheet = Worksheets(MomentSheetName)

    '* グラフ編集シートから基準線情報を取得する
    Call GetBaseLineInfo(GraphEditSheetName, baseList)
    
    '* グラフ編集シートから最大／最小／刻み幅を取得する
    Call GetMaxMinStepValue2(GraphEditSheetName, hValue)
    
    With momentWorksheet
    
        Set srchRange = .Range("W1")
        Set hStartRange = srchRange.Offset(0, 0)
        
        '* 横軸基準線データ展開
        writeIndex = 0
        If baseList(0).Name <> "" Then
            For II = 0 To UBound(baseList)
                '* ベースデータをコピー
                Set baseRange = .Range(.Range("C1").EntireColumn, .Range("D1").EntireColumn)
                Set destRange = .Range(hStartRange.Offset(0, writeIndex).EntireColumn, hStartRange.Offset(0, writeIndex + 1).EntireColumn)
                baseRange.Copy Destination:=destRange
                
                hStartRange.Offset(0, writeIndex).Value = baseList(II).Value
                hStartRange.Offset(0, writeIndex + 1).Value = baseList(II).Name
                
                writeIndex = writeIndex + 2
            Next
        End If
    
    End With
    
End Sub

'*******************************
'* グラフに新しい系統を追加する
'*******************************
Public Sub AddNewBaseLineSeries(currChartObj As ChartObject)
    Dim momentWorksheet  As Worksheet
    Dim srchRange           As Range
    Dim vStartRange         As Range
    Dim hStartRange         As Range
    Dim seriesName          As String
    Dim xRange              As Range
    Dim yRange              As Range
    Dim xRangeRC            As String
    Dim yRangeRC            As String
    Dim readIndex           As Integer

    Set momentWorksheet = Worksheets(MomentSheetName)
    
    With momentWorksheet
    
        Set srchRange = .Range("W1")
        Set hStartRange = srchRange.Offset(3, 0)
        
        '* 横軸方向
        readIndex = 0
        Do
            If hStartRange.Offset(0, readIndex).Value = "" Then Exit Do

            seriesName = srchRange.Offset(0, readIndex + 1).Value
            Set xRange = .Range(hStartRange.Offset(0, readIndex), hStartRange.Offset(0, readIndex).End(xlDown))
            Set yRange = .Range(hStartRange.Offset(0, readIndex + 1), hStartRange.Offset(0, readIndex + 1).End(xlDown))
            xRangeRC = "=" & MomentSheetName & "!" & xRange.Address
            yRangeRC = "=" & MomentSheetName & "!" & yRange.Address

            Call AddSeriesBaseLine(currChartObj, seriesName, xRangeRC, yRangeRC, RGB(255, 0, 0))
            
            readIndex = readIndex + 2
        Loop
    
    End With
        
End Sub

'******************************************
'* 散布図に対し、新たに系統を追加する
'******************************************
Public Sub AddSeriesBaseLine(currChartObj As ChartObject, seriesName As String, xValueRC As String, yValueRC As String, seriesRGB As Long)
    Dim currSeries          As Series
    
    With currChartObj.Chart
        Set currSeries = .SeriesCollection.NewSeries
        With currSeries
            .Name = seriesName
            .XValues = xValueRC
            .Values = yValueRC
            .MarkerStyle = xlMarkerStyleNone
            .Select
            With .Format.Line
                .Visible = msoTrue
                .DashStyle = msoLineDash
                .ForeColor.TintAndShade = 0
                .ForeColor.Brightness = 0
                .ForeColor.RGB = seriesRGB
                .Transparency = 0
                .Weight = 0.75
            End With
        End With
    End With
    
End Sub

'*********************************************
'* 指定シートにある基準線情報を取得する
'*********************************************
Public Sub GetBaseLineInfo(currSheetName As String, baseList() As BASEINFO)
    Dim currWorkseet        As Worksheet
    Dim srchRange           As Range
    Dim iFirstH             As Boolean
    Dim baseIndex          As Integer
    Dim readIndex           As Integer

    '* グラフのワークシート
    Set currWorkseet = Worksheets(currSheetName)
    
    With currWorkseet
        
        '* 縦軸基準線の情報を取得
        Set srchRange = .Cells.Find(What:="−基準線−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            iFirstH = True
            readIndex = 1
            Do While (srchRange.Offset(readIndex, 0).Value <> "−") And (srchRange.Offset(readIndex, 0).Value <> "")
                If iFirstH Then
                    baseIndex = 0
                    ReDim baseList(baseIndex)
                    iFirstH = False
                Else
                    baseIndex = UBound(baseList) + 1
                    ReDim Preserve baseList(baseIndex)
                End If
                baseList(baseIndex).Name = srchRange.Offset(readIndex, 0).Value
                baseList(baseIndex).Value = srchRange.Offset(readIndex, 1).Value
                readIndex = readIndex + 1
            Loop
        End If
    
    End With

End Sub

'***********************************************
'* グラフ内水平方向の文字列の位置を全て更新する
'***********************************************
Public Sub MoveShapeHorizonInGraphAll(graphWorksheet As Worksheet, _
                                      vBaseList() As BASEINFO, hBaseList() As BASEINFO, _
                                      vValue As MAXMINSTEPINFO, hValue As MAXMINSTEPINFO)
    Dim chartObj            As ChartObject
    Dim currShape           As Shape
    Dim workStr             As String
    Dim hMaxValue           As Variant
    Dim hMinValue           As Variant
    Dim hBaseValue          As Variant
    Dim vMaxValue           As Variant
    Dim vMinValue           As Variant
    Dim vBaseValue          As Variant
    Dim paMax               As Variant
    Dim paMin               As Variant
    Dim paWidth             As Variant
    Dim paPos               As Variant
    Dim II                  As Integer
    Dim prevDate            As Date
    Dim srchRange           As Range
    
    With graphWorksheet
    
        '* 埋め込みグラフオブジェクトの取得
        Set chartObj = .ChartObjects(HistGraph02Name)
        
        '* 中途半端にグラフ上にテキストボックスがあれば一旦全て削除する
        For Each currShape In chartObj.Chart.Shapes
            currShape.Delete
        Next
        
        '* グラフ上にテキストボックスがなければ追加する
        Set srchRange = Worksheets(ParamSheetName).Cells.Find(What:="前回更新日時", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            prevDate = srchRange.Offset(0, 1).Value
        Else
            prevDate = CDate("2000/01/01 00:00:00")
        End If
        If chartObj.Chart.Shapes.Count = 0 Then
            For II = 0 To UBound(vBaseList)
                '* 縦軸方向の基準線文字列を追加する
                workStr = "vTB_Level" & Format(II, "00") & "InGraph"
                Call AddShapeHorizonInGraph(chartObj, workStr, "現在時刻")
                '* 縦軸方向の基準線文字列の位置を調整する
                Set currShape = chartObj.Chart.Shapes(workStr)
                Call MoveShapeHorizonInGraph(chartObj, currShape, hValue.maxValue, hValue.minValue, prevDate)
            Next
            For II = 0 To UBound(hBaseList)
                '* 横軸方向の基準線文字列を追加する
                workStr = "hTB_Level" & Format(II, "00") & "InGraph"
                Call AddShapeHorizonInGraph(chartObj, workStr, hBaseList(II).Name)
                '* 横軸方向の基準線文字列の位置を調整する
                Set currShape = chartObj.Chart.Shapes(workStr)
                Call MoveShapeVerticalInGraph(chartObj, currShape, vValue.maxValue, vValue.minValue, hBaseList(II).Value)
            Next
        End If
        
    End With

End Sub

'*************************************
'* 指定グラフの水平方向に上に指定文字列／テキストボックスIDを追加する
'*************************************
Public Sub AddShapeHorizonInGraph(chartObj As ChartObject, idStr As String, textStr As String)
    Dim graphWorksheet      As Worksheet
    Dim currShape           As Shape
    
    With chartObj.Chart.Shapes.AddLabel(msoTextOrientationHorizontal, 10, 10, 72, 72)
        With .TextFrame2
            .VerticalAnchor = msoAnchorMiddle
            .HorizontalAnchor = msoAnchorCenter
            .AutoSize = msoAutoSizeShapeToFitText
            With .TextRange
                .text = textStr
                .Font.Bold = msoTrue
                .Font.Size = 10
            End With
        End With
        .Name = idStr
    End With
        
End Sub

'*************************************
'* 指定グラフの垂直方向に上に指定文字列／テキストボックスIDを追加する
'*************************************
Public Sub AddShapeVerticalInGraph(chartObj As ChartObject, idStr As String, textStr As String)
    Dim graphWorksheet      As Worksheet
    Dim currShape           As Shape
    
    With chartObj.Chart.Shapes.AddLabel(msoTextOrientationHorizontal, 10, 10, 72, 72)
        With .TextFrame2
            .VerticalAnchor = msoAnchorMiddle
            .HorizontalAnchor = msoAnchorNone
            .AutoSize = msoAutoSizeShapeToFitText
            With .TextRange
                .text = textStr
                .Font.Bold = msoTrue
                .Font.Size = 10
            End With
        End With
        .Name = idStr
    End With
        
End Sub

'*************************************
'* グラフ内の水平方向のテキストボックスを移動する
'*************************************
Public Sub MoveShapeHorizonInGraph(chartObj As ChartObject, currShape As Shape, hMaxValue, hMinValue, hBaseValue)
    Dim graphWorksheet      As Worksheet
    Dim graphDataWorksheet  As Worksheet
    Dim workStr             As String
    Dim paMax               As Variant
    Dim paMin               As Variant
    Dim paWidth             As Variant
    Dim paPos               As Variant
    
    '* 基準値があるならば文字列の位置を合わせる
    If hBaseValue <> 0 Then
        currShape.Visible = msoTrue
        paWidth = chartObj.Chart.PlotArea.InsideWidth
        paMin = chartObj.Chart.PlotArea.InsideLeft
        paPos = ((hBaseValue - hMinValue) / (hMaxValue - hMinValue) * paWidth + paMin) - currShape.Width / 2

        '* 文字シェイプを新しい座標にセットする
        currShape.Top = chartObj.Chart.PlotArea.InsideTop - currShape.Height
        currShape.Left = paPos
    '* 基準値がなければ文字列を非表示にする
    Else
        currShape.Visible = msoFalse
    End If


End Sub

'*************************************
'* グラフ内の垂直方向のテキストボックスを移動する
'*************************************
Public Sub MoveShapeVerticalInGraph(chartObj As ChartObject, currShape As Shape, vMaxValue, vMinValue, vBaseValue)
    Dim graphWorksheet      As Worksheet
    Dim graphDataWorksheet  As Worksheet
    Dim workStr             As String
    Dim paMax               As Variant
    Dim paMin               As Variant
    Dim paHeight            As Variant
    Dim paPos               As Variant
    
    '* 基準値があるならば文字列の位置を合わせる
    If vBaseValue <> 0 Then
        currShape.Visible = msoTrue
        paHeight = chartObj.Chart.PlotArea.InsideHeight
        paMin = chartObj.Chart.PlotArea.InsideTop
        paPos = paMin + paHeight - (vBaseValue * paHeight) / (vMaxValue - vMinValue) - currShape.Height / 2

        '* 文字シェイプを新しい座標にセットする
        currShape.Top = paPos
        currShape.Left = chartObj.Chart.PlotArea.InsideWidth + chartObj.Chart.PlotArea.InsideLeft + 5
    '* 基準値がなければ文字列を非表示にする
    Else
        currShape.Visible = msoFalse
    End If

End Sub







