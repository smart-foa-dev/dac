Attribute VB_Name = "HaikeiData_Module"
Option Explicit

'*******************************************************
' 背景ﾃﾞｰﾀ、凡例一覧表の作成
'*******************************************************
Public Sub HaikeiHanreiDataMake()

    ''Call HaikeiDataClear

    ''Call HaikeiDataToGraphSheet
    
    'Call HanreiMake
    
    Call GraphLabelCopy

End Sub

'*******************************************************
' 凡例一覧表の作成
'*******************************************************
Public Sub HanreiMake()
    Dim Mycontrol       As Object
    Dim olelist         As Object
    Dim objlist         As Object
    Dim TargetSheet     As Worksheet
    Dim CellSize        As Range
    Dim srchRange       As Range
    
    Dim currChartObj    As ChartObject
    Dim currSeries      As Series
    
    Dim wrkWidth
    Dim wrKTop
    Dim wrkLeft

    'リストボックス作成
    Set TargetSheet = Worksheets(FixGraphSheetName)
    
    'リストBox削除
    For Each Mycontrol In TargetSheet.OLEObjects
        Mycontrol.Delete
    Next
 
    '背景データ書込み位置
    Set srchRange = TargetSheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    Set CellSize = TargetSheet.Range(srchRange.Offset(0, 0), srchRange.Offset(0, 2))
    
    'リストコントロールの位置決め
    wrkWidth = srchRange.Offset(0, 0).Width + srchRange.Offset(0, 1).Width  ''' + srchRange.Offset(0, 2).Width
    wrKTop = TargetSheet.Range("M18").Top
    wrkLeft = TargetSheet.Range("M18").Left
    'Set olelist = TargetSheet.OLEObjects.Add(ClassType:="Forms.ListBox.1", Left:=wrkLeft, Top:=wrKTop, Width:=132, Height:=150)
    Set olelist = TargetSheet.OLEObjects.Add(ClassType:="Forms.ListBox.1", Left:=wrkLeft, Top:=wrKTop, Width:=wrkWidth, Height:=150)
    
    '作成したリストボックス生成
    Set objlist = TargetSheet.OLEObjects(olelist.Name).Object
'    objlist.FontName = "メイリオ"
'    objlist.FontSize = 12

    '凡例をリストBoxに作成
    Set currChartObj = TargetSheet.ChartObjects(HistGraph02Name)
    For Each currSeries In currChartObj.Chart.SeriesCollection
        If InStr(currSeries.Name, "系列") > 0 Then
        Else
            objlist.AddItem currSeries.Name
        End If
    Next currSeries
    
    
    'TargetSheet.Select
    
    '終了処理
'    Set objlist = Nothing
'    Set olelist = Nothing
End Sub

'*******************************************************
' 背景データをグラフシートに移動
'*******************************************************
Public Sub HaikeiDataToGraphSheet()
    Dim pos                 As Integer
    Dim cnt                 As Integer
    Dim endRow              As Integer
    
    Dim srcTarget           As Worksheet
    Dim srcRange            As Range
    Dim copyMoto            As Range
    Dim haikeiRange         As Range
    
    Dim TargetSheet         As Worksheet
    Dim TargetRange         As Range
    Dim copySaki            As Range

    Dim lineRange           As Range

    
    Set srcTarget = Worksheets(GraphEditSheetName)
    Set TargetSheet = Worksheets(FixGraphSheetName)
    
    'グラフ編集シートの背景データ数
    Set srcRange = srcTarget.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    Set haikeiRange = srcRange.Offset(1, 0)
    
    '背景データの有無チェック
    If haikeiRange.Offset(1, 0).Value = "" Or haikeiRange.Offset(1, 0).Value = "-" Or haikeiRange.Offset(1, 0).Value = "−" Or haikeiRange.Offset(1, 0).Value = Null Then
        Exit Sub
    End If
    
    Set copyMoto = srcTarget.Range(haikeiRange.Offset(1, 0).Cells, haikeiRange.End(xlDown).Cells)

'    If haikeiRange.End(xlDown).Row > 10 Then
'        Exit Sub
'    End If
'
    endRow = haikeiRange.End(xlDown).Cells.Row
    
    'グラフシートの背景データ書込み位置
    Set TargetRange = TargetSheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    Set copySaki = TargetRange.Offset(1, 0)

    '背景データコピー
    'copyMoto.Copy Destination:=copySaki
    cnt = 0
    For pos = 6 To endRow
        If haikeiRange.Offset(cnt + 1, 0).Value = "-" Or haikeiRange.Offset(cnt + 1, 0).Value = "−" Then
            Exit For
        End If
    
        copySaki.Offset(cnt, 0).Value = haikeiRange.Offset(cnt + 1, 0).Value
        copySaki.Offset(cnt, 2).Value = haikeiRange.Offset(cnt + 1, 1).Value & haikeiRange.Offset(cnt + 1, 2).Value
        '背景データ名称罫線作成
        Set lineRange = TargetSheet.Range(TargetRange.Offset(cnt + 1, 0).Cells, TargetRange.Offset(cnt + 1, 1).Cells)
        With lineRange
            .Borders(xlEdgeTop).LineStyle = xlContinuous
            .Borders(xlEdgeTop).Weight = xlThin
            .Borders(xlEdgeBottom).LineStyle = xlContinuous
            .Borders(xlEdgeBottom).Weight = xlThin
            .Borders(xlEdgeLeft).LineStyle = xlContinuous
            .Borders(xlEdgeLeft).Weight = xlThin
            .Borders(xlEdgeRight).LineStyle = xlContinuous
            .Borders(xlEdgeRight).Weight = xlThin
        End With
        
        '背景データ値+単位罫線作成
        Set lineRange = TargetSheet.Range(TargetRange.Offset(cnt + 1, 2).Cells, TargetRange.Offset(cnt + 1, 2).Cells)
        With lineRange
            .Borders(xlEdgeTop).LineStyle = xlContinuous
            .Borders(xlEdgeTop).Weight = xlThin
            .Borders(xlEdgeBottom).LineStyle = xlContinuous
            .Borders(xlEdgeBottom).Weight = xlThin
            .Borders(xlEdgeLeft).LineStyle = xlContinuous
            .Borders(xlEdgeLeft).Weight = xlThin
            .Borders(xlEdgeRight).LineStyle = xlContinuous
            .Borders(xlEdgeRight).Weight = xlThin
        End With
        cnt = cnt + 1
    Next pos
End Sub

'*******************************************************
' グラフシートの背景データクリアー
'*******************************************************
Public Sub HaikeiDataClear()

    Dim TargetSheet         As Worksheet
    Dim TargetRange         As Range
    Dim endCell             As Range
    Dim clearCell           As Range

    Set TargetSheet = Worksheets(FixGraphSheetName)
    
    'グラフシートの背景データ書込み位置
    Set TargetRange = TargetSheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    
        '背景データの有無チェック
    If TargetRange.Offset(1, 0).Value = "" Then
        Exit Sub
    End If
    
    Set endCell = TargetRange.End(xlDown).Cells

    Set clearCell = TargetSheet.Range(TargetRange.Offset(1, 0), endCell.Offset(0, 2))
'    clearCell.Delete (xlShiftUp)
    With clearCell
        .ClearContents
        .Borders(xlDiagonalDown).LineStyle = xlNone
        .Borders(xlDiagonalUp).LineStyle = xlNone
        .Borders(xlEdgeLeft).LineStyle = xlNone
        .Borders(xlEdgeTop).LineStyle = xlNone
        .Borders(xlEdgeBottom).LineStyle = xlNone
        .Borders(xlEdgeRight).LineStyle = xlNone
        .Borders(xlInsideVertical).LineStyle = xlNone
        .Borders(xlInsideHorizontal).LineStyle = xlNone
    End With
End Sub

'*******************************************************
' グラフラベルコピー
'*******************************************************
Public Sub GraphLabelCopy()
    Dim pvtWorksheet    As Worksheet
    Dim currChartObj    As ChartObject
    Dim actChartObj     As Object
    Dim etcChartObj     As Object
    
    Dim wrkTate         As String
    Dim wrkYoko         As String
        
    'Set pvtWorksheet = Worksheets("グラフ編集")
    'Set currChartObj = pvtWorksheet.ChartObjects("BulkyGraph")
    'Set actChartObj = currChartObj.Chart
    Set actChartObj = Worksheets(GraphEditSheetName).ChartObjects(HistGraph01Name).Chart

    'グラフ編集用ラベル取込
    With actChartObj
'        .HasTitle = True                  '---グラフタイトル表示
'        .ChartTitle.Text = "9月度売上"    '---タイトル文字列設定
        '横ラベル
         With .Axes(xlCategory, xlPrimary) '---主軸項目軸tate
            wrkYoko = .AxisTitle.text
'             .HasTitle = True              '---軸ラベル表示
'             .AxisTitle.Text = "売り場"    '---軸ラベル文字列設定
         End With
         '縦ラベル
         With .Axes(xlValue, xlPrimary)    '---主軸数値軸
            wrkTate = .AxisTitle.text
'             .HasTitle = True              '---軸ラベル表示
'             .AxisTitle.Text = "売上高"    '---軸ラベル文字列設定
         End With
    End With
    
    Set actChartObj = Worksheets(FixGraphSheetName).ChartObjects(HistGraph02Name).Chart
    'グラフ用ラベル書込
    With actChartObj
'        .HasTitle = True                  '---グラフタイトル表示
'        .ChartTitle.Text = "9月度売上"    '---タイトル文字列設定
        '横ラベル
         With .Axes(xlCategory, xlPrimary) '---主軸項目軸tate
             .HasTitle = True              '---軸ラベル表示
             .AxisTitle.text = wrkYoko     '---軸ラベル文字列設定
         End With
         '縦ラベル
         With .Axes(xlValue, xlPrimary)    '---主軸数値軸
             .HasTitle = True              '---軸ラベル表示
             .AxisTitle.text = wrkTate     '---軸ラベル文字列設定
         End With
    End With
End Sub





