VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} BackDataForm 
   Caption         =   "背景データリスト"
   ClientHeight    =   3120
   ClientLeft      =   45
   ClientTop       =   390
   ClientWidth     =   7605
   OleObjectBlob   =   "BackDataForm.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "BackDataForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

'Option Explicit

'背景データ書込シート名称
Dim HaikeiSheetName     As String

'****************************
'* 背景データ書込み位置取消
'****************************
Private Sub bttonHaikeiCellDelete_Click()
    Dim actSheet        As Worksheet
    Dim srchRange       As Range
    Dim wrkVal
    Dim writeRange      As Range
    Dim lastRange       As Range
    Dim workRange       As Range

    Set actSheet = Worksheets(HaikeiSheetName)
    Set srchRange = actSheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    'srchRange.Offset(0, 0).Value = ""

    Set writeRange = srchRange.Offset(0, 0)
    Set lastRange = writeRange.Offset(10, 0)
    Set workRange = actSheet.Range(writeRange, lastRange.Offset(0, 2))
    
    workRange.Clear
End Sub

'****************************
'* 背景データ書込み位置決定
'****************************
Private Sub bttonHaikeiCellSet_Click()

    Dim actSheet        As Worksheet
    Dim wrkVal
    
    HaikeiSheetName = ActiveSheet.Name
    Set actSheet = ActiveSheet
    wrkVal = ActiveCell.Address
    
    With actSheet.Range(ActiveCell.Address)
''        .Font.Bold = False
''        .Font.Color = RGB(0, 0, 0)
''        .Font.Name = "メイリオ"
''        .Font.Size = 9
        .Value = "−背景データ−"
    End With
    
'    With actSheet.Range(ActiveCell.Address).Offset(1, 0)
'        .Borders.LineStyle = xlContinuous
'        .Borders.Weight = xlThin
'        .Borders(xlEdgeBottom).LineStyle = xlContinuous
'        .Borders(xlEdgeBottom).Weight = xlThin
'        .Borders(xlEdgeLeft).LineStyle = xlContinuous
'        .Borders(xlEdgeLeft).Weight = xlThin
'        .Borders(xlEdgeRight).LineStyle = xlContinuous
'        .Borders(xlEdgeRight).Weight = xlThin
'    End With

End Sub

'*********************
'* 追加ボタン押下処理
'*********************
Private Sub BT_Add_Click()
    Dim currWorksheet       As Worksheet
    Dim currRange           As Range
    Dim srchRange           As Range
    
    Set currWorksheet = ActiveSheet
    With currWorksheet
        
        Set currRange = ActiveCell
        Set srchRange = .Cells(1, currRange.Column)
        
        With Me.LV_BackDataList.ListItems.Add
            .text = srchRange.Offset(0, 0).Value
            .SubItems(1) = currRange.Value
'            .SubItems(2) = srchRange.Offset(2, 0).Value
        End With
        Me.LV_BackDataList.ColumnHeaders(1).Width = 100
        Me.LV_BackDataList.ColumnHeaders(2).Width = 100
        Me.LV_BackDataList.ColumnHeaders(3).Width = 100
    End With
End Sub

'***********************
'* 全て削除ボタン押下処理
'***********************
Private Sub BT_AllDelete_Click()
    Me.LV_BackDataList.ListItems.Clear
End Sub

'***********************
'* 閉じるボタン押下処理
'***********************
Private Sub BT_Close_Click()
    'Dim itemLV          As ListItem
    Dim currWorksheet   As Worksheet
    Dim writeRange      As Range
    Dim srchRange       As Range
    Dim writeIndex      As Integer
    Dim workRange       As Range
    Dim lastRange       As Range
    
    If HaikeiSheetName = "" Or HaikeiSheetName = Null Then
        Exit Sub
    End If
    
    Set currWorksheet = Worksheets(HaikeiSheetName)
    'Set currWorksheet = Worksheets(GraphEditSheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    If Not srchRange Is Nothing Then
        
        '* 古いデータを削除
        Set writeRange = srchRange.Offset(1, 0)
        Set lastRange = writeRange.Offset(6, 0)
        Set workRange = currWorksheet.Range(writeRange, lastRange.Offset(0, 2))
        workRange.Clear
        
'        Set writeRange = srchRange.Offset(2, 0)
'        Set lastRange = writeRange.End(xlDown)
'        Set workRange = currWorksheet.Range(writeRange, lastRange.Offset(0, 2))
'        workRange.Clear
'        workRange.Interior.Color = RGB(255, 255, 255)
        
        writeIndex = 0
        For Each itemLV In Me.LV_BackDataList.ListItems
            writeRange.Offset(writeIndex, 0).Value = itemLV.text
            writeRange.Offset(writeIndex, 0).EntireColumn.AutoFit
            writeRange.Offset(writeIndex, 1).Value = itemLV.SubItems(1)
            writeRange.Offset(writeIndex, 1).EntireColumn.AutoFit
            Set workRange = currWorksheet.Range(writeRange.Offset(writeIndex, 0), writeRange.Offset(writeIndex, 1))
            
'            writeRange.Offset(writeIndex, 2).Value = itemLV.SubItems(2)
'            writeRange.Offset(writeIndex, 2).EntireColumn.AutoFit
'            Set workRange = currWorksheet.Range(writeRange.Offset(writeIndex, 0), writeRange.Offset(writeIndex, 2))

            With workRange
                .Borders.LineStyle = xlContinuous
                .Borders.Weight = xlThin
                .Borders(xlEdgeBottom).LineStyle = xlContinuous
                .Borders(xlEdgeBottom).Weight = xlThin
                .Borders(xlEdgeLeft).LineStyle = xlContinuous
                .Borders(xlEdgeLeft).Weight = xlThin
                .Borders(xlEdgeRight).LineStyle = xlContinuous
                .Borders(xlEdgeRight).Weight = xlThin
                
''                With .Font
''                    .Bold = False
''                    .Color = RGB(0, 0, 0)
''                    .Name = "メイリオ"
''                    .Size = 9
''                End With
                
            End With
            
            writeIndex = writeIndex + 1
        Next itemLV
    End If
    
    currWorksheet.Activate
    
    Unload Me
    
End Sub

'***********************
'* 削除ボタン押下処理
'***********************
Private Sub BT_Delete_Click()
    'Dim itemLV  As ListItem
    Dim II      As Integer
    
    For II = Me.LV_BackDataList.ListItems.Count To 1 Step -1
    
        Set itemLV = Me.LV_BackDataList.ListItems(II)
        If itemLV.Selected Then
            Me.LV_BackDataList.ListItems.Remove itemLV.Index
        End If
    
    Next

End Sub

Private Sub UserForm_Deactivate()
    Unload Me
End Sub

'***********************
'* ユーザフォーム初期化
'***********************
Private Sub UserForm_Initialize()
    
    HaikeiSheetName = ActiveSheet.Name
    
    With Me.LV_BackDataList
        .AllowColumnReorder = True
        .Enabled = True
        .LabelEdit = lvwManual
        .FullRowSelect = True
        .ColumnHeaders.Add , "_Name", "エレメント名"
        .ColumnHeaders.Add , "_Value", "値"
        .ColumnHeaders.Add , "_Unit", "単位"
    End With
    
    Call GetHaikeiData

End Sub

'***********************************************
'* シート上の背景データ取込
'***********************************************
Public Sub GetHaikeiData()
    Dim currWorksheet       As Worksheet
    Dim checkRange          As Range
    Dim srchRange           As Range
    Dim lastRange           As Range
    Dim startRange          As Range
    Dim readIndex           As Integer
    
    If HaikeiSheetName = "" Or HaikeiSheetName = Null Then
        Exit Sub
    End If

    Set currWorksheet = Worksheets(HaikeiSheetName)
    
    With currWorksheet
    
        Set srchRange = .Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            Set startRange = srchRange.Offset(1, 0)
            
            If startRange.Value = "" Then
                Exit Sub
            End If
            
            Set lastRange = srchRange.End(xlDown).Offset(1, 0)
            Set checkRange = .Range(srchRange.Offset(0, 0), lastRange.Offset(0, 2))
        

                '* 背景データ編集画面を開く
'                Load BackDataForm
                readIndex = 0
                Do While Not IsEmpty(startRange.Offset(readIndex, 0).Value)
                    If "−" <> startRange.Offset(readIndex, 0).Value Then  '**** 20160530
                        With BackDataForm.LV_BackDataList.ListItems.Add
                            .text = startRange.Offset(readIndex, 0).Value
                            .SubItems(1) = startRange.Offset(readIndex, 1).Value
                            .SubItems(2) = startRange.Offset(readIndex, 2).Value
                        End With
                    End If
                    readIndex = readIndex + 1
                Loop
'                BackDataForm.Show vbModeless

        End If
    End With

End Sub





