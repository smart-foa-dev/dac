Attribute VB_Name = "ManualHaikei_Module"
'***********************************************
'* 背景データの手入力時、罫線作成
'***********************************************
Public Sub HaikeiDataInput(ByVal sheetName As String, ByVal Target As Range)
    Dim wrkStr          As String
    Dim wrkTarget       As Range
    Dim srchRange       As Range
    Dim titleRange      As Range
    Dim currWorksheet   As Worksheet
    Dim wrkVal
    
    '行チェック：
    If Target.Offset(0, 0).text = Null Or Target.Offset(0, 0).text = "" Or Target.Offset(0, 0).text = "-" Or Target.Offset(0, 0).text = "−" Then
        Exit Sub
    End If
    
    '列チェック：背景データの列での変化か？
    Set currWorksheet = Worksheets(sheetName)
    Set srchRange = currWorksheet.Cells.Find(What:="−背景データ−", LookAt:=xlWhole)
    
    If srchRange Is Nothing Then
        Exit Sub
    End If
    
    wrkVal = srchRange.Column
    If wrkVal <> Target.Column Then
        Exit Sub
    End If
    
        ' 現在行の罫線作成
    If Target.Offset(0, 0).Value = "−背景データ−" Then
        Exit Sub
    End If
    
    Set wrkTarget = currWorksheet.Range(Target.Offset(0, 0), Target.Offset(0, 1))
    With wrkTarget
        .Borders.LineStyle = xlContinuous
        .Borders.Weight = xlThin
        .Borders(xlEdgeTop).LineStyle = xlContinuous    'xlDot
        .Borders(xlEdgeTop).Weight = xlThin
        .Borders(xlEdgeBottom).LineStyle = xlContinuous
        .Borders(xlEdgeBottom).Weight = xlThin  'xlMedium
        .Borders(xlEdgeLeft).LineStyle = xlContinuous
        .Borders(xlEdgeLeft).Weight = xlThin    'xlMedium
        .Borders(xlEdgeRight).LineStyle = xlContinuous
        .Borders(xlEdgeRight).Weight = xlThin 'xlMedium
    End With
    
    ' 次行の罫線作成
    Set wrkTarget = currWorksheet.Range(Target.Offset(1, 0), Target.Offset(1, 1))
    'Set wrkTarget = currWorksheet.Range(Target.Offset(1, 0), Target.Offset(1, 2))
    With wrkTarget
        .Borders.LineStyle = xlContinuous
        .Borders.Weight = xlThin
        .Borders(xlEdgeTop).LineStyle = xlContinuous    'xlDot
        .Borders(xlEdgeTop).Weight = xlThin
        .Borders(xlEdgeBottom).LineStyle = xlContinuous
        .Borders(xlEdgeBottom).Weight = xlThin  'xlMedium
        .Borders(xlEdgeLeft).LineStyle = xlContinuous
        .Borders(xlEdgeLeft).Weight = xlThin    'xlMedium
        .Borders(xlEdgeRight).LineStyle = xlContinuous
        .Borders(xlEdgeRight).Weight = xlThin 'xlMedium

''        With .Font
''            .Bold = False
''            .Color = RGB(0, 0, 0)
''            .Name = "メイリオ"
''            .Size = 10
''        End With
    End With
    
    '* セル背景／フォント処理
'    Set titleRange = Target.Offset(1, 0)
'    With titleRange
'        .Interior.Color = RGB(0, 112, 192)
'        .Value = "−"
'        .HorizontalAlignment = xlCenter
'        .VerticalAlignment = xlCenter
'        With .Font
'            .Bold = True
'            .Color = RGB(255, 255, 255)
'        End With
'    End With
        
    'Target.Offset(1, 0).Select
    
End Sub


'***********************************************
'* 背景データサブ画面表示
'***********************************************
Public Sub HaikeiFormDisp()
    
    Load BackDataForm
    
    BackDataForm.Show vbModeless
    
End Sub

