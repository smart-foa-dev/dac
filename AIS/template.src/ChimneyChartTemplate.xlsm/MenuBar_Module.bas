Attribute VB_Name = "MenuBar_Module"
'********************************
'* メニューバーの作成
'********************************
Public Sub MakeMenuBar()
    Dim objCB           As CommandBar
    Dim objCBCtrl       As CommandBarControl
    Dim objCBCtrlSub    As CommandBarControl
    
    Exit Sub
    
On Error Resume Next
    Application.CommandBars("AIS_ChimneyChart_Bar").Delete
    ThisWorkbook.Application.CommandBars.Add Name:="AIS_ChimneyChart_Bar", Position:=msoBarTop, MenuBar:=True
    Set objCB = ThisWorkbook.Application.CommandBars("AIS_ChimneyChart_Bar")
    objCB.Visible = True
    
''        '* 「最前面表示」を追加
''    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
''    With objCBCtrl
''        .Style = msoButtonIconAndCaption
''        .Caption = "最前面表示"   'メニュー名称
''        .OnAction = "OnTIme_Module.OnTimeStart"   '起動プログラム
''        .BeginGroup = False          '直前に区切り線有り
''        .FaceId = 1667                   'アイコン番号
''    End With
''
''        '* 「最前面表示停止」を追加
''    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
''    With objCBCtrl
''        .Style = msoButtonIconAndCaption
''        .Caption = "最前面表示停止"   'メニュー名称
''        .OnAction = "OnTIme_Module.OnTimeCancel"   '起動プログラム
''        .BeginGroup = False          '直前に区切り線有り
''        .FaceId = 1668               'アイコン番号
''    End With
    
    '* 「最大表示」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "最大表示"   'メニュー名称
        .OnAction = "DisplaySize_Module.ZoomDisplayFull"   '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 178               'アイコン番号
    End With
    
    '* 「表示（＋）」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIcon
        .Caption = "＋１０％）"   'メニュー名称
        .OnAction = "DisplaySize_Module.ZoomDisplayPlus"   '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 444               'アイコン番号
    End With
    
    '* 「表示（−）」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIcon
        .Caption = "−１０％"   'メニュー名称
        .OnAction = "DisplaySize_Module.ZoomDisplayMinus"   '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 445               'アイコン番号
    End With
    
    '* 「表示範囲登録」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "表示範囲設定"   'メニュー名称
        .OnAction = "DisplaySize_Module.ZoomSetDisplayRange"   '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 4359              'アイコン番号
    End With
    
    '* 「範囲設定解除」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "範囲設定解除"   'メニュー名称
        .OnAction = "DisplaySize_Module.ZoomUnSetDisplayRange"   '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 4272              'アイコン番号
    End With
    
    '* 「グラフ更新」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "グラフ更新"   'メニュー名称
'        .OnAction = "Main_Module.UpdateLoalData"      '起動ﾌﾟﾛｸﾞﾗﾑ
        .OnAction = "subScreen.frmGraphSetDisp"      '起動ﾌﾟﾛｸﾞﾗﾑ
        .BeginGroup = True         '直前に区切り線無し
        .FaceId = 435                 'アイコン番号
    End With
    
    '* 「テスト」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "テスト"   'メニュー名称
        .OnAction = "Main_Module.OperationTest"      '起動ﾌﾟﾛｸﾞﾗﾑ
        .BeginGroup = True         '直前に区切り線無し
        .FaceId = 7728                 'アイコン番号
    End With
    
    '* 「自動更新」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "自動更新"         'メニュー名称
        .OnAction = "Main_Module.AutoUpdateStart"       '起動プログラム
        .BeginGroup = True          '直前に区切り線有り
        .FaceId = 5597                'アイコン番号
    End With

    '* 「更新停止」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "更新停止"         'メニュー名称
        .OnAction = "Main_Module.AutoUpdateStop"       '起動プログラム
        .BeginGroup = False          '直前に区切り線有り
        .FaceId = 5597                'アイコン番号
        .Enabled = False
    End With
    
        ''「データアタッチ」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "データアタッチ"   'メニュー名称
        .OnAction = "subScreen.frmBackDataDisp"   '起動プログラム
        .BeginGroup = True          '直前に区切り線有り
        .FaceId = 2059               'アイコン番号
    End With
    
    
    '* 「登録」を追加
    Set objCBCtrl = objCB.Controls.Add(Type:=msoControlButton)
    With objCBCtrl
        .Style = msoButtonIconAndCaption
        .Caption = "登録"   'メニュー名称
        .OnAction = "Main_Module.SaveFileSub"   '起動プログラム
        .BeginGroup = True          '直前に区切り線有り
        .FaceId = 271               'アイコン番号
    End With
    
     
End Sub

''******************************
'' メニューの活性／非活性化処理
''******************************
Public Sub MenuCtrlEnableOrDisable(menuStr As String, flag As Boolean)
    Dim objCB           As CommandBar
    Dim objCBCtrl       As CommandBarControl
    Dim objCBCtrlSub    As CommandBarControl
    
    Exit Sub
    
    Set objCBCtrl = Application.CommandBars("AIS_ChimneyChart_Bar").Controls(menuStr)
    
    objCBCtrl.Enabled = flag
End Sub

''******************************************
''　EXCEL終了時の処理
''　　　　メニューバーの削除
''　削除しないと、他EXCELでも表示する為
''******************************************
Public Sub Auto_close()
    Debug.Print "Auto_close"
    
    '各種バー表示
    Call DispBar

    Application.DisplayAlerts = False
    
    Call ListDelete

    If quitFlag Then
        Debug.Print "SaveFileSubFromX"
        Call SaveFileSubFromX
    End If

On Error Resume Next
''    Application.CommandBars("AIS_StatusMonitor_Bar").Delete
    
    ThisWorkbook.Save
    Application.Quit

End Sub

''******************************************
''　EXCEL終了時(×マーク押下時)の処理
''　　　各種バーの表示
''******************************************
Public Sub DispBar()

    With ActiveWindow
        .DisplayHorizontalScrollBar = True  '水平スクロールバー
        .DisplayVerticalScrollBar = True  '垂直スクロールバー
        .DisplayWorkbookTabs = True  'シート見出し
'''        .DisplayFormulas = True  'セルが数式表示となる
        .DisplayWorkbookTabs = True
    End With

    With Application
        .DisplayFormulaBar = True  '数式バー
        .DisplayStatusBar = True    'ステータスバー
    End With
    
End Sub


