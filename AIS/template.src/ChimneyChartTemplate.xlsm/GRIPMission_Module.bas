Attribute VB_Name = "GRIPMission_Module"
'*******************************************
'*　GRIPミッション取込
'*******************************************
Public Function GetGripMission() As Integer

    Dim csvFile     As String
    Dim wrkStart    As String
    Dim wrkEnd      As String
    Dim dspTerm     As String
    Dim missionID   As String
    Dim strtTime    As Date
    Dim strFileName As String
    Dim wrkStr      As String
    
    Dim paramSheet  As Worksheet
    Dim srchRange   As Range
    Dim workRange   As Range
    Dim fistFlag    As Boolean
    
    Dim gripMission As GripDataRetriever.GripDataRetriever
    Dim IPAddr      As String
    Dim PortNo      As String
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    '20161027 Add
    fistFlag = True
    
    'GRIPミッション設定
    Set gripMission = New GripDataRetriever.GripDataRetriever
    
    With ThisWorkbook.Worksheets(ParamSheetName)
    
        '表示期間取得
        Set srchRange = ThisWorkbook.Worksheets(ParamSheetName).Range("A:A")
        Set workRange = srchRange.Find(What:="表示期間", LookAt:=xlWhole)
        If Not workRange Is Nothing Then
            dspTerm = workRange.Offset(0, 1).Value
        End If
    
        '*********************
        '* 収集開始日時を取得
        '*********************
        Set paramSheet = ThisWorkbook.Worksheets(ParamSheetName)
        wrkStart = GetCollectStartDT(paramSheet)
        
        '*********************
        '* 収集終了日時を取得
        '*********************
        wrkEnd = GetCollectEndDT(paramSheet)
        
        '*********************
        '* ミッションIDを取得
        '*********************
        missionID = ""
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "ミッションID項目が見つからないため処理を中断します。"
            GetGripMission = -1
            Exit Function
        End If
    
        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="GRIPサーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If

    End With
    
'    Set workRange = srchRange.Find(What:="問合開始日時", LookAt:=xlWhole)
'    If Not workRange Is Nothing Then
'        workRange.Offset(0, 1).Value = CStr(Format(strtTime, "YYYY/MM/DD hh:mm:ss"))
'    End If
'
'    Set workRange = srchRange.Find(What:="問合終了日時", LookAt:=xlWhole)
'    If Not workRange Is Nothing Then
'        workRange.Offset(0, 1).Value = CStr(Format(Now, "YYYY/MM/DD hh:mm:ss"))
'    End If

    
    'GRIPミッションの実行
    If useProxy = True Then
        csvFile = gripMission.GetGripDatatCsvFromProxy(useProxy, proxyUri, IPAddr, PortNo, missionID, wrkStart, wrkEnd)
    Else
        'GRIPミッションの実行
        '*****************************
        csvFile = gripMission.GetGripDatatCsv(IPAddr, PortNo, missionID, wrkStart, wrkEnd)
        '*****************************
    End If
    
    If csvFile = "" Then
        MsgBox "Gripミッション結果は有りません。", vbExclamation, "GetGripMission"
        GetGripMission = -1
        Exit Function
    End If
    
    ' フォルダの存在確認
    If Dir(csvFile, vbDirectory) = "" Then
        MsgBox "指定のフォルダは存在しません。", vbExclamation, "GetGripMission"
        GetGripMission = -1
        Exit Function
    End If

    ' 先頭のファイル名の取得
    strFileName = Dir(csvFile & "\*.*", vbNormal)
    ' ファイルが見つからなくなるまで繰り返す
    Do While strFileName <> ""
        'CSVﾌｧｲﾙをCELLに書込む
        wrkStr = Replace(strFileName, ".csv", "")
        
        If fistFlag = True Then
            Call CsvToCell(csvFile & "\" & strFileName, wrkStr)
        Else
            Call CsvToCellInsert(csvFile & "\" & strFileName, wrkStr)
        End If
        
        '* AISTEMP No.102 sunyi 2018/11/22 start
'        Call NullDelete(wrkStr)
        '* AISTEMP No.102 sunyi 2018/11/22 end
        strFileName = Dir()
    Loop
    
    GetGripMission = 0
    
End Function

Public Sub CsvToCell(argFileName As String, argSheetName As String) '
    
    Sheets(argSheetName).Cells.Clear

    With Sheets(argSheetName).QueryTables.Add(Connection:="TEXT;" & argFileName, Destination:=Sheets(argSheetName).Range("$A$1"))
'    With ActiveSheet.QueryTables.Add(Connection:= _
'        "TEXT;C:\Users\HelpMe\Desktop\GripClient_0612_2\grip_temp\2016-06-14-17-04-12-346\0_製品1生産実績_ルート1.csv", Destination:=Range("$A$1"))
        .Name = "temp"
        .FieldNames = True
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .RefreshOnFileOpen = False
        .RefreshStyle = xlOverwriteCells    'xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
        .AdjustColumnWidth = False  'True
        .RefreshPeriod = 0

        .TextFilePromptOnRefresh = False
        .TextFilePlatform = 65001 'UTF8  SHIF-JIS 932
        .TextFileStartRow = 1
        .TextFileParseType = xlDelimited
        .TextFileTextQualifier = xlTextQualifierDoubleQuote
        .TextFileConsecutiveDelimiter = False
        .TextFileTabDelimiter = False
        .TextFileSemicolonDelimiter = False
        .TextFileCommaDelimiter = True
        .TextFileSpaceDelimiter = False
        '.TextFileColumnDataTypes = Array(1, 1, 1)
        .TextFileTrailingMinusNumbers = True
        .Refresh BackgroundQuery:=False
        .Delete
    End With
    
    Sheets(argSheetName).Cells.Range("2:2").Clear
    Sheets(argSheetName).Cells.Range("3:3").Delete
    
End Sub


Public Sub CsvToCellInsert(argFileName As String, argSheetName As String)
    Dim intRow      As Variant
    Dim strRow      As String
    
    intRow = Sheets(argSheetName).Range("A3").End(xlDown).Row
    strRow = "A" & Trim(CStr(intRow + 1))
    
    With Sheets(argSheetName).QueryTables.Add(Connection:="TEXT;" & argFileName, Destination:=Sheets(argSheetName).Range(strRow))
'    With ActiveSheet.QueryTables.Add(Connection:= _
'        "TEXT;C:\Users\HelpMe\Desktop\GripClient_0612_2\grip_temp\2016-06-14-17-04-12-346\0_製品1生産実績_ルート1.csv", Destination:=Range("$A$1"))
        .Name = "temp"
        .FieldNames = True
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .RefreshOnFileOpen = False
        .RefreshStyle = xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
        .AdjustColumnWidth = False  'True
        .RefreshPeriod = 0

        .TextFilePromptOnRefresh = False
        .TextFilePlatform = 65001 'UTF8  SHIF-JIS 932
        .TextFileStartRow = 1
        .TextFileParseType = xlDelimited
        .TextFileTextQualifier = xlTextQualifierDoubleQuote
        .TextFileConsecutiveDelimiter = False
        .TextFileTabDelimiter = False
        .TextFileSemicolonDelimiter = False
        .TextFileCommaDelimiter = True
        .TextFileSpaceDelimiter = False
        '.TextFileColumnDataTypes = Array(1, 1, 1)
        .TextFileTrailingMinusNumbers = True
        .Refresh BackgroundQuery:=False
        .Delete
    End With
    
    Sheets(argSheetName).Rows(intRow + 3).Delete
    Sheets(argSheetName).Rows(intRow + 2).Delete
    Sheets(argSheetName).Rows(intRow + 1).Delete
    
End Sub



'開始時刻の行削除
Public Sub NullDelete(argSheetName As String)
    Dim pos         As Variant
    Dim wrkVal      As Variant
    
    wrkVal = Worksheets(argSheetName).Range("A65536").End(xlUp).Row
    For pos = wrkVal To 3 Step -1
        If Worksheets(argSheetName).Cells(pos, 2).Value = "" Then
            Worksheets(argSheetName).Rows(pos).Delete

        End If
    Next pos

End Sub


