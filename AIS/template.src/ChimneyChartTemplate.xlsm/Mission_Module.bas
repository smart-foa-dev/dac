Attribute VB_Name = "Mission_Module"
Option Explicit


'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する(追記用）
'****************************************************
'FOR COM高速化 lm 2018/06/13 Modify start
'Public Function GetMissionInfoAdd() As Integer
Public Function GetMissionInfoAdd(sType As String) As Integer
'FOR COM高速化 lm 2018/06/13 Modify End
    Dim ctmList()   As CTMINFO      ' CTM情報
    Dim II          As Integer
    Dim ctmFile     As String
    Dim ctmFiles    As Object
    Dim jsonParse As New FoaCoreCom.jsonParse
    
    'FOR COM高速化 lm 2018/06/13 Modify start
    'Set ctmFiles = GetMissionInfo(ctmList)
     Set ctmFiles = GetMissionInfo(ctmList, sType)
    'FOR COM高速化 lm 2018/06/13 Modify end
    
    '* シート毎（シート名＝CTM名）へ出力
    If ctmFiles.count > 0 Then
    
        '* CTM種別でシートに出力
        For II = 0 To UBound(ctmList)
            ctmFile = ctmFiles(ctmList(II).ID)
            If ctmFile <> "" Then
                Call AddCTMInfoToSheet(ctmList(II).Name, ctmFile)
            End If
        Next
        
        '* ISSUE_NO.702 sunyi 2018/05/18 start
        '* ""の場合、処理しない
        'Call jsonParse.removeCsvFiles(ctmFile)
        If ctmFile <> "" Then
            Call jsonParse.removeCsvFiles(ctmFile)
        End If
        '* ISSUE_NO.702 sunyi 2018/05/18 end
    Else
        GetMissionInfoAdd = -1
        Exit Function
    End If
    
    GetMissionInfoAdd = 0
    
End Function

'****************************************************
'* ミッションから情報を取得しCTM毎のシートへ出力する
'****************************************************
'FOR COM高速化 lm 2018/06/13 Modify  start
' Public Function GetMissionInfoAll() As Integer
Public Function GetMissionInfoAll(sType As String)
'FOR COM高速化 lm 2018/06/13 Modify  end
    Dim ctmList()   As CTMINFO      ' CTM情報
    Dim II          As Integer
    Dim ctmFile     As String
    Dim ctmFiles    As Object
    Dim jsonParse As New FoaCoreCom.jsonParse
    
    'FOR COM高速化 lm 2018/06/13 Modify  start
    'Set ctmFiles = GetMissionInfo(ctmList)
    Set ctmFiles = GetMissionInfo(ctmList, sType)
    'FOR COM高速化 lm 2018/06/13 Modify  end

    '* シート毎（シート名＝CTM名）へ出力
    If ctmFiles.count > 0 Then
    
        '* CTM種別でシートに出力
        For II = 0 To UBound(ctmList)
            ctmFile = ctmFiles(ctmList(II).ID)
            Call OutputCTMInfoToSheet(ctmList(II).Name, ctmFile)
        Next
        
        '* ISSUE_NO.702 sunyi 2018/05/18 start
        '* ""の場合、処理しない
        'Call jsonParse.removeCsvFiles(ctmFile)
        If ctmFile <> "" Then
            Call jsonParse.removeCsvFiles(ctmFile)
        End If
        '* ISSUE_NO.702 sunyi 2018/05/18 end
    
    End If
    
    GetMissionInfoAll = 0
    
End Function
'FOR COM高速化 lm 2018/06/13 Modify  end

'****************************************************************
'* 指定ミッションの情報を取得する
'* ※ParamシートにミッションID、CTMエレメント一覧が必要
'****************************************************************
'FOR COM高速化 lm 2018/06/13 Modify  start
'Private Function GetMissionInfo(ctmList() As CTMINFO) As Object
Private Function GetMissionInfo(ctmList() As CTMINFO, sType As String) As Object
'FOR COM高速化 lm 2018/06/13 Modify  end
    Dim sc          As Object       ' JSON形式の情報取得用
    Dim objJSON     As Object       ' JSON形式の情報取得用
    Dim httpObj     As Object
    
    Dim IPAddr      As String
    Dim PortNo      As String
    Dim missionID   As String
    Dim ctmID       As String
    Dim startTime   As Double
    Dim endTime     As Double
    Dim SendUrlStr  As String
    Dim SendString  As String
    Dim GetString   As String
    Dim srchRange   As Range
    Dim startD      As Date
    Dim startT      As Date
    Dim endD        As Date
    Dim endT        As Date
    Dim startDT     As Date
    Dim endDT       As Date
    Dim dispTerm    As Double
    Dim dispTermUnit    As String
    Dim getStartTime    As Double
    Dim getEndTime      As Double
    Dim iResult         As Integer
    Dim paramWorksheet  As Worksheet
    Dim bPmary      As Variant
    Dim II          As Integer
    Dim currCTM     As CTMINFO
    
    Dim cmsVersion  As String
    Dim mfIPAddr    As String
    Dim mfPortNo    As String
    Dim chgStr01    As String
    Dim chgStr02    As String
    Dim keyOrder()  As String
    Dim ctmEmptyFiles   As New Dictionary
    Dim ctmFiles    As New Dictionary
    Dim jsonParse   As New FoaCoreCom.jsonParse
    
    Dim useProxy As Boolean
    Dim proxyUri As String
    
    'FOR COM高速化 lm 2018/06/13 Add  start
    Dim ctmData As FoaCoreCom.CtmDataRetriever
    Dim csvWrite As FoaCoreCom.CsvWriteToExcelHandler
    Dim zipFile As FoaCoreCom.ZipFileHandler
    
    Dim zipUrl As String
    Dim unZipUrl As String
    Dim msg As String
    Dim test As Boolean
    'FOR COM高速化 lm 2018/06/13 Add  end
    
    On Error GoTo errhandle
    
    Set paramWorksheet = Workbooks(thisBookName).Worksheets(ParamSheetName)
    
    '* メインシートから各種情報を取得
    With paramWorksheet
    
        '*********************
        '* 収集開始日時を取得
        '*********************
       'FOR COM高速化 lm 2018/06/13 Modify Start
        'startTime = GetCollectStartDTstd(paramWorksheet)
        Set srchRange = .Cells.Find(What:="取得開始", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            startD = DateValue(Format(srchRange.Offset(0, 1).Value, "yyyy/m/d"))
            startT = TimeValue(Format(srchRange.Offset(0, 1).Value, "hh:mm:ss"))
        Else
            startD = DateValue("2016/1/1 00:00:00")
            startT = TimeValue("2016/1/1 00:00:00")
        End If
        If Not IsDate(startD + startT) Then
            MsgBox "収集開始日時欄が正しくないため処理を中断します。"
            Set GetMissionInfo = ctmEmptyFiles
            Exit Function
        End If
        
        startTime = GetUnixTime(startD + startT)
        'FOR COM高速化 lm 2018/06/13 Modify End
        
        '*********************
        '* 収集終了日時を取得
        '*********************
        endTime = GetCollectEndDT(paramWorksheet)
        
        '*********************
        '* ミッションIDを取得
        '*********************
        Set srchRange = .Cells.Find(What:="ミッションID", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            missionID = srchRange.Offset(0, 1).Value
        Else
            MsgBox "ミッションID項目が見つからないため処理を中断します。"
            Set GetMissionInfo = ctmEmptyFiles
            Exit Function
        End If

        '*******************************
        '* IPアドレスおよびPortNoを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="サーバIPアドレス", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            IPAddr = srchRange.Offset(0, 1).Value
            PortNo = srchRange.Offset(1, 1).Value
        Else
            IPAddr = "localhost"
            PortNo = "60000"
        End If

        ' 2016.07.20 Add *********************
        '* CMSバージョンを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="CmsVersion", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            cmsVersion = srchRange.Offset(0, 1).Value
        Else
            cmsVersion = "V5"
        End If
        
        ' 2016.07.20 Add *********************
        '* 旧バージョン用IPアドレスおよびPortNoを取得
        ' 2016.07.20 Add *********************
        Set srchRange = .Cells.Find(What:="MfHost", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            mfIPAddr = srchRange.Offset(0, 1).Value
            mfPortNo = srchRange.Offset(1, 1).Value
        Else
            mfIPAddr = "localhost"
            mfPortNo = "50031"
        End If
        
        '*******************************
        '* Proxyの使用可否とProxyのURIを取得
        '*******************************
        Set srchRange = .Cells.Find(What:="ProxyServer使用", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            useProxy = srchRange.Offset(0, 1).Value
            proxyUri = srchRange.Offset(1, 1).Value
        Else
            useProxy = False
            proxyUri = ""
        End If
    End With
    
    '* CTM情報一覧をシートから取得
    Call GetCTMList(ParamSheetName, ctmList)
    Call GetKeyOrder(ParamSheetName, keyOrder)
    
    '*******************************
    '* プログラムミッションに問合せ
    '*******************************
    '* ミッションIDがあれば
    If missionID <> "" Then
        getEndTime = endTime
        getStartTime = startTime
        
        'FOR COM高速化 lm 2018/06/13 Modify  start
        'SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pm?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & "&limit=" & Format(LimitCTMNumber) & "&lang=ja&noBulky=true"
        SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/mission/pmzip?id=" & missionID & "&start=" & Format(getStartTime) & "&end=" & Format(getEndTime) & "&limit=" & Format(LimitCTMNumber) & "&lang=ja&noBulky=true"
       
        
'        If useProxy = False Then
'            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'        Else
'            Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
'            httpObj.setProxy 2, proxyUri
'        End If
'
'        httpObj.Open "GET", SendUrlStr, False
'        httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***
'        httpObj.setTimeouts 60000, 60000, 30000, 30000
'        httpObj.send
'
'        '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
'        If InStr(GetString, "MISSION_NOT_FOUND") > 0 Then
'            MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
'
'            '周期起動停止
'            'Call ChangeStartStopBT
'            Set GetMissionInfo = ctmEmptyFiles
'            Exit Function
'        End If
'        '**********
'
'        '* 取得情報のデコード
'        GetString = httpObj.responseText
'        Set GetMissionInfo = jsonParse.buildEachCtmCsvFileCtmMission(GetString, GetTimezoneId, False, keyOrder)
'        Set httpObj = Nothing

        ' thisBookNameへカレントExcel対象をセット
        Call SetFilenameToVariable

        Set ctmData = CreateObject("FoaCoreCom.ais.retriever.CtmDataRetriever")
        Set csvWrite = CreateObject("FoaCoreCom.ais.retriever.CsvWriteToExcelHandler")
        Set zipFile = CreateObject("FoaCoreCom.ais.retriever.ZipFileHandler")
        msg = ""
        
        '情報を格納するZIPファイルパスを取得する
        zipUrl = ctmData.GetProgramMissionZipFileAsync(SendUrlStr, proxyUri, msg)
        
        '(2016/08/22)**********　ミッション未取得の場合、オンライン停止
        If InStr(msg, "MISSION_NOT_FOUND") > 0 Then
            MsgBox "ミッションを取込めません。IPｱﾄﾞﾚｽの不備が考えられます。　IP:" & SendUrlStr
            
            '周期起動停止
            'Call ChangeStartStopBT
            '失敗する場合
            Set GetMissionInfo = ctmEmptyFiles
            Exit Function
        End If
        '**********
        
        ' Zipファイルを解凍する
        unZipUrl = zipFile.unZipFile(zipUrl)
              
        ' excelへ書き込む
        Call csvWrite.ReadAllCtmInfoToExcel(Workbooks(thisBookName), unZipUrl, MaxCntImport, sType, GetTimezoneId(), ParamSheetName)
        
        '一時フォルダを削除
        zipFile.DeleteFolder (unZipUrl)
        
        '成功する場合
        Set GetMissionInfo = ctmFiles
        'FOR COM高速化 lm 2018/06/13 Modify  end
            
    '* ミッションIDがなければ
    Else
    
        ReDim ctmValue(0)
        ctmValue(0).ID = ""
        For II = 0 To UBound(ctmList)
    
            currCTM = ctmList(II)
            ctmID = currCTM.ID
        
            getEndTime = endTime
            getStartTime = startTime
            
            SendUrlStr = "http://" & IPAddr & ":" & PortNo & "/cms/rest/mib/ctm/find?testing=0"
            
            If useProxy = False Then
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
            Else
                Set httpObj = CreateObject("MSXML2.ServerXMLHTTP.6.0")
                httpObj.setProxy 2, proxyUri
            End If
            
            httpObj.Open "POST", SendUrlStr, False
            httpObj.setRequestHeader "Content-Type", "text/plain"
            httpObj.setRequestHeader "If-Modified-Since", "Thu, 01 Jun 1970 00:00:00 GMT"           '*** これがないとブラウザのキャッシュを読んでしまう ***Content-Type: text/plain
            httpObj.setTimeouts 60000, 60000, 30000, 30000
            
            ' 2016.07.20 Change ***************************
            If cmsVersion = "3.5" Then
                SendString = "{""ctmId"":""" & ctmID & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & ",""mfHostName"":" & """" & mfIPAddr & """" & ",""mfPort"":" & """" & mfPortNo & """" & "}"
            Else
                SendString = "{""ctmId"":""" & ctmID & """,""start"":" & Format(getStartTime) & ",""end"":" & Format(getEndTime) & "}"
            End If
            ' 2016.07.20 Change ***************************
            
            bPmary = StrConv(SendString, vbFromUnicode)
        
            ' 2016.07.20 Change *********************
'            Call httpObj.send(SendString)
            httpObj.send (SendString)
            
           
            ' 2016.07.20 Add ***************************
            If cmsVersion = "3.5" Then
                chgStr01 = Replace(GetString, "\""", "@@")
                chgStr02 = Replace(chgStr01, """", "")
                GetString = Replace(chgStr02, "@@", """")
            End If
            ' 2016.07.20 Add ***************************
            
            '* 取得情報のデコード
            GetString = httpObj.responseText
            ctmFiles.Add ctmID, jsonParse.buildEachCtmCsvFileCtmMission(GetString, GetTimezoneId, False, keyOrder)(ctmID)
            Set httpObj = Nothing
        
        Next
        '成功する場合
        Set GetMissionInfo = ctmFiles
        
    End If
    
    Set httpObj = Nothing
    Exit Function
    
errhandle:
    Set httpObj = Nothing
    Set GetMissionInfo = ctmEmptyFiles
End Function

'**********************
'* 収集開始日時の取得
'* →返り値はUNIX TIME
'**********************
Public Function GetCollectStartDT(currWorksheet As Worksheet) As Double
    Dim startD              As Date
    Dim startT              As Date
    Dim srchRange           As Range
    Dim onlineDiv           As String
    
    '****************************************
    '* paramシートから収集開始日時を取得する
    '****************************************
    With currWorksheet
        Set srchRange = .Cells.Find(What:="オンライン", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            onlineDiv = srchRange.Offset(0, 1).Value
        Else
            MsgBox "オンライン欄が正しくないため処理を中断します。"
        End If
        
        If onlineDiv = "OFFLINE" Then
            Set srchRange = .Cells.Find(What:="表示開始期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                startD = DateValue(Format(srchRange.Offset(0, 1).Value, "yyyy/m/d"))
                startT = TimeValue(Format(srchRange.Offset(0, 1).Value, "hh:mm:ss"))
            Else
                startD = DateValue("2016/1/1 00:00:00")
                startT = TimeValue("2016/1/1 00:00:00")
            End If
            If Not IsDate(startD + startT) Then
                MsgBox "表示開始期間欄が正しくないため処理を中断します。"
                GetCollectStartDT = -1
                Exit Function
            End If
            GetCollectStartDT = GetUnixTime(startD + startT)
            Exit Function
        
        Else
            Set srchRange = .Cells.Find(What:="取得開始", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                startD = DateValue(Format(srchRange.Offset(0, 1).Value, "yyyy/m/d"))
                startT = TimeValue(Format(srchRange.Offset(0, 1).Value, "hh:mm:ss"))
            Else
                startD = DateValue("2016/1/1 00:00:00")
                startT = TimeValue("2016/1/1 00:00:00")
            End If
            If Not IsDate(startD + startT) Then
                MsgBox "収集開始日時欄が正しくないため処理を中断します。"
                GetCollectStartDT = -1
                Exit Function
            End If
            GetCollectStartDT = GetUnixTime(startD + startT)
        End If
    End With
    
End Function


'**********************
'* 収集終了日時の取得
'* →返り値はUNIX TIME
'**********************
Public Function GetCollectEndDT(currWorksheet As Worksheet) As Double
    Dim endD                As Date
    Dim endT                As Date
    Dim srchRange           As Range
    Dim onlineDiv           As String
    
    '****************************************
    '* paramシートから収集終了日時を取得する
    '****************************************
    With currWorksheet
        Set srchRange = .Cells.Find(What:="オンライン", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            onlineDiv = srchRange.Offset(0, 1).Value
        Else
            MsgBox "オンライン欄が正しくないため処理を中断します。"
        End If
        
        If onlineDiv = "OFFLINE" Then
            Set srchRange = .Cells.Find(What:="表示終了期間", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                endD = DateValue(Format(srchRange.Offset(0, 1).Value, "yyyy/m/d"))
                endT = TimeValue(Format(srchRange.Offset(0, 1).Value, "hh:mm:ss"))
            Else
                endD = DateValue("2016/1/1 00:00:00")
                endT = TimeValue("2016/1/1 00:00:00")
            End If
            If Not IsDate(endD + endT) Then
                MsgBox "表示終了期間欄が正しくないため処理を中断します。"
                GetCollectEndDT = -1
                Exit Function
            End If
            GetCollectEndDT = GetUnixTime(endD + endT)
        Else
            Set srchRange = .Cells.Find(What:="取得終了", LookAt:=xlWhole)
            If Not srchRange Is Nothing Then
                endD = DateValue(Format(srchRange.Offset(0, 1).Value, "yyyy/m/d"))
                endT = TimeValue(Format(srchRange.Offset(0, 1).Value, "hh:mm:ss"))
            Else
                endD = DateValue("2016/1/1 00:00:00")
                endT = TimeValue("2016/1/1 00:00:00")
            End If
            If Not IsDate(endD + endT) Then
                MsgBox "収集終了日時欄が正しくないため処理を中断します。"
                GetCollectEndDT = -1
                Exit Function
            End If
            GetCollectEndDT = GetUnixTime(endD + endT)
        End If
    End With
    
End Function



'**********************
'* 収集開始日時の取得
'* →返り値はUNIX TIME
'**********************
Private Function GetCollectStartDTstd(currWorksheet As Worksheet) As Double
    Dim srcWorksheet        As Worksheet
    Dim startTime           As Double
    Dim startD              As Date
    Dim startT              As Date
    Dim ctmList()           As CTMINFO
    Dim II                  As Integer
    Dim recieveTimeArray()  As Double
    Dim iFirstFlag          As Boolean
    Dim recieveTime         As Variant
    Dim startRange          As Range
    Dim srchRange           As Range
    Dim rtIndex             As Integer
    Dim onlineDiv           As String
    
    '****************************************
    '* paramシートから収集開始日時を取得する
    '****************************************
    With currWorksheet
        Set srchRange = .Cells.Find(What:="オンライン", LookAt:=xlWhole)
        If Not srchRange Is Nothing Then
            onlineDiv = srchRange.Offset(0, 1).Value
        Else
            MsgBox "オンライン欄が正しくないため処理を中断します。"
        End If
        
        If onlineDiv = "OFFLINE" Then
            GetCollectStartDTstd = GetCollectStartDT(currWorksheet)
            Exit Function
        End If
    
        startTime = GetCollectStartDTstd = GetCollectStartDT(currWorksheet)

    End With
    
    '**********************************
    '* 全CTMシートの最新時刻を取得する
    '**********************************
    '* CTMリストを取得する
    Call GetCTMList(ParamSheetName, ctmList)
    
    
    '* CTM情報一覧をシートから取得
    iFirstFlag = True
    For Each srcWorksheet In Worksheets
    
        '* CTM情報一覧に存在するシートのみを処理対象とする
        For II = 0 To UBound(ctmList)
            If (ctmList(II).Name = srcWorksheet.Name) Then Exit For
        Next
        If II <= UBound(ctmList) Then

            '* 最上位のRECIEVE TIMEを取得する
            Set startRange = srcWorksheet.Range("A4")
            For II = 0 To 100
                recieveTime = startRange.Offset(II, 0).Value
                If recieveTime <> "" Then
                    Exit For
                End If
            Next
            '* 100行検索しても見つからなければ処理中断＝データ無
            If II > 100 Then
                Exit For
            End If
            
            '* 取得した値が日付形式ならば
            If IsDate(recieveTime) Then
            
                If iFirstFlag Then
                    rtIndex = 0
                    ReDim recieveTimeArray(rtIndex)
                    iFirstFlag = False
                Else
                    rtIndex = UBound(recieveTimeArray) + 1
                    ReDim Preserve recieveTimeArray(rtIndex)
                End If
                
                recieveTimeArray(rtIndex) = GetUnixTime(CDate(recieveTime))
                
            Else
                Exit For
            End If

        End If
    
    Next
        
    If iFirstFlag Then
        recieveTime = 0#
    Else
        '* 取得した時刻群の最小の時刻を取得
        recieveTime = Application.WorksheetFunction.Min(recieveTimeArray)
    End If

    GetCollectStartDTstd = Application.WorksheetFunction.Max(recieveTime, startTime)

End Function

'***********************************************
'* キー順を取得する
'***********************************************
Private Sub GetKeyOrder(currSheetName As String, keyOrder() As String)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim ctmRange        As Range
    Dim elmRange        As Range
    Dim currWorksheet   As Worksheet
    Dim ctmIndex        As Integer      ' CTM情報インデックス
    Dim elmIndex        As Integer      ' CTMID,エレメント情報インデックス
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    Dim maxRow          As Integer
    Dim maxCol          As Integer
    
    Set currWorksheet = Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim keyOrder(0)
    
    With currWorksheet
    
        '* CTM名称の項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        With srchRange.SpecialCells(xlCellTypeLastCell)
            maxRow = .Row - srchRange.Row + 1
            maxCol = .Column - srchRange.Column + 1
        End With
        
        ctmIndex = 0
        ReDim keyOrder(maxRow / 2, maxCol)
        
        For II = 0 To maxRow Step 2
            Set ctmRange = startRange.Offset(II, 0)
            elmIndex = 0
            
            If ctmRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            '* CTM情報取得
            keyOrder(ctmIndex, elmIndex) = ctmRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = ctmRange.Offset(0, 1)
            For JJ = 0 To maxCol - 1
                elmIndex = elmIndex + 1
                keyOrder(ctmIndex, elmIndex) = elmRange.Offset(1, JJ).Value
            Next
            ctmIndex = ctmIndex + 1
        Next
    End With
    
End Sub


'***********************************************
'* ミッションから取得した情報をシートへ出力する（追記用）
'***********************************************
Private Sub AddCTMInfoToSheet(currSheetName As String, ctmValueFile As String)
    Dim currWorksheet   As Worksheet
    Dim prevWorksheet   As Worksheet
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim delRange        As Range
    Dim dataRange       As Range
    Dim jsonParse As New FoaCoreCom.jsonParse
    
    Set prevWorksheet = ActiveSheet
        
    '* 出力先シートの取得
    Set currWorksheet = Worksheets(currSheetName)
    currWorksheet.Activate
    
    With currWorksheet
        
        
        Dim FileSize As Long
        FileSize = FileLen(ctmValueFile)

        If FileSize > 0 Then
            '* 書き出し位置の設定
            Set startRange = .Cells(4, 1)
            Set startRange = startRange.End(xlDown)
            Set startRange = startRange.Offset(1, 0)
                    
            With ActiveSheet.QueryTables.Add(Connection:="Text;" + ctmValueFile, Destination:=startRange)
                .TextFileCommaDelimiter = True
                .TextFilePlatform = 65001
                .Refresh BackgroundQuery:=False
                .Delete
            End With
            
            Set lastRange = startRange.End(xlDown)
            Set dataRange = .Range("4:" & Format(lastRange.Row))
            dataRange.Sort key1:=Range("A4"), order1:=xlDescending, Header:=xlNo
        End If
        
       
    End With
    
    prevWorksheet.Activate
End Sub

'***********************************************
'* ミッションから取得した情報をシートへ出力する
'***********************************************
Private Sub OutputCTMInfoToSheet(currSheetName As String, ctmValueFile As String)
    Dim currWorksheet   As Worksheet
    Dim prevWorksheet   As Worksheet
    Dim startRange      As Range
    Dim lastRange       As Range
    Dim delRange        As Range
    Dim dataRange       As Range
    Dim jsonParse As New FoaCoreCom.jsonParse
    
    Set prevWorksheet = ActiveSheet
        
    '* 出力先シートの取得
    Set currWorksheet = Worksheets(currSheetName)
    currWorksheet.Activate
    
    With currWorksheet
        
        '* 書き出し位置の設定
        Set startRange = .Cells(4, 1)
        
        '* 全情報クリア
        Set lastRange = startRange.End(xlDown)
        Set delRange = .Range("4:" & Format(lastRange.Row))
        delRange.Delete
        
        
        Dim FileSize As Long
        FileSize = FileLen(ctmValueFile)

        If FileSize > 0 Then
            '* 書き出し位置の設定
            Set startRange = .Cells(4, 1)
                    
            With ActiveSheet.QueryTables.Add(Connection:="Text;" + ctmValueFile, Destination:=Range("A4"))
                .TextFileCommaDelimiter = True
                .TextFilePlatform = 65001
                .Refresh BackgroundQuery:=False
                .Delete
            End With
            
            Set lastRange = startRange.End(xlDown)
            Set dataRange = .Range("4:" & Format(lastRange.Row))
            dataRange.Sort key1:=Range("A4"), order1:=xlDescending, Header:=xlNo
        End If
        
       
    End With
    
    prevWorksheet.Activate
End Sub

'***********************************************
'* メインシートにあるミッション情報を取得する
'***********************************************
Public Sub GetCTMList(currSheetName As String, ctmList() As CTMINFO)
    Dim srchRange       As Range
    Dim startRange      As Range
    Dim ctmRange        As Range
    Dim elmRange        As Range
    Dim workRange       As Range
    Dim currWorksheet   As Worksheet
    Dim aryIndex        As Integer      ' CTM情報インデックス
    Dim aryIndex2       As Integer      ' エレメント情報インデックス
    Dim iFirstFlag      As Boolean      ' 配列初回フラグ
    Dim II              As Integer      ' For文用カウンタ
    Dim JJ              As Integer      ' For文用カウンタ
    
    iFirstFlag = True
    
    Set currWorksheet = Worksheets(currSheetName)
    
    '* 配列を初期化
    ReDim ctmList(0)
    
    With currWorksheet
    
        '* CTM名称の項目セルを着目シート全体から検索する
        Set srchRange = .Cells.Find(What:="CTM NAME", LookAt:=xlWhole)
        Set startRange = srchRange.Offset(1, 0)
        
        For II = 0 To MaxCTMNumber Step 2
            Set ctmRange = startRange.Offset(II, 0)
            
            If ctmRange.Offset(0, 0).Value = "" Then
                Exit For
            End If
            
            If iFirstFlag Then
                aryIndex = 0
                ReDim ctmList(aryIndex)
                iFirstFlag = False
            Else
                aryIndex = UBound(ctmList) + 1
                ReDim Preserve ctmList(aryIndex)
            End If
            ReDim ctmList(aryIndex).Element(0)
            ctmList(aryIndex).Element(0).Name = ""
            
            '* CTM情報取得
            ctmList(aryIndex).Name = ctmRange.Offset(0, 0).Value
            ctmList(aryIndex).ID = ctmRange.Offset(1, 0).Value
            
            '* エレメント情報取得
            Set elmRange = ctmRange.Offset(0, 1)
            For JJ = 0 To MaxELMNumber
                If elmRange.Offset(0, JJ).Value = "" Then
                    Exit For
                End If
                
                aryIndex2 = UBound(ctmList(aryIndex).Element)
                If ctmList(aryIndex).Element(aryIndex2).Name <> "" Then
                    aryIndex2 = aryIndex2 + 1
                    ReDim Preserve ctmList(aryIndex).Element(aryIndex2)
                End If
                ctmList(aryIndex).Element(aryIndex2).Name = elmRange.Offset(0, JJ).Value
                ctmList(aryIndex).Element(aryIndex2).ID = elmRange.Offset(1, JJ).Value
            Next
            
                
        Next
    
    End With
    
End Sub

