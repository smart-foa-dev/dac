Attribute VB_Name = "sheetFind"
'*****************************************
'シート名検索
'  argCTm：CTM名称
'  argElement：エレメント名称
'　戻り値：シート名(該当なしの場合は、NULL)
'*****************************************
Public Function sheetFind(argCtm As String, argElement As String) As String
    Dim ws                  As Worksheet
    Dim ctmRange      As Range
    Dim eleRange       As Range
    
    sheetFind = ""
    
    'ワークシート分繰返す
    For Each ws In Worksheets
        'CTM名称の検索
        Set ctmRange = Worksheets(ws.Name).Cells.Find(What:=argCtm, LookAt:=xlWhole)
        If Not ctmRange Is Nothing Then
            'エレメント名称の検索
            Set eleRange = Worksheets(ws.Name).Cells.Find(What:=argElement, LookAt:=xlWhole)
            If Not eleRange Is Nothing Then
                If ws.Name <> GraphEditSheetName _
                    And ws.Name <> FixGraphSheetName _
                    And ws.Name <> ParamSheetName _
                    And ws.Name <> RoParamSheetName _
                    And ws.Name <> ForPivotSheetName _
                    And ws.Name <> PivotSheetName _
                Then
                    'シート名の決定
                    sheetFind = ws.Name
                End If
            End If
        End If
    Next
End Function


