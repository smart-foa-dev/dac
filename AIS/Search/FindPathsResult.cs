﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Search
{
    class FindPathsResult
    {
        /// <summary>
        /// ドメイン間検索か. ドメイン間検索の場合は true.
        /// </summary>
        public bool crossDomain = false;

        public List<FindResultPath> paths;

        /// <summary>
        /// &lt;NodeId/LinkId, CrossDomainSearchInfo&gt;
        /// </summary>
        //public Dictionary<string, CrossDomainSearchInfo> domainInfo = new Dictionary<string, CrossDomainSearchInfo>();
    }
    public class FindResultPath
    {
        public bool forward;
        public List<FindResultNode> nodes;
    }

    public class FindResultNode
    {
        public string id;
        public string ctmId;
        public string link;
    }
}
