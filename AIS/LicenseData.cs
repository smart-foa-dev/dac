﻿
namespace DAC.Model
{
    public class LicenseData
    {
        public string Classification { get; set; }
        public string Item { get; set; }
        public string Limit { get; set; }
    }
}
