﻿using DAC.AExcel;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using FoaCore.Common.Util;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpreadsheetGear;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_StreamGraphTemplate.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_StreamGraphTemplate : BaseControl
    {
        private const string EXCEL_MACRO = "CallBeforeExcelFromAIS";

        public long StartUnix { get; set; }
        public long EndUnix { get; set; }

        private bool isFirstFile = true;
        private string excelFilePath = string.Empty;

        private string CtmId { get; set; }
        private string ElementId { get; set; }

        private string CtmName { get; set; }
        private string maxCtmName { get; set; }
        private string maxCtmId { get; set; }

        private Dictionary<string, string> conditionDictionary = new Dictionary<string, string>();
        private Dictionary<string, string> paramDictionary = new Dictionary<string, string>();

        private Dictionary<Mission, List<CtmObject>> missionCtmDict = new Dictionary<Mission, List<CtmObject>>();
        private Dictionary<CtmObject, List<CtmElement>> missionCtmElementDict = new Dictionary<CtmObject, List<CtmElement>>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="isFirstFile"></param>
        /// <param name="filePath"></param>
        public UserControl_StreamGraphTemplate(bool isFirstFile = true, string filePath = "")
        {
            InitializeComponent();

            this.Mode = ControlMode.StreamGraph;
            this.TEMPLATE = "StreamTemplate.xlsm";

            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            // 画像読み込み
            string fileName = "StreamGraph.png";
            if (AisConf.UiLang == "en")
            {
                fileName = "en-StreamGraph.png";
            }

            string uri = string.Format(@"/DAC;component/Resources/Image/{0}", fileName);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(uri, UriKind.RelativeOrAbsolute);
            bi.EndInit();
            this.image_Thumbnail.Source = bi;
            this.mainWindow = (MainWindow)System.Windows.Application.Current.MainWindow;

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }

            this.dataGrid_Bulky.Init(this);
            this.dataGrid_Bulky.GetButton = this.button_Get;
            this.dataGrid_Bulky.OpenImage = this.image_OpenExcel;
            this.dataGrid_Bulky.CancelImage = this.image_CancelToGet;
            this.dataGrid_Bulky.GageImage = this.image_Gage;
            this.dataGrid_Bulky.Start = this.dateTimePicker_Start;
            this.dataGrid_Bulky.End = this.dateTimePicker_End;

            this.StartUnix = UnixTime.ToLong(this.dateTimePicker_Start.Value);
            this.EndUnix = UnixTime.ToLong(this.dateTimePicker_End.Value);

            lblExcelFile.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// 画面LOAD時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();

            // No.568 Disable get button.
            this.button_Get.IsEnabled = false;

            //言語が英語の場合の表示調整
            string cultureName = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;
//            int fontsize = 12;
            if (cultureName == "en-US")
            {
                ////RadioButton_Horizontal
                //this.radioButton_Horizontal.FontSize = fontsize;

                ////RadioButton_Vertical
                //this.radioButton_Vertical.FontSize = fontsize;
                //Canvas.SetLeft(this.radioButton_Vertical, -55);
                //Canvas.SetTop(this.radioButton_Vertical, 106);

                //Button_SelectAllColumn
                Canvas.SetLeft(this.button_SelectAllColumn, 223);

                //Button_SelectAllRow
                //this.button_SelectAllRow.Visibility = Visibility.Hidden;
                this.button_SelectAllRow_en.Visibility = Visibility.Visible;
                Canvas.SetTop(this.button_SelectAllRow_en, 200);
            }

            BitmapImage biCancel = new BitmapImage();
            biCancel.BeginInit();
            biCancel.UriSource = new Uri(@"/DAC;component/Resources/Image/cancel-to-get.png", UriKind.RelativeOrAbsolute);
            biCancel.EndInit();
            this.image_CancelToGet.Source = biCancel;
            this.image_CancelToGet.IsEnabled = false;

            BitmapImage biGage = new BitmapImage();
            biGage.BeginInit();
            biGage.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar-empty.png", UriKind.RelativeOrAbsolute);
            biGage.EndInit();
            this.image_Gage.Source = biGage;

            this.editControl.StreamGraph = this;
            this.image_OpenExcel.Deactivate();

            // No.479 In the case of Graph template screen, Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;

            if (!this.isFirstFile)
            {
                try
                {
                    readExcelFile(this.excelFilePath);
                    lblExcelFile.Content = this.excelFilePath;
                    this.image_OpenExcel.Activate();
                }
                catch (Exception ex)
                {
                    string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                    message += string.Format("\n" + ex.StackTrace);
#endif
                    AisMessageBox.DisplayErrorMessageBox(message);
                }
            }
        }

        #region イベントハンドラ

        /// <summary>
        /// GET 押下処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button_Get_Click(object sender, RoutedEventArgs e)
        {
            DateTime start1;
            DateTime end1;
            bool gotTime = AisUtil.GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out start1, out end1);
            if (!gotTime)
            {
                return;
            }
            
            await this.dataGrid_Bulky.CreateMaxCtmInfo(this.maxCtmId, "", start1, end1);
            //var folderPath = await retriever.GetMissionResultCsvAsync(missionId, this.StartUnix, this.EndUnix, cancelToken, this.maxCtmId, "");
            
            WriteToExcel1();
        }

        #endregion

        #region Excelファイルの作成

        private void WriteToExcel1()
        {
            this.image_CancelToGet.IsEnabled = true;
            AisUtil.LoadProgressBarImage(this.image_Gage, true);

            if (string.IsNullOrEmpty(this.dataGrid_Bulky.TempBulkyFilePath))
            {
                FoaMessageBox.ShowError("AIS_E_003");
                return;
            }

            List<string[]> data = readCsvData(this.dataGrid_Bulky.TempBulkyFilePath);

            string filePathDest = string.Empty;

            //bool isHorizontal = (bool)this.radioButton_Horizontal.IsChecked;
            bool isHorizontal = false;

            this.Bw = new BackgroundWorker();
            this.Bw.WorkerSupportsCancellation = true;

            Bw.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                filePathDest = WriteBulkyToExcel(data, this.dataGrid_Bulky.StartR, this.dataGrid_Bulky.EndR, isHorizontal, Bw);
                if (this.Bw.CancellationPending)
                {
                    args.Cancel = true;
                }
            };
            Bw.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                AisUtil.LoadProgressBarImage(this.image_Gage, false);
                this.image_CancelToGet.IsEnabled = false;
                this.image_OpenExcel.Activate();

                this.Bw = null;
                if (args.Cancelled)
                {
                    if (File.Exists(filePathDest))
                    {
                        File.Delete(filePathDest);
                    }

                    return;
                }

                if (args.Error != null)  // if an exception occurred during DoWork,
                {
                    // Do your error handling here
                    AisMessageBox.DisplayErrorMessageBox(args.Error.ToString());
                }

                if (string.IsNullOrEmpty(filePathDest))
                {
                    FoaMessageBox.ShowError("AIS_E_001");
                    return;
                }

                excelFilePath = filePathDest;
                lblExcelFile.Content = filePathDest;
                this.image_OpenExcel.Activate();
                this.dataGrid_Bulky.GotNewResult = false;
            };
            Bw.RunWorkerAsync();
        }

        private string WriteBulkyToExcel(List<string[]> data, DateTime start, DateTime end,
            bool isHorizontal, BackgroundWorker bWorker)
        {
            CtmObject ctm = (CtmUtil.GetCtmObj(editControl.Ctms, this.dataGrid_Bulky.CtmId));
            CtmObject maxCtm = (CtmUtil.GetCtmObj(editControl.Ctms, this.dataGrid_Bulky.MaxCtmId));

            string filePathDest = prepareToWriteBulky();

            var cparams = new Dictionary<ExcelConfigParam, object>();
            cparams.Add(ExcelConfigParam.START, start);
            cparams.Add(ExcelConfigParam.END, end);

            cparams.Add(ExcelConfigParam.TEMPLATE_NAME, "トルク波形");

            cparams.Add(ExcelConfigParam.CTM_ID, this.dataGrid_Bulky.CtmId);
            cparams.Add(ExcelConfigParam.ELEMENT_ID, this.dataGrid_Bulky.ElementId);
            cparams.Add(ExcelConfigParam.CTM_NAME, this.dataGrid_Bulky.CtmName);
            cparams.Add(ExcelConfigParam.B_CONNECTION, isHorizontal ? "横" : "縦");
            cparams.Add(ExcelConfigParam.MAX_CTM_NAME, this.maxCtmName);
            cparams.Add(ExcelConfigParam.MAX_CTM_ID, this.maxCtmId);

            if (this.DataSource == DataSourceType.MISSION)
            {
                cparams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                cparams.Add(ExcelConfigParam.MISSION_NAME, this.SelectedMission.Name);
            }
            else if (this.DataSource == DataSourceType.GRIP)
            {
                cparams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                cparams.Add(ExcelConfigParam.MISSION_NAME, this.SelectedGripMission.Name);
            }

            if (!this.isFirstFile)
            {
                var originalFile = System.IO.Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE);
                cparams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(this.excelFilePath));
            }
            //write bulkyData
            var writer = new SSGExcelWriterBulkyContinuous(ControlMode.Bulky, this.DataSource, ctm, bWorker, this.SelectedMission, this.dataGrid_Bulky.SelectedRowNumber, this.dataGrid_Bulky.SelectedColumnNumber);
            writer.WriteCtmDataStream(filePathDest, this.dataGrid_Bulky.TempBulkyFilePath, cparams, this.dataGrid_Bulky.CtmId, this.dataGrid_Bulky.CtmResultFilepath);
            //write maxCtmData
            writer = new SSGExcelWriterBulkyContinuous(ControlMode.Bulky, this.DataSource, maxCtm, bWorker, this.SelectedMission, this.dataGrid_Bulky.SelectedRowNumber, this.dataGrid_Bulky.SelectedColumnNumber);
            writer.WriteCtmData2(filePathDest, cparams, this.dataGrid_Bulky.MaxCtmId, this.dataGrid_Bulky.MaxCtmResultFilepath);

            return filePathDest;
        }

        /// <summary>
        /// エクセル出力準備
        /// </summary>
        /// <returns></returns>
        private string prepareToWriteBulky()
        {
            if (this.dataGrid_Bulky.SelectedColumnNumber.Count == 0)
            {
                this.dataGrid_Bulky.SelectAllColumn();
            }
            if (this.dataGrid_Bulky.SelectedRowNumber.Count == 0)
            {
                this.dataGrid_Bulky.SelectAllRow();
            }

            var filePathSrc = this.isFirstFile ? Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE) : this.excelFilePath;
            string filePathDest = getExcelFilepath(this.DataSource);
            File.Copy(filePathSrc, filePathDest);

            return filePathDest;
        }

        #endregion

        #region Bulky Grid関連の設定

        private void SetDirection(string direction)
        {
            //if (direction == "横")
            //{
            //    this.radioButton_Horizontal.IsChecked = true;

            //}
            //else
            //{
            //    this.radioButton_Vertical.IsChecked = true;
            //}
        }

        #endregion

        /// <summary>
        /// 収集開始日時と収集終了日時を取込みAIS画面に表示
        /// </summary>
        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string conditionSheetName = "Bulkyテンプレート";
                SpreadsheetGear.IWorksheet worksheetCondition = AisUtil.GetSheetFromSheetName_SSG(book, conditionSheetName);
                var conditionDic = readCondition(worksheetCondition);

                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var paramMap = readParam(worksheetParam);

                int firstRowIndex = 5;
                int firstColumnIndex = 0;

                //BULKYテンプレートの有効行の算出
                int rowLength = AisUtil.GetRowLength_SSG(worksheetCondition, firstRowIndex, firstColumnIndex);

                //BULKYテンプレートの有効列の算出
                int columnLength = AisUtil.GetColumnLength_SSG(worksheetCondition, firstRowIndex, firstColumnIndex);
                DataTable dtElement = AisUtil.CreateDataTableFromExcel_SSG(worksheetCondition, firstRowIndex, firstColumnIndex, rowLength, columnLength);
                var elementColumnDic = AisUtil.CountElementColumn(dtElement);

                if (paramMap.ContainsKey("収集タイプ") &&
                    !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                {
                    switch (paramMap["収集タイプ"])
                    {
                        case "CTM":
                            this.DataSource = DataSourceType.CTM_DIRECT;
                            break;
                        case "ミッション":
                            this.DataSource = DataSourceType.MISSION;
                            break;
                        case "GRIP":
                            this.DataSource = DataSourceType.GRIP;
                            break;
                        default:
                            break;
                    }
                }

                // set SelectedMission
                if (paramMap.ContainsKey("ミッションID") &&
                    !string.IsNullOrEmpty(paramMap["ミッションID"]))
                {
                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        string missionId = conditionDic["ミッションID"];
                        Mission mission = AisUtil.GetMissionFromId(missionId);
                        this.SelectedMission = mission;
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        string missionId = conditionDic["ミッションID"];
                        GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                        this.SelectedGripMission = mission;
                    }
                }
                this.conditionDictionary = conditionDic;
                this.paramDictionary = paramMap;

                DateTime dtStart = new DateTime();
                if (DateTime.TryParse(conditionDic["収集開始日時"], out dtStart))
                {
                    this.dateTimePicker_Start.Value = dtStart;
                }

                DateTime dtEnd = new DateTime();
                if (DateTime.TryParse(conditionDic["収集終了日時"], out dtEnd))
                {
                    this.dateTimePicker_End.Value = dtEnd;
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        #region 内容修正

        public void SetLoadedData(bool isMissionSelected, DataSourceType sourceType)
        {
            if (!this.paramDictionary.ContainsKey("CTMID"))
            {
                // 初回
                return;
            }
            string ctmId = this.paramDictionary["CTMID"];

            string selectedMissionId = null;
            if (this.conditionDictionary.ContainsKey("ミッションID"))
            {
                selectedMissionId = this.conditionDictionary["ミッションID"];
            }

            if (string.IsNullOrEmpty(selectedMissionId))
            {
                // 前回CTMを直接選択
                if (!isMissionSelected)
                {
                    Debug.Assert(false, string.Format("Invalid state: {0}, {1}, {2}", isMissionSelected, selectedMissionId, Enum.GetName(typeof(DataSourceType), sourceType)));
                    return;
                }

                this.mainWindow.TabCtrlDS.SelectedIndex = 0;

                var item = this.mainWindow.treeView_Catalog.GetItem(ctmId);
                if (item == null)
                {
                    return;
                }

                CheckBox cb = this.mainWindow.treeView_Catalog.GetCheckBox(item);
                cb.IsChecked = true;
                item.IsSelected = true;
            }
            else
            {
                // 前回ミッションから選択
                if (isMissionSelected)
                {
                    Debug.Assert(false, string.Format("Invalid state: {0}, {1}, {2}", isMissionSelected, selectedMissionId, Enum.GetName(typeof(DataSourceType), sourceType)));
                    return;
                }

                if (sourceType == DataSourceType.MISSION)
                {
                    var item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(selectedMissionId);
                    if (item == null)
                    {
                        if (this.SelectedMission != null)
                        {
                            this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                        }
                        return;
                    }
                    var parentNode = item.Parent as TreeViewItem;
                    parentNode.IsExpanded = true;
                    item.IsSelected = true;
                    this.mainWindow.treeView_Mission_CTM.Focus();
                    item.Focus();
                }
                else if (sourceType == DataSourceType.GRIP)
                {
                    // Mission Tree
                    var item = this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(selectedMissionId);
                    if (item == null)
                    {
                        if (this.SelectedGripMission != null)
                        {
                            this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(this.SelectedGripMission.Id);
                        }
                        return;
                    }
                    var parentNode = item.Parent as TreeViewItem;
                    parentNode.IsExpanded = true;
                    item.IsSelected = true;
                    this.mainWindow.treeView_Mission_Grip.Focus();
                    item.Focus();
                }
                else
                {
                    Debug.Assert(false, string.Format("Invalid state: {0}, {1}, {2}", isMissionSelected, selectedMissionId, Enum.GetName(typeof(DataSourceType), sourceType)));
                    return;
                }
            }

            if (!this.paramDictionary.ContainsKey("最大トルク参照CTMID"))
            {
                return;
            }
            this.maxCtmId = this.paramDictionary["最大トルク参照CTMID"];

            if (!this.paramDictionary.ContainsKey("最大トルク参照CTM名"))
            {
                return;
            }
            this.maxCtmName = this.paramDictionary["最大トルク参照CTM名"];
            this.Max_CtmName.Text = this.paramDictionary["最大トルク参照CTM名"];

            if (!this.paramDictionary.ContainsKey("CTM名"))
            {
                return;
            }
            this.dataGrid_Bulky.CtmName = this.paramDictionary["CTM名"];

            if (!this.paramDictionary.ContainsKey("エレメントID"))
            {
                return;
            }
            string elementId = this.paramDictionary["エレメントID"];

            if (this.paramDictionary.ContainsKey("連結方向"))
            {
                string direction = this.paramDictionary["連結方向"];
                SetDirection(direction);
            }

            string rows = string.Empty;
            if (!this.paramDictionary.ContainsKey("Bulky選択行"))
            {
                return;
            }
            rows = this.paramDictionary["Bulky選択行"];
            string[] rowsArray = rows.Split(',');

            string columns = string.Empty;
            if (!this.paramDictionary.ContainsKey("Bulky選択列"))
            {
                return;
            }
            columns = this.paramDictionary["Bulky選択列"];
            string[] columnsArray = columns.Split(',');

            SetLoadedDataX2(ctmId, elementId, rowsArray, columnsArray);
        }

        private async void SetLoadedDataX2(string ctmId, string elementId, string[] rowsArray, string[] columnsArray)
        {
            DateTime start1;
            DateTime end1;
            bool gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out start1, out end1);
            if (!gotTime)
            {
                return;
            }

            while (true)
            {
                if (this.CurrentDataSourceType != DataSourceType.NONE)
                {
                    logger.Debug("Waiting for item check event hander.");
                    break;
                }
                await Task.Delay(10);
            }

            try
            {
                await this.dataGrid_Bulky.UpdateBulkyTableX(ctmId, elementId, start1, end1);

                if (!this.dataGrid_Bulky.IsEmpty)
                {
                    foreach (string row in rowsArray)
                    {
                        this.dataGrid_Bulky.SelectedRowNumber.Add(int.Parse(row));
                    }

                    foreach (string column in columnsArray)
                    {
                        this.dataGrid_Bulky.SelectedColumnNumber.Add(int.Parse(column));
                    }
                    this.dataGrid_Bulky.UpdateCellColorAndGetButton();

                    this.dataGrid_Bulky.GotNewResult = false;
                }
            }
            catch (Exception)
            {
                //throw new Exception("該当するCTMまたはBulkyエレメントが見つかりませんでした。");
            }
        }

        #endregion

        private Dictionary<string, string> readCondition(SpreadsheetGear.IWorksheet worksheetCondition)
        {
            var dic = new Dictionary<string, string>();

            // 収集条件を出力
            SpreadsheetGear.IRange cellsCondition = worksheetCondition.Cells;
            for (int i = 0; i < 10; i++)   // とりあえず10×10のセルを検索
            {
                for (int j = 0; j < 10; j++)
                {
                    // null check
                    if (cellsCondition[i, j].Text == null)
                    {
                        continue;
                    }

                    // ミッション
                    if (cellsCondition[i, j].Text == "ミッション")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // ミッションID
                    if (cellsCondition[i, j].Text == "ミッションID")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // 収集開始日時
                    if (cellsCondition[i, j].Text == "収集開始日時")
                    {
                        string date = cellsCondition[i, j + 1].Text;
                        string time = cellsCondition[i, j + 2].Text;
                        string datetime = date + " " + time;
                        dic.Add(cellsCondition[i, j].Text, datetime);
                        continue;
                    }

                    // 収集終了日時
                    if (cellsCondition[i, j].Text == "収集終了日時")
                    {
                        string date = cellsCondition[i, j + 1].Text;
                        string time = cellsCondition[i, j + 2].Text;
                        string datetime = date + " " + time;
                        dic.Add(cellsCondition[i, j].Text, datetime);
                        continue;
                    }
                }
            }

            return dic;
        }

        private Dictionary<string, string> readParam(SpreadsheetGear.IWorksheet paramSheet)
        {
            var dic = new Dictionary<string, string>();

            SpreadsheetGear.IRange paramCells = paramSheet.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // サーバIPアドレス
                if (paramName == "サーバIPアドレス")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // サーバポート番号
                if (paramName == "サーバポート番号")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバIPアドレス
                if (paramName == "GRIPサーバIPアドレス")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバポート番号
                if (paramName == "GRIPサーバポート番号")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // CTM NAME
                if (paramName == "CTM名")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // CTMID
                if (paramName == "CTMID")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // エレメントID
                if (paramName == "エレメントID")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // Bulky選択行
                if (paramName == "Bulky選択行")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // Bulky選択列
                if (paramName == "Bulky選択列")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // 連結方向
                if (paramName == "連結方向")
                {
                    dic.Add(paramName, paramValue);
                    continue;
                }

                // ソートキー
                if (paramName == "ソートキー")
                {
                    if (paramValue != null)
                    {
                        dic.Add(paramName, paramValue);
                    }
                    continue;
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {

                    dic.Add(paramName, paramValue);
                    continue;
                }
                // 最大トルク参照CTM名
                if (paramName == "最大トルク参照CTM名")
                {

                    dic.Add(paramName, paramValue);
                    continue;
                }
                // 最大トルク参照CTMID
                if (paramName == "最大トルク参照CTMID")
                {

                    dic.Add(paramName, paramValue);
                    continue;
                }
            }

            return dic;
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void dateTimePicker_Start_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            this.StartUnix = UnixTime.ToLong(this.dateTimePicker_Start.Value);
        }

        private void dateTimePicker_End_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            this.EndUnix = UnixTime.ToLong(this.dateTimePicker_End.Value);
        }

        private void dataGrid_Bulky_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        private List<string[]> readCsvData(string filePath)
        {
            List<string[]> data = new List<string[]>();
            using (StreamReader sr = new StreamReader(filePath, AisUtil.Utf8))
            {
                while (-1 < sr.Peek())
                {
                    string src = sr.ReadLine();
                    string dsr = AisUtil.ConvertFromUtf8ToSjis(src);

                    string[] row = dsr.Split(',');  //sr.ReadLine().Split(',')
                    data.Add(row);
                }
            }

            return data;
        }
       
        /// <summary>
        /// カラム選択
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_SelectAllColumn_Click(object sender, RoutedEventArgs e)
        {
            if (this.dataGrid_Bulky.IsEmpty)
            {
                return;
            }
            this.dataGrid_Bulky.SelectAllColumn();
            this.dataGrid_Bulky.UpdateCellColorAndGetButton();
        }

        /// <summary>
        /// 行選択
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_button_SelectAllRow_Click(object sender, RoutedEventArgs e)
        {
            if (this.dataGrid_Bulky.IsEmpty)
            {
                return;
            }
            this.dataGrid_Bulky.SelectAllRow();
            this.dataGrid_Bulky.UpdateCellColorAndGetButton();
        }

        /// <summary>
        /// EXCELマクロをAISより実行：EXCELオープンから起動される。
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="macro"></param>
        /// <param name="saveChange"></param>
        private void getParamsFromExcel(string filepath, string macro, bool saveChange)
        {
            Microsoft.Office.Interop.Excel.Application excel = null;
            Microsoft.Office.Interop.Excel.Workbooks workBooks = null;
            Microsoft.Office.Interop.Excel.Workbook workBook = null;

            try
            {
                //マクロ実行準備
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = false;
                ExcelUtil.MoveExcelOutofScreen(excel);
                workBooks = excel.Workbooks;
                workBook = workBooks.Open(filepath);

                // マクロの実行
                excel.Run(macro);
            }
            catch (Exception ex)
            {
                string message = string.Format("Excelファイルへのアクセスに失敗しました。\n{0}", ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                /*
                System.Windows.Forms.MessageBox.Show(message, "Error!!",
                   System.Windows.Forms.MessageBoxButtons.OK,
                   System.Windows.Forms.MessageBoxIcon.Error);
                 * */
            }
            finally
            {
                if (workBook != null)
                {
                    workBook.Close(saveChange);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workBook);
                    workBook = null;
                }
                
                if (workBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workBooks);
                    workBooks = null;
                }
                
                if (excel != null)
                {
                    excel.Quit();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
                    excel = null;
                }

                // 強制的にガーベージコレクションを実行
                GC.Collect();
            }
        }

        private void image_CancelToGet_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.Bw != null)
            {
                this.Bw.CancelAsync();
            }

            DownloadCts.Cancel();
            DownloadCts = new CancellationTokenSource();
            AisUtil.LoadProgressBarImage(this.image_Gage, false);

            this.button_Get.IsEnabled = true;
            this.image_CancelToGet.IsEnabled = false;
        }

        private void image_OpenExcel_Tap(object sender, RoutedEventArgs e)
        {
            string filePathDest = (string)lblExcelFile.Content;

            //BugNo.380 
            //マクロ起動処理
            //getParamsFromExcel(filePathDest, EXCEL_MACRO, true);

            //EXCELﾌｧｲﾙ表示
            /*
            var processStartInfo = new ProcessStartInfo();
            processStartInfo.FileName = (string)lblExcelFile.Content;
            Process process = Process.Start(processStartInfo);
             * */
            ExcelUtil.OpenExcelFile(filePathDest);


            // ExcelUtil.StartExcelProcess((string)lblExcelFile.Content, true);
        }


        // No.493 Input control of period setting item. ----------------Start
        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {

                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }
        // No.493 Input control of period setting item. ----------------End

        private static readonly ILog logger = LogManager.GetLogger(typeof(UserControl_StreamGraphTemplate));

        private void textBlock_StartTime_Drop(object sender, DragEventArgs e)
        {
            string items = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = items.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            this.maxCtmName = itemArray[0];
            this.maxCtmId = itemArray[1];
            this.Max_CtmName.Text = maxCtmName;
        }
    }
}
