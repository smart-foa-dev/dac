﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    /// <summary>
    /// CMS-UIがアクセスする各アプリケーションのサーバのURLを取得します。
    /// </summary>
    static class CmsUrl
    {
        # region Base method
        /// <summary>
        /// MMSサーバのAPIのベースURL
        /// </summary>
        /// <returns></returns>
        public static string GetMmsBaseUrl()
        {
            return string.Format(AisConf.BaseUrl, AisConf.Config.CmsHost, AisConf.Config.CmsPort); ;
        }

        /// <summary>
        /// SYBサーバのAPIのベースURL
        /// </summary>
        /// <returns></returns>
        public static string GetSybBaseUrl()
        {
            return string.Format(AisConf.BaseUrl, AisConf.Config.CmsHost, AisConf.Config.CmsPort); ;
        }

        /// <summary>
        /// MIBサーバのAPIのベースURL
        /// </summary>
        /// <returns></returns>
        public static string GetMibBaseUrl()
        {
            return string.Format(AisConf.BaseUrl, AisConf.Config.CmsHost, AisConf.Config.CmsPort); ;
        }
        #endregion

        # region Mms - URL


        public static string GetVersionUrl()
        {
            return GetMmsBaseUrl() + "config/version";
        }

        /// <summary>
        /// カタログ言語取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetLangUrl()
        {
            return GetMmsBaseUrl() + "config/catalogLang";
        }

        /// <summary>
        /// カタログ言語設定情報取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetCmsLangInfoUrl()
        {
            return GetMmsBaseUrl() + "config/cmsLangInfo";
        }

        public static string GetMmsCtmStoredInfoUrl()
        {
            return GetMmsBaseUrl() + "ctm/stored";
        }
        #endregion

        # region Syb - URL
        /// <summary>
        /// ログインのURL(POST)
        /// </summary>
        /// <returns></returns>
        public static string GetLoginUrl()
        {
            return GetSybBaseUrl() + "login";
        }
        /// <summary>
        /// AISログインのURL(POST)
        /// </summary>
        /// <returns></returns>
        public static string GetAisLoginUrl()
        {
            return GetSybBaseUrl() + "login/aisLogin";
        }
        /// <summary>
        /// AISログアウトのURL(POST)
        /// </summary>
        /// <returns></returns>
        public static string GetAisLogoutUrl()
        {
            return GetSybBaseUrl() + "login/aisLogout";
        }
        /// <summary>
        /// ログイン中通知インターバル取得(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetIntervalAlive()
        {
            return GetSybBaseUrl() + "login/intervalAlive";
        }
        /// <summary>
        /// ログイン中通知(POST)
        /// </summary>
        /// <returns></returns>
        public static string GetAlive()
        {
            return GetSybBaseUrl() + "login/alive";
        }
        /// <summary>
        /// ログイン先システム登録のURL(PUT)
        /// </summary>
        /// <returns></returns>
        public static string GetLoginSystem()
        {
            return GetSybBaseUrl() + "login/system";
        }
        /// <summary>
        /// サーバポーリング間隔取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetInterval()
        {
            return GetSybBaseUrl() + "system_monitor/interval";
        }
        /// <summary>
        /// FEシステム状態取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetFeStatus()
        {
            return GetSybBaseUrl() + "system_monitor/fe_status";
        }
        /// <summary>
        /// FEジャーナル情報取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetFeJournal()
        {
            return GetSybBaseUrl() + "system_monitor/fe_journal";
        }

        /// <summary>
        /// FE設定情報取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetFeConfigure()
        {
            return GetSybBaseUrl() + "system_monitor/fe_configure";
        }
        /// <summary>
        /// FE生死状態取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetFeAlive()
        {
            return GetSybBaseUrl() + "system_monitor/fe_alive";
        }
        /// <summary>
        /// CGシステム状態取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetCgStatus()
        {
            return GetSybBaseUrl() + "system_monitor/cg_status";
        }
        /// <summary>
        /// CGバックアップ履歴取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetCgBackUp()
        {
            return GetSybBaseUrl() + "cgBackup";
        }
        /// <summary>
        /// CG設定情報取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetCgConfigure()
        {
            return GetSybBaseUrl() + "system_monitor/cg_configure";
        }
        /// <summary>
        /// CG生死状態取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetCgAlive()
        {
            return GetSybBaseUrl() + "system_monitor/cg_alive";
        }
        /// <summary>
        /// ミッション情報取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetMissionInfo()
        {
            return GetSybBaseUrl() + "mission_monitor";
        }
        /// <summary>
        /// ミッション詳細情報取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetMissionDetail()
        {
            return GetSybBaseUrl() + "mission_monitor/detail";
        }
        /// <summary>
        /// ユーザ一覧取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetUserList()
        {
            return GetSybBaseUrl() + "user";
        }
        /// <summary>
        /// ユーザ情報更新(追加)のURL(PUT)
        /// </summary>
        /// <returns></returns>
        public static string GetUserAdd()
        {
            return GetSybBaseUrl() + "user/add";
        }
        /// <summary>
        /// ユーザ情報更新(更新)のURL(PUT)
        /// </summary>
        /// <returns></returns>
        public static string GetUserModify()
        {
            return GetSybBaseUrl() + "user/modify";
        }
        /// <summary>
        /// ユーザ削除のURL(DELETE)
        /// </summary>
        /// <returns></returns>
        public static string GetUserDelete()
        {
            return GetSybBaseUrl() + "user/delete";
        }
        /// <summary>
        /// パスワード更新のURL(PUT)
        /// </summary>
        /// <returns></returns>
        public static string UpdatePass()
        {
            return GetSybBaseUrl() + "user/pass_update";
        }
        /// <summary>
        /// system_rootのパスワード更新(追加)のURL(PUT)
        /// </summary>
        /// <returns></returns>
        public static string GetUserUpdateSystemRoot()
        {
            return GetSybBaseUrl() + "user/update/system_root";
        }
        /// <summary>
        /// ユーザ情報更新のURL(PUT)
        /// </summary>
        /// <returns></returns>
        public static string GetUserUpdate()
        {
            return GetSybBaseUrl() + "user/update";
        }
        /// <summary>
        /// EmergencyMail情報新規、更新のURL(PUT)
        /// </summary>
        /// <returns></returns>
        public static string SetEmergencyMail()
        {
            return GetSybBaseUrl() + "emergencyMail/insertEmergencyMail";
        }
        /// <summary>
        /// EmergencyMail情報取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetInfomation()
        {
            return GetSybBaseUrl() + "emergencyMail";
        }
        /// <summary>
        /// EmergencyMail情報削除のURL(PUT)
        /// </summary>
        /// <returns></returns>
        public static string DeleteEmergencyMail()
        {
            return GetSybBaseUrl() + "emergencyMail/deleteEmergencyMail";
        }
        /// <summary>
        /// ライセンス情報取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetLicenseInfo()
        {
            return GetSybBaseUrl() + "license";
        }
        /// <summary>
        /// 操作履歴ダウンロードのURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetOperationHistory()
        {
            return GetSybBaseUrl() + "log/operation_history";
        }
        /// <summary>
        /// ログイン履歴取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetLoginHistory()
        {
            return GetSybBaseUrl() + "log/login_history";
        }
        /// <summary>
        /// FEログ一覧取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetFeLogList()
        {
            return GetSybBaseUrl() + "log/fe_log_list";
        }
        /// <summary>
        /// FEログダウンロードのURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetFeLogDownload()
        {
            return GetSybBaseUrl() + "log/fe";
        }
        /// <summary>
        /// CGログ一覧取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetCgLogList()
        {
            return GetSybBaseUrl() + "log/cg_log_list";
        }
        /// <summary>
        /// CGログダウンロードのURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetCgLogDownload()
        {
            return GetSybBaseUrl() + "log/cg";
        }
        /// <summary>
        /// FE接続確認のURL(MMSで使用)
        /// </summary>
        /// <returns></returns>
        public static string GetFeConnectTest()
        {
            return GetSybBaseUrl() + "system_monitor/fe_connect";
        }
        /// <summary>
        /// MF接続確認のURL(MMSで使用)
        /// </summary>
        /// <returns></returns>
        public static string GetMfConnectTest()
        {
            return GetSybBaseUrl() + "system_monitor/mf_connect";
        }
        /// <summary>
        /// CG接続確認のURL(MMSで使用)
        /// </summary>
        /// <returns></returns>
        public static string GetCgConnectTest()
        {
            return GetSybBaseUrl() + "system_monitor/cg_connect";
        }
        /// <summary>
        /// ドメイン一覧取得
        /// </summary>
        /// <returns></returns>
        public static string GetDomainList()
        {
            return GetMmsBaseUrl() + "domain/all/ex";
        }

        //workplace sunyi 2018/10/11 start
        public static string RegisterWp()
        {
            return GetMibBaseUrl() + "workplace/registerwp";
        }
        public static string GetAll()
        {
            return GetMibBaseUrl() + "workplace/getall";
        }
        public static string DeleteWp()
        {
            return GetMibBaseUrl() + "workplace/delete";
        }
        public static string DeleteGroup()
        {
            return GetMibBaseUrl() + "workplace/deletegroup";
        }
        //workplace sunyi 2018/10/11 end


        /// <summary>
        /// AISで「内容修正」機能処理（workplaceIdによって、ミッション、CTMとElement内容を検索する）
        /// </summary>
        /// <returns></returns>
        public static string GetAisSelect()
        {
            return GetSybBaseUrl() + "workplace/select";
        }

        /// <summary>
        /// 削除の場合、workplaceIDとファイルIDによって、
        /// テープル「WorkPlace参照マスタ 」の項目「参照カウント」に「1」をマイナスする。
        /// </summary>
        /// <returns></returns>
        public static string GetDecreaseref()
        {
            return GetSybBaseUrl() + "workplace/decreaseref";
        }

        /// <summary>
        /// AISでEXCELファイルを開けて、参照の場合
        /// </summary>
        /// <returns></returns>
        public static string GetReaseref()
        {
            return GetSybBaseUrl() + "workplace/reference";
        }
        // FOA_サーバー化開発 Processing On Server Xj 2018/08/16 End

        public static string GetEvents()
        {
            return GetSybBaseUrl() + "event/alert";
        }

		// FOA_サーバー化開発 Processing On Server Xj 2018/08/16 Start
        /// <summary>
        /// Workplaceサーバーへworkplaceデータを登録
        /// </summary>
        /// <returns></returns>
        public static string GetAisCreate()
        {
            return GetSybBaseUrl() + "workplace/aisCreate";
        }
		// FOA_サーバー化開発 Processing On Server Xj 2018/08/16 End

        /// <summary>
        /// システムモニター アクセス クラス
        /// </summary>
        public static class SystemMonitor
        {
            public static string Base()
            {
                return GetSybBaseUrl() + "system_monitor";
            }
            /// <summary>
            /// FE現在CTM平均受信頻度取得のURL(GET)
            /// </summary>
            /// <returns></returns>
            public static string FeCtmRInfo()
            {
                return Base() + "/fe_ctm_r_info";
            }
            /// <summary>
            /// FE現在CTM受信Q情報取得のURL(GET)
            /// </summary>
            /// <returns></returns>
            public static string FeCtmQInfo()
            {
                return Base() + "/fe_ctm_q_info";
            }

            /// <summary>
            /// FE現在CTM受信Q情報取得のURL(GET)
            /// </summary>
            /// <returns></returns>
            public static string FeJournalInfo()
            {
                return Base() + "/fe_journal2";
            }

            public static string FeConfigInfo2()
            {
                return Base() + "/fe_configure2";
            }

            public static string CgConfig()
            {
                return Base() + "/cg/config";
            }
            public static string CgSendCount()
            {
                return Base() + "/cg/sendCount";
            }
            public static string CgSendQ()
            {
                return Base() + "/cg/sendQ";
            }
            public static string CgTagQ()
            {
                return Base() + "/cg/tagQ";
            }
            public static string CgP2Dll()
            {
                return Base() + "/cg/p2Dll";
            }
            public static string CgCpu()
            {
                return Base() + "/cg/cpu";
            }
            public static string CgChangeFlag()
            {
                return Base() + "/cg/changeFlag";
            }

        }
        #endregion

        # region Mib - URL
        /// <summary>
        /// MIBサーバの設定を取得、更新するURL
        /// </summary>
        /// <returns></returns>
        public static string GetMibUrl()
        {
            return GetMibBaseUrl() + "mib";
        }

        /// <summary>
        /// MIBのサーバから！SYBのユーザを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string GetMibSybUserUrl()
        {
            return GetMibBaseUrl() + "mib/syb/user";
        }

        /// <summary>
        /// MIBのサーバから！MMSの参照カタログを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string GetMibMmsRefDicUrl()
        {
            return GetMibBaseUrl() + "mib/mms/refdic";
        }

        /// <summary>
        /// MIBのサーバから！MMSのCTMを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string GetMibMmsCtmUrl()
        {
            return GetMibBaseUrl() + "mib/mms/ctm";
        }

        /// <summary>
        /// MIBのサーバから！MongoDBに蓄積されたCTMの情報を取得するURL
        /// </summary>
        /// <returns></returns>
       /***
        public static string GetMibMmsCtmStoredInfoUrl()
        {
            return GetMibBaseUrl() + "mib/mms/ctm/stored";
        }
        **/

        /// <summary>
        /// エージェントのURL
        /// </summary>
        /// <returns></returns>
        public static string GetAgentUrl()
        {
            return GetMibBaseUrl() + "mib/agent";
        }

        /// <summary>
        /// ミッションのURL
        /// </summary>
        /// <returns></returns>
        public static string GetMissionUrl()
        {
            return GetMibBaseUrl() + "mib/mission";
        }

        /// <summary>
        /// ミッション(CTM-ID指定の使用中ミッションツリー)のURL
        /// </summary>
        /// <returns></returns>
        public static string GetMissionByCtmInUseUrl()
        {
            return GetMibBaseUrl() + "mib/mission/byCtmInUse";
        }

        /// <summary>
        /// ミッション開始のURL
        /// </summary>
        /// <returns></returns>
        public static string GetMissionStartUrl()
        {
            return GetMibBaseUrl() + "mib/mission/start";
        }

        /// <summary>
        /// ミッション停止のURL
        /// </summary>
        /// <returns></returns>
        public static string GetMissionStopUrl()
        {
            return GetMibBaseUrl() + "mib/mission/stop";
        }

        /// <summary>
        /// ミッションのCTMのURL
        /// </summary>
        /// <returns></returns>
        public static string GetMissionCtmUrl()
        {
            return GetMibBaseUrl() + "mib/mission/ctm";
        }

        /// <summary>
        /// ミッションのCTMのエレメントのURL
        /// </summary>
        /// <returns></returns>
        public static string GetMissionCtmElementUrl()
        {
            return GetMibBaseUrl() + "mib/mission/ctm/element";
        }

        /// <summary>
        /// ミッションの結果のURL
        /// </summary>
        /// <returns></returns>
        public static string GetMissionResultUrl()
        {
            return GetMibBaseUrl() + "mib/mission/result";
        }

        /// <summary>
        /// ミッションの結果のダウンロードURL
        /// </summary>
        /// <returns></returns>
        public static string GetMissionResultDownloadUrl()
        {
            return GetMibBaseUrl() + "mib/mission/result/download";
        }

        /// <summary>
        /// ミッションの結果のダウンロードURL(一括)
        /// </summary>
        /// <returns></returns>
        public static string GetMissionResultDownloadSummaryUrl()
        {
            return GetMibBaseUrl() + "mib/mission/result/download/summary";
        }
        #endregion

        public static class Block
        {
            public static string Base()
            {
                return GetSybBaseUrl() + "block";
            }
            /// <summary>
            /// FE現在CTM平均受信頻度取得のURL(GET)
            /// </summary>
            /// <returns></returns>
            public static string NamesByCgX()
            {
                return Base() + "/byCgX";
            }
        }

        /// <summary>
        /// GRIPサーバのAPIのベースURL
        /// </summary>
        /// <returns></returns>
        public static string GetGripBaseUrl()
        {
            return string.Format(AisConf.GripServerBaseUrl, AisConf.Config.GripServerHost, AisConf.Config.GripServerPort);
        }

        #region Search
        public static class Search
        {
            public static string Base()
            {
                return GetGripBaseUrl() + "search";
            }

            public static string DoSearch()
            {
                return Base() + "/search";
            }

            public static string CreateCsv()
            {
                return Base() + "/createcsv";
            }

            public static string GetCsvBlock()
            {
                return Base() + "/getcsvblock";
            }

            public static string Download()
            {
                return Base() + "/download?searchId={0}&endId={1}&pathId={2}";
            }

            public static string Paths()
            {
                return Base() + "/paths";
            }

            public static class Mission
            {
                public static string Base()
                {
                    return Search.Base() + "/mission";
                }

                public static string DoSearch()
                {
                    return Mission.Base() + "/search?missionId={0}&searchId={1}&start={2}&end={3}&searchByInCondition={4}&skipSameMainKey={5}";
                }

                public static string CreateCsv()
                {
                    return Mission.Base() + "/createcsv?missionId={0}&searchId={1}&start={2}&end={3}&searchByInCondition={4}&skipSameMainKey={5}&noBulky=true&linkBulky=true";
                }

                // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
                public static string FindPath()
                {
                    return Base() + "/" + "findpath";
                }
				// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End
            }

            public static class Ais2
            {
                public static string Base()
                {
                    return Search.Base() + "/ais2";
                }

                public static class BulkyTemplate
                {
                    public static string Base()
                    {
                        return Ais2.Base() + "/bulkyTemplate";
                    }

                    public static class GripMission
                    {
                        public static string Base()
                        {
                            return BulkyTemplate.Base() + "/gripMission";
                        }

                        public static string DoSearch()
                        {
							return GripMission.Base() + "?searchId={0}&missionId={1}&start={2}&end={3}&ctmId={4}&elementId={5}&limit={6}&lang={7}&noBulky=true&linkBulky=true&searchByInCondition=auto&skipAllNodesWhileNoCtms=true";
                        }

                        public static string Status()
                        {
                            return GripMission.Base() + "/status";
                        }

                        public static string GetStatus()
                        {
                            return Status() + "?searchId={0}&endId={1}&pathId={2}";
                        }

                        public static string Download()
                        {
                            return GripMission.Base() + "/download";
                        }

                        public static string DoDownload()
                        {
                            return Download() + "?searchId={0}&endId={1}&pathId={2}";
                        }
                    }
                }
            }
        }
        #endregion

        #region ExcelEngine
        public static class ExcelEngine
        {
            public static string Base()
            {
                return string.Format(AisConf.ExcelEngineBaseUrl, AisConf.Config.GripServerHost, AisConf.Config.GripServerPort);
            }

            public static string WorkPlace()
            {
                return Base() + "/workplace";
            }

            public static string GetExternalCsv()
            {
                return WorkPlace() + "/getExternalExcelCsvZip";
            }

            public static string GetExternalCsvWithParam()
            {
                return GetExternalCsv() + "?workplaceId={0}";
            }
        }
        #endregion
    }
}
