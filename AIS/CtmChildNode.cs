﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    [JsonConverter(typeof(CtmChildNodeConverter))]
    public interface CtmChildNode
    {
        bool IsElement();
    }
}
