﻿using DAC.Model.Util;
using DAC.View.Helpers;
using FoaCore.Common;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;

namespace DAC.Model
{
    public class EmergencyTimer : IDisposable
    {
        public EmergencyTimer()
        {
            this.Id = new GUID().ToString();

            //this.timer.Interval = new TimeSpan();
            //this.timer.Tick += new EventHandler(timer_Tick);
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            //マネージリソースおよびアンマネージリソースの解放
            this.dispose();

            //ガベコレから、このオブジェクトのデストラクタを対象外とする
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose
        /// </summary>
        protected virtual void dispose()
        {
            lock (this)
            {
                if (this.disposed)
                {
                    //既に呼びだしずみであるならばなんもしない
                    return;
                }

                this.disposed = true;

                this.TimerStop();
                this.EndTime = new DateTime().ToString("yyyy/MM/dd HH:mm:ss");
                this.timer.Tick -= new EventHandler(timer_Tick);
            }
        }

        private bool disposed;

        public string Id { get; set; }

        // タイマー関連
        [JsonIgnore]
        public DispatcherTimer Timer { get { return this.timer; } set { this.timer = value; } }
        [JsonIgnore]
        private DispatcherTimer timer = new DispatcherTimer(DispatcherPriority.Input);
        [JsonIgnore]
        public TimeSpan NowTimeSpan { get { return this.nowTimeSpan; } set { this.nowTimeSpan = value; } }
        [JsonIgnore]
        private TimeSpan nowTimeSpan = new TimeSpan();
        [JsonIgnore]
        public TimeSpan OldTimeSpan { get { return this.oldTimeSpan; } set { this.oldTimeSpan = value; } }
        [JsonIgnore]
        private TimeSpan oldTimeSpan = new TimeSpan();
        [JsonIgnore]
        public DateTime StartTime { get { return this.startTime; } set { this.startTime = value; } }
        [JsonIgnore]
        private DateTime startTime = new DateTime();
        public int IntervalMinute { get; set; }

        // 条件関連
        public string MissionName { get; set; }
        public string MissionId { get; set; }
        public string CtmName { get; set; }
        public string CtmId { get; set; }
        public string EndTime { get; set; }
        public int MaxCount { get; set; }
        public int Count { get { return this.count; } set { this.count = value; } }
        private int count = 0;
        public bool IsDetailCondition { get; set; }
        public string ElementName_01 { get; set; }
        public string ElementId_01 { get; set; }
        public int ElementType_01 { get; set; }
        public string ElementCondition_01 { get; set; }
        public string ElementConditionValue_01 { get; set; }
        public string ElementName_02 { get; set; }
        public string ElementId_02 { get; set; }
        public int ElementType_02 { get; set; }
        public string ElementCondition_02 { get; set; }
        public string ElementConditionValue_02 { get; set; }
        public string AndOr { get; set; }
        public string UserId { get; set; }
        public string UiLang { get; set; }
        public string MailAddress { get; set; }
        public string MailSubject { get; set; }
        public string MailMessage { get; set; }
        public string LoginUserId { get; set; }
        public bool AddFile { get; set; }
        [JsonIgnore]
        public CtmObject Ctm { get; set; }

        private bool isAvailable = true;

        private ProgramMission result = new ProgramMission();
        private List<int> hitIndex = new List<int>();
        [JsonIgnore]
        public string EndTimeString
        {
            get
            {
                string time = this.EndTime;
                return time;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!this.isAvailable)
            {
                TimerStop();
                return;
            }

            if (Convert.ToDateTime(this.EndTime) < DateTime.Now)
            {
                Console.WriteLine("### It's time now.");
                this.timer.Stop();
                return;
            }

            incrementHitCount(this.ElementId_01, this.ElementCondition_01, this.ElementConditionValue_01,
                this.ElementId_02, this.ElementCondition_02, this.ElementConditionValue_02, this.AndOr);
            Console.WriteLine("### Limit: {0} Count: {1}", this.MaxCount, this.count);
            if (this.MaxCount <= this.count)
            {
                this.count = 0;
                this.startTime = DateTime.Now;
                Console.WriteLine("### Count has reached the upper limit.");
                SendMail(this.MailAddress, this.MailSubject, this.MailMessage, this.AddFile);
                this.hitIndex = new List<int>();
                // this.timer.Stop();
            }
        }

        public void TimerStart()
        {
            this.timer.Interval = new TimeSpan(0, IntervalMinute, 0);
            this.startTime = DateTime.Now;
            this.timer.Start();
            this.isAvailable = true;
            Console.WriteLine("### Started.");
        }

        public void TimerStop()
        {
            this.oldTimeSpan = this.oldTimeSpan.Add(this.nowTimeSpan);
            this.timer.Stop();
            this.isAvailable = false;
            Console.WriteLine("### Stoped.");
        }

        private void timerReset()
        {
            this.oldTimeSpan = new TimeSpan();
        }

        /// <summary>
        /// プログラムミッションの結果を出力(保存)
        /// </summary>
        /// <param name="host">サーバのIPアドレス</param>
        /// <param name="port">サーバのポート番号</param>
        /// <param name="missionId">ミッションID</param>
        /// <param name="start">収集開始日時</param>
        /// <param name="end">収集終了日時</param>
        /// <rereturns>出力先ファイルの絶対パス</rereturns>
        private string writeProgramMissionResultJson(string host, string port, string missionId, long start, long end)
        {
            string limit = "0";
            string lang = "ja";
            DateTime dt = DateTime.Now;

            System.Diagnostics.Debug.Print(missionId + " : " + start.ToString() + " " + end.ToString());

            string url = String.Format("http://{0}:{1}/cms/rest/mib/mission/pm?id={2}&start={3}&end={4}&limit={5}&lang={6}",
                       host, port, missionId, start, end, limit, lang);
            Console.WriteLine("Request URL: {0}", url);

            string filePath = string.Empty;
            WebClient client = null;

            try
            {
                if (Directory.Exists(System.Windows.Forms.Application.StartupPath + @"\tmp") == false)
                {
                    Directory.CreateDirectory(System.Windows.Forms.Application.StartupPath + @"\tmp");
                }

                filePath = System.Windows.Forms.Application.StartupPath + @"\tmp\" + dt.ToString("yyyyMMddHHmmssfff") + ".json";
                //client = new WebClient();
                client = AisUtil.getProxyWebClient();
                client.DownloadFile(url, filePath);
            }
            finally
            {
                if (client != null)
                {
                    client.Dispose();
                }
            }

            return filePath;
        }

        private const int GET_STREAM_BUF_SIZE = 500;

        /// <summary>
        /// プログラムミッションの結果をデシリアライズ
        /// </summary>
        /// <param name="filePath">結果ファイルの絶対パス</param>
        /// <returns>デシリアライズされたプログラムミッションの結果</returns>
        private List<ProgramMission> deserializeProgramMissionResult(string filePath)
        {
            List<ProgramMission> resultList = new List<ProgramMission>();

            StreamReader sr = null;
            JsonTextReader jtr = null;
            var jObjectList = new JObject[GET_STREAM_BUF_SIZE];

            try
            {
                sr = new StreamReader(filePath, Encoding.UTF8);
                jtr = new JsonTextReader(sr);
                int cnt = 0;

                while (jtr.Read())
                {
                    if (jtr.TokenType == JsonToken.StartObject)
                    {
                        JObject jObj = JObject.Load(jtr);
                        string key = key = jObj["id"].ToString();

                        jObjectList[cnt] = jObj;
                        cnt++;

                        if (cnt == GET_STREAM_BUF_SIZE)
                        {
                            jObjectList = new JObject[GET_STREAM_BUF_SIZE];
                            cnt = 0;
                        }
                    }
                }
            }
            finally
            {
                if (jtr != null)
                {
                    jtr.Close();
                }
                if (sr != null)
                {
                    sr.Close();
                }
                if (File.Exists(filePath))
                {
                    // 使用したファイルは削除
                    File.Delete(filePath);
                }
            }

            foreach (var jsonObject in jObjectList)
            {
                if (jsonObject == null)
                {
                    continue;
                    // break;
                }

                ProgramMission result = jsonObject.ToObject<ProgramMission>();
                resultList.Add(result);
            }

            return resultList;
        }

        private string getJsonFromCtm(ProgramMission.Ctms ctm)
        {
            var serializer = new DataContractJsonSerializer(typeof(ProgramMission.Ctms));
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, ctm);
            string json = Encoding.UTF8.GetString(ms.ToArray());

            return json;
        }

        private void incrementHitCount(string elementId_01, string elementCondition_01, string elementConditionValue_01,
            string elementId_02, string elementCondition_02, string elementConditionValue_02, string andOr)
        {
            // UnixTimeStampに変換
            long startUnix = UnixTime.ToLong(this.startTime);
            long endUnix = UnixTime.ToLong(DateTime.Now);

            string host = AisConf.Config.CmsHost;
            string port = AisConf.Config.CmsPort;

            string filePath = writeProgramMissionResultJson(host, port, this.MissionId, startUnix, endUnix);
            List<ProgramMission> result = deserializeProgramMissionResult(filePath);

            foreach (ProgramMission pm in result)
            {
                Console.WriteLine("### Ctm Name: {0}", pm.name);
                if (pm.id != this.CtmId)
                {
                    continue;
                }

                this.result = pm;
                Console.WriteLine("### Result Count: {0}", pm.ctms.Count);

                if (!this.IsDetailCondition)
                {
                    // CTM受信監視(条件は見ず個数のみ)
                    this.count += pm.ctms.Count;
                    for (int i = 0; i < pm.ctms.Count; i++)
                    {
                        this.hitIndex.Add(i);
                    }
                    return;
                }

                // 閾値監視(エレメントに条件を設ける)
                string json = string.Empty;
                for (int i = 0; i < pm.ctms.Count; i++)
                {
                    bool hit = false;
                    foreach (string id in pm.ctms[i].EL.Keys)
                    {
                        if (id != elementId_01)
                        {
                            continue;
                        }

                        if (meetCondition(pm.ctms[i].EL[id].V, elementCondition_01, elementConditionValue_01))
                        {
                            hit = true;
                        }

                        break;
                    }

                    if (!string.IsNullOrEmpty(elementId_02))
                    {
                        // ふたつめの条件がある
                        if (!hit)
                        {
                            // ひとつめの条件NG
                            if (andOr == "AND")
                            {
                                // ANDなので終了(NG)
                                continue;
                            }
                            else
                            {
                                // ORなのでふたつめの条件判定
                                foreach (string id in pm.ctms[i].EL.Keys)
                                {
                                    if (id != elementId_02)
                                    {
                                        continue;
                                    }

                                    if (meetCondition(pm.ctms[i].EL[id].V, elementCondition_02, elementConditionValue_02))
                                    {
                                        json += getJsonFromCtm(pm.ctms[i]) + Environment.NewLine;
                                        this.hitIndex.Add(i);
                                        this.count++;
                                        Console.WriteLine("### {0} - {1} ... Hit ({2})",
                                            this.MissionId, this.CtmId, this.count);
                                    }

                                    break;
                                }
                            }
                        }
                        else
                        {
                            // ひとつめの条件がhit
                            if (andOr == "AND")
                            {
                                // ANDなのでふたつめの条件判定
                                foreach (string id in pm.ctms[i].EL.Keys)
                                {
                                    if (id != elementId_02)
                                    {
                                        continue;
                                    }

                                    if (meetCondition(pm.ctms[i].EL[id].V, elementCondition_02, elementConditionValue_02))
                                    {
                                        json += getJsonFromCtm(pm.ctms[i]) + Environment.NewLine;
                                        this.hitIndex.Add(i);
                                        this.count++;
                                        Console.WriteLine("### {0} - {1} ... Hit ({2})",
                                            this.MissionId, this.CtmId, this.count);
                                    }

                                    break;
                                }
                            }
                            else
                            {
                                // ORなので終了(OK)
                                json += getJsonFromCtm(pm.ctms[i]) + Environment.NewLine;
                                this.hitIndex.Add(i);
                                this.count++;
                                Console.WriteLine("### {0} - {1} ... Hit ({2})",
                                    this.MissionId, this.CtmId, this.count);
                            }
                        }
                    }
                    else
                    {
                        // ふたつめの条件が存在しない
                        if (!hit)
                        {
                            // hitしなかったので終了(NG)
                            continue;
                        }

                        // hitしたので終了(OK)
                        json += getJsonFromCtm(pm.ctms[i]) + Environment.NewLine;
                        this.hitIndex.Add(i);
                        this.count++;
                        Console.WriteLine("### {0} - {1} ... Hit ({2})",
                            this.MissionId, this.CtmId, this.count);
                    }
                }

                break;
            }
        }

        private bool meetCondition(string value, string elementCondition, string elementConditionValue)
        {
            Console.WriteLine("### value: {0}", value);
            Console.WriteLine("### condition: {0}", elementCondition);
            switch (elementCondition)
            {
                case "==":
                    if (value == elementConditionValue)
                    {
                        return true;
                    }
                    break;
                case "!=":
                    if (value != elementConditionValue)
                    {
                        return true;
                    }
                    break;
                case "<":
                    if (float.Parse(value) < float.Parse(elementConditionValue))
                    {
                        return true;
                    }
                    break;
                case "<=":
                    if (float.Parse(value) <= float.Parse(elementConditionValue))
                    {
                        return true;
                    }
                    break;
                case ">":
                    if (float.Parse(value) > float.Parse(elementConditionValue))
                    {
                        return true;
                    }
                    break;
                case ">=":
                    if (float.Parse(value) >= float.Parse(elementConditionValue))
                    {
                        return true;
                    }
                    break;
                default:
                    break;
            }

            return false;
        }

        public void SendMail(string address, string subject, string message, bool addFiles)
        {
            // jsonつくる
            string json = @"{{""to"":[""{0}""],""subject"":""{1}"",""message"":""{2}""";
            json = string.Format(json,
                address,
                subject,
                message);
            if (addFiles)
            {
                // 添付ファイル追加
                string attach = string.Empty;
                if (0 < this.hitIndex.Count)
                {
                    var resultList = getResultList(this.result, this.hitIndex);
                    string csv = createCsv(resultList);
                    string csvShiftJis = AisUtil.ConvertFromUtf8ToSjis(csv);
                    byte[] bytesToEncode = Encoding.GetEncoding("shift_jis").GetBytes(csvShiftJis);
                    attach = Convert.ToBase64String(bytesToEncode);
                    json += string.Format(@",""attach"":""{0}"",""attachFilename"":""ctm.csv""", attach);
                }
            }
            json += "}";

            string url = string.Format("http://{0}:{1}/cms/rest/mail", AisConf.Config.CmsHost, AisConf.Config.CmsPort);
            try
            {
                // リクエストおくる
                CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
                client.AddParam("addr", "nishimura.namihiro.foa@gmail.com");
                client.Post(url, json);
                Console.WriteLine("### send.");
            }
            catch (Exception e)
            {
                AisMessageBox.DisplayErrorMessageBox(e.Message + "\r\n\r\n" + e.StackTrace);
            }
        }

        private List<string[]> getResultList(ProgramMission pm, List<int> hitIndex)
        {
            var header = new Dictionary<string, string>();
            var data = new List<Dictionary<string, string>>();
            
            int rowNumber = 0;
            string guid = Guid.NewGuid().ToString();
            for (int i = 0; i < pm.ctms.Count; i++)
            {
                if (!hitIndex.Contains(i))
                {
                    continue;
                }

                var el = new Dictionary<string, string>();
                var elements = this.Ctm.GetAllElements();
                foreach (var element in elements)
                {
                    foreach (string elementId in pm.ctms[i].EL.Keys)
                    {
                        if (elementId != element.id.ToString())
                        {
                            continue;
                        }

                        // ヘッダーの数が足りていなければヘッダー追加
                        if (header.Count < pm.ctms[i].EL.Keys.Count)
                        {
                            header.Add(elementId, element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                        }

                        // 値を追加
                        if (pm.ctms[i].EL[elementId].T != "100")
                        {
                            el.Add(elementId, pm.ctms[i].EL[elementId].V);
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(pm.ctms[i].EL[elementId].V))
                            {
                                pm.ctms[i].EL[elementId].V = string.Empty;
                            }
                            else
                            {
                                // bulky file.
                                string emergencyBulkyDirectory = string.Format(@"{0}\{1}\{2}\{3}\{4}",
                                    System.Windows.Forms.Application.StartupPath,
                                    "Emergency",
                                    guid,
                                    CtmId,
                                    elementId);
                                if (!Directory.Exists(emergencyBulkyDirectory))
                                {
                                    Directory.CreateDirectory(emergencyBulkyDirectory);
                                }

                                string fileName = string.Format("{0:D5}", (rowNumber + 1)) + "_" + pm.ctms[i].EL[elementId].FN;
                                if (string.IsNullOrEmpty(fileName))
                                {
                                    fileName = elementId + ".bulky";
                                }

                                string saveFilePath = emergencyBulkyDirectory + "\\" + fileName;

                                byte[] binaryData = Convert.FromBase64String(pm.ctms[i].EL[elementId].V);
                                File.WriteAllBytes(saveFilePath, binaryData);

                                string hyperLink = string.Format("=HYPERLINK(\"" + saveFilePath + "\",\"" + fileName + "\")");

                                el.Add(elementId, "\"" + hyperLink.Replace("\"", "\"\"") + "\"");
                            }
                        }

                        break;
                    }
                }

                rowNumber++;
                data.Add(el);
            }

            List<string[]> outputData = new List<string[]>();
            List<string> row = new List<string>();
            foreach (var h in header.Keys)
            {
                row.Add(header[h]);
            }
            outputData.Add(row.ToArray());
            foreach (var d in data)
            {
                row.Clear();
                foreach (string elementId in d.Keys)
                {
                    row.Add(d[elementId]);
                }
                outputData.Add(row.ToArray());
            }

            return outputData;
        }

        private string createCsv(List<string[]> data)
        {
            StringBuilder csv = new StringBuilder();
            string csv2 = string.Empty;

            foreach (string[] row in data)
            {
                string line = string.Join(",", row);
                csv.AppendLine(line);
                csv2 += line + Environment.NewLine;
            }

            // return csv.ToString();
            

            return csv2;
        }
    }
}
