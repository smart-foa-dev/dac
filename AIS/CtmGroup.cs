﻿using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    public class CtmGroup : CtmNode, CtmChildNode
    {
        public List<CtmChildNode> children = new List<CtmChildNode>();

        public int parentNodeIdx4Ddmap;

        public bool IsElement()
        {
            return false;
        }

        public void SetChildrenNodeBiLink()
        {
            foreach (CtmChildNode child in children)
            {
                if (child.GetType().Equals(typeof(CtmGroup)))
                {
                    CtmGroup group = (CtmGroup)child;
                    group.parentId = new GUID(id.ToString());
                    group.SetChildrenNodeBiLink();
                }
                else if (child.GetType().Equals(typeof(CtmElement)))
                {
                    CtmElement element = (CtmElement)child;
                    element.parentId = new GUID(id.ToString());
                }
            }
        }

        public void UnsetChildrenNodeBiLink()
        {
            foreach (CtmChildNode child in children)
            {
                if (child.GetType().Equals(typeof(CtmGroup)))
                {
                    CtmGroup group = (CtmGroup)child;
                    group.parentId = null;
                    group.UnsetChildrenNodeBiLink();
                }
                else if (child.GetType().Equals(typeof(CtmElement)))
                {
                    CtmElement element = (CtmElement)child;
                    element.parentId = null;
                }
            }
        }
    }
}
