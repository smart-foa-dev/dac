﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace DAC.Model
{
    [DataContract]
    public class Ctm : DAC.Model.ProgramMission.Ctms
    {
        /// <summary>
        /// CTM-ID from Find CTM Result
        /// </summary>
        [DataMember]
        public string MID { get; private set; }
    }
}
