﻿using DAC.View;
using DAC.View.Helpers;
using FoaCore.Common;
using FoaCore.Common.Control;
using FoaCore.Common.Converter;
using FoaCore.Common.Entity;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;


namespace DAC.Model
{
    /// <summary>
    /// CTMツリーコントローラ
    /// </summary>
    public class CtmTreeController : MibController
    {
        /// <summary>
        /// JsonTreeView
        /// </summary>
        public JsonTreeView tree;

        /// <summary>
        /// ミッションのテーブルのPKを保持するコレクション
        /// キーはCTMのIDもしくはエレメントのID
        /// 値はミッションのテーブルのPK(MISSION_CTMもしくはMISSION_CTM_ELEMENT)
        /// </summary>
        private Dictionary<string, string> pkMap;
        // CTMやエレメントは、ミッション独自のオブジェクトやJsonの形式ではなく
        // MMSのオブジェクトならびにJsonの形式をそのまま使っています。
        // その為、ミッションのテーブルのPKに関する情報がセット出来ず、
        // UIからPKを渡すには、CtmやCtmElementを拡張もしくはフィールド追加する必要があります。
        // ただし拡張するとシリアライザやMMSのJsonを汚す事になるので、極力避けたいと考えました。
        // とはいえ、PKをアテにせずDBにアクセスするには、かなりつらい(DBを正規化した為)ので
        // PKとCTMのID、エレメントのIDのキーのMapをキャッシュしています。

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="viewTree"></param>
        public CtmTreeController(JsonTreeView viewTree)
        {
            tree = viewTree;
            SetupCtmTreeView(tree);
        }

        /// <summary>
        /// CTMのツリービューの設定を行います。
        /// </summary>
        /// <param name="viewTree"></param>
        public static void SetupCtmTreeView(JsonTreeView viewTree)
        {
            // ツリーのJsonキーの設定
            viewTree.IdKeyName = "id";
            viewTree.CollectionKeyName = "children";

            // ツリーのアイコンの設定
            viewTree.FrontIconKeyName = "type";
            viewTree.FrontIconMap = new Dictionary<string, string>(){
                {NodeType.Ctm, "ctm.png"},
                {NodeType.Group, "group.png"},
                {NodeType.Element, "element.png"},
                {NodeType.Default, "ctm.png"},
            };

            viewTree.LabelFunction = json =>
            {
                JToken token = JToken.Parse(json);
                JToken dnToken = token["displayName"];
                string label = DisplayNameConverter.Convert2(dnToken);
                if (token["datatype"] != null)
                {
                    string dataType = CtmElementType.GetNameByValue((int)token["datatype"]);
                    label = label + "（" + dataType + "）";
                }

                return label;
            };
        }


        private Dictionary<string, CollectionInfo> collectionInfoMap = new Dictionary<string, CollectionInfo>();


        public void Load(bool onInitWithExisting)
        {
            // 参照カタログを取得
            var userControl = Application.Current.MainWindow;
            CmsHttpClient client = new CmsHttpClient(userControl);
            client.LastInvokeCompleteHandler = true;
            client.CompleteHandler += resJson =>
            {
                this.tree.JsonItemSource = resJson;

                // MongoDBに蓄積されたCTMの情報を取得
                CmsHttpClient storedCtmInfoGetClient = new CmsHttpClient(userControl);
                storedCtmInfoGetClient.CompleteHandler += storedCtmInfoJson =>
                {
                    // CTMのコレクション情報を保持
                    JArray collectionInfos = new JArray();
                    try
                    {
                        collectionInfos = JArray.Parse(storedCtmInfoJson);
                    }
                    catch { return; } // Failed to parse the Json, MfService might be down in this case..

                    // ツリーに表示(コレクションの情報が取れなくても表示する為)
                    try
                    {
                        foreach (JToken token in collectionInfos)
                        {
                            CollectionInfo ci = JsonConvert.DeserializeObject<CollectionInfo>(token.ToString());
                            collectionInfoMap[ci.Id] = ci;
                        }
                    }
                    catch (Exception ex)
                    {
                        string message = string.Format(DAC.Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                        message += string.Format("\n" + ex.StackTrace);
#endif
                        AisMessageBox.DisplayErrorMessageBox(message);
                        //System.Windows.Forms.MessageBox.Show(message, Properties.Message.C_ERROR,
                        //   System.Windows.Forms.MessageBoxButtons.OK,
                        //   System.Windows.Forms.MessageBoxIcon.Error);
                    }
                    // CTMのラベル書き換え
                    List<TreeViewItem> items = this.tree.GetAllItems();
                    items.ForEach(item =>
                    {
                        item.IsExpanded = true;
                        string treeId = this.tree.GetId(item);
                        if (collectionInfoMap.ContainsKey(treeId))
                        {
                            CollectionInfo ci = collectionInfoMap[treeId];

                            // CTM Name & Count of CTM
                            var formattedNum = FormatUtil.FormatInteger(ci.Count);
                            string label = "（" + formattedNum + "）";
                            this.tree.UpdateItemLabel(treeId, label, true);
                        }
                    });

                };
                storedCtmInfoGetClient.Get(CmsUrl.GetMmsCtmStoredInfoUrl());

                List<TreeViewItem> items2 = this.tree.GetAllItems();
                foreach (var item in items2)
                {
                    item.IsExpanded = true;
                }
                MainWindow mainWindow = Application.Current.MainWindow as MainWindow;

                if (onInitWithExisting)
                {
                    try
                    {
                        if (mainWindow.CurrentMode == ControlMode.Spreadsheet)
                        {
                            mainWindow.CurrentEditCtrl.SetCtm2();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.Online)
                        {
                            mainWindow.CurrentEditCtrl.SetCtm2();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.Bulky)
                        {
                            mainWindow.CtmDtailsCtrl.BulkyEdit.SetLoadedData(true, DataSourceType.CTM_DIRECT);
                        }
                        else if (mainWindow.CurrentMode == ControlMode.LeadTime)
                        {
                            mainWindow.CtmDtailsCtrl.LeadTime.SetCtm();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.ChimneyChart)
                        {
                            mainWindow.CtmDtailsCtrl.ChimneyChart.SetCtm();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.StockTime)
                        {
                            mainWindow.CtmDtailsCtrl.StockTime.SetCtm();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.FIFO)
                        {
                            mainWindow.CtmDtailsCtrl.FIFO.SetCtm();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.Frequency)
                        {
                            mainWindow.CtmDtailsCtrl.Frequency.SetCtm();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.ContinuousGraph)
                        {
                            mainWindow.CtmDtailsCtrl.ContinuousGraph.SetLoadedData(true, DataSourceType.CTM_DIRECT);
                        }
                        else if (mainWindow.CurrentMode == ControlMode.StreamGraph)
                        {
                            mainWindow.CtmDtailsCtrl.StreamGraph.SetLoadedData(true, DataSourceType.CTM_DIRECT);
                        }
                        else if (mainWindow.CurrentMode == ControlMode.Moment)
                        {
                            mainWindow.CtmDtailsCtrl.Moment.SetCtm();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.Comment)
                        {
                            mainWindow.CtmDtailsCtrl.Comment.SetCtm();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.StatusMonitor)
                        {
                            mainWindow.CtmDtailsCtrl.StatusMonitor.SetCtm2();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.MultiStatusMonitor)
                        {
                            mainWindow.CtmDtailsCtrl.MultiStatusMonitor.SetCtm2();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.ProjectStatusMonitor)
                        {
                            mainWindow.CtmDtailsCtrl.ProjectStatusMonitor.SetCtm2();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.Torque)
                        {
                            mainWindow.CtmDtailsCtrl.Torque.SetCtm2();
                        }
                        else if (mainWindow.CurrentMode == ControlMode.PastStockTime)
                        {
                            mainWindow.CtmDtailsCtrl.PastStockTime.SetCtm();
                        }
                        //AIS_WorkPlace
                        else if (mainWindow.CurrentMode == ControlMode.WorkPlace)
                        {
                            mainWindow.CtmDtailsCtrl.WorkPlace.SetCtm2();
                        }
                    }
                    catch (Exception)
                    {
                        throw new Exception(Properties.Message.AIS_E_027);
                    }
                }
            };
            client.Get(CmsUrlMms.RefDict.Base());
        }
    }


}
