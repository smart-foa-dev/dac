﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    static class SystemId
    {
        public const int Mms = 1;

        public const int Mib = 2;

        public const int Syb = 3;

        public const int CtmPat = 4;

        public const int MmsLightCtmOperation = 5;

        public const int CatalogTree = 6;

        public static string GetSystemCode(int systemId)
        {
            switch (systemId){
                case Mms: return "MMS";
                case Mib: return "MiB";
                case Syb: return "SYB";
                case CtmPat: return "CtmPat";
                case MmsLightCtmOperation: return "MmsLCO";
                case CatalogTree: return "CatalogTree";
                default: return null;
            }
        }
    }


}
