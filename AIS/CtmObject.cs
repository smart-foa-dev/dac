﻿using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    [JsonConverter(typeof(CtmObjectConverter))]
    // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
    //public class CtmObject : CtmNode
    public class CtmObject : CtmNode, ICloneable
    // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End
    {
        public GUID domainId;
        public int revision = 0;
        // public int status2 = (int)MmsCtmStatus2.INITIAL;
        public String ctmClass;

        // CTM Comment MS.Word file use
        public Boolean useCommentFile = false;
        public int commentFileVersion = 0; 
       
        // 
        // Used in old version
        //
        public int status;
        public int workFlag;

        // Use as a flag to show temporary imported CTM table excel viewer
        public Boolean hasModifiedByImportExcel = false;
        public String editStatus;
        public bool isNullObject = false;

        // Excel
        public String updatedBy;
        public long lastUpdateTime;
        public String systemName;

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
        public String missionId;
        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End

        public List<CtmChildNode> children = new List<CtmChildNode>();

        public override bool IsCtm()
        {
            return true;
        }

        public CtmElement GetElement(GUID elementId)
        {
            //logger.Debug("### GetElement " + elementId);
            foreach (var node in new CtmTraverser(this))
            {
                //logger.Debug("!!! node.id=" + node.id);
                if (!node.IsCtm())
                {
                    CtmChildNode nodeC = (CtmChildNode)node;
                    if (nodeC.IsElement())
                    {
                        //logger.Debug("!!! element desu");
                        if (node.id.Equals(elementId))
                        {
                            return (CtmElement)node;
                        }
                    }
                }
            }
            //logger.Debug("!!!! not found!");
            return null;
        }

        public int CountElements()
        {
            int count = 0;
            foreach (var node in new CtmTraverser(this))
            {
                if (!node.IsCtm())
                {
                    CtmChildNode nodeC = (CtmChildNode)node;
                    if (nodeC.IsElement())
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        public List<CtmElement> GetAllElements()
        {
            List<CtmElement> elements = new List<CtmElement>();
            foreach (var node in new CtmTraverser(this))
            {
                if (!node.IsCtm())
                {
                    CtmChildNode nodeC = (CtmChildNode)node;
                    if (nodeC.IsElement())
                    {
                        elements.Add((CtmElement)nodeC);
                    }
                }
            }
            return elements;
        }

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
        public object Clone()
        {
            CtmObject obj = this.MemberwiseClone() as CtmObject;
            obj.children = new List<CtmChildNode>();
            obj.children.AddRange(this.children);
            return obj;
        }
    }
}
