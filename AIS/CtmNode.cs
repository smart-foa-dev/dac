﻿using FoaCore.Common.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FoaCore.Model;
using DAC.Properties;
using System.Globalization;

namespace DAC.Model
{
#if MULTI_LANG
    public class ForceLoginLangObject : MultiLangObject
    {
        private static readonly string ISO_REGION_JP = "jp";
        private static readonly string ISO_REGION_US = "us";

        private static readonly string AIS_RESOURCE_LANG_JA = "ja";
        private static readonly string AIS_RESOURCE_LANG_EN = "en";

        private static readonly string twoLetterISORegionName;
        private static readonly string aisResourceLang;

        static ForceLoginLangObject() {
            twoLetterISORegionName = (new RegionInfo(Resources.Culture.LCID)).TwoLetterISORegionName.ToLower();
            if (ISO_REGION_JP.Equals(twoLetterISORegionName))
            {
                aisResourceLang = AIS_RESOURCE_LANG_JA;
            }
            else if (ISO_REGION_US.Equals(twoLetterISORegionName))
            {
                aisResourceLang = AIS_RESOURCE_LANG_EN;
            }
        }

        public String Get(String lang, Boolean getAny)
        {
            String val = null;
            map.TryGetValue(aisResourceLang, out val);
            if (val != null)
            {
                return val;
            }
            if (!getAny)
            {
                return null;
            }
            List<String> keys = map.Keys.ToList();
            if (keys.Count > 0)
            {
                map.TryGetValue(keys[0], out val);
                return val;
            }
            return "";
        }
        public String Get2(String lang, Boolean getAny)
        {
            String val = null;
            map.TryGetValue(lang, out val);
            if (val != null)
            {
                return val;
            }
            if (!getAny)
            {
                return null;
            }
            List<String> keys = map.Keys.ToList();
            if (keys.Count > 0)
            {
                map.TryGetValue(keys[0], out val);
                return val;
            }
            return "";
        }
    }
#endif

    public class CtmNode
    {
        public GUID id;
        public string cd;
#if MULTI_LANG
        public ForceLoginLangObject displayName = new ForceLoginLangObject();
        public ForceLoginLangObject comment = new ForceLoginLangObject();
#else
        public MultiLangObject displayName = new MultiLangObject();
        public MultiLangObject comment = new MultiLangObject();
#endif
        public GUID parentId;

        public virtual bool IsCtm()
        {
            return false;
        }

        public bool _isParse2JsonUsMember;
    }
}
