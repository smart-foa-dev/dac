﻿using DAC.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grip.Net
{
    static class GripUrl
    {
        /// <summary>
        /// CMSサーバのAPIのベースURL
        /// </summary>
        /// <returns></returns>
        private static string GetCmsBaseUrl()
        {
            return string.Format("http://{0}:{1}/cms/rest/", AisConf.Config.CmsHost, AisConf.Config.CmsPort);
        }

        /// <summary>
        /// ログインのURL(POST)
        /// </summary>
        /// <returns></returns>
        public static string GetLogin()
        {
            return GetCmsBaseUrl() + "login";
        }

        /// <summary>
        /// カタログ言語取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetLangUrl()
        {
            return GetCmsBaseUrl() + "config/catalogLang";
        }

        # region Mms - URL
        /// <summary>
        /// 参照辞書取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetRefDict()
        {
            //return GetMmsBaseUrl() + "refDict";
            return GetCmsBaseUrl() + "refDict/domain";
        }

        /// <summary>
        /// 自ドメインのCtm情報取得のURL(GET)
        /// </summary>
        /// <returns></returns>
        public static string GetMyDomainCtm()
        {
            return GetCmsBaseUrl() + "ctm/myDomain";
        }
        #endregion

        #region Mail - URL
        public static class Mail
        {
            public static string Base()
            {
                return GetCmsBaseUrl() + "mail";
            }

            public static string Attach()
            {
                return Base() + "/" + "attach";
            }
        }
        #endregion

        #region Mib - URL
        public static class Mib
        {
            public static string Base()
            {
                return GetCmsBaseUrl() + "mib";
            }

            public static class Syb
            {
                public static string Base()
                {
                    return Mib.Base() + "/syb";
                }

                public static string User()
                {
                    return Base() + "/user";
                }
            }
        }
        #endregion



        /// <summary>
        /// GRIPサーバのAPIのベースURL
        /// </summary>
        /// <returns></returns>
        private static string GetGripBaseUrl()
        {
            return string.Format("http://{0}:{1}/grip/rest/", AisConf.Config.GripServerHost, AisConf.Config.GripServerPort);
        }

        #region Agent - URL
        public static class Agent
        {
            public static string Base()
            {
                return GetGripBaseUrl() + "folder";
            }

            public static string ByUser()
            {
                return Base() + "/" + "byUser";
            }

            public static string WithMission()
            {
                return Base() + "/" + "withMission";
            }

            public static string All()
            {
                return Base() + "/" + "all";
            }
        }
        #endregion

        #region Mission - URL
        public static class Mission
        {
            public static string Base()
            {
                return GetGripBaseUrl() + "mission";
            }

            public static string All()
            {
                return Base() + "/" + "all";
            }
        }
        #endregion
    }
}
