﻿using DAC.AExcel;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using FoaCore.Common.Util;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_SpreadsheetTemplate.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_SpreadsheetTemplate : BaseControl
    {
        private DateTime startR;
        private DateTime endR;

        /// <summary>
        /// 「Edit」が押されたか否か
        /// </summary>
        private bool gotNewResult = false;

        private bool isFirstFile = true;
        private string excelFilePath = string.Empty;

        public Dictionary<string, int> ElementColumnCount;

        private bool TableIsEmpty = true;
        private DataTable Dt;

        private Dictionary<string, string> conditionDictionary = new Dictionary<string, string>();

        public UserControl_SpreadsheetTemplate(bool isFirstFile = true, string filePath = "")
        {
            InitializeComponent();

            this.Mode = ControlMode.Spreadsheet;
            this.TEMPLATE = "集計表テンプレート.xlsm";

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }

            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }
        }

        #region CTM編集Gridの設定

        public override void SetMission2()
        {
            setMission();
        }

        private void setMission()
        {
            if (!this.conditionDictionary.ContainsKey("ミッションID"))
            {
                return;
            }

            TreeViewItem item = null;
            string missionId = this.conditionDictionary["ミッションID"];
            if (string.IsNullOrEmpty(missionId))
            {
                return;
            }

            this.dataGrid_Output.SetColumn(this.dtTemp);
            this.dataGrid_Output.SetDataTable(this.dtTemp);
            this.dataGrid_Output.AddNewColumn();
            this.dataGrid_Output.Esm.TableIsEmpty = false;

            this.IsSelectedProgramMission = true;

            // Mission Tree
            item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(missionId);
            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_CTM.Focus();
            item.Focus();
        }

        public override void SetGripMission2()
        {
            setGripMission();
        }

        private void setGripMission()
        {
            if (!this.conditionDictionary.ContainsKey("ミッションID"))
            {
                return;
            }

            TreeViewItem item = null;
            string missionId = this.conditionDictionary["ミッションID"];
            if (string.IsNullOrEmpty(missionId))
            {
                return;
            }

            // Mission Tree
            item = this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(missionId);
            if (item == null)
            {
                if (this.SelectedGripMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(this.SelectedGripMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_Grip.Focus();
            item.Focus();

            this.dataGrid_Output.SetColumn(this.dtTemp);
            this.dataGrid_Output.SetDataTable(this.dtTemp);
            this.dataGrid_Output.AddNewColumn();
            this.dataGrid_Output.Esm.TableIsEmpty = false;
        }

        public override void SetCtm2()
        {
            setCtm();
        }

        private void setCtm()
        {
            TreeViewItem item = null;
            if (this.dtId == null)
            {
                return;
            }

            this.mainWindow.TabCtrlDS.SelectedIndex = 0;
            foreach (var rowObject in this.dtId.Rows)
            {
                var row = rowObject as DataRow;
                for (int i = 0; i < row.ItemArray.Length; i++)
                {
                    item = this.mainWindow.treeView_Catalog.GetItem(row.ItemArray[0].ToString());
                    CheckBox cb = this.mainWindow.treeView_Catalog.GetCheckBox(item);
                    cb.IsChecked = true;
                }
            }

            if (item == null)
            {
                return;
            }
            item.IsSelected = true;
            List<string> list = this.mainWindow.treeView_Catalog.GetCheckedId(true);

            this.dataGrid_Output.SetColumn(this.dtTemp);
            this.dataGrid_Output.SetDataTable(this.dtTemp);
            this.dataGrid_Output.AddNewColumn();
            this.dataGrid_Output.Esm.TableIsEmpty = false;
        }

        #endregion

        public override void DataSourceSelectionChanged(string removingCtmName = null)
        {
            if (this.editControl.Ctms == null)
            {
                this.dataGrid_Output.SetInitialDataGrid();
                return;
            }

            if (string.IsNullOrEmpty(removingCtmName) &&
                this.DataSource == DataSourceType.CTM_DIRECT)
            {
                return;
            }

            DataTable dtClone = this.dataGrid_Output.Dt.Clone();
            foreach (object rowObj in this.dataGrid_Output.Dt.Rows)
            {
                DataRow row = rowObj as DataRow;
                if (row == null)
                {
                    continue;
                }

                string ctmName = row[0].ToString();
                if (ctmName == removingCtmName)
                {
                    continue;
                }

                DataRow rowClone = dtClone.NewRow();
                rowClone.ItemArray = row.ItemArray;
                dtClone.Rows.Add(rowClone);
            }

            if (dtClone.Rows.Count < 1)
            {
                this.dataGrid_Output.SetInitialDataGrid();
            }
            else
            {
                this.dataGrid_Output.SetDataTable(dtClone);
            }
        }

        #region Event Handler

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();
            if (string.IsNullOrEmpty(this.excelFilePath))
            {
                this.image_OpenExcel.Deactivate();
            }
            else
            {
                this.image_OpenExcel.Activate();
            }

            BitmapImage biCancel = new BitmapImage();
            biCancel.BeginInit();
            biCancel.UriSource = new Uri(@"/DAC;component/Resources/Image/cancel-to-get.png", UriKind.RelativeOrAbsolute);
            biCancel.EndInit();
            this.image_CancelToGet.Source = biCancel;
            this.image_CancelToGet.IsEnabled = false;
            this.image_CancelToGet.Visibility = System.Windows.Visibility.Visible;

            BitmapImage biGage = new BitmapImage();
            biGage.BeginInit();
            biGage.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar-empty.png", UriKind.RelativeOrAbsolute);
            biGage.EndInit();
            this.image_Gage.Source = biGage;
            this.image_Gage.Visibility = System.Windows.Visibility.Visible;

            //閉じるボタンを追加
            BitmapImage biClose = new BitmapImage();
            biClose.BeginInit();
            biClose.UriSource = new Uri(@"/DAC;component/Resources/Image/close-red.png", UriKind.RelativeOrAbsolute);
            biClose.EndInit();
            this.image_Close.Source = biClose;

            this.editControl.SpreadSheet = this;

            MainWindow m = Application.Current.MainWindow as MainWindow;
            //「CTM選択ボタン」使用可
            m.CtmDtailsCtrl.button_Selection.IsEnabled = true;

            if (this.isFirstFile)
            {
                //this.dataGrid_Output.SetInitialDataGrid();
                this.edit_Grid.Visibility = Visibility.Hidden;
            }
            else
            {
                try
                {
                    readExcelFile(this.excelFilePath);
                }
                catch (Exception ex)
                {
                    string message = Properties.Message.C_EXCEPTION;
//                    string message =
//    @"例外が発生しました。
//{0}
//
//----- StackTrace -----
//{1}";
                    AisMessageBox.DisplayErrorMessageBox(string.Format(message, ex.Message, ex.StackTrace));
                }
            }
        }

        /// <summary>
        /// 「GET」
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button_Get_Click(object sender, RoutedEventArgs e)
        {
            if (this.mainWindow.CtmDtailsCtrl.GetCtmDtailsEmpty() == string.Empty)
            {
                FoaMessageBox.ShowError("AIS_E_009");
                return;
            }

            if (this.edit_Grid.Visibility == Visibility.Visible)
            {
                if (this.dataGrid_Output.Dt.Rows.Count < 1 ||
                    this.dataGrid_Output.Dt.Rows[0].ToString() == string.Empty)
                {
                    FoaMessageBox.ShowError("AIS_E_007");
                    return;
                }
            }

            //if (this.dataGrid_Output.Dt.Rows.Count < 1 ||
            //    this.dataGrid_Output.Dt.Rows[0].ToString() == string.Empty)
            //{
            //    FoaMessageBox.ShowError("AIS_E_007");
            //    return;
            //}

            DateTime start1;
            DateTime end1;
            bool gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out start1, out end1);
            if (!gotTime)
            {
                return;
            }

            preRetrieveCtmData();

            this.DownloadCts = new CancellationTokenSource();
            var resultFolder = await RetrieveCtmData(start1, end1, this.DownloadCts);
            postRetrieveCtmData(resultFolder != null, start1, end1);
            if (resultFolder == null)
            {
                image_OpenExcel.Deactivate();
                return;
            }

            this.tmpResultFolderPath = resultFolder;

            this.gotNewResult = true;
        }

        private void image_OpenExcel_Tap(object sender, RoutedEventArgs e)
        {
            (App.Current.MainWindow as MainWindow).LoadinVisibleChange(false);
            if (!this.gotNewResult)
            {
                //ExcelUtil.StartExcelProcess(this.excelFilePath, true);
                ExcelUtil.OpenExcelFile(this.excelFilePath);

                this.image_OpenExcel.Activate();
                (App.Current.MainWindow as MainWindow).LoadinVisibleChange(true);
                return;
            }

            this.image_CancelToGet.IsEnabled = true;
            openExcelFile(DataSource, this.startR, this.endR);
            //(App.Current.MainWindow as MainWindow).LoadinVisibleChange(true);
        }

        private void image_CancelToGet_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            // Wang Issue MULTITEST-11 20190119 Start
            manualCancel = true;
            // Wang Issue MULTITEST-11 20190119 End

            if (this.Bw != null)
            {
                this.Bw.CancelAsync();
            }

            // Wang Issue MULTITEST-11 20190119 Start
            //DownloadCts.Cancel();
            //DownloadCts = new CancellationTokenSource();
            //CancelGripData();
            //AisUtil.LoadProgressBarImage(this.image_Gage, false);

            //this.button_Get.IsEnabled = true;
            //this.image_CancelToGet.IsEnabled = false;

            AfterCancel();
            // Wang Issue MULTITEST-11 20190119 End
        }

        // Wang Issue MULTITEST-11 20190119 Start
        public override void AfterCancel()
        {
            Dispatcher.BeginInvoke(new Action(() =>
            {
                if (!manualCancel)
                {
                    FoaMessageBox.ShowError("AIS_E_071");
                }
                manualCancel = false;
                restoreUIAfterCancel();
            }));
        }

        private void restoreUIAfterCancel()
        {
            DownloadCts.Cancel();
            DownloadCts = new CancellationTokenSource();
            CancelGripData();
            AisUtil.LoadProgressBarImage(this.image_Gage, false);

            this.button_Get.IsEnabled = true;
            this.image_CancelToGet.IsEnabled = false;
        }
        // Wang Issue MULTITEST-11 20190119 End

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// ドロップされたエレメントを追加
        /// </summary>
        /// <param name="ctmName">CTM名</param>
        /// <param name="headerIndex">追加する場所の列インデックス</param>
        /// <param name="dropedElements">ドロップされたエレメント</param>
        /// <param name="dt">DataTable</param>
        public void addElementToDataGrid(string ctmName, int headerIndex, List<string[]> dropedElements, DataTable dt)
        {
            int loopCount = -1;
            foreach (string[] elementData in dropedElements)
            {
                loopCount++;

                if (1 < dropedElements.Count &&
                    0 < dt.Rows.Count)
                {
                    // CTMがドロップされた
                    bool hitCtm = false;
                    DataRow row = null;
                    foreach (var rowObject in dt.Rows)
                    {
                        row = rowObject as DataRow;
                        if (row[Keywords.CTM_NAME].ToString() != ctmName)
                        {
                            continue;
                        }

                        hitCtm = true;
                        break;
                    }
                    //if (hitCtm &&
                    //    dropedElements.Count == Esm.getAddedElementCount(row))
                    //{
                    //    // 一度追加されたCTM
                    //    return;
                    //}

                    if (hitCtm)
                    {
                        // 追加中CTM
                        int n = 0;
                        for (int i = 1; i < row.ItemArray.Length; i++)
                        {
                            string headerContent = dt.Columns[i].ColumnName;
                            if (!headerContent.StartsWith(elementData[0]) ||
                                headerContent.Length - 3 != elementData[0].Length)
                            {
                                continue;
                            }

                            n = i;
                            break;
                        }
                        if (n == 0)
                        {
                            n = row.ItemArray.Length;
                            if (this.ElementColumnCount.ContainsKey(elementData[0]))
                            {
                                this.ElementColumnCount[elementData[0]]++;
                            }
                            else
                            {
                                this.ElementColumnCount.Add(elementData[0], 1);
                            }

                            int cnt = this.ElementColumnCount[elementData[0]];

                            string content = string.Format("{0} {1:D2}", elementData[0], cnt);
                            dt = addColumn(dt, content);
                        }

                        row = null;
                        foreach (var rowObject in dt.Rows)
                        {
                            row = rowObject as DataRow;
                            if (row[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            // hit
                            break;
                        }

                        row[dt.Columns[n].ColumnName] = elementData[0];
                    }
                    else
                    {
                        // 新規CTM
                        int n = 0;
                        for (int i = 1; i < row.ItemArray.Length; i++)
                        {
                            string headerContent = dt.Columns[i].ColumnName;
                            if (!headerContent.StartsWith(elementData[0]) ||
                                headerContent.Length - 3 != elementData[0].Length)
                            {
                                continue;
                            }

                            n = i;
                            break;
                        }
                        if (n == 0)
                        {
                            n = row.ItemArray.Length;
                            if (this.ElementColumnCount.ContainsKey(elementData[0]))
                            {
                                this.ElementColumnCount[elementData[0]]++;
                            }
                            else
                            {
                                this.ElementColumnCount.Add(elementData[0], 1);
                            }

                            int cnt = this.ElementColumnCount[elementData[0]];
                            string content = string.Format("{0} {1:D2}", elementData[0], cnt);
                            dt = addColumn(dt, content);
                        }

                        // CTMが追加されていない→新規CTM
                        DataRow newRow = dt.NewRow();
                        newRow[Keywords.CTM_NAME] = ctmName;
                        newRow[dt.Columns[n].ColumnName] = elementData[0];
                        dt.Rows.Add(newRow);
                    }
                }
                else
                {
                    if (TableIsEmpty)
                    {
                        // 最初のエレメント
                        dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));
                        dt.Columns.Add(new DataColumn(elementData[0] + " 01"));
                        DataRow row = dt.NewRow();
                        row[Keywords.CTM_NAME] = ctmName;
                        row[elementData[0] + " 01"] = elementData[0];
                        dt.Rows.Add(row);

                        if (this.ElementColumnCount.ContainsKey(elementData[0]))
                        {
                            this.ElementColumnCount[elementData[0]]++;
                        }
                        else
                        {
                            this.ElementColumnCount.Add(elementData[0], 1);
                        }

                        //this.Columns.Clear();
                        this.DataContext = dt.DefaultView;
                        //SetColumn(dt);
                        this.Dt = dt;

                        //this.Columns.Add(this.newColumn);
                        TableIsEmpty = false;
                        continue;
                    }

                    string elementName = string.Empty;
                    foreach (var rowObject in dt.Rows)
                    {
                        DataRow row = rowObject as DataRow;
                        if (row.ItemArray.Length - 1 < headerIndex + loopCount ||
                            row[headerIndex + loopCount].ToString() != elementData[0])
                        {
                            continue;
                        }

                        elementName = elementData[0];
                        break;
                    }

                    if (elementName.StartsWith(elementData[0]) &&
                        elementName.Length - 3 != elementData[0].Length)
                    {
                        // すでにあるカラム(同じエレメント)
                        bool hitCtm = false;
                        bool hitElement = false;
                        DataRow row = null;
                        foreach (var rowObject in dt.Rows)
                        {
                            row = rowObject as DataRow;
                            if (row[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            hitCtm = true;
                            if (row[dt.Columns[headerIndex + loopCount].ColumnName].ToString() == string.Empty)
                            {
                                break;
                            }

                            hitElement = true;
                            continue;
                        }
                        if (row != null &&
                            hitElement)
                        {
                            // 同じCTM、同じエレメントならば何もしない
                            continue;
                        }

                        if (row != null &&
                            hitCtm)
                        {
                            // CTMが既に追加されている
                            row[dt.Columns[headerIndex + loopCount].ColumnName] = elementData[0];
                        }
                        else
                        {
                            // CTMが追加されていない→新規CTM
                            DataRow newRow = dt.NewRow();
                            newRow[Keywords.CTM_NAME] = ctmName;
                            newRow[dt.Columns[headerIndex + loopCount].ColumnName] = elementData[0];
                            dt.Rows.Add(newRow);
                        }
                    }
                    //else if (elementName == newColumn.Header.ToString())
                    else
                    {
                        // 新規カラム
                        bool isAlreadyAddedElement = false;
                        foreach (var rowObject in dt.Rows)
                        {
                            var r = rowObject as DataRow;
                            if (r[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            foreach (var cell in r.ItemArray)
                            {
                                if (cell.ToString() != elementData[0])
                                {
                                    continue;
                                }

                                // すでに同じエレメントが同じCTMに追加されている
                                isAlreadyAddedElement = true;
                                break;
                            }

                            break;
                        }
                        if (isAlreadyAddedElement)
                        {
                            // すでに同じエレメントが同じCTMに追加されている
                            continue;
                        }

                        if (this.ElementColumnCount.ContainsKey(elementData[0]))
                        {
                            this.ElementColumnCount[elementData[0]]++;
                        }
                        else
                        {
                            this.ElementColumnCount.Add(elementData[0], 1);
                        }
                        int cnt = this.ElementColumnCount[elementData[0]];
                        string content = string.Format("{0} {1:D2}", elementData[0], cnt);
                        //dt = insertColumn(dt, content, dt.Columns.Count - 1);     //元なし
                        dt = addColumn(dt, content);

                        bool hit = false;
                        DataRow row = null;
                        foreach (var rowObject in dt.Rows)
                        {
                            row = rowObject as DataRow;
                            if (row[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            hit = true;
                            break;
                        }

                        if (row != null &
                            hit)
                        {
                            row[elementData[0] + " " + cnt.ToString("00")] = elementData[0];
                        }
                        else
                        {
                            DataRow newRow = dt.NewRow();
                            newRow[Keywords.CTM_NAME] = ctmName;
                            newRow[elementData[0] + " " + cnt.ToString("00")] = elementData[0];
                            dt.Rows.Add(newRow);
                        }
                    }
                }
            }

            this.DataContext = dt.DefaultView;
            this.Dt = dt;

        }

        #endregion

        #region CTMデータの取得

        private void preRetrieveCtmData()
        {
            this.button_Get.IsEnabled = false;
            this.image_CancelToGet.IsEnabled = true;
            AisUtil.LoadProgressBarImage(this.image_Gage, true);
        }

        /// <summary>
        /// 終了処理
        /// </summary>
        /// <param name="success"></param>
        private void postRetrieveCtmData(bool success, DateTime start, DateTime end)
        {
            if (success)
            {
                this.image_OpenExcel.Activate();
                this.startR = start;
                this.endR = end;
            }

            AisUtil.LoadProgressBarImage(this.image_Gage, false);
            this.image_CancelToGet.IsEnabled = false;
            this.button_Get.IsEnabled = true;
        }

        #endregion

        #region 既存ファイルの読込み

        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string conditionSheetName = "集計テンプレート";
                SpreadsheetGear.IWorksheet worksheetCondition = AisUtil.GetSheetFromSheetName_SSG(book, conditionSheetName);
                var conditionDic = readCondition(worksheetCondition);

                int firstRowIndex = 5;
                int firstColumnIndex = 0;
                int rowLength = AisUtil.GetRowLength_SSG(worksheetCondition, firstRowIndex, firstColumnIndex);
                int columnLength = AisUtil.GetColumnLength_SSG(worksheetCondition, firstRowIndex, firstColumnIndex);
                DataTable dtElementWithId = AisUtil.CreateDataTableFromExcel_SSG(worksheetCondition, firstRowIndex, firstColumnIndex, rowLength, columnLength);
                DataTable dtElement = AisUtil.DeleteIdRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                DataTable dtId = AisUtil.DeleteNameRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                var elementColumnDic = AisUtil.CountElementColumn(dtElement);

                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var paramMap = readParamSheet(worksheetParam);

                if (paramMap.ContainsKey("収集タイプ") &&
                    !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                {
                    switch (paramMap["収集タイプ"])
                    {
                        case "CTM":
                            this.DataSource = DataSourceType.CTM_DIRECT;
                            break;
                        case "ミッション":
                            this.DataSource = DataSourceType.MISSION;
                            break;
                        case "GRIP":
                            this.DataSource = DataSourceType.GRIP;
                            break;
                        default:
                            break;
                    }
                }

                // set SelectedMission
                if (paramMap.ContainsKey("ミッションID") &&
                    !string.IsNullOrEmpty(paramMap["ミッションID"]))
                {
                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        string missionId = conditionDic["ミッションID"];
                        Mission mission = AisUtil.GetMissionFromId(missionId);
                        this.SelectedMission = mission;
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        string missionId = conditionDic["ミッションID"];
                        GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                        this.SelectedGripMission = mission;
                    }
                }

                this.dtId = dtId;
                this.conditionDictionary = conditionDic;

                this.dataGrid_Output.Dt = dtElement;
                this.dataGrid_Output.ElementColumnCount = elementColumnDic;
                this.dataGrid_Output.Esm = new ElementSelectionManager(this.dataGrid_Output.Dt, this.dataGrid_Output.ElementColumnCount);
                this.dataGrid_Output.SetDataTable(dtElement);
                this.dtTemp = dtElement;

                DateTime dtStart = new DateTime();
                if (DateTime.TryParse(conditionDic["収集開始日時"], out dtStart))
                {
                    this.dateTimePicker_Start.Value = dtStart;
                }

                DateTime dtEnd = new DateTime();
                if (DateTime.TryParse(conditionDic["収集終了日時"], out dtEnd))
                {
                    this.dateTimePicker_End.Value = dtEnd;
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private Dictionary<string, string> readCondition(SpreadsheetGear.IWorksheet worksheetCondition)
        {
            var dic = new Dictionary<string, string>();

            // 収集条件を出力
            SpreadsheetGear.IRange cellsCondition = worksheetCondition.Cells;
            for (int i = 0; i < 10; i++)   // とりあえず10×10のセルを検索
            {
                for (int j = 0; j < 10; j++)
                {
                    // null check
                    if (cellsCondition[i, j].Value == null)
                    {
                        continue;
                    }

                    // ミッション
                    if (cellsCondition[i, j].Text == "ミッション")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // ミッションID
                    if (cellsCondition[i, j].Text == "ミッションID")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // 収集開始日時
                    if (cellsCondition[i, j].Text == "収集開始日時")
                    {
                        string date = cellsCondition[i, j + 1].Text;
                        string time = cellsCondition[i, j + 2].Text;
                        string datetime = date + " " + time;
                        dic.Add(cellsCondition[i, j].Text, datetime);
                        continue;
                    }

                    // 収集終了日時
                    if (cellsCondition[i, j].Text == "収集終了日時")
                    {
                        string date = cellsCondition[i, j + 1].Text;
                        string time = cellsCondition[i, j + 2].Text;
                        string datetime = date + " " + time;
                        dic.Add(cellsCondition[i, j].Text, datetime);
                        continue;
                    }
                }
            }

            return dic;
        }

        private Dictionary<string, string> readParamSheet(SpreadsheetGear.IWorksheet worksheetParam)
        {
            var map = new Dictionary<string, string>();

            SpreadsheetGear.IRange paramCells = worksheetParam.Cells;
            for (int i = 0; i < 30; i++)   // とりあえず30行ほど検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // サーバIPアドレス
                if (paramName == "サーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // サーバポート番号
                if (paramName == "サーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // ミッション
                if (paramName == "ミッション")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // ミッションID
                if (paramName == "ミッションID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバIPアドレス
                if (paramName == "GRIPサーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバポート番号
                if (paramName == "GRIPサーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }
            }

            return map;
        }

        #endregion

        public override void InitializeDataControl()
        {
            this.dataGrid_Output.SetInitialDataGrid();
        }

        #region Excelファイルの作成、オープン

        private void openExcelFile(DataSourceType resultType, DateTime startTime, DateTime endTime)
        {
            var resultFiles = Directory.GetFiles(tmpResultFolderPath, "*.csv");
            if (resultFiles.Length == 0)
            {
                //AisUtil.LoadProgressBarImage(this.image_Gage, false);
                FoaMessageBox.ShowError("AIS_E_006");
                //AISTEMP-129 sunyi 20190401 start
                (App.Current.MainWindow as MainWindow).LoadinVisibleChange(true);
                //AISTEMP-129 sunyi 20190401 end
                return;
            }

            string[] ctmNames = getCtmNamesFromDt(this.dataGrid_Output.Dt);

            //dataGrid_Outputのデータがない場合
            if (this.dataGrid_Output.Dt.Rows.Count == 0)
            {
                TableIsEmpty = true;
                ElementColumnCount = new Dictionary<string, int>();
                Dt = new DataTable();

                for (int i = 0; i < this.mainWindow.CtmDtailsCtrl.dataGrid_Element.Items.Count; i++)
                {
                    var tmprow = this.mainWindow.CtmDtailsCtrl.dataGrid_Element.Items[i] as DataRowView;

                    string content = tmprow[0].ToString();

                    if (content != Properties.Resources.CTM_UNIT)
                    {
                        string arg = this.mainWindow.CtmDtailsCtrl.GetArg(i);

                        string[] itemArray = arg.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        List<string[]> argsData = convertItemArrayToElementData(itemArray);

                        addElementToDataGrid(content, 0, argsData, Dt);

                    }

                }

                ctmNames = getCtmNamesFromDt(Dt);

                this.dataGrid_Output.Dt = Dt;
            }

            string[] ctmIds = getCtmIdsFromNames(ctmNames, this.editControl.Ctms);

            if (this.DataSource != DataSourceType.GRIP)
            {
                if (!existsResultFiles(resultFiles, ctmIds))
                {
                    //AisUtil.LoadProgressBarImage(this.image_Gage, false);
                    FoaMessageBox.ShowError("AIS_E_006");
                    //AISTEMP-129 sunyi 20190401 start
                    (App.Current.MainWindow as MainWindow).LoadinVisibleChange(true);
                    //AISTEMP-129 sunyi 20190401 end
                    return;
                }
            }

            var cparams = new Dictionary<ExcelConfigParam, object>();
            cparams.Add(ExcelConfigParam.START, startTime);
            cparams.Add(ExcelConfigParam.END, endTime);

            if (this.DataSource == DataSourceType.MISSION)
            {
                cparams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                cparams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetCatalogLang(this.SelectedMission));
            }
            else if (this.DataSource == DataSourceType.GRIP)
            {
                cparams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                cparams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetGripCatalogLang(this.SelectedGripMission));
            }

            executeOpenExcelTask(resultType, cparams);
        }

        private void executeOpenExcelTask(DataSourceType resultType, Dictionary<ExcelConfigParam, object> cParams)
        {
            string filePathDest = string.Empty;

            this.button_Get.IsEnabled = false;


            this.Bw = new BackgroundWorker();
            this.Bw.WorkerSupportsCancellation = true;
            var c = this.editControl.Ctms;

            // define the event handlers
            this.Bw.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                // Will be run on background thread.
                // Do your lengthy stuff here -- this will happen in a separate thread to avoid freezing the UI thread

                string filePathSrc = isFirstFile ? System.IO.Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE) : this.excelFilePath;
                filePathDest = this.getExcelFilepath(resultType);
                File.Copy(filePathSrc, filePathDest);

                WriteRetrievedDataToExcel(this.tmpResultFolderPath, this.isFirstFile,
                    filePathSrc, filePathDest, cParams, Bw);

                if (this.Bw.CancellationPending)
                {
                    args.Cancel = true;
                }
            };

            this.Bw.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                //AisUtil.LoadProgressBarImage(this.image_Gage, false);
                // Wang Issue AISTEMP-125 20190121 Start
                Dispatcher.Invoke(() =>
                {
                    mainWindow.LoadinVisibleChange(true);
                });
                // Wang Issue AISTEMP-125 20190121 End

                this.Bw = null;
                if (args.Cancelled)
                {
                    if (File.Exists(filePathDest))
                    {
                        File.Delete(filePathDest);
                    }

                    this.image_OpenExcel.Activate();
                    return;
                }

                if (args.Error != null)  // if an exception occurred during DoWork,
                {
                    // Do your error handling here
                    AisMessageBox.DisplayErrorMessageBox(args.Error.ToString());
                }

                // Do whatever else you need to do after the work is completed.
                // This happens in the main UI thread.
                (App.Current.MainWindow as MainWindow).LoadinVisibleChange(true);
                this.button_Get.IsEnabled = true;
                if (string.IsNullOrEmpty(filePathDest))
                {
                    FoaMessageBox.ShowError("AIS_E_001");
                    return;
                }
                //ExcelUtil.StartExcelProcess(filePathDest, true);
                ExcelUtil.OpenExcelFile(filePathDest);

                this.excelFilePath = filePathDest;
                this.gotNewResult = false;
            };

            // Set loading progress bar image..
            //AisUtil.LoadProgressBarImage(this.image_Gage, true);
            // Wang Issue AISTEMP-125 20190121 Start
            mainWindow.LoadinVisibleChange(false);
            // Wang Issue AISTEMP-125 20190121 End

            Bw.RunWorkerAsync(); // starts the background worker
        }

        private void WriteRetrievedDataToExcel(string folderPath, bool isFirstTime,
            string filePathSrc, string filePathDest, Dictionary<ExcelConfigParam, object> configParams,
            BackgroundWorker bWorker)
        {
            if (isFirstFile)
            {
                var writer = new SSGExcelWriter(this.Mode, this.DataSource, this.editControl.Ctms, this.dataGrid_Output.Dt, bWorker, this.SelectedMission);
                writer.WriteCtmData(filePathDest, folderPath, configParams);
            }
            else
            {
                configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, System.IO.Path.GetFileName(filePathSrc));
                var writer = new InteropExcelWriter(this.Mode, this.DataSource, this.editControl.Ctms, this.dataGrid_Output.Dt, bWorker, this.SelectedMission);
                writer.WriteCtmData(filePathDest, folderPath, configParams);
            }
        }

        private List<string[]> convertItemArrayToElementData(string[] itemArray)
        {
            List<string[]> data = new List<string[]>();

            for (int i = 2; i < itemArray.Length; i += 3)
            {
                string[] ary = new string[]
                {
                    itemArray[i],
                    itemArray[i + 1],
                    itemArray[i + 2]
                };
                data.Add(ary);
            }

            return data;
        }

         private DataTable addColumn(DataTable dt, string columnContent)
        {
            DataTable dtDest = new DataTable();

            int cnt = 0;
            foreach (var column in dt.Columns)
            {
                dtDest.Columns.Add(dt.Columns[cnt].ToString());
                cnt++;
            }
            dtDest.Columns.Add(columnContent);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dtDest.Rows.Add(dt.Rows[i].ItemArray);
            }

            return dtDest;
        }

        #endregion

        private static readonly ILog logger = LogManager.GetLogger(typeof(UserControl_SpreadsheetTemplate));

        private void image_Close_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.edit_Grid.Visibility = Visibility.Hidden;
            this.dataGrid_Output.SetInitialDataGrid();
        }

        // No.493 Input control of period setting item. ----------------Start
        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }
        // No.493 Input control of period setting item. ----------------End

    }
}
