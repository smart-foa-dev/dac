﻿namespace DAC.Model
{
    /// <summary>
    /// ミッションタイプ
    /// </summary>
    public class MissionType
    {
        /// <summary>
        /// モバイル
        /// </summary>
        public const int Mobile = 0;
        /// <summary>
        /// PM(ノーマル)
        /// </summary>
        public const int Normal = 1;
        /// <summary>
        /// PM(オンタイム)
        /// </summary>
        public const int Ontime = 2;
    }
}
