﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using log4net;
using System.Windows.Data;
using System.Globalization;
using DAC.Common;
using FoaCore.Common.Util;
using System.Net;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using Newtonsoft.Json;
using FoaCore.Net;
using System.Net.Http;
using DAC.View.Helpers;
using System.Resources;
using Foa.CommonClass.Util;
using DAC;
using Microsoft.WindowsAPICodePack.Taskbar;

namespace DAC.Model
{


    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        private const string UNEXPECTED_ERROR = "予期しないエラーが発生しました。\n{0}";
        private const string UNOBSERVED_ERROR = "バックグラウンドタスクでエラーが発生しました。\n{0}\n\n----- Debug StackTrace -----\n{1}";
        private const string STACKTRACE_MESSAGE = "\n\n----- StackTrace -----\n{0}";
        private const string WORKPLACE_ARG = "/workplace";

        public string startupApplication;
        public Foa.CommonClass.View.GRIPSplashUI splash = null;
        private object _mutex;

        public App()
        {
            TaskScheduler.UnobservedTaskException += TaskScheduler_UnobservedTaskException;

            //メッセージのリソースクラスを設定
            FoaMessageBox.SetResource(typeof(DAC.Properties.Message));
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            // Check start up mode
            /*foreach (string arg in e.Args)
            {
                if (arg.Equals(WORKPLACE_ARG, StringComparison.OrdinalIgnoreCase))
                {

                    break;
                }
            }*/

            List<string> argset = new List<string>(e.Args);
            string applicationStr = "/ais";
            foreach (var item in argset)
            {
                if (item.Equals("/workplace"))
                {
                    applicationStr = item;
                    TaskbarManager.Instance.ApplicationId = "Foa.FoaStudio.WorkPlace";
                }
                else if (item.Equals("/ais"))
                {
                    applicationStr = item;
                    TaskbarManager.Instance.ApplicationId = "Foa.FoaStudio.Ais";
                }
                else if (item.Equals("/dac"))
                {
                    applicationStr = item;
                    TaskbarManager.Instance.ApplicationId = "Foa.FoaStudio.Dac";
                }
            }
            if (applicationStr.Equals("/workplace",StringComparison.OrdinalIgnoreCase))
            {
                _mutex = new System.Threading.Mutex(false, "Global\\WORKPLACE");
                startupApplication = @"WORKPLACE";
            }
            else if (applicationStr.Equals("/dac", StringComparison.OrdinalIgnoreCase))
            {
                _mutex = new System.Threading.Mutex(false, "Global\\DAC");
                startupApplication = @"DAC";
            }
            else
            {
                _mutex = new System.Threading.Mutex(false, "Global\\AIS");
                startupApplication = @"AIS";
            }
            if ((_mutex as System.Threading.Mutex).WaitOne(0, false) == false)
            {
                MessageBox.Show("多重起動は許容されません");
                this.Shutdown();
            }
            splash = new Foa.CommonClass.View.GRIPSplashUI();

            AisConf.LoadAisConf();

            //
            //カルチャー設定
            //
            // 言語切り替えのためリソースを登録する
            var resourceNames = new[] { "ResourcesInstance", "MessageInstance", "FoaCoreResourcesInstance" };
            foreach (var name in resourceNames)
            {
                var provider = App.Current.TryFindResource(name) as ObjectDataProvider;
                if (provider != null)
                    CultureResources.RegisterProvider(provider.Data.GetType(), provider);
                else
                    logger.Error(string.Format("CultureResource '{0}' not found on App.xaml", name));

            }

            // サポートされていない言語の場合はカルチャーを変更する
            string systemCulture = CultureInfo.CurrentUICulture.TextInfo.CultureName;
            bool supports = CmsDataTable.UILangList.Any(x => x.Value.Equals(systemCulture));
            if (!supports)
            {
                CultureResources.ChangeCulture("en-US");
            }
            if (AisConf.Config.DefaultUiLang == "en")
            {
                CultureResources.ChangeCulture("en-US");
            }

            // 多言語テストのためDefaultを英語にする
            DAC.Common.CultureResources.ChangeCulture("en-US");

            //
            // 3. Login
            //

            // feature/HOME画面改修 Modified by Chu Yimin 2018/10/03 Start
            // パラメータは自動的に行く画面に行きます
            // USERとPassword・Sessionは環境変数で取得する
            var numOfArgs = e.Args.Length;
            var window = new Login();
            //const int PARAMETER_COUNT = 1;

            //Environment.SetEnvironmentVariable("FOAUSERID", "test_admin3");
            //Environment.SetEnvironmentVariable("FOAPASSWD", "foafoa");

            LoginUtil loginUtil = LoginUtil.GetInstance(AisConf.Config.CmsHost, AisConf.Config.CmsPort, AisConf.Config.ProxyURI);

            //string[] parameters = Environment.GetCommandLineArgs();


            window.startUpTemplate = startupApplication;


            if (!loginUtil.HasEnvironmentVariables())
            {
                // Login画面を呼び出す
                window.Callback = () => { window.Show(); };
                window.StartInitialization();
            }
            else
            {
                var catalogLang = AisConf.Config.DefaultCatalogLang;
                var uiLang = AisConf.Config.DefaultUiLang;
                window.Callback = () =>
                {
                    window.ChainLoginUiLang = uiLang;
                    window.Authenticate(null,null,catalogLang, true);
                    splash.Close();
                };
                window.StartInitialization();
            }

            //var userName = Environment.GetEnvironmentVariable(AIS.Properties.Resources.TEXT_FOAUSERID);
            //var passWord = Environment.GetEnvironmentVariable(AIS.Properties.Resources.TEXT_FOAPASSWD);



            /*if (userName != null && passWord != null)
            {

                var catalogLang = AisConf.CatalogLang;
                var uiLang = AisConf.UiLang;
                window.Callback = () =>
                {
                    window.ChainLoginUiLang = uiLang;
                    window.Authenticate(userName, passWord, catalogLang, true);
                };
                window.StartInitialization();
            }
            else
            {
                // Login画面を呼び出す
                window.Callback = () => { window.Show(); };
                window.StartInitialization();
            }
            */

            /*
            if (numOfArgs < 2)
            {
                window.Callback = () => { window.Show(); };
                window.StartInitialization();
            }
            else
            {
                AisConf.IsExternal = true;
                var userId = e.Args[0];
                var passwd = e.Args[1];
                var catalogLang = numOfArgs >= 3 ? e.Args[2] : "ja";
                var uiLang = numOfArgs >= 4 ? e.Args[3] : "ja";
                if (numOfArgs >= 5)
                {
                    AisConf.Config.CmsHost = e.Args[4];
                }
                if (numOfArgs >= 6)
                {
                    AisConf.Config.CmsPort = e.Args[5];
                }
                if (numOfArgs >= 7)
                {
                    AisConf.Config.GripServerHost = e.Args[6];
                }
                if (numOfArgs >= 8)
                {
                    AisConf.Config.GripServerPort = e.Args[7];
                }

                window.Callback = () =>
                {
                    window.ChainLoginUiLang = uiLang;
                    window.Authenticate(userId, passwd, catalogLang,true);
                };
                window.StartInitialization();
            }*/
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            LoginUtil loginUtil = LoginUtil.GetInstance();
            ClsLoginSend.INFO info = new ClsLoginSend.INFO();
            info.UserName = AisConf.UserId;
            info.Password = AisConf.Password;
            logger.Debug("UserName : " + AisConf.UserId);
            if (!AisConf.IsErrorLogin /*&& !string.IsNullOrEmpty(info.UserName)*/)
            {
                loginUtil.LogoutCompleteHandler += (string json) =>
                    {
                        logger.InfoFormat("{0} logout", info.UserName);
                    };
                loginUtil.Logout(startupApplication);
            }
            (_mutex as System.Threading.Mutex).ReleaseMutex();

            /*
            ClsLoginSend loginSend = new ClsLoginSend();
            ClsLoginSend.INFO info = new ClsLoginSend.INFO();
            info.UserName = AisConf.UserId;
            info.Password = AisConf.Password;
            loginSend.User = info;
            if (!AisConf.IsErrorLogin && !string.IsNullOrEmpty(info.UserName))
            {
                string reqJson = JsonConvert.SerializeObject(loginSend);

                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(CmsUrl.GetAisLogoutUrl());
                req.ContentType = "application/json";
                req.Method = "POST";

                if (AisConf.Config.UseProxy)
                {
                    System.Net.WebProxy proxy =
                        new System.Net.WebProxy("http://" + AisConf.Config.ProxyURI);
                    req.Proxy = proxy;
                }


                using (var streamWriter = new StreamWriter(req.GetRequestStream()))
                {
                    streamWriter.Write(reqJson);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var res = (HttpWebResponse)req.GetResponse();
                using (var streamReader = new StreamReader(res.GetResponseStream()))
                {
                    var result = streamReader.ReadToEnd();
                }
                logger.InfoFormat("{0} logout", info.UserName);
            }
            */
        }

        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            Exception ex = getOriginalException(e.Exception);

            //ISSUE_NO.554 sunyi 2018/06/06 start
            //CTMが削除された場合、エラーメッセージを表示します。
            //string msg = string.Format(UNEXPECTED_ERROR, ex.Message);
            string msg = string.Format(DAC.Properties.Message.AIS_E_061, ex.Message);
            //ISSUE_NO.554 sunyi 2018/06/06 end
            string logMsg = msg + string.Format(STACKTRACE_MESSAGE, ex.StackTrace);
            logger.Error(logMsg);

#if DEBUG
            msg += string.Format(STACKTRACE_MESSAGE, ex.StackTrace);
#endif

            AisMessageBox.DisplayErrorMessageBox(msg);
            //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
        }

        private void TaskScheduler_UnobservedTaskException(object sender, UnobservedTaskExceptionEventArgs e)
        {
            Exception ex = e.Exception.InnerException;
            logger.Error("Unhandled exception in background thread!", ex);

#if DEBUG
            string msg = string.Format(UNOBSERVED_ERROR, ex.Message, ex.StackTrace);

            var dispatcher = Application.Current.Dispatcher;
            if (dispatcher.CheckAccess())
            {
                showErrorMessageBox(msg);
            }
            else
            {
                dispatcher.Invoke(() => showErrorMessageBox(msg));
            }

            AisMessageBox.DisplayErrorMessageBox(msg);
            //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
#endif

            e.SetObserved();
        }

        private void showErrorMessageBox(string message)
        {
            AisMessageBox.DisplayErrorMessageBox(message);
            //Xceed.Wpf.Toolkit.MessageBox.Show(message, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// 例外の起源を取得します。
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        private static Exception getOriginalException(Exception ex)
        {
            if (ex.InnerException == null) return ex;

            return getOriginalException(ex.InnerException);
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(App));
    }
}
