﻿using FoaCore.Common.Entity;
using System.Collections.Generic;

namespace DAC.Model
{
    public class MissionCtm : CmsEntity
    {
        public string MissionId { get; set; }
        public string CtmId { get; set; }
        public string ElementCondition { get; set; }

        public List<MissionCtmElement> Elements { get; set; }
    }
}
