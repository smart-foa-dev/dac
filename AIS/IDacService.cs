﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DAC
{
    // メモ: [リファクター] メニューの [名前の変更] コマンドを使用すると、コードと config ファイルの両方で同時にインターフェイス名 "IDacService" を変更できます。
    [ServiceContract]
    public interface IDacService
    {
        [OperationContract]
        DacExecutionResult ValidateDac(string templateFilePath);

        [OperationContract]
        void CreateDac(string templateFilePath, DateTime? start, DateTime? end);
        //Dac_Home sunyi 20190530 start
        [OperationContract]
        void UpdateDac(string templateFilePath, DateTime? start, DateTime? end);
        //Dac_Home sunyi 20190530 end
    }

    [DataContract(Name = "DacExecutionResult")]
    public enum DacExecutionResult
    {
        [EnumMember]
        Success,
        [EnumMember]
        InvalidateDac
    }
}
