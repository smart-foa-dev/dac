﻿using FoaCore.Common;
using FoaCore.Common.Control;
using FoaCore.Common.Converter;
using FoaCore.Common.Entity;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using DAC.View;

namespace DAC.Model
{
    /// <summary>
    /// ミッションツリーコントローラ
    /// </summary>
    public class WorkPlaceTreeController  : MibController
    {
        /// <summary>
        /// JsonTreeView
        /// </summary>
        public JsonTreeView tree;

        /// <summary>
        /// Newly added agent item ID
        /// </summary>
        private string newAgentItemId;

        /// <summary>
        /// ユーザ毎の表示オブジェクトのコレクション
        /// </summary>
        private Dictionary<string, List<LangObject>> userNames;
        // やむなし。持ちたくないが...
        // サーバ側でユーザ名をひっつける処理をしたくない。
        // MibMainにもったほうが楽だが、コントローラにMibMainの参照は持ってはいけない。

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="viewTree"></param>
        public WorkPlaceTreeController(JsonTreeView viewTree)
        {
            tree = viewTree;

            // Jsonキーの設定
            tree.IdKeyName = "id";
            tree.LabelKeyName = "name";
            tree.CollectionKeyName = "children";

            //// アイコンの設定(前)
            //tree.FrontIconFunction = FrontIconFunction;

            //// アイコンの設定(後)
            //tree.BackIconKeyName = "status";
            //tree.BackIconMap = new Dictionary<string, string>(){
            //    { MissionStatus.New.ToString(), "transparent.png" },
            //    { MissionStatus.Running.ToString(), "mission-start.png" },
            //    { MissionStatus.Stopped.ToString(), "mission-stop.png" },
            //    { MissionStatus.Completed.ToString(), "mission-complete.png" },
            //    { MissionStatus.Making.ToString(), "mission-start.png" }
            //};

            //// ラベル
            //tree.LabelFunction = LabelFunction;

            // ラベルの色
            //tree.ForegroundFunction = LabelForegroundFunction;
        }

        /// <summary>
        /// ユーザ名の表示オブジェクトをセットします。
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="names"></param>
        public void AddUserNames(string userId, List<LangObject> names)
        {
            if (userNames == null) userNames = new Dictionary<string, List<LangObject>>();
            userNames[userId] = names;
        }

        /// <summary>
        /// 前のアイコンファンクション
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private string FrontIconFunction(string arg)
        {
            JToken token = JToken.Parse(arg);
            string type = (string)token["t"];
            if (type == NodeType.Mission)
            {
                // ミッション
                int missionType = (int)token["missionType"];
                if (missionType == MissionType.Mobile)
                {
                    return "mission.png";
                }
                else if (missionType == MissionType.Ontime)
                {
                    return "mission-ontime.png";
                }
                else
                {
                    return "mission-pm.png";
                }
            }
            else if (type == NodeType.Mms)
            {
                return "mms.png";
            }
            else if (type == "myAgent")
            {
                return null;
            }
            else
            {
                // エージェント
                if (type == NodeType.Agent)
                {
                    return "user.png";
                }
                else
                {
                    return "users.png";
                }
            }
        }

        /// <summary>
        /// ツリーのラベルファンクション
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private string LabelFunction(string arg)
        {
            JToken token = JToken.Parse(arg);
            string type = (string)token["t"];
            string label = (string)token["name"];
            string displayName = null;
            string catalogLang = LoginInfo.GetInstance().GetCatalogLang();
            JArray displayNameArray = token["displayName"] == null ? new JArray() : string.IsNullOrEmpty((string)token["displayName"]) ? new JArray() : JArray.Parse((string)token["displayName"]);
            for (int i = 0; i < displayNameArray.Count; i++)
            {
                JToken dnToken = displayNameArray[i];
                if (catalogLang == (string)dnToken["lang"])
                {
                    displayName = (string)dnToken["text"];
                    break;
                }
            }

            if (type != NodeType.Mission && type != NodeType.Mms)
            {
                // エージェントの場合、作成ユーザと共有ユーザの名前を表示
                if (userNames == null)
                {
                    return (displayName != null ? displayName : label);
                }

                // 作成ユーザ
                string createdUserId = (string)token["uc"];

                // 共有ユーザ
                var users = token["agentUserList"];
                if (users == null) return (displayName != null ? displayName : label);
                List<AgentUser> agentUserList = JsonConvert.DeserializeObject<List<AgentUser>>(users.ToString());
                if (agentUserList == null)
                {
                    return (displayName != null ? displayName : label);
                }

                agentUserList.ForEach(au =>
                {
                });
            }
            else if (type == NodeType.Mission)
            {
                return (displayName != null ? displayName : label);
            }
            if (displayName != null)
            {
                label = displayName;
            }
            return label;
        }

        /// <summary>
        /// ツリーのラベルカラーファンクション
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Brush LabelForegroundFunction(string arg)
        {
            JToken token = JToken.Parse(arg);
            string type = (string)token["t"];
            if (type == NodeType.Mission)
            {
                bool hasError = (bool)token["hasError"];
                if (hasError)
                {
                    return Brushes.Red;
                }
            }

            return Brushes.Black;
        }

        public Action LoadCallback = null;
        //workplace sunyi 2018/10/11 start
        //dbからjsonを取得
        /// <summary>
        /// ツリーのロード
        /// </summary>
        public void Load()
        {
            var userControl = Application.Current.MainWindow;
            CmsHttpClient client = new CmsHttpClient(userControl);
            client.CompleteHandler += (resJson) =>
            {
                //// 取得したデータをツリー表示

                tree.JsonItemSourceOnlyWorkPlace = resJson;
                if (LoadCallback != null)
                {
                    LoadCallback();
                }
            };

            //client.Get(CmsUrl.GetMissionUrl());
            client.Get(CmsUrl.GetAll());
        }
        //workplace sunyi 2018/10/11 end
        public void Load2(bool displayTimeInfo, bool isRetrievedAllMission, List<string> ctmIds, Action callback)
        {
            if (ctmIds == null || ctmIds.Count == 0)
            {
                tree.JsonItemSource = "[{\"t\":\"mms\",\"name\":\""
                        + "(なし.)" + "\",\"id\":\"" + (new GUID()).ToString() + "\"}]";

                if (LoadCallback != null)
                {
                    LoadCallback();
                }

                if (callback != null)
                {
                    callback();
                }

                return;
            }

            CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
            client.AddParam("shrink", "true");
            foreach (string id in ctmIds)
            {
                client.AddParam("ctmId", id);
            }
            if (isRetrievedAllMission)
            {
                // returns all mission from other users also
                client.AddParam("all", "true");
            }
            client.CompleteHandler += resJson =>
            {
                // 取得したデータをツリー表示
                if (isRetrievedAllMission) // Retrieve all missions to be displayed on Mission Monitor page at Syb
                {
                    if (string.IsNullOrEmpty(resJson) || resJson == "[]")
                    {
                        tree.JsonItemSource = "[{\"t\":\"mms\",\"name\":\""
                                + "(なし.)" + "\",\"id\":\"" + (new GUID()).ToString() + "\"}]";

                        if (LoadCallback != null)
                        {
                            LoadCallback();
                        }

                        if (callback != null)
                        {
                            callback();
                        }

                        return;
                    }

                    HashSet<String> userIdsCreatedMission = new HashSet<string>();
                    JArray missionFolders = JArray.Parse(resJson);
                    foreach (JToken missionFolderToken in missionFolders)
                    {
                        if (missionFolderToken[MmsTvJsonKey.Children] != null)
                        {
                            if (missionFolderToken[MmsTvJsonKey.Children].ToString() != "[]")
                            {
                                JArray missions = (JArray)missionFolderToken[MmsTvJsonKey.Children];
                                foreach (JToken mission in missions)
                                {
                                    if (mission["uc"] != null)
                                    {
                                        userIdsCreatedMission.Add(mission["uc"].ToString());
                                    }
                                }
                            }
                        }
                    }

                    string strItemSource = String.Empty;
                    int countUsers = userIdsCreatedMission.Count;
                    int index = 1;
                    foreach (string userId in userIdsCreatedMission)
                    {
                        string userName = "";
                        List<LangObject> lang;
                        if (userNames.TryGetValue(userId, out lang))
                        {
                            userName = DisplayNameConverter.Convert2(userNames[userId]);
                        }
                        else // The user is not exist, it might be already deleted?
                        {
                            continue; // skip
                        }
                        string myAgent = "My Agent " + userName;
                        string userAgent = userName + " Agent";

                        JArray createdMissionFolder = new JArray();
                        foreach (JToken missionFolderToken in missionFolders)
                        {
                            string type = (string)missionFolderToken["t"];
                            if (type == NodeType.Agent || type == NodeType.AgentShare) // Mission Folder or Shared Mission Folder
                            {
                                if (missionFolderToken["uc"].ToString() == userId)
                                {
                                    if (missionFolderToken[MmsTvJsonKey.Children] != null)
                                    {
                                        if (missionFolderToken[MmsTvJsonKey.Children].ToString() != "[]")
                                        {
                                            createdMissionFolder.Add(missionFolderToken);
                                        }
                                    }
                                }
                            }
                        }

                        // Change the display name of Mission if the start and end time are not equal to 0 (Not Set)
                        foreach (JToken missionFolderToken in createdMissionFolder)
                        {
                            if (missionFolderToken[MmsTvJsonKey.Children] != null)
                            {
                                if (missionFolderToken[MmsTvJsonKey.Children].ToString() != "[]")
                                {
                                    JArray missions = (JArray)missionFolderToken[MmsTvJsonKey.Children];
                                    foreach (JToken mission in missions)
                                    {
                                        mission["originalName"] = mission["name"];
                                        if (displayTimeInfo)
                                        {
                                            if (mission["missionType"].ToString() == "0") // mission type is mobile
                                            {
                                                if (mission["resultType"].ToString() == "1") // mission result type is 1 (Time)
                                                {
                                                    if (mission["startTime"].ToString() != "0" && mission["endTime"].ToString() != "0")
                                                    {
                                                        mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ "
                                                            + UnixTime.ToDateTime((long)mission["endTime"]).ToString() + " ) ";
                                                    }
                                                    else if (mission["startTime"].ToString() != "0" && mission["endTime"].ToString() == "0")
                                                    {
                                                        mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                    }
                                                    else if (mission["startTime"].ToString() == "0" && mission["endTime"].ToString() != "0")
                                                    {
                                                        mission["name"] = mission["name"] + " ( " + " ~ " + UnixTime.ToDateTime((long)mission["endTime"]).ToString() + " ) ";
                                                    }
                                                }
                                                else if (mission["resultType"].ToString() == "2") // mission result type is 2 (Count)
                                                {
                                                    if (mission["startTime"].ToString() != "0") // only start time is available to set ...
                                                    {
                                                        mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                    }
                                                }
                                            }
                                            else if (mission["missionType"].ToString() == "2") // mission type is on-time
                                            {
                                                if (mission["startTime"].ToString() != "0") // only start time is available to set ...
                                                {
                                                    mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        strItemSource += "{\"t\":\"mms\",\"name\":\""
                                + userAgent + "\",\"id\":\"" + (new GUID()).ToString() + "\",\"children\":" + createdMissionFolder.ToString() + "}";

                        if (index != countUsers)
                        {
                            strItemSource += ",";
                        }

                        index++;
                    }

                    tree.JsonItemSource = "[" + strItemSource + "]";
                }
                else
                {
                    string userAgent =
                    LoginInfo.GetInstance().GetLoginUserInfo().UserName + " Agent";

                    string userAgentId = LoginInfo.GetInstance().GetLoginUserInfo().UserId;

                    string jsonAddedUserAgent =
                        "{\"t\":\"mms\",\"name\":\"" + userAgent + "\",\"id\":\"" + userAgentId + "\",\"children\":" + (string.IsNullOrEmpty(resJson) ? "[]" : resJson) + "}";

                    tree.JsonItemSource = jsonAddedUserAgent;

                    // Expand the user agent item as default
                    TreeViewItem userAgentItem = tree.GetItem(userAgentId);
                    userAgentItem.IsExpanded = true;
                    //setToolTip();
                }

                if (!string.IsNullOrEmpty(newAgentItemId))
                {
                    TreeViewItem agentItem = tree.GetItem(newAgentItemId);
                    if (agentItem != null)
                    {
                        agentItem.Focus();
                        agentItem.IsSelected = true;

                        // Re-set to null again
                        newAgentItemId = string.Empty;
                    }
                }

                if (LoadCallback != null)
                {
                    LoadCallback();
                }

                if (callback != null)
                {
                    callback();
                }
            };

            client.Get(CmsUrl.GetMissionByCtmInUseUrl());
        }

        private void setToolTip()
        {
            JArray tvTopArray = JArray.Parse(tree.JsonItemSource);
            JToken token = tvTopArray[0];
            JToken topToken = token;
            JArray children = null;
            if (!string.IsNullOrEmpty(topToken[MmsTvJsonKey.Children].ToString()))
            {
                children = (JArray)topToken[MmsTvJsonKey.Children];
            }
            else
            {
                return;
            }

            if (children.Count > 0)
            {
                foreach (JToken childToken in children)
                {
                    string type = (string)childToken["t"];
                    if (type == NodeType.AgentShare) // Shared Mission Folder
                    {
                        string id = (string)childToken["id"];
                        var users = childToken["agentUserList"];
                        List<AgentUser> agentUserList = JsonConvert.DeserializeObject<List<AgentUser>>(users.ToString());
                        string agentUserLabel = "";
                        int count = agentUserList.Count;
                        int index = 1;
                        agentUserList.ForEach(au =>
                        {
                            if (!string.IsNullOrEmpty(au.UserId))
                            {
                                if (userNames.ContainsKey(au.UserId))
                                {
                                    if (index != count)
                                    {
                                        agentUserLabel += "\u2022 " + DisplayNameConverter.Convert2(userNames[au.UserId]) + "\n";
                                    }
                                    else
                                    {
                                        agentUserLabel += "\u2022 " + DisplayNameConverter.Convert2(userNames[au.UserId]);
                                    }
                                    index++;
                                }    
                            }
                            else
                            {
                                AgentUser autmp = au;
                            }
                        });

                        TreeViewItem tvItem = tree.GetItem(id);
                        ToolTip tooltip = new ToolTip { Content = agentUserLabel };
                        tooltip.FontSize = 14;
                        tooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;
                        tvItem.ToolTip = tooltip;
                    }
                }
            }
        }

        /// <summary>
        /// ミッションが選択されているか判定します。
        /// </summary>
        /// <returns></returns>
        public bool isMissionSelected()
        {
            string type = (string)tree.GetSelectedValueByKey("t");
            if (type == NodeType.Mission)
            {
                return true;
            }

            return false;
        }

        public bool isAgentUserSelected()
        {
            string type = (string)tree.GetSelectedValueByKey("t");
            if (type == NodeType.Mms)
            {
                return true;
            }

            return false;
        }
    }
}
