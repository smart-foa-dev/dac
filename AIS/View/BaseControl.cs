﻿//#define SEARCH_FROM_WEB

using DAC.CtmData;
using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using FoaCore.Common;
using FoaCore.Common.Control;
using FoaCore.Common.Util;
using Grip.Controller.GripMission;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Xceed.Wpf.Toolkit;

namespace DAC.View
{
    public class BaseControl : UserControl
    {
        public ControlMode Mode { get; set; }

        public bool IsSelectedProgramMission { get; set; }

        public BackgroundWorker Bw { get; set; }
        public CancellationTokenSource DownloadCts = new CancellationTokenSource();
        protected string tmpResultFolderPath;
        public DataSourceType DataSource;

        protected string resultDir = Path.Combine(AisConf.BaseDir, "tmp", "excel");

        protected string TEMPLATE;

        protected ElementSelectionManager esm;

        protected MainWindow mainWindow;

        protected CtmDetails editControl;

        public Mission SelectedMission { get; set; }

        public GripMissionCtm SelectedGripMission;

        public bool Initializing = true;

        protected DataTable dtId = null;
        protected DataTable dtTemp = new DataTable();

        // Wang Issue MULTITEST-12 20190119 Start
        protected bool manualCancel = false;
        public virtual void AfterCancel()
        {

        }
        // Wang Issue MULTITEST-12 20190119 End

        protected void Init()
        {
            this.mainWindow = (MainWindow)Application.Current.MainWindow;
            this.editControl = this.mainWindow.CtmDtailsCtrl;
            //初期化場合、「CTM選択ボタン」使用不可
            this.mainWindow.CtmDtailsCtrl.button_Selection.IsEnabled = false;
        }
        
        public Mission GetSelectedMission()
        {
            MissionTreeController ctrlMt = mainWindow.CtrlMt;
            if (!ctrlMt.isMissionSelected())
            {
                // return null;
            }
            var selectedItem = ctrlMt.tree.GetItem(this.editControl.SelectedMissionId);
            var selectedHeader = selectedItem.Header as StackPanel;
            var json = selectedHeader.DataContext.ToString();
            return JsonConvert.DeserializeObject<Mission>(json);
        }

        public GripMissionCtm GetSelectedMission_Grip()
        {
            GripMissionTreeController ctrlGr = mainWindow.CtrlGr;
            if (!ctrlGr.isMissionSelected())
            {
                return null;
            }
            var selectedItem = ctrlGr.tree.SelectedItem as TreeViewItem;
            var selectedHeader = selectedItem.Header as StackPanel;
            var json = selectedHeader.DataContext.ToString();
            return JsonConvert.DeserializeObject<GripMissionCtm>(json);
        }

        protected bool GetStartAndEndTime(System.DateTime? dtStart, System.DateTime? dtEnd,
            out System.DateTime start1, out System.DateTime end1, bool secondsFlag = false)
        {
            start1 = DateTime.MinValue;
            end1 = DateTime.Now;

            DateTime? start = dtStart;
            DateTime? end = dtEnd;

            if (start == null || end == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return false;
            }

            if (end <= start &&
                this.Mode != ControlMode.Dac &&
                // Wang Issue NO.863 20181005 Start
                this.Mode != ControlMode.MultiDac
                // Wang Issue NO.863 20181005 Start
                )
            {
                FoaMessageBox.ShowError("AIS_E_019");
                return false;
            }

            start1 = start ?? DateTime.MinValue;
            end1 = end ?? DateTime.Now;
            if (secondsFlag == true)
            {
                // 秒を表示しない場合はこちらで、秒の部分をclearする。
                start1 = start1.AddSeconds(-start1.Second); // 秒のゴミを秒を0にクリアする。
                end1 = end1.AddSeconds(-end1.Second); 　// 秒をゴミを0にして、1秒前にする。
            }
            start1 = start1.AddMilliseconds(-start1.Millisecond); // milli秒は0だけど、基本0クリアする。
            end1 = end1.AddMilliseconds(999 - end1.Millisecond);　// milli秒は0だけど、基本クリアしてからなぜか +0.999秒する(上記の組み合わせではこれがOK)。
            // もしもするとしたら普通は、-0.001秒と思うんだけど。
            return true;
        }

        // Wang Issue DAC-72 20181225 End

        protected bool GetStartAndEndTime(DateTimePicker pickerStart, DateTimePicker pickerEnd,
            out DateTime start1, out DateTime end1)
        {
            start1 = DateTime.MinValue;
            end1 = DateTime.Now;

            DateTime? start = pickerStart.Value;
            DateTime? end = pickerEnd.Value;

            if (start == null || end == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return false;
            }
            
            if (end <= start &&
                this.Mode != ControlMode.Dac &&
                // Wang Issue NO.863 20181005 Start
                this.Mode != ControlMode.MultiDac
                // Wang Issue NO.863 20181005 Start
                )
            {
                FoaMessageBox.ShowError("AIS_E_019");
                return false;
            }
            
            start1 = start ?? DateTime.MinValue;
            end1 = end ?? DateTime.Now;

            start1 = start1.AddMilliseconds(-start1.Millisecond);
            end1 = end1.AddMilliseconds(999 - end1.Millisecond);

            return true;
        }

        protected bool GetStartAndEndUnixTime(DateTimePicker pickerStart, DateTimePicker pickerEnd,
            out long start1, out long end1)
        {
            start1 = 0;
            end1 = 0;

            DateTime? start = pickerStart.Value;
            DateTime? end = pickerEnd.Value;

            if (start == null || end == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return false;
            }

            if (end <= start)
            {
                FoaMessageBox.ShowError("AIS_E_019");
                return false;
            }

            start1 = UnixTime.ToLong(start);
            end1 = UnixTime.ToLong(end);

            return true;
        }

        protected async Task<string> RetrieveCtmData(DateTime start1, DateTime end1, CancellationTokenSource cToken)
        {
            Task<String> resultT = null;
            string result = null;
            if (mainWindow.TabCtrlDS.SelectedIndex == 0) // Find CTM (RefDict.)
            {
                resultT = GetCtmDirect(start1, end1, cToken);
                DataSource = DataSourceType.CTM_DIRECT;
                result = await resultT;
            }
            else // Find Mission
            {
                if (this.IsSelectedProgramMission)  // Program Mission
                {
                    resultT = GetMission(start1, end1, cToken);
                    DataSource = DataSourceType.MISSION;
                    result = await resultT;
                }
                else // Grip Mission
                {
                    resultT = GetGripData(start1, end1);
                    DataSource = DataSourceType.GRIP;
                    result = await resultT;
                    if (result == null)
                    {
                        FoaMessageBox.ShowError("AIS_E_048");
                    }
                }
            }
            return result;
        }

        protected async Task<string> RetrieveCtmDataFromMissionIds(DateTime start1, DateTime end1, string[] missionIds)
        {
            Task<String> resultT = null;

            foreach (string missionId in missionIds)
            {
                this.DownloadCts = new CancellationTokenSource();

                resultT = GetMission(start1, end1, missionId, this.DownloadCts);
                DataSource = DataSourceType.MISSION;
            }

            return await resultT;
        }

        protected async Task<string> GetMission(DateTime start, DateTime end, CancellationTokenSource cToken)
        {
            Mission mission = this.SelectedMission;
            if (mission == null)
            {
                mission = GetSelectedMission();
                if (mission == null)
                {
                    FoaMessageBox.ShowError("AIS_E_025");
                    return null;
                }
            }

            // Wang Issue MULTITEST-11 20190119 Start
            //var folderPath = AisUtil.GetMission2(mission, start, end, cToken.Token);
            var folderPath = AisUtil.GetMission2(mission, start, end, cToken.Token, this);
            // Wang Issue MULTITEST-11 20190119 End

            this.SelectedMission = mission;

            return await folderPath;
        }
        protected async Task<string> GetMission(DateTime start, DateTime end, string missionId, CancellationTokenSource cToken)
        {
            if (missionId == null)
            {
                FoaMessageBox.ShowError("AIS_E_025");
                return null;
            }

            // Wang Issue MULTITEST-11 20190119 Start
            //var folderPath = AisUtil.GetMission2(missionId, start, end, cToken.Token);
            var folderPath = AisUtil.GetMission2(missionId, start, end, cToken.Token, this);
            // Wang Issue MULTITEST-11 20190119 End

            this.SelectedMission = GetSelectedMission();

            return await folderPath;
        }
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
        //Dac-86 sunyi 20181227 start
        //protected async Task<string> GetMissionCtm(DateTime start, DateTime end, string missionId, CancellationTokenSource cToken)
        protected async Task<string> GetMissionCtm(DateTime start, DateTime end, string missionId, CancellationTokenSource cToken, EventHandler handler = null, Dictionary<string, string> UUIDAndMissionIds = null)
        //Dac-86 sunyi 20181227 end
        {
            if (missionId == null)
            {
                //Dac-86 sunyi 20181227 start
                //FoaMessageBox.ShowError("AIS_E_045");
                if (handler != null)
                {
                    handler(null, new EventArgsEx() { Key = "AIS_E_025" });
                }
                else
                {
                    FoaMessageBox.ShowError("AIS_E_025");
                }
                //Dac-86 sunyi 20181227 end
                return null;
            }
            //Dac-86 sunyi 20181227 start
            //var folderPath = AisUtil.GetMission2(missionId, start, end, cToken.Token);
            // Wang Issue MULTITEST-11 20190119 Start
            //var folderPath = AisUtil.GetMission2(missionId, start, end, cToken.Token, handler);
            var folderPath = AisUtil.GetMission2(missionId, start, end, cToken.Token, this, handler, UUIDAndMissionIds);
            // Wang Issue MULTITEST-11 20190119 End
            //Dac-86 sunyi 20181227 end
            return await folderPath;
        }
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End

        //
        // CTMを直接取得
        //

        /// <summary>
        /// CTMを直接取得
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        protected async Task<string> GetCtmDirect(DateTime start, DateTime end, CancellationTokenSource cToken)
        {
            if (this.editControl.Ctms.Count == 0)
            {
                FoaMessageBox.ShowError("AIS_E_026");
                return null;
            }

            // Wang Issue MULTITEST-11 20190119 Start
            //var folderPath = AisUtil.GetCtmDirectResult2(this.editControl.Ctms, start, end, cToken.Token);
            var folderPath = AisUtil.GetCtmDirectResult2(this.editControl.Ctms, start, end, cToken.Token, this);
            // Wang Issue MULTITEST-11 20190119 End
            return await folderPath;
        }
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
        //AISMM-92 sunyi 20190227 start
        protected async Task<string> GetGripData(DateTime start, DateTime end, JToken missionToken,Dictionary<string,string> UUIDAndMissionIds = null)
        //AISMM-92 sunyi 20190227 end
        {
            long startUnix = UnixTime.ToLong(start);
            long endUnix = UnixTime.ToLong(end);

            if (string.IsNullOrEmpty((string)missionToken["linkDocumentId"]))
            {
                FoaMessageBox.ShowError("AIS_E_039");
                return null;
            }

            var retriever = new GripDataRetriever();
#if SEARCH_FROM_WEB
            // No.696 Modified by Chu Yimin 2018/05/17 Start
            // Trace検索に対応するため
            bool skipsamekey = true;
            // Wang Issue AISTEMP-122 20181228 Start
            //if (int.Parse((string)missionToken["searchType"]) == FoaCore.Model.GripR2.Search.SearchType.Trace)
            int searchType = int.Parse((string)missionToken["searchType"]);
            if (searchType == FoaCore.Model.GripR2.Search.SearchType.Trace || searchType == FoaCore.Model.GripR2.Search.SearchType.Period)
            // Wang Issue AISTEMP-122 20181228 End
            {
                skipsamekey = false;
            }
            return await retriever.GetGripDatatCsvAsyncByMissionId(missionToken, new GUID().ToString(), startUnix, endUnix, GripDataRetriever.SEARCH_CONDITION_AUTO, skipsamekey, DownloadCts.Token, UUIDAndMissionIds);
            //return await retriever.GetGripDatatCsvAsync(mission, new GUID().ToString(), startUnix, endUnix, GripDataRetriever.SEARCH_CONDITION_AUTO, skipsamekey, DownloadCts.Token);
            // No.696 Modified by Chu Yimin 2018/05/17 End
#else
            return await retriever.GetGripDatatCsvAsync(this.SelectedGripMission, startUnix, endUnix, DownloadCts.Token);
#endif
        }
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End

        protected async Task<string> GetGripData(DateTime start, DateTime end)
        {
            long startUnix = UnixTime.ToLong(start);
            long endUnix = UnixTime.ToLong(end);

            GripMissionCtm mission = this.SelectedGripMission;
            if (mission == null)
            {
                mission = GetSelectedMission_Grip();
                if (mission == null)
                {
                    FoaMessageBox.ShowError("AIS_E_025");
                    return null;
                }

            }
            this.SelectedGripMission = mission;
            if (string.IsNullOrEmpty(this.SelectedGripMission.LinkDocumentId))
            {
                FoaMessageBox.ShowError("AIS_E_039");
                return null;
            }

            var retriever = new GripDataRetriever();
#if SEARCH_FROM_WEB
            // No.696 Modified by Chu Yimin 2018/05/17 Start
            // Trace/Period検索に対応するため
            bool skipsamekey = true;
            if (this.SelectedGripMission.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace || this.SelectedGripMission.SearchType == FoaCore.Model.GripR2.Search.SearchType.Period)
            {
                skipsamekey = false;
            }
            return await retriever.GetGripDatatCsvAsync(this.SelectedGripMission, new GUID().ToString(), startUnix, endUnix, GripDataRetriever.SEARCH_CONDITION_AUTO, skipsamekey, DownloadCts.Token);
            // No.696 Modified by Chu Yimin 2018/05/17 End
#else
            return await retriever.GetGripDatatCsvAsync(this.SelectedGripMission, startUnix, endUnix, DownloadCts.Token);
#endif
        }

        protected void CancelGripData()
        {
            var retriever = new GripDataRetriever();
            retriever.CancelExecution();
        }

        protected bool existsResultFiles(string[] resultFiles, string[] ctmIds)
        {
            foreach (string resultFile in resultFiles)
            {
                string fileName = Path.GetFileNameWithoutExtension(resultFile);
                bool hit = false;
                foreach (string ctmId in ctmIds)
                {
                    if (ctmId == fileName)
                    {
                        hit = true;
                        break;
                    }
                }
                if (hit)
                {
                    return true;
                }
            }

            return false;
        }

        protected string[] getCtmNamesFromDt(DataTable dt)
        {
            List<string> ctmNames = new List<string>();

            foreach (var rowObject in dt.Rows)
            {
                var row = rowObject as DataRow;
                if (row == null)
                {
                    return ctmNames.ToArray();
                }

                string ctmName = row.ItemArray[0].ToString();
                ctmNames.Add(ctmName);
            }

            return ctmNames.ToArray();
        }

        protected string[] getCtmIdsFromNames(string[] ctmNames, List<CtmObject> ctms)
        {
            List<string> ctmIds = new List<string>();

            foreach (string ctmName in ctmNames)
            {
                foreach (CtmObject ctm in ctms)
                {
                    string ctmName2 = ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
                    if (ctmName == ctmName2)
                    {
                        ctmIds.Add(ctm.id.ToString());
                    }
                }
            }

            return ctmIds.ToArray();
        }

        protected string getExcelFilepath(DataSourceType resultType)
        {
            string nameMain = null;
            if (resultType == DataSourceType.CTM_DIRECT)
            {
                nameMain = "CTM_RESULT";
            }
            else if (resultType == DataSourceType.MISSION)
            {

                nameMain = AisUtil.GetCatalogLang(this.SelectedMission);
            }
            else
            {
                nameMain = AisUtil.GetGripCatalogLang(this.SelectedGripMission);
            }

            nameMain = nameMain.Replace("[", "");
            nameMain = nameMain.Replace("]", "");

            if (!Directory.Exists(this.resultDir))
            {
                Directory.CreateDirectory(this.resultDir);
            }

            var filename = string.Format("{0}_{1}.xlsm", nameMain, DateTime.Now.ToString("yyyyMMddHHmmss"));
            return Path.Combine(this.resultDir, filename);
        }

        public virtual void SetMission2()
        {

        }

        public virtual void SetGripMission2()
        {

        }      

        public virtual void SetCtm2()
        {

        }

        public void SetCtm()
        {
            TreeViewItem item = null;
            if (this.dtId == null)
            {
                return;
            }

            this.mainWindow.TabCtrlDS.SelectedIndex = 0;
            foreach (var rowObject in this.dtId.Rows)
            {
                var row = rowObject as DataRow;
                for (int i = 0; i < row.ItemArray.Length; i++)
                {
                    item = this.mainWindow.treeView_Catalog.GetItem(row.ItemArray[0].ToString());
                    System.Windows.Controls.CheckBox cb = this.mainWindow.treeView_Catalog.GetCheckBox(item);
                    cb.IsChecked = true;
                }
            }

            if (item == null)
            {
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Catalog.Focus();
            item.Focus();
        }

        public virtual void DataSourceSelectionChanged(string removingCtmName = null)
        {

        }

        public virtual void InitializeDataControl()
        {

        }

        protected void goHome()
        {
            Window home = new Window_Home();
            home.Show();

            this.mainWindow.CtmDtailsCtrl.Online = null;
            this.mainWindow.CtmDtailsCtrl.BulkyEdit = null;
            this.mainWindow.CtmDtailsCtrl.Emergency = null;
            this.mainWindow.Close();
        }

        public DataSourceType CurrentDataSourceType { get { return this.editControl.CurrentDataSourceType; } }

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End

    }
}
