﻿using DAC.Model.Util;
using AisUpdater.Common;
using FoaCore.Net;
using FoaCore.Common.Util;
using log4net;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Diagnostics;
using System.Reflection;
using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace DAC.Model
{
    /// <summary>
    /// Window_Home.xaml の相互作用ロジック
    /// </summary>
    public partial class Window_Home : Window
    {

        public Window_Home()
        {
            InitializeComponent();

            this.panel_R2Box.Visibility = Visibility.Hidden;

            this.lb_OptionMenuListSrc.Visibility = Visibility.Hidden;

            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            this.lb_OptionTemplateListSrc.Visibility = Visibility.Hidden;
            // Wang Issue NO.687 2018/05/18 End

            this.ScrollViewer2.SetExportList(this.panel_R2Box);

            cleanEmergencyBulkyFile();
            cleanTemporaryFileAsync();

            VersionInfo.ToolTip = "AIS Version: " + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion + "\tZAR";
           // AISHomeTitle.ToolTip = VersionInfo.ToolTip;
        }

        private void cleanEmergencyBulkyFile()
        {
            int periodDays = AisConf.Config.CleanPeriodDaysForEmergencyBulkyFile;
            string directoryPath = string.Format(@"{0}\{1}",
                System.Windows.Forms.Application.StartupPath,
                "Emergency");
            if (!Directory.Exists(directoryPath))
            {
                return;
            }

            string[] directories = Directory.GetDirectories(directoryPath);
            foreach (string directory in directories)
            {
                if (Directory.GetCreationTime(directory).AddDays(periodDays) < DateTime.Now)
                {
                    Directory.Delete(directory, true);
                }
            }
        }

        private async void cleanTemporaryFileAsync()
        {
            await Task.Delay(1500);

            // Delete mission folder
            try
            {
                string missionDir = Path.Combine(AisConf.TmpFolderPath, "mission");
                if (Directory.Exists(missionDir))
                {
                    FoaCore.Util.FileUtil.clearFolder(missionDir);
                }
                else
                {
                    Directory.CreateDirectory(missionDir);
                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("別のプロセスで使用されている"))
                {
                    FoaMessageBox.ShowWarning("AIS_E_028");
                }
            }

            // Delete ctm folder
            try
            {
                string ctmDir = Path.Combine(AisConf.TmpFolderPath, "ctm");
                if (Directory.Exists(ctmDir))
                {
                    FoaCore.Util.FileUtil.clearFolder(ctmDir);
                }
                else
                {
                    Directory.CreateDirectory(ctmDir);
                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("別のプロセスで使用されている"))
                {
                    FoaMessageBox.ShowWarning("AIS_E_028");
                }
            }

            // Delete excel folder
            try
            {
                string excelDir = Path.Combine(AisConf.TmpFolderPath, "excel");
                if (Directory.Exists(excelDir))
                {
                    FoaCore.Util.FileUtil.clearFolder(excelDir);
                }
                else
                {
                    Directory.CreateDirectory(excelDir);
                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("別のプロセスで使用されている"))
                {
                    FoaMessageBox.ShowWarning("AIS_E_028");
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (AisConf.IsExternal)
            {
                this.button_ActivateGrip.Visibility = Visibility.Hidden;
            }

            // none ならばGripを使用しない
            if (AisConf.Config.GripServerHost.ToLower() == "none")
            {
                this.button_ActivateGrip.IsEnabled = false;
            }

            ManagerExecUpdater.clearRecordExecuteUpdater(System.AppDomain.CurrentDomain.BaseDirectory);
        }

        public void StartUp(string startupTemplate)
        {
            //App app = Application.Current as App;
            if (startupTemplate.Equals(@"WORKPLACE"))
            {
                MainWindow mainWindow = new MainWindow(ControlMode.WorkPlace, true);
                mainWindow.VersionInfo.ToolTip = this.VersionInfo.ToolTip;
                mainWindow.Icon = new BitmapImage(new Uri("pack://application:,,,/DAC;component/Resources/Image/serializer_icon.ico"));
                mainWindow.Show();
                this.Close();
            }
            else if (startupTemplate.Equals(@"DAC"))
            {
				// Wang New dac flow 20190308 Start
                //MainWindow mainWindow = new MainWindow(ControlMode.MultiDac, true);
                //mainWindow.VersionInfo.ToolTip = this.VersionInfo.ToolTip;
                //mainWindow.Icon = new BitmapImage(new Uri("pack://application:,,,/AIS;component/Resources/Image/dac_icon.ico"));
                //mainWindow.Show();

                Dac_Home dacHome = new Dac_Home();
                dacHome.Show();
				// Wang New dac flow 20190308 End
                this.Close();
            }
            else
            {
                this.Icon = new BitmapImage(new Uri("pack://application:,,,/DAC;component/Resources/Image/ais_icon.ico"));
                this.Show();
            }
        }

        /// <summary>
        /// 「R2 BOX ▼」
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label_R2Box_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.panel_R2Box.Visibility == Visibility.Visible)
            {
                this.panel_R2Box.Visibility = Visibility.Hidden;
            }
            else
            {
                this.panel_R2Box.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// 「Store ▼」
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label_Store_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.lb_OptionMenuListSrc.Visibility == Visibility.Visible)
            {
                this.lb_OptionMenuListSrc.Visibility = Visibility.Hidden;
            }
            else
            {
                this.lb_OptionMenuListSrc.Visibility = Visibility.Visible;
            }
        }

        private void control_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void control_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// ヘルプファイル呼出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpViewer_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            AisUtil.ShowHelp(wfh.Child);
        }

        private void button_Reload_CTM_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.ScrollViewer2.InitItems();
        }

        private void ScrollViewer2_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void button_Reload_CTM_Click(object sender, RoutedEventArgs e)
        {
            this.ScrollViewer2.InitItems();
        }

        private void button_ActivatePat_Click(object sender, RoutedEventArgs e)
        {
            string userId = AisConf.UserId;
            string passwd = AisConf.Password;
            string catalogLang = AisConf.CatalogLang;
            string uiLang = AisConf.UiLang;

            AisUtil.ActivateCmsUi(userId, passwd, catalogLang, uiLang);
        }

        private void button_ActivateGrip_Click(object sender, RoutedEventArgs e)
        {
            string userId = AisConf.UserId;
            string passwd = AisConf.Password;
            string catalogLang = AisConf.CatalogLang;
            string uiLang = AisConf.UiLang;

            AisUtil.ActivateGripR2(userId, passwd, catalogLang, uiLang);
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(Window_Home));

        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        private void label_Option_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.lb_OptionTemplateListSrc.Visibility == Visibility.Visible)
            {
                this.lb_OptionTemplateListSrc.Visibility = Visibility.Hidden;
            }
            else
            {
                if (this.lb_OptionTemplateListSrc.ItemList.HasItems)
                {
                    this.lb_OptionTemplateListSrc.Visibility = Visibility.Visible;
                }
            }
        }
        // Wang Issue NO.687 2018/05/18 End

    }
}
