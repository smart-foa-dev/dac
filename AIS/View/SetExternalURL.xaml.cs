﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DAC.View
{
    /// <summary>
    /// SetExternalURL.xaml の相互作用ロジック
    /// </summary>
    public partial class SetExternalURL : Window
    {
        public string UrlText { get; set; }
        public SetExternalURL()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        private void BtnSetUrl_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(UrlText))
            {

            }
            else
            {
                this.DialogResult = true;
                this.Close();
            }
        }

        private void BtnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }
    }
}
