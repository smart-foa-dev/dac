﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace DAC.View.Helpers
{
    /// <summary>
    /// Ais用メッセージボックス
    /// </summary>
    public class AisMessageBox
    {
        /// <summary>
        /// コンストラクタ
        /// </summary>
        private AisMessageBox()
        {
        }

        /// <summary>
        /// エラーのメッセージボックスを表示
        /// messageがstring.Formatの形式(xxx{0}-{1}等)で定義されている場合、paramを使用します
        /// </summary>
        /// <param name="message">表示するメッセージ</param>
        /// <param name="param">パラメータ</param>
        public static void DisplayErrorMessageBox(string message, object[] param = null)
        {
            if (param != null && param.Length > 0)
            {
                message = message.Replace("\\r\\n", Environment.NewLine) + "\r\n";
                foreach (object o in param)
                {
                    message = message + o.ToString() + "\r\n";
                }
            }
            else
            {
                message = message.Replace("\\r\\n", Environment.NewLine);
            }

            Show(message, Properties.Message.C_ERROR, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        /// <summary>
        /// 警告のメッセージボックスを表示
        /// </summary>
        /// <param name="message">表示するメッセージ</param>
        public static void DisplayWarningMessageBox(string message)
        {
            Show(message.Replace("\\r\\n", Environment.NewLine), Properties.Message.C_WARNING, MessageBoxButton.OK, MessageBoxImage.Warning);
        }
        public static void DisplayWarningMessageBoxWithCaption(string caption, string message)
        {
            Show(message.Replace("\\r\\n", Environment.NewLine), caption, MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        /// <summary>
        /// 情報のメッセージボックスを表示
        /// </summary>
        /// <param name="message">表示するメッセージ</param>
        public static void DisplayInfoMessageBox(string message)
        {
            Show(message.Replace("\\r\\n", Environment.NewLine), Properties.Message.C_INFO, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        /// <summary>
        /// 確認のメッセージボックスを表示します。
        /// </summary>
        /// <param name="message">表示するメッセージ</param>
        /// <returns>クリックしたメッセージボタンのオブジェクト</returns>
        public static MessageBoxResult DisplayConfirmMessageBox(string message)
        {
            return Show(message.Replace("\\r\\n", Environment.NewLine), Properties.Message.C_CONFIRM, MessageBoxButton.YesNo, MessageBoxImage.Question);
        }

        /// <summary>
        /// 確認とキャンセルのメッセージボックスを表示します。
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static MessageBoxResult DisplayOkCancelMessageBox(string message)
        {
            return Show(message.Replace("\\r\\n", Environment.NewLine), Properties.Message.C_CONFIRM, MessageBoxButton.OKCancel, MessageBoxImage.Question);
        }

        /// <summary>
        /// メッセージボックスを表示します. UIスレッド以外から呼ばれた場合はUIスレッドで表示します.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="caption"></param>
        /// <param name="button"></param>
        /// <param name="icon"></param>
        /// <returns></returns>
        private static MessageBoxResult Show(string message, string caption, MessageBoxButton button, MessageBoxImage icon)
        {
            MessageBoxResult result = MessageBoxResult.None;
            if (Application.Current.CheckAccess())
            {
                result = Xceed.Wpf.Toolkit.MessageBox.Show(message, caption, button, icon);
            }
            else
            {
                Application.Current.Dispatcher.Invoke(() =>
                {
                    Window owner = Application.Current.MainWindow;

                    // Added try-catch to avoid Unexpected error due to system Invalid operation exception..
                    try
                    {
                        result = Xceed.Wpf.Toolkit.MessageBox.Show(owner, message, caption, button, icon);
                    }
                    catch (InvalidOperationException)
                    {
                        Application.Current.MainWindow.Dispatcher.BeginInvoke(
                            new Action(() =>
                            {
                                Xceed.Wpf.Toolkit.MessageBox.Show(message, caption, button, icon);
                            })
                        );
                    }
                });
            }
            return result;
        }
    }
}
