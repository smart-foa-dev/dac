﻿using DAC.Model;
using log4net;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using LiteDB;
using System.Collections.Generic;
using System.Windows.Input;
using DAC.Util;
using DAC.Model.Util;
using DAC.AExcel;
using DAC.View.Helpers;

namespace DAC.View.Controls
{
    /// <summary>
    /// ExportList.xaml の相互作用ロジック
    /// </summary>
    public partial class ExportList : UserControl
    {
        private const string EXPORT_DIR_NAME = "export";

        private readonly string exportDir;
        private readonly string registryDir;

        private readonly string exportDbFile;

        private RegisteredItemInfo deleteTarget;

        private static StackPanel dragSource = null;

        public ExportList()
        {
            InitializeComponent();

            var baseDir = AppDomain.CurrentDomain.BaseDirectory;

            exportDir = System.IO.Path.Combine(baseDir, EXPORT_DIR_NAME);
            if (!Directory.Exists(exportDir))
            {
                Directory.CreateDirectory(exportDir);
            }

            registryDir = AisConf.RegistryFolderPath;

            exportDbFile = System.IO.Path.Combine(exportDir, "export.db");

            InitItemList();
        }

        public void InitItemList()
        {
            var itemList = getRegisteredTemplate2();
            this.DataContext = itemList;
        }

        public bool IsExported(string filename)
        {
            using (var db = new LiteDatabase(exportDbFile))
            {
                try
                {
                    var col = db.GetCollection<Filename>("filenames");

                    var result = col.FindOne(x => x.Name == filename);
                    if (result != null)
                    {
                        // 既にある
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                    logger.Error(msg, ex);
                }
            }
            return false;
        }

        #region Event Handler

        private void ListBox_PreviewDrop(object sender, System.Windows.DragEventArgs e)
        {
            var dragData0 = e.Data;
            RegisteredItemInfo dragData = dragData0.GetData("registItem") as RegisteredItemInfo;
           
            // ファイルをコピー
            bool exported = exportExcelFile(dragData.FileName);
            if (!exported)
            {
                return;
            }

            // 表示を更新
            InitItemList();
        }

        private void ItemsControl_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            deleteTarget = null;
            var sp = sender as StackPanel;
            if (sp != null)
            {
                var item = sp.DataContext as RegisteredItemInfo;
                if (item != null)
                {
                    deleteTarget = item;
                    return;
                }
            }
            e.Handled = true;
            return;
        }

        private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (deleteTarget != null)
            {
                deleteExportedItem(deleteTarget.FileName);
            }
        }

        #endregion

        #region Private Method

        private void deleteExportedItem(string filename)
        {
            // DB
            using (var db = new LiteDatabase(exportDbFile))
            {
                try
                {
                    var col = db.GetCollection<Filename>("filenames");
                    int cnt = col.Delete(x => x.Name == filename);
                    if (cnt > 0)
                    {
                        InitItemList();
                    }

                }
                catch (Exception ex)
                {
                    var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                    logger.Error(msg, ex);
                    AisMessageBox.DisplayErrorMessageBox(msg);
                }
            }
        }

        private ObservableCollection<RegisteredItemInfo> getRegisteredTemplate2()
        {
            ObservableCollection<RegisteredItemInfo> lst = new ObservableCollection<RegisteredItemInfo>();

            try
            {
                using (var db = new LiteDatabase(exportDbFile))
                {
                    var col = db.GetCollection<Filename>("filenames");

                    List<string> ghosts = new List<string>();

                    var results = col.FindAll();
                    foreach (var fn in results)
                    {
                        var filepath = System.IO.Path.Combine(registryDir, fn.Name);
                        if (!File.Exists(filepath))
                        {
                            ghosts.Add(fn.Name);
                        }

                        FileInfo fileInfo = new FileInfo(filepath);

                        string[] categoryAndDispName = parseFilename(fileInfo.Name);
                        string cat = categoryAndDispName[0];
                        string dispName = categoryAndDispName[1];
                        ControlMode mode = ControlModeUtil.getControlModeFromString(cat);

                        RegisteredItemInfo itemInfo = new RegisteredItemInfo();
                        itemInfo.Mode = mode;
                        itemInfo.Name = dispName;
                        itemInfo.CreatedTime = fileInfo.CreationTime.ToString("yyyy.M.dd HH:mm");
                        itemInfo.TemplateName = cat;
                        itemInfo.FileName = fileInfo.Name;
                        if (mode == ControlMode.Online || mode == ControlMode.StatusMonitor 
							|| mode == ControlMode.MultiStatusMonitor || mode == ControlMode.Torque)
                        {
                            itemInfo.IsOnline = true;
                        }
                        else if (mode == ControlMode.ChimneyChart ||
                                           mode == ControlMode.StockTime ||
                                           mode == ControlMode.FIFO)
                        {
                            itemInfo.IsOnline = isNecessaryRedFrame(filepath);
                        }
                        lst.Add(itemInfo);
                    }

                    foreach (var target in ghosts)
                    {
                        col.Delete(x => x.Name == target);
                    }
                }
            }
            catch (Exception ex)
            {
                var message = string.Format(Properties.Message.AIS_E_050, ex.Message);
                logger.Error(message, ex);
                AisMessageBox.DisplayErrorMessageBox(message);
            }

            return lst;
        }

        /// <summary>
        /// 赤枠が必要(オンラインで動いている)かを取得
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <returns>true: 赤枠をつける  false: 赤枠をつけない</returns>
        private bool isNecessaryRedFrame(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet paramSheet = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var range = paramSheet.Cells[0, 0, 99, 9];
                var range2 = SSGUtil.GetFindRange(range, "オンライン");
                if (range2 != null)
                {
                    if (range2[0, 1].Value.ToString() == "ONLINE")
                    {
                        return true;
                    }
                }

                return false;
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private string[] parseFilename(string filename)
        {
            string[] rtn = new string[2];

            string kind = filename.Split('_')[0];
            rtn[0] = kind;

            string dispName0 = filename.Substring(kind.Length);
            string dispName = System.IO.Path.GetFileNameWithoutExtension(dispName0);
            rtn[1] = dispName;

            return rtn;
        }

        private bool exportExcelFile(string filename)
        {
            using (var db = new LiteDatabase(exportDbFile))
            {
                try
                {
                    var col = db.GetCollection<Filename>("filenames");

                    var fn = new Filename
                    {
                        Name = filename
                    };

                    var result = col.FindOne(x => x.Name == filename);
                    if (result != null)
                    {
                        // 既にある
                        return false;
                    }

                    col.Insert(fn);
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                    logger.Error(msg, ex);
                    AisMessageBox.DisplayErrorMessageBox(msg);
                    return false;
                }
            }
        }

        #endregion

        public class Filename
        {
            public string Name { get; set; }
        }

        private void StackPanel_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            dragSource = sender as StackPanel;
        }

        private void StackPanel_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (dragSource == sender)
                {
                    var rii = dragSource.DataContext as RegisteredItemInfo;
                    if (rii != null)
                    {
                        var fileColl = new System.Collections.Specialized.StringCollection();
                        var filepath = System.IO.Path.Combine(AisConf.RegistryFolderPath, rii.FileName);
                        fileColl.Add(filepath);
                        var dragData = new DataObject("registItem", rii);
                        dragData.SetFileDropList(fileColl);
                        DragDrop.DoDragDrop(dragSource, dragData, DragDropEffects.Copy);
                    }
                }
            }

            return;
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(ExportList));
    }
}
