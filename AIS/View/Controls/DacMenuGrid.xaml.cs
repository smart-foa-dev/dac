﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAC.Model;
using System;
using System.IO;
using DAC.AExcel;
using System.Collections.Generic;
using DAC.Model.Util;

namespace DAC.View.Controls
{
    /// <summary>
    /// DacMenuGrid.xaml の相互作用ロジック
    /// </summary>
    public partial class DacMenuGrid : UserControl
    {
        private string resultDir = Path.Combine(AisConf.BaseDir, "tmp", "excel");
        private ObservableCollection<AisMenuItem> itemList;

        public DacMenuGrid()
        {
            InitializeComponent();

            itemList = initializeMenuList();
           
            this.DataContext = itemList;
        }

        private ObservableCollection<AisMenuItem> initializeMenuList()
        {
            var itemList = new ObservableCollection<AisMenuItem>();

            var testItem = new AisMenuItem { Row = 0, Col = 0 };
                
            testItem = new AisMenuItem { Row = 0, Col = 0 };
            testItem.Name = "TEXT_MENU_MULTIDAC";
            testItem.Mode = ControlMode.MultiDac;
            itemList.Add(testItem);

            return itemList;
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                var sp = sender as StackPanel;
                var dc = sp.DataContext as AisMenuItem;
                if (dc != null)
                {
                    string dacFile = CreateWorkingFile();
                    Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
                    Microsoft.Office.Interop.Excel.Workbook oWBook = null;
                    try
                    {
                        oXls = new Microsoft.Office.Interop.Excel.Application();
                        oXls.Visible = true; //確認のためエクセルのウィンドウを表示する
                        oWBook = oXls.Workbooks.Open(dacFile);

                        var configParams = new Dictionary<ExcelConfigParam, object>();
                        configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(oWBook.FullName));
                        configParams.Add(ExcelConfigParam.ENABLE_MULTI_DAC, true);
                        InteropExcelWriterDac.InitializeConfigSheetParam(AisUtil.GetSheetFromSheetName_Interop(oWBook, DAC.Util.Keywords.PARAM_SHEET), configParams, "");
                        
                        ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Activate();
                        oXls.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlNormal;
                        ExcelUtil.OpenExcelFileForDAC(oXls);

                        Dac_Home.dacFile = dacFile;
                    }
                    catch (Exception err)
                    {
                        // ログ出力
                        System.Windows.MessageBox.Show("ExcelOpen Exception Error : \n" + err.ToString());
                    }
                    finally
                    {
                        if (oWBook != null)
                        {
                            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                            oWBook = null;
                        }
                        if (oXls != null)
                        {
                            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                            oXls = null;
                        }
                    }
                }
            }
        }

        private string CreateWorkingFile()
        {
            //DACテンプレートのパス
            string filePathSrc = System.IO.Path.Combine(AisConf.Config.DacFilePath, "DACReport.xlsm");

            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            //string fileName = string.Format("{0}_{1}.xlsm", "DAC", DateTime.Now.ToString("yyyyMMddHHmmss"));
            string fileName = string.Format("{0}_{1}.xlsm", DAC.Properties.Resources.TEXT_STD_MULTIDAC, DateTime.Now.ToString("yyyyMMddHHmmss"));
            // Wang Issue NO.687 2018/05/18 End
            string filePathDest = Path.Combine(resultDir, fileName);
            if (!File.Exists(filePathSrc))
            {
                MessageBox.Show("DACテンプレートファイル("+filePathSrc+")が存在しません。FOA Clientのインストールにミスがあります。ご確認ください。", "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return "";
            }
            File.Copy(filePathSrc, filePathDest);
            return filePathDest;
        }

        /*
        private void openMainWindow(ControlMode mode)
        {
            // Wang Issue AISTEMP-123 Start
            if (!mode.Equals(ControlMode.None))
            // Wang Issue AISTEMP-123 End
            {
                MainWindow mainWindow = new MainWindow(mode, true);
                mainWindow.Show();

                Window window = Window.GetWindow(this);
                mainWindow.VersionInfo.ToolTip = (window as Window_Home).VersionInfo.ToolTip;
                window.Close();
            }
        }
        **/
    }
}
