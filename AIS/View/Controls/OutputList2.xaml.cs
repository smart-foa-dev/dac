﻿using DAC.AExcel;
using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View.Helpers;
using DAC.View.Modal;
using FoaCore.Common;
using FoaCore.Common.Model;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Net.Http;
using System.Reflection;

namespace DAC.View.Controls
{
    /// <summary>
    /// OutputList2.xaml の相互作用ロジック
    /// </summary>
    public partial class OutputList2 : UserControl
    {
        private StackPanel dragSrcIC;

        private ExportList exportList;

        private RegisteredItemInfo deleteTarget;

        public OutputList2()
        {
            InitializeComponent();

            //InitItems();
        }

        public void SetExportList(ExportList el)
        {
            exportList = el;
        }

        public void InitItems()
        {
            List<RegisteredItemInfo> items = getRegisteredTemplate();
            this.DataContext = items;
        }

        private void UpdateView()
        {
            this.ic_OutputList2.Items.Refresh();
        }

        private async void UpdateOnlineStatus2(List<RegisteredItemInfo> suspects)
        {
            if (suspects.Count == 0)
            {
                return;
            }

            await Task.Run(() =>
            {
                foreach (var s in suspects)
                {
                    var filepath = System.IO.Path.Combine(AisConf.RegistryFolderPath, s.FileName);
                    bool isOnline = isNecessaryRedFrame(filepath);
                    s.IsOnline = isOnline;
                }
            });
            this.ic_OutputList2.Items.Refresh();
        }

        private List<RegisteredItemInfo> getRegisteredTemplate()
        {
            List<RegisteredItemInfo> data = new List<RegisteredItemInfo>();

            try
            {
                if (!Directory.Exists(AisConf.RegistryFolderPath))
                {
                    Directory.CreateDirectory(AisConf.RegistryFolderPath);
                }

                var suspects = new List<RegisteredItemInfo>();

                var pathes = Directory.GetFiles(AisConf.RegistryFolderPath).OrderByDescending(f => File.GetCreationTime(f));
                foreach (string filePath in pathes)
                {
                    FileInfo fileInfo = new FileInfo(filePath);
                    if (fileInfo.Extension.ToUpper() != ".XLSM" || fileInfo.Attributes.HasFlag(FileAttributes.Hidden))
                    {
                        continue;
                    }

                    string[] categoryAndDispName = parseFilename(fileInfo.Name);
                    string cat = categoryAndDispName[0];
                    string dispName = categoryAndDispName[1];
                    ControlMode mode = ControlModeUtil.getControlModeFromString(cat);

                    RegisteredItemInfo itemInfo = new RegisteredItemInfo();
                    itemInfo.Mode = mode;
                    itemInfo.Name = dispName;
                    itemInfo.CreatedTime = fileInfo.CreationTime.ToString("yyyy.M.dd HH:mm");
                    itemInfo.TemplateName = cat;
                    itemInfo.FileName = fileInfo.Name;
                    itemInfo.IsOnline = false;
                    if (mode == ControlMode.Online || mode == ControlMode.StatusMonitor
						|| mode == ControlMode.MultiStatusMonitor || mode == ControlMode.Torque)
                    {
                        itemInfo.IsOnline = true;
                    }
                    else if (mode == ControlMode.ChimneyChart ||
                        mode == ControlMode.StockTime ||
                        mode == ControlMode.FIFO)
                    {
                        suspects.Add(itemInfo);
                    }

                    // Wang Issue of quick graph 2018/06/01 Start
                    // Can NOT edit when it is a quick graph.
                    //itemInfo.CanEdit = true;
                    if (ControlMode.QuickGraph.Equals(mode))
                    {
                        itemInfo.CanEdit = false;
                    }
                    else
                    {
                        itemInfo.CanEdit = true;
                    }
                    // Wang Issue of quick graph 2018/06/01 End
                    //Multi_Dac sunyi 2018/11/30 start
                    //アウトプット追加
                    if (mode != ControlMode.MultiDac)
                    {
                    //Multi_Dac sunyi 2018/11/30　end
                        data.Add(itemInfo);
                    }
                }

                UpdateOnlineStatus2(suspects);
                setCreateUserName(data);
            }
            catch (Exception ex)
            {
                string message = string.Format(Properties.Message.AIS_E_051, ex.Message);


                message += string.Format("\n" + ex.StackTrace);

                AisMessageBox.DisplayErrorMessageBox(message);

            }

            return data;
        }

        private async void setCreateUserName(List<RegisteredItemInfo> infoList)
        {
            if (infoList.Count < 1)
            {
                return;
            }

            foreach (RegisteredItemInfo info in infoList)
            {
                // Wang Issue of quick graph 2018/06/01 Start
                // Can NOT edit when it is a quick graph.
                //info.CanEdit = true;
                if (ControlMode.QuickGraph.Equals(info.Mode))
                {
                    info.CanEdit = false;
                }
                else
                {
                    info.CanEdit = true;
                }
                // Wang Issue of quick graph 2018/06/01 End
                /*
                Window_Home home = Application.Current.MainWindow as Window_Home;
                if (home == null)
                {
                    continue;
                }
                */

                MainWindow main = Application.Current.MainWindow as MainWindow;
                if (main != null)
                {
                    continue;
                }

                await Task.Run(() =>
                {
                    string filepath = Path.Combine(AisConf.RegistryFolderPath, info.FileName);
                    string[] param = getMissionIdAndCreaterFromExcel(filepath, info.Mode);
                    info.MissionId = param[0];
                    info.CreateUserName = param[1];
                    /*
                    if (string.IsNullOrEmpty(param[0]) ||
                        string.IsNullOrEmpty(param[1]))
                    {
                        info.CanEdit = true;
                    }
                    else
                    {
                        info.CanEdit = true;
                    }
                     * */
                });
            }

            this.ic_OutputList2.Items.Refresh();
        }

        private List<string> getAgentIdList()
        {
            List<string> agentIdList = new List<string>();
            string url = CmsUrl.GetUserList();

            //using (WebClient client = new WebClient())
            using (WebClient client = AisUtil.getProxyWebClient())
            {
                client.Encoding = Encoding.UTF8;
                string response = client.DownloadString(url);

                List<Users> users = JsonConvert.DeserializeObject<List<Users>>(response);
                foreach (Users user in users)
                {
                    agentIdList.Add(user.UserName);
                }
            }

            return agentIdList;
        }

        private string getMissionAgentId(string missionId)
        {
            string agentId = string.Empty;

            string query = string.Format("id={0}", missionId);
            string url = string.Format("{0}?{1}", CmsUrl.GetMissionUrl(), query);

            //using (WebClient client = new WebClient())
            using (WebClient client = AisUtil.getProxyWebClient())
            {
                client.Encoding = Encoding.UTF8;
                string response = client.DownloadString(url);

                Mission mission = JsonConvert.DeserializeObject<Mission>(response);
                agentId = mission.AgentId;
            }

            return agentId;
        }

        /// <summary>
        /// ミッションIDと製作者名を取得
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <returns>0: ミッションID 1: 製作者名</returns>
        private string[] getMissionIdAndCreaterFromExcel(string filePath, ControlMode mode)
        {
            string[] ary = new string[]
            {
                string.Empty,   // ミッションID
                "不明"          // 製作者名
            };
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                string sheetName = Keywords.PARAM_SHEET;
                if (mode == ControlMode.Dac)
                {
                    // sheetName = "USERINFO";
                }
                SpreadsheetGear.IWorksheet sheet = AisUtil.GetSheetFromSheetName_SSG(book, sheetName);

                var range1 = sheet.Cells[0, 0, 99, 9];
                var range2 = SSGUtil.GetFindRange(range1, "ミッションID");
                if (range2 != null &&
                    range2[0, 1].Value != null)
                {
                    ary[0] = range2[0, 1].Value.ToString();
                }

                var range3 = sheet.Cells[0, 0, 99, 9];
                var range4 = SSGUtil.GetFindRange(range3, "製作者名");
                if (range4 != null &&
                    range4[0, 1].Value != null)
                {
                    ary[1] = range4[0, 1].Value.ToString();
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }

            return ary;
        }

        /// <summary>
        /// 赤枠が必要(オンラインで動いている)かを取得
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <returns>true: 赤枠をつける  false: 赤枠をつけない</returns>
        private bool isNecessaryRedFrame(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet paramSheet = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var range = paramSheet.Cells[0, 0, 99, 9];
                var range2 = SSGUtil.GetFindRange(range, "オンライン");
                if (range2 != null)
                {
                    if (range2[0, 1].Value.ToString() == "ONLINE")
                    {
                        return true;
                    }
                }

                return false;
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        /// <summary>
        /// データの取得元(ミッション or CTM or Grip)を取得
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <returns>データの取得元</returns>
        private DataSourceType getDataSourceType(string filePath)
        {
            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePath);
                string sheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet sheet = AisUtil.GetSheetFromSheetName_SSG(workbook, sheetName);
                SpreadsheetGear.IRange cells = sheet.Cells;

                DataSourceType dataSource = DataSourceType.MISSION;
                for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
                {
                    // null check
                    if (cells[i, 0].Value == null)
                    {
                        continue;
                    }

                    // 収集タイプ
                    if (cells[i, 0].Value.ToString() == "収集タイプ")
                    {
                        if (cells[i, 1].Value == null)
                        {
                            return dataSource;
                        }
                        if (cells[i, 1].Value.ToString() == "CTM")
                        {
                            dataSource = DataSourceType.CTM_DIRECT;
                        }
                        else if (cells[i, 1].Value.ToString() == "ミッション")
                        {
                            dataSource = DataSourceType.MISSION;
                        }
                        else // if (cells[i, 1].Value.ToString() == "GRIP")
                        {
                            dataSource = DataSourceType.GRIP;
                        }

                        break;
                    }
                }

                return dataSource;

            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close();
                }

            }
        }

        private string[] parseFilename(string filename)
        {
            string[] rtn = new string[2];

            string kind = filename.Split('_')[0];
            rtn[0] = kind;

            string dispName0 = filename.Substring(kind.Length + 1);
            string dispName = System.IO.Path.GetFileNameWithoutExtension(dispName0);
            rtn[1] = dispName;

            return rtn;
        }

        #region イベントハンドラ

        /// <summary>
        /// ラベルロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label_Loaded(object sender, RoutedEventArgs e)
        {
            Label l = sender as Label;
            if (l == null)
            {
                return;
            }

            // ラベル内に「_」があると、アクセスキー(キーボードショートカット)と判断され、表示されない。
            // 「__」のように2つつなげると表示されるので、ここで置き換えている
            var text = l.Content as string;
            if (text.Contains('_'))
            {
                text = text.Replace("_", "__");
                l.Content = text;
            }
        }

        private void ItemsControl_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.dragSrcIC = sender as StackPanel;

            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                var sp = sender as StackPanel;
                var dc = sp.DataContext as RegisteredItemInfo;
                if (dc != null)
                {
                    //ExcelUtil.StartExcelProcess(dc.FilePath, true);
					// FOA_サーバー化開発 Processing On Server Xj 2018/08/16 Start
                    var filepath = dc.FilePath;
                    if (filepath.Contains("Multi_StatusMonitor"))
                    {
                        Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
                        Microsoft.Office.Interop.Excel.Workbooks oWBooks = null;
                        Microsoft.Office.Interop.Excel.Workbook oWBook = null;
                        Microsoft.Office.Interop.Excel.Worksheet workSheet = null;
                        string workplaceId = string.Empty;
                        JArray workplaceArray = new JArray();
                        JToken workplaceJson = new JObject();


                        try
                        {
                            oXls = new Microsoft.Office.Interop.Excel.Application();
                            oXls.Visible = false; //確認のためエクセルのウィンドウを表示する

                            //エクセルファイルをオープンする
                            oWBooks = oXls.Workbooks;
                            oWBook = oWBooks.Open(filepath,2); // オープンするExcelファイル名

                            workSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWBook.Worksheets[DAC.Util.Keywords.RO_PARAM_SHEET];

                            workplaceId = (string)workSheet.get_Range("B3").Text;

                            oWBook.Close(false, filepath, false);

                            workplaceJson["work_place_id"] = workplaceId;
                            workplaceArray.Add(workplaceJson);

                            CmsHttpClient client = new CmsHttpClient(this);
                            client.CompleteHandler += resJson =>
                            {
                                if (resJson == "0")
                                {
                                    logger.Debug("WorkPlaceId =>「" + workplaceId + "」の参照処理を失敗しました。");
                                }
                            };
                            client.Post(CmsUrl.GetReaseref(), workplaceArray.ToString());
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                        }
                        finally {
                            if (workSheet != null)
                            {
                                System.Runtime.InteropServices.Marshal.ReleaseComObject(workSheet);
                                workSheet = null;
                            }
                            if (oWBook != null) {
                                System.Runtime.InteropServices.Marshal.ReleaseComObject(oWBook);
                                oWBook = null;
                            }
                            if (oWBooks != null) {
                                System.Runtime.InteropServices.Marshal.ReleaseComObject(oWBooks);
                                oWBooks = null;
                            }
                            if (oXls != null) {
                                System.Runtime.InteropServices.Marshal.ReleaseComObject(oXls);
                                oXls = null;
                            }
                        }
                    }
					// FOA_サーバー化開発 Processing On Server Xj 2018/08/16 End
					if (File.Exists(dc.FilePath) == true)
					{
						logger.Debug(MethodBase.GetCurrentMethod() + "() dc.FilePath exists. [" + dc.FilePath + "]");
					}
					else
					{

						logger.Debug(MethodBase.GetCurrentMethod() + "() dc.FilePath does not exist. [" + dc.FilePath + "]");
					}
                    ExcelUtil.OpenExcelFile(dc.FilePath);
                    e.Handled = true;
                }
            }
        }

        private void ItemsControl_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (this.dragSrcIC == sender)
                {
                    var rii = dragSrcIC.DataContext as RegisteredItemInfo;
                    if (rii != null)
                    {
                        var fileColl = new System.Collections.Specialized.StringCollection();
                        var filepath = System.IO.Path.Combine(AisConf.RegistryFolderPath, rii.FileName);
                        fileColl.Add(filepath);
                        var dragData = new DataObject("registItem", rii);
                        dragData.SetFileDropList(fileColl);
                        dragData.SetText("FromOutput");
                        DragDrop.DoDragDrop(dragSrcIC, dragData, DragDropEffects.Copy);
                    }
                }
            }
            return;
        }

        private void ItemsControl_ContextMenuOpening(object sender, System.Windows.Controls.ContextMenuEventArgs e)
        {
            deleteTarget = null;
            var sp = sender as StackPanel;

            if (sp != null)
            {
                var item = sp.DataContext as RegisteredItemInfo;
                if (item != null)
                {
                    deleteTarget = item;

                    // Wang Issue of quick graph 2018/06/01 Start
                    // Items[1] refers to CopyMenuItem. Do NOT think it should be controled by CanEdit attribute.
                    //var cm = sp.ContextMenu.Items[1] as MenuItem;
                    var cm = sp.ContextMenu.Items[2] as MenuItem;
                    // Wang Issue of quick graph 2018/06/01 End
                    if (cm != null)
                    {
                        try
                        {
                            if (deleteTarget.CanEdit)
                            {
                                cm.IsEnabled = true;
                            }
                            else
                            {
                                cm.IsEnabled = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                            logger.Error(msg, ex);
                            AisMessageBox.DisplayErrorMessageBox(msg);
                        }
                    }

                    // Wang Issue of quick graph 2018/06/01 Start
                    // Items[2] refers to EditMenuItem. Do NOT think it should be controled by if it is can be exported.
                    //var dm = sp.ContextMenu.Items[2] as MenuItem;
                    //if (dm != null)
                    //{
                    //    try
                    //    {
                    //        if (!exportList.IsExported(item.FileName))
                    //        {
                    //            dm.IsEnabled = true;
                    //            return;
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                    //        logger.Error(msg, ex);
                    //        AisMessageBox.DisplayErrorMessageBox(msg);
                    //    }
                    //    dm.IsEnabled = false;
                    //}
                    // Wang Issue of quick graph 2018/06/01 End
                    return;
                }
            }
        }

        private void DeleteMenuItem_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (deleteTarget == null)
            {
                return;
            }

            var filepath = deleteTarget.FilePath;

            //開かないのため、SSGに変更 sunyi 201900401 start
            //// FOA_サーバー化開発 Processing On Server Xj 2018/08/16 Start
            //Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
            //Microsoft.Office.Interop.Excel.Workbooks oWBooks = null;
            //Microsoft.Office.Interop.Excel.Workbook oWBook = null;
            SpreadsheetGear.IWorkbook workbook = null;
            //開かないのため、SSGに変更 sunyi 201900401 end

            string onlineId = string.Empty;
            JToken workplaceJson = new JObject();
            try
            {
                //開かないのため、SSGに変更 sunyi 201900401 start
                //oXls = new Microsoft.Office.Interop.Excel.Application();
                //oXls.Visible = false; //確認のためエクセルのウィンドウを表示する

                ////エクセルファイルをオープンする
                //oWBooks = oXls.Workbooks;
                //oWBook = oWBooks.Open(filepath); // オープンするExcelファイル名

                //Microsoft.Office.Interop.Excel.Worksheet workSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWBook.Worksheets[AIS.Util.Keywords.RO_PARAM_SHEET];
                //string fileId = (string)workSheet.get_Range("B2").Text;
                workbook = SpreadsheetGear.Factory.GetWorkbook(filepath);
                SpreadsheetGear.IWorksheet worksheet = AisUtil.GetSheetFromSheetName_SSG(workbook, DAC.Util.Keywords.RO_PARAM_SHEET);
                SpreadsheetGear.IRange cells = worksheet.Cells;
                string fileId = "";
                if (cells[1, 1].Value != null)
                {
                    fileId = cells[1, 1].Value.ToString();
                }

                //oWBook.Close(false, filepath, false);
                //開かないのため、SSGに変更 sunyi 201900401 end

                workplaceJson["file_id"] = fileId;
                workplaceJson["file_type"] = 0;

                CmsHttpClient client = new CmsHttpClient(this);
                client.CompleteHandler += resJson =>
                {
                    if (resJson == "0")
                    {
                        logger.Debug("FileId =>「" + fileId + "」の削除処理を失敗しました。");
                    }
                };
                client.Post(CmsUrl.GetDecreaseref(), workplaceJson.ToString());

            }
            catch
            {
                FoaMessageBox.ShowError("AIS_E_052");
                //                string message =
                //@"Excelファイルへのアクセスに失敗しました。
                //アクセスしたいファイルを開いている場合、閉じてから再度実行してください。";
                //                MessageBox.Show(message);
            }
            finally
            {
                //開かないのため、SSGに変更 sunyi 201900401 start
                if (workbook != null)
                {
                    workbook.Close();
                }
                //if (oWBook != null)
                //{
                //    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                //    oWBook = null;
                //}

                //if (oWBooks != null)
                //{
                //    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBooks);
                //    oWBooks = null;
                //}

                //if (oXls != null)
                //{
                //    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                //    oXls = null;
                //}
                //開かないのため、SSGに変更 sunyi 201900401 end
				try
			    {
	                //ファイル削除
	                File.Delete(filepath);

	                // ファイル一覧をリロード
	                InitItems();
				}
	            catch
	            {
	                FoaMessageBox.ShowError("AIS_E_052");
//                string message =
//@"Excelファイルへのアクセスに失敗しました。
//アクセスしたいファイルを開いている場合、閉じてから再度実行してください。";
//                MessageBox.Show(message);
	            }
            }
			// FOA_サーバー化開発 Processing On Server Xj 2018/08/16 End
        }

        private void NameMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (deleteTarget != null)
            {
                // ファイル名変更
                var reg = (RegisteredItemInfo)deleteTarget.Clone();
                var d = new NameEdit(reg);
                bool? applied = d.ShowDialog();

                if (applied != true)
                {
                    return;
                }

                if (renameFile(deleteTarget, reg))
                {
                    InitItems();
                }
            }
        }

        private bool renameFile(RegisteredItemInfo from, RegisteredItemInfo to)
        {
            var fromPath = from.FilePath;
            var toFilename = string.Format("{0}_{1}.xlsm", to.TemplateName, to.Name);
            var toPath = System.IO.Path.Combine(AisConf.RegistryFolderPath, toFilename);

            try
            {
                File.Move(fromPath, toPath);
                return true;
            }
            catch (ArgumentException ae)
            {

                var msg = string.Format(Properties.Message.AIS_E_053, Properties.Message.AIS_E_070);
                logger.Error(msg, ae);
                AisMessageBox.DisplayErrorMessageBox(msg);
                return false;
            }
            catch (UnauthorizedAccessException uae)
            {
                var msg = string.Format(Properties.Message.AIS_E_053, uae.Message);
                logger.Error(msg, uae);
                AisMessageBox.DisplayErrorMessageBox(msg);
                return false;
            }
            catch (IOException ioe)
            {
                var msg = string.Format(Properties.Message.AIS_E_053, ioe.Message);
                if (AisConf.UiLang.Equals("ja") && ioe.Message.StartsWith("ファイルが別のプロセスで"))
                {
                    msg = string.Format(Properties.Message.AIS_E_053, Properties.Message.AIS_E_069);
                }
                else if (AisConf.UiLang.Equals("ja") && ioe.Message.StartsWith("パスの一部が見つかりません"))
                {
                    msg = string.Format(Properties.Message.AIS_E_053, Properties.Message.AIS_E_070);
                }
                logger.Error(msg, ioe);
                AisMessageBox.DisplayErrorMessageBox(msg);
                return false;
            }
        }

        /// <summary>
        /// 内容修正
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditMenuItem_Click(object sender, RoutedEventArgs e)
        {
            RegisteredItemInfo info = deleteTarget;
            var filepath = info.FilePath;
            DataSourceType isMissionTreeSelected = DataSourceType.MISSION;

            try
            {
                isMissionTreeSelected = getDataSourceType(filepath);
            }
            catch (Exception ex)
            {
                var msg = string.Format(Properties.Message.AIS_E_054, ex.Message);
                msg += string.Format("\n" + ex.StackTrace);
                logger.Error(msg, ex);
                AisMessageBox.DisplayErrorMessageBox(msg);
            }

            MainWindow mainWindow = new MainWindow(info.Mode, false, filepath, isMissionTreeSelected);
            mainWindow.Show();

            Window_Home homeWindow = (Window_Home)System.Windows.Window.GetWindow(this);

            mainWindow.VersionInfo.ToolTip = (homeWindow as Window_Home).VersionInfo.ToolTip;
            homeWindow.Close();
        }

        #endregion

        private static readonly ILog logger = LogManager.GetLogger(typeof(OutputList2));

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            InitItems();
        }

        private void CopyMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (deleteTarget == null)
            {
                return;
            }

            RegisteredItemInfo info = deleteTarget;
            var filepath = info.FilePath;

            // Copy some files to the clipboard.
            List<string> fileList = new List<string>();
            fileList.Add(filepath);
            Clipboard.Clear();
            Clipboard.SetData(DataFormats.FileDrop, fileList.ToArray());
        }
    }
}
