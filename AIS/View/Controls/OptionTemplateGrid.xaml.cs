﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAC.Model;
using System;
using FoaCore.Common;

namespace DAC.View.Controls
{
    /// <summary>
    /// OptionTemplateGrid.xaml の相互作用ロジック
    /// </summary>
    public partial class OptionTemplateGrid : UserControl
    {
        // Wang Issue NO.655 2018/04/20 Start
        // ライセンスのフラグにてオプションテンプレートを表示・非表示
        private const char EnableFlag = '1';
        private const char DisableFlag = '0';
        // Wang Issue NO.655 2018/04/20 End

        private ObservableCollection<AisMenuItem> itemList;

        public OptionTemplateGrid()
        {
            InitializeComponent();

            itemList = initializeMenuList();
           
            this.DataContext = itemList;
        }

        private ObservableCollection<AisMenuItem> initializeMenuList()
        {
            // Wang Issue NO.655 2018/04/20 Start
            // ライセンスのフラグにてオプションテンプレートを表示・非表示

            //var itemList = new ObservableCollection<AisMenuItem>();

            //var testItem = new AisMenuItem { Row = 0, Col = 0 };
            //testItem.Name = "MENU_STD_MULTISTATUSMONITOR"; //"階層ステータスモニター"
            //testItem.Mode = ControlMode.MultiStatusMonitor;
            //itemList.Add(testItem);

            //testItem = new AisMenuItem { Row = 0, Col = 1 };
            //testItem.Name = "MENU_STD_TORQUE";    //"トルク"
            //testItem.Mode = ControlMode.Torque;
            //itemList.Add(testItem);

            //testItem = new AisMenuItem { Row = 0, Col = 2 };
            //testItem.Name = "MENU_GRAPH_STREAMGRAPH"; //トルク波形
            //testItem.Mode = ControlMode.StreamGraph;
            //itemList.Add(testItem);

            ObservableCollection<AisMenuItem> itemList = new ObservableCollection<AisMenuItem>();
            AisMenuItem testItem = null;

            LoginInfo loginInfo = LoginInfo.GetInstance();
            string enableMultiStatus = loginInfo.getEnableMultiStatusMonitor();

            if (!string.IsNullOrEmpty(enableMultiStatus))
            {
                if (enableMultiStatus.Length > 0 && EnableFlag.Equals(enableMultiStatus[0]))
                {
                    testItem = new AisMenuItem { Row = 0, Col = 0 };
                    testItem.Name = "MENU_STD_MULTISTATUSMONITOR"; //"階層ステータスモニター"
                    testItem.Mode = ControlMode.MultiStatusMonitor;
                    itemList.Add(testItem);
                }

                if (enableMultiStatus.Length > 1 && EnableFlag.Equals(enableMultiStatus[1]))
                {
                    testItem = new AisMenuItem { Row = 0, Col = 1 };
                    testItem.Name = "MENU_STD_TORQUE";    //"トルク"
                    testItem.Mode = ControlMode.Torque;
                    itemList.Add(testItem);
                }

                if (enableMultiStatus.Length > 2 && EnableFlag.Equals(enableMultiStatus[2]))
                {
                    testItem = new AisMenuItem { Row = 0, Col = 2 };
                    testItem.Name = "MENU_GRAPH_STREAMGRAPH"; //トルク波形
                    testItem.Mode = ControlMode.StreamGraph;
                    itemList.Add(testItem);
                }
            }
            // Wang Issue NO.655 2018/04/20 End
           
            return itemList;
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                var sp = sender as StackPanel;
                var dc = sp.DataContext as AisMenuItem;
                if (dc != null)
                {
                    openMainWindow(dc.Mode);
                }
            }
        }

        private void openMainWindow(ControlMode mode)
        {
            // Wang Issue AISTEMP-123 Start
            if (!mode.Equals(ControlMode.None))
            // Wang Issue AISTEMP-123 End
            {
                MainWindow mainWindow = new MainWindow(mode, true);
                mainWindow.Show();

                Window window = Window.GetWindow(this);
                mainWindow.ToolTip = window.ToolTip;
                window.Close();
            }
        }
    }
}
