﻿using DAC.CtmData;
using DAC.Model;
using DAC.Model.Util;
//ISSUE_NO.645 sunyi 2018/05/24 start
//DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
using DAC.Util;
//ISSUE_NO.645 sunyi 2018/05/24 end
using DAC.View.Helpers;
using FoaCore.Common.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;

namespace DAC.View.Controls
{
    /// <summary>
    /// BulkyDataGrid.xaml の相互作用ロジック
    /// </summary>
    public partial class BulkyDataGrid : DataGrid
    {
        /* コントロールは親のロード時に紐付けておく */
        public Button GetButton { get; set; }
        public ExcelOpenButton OpenImage { get; set; }
        public Image CancelImage { get; set; }
        public Image GageImage { get; set; }
        public DateTimePicker Start { get; set; }
        public DateTimePicker End { get; set; }

        private BaseControl baseControl;

        public DateTime StartR { get; set; }
        public DateTime EndR { get; set; }

        /// <summary>
        /// 「Edit」が押されたか否か
        /// </summary>
        public bool GotNewResult { get; set; }

        public string TempBulkyFilePath { get; set; }

        public string CtmResultFilepath { get; set; }

        public string MaxCtmResultFilepath { get; set; }

        public string CtmId { get; set; }
        public string MaxCtmId { get; set; }
        public string ElementId { get; set; }

        public string CtmName { get; set; }

        public List<int> SelectedRowNumber { get; set; }
        public List<int> SelectedColumnNumber { get; set; }

        public bool IsEmpty { get; set; }

        public BulkyDataGrid()
        {
            InitializeComponent();

            this.SelectedRowNumber = new List<int>();
            this.SelectedColumnNumber = new List<int>();

            SetInitialDataGrid();
        }

        public void Init(BaseControl bc)
        {
            this.baseControl = bc;
        }

        #region Event Handler

        private async void dataGrid_Bulky_Drop(object sender, DragEventArgs e)
        {
            DateTime start1;
            DateTime end1;
            bool gotTime = AisUtil.GetStartAndEndTime(this.Start, this.End, out start1, out end1);
            if (!gotTime)
            {
                return;
            }

            var dataGrid = sender as System.Windows.Controls.DataGrid;
            if (dataGrid == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string items = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = items.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 5)
            {
                return;
            }

            if ( !FoaDatatype.isBulky(int.Parse(itemArray[4])))  // Bulkyでない
            {
                return;
            }

            int subtype = FoaDatatype.getBulkySubtype(int.Parse(itemArray[4]));
            if (subtype != FoaDatatype.NORMALBULKY)
            {
                FoaMessageBox.ShowError("AIS_E_057");
                return;
            }

            string ctmName = itemArray[0];

            string ctmId = itemArray[1];
            string elementId = itemArray[3];

            this.CancelImage.IsEnabled = true;
            await UpdateBulkyTableOnDrop(ctmId, elementId, start1, end1);

            this.CtmName = ctmName;
        }

        private void columnHeader_Click(object sender, RoutedEventArgs e)
        {
            var columnHeader = sender as System.Windows.Controls.Primitives.DataGridColumnHeader;
            if (columnHeader == null)
            {
                return;
            }

            if (this.IsEmpty)
            {
                return;
            }

            // 選択した列を保存(選択済みだった場合は削除)
            int columnNumber = int.Parse(columnHeader.DisplayIndex.ToString());
            if (this.SelectedColumnNumber.Contains(columnNumber))
            {
                this.SelectedColumnNumber.Remove(columnNumber);
            }
            else
            {
                this.SelectedColumnNumber.Add(columnNumber);
            }

            UpdateCellColorAndGetButton();
        }

        private void rowHeader_Click(object sender, RoutedEventArgs e)
        {
            if (baseControl.Mode.Equals(ControlMode.StreamGraph)) return;

            //ISSUE_NO.645 sunyi 2018/05/24 start
            //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
            if((baseControl.Mode.Equals(ControlMode.Bulky) || baseControl.Mode.Equals(ControlMode.ContinuousGraph))
                && !AisUtil.RowClickIsEnable) return;
            //ISSUE_NO.645 sunyi 2018/05/24 end

            var rowHeader = sender as System.Windows.Controls.Primitives.DataGridRowHeader;
            if (rowHeader == null)
            {
                return;
            }

            if (this.IsEmpty)
            {
                return;
            }

            // 選択した行を保存(選択済みだった場合は削除)
            int rowNumber = int.Parse(rowHeader.Content.ToString()) - 1;
            if (this.SelectedRowNumber.Contains(rowNumber))
            {
                this.SelectedRowNumber.Remove(rowNumber);
            }
            else
            {
                this.SelectedRowNumber.Add(rowNumber);
            }

            UpdateCellColorAndGetButton();
        }

        private void dataGrid_Bulky_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            e.Row.Header = (e.Row.GetIndex() + 1).ToString();
        }

        #endregion

        private async Task UpdateBulkyTableOnDrop(string ctmId, string elementId, DateTime start, DateTime end)
        {
            AisUtil.LoadProgressBarImage(this.GageImage, true);
            try
            {
                await UpdateBulkyTableX(ctmId, elementId, start, end);

                ResetSelection();
                this.OpenImage.Deactivate();
            }
            finally
            {
                AisUtil.LoadProgressBarImage(this.GageImage, false);
            }
        }

        public async Task UpdateBulkyTableX(string ctmId, string elementId, DateTime start, DateTime end)
        {
            string folderPath = await GetBulkyData(ctmId, elementId, start, end);

            if (folderPath == null)
            {
                //System.Windows.Forms.MessageBox.Show("データの取得が中止されました。");
                return;
            }

            string bulkyFile = Path.Combine(folderPath, elementId + ".csv");
            string pathCtm = Path.Combine(folderPath, ctmId + ".csv");

            // 最新のBulkyファイルのみ読み込む
            List<string[]> csvData = readCsvDataOnlyFirstCtm(bulkyFile);

            //AISBUL-31 sunyi 20181221 start
            //jpg優先判定する
            if (this.isNoCsvOnly)
            {
                FoaMessageBox.ShowError("AIS_E_044");
                return;
            }
            //AISBUL-31 sunyi 20181221 end

            //ISSUE_NO.712 sunyi 2018/05/28 start
            //列の内容がCTMによって異なっている場合、エラーをしないで、メッセージにする。
            if (csvData == null)
            {
                FoaMessageBox.ShowWarning("AIS_I_006");
                return;
            }
            //ISSUE_NO.712 sunyi 2018/05/28 start
            if (csvData.Count < 1)
            {
                if (this.isNoBulky)
                {
                    FoaMessageBox.ShowError("AIS_E_029");
                }
                //AISBUL-31 sunyi 20181221 start
                //jpg優先判定する
                //else if (this.isNoCsvOnly)
                //{
                //    FoaMessageBox.ShowError("AIS_E_044");
                //}
                //else
                //{
                //    FoaMessageBox.ShowError("AIS_E_029");
                //}
                //AISBUL-31 sunyi 20181221 end
                return;
            }

            // DataGrid用DataTable作成
            DataTable dt = createDataTable(csvData);
            if (dt.Rows.Count < 1)
            {
                FoaMessageBox.ShowError("AIS_E_029");
                return;
            }

            SetBulkyData(dt);

            this.StartR = start;
            this.EndR = end;
            this.CtmId = ctmId;
            this.ElementId = elementId;

            this.TempBulkyFilePath = bulkyFile;
            this.CtmResultFilepath = pathCtm;
        }

        public async Task CreateMaxCtmInfo(string ctmId, string elementId, DateTime start, DateTime end)
        {
            string folderPath = await GetBulkyData(ctmId, elementId, start, end);

            if (folderPath == null)
            {
                //System.Windows.Forms.MessageBox.Show("データの取得が中止されました。");
                return;
            }
            string pathCtm = Path.Combine(folderPath, ctmId + ".csv");

            this.MaxCtmId = ctmId;
            this.MaxCtmResultFilepath = pathCtm;
        }

        private async Task<string> GetBulkyData(string ctmId, string elementId, DateTime start, DateTime end)
        {
            Task<string> bulkyFile1 = null;

            long start1 = UnixTime.ToLong(start);
            long end1 = UnixTime.ToLong(end);

            if (this.baseControl.CurrentDataSourceType == DataSourceType.CTM_DIRECT) // CTMダイレクト
            {
                bulkyFile1 = getCtmDirectResultAsync(ctmId, elementId, start1, end1);
                this.baseControl.DataSource = DataSourceType.CTM_DIRECT;
            }
            else if (this.baseControl.CurrentDataSourceType == DataSourceType.MISSION) // CTMミッションツリー
            {
                var mainWindow = (MainWindow)Application.Current.MainWindow;
                var missionId = mainWindow.treeView_Mission_CTM.SelectedId;

                bulkyFile1 = getMissionResultAsync(ctmId, elementId, missionId, start1, end1);
                this.baseControl.DataSource = DataSourceType.MISSION;
            }
            else if (this.baseControl.CurrentDataSourceType == DataSourceType.GRIP)  // GRIPミッションツリー
            {
                var mainWindow = (MainWindow)Application.Current.MainWindow;
                var missionId = mainWindow.treeView_Mission_Grip.SelectedId;

                bulkyFile1 = getGripMissionResultAsync(ctmId, elementId, missionId, start1, end1);
                this.baseControl.DataSource = DataSourceType.GRIP;
            }
            else
            {
                throw new TypeAccessException("BaseControl.CurrentDataSourceType is Invalid. [" + Enum.GetName(typeof(DataSourceType), this.baseControl.CurrentDataSourceType) + "]");
            }
            return await bulkyFile1;
        }

        private async Task<string> getCtmDirectResultAsync(string ctmId, string elementId, long start, long end)
        {
            this.CancelImage.IsEnabled = true;

            var retriever = new BulkyDataRetriever();
            // Wang Issue MULTITEST-12 20190119 Start
            //var bulkyFile = retriever.GetCtmDirectResultCsvAsync(ctmId, start, end, this.baseControl.DownloadCts.Token, elementId);
            var bulkyFile = retriever.GetCtmDirectResultCsvAsync(ctmId, start, end, this.baseControl.DownloadCts.Token, elementId, this.baseControl);
            // Wang Issue MULTITEST-12 20190119 End

            var ret = await bulkyFile;
            this.isNoBulky = retriever.isNoBulky;
            this.isNoCsvOnly = retriever.isNoCsvOnly;
            return ret;
        }

        private async Task<string> getMissionResultAsync(string ctmId, string elementId, string missionId, long start, long end)
        {
            var retriever = new BulkyDataRetriever();
            // Wang Issue MULTITEST-12 20190119 Start
            //var folderPath = retriever.GetMissionResultCsvAsync(missionId, start, end, this.baseControl.DownloadCts.Token, ctmId, elementId);
            var folderPath = retriever.GetMissionResultCsvAsync(missionId, start, end, this.baseControl.DownloadCts.Token, ctmId, elementId, this.baseControl);
            // Wang Issue MULTITEST-12 20190119 End

            // 選択されているミッション取得
            this.baseControl.SelectedMission = this.baseControl.GetSelectedMission();



            var ret = await folderPath;
            this.isNoBulky = retriever.isNoBulky;
            this.isNoCsvOnly = retriever.isNoCsvOnly;
            return ret;
        }

        private async Task<string> getGripMissionResultAsync(string ctmId, string elementId, string missionId, long start, long end)
        {
            var retriever = new BulkyDataRetriever();
            var folderPath = retriever.GetGripMissionResultCsvAsync(missionId, start, end, this.baseControl.DownloadCts.Token, ctmId, elementId);

            // 選択されているミッション取得
            this.baseControl.SelectedGripMission = this.baseControl.GetSelectedMission_Grip();

            var ret = await folderPath;
            this.isNoBulky = retriever.isNoBulky;
            this.isNoCsvOnly = retriever.isNoCsvOnly;
            return ret;
        }
        private bool isNoBulky;
        private bool isNoCsvOnly;

        public void SetInitialDataGrid()
        {
            DataTable dt = new DataTable();
            int columnLength = 5;
            int rowLength = 12;

            // Column
            for (int i = 1; i < columnLength; i++)
            {
                string content = string.Format("Column {0}", i);
                var column = new DataColumn(content, Type.GetType("System.String"));
                dt.Columns.Add(column);
            }

            // Row
            for (int i = 0; i < rowLength; i++)
            {
                DataRow row = dt.NewRow();
                dt.Rows.Add(row);
            }
            
            this.ColumnWidth = 75;
            this.DataContext = dt.DefaultView;
            this.IsEmpty = true;

            //setColumn(dt);
        }

        private void setColumn(DataTable dt)
        {
            this.Columns.Clear();
            int columnIndex = 1;
            foreach (var columnObject in dt.Columns)
            {
                DataColumn column = columnObject as DataColumn;
                DataGridTextColumn gridColumn = new DataGridTextColumn();
                gridColumn.Header = string.Format("Column {0}", columnIndex);
                Binding b = new Binding(column.ColumnName);
                gridColumn.Binding = b;
                this.Columns.Add(gridColumn);

                columnIndex++;
            }
        }

        private DataTable createDataTable(List<string[]> data)
        {
            DataTable dt = new DataTable();

            // header
            var map = new Dictionary<string, int>();
            //ISSUE_NO.645 sunyi 2018/05/24 start
            //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
            int colCount = 0;
            //ISSUE_NO.645 sunyi 2018/05/24 end
            foreach (string columnName in data[0])
            {
                //ISSUE_NO.645 sunyi 2018/05/24 start
                //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                if (colCount >= AisConf.Config.BulkyMaxColsCount) break;
                //ISSUE_NO.645 sunyi 2018/05/24 end
                string workDoc = columnName;
                if (dt.Columns.Contains(columnName))
                {
                    /*
                    throw new Exception(
@"Bulkyデータの表示に失敗しました。
ファイルの形式や文字コードを確認して下さい。");
                    */
                    if (!map.ContainsKey(columnName))
                    {
                        map[columnName] = 1;
                    }
                    map[columnName]++;
                    workDoc = columnName + map[columnName].ToString();
                }
                dt.Columns.Add(workDoc);
                //ISSUE_NO.645 sunyi 2018/05/24 start
                //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                colCount++;
                //ISSUE_NO.645 sunyi 2018/05/24 end
            }

            //ISSUE_NO.645 sunyi 2018/05/24 start
            //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
            AisUtil.RowClickIsEnable = true;
            //if (baseControl.Mode.Equals(ControlMode.StreamGraph))
            if (baseControl.Mode.Equals(ControlMode.StreamGraph) || baseControl.Mode.Equals(ControlMode.Bulky) || baseControl.Mode.Equals(ControlMode.ContinuousGraph))
            //ISSUE_NO.645 sunyi 2018/05/24 end
            {
                // row
                for (int i = 1; i < data.Count && i < AisConf.Config.BulkyMaxRowsCount + 1; i++)
                {
                    DataRow row = dt.NewRow();
                    //ISSUE_NO.645 sunyi 2018/05/24 start
                    //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                    //for (int j = 0; j < data[i].Length; j++)
                    for (int j = 0; j < data[i].Length && j < AisConf.Config.BulkyMaxColsCount; j++)
                    //ISSUE_NO.645 sunyi 2018/05/24 end
                    {
                        row[j] = data[i][j];
                    }

                    dt.Rows.Add(row);
                }

                //ISSUE_NO.645 sunyi 2018/05/24 start
                //DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
                //if (data.Count > AisConf.Config.BulkyMaxCount)
                //{
                //    FoaMessageBox.ShowInfo("AIS_I_003", AisConf.Config.BulkyMaxCount);
                //}
                // row
                if (data.Count > AisConf.Config.BulkyMaxRowsCount + 1)
                {
                    if (baseControl.Mode.Equals(ControlMode.StreamGraph))
                    {
                        FoaMessageBox.ShowInfo("AIS_I_003", AisConf.Config.BulkyMaxRowsCount);
                    }
                    else
                    {
                        FoaMessageBox.ShowInfo("AIS_I_005", AisConf.Config.BulkyMaxRowsCount);
                    }
                    AisUtil.RowClickIsEnable = false;
                }
                // col
                if (data[0].Length > AisConf.Config.BulkyMaxColsCount)
                {
                    if (!baseControl.Mode.Equals(ControlMode.StreamGraph))
                    {
                        FoaMessageBox.ShowInfo("AIS_I_004", AisConf.Config.BulkyMaxColsCount);
                    }
                }
                //ISSUE_NO.645 sunyi 2018/05/24 end
            }
            else
            {
                // row
                for (int i = 1; i < data.Count; i++)
                {
                    DataRow row = dt.NewRow();
                    for (int j = 0; j < data[i].Length; j++)
                    {
                        row[j] = data[i][j];
                    }

                    dt.Rows.Add(row);
                }
            }

            return dt;
        }

        private List<string[]> readCsvDataOnlyFirstCtm(string filePath)
        {
            List<string[]> data = new List<string[]>();
            using (StreamReader sr = new StreamReader(filePath, AisUtil.Utf8))
            {
                // ヘッダーの文字列を保存
                // 非定型の場合…？
                string[] header = new string[0];

                while (-1 < sr.Peek())
                {
                    string[] row = sr.ReadLine().Split(',');
                    //ISSUE_NO.712 sunyi 2018/05/28 start
                    //列の内容がCTMによって異なっている場合、エラーをしないで、メッセージにする。
                    if (row.Length != header.Length && header.Length > 0)
                    {
                        return null;
                    }
                    //ISSUE_NO.712 sunyi 2018/05/28 start
                    //列の内容がCTMによって異なっている場合、エラーをしないで、メッセージにする。
                    // row の要素が全て header と同じであれば次のCTMであるとみなして読み込み終了
                    bool isHeader = true;
                    for (int i = 0; i < header.Length; i++)
                    {
                        if (header[i] == row[i])
                        {
                            continue;
                        }

                        isHeader = false;
                        break;
                    }
                    if (0 < header.Length &&
                        isHeader)
                    {
                        break;
                    }

                    // ヘッダーを保存していなければ保存
                    if (header.Length < 1)
                    {
                        header = row;
                    }

                    data.Add(row);
                }
            }

            return data;
        }

        public void SetBulkyData(DataTable dt)
        {
            try
            {
                this.Columns.Clear();
                this.ItemsSource = dt.DefaultView;
                this.IsEmpty = false;
            }
            catch
            {
                SetInitialDataGrid();
                FoaMessageBox.ShowError("AIS_E_049");
//                string message =
//@"Bulkyデータの表示に失敗しました。
//ファイルの形式や文字コードを確認して下さい。";
//                System.Windows.MessageBox.Show(message);
            }
        }

        private void ResetSelection()
        {
            this.SelectedRowNumber = new List<int>();
            this.SelectedColumnNumber = new List<int>();
        }

        public void SelectAllColumn()
        {
            if (this.IsEmpty)
            {
                return;
            }

            int selectedColumnCount = this.SelectedColumnNumber.Count;
            this.SelectedColumnNumber = new List<int>();
            if (selectedColumnCount != this.Columns.Count)
            {
                for (int i = 0; i < this.Columns.Count; i++)
                {
                    this.SelectedColumnNumber.Add(i);
                }
            }
            else
            {
                // 行と列の両方の選択を解除
                this.SelectedRowNumber = new List<int>();
            }
        }
        
        public void SelectAllRow()
        {
            if (this.IsEmpty)
            {
                return;
            }

            int selectedRowNumber = this.SelectedRowNumber.Count;
            this.SelectedRowNumber = new List<int>();
            if (selectedRowNumber != this.Items.Count)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    this.SelectedRowNumber.Add(i);
                }
            }
            else
            {
                // 行と列の両方の選択を解除
                this.SelectedColumnNumber = new List<int>();
            }
        }


        public void UpdateCellColorAndGetButton()
        {
            Point p = saveScrollPosition(this.baseControl);

            if (setCellColor())
            {
                this.GotNewResult = true;
                this.GetButton.IsEnabled = true;
            }
            else
            {
                this.GetButton.IsEnabled = false;
            }

            setScrollPosition(this.baseControl, p);
        }

        private bool setCellColor()
        {
            bool canOpenExcel = false;

            // いったんすべて白色に
            for (int i = 0; i < this.Items.Count; i++)
            {
                this.ScrollIntoView(this.Items[i]);

                for (int j = 0; j < this.Columns.Count; j++)
                {
                    DataGridCellInfo cellInfo = new DataGridCellInfo(this.Items[i], this.Columns[j]);
                    DataGridCell cell = getCellFromCellInfo(cellInfo);

                    cell.Background = new SolidColorBrush(System.Windows.Media.Colors.White);
                    cell.Foreground = new SolidColorBrush(System.Windows.Media.Colors.Black);
                }
            }

            // 行は選択あり、列が選択なし
            if (0 < this.SelectedRowNumber.Count &&
                this.SelectedColumnNumber.Count < 1)
            {
                // 行単位で塗る
                foreach (int rowNumber in this.SelectedRowNumber)
                {
                    this.ScrollIntoView(this.Items[rowNumber]);

                    for (int j = 0; j < this.Columns.Count; j++)
                    {
                        DataGridCellInfo cellInfo = new DataGridCellInfo(this.Items[rowNumber], this.Columns[j]);
                        DataGridCell cell = getCellFromCellInfo(cellInfo);

                        cell.Background = new SolidColorBrush(System.Windows.Media.Colors.PaleGreen);
                    }
                }
                canOpenExcel = true;
            }

            // 行は選択なし、列が選択あり
            if (this.SelectedRowNumber.Count < 1 &&
                0 < this.SelectedColumnNumber.Count)
            {
                // 列単位で塗る
                for (int i = 0; i < this.Items.Count; i++)
                {
                    this.ScrollIntoView(this.Items[i]);

                    foreach (int columnNumber in this.SelectedColumnNumber)
                    {
                        DataGridCellInfo cellInfo = new DataGridCellInfo(this.Items[i], this.Columns[columnNumber]);
                        DataGridCell cell = getCellFromCellInfo(cellInfo);

                        cell.Background = new SolidColorBrush(System.Windows.Media.Colors.PaleGreen);
                    }
                }
                canOpenExcel = true;
            }

            // 行も列も選択あり
            if (0 < this.SelectedRowNumber.Count &&
                0 < this.SelectedColumnNumber.Count)
            {
                // 交差点のみ塗る
                foreach (int rowNumber in this.SelectedRowNumber)
                {
                    this.ScrollIntoView(this.Items[rowNumber]);

                    foreach (int columnNumber in this.SelectedColumnNumber)
                    {
                        DataGridCellInfo cellInfo = new DataGridCellInfo(this.Items[rowNumber], this.Columns[columnNumber]);
                        DataGridCell cell = getCellFromCellInfo(cellInfo);

                        cell.Background = new SolidColorBrush(System.Windows.Media.Colors.PaleGreen);
                    }
                }
                canOpenExcel = true;
            }

            this.ScrollIntoView(this.Items[0]);
            return canOpenExcel;
        }

        #region BulkyDataGrid用ScrollViewer制御

        private Point saveScrollPosition(BaseControl bc)
        {
            ScrollViewer sv = null;
            Point p = new Point();      // 座標を表すわけではないがdouble型のXとYがあるので利用
            if (bc.Mode == ControlMode.Bulky)
            {
                var control = bc as UserControl_BulkyEdit;
                if (control == null)
                {
                    return p;
                }

                sv = control.scrollViewer_Grid;
            }
            else if (bc.Mode == ControlMode.ContinuousGraph)
            {
                var control = bc as UserControl_ContinuousGraphTemplate;
                if (control == null)
                {
                    return p;
                }

                sv = control.scrollViewer_Grid;
            }
            else if (bc.Mode == ControlMode.StreamGraph)
            {
                var control = bc as UserControl_StreamGraphTemplate;
                if (control == null)
                {
                    return p;
                }

                sv = control.scrollViewer_Grid;
            }
            if (sv == null)
            {
                return p;
            }

            p.X = sv.ContentHorizontalOffset;
            p.Y = sv.ContentVerticalOffset;

            return p;
        }

        private void setScrollPosition(BaseControl bc, Point p)
        {
            ScrollViewer sv = null;
            if (bc.Mode == ControlMode.Bulky)
            {
                var control = bc as UserControl_BulkyEdit;
                if (control == null)
                {
                    return;
                }

                sv = control.scrollViewer_Grid;
            }
            else if (bc.Mode == ControlMode.ContinuousGraph)
            {
                var control = bc as UserControl_ContinuousGraphTemplate;
                if (control == null)
                {
                    return;
                }

                sv = control.scrollViewer_Grid;
            }
            else if (bc.Mode == ControlMode.StreamGraph)
            {
                var control = bc as UserControl_StreamGraphTemplate;
                if (control == null)
                {
                    return;
                }

                sv = control.scrollViewer_Grid;
            }
            if (sv == null)
            {
                return;
            }

            sv.ScrollToHorizontalOffset(p.X);
            sv.ScrollToVerticalOffset(p.Y);
        }

        #endregion

        /// <summary>
        /// DataGridCellInfoからDataGridCellを取得
        /// </summary>
        /// <param name="info">取得したいセルのDataGridCellInfo</param>
        /// <returns>infoが示す位置のDataGridCell</returns>
        private DataGridCell getCellFromCellInfo(DataGridCellInfo info)
        {
            this.CurrentCell = info;
            DataGridRow dataGridRow = (DataGridRow)this.ItemContainerGenerator.ContainerFromItem(this.CurrentCell.Item);
            DataGridCell cell = (DataGridCell)info.Column.GetCellContent(dataGridRow).Parent;
            return cell;
        }
    }
}
