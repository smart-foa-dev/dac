﻿using DAC.Util;
using FoaCore.Common.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DAC.View.Controls
{
    /// <summary>
    /// ElementEditDataGrid.xaml の相互作用ロジック
    /// </summary>
    public partial class ElementEditDataGrid : DataGrid
    {
        public ElementEditDataGrid()
        {
            InitializeComponent();
            this.newColumn.Header = "";

            this.Dt = new DataTable();
            this.ElementColumnCount = new Dictionary<string, int>();
            this.Esm = new ElementSelectionManager(this.Dt, this.ElementColumnCount);

            SetInitialDataGrid();
            this.DataContext = this.Dt.DefaultView;
        }

        /// <summary>
        /// DataContextになるDataTable
        /// </summary>
        public DataTable Dt { get; set; }

        /// <summary>
        /// エレメントの個数保存
        /// </summary>
        public Dictionary<string, int> ElementColumnCount { get; set; }

        /// <summary>
        /// 新規追加用カラム
        /// </summary>
        private DataGridTextColumn newColumn = new DataGridTextColumn();

        public ElementSelectionManager Esm { get; set; }

        public void SetInitialDataGrid()
        {
            DataTable dt = new DataTable();
            int columnLength = 7;
            int rowLength = 4;

            // Column
            for (int i = 0; i < columnLength; i++)
            {
                string content = string.Format("Element {0}", i);
                var column = new DataColumn(content, Type.GetType("System.String"));
                dt.Columns.Add(column);
            }

            // Row
            for (int i = 0; i < rowLength; i++)
            {
                DataRow row = dt.NewRow();
                dt.Rows.Add(row);
            }

            this.ColumnWidth = 125;
            this.DataContext = dt.DefaultView;

            SetColumn(dt);
            Esm.TableIsEmpty = true;

            this.Dt = new DataTable();
        }

        public void SetColumn(DataTable dt)
        {
            ElementSelectionManager.setColumn2(dt, this);
        }

        public void AddNewColumn()
        {
            this.Columns.Add(this.newColumn);
        }

        #region Event Handler

        /// <summary>
        /// コントロールロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DataGrid_Loaded(object sender, RoutedEventArgs e)
        {
        }

        /// <summary>
        /// 右クリック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGrid_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.SelectedCells.Count < 1)
            {
                return;
            }

            int rowIndex = this.Items.IndexOf(this.CurrentItem);

            DataGridCellInfo cellInfo = new DataGridCellInfo(this.Items[rowIndex], this.CurrentColumn);
            DataGridCell selectedCell = Esm.getCellFromCellInfo(this, cellInfo);
            if (string.IsNullOrEmpty(((TextBlock)selectedCell.Content).Text))
            {
                return;
            }

            System.Windows.Forms.ContextMenuStrip cMenu = new System.Windows.Forms.ContextMenuStrip();

            // エレメントを削除
            if (selectedCell.Column.Header.ToString() != Keywords.CTM_NAME)
            {
                System.Windows.Forms.ToolStripMenuItem menuItem_DeleteElement = new System.Windows.Forms.ToolStripMenuItem();
                menuItem_DeleteElement.Text = Properties.Resources.TEXT_DELETE_ELEMENT;
                menuItem_DeleteElement.Click += delegate
                {
                    DataGridCell c = Esm.getCellFromCellInfo(this, this.SelectedCells[0]);
                    if (c.Column.Header.ToString() == Keywords.CTM_NAME)
                    {
                        FoaMessageBox.ShowError("AIS_E_030");
                        return;
                    }

                    int selectedColumnIndex = this.CurrentColumn.DisplayIndex;
                    int maxColumnIndex = this.Dt.Columns.Count - 1;
                    DataRow row1 = this.Dt.Rows[rowIndex];
                    Esm.deleteElement(row1, selectedColumnIndex);
                    Esm.deleteColumn(this.Dt, selectedColumnIndex);
                    this.DataContext = this.Dt.DefaultView;
                    if (this.Dt.Rows.Count < 1)
                    {
                        SetInitialDataGrid();
                    }
                    else
                    {
                        SetColumn(this.Dt);
                        this.Columns.Add(this.newColumn);
                    }
                };
                cMenu.Items.Add(menuItem_DeleteElement);
            }

            // CTMを削除
            System.Windows.Forms.ToolStripMenuItem menuItem_DeleteCTM = new System.Windows.Forms.ToolStripMenuItem();
            menuItem_DeleteCTM.Text = Properties.Resources.TEXT_DELETE_CTM;
            menuItem_DeleteCTM.Click += delegate
            {
                DataRow row1 = this.Dt.Rows[rowIndex];
                row1.Delete();

                Esm.deleteEmptyColumns(this.Dt);
                this.DataContext = this.Dt.DefaultView;
                if (this.Dt.Rows.Count < 1)
                {
                    SetInitialDataGrid();
                }
                else
                {
                    SetColumn(this.Dt);
                    this.Columns.Add(this.newColumn);
                }
            };
            cMenu.Items.Add(menuItem_DeleteCTM);

            cMenu.Show(System.Windows.Forms.Cursor.Position);
        }

        /// <summary>
        /// ドロップされたエレメント情報をエレメント毎に配列化
        /// </summary>
        /// <param name="itemArray"></param>
        /// <returns></returns>
        private List<string[]> convertItemArrayToElementData(string[] itemArray)
        {
            List<string[]> data = new List<string[]>();

            for (int i = 2; i < itemArray.Length; i += 3)
            {
                string[] ary = new string[]
                {
                    itemArray[i],
                    itemArray[i + 1],
                    itemArray[i + 2]
                };
                data.Add(ary);
            }

            return data;
        }

        /// <summary>
        /// セルにドロップされた
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cell_Drop(object sender, DragEventArgs e)
        {
            var dataGridCell = sender as System.Windows.Controls.DataGridCell;
            if (dataGridCell == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string items = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = items.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 5)
            {
                return;
            }

            DataGridColumn header = dataGridCell.Column;
            List<string[]> argsData = convertItemArrayToElementData(itemArray);

            // エレメント追加
            addElementToDataGrid(itemArray[0], header.DisplayIndex, argsData, this.Dt);
        }

        /// <summary>
        /// ヘッダーにドロップされた
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void columnHeader_Drop(object sender, DragEventArgs e)
        {
            var header = sender as System.Windows.Controls.Primitives.DataGridColumnHeader;
            if (header == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = item.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 5)
            {
                return;
            }

            List<string[]> argsData = convertItemArrayToElementData(itemArray);

            // エレメント追加
            addElementToDataGrid(itemArray[0], header.DisplayIndex, argsData, this.Dt);
        }

        /// <summary>
        /// ドロップされたエレメントを追加
        /// </summary>
        /// <param name="ctmName">CTM名</param>
        /// <param name="headerIndex">追加する場所の列インデックス</param>
        /// <param name="dropedElements">ドロップされたエレメント</param>
        /// <param name="dt">DataTable</param>
        private void addElementToDataGrid(string ctmName, int headerIndex, List<string[]> dropedElements, DataTable dt)
        {
            int loopCount = -1;
            foreach (string[] elementData in dropedElements)
            {
                loopCount++;

                if (1 < dropedElements.Count &&
                    0 < dt.Rows.Count)
                {
                    // CTMがドロップされた
                    bool hitCtm = false;
                    DataRow row = null;
                    foreach (var rowObject in dt.Rows)
                    {
                        row = rowObject as DataRow;
                        if (row[Keywords.CTM_NAME].ToString() != ctmName)
                        {
                            continue;
                        }

                        hitCtm = true;
                        break;
                    }
                    if (hitCtm &&
                        dropedElements.Count == Esm.getAddedElementCount(row))
                    {
                        // 一度追加されたCTM
                        return;
                    }

                    if (hitCtm)
                    {
                        // 追加中CTM
                        int n = 0;
                        for (int i = 1; i < row.ItemArray.Length; i++)
                        {
                            string headerContent = dt.Columns[i].ColumnName;
                            if (!headerContent.StartsWith(elementData[0]) ||
                                headerContent.Length - 3 != elementData[0].Length)
                            {
                                continue;
                            }

                            n = i;
                            break;
                        }
                        if (n == 0)
                        {
                            n = row.ItemArray.Length;
                            if (this.ElementColumnCount.ContainsKey(elementData[0]))
                            {
                                this.ElementColumnCount[elementData[0]]++;
                            }
                            else
                            {
                                this.ElementColumnCount.Add(elementData[0], 1);
                            }

                            int cnt = this.ElementColumnCount[elementData[0]];
                            string content = string.Format("{0} {1:D2}", elementData[0], cnt);
                            dt = addColumn(dt, content);
                        }

                        row = null;
                        foreach (var rowObject in dt.Rows)
                        {
                            row = rowObject as DataRow;
                            if (row[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            // hit
                            break;
                        }

                        row[dt.Columns[n].ColumnName] = elementData[0];
                    }
                    else
                    {
                        // 新規CTM
                        int n = 0;
                        for (int i = 1; i < row.ItemArray.Length; i++)
                        {
                            string headerContent = dt.Columns[i].ColumnName;
                            if (!headerContent.StartsWith(elementData[0]) ||
                                headerContent.Length - 3 != elementData[0].Length)
                            {
                                continue;
                            }

                            n = i;
                            break;
                        }
                        if (n == 0)
                        {
                            n = row.ItemArray.Length;
                            if (this.ElementColumnCount.ContainsKey(elementData[0]))
                            {
                                this.ElementColumnCount[elementData[0]]++;
                            }
                            else
                            {
                                this.ElementColumnCount.Add(elementData[0], 1);
                            }

                            int cnt = this.ElementColumnCount[elementData[0]];
                            string content = string.Format("{0} {1:D2}", elementData[0], cnt);
                            dt = addColumn(dt, content);
                        }

                        // CTMが追加されていない→新規CTM
                        DataRow newRow = dt.NewRow();
                        newRow[Keywords.CTM_NAME] = ctmName;
                        newRow[dt.Columns[n].ColumnName] = elementData[0];
                        dt.Rows.Add(newRow);
                    }
                }
                else
                {
                    if (Esm.TableIsEmpty)
                    {
                        // 最初のエレメント
                        dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));
                        dt.Columns.Add(new DataColumn(elementData[0] + " 01"));
                        DataRow row = dt.NewRow();
                        row[Keywords.CTM_NAME] = ctmName;
                        row[elementData[0] + " 01"] = elementData[0];
                        dt.Rows.Add(row);

                        if (this.ElementColumnCount.ContainsKey(elementData[0]))
                        {
                            this.ElementColumnCount[elementData[0]]++;
                        }
                        else
                        {
                            this.ElementColumnCount.Add(elementData[0], 1);
                        }

                        this.Columns.Clear();
                        this.DataContext = dt.DefaultView;
                        SetColumn(dt);
                        this.Dt = dt;

                        this.Columns.Add(this.newColumn);
                        Esm.TableIsEmpty = false;
                        continue;
                    }

                    string elementName = string.Empty;
                    foreach (var rowObject in dt.Rows)
                    {
                        DataRow row = rowObject as DataRow;
                        if (row.ItemArray.Length - 1 < headerIndex + loopCount ||
                            row[headerIndex + loopCount].ToString() != elementData[0])
                        {
                            continue;
                        }

                        elementName = elementData[0];
                        break;
                    }

                    if (elementName.StartsWith(elementData[0]) &&
                        elementName.Length - 3 != elementData[0].Length)
                    {
                        // すでにあるカラム(同じエレメント)
                        bool hitCtm = false;
                        bool hitElement = false;
                        DataRow row = null;
                        foreach (var rowObject in dt.Rows)
                        {
                            row = rowObject as DataRow;
                            if (row[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            hitCtm = true;
                            if (row[dt.Columns[headerIndex + loopCount].ColumnName].ToString() == string.Empty)
                            {
                                break;
                            }

                            hitElement = true;
                            continue;
                        }
                        if (row != null &&
                            hitElement)
                        {
                            // 同じCTM、同じエレメントならば何もしない
                            continue;
                        }

                        if (row != null &&
                            hitCtm)
                        {
                            // CTMが既に追加されている
                            row[dt.Columns[headerIndex + loopCount].ColumnName] = elementData[0];
                        }
                        else
                        {
                            // CTMが追加されていない→新規CTM
                            DataRow newRow = dt.NewRow();
                            newRow[Keywords.CTM_NAME] = ctmName;
                            newRow[dt.Columns[headerIndex + loopCount].ColumnName] = elementData[0];
                            dt.Rows.Add(newRow);
                        }
                    }
                    else if (elementName == newColumn.Header.ToString())
                    {
                        // 新規カラム
                        bool isAlreadyAddedElement = false;
                        foreach (var rowObject in dt.Rows)
                        {
                            var r = rowObject as DataRow;
                            if (r[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            foreach (var cell in r.ItemArray)
                            {
                                if (cell.ToString() != elementData[0])
                                {
                                    continue;
                                }

                                // すでに同じエレメントが同じCTMに追加されている
                                isAlreadyAddedElement = true;
                                break;
                            }

                            break;
                        }
                        if (isAlreadyAddedElement)
                        {
                            // すでに同じエレメントが同じCTMに追加されている
                            continue;
                        }

                        if (this.ElementColumnCount.ContainsKey(elementData[0]))
                        {
                            this.ElementColumnCount[elementData[0]]++;
                        }
                        else
                        {
                            this.ElementColumnCount.Add(elementData[0], 1);
                        }
                        int cnt = this.ElementColumnCount[elementData[0]];
                        string content = string.Format("{0} {1:D2}", elementData[0], cnt);
                        // dt = insertColumn(dt, content, dt.Columns.Count - 1);
                        dt = addColumn(dt, content);

                        bool hit = false;
                        DataRow row = null;
                        foreach (var rowObject in dt.Rows)
                        {
                            row = rowObject as DataRow;
                            if (row[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            hit = true;
                            break;
                        }

                        if (row != null &
                            hit)
                        {
                            row[elementData[0] + " " + cnt.ToString("00")] = elementData[0];
                        }
                        else
                        {
                            DataRow newRow = dt.NewRow();
                            newRow[Keywords.CTM_NAME] = ctmName;
                            newRow[elementData[0] + " " + cnt.ToString("00")] = elementData[0];
                            dt.Rows.Add(newRow);
                        }
                    }
                }
            }

            this.DataContext = dt.DefaultView;
            this.Dt = dt;
            SetColumn(dt);

            this.Columns.Remove(this.newColumn);
            this.Columns.Add(this.newColumn);
        }

        /// <summary>
        /// 新規カラム追加
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="columnContent"></param>
        /// <returns></returns>
        private DataTable addColumn(DataTable dt, string columnContent)
        {
            DataTable dtDest = new DataTable();

            int cnt = 0;
            foreach (var column in dt.Columns)
            {
                dtDest.Columns.Add(dt.Columns[cnt].ToString());
                cnt++;
            }
            dtDest.Columns.Add(columnContent);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dtDest.Rows.Add(dt.Rows[i].ItemArray);
            }

            return dtDest;
        }

        public void SetDataTable(DataTable dt)
        {
            this.Dt = dt;
            this.DataContext = this.Dt.DefaultView;
        }

        #endregion
    }
}
