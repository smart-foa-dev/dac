﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAC.Model;
using System;

namespace DAC.View.Controls
{
    /// <summary>
    /// StandardMenuGrid.xaml の相互作用ロジック
    /// </summary>
    public partial class StandardMenuGrid : UserControl
    {
        private ObservableCollection<AisMenuItem> itemList;

        public StandardMenuGrid()
        {
            InitializeComponent();

            itemList = initializeMenuList();
           
            this.DataContext = itemList;
        }

        private ObservableCollection<AisMenuItem> initializeMenuList()
        {
            var itemList = new ObservableCollection<AisMenuItem>();

#if MULTI_LANG
            
            //var testItem = new AisMenuItem { Row = 0, Col = 0 };
            /*
            testItem.Name = "MENU_STD_SPREADSHEET"; //"集計表"
            testItem.Mode = ControlMode.Spreadsheet;
            itemList.Add(testItem);
            */
            /*
            testItem = new AisMenuItem { Row = 0 , Col = 1  };
            testItem.Name = "MENU_STD_ONLINE"; 　　//"オンライン"
            testItem.Mode = ControlMode.Online;
            itemList.Add(testItem);
            */
            /*
            testItem = new AisMenuItem { Row = 0 , Col = 2 };
            testItem.Name = "MENU_STD_BULKY";      //"バルキー"
            testItem.Mode = ControlMode.Bulky;
            itemList.Add(testItem);
            */
            // For the V4 Version & V5_1.0.3.5 Version.　緊急通報は表示しない
            if (AisConf.Config.CmsVersion == "V4" || AisConf.Config.CmsVersion == "3.5")
            {

                var testItem = new AisMenuItem { Row = 0, Col = 0 };
                
                testItem.Name = "MENU_STD_SPREADSHEET"; //"集計表"
                testItem.Mode = ControlMode.Spreadsheet;
                itemList.Add(testItem);
                
                
                testItem = new AisMenuItem { Row = 0 , Col = 1  };
                testItem.Name = "MENU_STD_ONLINE"; 　　//"オンライン"
                testItem.Mode = ControlMode.Online;
                itemList.Add(testItem);
                
                testItem = new AisMenuItem { Row = 0 , Col = 2 };
                testItem.Name = "MENU_STD_BULKY";      //"バルキー"
                testItem.Mode = ControlMode.Bulky;
                itemList.Add(testItem);
                
                testItem = new AisMenuItem { Row = 1, Col = 0 };
                testItem.Name = "MENU_STD_DAC";           //"DAC"
                //testItem.Type="dac";
                testItem.Mode = ControlMode.Dac;
                itemList.Add(testItem);

                testItem = new AisMenuItem { Row = 1, Col = 1 };
                testItem.Name = "MENU_STD_STATUSMONITOR"; //"ステータスモニター"
                testItem.Mode = ControlMode.StatusMonitor;
                itemList.Add(testItem);
            }
            else
            {

                var testItem = new AisMenuItem { Row = 0, Col = 0 };
                //var testItem = new AisMenuItem { Row = 0, Col = 0 };
                
                testItem.Name = "MENU_STD_SPREADSHEET"; //"集計表"
                testItem.Mode = ControlMode.Spreadsheet;
                itemList.Add(testItem);

                testItem = new AisMenuItem { Row = 0, Col = 1 };
                testItem.Name = "MENU_STD_ONLINE"; 　　//"オンライン"
                testItem.Mode = ControlMode.Online;
                itemList.Add(testItem);

                testItem = new AisMenuItem { Row = 0, Col = 2 };
                testItem.Name = "MENU_GRAPH_CONTINUOUSGRAPH";    //"連続グラフ"
                testItem.Mode = ControlMode.ContinuousGraph;
                itemList.Add(testItem);

                testItem = new AisMenuItem { Row = 1, Col = 0 };
                testItem.Name = "MENU_STD_MULTISTATUSMONITOR";    //"マルチモニタ"
                testItem.Mode = ControlMode.MultiStatusMonitor;
                itemList.Add(testItem);

                
                testItem = new AisMenuItem { Row = 1, Col = 1 };
                testItem.Name = "MENU_STD_EMERGENCY";    //"緊急通報"
                testItem.Mode = ControlMode.Emergency;
                itemList.Add(testItem);
                /*
                testItem = new AisMenuItem { Row = 1, Col = 1 };
                testItem.Name = "MENU_STD_DAC";          //"DAC"
                testItem.Mode = ControlMode.Dac;
                itemList.Add(testItem);

                testItem = new AisMenuItem { Row = 1, Col = 2 };
                testItem.Name = "MENU_STD_STATUSMONITOR"; //"ステータスモニター"
                testItem.Mode = ControlMode.StatusMonitor;
                itemList.Add(testItem);
                */ 
            }
#else
            var testItem = new AisMenuItem { Row = 0, Col = 0 };
            testItem.Name = Properties.Resources.MENU_STD_SPREADSHEET; //"集計表"
            testItem.Mode = ControlMode.Spreadsheet;
            itemList.Add(testItem);

            testItem = new AisMenuItem { Row = 0, Col = 1 };
            testItem.Name = Properties.Resources.MENU_STD_ONLINE; 　　//"オンライン"
            testItem.Mode = ControlMode.Online;
            itemList.Add(testItem);

            testItem = new AisMenuItem { Row = 0, Col = 2 };
            testItem.Name = Properties.Resources.MENU_STD_BULKY;      //"バルキー"
            testItem.Mode = ControlMode.Bulky;
            itemList.Add(testItem);

            // For the V4 Version & V5_1.0.3.5 Version.　緊急通報は表示しない
            if (AisConf.Config.CmsVersion == "V4" || AisConf.Config.CmsVersion == "3.5")
            {
                testItem = new AisMenuItem { Row = 1, Col = 0 };
                testItem.Name = Properties.Resources.MENU_STD_DAC;           //"DAC"
                //testItem.Type="dac";
                testItem.Mode = ControlMode.Dac;
                itemList.Add(testItem);

                testItem = new AisMenuItem { Row = 1, Col = 1 };
                testItem.Name = Properties.Resources.MENU_STD_STATUSMONITOR; //"ステータスモニター"
                testItem.Mode = ControlMode.StatusMonitor;
                itemList.Add(testItem);
            }
            else
            {
                testItem = new AisMenuItem { Row = 1, Col = 0 };
                testItem.Name = Properties.Resources.MENU_STD_EMERGENCY;    //"緊急通報"
                testItem.Mode = ControlMode.Emergency;
                itemList.Add(testItem);

                testItem = new AisMenuItem { Row = 1, Col = 1 };
                testItem.Name = Properties.Resources.MENU_STD_DAC;          //"DAC"
                testItem.Mode = ControlMode.Dac;
                itemList.Add(testItem);

                testItem = new AisMenuItem { Row = 1, Col = 2 };
                testItem.Name = Properties.Resources.MENU_STD_STATUSMONITOR; //"ステータスモニター"
                testItem.Mode = ControlMode.StatusMonitor;
                itemList.Add(testItem);
            }
#endif
            return itemList;
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                var sp = sender as StackPanel;
                var dc = sp.DataContext as AisMenuItem;
                if (dc != null)
                {
                    openMainWindow(dc.Mode);
                }
            }
        }

        private void openMainWindow(ControlMode mode)
        {
            // Wang Issue AISTEMP-123 Start
            if (!mode.Equals(ControlMode.None))
            // Wang Issue AISTEMP-123 End
            {
                MainWindow mainWindow = new MainWindow(mode, true);
                mainWindow.Show();

                Window window = Window.GetWindow(this);
                mainWindow.VersionInfo.ToolTip = (window as Window_Home).VersionInfo.ToolTip;
                window.Close();
            }
        }

        private void StackPanel_MouseEnter(object sender, MouseEventArgs e)
        {

        }

        private void StackPanel_MouseLeave(object sender, MouseEventArgs e)
        {

        }

        private void StackPanel_MouseUp(object sender, MouseButtonEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var sp = sender as Button;
            var dc = sp.DataContext as AisMenuItem;
            if (dc != null)
            {
                openMainWindow(dc.Mode);
            }
        }
    }
}
