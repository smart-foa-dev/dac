﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DAC.Model;
using FoaCore.Common.Net;
using FoaCore.Common;
using FoaCore.Common.Util;
using Newtonsoft.Json.Linq;
using System.IO;
using Microsoft.Win32;

namespace DAC.View.Controls
{
    /// <summary>
    /// ExternalExcelList.xaml の相互作用ロジック
    /// </summary>
    public partial class ExternalExcelList : UserControl
    {
        private ObservableCollection<FileListData> _fileList = new ObservableCollection<FileListData>();
        private const string tmpdir = "tmp";
        public Window ParentWindow;
        private string[] fileinfo;
        public ObservableCollection<FileListData> FileList {
            get
            {
                return _fileList;
            }
            set
            {
                _fileList = value;
            }
        }            
        public ExternalExcelList()
        {
            InitializeComponent();
            this.DataContext = this;            
        }

        private void ExcelFileList_Drop(object sender, DragEventArgs e)
        {
            var fileInfo = e.Data;
            if(fileInfo.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])fileInfo.GetData(DataFormats.FileDrop);
                fileinfo = files;
                string filepath = files[0];
                string ext = System.IO.Path.GetExtension(filepath);
                if(ext.Equals(".xlsx", StringComparison.OrdinalIgnoreCase))
                {
                    string filename = System.IO.Path.GetFileName(filepath);
                    // Upload 
                    string userId = LoginInfo.GetInstance().GetLoginUserInfo().UserId;
                    CmsHttpClient client = new CmsHttpClient(ParentWindow);
                    client.AddParam("userId", userId);
                    client.AddParam("filename", filename);
                    client.AddParam("force", false.ToString());
                    client.CompleteHandler += UploadCompleted;
                    client.FileUpload(ExcelEngineUrl.ExternalExcel.GetExternalExcelBaseUrl(), filepath);
                }
                else
                {
                    FoaMessageBox.ShowError("ERROR_WRONG_FILE_EXT", ".xlsx");
                }
            }
        }

        private void UploadCompleted(string json)
        {
            JToken token = JToken.Parse(json);
            bool isSuccess = false;
            if(token["isSuccess"] != null)
            {
                isSuccess = (bool)token["isSuccess"];
            }
            if(isSuccess)
            {
                JToken data = token["fileInfo"];
                if(data != null)
                {
                    string filename = data["fileName"].ToString();
                    string filepath = data["filePath"].ToString();
                    long modifiedtime = (long)data["lastModifiedTime"];
                    FileListData filedata = null;
                    try
                    {
                        filedata = this.FileList.First(item => item.FileName.Equals(filename));
                    }
#pragma warning disable
                    catch (InvalidOperationException ioe)
#pragma warning restore
                    {
                        filedata = null;
                    }
                    if (filedata == null)
                        this.FileList.Add(new FileListData() { FileName = filename, FilePath = filepath, sourceType = FileListData.SourceType.LOCALFILE, UpdatedTime = modifiedtime });
                    else
                    {
                        filedata.FilePath = filepath;
                        filedata.UpdatedTime = modifiedtime;
                    }
                }
            }
            else
            {
                // Retry
                MessageBoxResult result = FoaMessageBox.ShowConfirm("EXTERNAL_EXCEL_CONFIRM_UPLOAD_RETRY");
                if(result == MessageBoxResult.Yes)
                {
                    string filepath = fileinfo[0];
                    string filename = System.IO.Path.GetFileName(filepath);
                    // Upload 
                    string userId = LoginInfo.GetInstance().GetLoginUserInfo().UserId;
                    CmsHttpClient client = new CmsHttpClient(ParentWindow);
                    client.AddParam("userId", userId);
                    client.AddParam("filename", filename);
                    client.AddParam("force", true.ToString());
                    client.CompleteHandler += UploadCompleted;
                    client.FileUpload(ExcelEngineUrl.ExternalExcel.GetExternalExcelBaseUrl(), filepath);
                }

            }
        }

        private void ExternalFileDelete_Click(object sender, RoutedEventArgs e)
        {
            FileListData data = ExcelFileList.SelectedItem as FileListData;
            CmsHttpClient deleteclient = new CmsHttpClient(Application.Current.MainWindow);
            deleteclient.CompleteHandler += (resJson) =>
            {
                GetFileList();
                (ParentWindow as MainWindow).CtmDtailsCtrl.DeleteExternalFileFromTree(data.ToString());
                
            };
            deleteclient.AddParam("userId", LoginInfo.GetInstance().GetLoginUserInfo().UserId);
            deleteclient.AddParam("filename", data.FileName);
            deleteclient.Delete(ExcelEngineUrl.ExternalExcel.GetExternalExcelBaseUrl(), "{}");
        }

        private void ExternalFileDownload_Click(object sender, RoutedEventArgs e)
        {
            FileListData data = ExcelFileList.SelectedItem as FileListData;
            string applicationPath = AppDomain.CurrentDomain.BaseDirectory;
            string tmpfolder = System.IO.Path.Combine(applicationPath, tmpdir);
            string userfolder = System.IO.Path.Combine(tmpfolder, LoginInfo.GetInstance().GetLoginUserInfo().UserId);
            if (!Directory.Exists(userfolder))
                Directory.CreateDirectory(userfolder);
            string filepath = System.IO.Path.Combine(userfolder, data.FileName);
            CmsHttpClient downloadclient = new CmsHttpClient(Application.Current.MainWindow);
            downloadclient.AddParam("userId", LoginInfo.GetInstance().GetLoginUserInfo().UserId);
            downloadclient.AddParam("filename", data.FileName);
            downloadclient.CompleteHandler += (response) =>
            {
                System.Diagnostics.Process.Start(filepath);
            };
            downloadclient.FileDownload(ExcelEngineUrl.ExternalExcel.GetExternalExcelBaseUrl(),filepath);
        }

        private void Btn_SelectExcel_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofg = new OpenFileDialog();
            ofg.RestoreDirectory = true;
            ofg.Filter = "Excel files (*.xlsx)|*.xlsx";
            ofg.CheckFileExists = true;
            ofg.CheckPathExists = true;
            ofg.Title = Properties.Resources.TITLE_SET_EXTERNAL_EXCEL;
            bool? result = ofg.ShowDialog();
            if(result != null && result == true)
            {
                string filepath = ofg.FileName;
                string filename = ofg.SafeFileName;
                fileinfo = new string[] { filepath };
                string userId = LoginInfo.GetInstance().GetLoginUserInfo().UserId;
                CmsHttpClient client = new CmsHttpClient(ParentWindow);
                client.AddParam("userId", userId);
                client.AddParam("filename", filename);
                client.AddParam("force", false.ToString());
                client.CompleteHandler += UploadCompleted;
                client.FileUpload(ExcelEngineUrl.ExternalExcel.GetExternalExcelBaseUrl(), filepath);
            }
        }

        private void Btn_SetUrl_Click(object sender, RoutedEventArgs e)
        {
            Window w = new SetExternalURL();
            bool? result = w.ShowDialog();
            if (result == true)
            {
                string url = (w as SetExternalURL).UrlText;
                FileListData data = new FileListData()
                {
                    FileName = Properties.Resources.EXTERNAL_URL,
                    FilePath = url,
                    sourceType = FileListData.SourceType.URL,
                    UpdatedTime = UnixTime.ToLong2(DateTime.Now)
                };
                this.FileList.Add(data);
            }
        }

        private void Btn_Reload_ExternalList_Click(object sender, RoutedEventArgs e)
        {
            GetFileList();
        }


        private void ExternalExcelExpander_Expanded(object sender, RoutedEventArgs e)
        {
            GetFileList();
        }

        private void GetListCompleted(string json)
        {
            this.FileList.Clear();
            JArray token = JArray.Parse(json);
            foreach (var fileitem in token)
            {
                string filename = fileitem["fileName"].ToString();
                string filepath = fileitem["filePath"].ToString();
                long modifiedtime = (long)fileitem["lastModifiedTime"];
                this.FileList.Add(new FileListData() { FileName = filename, FilePath = filepath, sourceType = FileListData.SourceType.LOCALFILE, UpdatedTime = modifiedtime });
            }
        }

        private void ExcelFileList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            FileListData selectedfile = (FileListData)ExcelFileList.SelectedItem;
            if(selectedfile != null)
            {
                (ParentWindow as MainWindow).SetExternalFile(selectedfile.ToString());
            }
        }

        private void GetFileList()
        {
            string userId = LoginInfo.GetInstance().GetLoginUserInfo().UserId;
            CmsHttpClient client = new CmsHttpClient(ParentWindow);
            client.AddParam("userId", userId);
            client.CompleteHandler += GetListCompleted;
            client.Get(ExcelEngineUrl.ExternalExcel.ListExternalExcelUrl());
        }



    }
}
