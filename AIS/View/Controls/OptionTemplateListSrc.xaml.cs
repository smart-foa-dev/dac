﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAC.Model;
using System.Collections.Generic;
using FoaCore.Common;

namespace DAC.View.Controls
{
    /// <summary>
    /// OptionTemplateListSrc.xaml の相互作用ロジック
    /// </summary>
    public class OptionMenuItem
    {
        public string Name;
        public string Name2;
        public ControlMode Mode;
        public bool IsValid;
    }

    public partial class OptionTemplateListSrc : UserControl
    {
        public static readonly int ItemCols = 3;

        public static readonly List<OptionMenuItem> thisList;

        private const char EnableFlag = '1';
        private const char DisableFlag = '0';

        private static ListBox _dragSource = null;
       
        private ObservableCollection<AisMenuItem> itemList;

        static OptionTemplateListSrc()
        {
            LoginInfo loginInfo = LoginInfo.GetInstance();
            string enableMultiStatus = loginInfo.getEnableMultiStatusMonitor();

            thisList = new List<OptionMenuItem> {
                new OptionMenuItem() { Name = "MENU_STD_MULTIDAC", Name2 = "TEXT_STD_MULTIDAC", Mode = ControlMode.MultiDac,
                    IsValid = loginInfo.GetEnableMultiDac() },
                new OptionMenuItem() { Name = "MENU_STD_MULTISTATUSMONITOR", Name2 = "TEXT_STD_MULTISTATUSMONITOR", Mode = ControlMode.MultiStatusMonitor,
                    IsValid = !string.IsNullOrEmpty(enableMultiStatus) && enableMultiStatus.Length > 0 && EnableFlag.Equals(enableMultiStatus[0]) },
                new OptionMenuItem() { Name = "MENU_STD_TORQUE", Name2 = "TEXT_STD_TORQUE", Mode = ControlMode.Torque,
                    IsValid = !string.IsNullOrEmpty(enableMultiStatus) && enableMultiStatus.Length > 1 && EnableFlag.Equals(enableMultiStatus[1]) },
                new OptionMenuItem() { Name = "MENU_GRAPH_STREAMGRAPH", Name2 = "TEXT_GRAPH_STREAMGRAPH", Mode = ControlMode.StreamGraph,
                    IsValid = !string.IsNullOrEmpty(enableMultiStatus) && enableMultiStatus.Length > 2 && EnableFlag.Equals(enableMultiStatus[2]) }
            };
        }

        public OptionTemplateListSrc()
        {
            InitializeComponent();

            itemList = initializeMenuList();

            this.DataContext = itemList;
        }

        private static void AddAisMenuItem(Collection<AisMenuItem> list, OptionMenuItem item)
        {
            list.Add(new AisMenuItem()
            {
                Row = list.Count / OptionTemplateListSrc.ItemCols,
                Col = list.Count % OptionTemplateListSrc.ItemCols,
                Name = item.Name,
                Name2 = item.Name2,
                Mode = item.Mode,
                GroupKey = Properties.Properties.AIS_TEMPLATE_MENU_GROUPKEY
            });
        }

        private ObservableCollection<AisMenuItem> initializeMenuList()
        {
            var itemList = new ObservableCollection<AisMenuItem>();

            // Add menu items
            for (int i = 0; i < thisList.Count; i++)
            {
                if (thisList[i].IsValid) {
                    AddAisMenuItem(itemList, thisList[i]);
                }
            }

            return itemList;
        }

        private void ListBox_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _dragSource = sender as ListBox;

            return;
        }

        private void ListBox_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (_dragSource == sender)
                {
                    var _draggedItem = e.OriginalSource as FrameworkElement;
                    var itemName = _draggedItem.DataContext as string;

                    ItemCollection items = _dragSource.Items;

                    var srcItem = findSourceItem(items, itemName);
                    if (srcItem != null)
                    {
                        var dragData = new DataObject("aisMenu", srcItem);
                        DragDrop.DoDragDrop(_dragSource, dragData, DragDropEffects.Copy);
                    }
                }
            }
        }

        private AisMenuItem findSourceItem(ItemCollection ic, string name)
        {
            foreach (var itemObject in ic)
            {
                var item = itemObject as AisMenuItem;
                if (item != null)
                {
                    if (item.Name2 == name)
                    {
                        return item;
                    }
                }
            }

            return null;
        }
    }
}
