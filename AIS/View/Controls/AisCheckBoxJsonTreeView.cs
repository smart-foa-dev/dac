﻿using Newtonsoft.Json.Linq;
using System.Windows.Controls;
using System.Windows;
using System.Linq;
using System.Collections.Generic;
using System;
using FoaCore.Common.Control;

namespace DAC.View.Controls
{
    /// <summary>
    /// チェックボックス付きのJsonTreeView
    /// </summary>
    public class AisCheckBoxJsonTreeView : StatefulJsonTreeView
    {
        // Checkイベントのデータを持たないイベントデリゲートの宣言
        public event EventHandler CheckChangedEvent;
        protected virtual void OnCheckChanged(EventArgs e)
        {
            if (CheckChangedEvent != null)
            {
                CheckChangedEvent(this, e);
            }
        }

        public event EventHandler CheckCtmsOverEvent;
        protected virtual void OnCheckCtmsOverEventOccured(EventArgs e)
        {
            if (CheckCtmsOverEvent != null)
            {
                CheckCtmsOverEvent(this, e);
            }
        }

        /// <summary>
        /// ツリーアイテムのヘッダをチェックボックス付きで作成します。
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        protected override StackPanel CreateTreeItemHeader(JToken token)
        {
            // 通常のツリーアイテムのStackPanel
            StackPanel st = base.CreateTreeItemHeader(token);

            CheckBox chk = new CheckBox();
            chk.PreviewMouseLeftButtonDown += OnMouseLeftButtonDown;
            chk.IsThreeState = true;
            chk.Margin = new Thickness(0, 3, 5, 0);
            chk.SetValue(CheckBox.VerticalAlignmentProperty, VerticalAlignment.Center);
            chk.Checked += chk_CheckedChanged;
            chk.Unchecked += chk_CheckedChanged;

            // StackPanelの先頭に挿入
            st.Children.Insert(0, chk);
            return st;
        }

        /// <summary>
        /// チェックボックスのチェック状態が変更された時のイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chk_CheckedChanged(object sender, EventArgs e)
        {
            OnCheckChanged(e);
        }

        public volatile bool SkipIsSelectedEvent;
        /// <summary>
        /// 左クリック時のイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMouseLeftButtonDown(object sender, RoutedEventArgs e)
        {
            // チェックの状態
            CheckBox chk = (CheckBox)sender;
            bool isChecked = false;
            SkipIsSelectedEvent = false;

            // チェックされたツリーアイテム
            StackPanel st = (StackPanel)chk.Parent;
            TreeViewItem checkedItem = (TreeViewItem)st.Parent;

            // 葉でなければreturn
            if (checkedItem.HasItems)
            {
                e.Handled = true;
                return;
            }

            if (chk.IsChecked == false)
            {
                // 未チェック状態からのチェックは、trueになる。
                isChecked = true;
                chk.IsChecked = true;
            }
            else if (chk.IsChecked == true)
            {
                // チェック状態からのチェックは、NULLになるので、チェックを外す。
                isChecked = false;
                chk.IsChecked = false;
            }
            else
            {
                // NUll状態からのチェックは、falseになる。
                chk.IsChecked = false;
            }

            // イベントを停止する事で、チェックボックスのON/OFFをプログラムで制御する。
            e.Handled = true;

            // Set the checkedItem become selected or not
            List<string> checkedIds = new List<string>();
            checkedIds = this.GetCheckedId("type", "c");
            if (checkedIds.Count == 0)
            {
                checkedItem.IsSelected = false;
            }
            else if (SkipIsSelectedEvent == false &&
                chk.IsChecked == true || checkedItem.IsSelected == true)
            {
                checkedItem.IsSelected = true;
            }

            // チェックされたツリーアイテムが持つ子の状態を変更(再帰)
            UpdateChildIsChecked(checkedItem, isChecked);

            // チェックされたツリーアイテムが持つ親の状態を変更(再帰)
            UpdateParentIsChecked(checkedItem);

            // OnCheckChanged(EventArgs.Empty);
        }

        /// <summary>
        /// 子ツリーアイテムのチェック状態を変更します。
        /// </summary>
        /// <param name="item"></param>
        /// <param name="isChecked"></param>
        private void UpdateChildIsChecked(TreeViewItem item, bool? isChecked)
        {
            // ツリーアイテムの子
            ItemCollection col = item.Items;
            if (col == null ||
                col.Count < 1)
            {
                return;
            }

            foreach (TreeViewItem childItem in col)
            {
                // 子のチェックボックスの状態変更
                CheckBox chk = GetCheckBox(childItem);
                if (chk != null)
                {
                    chk.IsChecked = isChecked;
                }

                // 再帰
                UpdateChildIsChecked(childItem, isChecked);
            }
        }

        /// <summary>
        /// 親ツリーアイテムのチェック状態を変更します。
        /// </summary>
        /// <param name="item"></param>
        private void UpdateParentIsChecked(TreeViewItem item)
        {
            TreeViewItem parentItem = (TreeViewItem)item.Parent;
            if (parentItem == null)
            {
                return;
            }

            ItemCollection col = parentItem.Items;
            if (col == null ||
                col.Count < 1)
            {
                return;
            }

            // 全ての子の状態を判定
            int checkedCount = 0;
            bool isExistNull = false;
            foreach (TreeViewItem childItem in col)
            {
                CheckBox chk = GetCheckBox(childItem);
                if (chk.IsChecked == true)
                {
                    checkedCount++;
                }
                if (chk.IsChecked == null)
                {
                    isExistNull = true;
                }
            }

            // 親のチェックボックス
            CheckBox parentChk = GetCheckBox(parentItem);
            
            // TODO もっといい判定方法が...
            if (isExistNull)
            {
                parentChk.IsChecked = null;
            }
            else
            {
                if (checkedCount == 0)
                {
                    parentChk.IsChecked = false;
                }
                else if (checkedCount == col.Count)
                {
                    parentChk.IsChecked = true;
                }
                else
                {
                    parentChk.IsChecked = null;
                }
            }

            // 再帰
            UpdateParentIsChecked(parentItem);
        }

        /// <summary>
        /// ツリーアイテムからチェックボックスを取得します。
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public CheckBox GetCheckBox(TreeViewItem item)
        {
            StackPanel st = (StackPanel)item.Header;
            UIElementCollection uiCol = st.Children;
            foreach (UIElement ui in uiCol)
            {
                if (ui is CheckBox)
                {
                    return (CheckBox)ui;
                }
            }
            return null;
        }

        /// <summary>
        /// チェックされたツリーアイテムのIDのコレクションを取得します。
        /// </summary>
        /// <param name="keyName">取得対象とするJsonのキー</param>
        /// <param name="value">取得対象の値</param>
        /// <returns></returns>
        public List<string> GetCheckedId(string keyName, object value)
        {
            if (itemList == null)
            {
                return null;
            }

            List<string> list = new List<string>();
            foreach (KeyValuePair<string, TreeViewItem> item in itemList)
            {
                // チェックしているか
                CheckBox chk = GetCheckBox(item.Value);
                if (chk.IsChecked != true)
                {
                    continue;
                }

                // 値が一致しているか
                JToken token = GetToken(item.Value);
                var keyValue = token[keyName];
                if (keyValue == null) continue;
                if (keyValue.ToString() == value.ToString())
                {
                    list.Add(GetId(item.Value));
                }
            }
            return list;
        }

        /// <summary>
        /// チェックされたツリーアイテムのIDのコレクションを取得します。
        /// </summary>
        /// <param name="excludeParent">trueの場合にのみ、親のチェックは除外する。</param>
        /// <returns></returns>
        public List<string> GetCheckedId(bool excludeParent)
        {
            if (itemList == null)
            {
                return null;
            }

            List<string> list = new List<string>();
            foreach (KeyValuePair<string, TreeViewItem> item in itemList)
            {
                if (excludeParent)
                {
                    // 子がいる。もしくは、親がいない(＝root)
                    if (item.Value.HasItems || item.Value.Parent == null)
                    {
                        continue;
                    }
                }

                // チェックしているか
                CheckBox chk = GetCheckBox(item.Value);
                if (chk.IsChecked == true)
                {
                    var token = GetToken(item.Value);
                    var type = (string)token["type"];

                    // Exclude Group!
                    list.Add(GetId(item.Value));
                }
            }

            return list;
        }

        /// <summary>
        /// チェックボックスの状態を変更します。
        /// ただし、そのIDの位置するツリーアイテムの親や子の状態は変更されません。
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isChecked"></param>
        public void SetCheckState(string id, bool? isChecked)
        {
            if (ContainsKeyTreeViewItem(id) == false)
            {
                return;
            }

            TreeViewItem item = GetTreeViewItem(id);
            if (item != null)
            {
                CheckBox chk = GetCheckBox(item);
                chk.IsChecked = isChecked;

                // チェックされたツリーアイテムが持つ子の状態を変更(再帰)
                UpdateChildIsChecked(item, isChecked);

                // チェックされたツリーアイテムが持つ親の状態を変更(再帰)
                UpdateParentIsChecked(item);
            }
        }

        /// <summary>
        /// チェックボックスの状態を変更します。
        /// ただし、そのIDの位置するツリーアイテムの親や子の状態は変更されません。
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isChecked"></param>
        public void SetCheckState2(string id, bool? isChecked)
        {
            if (ContainsKeyTreeViewItem(id) == false)
            {
                return;
            }

            TreeViewItem item = GetTreeViewItem(id);
            if (item != null)
            {
                CheckBox chk = GetCheckBox(item);
                chk.IsChecked = isChecked;
            }
        }

        public bool? IsCheckState(string id)
        {
            if (ContainsKeyTreeViewItem(id) == false) return false;
            TreeViewItem item = GetTreeViewItem(id);
            if (item != null)
            {
                CheckBox chk = GetCheckBox(item);
                return chk.IsChecked;
            }

            return false;
        }
    }

    public static class CheckedJsonTreeViewItemUtil
    {
        public static Dictionary<string, Dictionary<string, CheckedJsonTreeViewItem>> Dict = new Dictionary<string, Dictionary<string, CheckedJsonTreeViewItem>>();
        public static Dictionary<string, Dictionary<string, HashSet<string>>> CtmClassDict = new Dictionary<string, Dictionary<string, HashSet<string>>>();
        public static Dictionary<string, KeyValuePair<string, Dictionary<string, bool>>> CheckedCtmClassDict = new Dictionary<string, KeyValuePair<string, Dictionary<string, bool>>>();
    }

    public class CheckedJsonTreeViewItem
    {
        public bool? IsChecked;
        public string Type;
        public JToken Token;
    }
}