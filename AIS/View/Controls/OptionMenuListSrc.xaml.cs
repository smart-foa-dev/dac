﻿using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAC.Model;

namespace DAC.View.Controls
{
    /// <summary>
    /// OptionMenuListSrc.xaml の相互作用ロジック
    /// </summary>
    public partial class OptionMenuListSrc : UserControl
    {
        private static ListBox _dragSource = null;
       
        private ObservableCollection<AisMenuItem> itemList;

        public OptionMenuListSrc()
        {
            InitializeComponent();

            itemList = initializeMenuList();

            this.DataContext = itemList;
        }

        private ObservableCollection<AisMenuItem> initializeMenuList()
        {
            var itemList = new ObservableCollection<AisMenuItem>();

#if MULTI_LANG
            var testItem = new AisMenuItem { Row = 0, Col = 0 };
            /*
            testItem.Name = "MENU_GRAPH_LEADTIMEANALYSIS";　//リードタイム分析
            testItem.Name2 = "TEXT_GRAPH_LEADTIMEANALYSIS";
            testItem.Mode = ControlMode.LeadTime; 
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);
            */
            
            //testItem = new AisMenuItem { Row = 2, Col = 0 };
            testItem.Name = "MENU_GRAPH_FREQUENCY";         //度数分布
            testItem.Mode = ControlMode.Frequency;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);
            
            /*
            testItem = new AisMenuItem { Row = 0, Col = 0 };
            testItem.Name = "MENU_GRAPH_ONLINE";    //オンラインチャート
            testItem.Name2 = "TEXT_GRAPH_ONLINE";
            testItem.Mode = ControlMode.Online;
            */
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            /*
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);
            */
            /*
            testItem = new AisMenuItem { Row = 0, Col = 1 };
            testItem.Name = "MENU_GRAPH_CONTINUOUSGRAPH";   //連続グラフ(Bulky)
            testItem.Name2 = "TEXT_GRAPH_CONTINUOUSGRAPH";
            testItem.Mode = ControlMode.ContinuousGraph;
             
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);
            */

            testItem = new AisMenuItem { Row = 0, Col = 1 };
            testItem.Name = "MENU_GRAPH_STOCKTIMECHART";    //在庫状況チャート
            testItem.Name2 = "TEXT_GRAPH_STOCKTIMECHART";
            testItem.Mode = ControlMode.StockTime;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);

            /*
            testItem = new AisMenuItem { Row = 0, Col = 2 };
            testItem.Name = "MENU_GRAPH_CHIMNEYCHART";      //煙突チャート
            testItem.Name2 = "TEXT_GRAPH_CHIMNEYCHART";
            testItem.Mode = ControlMode.ChimneyChart;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);
            */

            testItem = new AisMenuItem { Row = 0, Col = 2 };
            testItem.Name = "MENU_GRAPH_FIFOCHART";         //先入/先出チャート
            testItem.Name2 = "TEXT_GRAPH_FIFOCHART";
            testItem.Mode = ControlMode.FIFO;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);

            /*
            testItem = new AisMenuItem { Row = 2, Col = 0 };
            testItem.Name = "MENU_GRAPH_FREQUENCY";         //度数分布
            testItem.Mode = ControlMode.Frequency;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);
            */
                     
            /*
            testItem = new AisMenuItem { Row = 1, Col = 0 };
            testItem.Name = "MENU_GRAPH_RADARCHART";        //レーダーチャート
            testItem.Name2 = "TEXT_GRAPH_RADARCHART";
            testItem.Mode = ControlMode.Moment;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);
            */
            /*
            testItem = new AisMenuItem { Row = 3, Col = 0 };
            testItem.Name = "MENU_GRAPH_COMMENTLIST";       //コメント一覧
            testItem.Name2 = "TEXT_GRAPH_COMMENTLIST";
            testItem.Mode = ControlMode.Comment;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);
            */

            testItem = new AisMenuItem { Row = 1, Col = 0 };
            testItem.Name = "MENU_GRAPH_STOCKTIMECHART_PAST";  //過去在庫状況
            testItem.Name2 = "TEXT_GRAPH_STOCKTIMECHART_PAST";
            testItem.Mode = ControlMode.PastStockTime;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            testItem.GroupKey = Properties.Properties.AIS_OPTION_MENU_GROUPKEY;
            // Wang Issue NO.687 2018/05/18 End
            itemList.Add(testItem);
#else
            var testItem = new AisMenuItem { Row = 0, Col = 0 };
            testItem.Name = Properties.Resources.MENU_GRAPH_ONLINE;　//オンラインチャート
            testItem.Name2 = Properties.Resources.TEXT_GRAPH_ONLINE;
            testItem.Mode = ControlMode.Online;
            itemList.Add(testItem);

            testItem = new AisMenuItem { Row = 0, Col = 1 };
            testItem.Name = Properties.Resources.MENU_GRAPH_CONTINUOUSGRAPH;   //連続グラフ(Bulky)
            testItem.Name2 = Properties.Resources.TEXT_GRAPH_CONTINUOUSGRAPH;
            testItem.Mode = ControlMode.ContinuousGraph;
            itemList.Add(testItem);


            /*
            testItem.Name = Properties.Resources.MENU_GRAPH_LEADTIMEANALYSIS;　//リードタイム分析
            testItem.Name2 = Properties.Resources.TEXT_GRAPH_LEADTIMEANALYSIS;
            testItem.Mode = ControlMode.LeadTime;
            itemList.Add(testItem);
            */

            testItem = new AisMenuItem { Row = 0, Col = 2 };
            testItem.Name = Properties.Resources.MENU_GRAPH_STOCKTIMECHART;    //在庫状況チャート
            testItem.Name2 = Properties.Resources.TEXT_GRAPH_STOCKTIMECHART;
            testItem.Mode = ControlMode.StockTime;
            itemList.Add(testItem);

            /*
            testItem = new AisMenuItem { Row = 0, Col = 2 };
            testItem.Name = Properties.Resources.MENU_GRAPH_CHIMNEYCHART;      //煙突チャート
            testItem.Name2 = Properties.Resources.TEXT_GRAPH_CHIMNEYCHART;
            testItem.Mode = ControlMode.ChimneyChart;
            itemList.Add(testItem);
            */

            testItem = new AisMenuItem { Row = 1, Col = 0 };
            testItem.Name = Properties.Resources.MENU_GRAPH_FIFOCHART;         //先入/先出チャート
            testItem.Name2 = Properties.Resources.TEXT_GRAPH_FIFOCHART;
            testItem.Mode = ControlMode.FIFO;
            itemList.Add(testItem);

            /*
            testItem = new AisMenuItem { Row = 2, Col = 0 };
            testItem.Name = Properties.Resources.MENU_GRAPH_FREQUENCY;         //度数分布
            testItem.Mode = ControlMode.Frequency;
            itemList.Add(testItem);
            */

            
            testItem = new AisMenuItem { Row = 1, Col = 1 };
            testItem.Name = Properties.Resources.MENU_GRAPH_RADARCHART;        //レーダーチャート
            testItem.Name2 = Properties.Resources.TEXT_GRAPH_RADARCHART;
            testItem.Mode = ControlMode.Moment;
            itemList.Add(testItem);

            /*
            testItem = new AisMenuItem { Row = 3, Col = 0 };
            testItem.Name = Properties.Resources.MENU_GRAPH_COMMENTLIST;       //コメント一覧
            testItem.Name2 = Properties.Resources.TEXT_GRAPH_COMMENTLIST;
            testItem.Mode = ControlMode.Comment;
            itemList.Add(testItem);
            */

            testItem = new AisMenuItem { Row = 1, Col = 2 };
            testItem.Name = Properties.Resources.MENU_GRAPH_STOCKTIMECHART_PAST;  //過去在庫状況
            testItem.Name2 = Properties.Resources.TEXT_GRAPH_STOCKTIMECHART_PAST;
            testItem.Mode = ControlMode.PastStockTime;
            itemList.Add(testItem);
#endif
            return itemList;
        }

        private void ListBox_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _dragSource = sender as ListBox;

            return;
        }

        private void ListBox_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (_dragSource == sender)
                {
                    var _draggedItem = e.OriginalSource as FrameworkElement;
                    var itemName = _draggedItem.DataContext as string;

                    ItemCollection items = _dragSource.Items;

                    var srcItem = findSourceItem(items, itemName);
                    if (srcItem != null)
                    {
                        var dragData = new DataObject("aisMenu", srcItem);
                        DragDrop.DoDragDrop(_dragSource, dragData, DragDropEffects.Copy);
                    }
                }
            }
        }

        private AisMenuItem findSourceItem(ItemCollection ic, string name)
        {
            foreach (var itemObject in ic)
            {
                var item = itemObject as AisMenuItem;
                if (item != null)
                {
                    if (item.Name2 == name)
                    {
                        return item;
                    }
                }
            }

            return null;
        }
    }
}
