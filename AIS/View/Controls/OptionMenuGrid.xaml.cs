﻿using DAC.Model;
using DAC.View.Helpers;
using LiteDB;
using log4net;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.ComponentModel;
using FoaCore.Common;

namespace DAC.View.Controls
{
    /// <summary>
    /// StandardMenuGrid.xaml の相互作用ロジック
    /// </summary>
    public partial class OptionMenuGrid : UserControl
    {
        private static readonly string OPTION_DIR_NAME = "option";
        private readonly Brush HILIGHT_BRUSH = new SolidColorBrush(Color.FromRgb(0xBD, 0xC2, 0x39));

        private ObservableCollection<AisMenuItem> itemList;

        private AisMenuItem deleteTarget;

        private StackPanel dragSrc;

        private readonly string optionMenuDbFile;

        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        public string GroupKey
        {
            get { return (string)GetValue(GroupKeyProperty); }
            set
            {
                SetValue(GroupKeyProperty, value);
            }
        }

        public static readonly DependencyProperty GroupKeyProperty =
          DependencyProperty.Register("GroupKey", typeof(string), typeof(OptionMenuGrid), new PropertyMetadata(null, OnPropertyChanged));

        private static void OnPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
        }
        // Wang Issue NO.687 2018/05/18 End

        public OptionMenuGrid()
        {
            InitializeComponent();

            var baseDir = AppDomain.CurrentDomain.BaseDirectory;

            var exportDir = System.IO.Path.Combine(baseDir, OPTION_DIR_NAME);
            if (!Directory.Exists(exportDir))
            {
                Directory.CreateDirectory(exportDir);
            }
            optionMenuDbFile = System.IO.Path.Combine(exportDir, "option.db");

            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            //initializeMenuList();
            // Wang Issue NO.687 2018/05/18 End
        }

        #region Event Handler

        private void StackPanel_DragEnter(object sender, DragEventArgs e)
        {
            var sp = sender as StackPanel;
            if (sp != null)
            {
                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                //var current = sp.DataContext as AisMenuItem;
                //if (current.Mode != ControlMode.None)
                //{
                //    e.Effects = DragDropEffects.None;
                //    return;
                //}
                //else if (e.Effects == DragDropEffects.Copy)
                //{
                //    AisMenuItem dragData = e.Data.GetData("aisMenu") as AisMenuItem;
                //    if (dragData == null || hasMenuItem(dragData.Mode))
                //    {
                //        e.Effects = DragDropEffects.None;
                //        return;
                //    }
                //}
                var current = sp.DataContext as AisMenuItem;
                if (current.Mode != ControlMode.None)
                {
                    e.Effects = DragDropEffects.None;
                    e.Handled = true;
                    return;
                }
                else if (e.Effects == DragDropEffects.Copy)
                {
                    AisMenuItem dragData = e.Data.GetData("aisMenu") as AisMenuItem;
                    if (dragData == null || hasMenuItem(dragData.Mode) || !this.GroupKey.Equals(dragData.GroupKey))
                    {
                        e.Effects = DragDropEffects.None;
                        e.Handled = true;
                        return;
                    }
                }
                else if (e.Effects == DragDropEffects.Move)
                {
                    AisMenuItem dragData = e.Data.GetData("aisMenu") as AisMenuItem;
                    if (dragData == null || !this.GroupKey.Equals(dragData.GroupKey))
                    {
                        e.Effects = DragDropEffects.None;
                        e.Handled = true;
                        return;
                    }
                }
                // Wang Issue NO.687 2018/05/18 End

                sp.Background = HILIGHT_BRUSH;
            }
        }

        private void StackPanel_DragLeave(object sender, DragEventArgs e)
        {
            var sp = sender as StackPanel;
            if (sp != null)
            {
                sp.Background = null;
            }
        }

        private void StackPanel_DragOver(object sender, DragEventArgs e)
        {
            var sp = sender as StackPanel;
            if (sp != null)
            {
                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                //var current = sp.DataContext as AisMenuItem;
                //if (current.Mode != ControlMode.None)
                //{
                //    e.Effects = DragDropEffects.None;
                //    e.Handled = true;
                //    return;
                //}
                //else if (e.Effects == DragDropEffects.Copy)
                //{
                //    AisMenuItem dragData = e.Data.GetData("aisMenu") as AisMenuItem;
                //    if (dragData == null || hasMenuItem(dragData.Mode))
                //    {
                //        e.Effects = DragDropEffects.None;
                //        return;
                //    }
                //}
                var current = sp.DataContext as AisMenuItem;
                if (current.Mode != ControlMode.None)
                {
                    e.Effects = DragDropEffects.None;
                    e.Handled = true;
                    return;
                }
                else if (e.Effects == DragDropEffects.Copy)
                {
                    AisMenuItem dragData = e.Data.GetData("aisMenu") as AisMenuItem;
                    if (dragData == null || hasMenuItem(dragData.Mode) || !this.GroupKey.Equals(dragData.GroupKey))
                    {
                        e.Effects = DragDropEffects.None;
                        e.Handled = true;
                        return;
                    }
                }
                else if (e.Effects == DragDropEffects.Move)
                {
                    AisMenuItem dragData = e.Data.GetData("aisMenu") as AisMenuItem;
                    if (dragData == null || !this.GroupKey.Equals(dragData.GroupKey))
                    {
                        e.Effects = DragDropEffects.None;
                        e.Handled = true;
                        return;
                    }
                }
                // Wang Issue NO.687 2018/05/18 End
            }
        }

        private void StackPanel_PreviewDrop(object sender, DragEventArgs e)
        {
            var dragData0 = e.Data;
            AisMenuItem dragData = dragData0.GetData("aisMenu") as AisMenuItem;
            StackPanel sp = sender as StackPanel;
            var currentItem = sp.DataContext as AisMenuItem;

            if (currentItem.Mode != ControlMode.None)
            {
                // TODO 位置をずらして入れる
                return;
            }

            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            if (!this.GroupKey.Equals(dragData.GroupKey))
            {
                return;
            }
            // Wang Issue NO.687 2018/05/18 End

            if (e.Effects == DragDropEffects.Copy)
            {
                copyDroppedMenuItem(dragData, currentItem);
            }
            else
            {
                moveDroppedMenuItem(dragData, currentItem);
            }
        }

        private void ItemsControl_ContextMenuOpening(object sender, System.Windows.Controls.ContextMenuEventArgs e)
        {
            deleteTarget = null;
            var src = e.OriginalSource as FrameworkElement;
            if (src != null)
            {
                var item = src.DataContext as AisMenuItem;
                if (item != null)
                {
                    if (item.Mode == ControlMode.None)
                    {
                        // コンテキストメニュー非表示
                        e.Handled = true;
                    }
                    else
                    {
                        deleteTarget = item;
                    }
                }
            }
        }

        private void DeleteMenuItem_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (deleteTarget != null)
            {
                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                //removeMenuItem(selectedItem.Row, selectedItem.Col);
                removeMenuItem(deleteTarget.Row, deleteTarget.Col, deleteTarget.GroupKey);
                // Wang Issue NO.687 2018/05/18 End
            }
        }

        private void StackPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                var sp = sender as StackPanel;
                var dc = sp.DataContext as AisMenuItem;
                if (dc != null)
                {
                    openMainWindow(dc.Mode);
                }
            }
        }

        private void StackPanel_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            dragSrc = null;
            var sp = sender as StackPanel;
            if (sp != null)
            {
                var aim = sp.DataContext as AisMenuItem;
                if (aim != null)
                {
                    if (aim.Mode != ControlMode.None)
                    {
                        dragSrc = sp;
                        return;
                    }
                }
            }
        }

        private void StackPanel_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (dragSrc == sender)
                {
                    var dragItem = dragSrc.DataContext as AisMenuItem;
                    // Wang Issue NO.687 2018/05/18 Start
                    // 1.階層DAC追加(オプションテンプレート表示統一)
                    // 2.DAC Excel メニュー整理
                    //if (dragItem != null)
                    if (dragItem != null && this.GroupKey.Equals(dragItem.GroupKey))
                    // Wang Issue NO.687 2018/05/18 End
                    {
                        var dragData = new DataObject("aisMenu", dragItem);
                        DragDrop.DoDragDrop(dragSrc, dragData, DragDropEffects.Move);
                    }
                }

            }
        }

        #endregion

        #region Private Method

        private void initializeMenuList()
        {
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            //itemList = loadMenuListDb();
            itemList = loadMenuListDb(this.GroupKey);
            // Wang Issue NO.687 2018/05/18 End
            this.DataContext = itemList;
        }

        private ObservableCollection<AisMenuItem> loadMenuListDb(string groupKey)
        {
            var aisItemList = new ObservableCollection<AisMenuItem>();
            AisMenuItem[,] resultTab = new AisMenuItem[4, 3];

            using (var db = new LiteDatabase(optionMenuDbFile))
            {
                try
                {
                    // Wang Issue NO.687 2018/05/18 Start
                    // 1.階層DAC追加(オプションテンプレート表示統一)
                    // 2.DAC Excel メニュー整理
                    //var col = db.GetCollection<AisMenuItemE>("menuItems");
                    //var results = col.FindAll();
                    //foreach (var entity in results)
                    //{
                    //    var item = fromEntity(entity);
                    //    resultTab[item.Row, item.Col] = item;
                    //}
                    var col = db.GetCollection<AisMenuItemE>("menuItems");
                    var results = col.Find(x => x.GroupKey.Equals(groupKey));

                    if (groupKey.Equals(Properties.Properties.AIS_TEMPLATE_MENU_GROUPKEY))
                    {
                        foreach (AisMenuItemE entity in results)
                        {
                            AisMenuItem item = fromEntity(entity);
                            for (int i = 0; i < OptionTemplateListSrc.thisList.Count; i++)
                            {
                                if (OptionTemplateListSrc.thisList[i].Mode == item.Mode && OptionTemplateListSrc.thisList[i].IsValid)
                                {
                                    resultTab[item.Row, item.Col] = item;
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (AisMenuItemE entity in results)
                        {
                            AisMenuItem item = fromEntity(entity);
                            resultTab[item.Row, item.Col] = item;
                        }
                    }
                    // Wang Issue NO.687 2018/05/18 End
                }
                catch (Exception ex)
                {
                    var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                    logger.Error(msg, ex);
                    AisMessageBox.DisplayErrorMessageBox(msg);
                }
            }

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    var teim = resultTab[i, j];
                    if (teim == null)
                    {
                        teim = new AisMenuItem { Row = i, Col = j, Opacity = 0, Name = "dummy" };
                    }
                    aisItemList.Add(teim);
                }
            }

            return aisItemList;
        }

        private void openMainWindow(ControlMode mode)
        {
            // Wang Issue AISTEMP-123 Start
            if (!mode.Equals(ControlMode.None))
            // Wang Issue AISTEMP-123 End
            {
                MainWindow mainWindow = new MainWindow(mode, true);
                mainWindow.Show();

                Window window = Window.GetWindow(this);
                mainWindow.ToolTip = window.ToolTip;
                window.Close();
            }
        }


        private void copyDroppedMenuItem(AisMenuItem dragData, AisMenuItem currentItem)
        {
            AisMenuItem existing = getMenuItem(dragData.Mode);
            if (existing != null)
            {
                // 既にあるので追加しない
                return;
            }

            var newMenuItem = (AisMenuItem)dragData.Clone();
            newMenuItem.Row = currentItem.Row;
            newMenuItem.Col = currentItem.Col;

            bool modified = insertMenuItemDb(newMenuItem);
            if (modified)
            {
                initializeMenuList();
            }
        }

        private void moveDroppedMenuItem(AisMenuItem dragData, AisMenuItem currentItem)
        {
            var newMenuItem = (AisMenuItem)dragData.Clone();
            newMenuItem.Row = currentItem.Row;
            newMenuItem.Col = currentItem.Col;

            bool modified = updateMenuItemDb(dragData.Row, dragData.Col, newMenuItem);
            if (modified)
            {
                initializeMenuList();
            }

            return;
        }

        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        //private void removeMenuItem(int row, int column)
        //{
        //    bool modified = deleteMenuItemDb(row, column);
        //    if (modified)
        //    {
        //        initializeMenuList();
        //    }
        //}
        private void removeMenuItem(int row, int column, string keyword)
        {
            bool modified = deleteMenuItemDb(row, column, keyword);
            if (modified)
            {
                initializeMenuList();
            }
        }
        // Wang Issue NO.687 2018/05/18 End

        private AisMenuItem getMenuItem(ControlMode mode)
        {
            foreach (var item in itemList)
            {
                if (item.Mode == mode)
                {
                    return item;
                }
            }

            return null;
        }

        private bool hasMenuItem(ControlMode mode)
        {
            foreach (var item in itemList)
            {
                if (item.Mode == mode)
                {
                    return true;
                }
            }
            return false;
        }

        #endregion

        #region LiteDB

        private bool insertMenuItemDb(AisMenuItem ami)
        {
            var entity = toEntity(ami);
            using (var db = new LiteDatabase(optionMenuDbFile))
            {
                try
                {
                    var col = db.GetCollection<AisMenuItemE>("menuItems");
                    // Wang Issue NO.687 2018/05/18 Start
                    // 1.階層DAC追加(オプションテンプレート表示統一)
                    // 2.DAC Excel メニュー整理
                    //var result = col.FindOne(x => x.Row == ami.Row && x.Col == ami.Col);
                    var result = col.FindOne(x => x.Row == ami.Row && x.Col == ami.Col && x.GroupKey.Equals(ami.GroupKey));
                    // Wang Issue NO.687 2018/05/18 End
                    Debug.Assert(result == null, string.Format("Doc with ({0},{1}) exists.", ami.Row, ami.Col));
                    col.Insert(entity);
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                    logger.Error(msg, ex);
                    AisMessageBox.DisplayErrorMessageBox(msg);
                    return false;
                }
            }
        }

        private bool updateMenuItemDb(int row, int column, AisMenuItem ami)
        {
            var entity = toEntity(ami);
            using (var db = new LiteDatabase(optionMenuDbFile))
            {
                try
                {
                    var col = db.GetCollection<AisMenuItemE>("menuItems");
                    // Wang Issue NO.687 2018/05/18 Start
                    // 1.階層DAC追加(オプションテンプレート表示統一)
                    // 2.DAC Excel メニュー整理
                    //var result = col.FindOne(x => x.Row == row && x.Col == column);
                    var result = col.FindOne(x => x.Row == row && x.Col == column && x.GroupKey.Equals(ami.GroupKey));
                    // Wang Issue NO.687 2018/05/18 End
                    Debug.Assert(result != null, string.Format("Doc with ({0},{1}) exists.", ami.Row, ami.Col));
                    entity.AisMenuItemEId = result.AisMenuItemEId;
                    col.Update(entity);
                    return true;
                }
                catch (Exception ex)
                {
                    var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                    logger.Error(msg, ex);
                    AisMessageBox.DisplayErrorMessageBox(msg);
                    return false;
                }
            }
        }

        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        //private bool deleteMenuItemDb(int row, int column)
        //{
        //    using (var db = new LiteDatabase(optionMenuDbFile))
        //    {
        //        try
        //        {
        //            var col = db.GetCollection<AisMenuItemE>("menuItems");
        //            int deleted = col.Delete(x => x.Row == row && x.Col == column);
        //            if (deleted > 0)
        //            {
        //                return true;
        //            }

        //        }
        //        catch (Exception ex)
        //        {
        //            var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
        //            logger.Error(msg, ex);
        //            AisMessageBox.DisplayErrorMessageBox(msg);
        //        }
        //    }
        //    return false;
        //}
        private bool deleteMenuItemDb(int row, int column, string groupKey)
        {
            using (var db = new LiteDatabase(optionMenuDbFile))
            {
                try
                {
                    var col = db.GetCollection<AisMenuItemE>("menuItems");
                    int deleted = col.Delete(x => x.Row == row && x.Col == column && x.GroupKey.Equals(groupKey));
                    if (deleted > 0)
                    {
                        return true;
                    }

                }
                catch (Exception ex)
                {
                    var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                    logger.Error(msg, ex);
                    AisMessageBox.DisplayErrorMessageBox(msg);
                }
            }
            return false;
        }
        // Wang Issue NO.687 2018/05/18 End

        private AisMenuItemE toEntity(AisMenuItem item)
        {
            AisMenuItemE entity = new AisMenuItemE();
            entity.Row = item.Row;
            entity.Col = item.Col;
            entity.Mode = (int)item.Mode;
            entity.Opacity = item.Opacity;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            entity.GroupKey = item.GroupKey;
            // Wang Issue NO.687 2018/05/18 End
#if MULTI_LANG
            entity.Name = item.NameId;
            entity.Name2 = item.NameId2;
#else
            entity.Name = item.Name;
            entity.Name2 = item.GetName2();
#endif
            return entity;
        }

        private AisMenuItem fromEntity(AisMenuItemE item)
        {
            AisMenuItem entity = new AisMenuItem();
            entity.Row = item.Row;
            entity.Col = item.Col;
            entity.Mode = (ControlMode)item.Mode;
            entity.Opacity = item.Opacity;
            entity.Name = item.Name;
            entity.Name2 = item.Name2;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            entity.GroupKey = item.GroupKey;
            // Wang Issue NO.687 2018/05/18 End

            return entity;
        }

        public class AisMenuItemE
        {
            [BsonId]
            public ObjectId AisMenuItemEId { get; set; }
            public int Col { get; set; }
            public int Row { get; set; }
            public int Mode { get; set; }

            public double Opacity { get; set; }

            public string Name { get; set; }
            public string Name2 { get; set; }

            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            public string GroupKey { get; set; }
            // Wang Issue NO.687 2018/05/18 End
        }

        #endregion

        private static readonly ILog logger = LogManager.GetLogger(typeof(OptionMenuGrid));

        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        private void Control_Loaded(object sender, RoutedEventArgs e)
        {
            initializeMenuList();
            if (!this.IsVisible)
                return;
            Grid grid = FoaCore.Common.Util.WpfUtil.FindVisualChild<Grid>(this.menuItemControl, "grid2");
            int rows = 1;

            if (Properties.Properties.AIS_TEMPLATE_MENU_GROUPKEY.Equals(GroupKey))
            {
                rows = 2;
            }
            else if (Properties.Properties.AIS_OPTION_MENU_GROUPKEY.Equals(GroupKey))
            {
                rows = 3;
            }

            for (int i = 0; i < rows; i++ )
            {
                grid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });
            }
            
        }
        // Wang Issue NO.687 2018/05/18 End
    }
}
