﻿using System.Windows;
using System.Windows.Media;

namespace DAC.Model.View.Controls
{
    public class AisAttachedProperty
    {
        public static readonly DependencyProperty PressedForegroundProperty =
            DependencyProperty.RegisterAttached(
                "PressedForeground",
                typeof(SolidColorBrush),
                typeof(AisAttachedProperty),
                new FrameworkPropertyMetadata(new SolidColorBrush(Colors.Black))
                );
        public static SolidColorBrush GetPressedForeground(UIElement element)
        {
            return (SolidColorBrush)element.GetValue(PressedForegroundProperty);
        }
        public static void SetPressedForeground(UIElement element, SolidColorBrush value)
        {
            element.SetValue(PressedForegroundProperty, value);
        }

        public static readonly DependencyProperty PressedBackgroundProperty =
            DependencyProperty.RegisterAttached(
                "PressedBackground",
                typeof(SolidColorBrush),
                typeof(AisAttachedProperty),
                new FrameworkPropertyMetadata(new SolidColorBrush(Colors.White))
                );
        public static SolidColorBrush GetPressedBackground(UIElement element)
        {
            return (SolidColorBrush)element.GetValue(PressedBackgroundProperty);
        }
        public static void SetPressedBackground(UIElement element, SolidColorBrush value)
        {
            element.SetValue(PressedBackgroundProperty, value);
        }

        public static readonly DependencyProperty PressedBorderBrushProperty =
            DependencyProperty.RegisterAttached(
                "PressedBorderBrush",
                typeof(SolidColorBrush),
                typeof(AisAttachedProperty),
                new FrameworkPropertyMetadata(new SolidColorBrush(Colors.White))
                );
        public static SolidColorBrush GetPressedBorderBrush(UIElement element)
        {
            return (SolidColorBrush)element.GetValue(PressedBorderBrushProperty);
        }
        public static void SetPressedBorderBrush(UIElement element, SolidColorBrush value)
        {
            element.SetValue(PressedBorderBrushProperty, value);
        }
    }
}
