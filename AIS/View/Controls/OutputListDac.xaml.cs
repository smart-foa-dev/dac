﻿using DAC.AExcel;
using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View.Helpers;
using DAC.View.Modal;
using FoaCore.Common;
using FoaCore.Common.Model;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using FoaCoreCom.dac.model;
using FoaCoreCom.dac.util;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Net.Http;
using System.Reflection;

namespace DAC.View.Controls
{
    /// <summary>
    /// OutputListDacVertical.xaml の相互作用ロジック
    /// </summary>
    public partial class OutputListDac : UserControl
    {
        private StackPanel dragSrcIC;

        private ExportList exportList;

        private RegisteredItemInfo selectedItem;

        public OutputListDac()
        {
            InitializeComponent();

            //InitItems();
        }

        public void SetExportList(ExportList el)
        {
            exportList = el;
        }

        public void InitItems()
        {
            List<RegisteredItemInfo> items = getRegisteredTemplate();
            this.DataContext = items;
        }

        private void UpdateView()
        {
            this.ic_OutputListDacVertical.Items.Refresh();
        }

        private async void UpdateOnlineStatus2(List<RegisteredItemInfo> suspects)
        {
            if (suspects.Count == 0)
            {
                return;
            }

            await Task.Run(() =>
            {
                foreach (var s in suspects)
                {
                    var filepath = System.IO.Path.Combine(AisConf.DacRegistryFolderPath,s.DacId, s.FileName);
                    bool isOnline = isNecessaryRedFrame(filepath);
                    s.IsOnline = isOnline;
                }
            });
            this.ic_OutputListDacVertical.Items.Refresh();
        }

        private List<RegisteredItemInfo> getRegisteredTemplate()
        {
            List<RegisteredItemInfo> data = new List<RegisteredItemInfo>();

            try
            {
                if (!Directory.Exists(AisConf.DacRegistryFolderPath))
                {
                    Directory.CreateDirectory(AisConf.DacRegistryFolderPath);
                }
                var suspects = new List<RegisteredItemInfo>();
                var subfolders = Directory.GetDirectories(AisConf.DacRegistryFolderPath);
                foreach (var subfolder in subfolders)
                {  
                    DirectoryInfo dInfo = new DirectoryInfo(subfolder);
                    string dacId = dInfo.Name;
                    if(Directory.Exists(Path.Combine(subfolder,"__FOA")))
                    {
                        var manifestfilepath = Path.Combine(subfolder, "__FOA", "manifest.json");
                        if(File.Exists(manifestfilepath))
                        {
                            DacManifest manifest = DacManifest.loadFromFile(manifestfilepath);
                            DacManifest.ManifestFileInfo mainDac = manifest.GetMainDacInfo();
                            string filename = mainDac.name;
                            FileInfo fileInfo = new FileInfo(Path.Combine(subfolder, filename));
                            string[] categoryAndDispName = parseFilename(fileInfo.Name);
                            string cat = categoryAndDispName[0];
                            string dispName = categoryAndDispName[1];
                            ControlMode mode = ControlModeUtil.getControlModeFromString(cat);

                            RegisteredItemInfo itemInfo = new RegisteredItemInfo();
                            itemInfo.Mode = mode;
                            itemInfo.Name = dispName;
                            itemInfo.CreatedTimeDT = fileInfo.LastWriteTime;
                            itemInfo.CreatedTime = fileInfo.LastWriteTime.ToString("yyyy.M.dd HH:mm");
                            itemInfo.TemplateName = cat;
                            itemInfo.FileName = fileInfo.FullName;
                            itemInfo.IsOnline = false;
                            itemInfo.DacId = dacId;
                            // Wang Issue of quick graph 2018/06/01 End
                            //Multi_Dac sunyi 2018/11/30 start
                            //アウトプット追加
                            if (mode == ControlMode.MultiDac)
                            {
                                //Multi_Dac sunyi 2018/11/30　end
                                data.Add(itemInfo);
                            }
                        
                        }
                    }
                }
                UpdateOnlineStatus2(suspects);
                setCreateUserName(data);
                data.Sort(delegate (RegisteredItemInfo x, RegisteredItemInfo y)
                {
                    return -1*(x.CreatedTimeDT.CompareTo(y.CreatedTimeDT));
                });
                /*
                if (!Directory.Exists(AisConf.RegistryFolderPath))
                {
                    Directory.CreateDirectory(AisConf.RegistryFolderPath);
                }

                var suspects = new List<RegisteredItemInfo>();

                //updateTime sort
                //var pathes = Directory.GetFiles(AisConf.RegistryFolderPath).OrderByDescending(f => File.GetCreationTime(f));
                var pathes = Directory.GetFiles(AisConf.RegistryFolderPath).OrderByDescending(f => File.GetLastWriteTime(f));
                foreach (string filePath in pathes)
                {
                    FileInfo fileInfo = new FileInfo(filePath);
                    if (fileInfo.Extension.ToUpper() != ".XLSM" || fileInfo.Attributes.HasFlag(FileAttributes.Hidden))
                    {
                        continue;
                    }

                    string[] categoryAndDispName = parseFilename(fileInfo.Name);
                    string cat = categoryAndDispName[0];
                    string dispName = categoryAndDispName[1];
                    ControlMode mode = ControlModeUtil.getControlModeFromString(cat);

                    RegisteredItemInfo itemInfo = new RegisteredItemInfo();
                    itemInfo.Mode = mode;
                    itemInfo.Name = dispName;
                    itemInfo.CreatedTime = fileInfo.LastWriteTime.ToString("yyyy.M.dd HH:mm");
                    itemInfo.TemplateName = cat;
                    itemInfo.FileName = fileInfo.Name;
                    itemInfo.IsOnline = false;
                    // Wang Issue of quick graph 2018/06/01 End
                    //Multi_Dac sunyi 2018/11/30 start
                    //アウトプット追加
                    if (mode == ControlMode.MultiDac)
                    {
                    //Multi_Dac sunyi 2018/11/30　end
                        data.Add(itemInfo);
                    }
                }

                UpdateOnlineStatus2(suspects);
                setCreateUserName(data);
                */
            }
            catch (Exception ex)
            {
                string message = string.Format(Properties.Message.AIS_E_051, ex.Message);

//#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
//#endif
                AisMessageBox.DisplayErrorMessageBox(message);

            }

            return data;
        }

        private async void setCreateUserName(List<RegisteredItemInfo> infoList)
        {
            if (infoList.Count < 1)
            {
                return;
            }

            foreach (RegisteredItemInfo info in infoList)
            {
                // Wang Issue of quick graph 2018/06/01 Start
                // Can NOT edit when it is a quick graph.
                //info.CanEdit = true;
                if (ControlMode.QuickGraph.Equals(info.Mode))
                {
                    info.CanEdit = false;
                }
                else
                {
                    info.CanEdit = true;
                }
                // Wang Issue of quick graph 2018/06/01 End
                /*
                Window_Home home = Application.Current.MainWindow as Window_Home;
                if (home == null)
                {
                    continue;
                }
                */

                MainWindow main = Application.Current.MainWindow as MainWindow;
                if (main != null)
                {
                    continue;
                }

                await Task.Run(() =>
                {
                    string filepath = Path.Combine(AisConf.DacRegistryFolderPath, info.FileName);
                    string[] param = getMissionIdAndCreaterFromExcel(filepath, info.Mode);
                    info.MissionId = param[0];
                    info.CreateUserName = param[1];
                    /*
                    if (string.IsNullOrEmpty(param[0]) ||
                        string.IsNullOrEmpty(param[1]))
                    {
                        info.CanEdit = true;
                    }
                    else
                    {
                        info.CanEdit = true;
                    }
                     * */
                });
            }

            this.ic_OutputListDacVertical.Items.Refresh();
        }

        private List<string> getAgentIdList()
        {
            List<string> agentIdList = new List<string>();
            string url = CmsUrl.GetUserList();

            //using (WebClient client = new WebClient())
            using (WebClient client = AisUtil.getProxyWebClient())
            {
                client.Encoding = Encoding.UTF8;
                string response = client.DownloadString(url);

                List<Users> users = JsonConvert.DeserializeObject<List<Users>>(response);
                foreach (Users user in users)
                {
                    agentIdList.Add(user.UserName);
                }
            }

            return agentIdList;
        }

        private string getMissionAgentId(string missionId)
        {
            string agentId = string.Empty;

            string query = string.Format("id={0}", missionId);
            string url = string.Format("{0}?{1}", CmsUrl.GetMissionUrl(), query);

            //using (WebClient client = new WebClient())
            using (WebClient client = AisUtil.getProxyWebClient())
            {
                client.Encoding = Encoding.UTF8;
                string response = client.DownloadString(url);

                Mission mission = JsonConvert.DeserializeObject<Mission>(response);
                agentId = mission.AgentId;
            }

            return agentId;
        }

        /// <summary>
        /// ミッションIDと製作者名を取得
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <returns>0: ミッションID 1: 製作者名</returns>
        private string[] getMissionIdAndCreaterFromExcel(string filePath, ControlMode mode)
        {
            string[] ary = new string[]
            {
                string.Empty,   // ミッションID
                "不明"          // 製作者名
            };
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                string sheetName = Keywords.PARAM_SHEET;
                if (mode == ControlMode.Dac)
                {
                    // sheetName = "USERINFO";
                }
                SpreadsheetGear.IWorksheet sheet = AisUtil.GetSheetFromSheetName_SSG(book, sheetName);

                var range1 = sheet.Cells[0, 0, 99, 9];
                var range2 = SSGUtil.GetFindRange(range1, "ミッションID");
                if (range2 != null &&
                    range2[0, 1].Value != null)
                {
                    ary[0] = range2[0, 1].Value.ToString();
                }
                
                var range3 = sheet.Cells[0, 0, 99, 9];
                var range4 = SSGUtil.GetFindRange(range3, "製作者名");
                if (range4 != null &&
                    range4[0, 1].Value != null)
                {
                    ary[1] = range4[0, 1].Value.ToString();
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }

            return ary;
        }

        /// <summary>
        /// 赤枠が必要(オンラインで動いている)かを取得
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <returns>true: 赤枠をつける  false: 赤枠をつけない</returns>
        private bool isNecessaryRedFrame(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet paramSheet = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var range = paramSheet.Cells[0, 0, 99, 9];
                var range2 = SSGUtil.GetFindRange(range, "オンライン");
                if (range2 != null)
                {
                    if (range2[0, 1].Value.ToString() == "ONLINE")
                    {
                        return true;
                    }
                }

                return false;
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        /// <summary>
        /// データの取得元(ミッション or CTM or Grip)を取得
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <returns>データの取得元</returns>
        private DataSourceType getDataSourceType(string filePath)
        {
            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePath);
                string sheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet sheet = AisUtil.GetSheetFromSheetName_SSG(workbook, sheetName);
                SpreadsheetGear.IRange cells = sheet.Cells;

                DataSourceType dataSource = DataSourceType.MISSION;
                for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
                {
                    // null check
                    if (cells[i, 0].Value == null)
                    {
                        continue;
                    }

                    // 収集タイプ
                    if (cells[i, 0].Value.ToString() == "収集タイプ")
                    {
                        if (cells[i, 1].Value == null)
                        {
                            return dataSource;
                        }
                        if (cells[i, 1].Value.ToString() == "CTM")
                        {
                            dataSource = DataSourceType.CTM_DIRECT;
                        }
                        else if (cells[i, 1].Value.ToString() == "ミッション")
                        {
                            dataSource = DataSourceType.MISSION;
                        }
                        else // if (cells[i, 1].Value.ToString() == "GRIP")
                        {
                            dataSource = DataSourceType.GRIP;
                        }

                        break;
                    }
                }

                return dataSource;

            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close();
                }

            }
        }

        private string[] parseFilename(string filename)
        {
            string[] rtn = new string[2];

            string kind = filename.Split('_')[0];
            rtn[0] = kind;

            string dispName0 = filename.Substring(kind.Length + 1);
            string dispName = System.IO.Path.GetFileNameWithoutExtension(dispName0);
            rtn[1] = dispName;

            return rtn;
        }

        #region イベントハンドラ

        /// <summary>
        /// ラベルロード
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label_Loaded(object sender, RoutedEventArgs e)
        {
            Label l = sender as Label;
            if (l == null)
            {
                return;
            }

            // ラベル内に「_」があると、アクセスキー(キーボードショートカット)と判断され、表示されない。
            // 「__」のように2つつなげると表示されるので、ここで置き換えている
            var text = l.Content as string;
            if (text != null)
            {
                if (text.Contains('_'))
                {
                    text = text.Replace("_", "__");
                    l.Content = text;
                }
            }
        }

        private void ItemsControl_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            this.dragSrcIC = sender as StackPanel;

            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                var sp = sender as StackPanel;
                var dc = sp.DataContext as RegisteredItemInfo;
                if (dc != null)
                {
                    //ExcelUtil.StartExcelProcess(dc.FilePath, true);
					// FOA_サーバー化開発 Processing On Server Xj 2018/08/16 Start
                    var filepath = dc.FilePath;
                    if (filepath.Contains("Multi_StatusMonitor"))
                    {
                        Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
                        Microsoft.Office.Interop.Excel.Workbooks oWBooks = null;
                        Microsoft.Office.Interop.Excel.Workbook oWBook = null;

                        string workplaceId = string.Empty;
                        JArray workplaceArray = new JArray();
                        JToken workplaceJson = new JObject();

                        try
                        {
                            oXls = new Microsoft.Office.Interop.Excel.Application();
                            oXls.Visible = false; //確認のためエクセルのウィンドウを表示する

                            //エクセルファイルをオープンする
                            oWBooks = oXls.Workbooks;
                            oWBook = oWBooks.Open(filepath); // オープンするExcelファイル名

                            Microsoft.Office.Interop.Excel.Worksheet workSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWBook.Worksheets[DAC.Util.Keywords.RO_PARAM_SHEET];

                            workplaceId = (string)workSheet.get_Range("B3").Text;

                            oWBook.Close(false, filepath, false);

                            workplaceJson["work_place_id"] = workplaceId;
                            workplaceArray.Add(workplaceJson);

                            CmsHttpClient client = new CmsHttpClient(this);
                            client.CompleteHandler += resJson =>
                            {
                                if (resJson == "0")
                                {
                                    logger.Debug("WorkPlaceId =>「" + workplaceId + "」の参照処理を失敗しました。");
                                }
                            };
                            client.Post(CmsUrl.GetReaseref(), workplaceArray.ToString());
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                        }
                    }
					// FOA_サーバー化開発 Processing On Server Xj 2018/08/16 End
                    ExcelUtil.OpenExcelFile(dc.FilePath);
                    e.Handled = true;
                }
            }
        }

        private void ItemsControl_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (this.dragSrcIC == sender)
                {
                    var rii = dragSrcIC.DataContext as RegisteredItemInfo;
                    if (rii != null)
                    {
                        var fileColl = new System.Collections.Specialized.StringCollection();
                        var filepath = System.IO.Path.Combine(AisConf.RegistryFolderPath, rii.FileName);
                        fileColl.Add(filepath);
                        var dragData = new DataObject("registItem", rii);
                        dragData.SetFileDropList(fileColl);
                        dragData.SetText("FromOutput");
                        DragDrop.DoDragDrop(dragSrcIC, dragData, DragDropEffects.Copy);
                    }
                }
            }
            return;
        }

        private void ItemsControl_ContextMenuOpening(object sender, System.Windows.Controls.ContextMenuEventArgs e)
        {
            selectedItem = null;
            var sp = sender as StackPanel;

            if (sp != null)
            {
                var item = sp.DataContext as RegisteredItemInfo;
                if (item != null)
                {
                    selectedItem = item;

                    // Wang Issue of quick graph 2018/06/01 Start
                    // Items[1] refers to CopyMenuItem. Do NOT think it should be controled by CanEdit attribute.
                    //var cm = sp.ContextMenu.Items[1] as MenuItem;
                    var cm = sp.ContextMenu.Items[2] as MenuItem;
                    // Wang Issue of quick graph 2018/06/01 End
                    if (cm != null)
                    {
                        try
                        {
                            /*
                            if (selectedItem.CanEdit)
                            {
                                cm.IsEnabled = true;
                            }
                            else
                            {
                                cm.IsEnabled = false;
                            }
                            */
                        }
                        catch (Exception ex)
                        {
                            var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                            logger.Error(msg, ex);
                            AisMessageBox.DisplayErrorMessageBox(msg);
                        }
                    }

                    // Wang Issue of quick graph 2018/06/01 Start
                    // Items[2] refers to EditMenuItem. Do NOT think it should be controled by if it is can be exported.
                    //var dm = sp.ContextMenu.Items[2] as MenuItem;
                    //if (dm != null)
                    //{
                    //    try
                    //    {
                    //        if (!exportList.IsExported(item.FileName))
                    //        {
                    //            dm.IsEnabled = true;
                    //            return;
                    //        }
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        var msg = string.Format(Properties.Message.AIS_E_050, ex.Message);
                    //        logger.Error(msg, ex);
                    //        AisMessageBox.DisplayErrorMessageBox(msg);
                    //    }
                    //    dm.IsEnabled = false;
                    //}
                    // Wang Issue of quick graph 2018/06/01 End
                    return;
                }
            }
        }

        private void DeleteMenuItem_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (selectedItem == null)
            {
                return;
            }

            var filepath = selectedItem.FilePath;
			// FOA_サーバー化開発 Processing On Server Xj 2018/08/16 Start
            Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
            Microsoft.Office.Interop.Excel.Workbooks oWBooks = null;
            Microsoft.Office.Interop.Excel.Workbook oWBook = null;

            string onlineId = string.Empty;
            JToken workplaceJson = new JObject();

            try
            {
                oXls = new Microsoft.Office.Interop.Excel.Application();
                oXls.Visible = false; //確認のためエクセルのウィンドウを表示する

                //エクセルファイルをオープンする
                oWBooks = oXls.Workbooks;
                oWBook = oWBooks.Open(filepath); // オープンするExcelファイル名
                // Wang Issue of excel application not exit while dac is delete from output 20190409 Start
                ExcelUtil.MoveExcelOutofScreen(oXls);
                // Wang Issue of excel application not exit while dac is delete from output 20190409 End

                Microsoft.Office.Interop.Excel.Worksheet workSheet = (Microsoft.Office.Interop.Excel.Worksheet)oWBook.Worksheets[DAC.Util.Keywords.RO_PARAM_SHEET];

                string fileId = (string)workSheet.get_Range("B2").Text;

                oWBook.Close(false, filepath, false);
                // Wang Issue of excel application not exit while dac is delete from output 20190409 Start
                oXls.Quit();
                // Wang Issue of excel application not exit while dac is delete from output 20190409 End

                workplaceJson["file_id"] = fileId;
                workplaceJson["file_type"] = 0;

                CmsHttpClient client = new CmsHttpClient(this);
                client.CompleteHandler += resJson =>
                {
                    if (resJson == "0")
                    {
                        logger.Debug("FileId =>「" + fileId + "」の削除処理を失敗しました。");
                    }
                };
                client.Post(CmsUrl.GetDecreaseref(), workplaceJson.ToString());

            }
            catch
            {
                FoaMessageBox.ShowError("AIS_E_052");
                //                string message =
                //@"Excelファイルへのアクセスに失敗しました。
                //アクセスしたいファイルを開いている場合、閉じてから再度実行してください。";
                //                MessageBox.Show(message);
            }
            finally
            {
                if (oWBook != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                    oWBook = null;
                }

                if (oWBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBooks);
                    oWBooks = null;
                }

                if (oXls != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                    oXls = null;
                }
				try
			    {
                    //ファイル削除
                    //File.Delete(filepath);
                    string dacPath = Path.GetDirectoryName(filepath);
                    Directory.Delete(dacPath, true);
	                // ファイル一覧をリロード
	                InitItems();
				}
	            catch
	            {
	                FoaMessageBox.ShowError("AIS_E_052");
//                string message =
//@"Excelファイルへのアクセスに失敗しました。
//アクセスしたいファイルを開いている場合、閉じてから再度実行してください。";
//                MessageBox.Show(message);
	            }
            }
			// FOA_サーバー化開発 Processing On Server Xj 2018/08/16 End
        }

        private void NameMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (selectedItem != null)
            {
                // ファイル名変更
                var reg = (RegisteredItemInfo)selectedItem.Clone();
                var d = new NameEdit(reg);
                bool? applied = d.ShowDialog();

                if (applied != true)
                {
                    return;
                }

                if (renameFile(selectedItem, reg))
                {
                    InitItems();
                }
            }
        }

        private bool renameFile(RegisteredItemInfo from, RegisteredItemInfo to)
        {
            var fromPath = from.FilePath;
            var fromFilename = from.FileName;
            var toFilename = string.Format("{0}_{1}.xlsm", to.TemplateName, to.Name);
            //var toPath = System.IO.Path.Combine(AisConf.RegistryFolderPath, toFilename);
            string dacpath = Path.GetDirectoryName(from.FilePath);
            var toPath = Path.Combine(dacpath, toFilename);

            try
            {
                char[] invalidChars = System.IO.Path.GetInvalidFileNameChars();

                if (to.Name.IndexOfAny(invalidChars) >= 0)
                {
                    var msg = string.Format(Properties.Message.AIS_E_053, Properties.Message.AIS_E_070);
                    AisMessageBox.DisplayErrorMessageBox(msg);
                    return false;
                }
                File.Move(fromPath, toPath);
                FoaCoreCom.FoaCoreCom foacoreObj = new FoaCoreCom.FoaCoreCom();
                string newname = foacoreObj.ManifestRename(dacpath, toFilename);
                AisMessageBox.DisplayInfoMessageBox(newname);
                return true;
            }
            catch (ArgumentException ae)
            {

                var msg = string.Format(Properties.Message.AIS_E_053, Properties.Message.AIS_E_070);
                logger.Error(msg, ae);
                AisMessageBox.DisplayErrorMessageBox(msg);
                return false;
            }
            catch (UnauthorizedAccessException uae)
            {
                var msg = string.Format(Properties.Message.AIS_E_053, uae.Message);
                logger.Error(msg, uae);
                AisMessageBox.DisplayErrorMessageBox(msg);
                return false;
            }
            catch (IOException ioe)
            {
                var msg = string.Format(Properties.Message.AIS_E_053, ioe.Message);
                if (AisConf.UiLang.Equals("ja") && ioe.Message.StartsWith("ファイルが別のプロセスで"))
                {
                    msg = string.Format(Properties.Message.AIS_E_053, Properties.Message.AIS_E_069);
                }
                else if (AisConf.UiLang.Equals("ja") && ioe.Message.StartsWith("パスの一部が見つかりません"))
                {
                    msg = string.Format(Properties.Message.AIS_E_053, Properties.Message.AIS_E_070);
                }
                logger.Error(msg, ioe);
                AisMessageBox.DisplayErrorMessageBox(msg);
                return false;
            }
        }

        /// <summary>
        /// 内容修正
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EditMenuItem_Click(object sender, RoutedEventArgs e)
        {
            RegisteredItemInfo info = selectedItem;
            var filepath = info.FilePath;
            DataSourceType isMissionTreeSelected = DataSourceType.MISSION;

            try
            {
                isMissionTreeSelected = getDataSourceType(filepath);
            }
            catch (Exception ex)
            {
                var msg = string.Format(Properties.Message.AIS_E_054, ex.Message);
                msg += string.Format("\n" + ex.StackTrace);
                logger.Error(msg, ex);
                AisMessageBox.DisplayErrorMessageBox(msg);
            }

            MainWindow mainWindow = new MainWindow(info.Mode, false, filepath, isMissionTreeSelected);
            mainWindow.VersionInfo.ToolTip = "AIS Version: " + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion + "\tZAR";
            mainWindow.Show();

            Dac_Home homeWindow = (Dac_Home)System.Windows.Window.GetWindow(this);
            homeWindow.Close();
        }

        #endregion

        private static readonly ILog logger = LogManager.GetLogger(typeof(OutputListDacVertical));

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            InitItems();
        }

        private void CopyMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (selectedItem == null)
            {
                return;
            }

            RegisteredItemInfo info = selectedItem;
            var filepath = info.FilePath;

            // Copy some files to the clipboard.
            List<string> fileList = new List<string>();
            fileList.Add(filepath);
            Clipboard.Clear();
            Clipboard.SetData(DataFormats.FileDrop, fileList.ToArray());
        }

        

        private void ExportMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (selectedItem == null)
            {
                return;
            }
            var filepath = selectedItem.FilePath;
            string zipsrcfolder = Path.GetDirectoryName(filepath);
            string ziprootfolder = new DirectoryInfo(zipsrcfolder).Parent.FullName;
            string zipfilepath = FoaCoreCom.ais.retriever.DacHomeRetriever.zipDacFile(ziprootfolder, selectedItem.DacId);
            string manifestPath = Path.Combine(zipsrcfolder, "__FOA", "manifest.json");
            DacManifest manifest = DacManifest.loadFromFile(manifestPath);
            string DacName = string.Format("{0}_{1}", selectedItem.TemplateName,selectedItem.Name);
            JToken responseToken = JToken.Parse("{}");
            Task task = Task.Run(() =>
            {
                responseToken = FoaCoreCom.ais.retriever.DacHomeRetriever.UploadToSharePanel(zipfilepath, DacName, selectedItem.DacId, 
                    AisConf.UserGuID, AisConf.PHPRestRoot, manifest.revision).Result;
            });
            task.GetAwaiter().GetResult();
            if(responseToken["status"].ToString()!="OK")
            {
                int currentRevision = responseToken["message"]["currrentRevision"].ToObject<int>();
                MessageBoxResult result = AisMessageBox.DisplayOkCancelMessageBox("このIDのDacファイルは既にあります。強制的にアップードしますか？");
                if(result == MessageBoxResult.OK)
                {
                    manifest.revision = currentRevision + 1;
                    manifest.Save();
                    zipfilepath = FoaCoreCom.ais.retriever.DacHomeRetriever.zipDacFile(ziprootfolder, selectedItem.DacId);
                    task.GetAwaiter().GetResult();
                }
            }
        }
    }
}
