﻿using log4net;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace DAC.View.Controls
{
    /// <summary>
    /// ExcelOpenButton.xaml の相互作用ロジック
    /// </summary>
    public partial class ExcelOpenButton : Image
    {
        private bool isActive = false;

        // "Tap"イベントをルーティングイベントとして登録する。  
        public static readonly RoutedEvent TapEvent = EventManager.RegisterRoutedEvent("Tap", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ExcelOpenButton));


        public event RoutedEventHandler Tap
        {
            add { AddHandler(TapEvent, value); }
            remove { RemoveHandler(TapEvent, value); }
        }


        public ExcelOpenButton()
        {
            InitializeComponent();
        }

        public void Activate()
        {
            BitmapImage biUser = new BitmapImage();
            biUser.BeginInit();
            biUser.UriSource = new Uri(@"/DAC;component/Resources/Image/rf-orange.png", UriKind.RelativeOrAbsolute);
            biUser.EndInit();

            this.Source = biUser;
            this.IsEnabled = true;

            isActive = true;
        }

        /// <summary>
        /// エクセルオープンのイメージボタンを無効化する
        /// </summary>
        public void Deactivate()
        {
            BitmapImage biUser = new BitmapImage();
            biUser.BeginInit();
            biUser.UriSource = new Uri(@"/DAC;component/Resources/Image/rf-gray.png", UriKind.RelativeOrAbsolute);
            biUser.EndInit();

            this.Source = biUser;
            this.IsEnabled = false;

            isActive = false;
        }

        private void image_OpenExcel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount != 1)
            {
                logger.Debug("Double click!");
                return;
            }

            this.IsEnabled = false;

            try
            {
                RoutedEventArgs newEventArgs = new RoutedEventArgs(ExcelOpenButton.TapEvent);
                RaiseEvent(newEventArgs);
            }
            finally
            {
                if (isActive)
                {
                    this.IsEnabled = true;
                }
            }
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(ExcelOpenButton));
    }
}
