﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DAC.Model;
using System;
using System.IO;
using DAC.AExcel;
using System.Collections.Generic;
using DAC.Model.Util;
using DAC.View.Helpers;
using System.Linq;
using System.Threading.Tasks;
using DAC.Util;
using DAC.Properties;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO.Compression;
using FoaCore.Net;
using DAC.View.Modal;
using System.Threading;
using ICSharpCode.SharpZipLib.Zip;
using System.Text;
using log4net;
using log4net.Repository.Hierarchy;
using FoaCoreCom;

namespace DAC.View.Controls
{
    /// <summary>
    /// DacMenuGridSample.xaml の相互作用ロジック
    /// </summary>
    public partial class DacMenuGridSample : UserControl
    {
        private string resultDir = Path.Combine(AisConf.BaseDir, "tmp", "excel");

        private RegisteredTempleteItemInfo deleteTarget;

        // 2019/05/30 dn downloadため add  start

        private static readonly string[] arrayExtension = new string[] { ".xlsm", ".xlsx", ".xls" };
        // Excelを開くとき、SubDacが取得できるため、パスを作成
        private string repoDir = Path.Combine(AisConf.Config.GripFolderPath, "tmp", "dashbox");
        private string templateDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "dac");
        //private string repoDir = Path.Combine("C:\\git\\02.client\\grip2\\grip_client\\Grip\\bin\\Debug", "tmp", "dashbox");

        //public string GetFilepath(string id, string filename)
        public string GetFilepath(string id)
        {
            /*
            string dirPath = Path.Combine(templateDir, "templatezip");
            Directory.CreateDirectory(dirPath);
            
            if (!string.IsNullOrEmpty(filename))
            {
                return Path.Combine(dirPath, filename);
            }
            if(!string.IsNullOrEmpty(id))
            {
                return Path.Combine(dirPath, id + ".zip");
            }*/
            string dirPath = Path.Combine(AisConf.DacTemplateDownloadPath, id + ".zip");
            return dirPath;
        }
        // 2019/05/30 dn downloadため add  end

        public DacMenuGridSample()
        {

            InitializeComponent();

        }


        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            InitItems();
        }

        public void InitItems()
        {
            List<RegisteredTempleteItemInfo> items = getRegisteredTemplate();
            //this.DataContext = items;
        }

        private List<RegisteredTempleteItemInfo> getRegisteredTemplate()
        {
            List<RegisteredTempleteItemInfo> data = new List<RegisteredTempleteItemInfo>();

            data = getTempleteItems(data);
            try
            {
                var testItemNew = new RegisteredTempleteItemInfo(Guid.NewGuid().ToString("N").ToUpper());
                testItemNew.FileName = DAC.Properties.Resources.TEXT_MENU_MULTIDAC;
                testItemNew.Mode = ControlMode.DacTemplate;

                var testItemSample = new RegisteredTempleteItemInfo(Guid.NewGuid().ToString("N").ToUpper());
                testItemSample.FileName = DAC.Properties.Resources.TEXT_MENU_MULTIDAC_SAMPLE;
                testItemSample.Mode = ControlMode.DacSample;

            
                data.Insert(0, testItemNew);
                data.Insert(1, testItemSample);
                this.ic_templeteListDacHorizontal.Items.Refresh();

            }
            catch (Exception ex)
            {
                string message = string.Format(DAC.Properties.Message.AIS_E_051, ex.Message);

//#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
//#endif
                AisMessageBox.DisplayErrorMessageBox(message);

            }

            return data;
        }

        private string[] parseFilename(string filename)
        {
            string[] rtn = new string[2];

            string kind = filename.Split('_')[0];
            rtn[0] = kind;

            string dispName0 = filename.Substring(kind.Length + 1);
            string dispName = System.IO.Path.GetFileNameWithoutExtension(dispName0);
            rtn[1] = dispName;

            return rtn;
        }

        private async void UpdateOnlineStatus2(List<RegisteredTempleteItemInfo> suspects)
        {
            if (suspects.Count == 0)
            {
                return;
            }

            await Task.Run(() =>
            {
                foreach (var s in suspects)
                {
                    var filepath = System.IO.Path.Combine(AisConf.DacRegistryFolderPath, s.DacId, s.FileName);
                    bool isOnline = isNecessaryRedFrame(filepath);
                    s.IsOnline = isOnline;
                }
            });
            this.ic_templeteListDacHorizontal.Items.Refresh();
        }

        /// <summary>
        /// 赤枠が必要(オンラインで動いている)かを取得
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <returns>true: 赤枠をつける  false: 赤枠をつけない</returns>
        private bool isNecessaryRedFrame(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet paramSheet = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var range = paramSheet.Cells[0, 0, 99, 9];
                var range2 = SSGUtil.GetFindRange(range, "オンライン");
                if (range2 != null)
                {
                    if (range2[0, 1].Value.ToString() == "ONLINE")
                    {
                        return true;
                    }
                }

                return false;
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        public string unZipTemplateFile(string zipFileUrl, string unZipDir)
        {
            //unZip後の保存パス
            //String unZipDir = null;

            //拡張子名を取得する
            String extensionName = Path.GetExtension(zipFileUrl);

            //拡張子名は「.zip」かどうかを判断する
            if (!extensionName.Equals(".zip"))
            {
                //return unZipDir;
                return null;
            }

            //zipFileは存在かどうかを判断する
            if (!File.Exists(zipFileUrl))
            {
                //return unZipDir;
                return null;
            }
            // Processing On Server dn 2018/08/30 start
            //コードフォーマットを修正する
            //Encoding utf = Encoding.GetEncoding("utf-8");
            Encoding utf = Encoding.GetEncoding("Shift_JIS");
            ZipConstants.DefaultCodePage = utf.CodePage;
            // Processing On Server dn 2018/08/30 end

            //unZip後の保存パス
            //unZipDir = Path.GetDirectoryName(zipFileUrl);
            // Processing On Server dn 2018/09/13 start
            // Processing On Server dn 2018/09/13 end
            logger.Debug("Start Unzip");
            ZipInputStream s = null;
            try
            {
                if(!Directory.Exists(unZipDir))
                {
                    Directory.CreateDirectory(unZipDir);
                }
                //ファイルを解凍する
                if (!unZipDir.EndsWith("/"))
                {
                    String unZipDirNew = unZipDir;
                    s = new ZipInputStream(File.OpenRead(zipFileUrl));
                    ZipEntry theEntry;
                    while ((theEntry = s.GetNextEntry()) != null)
                    {
                        string directoryName = Path.GetDirectoryName(theEntry.Name);
                        string fileName = Path.GetFileName(theEntry.Name);

                        //フォルダーは存在かどうかを判断する
                        if (directoryName.Length > 0)
                        {
                            //フォルダーを作成する
                            Directory.CreateDirectory(Path.Combine(unZipDirNew,directoryName));
                        }
                        if (!directoryName.EndsWith("/"))
                        {
                            //directoryName += "/";
                            if (fileName != String.Empty)
                            {
                                using (FileStream streamWriter = File.Create(Path.Combine(unZipDirNew,theEntry.Name)))
                                {
                                    int size = 2048;
                                    byte[] data = new byte[2048];
                                    while (true)
                                    {
                                        size = s.Read(data, 0, data.Length);
                                        if (size > 0)
                                        {
                                            streamWriter.Write(data, 0, size);
                                        }
                                        else
                                        {
                                            break;
                                        }
                                    }
                                }
                                File.SetLastAccessTime(Path.Combine(unZipDirNew, theEntry.Name), theEntry.DateTime);
                            }
                        }
                    }
                    s.Close();
                }
                logger.Debug("Unzipped File: " + unZipDir);
                return unZipDir;
            }
            catch (Exception e)
            {
                s.Close();
                unZipDir = null;
                return unZipDir;
            }
        }
        private void WrapPanel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                var sp = sender as WrapPanel;
                var dc = sp.DataContext as RegisteredTempleteItemInfo;
                if (dc != null)
                {
                    if (dc.FileName.Equals(DAC.Properties.Resources.TEXT_MENU_MULTIDAC)
                        || dc.FileName.Equals(DAC.Properties.Resources.TEXT_MENU_MULTIDAC_SAMPLE))
                    {
                        string dacFile = CreateWorkingFile(dc.FileName);
                        logger.Debug("dacFile: " + dacFile);
                        Dac_Home.dacFile = dacFile;
                        ExcelUtil.CreateExcelFile(dacFile);
                    }
                    else
                    {
                            //InitItems();
                            //var newdcs = this.DataContext as List<RegisteredTempleteItemInfo>;
                            //foreach(var newdc in newdcs)
                            //{
                            //    if(newdc.FileName.Equals(dc.FileName))
                            //    {
                            //        dc.id = newdc.FileName;
                            //    }
                            //}
                            string Id = "";    
                            Task task1 = Task.Run(() =>
                            {
                                Id = getFileIdTask(dc.FileName).Result;
                            });
                            task1.GetAwaiter().GetResult();

                            Task task2 = Task.Run(() =>
                            {
                                DownloadFile(Id, dc.FileName, dc);
                            });
                            task2.GetAwaiter().GetResult();
    
                        
                    }
                }
                //dac_home execle show top
                e.Handled = true;
            }
        }

        public static async Task<string> getFileIdTask(string fileName)
        {
            string url = string.Format("http://{0}:{1}/grip/rest/dacHome/file/getFileId", AisConf.Config.GripServerHost, AisConf.Config.GripServerPort);
            var content = new MultipartFormDataContent();
            content.Add(new StringContent(fileName), "name");
            var client = new FoaHttpClient();
            string resultId = await client.Post(url, content);
            return resultId;
        }

        private string CreateWorkingFile(string filename)
        {
            // 1. DacIDを発行する
            string dacId = Guid.NewGuid().ToString("N").ToUpper();

            // 2. DACフォルダを作る
            string tmpDacPath = Path.Combine(AisConf.DacTmpFolderPath, dacId);

            if(Directory.Exists(tmpDacPath))
            {
                Directory.Delete(tmpDacPath, true);
                
            }
            Directory.CreateDirectory(tmpDacPath);
            string manifestPath = Path.Combine(tmpDacPath, "__FOA");
            Directory.CreateDirectory(manifestPath);            
            string filePathSrc ="";
            if (filename.Equals(DAC.Properties.Resources.TEXT_MENU_MULTIDAC))
            { 
                //DACテンプレートのパス
                filePathSrc = System.IO.Path.Combine(AisConf.Config.DacFilePath, "DACReport.xlsm");
                
            }
            else
            {
                //DACテンプレートのパス
                filePathSrc = System.IO.Path.Combine(AisConf.Config.DacFilePath, "DACReportSample.xlsm");
            }
            logger.Debug("Template file: " + filePathSrc);
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            //string fileName = string.Format("{0}_{1}.xlsm", "DAC", DateTime.Now.ToString("yyyyMMddHHmmss"));
            //dac_home dacFileName修正 名称の階層DACを削除 　影響範囲広い、一応戻します
            string fileName = string.Format("{0}_{1}.xlsm", DAC.Properties.Resources.TEXT_STD_MULTIDAC, DateTime.Now.ToString("yyyyMMddHHmmss"));
            //string fileName = string.Format("{0}.xlsm", DateTime.Now.ToString("yyyyMMddHHmmss"));
            // Wang Issue NO.687 2018/05/18 End

            // 3. MainDACファイルを作成する
            string filePathDest = Path.Combine(/*resultDir*/tmpDacPath, fileName);
            if (!File.Exists(filePathSrc))
            {
                MessageBox.Show("DACテンプレートファイル(" + filePathSrc + ")が存在しません。FOA Clientのインストールにミスがあります。ご確認ください。", "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return "";
            }
            File.Copy(filePathSrc, filePathDest);
            // 4. Manifest.jsonを作る
            FoaCoreCom.FoaCoreCom FoaCoreComObj = new FoaCoreCom.FoaCoreCom();
            FoaCoreComObj.InitializeManifest(tmpDacPath, dacId, fileName, AisConf.DomainId);
            return filePathDest;
        }

        // 2019/05/30 dongning downloadため add Start
        public async Task<string> DownloadFile(string id, string filename, RegisteredTempleteItemInfo fo)
        {
            return await Task.Run<string>(() =>
            {
                string downloadPath = string.Empty;
                downloadPath = GetFilepath(id);
                //TODO host、post取得方法再作成
                string url = string.Format("http://{0}:{1}/grip/rest/dacHome/file/download?id={2}", AisConf.Config.GripServerHost, AisConf.Config.GripServerPort, id);

                WebClient client = new WebClient();
                string outputfolder = AisConf.DacTmpFolderPath;
                client.DownloadDataCompleted += (sender, eventArgs) =>
                {
                    byte[] fileData = eventArgs.Result;
                    using (FileStream fileStream = new FileStream(downloadPath, FileMode.Create))
                    {
                        fileStream.Write(fileData, 0, fileData.Length);
                        fileStream.Flush();
                        fileStream.Close();
                        if (null != fo)
                        {
                            fo.FilePrepared = true;
                        }
                    }
                    string outputpath = unZipTemplateFile(downloadPath,Path.Combine(outputfolder, id));
                    if (!string.IsNullOrEmpty(outputpath))
                    {
                        string manifestpath = Path.Combine(outputpath, "__FOA", "manifest.json");
                        FoaCoreCom.dac.model.DacManifest dacmanifest = FoaCoreCom.dac.model.DacManifest.loadFromFile(manifestpath);
                        if (dacmanifest != null)
                        {
                            FoaCoreCom.dac.model.DacManifest.ManifestFileInfo maindac = dacmanifest.GetMainDacInfo();
                            if (maindac != null)
                            {
                                string maindacpath = Path.Combine(outputpath, maindac.name);
                                Application.Current.Dispatcher.BeginInvoke(
                                   new Action(() =>
                                   {
                                       //paramを自分のローカル情報を記入する
                                       //OpenFile(outputPath);
                                       Microsoft.Office.Interop.Excel.Application oXls = null; //エクセルオブジェクト
                                       Microsoft.Office.Interop.Excel.Workbook oWBook = null;
                                       try
                                       {
                                           oXls = new Microsoft.Office.Interop.Excel.Application();
                                           oXls.Visible = true; //確認のためエクセルのウィンドウを表示する
                                           oWBook = oXls.Workbooks.Open(maindacpath);
                                           var configParams = new Dictionary<ExcelConfigParam, object>();
                                           configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(oWBook.FullName));
                                           configParams.Add(ExcelConfigParam.ENABLE_MULTI_DAC, true);
                                           InteropExcelWriterDac.InitializeConfigSheetParamTemplate(AisUtil.GetSheetFromSheetName_Interop(oWBook, DAC.Util.Keywords.PARAM_SHEET), configParams, id);

                                           ((Microsoft.Office.Interop.Excel._Workbook)oWBook).Activate();
                                           oXls.WindowState = Microsoft.Office.Interop.Excel.XlWindowState.xlNormal;
                                           ExcelUtil.OpenExcelFileForDAC(oXls);
                                       }
                                       catch (Exception err)
                                       {
                                       // ログ出力
                                       System.Windows.MessageBox.Show("ExcelOpen Exception Error : \n" + err.ToString());
                                       }
                                       finally
                                       {
                                           if (oWBook != null)
                                           {
                                               System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oWBook);
                                               oWBook = null;
                                           }
                                           if (oXls != null)
                                           {
                                               System.Runtime.InteropServices.Marshal.FinalReleaseComObject(oXls);
                                               oXls = null;
                                           }
                                       }
                                   }));
                            }
                        }
                    }
                };

                client.DownloadDataAsync(new System.Uri(url));
                client.Dispose();
                return Path.Combine(outputfolder,id);
            });
        }

        /// <summary>
        /// 
        /// ファイルを開く
        /// </summary>
        /// <param name="outputPath"></param>
        public void OpenFile(string outputPath)
        {
            // open file
            if (File.Exists(outputPath) && (new FileInfo(outputPath)).Length > 0)
            {
                // Wang Issue NO.856 20180921 Start
                //if (isExcelFile(outputPath))
                if (isExcelFile(outputPath))
                // Wang Issue NO.856 20180921 End
                {
                    ExcelUtil.StartExcelProcess(outputPath, true);
                }
                else
                {
                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.StartInfo.FileName = outputPath;
                    proc.Start();
                }
            }
            else
            {
                MessageBox.Show("ファイルがサーバーに存在しません。");

            }
        }

        private bool isExcelFile(string path)
        {
            System.IO.FileInfo targetFileInfo = new System.IO.FileInfo(path);
            string stExtension = targetFileInfo.Extension;
            if (arrayExtension.Any(stExtension.Contains))
            {
                return true;
            }
            return false;
        }
        // 2019/05/30 dongning downloadため add End

        // 2019/05/31 dn サーバから画面templeteを取得ため add  start
        private List<RegisteredTempleteItemInfo> getTempleteItems(List<RegisteredTempleteItemInfo> dacTempleteList)
        {
            getTemplateItemsFromServer(dacTempleteList);
            return dacTempleteList;
        }

         //private void getTempleteItemsFromServer(List<RegisteredTempleteItemInfo> dacTempleteList)
        // TODO server側メソッド実装しないので、一応comment Start
        private async void getTemplateItemsFromServer(List<RegisteredTempleteItemInfo> dacTempleteList)
        // TODO server側メソッド実装しないので、一応comment End
        {

            //string jsonResult = "[ {'id': '0D83160317394A85B02DDEB449A5A06A', 'filename': '階層DAC_階層DAC_20190419111005', 'online': false, 'file': null },{'id': '456789', 'filename': '階層DAC_階層DAC_20190419ccc', 'online': false, 'file': null} ]";
            
             // TODO server側メソッド実装しないので、一応comment Start
            Task<string> templateFiles = getDacTemplateFiles();
            string jsonResult = await templateFiles;
            // TODO server側メソッド実装しないので、一応comment End
            JArray jarray = JArray.Parse(jsonResult);
            if (null == jarray)
            {
                return;
            }
            foreach (JToken jto in jarray)
            {
                RegisteredTempleteItemInfo fo = new RegisteredTempleteItemInfo();
                string id = (string)jto["id"];
                string filename = (string)jto["filename"];
                bool? online = (bool?)jto["online"];

                // TODO デバッグ時はassertにする
                if (id == null || filename == null)
                {
                    continue;
                }


                if (null != fo)
                {
                    fo.DacId = id;
                    fo.FileName = filename;
                    fo.Mode = ControlMode.MultiDac;
                    fo.IsOnline = online.HasValue ? (bool)online : false;
                    dacTempleteList.Add(fo);
                }
            }

            this.DataContext = dacTempleteList;
        }

        private void ItemsControl_ContextMenuOpening(object sender, System.Windows.Controls.ContextMenuEventArgs e)
        {
            deleteTarget = null;
            var sp = sender as WrapPanel;

            if (sp != null)
            {
                var item = sp.DataContext as RegisteredTempleteItemInfo;
                if (item != null)
                {
                    deleteTarget = item;

                    if (deleteTarget.FileName.Equals(DAC.Properties.Resources.TEXT_MENU_MULTIDAC)
                                    || deleteTarget.FileName.Equals(DAC.Properties.Resources.TEXT_MENU_MULTIDAC_SAMPLE))
                    {
                        sp.ContextMenu = null;
                    }
                }
            }
        }

        private void NameMenuItem_Click(object sender, RoutedEventArgs e)
        {
            if (deleteTarget != null)
            {
                // ファイル名変更
                var reg = (RegisteredTempleteItemInfo)deleteTarget.Clone();
                string[] categoryAndDispName = parseFilename(reg.FileName);
                reg.FileName = categoryAndDispName[1];
                var d = new TemplateNameEdit(reg);
                bool? applied = d.ShowDialog();

                if (applied != true)
                {
                    return;
                }
                else
                {
                    if (parseFilename(deleteTarget.FileName)[1].Equals(d.tb_Name.Text))
                    {
                        return;
                    }
                }

                char[] invalidChars = System.IO.Path.GetInvalidFileNameChars();

                if (d.tb_Name.Text.IndexOfAny(invalidChars) >= 0)
                {
                    var msg = string.Format(DAC.Properties.Message.AIS_E_053, DAC.Properties.Message.AIS_E_070);
                    AisMessageBox.DisplayErrorMessageBox(msg);
                    return;
                }

                foreach (RegisteredTempleteItemInfo Item in this.ic_templeteListDacHorizontal.Items)
                {
                    if (Item.FileName.Equals("階層DAC_" + d.tb_Name.Text))
                    {
                        var msg = string.Format(DAC.Properties.Message.AIS_E_053, DAC.Properties.Message.AIS_E_072);
                        AisMessageBox.DisplayErrorMessageBox(msg);
                        return;
                    }
                }

                renameServerFileTask(deleteTarget.FileName, categoryAndDispName[0] + "_" + d.tb_Name.Text);
            }
            Thread.Sleep(1000);
            InitItems();
        }

        private void DeleteMenuItem_Click(object sender, RoutedEventArgs e)
        {
            deleteServerFileTask(deleteTarget.FileName);
            Thread.Sleep(1000);
            InitItems();
        }

        public static async Task deleteServerFileTask(string fileName)
        {
            string url = string.Format("http://{0}:{1}/grip/rest/dacHome/file/deleteOldFile", AisConf.Config.GripServerHost, AisConf.Config.GripServerPort);
            var content = new MultipartFormDataContent();
            content.Add(new StringContent(fileName), "name");
            var client = new FoaHttpClient();
            await client.Post(url, content);
        }

        public static async Task renameServerFileTask(string oldFileName, string newFileName)
        {
            string url = string.Format("http://{0}:{1}/grip/rest/dacHome/file/renameDacFile", AisConf.Config.GripServerHost, AisConf.Config.GripServerPort);
            var content = new MultipartFormDataContent();
            content.Add(new StringContent(oldFileName), "oldfilename");
            content.Add(new StringContent(newFileName), "newfilename");
            var client = new FoaHttpClient();
            await client.Post(url, content);
        }

        private static async Task<string> getDacTemplateFiles()
        {
            var url = string.Format("http://{0}:{1}/grip/rest/dacHome/file/getDacTemplateFiles", AisConf.Config.GripServerHost, AisConf.Config.GripServerPort);
            var client = new FoaHttpClient();
            var textData = await client.Get(url);
            return textData;
        }
        private static readonly ILog logger = LogManager.GetLogger(typeof(DacMenuGridSample));
    }
}
