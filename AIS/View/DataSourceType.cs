﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.View
{
    public enum DataSourceType
    {
        NONE,
        CTM_DIRECT,
        MISSION,
        GRIP,
        CSV_FILE
    }
}
