﻿using DAC.Model.Util;
using AisUpdater.Common;
using FoaCore.Net;
using FoaCore.Common.Util;
using log4net;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Diagnostics;
using System.Reflection;
using System.ComponentModel;
using System.Windows.Media.Imaging;
using System.ServiceModel;
using DAC;
using DAC.View;
using DAC.Util;

namespace DAC.Model
{
    // Wang Issue DAC-96 20190320 Start
    public class CreateDacParam
    {
        public string templateFile;
        public DateTime? start;
        public DateTime? end;
    }
    // Wang Issue DAC-96 20190320 End

    /// <summary>
    /// Dac_Home.xaml の相互作用ロジック
    /// </summary>
    public partial class Dac_Home : Window
    {
        private ServiceHost host = null;
        // Wang Issue DAC-96 20190320 Start
        //public static event EventHandler<string> createDacEvent;
        public static event EventHandler<CreateDacParam> createDacEvent;
        //Dac_Home sunyi 20190530 start
        public static event EventHandler<CreateDacParam> updateDacEvent;
        //Dac_Home sunyi 20190530 end
        
        // Wang Issue DAC-96 20190320 End
        public static string dacFile;

        public Dac_Home()
        {
            InitializeComponent();

            cleanEmergencyBulkyFile();
            cleanTemporaryFileAsync();

            CreateDacTut();
        }

        private void WebTutorial_Navigating(object sender, System.Windows.Navigation.NavigatingCancelEventArgs e)
        {

        }
        private void WebTutorial_Navigated(object sender, System.Windows.Navigation.NavigationEventArgs e)
        {

        }

        private void CreateDacTut()
        {
            Foa.CommonClass.Util.Initializer.InitializeTutorialFolder();

            string HOMEFOLDER = Foa.CommonClass.Util.ConfManager.HomePath;
            const string TUTORIALHOME = @"html";
            string dachomepath = Path.Combine(Path.Combine(HOMEFOLDER, TUTORIALHOME), @"dactut.html");
            WebTutorial.Navigate(new System.Uri(dachomepath));
            WebTutorial.Navigating += new System.Windows.Navigation.NavigatingCancelEventHandler(WebTutorial_Navigating);
            WebTutorial.Navigated += new System.Windows.Navigation.NavigatedEventHandler(WebTutorial_Navigated);
        }

        private void cleanEmergencyBulkyFile()
        {
            int periodDays = AisConf.Config.CleanPeriodDaysForEmergencyBulkyFile;
            string directoryPath = string.Format(@"{0}\{1}",
                System.Windows.Forms.Application.StartupPath,
                "Emergency");
            if (!Directory.Exists(directoryPath))
            {
                return;
            }

            string[] directories = Directory.GetDirectories(directoryPath);
            foreach (string directory in directories)
            {
                if (Directory.GetCreationTime(directory).AddDays(periodDays) < DateTime.Now)
                {
                    Directory.Delete(directory, true);
                }
            }
        }

        private async void cleanTemporaryFileAsync()
        {
            await Task.Delay(1500);

            // Delete mission folder
            try
            {
                string missionDir = Path.Combine(AisConf.TmpFolderPath, "mission");
                if (Directory.Exists(missionDir))
                {
                    FoaCore.Util.FileUtil.clearFolder(missionDir);
                }
                else
                {
                    Directory.CreateDirectory(missionDir);
                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("別のプロセスで使用されている"))
                {
                    FoaMessageBox.ShowWarning("AIS_E_028");
                }
            }

            // Delete ctm folder
            try
            {
                string ctmDir = Path.Combine(AisConf.TmpFolderPath, "ctm");
                if (Directory.Exists(ctmDir))
                {
                    FoaCore.Util.FileUtil.clearFolder(ctmDir);
                }
                else
                {
                    Directory.CreateDirectory(ctmDir);
                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("別のプロセスで使用されている"))
                {
                    FoaMessageBox.ShowWarning("AIS_E_028");
                }
            }

            // Delete excel folder
            try
            {
                string excelDir = Path.Combine(AisConf.TmpFolderPath, "excel");
                if (Directory.Exists(excelDir))
                {
                    FoaCore.Util.FileUtil.clearFolder(excelDir);
                }
                else
                {
                    Directory.CreateDirectory(excelDir);
                }
            }
            catch (Exception ex)
            {
                if (!ex.Message.Contains("別のプロセスで使用されている"))
                {
                    FoaMessageBox.ShowWarning("AIS_E_028");
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ManagerExecUpdater.clearRecordExecuteUpdater(System.AppDomain.CurrentDomain.BaseDirectory);

            createDacEvent += this.CreateDacFile;

            //Dac_Home sunyi 20190530 start
            updateDacEvent += this.UpdateDacFile;
            //Dac_Home sunyi 20190530 end

            OpenService();
        }

        private void OpenService()
        {
            Type serviceType = typeof(DacService);
            host = new ServiceHost(serviceType);
            host.Open();
        }

        // Wang Issue DAC-96 20190320 Start
        //public void StartUp(string templateFile)
        public void StartUp(CreateDacParam param)
        // Wang Issue DAC-96 20190320 Start
        {
            // Wang Issue DAC-96 20190320 Start
            //MainWindow mainWindow = new MainWindow(ControlMode.MultiDac, true, templateFile);
            // Wang Issue DAC-96 20190320 End
            MainWindow mainWindow = new MainWindow(ControlMode.MultiDac, param);
            mainWindow.VersionInfo.ToolTip = "AIS Version: " + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion + "\tZAR";
            mainWindow.Icon = new BitmapImage(new Uri("pack://application:,,,/DAC;component/Resources/Image/dac_icon.ico"));
            mainWindow.Show();
            Dac_Home.dacFile = string.Empty;
            this.Close();
        }

        private void control_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void control_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// ヘルプファイル呼出
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HelpViewer_MouseLeftButtonUp(object sender, RoutedEventArgs e)
        {
            AisUtil.ShowHelp(wfh.Child,ControlMode.Dac);
        }

        private void button_Reload_CTM_Click(object sender, RoutedEventArgs e)
        {
        }

        public static DacExecutionResult ValidateDac(string templateFile)
        {
            if (!templateFile.Equals(Dac_Home.dacFile))
            {
                return DacExecutionResult.InvalidateDac;
            }

            return DacExecutionResult.Success;
        }

        // Wang Issue DAC-96 20190320 Start
        //public static void CreateDac(string templateFile)
        //{
        //    createDacEvent(null, templateFile);
        //}

        //private void CreateDacFile(object sender, string templateFile)
        //{
        //    BackgroundWorker worker = new BackgroundWorker();
        //    worker.DoWork += OnDoWork;
        //    worker.RunWorkerCompleted += OnRunWorkerCompleted;
        //    worker.RunWorkerAsync(templateFile);
        //}
        
        public static void CreateDac(string templateFile, DateTime? start, DateTime? end)
        {
            createDacEvent(null, new CreateDacParam() { templateFile = templateFile, start = start, end = end });
        }

        //Dac_Home sunyi 20190530 start
        public static void UpdateDac(string templateFile, DateTime? start, DateTime? end)
        {
            updateDacEvent(null, new CreateDacParam() { templateFile = templateFile, start = start, end = end });
        }

        public void UpdateDacFile(object sender, CreateDacParam param)
        {
            var filepath = param.templateFile;
            DataSourceType isMissionTreeSelected = DataSourceType.MISSION;

            try
            {
                isMissionTreeSelected = getDataSourceType(filepath);
            }
            catch (Exception ex)
            {
                var msg = string.Format(Properties.Message.AIS_E_054, ex.Message);
                msg += string.Format("\n" + ex.StackTrace);
                logger.Error(msg, ex);
                FoaMessageBox.ShowError(msg);
            }

            MainWindow mainWindow = new MainWindow(ControlMode.MultiDac, false, filepath, isMissionTreeSelected, param);
            mainWindow.VersionInfo.ToolTip = "AIS Version: " + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion + "\tZAR";
            mainWindow.Show();

            this.Close();
        }

        /// <summary>
        /// データの取得元(ミッション or CTM or Grip)を取得
        /// </summary>
        /// <param name="filePath">ファイルパス</param>
        /// <returns>データの取得元</returns>
        private DataSourceType getDataSourceType(string filePath)
        {
            SpreadsheetGear.IWorkbook workbook = null;

            try
            {
                workbook = SpreadsheetGear.Factory.GetWorkbook(filePath);
                string sheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet sheet = AisUtil.GetSheetFromSheetName_SSG(workbook, sheetName);
                SpreadsheetGear.IRange cells = sheet.Cells;

                DataSourceType dataSource = DataSourceType.MISSION;
                for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
                {
                    // null check
                    if (cells[i, 0].Value == null)
                    {
                        continue;
                    }

                    // 収集タイプ
                    if (cells[i, 0].Value.ToString() == "収集タイプ")
                    {
                        if (cells[i, 1].Value == null)
                        {
                            return dataSource;
                        }
                        if (cells[i, 1].Value.ToString() == "CTM")
                        {
                            dataSource = DataSourceType.CTM_DIRECT;
                        }
                        else if (cells[i, 1].Value.ToString() == "ミッション")
                        {
                            dataSource = DataSourceType.MISSION;
                        }
                        else // if (cells[i, 1].Value.ToString() == "GRIP")
                        {
                            dataSource = DataSourceType.GRIP;
                        }

                        break;
                    }
                }

                return dataSource;

            }
            finally
            {
                if (workbook != null)
                {
                    workbook.Close();
                }

            }
        }
        //Dac_Home sunyi 20190530 end

        private void CreateDacFile(object sender, CreateDacParam param)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.DoWork += OnDoWork;
            worker.RunWorkerCompleted += OnRunWorkerCompleted;
            worker.RunWorkerAsync(param);
        }
        // Wang Issue DAC-96 20190320 End

        private void OnDoWork(object sender, DoWorkEventArgs e)
        {
            this.Dispatcher.Invoke(() =>
            {
                // Wang Issue DAC-96 20190320 End
                //this.StartUp(e.Argument as string);
                // Wang Issue DAC-96 20190320 End
                this.StartUp(e.Argument as CreateDacParam);
            });
        }

        private void OnRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(Dac_Home));

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            createDacEvent -= this.CreateDacFile;

            //Dac_Home sunyi 20190530 start
            updateDacEvent -= this.UpdateDacFile;
            //Dac_Home sunyi 20190530 end

            if (host != null)
            {
                host.Close();
            }
        }

        private void button_Reload_Click(object sender, RoutedEventArgs e)
        {
            this.dacMenuGrid.InitItems();

            this.ScrollViewer2.InitItems();
        }
    }
}
