﻿using FoaCore.Net;
using FoaCore.Common;
using FoaCore.Common.Entity;
using FoaCore.Common.Model;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using Foa.CommonClass.Util;
using AisUpdater.Common;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Net.Sockets;
using System.Windows;
using DAC.Properties;
using System.ComponentModel;
using System.Threading;
using Newtonsoft.Json.Linq;
using DAC.Util;

namespace DAC.Model
{
    public class ClsLoginSend
    {
        public INFO User { get; set; }
        public class INFO
        {
            public string UserName { get; set; }
            public string Password { get; set; }
        }
    }

    /// <summary>
    /// LoginControl.xaml の相互作用ロジック
    /// </summary>
    public partial class Login : Window
    {
        private Window_Home home;
        private readonly BackgroundWorker isAliveWorker = new BackgroundWorker();
        private bool isExternal;

        public string startUpTemplate = null;
        /// <summary>
        /// コンストラクタ
        /// </summary>
        public Login()
        {
            InitializeComponent();
        }

        public Action Callback = null;
        public void StartInitialization()
        {
            GetCmsLangInfo();
        }
        public void Initialize()
        {
            if (@Settings.Default.LoginModeJapaneseOnly)
            {
                int catalogLangListCount = AisConf.CatalogLangList.Count;
                AisConf.CatalogLangList.RemoveRange(1, catalogLangListCount - 1);
                AisConf.CatalogLangListForDisplay.RemoveRange(1, catalogLangListCount - 1);
                //this.CmbCatalogLang.SelectedValue = GripConf.CatalogLangList[GripConf.CatalogLangList.IndexOf("ja")]; // Japanese
            }

            this.CmbUiLang.ItemsSource = AisConf.UiLangListForDisplay;
            this.CmbCatalogLang.ItemsSource = AisConf.CatalogLangListForDisplay;

            pressedLoginButton = false;

            setDefaultCatalogLang2(@Settings.Default.LoginModeJapaneseOnly ? "ja" : AisConf.Config.DefaultCatalogLang);

            bool setUiLang = false;
            this.CmbUiLang.SelectedIndex = 0;
            string tmpFolderPath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "tmp";
            if (Directory.Exists(tmpFolderPath) == false)
            {
                Directory.CreateDirectory(tmpFolderPath);
            }
            string tmpLoginFolderPath = tmpFolderPath + "\\" + "login";
            if (Directory.Exists(tmpLoginFolderPath) == false)
            {
                Directory.CreateDirectory(tmpLoginFolderPath);
            }
            string lastLoginLangInfoJsonFile = Path.Combine(tmpLoginFolderPath, "lastLoginLangInfo.json");
            if (File.Exists(lastLoginLangInfoJsonFile) && !@Settings.Default.LoginModeJapaneseOnly)
            {
                try
                {
                    JToken token = JToken.Parse(File.ReadAllText(lastLoginLangInfoJsonFile));
                    if (token["uiLang"] != null)
                    {
                        string uiLang = (string)token["uiLang"];
                        foreach (string lang in AisConf.UiLangListForDisplay)
                        {
                            if (lang == uiLang)
                            {
                                this.CmbUiLang.SelectedValue = uiLang;
                                setUiLang = true;
                                break;
                            }
                        }
                    }
                    if (token["catalogLang"] != null)
                    {
                        string catalogLang = (string)token["catalogLang"];
                        foreach (string lang in AisConf.CatalogLangListForDisplay)
                        {
                            if (lang == catalogLang)
                            {
                                this.CmbCatalogLang.SelectedValue = catalogLang;
                                break;
                            }
                        }
                    }
                }
                catch
                {

                }
            }

            if (!setUiLang)
            {
                if (!string.IsNullOrEmpty(AisConf.Config.DefaultUiLang))
                {
                    foreach (string lang in AisConf.UiLangList)
                    {
                        if (lang == AisConf.Config.DefaultUiLang)
                        {
                            this.CmbUiLang.SelectedValue = AisConf.UiLangListForDisplay[AisConf.UiLangList.IndexOf(lang)];
                            setUiLang = true;
                            break;
                        }
                    }
                }
            }

            if (Callback != null)
            {
                Callback();
            }

            // 自動ログイン(デバッグ用)
#if DEBUG
            //Authenticate("test_admin", "foafoa", "ja");
#endif
        }

        /// <summary>
        /// ログインボタン押下フラグ (ログインボタン連打対策)
        /// </summary>
        private bool pressedLoginButton;

        /// <summary>
        /// ログインボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            // ログイン処理中時は、続けて処理せずに終了する。(ログインボタン連打対策)
            if (!pressedLoginButton)
            {
                // ログインボタン押下フラグをONに設定する。 (ログインボタン連打対策)
                pressedLoginButton = true;

                this.LblAuthErrorMessage.Text = string.Empty;

                // ログインは以下の順番で行う
                // 1. ログイン画面でカタログ言語を設定
                // 2. カタログ言語を渡し、SYBで認証
                string uiLang = AisConf.UiLangList[CmbUiLang.SelectedIndex];
                string catalogLang = AisConf.CatalogLangList[CmbCatalogLang.SelectedIndex];
                string userId = this.TxtLoginUserID.Text;
                string passwd = this.PassLoginPassword.Password;

                string tmpFolderPath = System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "tmp";
                if (Directory.Exists(tmpFolderPath) == false)
                {
                    Directory.CreateDirectory(tmpFolderPath);
                }
                string tmpLoginFolderPath = tmpFolderPath + "\\" + "login";
                if (Directory.Exists(tmpLoginFolderPath) == false)
                {
                    Directory.CreateDirectory(tmpLoginFolderPath);
                }
                string lastLoginLangInfoJsonFile = Path.Combine(tmpLoginFolderPath, "lastLoginLangInfo.json");
                JToken token = JToken.Parse("{}");
                token["uiLang"] = (string)CmbUiLang.SelectedValue;
                token["catalogLang"] = (string)CmbCatalogLang.SelectedValue;
                System.IO.File.WriteAllText(lastLoginLangInfoJsonFile, token.ToString());

                LoginInfo.GetInstance().CreateUiLang(uiLang);

                Authenticate(userId, passwd, catalogLang,false);

            }
        }

        /// <summary>
        /// 閉じるボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LoginCloseButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            Window.GetWindow(this).Close();
        }

        public void Authenticate(string userId, string passwd, string lang, bool isExternal)
        {
            LoginInfo.GetInstance().CreateCatalogLang(lang);
            this.isExternal = isExternal;
            AisConf.UserId = userId;
            AisConf.Password = passwd;
            AisConf.IsErrorLogin = false;
            AisConf.CatalogLang = lang;
            if (AisConf.Config.LoginAuthentication == "ForcedRelease")
            {
                /*
                Dictionary<string, object> user = new Dictionary<string, object>();
                user.Add("UserName", userId);
                user.Add("Password", passwd);
                Dictionary<string, object> loginInfo = new Dictionary<string, object>();
                loginInfo.Add("Lang", lang);
                loginInfo.Add("User", user);
                string reqJson = JsonConvert.SerializeObject(loginInfo);

                CmsHttpClient client = new CmsHttpClient(this);
                client.CompleteHandler += loginComplete;
                client.ExceptionHandler = (Exception e, object[] ps) =>
                {
                    AisConf.IsErrorLogin = true;
                    if (this.isExternal)
                    {
                        Application.Current.Shutdown();
                    }
                    logger.Error("リモート サーバーに接続できません。" + AisConf.Config.CmsHost + ":" + AisConf.Config.CmsPort);
                    FoaMessageBox.ShowError(new System.Net.WebException("リモート サーバーに接続できません。"),
                        new object[] { AisConf.Config.CmsHost + ":" + AisConf.Config.CmsPort });
                    pressedLoginButton = false;
                };
                client.ShowErrorMessage = false;
                client.Post(CmsUrl.GetLoginUrl(), reqJson);
                 */
                LoginUtil util = LoginUtil.GetInstance();
                util.LoginCompleteHandler += loginComplete;
                util.OwnerWindow = this;
                util.ExceptionHandler += (Exception e, object[] ps) =>
                {
                    AisConf.IsErrorLogin = true;
                    if (this.isExternal)
                    {
                        Application.Current.Shutdown();
                    }
                    logger.Error("リモート サーバーに接続できません。" + AisConf.Config.CmsHost + ":" + AisConf.Config.CmsPort);
                    FoaMessageBox.ShowError(new System.Net.WebException("リモート サーバーに接続できません。"),
                        new object[] { AisConf.Config.CmsHost + ":" + AisConf.Config.CmsPort });
                    pressedLoginButton = false;
                };
                util.Login(startUpTemplate,userId,passwd);
            }
            else
            {

                LoginUtil util = LoginUtil.GetInstance();
                util.OwnerWindow = this;
                util.LoginCompleteHandler += (loginRst) =>
                {
                    var rst = JsonConvert.DeserializeObject<AuthResult>(loginRst);

                    // カルチャーの変更 言語切り替え
                    string cultureName = string.Empty;
                    if (lang == "ja") cultureName = "ja-JP";
                    else if (lang == "en") cultureName = "en-US";
                    DAC.Common.CultureResources.ChangeCulture(cultureName);

                    Console.WriteLine("### Login.");
                    Console.WriteLine("### url: {0}", CmsUrl.GetLoginUrl());
                    loginComplete(loginRst);

                };
                util.ExceptionHandler += (Exception e, object[] ps) =>
                {
                    AisConf.IsErrorLogin = true;
                    if (this.isExternal)
                    {
                        Application.Current.Shutdown();
                    }
                    logger.Error("リモート サーバーに接続できません。" + AisConf.Config.CmsHost + ":" + AisConf.Config.CmsPort);
                    FoaMessageBox.ShowError(new System.Net.WebException("リモート サーバーに接続できません。"),
                        new object[] { AisConf.Config.CmsHost + ":" + AisConf.Config.CmsPort });
                    pressedLoginButton = false;
                };
                util.Login(startUpTemplate,userId,passwd);
                /*ClsLoginSend loginSend = new ClsLoginSend();
                ClsLoginSend.INFO info = new ClsLoginSend.INFO();
                info.UserName = userId;
                info.Password = passwd;
                loginSend.User = info;
                string sendJson = JsonConvert.SerializeObject(loginSend);

                var client = new CmsHttpClient(this);
                //StringContent theContent = new StringContent(sendJson, new UTF8Encoding(false), "application/json");
                client.CompleteHandler += (loginRst) =>
                {
                    var rst = JsonConvert.DeserializeObject<AuthResult>(loginRst);

                    // カルチャーの変更 言語切り替え
                    string cultureName = string.Empty;
                    if (lang == "ja") cultureName = "ja-JP";
                    else if (lang == "en") cultureName = "en-US";
                    AIS.Common.CultureResources.ChangeCulture(cultureName);

                    Console.WriteLine("### Login.");
                    Console.WriteLine("### url: {0}", CmsUrl.GetAisLoginUrl());
                    loginComplete(loginRst);

                };
                client.ExceptionHandler = (Exception e, object[] ps) =>
                {
                    AisConf.IsErrorLogin = true;
                    if (this.isExternal)
                    {
                        Application.Current.Shutdown();
                    }
                    logger.Error("リモート サーバーに接続できません。" + AisConf.Config.CmsHost + ":" + AisConf.Config.CmsPort);
                    FoaMessageBox.ShowError(new System.Net.WebException("リモート サーバーに接続できません。"),
                        new object[] { AisConf.Config.CmsHost + ":" + AisConf.Config.CmsPort });
                    pressedLoginButton = false;
                };
                client.ShowErrorMessage = false;
                client.Post(CmsUrl.GetAisLoginUrl(), sendJson);
                */
            }
           

            /*

            if (isConnectedToCms() == true)
            {
                if (AisConf.Config.LoginAuthentication == "ForcedRelease")
                {
                    Dictionary<string, object> user = new Dictionary<string, object>();
                    user.Add("UserName", userId);
                    user.Add("Password", passwd);

                    Dictionary<string, object> loginInfo = new Dictionary<string, object>();
                    loginInfo.Add("Lang", lang);
                    loginInfo.Add("User", user);
                    string reqJson = JsonConvert.SerializeObject(loginInfo);

                    CmsHttpClient client = new CmsHttpClient(this);
                    client.CompleteHandler += loginComplete;
                    client.ExceptionHandler = (Exception e, object[] ps) =>
                    {
                        AisConf.IsErrorLogin = true;
                        if (AisConf.IsExternal)
                        {
                            Application.Current.Shutdown();
                        }
                        pressedLoginButton = false;
                    };
                    client.Post(CmsUrl.GetLoginUrl(), reqJson);
                    Console.WriteLine("### Login.");
                    Console.WriteLine("### url: {0}", CmsUrl.GetLoginUrl());
                }
                else
                {
                    ClsLoginSend loginSend = new ClsLoginSend();
                    ClsLoginSend.INFO info = new ClsLoginSend.INFO();
                    info.UserName = userId;
                    info.Password = passwd;
                    loginSend.User = info;
                    string sendJson = JsonConvert.SerializeObject(loginSend);

                    var client = new FoaHttpClient();
                    StringContent theContent = new StringContent(sendJson, new UTF8Encoding(false), "application/json");

                    var loginRst = await client.Post(CmsUrl.GetAisLoginUrl(), theContent);
                    var rst = JsonConvert.DeserializeObject<AuthResult>(loginRst);

                    // カルチャーの変更 言語切り替え
                    string cultureName = string.Empty;
                    if (lang == "ja") cultureName = "ja-JP";
                    else if (lang == "en") cultureName = "en-US";
                    AIS.Common.CultureResources.ChangeCulture(cultureName);

                    Console.WriteLine("### Login.");
                    Console.WriteLine("### url: {0}", CmsUrl.GetAisLoginUrl());
                    loginComplete(loginRst);
                }
            }
            else
            {
                AisConf.IsErrorLogin = true;
                if (AisConf.IsExternal)
                {
                    Application.Current.Shutdown();
                }
                // Could not connect to CMS Server
                logger.Error("リモート サーバーに接続できません。" + AisConf.Config.CmsHost + ":" + AisConf.Config.CmsPort);
                FoaMessageBox.ShowError(new System.Net.WebException(Properties.Message.AIS_E_034),
                    new object[] { AisConf.Config.CmsHost + ":" + AisConf.Config.CmsPort });

                // ボタン押下状態解除 (ログインボタン連打対策)
                pressedLoginButton = false;
            }*/
        }
        /*
        private bool isConnectedToCms()
        {
            string host = AisConf.Config.CmsHost;
            int port = int.TryParse(AisConf.Config.CmsPort, out port) ? port : 60000;
            Socket s = null; ;
            try
            {
                s = new Socket(AddressFamily.InterNetwork,
                SocketType.Stream,
                ProtocolType.Tcp);
                if (CmsHttpClient.proxy != null)
                {
                    string proxystring = CmsHttpClient.proxy.Address.ToString();
                    proxystring = proxystring.Replace("http://","");
                    int proxyport;
                    int.TryParse(proxystring.Substring(proxystring.LastIndexOf(':') + 1),out proxyport);
                    string proxyhost = proxystring.Substring(0, proxystring.LastIndexOf(":"));
                    s.Connect(proxyhost, proxyport);
                    
                }

                s.Connect(host, port); // test connection..
                return true;

            }
            catch (Exception)
            {
                // something went wrong
                return false;
            }
            finally
            {
                if (s != null)
                {
                    s.Close();
                }
            }
        }
        */
        public string ChainLoginUiLang = null;
        /// <summary>
        /// ログイン結果取得
        /// </summary>
        /// <param name="json"></param>
        /// 
       
        private void loginComplete(string json)
        {
            // ボタン押下状態解除 (ログインボタン連打対策)
            pressedLoginButton = false;

            // 認証結果確認
            AuthResult authResult = JsonConvert.DeserializeObject<AuthResult>(json);
            if (authResult == null)
            {
                AisConf.IsErrorLogin = true;
                if (AisConf.IsExternal)
                {
                    Application.Current.Shutdown();
                }
                this.LblAuthErrorMessage.Text = DAC.Properties.Resources.MSG_AUTH_ERROR;
                return;
            }
            if (!authResult.Auth)
            {
                AisConf.IsErrorLogin = true;
                if (AisConf.IsExternal)
                {
                    if (authResult.Reason.Contains("AUTH.DUP"))
                    {
                        MessageBox.Show(DAC.Properties.Resources.MSG_DUP_ERROR, "AIS", 
                            MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                    Application.Current.Shutdown();
                }
                if (authResult.Reason.Contains("LICENSE."))
                {
                    // ライセンスエラー
                    this.LblAuthErrorMessage.Text = DAC.Properties.Resources.MSG_LICENSE_ERROR;
                    this.Show();
                    return;
                }
                else if (authResult.Reason.Contains("AUTH.NOTPERMITTED"))
                {
                    // 認証失敗の場合はエラーメッセージ設定
                    this.LblAuthErrorMessage.Text = DAC.Properties.Resources.MSG_NOT_PERMITTED_ERROR;
                    this.Show();
                    return;
                }
                else if (authResult.Reason.Contains("AUTH.DUP"))
                {
                    // 認証失敗の場合はエラーメッセージ設定
                    this.LblAuthErrorMessage.Text = DAC.Properties.Resources.MSG_DUP_ERROR;
                    this.Show();
                    return;
                }
                else
                {
                    // 認証失敗の場合はエラーメッセージ設定
                    this.LblAuthErrorMessage.Text = DAC.Properties.Resources.MSG_AUTH_ERROR;
                    this.Show();
                    return;
                }
            }

            AisConf.IsErrorLogin = false;

            // 認証成功の場合はエラーメッセージクリア
            this.LblAuthErrorMessage.Text = string.Empty;

            LoginInfo loginInfo = LoginInfo.GetInstance();

            // LoginInfoにユーザ情報を設定
            loginInfo.CreateLoginUserInfo(
                new UserInfo()
                {
                    UserId = authResult.User.UserId,
                    UserName = authResult.User.UserName,
                    DisplayName = authResult.User.DisplayName,
                    DisplayNameObj = JsonConvert.DeserializeObject<List<LangObject>>(authResult.User.DisplayName),
                    Role = authResult.User.Role,
                    Affiliation = authResult.User.Affiliation,
                    MailAddress = authResult.User.MailAddress,
                    uiLang = authResult.User.uiLang
                });

            AisConf.UserGuID = authResult.User.UserId;

            // ドメインIDの設定
            loginInfo.SetDomainId(authResult.DomainId);

            // TimezoneIdの設定
            loginInfo.SetTimezoneId(authResult.TimezoneId);

            // MultiDac使用できるか
            loginInfo.SetEnableMultiDac(authResult.Enable_Multi_Dac);

            // MultiStatusMonitor使用できるか
            loginInfo.SetEnableMultiStatusMonitor(authResult.Enable_Multi_Status_Monitor);

            // バージョンチェック
            if (!AisConf.IsExternal)
            {
                AppUpdaterInfo appUpdaterInfo = AppUpdaterInfo.versionCheck();
                bool isExecuted = ManagerExecUpdater.isExecutedUpdater(System.AppDomain.CurrentDomain.BaseDirectory);

                if (appUpdaterInfo.isUpdateChecked && !appUpdaterInfo.isLatest() && !isExecuted)
                {
                    ManagerExecUpdater.recordExecuteUpdater(System.AppDomain.CurrentDomain.BaseDirectory);
                    AppUpdaterInfo.activateProgram(appUpdaterInfo.currentDirPath, "AisUpdater.exe");
                    Application.Current.Shutdown();
                }
            }

            // カルチャーの名前を取得
            //-----------------------------------------------------
            string cultureName = (string)CmbUiLang.SelectedValue;
            if (!string.IsNullOrEmpty(this.ChainLoginUiLang))
            {
                cultureName = AisConf.UiLangListForDisplay[AisConf.UiLangList.IndexOf(this.ChainLoginUiLang)];
                AisConf.UiLang = AisConf.UiLangList[AisConf.UiLangList.IndexOf(this.ChainLoginUiLang)];
                LoginInfo.GetInstance().CreateUiLang(AisConf.UiLangList[AisConf.UiLangList.IndexOf(this.ChainLoginUiLang)]);
            }
            else
            {
                cultureName = (string)CmbUiLang.SelectedValue;
                AisConf.UiLang = AisConf.UiLangList[CmbUiLang.SelectedIndex];
            }

            if (cultureName == "Japanese") cultureName = "ja-JP";
            else if (cultureName == "English") cultureName = "en-US";
            //-----------------------------------------------------
            // カルチャーの変更 言語切り替え
            DAC.Common.CultureResources.ChangeCulture(cultureName);

            //string uilang = authResult.User.uiLang.Contains("en") ? "en-US" : "ja-JP"; 
            //string uilang = strlang.Contains("en") ? "en-US" : "ja-JP";
            string uilang = cultureName.Contains("en") ? "en-US" : "ja-JP";
            authResult.User.uiLang = uilang;

            //// カルチャーを変更
            //string userUiLang = AisConf.CatalogLang;  //loginInfo.GetLoginUserInfo().uiLang;
            //if (!userUiLang.Equals(CultureInfo.CurrentUICulture.TextInfo.CultureName))
            //{
            //    // CultureResources.ChangeCulture(loginInfo.GetLoginUserInfo().uiLang);
            //}

            //// カルチャーの名前を取得
            ////string cultureName = (string)CmbCatalogLang.SelectedValue;
            ////if (cultureName == "Japanese") cultureName = "ja-JP";
            ////else if (cultureName == "English") cultureName = "en-US";

            ////-----------------------------------------------------
            //string cultureName = string.Empty;
            //if (userUiLang == "ja") cultureName = "ja-JP";
            //else if (userUiLang == "en") cultureName = "en-US";
            ////-----------------------------------------------------
            //// カルチャーの変更 言語切り替え
            //AIS.Common.CultureResources.ChangeCulture(cultureName);


            // クッキーの設定
            CmsHttpClient.SetCookie(CmsUrl.GetMmsBaseUrl(), loginInfo.GetLoginUserInfo());
            CmsHttpClient.SetCookie(CmsUrl.GetSybBaseUrl(), loginInfo.GetLoginUserInfo());
            CmsHttpClient.SetCookie(CmsUrl.GetMibBaseUrl(), loginInfo.GetLoginUserInfo());

            // Wang Issue DAC-72 20181225 Start
            AisHttpClient.SetCookie(CmsUrl.GetMmsBaseUrl(), loginInfo.GetLoginUserInfo());
            AisHttpClient.SetCookie(CmsUrl.GetSybBaseUrl(), loginInfo.GetLoginUserInfo());
            AisHttpClient.SetCookie(CmsUrl.GetMibBaseUrl(), loginInfo.GetLoginUserInfo());
            // Wang Issue DAC-72 20181225 End

            updateCmbCatalogLang(authResult.CatalogLang.defaultLang, authResult.CatalogLang.maxLangs);

            loginInfo.EnableMask = authResult.EnableMask;
            // loginInfo.MaxDomains = authResult.MaxDomains;

            // Get license info
            CmsLicense.Init(this);

            AisConf.RegistryFolderPath = Path.Combine(AisConf.BaseDir, "registry", authResult.User.UserName);            
            if (!Directory.Exists(AisConf.RegistryFolderPath))
            {
                Directory.CreateDirectory(AisConf.RegistryFolderPath);
            }
            AisConf.DacRegistryFolderPath = Path.Combine(AisConf.DacBaseDir, "MyDacs", authResult.User.UserId);
            if (!Directory.Exists(AisConf.DacRegistryFolderPath))
            {
                Directory.CreateDirectory(AisConf.DacRegistryFolderPath);
            }
            // 生死確認開始
#if !DEBUG
            isAliveWorker.DoWork += worker_DoWork;
            isAliveWorker.RunWorkerAsync(authResult.LoginKey);
#endif
            home = new Window_Home();
            if (startUpTemplate != null)
            {
                // todo : homeから自動テンプレートを開く
                home.StartUp(startUpTemplate);
            }
            else
            {
                home.Show();
            }
            noShutdown = true;
            this.Close();
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            string loginKey = (string)e.Argument;

            CmsHttpClient client = new CmsHttpClient(null);
            string res = client.SyncGet(CmsUrl.GetIntervalAlive());
            int interval = 0;
            if (!int.TryParse(res, out interval))
            {
                return;
            }
            if (interval <= 0)
            {
                return;
            }

            string reqJson = JsonConvert.SerializeObject(new Dictionary<string, string>(){
                {"LoginKey", loginKey}
            });

            while (true)
            {
                client.Post(CmsUrl.GetAlive(), reqJson);
                Thread.Sleep(interval * 60 * 1000);
            }

        }

        private bool noShutdown = false;

        /// <summary>
        /// 認証結果
        /// </summary>
        public class AuthResult
        {
            /// <summary>
            /// 認証結果
            /// </summary>
            public bool Auth { get; set; }

            /// <summary>
            /// 理由
            /// </summary>
            public String Reason { get; set; }

            /// <summary>
            /// カタログ言語
            /// </summary>
            public CatalogLangInfo CatalogLang { get; set; }

            /// <summary>
            /// ユーザ情報
            /// </summary>
            public UserInfo User { get; set; }

            /// <summary>
            /// ドメインID
            /// </summary>
            public string DomainId;

            public bool EnableMask;

            public string TimezoneId;

            public List<Permission> Permissions;

            public string LoginKey;

            public bool Enable_Multi_Dac;

            // Wang Issue NO.655 2018/04/20 Start
            // ライセンスのフラグにてオプションテンプレートを表示・非表示
            //public bool Enable_Multi_Status_Monitor;
            public string Enable_Multi_Status_Monitor;
            // Wang Issue NO.655 2018/04/20 End
        }

        private void checkInputValue()
        {
            string id = this.TxtLoginUserID.Text;
            string pw = this.PassLoginPassword.Password;

            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(pw))
            {
                this.BtnLogin.IsEnabled = true;
            }
            else
            {
                this.BtnLogin.IsEnabled = false;
            }
        }

        private void TxtLoginUserID_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            checkInputValue();
        }

        private void PassLoginPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            checkInputValue();
        }

        private void GetCmsLangInfo()
        {
            CmsHttpClient client = new CmsHttpClient(this);
            client.CompleteHandler += GetCmsLangInfoComplete;
            client.Get(CmsUrl.GetCmsLangInfoUrl());
        }

        private void GetCmsLangInfoComplete(string json)
        {
            JToken token = JToken.Parse(json);
            string defaultCatalogLang = (string)token["defaultLang"];
            int maxLangs = (int)token["maxLangs"];
            JArray catalogLangList = (JArray)token["catalogLangList"];
            JArray uiLangList = (JArray)token["uiLangList"];

            List<string> CatalogLangList = new List<string>();
            List<string> CatalogLangListForDisplay = new List<string>();
            List<string> CatalogLangListForData = new List<string>();
            foreach (JToken cToken in catalogLangList)
            {
                string code = (string)cToken["code"];
                string name = (string)cToken["name"];
                string header = (string)cToken["header"];
                CatalogLangList.Add(code);
                CatalogLangListForDisplay.Add(name);
                CatalogLangListForData.Add(header);
            }
            AisConf.CatalogLangList.Clear();
            AisConf.CatalogLangList.AddRange(CatalogLangList);
            AisConf.CatalogLangListForDisplay.Clear();
            AisConf.CatalogLangListForDisplay.AddRange(CatalogLangListForDisplay);
            AisConf.CatalogLangListForData.Clear();
            AisConf.CatalogLangListForData.AddRange(CatalogLangListForData);

            List<string> UiLangList = new List<string>();
            List<string> UiLangListForDisplay = new List<string>();
            foreach (JToken uToken in uiLangList)
            {
                string code = (string)uToken["code"];
                string name = (string)uToken["name"];
                UiLangList.Add(code);
                UiLangListForDisplay.Add(name);
            }
            AisConf.UiLangList.Clear();
            AisConf.UiLangList.AddRange(UiLangList);
            AisConf.UiLangListForDisplay.Clear();
            AisConf.UiLangListForDisplay.AddRange(UiLangListForDisplay);

            Initialize();
        }



        private void setDefaultCatalogLang2(string lang)
        {
            if (lang == "en")
            {
                CmbCatalogLang.SelectedIndex = 1;
            }
        }

        private void getDefaultCatalogLang()
        {
            CmsHttpClient client = new CmsHttpClient(this);
            client.CompleteHandler += getDefaultCLComplete;
            client.Get(CmsUrl.GetLangUrl());
        }


        private void getDefaultCLComplete(string json)
        {
            switch (json)
            {
                case "ja":
                    return; // 何もしない
                case "en":
                    CmbCatalogLang.SelectedIndex = 1;
                    break;
            }
        }

        private void updateCmbCatalogLang(string defaultLang, int maxLangs)
        {
            if (maxLangs == 1)
            {
                if (defaultLang == "ja")
                {
                    AisConf.CatalogLangList.Clear();
                    AisConf.CatalogLangList.Add(defaultLang);

                    AisConf.CatalogLangListForDisplay.Clear();
                    AisConf.CatalogLangListForDisplay.Add("Japanese");

                    LoginInfo.GetInstance().CreateCatalogLang(defaultLang);
                }
                else if (defaultLang == "en")
                {
                    AisConf.CatalogLangList.Clear();
                    AisConf.CatalogLangList.Add(defaultLang);

                    AisConf.CatalogLangListForDisplay.Clear();
                    AisConf.CatalogLangListForDisplay.Add("English");

                    LoginInfo.GetInstance().CreateCatalogLang(defaultLang);
                }
                else
                {
                    AisConf.CatalogLangList.Clear();
                    AisConf.CatalogLangList.Add(defaultLang);

                    AisConf.CatalogLangListForDisplay.Clear();
                    AisConf.CatalogLangListForDisplay.Add(defaultLang);

                    LoginInfo.GetInstance().CreateCatalogLang(defaultLang);
                }
            }
        }

        public class CatalogLangInfo
        {
            public string defaultLang;
            public int maxLangs;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!noShutdown)
            {
                Application.Current.Shutdown();
            }
        }

        public class Permission
        {
            public string PermissionId;
            public string Privilege;
            public string UserName;
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(Login));
    }
}
