﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Reflection;
using FoaCore.Common.Util.Json;
using System.IO;
using System.Diagnostics;
using FoaCore.Common.Util;
using DAC.Model;
using DAC.View.Helpers;

namespace DAC.View
{
    [System.AttributeUsage(AttributeTargets.Field)]
    public class IconInfo : System.Attribute
    {
        public string Name { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }

        public IconInfo(string name, int width, int height)
        {
            this.Name = name;
            this.Width = width;
            this.Height = height;
        }
    }

    public enum OutputType
    {
        // Wang Change Icon 20180905 Start
        //[IconInfo("rf-orange.png", 51, 28)]
        [IconInfo("xlsx.png", 51, 28)]
        // Wang Change Icon 20180905 End
        Common,
        // Wang Change Icon 20180905 Start
        //[IconInfo("rf-gray.png", 51, 28)]
        [IconInfo("xlsx.png", 51, 28)]
        // Wang Change Icon 20180905 End
        Mission,
        [IconInfo("rf-csv.png", 51, 28)]
        CSV
    }

    public static class EnumExtensions
    {
        public static IconInfo GetIconInfo(this Enum enumValue, Type enumType)
        {
            MemberInfo info = enumType.GetMember(enumValue.ToString()).First();

            if (info != null && info.CustomAttributes.Any())
            {
                IconInfo iconAttr = info.GetCustomAttribute<IconInfo>();
                return iconAttr;
            }

            return null;
        }
    }

    public class OutputItem
    {
        public OutputType type;
        public string path;
        public DateTime creationTime;
        public List<string> sheetNames;

        [JsonIgnore]
        public StackPanel panel;
    }

    /// <summary>
    /// ResultFolderGrip.xaml の相互作用ロジック
    /// </summary>
    public partial class ResultFolderGrip : UserControl
    {
        private List<OutputItem> outputFiles = new List<OutputItem>();
        // Wang Issue of quick graph 20180907 Start
        private static event EventHandler updateEvent;
        // Wang Issue of quick graph 20180907 End

        public ResultFolderGrip()
        {
            InitializeComponent();

            InitializeOutputItems();

            this.DataContext = this;
        }

        public void InitializeOutputItems()
        {
            ClearOutputGrid();

            string jsonFile = System.IO.Path.Combine(AisConf.OutputDir, Properties.Resources.TEXT_OUTPUT_JSON);
            if (File.Exists(jsonFile))
            {
                using (StreamReader sr = new StreamReader(jsonFile))
                {
                    outputFiles = JsonConvert.DeserializeObject<List<OutputItem>>(sr.ReadToEnd());
                }
            }

            List<OutputItem> invalidItems = new List<OutputItem>();
            foreach (OutputItem item in outputFiles)
            {
                if (IsValidOutputItem(item))
                {
                    item.panel = AddOutputItemToGrid(item);
                }
                else
                {
                    invalidItems.Add(item);
                    try
                    {
                        File.Delete(item.path);
                    }
#pragma warning disable
                    catch (Exception e)
#pragma warning restore
                    {

                    }
                }
            }

            foreach (OutputItem item in invalidItems) {
                outputFiles.Remove(item);
            }

            this.SaveJsonFile();
        }

        // Wang Issue of quick graph 20180907 Start
        public void UpdateOutputItems()
        {
            ClearOutputGrid();

            string jsonFile = System.IO.Path.Combine(AisConf.OutputDir, Properties.Resources.TEXT_OUTPUT_JSON);
            if (File.Exists(jsonFile))
            {
                using (StreamReader sr = new StreamReader(jsonFile))
                {
                    outputFiles = JsonConvert.DeserializeObject<List<OutputItem>>(sr.ReadToEnd());
                }
            }

            List<OutputItem> invalidItems = new List<OutputItem>();
            foreach (OutputItem item in outputFiles)
            {
                if (IsValidOutputItem(item))
                {
                    item.panel = AddOutputItemToGrid(item);
                }
                else
                {
                    invalidItems.Add(item);
                }
            }

            foreach (OutputItem item in invalidItems)
            {
                outputFiles.Remove(item);
            }
        }
        // Wang Issue of quick graph 20180907 End

        private bool IsValidOutputItem(OutputItem item)
        {
            if (File.Exists(item.path))
            {
                DateTime creationTime = item.creationTime;
                DateTime now = DateTime.Now;
                DateTime today = new DateTime(now.Year, now.Month, now.Day);
                if (creationTime >= today)
                {
                    return true;
                }
            }
            return false;
        }

        public void AddOutputItem(OutputItem item)
        {
            OutputItem exist = outputFiles.FirstOrDefault(x => x.path.ToLower().Equals(item.path.ToLower()));
            if (exist != null)
            {
                outputFiles.Remove(exist);
                item.panel = exist.panel;
                item.panel.Tag = item;
                outputFiles.Add(item);
            }
            else
            {
                outputFiles.Add(item);
                item.panel = AddOutputItemToGrid(item);
            }
            SaveJsonFile();
        }

        private StackPanel AddOutputItemToGrid(OutputItem item)
        {
            IconInfo icon = item.type.GetIconInfo(typeof(OutputType));
            Image image = GetImage(icon.Name, icon.Width, icon.Height);

            StackPanel panel = new StackPanel();
            panel.Margin = new Thickness(5, 0, 5, 0);
            panel.Orientation = Orientation.Vertical;
            panel.Tag = item;

            panel.Children.Add(image);
            TextBlock name = new TextBlock();
            name.Text = System.IO.Path.GetFileNameWithoutExtension(item.path);
            name.Visibility = Visibility.Collapsed;
            panel.Children.Add(name);

            // Display name - As visible by user
            TextBlock displayName = new TextBlock();
            displayName.MaxWidth = 80;
            displayName.MaxHeight = 40;
            displayName.TextWrapping = TextWrapping.Wrap;
            displayName.TextTrimming = TextTrimming.CharacterEllipsis;
            displayName.Text = System.IO.Path.GetFileNameWithoutExtension(item.path);
            displayName.ToolTip = displayName.Text;
            displayName.Visibility = Visibility.Visible;
            panel.Children.Add(displayName);

            panel.MouseLeftButtonUp += panel_MouseLeftButtonUp;
            panel.MouseRightButtonUp += panel_MouseRightButtonUp;
            panel.MouseMove += OnFolderPanelMouseMove;

            StackPanel.Children.Add(panel);

            return panel;
        }

        private void SaveJsonFile()
        {
            Directory.CreateDirectory(AisConf.OutputDir);
            string jsonFile = System.IO.Path.Combine(AisConf.OutputDir, Properties.Resources.TEXT_OUTPUT_JSON);
            File.WriteAllText(jsonFile, JsonConvert.SerializeObject(outputFiles).ToString(), new UTF8Encoding(true));

            // Wang Issue of quick graph 20180907 Start
            RaiseUpdateEvent(this);
            // Wang Issue of quick graph 20180907 End
        }

        // Wang Issue of quick graph 20180907 Start
        private static void RaiseUpdateEvent(ResultFolderGrip myself)
        {
            if (updateEvent != null)
            {
                updateEvent(myself, EventArgs.Empty);
            }
        }
        // Wang Issue of quick graph 20180907 End

        public bool DeleteExistingCsvFiles(string prefix)
        {
            // delete csv files
            List<OutputItem> deleteFiles = outputFiles.FindAll(x =>
                x.path.ToLower().StartsWith(prefix, StringComparison.CurrentCultureIgnoreCase) &&
                x.path.ToLower().EndsWith(".csv", StringComparison.CurrentCultureIgnoreCase));

            if (deleteFiles != null && deleteFiles.Count > 0)
            {
                if (AisMessageBox.DisplayOkCancelMessageBox(Properties.Message.AIS_W_002) == MessageBoxResult.OK)
                {
                    try
                    {
                        foreach (OutputItem item in deleteFiles)
                        {
                            File.Delete(item.path);
                            StackPanel.Children.Remove(item.panel);
                            outputFiles.Remove(item);
                        }
                    }
#pragma warning disable
                    catch (Exception e)
#pragma warning restore
                    {
                        AisMessageBox.DisplayErrorMessageBox(Properties.Message.AIS_E_062);
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            return true;
        }

        private void BtnExpandCsv_Click(object sender, RoutedEventArgs e)
        {
            if (rightClickedPanel == null)
            {
                return;
            }
            else
            {
                OutputItem rightClickedItem = (OutputItem)rightClickedPanel.Tag;
                if (rightClickedItem == null || rightClickedItem.type.Equals(OutputType.CSV)) {
                    return;
                }
            }

            // delete csv files
            List<OutputItem> deleteFiles = outputFiles.FindAll(FindDeleteOutputItem);
            if (deleteFiles != null && deleteFiles.Count > 0)
            {
                if (AisMessageBox.DisplayOkCancelMessageBox(Properties.Message.AIS_W_002) == MessageBoxResult.OK)
                {
                    try
                    {
                        foreach (OutputItem item in deleteFiles)
                        {
                            File.Delete(item.path);
                            StackPanel.Children.Remove(item.panel);
                            outputFiles.Remove(item);
                        }
                    }
#pragma warning disable
                    catch (Exception e1)
#pragma warning restore
                    {
                        AisMessageBox.DisplayErrorMessageBox(Properties.Message.AIS_E_062);
                        return;
                    }
                }
                else
                {
                    return;
                }
            }

            // export as csv
            List<string> exportedFiles = ExportCsv();
            foreach (string exportedFile in exportedFiles)
            {
                OutputItem item = new OutputItem()
                {
                    creationTime = File.GetLastWriteTime(exportedFile),
                    path = exportedFile,
                    type = OutputType.CSV,
                    sheetNames = null
                };
                outputFiles.Add(item);
                item.panel = AddOutputItemToGrid(item);
            }
            SaveJsonFile();
        }

        private List<string> ExportCsv()
        {
            OutputItem rightClickedItem = (OutputItem)rightClickedPanel.Tag;
            return DAC.AExcel.ExcelUtil.SaveSheetsAsCsv(rightClickedItem.path, rightClickedItem.sheetNames);
        }

        private bool FindDeleteOutputItem(OutputItem item)
        {
            OutputItem rightClickedItem = (OutputItem)rightClickedPanel.Tag;
            if (item.type.Equals(OutputType.CSV))
            {
                string excelName = System.IO.Path.GetFileNameWithoutExtension(rightClickedItem.path) + "_";
                string itemName = System.IO.Path.GetFileName(item.path);
                if (itemName.StartsWith(excelName, StringComparison.CurrentCultureIgnoreCase) && itemName.EndsWith(".csv", StringComparison.CurrentCultureIgnoreCase))
                {
                    return true;
                }
            }

            return false;
        }

        private void BtnCopy_Click(object sender, RoutedEventArgs e)
        {
            StackPanel panel = rightClickedPanel;

            if (rightClickedPanel != null)
            {
                // コピーするファイルのパスをStringCollectionに追加する
                System.Collections.Specialized.StringCollection files = new System.Collections.Specialized.StringCollection();
                files.Add(((OutputItem)panel.Tag).path);
                Clipboard.SetFileDropList(files);
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (rightClickedPanel != null)
                {
                    OutputItem item = (OutputItem)rightClickedPanel.Tag;
                    File.Delete(item.path);
                    StackPanel.Children.Remove(rightClickedPanel);
                    outputFiles.Remove(item);
                    SaveJsonFile();
                }
            }
#pragma warning disable
            catch (Exception ex)
#pragma warning restore
            {
                AisMessageBox.DisplayErrorMessageBox(Properties.Message.AIS_E_062);
            }
        }

        private void ClearOutputGrid()
        {
            StackPanel.Children.Clear();
        }

        private Image GetImage(string imagePath, int width, int height)
        {
            if (imagePath == null) return null;

            Image img = new Image();
            BitmapImage biUser = new BitmapImage();
            biUser.BeginInit();
            biUser.UriSource = new Uri(@"/DAC;component/Resources/Image/" + imagePath, UriKind.RelativeOrAbsolute);
            biUser.EndInit();

            img.Source = biUser;
            img.Width = width;
            img.Height = height;
            return img;
        }

        private StackPanel rightClickedPanel;
        private void panel_MouseRightButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            ContextMenu cm = this.FindResource("OutputMenu") as ContextMenu;
            rightClickedPanel = sender as StackPanel;
            cm.PlacementTarget = sender as StackPanel;

            MenuItem menuItem = cm.Items[0] as MenuItem;
            OutputItem item = rightClickedPanel.Tag as OutputItem;
            if (item.type.Equals(OutputType.CSV))
            {
                menuItem.IsEnabled = false;
            }
            else
            {
                menuItem.IsEnabled = true;
            }

            cm.IsOpen = true;
        }
        private void panel_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            try {
                StackPanel panel = sender as StackPanel;
                if (panel != null)
                {
                    OutputItem item = (OutputItem)panel.Tag;
                    Process.Start(item.path);
                }
            }
#pragma warning disable
            catch (Exception ex)
#pragma warning restore
            {
                AisMessageBox.DisplayErrorMessageBox(Properties.Message.AIS_E_063);
            }
        }
        private void OnFolderPanelMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && e.MiddleButton == MouseButtonState.Released && e.RightButton == MouseButtonState.Released)
            {
                StackPanel panel = sender as StackPanel;
                string[] fileNames = new string[] { ((OutputItem)panel.Tag).path };
                DataObject dataObj = new DataObject(DataFormats.FileDrop, fileNames);
                DragDropEffects effect = DragDropEffects.Copy;
                DragDrop.DoDragDrop(panel, dataObj, effect);
            }
        }

        // Wang Issue of quick graph 20180907 Start
        public void addUpdateHandler(System.EventHandler action)
        {
            updateEvent += action;
        }
        // Wang Issue of quick graph 20180907 End
    }
}
