﻿using DAC.Model.Util;
using DAC.Model;
using DAC.View;
using FoaCore.Common.Control;
using FoaCore.Common.Entity;
using FoaCore.Common.Model;
using FoaCore.Common.Net;
using Grip.Controller.GripMission;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace DAC.Model
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        private ResultFolderFrame resultFolderFrame;

        private CtmDetails ctmDetailsCtrl;
        public CtmDetails CtmDtailsCtrl { get { return ctmDetailsCtrl; } }

        private BaseControl currentEditCtrl;
        public BaseControl CurrentEditCtrl { get { return currentEditCtrl; } }

        private MissionTreeController ctrlMt;
        public MissionTreeController CtrlMt { get { return ctrlMt; } }
        private CtmTreeController ctrlCtm;
        public CtmTreeController CtrlCtm { get { return ctrlCtm; } }
        private GripMissionTreeController ctrlGr;
        public GripMissionTreeController CtrlGr { get { return ctrlGr; } }

        private bool isLoadedMissionTree = false;
        private bool loadedCtmCatalog = false;
        private bool loadedGripMissions = false;

        private ControlMode controlMode = ControlMode.None;
        public ControlMode CurrentMode { get { return controlMode; } }

        private bool isFirstFile = true;
        private string excelFilepath = string.Empty;
        private DataSourceType originalDataSrc = DataSourceType.MISSION;

        public bool IsFirstFile { get { return isFirstFile; } }

        private bool onInitWithExisting = false;
        //Multi_Dac sunyi 2018/11/30 start
        private string DAC = "DAC";
        //Multi_Dac sunyi 2018/11/30 end
        private bool isFirstShow = true;

        // Wang New dac flow 20190308 Start
        private CreateDacParam createDacParam = null;
        // Wang New dac flow 20190308 End

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }
        }
        private string _modeName = "AIS Tool";
        public string ModeName {
            get {
                return _modeName;
            }
            set
            {
                _modeName = SetWindowTitle(value);
                OnPropertyChanged("ModeName");
            }
        }

        private string SetWindowTitle(string modename)
        {
            ControlMode mode = (ControlMode)Enum.Parse(typeof(ControlMode), modename);
            switch (mode)
            {
                case ControlMode.MultiStatusMonitor:
                    return Properties.Resources.TEXT_STD_MULTISTATUSMONITOR;
                case ControlMode.ContinuousGraph:
                    return Properties.Resources.TEXT_GRAPH_CONTINUOUSGRAPH;
                case ControlMode.Emergency:
                    return Properties.Resources.TEXT_STD_EMERGENCY;
                case ControlMode.Online:
                    return Properties.Resources.TEXT_STD_ONLINE;
                case ControlMode.StockTime:
                    return Properties.Resources.TEXT_GRAPH_STOCKTIMECHART;
                case ControlMode.FIFO:
                    return Properties.Resources.TEXT_GRAPH_FIFOCHART;
                case ControlMode.Moment:
                    return Properties.Resources.TEXT_GRAPH_RADARCHART;
                case ControlMode.PastStockTime:
                    return Properties.Resources.TEXT_GRAPH_STOCKTIMECHART_PAST;
                case ControlMode.WorkPlace:
                    return Properties.Resources.TEXT_STD_WORKPLACE;
                case ControlMode.MultiDac:
                    return Properties.Resources.TEXT_STD_DAC;
                case ControlMode.Spreadsheet:
                    return Properties.Resources.TEXT_STD_SPREADSHEET;
                default:
                    return modename;
            }
        }

        // Wang New dac flow 20190308 Start
        public MainWindow(ControlMode mode, CreateDacParam param, bool isFirstFile = true, DataSourceType dataSource = DataSourceType.MISSION) :
            this(mode, true, param.templateFile)
        {
            this.createDacParam = param;
        }
        // Wang New dac flow 20190308 End

        public MainWindow(ControlMode mode, bool isFirstFile = true, string filePath = "", DataSourceType dataSource = DataSourceType.MISSION, CreateDacParam param = null)
        {
            InitializeComponent();
            //AIS_Workplace merge sunyi 2018/10/19 start
            LoadinVisibleChange(false);
            CatalogTab.Visibility = Visibility.Collapsed;
            //AIS_Workplace merge sunyi 2018/10/19 end
            DataContext = this;
            this.createDacParam = param;
            this.controlMode = mode;
            this.isFirstFile = isFirstFile;
            this.excelFilepath = filePath;
            this.originalDataSrc = dataSource;
            this.ModeName = Enum.GetName(typeof(ControlMode), mode);
            //this.Icon = new BitmapImage(new Uri("pack://application:,,,/AIS;component/Resources/Image/ais_icon.ico"));

            // For the V4 Version.　参照カタログとGripMissionは表示しない
            if (AisConf.Config.CmsVersion != null && AisConf.Config.CmsVersion == "V4")
            {
                CatalogTab.Visibility = Visibility.Collapsed;
                MissionTab_GripMissionArea.Visibility = Visibility.Collapsed;
                MissionTab_RowDefinition1.Height = new GridLength(1, GridUnitType.Star);
                MissionTab_RowDefinition2.Height = new GridLength(0, GridUnitType.Pixel);
            }

			// Wang New dac flow 20190308 Start
            //if (mode.Equals(ControlMode.WorkPlace) || mode.Equals(ControlMode.Dac) || mode.Equals(ControlMode.MultiDac))
            if (mode.Equals(ControlMode.WorkPlace) || mode.Equals(ControlMode.Dac) /*|| mode.Equals(ControlMode.MultiDac)*/)
			// Wang New dac flow 20190308 End
            {
                this.btnHome.Visibility = System.Windows.Visibility.Collapsed;
                //Multi_Dac sunyi 2018/11/30 start
                this.DacTemplateName.Visibility = Visibility.Visible;
                //Multi_Dac sunyi 2018/11/30 end

            }
            else if(mode.Equals(ControlMode.MultiDac))
            {
                this.Icon = new BitmapImage(new Uri("pack://application:,,,/DAC;component/Resources/Image/dac_icon.ico"));
            }
            else
            {
                this.Icon = new BitmapImage(new Uri("pack://application:,,,/DAC;component/Resources/Image/ais_icon.ico"));
            }
            if(mode.Equals(ControlMode.MultiStatusMonitor))
            {
                this.ExternalExcelControl.Visibility = Visibility.Visible;
                this.ExternalExcelControl.ParentWindow = this;
                Thickness newMargin = this.TabCtrlDS.Margin;
                newMargin.Bottom = 20.0;
                this.TabCtrlDS.Margin = newMargin;
            }
            else
            {
                this.ExternalExcelControl.Visibility = Visibility.Collapsed;
            }

        }

        #region イベントハンドラ

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            isFirstShow = true;
            Application.Current.MainWindow = this;

            // コントローラ
            this.ctrlMt = new MissionTreeController(this.treeView_Mission_CTM);
            this.ctrlCtm = new CtmTreeController(this.treeView_Catalog);
            this.ctrlGr = new GripMissionTreeController(this.treeView_Mission_Grip);

            if (AisConf.IsExternal)
            {
                // this.button_ActivateGrip.Visibility = Visibility.Hidden;
            }
            // none ならばGripを使用しない
            if (AisConf.Config.GripServerHost.ToLower() == "none")
            {
                this.MissionTab_GripMissionArea.Visibility = Visibility.Collapsed;
            }

            Task task = Task.Run(() =>
            {
                return prepareMibServer();
            });
            // ミッションサーバのデータを最新化
            //Multi_Dac sunyi 2018/11/30 start
            if (Application.Current.MainWindow.Title == DAC)
            {
                Application.Current.MainWindow.Title = "FOA-Agile";
            }
            //Multi_Dac sunyi 2018/11/30 end
        }


        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.TabCtrlDS.SelectedIndex == 0)
            {
                if (!this.loadedCtmCatalog && this.ctrlCtm != null)
                {
                    getCatalogTreeData(false);
                    this.loadedCtmCatalog = true;
                }
            }
            else
            {
                if (!this.isLoadedMissionTree && this.ctrlMt != null)
                {
                    getMissionTreeData(onInitWithExisting);
                    this.onInitWithExisting = false;

                    this.isLoadedMissionTree = true;
                }
            }
        }

        private void clearCheck()
        {
            List<string> list = this.treeView_Catalog.GetCheckedId(false);
            if (list == null ||
                list.Count < 1)
            {
                return;
            }

            this.treeView_Catalog.SkipIsSelectedEvent = true;
            foreach (string ctmId in list)
            {
                var item = this.treeView_Catalog.GetItem(ctmId);
                var checkBox = this.treeView_Catalog.GetCheckBox(item);
                if (checkBox != null)
                {
                    checkBox.IsChecked = false;
                }
            }
            this.treeView_Catalog.SkipIsSelectedEvent = false;
        }

        private void button_Reload_Mission_Click(object sender, RoutedEventArgs e)
        {
            getMissionTreeData(false);
        }

        private void treeView_Catalog_CheckChangedEvent(object sender, EventArgs e)
        {
            try
            {
                System.Threading.Thread.Sleep(1000);


                if (this.treeView_Catalog.SkipIsSelectedEvent)
                {
                    return;
                }

                // カタログツリーのチェックを無視
                if (this.CurrentMode == ControlMode.Online ||
                    this.CurrentMode == ControlMode.Dac ||
                    // Wang Issue NO.687 2018/05/18 Start
                    // 1.階層DAC追加(オプションテンプレート表示統一)
                    // 2.DAC Excel メニュー整理
                    this.CurrentMode == ControlMode.MultiDac ||
                    // Wang Issue NO.687 2018/05/18 End
                    this.CurrentMode == ControlMode.Emergency ||
                    this.CurrentMode == ControlMode.StockTime ||
                    this.CurrentMode == ControlMode.StatusMonitor ||
                    this.CurrentMode == ControlMode.MultiStatusMonitor ||
                    this.CurrentMode == ControlMode.ProjectStatusMonitor ||
                    this.CurrentMode == ControlMode.Torque ||
                    //AIS_WorkPlace
                    this.CurrentMode == ControlMode.WorkPlace)
                {
                    clearCheck();
                    return;
                }

                // オンラインの時のみ無視
                if (this.CurrentMode == ControlMode.ChimneyChart)
                {
                    if ((bool)this.ctmDetailsCtrl.ChimneyChart.radioOn.IsChecked)
                    {
                        clearCheck();
                        return;
                    }
                }

                // オンラインの時のみ無視
                if (this.CurrentMode == ControlMode.FIFO)
                {
                    if ((bool)this.ctmDetailsCtrl.FIFO.radioOn.IsChecked)
                    {
                        clearCheck();
                        return;
                    }
                }

                RoutedEventArgs re = e as RoutedEventArgs;
                CheckBox checkBox = re.OriginalSource as CheckBox;
                StackPanel sp = checkBox.Parent as StackPanel;
                TextBlock tb = null;
                foreach (var child in sp.Children)
                {
                    tb = child as TextBlock;
                    if (tb != null)
                    {
                        break;
                    }
                }
                string ctmName = string.Empty;
                if (tb != null)
                {
                    ctmName = tb.Text.Split(new string[]{ " （" }, StringSplitOptions.None)[0].Trim();
                }

                List<string> list = this.treeView_Catalog.GetCheckedId(true);
                this.ctmDetailsCtrl.SetElementDataToDataGridFromCtmList(list);

                if (this.CurrentEditCtrl.DataSource != DataSourceType.CTM_DIRECT)
                {
                    this.ctmDetailsCtrl.SetInitialDataGrid();
                    this.CurrentEditCtrl.InitializeDataControl();
                }

                if (this.CurrentMode == ControlMode.Spreadsheet)
                {
                    if (this.isFirstFile)
                    {
                        if ((bool)checkBox.IsChecked)
                        {
                            this.currentEditCtrl.DataSourceSelectionChanged();
                        }
                        else
                        {
                            this.currentEditCtrl.DataSourceSelectionChanged(ctmName);
                        }

                        this.CtmDtailsCtrl.SpreadSheet.dataGrid_Output.SetInitialDataGrid();
                        this.CtmDtailsCtrl.SpreadSheet.edit_Grid.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (this.isFirstShow)
                        {
                            this.CtmDtailsCtrl.SpreadSheet.edit_Grid.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            this.CtmDtailsCtrl.SpreadSheet.dataGrid_Output.SetInitialDataGrid();
                            this.CtmDtailsCtrl.SpreadSheet.edit_Grid.Visibility = Visibility.Hidden;
                        }
                    }
                }
                else if (this.CurrentMode == ControlMode.Bulky)
                {
                    if (this.isFirstFile)
                    {
                        this.CtmDtailsCtrl.BulkyEdit.dataGrid_Bulky.SetInitialDataGrid();
                    }
                }
                else if (this.CurrentMode == ControlMode.Comment)
                {
                    if (this.isFirstFile)
                    {
                        if (this.CtmDtailsCtrl.Comment.CurrentTreeSelection != DataSourceType.CTM_DIRECT)
                        {
                            this.CtmDtailsCtrl.Comment.SetInitialList();
                        }
                    }
                }
                this.isFirstShow = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("エラーが発生しました。 treeView_Catalog_CheckChangedEvent()" + Environment.NewLine + ex.Message + Environment.NewLine + ex.GetType().FullName + Environment.NewLine + ex.Source + Environment.NewLine + ex.StackTrace);
                logger.Error("treeView_Catalog_CheckChangedEvent() ", ex);
            }
        }

        private void button_Reload_Grip_Click(object sender, RoutedEventArgs e)
        {
            getGripTreeData(false);
        }

        private void button_Reload_Excel_Click(object sender, RoutedEventArgs e)
        {
            //getGripTreeData(false);
        }

        //ISSUE_NO.749 sunyi 2018/08/13 Start
        //選択したミッションエリアが表示される
        //private void treeView_Mission_Grip_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        private void treeView_Mission_Grip_SelectedItemGotFocus(object sender, RoutedEventArgs e)
        //ISSUE_NO.749 sunyi 2018/08/13 End
        {
            try
            {

                if (this.grid_Element.Visibility != Visibility.Visible
					&& this.controlMode != ControlMode.WorkPlace  // FOA_サーバー化開発 Processing On Server Xj 2018/07/23
					&& this.controlMode != ControlMode.MultiStatusMonitor  // FOA_サーバー化開発 Processing On Server Xj 2018/07/23
                    && this.controlMode != ControlMode.MultiDac)  // FOA_サーバー化開発 Processing On Server sunyi 2018/10/16
                {
                    return;
                }

                if (this.treeView_Mission_Grip.SelectedItem == null)
                {
                    return;
                }

                //ISSUE_NO.727 sunyi 2018/06/14 Start
                //grip mission処理ができるように修正にする
                //// Wang Issue NO.687 2018/05/18 Start
                //// 1.階層DAC追加(オプションテンプレート表示統一)
                //// 2.DAC Excel メニュー整理
                ////if (this.CurrentMode == ControlMode.Dac || this.CurrentMode == ControlMode.StatusMonitor || this.CurrentMode == ControlMode.Emergency ||
                //if (this.CurrentMode == ControlMode.Dac || this.CurrentMode == ControlMode.MultiDac || this.CurrentMode == ControlMode.StatusMonitor || this.CurrentMode == ControlMode.Emergency ||
                //// Wang Issue NO.687 2018/05/18 End
                //    this.CurrentMode == ControlMode.Moment || this.CurrentMode == ControlMode.Comment || this.CurrentMode == ControlMode.MultiStatusMonitor
                if (this.CurrentMode == ControlMode.Dac || this.CurrentMode == ControlMode.Emergency ||
                    this.CurrentMode == ControlMode.Moment || this.CurrentMode == ControlMode.Comment
                //ISSUE_NO.727 sunyi 2018/06/14 Start
                    || this.CurrentMode == ControlMode.ProjectStatusMonitor || this.CurrentMode == ControlMode.Torque || this.CurrentMode == ControlMode.StreamGraph)
                {
                    return;
                }

                //「CTM選択ボタン」使用不可
                this.CtmDtailsCtrl.button_Selection.IsEnabled = false;

                if (this.ctrlGr.isMissionSelected())
                {
                    string selectedMissionId = this.treeView_Mission_Grip.SelectedId;
					// FOA_サーバー化開発 Processing On Server Dcs 2018/07/23 Start
                    if (this.controlMode == ControlMode.MultiStatusMonitor)
                    {
                        //選択されたミッションを判断
                        if (isSelectedMission(selectedMissionId))
                        {
                            return;
                        }
                        //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                        //DAC-93 sunyi 20190916 start
                        //選択ミッションに表示されたミッションを削除して再度に選択しても表示されないについて、検討が必要。
                        if (GripMissionSelectedId.Equals(selectedMissionId)) return;
                        //DAC-93 sunyi 20190916 end
                        //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                        this.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(selectedMissionId, this.ctrlGr);
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                        GripMissionSelectedId = selectedMissionId;
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                    }
                    else if (this.controlMode == ControlMode.MultiDac)
                    {
                        //選択されたミッションを判断
                        if (isSelectedMission_Dac(selectedMissionId))
                        {
                            return;
                        }
                        //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                        //DAC-93 sunyi 20190916 start
                        //選択ミッションに表示されたミッションを削除して再度に選択しても表示されないについて、検討が必要。
                        if (GripMissionSelectedId.Equals(selectedMissionId)) return;
                        //DAC-93 sunyi 20190916 end
                        //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                        this.CtmDtailsCtrl.SetElementDataToDataGrid_Grip_Dac(selectedMissionId, this.ctrlGr);
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                        GripMissionSelectedId = selectedMissionId;
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                    }
                    else if (this.controlMode == ControlMode.WorkPlace)
                    {
                        //選択されたミッションを判断
                        if (isSelectedMission_WorkPlace(selectedMissionId))
                        {
                            return;
                        }
                        //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                        //DAC-93 sunyi 20190916 start
                        //選択ミッションに表示されたミッションを削除して再度に選択しても表示されないについて、検討が必要。
                        if (GripMissionSelectedId.Equals(selectedMissionId)) return;
                        //DAC-93 sunyi 20190916 end
                        //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                        this.CtmDtailsCtrl.SetElementDataToDataGrid_Grip_Wp(selectedMissionId, this.ctrlGr);
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                        GripMissionSelectedId = selectedMissionId;
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                    }
                    else
                    {
                        this.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(selectedMissionId);
                    }
                }

                if (this.CurrentEditCtrl.DataSource != DataSourceType.GRIP)
                {
                    this.ctmDetailsCtrl.SetInitialDataGrid();
                    this.CurrentEditCtrl.InitializeDataControl();
                }

                if (this.CurrentMode == ControlMode.Spreadsheet)
                {
                    if (this.isFirstFile)
                    {
                        this.currentEditCtrl.DataSourceSelectionChanged();

                    }
                        //「edit_Grid」が表示しない
                    ((DAC.Model.UserControl_SpreadsheetTemplate)(this.CurrentEditCtrl)).edit_Grid.Visibility = Visibility.Hidden;

                }
                else if (this.CurrentMode == ControlMode.Online)
                {
                    if (this.isFirstFile)
                    {
                        this.CtmDtailsCtrl.Online.dataGrid_Output.SetInitialDataGrid();

                    }
                        //「edit_Grid」が表示しない
                    ((DAC.Model.UserControl_OnlineTemplate)(this.CurrentEditCtrl)).edit_Grid.Visibility = Visibility.Hidden;

                }
                else if (this.CurrentMode == ControlMode.Bulky)
                {
                    if (this.isFirstFile)
                    {
                        this.CtmDtailsCtrl.BulkyEdit.dataGrid_Bulky.SetInitialDataGrid();
                    }
                }
                else if (this.CurrentMode == ControlMode.Emergency)
                {
                    Mission mission = getSelectedMission(this.ctrlGr);
                    this.CtmDtailsCtrl.Emergency.SetMissionName(AisUtil.GetCatalogLang(mission), mission.Id);
                }
                else if (this.CurrentMode == ControlMode.StatusMonitor)
                {
                    if (this.ctrlGr.isMissionSelected() == true)
                    {
                        Mission mission = getSelectedMission(this.ctrlGr);
                        this.CtmDtailsCtrl.StatusMonitor.currentSelectedMission = mission;
                        this.CtmDtailsCtrl.StatusMonitor.lbMission.SelectedIndex = -1;
                    }
                }
                else if (this.CurrentMode == ControlMode.MultiStatusMonitor)
                {
                    if (this.ctrlGr.isMissionSelected() == true)
                    {
                        Mission mission = getSelectedMission(this.ctrlGr);
                        this.CtmDtailsCtrl.MultiStatusMonitor.currentSelectedMission = mission;
                        //this.CtmDtailsCtrl.MultiStatusMonitor.lbMission.SelectedIndex = -1;
                    }
                }
                else if (this.CurrentMode == ControlMode.ProjectStatusMonitor)
                {
                    if (this.ctrlGr.isMissionSelected() == true)
                    {
                        Mission mission = getSelectedMission(this.ctrlGr);
                        this.CtmDtailsCtrl.ProjectStatusMonitor.currentSelectedMission = mission;
                        this.CtmDtailsCtrl.ProjectStatusMonitor.lbMission.SelectedIndex = -1;
                    }
                }
                else if (this.CurrentMode == ControlMode.Torque)
                {
                    if (this.ctrlGr.isMissionSelected() == true)
                    {
                        Mission mission = getSelectedMission(this.ctrlGr);
                        this.CtmDtailsCtrl.Torque.currentSelectedMission = mission;
                        this.CtmDtailsCtrl.Torque.lbMission.SelectedIndex = -1;
                    }
                }
                else if (this.CurrentMode == ControlMode.WorkPlace)
                {
                    if (this.ctrlGr.isMissionSelected() == true)
                    {
                        Mission mission = getSelectedMission(this.ctrlGr);
                        this.CtmDtailsCtrl.WorkPlace.currentSelectedMission = mission;
                    }
                }

                var control = this.grid_Main.Children[0] as BaseControl;
                if (control != null)
                {
					// FOA_サーバー化開発 Processing On Server Xj 2018/07/23 Start
                    if (this.CurrentMode == ControlMode.MultiStatusMonitor)
                    {
                        control.IsSelectedProgramMission = true;
                    }
                    else
                    {
                        control.IsSelectedProgramMission = false;
                    }
					// FOA_サーバー化開発 Processing On Server Xj 2018/07/23 End
                    //ISSUE_NO.727 sunyi 2018/06/14 Start
                    ////grip mission処理ができるように修正にする
                    //ISSUE_NO.772 sunyi 2018/07/09 Start
                    //エレメント編集画面を正しく出力する
                    //this.CurrentEditCtrl.InitializeDataControl();
                    if (this.CurrentMode == ControlMode.MultiStatusMonitor || this.CurrentMode == ControlMode.StatusMonitor
                        || this.CurrentMode == ControlMode.WorkPlace || this.CurrentMode == ControlMode.MultiDac) // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 Start  // Multi_Dac 修正
                    {
                        this.CurrentEditCtrl.InitializeDataControl();
                    }
                    //ISSUE_NO.772 sunyi 2018/07/09 End
                    //ISSUE_NO.727 sunyi 2018/06/14 End
                }

                control.SelectedGripMission = getSelectedGripMission(this.ctrlGr);

                //CTMクラスを含むG-Missionをクリック時、エラーを表示する
                //E2NaviUI sunyi 20190305 start
                //MISSION条件対応
                if (control.SelectedGripMission.StartNode != null)
                {
                    if (control.SelectedGripMission.StartNode.CtmId.Contains("@"))
                    {
                        FoaCore.Common.Util.FoaMessageBox.ShowError("AIS_E_042");
                        this.CurrentEditCtrl.DataSource = DataSourceType.NONE;
                        this.ctmDetailsCtrl.SetInitialDataGrid();
                        this.CurrentEditCtrl.InitializeDataControl();
                        return;
                    }
                }
                if (control.SelectedGripMission.StartNodes != null)
                {
                    foreach (var startNode in control.SelectedGripMission.StartNodes)
                    {
                        if (startNode.Value.CtmId.Contains("@"))
                        {
                            FoaCore.Common.Util.FoaMessageBox.ShowError("AIS_E_042");
                            this.CurrentEditCtrl.DataSource = DataSourceType.NONE;
                            this.ctmDetailsCtrl.SetInitialDataGrid();
                            this.CurrentEditCtrl.InitializeDataControl();
                            return;
                        }
                    }
                }
                //E2NaviUI sunyi 20190305 end

                if (control.SelectedGripMission.EndNodes != null)
                {
                    for (int i = 0; i < control.SelectedGripMission.EndNodes.Count; i++)
                    {
                        if (control.SelectedGripMission.EndNodes[i].CtmId.Contains("@"))
                        {
                            FoaCore.Common.Util.FoaMessageBox.ShowError("AIS_E_042");
                            this.CurrentEditCtrl.DataSource = DataSourceType.NONE;
                            this.ctmDetailsCtrl.SetInitialDataGrid();
                            this.CurrentEditCtrl.InitializeDataControl();
                            return;
                        }
                    }
                }

                //ISSUE_NO.616,708 sunyi 2018/05/31 start
                //ミッションを変更したら設定内容をクリアする。
                if (!this.isFirstShow)
                {
                    //ISSUE_NO.861 sunyi 2018/10/04 start
                    //内容修正の時、ミッションが変更ない場合、設定条件をクリアしない
                    if (!GripValueChangedId.Equals(this.treeView_Mission_Grip.SelectedId))
                    {
                        clearOldMissionData();
                    }
                }
                GripValueChangedId = this.treeView_Mission_Grip.SelectedId;
                this.isFirstShow = false;
                //ISSUE_NO.616,708 sunyi 2018/05/31 end
            }
            catch (Exception ex)
            {
                MessageBox.Show("エラーが発生しました。 treeView_Mission_Grip_SelectedItemChanged()" + Environment.NewLine + ex.Message + Environment.NewLine + ex.GetType().FullName + Environment.NewLine + ex.Source + Environment.NewLine + ex.StackTrace);
                logger.Error("treeView_Mission_Grip_SelectedItemChanged() ", ex);
            }

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (this.currentEditCtrl.Bw != null)
            {
                this.currentEditCtrl.Bw.CancelAsync();
            }
            if (this.currentEditCtrl.DownloadCts != null)
            {
                this.currentEditCtrl.DownloadCts.Cancel();
            }

			// Wang New dac flow 20190308 Start
            //Window home = new Window_Home();
            Window home;
            if (this.CurrentMode.Equals(ControlMode.MultiDac))
            {
                home = new Dac_Home();
            }
            else
            {
                home = new Window_Home();
            }
			// Wang New dac flow 20190308 End
            this.CtmDtailsCtrl.Online = null;
            this.CtmDtailsCtrl.BulkyEdit = null;
            this.CtmDtailsCtrl.Emergency = null;
            Application.Current.MainWindow = home;
            this.Close();

            home.Show();
        }

        private void button_ActivatePat_Click(object sender, RoutedEventArgs e)
        {
            string userId = AisConf.UserId;
            string passwd = AisConf.Password;
            string catalogLang = AisConf.CatalogLang;
            string uiLang = AisConf.UiLang;

            AisUtil.ActivateCmsUi(userId, passwd, catalogLang, uiLang);
        }

        private void button_ActivateGrip_Click(object sender, RoutedEventArgs e)
        {
            string userId = AisConf.UserId;
            string passwd = AisConf.Password;
            string catalogLang = AisConf.CatalogLang;
            string uiLang = AisConf.UiLang;

            AisUtil.ActivateGripR2(userId, passwd, catalogLang, uiLang);
        }

        #endregion

        #region ミッション表示処理

        /// <summary>
        /// ミッションサーバに対して、他システム(MMSやSYB)の
        /// データをキャッシュする為の準備を行います。
        /// </summary>
        private async Task prepareMibServer()
        {
            CmsHttpClient client = new CmsHttpClient(this);
            client.LastInvokeCompleteHandler = true;
            client.CompleteHandler += resJson =>
            {
                // ユーザ一覧
                JToken token = JToken.Parse(resJson);
                JArray users = (JArray)token["users"];

                // エージェントにユーザ名を表示する必要があり、
                // MIBの画面を開いたレスポンスに全ユーザのJsonが返ってきます。
                List<UserInfo> allUserList = JsonConvert.DeserializeObject<List<UserInfo>>(users.ToString());
                allUserList.ForEach(ui =>
                {
                    // これはなんとも...UserInfoのデータの持ち方が悪い
                    List<LangObject> displayNames = JsonConvert.DeserializeObject<List<LangObject>>(ui.DisplayName);
                    ctrlMt.AddUserNames(ui.Id, displayNames);
                });


                if (this.originalDataSrc == DataSourceType.CTM_DIRECT)
                {
                    // 最初はSelectedIndex=0なのでここでSelectedIndexを0にしてもイベントが発生しない。
                    getCatalogTreeData(!this.isFirstFile);
                    // TODO 失敗する可能性があるので成功したときだけtrueにすべき？
                    this.loadedCtmCatalog = true;
                }
                else if (this.originalDataSrc == DataSourceType.MISSION)
                {
                    this.onInitWithExisting = !this.isFirstFile;
                    this.TabCtrlDS.SelectedIndex = 1;
                }
                else
                {
                    if (!this.isFirstFile)
                    {
                        getGripTreeData(true);
                        loadedGripMissions = true;

                        this.TabCtrlDS.SelectedIndex = 1;
                    }
                }

                if (!loadedGripMissions && !this.loadedCtmCatalog)
                {
                    getGripTreeData(false);
                    loadedGripMissions = true;

                    this.TabCtrlDS.SelectedIndex = 1;
                }

                changeControlMode(this.controlMode);
                LoadinVisibleChange(true);
            };

            client.Put(CmsUrl.GetMibUrl(), "");
        }
		// FOA_サーバー化開発 Processing On Server Xj 2018/07/31 Start
        /// <summary>
        /// 選択されているミッションの存在判断
        /// </summary>
        /// <param name="missionId">選択されているミッションID</param>
        /// <returns>ある、なし</returns>
        public bool isSelectedMission(string missionId)
        {
            bool isExists = false;
            var tvRefDicMission = this.CtmDtailsCtrl.MultiStatusMonitor.AisTvRefDicMission;
            List<TreeViewItem> allItems = tvRefDicMission.GetAllItems();

            foreach (var item in allItems)
            {
                JToken token = tvRefDicMission.GetToken(item);
                string tmpMissionId = (string)token["id"];
                if (missionId == tmpMissionId)
                {
                    isExists = true;
                    break;
                }
            }

            return isExists;
        }
        public bool isSelectedMission_WorkPlace(string missionId)
        {
            bool isExists = false;
            var tvRefDicMission = this.CtmDtailsCtrl.WorkPlace.AisTvRefDicMission;
            List<TreeViewItem> allItems = tvRefDicMission.GetAllItems();

            foreach (var item in allItems)
            {
                JToken token = tvRefDicMission.GetToken(item);
                string tmpMissionId = (string)token["id"];
                if (missionId == tmpMissionId)
                {
                    isExists = true;
                    break;
                }
            }

            return isExists;
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 Start
        // Multi_Dac 修正
        public bool isSelectedMission_Dac(string missionId)
        {
            bool isExists = false;
            var tvRefDicMission = this.CtmDtailsCtrl.DAC.AisTvRefDicMission;
            List<TreeViewItem> allItems = tvRefDicMission.GetAllItems();

            foreach (var item in allItems)
            {
                JToken token = tvRefDicMission.GetToken(item);
                string tmpMissionId = (string)token["id"];
                if (missionId == tmpMissionId)
                {
                    isExists = true;
                    break;
                }
            }

            return isExists;
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 end

        /// <summary>
        /// 選択されているミッションを取得
        /// </summary>
        /// <param name="ctrlMt">ミッションツリーコントローラ</param>
        /// <returns>選択されているミッション</returns>
        public Mission getSelectedMission(MissionTreeController ctrlMt)
        {
            Mission selectedMission = new Mission();

            TreeViewItem selectedItem = ctrlMt.tree.SelectedItem as TreeViewItem;
            StackPanel selectedHeader = selectedItem.Header as StackPanel;
            string json = selectedHeader.DataContext.ToString();
            selectedMission = JsonConvert.DeserializeObject<Mission>(json);

            return selectedMission;
        }

        /// <summary>
        /// 選択されているミッションを取得
        /// </summary>
        /// <param name="ctrlGr">グリップツリーコントローラ</param>
        /// <returns>選択されているミッション</returns>
        private Mission getSelectedMission(GripMissionTreeController ctrlGr)
        {
            Mission selectedMission = new Mission();

            TreeViewItem selectedItem = ctrlGr.tree.SelectedItem as TreeViewItem;
            StackPanel selectedHeader = selectedItem.Header as StackPanel;
            string json = selectedHeader.DataContext.ToString();
            selectedMission = JsonConvert.DeserializeObject<Mission>(json);

            return selectedMission;
        }

        /// <summary>
        /// 選択されているミッションを取得 (GripMissionCtm)
        /// </summary>
        /// <param name="ctrlGr">グリップツリーコントローラ</param>
        /// <returns>選択されているミッション</returns>
        private GripMissionCtm getSelectedGripMission(GripMissionTreeController ctrlGr)
        {
            GripMissionCtm selectedMission = new GripMissionCtm();

            TreeViewItem selectedItem = ctrlGr.tree.SelectedItem as TreeViewItem;
            StackPanel selectedHeader = selectedItem.Header as StackPanel;
            string json = selectedHeader.DataContext.ToString();
            selectedMission = JsonConvert.DeserializeObject<GripMissionCtm>(json);

            return selectedMission;
        }


        /// <summary>
        /// ミッションツリーのデータ取得
        /// </summary>
        private async void getMissionTreeData(bool onInitWithE)
        {
            await Task.Delay(100);
            this.ctrlMt.Load(false, true, onInitWithE);
        }

        //ISSUE_NO.616,708 sunyi 2018/05/31 start
        //ミッションを変更したら設定内容をクリアする。
        private System.Data.DataTable getBuklyGraphFormat()
        {
            System.Data.DataTable dt = new System.Data.DataTable();
            int columnLength = 5;
            int rowLength = 12;

            // Column
            for (int i = 1; i < columnLength; i++)
            {
                string content = string.Format("Column {0}", i);
                var column = new System.Data.DataColumn(content, Type.GetType("System.String"));
                dt.Columns.Add(column);
            }
            // Row
            for (int i = 0; i < rowLength; i++)
            {
                System.Data.DataRow row = dt.NewRow();
                dt.Rows.Add(row);
            }
            return dt;
        }

        private void clearOldMissionData()
        {
            switch (this.controlMode)
            {
                case ControlMode.FIFO:
                    {
                        this.CtmDtailsCtrl.FIFO.textBlock_EndTime.Text = "";
                        this.CtmDtailsCtrl.FIFO.textBlock_StartTime.Text = "";
                        break;
                    }
                case ControlMode.LeadTime:
                    {
                        this.CtmDtailsCtrl.LeadTime.textBlock_EndTime.Text = "";
                        this.CtmDtailsCtrl.LeadTime.textBlock_StartTime.Text = "";
                        this.CtmDtailsCtrl.LeadTime.textBlock_LT.Text = "";
                        break;
                    }
                case ControlMode.StockTime:
                    {
                        this.CtmDtailsCtrl.StockTime.textBlock_EndTime.Text = "";
                        this.CtmDtailsCtrl.StockTime.textBlock_StartTime.Text = "";
                        break;
                    }
                case ControlMode.ChimneyChart:
                    {
                        this.CtmDtailsCtrl.ChimneyChart.textBlock_EndTime.Text = "";
                        this.CtmDtailsCtrl.ChimneyChart.textBlock_StartTime.Text = "";
                        this.CtmDtailsCtrl.ChimneyChart.textBlock_LT.Text = "";
                        break;
                    }
                case ControlMode.Bulky:
                    {
                        this.CtmDtailsCtrl.BulkyEdit.radioButton_Horizontal.IsChecked = false;
                        this.CtmDtailsCtrl.BulkyEdit.radioButton_Vertical.IsChecked = true;
                        this.CtmDtailsCtrl.BulkyEdit.dataGrid_Bulky.ColumnWidth = 75;
                        this.CtmDtailsCtrl.BulkyEdit.dataGrid_Bulky.ItemsSource = getBuklyGraphFormat().DefaultView;
                        break;
                    }
                case ControlMode.ContinuousGraph:
                    {
                        this.CtmDtailsCtrl.ContinuousGraph.radioButton_Horizontal.IsChecked = false;
                        this.CtmDtailsCtrl.ContinuousGraph.radioButton_Vertical.IsChecked = true;
                        this.CtmDtailsCtrl.ContinuousGraph.dataGrid_Bulky.ColumnWidth = 75;
                        this.CtmDtailsCtrl.ContinuousGraph.dataGrid_Bulky.ItemsSource = getBuklyGraphFormat().DefaultView;
                        break;
                    }
                case ControlMode.Comment:
                    {
                        this.CtmDtailsCtrl.Comment.elementList.Clear();
                        this.CtmDtailsCtrl.Comment.backDataList.Clear();
                        this.CtmDtailsCtrl.Comment.backDataListForDisplay.Clear();
                        break;
                    }
                case ControlMode.PastStockTime:
                    {
                        this.CtmDtailsCtrl.PastStockTime.textBlock_EndTime.Text = "";
                        this.CtmDtailsCtrl.PastStockTime.textBlock_StartTime.Text = "";
                        break;
                    }
                case ControlMode.Frequency:
                    {
                        this.CtmDtailsCtrl.Frequency.textBlock_LT.Text = "";
                        this.CtmDtailsCtrl.Frequency.cmbCalc.SelectedIndex = 0;
                        break;
                    }
                case ControlMode.Moment:
                    {
                        this.CtmDtailsCtrl.Moment.textBlock_Element.Text = "";
                        this.CtmDtailsCtrl.Moment.textBlock_ElementSita.Text = "";
                        break;
                    }
                default:
                    break;
            }
        }
        //ISSUE_NO.616,708 sunyi 2018/05/31 end

        //ISSUE_NO.861 sunyi 2018/10/04 start
        //内容修正の時、ミッションが変更ない場合、設定条件をクリアしない
        private static string MissionValueChangedId = "";
        private static string GripValueChangedId = "";
        //ISSUE_NO.861 sunyi 2018/10/04 end

        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
        private string CtmMissionSelectedId = "";
        private string GripMissionSelectedId = "";
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
        //ISSUE_NO.749 sunyi 2018/08/13 Start
        //選択したミッションエリアが表示される
        //private void treeView_Mission_CTM_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        private void treeView_Mission_CTM_SelectedItemGotFocus(object sender, RoutedEventArgs e)
        //ISSUE_NO.749 sunyi 2018/08/13 End
        {
            try
            {
                //TreeView tv = (TreeView)e.OriginalSource;
                string id = (string)this.treeView_Mission_CTM.SelectedId;
                TreeViewItem tvi2 = (TreeViewItem)this.treeView_Mission_CTM.SelectedItem;

                if (this.grid_Element.Visibility != Visibility.Visible
                    && this.controlMode != ControlMode.WorkPlace
                    && this.controlMode != ControlMode.MultiStatusMonitor // FOA_サーバー化開発 Processing On Server Xj 2018/07/23
                    && this.controlMode != ControlMode.MultiDac)  // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 Start  // Multi_Dac 修正
                {
                    return;
                }

                if (this.treeView_Mission_CTM.SelectedItem == null)
                {
                    return;
                }

                if (!this.ctrlMt.isMissionSelected())
                {
                    return;
                }

                //「CTM選択ボタン」使用制御
                if (this.CurrentMode == ControlMode.Online || this.CurrentMode == ControlMode.Spreadsheet)
                {
                    this.CtmDtailsCtrl.button_Selection.IsEnabled = true;
                }
                else
                {
                    this.CtmDtailsCtrl.button_Selection.IsEnabled = false;
                }

                string selectedMissionId = this.treeView_Mission_CTM.SelectedId;
                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 Start
                // Multi_Dac 修正
                if (this.controlMode == ControlMode.MultiDac)
                {
                    //選択されたミッションを判断
                    if (isSelectedMission_Dac(selectedMissionId))
                    {
                        return;
                    }
                    //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                    //DAC-93 sunyi 20190916 start
                    //選択ミッションに表示されたミッションを削除して再度に選択しても表示されないについて、検討が必要。
                    if (CtmMissionSelectedId.Equals(selectedMissionId)) return;
                    //DAC-93 sunyi 20190916 end
                    //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                    this.CtmDtailsCtrl.SetElementDataToDataGrid_Dac(selectedMissionId, this.ctrlMt);
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                    CtmMissionSelectedId = selectedMissionId;
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                }
                // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 end

				// FOA_サーバー化開発 Processing On Server Dcs 2018/07/31 Start
                else if (this.controlMode == ControlMode.MultiStatusMonitor)
                {
                    //選択されたミッションを判断
                    if (isSelectedMission(selectedMissionId))
                    {
                        return;
                    }
                    //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                    //DAC-93 sunyi 20190916 start
                    //選択ミッションに表示されたミッションを削除して再度に選択しても表示されないについて、検討が必要。
                    if (CtmMissionSelectedId.Equals(selectedMissionId)) return;
                    //DAC-93 sunyi 20190916 end
                    //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                    this.CtmDtailsCtrl.SetElementDataToDataGrid(selectedMissionId, this.ctrlMt);
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                    CtmMissionSelectedId = selectedMissionId;
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                }
                else if (this.controlMode == ControlMode.WorkPlace)
                {
                    //選択されたミッションを判断
                    if (isSelectedMission_WorkPlace(selectedMissionId))
                    {
                        return;
                    }
                    //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                    //DAC-93 sunyi 20190916 start
                    //選択ミッションに表示されたミッションを削除して再度に選択しても表示されないについて、検討が必要。
                    if (CtmMissionSelectedId.Equals(selectedMissionId)) return;
                    //DAC-93 sunyi 20190916 end
                    //// FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                    this.CtmDtailsCtrl.SetElementDataToDataGrid_Wp(selectedMissionId, this.ctrlMt);
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 start
                    CtmMissionSelectedId = selectedMissionId;
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/10/04 end
                }
                else
                {
                    this.CtmDtailsCtrl.SetElementDataToDataGrid(selectedMissionId);
                }

                //ISSUE_NO.616,708 sunyi 2018/05/31 start
                //ミッションを変更したら設定内容をクリアする。
                if (!isFirstShow)
                {
                    //ISSUE_NO.861 sunyi 2018/10/04 start
                    //内容修正の時、ミッションが変更ない場合、設定条件をクリアしない
                    if (!MissionValueChangedId.Equals(selectedMissionId))
                    {
                    //ISSUE_NO.861 sunyi 2018/10/04 end
                        clearOldMissionData();
                    }
                }
                //ISSUE_NO.616,708 sunyi 2018/05/31 end

                if (!this.currentEditCtrl.Initializing &&
                    this.CurrentEditCtrl.DataSource != DataSourceType.MISSION)
                {
                    this.ctmDetailsCtrl.SetInitialDataGrid();
                    this.CurrentEditCtrl.InitializeDataControl();
                }

                if (this.CurrentMode == ControlMode.Spreadsheet)
                {
                    if (this.isFirstFile)
                    {
                        this.CtmDtailsCtrl.SpreadSheet.dataGrid_Output.SetInitialDataGrid();
                        this.CtmDtailsCtrl.SpreadSheet.edit_Grid.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (this.isFirstShow) {
                            this.CtmDtailsCtrl.SpreadSheet.edit_Grid.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            //ISSUE_NO.861 sunyi 2018/10/04 start
                            //内容修正の時、ミッションが変更ない場合、設定条件をクリアしない
                            if (!MissionValueChangedId.Equals(selectedMissionId))
                            {
                                //ISSUE_NO.861 sunyi 2018/10/04 end
                                this.CtmDtailsCtrl.SpreadSheet.dataGrid_Output.SetInitialDataGrid();
                                this.CtmDtailsCtrl.SpreadSheet.edit_Grid.Visibility = Visibility.Hidden;
                            }
                        }
                    }
                }
                else if (this.CurrentMode == ControlMode.Online)
                {
                    if (this.isFirstFile)
                    {
                        this.CtmDtailsCtrl.Online.dataGrid_Output.SetInitialDataGrid();
                        this.CtmDtailsCtrl.Online.edit_Grid.Visibility = Visibility.Hidden;
                    }
                    else
                    {
                        if (this.isFirstShow) {
                            this.CtmDtailsCtrl.Online.edit_Grid.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            //ISSUE_NO.861 sunyi 2018/10/04 start
                            //内容修正の時、ミッションが変更ない場合、設定条件をクリアしない
                            if (!MissionValueChangedId.Equals(selectedMissionId))
                            {
                                this.CtmDtailsCtrl.Online.dataGrid_Output.SetInitialDataGrid();
                                this.CtmDtailsCtrl.Online.edit_Grid.Visibility = Visibility.Hidden;
                            }
                        }
                    }
                }
                else if (this.CurrentMode == ControlMode.Bulky)
                {
                    if (this.isFirstFile)
                    {
                        this.CtmDtailsCtrl.BulkyEdit.dataGrid_Bulky.SetInitialDataGrid();
                    }
                }
                else if (this.CurrentMode == ControlMode.Emergency)
                {
                    Mission mission = getSelectedMission(this.ctrlMt);
                    this.CtmDtailsCtrl.Emergency.SetMissionName(AisUtil.GetCatalogLang(mission), mission.Id);
                }
                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                //else if (this.CurrentMode == ControlMode.Dac)
                else if (this.CurrentMode == ControlMode.Dac || this.CurrentMode == ControlMode.MultiDac)
                // Wang Issue NO.687 2018/05/18 End
                {
                    //10/16
                    //this.currentEditCtrl.DataSourceSelectionChanged();
                    if (this.ctrlMt.isMissionSelected() == true)
                    {
                        Mission mission = getSelectedMission(this.ctrlMt);
                        this.CtmDtailsCtrl.DAC.currentSelectedMission = mission;
                        //this.CtmDtailsCtrl.MultiStatusMonitor.lbMission.SelectedIndex = -1;
                    }
                }
                else if (this.CurrentMode == ControlMode.StatusMonitor)
                {
                    if (this.ctrlMt.isMissionSelected() == true)
                    {
                        Mission mission = getSelectedMission(this.ctrlMt);
                        this.CtmDtailsCtrl.StatusMonitor.currentSelectedMission = mission;
                        this.CtmDtailsCtrl.StatusMonitor.lbMission.SelectedIndex = -1;
                    }
                }
                else if (this.CurrentMode == ControlMode.MultiStatusMonitor)
                {
                    if (this.ctrlMt.isMissionSelected() == true)
                    {
                        Mission mission = getSelectedMission(this.ctrlMt);
                        this.CtmDtailsCtrl.MultiStatusMonitor.currentSelectedMission = mission;
                        //this.CtmDtailsCtrl.MultiStatusMonitor.lbMission.SelectedIndex = -1;
                    }
                }
                else if (this.CurrentMode == ControlMode.ProjectStatusMonitor)
                {
                    if (this.ctrlMt.isMissionSelected() == true)
                    {
                        Mission mission = getSelectedMission(this.ctrlMt);
                        this.CtmDtailsCtrl.ProjectStatusMonitor.currentSelectedMission = mission;
                        this.CtmDtailsCtrl.ProjectStatusMonitor.lbMission.SelectedIndex = -1;
                    }
                }
                else if (this.CurrentMode == ControlMode.Torque)
                {
                    if (this.ctrlMt.isMissionSelected() == true)
                    {
                        Mission mission = getSelectedMission(this.ctrlMt);
                        this.CtmDtailsCtrl.Torque.currentSelectedMission = mission;
                        this.CtmDtailsCtrl.Torque.lbMission.SelectedIndex = -1;
                    }
                }
                else if (this.CurrentMode == ControlMode.Comment)
                {
                    if (this.isFirstFile)
                    {
                        if (this.CtmDtailsCtrl.Comment.CurrentTreeSelection != DataSourceType.MISSION)
                        {
                            this.CtmDtailsCtrl.Comment.CurrentTreeSelection = DataSourceType.MISSION;
                            this.CtmDtailsCtrl.Comment.SetInitialList();
                        }
                    }
                }

                var control = this.grid_Main.Children[0] as BaseControl;
                if (control != null)
                {
                    control.IsSelectedProgramMission = true;
                    //ISSUE_NO.727 sunyi 2018/06/14 Start
                    ////grip mission処理ができるように修正にする
                    //ISSUE_NO.772 sunyi 2018/07/09 Start
                    //エレメント編集画面を正しく出力する
                    //this.CurrentEditCtrl.InitializeDataControl();
                    if (this.CurrentMode == ControlMode.MultiStatusMonitor || this.CurrentMode == ControlMode.StatusMonitor
                        || this.CurrentMode == ControlMode.MultiDac)// FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 Start  // Multi_Dac 修正
                    {
                        this.CurrentEditCtrl.InitializeDataControl();
                    }
                    //ISSUE_NO.772 sunyi 2018/07/09 End
                    //ISSUE_NO.727 sunyi 2018/06/14 End
                }

                control.SelectedMission = getSelectedMission(this.ctrlMt);
                //ISSUE_NO.861 sunyi 2018/10/04 start
                //内容修正の時、ミッションが変更ない場合、設定条件をクリアしない
                MissionValueChangedId = selectedMissionId;
                //ISSUE_NO.861 sunyi 2018/10/04 end
                this.isFirstShow = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("エラーが発生しました。 treeView_Mission_CTM_SelectedItemChanged()" + Environment.NewLine + ex.Message + Environment.NewLine + ex.GetType().FullName + Environment.NewLine + ex.Source + Environment.NewLine + ex.StackTrace);
                logger.Error("treeView_Mission_CTM_SelectedItemChanged() ", ex);

            }
        }

        #endregion

        /// <summary>
        /// Gripツリーのデータ取得
        /// </summary>
        private async void getGripTreeData(bool onInitWithE)
        {
            await Task.Delay(100);
            // none ならばGripを使用しない
            if (AisConf.Config.GripServerHost.ToLower() == "none")
            {
                return;
            }

            this.ctrlGr.Load(onInitWithE);
        }

        private void getCatalogTreeData(bool onInitWithE)
        {
            this.ctrlCtm.Load(onInitWithE);
        }

        private void changeControlMode(ControlMode mode)
        {
            // FOA_サーバー化開発 Processing On Server Dcs 2018/07/20 Start
            // showEditElementPanel();
            showEditElementPanel(mode);
            // FOA_サーバー化開発 Processing On Server Dcs 2018/07/20 End
            switch (mode)
            {
                case ControlMode.Spreadsheet:
                    {
                        var ctrl = new UserControl_SpreadsheetTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.Online:
                    {
                        var ctrl = new UserControl_OnlineTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.Bulky:
                    {
                        var ctrl = new UserControl_BulkyEdit(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.Emergency:
                    {
                        var ctrl = new UserControl_EmergencyTemplate();
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.Dac:
                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                case ControlMode.MultiDac:
                // Wang Issue NO.687 2018/05/18 End
                    {
                        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/20 Start
                        this.grid_right_all.RowDefinitions[0].Height = new GridLength(1, GridUnitType.Star);
                        this.grid_right_all.RowDefinitions[1].Height = new GridLength(0);
                        this.grid_right_all.RowDefinitions[2].Height = new GridLength(0);
                        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/20 End
                        // Wang Issue NO.687 2018/05/18 Start
                        // 1.階層DAC追加(オプションテンプレート表示統一)
                        // 2.DAC Excel メニュー整理
                        // Wang Issue DAC-96 20190320 Start
                        //var ctrl = new UserControl_DACTemplate(this.isFirstFile, this.excelFilepath);
                        var ctrl = this.createDacParam == null ?
                            new UserControl_DACTemplate(mode, this.isFirstFile, this.excelFilepath) :
                            new UserControl_DACTemplate(mode, this.createDacParam, this.isFirstFile);
                        // Wang Issue DAC-96 20190320 End
                        // Wang Issue NO.687 2018/05/18 End
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.LeadTime:
                    {
                        var ctrl = new UserControl_LeadTimeTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                    }
                    break;
                case ControlMode.ChimneyChart:
                    {
                        var ctrl = new UserControl_ChimneyChartTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.StockTime:
                    {
                        var ctrl = new UserControl_StockTimeTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.FIFO:
                    {
                        var ctrl = new UserControl_FIFOTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.Frequency:
                    {
                        var ctrl = new UserControl_FrequencyTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.ContinuousGraph:
                    {
                        var ctrl = new UserControl_ContinuousGraphTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.StreamGraph:
                    {
                        var ctrl = new UserControl_StreamGraphTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.Moment:
                    {
                        var ctrl = new UserControl_MomentTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.Comment:
                    {
                        var ctrl = new UserControl_Comment(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.StatusMonitor:
                    {
                        var ctrl = new UserControl_StatusMonitor(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.MultiStatusMonitor:
                    {
                        var ctrl = new UserControl_MultiStatusMonitor(this.isFirstFile, this.excelFilepath);
                        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/20 Start
                        this.grid_right_all.RowDefinitions[0].Height = new GridLength(1, GridUnitType.Star);
                        this.grid_right_all.RowDefinitions[1].Height = new GridLength(0);
                        this.grid_right_all.RowDefinitions[2].Height = new GridLength(0);
                        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/20 End
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.ProjectStatusMonitor:
                    {
                        var ctrl = new UserControl_ProjectStatusMonitor(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.Torque:
                    {
                        var ctrl = new UserControl_TorqueTemplate(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                case ControlMode.PastStockTime:
                    {
                        var ctrl = new UserControl_PastStockTime(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        break;
                    }
                //AIS_WorkPlace
                case ControlMode.WorkPlace:
                    {
                        var ctrl = new UserControl_WorkPlace(this.isFirstFile, this.excelFilepath);
                        this.grid_Main.Visibility = Visibility.Visible;
                        this.grid_Main.Children.Add(ctrl);
                        this.currentEditCtrl = ctrl;
                        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/20 Start
                        // showEditElementPanel();
                        showEditElementPanel(mode,ctrl);
                        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/20 End
                        break;
                    }
                default:
                    break;
            }
        }

        private void showEditElementPanel(ControlMode mode, UserControl_WorkPlace ctrl = null)
        {
            // FOA_サーバー化開発 Processing On Server Dcs 2018/07/20 Start
            // this.grid_Element.Visibility = Visibility.Visible;
            if (mode != ControlMode.MultiStatusMonitor && mode != ControlMode.MultiDac)
            {
                this.grid_Element.Visibility = Visibility.Visible;
            }
            // FOA_サーバー化開発 Processing On Server Dcs 2018/07/20 End
            this.ctmDetailsCtrl = new CtmDetails();
            if (mode.Equals(ControlMode.WorkPlace))
            {
                this.resultFolderFrame = new ResultFolderFrame(ctrl);
                this.grid_Element.Children.Add(this.resultFolderFrame);
            }
            else
            {
                this.grid_Element.Children.Add(this.ctmDetailsCtrl);
            }
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(MainWindow));

        //public WorkPlaceSearchParam GetSelectedWorkPlaceInfo()
        //{
        //    WorkPlaceSearchParam wpInfor = new WorkPlaceSearchParam()
        //    {
        //        workplaceId = "03A9634E93874BFBB85B9F75925571F0",
        //        start = 1538578800000,
        //        end = 1538641298298,
        //        displayName = "testwp001",
        //        elementInfoType = 1,
        //        missions = new List<string>() { "0653E18925A041539CD24068E4EF498F" }
        //    };
        //    return wpInfor;
        //}

        private void control_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void control_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void HelpViewer_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
			AisUtil.ShowHelp(wfh.Child,ControlMode.WorkPlace);
        }

        public void LoadinVisibleChange(bool hide)
        {
            //Foa.CommonClass.Control.LoadingAnimation loading = (Foa.CommonClass.Control.LoadingAnimation)System.Windows.Application.Current.MainWindow.FindName("Loading");
            FoaCore.Common.Control.LoadingAnimation loading = (FoaCore.Common.Control.LoadingAnimation)FindName("Loading");
            if (loading != null)
            {
                loading.Visibility = hide ? Visibility.Hidden : Visibility.Visible;
            }
        }

        private void treeView_Mission_CTM_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            logger.Debug("Here");
        }

        private void treeView_Mission_CTM_FocusableChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            logger.Debug("FocusChanged");
        }

        public void SetExternalFile(string fileInfo)
        {
            this.CtmDtailsCtrl.AddExternalFileData(fileInfo);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            //if(Application.Current.MainWindow.Equals(this))
            //    (Application.Current as App).Shutdown();
        }
    }
}
