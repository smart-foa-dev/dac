﻿using DAC.CtmData;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using CsvHelper;
using FoaCore.Common;
using FoaCore.Common.Entity;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using FoaCore.Net;
using Grip.Net;
using log4net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SpreadsheetGear;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;

using System.Net.Http;

namespace DAC.Model.Util
{
	public class AisUtil
	{
		public static readonly string TEMPLATE_DIR_PATH = System.IO.Path.Combine(System.Windows.Forms.Application.StartupPath, "template");

		private const int GET_STREAM_BUF_SIZE = 500;
		public const int MAX_BUFF_ROWS = 5000;
		public const String TIME_FORMAT = "yyyy/M/dd HH:mm:ss.fff";
		public const String RT_VIEW_FORMAT = "yyyy/m/d h:mm";

		/// <summary>
		/// 指定時刻表示フォーマット
		/// </summary>
		public const string DisplayTimeFormat = "yyyy/MM/dd HH:mm";

		/// <summary>
		/// DateTimePicker用フォーマット
		/// </summary>
		public const string DateTimeFormat = "yyyy/MM/dd HH:mm:ss";
		//ISSUE_NO.645 sunyi 2018/05/24 start
		//DataTableに膨大なデータを入るとエラーになるため、表示するデータの大きさを制限します。
		public static bool RowClickIsEnable = true;
		//ISSUE_NO.645 sunyi 2018/05/24 end

		private static readonly string CSV_EXTENSION = ".csv";


		/// <summary>
		/// CMS-UI起動
		/// </summary>
		/// <param name="id"</param>
		/// <param name="pass">パスワード</param>
		/// <param name="catalogLang">カタログ言語 (デフォルトは ja)</param>
		/// <param name="uiLang">UI言語 (デフォルトは ja)</param>
		public static void ActivateCmsUi(string id, string pass, string catalogLang = "ja", string uiLang = "ja")
		{
			// Browser を起動する。
			System.Diagnostics.Process.Start(String.Format("http://{}:{}/pathome", AisConf.Config.CmsHost, 80));
#if false
			var processStartInfo = new ProcessStartInfo();
			processStartInfo.FileName = Path.Combine(AisConf.Config.CmsUiFolderPath, "CMS-UI.exe");
			processStartInfo.WorkingDirectory = AisConf.Config.CmsUiFolderPath;
			processStartInfo.Arguments = string.Format("{0} {1} {2} {3} \"\" {4} {5}",
														id,
														pass,
														catalogLang,
														uiLang,
														AisConf.Config.CmsHost,
														AisConf.Config.CmsPort);
			processStartInfo.Arguments = "PAT " + processStartInfo.Arguments;
			try
			{
				if (File.Exists(processStartInfo.FileName))
				{
					using (Process process = new Process())
					{
						process.StartInfo = processStartInfo;
						process.Start();
					}
				}
				else
				{
					throw new Exception(string.Format(Properties.Message.AIS_E_035, processStartInfo.FileName));
				}
			}
			catch (Exception ex)
			{
				// Exception
				AisMessageBox.DisplayErrorMessageBox(ex.Message);
			}
#endif
		}


		/// <summary>
		/// GripR2起動
		/// </summary>
		/// <param name="id">ユーザID</param>
		/// <param name="pass">パスワード</param>
		/// <param name="catalogLang">カタログ言語 (デフォルトは ja)</param>
		/// <param name="uiLang">UI言語 (デフォルトは ja)</param>
		public static void ActivateGripR2(string id, string pass, string catalogLang = "ja", string uiLang = "ja")
		{
			// Browser を起動する。
			System.Diagnostics.Process.Start(String.Format("http://{}:{}/e2nhome", AisConf.Config.CmsHost, 80));
#if false
			var processStartInfo = new ProcessStartInfo();
			processStartInfo.FileName = Path.Combine(AisConf.Config.GripR2FolderPath, "R2.exe");
			processStartInfo.WorkingDirectory = AisConf.Config.GripR2FolderPath;
			processStartInfo.Arguments = string.Format("{0} {1} {2} {3}", id, pass, catalogLang, uiLang);
			processStartInfo.Arguments = "/e2 " + processStartInfo.Arguments;
			// System.Windows.MessageBox.Show("comand=" + processStartInfo.FileName + "arg=" + processStartInfo.Arguments);
			try
			{
				if (File.Exists(processStartInfo.FileName))
				{
					using (Process process = new Process())
					{
						process.StartInfo = processStartInfo;
						process.Start();
					}
				}
				else
				{
					throw new Exception(string.Format(Properties.Message.AIS_E_035, processStartInfo.FileName));
				}
			}
			catch (Exception ex)
			{
				// Exception
				AisMessageBox.DisplayErrorMessageBox(ex.Message);
			}
#endif
		}

		/// <summary>
		/// ヘルプファイル表示
		/// </summary>
		public static void ShowHelp(System.Windows.Forms.Control parent, ControlMode cntlMode = ControlMode.None)
		{

			// カルチャーの名前を取得
			string cultureName = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;

			// ヘルプファイル表示
			//if (cultureName == "ja-JP")
			//{
			//	string filePath = Path.Combine(AisConf.BaseDir, "Help", "AIS-HELP.chm");
			//	System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm");
			//}
			//else
			//{
			//	string filePath = Path.Combine(AisConf.BaseDir, "Help", "AIS-HELP_en.chm");
			//	System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP_en.chm");
			//}
            MainWindow mainWindow = Application.Current.MainWindow as MainWindow;
            if (mainWindow != null)
            {
                if (cntlMode == ControlMode.WorkPlace)
                {
                    cntlMode = mainWindow.CurrentMode;
                }
            }
			switch (cntlMode)
			{
				case ControlMode.MultiStatusMonitor:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "StandardTemplate5.html");
					break;
				case ControlMode.ContinuousGraph:
//                    System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "GraphTemplate6.html");
                    System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "StandardTemplate3.html");
					break;
				case ControlMode.Emergency:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "StandardTemplate4.html");
					break;
				case ControlMode.Online:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "StandardTemplate2.html");
					break;
				case ControlMode.StockTime:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "GraphTemplate3.html");
					break;
				case ControlMode.FIFO:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "GraphTemplate4.html");
					break;
				case ControlMode.Moment:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "GraphTemplate7.html");
					break;
				case ControlMode.PastStockTime:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "GraphTemplate9.html");
					break;
				case ControlMode.WorkPlace:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm");
					break;
				case ControlMode.MultiDac:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/DAC-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "DAC2_3.html");
					break;
				case ControlMode.Spreadsheet:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "StandardTemplate1.html");
					break;
				case ControlMode.Comment:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "GraphTemplate8.html");
					break;
				case ControlMode.Frequency:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "GraphTemplate5.html");
					break;
				case ControlMode.LeadTime:
                    System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "GraphTemplate1.html");
					break;
                case ControlMode.Dac:
                    System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/DAC-HELP.chm", System.Windows.Forms.HelpNavigator.Topic, "Dac1.html");
                    break;
				default:
					System.Windows.Forms.Help.ShowHelp(parent, System.Windows.Forms.Application.StartupPath + "/Help/AIS-HELP.chm");
					break;
			}

		}

		public static AhInfo GetAhInfo(string ctmId)
		{
			AhInfo ahInfo = null;

			string url = string.Format("{0}{1}?id={2}", CmsUrl.GetMibBaseUrl(), "ctm/ahs", ctmId);
			//WebClient client = new WebClient();
			WebClient client = getProxyWebClient();
			string result = client.DownloadString(url);
			List<AhInfo> ahList = FoaCore.Common.Util.Json.JsonUtil.Deserialize<List<AhInfo>>(result);
			if (ahList != null && ahList.Count > 0)
			{
				ahInfo = ahList[0];
			}
			else
			{
				return null;
			}

			return ahInfo;
		}

		public static async Task<AhInfo> GetAhInfoAsync(string ctmId)
		{
			AhInfo ahInfo = null;

			string url = string.Format("{0}{1}?id={2}", CmsUrl.GetMibBaseUrl(), "ctm/ahs", ctmId);
			FoaCore.Net.FoaHttpClient client = new FoaCore.Net.FoaHttpClient();
			string result = await client.Get(url);
			List<AhInfo> ahList = FoaCore.Common.Util.Json.JsonUtil.Deserialize<List<AhInfo>>(result);
			if (ahList != null && ahList.Count > 0)
			{
				ahInfo = ahList[0];
			}
			else
			{
				return null;
			}

			return ahInfo;
		}

		// Wang Issue MULTITEST-11 20190119 Start
		//public static async Task<string> GetMission2(Mission mission, DateTime? start, DateTime? end, CancellationToken token)
		public static async Task<string> GetMission2(Mission mission, DateTime? start, DateTime? end, CancellationToken token, BaseControl bc)
		// Wang Issue MULTITEST-11 20190119 End
		{
			long startUnix = UnixTime.ToLong(start);
			long endUnix = UnixTime.ToLong(end);

			// Wang Issue MULTITEST-11 20190119 Start
			//return await new CtmDataRetriever().GetProgramMissionResultCsvAsync(mission.Id, startUnix, endUnix, token);
			return await new CtmDataRetriever().GetProgramMissionResultCsvAsync(mission.Id, startUnix, endUnix, token, bc);
			// Wang Issue MULTITEST-11 20190119 End
		}
		//Dac-86 sunyi 20181227 start
		//public static async Task<string> GetMission2(string missionId, DateTime? start, DateTime? end, CancellationToken token)
		// Wang Issue MULTITEST-11 20190119 Start
		//public static async Task<string> GetMission2(string missionId, DateTime? start, DateTime? end, CancellationToken token, EventHandler handler = null)
		public static async Task<string> GetMission2(string missionId, DateTime? start, DateTime? end, CancellationToken token, BaseControl bc, EventHandler handler = null, Dictionary<string, string> UUIDAndMissionIds = null)
		// Wang Issue MULTITEST-11 20190119 End
		//Dac-86 sunyi 20181227 end
		{
			long startUnix = UnixTime.ToLong(start);
			long endUnix = UnixTime.ToLong(end);
			//Dac-86 sunyi 20181227 start
			//return await new CtmDataRetriever().GetProgramMissionResultCsvAsync(missionId, startUnix, endUnix, token);
			// Wang Issue MULTITEST-11 20190119 Start
			//return await new CtmDataRetriever().GetProgramMissionResultCsvAsync(missionId, startUnix, endUnix, token, handler);
			return await new CtmDataRetriever().GetProgramMissionResultCsvAsync(missionId, startUnix, endUnix, token, bc, handler, UUIDAndMissionIds);
			// Wang Issue MULTITEST-11 20190119 End
			//Dac-86 sunyi 20181227 end
		}



		// Wang Issue MULTITEST-11 20190119 Start
		//public static async Task<string> GetCtmDirectResult2(List<CtmObject> findCtmElementMap, DateTime start, DateTime end, CancellationToken ctoken)
		public static async Task<string> GetCtmDirectResult2(List<CtmObject> findCtmElementMap, DateTime start, DateTime end, CancellationToken ctoken, BaseControl bc)
		// Wang Issue MULTITEST-11 20190119 End
		{
			long startUnix = UnixTime.ToLong(start);
			long endUnix = UnixTime.ToLong(end);

			// Wang Issue MULTITEST-11 20190119 Start
			//return await new CtmDataRetriever().GetCtmDirectResultCsvAsync(findCtmElementMap, startUnix, endUnix, ctoken);
			return await new CtmDataRetriever().GetCtmDirectResultCsvAsync(findCtmElementMap, startUnix, endUnix, ctoken, bc);
			// Wang Issue MULTITEST-11 20190119 End
		}





		/// <summary>
		/// 選択されているミッションを取得
		/// </summary>
		/// <param name="ctrlMt">ミッションツリーコントローラ</param>
		/// <returns>選択されているミッション</returns>
		public static Mission GetSelectedMission(MissionTreeController ctrlMt)
		{
			if (!ctrlMt.isMissionSelected())
			{
				return null;
			}
			var selectedItem = ctrlMt.tree.SelectedItem as TreeViewItem;
			var selectedHeader = selectedItem.Header as StackPanel;
			var json = selectedHeader.DataContext.ToString();
			return JsonConvert.DeserializeObject<Mission>(json);
		}

#region ミッション結果取得

		/// <summary>
		/// プログラムミッションの結果を出力(保存)
		/// </summary>
		/// <param name="host">サーバのIPアドレス</param>
		/// <param name="port">サーバのポート番号</param>
		/// <param name="missionId">ミッションID</param>
		/// <param name="start">収集開始日時</param>
		/// <param name="end">収集終了日時</param>
		/// <rereturns>出力先ファイルの絶対パス</rereturns>
		public static string WriteProgramMissionResultJson(string host, string port, string missionId, long start, long end)
		{
			string limit = "0";
			string lang = "ja";
			DateTime dt = DateTime.Now;

			string url = String.Format("http://{0}:{1}/cms/rest/mib/mission/pm?id={2}&start={3}&end={4}&limit={5}&lang={6}",
					   host, port, missionId, start, end, limit, lang);
			Console.WriteLine("Request URL: {0}", url);

			string filePath = string.Empty;
			WebClient client = null;

			try
			{
				if (Directory.Exists(System.Windows.Forms.Application.StartupPath + @"\tmp") == false)
				{
					Directory.CreateDirectory(System.Windows.Forms.Application.StartupPath + @"\tmp");
				}

				filePath = System.Windows.Forms.Application.StartupPath + @"\tmp\" + dt.ToString("yyyyMMddHHmmssfff") + ".json";
				//client = new WebClient();
				client = getProxyWebClient();
				client.DownloadFile(url, filePath);
			}
			finally
			{
				if (client != null)
				{
					client.Dispose();
				}
			}

			return filePath;
		}


#endregion


#region CTM直接取得

		/// <summary>
		/// Find CTM
		/// </summary>
		private static string GetCtmDirectResultJson(Dictionary<string, List<string>> findCtmElementMap, long start, long end, Action<string> callback, CmsHttpClient client)
		{
			string filePath = string.Empty;

			// CTMを検索する為にPOSTするJSONを生成
			JArray postTokenArray = new JArray();
			List<string> ctmIdList = new List<string>();
			foreach (KeyValuePair<string, List<string>> ctmInfo in findCtmElementMap)
			{
				string ctmId = ctmInfo.Key;

				ctmIdList.Add(ctmId);
				JToken postToken = JToken.Parse("{}");
				postToken["ctmId"] = ctmId;
				postToken["start"] = start;
				postToken["end"] = end;
				postToken["findElementIds"] = string.Join(",", ctmInfo.Value.ToArray());

				postTokenArray.Add(postToken);
			}

			DateTime dt = DateTime.Now;
			filePath = System.Windows.Forms.Application.StartupPath + @"\tmp\" + dt.ToString("yyyyMMddHHmmssfff") + "_ctm.json";

			// FIND!!
			client.NotShowLoadingImage = true;
			client.LastInvokeCompleteHandler = true;
			client.CompleteHandler += resJson =>
			{
				callback(resJson);
			};

			string strPostJson = postTokenArray.ToString();
			if (string.IsNullOrEmpty(strPostJson) || strPostJson.Equals("[]"))
			{
				return null;
			}
			client.Post(CmsUrl.GetMibBaseUrl() + "mib/ctm/find/bulk", strPostJson);

			return filePath;
		}



#endregion

		public static string ConvertFromUtf8ToSjis(string src)
		{
			byte[] srcBytes = Encoding.UTF8.GetBytes(src);
			srcBytes = RemoveBom(srcBytes);

			byte[] dstBytes = Encoding.Convert(Utf8, ShiftJis, srcBytes);

			string dst = ShiftJis.GetString(dstBytes);
			return dst;
		}

		public static byte[] ConvertToWoUtf8Bytes(byte[] bulky, Encoding encoding)
		{
			byte[] dstBytes = Encoding.Convert(encoding, Utf8, bulky);

			return dstBytes;
		}

		public static string GetCatalogLang(Mission value)
		{
			string lang = LoginInfo.GetInstance().GetCatalogLang();
			string name = value.Name;
			if (!string.IsNullOrEmpty(value.DisplayName))
			{
				List<LangObject> displayNames = JsonConvert.DeserializeObject<List<LangObject>>(value.DisplayName);
				foreach (LangObject displayName in displayNames)
				{
					if (lang.Equals(displayName.Lang))
					{
						name = displayName.Text;
					}
				}
			}
			return name;
		}

		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
		/// <summary>
		/// 表示名称を取得
		/// </summary>
		/// <param name="JToken">トークンオブジェクト</param>
		/// <returns>表示名称</returns>
		public static string GetCatalogLangNew(JToken value)
		{
			string lang = LoginInfo.GetInstance().GetCatalogLang();
			string name = (string)value["name"];
			string strDisplayName = value["displayName"].ToString();
			if (!string.IsNullOrEmpty(strDisplayName))
			{
				List<LangObject> displayNames = JsonConvert.DeserializeObject<List<LangObject>>(strDisplayName);
				foreach (LangObject displayName in displayNames)
				{
					if (lang.Equals(displayName.Lang))
					{
						name = displayName.Text;
					}
				}
			}
			return name;
		}
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End

		public static string GetGripCatalogLang(GripMissionCtm value)
		{
			string lang = LoginInfo.GetInstance().GetCatalogLang();
			string name = value.Name;
			if (!string.IsNullOrEmpty(value.DisplayName))
			{
				List<LangObject> displayNames = JsonConvert.DeserializeObject<List<LangObject>>(value.DisplayName);
				foreach (LangObject displayName in displayNames)
				{
					if (lang.Equals(displayName.Lang))
					{
						name = displayName.Text;
					}
				}
			}
			return name;
		}

		public static void LoadProgressBarImage(Image img, bool isLoading)
		{
			var image = new BitmapImage();
			image.BeginInit();
			if (isLoading)
			{
				image.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar.gif", UriKind.RelativeOrAbsolute);
				image.EndInit();
				WpfAnimatedGif.ImageBehavior.SetAnimatedSource(img, image);
				WpfAnimatedGif.ImageBehavior.SetRepeatBehavior(img, new System.Windows.Media.Animation.RepeatBehavior(0));
				WpfAnimatedGif.ImageBehavior.SetRepeatBehavior(img, System.Windows.Media.Animation.RepeatBehavior.Forever);
			}
			else
			{
				image.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar-empty.png", UriKind.RelativeOrAbsolute);
				image.EndInit();
				WpfAnimatedGif.ImageBehavior.SetAnimatedSource(img, image);
			}
		}

		public static void WriteDataSheet(IWorksheet worksheet, ProgramMission resultCtm, int headerLength, DataTable dtWithoutUnit, List<CtmObject> myCtms)
		{
			SpreadsheetGear.IRange cells = worksheet.Cells;
			int columnIndex = 0;

			foreach (var ctm in resultCtm.ctms)
			{
				cells["A1"].Value = Keywords.RECEIVE_TIME;
				cells["B1"].Value = Keywords.CTM_NAME;

				// エレメント名
				int i = 2;



				foreach (string elementId in ctm.EL.Keys)
				{
					string elementName = string.Empty;
					string elementUnit = string.Empty;
					foreach (CtmObject originalCtm in myCtms)
					{
						foreach (var element in originalCtm.GetAllElements())
						{
							if (element.id.ToString() == elementId)
							{
								elementName = element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
								elementUnit = element.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
								break;
							}
						}
					}

					if (string.IsNullOrEmpty(elementName))
					{
						continue;
					}

					if (dtWithoutUnit != null)
					{
						// 不要なエレメントを削除
						if (!ContainsElementInDataTable(dtWithoutUnit, resultCtm.name, elementName))
						{
							continue;
						}
					}

					cells[0, i].Value = elementName;
					cells[1, i].Value = CtmElementType.GetNameByValue(int.Parse(ctm.EL[elementId].T)).ToUpper();
					cells[2, i].Value = elementUnit;
					i++;
				}

				// 1行出力
				List<string> row2 = new List<string>();
				row2.Add(UnixTime.ToDateTime(ctm.RT).ToString(TIME_FORMAT));
				cells[headerLength + columnIndex, 0].Value = UnixTime.ToDateTime(ctm.RT).ToString(TIME_FORMAT);
				row2.Add(resultCtm.name);
				cells[headerLength + columnIndex, 1].Value = resultCtm.name;
				int rowIndex = 2;
				foreach (string elementId in ctm.EL.Keys)
				{
					string elementName = string.Empty;
					foreach (CtmObject originalCtm in myCtms)
					{
						foreach (var element in originalCtm.GetAllElements())
						{
							if (element.id.ToString() == elementId)
							{
								elementName = element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true);
								break;
							}
						}
					}

					if (string.IsNullOrEmpty(elementName))
					{
						continue;
					}
					if (dtWithoutUnit != null)
					{
						// 不要なエレメントを削除
						if (!ContainsElementInDataTable(dtWithoutUnit, resultCtm.name, elementName))
						{
							continue;
						}
					}

					if (string.IsNullOrEmpty(ctm.EL[elementId].FN))
					{
						cells[headerLength + columnIndex, rowIndex].Value = string.IsNullOrEmpty(ctm.EL[elementId].V) ? string.Empty : ctm.EL[elementId].V;
					}
					else
					{
						cells[headerLength + columnIndex, rowIndex].Value = string.IsNullOrEmpty(ctm.EL[elementId].FN) ? string.Empty : ctm.EL[elementId].FN;
					}

					rowIndex++;
				}

				columnIndex++;
			}

		}

		/// <summary>
		/// DataTable内の「単位」行を削除
		/// </summary>
		/// <param name="dt">削除を実行するDataTable</param>
		public static void DeleteUnitRow(DataTable dt)
		{
			for (int i = 0; i < dt.Rows.Count; i++)
			{
				var row = (DataRow)dt.Rows[i];
				if (row[0].ToString() == Properties.Resources.CTM_UNIT)
				{
					dt.Rows[i].Delete();
					i--;
				}
			}
		}

		private static byte[] bom = Encoding.UTF8.GetPreamble();
		public static Encoding Utf8 = new UTF8Encoding(false);
		public static Encoding ShiftJis = Encoding.GetEncoding("Shift-JIS");

		/// <summary>
		/// UTF8でエンコードされている文字列からBOMを外す
		/// </summary>
		/// <param name="doc">文字列</param>
		/// <returns>BOM抜き文字列</returns>
		public static string RemoveBom(string doc)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(doc);
			if (bytes.Length < 3)
			{
				return string.Empty;
			}

			bool withBom = true;
			for (int i = 0; i < bom.Length; i++)
			{
				if (bom[i] == bytes[i])
				{
					continue;
				}

				withBom = false;
				break;
			}

			if (!withBom)
			{
				return doc;
			}

			byte[] encodedBytes = new byte[bytes.Length - 3];
			for (int i = 0; i < encodedBytes.Length; i++)
			{
				encodedBytes[i] = bytes[i + 3];
			}

			string encodedString = Encoding.UTF8.GetString(encodedBytes);
			return encodedString;
		}

		/// <summary>
		/// 文字列のバイナリからBOMを外す
		/// </summary>
		/// <param name="doc">BOMが付いているかもしれない文字列のバイナリデータ</param>
		/// <returns>BOM抜き文字列のバイナリデータ</returns>
		public static byte[] RemoveBom(byte[] src)
		{
			if (src.Length < 3)
			{
				return new byte[0];
			}

			bool withBom = true;
			for (int i = 0; i < bom.Length; i++)
			{
				if (bom[i] == src[i])
				{
					continue;
				}

				withBom = false;
				break;
			}

			if (!withBom)
			{
				return src;
			}

			byte[] encodedBytes = new byte[src.Length - 3];
			for (int i = 0; i < encodedBytes.Length; i++)
			{
				encodedBytes[i] = src[i + 3];
			}

			return encodedBytes;
		}

		public static bool IsBomUtf8(byte[] src)
		{
			if (src.Length < 3)
			{
				return false;
			}

			bool withBom = true;
			for (int i = 0; i < bom.Length; i++)
			{
				if (bom[i] == src[i])
				{
					continue;
				}

				withBom = false;
				break;
			}

			return withBom;
		}

		/// <summary>
		/// 文字コードを判別する
		/// </summary>
		/// <remarks>
		/// Jcode.pmのgetcodeメソッドを移植したものです。
		/// Jcode.pm(http://openlab.ring.gr.jp/Jcode/index-j.html)
		/// Jcode.pmの著作権情報
		/// Copyright 1999-2005 Dan Kogai <dankogai@dan.co.jp>
		/// This library is free software; you can redistribute it and/or modify it
		///  under the same terms as Perl itself.
		/// </remarks>
		/// <param name="bytes">文字コードを調べるデータ</param>
		/// <returns>適当と思われるEncodingオブジェクト。
		/// 判断できなかった時はnull。</returns>
		public static System.Text.Encoding GetCode(byte[] bytes)
		{
			const byte bEscape = 0x1B;
			const byte bAt = 0x40;
			const byte bDollar = 0x24;
			const byte bAnd = 0x26;
			const byte bOpen = 0x28;    //'('
			const byte bB = 0x42;
			const byte bD = 0x44;
			const byte bJ = 0x4A;
			const byte bI = 0x49;

			int len = bytes.Length;
			byte b1, b2, b3, b4;

			//Encode::is_utf8 は無視

			bool isBinary = false;
			for (int i = 0; i < len; i++)
			{
				b1 = bytes[i];
				if (b1 <= 0x06 || b1 == 0x7F || b1 == 0xFF)
				{
					//'binary'
					isBinary = true;
					if (b1 == 0x00 && i < len - 1 && bytes[i + 1] <= 0x7F)
					{
						//smells like raw unicode
						return System.Text.Encoding.Unicode;
					}
				}
			}
			if (isBinary)
			{
				return null;
			}

			//not Japanese
			bool notJapanese = true;
			for (int i = 0; i < len; i++)
			{
				b1 = bytes[i];
				if (b1 == bEscape || 0x80 <= b1)
				{
					notJapanese = false;
					break;
				}
			}
			if (notJapanese)
			{
				return System.Text.Encoding.ASCII;
			}

			for (int i = 0; i < len - 2; i++)
			{
				b1 = bytes[i];
				b2 = bytes[i + 1];
				b3 = bytes[i + 2];

				if (b1 == bEscape)
				{
					if (b2 == bDollar && b3 == bAt)
					{
						//JIS_0208 1978
						//JIS
						return System.Text.Encoding.GetEncoding(50220);
					}
					else if (b2 == bDollar && b3 == bB)
					{
						//JIS_0208 1983
						//JIS
						return System.Text.Encoding.GetEncoding(50220);
					}
					else if (b2 == bOpen && (b3 == bB || b3 == bJ))
					{
						//JIS_ASC
						//JIS
						return System.Text.Encoding.GetEncoding(50220);
					}
					else if (b2 == bOpen && b3 == bI)
					{
						//JIS_KANA
						//JIS
						return System.Text.Encoding.GetEncoding(50220);
					}
					if (i < len - 3)
					{
						b4 = bytes[i + 3];
						if (b2 == bDollar && b3 == bOpen && b4 == bD)
						{
							//JIS_0212
							//JIS
							return System.Text.Encoding.GetEncoding(50220);
						}
						if (i < len - 5 &&
							b2 == bAnd && b3 == bAt && b4 == bEscape &&
							bytes[i + 4] == bDollar && bytes[i + 5] == bB)
						{
							//JIS_0208 1990
							//JIS
							return System.Text.Encoding.GetEncoding(50220);
						}
					}
				}
			}

			//should be euc|sjis|utf8
			//use of (?:) by Hiroki Ohzaki <ohzaki@iod.ricoh.co.jp>
			int sjis = 0;
			int euc = 0;
			int utf8 = 0;
			for (int i = 0; i < len - 1; i++)
			{
				b1 = bytes[i];
				b2 = bytes[i + 1];
				if (((0x81 <= b1 && b1 <= 0x9F) || (0xE0 <= b1 && b1 <= 0xFC)) &&
					((0x40 <= b2 && b2 <= 0x7E) || (0x80 <= b2 && b2 <= 0xFC)))
				{
					//SJIS_C
					sjis += 2;
					i++;
				}
			}
			for (int i = 0; i < len - 1; i++)
			{
				b1 = bytes[i];
				b2 = bytes[i + 1];
				if (((0xA1 <= b1 && b1 <= 0xFE) && (0xA1 <= b2 && b2 <= 0xFE)) ||
					(b1 == 0x8E && (0xA1 <= b2 && b2 <= 0xDF)))
				{
					//EUC_C
					//EUC_KANA
					euc += 2;
					i++;
				}
				else if (i < len - 2)
				{
					b3 = bytes[i + 2];
					if (b1 == 0x8F && (0xA1 <= b2 && b2 <= 0xFE) &&
						(0xA1 <= b3 && b3 <= 0xFE))
					{
						//EUC_0212
						euc += 3;
						i += 2;
					}
				}
			}
			for (int i = 0; i < len - 1; i++)
			{
				b1 = bytes[i];
				b2 = bytes[i + 1];
				if ((0xC0 <= b1 && b1 <= 0xDF) && (0x80 <= b2 && b2 <= 0xBF))
				{
					//UTF8
					utf8 += 2;
					i++;
				}
				else if (i < len - 2)
				{
					b3 = bytes[i + 2];
					if ((0xE0 <= b1 && b1 <= 0xEF) && (0x80 <= b2 && b2 <= 0xBF) &&
						(0x80 <= b3 && b3 <= 0xBF))
					{
						//UTF8
						utf8 += 3;
						i += 2;
					}
				}
			}
			//M. Takahashi's suggestion
			//utf8 += utf8 / 2;

			//System.Diagnostics.Debug.WriteLine(
			//    string.Format("sjis = {0}, euc = {1}, utf8 = {2}", sjis, euc, utf8));
			if (euc > sjis && euc > utf8)
			{
				//EUC
				return System.Text.Encoding.GetEncoding(51932);
			}
			else if (sjis > euc && sjis > utf8)
			{
				//SJIS
				return System.Text.Encoding.GetEncoding(932);
			}
			else if (utf8 > euc && utf8 > sjis)
			{
				//UTF8
				return System.Text.Encoding.UTF8;
			}

			return null;
		}

		public static Mission GetMissionFromId(string missionId)
		{
			try
			{
				//WebClient client = new WebClient();
				WebClient client = getProxyWebClient();
				client.Encoding = Encoding.UTF8;
				string url = string.Format("{0}?id={1}", CmsUrl.GetMissionUrl(), missionId);
				string res = client.DownloadString(url);
				Mission mission = JsonConvert.DeserializeObject<Mission>(res);

				return mission;
			}
			catch (WebException e)
			{
				logger.Debug(e.Message);
				throw new WebException(Properties.Message.AIS_E_036);
			}
		}

		public static GripMissionCtm GetGripMissionFromId(string missionId)
		{
			//WebClient client = new WebClient();
			WebClient client = getProxyWebClient();
			client.Encoding = Encoding.UTF8;
			string url = string.Format("{0}?id={1}", GripUrl.Mission.Base(), missionId);
			string res = client.DownloadString(url);
			GripMissionCtm mission = JsonConvert.DeserializeObject<GripMissionCtm>(res);

			return mission;
		}

		/// <summary>
		/// 指定範囲のセルに格子状の罫線を書く
		/// </summary>
		/// <param name="worksheet">シート</param>
		/// <param name="range">範囲</param>
		public static void WriteBorder_SSG(SpreadsheetGear.IWorksheet worksheet, SpreadsheetGear.IRange range)
		{
			SpreadsheetGear.IBorder top = range.Borders[SpreadsheetGear.BordersIndex.EdgeTop];
			top.LineStyle = SpreadsheetGear.LineStyle.Continuous;
			SpreadsheetGear.IBorder bottom = range.Borders[SpreadsheetGear.BordersIndex.EdgeBottom];
			bottom.LineStyle = SpreadsheetGear.LineStyle.Continuous;
			SpreadsheetGear.IBorder left = range.Borders[SpreadsheetGear.BordersIndex.EdgeLeft];
			left.LineStyle = SpreadsheetGear.LineStyle.Continuous;
			SpreadsheetGear.IBorder right = range.Borders[SpreadsheetGear.BordersIndex.EdgeRight];
			right.LineStyle = SpreadsheetGear.LineStyle.Continuous;
			SpreadsheetGear.IBorder horizontal = range.Borders[SpreadsheetGear.BordersIndex.InsideHorizontal];
			horizontal.LineStyle = SpreadsheetGear.LineStyle.Continuous;
			SpreadsheetGear.IBorder vertical = range.Borders[SpreadsheetGear.BordersIndex.InsideVertical];
			vertical.LineStyle = SpreadsheetGear.LineStyle.Continuous;
		}

		/// <summary>
		/// 指定範囲のセルに格子状の罫線を書く
		/// </summary>
		/// <param name="worksheet">シート</param>
		/// <param name="range">範囲</param>
		public static void WriteBorder_Interop(Microsoft.Office.Interop.Excel.Worksheet worksheet, Microsoft.Office.Interop.Excel.Range range)
		{
			range.Borders.get_Item(Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
			range.Borders.get_Item(Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
			range.Borders.get_Item(Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
			range.Borders.get_Item(Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
			range.Borders.get_Item(Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
			range.Borders.get_Item(Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical).LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
		}



		/// <summary>
		/// DataTable内にエレメントが存在するか検索
		/// </summary>
		/// <param name="dt">検索対象のDataTable</param>
		/// <param name="ctmName">検索するエレメントが存在するCTMの名前</param>
		/// <param name="elementName">検索するエレメントの名前</param>
		/// <returns></returns>
		private static bool ContainsElementInDataTable(DataTable dt, string ctmName, string elementName)
		{
			// 不要なエレメントを削除
			foreach (var rowObject in dt.Rows)
			{
				var row = (DataRow)rowObject;

				// DataTableからCTM名で列を特定
				if (row.ItemArray[0].ToString() == ctmName)
				{
					foreach (var item in row.ItemArray)
					{
						// エレメント名で検索
						if (item.ToString() == elementName)
						{
							return true;
						}
					}
				}
			}

			return false;
		}

		/// <summary>
		/// DataTable内にエレメントが存在するか検索
		/// </summary>
		/// <param name="dt">検索対象のDataTable</param>
		/// <param name="elementName">検索するエレメントの名前</param>
		/// <returns></returns>
		private static bool containsElementInDataRow(DataRow row, string elementName)
		{
			foreach (var item in row.ItemArray)
			{
				// エレメント名で検索
				if (item.ToString() == elementName)
				{
					return true;
				}
			}

			return false;
		}

		public static int GetPositionInDataRow(DataRow row, string elementName)
		{
			int pos = 0;
			var array = row.ItemArray;
			int idx = 1;

			for (int i = idx; i < array.Length; i++)
			{
				var val = array[i] as string;
				if (string.IsNullOrEmpty(val))
				{
					continue;
				}


				if (array[i].ToString() == elementName)
				{
					return pos;  // 最初のカラムはCTM名
				}
				pos++;
			}

			return -1;
		}





		public static int GetPositionInDataRowForAllCtms(DataRow row, string elementName)
		{
			int pos = 0;
			var array = row.ItemArray;
			int idx = 1; // 最初のカラムはCTM名
			for (int i = idx; i < array.Length; i++)
			{
				var val = array[i] as string;
				if (val != null && val == elementName)
				{
					return pos;
				}
				pos++;
			}

			return -1;
		}

		/// <summary>
		/// DataTableの内容をエクセルに出力
		/// </summary>
		/// <param name="sheet">出力するシート</param>
		/// <param name="dt">DataTable</param>
		/// <param name="rowNumber">出力を開始する座標の縦インデックス、0基準</param>
		/// <param name="columnNumber">出力を開始する座標の横インデックス、0基準</param>
		/// <returns></returns>
		public static Microsoft.Office.Interop.Excel.Worksheet WriteDataTableToExcel(Microsoft.Office.Interop.Excel.Worksheet sheet, DataTable dt, int rowNumber, int columnNumber)
		{
			// データテーブルの列ごとでExcelに出力
			for (int col = 0; col < dt.Columns.Count; col++)
			{
				// +1は項目名の行
				object[,] obj = new object[dt.Rows.Count + 1, 1];

				// 項目名出力
				obj[0, 0] = dt.Columns[col].ColumnName;

				for (int row = 0; row < dt.Rows.Count; row++)
				{
					// データテーブルをobject配列に格納
					obj[row + 1, 0] = dt.Rows[row][col].ToString();
				}

				Microsoft.Office.Interop.Excel.Range rgn = sheet.Range[sheet.Cells[rowNumber + 1, columnNumber + col + 1],
					sheet.Cells[rowNumber + dt.Rows.Count + 1, columnNumber + col + 1]];

				DataColumn dtcol = dt.Columns[col];
				rgn.NumberFormatLocal = "@";
				rgn.Value2 = obj;
			}

			return sheet;
		}

		/// <summary>
		/// データが存在するヘッダー抜き行数を返す、1列目のセルが空白になったらそこでデータが存在しないと判定する
		/// </summary>
		/// <param name="sheet">シート</param>
		/// <param name="firstRowIndex">行数を数え始めるヘッダー行のインデックス、1ベース</param>
		/// <param name="firstColumnIndex">行数を数え始めるセルの列インデックス、1ベース</param>
		/// <returns>データが存在するヘッダー抜き行数</returns>
		public static int GetRowLength(Microsoft.Office.Interop.Excel.Worksheet sheet, int firstRowIndex, int firstColumnIndex)
		{
			int rowLength = 0;

			for (int i = 0; i < 100; i++)
			{
				Microsoft.Office.Interop.Excel.Range range = sheet.Cells[firstRowIndex + i + 1, firstColumnIndex];
				if (range.Value2 != null)
				{
					continue;
				}

				rowLength = i;
				break;
			}

			return rowLength;
		}

		/// <summary>
		/// データが存在するヘッダー抜き行数を返す、1列目のセルが空白になったらそこでデータが存在しないと判定する
		/// </summary>
		/// <param name="sheet">シート</param>
		/// <param name="firstRowIndex">行数を数え始めるヘッダー行のインデックス、1ベース</param>
		/// <param name="firstColumnIndex">行数を数え始めるセルの列インデックス、1ベース</param>
		/// <returns>データが存在するヘッダー抜き行数</returns>
		public static int GetRowLength_SSG(SpreadsheetGear.IWorksheet sheet, int firstRowIndex, int firstColumnIndex)
		{
			int rowLength = 0;

			for (int i = 0; i < 100; i++)
			{
				SpreadsheetGear.IRange range = sheet.Cells[firstRowIndex + i + 1, firstColumnIndex];
				if (range.Value != null)
				{
					continue;
				}

				rowLength = i;
				break;
			}

			return rowLength;
		}

		/// <summary>
		/// データが存在する列数を返す、1列目のセルが空白になったらそこでデータが存在しないと判定する
		/// </summary>
		/// <param name="sheet">シート</param>
		/// <param name="firstRowIndex">列数を数え始めるヘッダー行のインデックス、1ベース</param>
		/// <param name="firstColumnIndex">列数を数え始める列のインデックス、1ベース</param>
		/// <returns>データが存在する列数</returns>
		public static int GetColumnLength(Microsoft.Office.Interop.Excel.Worksheet sheet, int firstRowIndex, int firstColumnIndex)
		{
			int columnLength = 0;

			for (int i = 0; i < 300; i++)
			{
				Microsoft.Office.Interop.Excel.Range range = sheet.Cells[firstRowIndex, firstColumnIndex + i];
				if (range.Value2 != null)
				{
					continue;
				}

				columnLength = i;
				break;
			}

			return columnLength;
		}

		/// <summary>
		/// データが存在する列数を返す、1列目のセルが空白になったらそこでデータが存在しないと判定する
		/// </summary>
		/// <param name="sheet">シート</param>
		/// <param name="firstRowIndex">列数を数え始めるヘッダー行のインデックス、1ベース</param>
		/// <param name="firstColumnIndex">列数を数え始める列のインデックス、1ベース</param>
		/// <returns>データが存在する列数</returns>
		public static int GetColumnLength_SSG(SpreadsheetGear.IWorksheet sheet, int firstRowIndex, int firstColumnIndex)
		{
			int columnLength = 0;

			for (int i = 0; i < 300; i++)
			{
				SpreadsheetGear.IRange range = sheet.Cells[firstRowIndex, firstColumnIndex + i];
				if (range.Value != null)
				{
					continue;
				}

				columnLength = i;
				break;
			}

			return columnLength;
		}

		/// <summary>
		/// エクセルのテーブルからDataTableを作成
		/// </summary>
		/// <param name="sheet">シート</param>
		/// <param name="firstRowNumber">DataTableにしたい範囲の最初の列(ヘッダー部分), 1ベース</param>
		/// <param name="firstColumnNumber">DataTableにしたい範囲の最初の行, 1ベース</param>
		/// <param name="rowLength">行数(ヘッダー抜き)</param>
		/// <param name="columnLength">列数</param>
		/// <returns>指定された範囲から生成されたDataTable</returns>
		public static DataTable CreateDataTableFromExcel(Microsoft.Office.Interop.Excel.Worksheet sheet,
			int firstRowNumber, int firstColumnNumber, int rowLength, int columnLength)
		{
			DataTable dt = new DataTable();

			for (int i = 0; i < columnLength; i++)
			{
				//カラム名にダミーを設定
				dt.Columns.Add("dummy_" + i.ToString());
			}

			for (int col = 0; col < columnLength; col++)
			{
				Microsoft.Office.Interop.Excel.Range range = sheet.Cells[firstRowNumber, col + firstColumnNumber];
				dt.Columns[col].ColumnName = range.Value2;
			}

			Microsoft.Office.Interop.Excel.Range leftTop = sheet.Cells[firstRowNumber + 1, firstColumnNumber];
			Microsoft.Office.Interop.Excel.Range rightBottom = sheet.Cells[firstRowNumber + rowLength, firstColumnNumber + columnLength];
			Microsoft.Office.Interop.Excel.Range dataRange = sheet.Range[leftTop, rightBottom];
			for (int row = 1; row <= rowLength; row++)
			{
				DataRow dr = dt.NewRow();
				for (int col = 1; col <= columnLength; col++)
				{
					dr[dt.Columns[col - 1].ColumnName] = dataRange[row, col].Value2;
				}

				dt.Rows.Add(dr);
			}

			return dt;
		}

		/// <summary>
		/// エクセルのテーブルからDataTableを作成
		/// </summary>
		/// <param name="sheet">シート</param>
		/// <param name="firstRowNumber">DataTableにしたい範囲の最初の列(ヘッダー部分), 1ベース</param>
		/// <param name="firstColumnNumber">DataTableにしたい範囲の最初の行, 1ベース</param>
		/// <param name="rowLength">行数(ヘッダー抜き)</param>
		/// <param name="columnLength">列数</param>
		/// <returns>指定された範囲から生成されたDataTable</returns>
		public static DataTable CreateDataTableFromExcel_SSG(SpreadsheetGear.IWorksheet sheet,
			int firstRowNumber, int firstColumnNumber, int rowLength, int columnLength)
		{
			DataTable dt = new DataTable();

			for (int i = 0; i < columnLength; i++)
			{
				//カラム名にダミーを設定
				dt.Columns.Add("dummy_" + i.ToString());
			}

			for (int col = 0; col < columnLength; col++)
			{
				SpreadsheetGear.IRange range = sheet.Cells[firstRowNumber, col + firstColumnNumber];
				dt.Columns[col].ColumnName = range.Text;
			}


			for (int row = 1; row <= rowLength; row++)
			{
				DataRow dr = dt.NewRow();
				for (int col = 0; col < columnLength; col++)
				{
					SpreadsheetGear.IRange data = sheet.Cells[firstRowNumber + row, firstColumnNumber + col];
					dr[dt.Columns[col].ColumnName] = data.Value;
				}

				dt.Rows.Add(dr);
			}

			return dt;
		}

		public static DataTable DeepCopyDataTable(DataTable targetTable)
		{
			DataTable copyTable = null;
			System.IO.MemoryStream stream = new System.IO.MemoryStream();

			// コピー元オブジェクトをシリアライズ
			BinaryFormatter formatter = new BinaryFormatter();
			formatter.Serialize(stream, targetTable);
			stream.Position = 0;

			// シリアライズデータをコピー先オブジェクトにデシリアライズ
			copyTable = (DataTable)formatter.Deserialize(stream);

			return copyTable;
		}

		public static DataTable DeleteIdRow(DataTable dt)
		{
			for (int i = dt.Rows.Count - 1; 0 <= i; i--)
			{
				if (i % 2 == 0)
				{
					continue;
				}

				dt.Rows[i].Delete();
			}

			return dt;
		}

		public static DataTable DeleteNameRow(DataTable dt)
		{
			for (int i = dt.Rows.Count - 1; 0 <= i; i--)
			{
				if (i % 2 == 1)
				{
					continue;
				}

				dt.Rows[i].Delete();
			}

			return dt;
		}

		/// <summary>
		/// シート名からシートを取得、該当するシートが存在しない場合は新規シートをブックに追加して取得
		/// </summary>
		/// <param name="book">シートが存在するブック</param>
		/// <param name="sheetName">シート名</param>
		/// <returns>該当するシート</returns>
		public static Microsoft.Office.Interop.Excel.Worksheet GetSheetFromSheetName_Interop(Microsoft.Office.Interop.Excel.Workbook book, string sheetName)
		{
			Microsoft.Office.Interop.Excel.Worksheet sheet = null;

			foreach (Microsoft.Office.Interop.Excel.Worksheet s in book.Sheets)
			{
				if (s.Name != sheetName)
				{
					continue;
				}

				sheet = s;
				break;
			}

			// 見つからなかった場合はシートを追加
			if (sheet == null)
			{
				book.Sheets.Add(Type.Missing, book.Sheets[book.Sheets.Count]);
				sheet = book.Sheets[book.Sheets.Count];
				sheet.Name = sheetName;
			}

			return sheet;
		}

		/// <summary>
		/// シート名からシートを取得、該当するシートが存在しない場合は新規シートをブックに追加して取得
		/// </summary>
		/// <param name="book">シートが存在するブック</param>
		/// <param name="sheetName">シート名</param>
		/// <returns>該当するシート</returns>
		public static SpreadsheetGear.IWorksheet GetSheetFromSheetName_SSG(SpreadsheetGear.IWorkbook book, string sheetName)
		{
			SpreadsheetGear.IWorksheet sheet = null;

			foreach (SpreadsheetGear.IWorksheet s in book.Worksheets)
			{
				if (s.Name != sheetName)
				{
					continue;
				}

				sheet = s;
				break;
			}

			// 見つからなかった場合はシートを追加
			if (sheet == null)
			{
				book.Worksheets.Add();
				sheet = book.Worksheets[book.Worksheets.Count - 1];
				sheet.Name = sheetName;
			}

			return sheet;
		}

		public static Dictionary<string, int> CountElementColumn(DataTable dt)
		{
			var dic = new Dictionary<string, int>();

			foreach (var columnObject in dt.Columns)
			{
				DataColumn column = columnObject as DataColumn;
				string columnName = column.ColumnName;
				if (columnName == Keywords.CTM_NAME)
				{
					continue;
				}
				string elementName = columnName.Substring(0, columnName.Length - 3);
				if (dic.ContainsKey(elementName))
				{
					dic[elementName]++;
				}
				else
				{
					dic.Add(elementName, 1);
				}
			}

			return dic;
		}

		public static List<CtmObject> GetCtmsFromCtmIds(List<string> ctmIds)
		{
			List<CtmObject> ctmList = new List<CtmObject>();

			string query = string.Empty;
			foreach (string ctmId in ctmIds)
			{
				query += string.Format("id={0}&", ctmId);
			}
			query.Trim('&');
			string url = string.Format("{0}?{1}", CmsUrlMms.Ctm.List(), query);

			WebClient client = null;

			try
			{
				//client = new WebClient();
				client = getProxyWebClient();
				client.Encoding = Encoding.UTF8;
				string response = client.DownloadString(url);

				ctmList = JsonConvert.DeserializeObject<List<CtmObject>>(response);
			}
			finally
			{
				if (client != null)
				{
					client.Dispose();
				}
			}

			return ctmList;
		}

		public static bool GetStartAndEndTime(DateTimePicker pickerStart, DateTimePicker pickerEnd,
			out DateTime start1, out DateTime end1)
		{
			start1 = DateTime.MinValue;
			end1 = DateTime.Now;

			DateTime? start = pickerStart.Value;
			DateTime? end = pickerEnd.Value;

			if (start == null || end == null)
			{
				FoaMessageBox.ShowError("AIS_E_021");
				return false;
			}

			if (end <= start)
			{
				FoaMessageBox.ShowError("AIS_E_019");
				return false;
			}

			start1 = start ?? DateTime.MinValue;
			end1 = end ?? DateTime.Now;

			return true;
		}

		/// <summary>
		/// Range取込
		/// </summary>
		/// <param name="srchRange"></param>
		/// <param name="strWhat"></param>
		/// <returns></returns>
		public static Microsoft.Office.Interop.Excel.Range GetFindRange_Interop(Microsoft.Office.Interop.Excel.Range srchRange, String strWhat)
		{
			return srchRange.Find(strWhat,
								Type.Missing,
								Microsoft.Office.Interop.Excel.XlFindLookIn.xlValues,
								Microsoft.Office.Interop.Excel.XlLookAt.xlWhole,
								Microsoft.Office.Interop.Excel.XlSearchOrder.xlByRows,
								Microsoft.Office.Interop.Excel.XlSearchDirection.xlNext,
								false,
								Type.Missing, Type.Missing);
		}

		/// <summary>
		/// ミッションを参照できるか
		/// </summary>
		/// <param name="missionId"></param>
		/// <param name="userId"></param>
		/// <returns></returns>
		public static bool CanReferenceMission(string missionId, string userId)
		{
			string query = string.Format("?missionId={0}&userId={1}", missionId, userId);
			string url = string.Format("{0}/canReference{1}", CmsUrl.GetMissionUrl(), query);

			try
			{
				//using (WebClient client = new WebClient())
				using (WebClient client = getProxyWebClient())
				{
					string result = client.DownloadString(url);
					if (result == "true")
					{
						return true;
					}

					return false;
				}
			}
			catch (Exception)
			{
				return true;
			}
		}

		/// <summary>
		/// ファイル名・ファイルパスから『.』を含む拡張子を取得する
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public static string GetExtention(string fileName)
		{
			string[] ary = fileName.Split('.');
			if (ary.Length < 1)
			{
				return string.Empty;
			}

			string ext = "." + ary[ary.Length - 1];
			return ext;
		}

		/// <summary>
		/// ファイル名・ファイルパスから『.』を含む拡張子を取り除く
		/// </summary>
		/// <param name="fileName"></param>
		/// <returns></returns>
		public static string RemoveExtention(string fileName)
		{
			string ext = GetExtention(fileName);

			string withoutExt = fileName.Substring(0, fileName.Length - ext.Length);
			return withoutExt;
		}

		/// <summary>
		/// ファイルパスからファイル名のみを取得する
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public static string GetFileNameFromPath(string filePath)
		{
			string[] ary = filePath.Split('\\');
			return ary[ary.Length - 1];
		}
		public static string GetParentFolderNameFromPath(string filePath)
		{
			string[] ary = filePath.Split('\\');
			return ary[ary.Length - 2];
		}

		private static readonly ILog logger = LogManager.GetLogger(typeof(AisUtil));

		public static HttpClient getProxyHttpClient()
		{
			HttpClient client = null;

			if (AisConf.Config.UseProxy == true)
			{

				HttpClientHandler clientHandler = new HttpClientHandler();
				clientHandler.Proxy = new WebProxy("http://" + AisConf.Config.ProxyURI);
				clientHandler.UseProxy = true;
				client = new HttpClient(clientHandler);
			}
			else
			{
				HttpClientHandler clientHandler = new HttpClientHandler();
				clientHandler.UseProxy = false;
				client = new HttpClient(clientHandler);
			}

			client.Timeout = TimeSpan.FromSeconds(100);

			return client;

		}

		public static WebClient getProxyWebClient()
		{
			WebClient client = new WebClient();


			if (AisConf.Config.UseProxy == true)
			{
				client.Proxy = new System.Net.WebProxy("http://" + AisConf.Config.ProxyURI);
			}
			else
			{
				client.Proxy = null;
			}

			return client;
		}


		public static bool IsCsvFile(string file)
		{
			return Path.GetExtension(file).Equals(CSV_EXTENSION, StringComparison.OrdinalIgnoreCase);
		}
	}
}
