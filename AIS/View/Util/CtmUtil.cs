﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAC.Model;

namespace DAC.Util
{
    class CtmUtil
    {
        public static CtmObject GetCtmObj(List<CtmObject> objs, string ctmId)
        {
            return objs.Find(x => x.id.ToString() == ctmId);
        }
    }
}
