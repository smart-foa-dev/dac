﻿using System.Collections.Generic;
using System.Data;
using System.Windows.Controls;
using System.Windows.Data;

namespace DAC.Util
{
    public class ElementSelectionManager
    {
        private DataTable dtOutput;
        private Dictionary<string, int> elementColumnCount;

        public ElementSelectionManager(DataTable dt, Dictionary<string, int> counter)
        {
            this.dtOutput = dt;
            this.elementColumnCount = counter;
        }

        public void deleteEmptyColumns(DataTable dt)
        {
            for (int i = 1; i < dt.Columns.Count; i++)
            {
                if (dt.Columns[i] == null)
                {
                    return;
                }

                bool canDelete = true;
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    if (!string.IsNullOrEmpty(dt.Rows[j][i].ToString()))
                    {
                        canDelete = false;
                        break;
                    }
                }

                if (canDelete)
                {
                    string key = dt.Columns[i].ColumnName;
                    key = key.Substring(0, key.Length - 3);
                    dt.Columns.RemoveAt(i);
                    this.elementColumnCount[key]--;
                    if (this.elementColumnCount[key] < 1)
                    {
                        this.elementColumnCount.Remove(key);
                    }
                    i--;
                }
            }
        }

        public void deleteColumn(DataTable dt, int index)
        {
            bool canDelete = true;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (!string.IsNullOrEmpty(dt.Rows[i][index].ToString()))
                {
                    canDelete = false;
                    break;
                }
            }

            if (canDelete)
            {
                string key = dt.Columns[index].ColumnName;
                key = key.Substring(0, key.Length - 3);
                dt.Columns.RemoveAt(index);
                this.elementColumnCount[key]--;
                if (this.elementColumnCount[key] < 1)
                {
                    this.elementColumnCount.Remove(key);
                }
            }
        }


        public void deleteElement(DataRow row, int selectedColumnIndex)
        {
            row[selectedColumnIndex] = string.Empty;

            bool isEmptyRow = true;
            for (int i = 1; i < row.ItemArray.Length; i++)
            {
                if (string.IsNullOrEmpty(row[i].ToString()))
                {
                    continue;
                }

                isEmptyRow = false;
                break;
            }

            if (isEmptyRow)
            {
                row.Delete();
            }
        }

        public int getAddedElementCount(DataRow row)
        {
            int cnt = 0;
            foreach (var item in row.ItemArray)
            {
                if (string.IsNullOrEmpty(item.ToString()))
                {
                    continue;
                }

                cnt++;
            }

            return cnt - 1;     // CTM NAMEの部分を引く
        }

        /// <summary>
        /// DataGridCellInfoからDataGridCellを取得
        /// </summary>
        /// <param name="dg">取得対象のDataGrid</param>
        /// <param name="info">取得したいセルのDataGridCellInfo</param>
        /// <returns>infoが示す位置のDataGridCell</returns>
        public DataGridCell getCellFromCellInfo(DataGrid dg, DataGridCellInfo info)
        {
            dg.CurrentCell = info;
            DataGridRow dataGridRow = (DataGridRow)dg.ItemContainerGenerator.ContainerFromItem(dg.CurrentCell.Item);
            DataGridCell cell = (DataGridCell)info.Column.GetCellContent(dataGridRow).Parent;

            return cell;
        }

        public bool TableIsEmpty
        {
            get;
            set;
        }

       
        public static void setColumn2(DataTable dt, DataGrid dataGrid)
        {
            dataGrid.Columns.Clear();
            int columnIndex = 0;
            foreach (var columnObject in dt.Columns)
            {
                DataColumn column = columnObject as DataColumn;
                DataGridTextColumn gridColumn = new DataGridTextColumn();
                string header = string.Empty;
                if (columnIndex == 0)
                {
                    header = Keywords.CTM_NAME;
                }
                else
                {
                    header = string.Format("Element {0}", columnIndex);
                }
                gridColumn.Header = header;
                Binding b = new Binding("[" + column.ColumnName + "]");
                gridColumn.Binding = b;
                dataGrid.Columns.Add(gridColumn);

                columnIndex++;
            }
        }
    }
}
