﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Util
{
    public class Keywords
    {
        public static readonly string RECEIVE_TIME = "RECEIVE TIME";
        public static readonly string CTM_NAME = "CTM NAME";
        public static readonly string CTM_ID = "CTM ID";
        public static readonly string RT = "RT";

        public static readonly string CACHE_PARAM_SHEET = "cache_param";
        public static readonly string PARAM_SHEET = "param";
        public static readonly string RO_PARAM_SHEET = "ro_param";

        //ISSUE_NO.790 sunyi 2018/07/18 Start
        public static readonly string SPREAD_SHEET = "集計テンプレート";
        public static readonly string ONLINE_SHEET = "オンラインテンプレート";
        //ISSUE_NO.790 sunyi 2018/07/18 End

        //dac home start
        public static readonly string PERIOD_TIME_SHEET = "業務時間";
        //dac home start
    }
}
