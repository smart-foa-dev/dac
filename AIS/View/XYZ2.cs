﻿using DAC.CtmData;
using DAC.Model;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;

namespace DAC.View
{
    public class XYZ
    {
        public string id;
        public string name;
        public CsvFile csvFile;
        public CtmObject ctmObj;
        public DataRow dtatRow;
        public CsvReader reader;
        public Dictionary<int, int> pos2Pos;

        public void Init()
        {
            reader = new CsvReader(new StreamReader(csvFile.Filepath, Encoding.UTF8));
        }

        public int CompareTo(XYZ other)
        {
            if (other == null)
            {
                return 1;
            }

            var myval = reader.CurrentRecord;
            var yourval = other.Current();

            return myval[0].CompareTo(yourval[0]);
        }

        public bool Read()
        {
           return reader.Read();
        }

        public string[] Current()
        {
            return reader.CurrentRecord;
        }

        public void Dispose() 
        {
            if (reader != null)
            {
                reader.Dispose();
            }
        }

        public string Name()
        {
            return name;
        }
    }

    public class MergeIterators : IDisposable
    {
        // 常にソート済
        private readonly LinkedList<XYZ> srcLst = new LinkedList<XYZ>();
        private XYZ currentRec;

        public MergeIterators(List<XYZ> srcList)
        {
            foreach (var src in srcList)
            {
                add(src);
            }
            currentRec = srcLst.First.Value;
        }

        private void add(XYZ newItem)
        {
            for (var recentNode = srcLst.First; recentNode != null; recentNode = recentNode.Next)
            {
                var item = recentNode.Value;
                if (newItem.CompareTo(item) > 0)
                {
                    srcLst.AddBefore(recentNode, newItem);
                    return;
                }
            }

            srcLst.AddLast(newItem);
        }

        public XYZ Current()
        {
            return currentRec;
        }

        public void Dispose()
        {
            foreach (var src in srcLst)
            {
                src.Dispose();
            }
        }

        public bool MoveNext()
        {
            if (currentRec.Read())
            {
                srcLst.RemoveFirst();
                add(currentRec);
                currentRec = srcLst.First.Value;

                return true;
            }
            else
            {
                srcLst.RemoveFirst();
                if (srcLst.Count > 0)
                {
                    currentRec = srcLst.First.Value;
                    return true;
                }

                return false;
            }
        }
    }
}
