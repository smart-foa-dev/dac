﻿using DAC.Model;
using System;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace DAC.View
{
    public class RowColorConverter : IValueConverter
    {
        public object Convert (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var et = value as EmergencyTimer;
            if (et != null)
            {
                // 終了時刻と現在を比較
                DateTime dtEnd = System.Convert.ToDateTime(et.EndTime);
                DateTime dtNow = DateTime.Now;
                if (dtEnd < dtNow)
                {
                    // 終了済み
                    return new SolidColorBrush(Colors.Black);
                }
                else
                {
                    // 稼働中
                    return new SolidColorBrush(Color.FromRgb(219, 61, 54));
                }
            }
            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack (object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException ();
        }
    }
}