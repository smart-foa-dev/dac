﻿using DAC.CtmData;
using DAC.Model;
using DAC.Model.Util;
using DAC.Search;
using DAC.Util;
using FoaCore;
using FoaCore.Net;
using FoaCore.Common.Converter;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using FoaCore.Common.Util.Json;
using Grip.Controller.GripMission;
using Grip.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Text;
using System.Net.Http;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using FoaCore.Model.GripR2.CtmInfo;
using FoaCore.Common;
using log4net;
using System.Reflection;



namespace DAC.View
{
    public class GripMissionHasNoElementException : Exception
    {

    }


    /// <summary>
    /// UserControl_ElementEdit.xaml の相互作用ロジック
    /// </summary>
    public partial class CtmDetails : UserControl
    {
        private const int GET_STREAM_BUF_SIZE = 500;

        private List<MissionCtm> missionCtms = new List<MissionCtm>();
        private CtmObject selectedCtm = new CtmObject();

        private int conditionPattern = 0;
        private List<ElementConditionData> conditionList = new List<ElementConditionData>();
        private Dictionary<ElementConditionData, string> elementCondition = new Dictionary<ElementConditionData, string>();
        //ISSUE_NO.740 Sunyi 2018/07/10 Start
        //複数のCTMを対応する
        private Dictionary<string, int> conditionPatterns = new Dictionary<string, int>();
        private Dictionary<string, List<ElementConditionData>> conditionLists = new Dictionary<string, List<ElementConditionData>>();
        private List<int> DT_X = new List<int>();
        private List<int> DT_Y = new List<int>();
        private List<string> tooltips = new List<string>();
        private Dictionary<string, Dictionary<ElementConditionData, string>> elementConditions = new Dictionary<string, Dictionary<ElementConditionData, string>>();
        //ISSUE_NO.740 Sunyi 2018/07/10 end

        //AISMM No.80 sunyi 20181211 start
        //GripMission条件表示
        private Dictionary<string, List<ElementConditionData>> conditionLists_Grip = new Dictionary<string, List<ElementConditionData>>();
        private Dictionary<string, Dictionary<ElementConditionData, string>> elementConditions_Grip = new Dictionary<string, Dictionary<ElementConditionData, string>>();
        //AISMM No.80 sunyi 20181211 end
        private volatile DataSourceType sourceType = DataSourceType.NONE;

        public CtmDetails()
        {
            InitializeComponent();
        }


        public List<CtmObject> Ctms { get; set; }

        public UserControl_SpreadsheetTemplate SpreadSheet { get; set; }
        public UserControl_OnlineTemplate Online { get; set; }
        public UserControl_BulkyEdit BulkyEdit { get; set; }
        public UserControl_EmergencyTemplate Emergency { get; set; }
        public UserControl_ContinuousGraphTemplate ContinuousGraph { get; set; }
        public UserControl_StreamGraphTemplate StreamGraph { get; set; }
        public UserControl_LeadTimeTemplate LeadTime { get; set; }
        public UserControl_StockTimeTemplate StockTime { get; set; }
        public UserControl_ChimneyChartTemplate ChimneyChart { get; set; }
        public UserControl_MomentTemplate Moment { get; set; }
        public UserControl_DACTemplate DAC { get; set; }
        public UserControl_FIFOTemplate FIFO { get; set; }
        public UserControl_FrequencyTemplate Frequency { get; set; }
        public UserControl_StatusMonitor StatusMonitor { get; set; }
        public UserControl_MultiStatusMonitor MultiStatusMonitor { get; set; }
        public UserControl_ProjectStatusMonitor ProjectStatusMonitor { get; set; }

        public void DeleteExternalFileFromTree(string datatoken)
        {
            // Create a new JsonTreeViewItem object and put the data in
            JToken fileInfoToken = JToken.Parse(datatoken);
            JObject itemobj = new JObject();

            List<TreeViewItem> items = this.MultiStatusMonitor.AisTvRefDicMission.GetAllItems();
            TreeViewItem sameItem = null;
            string sameItemId = null;
            foreach (var item in items)
            {
                JToken token = this.MultiStatusMonitor.AisTvRefDicMission.GetToken(item);
                if (token["name"].ToString().Equals(fileInfoToken["fileName"].ToString()))
                {
                    sameItem = item;
                    sameItemId = this.MultiStatusMonitor.AisTvRefDicMission.GetId(item);
                    break;
                }

            }
            if (sameItem == null)
                return;
            else
            {
                itemobj["id"] = sameItemId;
                this.MultiStatusMonitor.AisTvRefDicMission.DeleteItem(sameItemId);
            }
        }

        public UserControl_TorqueTemplate Torque { get; set; }
        public UserControl_Comment Comment { get; set; }
        public UserControl_PastStockTime PastStockTime { get; set; }
        //AIS_WorkPlace
        public UserControl_WorkPlace WorkPlace { get; set; }


        public DataSourceType CurrentDataSourceType { get { return this.sourceType; } }

        //
        // 不要だが、用途別のUserControlで使われているので消せない。
        //
        public DataTable Dt { get; set; }

        public string SelectedMissionId { get; set; }

		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
        public void SetElementDataToDataGrid(string missionId, MissionTreeController ctrlMt)
        {
            this.SelectedMissionId = missionId;

            // 個別に指定する場合は、既存の設定をサーバから再取得
            CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
            missionCtmGetClient.AddParam("missionId", missionId);
            // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
            missionCtmGetClient.CompleteHandler += missionCtmJson =>
            {
                this.missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);

                // 条件式
                if (this.missionCtms.Count < 1)
                {
                    FoaMessageBox.ShowError("AIS_E_027");
                    SetInitialDataGrid();
                    return;
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 Start
                //複数のCTMを対応する
                this.conditionLists.Clear();
                this.conditionPatterns.Clear();
                foreach (var missionCtm in this.missionCtms)
                {
                    if (string.IsNullOrEmpty(missionCtm.ElementCondition) == false)
                    {
                        JToken condJson = JToken.Parse(missionCtm.ElementCondition);
                        JArray condElements = (JArray)condJson["elements"];
                        this.conditionPatterns.Add(missionCtm.CtmId, int.Parse(condJson["pattern"].ToString()));

                        List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
                        ElementConditionDatas = JsonConvert.DeserializeObject<List<ElementConditionData>>(condElements.ToString());
                        this.conditionLists.Add(missionCtm.CtmId, ElementConditionDatas);
                    }
                    else
                    {
                        //this.conditionList = new List<ElementConditionData>();
                        this.conditionLists.Add(missionCtm.CtmId, new List<ElementConditionData>());
                    }
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 End
                getElements(missionId);
                this.sourceType = DataSourceType.MISSION;
            };
            missionCtmGetClient.Get(CmsUrl.GetMissionCtmUrl());

            // ミッションTreeViewを設定
            TreeViewItem selectedItem = ctrlMt.tree.SelectedItem as TreeViewItem;
            StackPanel selectedHeader = selectedItem.Header as StackPanel;

            this.MultiStatusMonitor.AisTvRefDicMission.AisCheckFlag = 1;
            this.MultiStatusMonitor.AisTvRefDicMission.MissionNewType = 1;
            this.MultiStatusMonitor.AisTvRefDicMission.CheckChangedEvent +=
                    this.MultiStatusMonitor.AisTvRefDicMissionCheckChanged;
            this.MultiStatusMonitor.AisTvRefDicMission.JsonItemSourceAdd = selectedHeader.DataContext.ToString();
            // Processing On Server 検証＃6　dn 2018/10/05 start
            TreeViewItem addItem = this.MultiStatusMonitor.AisTvRefDicMission.GetItem(missionId);
            addItem.IsSelected = true;
            // Processing On Server 検証＃6　dn 2018/10/05 end
        }
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 Start
        // Multi_Dac 修正
        public void SetElementDataToDataGrid_Dac(string missionId, MissionTreeController ctrlMt)
        {
            this.SelectedMissionId = missionId;

            // 個別に指定する場合は、既存の設定をサーバから再取得
            CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
            missionCtmGetClient.AddParam("missionId", missionId);
            // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
            missionCtmGetClient.CompleteHandler += missionCtmJson =>
            {
                this.missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);

                // 条件式
                if (this.missionCtms.Count < 1)
                {
                    FoaMessageBox.ShowError("AIS_E_027");
                    SetInitialDataGrid();
                    return;
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 Start
                //複数のCTMを対応する
                this.conditionLists.Clear();
                this.conditionPatterns.Clear();
                foreach (var missionCtm in this.missionCtms)
                {
                    if (string.IsNullOrEmpty(missionCtm.ElementCondition) == false)
                    {
                        JToken condJson = JToken.Parse(missionCtm.ElementCondition);
                        JArray condElements = (JArray)condJson["elements"];
                        this.conditionPatterns.Add(missionCtm.CtmId, int.Parse(condJson["pattern"].ToString()));

                        List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
                        ElementConditionDatas = JsonConvert.DeserializeObject<List<ElementConditionData>>(condElements.ToString());
                        this.conditionLists.Add(missionCtm.CtmId, ElementConditionDatas);
                    }
                    else
                    {
                        //this.conditionList = new List<ElementConditionData>();
                        this.conditionLists.Add(missionCtm.CtmId, new List<ElementConditionData>());
                    }
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 End
                getElements(missionId);
                this.sourceType = DataSourceType.MISSION;
            };
            missionCtmGetClient.Get(CmsUrl.GetMissionCtmUrl());

            // ミッションTreeViewを設定
            TreeViewItem selectedItem = ctrlMt.tree.SelectedItem as TreeViewItem;
            StackPanel selectedHeader = selectedItem.Header as StackPanel;

            this.DAC.AisTvRefDicMission.AisCheckFlag = 1;
            this.DAC.AisTvRefDicMission.MissionNewType = 1;
            this.DAC.AisTvRefDicMission.CheckChangedEvent +=
                    this.DAC.AisTvRefDicMissionCheckChanged;

            this.DAC.AisTvRefDicMission.JsonItemSourceAdd = selectedHeader.DataContext.ToString();
            // Processing On Server 検証＃6　dn 2018/10/05 start
            TreeViewItem addItem = this.DAC.AisTvRefDicMission.GetItem(missionId);
            addItem.IsSelected = true;
            // Processing On Server 検証＃6　dn 2018/10/05 end
        }
        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 end
        public void SetElementDataToDataGrid(string missionId)
        {
            this.SelectedMissionId = missionId;

            // 個別に指定する場合は、既存の設定をサーバから再取得
            CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
            missionCtmGetClient.AddParam("missionId", missionId);
            // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
            missionCtmGetClient.CompleteHandler += missionCtmJson =>
            {
                this.missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);

                // 条件式
                if (this.missionCtms.Count < 1)
                {
                    FoaMessageBox.ShowError("AIS_E_027");
                    SetInitialDataGrid();
                    return;
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 Start
                //複数のCTMを対応する
                this.conditionLists.Clear();
                this.conditionPatterns.Clear();
                foreach (var missionCtm in this.missionCtms)
                {
                    //if (string.IsNullOrEmpty(this.missionCtms[0].ElementCondition) == false)
                    //{
                    //    JToken condJson = JToken.Parse(this.missionCtms[0].ElementCondition);
                    //    JArray condElements = (JArray)condJson["elements"];
                    //    this.conditionPattern = int.Parse(condJson["pattern"].ToString());
                    //    this.conditionList = JsonConvert.DeserializeObject<List<ElementConditionData>>(condElements.ToString());
                    //}
                    //else
                    //{
                    //    this.conditionList = new List<ElementConditionData>();
                    //}
                    if (string.IsNullOrEmpty(missionCtm.ElementCondition) == false)
                    {
                        JToken condJson = JToken.Parse(missionCtm.ElementCondition);
                        JArray condElements = (JArray)condJson["elements"];
                        this.conditionPatterns.Add(missionCtm.CtmId, int.Parse(condJson["pattern"].ToString()));

                        List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
                        ElementConditionDatas = JsonConvert.DeserializeObject<List<ElementConditionData>>(condElements.ToString());
                        this.conditionLists.Add(missionCtm.CtmId, ElementConditionDatas);
                    }
                    else
                    {
                        //this.conditionList = new List<ElementConditionData>();
                        this.conditionLists.Add(missionCtm.CtmId,new List<ElementConditionData>());
                    }
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 End
                getElements(missionId);
                this.sourceType = DataSourceType.MISSION;
            };
            missionCtmGetClient.Get(CmsUrl.GetMissionCtmUrl());
        }
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
        public void SetElementDataToDataGrid_Grip(string missionId, GripMissionTreeController ctrlGr)
        {
            CmsHttpClient missionPathsGetClient = new CmsHttpClient(Application.Current.MainWindow);
            missionPathsGetClient.AddParam("missionId", missionId);
            missionPathsGetClient.CompleteHandler += missionPathsJson =>
            {
                // BugFix AIS_MM.99-1, AIS_MM.101 20190611 yakiyama start
				logger.Debug(MethodBase.GetCurrentMethod().Name + "() " + "URL: " + CmsUrl.Search.Mission.FindPath() + "?missionId=" + missionId);
				logger.Debug(MethodBase.GetCurrentMethod().Name + "() " + "response: " + (missionPathsJson == null ? "NULL" : missionPathsJson));
				bool nullMissionPath = false;
				if (string.IsNullOrEmpty(missionPathsJson) || missionPathsJson.Trim().Equals("{}"))
				{
					nullMissionPath = true;
				}
				List<FindPathsResult> results = nullMissionPath ? new List<FindPathsResult>() : JsonUtil.Deserialize<List<FindPathsResult>>(missionPathsJson);

				bool zeroMissionPath = true;
				foreach (FindPathsResult result in results)
				{
					if (result.paths != null)
					{
						foreach (FindResultPath path in result.paths)
						{
							if (path.nodes != null && path.nodes.Count > 0)
							{
								zeroMissionPath = false;
								break;
							}
						}
					}

					if (zeroMissionPath == false)
					{
						break;
					}
				}
				if (zeroMissionPath)
				{
					FoaMessageBox.ShowWarning("AIS_W_006");
				}
				// BugFix AIS_MM.99-1, AIS_MM.101 20190611 yakiyama end

                List<CtmObject> listCtm = new List<CtmObject>();
                JArray array = new JArray();
                if (null != results && results.Count > 0)
                {
                    int routeIndex = 0;
                    foreach (FindPathsResult result in results)
                    {
                        if (null == result.paths)
                        {
                            continue;
                        }

                        int endCtmIndex = 1;
                        string ctmName = "";
                        string ctmId = "";
                        foreach (FindResultPath path in result.paths)
                        {
                            if (null != path.nodes && path.nodes.Count > 0)
                            {
                                List<string> listCtms = new List<string>();
                                for (int i = 0; i < path.nodes.Count; i++)
                                {
                                    if (string.IsNullOrEmpty(path.nodes[i].ctmId))
                                    {
                                        continue;
                                    }
                                    listCtms.Add(path.nodes[i].ctmId);
                                }

                                JToken ctmToken = new JObject();
                                ctmId = path.nodes[path.nodes.Count - 1].ctmId;
                                var list = AisUtil.GetCtmsFromCtmIds(new List<string>() { ctmId });
                                JToken jtoken = JToken.Parse(JsonConvert.SerializeObject(list[0]));
                                JToken dnToken = jtoken["displayName"];
                                ctmName = DisplayNameConverter.Convert2(dnToken);
                                ctmToken["id"] = missionId + "_" + routeIndex + "_" + ctmId + "_" + endCtmIndex;
                                ctmToken["name"] = routeIndex + "_" + ctmName + "_Route-" + endCtmIndex;
                                ctmToken["missionId"] = missionId;
                                ctmToken["nodes"] = string.Join(",", listCtms.ToArray());
                                array.Add(ctmToken);
                                endCtmIndex++;
                            }
                        }

                        routeIndex++;
                    }
                }

                this.MultiStatusMonitor.AisTvRefDicCtm.AisCheckFlag = 1;
                this.MultiStatusMonitor.AisTvRefDicCtm.MissionNewType = 0;
                this.MultiStatusMonitor.AisTvRefDicCtm.MissionId = missionId;
                this.MultiStatusMonitor.AisTvRefDicCtm.JsonItemSourceAdd = array.ToString();
                this.MultiStatusMonitor.AisTvRefDicCtm.CheckChangedEvent += this.MultiStatusMonitor.AisTvRefDicCtmCheckChanged;

                // ミッションTreeViewを設定
                TreeViewItem selectedItem = ctrlGr.tree.SelectedItem as TreeViewItem;
                StackPanel selectedHeader = selectedItem.Header as StackPanel;

                this.MultiStatusMonitor.AisTvRefDicMission.AisCheckFlag = 1;
                this.MultiStatusMonitor.AisTvRefDicMission.MissionNewType = 0;
                this.MultiStatusMonitor.AisTvRefDicMission.CheckChangedEvent += this.MultiStatusMonitor.AisTvRefDicMissionCheckChanged;
                this.MultiStatusMonitor.AisTvRefDicMission.JsonItemSourceAdd = selectedHeader.DataContext.ToString();
                // Processing On Server 検証＃6 dn 2018/10/05 start
                TreeViewItem addItem = this.MultiStatusMonitor.AisTvRefDicMission.GetItem(missionId);
                addItem.IsSelected = true;
                // Processing On Server 検証＃6 dn 2018/10/05 end

                // 個別に指定する場合は、既存の設定をサーバから再取得
                CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                missionCtmGetClient.AddParam("id", missionId);
                // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
                missionCtmGetClient.CompleteHandler += missionCtmJson =>
                {
                    var missionCtms = JsonConvert.DeserializeObject<GripMissionCtm>(missionCtmJson);
                    this.conditionList = new List<ElementConditionData>();
                    getElements_Grip(missionCtms);

                    this.sourceType = DataSourceType.GRIP;
                };
                missionCtmGetClient.Get(GripUrl.Mission.Base());

            };
            missionPathsGetClient.Get(CmsUrl.Search.Mission.FindPath());

        }
		// FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
        public void SetElementDataToDataGrid_Grip_Dac(string missionId, GripMissionTreeController ctrlGr)
        {
            CmsHttpClient missionPathsGetClient = new CmsHttpClient(Application.Current.MainWindow);
            missionPathsGetClient.AddParam("missionId", missionId);
            missionPathsGetClient.CompleteHandler += missionPathsJson =>
            {
				// BugFix AIS_MM.99-1, AIS_MM.101 20190611 yakiyama start
				logger.Debug(MethodBase.GetCurrentMethod().Name + "() " + "URL: " + CmsUrl.Search.Mission.FindPath() + "?missionId=" + missionId);
				logger.Debug(MethodBase.GetCurrentMethod().Name + "() " + "response: " + (missionPathsJson == null ? "NULL" : missionPathsJson));
				bool nullMissionPath = false;
				if (string.IsNullOrEmpty(missionPathsJson) || missionPathsJson.Trim().Equals("{}"))
				{
					nullMissionPath = true;
				}
                List<FindPathsResult> results = nullMissionPath ? new List<FindPathsResult>() : JsonUtil.Deserialize<List<FindPathsResult>>(missionPathsJson);

				bool zeroMissionPath = true;
				foreach (FindPathsResult result in results)
				{
					if (result.paths != null)
					{
						foreach (FindResultPath path in result.paths)
						{
							if (path.nodes != null && path.nodes.Count > 0)
							{
								zeroMissionPath = false;
								break;
							}
						}
					}

					if (zeroMissionPath == false)
					{
						break;
					}
				}
				if (zeroMissionPath)
				{
					FoaMessageBox.ShowWarning("AIS_W_006");
				}
				// BugFix AIS_MM.99-1, AIS_MM.101 20190611 yakiyama end

                List<CtmObject> listCtm = new List<CtmObject>();
                JArray array = new JArray();
                if (null != results && results.Count > 0)
                {
                    int routeIndex = 0;
                    foreach (FindPathsResult result in results)
                    {
                        if (null == result.paths)
                        {
                            continue;
                        }

                        int endCtmIndex = 1;
                        string ctmName = "";
                        string ctmId = "";
                        foreach (FindResultPath path in result.paths)
                        {
                            if (null != path.nodes && path.nodes.Count > 0)
                            {
                                List<string> listCtms = new List<string>();
                                for (int i = 0; i < path.nodes.Count; i++)
                                {
                                    if (string.IsNullOrEmpty(path.nodes[i].ctmId))
                                    {
                                        continue;
                                    }
                                    listCtms.Add(path.nodes[i].ctmId);
                                }

                                JToken ctmToken = new JObject();
                                ctmId = path.nodes[path.nodes.Count - 1].ctmId;
                                var list = AisUtil.GetCtmsFromCtmIds(new List<string>() { ctmId });
                                JToken jtoken = JToken.Parse(JsonConvert.SerializeObject(list[0]));
                                JToken dnToken = jtoken["displayName"];
                                ctmName = DisplayNameConverter.Convert2(dnToken);
                                ctmToken["id"] = missionId + "_" + routeIndex + "_" + ctmId + "_" + endCtmIndex;
                                ctmToken["name"] = routeIndex + "_" + ctmName + "_Route-" + endCtmIndex;
                                ctmToken["missionId"] = missionId;
                                ctmToken["nodes"] = string.Join(",", listCtms.ToArray());
                                array.Add(ctmToken);
                                endCtmIndex++;
                            }
                        }

                        routeIndex++;
                    }
                }

                this.DAC.AisTvRefDicCtm.AisCheckFlag = 1;
                this.DAC.AisTvRefDicCtm.MissionNewType = 0;
                this.DAC.AisTvRefDicCtm.MissionId = missionId;
                this.DAC.AisTvRefDicCtm.JsonItemSourceAdd = array.ToString();
                this.DAC.AisTvRefDicCtm.CheckChangedEvent += this.DAC.AisTvRefDicCtmCheckChanged;

                // ミッションTreeViewを設定
                TreeViewItem selectedItem = ctrlGr.tree.SelectedItem as TreeViewItem;
                StackPanel selectedHeader = selectedItem.Header as StackPanel;

                this.DAC.AisTvRefDicMission.AisCheckFlag = 1;
                this.DAC.AisTvRefDicMission.MissionNewType = 0;
                this.DAC.AisTvRefDicMission.CheckChangedEvent += this.DAC.AisTvRefDicMissionCheckChanged;
                this.DAC.AisTvRefDicMission.JsonItemSourceAdd = selectedHeader.DataContext.ToString();
                // Processing On Server 検証＃6 dn 2018/10/05 start
                TreeViewItem addItem = this.DAC.AisTvRefDicMission.GetItem(missionId);
                addItem.IsSelected = true;
                // Processing On Server 検証＃6 dn 2018/10/05 end

                // 個別に指定する場合は、既存の設定をサーバから再取得
                CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                missionCtmGetClient.AddParam("id", missionId);
                // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
                missionCtmGetClient.CompleteHandler += missionCtmJson =>
                {
					var missionCtms = JsonConvert.DeserializeObject<GripMissionCtm>(missionCtmJson);
                    this.conditionList = new List<ElementConditionData>();
                    getElements_Grip(missionCtms);

                    this.sourceType = DataSourceType.GRIP;
                };
                missionCtmGetClient.Get(GripUrl.Mission.Base());

            };
            missionPathsGetClient.Get(CmsUrl.Search.Mission.FindPath());

        }
        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End

        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
        public void SetElementDataToDataGrid_Grip_Wp(string missionId, GripMissionTreeController ctrlGr)
        {
            CmsHttpClient missionPathsGetClient = new CmsHttpClient(Application.Current.MainWindow);
            missionPathsGetClient.AddParam("missionId", missionId);
            missionPathsGetClient.CompleteHandler += missionPathsJson =>
            {
				// BugFix AIS_MM.99-1, AIS_MM.101 20190611 yakiyama start
				logger.Debug(MethodBase.GetCurrentMethod().Name + "() " + "URL: " + CmsUrl.Search.Mission.FindPath() + "?missionId=" + missionId);
				logger.Debug(MethodBase.GetCurrentMethod().Name + "() " + "response: " + (missionPathsJson == null ? "NULL" : missionPathsJson));
				bool nullMissionPath = false;
				if (string.IsNullOrEmpty(missionPathsJson) || missionPathsJson.Trim().Equals("{}"))
				{
					nullMissionPath = true;
				}
				List<FindPathsResult> results = nullMissionPath ? new List<FindPathsResult>() : JsonUtil.Deserialize<List<FindPathsResult>>(missionPathsJson);

				bool zeroMissionPath = true;
				foreach (FindPathsResult result in results)
				{
					if (result.paths != null)
					{
						foreach (FindResultPath path in result.paths)
						{
							if (path.nodes != null && path.nodes.Count > 0)
							{
								zeroMissionPath = false;
								break;
							}
						}
					}

					if (zeroMissionPath == false)
					{
						break;
					}
				}
				if (zeroMissionPath)
				{
					FoaMessageBox.ShowWarning("AIS_W_006");
				}
				// BugFix AIS_MM.99-1, AIS_MM.101 20190611 yakiyama end

                List<CtmObject> listCtm = new List<CtmObject>();
                JArray array = new JArray();
                if (null != results && results.Count > 0)
                {
                    int routeIndex = 0;
                    foreach (FindPathsResult result in results)
                    {
                        if (null == result.paths)
                        {
                            continue;
                        }

                        int endCtmIndex = 1;
                        string ctmName = "";
                        string ctmId = "";
                        foreach (FindResultPath path in result.paths)
                        {
                            if (null != path.nodes && path.nodes.Count > 0)
                            {
                                List<string> listCtms = new List<string>();
                                for (int i = 0; i < path.nodes.Count; i++)
                                {
                                    if (string.IsNullOrEmpty(path.nodes[i].ctmId))
                                    {
                                        continue;
                                    }
                                    listCtms.Add(path.nodes[i].ctmId);
                                }

                                JToken ctmToken = new JObject();
                                ctmId = path.nodes[path.nodes.Count - 1].ctmId;
                                var list = AisUtil.GetCtmsFromCtmIds(new List<string>() { ctmId });
                                JToken jtoken = JToken.Parse(JsonConvert.SerializeObject(list[0]));
                                JToken dnToken = jtoken["displayName"];
                                ctmName = DisplayNameConverter.Convert2(dnToken);
                                ctmToken["id"] = missionId + "_" + routeIndex + "_" + ctmId + "_" + endCtmIndex;
                                ctmToken["name"] = routeIndex + "_" + ctmName + "_Route-" + endCtmIndex;
                                ctmToken["missionId"] = missionId;
                                ctmToken["nodes"] = string.Join(",", listCtms.ToArray());
                                array.Add(ctmToken);
                                endCtmIndex++;
                            }
                        }

                        routeIndex++;
                    }
                }

                this.WorkPlace.AisTvRefDicCtm.AisCheckFlag = 1;
                this.WorkPlace.AisTvRefDicCtm.MissionNewType = 0;
                this.WorkPlace.AisTvRefDicCtm.MissionId = missionId;
                this.WorkPlace.AisTvRefDicCtm.JsonItemSourceAdd = array.ToString();
                this.WorkPlace.AisTvRefDicCtm.CheckChangedEvent += this.WorkPlace.AisTvRefDicCtmCheckChanged;

                // ミッションTreeViewを設定
                TreeViewItem selectedItem = ctrlGr.tree.SelectedItem as TreeViewItem;
                StackPanel selectedHeader = selectedItem.Header as StackPanel;

                this.WorkPlace.AisTvRefDicMission.AisCheckFlag = 1;
                this.WorkPlace.AisTvRefDicMission.MissionNewType = 0;
                this.WorkPlace.AisTvRefDicMission.CheckChangedEvent += this.WorkPlace.AisTvRefDicMissionCheckChanged;
                this.WorkPlace.AisTvRefDicMission.JsonItemSourceAdd = selectedHeader.DataContext.ToString();

                // 個別に指定する場合は、既存の設定をサーバから再取得
                CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                missionCtmGetClient.AddParam("id", missionId);
                // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
                missionCtmGetClient.CompleteHandler += missionCtmJson =>
                {
					var missionCtms = JsonConvert.DeserializeObject<GripMissionCtm>(missionCtmJson);
                    this.conditionList = new List<ElementConditionData>();
                    getElements_Grip(missionCtms);

                    this.sourceType = DataSourceType.GRIP;
                };
                missionCtmGetClient.Get(GripUrl.Mission.Base());

            };
            missionPathsGetClient.Get(CmsUrl.Search.Mission.FindPath());

        }
        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End

        public void SetElementDataToDataGrid_Grip(string missionId)
        {
            // 個別に指定する場合は、既存の設定をサーバから再取得
            CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
            missionCtmGetClient.AddParam("id", missionId);
            // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
            missionCtmGetClient.CompleteHandler += missionCtmJson =>
            {
				//ISSUE_NO.778 Sunyi 2018/07/23 Start
                //G-missionの条件設定の色付く
                //var missionCtms = JsonConvert.DeserializeObject<GripMissionCtm>(missionCtmJson);
                //this.conditionList = new List<ElementConditionData>();
                //getElements_Grip(missionCtms);
                var gripMissionCtms = JsonConvert.DeserializeObject<GripMissionCtm>(missionCtmJson);
                GetElementConditionDatas(gripMissionCtms);
                getElements_Grip(gripMissionCtms);
                //ISSUE_NO.778 Sunyi 2018/07/23 End

                this.sourceType = DataSourceType.GRIP;
            };
            missionCtmGetClient.Get(GripUrl.Mission.Base());
        }
        //ISSUE_NO.778 Sunyi 2018/07/23 Start
        //G-missionの条件設定の色付く
        private void GetElementConditionDatas(GripMissionCtm gripMissionCtms)
        {
            //AISMM No.80 sunyi 20181211 start
            //GripMission条件表示
            //this.conditionLists.Clear();
            this.conditionLists_Grip.Clear();
            //AISMM No.80 sunyi 20181211 end
            this.conditionPatterns.Clear();

            List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
            //E2NaviUI sunyi 20190305 start
            //MISSION条件対応
            if (gripMissionCtms.StartNodes == null)
            {
                //start
                if (gripMissionCtms.StartNode.CondSet != null)
                {
                    this.conditionPatterns.Add(gripMissionCtms.StartNode.CtmId, gripMissionCtms.StartNode.CondSet.Pattern);
                    foreach (var Condition in gripMissionCtms.StartNode.CondSet.Conditions)
                    {
                        ElementConditionData ElementConditionData = new ElementConditionData
                        {
                            Id = Condition.ElementId,
                            Type = Condition.Datatype,
                            Condition = Condition.Operator,
                            Value = Condition.Value
                        };
                        ElementConditionDatas.Add(ElementConditionData);
                    }
                    //AISMM No.80 sunyi 20181211 start
                    //GripMission条件表示
                    //this.conditionLists.Add(gripMissionCtms.StartNode.CtmId, ElementConditionDatas);
                    this.conditionLists_Grip.Add(gripMissionCtms.StartNode.CtmId, ElementConditionDatas);
                    //AISMM No.80 sunyi 20181211 end
                }
            }
            else
            {
                //StartNodes
                foreach (var condSet in gripMissionCtms.StartNodes)
                {
                    if (condSet.Value.CondSet != null)
                    {
                        this.conditionPatterns.Add(condSet.Value.CtmId, condSet.Value.CondSet.Pattern);
                        foreach (var Condition in condSet.Value.CondSet.Conditions)
                        {
                            ElementConditionData ElementConditionData = new ElementConditionData
                            {
                                Id = Condition.ElementId,
                                Type = Condition.Datatype,
                                Condition = Condition.Operator,
                                Value = Condition.Value
                            };
                            ElementConditionDatas.Add(ElementConditionData);
                        }
                        //AISMM No.80 sunyi 20181211 start
                        //GripMission条件表示
                        //this.conditionLists.Add(condSet.CtmId, ElementConditionDatas);
                        this.conditionLists_Grip.Add(condSet.Value.CtmId, ElementConditionDatas);
                        //AISMM No.80 sunyi 20181211 end
                    }
                }
            }
            //E2NaviUI sunyi 20190305 end

            //end
            foreach (var condSet in gripMissionCtms.EndNodes)
            {
                if (condSet.CondSet_End != null)
                {
                    this.conditionPatterns.Add(condSet.CtmId, condSet.CondSet_End.Pattern);
                    foreach (var Condition in condSet.CondSet_End.Conditions)
                    {
                        ElementConditionData ElementConditionData = new ElementConditionData
                        {
                            Id = Condition.ElementId,
                            Type = Condition.Datatype,
                            Condition = Condition.Operator,
                            Value = Condition.Value
                        };
                        ElementConditionDatas.Add(ElementConditionData);
                    }
                    //AISMM No.80 sunyi 20181211 start
                    //GripMission条件表示
                    //this.conditionLists.Add(condSet.CtmId, ElementConditionDatas);
                    this.conditionLists_Grip.Add(condSet.CtmId, ElementConditionDatas);
                    //AISMM No.80 sunyi 20181211 end
                }
            }

            //trace
            HashSet<string> branchNodeIds = new HashSet<string>();
            if (gripMissionCtms.BranchNodes != null)
            {
                foreach (Node node in gripMissionCtms.BranchNodes)
                {
                    branchNodeIds.Add(node.CtmId);
                }
            }
            // Wang Issue AISTEMP-122 20181228 Start
            //if (gripMissionCtms.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace && gripMissionCtms.TraceNodes != null)
            if ((gripMissionCtms.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace || gripMissionCtms.SearchType == FoaCore.Model.GripR2.Search.SearchType.Period) && gripMissionCtms.TraceNodes != null)
            // Wang Issue AISTEMP-122 20181228 End
            {
                foreach (var condSet in gripMissionCtms.TraceNodes)
                {
                    if (condSet.CondSet_Trace != null)
                    {
                        this.conditionPatterns.Add(condSet.CtmId, condSet.CondSet_Trace.Pattern);
                        foreach (var Condition in condSet.CondSet_Trace.Conditions)
                        {
                            ElementConditionData ElementConditionData = new ElementConditionData
                            {
                                Id = Condition.ElementId,
                                Type = Condition.Datatype,
                                Condition = Condition.Operator,
                                Value = Condition.Value
                            };
                            ElementConditionDatas.Add(ElementConditionData);
                        }
                        //AISMM No.80 sunyi 20181211 start
                        //GripMission条件表示
                        //this.conditionLists.Add(condSet.CtmId, ElementConditionDatas);
                        this.conditionLists_Grip.Add(condSet.CtmId, ElementConditionDatas);
                        //AISMM No.80 sunyi 20181211 end
                    }
                }
            }
        }
        //ISSUE_NO.778 Sunyi 2018/07/23 End

        public void SetElementDataToDataGridFromCtmList(List<string> ctmIdList)
        {
            if (ctmIdList.Count < 1)
            {
                SetInitialDataGrid();
                return;
            }

            // 個別に指定する場合は、既存の設定をサーバから再取得
            CmsHttpClient ctmGetClient = new CmsHttpClient(Application.Current.MainWindow);
            foreach (string ctmId in ctmIdList)
            {
                ctmGetClient.AddParam("id", ctmId);
            }
            // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
            ctmGetClient.CompleteHandler += ctmJson =>
            {
                // this.missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);
                List<CtmObject> ctmList = JsonConvert.DeserializeObject<List<CtmObject>>(ctmJson);

                // 条件式
                this.conditionList = new List<ElementConditionData>();
                this.Ctms = ctmList;

                var ctmData = getCtmsData2(ctmList);
                showDataGrid(ctmData);

                this.sourceType = DataSourceType.CTM_DIRECT;
            };
            ctmGetClient.Get(CmsUrlMms.Ctm.List());
        }

        public List<string> GetElementIdListUsingMissionCtm(List<ProgramMission> deserializedData, string ctmId)
        {
            List<string> elementIdList = new List<string>();

            foreach (ProgramMission missionCtm in deserializedData)
            {
                if (missionCtm.id != ctmId)
                {
                    continue;
                }

                if (missionCtm.num < 1)
                {
                    return elementIdList;
                }

                foreach (string elementId in missionCtm.ctms[0].EL.Keys)
                {
                    if (FoaDatatype.isBulky(Int32.Parse(missionCtm.ctms[0].EL[elementId].T)))
                    {
                        continue;
                    }

                    elementIdList.Add(elementId);
                }

                break;
            }

            return elementIdList;
        }

        public string GetCtmIdFromName(string ctmName)
        {
            foreach (var ctm in this.Ctms)
            {
                if (ctmName != ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true))
                {
                    continue;
                }

                return ctm.id.ToString();
            }

            return string.Empty;
        }

        public string GetElementIdFromName(string ctmName, string elementName)
        {
            foreach (var ctm in this.Ctms)
            {
                if (ctmName != ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true))
                {
                    continue;
                }

                foreach (var element in ctm.GetAllElements())
                {
                    if (elementName != element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true))
                    {
                        continue;
                    }

                    return element.id.ToString();
                }

                break;
            }

            return string.Empty;
        }

        /// <summary>
        /// CtmDtailsの中が中身を判断する
        /// </summary>
        /// <param name="ctmName"></param>
        /// <returns></returns>
        public string GetCtmDtailsEmpty()
        {
            string GetCtmDtailsEmpty = "";

            MainWindow m = Application.Current.MainWindow as MainWindow;
            var row = m.CtmDtailsCtrl.dataGrid_Element.Items[0] as DataRowView;
            GetCtmDtailsEmpty = row[0].ToString();

            return GetCtmDtailsEmpty;
        }

        public string GetArg(int rowIndex)
        {
            string content = string.Empty;

            // 表示待機 以下を飛ばすと例外が発生する
            this.dataGrid_Element.ScrollIntoView(this.dataGrid_Element.Items[rowIndex]);

            DataGridCellInfo cellInfo = new DataGridCellInfo(this.dataGrid_Element.Items[rowIndex], this.dataGrid_Element.Columns[0]);
            DataGridCell cell = getCellFromCellInfo(this.dataGrid_Element, cellInfo);

            var row = this.dataGrid_Element.Items[rowIndex] as DataRowView;
            content = row[this.dataGrid_Element.CurrentColumn.DisplayIndex].ToString();

            string ctmId = GetCtmIdFromName(row[0].ToString());
            string elementId = GetElementIdFromName(row[0].ToString(), content);
            string arg = row[0].ToString() + "," + ctmId + "," + content + "," + elementId;

            if (cell.Column.Header.ToString() == Keywords.CTM_NAME)
            {
                if (this.DAC != null)
                {
                    this.DAC.isDragCtmName = true;
                }
                else if (this.StatusMonitor != null)
                {
                    this.StatusMonitor.isDragCtmName = true;
                }
                else if (this.MultiStatusMonitor != null)
                {
                    this.MultiStatusMonitor.isDragCtmName = true;
                }
                else if (this.ProjectStatusMonitor != null)
                {
                    this.ProjectStatusMonitor.isDragCtmName = true;
                }
                else if (this.Torque != null)
                {
                    this.Torque.isDragCtmName = true;
                }
                //AIS_WorkPlace
                else if (this.WorkPlace != null)
                {
                    this.WorkPlace.isDragCtmName = true;
                }
                arg = row[0].ToString() + "," + ctmId;
                //ISSUE_NO.758 Sunyi 2018/06/26 Start
                //ItemArray[0]はCTM名なので、第二回目から処理する
                //foreach (var item in row.Row.ItemArray)
                //{
                for (int i = 1; i < row.Row.ItemArray.Length; i++)
                {
                    var item = row.Row.ItemArray[i];
                    //ISSUE_NO.758 Sunyi 2018/06/26 End
                    if (item.ToString() == string.Empty)
                    {
                        break;
                    }

                    //ISSUE_NO.758 Sunyi 2018/06/26 Start
                    //ItemArray[0]はCTM名なので、第二回目から処理する
                    //if (item.ToString() == row[0].ToString())
                    //{
                    //    continue;
                    //}
                    //ISSUE_NO.758 Sunyi 2018/06/26 End

                    string element = item.ToString() + "," + GetElementIdFromName(row[0].ToString(), item.ToString());
                    int dataType = -1;
                    foreach (CtmObject ctm in this.Ctms)
                    {
                        if (ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != row[0].ToString())
                        {
                            continue;
                        }
                        foreach (var el in ctm.GetAllElements())
                        {
                            if (el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != item.ToString())
                            {
                                continue;
                            }

                            dataType = el.datatype;
                            break;
                        }

                        break;
                    }

                    element += "," + dataType.ToString();

                    arg += "," + element;
                }
            }
            else
            {
                if (this.DAC != null)
                {
                    this.DAC.isDragCtmName = false;
                }
                else if (this.StatusMonitor != null)
                {
                    this.StatusMonitor.isDragCtmName = false;
                }
                else if (this.MultiStatusMonitor != null)
                {
                    this.MultiStatusMonitor.isDragCtmName = false;
                }
                else if (this.ProjectStatusMonitor != null)
                {
                    this.ProjectStatusMonitor.isDragCtmName = false;
                }
                else if (this.Torque != null)
                {
                    this.Torque.isDragCtmName = false;
                }
                //AIS_WorkPlace
                else if (this.WorkPlace != null)
                {
                    this.WorkPlace.isDragCtmName = false;
                }

                // エレメントの型も渡したい
                int dataType = -1;
                foreach (CtmObject ctm in this.Ctms)
                {
                    if (ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != row[0].ToString())
                    {
                        continue;
                    }

                    foreach (var el in ctm.GetAllElements())
                    {
                        if (el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != content)
                        {
                            continue;
                        }

                        dataType = el.datatype;
                        break;
                    }

                    break;
                }

                arg += "," + dataType.ToString();
            }

            // 単位行は無視
            if (rowIndex % 2 == 1)
            {
                arg = string.Empty;
            }

            return arg;
        }

        #region Event Handler

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            SetInitialDataGrid();
        }




        private void cell_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
            {
                return;
            }

            DataGridCell cell = sender as DataGridCell;
            if (cell == null)
            {
                return;
            }

            string content = ((TextBlock)cell.Content).Text;
            if (string.IsNullOrEmpty(content))
            {
                return;
            }

            int rowIndex = this.dataGrid_Element.Items.IndexOf(this.dataGrid_Element.CurrentItem);
            if (content == "Bulky")
            {
                rowIndex--;
            }
            var row = this.dataGrid_Element.Items[rowIndex] as DataRowView;
            content = row[this.dataGrid_Element.CurrentColumn.DisplayIndex].ToString();

            string ctmId = GetCtmIdFromName(row[0].ToString());
            string elementId = GetElementIdFromName(row[0].ToString(), content);
            string arg = row[0].ToString() + "," + ctmId + "," + content + "," + elementId;

            if (cell.Column.Header.ToString() == Keywords.CTM_NAME)
            {
                if (this.DAC != null)
                {
                    this.DAC.isDragCtmName = true;
                }
                else if (this.StatusMonitor != null)
                {
                    this.StatusMonitor.isDragCtmName = true;
                }
                else if (this.MultiStatusMonitor != null)
                {
                    this.MultiStatusMonitor.isDragCtmName = true;
                }
                else if (this.ProjectStatusMonitor != null)
                {
                    this.ProjectStatusMonitor.isDragCtmName = true;
                }
                //AIS_WorkPlace
                else if (this.Torque != null)
                {
                    this.Torque.isDragCtmName = true;
                }
                //AIS_WorkPlace
                else if (this.WorkPlace != null)
                {
                    this.WorkPlace.isDragCtmName = true;
                }
                arg = row[0].ToString() + "," + ctmId;
                //ISSUE_NO.758 Sunyi 2018/06/26 Start
                //ItemArray[0]はCTM名なので、第二回目から処理する
                //foreach (var item in row.Row.ItemArray)
                //{
                for (int i = 1; i < row.Row.ItemArray.Length; i++)
                {
                    var item = row.Row.ItemArray[i];
                    //ISSUE_NO.758 Sunyi 2018/06/26 End
                    if (item.ToString() == string.Empty)
                    {
                        break;
                    }
                    //ISSUE_NO.758 Sunyi 2018/06/26 Start
                    //ItemArray[0]はCTM名なので、第二回目から処理する
                    //if (item.ToString() == row[0].ToString())
                    //{
                    //    continue;
                    //}
                    //ISSUE_NO.758 Sunyi 2018/06/26 End

                    string element = item.ToString() + "," + GetElementIdFromName(row[0].ToString(), item.ToString());
                    int dataType = -1;
                    foreach (CtmObject ctm in this.Ctms)
                    {
                        if (ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != row[0].ToString())
                        {
                            continue;
                        }
                        foreach (var el in ctm.GetAllElements())
                        {
                            if (el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != item.ToString())
                            {
                                continue;
                            }

                            dataType = el.datatype;
                            break;
                        }

                        break;
                    }

                    element += "," + dataType.ToString();

                    arg += "," + element;
                }
            }
            else
            {
                if (this.DAC != null)
                {
                    this.DAC.isDragCtmName = false;
                }
                else if (this.StatusMonitor != null)
                {
                    this.StatusMonitor.isDragCtmName = false;
                }
                else if (this.MultiStatusMonitor != null)
                {
                    this.MultiStatusMonitor.isDragCtmName = false;
                }
                else if (this.ProjectStatusMonitor != null)
                {
                    this.ProjectStatusMonitor.isDragCtmName = false;
                }
                else if (this.Torque != null)
                {
                    this.Torque.isDragCtmName = false;
                }
                //AIS_WorkPlace
                else if (this.WorkPlace != null)
                {
                    this.WorkPlace.isDragCtmName = false;
                }

                // エレメントの型も渡したい
                int dataType = -1;
                foreach (CtmObject ctm in this.Ctms)
                {
                    if (ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != row[0].ToString())
                    {
                        continue;
                    }

                    foreach (var el in ctm.GetAllElements())
                    {
                        if (el.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != content)
                        {
                            continue;
                        }

                        dataType = el.datatype;
                        if (dataType == FoaDatatype.BULKY)
                        {
                            int subtype = el.bulkysubtype;
                            dataType += subtype;
                        }
                        break;
                    }

                    break;
                }

                arg += "," + dataType.ToString();
            }

            // 単位行は無視
            if (rowIndex % 2 == 1)
            {
                arg = string.Empty;
            }

            // 空白は無視
            if (string.IsNullOrEmpty(content))
            {
                arg = string.Empty;
            }

            DragDrop.DoDragDrop(cell,
                arg,
                DragDropEffects.Copy);
        }




        //
        // インポートアイコン
        //

        private void Image_PreviewDragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effects = DragDropEffects.Copy;
            }
        }

        private void Image_PreviewDrop(object sender, DragEventArgs e)
        {
            var dataObj = e.Data as DataObject;
            var fileList = dataObj.GetFileDropList();
            foreach (var fn in fileList)
            {
                importCsvFile(fn);
            }
        }

        #endregion

        private void importCsvFile(string filepath)
        {
            var ext = System.IO.Path.GetExtension(filepath);
            if (ext == null)
            {
                return;
            }

            if (ext.ToLower() != ".csv")
            {
                return;
            }

            if (!File.Exists(filepath))
            {
                return;
            }


            // Copy file
            Suid id = Suid.NewID();
            string destFilepath = System.IO.Path.Combine(AisConf.ImportFolderPath, Suid.NewID().ToString() + ".csv");
            var name = System.IO.Path.GetFileNameWithoutExtension(filepath);
            var inputData = new CsvFile() { Id = id, Filepath = destFilepath, Name = name };

            Directory.CreateDirectory(AisConf.ImportFolderPath);
            File.Copy(filepath, inputData.Filepath);

            InputDataFileManager.Instance.Add(inputData);

            this.Ctms = new List<CtmObject>();
            this.Ctms.Add(inputData.GetCtmObject());

            var elemInfo = inputData.GetElementInfo();
            var ctmInfo = new Dictionary<string, Dictionary<string, string>>();
            ctmInfo.Add(inputData.Name, elemInfo);

            showDataGrid2(ctmInfo);


        }

        public void SetInitialDataGrid()
        {
            DataTable dt = new DataTable();
            int columnLength = 14;
            int rowLength = 6;

            // Column
            for (int i = 0; i < columnLength; i++)
            {
                string content = string.Format("Element {0}", i);
                var column = new DataColumn(content, Type.GetType("System.String"));
                dt.Columns.Add(column);
            }

            // Row
            for (int i = 0; i < rowLength; i++)
            {
                DataRow row = dt.NewRow();
                dt.Rows.Add(row);
            }

            this.dataGrid_Element.DataContext = dt.DefaultView;

            setColumn(dt);
        }


        private void setColumn(DataTable dt)
        {
            this.dataGrid_Element.Columns.Clear();
            int columnIndex = 0;
            foreach (var columnObject in dt.Columns)
            {
                DataColumn column = columnObject as DataColumn;
                DataGridTextColumn gridColumn = new DataGridTextColumn();
                if (columnIndex == 0)
                {
                    gridColumn.Header = Keywords.CTM_NAME;
                }
                else
                {
                    gridColumn.Header = string.Format("Element {0}", columnIndex);
                }
                Binding b = new Binding(column.ColumnName);
                gridColumn.Binding = b;
                this.dataGrid_Element.Columns.Add(gridColumn);

                columnIndex++;
            }

            setDataGridSize(this.cellSize);
        }



        private void getElements(string missionId)
        {
            var pkMap = new Dictionary<string, string>();

            // 設定したCTMをCTMの形(ツリー形式でグループ含む)で表現したい為、
            // CTMの取得(MMSのJson形式)とミッションCTM、ミッションエレメントの取得を行う。
            // 1リクエストで行うのはかなり複雑になるので、2リクエスト。
            // 一つ一つのリクエストはシンプルなのもプラス要素。ただ若干メソッドが長くネストが深い...

            // まずミッションCTMとミッションエレメントを取得
            CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
            client.LastInvokeCompleteHandler = true;
            client.UseMultipleRequest = true;
            client.AddParam("missionId", missionId);
            client.CompleteHandler += (resJson) =>
            {
                // 選択済みのCTMの情報
                List<MissionCtm> mcList = JsonConvert.DeserializeObject<List<MissionCtm>>(resJson);

                List<string> selectedElementIds = new List<string>();
                mcList.ForEach(mc =>
                {
                    // CTMのIDとミッションCTMのPKを取得
                    //  searchCtmIdJson.Add(CmsUtil.GetIdToken(mc.CtmId));
                    pkMap[mc.CtmId] = mc.Id;
                    mc.Elements.ForEach(mce =>
                    {
                        // エレメントのIDとミッションCTMエレメントのPKを取得
                        selectedElementIds.Add(mce.ElementId);
                        pkMap[mce.ElementId] = mce.Id;
                    });
                });

                // CTMを取得
                {
                    CmsHttpClient ctmGetClient = new CmsHttpClient(Application.Current.MainWindow);
                    ctmGetClient.UseMultipleRequest = true;
                    mcList.ForEach(mc =>
                    {
                        // CTMのIDとミッションCTMのPKを取得
                        ctmGetClient.AddParam("id", mc.CtmId);

                    });

                    ctmGetClient.CompleteHandler += (ctmJson) =>
                    {
                        this.Ctms = JsonConvert.DeserializeObject<List<CtmObject>>(ctmJson);

                        // IDからエレメント名と単位を取得
                        var ctmsData = getCtmsData();

                        // DataGridに表示
                        //ISSUE_NO.740 Sunyi 2018/07/10 Start
                        //複数のCTMを対応する
                        //showDataGrid(ctmsData);
                        showDataGrid_Mission(ctmsData);
                        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/23 Start
                        if (this.MultiStatusMonitor != null)
                        {
                        	// IDからエレメント名と単位を取得
                        	string onlyCtmJson = getCtmsDataToJson(missionId);

                            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 start
                            //条件付けエレメントのIDをJsonTreeViewに渡す
                            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 start
                            //吹き出し追加
                            //List<string> tmp = new List<string>();
                            //foreach (var condition in this.conditionLists)
                            //{
                            //    foreach (ElementConditionData a in condition.Value)
                            //    {
                            //        tmp.Add(a.Id);
                            //    }
                            //}
                            //this.MultiStatusMonitor.AisTvRefDicElement.conditionLists = tmp;
                            Dictionary<string, string> tooltips = new Dictionary<string, string>();

                            foreach (var elementCondition in this.elementConditions)
                            {
                                foreach (var element in elementCondition.Value)
                                {
                                    foreach (var conditionPattern in conditionPatterns)
                                    {
                                        if (conditionPattern.Key.Equals(elementCondition.Key))
                                        {
                                            tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                                        }
                                    }
                                }
                            }
                            this.MultiStatusMonitor.AisTvRefDicElement.conditionLists = tooltips;
                            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 end
                            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/05 end
                            this.MultiStatusMonitor.AisTvRefDicCtm.AisCheckFlag = 1;
                            this.MultiStatusMonitor.AisTvRefDicCtm.MissionNewType = 1;
                            this.MultiStatusMonitor.AisTvRefDicCtm.MissionId = missionId;
                            onlyCtmJson = onlyCtmJson.Replace("\"id\":\"", "\"id\":\"" + missionId + "-");
                            onlyCtmJson = onlyCtmJson.Replace("\"id\": \"", "\"id\":\"" + missionId + "-");
                            this.MultiStatusMonitor.AisTvRefDicCtm.JsonItemSourceAdd = onlyCtmJson;
                            this.MultiStatusMonitor.AisTvRefDicCtm.CheckChangedEvent += this.MultiStatusMonitor.AisTvRefDicCtmCheckChanged;

							this.MultiStatusMonitor.AisTvRefDicElement.AisCheckFlag = 1;
                            this.MultiStatusMonitor.AisTvRefDicElement.MissionNewType = 1;
                            this.MultiStatusMonitor.AisTvRefDicElement.MissionId = missionId;
							string currentJson = this.MultiStatusMonitor.AisTvRefDicElement.GetItemsSource() == null ? "[]" : this.MultiStatusMonitor.AisTvRefDicElement.JsonItemSource;
							ctmJson = ctmJson.Replace("\"id\":\"", "\"id\":\"" + missionId + "-");
                            ctmJson = ctmJson.Replace("\"id\": \"", "\"id\":\"" + missionId + "-");
							this.MultiStatusMonitor.AisTvRefDicElement.JsonItemSourceAdd = ctmJson;
							__getElements_shrinkAisTvRefDicElement(missionId, this.MultiStatusMonitor.AisTvRefDicElement, currentJson);
                            this.MultiStatusMonitor.AisTvRefDicElement.CheckChangedEvent += this.MultiStatusMonitor.AisTvRefDicElementCheckChanged;
                        }
                        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/23 End
                        //ISSUE_NO.740 Sunyi 2018/07/10 End

                        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/23 Start
                        //AIS_Workplace merge sunyi 2018/10/19 start
                        else if (this.WorkPlace != null)
                        //AIS_Workplace merge sunyi 2018/10/19 end
                        {
                            // IDからエレメント名と単位を取得
                            string onlyCtmJson = getCtmsDataToJson(missionId);
                            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 start
                            //吹き出し追加
                            Dictionary<string, string> tooltips = new Dictionary<string, string>();

                            foreach (var elementCondition in this.elementConditions)
                            {
                                foreach (var element in elementCondition.Value)
                                {
                                    foreach (var conditionPattern in conditionPatterns)
                                    {
                                        if (conditionPattern.Key.Equals(elementCondition.Key))
                                        {
                                            tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                                        }
                                    }
                                }
                            }
                            this.WorkPlace.AisTvRefDicElement.conditionLists = tooltips;
                            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 end
                            this.WorkPlace.AisTvRefDicCtm.AisCheckFlag = 1;
                            this.WorkPlace.AisTvRefDicCtm.MissionNewType = 1;
                            this.WorkPlace.AisTvRefDicCtm.MissionId = missionId;
                            onlyCtmJson = onlyCtmJson.Replace("\"id\":\"", "\"id\":\"" + missionId + "-");
                            onlyCtmJson = onlyCtmJson.Replace("\"id\": \"", "\"id\":\"" + missionId + "-");
                            this.WorkPlace.AisTvRefDicCtm.JsonItemSourceAdd = onlyCtmJson;
                            this.WorkPlace.AisTvRefDicCtm.CheckChangedEvent += this.WorkPlace.AisTvRefDicCtmCheckChanged;

                            this.WorkPlace.AisTvRefDicElement.AisCheckFlag = 1;
                            this.WorkPlace.AisTvRefDicElement.MissionNewType = 1;
                            this.WorkPlace.AisTvRefDicElement.MissionId = missionId;
							string currentJson = this.WorkPlace.AisTvRefDicElement.GetItemsSource() == null ? "[]" : this.WorkPlace.AisTvRefDicElement.JsonItemSource;
							ctmJson = ctmJson.Replace("\"id\":\"", "\"id\":\"" + missionId + "-");
                            ctmJson = ctmJson.Replace("\"id\": \"", "\"id\":\"" + missionId + "-");
							this.WorkPlace.AisTvRefDicElement.JsonItemSourceAdd = ctmJson;
							__getElements_shrinkAisTvRefDicElement(missionId, this.WorkPlace.AisTvRefDicElement, currentJson);
							this.WorkPlace.AisTvRefDicElement.CheckChangedEvent += this.WorkPlace.AisTvRefDicElementCheckChanged;
                        }
                        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/23 End
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 Start
                        // Multi_Dac 修正
                        if (this.DAC != null )
                        {
                            // IDからエレメント名と単位を取得
                            string onlyCtmJson = getCtmsDataToJson(missionId);
                            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 start
                            //吹き出し追加
                            Dictionary<string, string> tooltips = new Dictionary<string, string>();

                            foreach (var elementCondition in this.elementConditions)
                            {
                                foreach (var element in elementCondition.Value)
                                {
                                    foreach (var conditionPattern in conditionPatterns)
                                    {
                                        if (conditionPattern.Key.Equals(elementCondition.Key))
                                        {
                                            tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                                        }
                                    }
                                }
                            }
                            this.DAC.AisTvRefDicElement.conditionLists = tooltips;
                            // FOA_サーバー化開発 Processing On Server sunyi 2018/10/15 end
                            this.DAC.AisTvRefDicCtm.AisCheckFlag = 1;
                            this.DAC.AisTvRefDicCtm.MissionNewType = 1;
                            this.DAC.AisTvRefDicCtm.MissionId = missionId;
                            onlyCtmJson = onlyCtmJson.Replace("\"id\":\"", "\"id\":\"" + missionId + "-");
                            onlyCtmJson = onlyCtmJson.Replace("\"id\": \"", "\"id\":\"" + missionId + "-");
                            this.DAC.AisTvRefDicCtm.JsonItemSourceAdd = onlyCtmJson;
                            this.DAC.AisTvRefDicCtm.CheckChangedEvent += this.DAC.AisTvRefDicCtmCheckChanged;

                            this.DAC.AisTvRefDicElement.AisCheckFlag = 1;
                            this.DAC.AisTvRefDicElement.MissionNewType = 1;
                            this.DAC.AisTvRefDicElement.MissionId = missionId;
							string currentJson = this.DAC.AisTvRefDicElement.GetItemsSource() == null ? "[]" : this.DAC.AisTvRefDicElement.JsonItemSource;
							ctmJson = ctmJson.Replace("\"id\":\"", "\"id\":\"" + missionId + "-");
                            ctmJson = ctmJson.Replace("\"id\": \"", "\"id\":\"" + missionId + "-");
                            this.DAC.AisTvRefDicElement.JsonItemSourceAdd = ctmJson;
							__getElements_shrinkAisTvRefDicElement(missionId, this.DAC.AisTvRefDicElement, currentJson);
							this.DAC.AisTvRefDicElement.CheckChangedEvent += this.DAC.AisTvRefDicElementCheckChanged;
                        }
                        // FOA_サーバー化開発 Processing On Server sunyi 2018/10/17 end
                    };

                    ctmGetClient.Get(CmsUrlMms.Ctm.List());
                }
            };

            client.Get(CmsUrl.GetMissionCtmUrl());
        }
		void __getElements_shrinkAisTvRefDicElement(string missionId, FoaCore.Common.Control.CheckBoxJsonTreeView treeView, string currentJson)
		{
			FoaCore.Common.Control.CheckBoxJsonTreeView currentTreeView = new FoaCore.Common.Control.CheckBoxJsonTreeView();
			currentTreeView.JsonItemSource = currentJson;

			List<string> missionIdelementIds = new List<string>();
			foreach (MissionCtm missionCtm in this.missionCtms)
			{
				foreach (MissionCtmElement element in missionCtm.Elements)
				{
					missionIdelementIds.Add(missionId + "-" + element.ElementId);
				}
			}

			List<string> deleteElementTreeViewIds = new List<string>();
			foreach (TreeViewItem elementTreeView in treeView.GetAllItems())
			{
				JToken elementTreeViewToken = elementTreeView.DataContext as JToken;
				if (elementTreeViewToken != null && elementTreeViewToken["type"] != null)
				{
					string elementTreeViewTokenId = (string)elementTreeViewToken["id"];
					string elementTreeViewTokenType = (string)elementTreeViewToken["type"];
					if (elementTreeViewTokenType == NodeType.Element && currentTreeView.GetItem(elementTreeViewTokenId) == null && missionIdelementIds.Contains(elementTreeViewTokenId) == false)
					{
						deleteElementTreeViewIds.Add(elementTreeViewTokenId);
					}
				}
			}
			foreach (string elementTreeViewTokenId in deleteElementTreeViewIds)
			{
				treeView.DeleteItem(elementTreeViewTokenId);
			}

			List<string> deleteRefNodeTreeViewItemIds = new List<string>();
			foreach (TreeViewItem refNodeTreeViewItem in treeView.GetAllItems())
			{
				bool hasElementTreeViewItem = __getElements_existsCtmElementTreeViewItem(refNodeTreeViewItem);
				if (hasElementTreeViewItem == false)
				{
					JToken refNodeTreeViewToken = refNodeTreeViewItem.DataContext as JToken;
					if (refNodeTreeViewToken != null)
					{
						deleteRefNodeTreeViewItemIds.Add((string)refNodeTreeViewToken["id"]);
					}
				}
			}
			foreach (string refNodeTreeViewTokenId in deleteRefNodeTreeViewItemIds)
			{
				if (treeView.GetTreeViewItem(refNodeTreeViewTokenId) != null)
				{
					treeView.DeleteItem(refNodeTreeViewTokenId);
				}
			}
		}
		bool __getElements_existsCtmElementTreeViewItem(TreeViewItem item)
		{
			if (item == null) return false;

			JToken token = item.DataContext as JToken;
			if (token != null && token["type"] != null)
			{
				string type = (string)token["type"];
				if (type == NodeType.Element)
				{
					return true;
				}
			}

			bool hasElementTreeViewItem = false;
			foreach (TreeViewItem childItem in item.Items)
			{
				bool resultChildItem = __getElements_existsCtmElementTreeViewItem(childItem);
				if (resultChildItem == true)
				{
					hasElementTreeViewItem = true;
					break;
				}
			}

			return hasElementTreeViewItem;
		}

        private void getElements_Grip(GripMissionCtm result)
        {
            List<string> ctmIdList = new List<string>();
            //E2NaviUI sunyi 20190305 start
            //MISSION条件対応
            if (result.StartNodes == null)
            {
                ctmIdList.Add(result.StartNode.CtmId);
            }
            else
            {
                foreach (var startNode in result.StartNodes)
                {
                    ctmIdList.Add(startNode.Value.CtmId);
                }
            }
            //E2NaviUI sunyi 20190305 end

            HashSet<string> branchNodeIds = new HashSet<string>();
            if (result.BranchNodes != null)
            {
                foreach (Node node in result.BranchNodes) {
                    branchNodeIds.Add(node.CtmId);
                }
            }

            // Wang Issue AISTEMP-122 20181228 Start
            //if (result.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace && result.TraceNodes != null)
            if ((result.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace || result.SearchType == FoaCore.Model.GripR2.Search.SearchType.Period) && result.TraceNodes != null)
            // Wang Issue AISTEMP-122 20181228 End
            {
                foreach (TraceNodeObject traceNode in result.TraceNodes)
                {
                    // Wang Issue AISTEMP-122 20181228 Start
                    //if (branchNodeIds.Contains(traceNode.CtmId) && traceNode.ElementIds != null && traceNode.ElementIds.Count > 0)

                    // 以下の !branch... の部分を外すと、branch の項目が入るようになる。 iwai
                    if (!branchNodeIds.Contains(traceNode.CtmId) && traceNode.ElementIds != null && traceNode.ElementIds.Count > 0)
                    // Wang Issue AISTEMP-122 20181228 End
                    {
                        ctmIdList.Add(traceNode.CtmId);
                    }
                }
            }
            foreach (var endNode in result.EndNodes)
            {
                ctmIdList.Add(endNode.CtmId);
            }

            //AISMM No.80 sunyi 20181211 start
            //GripMission条件表示
            GetElementConditionDatas(result);
            //AISMM No.80 sunyi 20181211 end
            CmsHttpClient client2 = new CmsHttpClient(Application.Current.MainWindow);
            client2.LastInvokeCompleteHandler = true;
            client2.UseMultipleRequest = true;
            foreach (string ctmId in ctmIdList)
            {
                client2.AddParam("id", ctmId);
            }

            client2.CompleteHandler += resJson =>
            {
                List<CtmObject> ctmObjectList = JsonConvert.DeserializeObject<List<CtmObject>>(resJson);
                this.Ctms = ctmObjectList;
                MainWindow mainWindow = Application.Current.MainWindow as MainWindow;

                // IDからエレメント名と単位を取得
                try
                {
                    var ctmsData = getCtmsData_Grip(result);
                    //ISSUE_NO.778 Sunyi 2018/07/23 Start
                    //G-missionの条件設定の色付く
                    // DataGridに表示
                    //showDataGrid(ctmsData);
                    showDataGrid_Mission(ctmsData);
                    //ISSUE_NO.778 Sunyi 2018/07/23 End
                    mainWindow.CurrentEditCtrl.DataSource = DataSourceType.GRIP;
                    // FOA_サーバー化開発 Processing On Server Dcs 2018/07/23 Start
                    if (this.MultiStatusMonitor != null)
                    {
                        //Gripデータを再作成
                        if (string.IsNullOrEmpty(resJson)) resJson = "[]";
                        JToken rootToken = JToken.Parse(resJson);

                        JArray rootJsons;
                        if (rootToken is JArray)
                        {
                            rootJsons = (JArray)rootToken;
                        }
                        else
                        {
                            rootJsons = new JArray();
                            rootJsons.Add(rootToken);
                        }

                        foreach (JToken token in rootJsons)
                        {
                            if (token.HasValues == false) continue;
                            JArray childrenGroup = (JArray)token["children"];

                            for (int i = childrenGroup.Count - 1; 0 <= i; i--)
                            {
								JToken childToken = (JToken)childrenGroup[i];
								JArray childrenElements = (JArray)childToken["children"];

								if (childrenElements != null)
								{
									for (int j = childrenElements.Count - 1; 0 <= j; j--)
									{
										JToken elToken = childrenElements[j];
										if (!hasElements(elToken, result))
										{
											childrenElements.RemoveAt(j);
										}
									}

									if (childrenElements.Count == 0)
									{
										childrenGroup.RemoveAt(i);
									}

								}
								else
								{
									if (!hasElements(childToken, result))
									{
										childrenGroup.RemoveAt(i);
									}
								}

                            }
                        }
                        //AISMM No.80 sunyi 20181211 start
                        //GripMission条件表示
                        Dictionary<string, string> tooltips = new Dictionary<string, string>();
                        foreach (var elementCondition in this.elementConditions_Grip)
                        {
                            foreach (var element in elementCondition.Value)
                            {
                                foreach (var conditionPattern in conditionPatterns)
                                {
                                    if (conditionPattern.Key.Equals(elementCondition.Key))
                                    {
                                        tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                                    }
                                }
                            }
                        }
                        this.MultiStatusMonitor.AisTvRefDicElement.conditionLists = tooltips;
                        //AISMM No.80 sunyi 20181211 end
                        this.MultiStatusMonitor.AisTvRefDicElement.AisCheckFlag = 1;
                        this.MultiStatusMonitor.AisTvRefDicElement.MissionNewType = 0;
                        this.MultiStatusMonitor.AisTvRefDicElement.MissionId = result.Id;
                        resJson = rootJsons.ToString();
                        resJson = resJson.Replace("\"id\":\"", "\"id\":\"" + result.Id + "-");
                        resJson = resJson.Replace("\"id\": \"", "\"id\":\"" + result.Id + "-");
                        this.MultiStatusMonitor.AisTvRefDicElement.JsonItemSourceAdd = resJson;
                        this.MultiStatusMonitor.AisTvRefDicElement.CheckChangedEvent += this.MultiStatusMonitor.AisTvRefDicElementCheckChanged;
                    }
                    //AIS_Workplace merge sunyi 2018/10/19 start
                    else if (this.WorkPlace != null)
                    {
                        //Gripデータを再作成
                        if (string.IsNullOrEmpty(resJson)) resJson = "[]";
                        JToken rootToken = JToken.Parse(resJson);

                        JArray rootJsons;
                        if (rootToken is JArray)
                        {
                            rootJsons = (JArray)rootToken;
                        }
                        else
                        {
                            rootJsons = new JArray();
                            rootJsons.Add(rootToken);
                        }

                        foreach (JToken token in rootJsons)
                        {
                            if (token.HasValues == false) continue;
                            JArray childrenGroup = (JArray)token["children"];

                            for (int i = childrenGroup.Count - 1; 0 <= i; i--)
                            {
								JToken childToken = (JToken)childrenGroup[i];
								JArray childrenElements = (JArray)childToken["children"];

								if (childrenElements != null)
								{
									for (int j = childrenElements.Count - 1; 0 <= j; j--)
									{
										JToken elToken = childrenElements[j];
										if (!hasElements(elToken, result))
										{
											childrenElements.RemoveAt(j);
										}
									}

									if (childrenElements.Count == 0)
									{
										childrenGroup.RemoveAt(i);
									}

								}
								else
								{
									if (!hasElements(childToken, result))
									{
										childrenGroup.RemoveAt(i);
									}
								}

							}
                        }
                        //AISMM No.80 sunyi 20181211 start
                        //GripMission条件表示
                        Dictionary<string, string> tooltips = new Dictionary<string, string>();
                        foreach (var elementCondition in this.elementConditions_Grip)
                        {
                            foreach (var element in elementCondition.Value)
                            {
                                foreach (var conditionPattern in conditionPatterns)
                                {
                                    if (conditionPattern.Key.Equals(elementCondition.Key))
                                    {
                                        tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                                    }
                                }
                            }
                        }
                        this.WorkPlace.AisTvRefDicElement.conditionLists = tooltips;
                        //AISMM No.80 sunyi 20181211 end
                        this.WorkPlace.AisTvRefDicElement.AisCheckFlag = 1;
                        this.WorkPlace.AisTvRefDicElement.MissionNewType = 0;
                        this.WorkPlace.AisTvRefDicElement.MissionId = result.Id;
                        resJson = rootJsons.ToString();
                        resJson = resJson.Replace("\"id\":\"", "\"id\":\"" + result.Id + "-");
                        resJson = resJson.Replace("\"id\": \"", "\"id\":\"" + result.Id + "-");
                        this.WorkPlace.AisTvRefDicElement.JsonItemSourceAdd = resJson;
                        this.WorkPlace.AisTvRefDicElement.CheckChangedEvent += this.WorkPlace.AisTvRefDicElementCheckChanged;
                    }
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 Start
                    // Multi_Dac 修正
                        // TODO: iwai ここから、DACのバグの修正
                    else if (this.DAC != null)
                    {
                        //Gripデータを再作成
                        if (string.IsNullOrEmpty(resJson)) resJson = "[]";
                        JToken rootToken = JToken.Parse(resJson);

                        JArray rootJsons;
                        if (rootToken is JArray)
                        {
                            rootJsons = (JArray)rootToken;
                        }
                        else
                        {
                            rootJsons = new JArray();
                            rootJsons.Add(rootToken);
                        }

                        foreach (JToken token in rootJsons)
                        {
                            if (token.HasValues == false) continue;
                            JArray childrenGroup = (JArray)token["children"];

                            for (int i = childrenGroup.Count - 1; 0 <= i; i--)
                            {
								JToken childToken = (JToken)childrenGroup[i];
								JArray childrenElements = (JArray)childToken["children"];

								if (childrenElements != null)
								{
									for (int j = childrenElements.Count - 1; 0 <= j; j--)
									{
										JToken elToken = childrenElements[j];
										if (!hasElements(elToken, result))
										{
											childrenElements.RemoveAt(j);
										}
									}

									if (childrenElements.Count == 0)
									{
										childrenGroup.RemoveAt(i);
									}

								}
								else
								{
									if (!hasElements(childToken, result))
									{
										childrenGroup.RemoveAt(i);
									}
								}

							}
                        }
                        //AISMM No.80 sunyi 20181211 start
                        //GripMission条件表示
                        Dictionary<string, string> tooltips = new Dictionary<string, string>();
                        foreach (var elementCondition in this.elementConditions_Grip)
                        {
                            foreach (var element in elementCondition.Value)
                            {
                                foreach (var conditionPattern in conditionPatterns)
                                {
                                    if (conditionPattern.Key.Equals(elementCondition.Key))
                                    {
                                        tooltips.Add(element.Key.Id.ToString(), (getConditionString(elementCondition.Value, conditionPattern.Value)));
                                    }
                                }
                            }
                        }
                        this.DAC.AisTvRefDicElement.conditionLists = tooltips;
                        //AISMM No.80 sunyi 20181211 end
                        this.DAC.AisTvRefDicElement.AisCheckFlag = 1;
                        this.DAC.AisTvRefDicElement.MissionNewType = 0;
                        this.DAC.AisTvRefDicElement.MissionId = result.Id;
                        resJson = rootJsons.ToString();
                        resJson = resJson.Replace("\"id\":\"", "\"id\":\"" + result.Id + "-");
                        resJson = resJson.Replace("\"id\": \"", "\"id\":\"" + result.Id + "-");

                        // TODO: 以下は、workplaceの画面の３つの列の右側の部分のリストを作るところ。それとハンドラー
                        this.DAC.AisTvRefDicElement.JsonItemSourceAdd = resJson;
                        this.DAC.AisTvRefDicElement.CheckChangedEvent += this.DAC.AisTvRefDicElementCheckChanged;
                    }
                    // FOA_サーバー化開発 Processing On Server sunyi 2018/11/12 end
                }
                catch (GripMissionHasNoElementException)
                {
                    mainWindow.CurrentEditCtrl.DataSource =  DataSourceType.NONE;
                    FoaMessageBox.ShowError("AIS_E_043");
                    SetInitialDataGrid();
                }

            };
            client2.Get(CmsUrlMms.Ctm.List());
        }

        private Dictionary<string, Dictionary<string, string>> getCtmsData()
        {
            var ctmData = new Dictionary<string, Dictionary<string, string>>();

            foreach (CtmObject ctm in this.Ctms)
            {
                foreach (MissionCtm missionCtm in this.missionCtms)
                {
                    //workplace sunyi 2018/10/11 start
                    if (missionCtm.CtmId != ctm.id.ToString())
                    {
                        continue;
                    }
                    //workplace sunyi 2018/10/11 end

                    var elementData = new Dictionary<string, string>();
                    foreach (DAC.Model.CtmElement element in ctm.GetAllElements())
                    {
                        foreach (MissionCtmElement missionElement in missionCtm.Elements)
                        {
                            if (missionElement.ElementId == element.id.ToString())
                            {
                                elementData.Add(element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), element.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                break;
                            }
                        }
                    }

                    ctmData.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), elementData);
                    break;
                }
            }

            return ctmData;
        }

        private Dictionary<string, Dictionary<string, string>> getCtmsData_Grip(GripMissionCtm gripMission)
        {
            var ctmData = new Dictionary<string, Dictionary<string, string>>();

            foreach (CtmObject ctm in this.Ctms)
            {
                //E2NaviUI sunyi 20190305 start
                //MISSION条件対応
                if (gripMission.StartNodes == null)
                {
                    if (ctm.id.ToString() == gripMission.StartNode.CtmId)
                    {
                        var elementData = new Dictionary<string, string>();

                        if (gripMission.StartNode.ElementIds == null)
                        {
                            ctmData.Clear();
                            throw new GripMissionHasNoElementException();
                        }

                        foreach (string selectedElementId in gripMission.StartNode.ElementIds)
                        {
                            foreach (DAC.Model.CtmElement element in ctm.GetAllElements())
                            {
                                if (selectedElementId == element.id.ToString())
                                {
                                    elementData.Add(element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), element.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                    break;
                                }
                            }
                        }

                        ctmData.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), elementData);
                    }
                }
                else
                {
                    foreach (var startNode in gripMission.StartNodes)
                    //ISSUE_NO.778 Sunyi 2018/07/23 End
                    {
                        if (ctm.id.ToString() == startNode.Value.CtmId)
                        {
                            var elementData = new Dictionary<string, string>();
                            if (startNode.Value.ElementIds == null)
                            {
                                ctmData.Clear();
                                throw new GripMissionHasNoElementException();
                            }

                            foreach (string selectedElementId in startNode.Value.ElementIds)
                            {
                                foreach (DAC.Model.CtmElement element in ctm.GetAllElements())
                                {
                                    if (selectedElementId == element.id.ToString())
                                    {
                                        elementData.Add(element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), element.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                        break;
                                    }
                                }
                            }

                            ctmData.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), elementData);
                        }
                    }
                }
                //E2NaviUI sunyi 20190305 end

                if ((gripMission.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace || gripMission.SearchType == FoaCore.Model.GripR2.Search.SearchType.Period) && gripMission.TraceNodes != null)
                {
                    //ISSUE_NO.778 Sunyi 2018/07/23 Start
                    //G-missionの条件設定の色付く
                    //foreach (Node traceNode in gripMission.TraceNodes)
                    foreach (TraceNodeObject traceNode in gripMission.TraceNodes)
                    //ISSUE_NO.778 Sunyi 2018/07/23 End
                    {
                        if (ctm.id.ToString() == traceNode.CtmId)
                        {
                            //ISSUE_NO.778 Sunyi 2018/07/23 Start
                            //G-missionの条件設定の色付く
                            if(traceNode.ElementIds != null)
                            //ISSUE_NO.778 Sunyi 2018/07/23 End
                            {
                                var elementData = new Dictionary<string, string>();
                                foreach (string selectedElementId in traceNode.ElementIds)
                                {
                                    foreach (DAC.Model.CtmElement element in ctm.GetAllElements())
                                    {
                                        if (selectedElementId == element.id.ToString())
                                        {
                                            elementData.Add(element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), element.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                            break;
                                        }
                                    }
                                }
                                ctmData.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), elementData);
                            }
                        }
                    }
                }
                //ISSUE_NO.778 Sunyi 2018/07/23 Start
                //G-missionの条件設定の色付く
                //foreach (Node endNode in gripMission.EndNodes)
                foreach (EndNodeObject endNode in gripMission.EndNodes)
                //ISSUE_NO.778 Sunyi 2018/07/23 End
                {
                    if (ctm.id.ToString() == endNode.CtmId)
                    {
                        var elementData = new Dictionary<string, string>();
                        if (endNode.ElementIds == null)
                        {
                            ctmData.Clear();
                            throw new GripMissionHasNoElementException();
                        }

                        foreach (string selectedElementId in endNode.ElementIds)
                        {
                            foreach (DAC.Model.CtmElement element in ctm.GetAllElements())
                            {
                                if (selectedElementId == element.id.ToString())
                                {
                                    elementData.Add(element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), element.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                    break;
                                }
                            }
                        }

                        ctmData.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), elementData);
                    }
                }
            }

            return ctmData;
        }

        /// <summary>
        /// Dic<CTM名, Dic<エレメント名, 単位>>
        /// </summary>
        /// <param name="ctmList"></param>
        /// <returns></returns>
        private Dictionary<string, Dictionary<string, string>> getCtmsData2(List<CtmObject> ctmList)
        {
            var ctmData = new Dictionary<string, Dictionary<string, string>>();

            foreach (CtmObject ctm in ctmList)
            {
                var elementData = new Dictionary<string, string>();
                foreach (DAC.Model.CtmElement element in ctm.GetAllElements())
                {
                    elementData.Add(element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), element.unit.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                }

                ctmData.Add(ctm.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true), elementData);
            }

            return ctmData;
        }

        //ISSUE_NO.740 Sunyi 2018/07/10 Start
        //複数のCTMを対応する
        private void showDataGrid_Mission(Dictionary<string, Dictionary<string, string>> missionCtmsData)
        {
            this.dataGrid_Element.Columns.Clear();

            DataGridTextColumn header = new DataGridTextColumn();
            header.Header = Keywords.CTM_NAME;
            header.IsReadOnly = false;
            header.Binding = new Binding(Keywords.CTM_NAME);
            this.dataGrid_Element.Columns.Add(header);

            int length = 0;
            foreach (var item in missionCtmsData.Values)
            {
                if (length < item.Count)
                {
                    length = item.Count;
                }
            }

            for (int i = 0; i < length; i++)
            {
                DataGridTextColumn column = new DataGridTextColumn();
                column.Header = string.Format("Element {0}", i + 1);
                column.IsReadOnly = false;
                column.Binding = new Binding(string.Format("Element {0}", i + 1));
                this.dataGrid_Element.Columns.Add(column);
            }

            try
            {
                //ISSUE_NO.740 Sunyi 2018/07/10 Start
                //複数のCTMを対応する
                //this.Dt = createDataTable(missionCtmsData);
                this.Dt = createDataTableById(missionCtmsData);
                //setElementNameWithCondition(this.conditionList);
                setElementNameWithCondition_Mission(this.conditionLists);
                //AISMM No.80 sunyi 20181211 start
                //GripMission条件表示
                setElementNameWithCondition_Grip(this.conditionLists_Grip);
                //AISMM No.80 sunyi 20181211 end
                //条件あるエレメントの位置を取得
                GetSettingPosition();

                this.Dt = createDataTable(missionCtmsData);

                this.dataGrid_Element.Visibility = Visibility.Visible;
                this.dataGrid_Element.DataContext = this.Dt.DefaultView;

                setCell_CTM();
                //ISSUE_NO.740 Sunyi 2018/07/10 End
            }
#pragma warning disable
            catch (Exception)
#pragma warning restore
            {
                // int x = 333;
            }


        }
        //ISSUE_NO.740 Sunyi 2018/07/10 End

        private void showDataGrid(Dictionary<string, Dictionary<string, string>> missionCtmsData)
        {
            this.dataGrid_Element.Columns.Clear();

            DataGridTextColumn header = new DataGridTextColumn();
            header.Header = Keywords.CTM_NAME;
            header.IsReadOnly = false;
            header.Binding = new Binding(Keywords.CTM_NAME);
            this.dataGrid_Element.Columns.Add(header);

            int length = 0;
            foreach (var item in missionCtmsData.Values)
            {
                if (length < item.Count)
                {
                    length = item.Count;
                }
            }

            for (int i = 0; i < length; i++)
            {
                DataGridTextColumn column = new DataGridTextColumn();
                column.Header = string.Format("Element {0}", i + 1);
                column.IsReadOnly = false;
                column.Binding = new Binding(string.Format("Element {0}", i + 1));
                this.dataGrid_Element.Columns.Add(column);
            }

            try
            {
                this.Dt = createDataTable(missionCtmsData);
                setElementNameWithCondition(this.conditionList);

                this.dataGrid_Element.Visibility = Visibility.Visible;
                this.dataGrid_Element.DataContext = this.Dt.DefaultView;

                setCell();
            }
#pragma warning disable
            catch (Exception)
#pragma warning restore
            {
                int x = 333;
            }


        }

        private void showDataGrid2(Dictionary<string, Dictionary<string, string>> missionCtmsData)
        {
            this.dataGrid_Element.Columns.Clear();

            DataGridTextColumn header = new DataGridTextColumn();
            header.Header = "CTM名称";
            header.IsReadOnly = false;
            header.Binding = new Binding("CTM名称");
            this.dataGrid_Element.Columns.Add(header);

            int length = 0;
            foreach (var item in missionCtmsData.Values)
            {
                if (length < item.Count)
                {
                    length = item.Count;
                }
            }

            for (int i = 0; i < length; i++)
            {
                DataGridTextColumn column = new DataGridTextColumn();
                column.Header = string.Format("Element {0}", i + 1);
                column.IsReadOnly = false;
                column.Binding = new Binding(string.Format("Element {0}", i + 1));
                this.dataGrid_Element.Columns.Add(column);
            }

            try
            {
                this.Dt = createDataTable2(missionCtmsData);
                setElementNameWithCondition(this.conditionList);

                this.dataGrid_Element.Visibility = Visibility.Visible;
                this.dataGrid_Element.DataContext = this.Dt.DefaultView;

                setCell();
            }
            catch (Exception)
            {

            }

            setDataGridSize(this.cellSize);
        }

        private DataTable createDataTable(Dictionary<string, Dictionary<string, string>> missionCtmsData)
        {
            int length = 0;
            foreach (var item in missionCtmsData.Values)
            {
                if (length < item.Count)
                {
                    length = item.Count;
                }
            }

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(Keywords.CTM_NAME, typeof(string)));
            for (int i = 0; i < length; i++)
            {
                dt.Columns.Add(new DataColumn(string.Format("Element {0}", i + 1), typeof(string)));
            }

            foreach (var ctm in missionCtmsData)
            {
                DataRow row1 = dt.NewRow();
                row1[Keywords.CTM_NAME] = ctm.Key;

                List<string> namesRow = new List<string>();
                foreach (var elementName in ctm.Value.Keys)
                {
                    // エレメント名
                    namesRow.Add(elementName);
                }

                for (int i = 0; i < namesRow.Count; i++)
                {
                    string columnName = string.Format("Element {0}", i + 1);
                    row1[columnName] = namesRow[i];
                }

                dt.Rows.Add(row1);

                DataRow row2 = dt.NewRow();
                row2[Keywords.CTM_NAME] = Properties.Resources.CTM_UNIT;

                List<string> unitsRow = new List<string>();
                foreach (var elementName in ctm.Value.Keys)
                {
                    string elementUnit = ctm.Value[elementName];

                    bool isBulky = false;
                    int bulkysubType = 0;
                    foreach(var ctmObject in this.Ctms)
                    {
                        // CTM名で検索
                        if (ctmObject.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != ctm.Key)
                        {
                            continue;
                        }

                        foreach (var element in ctmObject.GetAllElements())
                        {
                            // エレメント名で検索
                            if (element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != elementName)
                            {
                                continue;
                            }

                            // Bulky
                            if (element.datatype == FoaDatatype.BULKY)
                            {
                                bulkysubType = element.bulkysubtype;
                                isBulky = true;
                                break;
                            }
                        }

                        break;
                    }

                    // 単位
                    if (isBulky)
                    {
                        switch (bulkysubType)
                        {
                            case FoaDatatype.LINKBULKY:
                                unitsRow.Add("LinkBulky");
                                break;
                            case FoaDatatype.VIDEOFOA:
                                unitsRow.Add("VideoFOA");
                                break;
                            case FoaDatatype.NORMALBULKY:
                            default:
                                unitsRow.Add("Bulky");

                                break;
                        }
                        continue;
                    }

                    unitsRow.Add(elementUnit);
                }

                for (int i = 0; i < unitsRow.Count; i++)
                {
                    string columnName = string.Format("Element {0}", i + 1);
                    row2[columnName] = unitsRow[i];
                }

                dt.Rows.Add(row2);
            }

            return dt;
        }
        //ISSUE_NO.740 Sunyi 2018/07/10 Start
        //複数のCTMを対応する
        private DataTable createDataTableById(Dictionary<string, Dictionary<string, string>> missionCtmsData)
        {
            int length = 0;
            foreach (var item in missionCtmsData.Values)
            {
                if (length < item.Count)
                {
                    length = item.Count;
                }
            }

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn(Keywords.CTM_NAME, typeof(string)));
            for (int i = 0; i < length; i++)
            {
                dt.Columns.Add(new DataColumn(string.Format("Element {0}", i + 1), typeof(string)));
            }
            //ISSUE_NO.740 Sunyi 2018/07/10 Start
            //複数のCTMを対応する
            int index = 0;
            //ISSUE_NO.740 Sunyi 2018/07/10 End
            foreach (var ctm in missionCtmsData)
            {
                DataRow row1 = dt.NewRow();
                row1[Keywords.CTM_NAME] = ctm.Key;

                List<string> namesRow = new List<string>();
                //ISSUE_NO.740 Sunyi 2018/07/10 Start
                //複数のCTMを対応する
                //foreach (var elementName in ctm.Value.Keys)
                foreach (var element in this.Ctms[index].GetAllElements())
                //ISSUE_NO.740 Sunyi 2018/07/10 End
                {
                    // エレメント名
                    //ISSUE_NO.740 Sunyi 2018/07/10 Start
                    //複数のCTMを対応する
                    //namesRow.Add(elementName);
                    foreach (var elementName in ctm.Value.Keys)
                    {
                        if (elementName.Equals(element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true)))
                        {
                            namesRow.Add(element.id.ToString());
                        }
                    }
                    //ISSUE_NO.740 Sunyi 2018/07/10 End
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 Start
                //複数のCTMを対応する
                index++;
                //ISSUE_NO.740 Sunyi 2018/07/10 End
                for (int i = 0; i < namesRow.Count; i++)
                {
                    string columnName = string.Format("Element {0}", i + 1);
                    row1[columnName] = namesRow[i];
                }

                dt.Rows.Add(row1);

                DataRow row2 = dt.NewRow();
                row2[Keywords.CTM_NAME] = Properties.Resources.CTM_UNIT;

                List<string> unitsRow = new List<string>();
                foreach (var elementName in ctm.Value.Keys)
                {
                    string elementUnit = ctm.Value[elementName];

                    bool isBulky = false;
                    int bulkysubType = 0;
                    foreach (var ctmObject in this.Ctms)
                    {
                        // CTM名で検索
                        if (ctmObject.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != ctm.Key)
                        {
                            continue;
                        }

                        foreach (var element in ctmObject.GetAllElements())
                        {
                            // エレメント名で検索
                            if (element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true) != elementName)
                            {
                                continue;
                            }

                            // Bulky
                            if (element.datatype == FoaDatatype.BULKY)
                            {
                                bulkysubType = element.bulkysubtype;
                                isBulky = true;
                                break;
                            }
                        }

                        break;
                    }

                    // 単位
                    if (isBulky)
                    {
                        switch (bulkysubType)
                        {
                            case FoaDatatype.LINKBULKY:
                                unitsRow.Add("LinkBulky");
                                break;
                            case FoaDatatype.VIDEOFOA:
                                unitsRow.Add("VideoFOA");
                                break;
                            case FoaDatatype.NORMALBULKY:
                            default:
                                unitsRow.Add("Bulky");

                                break;
                        }
                        continue;
                    }

                    unitsRow.Add(elementUnit);
                }

                for (int i = 0; i < unitsRow.Count; i++)
                {
                    string columnName = string.Format("Element {0}", i + 1);
                    row2[columnName] = unitsRow[i];
                }

                dt.Rows.Add(row2);
            }

            return dt;
        }
        //ISSUE_NO.740 Sunyi 2018/07/10 End

        private DataTable createDataTable2(Dictionary<string, Dictionary<string, string>> missionCtmsData)
        {
            int length = 0;
            foreach (var item in missionCtmsData.Values)
            {
                if (length < item.Count)
                {
                    length = item.Count;
                }
            }

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("CTM名称", typeof(string)));
            for (int i = 0; i < length; i++)
            {
                dt.Columns.Add(new DataColumn(string.Format("Element {0}", i + 1), typeof(string)));
            }

            foreach (var ctm in missionCtmsData)
            {
                DataRow row1 = dt.NewRow();
                row1["CTM名称"] = ctm.Key;

                List<string> namesRow = new List<string>();
                foreach (var elementName in ctm.Value.Keys)
                {
                    // エレメント名
                    namesRow.Add(elementName);
                }

                for (int i = 0; i < namesRow.Count; i++)
                {
                    string columnName = string.Format("Element {0}", i + 1);
                    row1[columnName] = namesRow[i];
                }

                dt.Rows.Add(row1);

                DataRow row2 = dt.NewRow();
                row2["CTM名称"] = Properties.Resources.CTM_UNIT;

                List<string> unitsRow = new List<string>();
                foreach (var elementName in ctm.Value.Keys)
                {
                    string elementUnit = ctm.Value[elementName];
                    unitsRow.Add(elementUnit);
                }

                for (int i = 0; i < unitsRow.Count; i++)
                {
                    string columnName = string.Format("Element {0}", i + 1);
                    row2[columnName] = unitsRow[i];
                }

                dt.Rows.Add(row2);
            }

            return dt;
        }

        private void setElementNameWithCondition(List<ElementConditionData> elementConditionsData)
        {
            this.elementCondition.Clear();

            foreach (var elementConditionData in elementConditionsData)
            {
                foreach (var ctm in this.Ctms)
                {
                    foreach (var element in ctm.GetAllElements())
                    {
                        if (element.id.ToString() == elementConditionData.Id)
                        {
                            this.elementCondition.Add(elementConditionData, element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                            break;
                        }
                    }
                }
            }
        }
        //ISSUE_NO.740 Sunyi 2018/07/10 Start
        //複数のCTMを対応する
        private void setElementNameWithCondition_Mission(Dictionary<string, List<ElementConditionData>> elementConditions)
        {
            this.elementConditions.Clear();
            foreach (var elementCondition in elementConditions)
            {
                foreach (var ctm in this.Ctms)
                {
                    if (ctm.id.ToString().Equals(elementCondition.Key))
                    {
                        Dictionary<ElementConditionData, string> tmp = new Dictionary<ElementConditionData, string>();
                        foreach(var elementConditionData in elementCondition.Value)
                        {
                            foreach (var element in ctm.GetAllElements())
                            {
                                if (element.id.ToString() == elementConditionData.Id)
                                {
                                    tmp.Add(elementConditionData, element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                    break;
                                }
                            }
                        }
                        if (tmp.Count > 0)
                        {
                            this.elementConditions.Add(ctm.id.ToString(), tmp);
                        }
                    }
                }
            }
        }
        //ISSUE_NO.740 Sunyi 2018/07/10 End
        //AISMM No.80 sunyi 20181211 start
        //GripMission条件表示
        private void setElementNameWithCondition_Grip(Dictionary<string, List<ElementConditionData>> elementConditions_Grip)
        {
            this.elementConditions_Grip.Clear();
            foreach (var elementCondition in elementConditions_Grip)
            {
                foreach (var ctm in this.Ctms)
                {
                    if (ctm.id.ToString().Equals(elementCondition.Key))
                    {
                        Dictionary<ElementConditionData, string> tmp = new Dictionary<ElementConditionData, string>();
                        foreach (var elementConditionData in elementCondition.Value)
                        {
                            foreach (var element in ctm.GetAllElements())
                            {
                                if (element.id.ToString() == elementConditionData.Id)
                                {
                                    tmp.Add(elementConditionData, element.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                                    break;
                                }
                            }
                        }
                        if (tmp.Count > 0)
                        {
                            this.elementConditions_Grip.Add(ctm.id.ToString(), tmp);
                        }
                    }
                }
            }
        }
        //AISMM No.80 sunyi 20181211 end


        //ISSUE_NO.740 Sunyi 2018/07/10 Start
        //複数のCTMを対応する
        /// <summary>
        /// セル位置を取得
        /// </summary>
        //private void setCell()
        private void GetSettingPosition()
        {
            this.DT_X.Clear();
            this.DT_Y.Clear();
            this.tooltips.Clear();

            for (int i = 0; i < this.Dt.Rows.Count; i++)
            {
                for (int j = 0; j < this.Dt.Columns.Count; j++)
                {
                    //ISSUE_NO.740 Sunyi 2018/07/10 Start
                    //複数のCTMを対応する
                    foreach (var elementCondition in this.elementConditions)
                    {
                        foreach (var element in elementCondition.Value)
                        {
                            if (!this.Dt.Rows[i].ItemArray[j].ToString().Equals(element.Key.Id))
                            {
                                continue;
                            }

                            foreach (var conditionPattern in conditionPatterns)
                            {
                                if (conditionPattern.Key.Equals(elementCondition.Key))
                                {
                                    tooltips.Add((getConditionString(elementCondition.Value, conditionPattern.Value)));
                                }
                            }
                            DT_X.Add(i);
                            DT_Y.Add(j);
                        }
                    }
                }
            }
        }
        //ISSUE_NO.740 Sunyi 2018/07/10 End

        //ISSUE_NO.740 Sunyi 2018/07/10 Start
        //複数のCTMを対応する
        /// <summary>
        /// セルに条件式を設定
        /// </summary>
        private void setCell_CTM()
        {
            // 表示待機 以下を飛ばすと例外が発生する
            this.dataGrid_Element.UpdateLayout();

            // BackColorとToolTipを初期化
            for (int i = 0; i < this.dataGrid_Element.Items.Count; i++)
            {
                for (int j = 0; j < this.dataGrid_Element.Columns.Count; j++)
                {
                    // 表示待機 以下を飛ばすと例外が発生する
                    this.dataGrid_Element.ScrollIntoView(this.dataGrid_Element.Items[i]);

                    DataGridCellInfo cellInfo = new DataGridCellInfo(this.dataGrid_Element.Items[i], this.dataGrid_Element.Columns[j]);
                    DataGridCell cell = getCellFromCellInfo(this.dataGrid_Element, cellInfo);

                    cell.Background = new SolidColorBrush(Colors.White);
                    cell.Foreground = new SolidColorBrush(Colors.Black);
                    cell.ToolTip = null;
                }
            }
            //ISSUE_NO.740 Sunyi 2018/07/10 End

            // 表示待機 以下を飛ばすと例外が発生する
            this.dataGrid_Element.UpdateLayout();
            for (int i = 0; i < this.dataGrid_Element.Items.Count; i++)
            {
                for (int j = 0; j < this.dataGrid_Element.Columns.Count; j++)
                {
                    // 表示待機 以下を飛ばすと例外が発生する
                    this.dataGrid_Element.ScrollIntoView(this.dataGrid_Element.Items[i]);

                    DataGridCellInfo cellInfo = new DataGridCellInfo(this.dataGrid_Element.Items[i], this.dataGrid_Element.Columns[j]);
                    DataGridCell cell = getCellFromCellInfo(this.dataGrid_Element, cellInfo);

                    if (((TextBlock)cell.Content).Text == "Bulky")
                    {
                        cell.Foreground = new SolidColorBrush(Colors.Blue);
                        continue;
                    }
                    if (((TextBlock)cell.Content).Text == "LinkBulky")
                    {
                        cell.Foreground = new SolidColorBrush(Colors.Blue);
                        continue;
                    }
                    if (((TextBlock)cell.Content).Text == "VideoFOA")
                    {
                        cell.Foreground = new SolidColorBrush(Colors.Blue);
                        continue;
                    }
                }
            }

            for (int i = 0; i < DT_X.Count; i++)
            {
                //表示待機 以下を飛ばすと例外が発生する
                this.dataGrid_Element.ScrollIntoView(this.dataGrid_Element.Items[DT_X[i]]);
                DataGridCellInfo cellInfo = new DataGridCellInfo(this.dataGrid_Element.Items[DT_X[i]], this.dataGrid_Element.Columns[DT_Y[i]]);
                DataGridCell cell = getCellFromCellInfo(this.dataGrid_Element, cellInfo);

                cell.Background = new SolidColorBrush(Colors.Gray);
                ToolTip tt = new ToolTip();
                tt.Content = tooltips[i];
                cell.ToolTip = tt;
            }
        }
        //ISSUE_NO.740 Sunyi 2018/07/10 End

        /// <summary>
        /// セルに条件式を設定
        /// </summary>
        private void setCell()
        {
            // 表示待機 以下を飛ばすと例外が発生する
            this.dataGrid_Element.UpdateLayout();

            // BackColorとToolTipを初期化
            for (int i = 0; i < this.dataGrid_Element.Items.Count; i++)
            {
                for (int j = 0; j < this.dataGrid_Element.Columns.Count; j++)
                {
                    // 表示待機 以下を飛ばすと例外が発生する
                    this.dataGrid_Element.ScrollIntoView(this.dataGrid_Element.Items[i]);

                    DataGridCellInfo cellInfo = new DataGridCellInfo(this.dataGrid_Element.Items[i], this.dataGrid_Element.Columns[j]);
                    DataGridCell cell = getCellFromCellInfo(this.dataGrid_Element, cellInfo);

                    cell.Background = new SolidColorBrush(Colors.White);
                    cell.Foreground = new SolidColorBrush(Colors.Black);
                    cell.ToolTip = null;
                }
            }

            // 表示待機 以下を飛ばすと例外が発生する
            this.dataGrid_Element.UpdateLayout();
            for (int i = 0; i < this.dataGrid_Element.Items.Count; i++)
            {
                for (int j = 0; j < this.dataGrid_Element.Columns.Count; j++)
                {
                    // 表示待機 以下を飛ばすと例外が発生する
                    this.dataGrid_Element.ScrollIntoView(this.dataGrid_Element.Items[i]);

                    DataGridCellInfo cellInfo = new DataGridCellInfo(this.dataGrid_Element.Items[i], this.dataGrid_Element.Columns[j]);
                    DataGridCell cell = getCellFromCellInfo(this.dataGrid_Element, cellInfo);

                    if (((TextBlock)cell.Content).Text == "Bulky")
                    {
                        cell.Foreground = new SolidColorBrush(Colors.Blue);
                        continue;
                    }
                    if (((TextBlock)cell.Content).Text == "LinkBulky")
                    {
                        cell.Foreground = new SolidColorBrush(Colors.Blue);
                        continue;
                    }
                    if (((TextBlock)cell.Content).Text == "VideoFOA")
                    {
                        cell.Foreground = new SolidColorBrush(Colors.Blue);
                        continue;
                    }

                    foreach (string elementName in this.elementCondition.Values)
                    {
                        if (((TextBlock)cell.Content).Text != elementName)
                        {
                            continue;
                        }

                        cell.Background = new SolidColorBrush(Colors.Gray);

                        ToolTip tt = new ToolTip();
                        tt.Content = getConditionString(this.elementCondition, this.conditionPattern);
                        cell.ToolTip = tt;
                    }
                }
            }
        }

        /// <summary>
        /// 条件式をstring形式で取得
        /// </summary>
        /// <param name="condition"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        private string getConditionString(Dictionary<ElementConditionData, string> condition, int pattern)
        {
            // かっこで囲む必要があるのは、条件が3つ指定してあり、かつANDとORが複合してる時
            bool useBracket = false;
            if (condition.Count == 3 &&
                6 <= pattern)
            {
                useBracket = true;
            }

            int cnt = 0;
            string conditionText = useBracket ? "(" : string.Empty;
            foreach (ElementConditionData ecd in condition.Keys)
            {
                conditionText += condition[ecd];
                conditionText += CmsDataTable.ToStringExp(ecd.Condition);
                conditionText += ecd.Value;

                if (useBracket && cnt == 1)
                {
                    conditionText += ")";
                }
                if (cnt == condition.Count - 1)
                {
                    break;
                }

                if (cnt == 0)
                {
                    conditionText += " ";
                    if (pattern == 2 ||
                        pattern == 4 ||
                        pattern == 6)
                    {
                        conditionText += "AND ";
                    }
                    else
                    {
                        conditionText += "OR ";
                    }
                }
                if (cnt == 1)
                {
                    conditionText += " ";
                    if (pattern == 4 ||
                        pattern == 7)
                    {
                        conditionText += "AND ";
                    }
                    else
                    {
                        conditionText += "OR ";
                    }
                }

                cnt++;
            }

            return conditionText;
        }

        /// <summary>
        /// DataGridCellInfoからDataGridCellを取得
        /// </summary>
        /// <param name="dg">取得対象のDataGrid</param>
        /// <param name="info">取得したいセルのDataGridCellInfo</param>
        /// <returns>infoが示す位置のDataGridCell</returns>
        private DataGridCell getCellFromCellInfo(DataGrid dg, DataGridCellInfo info)
        {
            dg.CurrentCell = info;
            DataGridRow dataGridRow = (DataGridRow)dg.ItemContainerGenerator.ContainerFromItem(dg.CurrentCell.Item);
            DataGridCell cell = (DataGridCell)info.Column.GetCellContent(dataGridRow).Parent;

            return cell;
        }


        /// <summary>
        /// プログラムミッションの結果をデシリアライズ
        /// </summary>
        /// <param name="filePath">結果ファイルの絶対パス</param>
        /// <returns>デシリアライズされたプログラムミッションの結果</returns>
        private List<ProgramMission> deserializeProgramMissionResult(string filePath)
        {
            List<ProgramMission> resultList = new List<ProgramMission>();

            StreamReader sr = null;
            JsonTextReader jtr = null;
            var jObjectList = new JObject[GET_STREAM_BUF_SIZE];

            try
            {
                sr = new StreamReader(filePath, Encoding.UTF8);
                jtr = new JsonTextReader(sr);
                int cnt = 0;

                while (jtr.Read())
                {
                    if (jtr.TokenType == JsonToken.StartObject)
                    {
                        JObject jObj = JObject.Load(jtr);
                        string key = key = jObj["id"].ToString();

                        jObjectList[cnt] = jObj;
                        cnt++;

                        if (cnt == GET_STREAM_BUF_SIZE)
                        {
                            jObjectList = new JObject[GET_STREAM_BUF_SIZE];
                            cnt = 0;
                        }
                    }
                }
            }
            finally
            {
                if (jtr != null)
                {
                    jtr.Close();
                }
                if (sr != null)
                {
                    sr.Close();
                }
            }

            foreach (var jsonObject in jObjectList)
            {
                if (jsonObject == null)
                {
                    continue;
                    // break;
                }

                ProgramMission result = jsonObject.ToObject<ProgramMission>();
                resultList.Add(result);
            }

            return resultList;
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void image_Small_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            setDataGridSize(DataGridCellSize.Small);
        }

        private void image_Medium_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            setDataGridSize(DataGridCellSize.Medium);
        }

        private void image_Large_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            setDataGridSize(DataGridCellSize.Large);
        }

        private DataGridCellSize cellSize = DataGridCellSize.Medium;
        private void setDataGridSize(DataGridCellSize size)
        {
            switch (size)
            {
                case DataGridCellSize.Small:
                    this.dataGrid_Element.FontSize = 10;
                    foreach (var column in this.dataGrid_Element.Columns)
                    {
                        column.Width = 0;
                        column.Width = new DataGridLength(0, DataGridLengthUnitType.Auto);
                    }
                    break;
                case DataGridCellSize.Medium:
                    this.dataGrid_Element.FontSize = 12;
                    foreach (var column in this.dataGrid_Element.Columns)
                    {
                        column.Width = 0;
                        column.Width = new DataGridLength(0, DataGridLengthUnitType.Auto);
                    }
                    break;
                case DataGridCellSize.Large:
                    this.dataGrid_Element.FontSize = 16;
                    foreach (var column in this.dataGrid_Element.Columns)
                    {
                        column.Width = 0;
                        column.Width = new DataGridLength(0, DataGridLengthUnitType.Auto);
                    }
                    break;
                default:
                    break;
            }

            this.cellSize = size;
        }


        /// <summary>
        /// エレメント選択イベント
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_Selection_Click(object sender, RoutedEventArgs e)
        {
            var row = this.dataGrid_Element.Items[0] as DataRowView;

            //エレンント選択なし場合、エラーメッセージを表示する
            if (row == null || row[0].ToString() == string.Empty)
            {
                FoaMessageBox.ShowError("AIS_E_009");
                return;
            }

            MainWindow mainWindow = Application.Current.MainWindow as MainWindow;

            //集計表場合
            if (mainWindow.CurrentMode == ControlMode.Spreadsheet)
            {
                ((DAC.Model.UserControl_SpreadsheetTemplate)(mainWindow.CurrentEditCtrl)).edit_Grid.Visibility = Visibility.Visible;

                ((DAC.Model.UserControl_SpreadsheetTemplate)(mainWindow.CurrentEditCtrl)).dataGrid_Output.SetInitialDataGrid();

            }
            //オンライン場合
            else if (mainWindow.CurrentMode == ControlMode.Online)
            {
                ((DAC.Model.UserControl_OnlineTemplate)(mainWindow.CurrentEditCtrl)).edit_Grid.Visibility = Visibility.Visible;

                ((DAC.Model.UserControl_OnlineTemplate)(mainWindow.CurrentEditCtrl)).dataGrid_Output.SetInitialDataGrid();
            }
        }



        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 Start
        public void SetElementDataToDataGrid_Wp(string missionId, MissionTreeController ctrlMt)
        {
            this.SelectedMissionId = missionId;

            // 個別に指定する場合は、既存の設定をサーバから再取得
            CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
            missionCtmGetClient.AddParam("missionId", missionId);
            // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
            missionCtmGetClient.CompleteHandler += missionCtmJson =>
            {
                this.missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);

                // 条件式
                if (this.missionCtms.Count < 1)
                {
                    FoaMessageBox.ShowError("AIS_E_027");
                    SetInitialDataGrid();
                    return;
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 Start
                //複数のCTMを対応する
                this.conditionLists.Clear();
                this.conditionPatterns.Clear();
                foreach (var missionCtm in this.missionCtms)
                {
                    if (string.IsNullOrEmpty(missionCtm.ElementCondition) == false)
                    {
                        JToken condJson = JToken.Parse(missionCtm.ElementCondition);
                        JArray condElements = (JArray)condJson["elements"];
                        this.conditionPatterns.Add(missionCtm.CtmId, int.Parse(condJson["pattern"].ToString()));

                        List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
                        ElementConditionDatas = JsonConvert.DeserializeObject<List<ElementConditionData>>(condElements.ToString());
                        this.conditionLists.Add(missionCtm.CtmId, ElementConditionDatas);
                    }
                    else
                    {
                        //this.conditionList = new List<ElementConditionData>();
                        this.conditionLists.Add(missionCtm.CtmId, new List<ElementConditionData>());
                    }
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 End
                getElements(missionId);
                this.sourceType = DataSourceType.MISSION;
            };
            missionCtmGetClient.Get(CmsUrl.GetMissionCtmUrl());

            // ミッションTreeViewを設定
            TreeViewItem selectedItem = ctrlMt.tree.SelectedItem as TreeViewItem;
            StackPanel selectedHeader = selectedItem.Header as StackPanel;

            this.WorkPlace.AisTvRefDicMission.AisCheckFlag = 1;
            this.WorkPlace.AisTvRefDicMission.MissionNewType = 1;
            this.WorkPlace.AisTvRefDicMission.CheckChangedEvent +=
            this.WorkPlace.AisTvRefDicMissionCheckChanged;
            this.WorkPlace.AisTvRefDicMission.JsonItemSourceAdd = selectedHeader.DataContext.ToString();

        }
        // FOA_サーバー化開発 Processing On Server Dcs 2018/08/16 End


        //workplace sunyi 2018/10/11 start
        //WorkPlace用
        public void SetElementDataToDataGrid(string missionId, WorkPlaceTreeController ctrlWp,JToken value)
        {
            this.SelectedMissionId = missionId;

            // 個別に指定する場合は、既存の設定をサーバから再取得
            CmsHttpClient missionCtmGetClient = new CmsHttpClient(Application.Current.MainWindow);
            missionCtmGetClient.AddParam("missionId", missionId);
            // missionCtmGetClient.AddParam("ctmId", ctmIds[0]);
            missionCtmGetClient.CompleteHandler += missionCtmJson =>
            {
                this.missionCtms = JsonConvert.DeserializeObject<List<MissionCtm>>(missionCtmJson);

                // 条件式
                if (this.missionCtms.Count < 1)
                {
                    FoaMessageBox.ShowError("AIS_E_027");
                    SetInitialDataGrid();
                    return;
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 Start
                //複数のCTMを対応する
                this.conditionLists.Clear();
                this.conditionPatterns.Clear();
                foreach (var missionCtm in this.missionCtms)
                {
                    if (string.IsNullOrEmpty(missionCtm.ElementCondition) == false)
                    {
                        JToken condJson = JToken.Parse(missionCtm.ElementCondition);
                        JArray condElements = (JArray)condJson["elements"];
                        this.conditionPatterns.Add(missionCtm.CtmId, int.Parse(condJson["pattern"].ToString()));

                        List<ElementConditionData> ElementConditionDatas = new List<ElementConditionData>();
                        ElementConditionDatas = JsonConvert.DeserializeObject<List<ElementConditionData>>(condElements.ToString());
                        this.conditionLists.Add(missionCtm.CtmId, ElementConditionDatas);
                    }
                    else
                    {
                        //this.conditionList = new List<ElementConditionData>();
                        this.conditionLists.Add(missionCtm.CtmId, new List<ElementConditionData>());
                    }
                }
                //ISSUE_NO.740 Sunyi 2018/07/10 End
                getElements(missionId);
                this.sourceType = DataSourceType.MISSION;
            };
            missionCtmGetClient.Get(CmsUrl.GetMissionCtmUrl());

            //// ミッションTreeViewを設定
            //TreeViewItem selectedItem = ctrlWp.tree.SelectedItem as TreeViewItem;
            //if(selectedItem == null ) return;
            //JToken token = selectedItem.DataContext as JToken;

            //StackPanel selectedHeader = selectedItem.Header as StackPanel;

            this.WorkPlace.AisTvRefDicMission.AisCheckFlag = 1;
            this.WorkPlace.AisTvRefDicMission.MissionNewType = 1;
            this.WorkPlace.AisTvRefDicMission.CheckChangedEvent +=
            this.WorkPlace.AisTvRefDicMissionCheckChanged;
            this.WorkPlace.AisTvRefDicMission.JsonItemSourceAdd = value.ToString();

        }
        //workplace sunyi 2018/10/11 end

        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/23 Start
        private bool hasElements(JToken token, GripMissionCtm gripMission)
        {
            if (gripMission == null) return false;

            string elementId = (string)token["id"];

            //E2NaviUI sunyi 20190305 start
            //MISSION条件対応
            if (gripMission.StartNodes == null)
            {
                foreach (string selectedElementId in gripMission.StartNode.ElementIds)
                {
                    if (selectedElementId == elementId)
                    {
                        return true;
                    }
                }
            }
            else
            {
                foreach (var startNode in gripMission.StartNodes)
                {
                    foreach (string selectedElementId in startNode.Value.ElementIds)
                    {
                        if (selectedElementId == elementId)
                        {
                            return true;
                        }
                    }
                }
            }
            //E2NaviUI sunyi 20190305 end

            // Wang Issue AISTEMP-122 20181228 Start
            //if (gripMission.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace && gripMission.TraceNodes != null)
            if ((gripMission.SearchType == FoaCore.Model.GripR2.Search.SearchType.Trace || gripMission.SearchType == FoaCore.Model.GripR2.Search.SearchType.Period) && gripMission.TraceNodes != null)
            // Wang Issue AISTEMP-122 20181228 End
            {
                foreach (TraceNodeObject traceNode in gripMission.TraceNodes)
                {
                    // Wang Issue AISTEMP-122 20181228 Start
                    if (traceNode.ElementIds != null)
                    // Wang Issue AISTEMP-122 20181228 End
                    {
                        foreach (string selectedElementId in traceNode.ElementIds)
                        {
                            if (selectedElementId == elementId)
                            {
                                return true;
                            }
                        }
                    }
                }
            }

            foreach (EndNodeObject endNode in gripMission.EndNodes)
            {
                foreach (string selectedElementId in endNode.ElementIds)
                {
                    if (selectedElementId == elementId)
                    {
                        return true;
                    }
                }
            }

            return false;
        }


        private String getCtmsDataToJson(string missionId)
        {
            JArray array = new JArray();
            foreach (CtmObject ctm in this.Ctms)
            {
                CtmObject tmpCtm = new CtmObject();
                tmpCtm = (CtmObject)ctm.Clone();

                foreach (MissionCtm missionCtm in this.missionCtms)
                {
                    //workplace sunyi 2018/10/11 start
                    //if (missionCtm.CtmId == ctm.id.ToString())
                    //{
                    //workplace sunyi 2018/10/11 end
                        tmpCtm.children = null;
                        JToken jtoken = JToken.Parse(JsonConvert.SerializeObject(tmpCtm));
                        jtoken["missionId"] = missionId;
                        array.Add(jtoken);
                        break;
                    //}
                }
            }

            return array.ToString();
        }
        // FOA_サーバー化開発 Processing On Server Dcs 2018/07/23 End

        public void AddExternalFileData(string fileInfo)
        {
            // Create a new JsonTreeViewItem object and put the data in
            JToken fileInfoToken = JToken.Parse(fileInfo);
            JObject itemobj = new JObject();

            List<TreeViewItem> items = this.MultiStatusMonitor.AisTvRefDicMission.GetAllItems();
            TreeViewItem sameItem = null;
            string sameItemId = null;
            foreach (var item in items)
            {
                JToken token = this.MultiStatusMonitor.AisTvRefDicMission.GetToken(item);
                if(token["name"].ToString().Equals(fileInfoToken["fileName"].ToString()))
                {
                    sameItem = item;
                    sameItemId = this.MultiStatusMonitor.AisTvRefDicMission.GetId(item);
                    break;
                }

            }
            itemobj.Add("id", Guid.NewGuid().ToString("N"));
            itemobj.Add("name", fileInfoToken["fileName"].ToString());
            itemobj.Add("data", fileInfo);

            if (sameItem == null)
                this.MultiStatusMonitor.AisTvRefDicMission.JsonItemSourceAdd = itemobj.ToString();
            else
            {
                itemobj["id"] = sameItemId;
                this.MultiStatusMonitor.AisTvRefDicMission.UpdateItem(sameItemId, itemobj.ToString());
            }
            TreeViewItem treeitem = this.MultiStatusMonitor.AisTvRefDicMission.GetItem(itemobj["id"].ToString());
            treeitem.IsSelected = true;
            CheckBox checkbox = this.MultiStatusMonitor.AisTvRefDicMission.GetCheckBox(treeitem);
            checkbox.IsChecked = true;
        }

		private static readonly ILog logger = LogManager.GetLogger(typeof(CtmDetails).Name);
    }
}
