﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using DAC.Model;
using FoaCore.Net;
using System.Net.Http;
using System.IO;
using DAC.View.Helpers;
using System.IO.Compression;
using log4net;
using FoaCoreCom.ais.dto;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DAC.Model.Util;
using SpreadsheetGear;
using Microsoft.VisualBasic.FileIO;
using FoaCore.Common.Util;
using FoaCore.Common.Util.Json;
using FoaCore.Common.Net;
using DAC.Util;

namespace DAC.View
{
    /// <summary>
    /// ResultFolderFrame.xaml の相互作用ロジック
    /// </summary>
    public partial class ResultFolderFrame : UserControl
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(ResultFolderFrame));
        private static readonly string ZIP_EXTENSION = ".zip";
        private static readonly string JSON_EXTENSION = ".json";
        private static readonly string H_EXTENSION = ".h";
        private static readonly string GRIP_MISSION = "0";
        private static readonly string CMS_MISSION = "1";

        private const int MAX_BUFF_ROWS = 5000;
        private const string PARAM_SHEET_NAME = "param";
        private const string CSV_SHEET_NAME = "csvSheet";
        private const string REGISTER_CELL_NAME = "登録フォルダ";
        private static readonly int SKIP_SUB_HEADERS = 2;

        private MainWindow mainWindow;

        private UserControl_WorkPlace workplace;

        public ResultFolderFrame(UserControl_WorkPlace workplace)
        {
            InitializeComponent();

            this.workplace = workplace;
        }

        private void GetSearchResultButton_Click(object sender, RoutedEventArgs e)
        {
            WorkPlaceSearchParam wpInfo = this.workplace.GetSelectedWorkPlaceInfo();
            if (wpInfo != null)
            {
                CreateQuickGraph(wpInfo);
            }
        }

        private string GetTempZipFilePath(WorkPlaceSearchParam wpInfo)
        {
            string tempDir = System.IO.Path.Combine(AisConf.DownloadDir, System.Guid.NewGuid().ToString());
            System.IO.Directory.CreateDirectory(tempDir);
            return System.IO.Path.Combine(tempDir, wpInfo.workplaceId + ZIP_EXTENSION);
        }

        private string DownloadZipFile(WorkPlaceSearchParam wpInfo)
        {
            string url = ExcelEngineUrl.Workplace.GetScopeSearchUrl();

            FoaHttpClient client = new FoaHttpClient();
            string tempZip = GetTempZipFilePath(wpInfo);

            StringContent content = new StringContent(JsonUtil.Serialize(wpInfo), new UTF8Encoding(false), "application/json");

            HttpResponseMessage response = client.PostAsync(url, content).GetAwaiter().GetResult();

            if (response.IsSuccessStatusCode)
            {
                System.Net.Http.HttpContent streamContent = response.Content;

                using (var file = System.IO.File.Create(tempZip))
                {
                    using (var contentStream = streamContent.ReadAsStreamAsync().GetAwaiter().GetResult())
                    {
                        contentStream.CopyToAsync(file).GetAwaiter().GetResult();
                        file.FlushAsync().GetAwaiter().GetResult();
                    }
                }
            }
            else
            {
                File.Delete(tempZip);
                System.Net.Http.HttpContent streamContent = response.Content;
                using (var contentStream = streamContent.ReadAsStreamAsync().GetAwaiter().GetResult())
                {
                    using (var sr = new StreamReader(contentStream))
                    {
                        string exception = sr.ReadToEnd();
                        AisWebException we = JsonUtil.Deserialize<AisWebException>(exception);
                        we.ShowMessage();
                    }
                }
                return null;
            }

            return tempZip;
        }

        private string GetExcelName(WorkPlaceSearchParam wpInfo)
        {
            string excelName = null;
            excelName = wpInfo.displayName + System.IO.Path.GetExtension(Properties.Resources.TEXT_DEFAULT_QUICK_GRAPH);
            return excelName;
        }

        private string GetExcelPath(WorkPlaceSearchParam wpInfo)
        {
            string excelName = GetExcelName(wpInfo);
            //ISSUE_NO.807 sunyi 2018/07/27 Start
            //記号を""に変更する
            excelName = excelName.Replace("[", "");
            excelName = excelName.Replace("]", "");
            //ISSUE_NO.807 sunyi 2018/07/27 End
            string excelPath = System.IO.Path.Combine(AisConf.OutputDir, excelName);

            System.IO.Directory.CreateDirectory(AisConf.OutputDir);

            return excelPath;
        }

        private HashSet<string> UnzipDownloadFile(string zipPath)
        {
            HashSet<string> unzipFiles = new HashSet<string>();
            try
            {
                using (ZipArchive archive = ZipFile.OpenRead(zipPath))
                {
                    string decompressDir = Path.GetDirectoryName(zipPath);

                    foreach (ZipArchiveEntry file in archive.Entries)
                    {
                        string fullpath = Path.Combine(decompressDir, file.FullName);
                        Directory.CreateDirectory(Path.GetDirectoryName(fullpath));
                        ZipFileExtensions.ExtractToFile(file, fullpath, true);
                        unzipFiles.Add(fullpath);
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error("Failed in extracting file : " + zipPath, e);
            }

            return unzipFiles;
        }

        private void WriteQuickGraphCsv(string zipPath, string prefix, WorkPlaceSearchParam wpInfo)
        {
        }

        private OutputItem WriteQuickGraphExcel(string zipPath, string prefix, WorkPlaceSearchParam wpInfo)
        {
            UnzipDownloadFile(zipPath);
            
            string baseDir = Path.GetDirectoryName(zipPath);
            string resultJson = GetResultJsonFile(wpInfo, baseDir);

            if (!string.IsNullOrEmpty(resultJson))
            {
                List<string> sheetNames = new List<string>();

                List<WorkplaceJsonResultDto> jsonResult = null;
                using (StreamReader sr = new StreamReader(resultJson))
                {
                    string rte = sr.ReadToEnd();
                    jsonResult = JsonConvert.DeserializeObject<List<WorkplaceJsonResultDto>>(rte);
                }

                if (jsonResult != null)
                {
                    int sheetIndex = 0;
                    foreach (WorkplaceJsonResultDto result in jsonResult)
                    {
                        string missionZip = Path.Combine(baseDir, result.missionId + ZIP_EXTENSION);
                        if (File.Exists(missionZip))
                        {
                            HashSet<string> unzipFiles = UnzipDownloadFile(missionZip);
                            if (result.missionType.Equals(GRIP_MISSION))
                            {
                                ImportGripResultToQuickGraph(unzipFiles, prefix, ref sheetIndex, sheetNames);
                            }
                            else
                            {
                                ImportCmsResultToQuickGraph(unzipFiles, prefix, ref sheetIndex, sheetNames);
                            }
                        }
                    }
                }

                return new OutputItem()
                {
                    creationTime = File.GetLastWriteTime(prefix),
                    path = prefix,
                    type = OutputType.Mission,
                    sheetNames = sheetNames
                };
            }

            return null;
        }

        private void ImportCmsResultToQuickGraph(HashSet<string> unzipFiles, string prefix, ref int sheetIndex, List<string> sheetNames)
        {
            foreach (string unzipFile in unzipFiles)
            {
                string sheetName = GetSheetName(unzipFile, sheetIndex);
                InsertCmsCsvToExcel(prefix, unzipFile, sheetName, sheetIndex);
                sheetIndex++;
                sheetNames.Add(sheetName);
            }
        }

        private void ImportGripResultToQuickGraph(HashSet<string> unzipFiles, string prefix, ref int sheetIndex, List<string> sheetNames)
        {
            foreach (string unzipFile in unzipFiles)
            {
                if (AisUtil.IsCsvFile(unzipFile))
                {
                    string hFile = Path.Combine(Path.GetDirectoryName(unzipFile), Path.GetFileNameWithoutExtension(unzipFile) + H_EXTENSION);
                    string sheetName = GetSheetName(hFile, unzipFile, sheetIndex);
                    InsertGripCsvToExcel(prefix, unzipFile, sheetName, sheetIndex);
                    sheetIndex++;
                    sheetNames.Add(sheetName);
                }
            }
        }

        private SpreadsheetGear.IWorksheet GetSheet(IWorkbook workbook, string sheetName)
        {
            SpreadsheetGear.IWorksheet sheet = null;
            try
            {
                sheet = workbook.Worksheets[sheetName];
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }

            return sheet;
        }

        private SpreadsheetGear.IWorksheet CreateSheet(IWorkbook workbook, string sheetName)
        {
            SpreadsheetGear.IWorksheet sheet = GetSheet(workbook, sheetName);

            try
            {
                if (sheet == null)
                {
                    sheet = workbook.Worksheets.Add();
                    sheet.Name = sheetName;
                }
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {

            }

            return sheet;
        }

        private void InsertGripCsvToExcel(string excelFile, string csvFile, string sheetName, int sheetIndex)
        {
            IWorkbookSet workbookSet = SpreadsheetGear.Factory.GetWorkbookSet();
            IWorkbook workbook = workbookSet.Workbooks.Open(excelFile);
            SpreadsheetGear.IWorksheet worksheet = CreateSheet(workbook, sheetName);
            SpreadsheetGear.IWorksheet csvsheet = CreateSheet(workbook, CSV_SHEET_NAME);
            StreamReader sr = null;
            TextFieldParser parser = null;

            try
            {
                csvsheet.Visible = SheetVisibility.Hidden;

                sr = new StreamReader(csvFile, Encoding.GetEncoding(932));

                parser = new TextFieldParser(sr);
                parser.TextFieldType = FieldType.Delimited; // 区切り形式とする
                parser.SetDelimiters(","); // 区切り文字はコンマとする
                parser.HasFieldsEnclosedInQuotes = true;
                parser.TrimWhiteSpace = false;

                int rowIndex = 0;
                int remainder = 0;
                int columns = 0;
                object[,] values = null;
                object[,] skipHeaders = null;

                while (!parser.EndOfData)
                {
                    string[] textrow = parser.ReadFields(); // 1行読み込んでフィールド毎に配列に入れる

                    // 中間データ格納領域
                    if (values == null)
                    {
                        columns = textrow.Length;
                        values = new object[MAX_BUFF_ROWS, columns];

                        if (SKIP_SUB_HEADERS > 0)
                        {
                            skipHeaders = new object[SKIP_SUB_HEADERS + 1, columns];
                            for (int skip = 0; skip < SKIP_SUB_HEADERS + 1 && !parser.EndOfData; skip++)
                            {
                                if (skip == 0)
                                {
                                    for (int i = 0; i < columns; i++)
                                    {
                                        skipHeaders[skip, i] = i == 0 ? worksheet.Name : string.Empty;
                                    }
                                }
                                else
                                {
                                    string[] skiprow = parser.ReadFields();
                                    for (int i = 0; i < skiprow.Length; i++)
                                    {
                                        skipHeaders[skip, i] = skiprow[i];
                                    }
                                }
                            }
                            SpreadsheetGear.IRange range = csvsheet.Cells[sheetIndex * (SKIP_SUB_HEADERS + 1), 0,
                                (sheetIndex + 1) * (SKIP_SUB_HEADERS + 1) - 1, columns - 1];
                            range.Value = skipHeaders;
                        }
                    }

                    for (int indexCol = 0; indexCol < textrow.Length; indexCol++)
                    {
                        values[remainder, indexCol] = textrow[indexCol];
                    }

                    rowIndex++;
                    remainder = rowIndex % MAX_BUFF_ROWS;
                    if (remainder == 0)
                    {
                        SpreadsheetGear.IRange range = worksheet.Cells[rowIndex - MAX_BUFF_ROWS, 0, rowIndex - 1, columns - 1];
                        range.Value = values;
                    }
                }

                if (remainder != 0)
                {
                    object[,] remainValues = new object[remainder, columns];
                    for (int i = 0; i < remainder; i++)
                    {
                        for (int j = 0; j < columns; j++)
                        {
                            remainValues[i, j] = values[i, j];
                        }
                    }
                    SpreadsheetGear.IRange range = worksheet.Cells[rowIndex - remainder, 0, rowIndex - 1, columns - 1];
                    range.Value = remainValues;
                }
                workbook.Save();
            }
            finally
            {
                if (parser != null) parser.Close();
                if (sr != null) sr.Close();
                workbook.Close();
            }
        }

        private void InsertCmsCsvToExcel(string excelFile, string csvFile, string sheetName, int sheetIndex)
        {
            IWorkbookSet workbookSet = SpreadsheetGear.Factory.GetWorkbookSet();
            IWorkbook workbook = workbookSet.Workbooks.Open(excelFile);
            SpreadsheetGear.IWorksheet worksheet = CreateSheet(workbook, sheetName);
            SpreadsheetGear.IWorksheet csvsheet = CreateSheet(workbook, CSV_SHEET_NAME);
            StreamReader sr = null;
            TextFieldParser parser = null;

            try
            {
                csvsheet.Visible = SheetVisibility.Hidden;

                sr = new StreamReader(csvFile, Encoding.UTF8);

                parser = new TextFieldParser(sr);
                parser.TextFieldType = FieldType.Delimited; // 区切り形式とする
                parser.SetDelimiters(","); // 区切り文字はコンマとする
                parser.HasFieldsEnclosedInQuotes = true;
                parser.TrimWhiteSpace = false;

                int rowIndex = 0;
                int remainder = 0;
                int columns = 0;
                object[,] values = null;
                object[,] skipHeaders = null;

                while (!parser.EndOfData)
                {
                    string[] textrow = null;

                    if (values == null)
                    {
                        parser.ReadFields(); // Skip ctm name
                        if (!parser.EndOfData)
                        {
                            parser.ReadFields(); // Skip element id
                        }
                        textrow = parser.ReadFields(); // 1行読み込んでフィールド毎に配列に入れる
                        //workplace RT⇒RECEIVE_TIME sunyi 2018/10/17 start
                        if (textrow[0] == Keywords.RT)
                        {
                            textrow[0] = Keywords.RECEIVE_TIME;
                        }
                        //workplace RT⇒RECEIVE_TIME sunyi 2018/10/17 end
                    }
                    else
                    {
                        textrow = parser.ReadFields(); // 1行読み込んでフィールド毎に配列に入れる
                        textrow[0] = UnixTime.ToDateTime2(long.Parse(textrow[0])).ToString("yyyy/MM/dd HH:mm:ss");
                    }


                    // 中間データ格納領域
                    if (values == null)
                    {
                        columns = textrow.Length;
                        values = new object[MAX_BUFF_ROWS, columns];

                        if (SKIP_SUB_HEADERS > 0)
                        {
                            skipHeaders = new object[SKIP_SUB_HEADERS + 1, columns];
                            for (int skip = 0; skip < SKIP_SUB_HEADERS + 1 && !parser.EndOfData; skip++)
                            {
                                if (skip == 0)
                                {
                                    for (int i = 0; i < columns; i++)
                                    {
                                        skipHeaders[skip, i] = i == 0 ? worksheet.Name : string.Empty;
                                    }
                                }
                                else
                                {
                                    string[] skiprow = parser.ReadFields();
                                    for (int i = 0; i < skiprow.Length; i++)
                                    {
                                        skipHeaders[skip, i] = skiprow[i];
                                    }
                                }
                            }
                            SpreadsheetGear.IRange range = csvsheet.Cells[sheetIndex * (SKIP_SUB_HEADERS + 1), 0,
                                (sheetIndex + 1) * (SKIP_SUB_HEADERS + 1) - 1, columns - 1];
                            range.Value = skipHeaders;
                        }
                    }

                    for (int indexCol = 0; indexCol < textrow.Length; indexCol++)
                    {
                        values[remainder, indexCol] = textrow[indexCol];
                    }

                    rowIndex++;
                    remainder = rowIndex % MAX_BUFF_ROWS;
                    if (remainder == 0)
                    {
                        SpreadsheetGear.IRange range = worksheet.Cells[rowIndex - MAX_BUFF_ROWS, 0, rowIndex - 1, columns - 1];
                        range.Value = values;
                    }
                }

                if (remainder != 0)
                {
                    object[,] remainValues = new object[remainder, columns];
                    for (int i = 0; i < remainder; i++)
                    {
                        for (int j = 0; j < columns; j++)
                        {
                            remainValues[i, j] = values[i, j];
                        }
                    }
                    SpreadsheetGear.IRange range = worksheet.Cells[rowIndex - remainder, 0, rowIndex - 1, columns - 1];
                    range.Value = remainValues;
                }
                workbook.Save();
            }
            finally
            {
                if (parser != null) parser.Close();
                if (sr != null) sr.Close();
                workbook.Close();
            }
        }

        private string GetSheetName(string csvFile, int index)
        {
            string line;
            using (FileStream csvStream = new System.IO.FileStream(csvFile, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                using (StreamReader csvReader = new StreamReader(csvStream, Encoding.UTF8))
                {
                    while ((line = csvReader.ReadLine()) != null)
                    {
                        break;
                    }
                }
            }

            return string.Format("{0}_{1}", index + 1, line);
        }

        private string GetSheetName(string hFile, string csvFile, int index)
        {
            string line;
            using (FileStream hStream = new System.IO.FileStream(hFile, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                using (StreamReader hReader = new StreamReader(hStream, Encoding.GetEncoding(932)))
                {
                    while ((line = hReader.ReadLine()) != null)
                    {
                        break;
                    }
                }
            }
            string[] columns = line.Split(',');

            using (FileStream csvStream = new System.IO.FileStream(csvFile, System.IO.FileMode.Open, System.IO.FileAccess.Read))
            {
                using (StreamReader csvReader = new StreamReader(csvStream, Encoding.GetEncoding(932)))
                {
                    while ((line = csvReader.ReadLine()) != null)
                    {
                        break;
                    }
                }
            }
            string[] headers = line.Split(',');

            string[] parts = Path.GetFileNameWithoutExtension(hFile).Split('_');

            return string.Format("{0}_{1}_Route-{2}", index + 1, headers[int.Parse(columns[columns.Length - 1])].Replace("\"",""), parts[2]);
        }

        private string GetResultJsonFile(WorkPlaceSearchParam wpInfo, string baseDir)
        {
            return Path.Combine(baseDir, wpInfo.workplaceId + JSON_EXTENSION);
        }

        private void CreateQuickGraph(WorkPlaceSearchParam wpInfo)
        {
            string zipPath = DownloadZipFile(wpInfo);
            if (!string.IsNullOrEmpty(zipPath))
            {
                string filePath = GetExcelPath(wpInfo);
                string templatePath = AisConf.Config.GraphTemplateDir;
                if (!File.Exists(templatePath))
                {
                    AisMessageBox.DisplayInfoMessageBox(Properties.Message.AIS_W_003);
                    string prefix = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(filePath), System.IO.Path.GetFileNameWithoutExtension(filePath) + "_");
                    if (ResultFolderGrid.DeleteExistingCsvFiles(prefix))
                    {
                        WriteQuickGraphCsv(zipPath, prefix, wpInfo);
                    }
                }
                else
                {
                    try
                    {
                        File.Copy(templatePath, filePath, true);
                    }
#pragma warning disable
                    catch (Exception e)
#pragma warning restore
                    {
                        AisMessageBox.DisplayErrorMessageBox(Properties.Message.AIS_E_062);
                        return;
                    }
                    OutputItem item = WriteQuickGraphExcel(zipPath, filePath, wpInfo);
                    ResultFolderGrid.AddOutputItem(item);
                }
            }
        }
    }
}
