﻿using DAC.AExcel;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using FoaCore.Common.Util;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using SpreadsheetGear;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_StockTimeTemplate.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_FIFOTemplate : BaseControl
    {
        /// <summary>
        /// 指定時刻表示フォーマット
        /// </summary>
        private const string displayTimeFormat = "yyyy/MM/dd HH:mm";

        private const string EXCEL_MACRO = "CallBeforeExcelFromAIS";
        private const int PARAM_SHEET_MAX_PARAMS = 100;

        private bool isFirstFile = true;
        private string excelFilePath = string.Empty;

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public UserControl_FIFOTemplate(bool isFirstFile = true, string filePath = "")
        {
            InitializeComponent();

            this.Mode = ControlMode.FIFO;
            this.TEMPLATE = "FIFOTemplate.xlsm";

            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            // 画像読み込み
            string fileName = "FIFO.png";
            if (AisConf.UiLang == "en")
            {
                fileName = "en_FIFO.png";
            }

            string uri = string.Format(@"/DAC;component/Resources/Image/{0}", fileName);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(uri, UriKind.RelativeOrAbsolute);
            bi.EndInit();
            this.image_Thumbnail.Source = bi;

            this.image_OpenExcel.Deactivate();

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }

            setKeyValuePair();
            setComboBoxItem();
        }

        /// <summary>
        /// 更新周期
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        List<KeyValuePair<string, TimeSpan>> intervalItems = new List<KeyValuePair<string, TimeSpan>>();

        /// <summary>
        /// 表示期間
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        List<KeyValuePair<string, TimeSpan>> displayPeriodItems = new List<KeyValuePair<string, TimeSpan>>();

        /// <summary>
        /// 取得期間
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        List<KeyValuePair<string, TimeSpan>> getPeriodItems = new List<KeyValuePair<string, TimeSpan>>();

        private void setKeyValuePair()
        {
            // 更新周期
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("0.5", new TimeSpan(0, 0, 30)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("1", new TimeSpan(0, 1, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("3", new TimeSpan(0, 3, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("5", new TimeSpan(0, 5, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("10", new TimeSpan(0, 10, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("15", new TimeSpan(0, 15, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("20", new TimeSpan(0, 20, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("25", new TimeSpan(0, 25, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("30", new TimeSpan(0, 30, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("45", new TimeSpan(0, 45, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("60", new TimeSpan(1, 0, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("90", new TimeSpan(1, 30, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("120", new TimeSpan(2, 0, 0)));

            // 表示期間
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("1", new TimeSpan(1, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("2", new TimeSpan(2, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("3", new TimeSpan(3, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("6", new TimeSpan(6, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("12", new TimeSpan(12, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("24", new TimeSpan(24, 0, 0)));
        }

        private void setComboBoxItem()
        {
            // 更新周期
            foreach (var pair in this.intervalItems)
            {
                this.comboBox_Interval.Items.Add(pair.Key);
            }
            this.comboBox_Interval.SelectedItem = "60";

            // 表示期間
            foreach (var pair in this.displayPeriodItems)
            {
                this.comboBox_DisplayPeriod.Items.Add(pair.Key);
            }
            this.comboBox_DisplayPeriod.SelectedItem = "1";
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();

            BitmapImage biCancel = new BitmapImage();
            biCancel.BeginInit();
            biCancel.UriSource = new Uri(@"/DAC;component/Resources/Image/cancel-to-get.png", UriKind.RelativeOrAbsolute);
            biCancel.EndInit();
            this.image_CancelToGet.Source = biCancel;
            this.image_CancelToGet.IsEnabled = false;

            BitmapImage biGage = new BitmapImage();
            biGage.BeginInit();
            biGage.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar-empty.png", UriKind.RelativeOrAbsolute);
            biGage.EndInit();
            this.image_Gage.Source = biGage;

            this.editControl.FIFO  = this;
            this.radioOn.IsChecked = true;

            // No.479 In the case of Graph template screen, Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;

            if (!this.isFirstFile)
            {
                try
                {
                    readExcelFile(this.excelFilePath);
                    this.image_OpenExcel.Activate();
                }
                catch (Exception ex)
                {
                    string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                    message += string.Format("\n" + ex.StackTrace);
#endif
                    AisMessageBox.DisplayErrorMessageBox(message);
                }
            }
        }

        /// <summary>
        /// 選択されているミッションを取得
        /// </summary>
        /// <param name="ctrlMt">ミッションツリーコントローラ</param>
        /// <returns>選択されているミッション</returns>
        private Mission getSelectedMission(MissionTreeController ctrlMt)
        {
            Mission selectedMission = new Mission();

            var selectedItem = ctrlMt.tree.SelectedItem as TreeViewItem;
            var selectedHeader = selectedItem.Header as StackPanel;
            var json = selectedHeader.DataContext.ToString();
            selectedMission = JsonConvert.DeserializeObject<Mission>(json);

            return selectedMission;
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// 開始時刻エレメントDrag&Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_StartTime_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.textBlock_StartTime.Text = string.Format("{0}・{1} ", ary[0], ary[2]);
        }

        /// <summary>
        /// 終了時刻エレメントDrag&Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_EndTime_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.textBlock_EndTime.Text = string.Format("{0}・{1} ", ary[0], ary[2]);
        }

        /// <summary>
        /// 有効化/無効化切替
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !isTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool isTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        // Accept numeric number only as input（更新周期用に.を許可）
        private void NumberValidationTextBox2(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed2(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed2(string text)
        {
            Regex regex = new Regex("[^0-9.]+");
            return !regex.IsMatch(text);
        }

        /// <summary>
        /// 開始時刻エレメント削除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_StartTime_Click(object sender, MouseButtonEventArgs e)
        {
            textBlock_StartTime.Text = string.Empty;
        }

        /// <summary>
        /// 終了エレメント削除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_EndTime_Click(object sender, MouseButtonEventArgs e)
        {
            textBlock_EndTime.Text = string.Empty;
        }

        private void image_OpenExcel_Tap(object sender, RoutedEventArgs e)
        {
            if (this.tmpResultFolderPath != null)
            {
                var reultFiles = Directory.GetFiles(this.tmpResultFolderPath, "*.csv");
                if (reultFiles.Length == 0)
                {
                    AisUtil.LoadProgressBarImage(this.image_Gage, false);
                    FoaMessageBox.ShowError("AIS_E_006");
                    return;
                }
            }

            // Wang Issue AISTEMP-125 20190121 Start
            ////マクロ起動処理
            //getParamsFromExcel(this.excelFilePath, EXCEL_MACRO, true);

            ////EXCELﾌｧｲﾙ表示
            ///*
            //var processStartInfo = new ProcessStartInfo();
            //processStartInfo.FileName = this.excelFilePath;
            //Process process = Process.Start(processStartInfo);
            // * */
            //ExcelUtil.OpenExcelFile(this.excelFilePath);

            mainWindow.LoadinVisibleChange(false);

            OpenExcelWorker = new BackgroundWorker();
            OpenExcelWorker.WorkerReportsProgress = true;
            OpenExcelWorker.DoWork += new DoWorkEventHandler(OpenExcel);
            OpenExcelWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OpenExcelCompleted);
            OpenExcelWorker.RunWorkerAsync();
            // Wang Issue AISTEMP-125 20190121 End
        }

        // Wang Issue AISTEMP-125 20190121 Start
        BackgroundWorker OpenExcelWorker;
        private void OpenExcelCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                mainWindow.LoadinVisibleChange(true);
            });
        }
        private void OpenExcel(object sender, DoWorkEventArgs e)
        {
            //マクロ起動処理
            getParamsFromExcel(this.excelFilePath, EXCEL_MACRO, true);

            //EXCELﾌｧｲﾙ表示
            /*
            var processStartInfo = new ProcessStartInfo();
            processStartInfo.FileName = this.excelFilePath;
            Process process = Process.Start(processStartInfo);
             * */
            ExcelUtil.OpenExcelFile(this.excelFilePath);
        }
        // Wang Issue AISTEMP-125 20190121 End

        /// <summary>
        /// ミッションからの情報取得（「GET」ボタンクリック時の処理）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button_Get_Click(object sender, RoutedEventArgs e)
        {
            if (this.mainWindow.CtmDtailsCtrl.GetCtmDtailsEmpty() == string.Empty)
            {
                FoaMessageBox.ShowError("AIS_E_009");
                return;
            }

            bool isOnline = (bool)this.radioOn.IsChecked;
            //DateTime dtStart = (DateTime)this.dateTimePicker_Start.Value;
            //DateTime dtEnd = (DateTime)this.dateTimePicker_End.Value;

            // 入力値チェック
            if (this.dateTimePicker_Start.Value == null || this.dateTimePicker_End.Value == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return;
            }

            if (this.dateTimePicker_End.Value <= this.dateTimePicker_Start.Value)
            {
                FoaMessageBox.ShowError("AIS_E_019");
                return;
            }

            if (isOnline)
            {
                if (this.dateTimePicker_End.Value < DateTime.Now)
                {
                    FoaMessageBox.ShowError("AIS_E_002");
                    return;
                }
            }

            if (string.IsNullOrEmpty(this.textBlock_StartTime.Text) ||
                string.IsNullOrEmpty(this.textBlock_EndTime.Text))
            {
                FoaMessageBox.ShowError("AIS_E_003");
                return;
            }

            DateTime getStart;
            DateTime getEnd;
            bool gotTime;
            gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out getStart, out getEnd);
            if (!gotTime)
            {
                return;
            }

            //AISTEMP-79 sunyi 20190111 start
            //DataTime確認
            MessageBoxResult result = FoaMessageBox.ShowConfirm("AIS_I_008");
            if (result.ToString() == "No")
            {
                return;
            }
            //AISTEMP-79 sunyi 20190111 end

            Mission mission = DAC.Model.Util.AisUtil.GetSelectedMission(this.mainWindow.CtrlMt);
            try
            {
                string startElement = this.textBlock_StartTime.Text.Trim();
                string endElement = this.textBlock_EndTime.Text.Trim();

                double reloadInterval = 0;
                if (!double.TryParse(this.comboBox_Interval.Text.Trim(), out reloadInterval))
                {
                    FoaMessageBox.ShowError("AIS_E_004");
                    return;
                }
                double displayPeriod = 0;
                if (!double.TryParse(this.comboBox_DisplayPeriod.Text.Trim(), out displayPeriod))
                {
                    FoaMessageBox.ShowError("AIS_E_005");
                    return;
                }
                DateTime displayStart = (DateTime)this.dateTimePicker_Start.Value;
                DateTime displayEnd = (DateTime)this.dateTimePicker_End.Value;
                
                string filePathDest = string.Empty;

                preRetrieveCtmData();

                this.DownloadCts = new CancellationTokenSource();
                var dirRetrieve = await RetrieveCtmData(getStart, getEnd, this.DownloadCts);
                if (dirRetrieve == null)
                {
                    postRetrieveCtmData(false);
                    return;
                }

                this.Bw = new BackgroundWorker();
                // define the event handlers
                Bw.DoWork += delegate(object s, DoWorkEventArgs args)
                {
                    var configParams = new Dictionary<ExcelConfigParam, object>();
                    configParams.Add(ExcelConfigParam.TEMPLATE_NAME, "先入先出");
                    configParams.Add(ExcelConfigParam.KAISHI_ELEM, startElement);
                    configParams.Add(ExcelConfigParam.SHURYO_ELEM, endElement);
                    configParams.Add(ExcelConfigParam.ONLINE_FLAG, isOnline);

                    if (isOnline)
                    {
                        configParams.Add(ExcelConfigParam.SHUTOKU_KAISHI, getStart.ToString("yyyy/MM/dd HH:mm"));
                        configParams.Add(ExcelConfigParam.SHUTOKU_SHURYO, getEnd.ToString("yyyy/MM/dd HH:mm"));
                        configParams.Add(ExcelConfigParam.RELOAD_INTERVAL, reloadInterval.ToString());
                        configParams.Add(ExcelConfigParam.DISPLAY_PERIOD, displayPeriod.ToString());
                    }
                    else
                    {
                        //DAC-87 sunyi 20190107 start
                        configParams.Add(ExcelConfigParam.SHUTOKU_KAISHI, getStart.ToString("yyyy/MM/dd HH:mm"));
                        configParams.Add(ExcelConfigParam.SHUTOKU_SHURYO, getEnd.ToString("yyyy/MM/dd HH:mm"));
                        //DAC-87 sunyi 20190107 end
                        configParams.Add(ExcelConfigParam.DISPLAY_START, displayStart.ToString("yyyy/MM/dd HH:mm"));
                        configParams.Add(ExcelConfigParam.DISPLAY_END, displayEnd.ToString("yyyy/MM/dd HH:mm"));
                    }

                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                        configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetCatalogLang(this.SelectedMission));
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                        configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetGripCatalogLang(this.SelectedGripMission));
                    }

                    if (!isFirstFile)
                    {
                        configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(this.excelFilePath));
                    }

                    filePathDest = writeResultToExcel_X(getStart, getEnd, Bw, this.DataSource, dirRetrieve, configParams);

                    if (string.IsNullOrEmpty(filePathDest))
                    {
                        FoaMessageBox.ShowError("AIS_E_001");
                        return;
                    }
                };
                Bw.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
                {
                    AisUtil.LoadProgressBarImage(this.image_Gage, false);
                    this.Bw = null;

                    bool success = true;
                    if (args.Error != null)  // if an exception occurred during DoWork,
                    {
                        // Do your error handling here
                        AisMessageBox.DisplayErrorMessageBox(args.Error.ToString());
                        success = false;
                    }

                    postRetrieveCtmData(success);

                    this.tmpResultFolderPath = dirRetrieve;
                    this.excelFilePath = filePathDest;
                };
                Bw.RunWorkerAsync(); // starts the background worker
            }
            catch (Exception ex)
            {
                string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                AisMessageBox.DisplayErrorMessageBox(message);
            }

            AisUtil.LoadProgressBarImage(this.image_Gage, false);
        }

        #region Excelファイルの作成

        /// <summary>
        /// 開始処理
        /// </summary>
        private void preRetrieveCtmData()
        {
            this.button_Get.IsEnabled = false;
            this.image_CancelToGet.IsEnabled = true;
            this.image_OpenExcel.Deactivate();
            AisUtil.LoadProgressBarImage(this.image_Gage, true);
        }

        /// <summary>
        /// 終了処理
        /// </summary>
        /// <param name="success"></param>
        private void postRetrieveCtmData(bool success)
        {
            if (success)
            {
                this.image_OpenExcel.Activate();
            }

            AisUtil.LoadProgressBarImage(this.image_Gage, false);
            this.image_CancelToGet.IsEnabled = false;
            this.button_Get.IsEnabled = true;
        }

        #endregion

        private string writeResultToExcel_X(DateTime start, DateTime end, BackgroundWorker bWorker, DataSourceType resultType,
            string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            var excelFilenameMain = string.Empty;
            if (resultType == DataSourceType.CTM_DIRECT)
            {
                excelFilenameMain = "CTM_RESULT";
            }
            else if (resultType == DataSourceType.MISSION)
            {
                excelFilenameMain = AisUtil.GetCatalogLang(this.SelectedMission);
            }
            else
            {
                excelFilenameMain = AisUtil.GetGripCatalogLang(this.SelectedGripMission);
            }


            // コピー元ファイルの絶対パス
            string filePathSrc = isFirstFile ? System.IO.Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE) : this.excelFilePath;

            string fileName = string.Format("{0}_{1}.xlsm", excelFilenameMain, DateTime.Now.ToString("yyyyMMddHHmmss"));
            fileName = fileName.Replace("[", string.Empty);
            fileName = fileName.Replace("]", string.Empty);

            string filePathDest = Path.Combine(resultDir, fileName);
            File.Copy(filePathSrc, filePathDest);

            var writer = new SSGExcelWriterStockTime(this.Mode, this.DataSource, this.editControl.Ctms, this.editControl.Dt.Copy(), bWorker, this.SelectedMission);
            writer.WriteCtmData(filePathDest, folderPath, configParams);

            return filePathDest;
        }

        /// <summary>
        /// EXCELマクロをAISより実行：EXCELオープンから起動される。
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="macro"></param>
        /// <param name="saveChange"></param>
        private void getParamsFromExcel(string filePath, string macro, bool saveChange)
        {
            Microsoft.Office.Interop.Excel.Application excel = null;
            Microsoft.Office.Interop.Excel.Workbooks workBooks = null;
            Microsoft.Office.Interop.Excel.Workbook workBook = null;

            try
            {
                //マクロ実行準備
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = false;
                ExcelUtil.MoveExcelOutofScreen(excel);
                workBooks = excel.Workbooks;
                workBook = workBooks.Open(filePath);

                // マクロの実行
                excel.Run(macro);
            }
            catch (Exception ex)
            {
                string message = string.Format("Excelファイルへのアクセスに失敗しました。\n{0}", ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                /*
                System.Windows.Forms.MessageBox.Show(message, "Error!!",
                   System.Windows.Forms.MessageBoxButtons.OK,
                   System.Windows.Forms.MessageBoxIcon.Error);
                 * */
            }
            finally
            {
                if (workBook != null)
                {
                    workBook.Close(saveChange);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workBook);
                    workBook = null;
                }

                if (workBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workBooks);
                    workBooks = null;
                }

                if (excel != null)
                {
                    excel.Quit();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
                    excel = null;
                }

                // 強制的にガーベージコレクションを実行
                GC.Collect();
            }

        }

        private void radioOn_Checked(object sender, RoutedEventArgs e)
        {
            this.OnlineContents.Visibility = Visibility.Visible;
        }

        private void radioOff_Checked(object sender, RoutedEventArgs e)
        {
            this.OnlineContents.Visibility = Visibility.Hidden;
        }

        private Dictionary<string, string> paramMap = new Dictionary<string, string>();
        private void image_CancelToGet_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.Bw != null)
            {
                this.Bw.CancelAsync();
            }

            DownloadCts.Cancel();
            DownloadCts = new CancellationTokenSource();
            CancelGripData();
            AisUtil.LoadProgressBarImage(this.image_Gage, false);

            this.button_Get.IsEnabled = true;
            this.image_CancelToGet.IsEnabled = false;
        }

        #region 内容修正

        /// <summary>
        /// 収集開始日時と収集終了日時を取込みAIS画面に表示
        /// </summary>
        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // param
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var paramMap = readParam(worksheetParam);

                int firstRowIndex = 0;
                int firstColumnIndex = 3;

                //paramの有効行の算出
                int rowLength = AisUtil.GetRowLength_SSG(worksheetParam, firstRowIndex, firstColumnIndex);

                //paramの有効列の算出
                int columnLength = AisUtil.GetColumnLength_SSG(worksheetParam, firstRowIndex, firstColumnIndex);
                System.Data.DataTable dtElementWithId = AisUtil.CreateDataTableFromExcel_SSG(worksheetParam, firstRowIndex, firstColumnIndex, rowLength, columnLength);
                System.Data.DataTable dtElement = AisUtil.DeleteIdRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                System.Data.DataTable dtId = AisUtil.DeleteNameRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                var elementColumnDic = AisUtil.CountElementColumn(dtElementWithId);

                if (paramMap.ContainsKey("収集タイプ") &&
                    !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                {
                    switch (paramMap["収集タイプ"])
                    {
                        case "CTM":
                            this.DataSource = DataSourceType.CTM_DIRECT;
                            break;
                        case "ミッション":
                            this.DataSource = DataSourceType.MISSION;
                            break;
                        case "GRIP":
                            this.DataSource = DataSourceType.GRIP;
                            break;
                        default:
                            break;
                    }
                }

                // set SelectedMission
                if (paramMap.ContainsKey("ミッションID") &&
                    !string.IsNullOrEmpty(paramMap["ミッションID"]))
                {
                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        string missionId = paramMap["ミッションID"];
                        Mission mission = AisUtil.GetMissionFromId(missionId);
                        this.SelectedMission = mission;
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        string missionId = paramMap["ミッションID"];
                        GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                        this.SelectedGripMission = mission;
                    }
                }

                this.dtId = dtId;

                this.paramMap = paramMap;

                DateTime dtStart = new DateTime();
                if (DateTime.TryParse(paramMap["取得開始"], out dtStart))
                {
                    this.dateTimePicker_Start.Value = dtStart;
                }

                DateTime dtEnd = new DateTime();
                if (DateTime.TryParse(paramMap["取得終了"], out dtEnd))
                {
                    this.dateTimePicker_End.Value = dtEnd;
                }

                if (paramMap["終了エレメント"] != "")
                {
                    if (paramMap["終了エレメント"] != null)
                    {
                        this.textBlock_EndTime.Text = (string)paramMap["終了エレメント"];
                    }
                }

                if (paramMap["開始エレメント"] != "")
                {
                    if (paramMap["開始エレメント"] != null)
                    {
                        this.textBlock_StartTime.Text = (string)paramMap["開始エレメント"];
                    }
                }

                if (paramMap["表示期間"] != "")
                {
                    if (paramMap["表示期間"] != null)
                    {
                        this.comboBox_DisplayPeriod.Text = (string)paramMap["表示期間"];
                    }
                }

                if (paramMap["更新周期"] != "")
                {
                    if (paramMap["更新周期"] != null)
                    {
                        this.comboBox_Interval.Text = (string)paramMap["更新周期"];
                    }
                }

                DateTime displayStart = new DateTime();
                if (DateTime.TryParse(paramMap["表示開始期間"], out displayStart))
                {
                    this.dateTimePicker_Start.Value = displayStart;
                }

                DateTime displayEnd = new DateTime();
                if (DateTime.TryParse(paramMap["表示終了期間"], out displayEnd))
                {
                    this.dateTimePicker_End.Value = displayEnd;
                }

                if (!string.IsNullOrEmpty(paramMap["オンライン"]))
                {
                    if (paramMap["オンライン"] == "ONLINE")
                    {
                        this.radioOn.IsChecked = true;
                    }
                    else
                    {
                        this.radioOff.IsChecked = true;
                    }
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private Dictionary<string, string> readParam(SpreadsheetGear.IWorksheet worksheetParam)
        {
            var map = new Dictionary<string, string>();

            SpreadsheetGear.IRange paramCells = worksheetParam.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // サーバIPアドレス
                if (paramName == "サーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // サーバポート番号
                if (paramName == "サーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバIPアドレス
                if (paramName == "GRIPサーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバポート番号
                if (paramName == "GRIPサーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // CTM NAME
                if (paramName == "CTM名")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // CTMID
                if (paramName == "CTMID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // エレメントID
                if (paramName == "エレメントID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 終了エレメント
                if (paramName == "終了エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // 開始エレメント
                if (paramName == "開始エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // 結果エレメント
                if (paramName == "結果エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // ミッション
                if (paramName == "ミッション")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // ミッションID
                if (paramName == "ミッションID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 収集開始日時
                if (paramName == "取得開始")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 収集終了日時
                if (paramName == "取得終了")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 更新周期
                if (paramName == "更新周期")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 周期
                if (paramName == "周期")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 表示期間
                if (paramName == "表示期間")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 表示開始期間
                if (paramName == "表示開始期間")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 表示終了期間
                if (paramName == "表示終了期間")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // オンライン
                if (paramName == "オンライン")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }
            }

            return map;
        }

        /// <summary>
        /// ミッションIDからミッション内容を表示
        /// </summary>
        public void SetMission()
        {
            if (!this.paramMap.ContainsKey("ミッションID"))
            {
                return;
            }
            string selectedMissionId = this.paramMap["ミッションID"];

            this.IsSelectedProgramMission = true;

            var item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(selectedMissionId);
            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_CTM.Focus();
            item.Focus();
        }

        public void SetGripMission()
        {
            if (!this.paramMap.ContainsKey("ミッションID"))
            {
                return;
            }
            string selectedMissionId = this.paramMap["ミッションID"];

            this.IsSelectedProgramMission = true;

            var item = this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(selectedMissionId);
            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(this.SelectedMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_Grip.Focus();
            item.Focus();
        }

        #endregion

        private void radioOn_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            var tree = this.mainWindow.treeView_Catalog;
            List<string> checkedIds = tree.GetCheckedId(true);
            if (checkedIds == null)
            {
                return;
            }

            if (checkedIds.Count < 1)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}