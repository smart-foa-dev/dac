﻿using FoaCore.Common;
using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    public class CtmChildNodeConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(GUID).IsAssignableFrom(objectType);
        }


        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {

            JObject jObj = JObject.Load(reader);
            return DeserializeChildNode(jObj);
        }


        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            CtmChildNode ctmChildNode = (CtmChildNode)value;
            SerializeChildNode(writer, ctmChildNode, serializer);

            //throw new NotImplementedException();
        }
        private void SerializeChildNode(JsonWriter writer, CtmChildNode node, JsonSerializer serializer)
        {
            if (node is CtmElement)
            {
                SerializeCtmElement(writer, (CtmElement)node, serializer);
            }
            if (node is CtmGroup)
            {
                SerializeCtmGroup(writer, (CtmGroup)node, serializer);
            }
        }
        private void SerializeCtmElement(JsonWriter writer, CtmElement node, JsonSerializer serializer)
        {
            JObject token = new JObject();
            token[MmsTvJsonKey.Id] = node.id.ToString();
            token[MmsTvJsonKey.Type] = NodeType.Element;
            token[MmsTvJsonKey.DisplayName] = JArray.Parse(JsonConvert.SerializeObject(node.displayName));
            token[MmsTvJsonKey.Unit] = JArray.Parse(JsonConvert.SerializeObject(node.unit));
            token[MmsTvJsonKey.Comment] = JArray.Parse(JsonConvert.SerializeObject(node.comment));
            if (!string.IsNullOrEmpty(node.cd))
            {
                token[MmsTvJsonKey.Cd] = node.cd;
            }
            token[MmsTvJsonKey.Datatype] = node.datatype;
            token[MmsTvJsonKey.Bulkysubtype] = node.bulkysubtype;
            token["size"] = node.size;

            writer.WriteRaw(token.ToString());
        }
        private void SerializeCtmGroup(JsonWriter writer, CtmGroup node, JsonSerializer serializer)
        {
            JObject token = new JObject();
            token[MmsTvJsonKey.Id] = node.id.ToString();
            token[MmsTvJsonKey.Type] = NodeType.Group;
            token[MmsTvJsonKey.DisplayName] = JArray.Parse(JsonConvert.SerializeObject(node.displayName));
            token[MmsTvJsonKey.Comment] = JArray.Parse(JsonConvert.SerializeObject(node.comment));
            if (!string.IsNullOrEmpty(node.cd))
            {
                token[MmsTvJsonKey.Cd] = node.cd;
            }

            JArray childrenArray = new JArray();
            foreach (CtmChildNode childNode in node.children)
            {
                if (childNode is CtmGroup) (childNode as CtmGroup)._isParse2JsonUsMember = node._isParse2JsonUsMember;
                else if (childNode is CtmElement) (childNode as CtmElement)._isParse2JsonUsMember = node._isParse2JsonUsMember;

                JToken childToken = JToken.Parse(JsonConvert.SerializeObject(childNode));
                childrenArray.Add(childToken);
            }
            if (childrenArray.Count > 0)
            {
                token[MmsTvJsonKey.Children] = childrenArray;
            }

            writer.WriteRaw(token.ToString());
        }


        private CtmChildNode DeserializeChildNode(JToken token)
        {
            var type = (string)token["type"];
            if (type == "el")
            {
                return DeserializeElement(token);
            }
            else
            {
                return DeserializeGroup(token);
            }
        }

        private CtmElement DeserializeElement(JToken token)
        {
            CtmElement elem = new CtmElement();
            elem.id = new GUID((string)token[MmsTvJsonKey.Id]);
            elem.cd = (string)token[MmsTvJsonKey.Cd];
            foreach (var x in (JArray)token[MmsTvJsonKey.DisplayName])
            {
                elem.displayName.Put((string)x[MmsTvJsonKey.Lang], (string)x[MmsTvJsonKey.Text]);
                //MlItem item = new MlItem();
                //item.lang = (string)x["lang"];
                //item.text = (string)x["text"];
                //elem.displayName.Add(item);
            }
            var comment = (JArray)token[MmsTvJsonKey.Comment];
            if (comment != null)
            {
                foreach (var x in comment)
                {
                    elem.comment.Put((string)x[MmsTvJsonKey.Lang], (string)x[MmsTvJsonKey.Text]);
                    //MlItem item = new MlItem();
                    //item.lang = (string)x["lang"];
                    //item.text = (string)x["text"];
                    //elem.comment.Add(item);
                }
            }

            if (token[MmsTvJsonKey.Datatype].Type == JTokenType.Integer)
            {
                elem.datatype = token[MmsTvJsonKey.Datatype].Value<int>();
            }
            else
            {
                elem.datatype = FoaDatatype.key[token[MmsTvJsonKey.Datatype].ToString().ToUpper()];
            }
            if (elem.datatype == FoaDatatype.BULKY)
            {
                if (token[MmsTvJsonKey.Bulkysubtype] != null)
                {
                    if (token[MmsTvJsonKey.Bulkysubtype].Type == JTokenType.Integer)
                    {
                        elem.bulkysubtype = token[MmsTvJsonKey.Bulkysubtype].Value<int>();
                    }
                    else
                    {
                        elem.bulkysubtype = FoaDatatype.bulkysubtypekey[token[MmsTvJsonKey.Bulkysubtype].ToString().ToUpper()];
                    }
                }
                else
                {
                    elem.bulkysubtype = FoaDatatype.NORMALBULKY;
                }
            }
            var unit = (JArray)token[MmsTvJsonKey.Unit];
            if (unit != null)
            {
                foreach (var x in unit)
                {
                    elem.unit.Put((string)x[MmsTvJsonKey.Lang], (string)x[MmsTvJsonKey.Text]);
                    //MlItem item = new MlItem();
                    //item.lang = (string)x["lang"];
                    //item.text = (string)x["text"];
                    //elem.unit.Add(item);
                }
            }
            elem.size = token["size"] == null ? 0 : token["size"].Value<int>();

            return elem;
        }

        private CtmGroup DeserializeGroup(JToken token)
        {
            CtmGroup elem = new CtmGroup();
            elem.id = new GUID((string)token["id"]);
            elem.cd = (string)token["cd"];
            foreach (var x in (JArray)token["displayName"])
            {
                elem.displayName.Put((string)x["lang"], (string)x["text"]);
                //MlItem item = new MlItem();
                //item.lang = (string)x["lang"];
                //item.text = (string)x["text"];
                //elem.displayName.Add(item);
            }
            var comment = (JArray)token["comment"];
            if (comment != null)
            {
                foreach (var x in comment)
                {
                    elem.comment.Put((string)x["lang"], (string)x["text"]);
                    //MlItem item = new MlItem();
                    //item.lang = (string)x["lang"];
                    //item.text = (string)x["text"];
                    //elem.comment.Add(item);
                }
            }

            var children = (JArray)token["children"];
            if (children != null)
            {
                foreach (var x in children)
                {
                    if (token["_isParse2JsonUsMember"] != null) x["_isParse2JsonUsMember"] = token["_isParse2JsonUsMember"];
                    elem.children.Add(DeserializeChildNode(x));
                }
            }

            return elem;
        }
    }
}
