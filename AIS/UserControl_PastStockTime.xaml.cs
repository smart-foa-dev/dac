﻿using DAC.AExcel;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using FoaCore.Common.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_PastStockTime.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_PastStockTime : BaseControl
    {
        private bool isFirstFile = true;
        private string excelFilePath = string.Empty;

        private const string EXCEL_MACRO = "CallBeforeExcelFromAIS";

        private Dictionary<string, string> paramMap = new Dictionary<string, string>();

        /// <summary>
        /// 更新周期
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        List<KeyValuePair<string, TimeSpan>> intervalItems = new List<KeyValuePair<string, TimeSpan>>();

        /// <summary>
        /// 表示期間
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        List<KeyValuePair<string, TimeSpan>> displayPeriodItems = new List<KeyValuePair<string, TimeSpan>>();

        public UserControl_PastStockTime(bool isFirstFile, string excelFilePath)
        {
            InitializeComponent();

            this.isFirstFile = isFirstFile;
            this.excelFilePath = excelFilePath;

            this.Mode = ControlMode.PastStockTime;
            this.TEMPLATE = "PastStockTimeTemplate.xlsm";

            // 画像読み込み
            string fileName = "PastStockTime.png";
            if (AisConf.UiLang == "en")
            {
                fileName = "en_PastStockTime.png";
            }

            string uri = string.Format(@"/DAC;component/Resources/Image/{0}", fileName);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(uri, UriKind.RelativeOrAbsolute);
            bi.EndInit();
            this.image_Thumbnail.Source = bi;

            this.image_OpenExcel.Deactivate();

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }

            this.dateTimePicker_End.Value = DateTime.Now;
            setKeyValuePair();
            setComboBoxItem();

            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }
        }

        private void setKeyValuePair()
        {
            // 更新周期
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("5", new TimeSpan(0, 5, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("10", new TimeSpan(0, 10, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("30", new TimeSpan(0, 30, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("60", new TimeSpan(1, 0, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("120", new TimeSpan(2, 0, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("180", new TimeSpan(3, 0, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("240", new TimeSpan(4, 0, 0)));

            // 表示期間
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("1", new TimeSpan(1, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("2", new TimeSpan(2, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("3", new TimeSpan(3, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("6", new TimeSpan(6, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("12", new TimeSpan(12, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("24", new TimeSpan(24, 0, 0)));
        }

        private void setComboBoxItem()
        {
            // 更新周期
            foreach (var pair in this.intervalItems)
            {
                this.comboBox_Interval.Items.Add(pair.Key);
            }
            this.comboBox_Interval.SelectedItem = "60";

            // 表示期間
            foreach (var pair in this.displayPeriodItems)
            {
                this.comboBox_DisplayPeriod.Items.Add(pair.Key);
            }
            this.comboBox_DisplayPeriod.SelectedItem = "1";
        }

        #region load Excel

        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet paramSheet = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var paramMap = readParam(paramSheet);

                int firstRowIndex = 0;
                int firstColumnIndex = 3;

                int rowLength = AisUtil.GetRowLength_SSG(paramSheet, firstRowIndex, firstColumnIndex);
                int columnLength = AisUtil.GetColumnLength_SSG(paramSheet, firstRowIndex, firstColumnIndex);

                System.Data.DataTable dtElementWithId = AisUtil.CreateDataTableFromExcel_SSG(paramSheet, firstRowIndex, firstColumnIndex, rowLength, columnLength);
                System.Data.DataTable dtElement = AisUtil.DeleteIdRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                System.Data.DataTable dtId = AisUtil.DeleteNameRow(AisUtil.DeepCopyDataTable(dtElementWithId));

                if (paramMap.ContainsKey("収集タイプ") &&
                    !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                {
                    switch (paramMap["収集タイプ"])
                    {
                        case "CTM":
                            this.DataSource = DataSourceType.CTM_DIRECT;
                            break;
                        case "ミッション":
                            this.DataSource = DataSourceType.MISSION;
                            break;
                        case "GRIP":
                            this.DataSource = DataSourceType.GRIP;
                            break;
                        default:
                            break;
                    }
                }

                // set SelectedMission
                if (paramMap.ContainsKey("ミッションID") &&
                    !string.IsNullOrEmpty(paramMap["ミッションID"]))
                {
                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        string missionId = paramMap["ミッションID"];
                        Mission mission = AisUtil.GetMissionFromId(missionId);
                        this.SelectedMission = mission;
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        string missionId = paramMap["ミッションID"];
                        GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                        this.SelectedGripMission = mission;
                    }
                }

                this.dtId = dtId;
                this.paramMap = paramMap;

                if (paramMap.ContainsKey("取得開始"))
                {
                    DateTime dtStart = new DateTime();
                    if (DateTime.TryParse(paramMap["取得開始"], out dtStart))
                    {
                        this.dateTimePicker_Start.Value = dtStart;
                    }
                }

                if (paramMap.ContainsKey("取得終了"))
                {
                    DateTime dtEnd = new DateTime();
                    if (DateTime.TryParse(paramMap["取得終了"], out dtEnd))
                    {
                        this.dateTimePicker_End.Value = dtEnd;
                    }
                }

                if (paramMap.ContainsKey("開始エレメント"))
                {
                    this.textBlock_StartTime.Text = paramMap["開始エレメント"];
                }

                if (paramMap.ContainsKey("終了エレメント"))
                {
                    this.textBlock_EndTime.Text = paramMap["終了エレメント"];
                }

                if (paramMap.ContainsKey("表示期間"))
                {
                    this.comboBox_DisplayPeriod.Text = paramMap["表示期間"];
                }

                if (paramMap.ContainsKey("周期"))
                {
                    this.comboBox_Interval.Text = paramMap["周期"];
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private Dictionary<string, string> readParam(SpreadsheetGear.IWorksheet worksheetParam)
        {
            var map = new Dictionary<string, string>();

            SpreadsheetGear.IRange paramCells = worksheetParam.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // サーバIPアドレス
                if (paramName == "サーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // サーバポート番号
                if (paramName == "サーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // CTM NAME
                if (paramName == "CTM名")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // CTMID
                if (paramName == "CTMID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // エレメントID
                if (paramName == "エレメントID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 開始エレメント
                if (paramName == "開始エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // 終了エレメント
                if (paramName == "終了エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // ミッションID
                if (paramName == "ミッションID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 取得開始
                if (paramName == "取得開始")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 取得終了
                if (paramName == "取得終了")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 周期
                if (paramName == "周期")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 表示期間
                if (paramName == "表示期間")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバIPアドレス
                if (paramName == "GRIPサーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバポート番号
                if (paramName == "GRIPサーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }
            }

            return map;
        }

        /// <summary>
        /// ミッションIDからミッション内容を表示
        /// </summary>
        public void SetMission()
        {
            if (!this.paramMap.ContainsKey("ミッションID"))
            {
                return;
            }
            string selectedMissionId = this.paramMap["ミッションID"];

            this.IsSelectedProgramMission = true;

            var item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(selectedMissionId);
            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                }
                return;
            }
            item.IsSelected = true;

            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_CTM.Focus();
            item.Focus();
        }

        /// <summary>
        /// ミッションIDからミッション内容を表示
        /// </summary>
        public void SetGripMission()
        {
            if (!this.paramMap.ContainsKey("ミッションID"))
            {
                return;
            }
            string selectedMissionId = this.paramMap["ミッションID"];
            var item = this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(selectedMissionId);
            if (item == null)
            {
                if (this.SelectedGripMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(this.SelectedGripMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_Grip.Focus();
            item.Focus();
        }
        #endregion

        #region Event Handler

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();
            this.editControl.PastStockTime = this;

            //言語が英語の場合の表示調整
            string cultureName = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;
            int heightvalue = 36;
            int topvalue = 7;
            if (cultureName == "en-US")
            {
                //Interval
                this.canvas_Interval.Height = heightvalue;
                Canvas.SetTop(this.comboBox_Interval, topvalue);

                //DisplayPeriod
                this.canvas_DisplayPeriod.Height = heightvalue;
                Canvas.SetTop(this.comboBox_DisplayPeriod, topvalue);
            }

            BitmapImage biCancel = new BitmapImage();
            biCancel.BeginInit();
            biCancel.UriSource = new Uri(@"/DAC;component/Resources/Image/cancel-to-get.png", UriKind.RelativeOrAbsolute);
            biCancel.EndInit();
            this.image_CancelToGet.Source = biCancel;
            this.image_CancelToGet.IsEnabled = false;

            BitmapImage biGage = new BitmapImage();
            biGage.BeginInit();
            biGage.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar-empty.png", UriKind.RelativeOrAbsolute);
            biGage.EndInit();
            this.image_Gage.Source = biGage;

            // No.479 In the case of Graph template screen, Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;

            if (!this.isFirstFile)
            {
                try
                {
                    readExcelFile(this.excelFilePath);
                    this.image_OpenExcel.Activate();
                }
                catch (Exception ex)
                {
                    string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                    message += string.Format("\n" + ex.StackTrace);
#endif
                    AisMessageBox.DisplayErrorMessageBox(message);
                }
            }
        }

        /// <summary>
        /// 「Edit」
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button_Edit_Click(object sender, RoutedEventArgs e)
        {
            if (this.mainWindow.CtmDtailsCtrl.GetCtmDtailsEmpty() == string.Empty)
            {
                FoaMessageBox.ShowError("AIS_E_009");
                return;
            }

            // 入力値チェック
            if (this.dateTimePicker_Start.Value == null || this.dateTimePicker_End.Value == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return;
            }

            if (this.dateTimePicker_End.Value <= this.dateTimePicker_Start.Value)
            {
                FoaMessageBox.ShowError("AIS_E_019");
                return;
            }

            if (this.dateTimePicker_End.Value > DateTime.Now)
            {
                FoaMessageBox.ShowError("AIS_E_056");
                return;
            }


            if (string.IsNullOrEmpty(this.textBlock_StartTime.Text) ||
                string.IsNullOrEmpty(this.textBlock_EndTime.Text))
            {
                FoaMessageBox.ShowError("AIS_E_003");
                return;
            }

            DateTime getStart;
            DateTime getEnd;
            bool gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out getStart, out getEnd);
            if (!gotTime)
            {
                return;
            }

            int displayPeriod = 0;
            if (!int.TryParse(this.comboBox_DisplayPeriod.Text.Trim(), out displayPeriod))
            {
                FoaMessageBox.ShowError("AIS_E_020");
                return;
            }

            int interval = 0;
            if (!int.TryParse(this.comboBox_Interval.Text.Trim(), out interval))
            {
                FoaMessageBox.ShowError("AIS_E_023");
                return;
            }

            DateTime displayStart = getEnd - new TimeSpan(displayPeriod, 0, 0);
            DateTime displayEnd = getEnd;

            //AISTEMP-79 sunyi 20190111 start
            //DataTime確認
            MessageBoxResult result = FoaMessageBox.ShowConfirm("AIS_I_008");
            if (result.ToString() == "No")
            {
                return;
            }
            //AISTEMP-79 sunyi 20190111 end

            try
            {
                string filePathDest = string.Empty;

                preRetrieveCtmData();

                this.DownloadCts = new CancellationTokenSource();
                var dirRetrieve = await RetrieveCtmData(getStart, getEnd, this.DownloadCts);
                if (dirRetrieve == null)
                {
                    image_OpenExcel.Deactivate();
                    return;
                }

                string startElement = this.textBlock_StartTime.Text.Trim();
                string endElement = this.textBlock_EndTime.Text.Trim();
                string aggregateInterval = interval.ToString();
                bool isOnline = false;

                this.Bw = new BackgroundWorker();
                this.Bw.WorkerSupportsCancellation = true;

                // define the event handlers
                Bw.DoWork += delegate(object s, DoWorkEventArgs args)
                {
                    var configParams = new Dictionary<ExcelConfigParam, object>();
                    configParams.Add(ExcelConfigParam.KAISHI_ELEM, startElement);
                    configParams.Add(ExcelConfigParam.SHURYO_ELEM, endElement);
                    configParams.Add(ExcelConfigParam.SHUTOKU_KAISHI, getStart.ToString(AisUtil.DateTimeFormat));
                    configParams.Add(ExcelConfigParam.SHUTOKU_SHURYO, getEnd.ToString(AisUtil.DateTimeFormat));

                    configParams.Add(ExcelConfigParam.RELOAD_INTERVAL, aggregateInterval);
                    configParams.Add(ExcelConfigParam.DISPLAY_PERIOD, displayPeriod.ToString());

                    configParams.Add(ExcelConfigParam.DISPLAY_START, displayStart.ToString(AisUtil.DateTimeFormat));
                    configParams.Add(ExcelConfigParam.DISPLAY_END, displayEnd.ToString(AisUtil.DateTimeFormat));

                    configParams.Add(ExcelConfigParam.ONLINE_FLAG, isOnline);

                    configParams.Add(ExcelConfigParam.TEMPLATE_NAME, "過去在庫状況");

                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                        configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetCatalogLang(this.SelectedMission));
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                        configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetGripCatalogLang(this.SelectedGripMission));
                    }

                    if (!isFirstFile)
                    {
                        configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(this.excelFilePath));
                    }

                    filePathDest = writeResultToExcel_X(getStart, getEnd, this.Bw, this.DataSource, dirRetrieve, configParams);

                    if (string.IsNullOrEmpty(filePathDest))
                    {
                        FoaMessageBox.ShowError("AIS_E_001");
                        return;
                    }
                };
                Bw.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
                {
                    this.Bw = null;

                    bool success = true;
                    if (args.Error != null)  // if an exception occurred during DoWork,
                    {
                        // Do your error handling here
                        AisMessageBox.DisplayErrorMessageBox(args.Error.ToString());
                        success = false;
                    }

                    postRetrieveCtmData(success);

                    this.tmpResultFolderPath = dirRetrieve;

                    this.excelFilePath = filePathDest;
                };
                Bw.RunWorkerAsync(); // starts the background worker
            }
            catch (Exception ex)
            {
                string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                AisMessageBox.DisplayErrorMessageBox(message);
            }
            finally
            {
                this.button_Edit.IsEnabled = true;
                AisUtil.LoadProgressBarImage(this.image_Gage, false);
            }
        }

        private void image_OpenExcel_Tap(object sender, RoutedEventArgs e)
        {
            if (this.tmpResultFolderPath!= null)
            {
                var reultFiles = Directory.GetFiles(this.tmpResultFolderPath, "*.csv");
                if (reultFiles.Length == 0)
                {
                    AisUtil.LoadProgressBarImage(this.image_Gage, false);
                    FoaMessageBox.ShowError("AIS_E_006");
                    return;
                }
            }

            if (IsFileInUse(this.excelFilePath))
            {
                return;
            }

            // Wang Issue AISTEMP-65 Start
            ////マクロ起動処理
            //getParamsFromExcel(this.excelFilePath, EXCEL_MACRO, true);

            ////Excel Open
            ///*
            //var processStartInfo = new ProcessStartInfo();
            //processStartInfo.FileName = this.excelFilePath;
            //Process process = Process.Start(processStartInfo);
            // * */
            //ExcelUtil.OpenExcelFile(this.excelFilePath);
            
            mainWindow.LoadinVisibleChange(false);

            OpenExcelWorker = new BackgroundWorker();
            OpenExcelWorker.WorkerReportsProgress = true;
            OpenExcelWorker.DoWork += new DoWorkEventHandler(OpenExcel);
            OpenExcelWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OpenExcelCompleted);
            OpenExcelWorker.RunWorkerAsync();
            // Wang Issue AISTEMP-65 End
        }

        // Wang Issue AISTEMP-65 Start
        BackgroundWorker OpenExcelWorker;
        private void OpenExcelCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                mainWindow.LoadinVisibleChange(true);
            });
        }
        private void OpenExcel(object sender, DoWorkEventArgs e)
        {
            //マクロ起動処理
            getParamsFromExcel(this.excelFilePath, EXCEL_MACRO, true);

            //Excel Open
            /*
            var processStartInfo = new ProcessStartInfo();
            processStartInfo.FileName = this.excelFilePath;
            Process process = Process.Start(processStartInfo);
             * */
            ExcelUtil.OpenExcelFile(this.excelFilePath);
        }
        // Wang Issue AISTEMP-65 End

        private bool IsFileInUse(string excel_file)
        {
            FileInfo file = new FileInfo(excel_file);
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        /// <summary>
        /// Cancel
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void image_CancelToGet_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.Bw != null)
            {
                this.Bw.CancelAsync();
            }

            this.DownloadCts.Cancel();
            this.DownloadCts = new CancellationTokenSource();
            CancelGripData();
            AisUtil.LoadProgressBarImage(this.image_Gage, false);

            this.button_Edit.IsEnabled = true;
            this.image_CancelToGet.IsEnabled = false;
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void textBlock_StartTime_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.textBlock_StartTime.Text = string.Format("{0}・{1} ", ary[0], ary[2]);
        }

        private void textBlock_EndTime_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.textBlock_EndTime.Text = string.Format("{0}・{1} ", ary[0], ary[2]);
        }

        private void textBlock_StartTime_Click(object sender, MouseButtonEventArgs e)
        {
            this.textBlock_StartTime.Text = string.Empty;
        }

        private void textBlock_EndTime_Click(object sender, MouseButtonEventArgs e)
        {
            this.textBlock_EndTime.Text = string.Empty;
        }

        #endregion

        #region Write / Open Excel

        /// <summary>
        /// 開始処理
        /// </summary>
        private void preRetrieveCtmData()
        {
            this.button_Edit.IsEnabled = false;
            this.image_CancelToGet.IsEnabled = true;
            this.image_OpenExcel.Deactivate();
            AisUtil.LoadProgressBarImage(this.image_Gage, true);
        }

        /// <summary>
        /// 終了処理
        /// </summary>
        /// <param name="success"></param>
        private void postRetrieveCtmData(bool success)
        {
            if (success)
            {
                this.image_OpenExcel.Activate();
            }

            AisUtil.LoadProgressBarImage(this.image_Gage, false);
            this.image_CancelToGet.IsEnabled = false;
            this.button_Edit.IsEnabled = true;
        }

        private string writeResultToExcel_X(DateTime start, DateTime end, BackgroundWorker bw, DataSourceType resultType,
            string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            string filePathSrc = isFirstFile ? System.IO.Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE) : this.excelFilePath;
            string filePathDest = getExcelFilepath(resultType);

            File.Copy(filePathSrc, filePathDest);

            var writer = new SSGExcelWriterStockTime(this.Mode, this.DataSource, this.editControl.Ctms, this.editControl.Dt.Copy(), bw, this.SelectedMission);
            writer.WriteCtmData(filePathDest, folderPath, configParams);

            return filePathDest;
        }

        private void getParamsFromExcel(string filepath, string macro, bool saveChange)
        {
            Microsoft.Office.Interop.Excel.Application excel = null;
            Microsoft.Office.Interop.Excel.Workbooks books = null;
            Microsoft.Office.Interop.Excel.Workbook book = null;

            try
            {
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = false;
                ExcelUtil.MoveExcelOutofScreen(excel);
                books = excel.Workbooks;
                book = books.Open(filepath);

                // マクロの実行
                excel.Run(macro);
            }
            catch (Exception ex)
            {
                string message = string.Format("Excelファイルへのアクセスに失敗しました。\n{0}", ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                /*
                System.Windows.Forms.MessageBox.Show(message, "Error!!",
                   System.Windows.Forms.MessageBoxButtons.OK,
                   System.Windows.Forms.MessageBoxIcon.Error);
                 * */
            }
            finally
            {
                if (book != null)
                {
                    book.Close(saveChange);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(book);
                    book = null;
                }

                if (books != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(books);
                    books = null;
                }

                if (excel != null)
                {
                    excel.Quit();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
                    excel = null;
                }

                // 強制的にガーベージコレクションを実行
                GC.Collect();
            }
        }

        #endregion

        // No.493 Input control of period setting item. ----------------Start
        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }
        // No.493 Input control of period setting item. ----------------End

    }
}
