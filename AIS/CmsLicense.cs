﻿using DAC.View.Helpers;
using FoaCore.Common;
using FoaCore.Common.Net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace DAC.Model
{
    /// <summary>
    /// MMS-UIで使用するライセンス管理クラス (ReadOnly)
    /// </summary>
    public static class CmsLicense
    {
        /// <summary>
        /// ユーザID
        /// </summary>
        // public static String userId;

        /// <summary>
        /// 1CTMで設定できる最大CTMエレメント数
        /// </summary>
        public static int maxElements;
        /// <summary>
        /// 1ドメインで設定できる最大CTM数
        /// </summary>
        public static int maxCtms;
        /// <summary>
        /// 1ドメインで設定できる最大AH数
        /// </summary>
        public static int maxAhs;
        public static int maxCgs;

        public static int maxUsers;

        public static int maxDomains;

        /// <summary>
        /// 初期化 (ライセンスデータを取得する)
        /// </summary>
        public static void Init(Control ctrl)
        {
            // ユーザID取得
            // userId = LoginInfo.GetInstance().GetLoginUserInfo().UserId;

            // ライセンスデータを取得する。
            // CmsHttpClient client = new CmsHttpClient((Cms.Common.View.MainWindow)Application.Current.MainWindow);
            CmsHttpClient client = new CmsHttpClient(ctrl);
            client.CompleteHandler += resJson =>
            {
                var jsonData = JsonConvert.DeserializeObject<List<LicenseData>>(resJson);
                ObservableCollection<List<LicenseData>> LicenseInfoData = new ObservableCollection<List<LicenseData>>();
                LicenseInfoData.Add(jsonData);
                List<LicenseData> licenseList = LicenseInfoData[0];
                try
                {
                    foreach (LicenseData license in licenseList)
                    {
                        switch (license.Item)
                        {

                            case "MAX_DOMAIN":
                                maxDomains = int.Parse(license.Limit);
                                break;
                            case "MAX_FE":
                                maxAhs = int.Parse(license.Limit);
                                break;

                            case "MAX_CG":
                                maxCgs = int.Parse(license.Limit);
                                break;

                            case "MAX_CTM":
                                maxCtms = int.Parse(license.Limit);
                                break;

                            case "MAX_ELEMENT":
                                maxElements = int.Parse(license.Limit);
                                break;

                            // case "MAX_MISSION":
                            //    break;

                            case "MAX_USERS":
                                maxUsers = int.Parse(license.Limit);
                                break;

                            default:
                                break;
                        }
                    }
                }
                catch (Exception ex)
                {
                    AisMessageBox.DisplayErrorMessageBox(ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine + ex.Source);
                    //MessageBox.Show(ex.Message);
                    //MessageBox.Show(ex.StackTrace);
                    //MessageBox.Show(ex.Source);
                    throw ex;
                }
            };
            client.Get(CmsUrl.GetLicenseInfo());
        }
    }
}
