﻿using DAC.AExcel;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using FoaCore.Common.Util;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;


namespace DAC.Model
{
    /// <summary>
    /// UserControl_StockTimeTemplate.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_StockTimeTemplate : BaseControl
    {
        /// <summary>
        /// 選択した終了時刻
        /// </summary>
        private DateTime _dtEnd { get; set; }

        /// <summary>
        /// 選択した開始時刻
        /// </summary>
        private DateTime _dtStart { get; set; }

        private const string EXCEL_MACRO = "CallBeforeExcelFromAIS";
        private const int PARAM_SHEET_MAX_PARAMS = 100;

        private bool isFirstFile = true;
        private string excelFilePath = string.Empty;

        /// <summary>
        /// 更新周期
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        List<KeyValuePair<string, TimeSpan>> intervalItems = new List<KeyValuePair<string, TimeSpan>>();

        /// <summary>
        /// 表示期間
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        List<KeyValuePair<string, TimeSpan>> displayPeriodItems = new List<KeyValuePair<string, TimeSpan>>();

        /// <summary>
        /// コンストラクタ
        /// </summary>
        public UserControl_StockTimeTemplate(bool isFirstFile = true, string filePath = "")
        {
            InitializeComponent();

            this.Mode = ControlMode.StockTime;
            this.TEMPLATE = "StockTimeTemplate.xlsm";

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            // 画像読み込み
            string fileName = "Inventory.png";
            if (AisConf.UiLang == "en")
            {
                fileName = "en_Inventory.png";
            }

            string uri = string.Format(@"/DAC;component/Resources/Image/{0}", fileName);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(uri, UriKind.RelativeOrAbsolute);
            bi.EndInit();
            this.image_Thumbnail.Source = bi;

            this.image_OpenExcel.Deactivate();

            //キャンバスの非表示
            cnvsHaikei.Visibility = Visibility.Hidden;

            cnvsElement1.Visibility = Visibility.Hidden;
            cnvsElement2.Visibility = Visibility.Hidden;
            cnvsElement3.Visibility = Visibility.Hidden;

            //エレメント条件
            cmbElement1.Items.Add("==");
            cmbElement1.Items.Add("!=");

            cmbElement2.Items.Add("==");
            cmbElement2.Items.Add("!=");

            cmbElement3.Items.Add("==");
            cmbElement3.Items.Add("!=");

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }

            this.dateTimePicker_End.Value = DateTime.Now;
            setKeyValuePair();
            setComboBoxItem();

            bttnSyosai.Visibility = Visibility.Hidden;

            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }
        }

        private void setKeyValuePair()
        {
            // 更新周期
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("10", new TimeSpan(0, 10, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("30", new TimeSpan(0, 30, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("60", new TimeSpan(1, 0, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("120", new TimeSpan(2, 0, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("180", new TimeSpan(3, 0, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("240", new TimeSpan(4, 0, 0)));

            // 表示期間
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("1", new TimeSpan(1, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("2", new TimeSpan(2, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("3", new TimeSpan(3, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("6", new TimeSpan(6, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("12", new TimeSpan(12, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("24", new TimeSpan(24, 0, 0)));
        }

        private void setComboBoxItem()
        {
            // 更新周期
            foreach (var pair in this.intervalItems)
            {
                this.comboBox_Interval.Items.Add(pair.Key);
            }
            this.comboBox_Interval.SelectedItem = "60";

            // 表示期間
            foreach (var pair in this.displayPeriodItems)
            {
                this.comboBox_DisplayPeriod.Items.Add(pair.Key);
            }
            this.comboBox_DisplayPeriod.SelectedItem = "1";
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();

            //言語が英語の場合の表示調整
            string cultureName = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;
            int heightvalue = 36;
            int topvalue = 7;
            if (cultureName == "en-US")
            {
                //Interval
                this.canvas_Interval.Height = heightvalue;
                Canvas.SetTop(this.comboBox_Interval, topvalue);

                //DisplayPeriod
                this.canvas_DisplayPeriod.Height = heightvalue;
                Canvas.SetTop(this.comboBox_DisplayPeriod, topvalue);
            }

            BitmapImage biCancel = new BitmapImage();
            biCancel.BeginInit();
            biCancel.UriSource = new Uri(@"/DAC;component/Resources/Image/cancel-to-get.png", UriKind.RelativeOrAbsolute);
            biCancel.EndInit();
            this.image_CancelToGet.Source = biCancel;

            this.image_CancelToGet.IsEnabled = false;

            BitmapImage biGage = new BitmapImage();
            biGage.BeginInit();
            biGage.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar-empty.png", UriKind.RelativeOrAbsolute);
            biGage.EndInit();
            this.image_Gage.Source = biGage;

            this.editControl.StockTime = this;

            // No.479 In the case of Graph template screen, Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;

            if (!this.isFirstFile)
            {
                try
                {
                    readExcelFile(this.excelFilePath);
                    this.image_OpenExcel.Activate();
                }
                catch (Exception ex)
                {
                    string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                    message += string.Format("\n" + ex.StackTrace);
#endif
                    AisMessageBox.DisplayErrorMessageBox(message);
                }
            }
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        /// <summary>
        /// 開始時刻エレメントDrag&Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_StartTime_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.textBlock_StartTime.Text = string.Format("{0}・{1} ", ary[0], ary[2]);
        }

        /// <summary>
        /// 終了時刻エレメントDrag&Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_EndTime_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.textBlock_EndTime.Text = string.Format("{0}・{1} ", ary[0], ary[2]);
        }

        /// <summary>
        /// 【詳細】ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //詳細画面表示有無チェック
            if (cnvsHaikei.Visibility == Visibility.Hidden)
            {
                //詳細画面表示
                cnvsHaikei.Visibility = Visibility.Visible;

                cnvsElement1.Visibility = Visibility.Visible;
                cnvsElement2.Visibility = Visibility.Visible;
                cnvsElement3.Visibility = Visibility.Visible;
            }
            else
            {
                //詳細画面非表示
                cnvsHaikei.Visibility = Visibility.Hidden;

                cnvsElement1.Visibility = Visibility.Hidden;
                cnvsElement2.Visibility = Visibility.Hidden;
                cnvsElement3.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// 背景データ削除（指定項目のみ）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bttnDelete_Click(object sender, RoutedEventArgs e)
        {
            //背景データ選択有無チェック(-1:未選択)
            if (lstHaikei.SelectedIndex < 0)
            {
                //背景データ未選択
            }
            else
            {
                //背景データ削除
                lstHaikei.Items.RemoveAt(lstHaikei.SelectedIndex);
            }
        }

        /// <summary>
        /// 日付の更新
        /// </summary>
        /// <param name="dt"></param>
        public void updateTime(DateTime? dt, DateTimePicker p)
        {
            p.Value = dt;
        }
        public void updateTime(DateTime? dt, System.Windows.Controls.TextBox t)
        {
            t.Text = ((DateTime)dt).ToString(AisUtil.DisplayTimeFormat);
        }

        /// <summary>
        /// 有効化/無効化切替
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.X) // CTRL + X -> Cut Function
            {
                e.Handled = true;
            }
        }
        private void DtPickerStart_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                if (e.OriginalSource is Xceed.Wpf.Toolkit.DateTimePicker)
                {
                    _dtStart = (DateTime)this.dateTimePicker_Start.Value;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }
        private void DtPickerEnd_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                if (e.OriginalSource is Xceed.Wpf.Toolkit.DateTimePicker)
                {
                    _dtEnd = (DateTime)this.dateTimePicker_End.Value;
                }
            }
            catch (Exception ex)
            { throw ex; }
        }

        // Accept numeric number only as input
        private void numberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !isTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool isTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        /// <summary>
        /// 開始時刻エレメント削除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_StartTime_Click(object sender, MouseButtonEventArgs e)
        {
            textBlock_StartTime.Text = "";
        }

        /// <summary>
        /// 終了エレメント削除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void textBlock_EndTime_Click(object sender, MouseButtonEventArgs e)
        {
            textBlock_EndTime.Text = "";
        }

        /// <summary>
        /// 背景データ取込
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstHaikei_Drop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.lstHaikei.Items.Add(string.Format("{0}・{1}", ary[0], ary[2]));
        }

        /// <summary>
        /// 背景データ手入力追加Drag&Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void txtHaikei_KeyDown(object sender, KeyEventArgs e)
        {
            if ((Keyboard.Modifiers == ModifierKeys.None) && (e.Key == Key.Enter))
            {
                string wrkStr = txtHaikei.Text;
                this.lstHaikei.Items.Add(txtHaikei.Text);
                this.txtHaikei.Text = "";
            }
        }

        private void bttnAdd_Click(object sender, RoutedEventArgs e)
        {
            if ((txtHaikei.Text != "") && (txtHaikei.Text != null))
            {
                this.lstHaikei.Items.Add(txtHaikei.Text);
                this.txtHaikei.Text = "";
            }

        }

        private void image_OpenExcel_Tap(object sender, RoutedEventArgs e)
        {
            if (this.tmpResultFolderPath != null)
            {
                var reultFiles = Directory.GetFiles(this.tmpResultFolderPath, "*.csv");
                if (reultFiles.Length == 0)
                {
                    AisUtil.LoadProgressBarImage(this.image_Gage, false);
                    FoaMessageBox.ShowError("AIS_E_006");
                    return;
                }
            }

            // Wang Issue AISTEMP-78 Start
            //// ワークブック作成
            //string filePathDest = this.excelFilePath;

            ////マクロ起動処理
            //getParamsFromExcel(filePathDest, EXCEL_MACRO, true);

            ////EXCELﾌｧｲﾙ表示
            ///*
            //var processStartInfo = new ProcessStartInfo();
            //processStartInfo.FileName = filePathDest;
            //Process process = Process.Start(processStartInfo);
            // * */
            //ExcelUtil.OpenExcelFile(filePathDest);

            mainWindow.LoadinVisibleChange(false);

            OpenExcelWorker = new BackgroundWorker();
            OpenExcelWorker.WorkerReportsProgress = true;
            OpenExcelWorker.DoWork += new DoWorkEventHandler(OpenExcel);
            OpenExcelWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OpenExcelCompleted);
            OpenExcelWorker.RunWorkerAsync();
            // Wang Issue AISTEMP-78 End
        }

        // Wang Issue AISTEMP-78 Start
        BackgroundWorker OpenExcelWorker;
        private void OpenExcelCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Dispatcher.Invoke(() =>
            {
                mainWindow.LoadinVisibleChange(true);
            });
        }
        private void OpenExcel(object sender, DoWorkEventArgs e)
        {
            // ワークブック作成
            string filePathDest = this.excelFilePath;

            //マクロ起動処理
            getParamsFromExcel(filePathDest, EXCEL_MACRO, true);

            //EXCELﾌｧｲﾙ表示
            /*
            var processStartInfo = new ProcessStartInfo();
            processStartInfo.FileName = filePathDest;
            Process process = Process.Start(processStartInfo);
             * */
            ExcelUtil.OpenExcelFile(filePathDest);
        }
        // Wang Issue AISTEMP-78 End

        #region Excelファイルの作成

        /// <summary>
        /// ミッションからの情報取得（「GET」ボタンクリック時の処理）
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button_Get_Click(object sender, RoutedEventArgs e)
        {
            if (this.mainWindow.CtmDtailsCtrl.GetCtmDtailsEmpty() == string.Empty)
            {
                FoaMessageBox.ShowError("AIS_E_009");
                return;
            }

            // 入力値チェック
            if (this.dateTimePicker_Start.Value == null || this.dateTimePicker_End.Value == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return;
            }

            if (this.dateTimePicker_End.Value <= this.dateTimePicker_Start.Value)
            {
                FoaMessageBox.ShowError("AIS_E_019");
                return;
            }

            if (this.dateTimePicker_End.Value < DateTime.Now)
            {
                FoaMessageBox.ShowError("AIS_E_024");
                return;
            }

            if (string.IsNullOrEmpty(this.textBlock_StartTime.Text) ||
                string.IsNullOrEmpty(this.textBlock_EndTime.Text))
            {
                FoaMessageBox.ShowError("AIS_E_003");
                return;
            }

            DateTime getStart;
            DateTime getEnd;
            bool gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out getStart, out getEnd);
            if (!gotTime)
            {
                return;
            }

            int displayPeriod = 0;
            if (!int.TryParse(this.comboBox_DisplayPeriod.Text.Trim(), out displayPeriod))
            {
                FoaMessageBox.ShowError("AIS_E_020");
                return;
            }

            int interval = 0;
            if (!int.TryParse(this.comboBox_Interval.Text.Trim(), out interval))
            {
                FoaMessageBox.ShowError("AIS_E_023");
                return;
            }

            DateTime displayStart = getEnd - new TimeSpan(displayPeriod, 0, 0);
            DateTime displayEnd = getEnd;

            //AISTEMP-79 sunyi 20190111 start
            //DataTime確認
            MessageBoxResult result = FoaMessageBox.ShowConfirm("AIS_I_008");
            if (result.ToString() == "No")
            {
                return;
            }
            //AISTEMP-79 sunyi 20190111 end

            try
            {
                string filePathDest = string.Empty;

                preRetrieveCtmData();

                this.DownloadCts = new CancellationTokenSource();
                var dirRetrieve = await RetrieveCtmData(getStart, getEnd, this.DownloadCts);
                if (dirRetrieve == null)
                {
                    image_OpenExcel.Deactivate();
                    return;
                }

                string startElement = this.textBlock_StartTime.Text.Trim();
                string endElement = this.textBlock_EndTime.Text.Trim();
                string aggregateInterval = interval.ToString();

                this.Bw = new BackgroundWorker();
                this.Bw.WorkerSupportsCancellation = true;

                // define the event handlers
                Bw.DoWork += delegate(object s, DoWorkEventArgs args)
                {
                    var configParams = new Dictionary<ExcelConfigParam, object>();
                    configParams.Add(ExcelConfigParam.KAISHI_ELEM, startElement);
                    configParams.Add(ExcelConfigParam.SHURYO_ELEM, endElement);
                    configParams.Add(ExcelConfigParam.SHUTOKU_KAISHI, getStart.ToString(AisUtil.DateTimeFormat));
                    configParams.Add(ExcelConfigParam.SHUTOKU_SHURYO, getEnd.ToString(AisUtil.DateTimeFormat));

                    configParams.Add(ExcelConfigParam.RELOAD_INTERVAL, aggregateInterval);
                    configParams.Add(ExcelConfigParam.DISPLAY_PERIOD, displayPeriod.ToString());

                    configParams.Add(ExcelConfigParam.DISPLAY_START, displayStart.ToString(AisUtil.DateTimeFormat));
                    configParams.Add(ExcelConfigParam.DISPLAY_END, displayEnd.ToString(AisUtil.DateTimeFormat));

                    configParams.Add(ExcelConfigParam.ONLINE_FLAG, true);

                    configParams.Add(ExcelConfigParam.TEMPLATE_NAME, "在庫状況");

                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                        configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetCatalogLang(this.SelectedMission));
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        configParams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                        configParams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetGripCatalogLang(this.SelectedGripMission));
                    }

                    if (!isFirstFile)
                    {
                        configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, AisUtil.GetFileNameFromPath(this.excelFilePath));
                    }

                    filePathDest = writeResultToExcel_X(getStart, getEnd, this.Bw, this.DataSource, dirRetrieve, configParams);

                    if (string.IsNullOrEmpty(filePathDest))
                    {
                        FoaMessageBox.ShowError("AIS_E_001");
                        return;
                    }
                };
                Bw.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
                {
                    this.Bw = null;

                    bool success = true;
                    if (args.Error != null)  // if an exception occurred during DoWork,
                    {
                        // Do your error handling here
                        AisMessageBox.DisplayErrorMessageBox(args.Error.ToString());
                        success = false;
                    }

                    postRetrieveCtmData(success);

                    this.tmpResultFolderPath = dirRetrieve;

                    //EXCELﾌｧｲﾙ名保存
                    this.excelFilePath = filePathDest;
                };
                Bw.RunWorkerAsync(); // starts the background worker
            }
            catch (Exception ex)
            {
                string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                AisMessageBox.DisplayErrorMessageBox(message);
            }
            finally
            {
                this.button_Get.IsEnabled = true;
                AisUtil.LoadProgressBarImage(this.image_Gage, false);
            }
        }

        /// <summary>
        /// 開始処理
        /// </summary>
        private void preRetrieveCtmData()
        {
            this.button_Get.IsEnabled = false;
            this.image_CancelToGet.IsEnabled = true;
            this.image_OpenExcel.Deactivate();
            AisUtil.LoadProgressBarImage(this.image_Gage, true);
        }

        /// <summary>
        /// 終了処理
        /// </summary>
        /// <param name="success"></param>
        private void postRetrieveCtmData(bool success)
        {
            if (success)
            {
                this.image_OpenExcel.Activate();
            }

            AisUtil.LoadProgressBarImage(this.image_Gage, false);
            this.image_CancelToGet.IsEnabled = false;
            this.button_Get.IsEnabled = true;
        }

        private string writeResultToExcel_X(DateTime start, DateTime end, BackgroundWorker bWorker, DataSourceType resultType,
            string folderPath, Dictionary<ExcelConfigParam, object> configParams)
        {
            // コピー元ファイルの絶対パス
            string filePathSrc = isFirstFile ? System.IO.Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE) : this.excelFilePath;
            var filePathDest = getExcelFilepath(resultType);

            File.Copy(filePathSrc, filePathDest);

            var writer = new SSGExcelWriterStockTime(this.Mode, this.DataSource, this.editControl.Ctms, this.editControl.Dt.Copy(), bWorker, this.SelectedMission);
            writer.WriteCtmData(filePathDest, folderPath, configParams);

            return filePathDest;
        }

        #endregion

        /// <summary>
        /// EXCELマクロをAISより実行：EXCELオープンから起動される。
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="macro"></param>
        /// <param name="saveChange"></param>
        private void getParamsFromExcel(string filepath, string macro, bool saveChange)
        {
            Microsoft.Office.Interop.Excel.Application excel = null;
            Microsoft.Office.Interop.Excel.Workbooks workBooks = null;
            Microsoft.Office.Interop.Excel.Workbook workBook = null;

            try
            {
                //マクロ実行準備
                excel = new Microsoft.Office.Interop.Excel.Application();
                excel.Visible = false;
                ExcelUtil.MoveExcelOutofScreen(excel);
                workBooks = excel.Workbooks;
                workBook = workBooks.Open(filepath);

                var paramSheet = workBook.Worksheets[Keywords.PARAM_SHEET];

                Dispatcher.Invoke(() =>
                {
                    writeConfigSheetNoSSG(paramSheet);
                });

                // マクロの実行
                excel.Run(macro);
            }
            catch (Exception ex)
            {
                string message = string.Format("Excelファイルへのアクセスに失敗しました。\n{0}", ex.Message);

#if DEBUG
                message += string.Format("\n" + ex.StackTrace);
#endif
                /*
                System.Windows.Forms.MessageBox.Show(message, "Error!!",
                   System.Windows.Forms.MessageBoxButtons.OK,
                   System.Windows.Forms.MessageBoxIcon.Error);
                 * */

            }
            finally
            {
                if (workBook != null)
                {
                    workBook.Close(saveChange);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workBook);
                    workBook = null;
                }

                if (workBooks != null)
                {
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workBooks);
                    workBooks = null;
                }

                if (excel != null)
                {
                    excel.Quit();
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excel);
                    excel = null;
                }

                // 強制的にガーベージコレクションを実行
                GC.Collect();
            }
        }

        /// <summary>
        /// エレメント条件Drag&Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtblcElement3_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.txtblcElement3.Text = string.Format("{0}", ary[2]);
        }

        private void txtblcElement2_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.txtblcElement2.Text = string.Format("{0} ", ary[2]);
        }

        private void txtblcElement1_Drop(object sender, DragEventArgs e)
        {
            TextBlock tb = sender as TextBlock;
            if (tb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string item = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] ary = item.Split(',');
            if (ary.Length < 5)
            {
                return;
            }

            this.txtblcElement1.Text = string.Format("{0}", ary[2]);
        }

        private void writeConfigSheetNoSSG(Microsoft.Office.Interop.Excel.Worksheet worksheetParam)
        {
            var cellsParam = worksheetParam.Cells;
            Range srchRange;

            // 終了エレメント
            srchRange = getFindRange2(cellsParam, "終了エレメント");
            if (srchRange != null) srchRange[1, 2].Value = textBlock_EndTime.Text.Trim();

            // 開始エレメント
            srchRange = getFindRange2(cellsParam, "開始エレメント");
            if (srchRange != null) srchRange[1, 2].Value = textBlock_StartTime.Text.Trim();

            // 結果エレメント

            // 取得開始
            srchRange = getFindRange2(cellsParam, "取得開始");
            if (srchRange != null) srchRange[1, 2].Value = dateTimePicker_Start.Text.Trim();

            // 取得終了
            srchRange = getFindRange2(cellsParam, "取得終了");
            if (srchRange != null) srchRange[1, 2].Value = dateTimePicker_End.Text.Trim();

            // 背景データ
            srchRange = getFindRange2(cellsParam, "背景データ");
            if (srchRange != null)
            {
                if (lstHaikei.Items.Count > 0)
                {
                    for (int i = 0; i < lstHaikei.Items.Count; i++)
                    {
                        srchRange[1 + i, 1].Value = "背景データ";
                        srchRange[1 + i, 2].Value = lstHaikei.Items[i].ToString().Trim();
                    }
                }
            }

            //エレメント条件設定
            if ((txtElement1.Text != "") || (txtblcElement1.Text != ""))
            {
                srchRange = getFindRange2(cellsParam, "エレメント条件1");
                if (srchRange != null)
                {
                    srchRange[1, 2].Value = txtblcElement1.Text.Trim();
                    srchRange[1, 3].Value = "'" + cmbElement1.Text.ToString().Trim();
                    if ((bool)rdoOr1.IsChecked)
                    {
                        srchRange[1, 4].Value = "OR";
                    }
                    else
                    {
                        srchRange[1, 4].Value = "AND";
                    }
                    srchRange[1, 5].Value = txtElement1.Text.Trim();
                }
            }
            if ((txtElement2.Text != "") || (txtblcElement2.Text != ""))
            {
                srchRange = getFindRange2(cellsParam, "エレメント条件2");
                if (srchRange != null)
                {
                    srchRange[1, 2].Value = txtblcElement2.Text.Trim();
                    srchRange[1, 3].Value = "'" + cmbElement2.Text.ToString().Trim();
                    if ((bool)rdoOr2.IsChecked)
                    {
                        srchRange[1, 4].Value = "OR";
                    }
                    else
                    {
                        srchRange[1, 4].Value = "AND";
                    }
                    srchRange[1, 5].Value = txtElement2.Text.Trim();
                }
            }
            if ((txtElement3.Text != "") || (txtblcElement3.Text != ""))
            {
                srchRange = getFindRange2(cellsParam, "エレメント条件3");
                if (srchRange != null)
                {
                    srchRange[1, 2].Value = txtblcElement3.Text.Trim();
                    srchRange[1, 3].Value = "'" + cmbElement3.Text.ToString().Trim();
                    if ((bool)rdoOr3.IsChecked)
                    {
                        srchRange[1, 4].Value = "OR";
                    }
                    else
                    {
                        srchRange[1, 4].Value = "AND";
                    }
                    srchRange[1, 5].Value = txtElement3.Text.Trim();
                }
            }
        }

        /// <summary>
        /// Range取込
        /// </summary>
        /// <param name="srchRange"></param>
        /// <param name="strWhat"></param>
        /// <returns></returns>
        private Range getFindRange2(Range srchRange, String strWhat)
        {
            return srchRange.Find(strWhat,
                                Type.Missing,
                                XlFindLookIn.xlValues,
                                XlLookAt.xlWhole,
                                XlSearchOrder.xlByRows,
                                XlSearchDirection.xlNext,
                                false,
                                Type.Missing, Type.Missing);
        }

        private Dictionary<string, string> paramMap = new Dictionary<string, string>();

        /// <summary>
        /// 収集開始日時と収集終了日時を取込みAIS画面に表示
        /// </summary>
        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet paramSheet = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var paramMap = readParam(paramSheet);

                int firstRowIndex = 0;
                int firstColumnIndex = 3;

                //paramの有効行の算出
                int rowLength = AisUtil.GetRowLength_SSG(paramSheet, firstRowIndex, firstColumnIndex);

                //paramの有効列の算出
                int columnLength = AisUtil.GetColumnLength_SSG(paramSheet, firstRowIndex, firstColumnIndex);
                System.Data.DataTable dtElementWithId = AisUtil.CreateDataTableFromExcel_SSG(paramSheet, firstRowIndex, firstColumnIndex, rowLength, columnLength);
                System.Data.DataTable dtElement = AisUtil.DeleteIdRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                System.Data.DataTable dtId = AisUtil.DeleteNameRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                var elementColumnDic = AisUtil.CountElementColumn(dtElementWithId);

                if (paramMap.ContainsKey("収集タイプ") &&
                    !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                {
                    switch (paramMap["収集タイプ"])
                    {
                        case "CTM":
                            this.DataSource = DataSourceType.CTM_DIRECT;
                            break;
                        case "ミッション":
                            this.DataSource = DataSourceType.MISSION;
                            break;
                        case "GRIP":
                            this.DataSource = DataSourceType.GRIP;
                            break;
                        default:
                            break;
                    }
                }

                // set SelectedMission
                if (paramMap.ContainsKey("ミッションID") &&
                    !string.IsNullOrEmpty(paramMap["ミッションID"]))
                {
                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        string missionId = paramMap["ミッションID"];
                        Mission mission = AisUtil.GetMissionFromId(missionId);
                        this.SelectedMission = mission;
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        string missionId = paramMap["ミッションID"];
                        GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                        this.SelectedGripMission = mission;
                    }
                }

                this.dtId = dtId;
                this.paramMap = paramMap;
                
                DateTime dtStart = new DateTime();
                if (DateTime.TryParse(paramMap["取得開始"], out dtStart))
                {
                    this.dateTimePicker_Start.Value = dtStart;
                }

                DateTime dtEnd = new DateTime();
                if (DateTime.TryParse(paramMap["取得終了"], out dtEnd))
                {
                    this.dateTimePicker_End.Value = dtEnd;
                }

                if (paramMap["終了エレメント"] != "")
                {
                    if (paramMap["終了エレメント"] != null)
                    {
                        this.textBlock_EndTime.Text = (string)paramMap["終了エレメント"];
                    }
                }

                if (paramMap["開始エレメント"] != "")
                {
                    if (paramMap["開始エレメント"] != null)
                    {
                        this.textBlock_StartTime.Text = (string)paramMap["開始エレメント"];
                    }
                }

                if (paramMap["表示期間"] != "")
                {
                    if (paramMap["表示期間"] != null)
                    {
                        this.comboBox_DisplayPeriod.Text = (string)paramMap["表示期間"];
                    }
                }

                if (paramMap["周期"] != "")
                {
                    if (paramMap["周期"] != null)
                    {
                        this.comboBox_Interval.Text = (string)paramMap["周期"];
                    }
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private Dictionary<string, string> readParam(SpreadsheetGear.IWorksheet worksheetParam)
        {
            var map = new Dictionary<string, string>();

            SpreadsheetGear.IRange paramCells = worksheetParam.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // サーバIPアドレス
                if (paramName == "サーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // サーバポート番号
                if (paramName == "サーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // CTM NAME
                if (paramName == "CTM名")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // CTMID
                if (paramName == "CTMID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // エレメントID
                if (paramName == "エレメントID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 終了エレメント
                if (paramName == "終了エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // 開始エレメント
                if (paramName == "開始エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // 結果エレメント
                if (paramName == "結果エレメント")
                {
                    if (paramValue != null)
                    {
                        map.Add(paramName, paramValue);
                        continue;
                    }
                    else
                    {
                        map.Add(paramName, "");
                        continue;
                    }
                }

                // ミッションID
                if (paramName == "ミッションID")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 周期
                if (paramName == "周期")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 表示期間
                if (paramName == "表示期間")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 取得開始
                if (paramName == "取得開始")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 取得終了
                if (paramName == "取得終了")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバIPアドレス
                if (paramName == "GRIPサーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバポート番号
                if (paramName == "GRIPサーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }
            }

            return map;
        }

        /// <summary>
        /// ミッションIDからミッション内容を表示
        /// </summary>
        public void setMission()
        {
            if (!this.paramMap.ContainsKey("ミッションID"))
            {
                return;
            }
            string selectedMissionId = this.paramMap["ミッションID"];

            this.IsSelectedProgramMission = true;

            var item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(selectedMissionId);
            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_CTM.Focus();
            item.Focus();
        }

        /// <summary>
        /// ミッションIDからミッション内容を表示
        /// </summary>
        public void SetGripMission()
        {
            if (!this.paramMap.ContainsKey("ミッションID"))
            {
                return;
            }
            string selectedMissionId = this.paramMap["ミッションID"];
            var item = this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(selectedMissionId);
            if (item == null)
            {
                if (this.SelectedGripMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(this.SelectedGripMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_Grip.Focus();
            item.Focus();
        }

        private void image_CancelToGet_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.Bw != null)
            {
                this.Bw.CancelAsync();
            }

            DownloadCts.Cancel();
            DownloadCts = new CancellationTokenSource();
            CancelGripData();
            AisUtil.LoadProgressBarImage(this.image_Gage, false);

            this.button_Get.IsEnabled = true;
            this.image_CancelToGet.IsEnabled = false;
        }

        // No.493 Input control of period setting item. ----------------Start
        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }
        // No.493 Input control of period setting item. ----------------End
    }
}