﻿using FoaCore.Common.Entity;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DAC.Model
{
    public class WorkPlaceInfo : CmsEntity
    {
        public string missionId { get; set; }
        public string GroupName { get; set; }
    }
}
