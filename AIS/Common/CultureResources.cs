﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Threading;

namespace DAC.Common
{
    /// <summary>
    /// CultureResources による言語切り替えに対応した ObjectDataProvider
    /// </summary>
    public class CultureResourceProvider : ObjectDataProvider
    {
        protected override void OnQueryFinished(object newData, Exception error, DispatcherOperationCallback completionWork, object callbackArguments)
        {
            base.OnQueryFinished(newData, error, completionWork, callbackArguments);

            CultureResources.RegisterProvider(newData.GetType(), this);
        }
    }

    ///// <summary>
    ///// カルチャーに則ったリソースの切替機能を提供します。
    ///// </summary>
    public static class CultureResources
    {
        // リソースの追加方法
        // 1. resxファイルを作る。
        // 2. App.xamlにリソースのObjectDataProviderを作ってバインディング出来るようにする。
        // 3. アプリケーションの初期化時に CultureResources.RegisterProvider で全ての ObjectDataProvider を登録する
        //    (App.Application_Startup を参照)
        //    もしくは ObjectDataProvider の代わりに CultureResourceProvider を使用して、自動登録する
        // 4. CultureResources.ChangeCultureで、リソースのカルチャーを一括変更する。

        private static Dictionary<ObjectDataProvider, Type> _providers = new Dictionary<ObjectDataProvider, Type>();

        /// <summary>
        /// 言語リソースに対応する ObjectDataProvider を登録します.
        /// </summary>
        /// <param name="resourceType"></param>
        /// <param name="provider"></param>
        public static void RegisterProvider(Type resourceType, ObjectDataProvider provider)
        {
            if (!_providers.ContainsKey(provider))
                _providers.Add(provider, resourceType);
        }

        /// <summary>
        /// リソースのインスタンスを取得します。
        /// </summary>
        /// <param name="resourceType"></param>
        /// <returns></returns>
        public static object GetResourceInstance(Type resourceType)
        {
            return Activator.CreateInstance(resourceType, true);
        }

        /// <summary>
        /// カルチャーを変更します。
        /// </summary>
        /// <param name="cultureName"></param>
        public static void ChangeCulture(string cultureName)
        {
            CultureInfo culture = new CultureInfo(cultureName);

            // スレッドのカルチャーも切替(フォーマット時に現在のカルチャーを参照するので)
            Thread.CurrentThread.CurrentCulture = culture;
            Thread.CurrentThread.CurrentUICulture = culture;

            // 登録済みの言語リソースのカルチャーを切り替えて参照するバインディングを全て更新
            foreach (var entry in _providers.ToArray())
            {
                var cultureProperty = entry.Value.GetProperty("Culture", BindingFlags.Public | BindingFlags.Static);
                if (cultureProperty != null)
                {
                    cultureProperty.SetValue(null, culture);
                    entry.Key.Refresh();
                }
            }
        }
    }

}
