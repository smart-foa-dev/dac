﻿using FoaCore.Common.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using Xceed.Wpf.Toolkit.Primitives;

namespace DAC.Common
{

    public class UserNameValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            var inputValue = value as string;
            return CommonValidator.IsValudValue(CommonValidator.BaseDigit, inputValue);
        }
    }

    public class CharacterValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            var inputValue = value as string;
            ValidationResult res = CommonValidator.IsValudValue(CommonValidator.BaseDigit, inputValue);
            if (res.IsValid)
            {
                return CommonValidator.IsBaseRegexValue(inputValue);
            }
            else
            {
                return res;
            }
        }
    }

    public class XssAndCharMaxLengthValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            var inputValue = value as string;
            return CommonValidator.IsValudXssAndMaxLenValue(CommonValidator.BaseDigit, inputValue);
        }
    }

    public class MailAddressValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            var inputValue = value as string;
            if (CommonValidator.IsValidEmail(inputValue))
            {
                return CommonValidator.IsValudValue(CommonValidator.BaseDigit, inputValue);
            }
            else
            {
                return new ValidationResult(false, DAC.Properties.Resources.ISINVALID_MAILFORMAT);
            }
        }
    }

    public class PasswordValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, System.Globalization.CultureInfo cultureInfo)
        {
            var inputValue = value as string;
            return CommonValidator.IsValudValue(CommonValidator.PasswordDigit, inputValue);
        }
    }

    /// <summary>
    /// 共通のバリデーションを提供します。
    /// </summary>
    public class AisValidation
    {
        /// <summary>
        /// 必須バリデーション(TextのPropertyを持つコントロール)
        /// </summary>
        /// <param name="ctrl"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static bool Required(Control ctrl, string name)
        {
            PropertyInfo property = ctrl.GetType().GetProperty("Text");
            String value = "";
            if (property != null)
            {
                value = (string)property.GetValue(ctrl, null);
            }
            return Required(ctrl, value, name);
        }

        /// <summary>
        /// 文字列の必須バリデーション
        /// </summary>
        /// <param name="s"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private static bool Required(Control ctrl, string s, string name)
        {
            if (string.IsNullOrEmpty(s))
            {
                ctrl.Focus();
                FoaMessageBox.ShowError("C_REQUIRED", name);
                return false;
            }
            return true;
        }

        /// <summary>
        /// 有効なファイル名か検証します。
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool ValidFileName(string s, string errorCode)
        {
            if (s.IndexOfAny(System.IO.Path.GetInvalidFileNameChars()) >= 0)
            {
                FoaMessageBox.ShowError(errorCode);
                return false;
            }
            return true;
        }

    }

    public class CommonValidator
    {
        /// <summary>
        /// 最大桁数基準値
        /// </summary>
        public const int BaseDigit = 64;

        /// <summary>
        /// パスワード最大桁数
        /// </summary>
        public const int PasswordDigit = 32;

        /// <summary>
        /// XSSチェック用
        /// </summary>
        const string XssErrorPattern = ".*[<>&\"',].*";

        /// <summary>
        /// 使用可能文字
        /// </summary>
        const string RegexPattern = "^[a-zA-Z0-9_-]+$";

        /// <summary>
        /// メールアドレスパターン
        /// </summary>
        const string EmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                                             + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                                             + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

        /// <summary>
        /// Reserved characters
        /// </summary>
        readonly char[] reservedCharacters = new [] {
                                                    '>',  // less than
                                                    '<',  // greater than
                                                    ':',  // colon
                                                    '"',  // double quote
                                                    '/',  // forward slash
                                                    '\\', // backslash
                                                    '|',  // vertical bar or pipe
                                                    '?',  // question mark
                                                    '*'   // asterisk
                                            };

        /// <summary>
        /// Reserved Charactersチェック
        /// </summary>
        /// <param name="value">対象文字列</param>
        /// <returns>チェック結果</returns>
        public bool ReservedCharactersCheck(string value)
        {
            if (value.IndexOfAny(reservedCharacters) != -1)
                return true;
            else
                return false;
        }

        /// <summary>
        /// XSSチェック
        /// </summary>
        /// <param name="value">対象文字列</param>
        /// <returns>チェック結果</returns>
        public static bool XSSCheck(string value)
        {
            return !Regex.IsMatch(value, XssErrorPattern);
        }

        /// <summary>
        /// 使用可能文字列チェック
        /// </summary>
        /// <param name="value">対象文字列</param>
        /// <returns>チェック結果</returns>
        public static bool RegexCheck(string value)
        {
            return Regex.IsMatch(value, RegexPattern);
        }

        /// <summary>
        /// 文字列長入力チェック
        /// </summary>
        /// <param name="value">対象文字列</param>
        /// <returns>チェック結果</returns>
        public static bool CharInputCheck(string value)
        {
            return !string.IsNullOrEmpty(value);
        }

        /// <summary>
        /// 文字列長チェック
        /// </summary>
        /// <param name="digit">桁数</param>
        /// <param name="value">対象文字列</param>
        /// <returns>チェック結果</returns>
        public static bool CharLengthCheck(int digit, string value)
        {
            return (value.Length <= digit);
        }

        /// <summary>
        /// 基本バリデーション
        /// </summary>
        /// <param name="digit">桁数</param>
        /// <param name="value">対象文字列</param>
        /// <returns>チェック結果</returns>
        public static ValidationResult IsValudValue(int digit, string value)
        {
            if (!CharInputCheck(value))
            {
                return new ValidationResult(false, DAC.Properties.Resources.ISINVALID_INPUT);
            }
            if (!XSSCheck(value))
            {
                return new ValidationResult(false, DAC.Properties.Resources.ISINVALID_XSSCHK);
            }
            if (!CharLengthCheck(digit, value))
            {
                return new ValidationResult(false, string.Format(DAC.Properties.Resources.ISINVALID_LENGTH, digit));
            }
            return ValidationResult.ValidResult;
        }

        /// <summary>
        /// XSSと最大文字列長バリデーション
        /// </summary>
        /// <param name="digit">桁数</param>
        /// <param name="value">対象文字列</param>
        /// <returns>チェック結果</returns>
        public static ValidationResult IsValudXssAndMaxLenValue(int digit, string value)
        {
            if (!XSSCheck(value))
            {
                return new ValidationResult(false, DAC.Properties.Resources.ISINVALID_XSSCHK);
            }
            if (!CharLengthCheck(digit, value))
            {
                return new ValidationResult(false, string.Format(DAC.Properties.Resources.ISINVALID_LENGTH, digit));
            }
            return ValidationResult.ValidResult;
        }

        /// <summary>
        /// 半角英数記号チェック
        /// </summary>
        /// <param name="value">対象文字列</param>
        /// <returns>チェック結果</returns>
        public static ValidationResult IsBaseRegexValue(string value)
        {
            if (!RegexCheck(value))
            {
                return new ValidationResult(false, DAC.Properties.Resources.ISINVALID_VALIDCHAR);
            }
            return ValidationResult.ValidResult;
        }

        /// <summary>
        /// Eメールパターンチェック
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static bool IsValidEmail(string emailAddress)
        {
            return Regex.IsMatch(emailAddress, EmailPattern, RegexOptions.IgnoreCase);
        }

        /// <summary>
        /// 有効パスワードチェック
        /// </summary>
        /// <param name="password">パスワード</param>
        /// <param name="toolTip">tooltip</param>
        /// <returns>チェック結果</returns>
        public static bool IsValidPassword(string password, ref string toolTip)
        {
            if (!CharInputCheck(password))
            {
                toolTip = DAC.Properties.Resources.ISINVALID_INPUT;
                return false;
            }
            if (!CharLengthCheck(PasswordDigit, password))
            {
                toolTip = string.Format(DAC.Properties.Resources.ISINVALID_LENGTH, PasswordDigit);
                return false;
            }
            if (!RegexCheck(password))
            {
                toolTip = DAC.Properties.Resources.ISINVALID_XSSCHK;
                return false;
            }
            return true;
        }
    }
}
