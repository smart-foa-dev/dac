﻿using DAC.AExcel;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using DAC.View.Helpers;
using FoaCore.Common.Util;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_Comment.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_Comment : BaseControl
    {
        //ISSUE_NO.616,708 sunyi 2018/05/31 start
        //ミッションを変更したら設定内容をクリアする。
        //private ObservableCollection<ElementListBoxItem> elementList;
        //private ObservableCollection<BackDataListBoxItem> backDataList;
        //private ObservableCollection<BackDataListBoxItem> backDataListForDisplay;
        public ObservableCollection<ElementListBoxItem> elementList;
        public ObservableCollection<BackDataListBoxItem> backDataList;
        public ObservableCollection<BackDataListBoxItem> backDataListForDisplay;
        //ISSUE_NO.616,708 sunyi 2018/05/31 end

        private bool gotNewResult = false;
        private DateTime dtStart = new DateTime();
        private DateTime dtEnd = new DateTime();
        private bool isFirstFile = true;
        private string excelFilePath = string.Empty;

        private Dictionary<string, string> paramMap = new Dictionary<string, string>();

        public DataSourceType CurrentTreeSelection { get; set; }

        public UserControl_Comment(bool isFirstFile = true, string filePath = "")
        {
            InitializeComponent();
            this.Mode = ControlMode.Comment;

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            // 画像読み込み
            string fileName = "comment.png";
            if (AisConf.UiLang == "en")
            {
                fileName = "en_comment.png";
            }

            string uri = string.Format(@"/DAC;component/Resources/Image/{0}", fileName);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new Uri(uri, UriKind.RelativeOrAbsolute);
            bi.EndInit();
            this.image_Thumbnail.Source = bi;

            this.image_OpenExcel.Deactivate();

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
            this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };
            this.comboBox_End.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
            this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End.Items.Count)
            {
                this.comboBox_End.SelectedIndex = 0;
            }
        }

        public void SetMission()
        {
            if (!this.paramMap.ContainsKey("ミッションID"))
            {
                return;
            }

            TreeViewItem item = null;
            string missionId = this.paramMap["ミッションID"];
            if (string.IsNullOrEmpty(missionId))
            {
                return;
            }

            this.IsSelectedProgramMission = true;

            // Mission Tree
            item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(missionId);
            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                }
                return;
            }

            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_CTM.Focus();
            item.Focus();
        }

        public void SetGripMission()
        {
            if (!this.paramMap.ContainsKey("ミッションID"))
            {
                return;
            }

            TreeViewItem item = null;
            string missionId = this.paramMap["ミッションID"];
            if (string.IsNullOrEmpty(missionId))
            {
                return;
            }

            // Mission Tree
            item = this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(missionId);
            if (item == null)
            {
                if (this.SelectedGripMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(this.SelectedGripMission.Id);
                }
                return;
            }

            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_Grip.Focus();
            item.Focus();
        }

        public void SetInitialList()
        {
            this.elementList.Clear();
            this.backDataList.Clear();
            this.backDataListForDisplay.Clear();
        }

        public void RemoveElementListItemByCtmName(string ctmName)
        {
            foreach (ElementListBoxItem item in this.elementList)
            {
                if (item.CtmName != ctmName)
                {
                    continue;
                }

                removeElementListItem(this.elementList, this.backDataList, this.backDataListForDisplay, item);
            }
        }

        private void removeElementListItem(ObservableCollection<ElementListBoxItem> elemList,
            ObservableCollection<BackDataListBoxItem> backList, ObservableCollection<BackDataListBoxItem> backListForDisplay, ElementListBoxItem item)
        {
            elemList.Remove(item);

            bool hit = false;
            foreach (var el in elemList)
            {
                if (el.CtmId != item.CtmId)
                {
                    continue;
                }
                hit = true;
                break;
            }
            if (hit)
            {
                return;
            }

            List<int> targets = new List<int>();
            for (int i = 0; i < backList.Count; i++)
            {
                if (backList[i].CtmId != item.CtmId)
                {
                    continue;
                }

                targets.Add(i);
            }
            targets.Reverse();
            foreach (int target in targets)
            {
                backList.RemoveAt(target);
            }

            backListForDisplay.Clear();
            foreach (var bd in backListForDisplay)
            {
                backListForDisplay.Add(bd);
            }
        }

        private void removeBackDataItem(ObservableCollection<BackDataListBoxItem> bakcList, BackDataListBoxItem item)
        {
            bakcList.Remove(item);

            int selectedIndex = this.listBox_Element.SelectedIndex;
            this.listBox_Element.SelectedIndex = -1;
            this.listBox_Element.SelectedIndex = selectedIndex;
        }

        #region 内容修正

        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet paramSheet = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                this.paramMap = readParam(paramSheet);

                int firstRowIndex = 0;
                int firstColumnIndex = 3;

                //paramの有効行の算出
                int rowLength = AisUtil.GetRowLength_SSG(paramSheet, firstRowIndex, firstColumnIndex);

                //paramの有効列の算出
                int columnLength = AisUtil.GetColumnLength_SSG(paramSheet, firstRowIndex, firstColumnIndex);

                System.Data.DataTable dtElementWithId = AisUtil.CreateDataTableFromExcel_SSG(paramSheet, firstRowIndex, firstColumnIndex, rowLength, columnLength);
                System.Data.DataTable dtElement = AisUtil.DeleteIdRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                System.Data.DataTable dtId = AisUtil.DeleteNameRow(AisUtil.DeepCopyDataTable(dtElementWithId));

                if (paramMap.ContainsKey("収集タイプ") &&
                    !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                {
                    switch (paramMap["収集タイプ"])
                    {
                        case "CTM":
                            this.DataSource = DataSourceType.CTM_DIRECT;
                            break;
                        case "ミッション":
                            this.DataSource = DataSourceType.MISSION;
                            break;
                        case "GRIP":
                            this.DataSource = DataSourceType.GRIP;
                            break;
                        default:
                            break;
                    }
                }

                // set SelectedMission
                if (paramMap.ContainsKey("ミッションID") &&
                    !string.IsNullOrEmpty(paramMap["ミッションID"]))
                {
                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        string missionId = paramMap["ミッションID"];
                        Mission mission = AisUtil.GetMissionFromId(missionId);
                        this.SelectedMission = mission;
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        string missionId = paramMap["ミッションID"];
                        GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                        this.SelectedGripMission = mission;
                    }
                }

                this.dtId = dtId;
                setElements(dtElement, dtId);

                DateTime dtStart = new DateTime();
                if (DateTime.TryParse(this.paramMap["取得開始"], out dtStart))
                {
                    this.dateTimePicker_Start.Value = dtStart;
                }

                DateTime dtEnd = new DateTime();
                if (DateTime.TryParse(this.paramMap["取得終了"], out dtEnd))
                {
                    this.dateTimePicker_End.Value = dtEnd;
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private Dictionary<string, string> readParam(SpreadsheetGear.IWorksheet paramSheet)
        {
            var paramMap = new Dictionary<string, string>();

            // 収集条件を出力
            SpreadsheetGear.IRange paramCells = paramSheet.Cells;
            for (int i = 0; i < 30; i++)   // とりあえず10×10のセルを検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // ミッション
                if (paramName == "ミッション")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }

                // ミッションID
                if (paramName == "ミッションID")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }

                // 収集開始日時
                if (paramName == "取得開始")
                {
                    string date = paramCells[i, 1].Text;
                    string time = paramCells[i, 2].Text;
                    string datetime = date;

                    paramMap.Add(paramName, datetime);
                    continue;
                }

                // 収集終了日時
                if (paramName == "取得終了")
                {
                    string date = paramCells[i, 1].Text;
                    string time = paramCells[i, 2].Text;
                    string datetime = date;

                    paramMap.Add(paramName, datetime);
                    continue;
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {
                    paramMap.Add(paramName, paramValue);
                    continue;
                }
            }

            return paramMap;
        }

        private void setElements(DataTable dtElement, DataTable dtId)
        {
            for (int i = 0; i < dtElement.Rows.Count; i++)
            {
                DataRow nameRow = dtElement.Rows[i] as DataRow;
                if (nameRow == null)
                {
                    continue;
                }
                DataRow idRow = dtId.Rows[i] as DataRow;
                if (idRow == null)
                {
                    continue;
                }

                string ctmName = nameRow.ItemArray[0].ToString();
                string ctmId = idRow.ItemArray[0].ToString();
                string elementName = nameRow.ItemArray[1].ToString();
                string elementId = idRow.ItemArray[1].ToString();

                ElementListBoxItem item = new ElementListBoxItem();
                item.CtmName = ctmName;
                item.CtmId = ctmId;
                item.ElementName = elementName;
                item.ElementId = elementId;
                this.elementList.Add(item);
                
                for (int j = 2; j < nameRow.ItemArray.Length; j++)
                {
                    string backElementName = nameRow.ItemArray[j].ToString();
                    if (string.IsNullOrEmpty(backElementName))
                    {
                        continue;
                    }
                    string backElementId = idRow.ItemArray[j].ToString();

                    BackDataListBoxItem backItem = new BackDataListBoxItem();
                    backItem.CtmName = ctmName;
                    backItem.CtmId = ctmId;
                    backItem.ElementName = backElementName;
                    backItem.ElementId = backElementId;
                    this.backDataList.Add(backItem);
                }
            }

            this.backDataListForDisplay.Clear();
            foreach (var bd in this.backDataList)
            {
                this.backDataListForDisplay.Add(bd);
            }
        }

        #endregion

        #region Event Handler

        /// <summary>
        /// Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();

            this.TEMPLATE = "CommentTemplate.xlsm";

            BitmapImage biCancel = new BitmapImage();
            biCancel.BeginInit();
            biCancel.UriSource = new Uri(@"/DAC;component/Resources/Image/cancel-to-get.png", UriKind.RelativeOrAbsolute);
            biCancel.EndInit();
            this.image_CancelToGet.Source = biCancel;

            this.image_CancelToGet.IsEnabled = false;

            BitmapImage biGage = new BitmapImage();
            biGage.BeginInit();
            biGage.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar-empty.png", UriKind.RelativeOrAbsolute);
            biGage.EndInit();
            this.image_Gage.Source = biGage;

            this.elementList = new ObservableCollection<ElementListBoxItem>();
            this.listBox_Element.DataContext = this.elementList;

            this.editControl.Comment = this;

            this.backDataList = new ObservableCollection<BackDataListBoxItem>();
            this.backDataListForDisplay = new ObservableCollection<BackDataListBoxItem>();
            this.listBox_BackData.DataContext = this.backDataListForDisplay;

            // No.479 In the case of Graph template screen, Do not display edit button.
            MainWindow m = System.Windows.Application.Current.MainWindow as MainWindow;
            m.CtmDtailsCtrl.button_Selection.Visibility = Visibility.Hidden;

            if (!this.isFirstFile)
            {
                readExcelFile(this.excelFilePath);
                this.image_OpenExcel.Activate();
            }
            if (this.elementList.Count >= 1)
            {
                this.listBox_Element.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox_Element_Drop(object sender, DragEventArgs e)
        {
            var lb = sender as System.Windows.Controls.ListBox;
            if (lb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string items = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = items.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 5)
            {
                return;
            }

            ElementListBoxItem item = new ElementListBoxItem();
            item.CtmName = itemArray[0];
            item.CtmId = itemArray[1];
            item.ElementName = itemArray[2];
            item.ElementId = itemArray[3];
            foreach (var target in this.elementList)
            {
                if (target.CtmId == item.CtmId &&
                    target.ElementId == item.ElementId)
                {
                    return;
                }
            }

            this.elementList.Add(item);
            if (this.elementList.Count == 1)
            {
                this.listBox_Element.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Drop
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void listBox_BackData_Drop(object sender, DragEventArgs e)
        {
            var lb = sender as System.Windows.Controls.ListBox;
            if (lb == null)
            {
                return;
            }

            if (!e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                return;
            }

            string items = (string)e.Data.GetData(DataFormats.StringFormat);
            string[] itemArray = items.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (itemArray.Length < 5)
            {
                return;
            }

            bool hit = false;
            foreach (ElementListBoxItem el in this.elementList)
            {
                if (el.CtmId != itemArray[1])
                {
                    continue;
                }

                hit = true;
                break;
            }
            if (!hit)
            {
                return;
            }

            BackDataListBoxItem item = new BackDataListBoxItem();
            item.CtmName = itemArray[0];
            item.CtmId = itemArray[1];
            item.ElementName = itemArray[2];
            item.ElementId = itemArray[3];
            foreach (var target in this.backDataList)
            {
                if (target.CtmId == item.CtmId &&
                    target.ElementId == item.ElementId)
                {
                    return;
                }
            }
            this.backDataList.Add(item);

            int selectedIndex = this.listBox_Element.SelectedIndex;
            if (selectedIndex == -1 &&
                0 < this.elementList.Count)
            {
                this.listBox_Element.SelectedIndex = 0;
            }
            this.listBox_Element.SelectedIndex = -1;
            this.listBox_Element.SelectedIndex = selectedIndex;
        }

        private async void button_Get_Click(object sender, RoutedEventArgs e)
        {
            if (this.mainWindow.CtmDtailsCtrl.GetCtmDtailsEmpty() == string.Empty)
            {
                FoaMessageBox.ShowError("AIS_E_009");
                return;
            }

            if (this.elementList.Count < 1)
            {
                FoaMessageBox.ShowError("AIS_E_007");
                return;
            }

            DateTime start1;
            DateTime end1;
            bool gotTime = GetStartAndEndTime(this.dateTimePicker_Start, this.dateTimePicker_End, out start1, out end1);
            if (!gotTime)
            {
                return;
            }

            preRetrieveCtmData();
            var resultFolder = await RetrieveCtmData(start1, end1, this.DownloadCts);
            postRetrieveCtmData(resultFolder != null, start1, end1);
            if (resultFolder == null)
            {
                image_OpenExcel.Deactivate();
                return;
            }

            this.tmpResultFolderPath = resultFolder;

            this.gotNewResult = true;
        }

        private void image_OpenExcel_Tap(object sender, RoutedEventArgs e)
        {
            if (!this.gotNewResult)
            {
                //ExcelUtil.StartExcelProcess(this.excelFilePath, true);
                ExcelUtil.OpenExcelFile(this.excelFilePath);

                this.image_OpenExcel.Activate();
                return;
            }

            this.image_CancelToGet.IsEnabled = true;
            openExcelFile(DataSource, this.dtStart, this.dtEnd);
        }

        private void image_CancelToGet_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            CancelGripData();
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private void listBox_Element_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            ElementListBoxItem item = this.listBox_Element.SelectedItem as ElementListBoxItem;
            if (item == null)
            {
                this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                return;
            }

            System.Windows.Forms.ContextMenuStrip cMenu = new System.Windows.Forms.ContextMenuStrip();

            // 削除
            System.Windows.Forms.ToolStripMenuItem menuItem_Delete = new System.Windows.Forms.ToolStripMenuItem();
            menuItem_Delete.Text = "削除";
            menuItem_Delete.Click += delegate
            {
                removeElementListItem(this.elementList, this.backDataList, this.backDataListForDisplay, item);
            };
            cMenu.Items.Add(menuItem_Delete);

            cMenu.Show(System.Windows.Forms.Cursor.Position);
        }

        private void listBox_BackData_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            BackDataListBoxItem item = this.listBox_BackData.SelectedItem as BackDataListBoxItem;
            if (item == null)
            {
                return;
            }

            System.Windows.Forms.ContextMenuStrip cMenu = new System.Windows.Forms.ContextMenuStrip();

            // 削除
            System.Windows.Forms.ToolStripMenuItem menuItem_Delete = new System.Windows.Forms.ToolStripMenuItem();
            menuItem_Delete.Text = "削除";
            menuItem_Delete.Click += delegate
            {
                this.removeBackDataItem(this.backDataList, item);
            };
            cMenu.Items.Add(menuItem_Delete);

            cMenu.Show(System.Windows.Forms.Cursor.Position);
        }

        private void listBox_Element_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.listBox_Element.SelectedIndex = -1;
        }

        private void listBox_Element_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.listBox_Element.SelectedIndex == -1)
            {
                this.backDataListForDisplay.Clear();
                foreach (var bd in this.backDataList)
                {
                    this.backDataListForDisplay.Add(bd);
                }

                return;
            }

            ElementListBoxItem item = this.listBox_Element.SelectedItem as ElementListBoxItem;
            if (item == null)
            {
                return;
            }

            this.backDataListForDisplay.Clear();

            foreach (var bd in this.backDataList)
            {
                if (bd.CtmId != item.CtmId)
                {
                    continue;
                }

                this.backDataListForDisplay.Add(bd);
            }
        }

        #endregion

        #region CTMデータの取得

        private void preRetrieveCtmData()
        {
            this.button_Get.IsEnabled = false;
            this.image_CancelToGet.IsEnabled = true;
            AisUtil.LoadProgressBarImage(this.image_Gage, true);
        }

        /// <summary>
        /// 終了処理
        /// </summary>
        /// <param name="success"></param>
        private void postRetrieveCtmData(bool success, DateTime start, DateTime end)
        {
            if (success)
            {
                this.image_OpenExcel.Activate();
                this.dtStart = start;
                this.dtEnd= end;
            }

            AisUtil.LoadProgressBarImage(this.image_Gage, false);
            this.image_CancelToGet.IsEnabled = false;
            this.button_Get.IsEnabled = true;
        }

        #endregion

        #region Excelファイルの作成・オープン

        private void openExcelFile(DataSourceType resultType, DateTime startTime, DateTime endTime)
        {
            if (this.tmpResultFolderPath != null)
            {
                var reultFiles = Directory.GetFiles(this.tmpResultFolderPath, "*.csv");
                if (reultFiles.Length == 0)
                {
                    AisUtil.LoadProgressBarImage(this.image_Gage, false);
                    FoaMessageBox.ShowError("AIS_E_006");
                    return;
                }
            }

            var cparams = new Dictionary<ExcelConfigParam, object>();
            cparams.Add(ExcelConfigParam.START, startTime.ToString("yyyy/MM/dd HH:mm:ss"));
            cparams.Add(ExcelConfigParam.END, endTime.ToString("yyyy/MM/dd HH:mm:ss"));

            if (this.DataSource == DataSourceType.MISSION)
            {
                cparams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                cparams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetCatalogLang(this.SelectedMission));
            }
            else if (this.DataSource == DataSourceType.GRIP)
            {
                cparams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                cparams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetGripCatalogLang(this.SelectedGripMission));
            }

            executeOpenExcelTask(resultType, cparams);
        }

        private void executeOpenExcelTask(DataSourceType resultType, Dictionary<ExcelConfigParam, object> cParams)
        {
            string filePathDest = string.Empty;

            this.button_Get.IsEnabled = false;


            this.Bw = new BackgroundWorker();
            this.Bw.WorkerSupportsCancellation = true;
            var c = this.editControl.Ctms;
            // define the event handlers
            this.Bw.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                // Will be run on background thread.
                // Do your lengthy stuff here -- this will happen in a separate thread to avoid freezing the UI thread

                string filePathSrc = isFirstFile ? System.IO.Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE) : this.excelFilePath;
                filePathDest = getExcelFilepath(resultType);
                if (!Directory.Exists(resultDir))
                {
                    Directory.CreateDirectory(resultDir);
                }
                File.Copy(filePathSrc, filePathDest);

                writeRetrievedDataToExcel(this.tmpResultFolderPath, this.isFirstFile,
                    filePathSrc, filePathDest, cParams, Bw);

                if (this.Bw.CancellationPending)
                {
                    args.Cancel = true;
                }
            };

            this.Bw.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                AisUtil.LoadProgressBarImage(this.image_Gage, false);
                this.Bw = null;
                if (args.Cancelled)
                {
                    if (File.Exists(filePathDest))
                    {
                        File.Delete(filePathDest);
                    }
                    return;
                }

                if (args.Error != null)  // if an exception occurred during DoWork,
                {
                    // Do your error handling here
                    AisMessageBox.DisplayErrorMessageBox(args.Error.ToString());
                }

                // Do whatever else you need to do after the work is completed.
                // This happens in the main UI thread.
                this.button_Get.IsEnabled = true;
                if (string.IsNullOrEmpty(filePathDest))
                {
                    FoaMessageBox.ShowError("AIS_E_001");
                    return;
                }

                //ExcelUtil.StartExcelProcess(filePathDest, true);
                ExcelUtil.OpenExcelFile(filePathDest);

                this.excelFilePath = filePathDest;
                this.gotNewResult = false;
            };

            // Set loading progress bar image..
            AisUtil.LoadProgressBarImage(this.image_Gage, true);

            Bw.RunWorkerAsync(); // starts the background worker
        }

        private void writeRetrievedDataToExcel(string folderPath, bool isFirstTime,
            string filePathSrc, string filePathDest, Dictionary<ExcelConfigParam, object> configParams,
            BackgroundWorker bWorker)
        {
            DataTable dt = createDataTable2(this.elementList, this.backDataList);

            if (isFirstTime && DataSource != DataSourceType.GRIP)
            {
            }
            else
            {
                configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, System.IO.Path.GetFileName(filePathSrc));
            }

            var writer = new InteropExcelWriterComment(this.Mode, this.DataSource, this.editControl.Ctms, dt, bWorker, this.SelectedMission);
            writer.WriteCtmData(filePathDest, folderPath, configParams);
        }

        private DataTable createDataTable(ObservableCollection<ElementListBoxItem> elementList, ObservableCollection<BackDataListBoxItem> backDataList)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(Keywords.CTM_NAME);
            dt.Columns.Add("コメント");
            foreach (BackDataListBoxItem bd in backDataList)
            {
                if (dt.Columns.Contains(bd.ElementName))
                {
                    continue;
                }
                dt.Columns.Add(bd.ElementName);
            }

            foreach (ElementListBoxItem el in elementList)
            {
                DataRow row = dt.NewRow();
                row[Keywords.CTM_NAME] = el.CtmName;
                row["コメント"] = el.ElementName;
                foreach (BackDataListBoxItem bd in backDataList)
                {
                    if (bd.CtmId != el.CtmId)
                    {
                        continue;
                    }
                    row[bd.ElementName] = bd.ElementName;
                }

                dt.Rows.Add(row);
            }

            return dt;
        }

        private DataTable createDataTable2(ObservableCollection<ElementListBoxItem> elementList, ObservableCollection<BackDataListBoxItem> backDataList)
        {
            DataTable dt = new DataTable();

            dt.Columns.Add(Keywords.CTM_NAME);
            dt.Columns.Add("コメント");
            var lengthDict = new Dictionary<string, int>();
            foreach (BackDataListBoxItem bd in backDataList)
            {
                if (!lengthDict.ContainsKey(bd.CtmId))
                {
                    lengthDict.Add(bd.CtmId, 0);
                }
                lengthDict[bd.CtmId]++;
            }

            int maxLength = 0;
            foreach (string key in lengthDict.Keys)
            {
                int length = lengthDict[key];
                if (length <= maxLength)
                {
                    continue;
                }
                maxLength = length;
            }

            for (int i = 0; i < maxLength; i++)
            {
                string col = string.Format("背景データ_{0}", (i + 1).ToString("00"));
                dt.Columns.Add(col);
            }

            foreach (ElementListBoxItem el in elementList)
            {

                List<string> items = new List<string>();
                items.Add(el.CtmName);
                items.Add(el.ElementName);
                foreach (BackDataListBoxItem bd in backDataList)
                {
                    if (bd.CtmId != el.CtmId)
                    {
                        continue;
                    }
                    items.Add(bd.ElementName);
                }

                DataRow row = dt.NewRow();
                row.ItemArray = items.ToArray();
                dt.Rows.Add(row);
            }

            return dt;
        }

        #endregion

        //ISSUE_NO.616,708 sunyi 2018/05/31 start
        //ミッションを変更したら設定内容をクリアする。
        //private class ElementListBoxItem
        public class ElementListBoxItem
        //ISSUE_NO.616,708 sunyi 2018/05/31 end
        {
            public string CtmName { get; set; }
            public string CtmId { get; set; }
            public string ElementName { get; set; }
            public string ElementId{ get; set; }
            
            public string DisplayDoc
            {
                get
                {
                    string doc = string.Format("{0} - {1} (コメント)",
                        this.CtmName,
                        this.ElementName);
                    return doc;
                }
            }
        }
        //ISSUE_NO.616,708 sunyi 2018/05/31 start
        //ミッションを変更したら設定内容をクリアする。
        //private class BackDataListBoxItem
        public class BackDataListBoxItem
        //ISSUE_NO.616,708 sunyi 2018/05/31 end
        {
            public string CtmName { get; set; }
            public string CtmId { get; set; }
            public string ElementName { get; set; }
            public string ElementId { get; set; }
        }

        // No.493 Input control of period setting item. ----------------Start
        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }

        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }
        // No.493 Input control of period setting item. ----------------End
    }
}
