﻿using DAC.AExcel;
using DAC.Model.Util;
using DAC.Util;
using DAC.View;
using FoaCore.Common.Util;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;
using System.Threading.Tasks;
using DAC.View.Helpers;

namespace DAC.Model
{
    /// <summary>
    /// UserControl_OnlineTemplate.xaml の相互作用ロジック
    /// </summary>
    public partial class UserControl_OnlineTemplate : BaseControl
    {
        private DateTime startR;
        private DateTime endR;

        /// <summary>
        /// 「Edit」が押されたか否か
        /// </summary>
        private bool gotNewResult = false;

        private bool isFirstFile = true;
        private string excelFilePath = string.Empty;

        private Dictionary<string, string> conditionDictionary = new Dictionary<string, string>();
        private Dictionary<string, string> paramDictionary = new Dictionary<string, string>();


        private DataGridTextColumn newColumn = new DataGridTextColumn();

        public ElementSelectionManager Esm;

        public Dictionary<string, int> ElementColumnCount;

        private bool TableIsEmpty = true;
        private DataTable Dt;

        public UserControl_OnlineTemplate(bool isFirstFile = true, string filePath = "")
        {
            InitializeComponent();

            this.Mode = ControlMode.Online;
            this.TEMPLATE = "オンラインテンプレート.xlsm";

            this.isFirstFile = isFirstFile;
            this.excelFilePath = filePath;

            // ComboBoxとDateTimePickerを紐付ける
            this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
            this.comboBox_End_01.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End_01 };
            this.comboBox_End_01.toEnd = true;
            this.comboBox_End_02.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End_02 };
            this.comboBox_End_02.toEnd = true;
            this.comboBox_End_03.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End_03 };
            this.comboBox_End_03.toEnd = true;
            this.comboBox_End_04.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End_04 };
            this.comboBox_End_04.toEnd = true;

            // ComboBoxのアイテムソースを設定
            this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISOnlineStart()).GetList();
            this.comboBox_End_01.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISOnlineEnd()).GetList();
            this.comboBox_End_02.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISOnlineEnd()).GetList();
            this.comboBox_End_03.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISOnlineEnd()).GetList();
            this.comboBox_End_04.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISOnlineEnd()).GetList();

            if (0 < this.comboBox_Start.Items.Count)
            {
                this.comboBox_Start.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End_01.Items.Count)
            {
                this.comboBox_End_01.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End_02.Items.Count)
            {
                this.comboBox_End_02.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End_03.Items.Count)
            {
                this.comboBox_End_03.SelectedIndex = 0;
            }
            if (0 < this.comboBox_End_04.Items.Count)
            {
                this.comboBox_End_04.SelectedIndex = 0;
            }

            this.dateTimePicker_DisplayStart_03.Value = DateTime.Today;
            this.dateTimePicker_DisplayStart_04.Value = DateTime.Today;
            this.dateTimePicker_DisplayEnd_04.Value = DateTime.Today;

            setKeyValuePair();
            setComboBoxItem();

            if (!Directory.Exists(resultDir))
            {
                Directory.CreateDirectory(resultDir);
            }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            Init();
            if (string.IsNullOrEmpty(this.excelFilePath))
            {
                this.image_OpenExcel.Deactivate();
            }
            else
            {
                this.image_OpenExcel.Activate();
            }

            //言語が英語の場合の表示調整
            string cultureName = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;
            int heightvalue = 36;
            int topvalue = 7;
            if (cultureName == "en-US")
            {
                this.canvas_Interval_01.Height = heightvalue;
                this.canvas_Interval_02.Height = heightvalue;
                this.canvas_Interval_03.Height = heightvalue;
                this.canvas_Interval_04.Height = heightvalue;
                this.canvas_DisplayPeriod_02.Height = heightvalue;
                this.canvas_DisplayPeriod_03.Height = heightvalue;

                Canvas.SetTop(this.canvas_Interval_04, 116);
                Canvas.SetTop(this.comboBox_Interval_01, topvalue);
                Canvas.SetTop(this.comboBox_Interval_02, topvalue);
                Canvas.SetTop(this.comboBox_Interval_03, topvalue);
                Canvas.SetTop(this.comboBox_Interval_04, topvalue);
                Canvas.SetTop(this.comboBox_DisplayPeriod_02, topvalue);
                Canvas.SetTop(this.comboBox_DisplayPeriod_03, topvalue);
            }

            BitmapImage biCancel = new BitmapImage();
            biCancel.BeginInit();
            biCancel.UriSource = new Uri(@"/DAC;component/Resources/Image/cancel-to-get.png", UriKind.RelativeOrAbsolute);
            biCancel.EndInit();
            this.image_CancelToGet.Source = biCancel;

            this.image_CancelToGet.IsEnabled = false;

            BitmapImage biGage = new BitmapImage();
            biGage.BeginInit();
            biGage.UriSource = new Uri(@"/DAC;component/Resources/Image/progressBar-empty.png", UriKind.RelativeOrAbsolute);
            biGage.EndInit();
            this.image_Gage.Source = biGage;

            //閉じるボタンを追加
            BitmapImage biClose = new BitmapImage();
            biClose.BeginInit();
            biClose.UriSource = new Uri(@"/DAC;component/Resources/Image/close-red.png", UriKind.RelativeOrAbsolute);
            biClose.EndInit();
            this.image_Close.Source = biClose;

            this.editControl.Online = this;

            MainWindow m = Application.Current.MainWindow as MainWindow;
            //CTM選択ボタン」使用可
            m.CtmDtailsCtrl.button_Selection.IsEnabled = true;

            if (this.isFirstFile)
            {
                //this.dataGrid_Output.SetInitialDataGrid();
                this.edit_Grid.Visibility = Visibility.Hidden;
            }
            else
            {
                try
                {
                    readExcelFile(this.excelFilePath);
                    this.edit_Grid.Visibility = Visibility.Visible;
                }
                catch (Exception ex)
                {
                    string message = string.Format(Properties.Message.AIS_E_038, ex.Message);

#if DEBUG
                    message += string.Format("\n" + ex.StackTrace);
#endif
                    AisMessageBox.DisplayErrorMessageBox(message);
                }
            }
        }

        #region 既存ファイルの読込み

        private void readExcelFile(string filePath)
        {
            SpreadsheetGear.IWorkbook book = null;

            try
            {
                // ワークブック
                book = SpreadsheetGear.Factory.GetWorkbook(filePath);

                // シート
                string conditionSheetName = "オンラインテンプレート";
                SpreadsheetGear.IWorksheet worksheetCondition = AisUtil.GetSheetFromSheetName_SSG(book, conditionSheetName);
                var conditionDic = readCondition(worksheetCondition);

                string paramSheetName = Keywords.PARAM_SHEET;
                SpreadsheetGear.IWorksheet worksheetParam = AisUtil.GetSheetFromSheetName_SSG(book, paramSheetName);
                var paramMap = readParam(worksheetParam);

                int firstRowIndex = 6;
                int firstColumnIndex = 0;
                int rowLength = AisUtil.GetRowLength_SSG(worksheetCondition, firstRowIndex, firstColumnIndex);
                int columnLength = AisUtil.GetColumnLength_SSG(worksheetCondition, firstRowIndex, firstColumnIndex);
                DataTable dtElementWithId = AisUtil.CreateDataTableFromExcel_SSG(worksheetCondition, firstRowIndex, firstColumnIndex, rowLength, columnLength);
                DataTable dtElement = AisUtil.DeleteIdRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                DataTable dtId = AisUtil.DeleteNameRow(AisUtil.DeepCopyDataTable(dtElementWithId));
                var elementColumnDic = AisUtil.CountElementColumn(dtElement);

                this.dtId = dtId;
                this.conditionDictionary = conditionDic;
                this.paramDictionary = paramMap;

                if (paramMap.ContainsKey("収集タイプ") &&
                    !string.IsNullOrEmpty(paramMap["収集タイプ"]))
                {
                    switch (paramMap["収集タイプ"])
                    {
                        case "CTM":
                            this.DataSource = DataSourceType.CTM_DIRECT;
                            break;
                        case "ミッション":
                            this.DataSource = DataSourceType.MISSION;
                            break;
                        case "GRIP":
                            this.DataSource = DataSourceType.GRIP;
                            break;
                        default:
                            break;
                    }
                }

                // set SelectedMission
                if (paramMap.ContainsKey("ミッションID") &&
                    !string.IsNullOrEmpty(paramMap["ミッションID"]))
                {
                    if (this.DataSource == DataSourceType.MISSION)
                    {
                        string missionId = conditionDic["ミッションID"];
                        Mission mission = AisUtil.GetMissionFromId(missionId);
                        this.SelectedMission = mission;
                    }
                    else if (this.DataSource == DataSourceType.GRIP)
                    {
                        string missionId = conditionDic["ミッションID"];
                        GripMissionCtm mission = AisUtil.GetGripMissionFromId(missionId);
                        this.SelectedGripMission = mission;
                    }
                }

                this.dataGrid_Output.Dt = dtElement;
                this.dataGrid_Output.ElementColumnCount = elementColumnDic;
                this.dataGrid_Output.Esm = new ElementSelectionManager(this.dataGrid_Output.Dt, this.dataGrid_Output.ElementColumnCount);
                this.dataGrid_Output.SetDataTable(dtElement);
                this.dtTemp = dtElement;

                DateTime dtStart = new DateTime();
                DateTime dtEnd = new DateTime();
                DateTime dtDisplayEnd = new DateTime();
                int interval = 0;
                int displayPeriod = 0;
                switch (conditionDic["オンライン種別"])
                {
                    case "期間固定":
                        if (DateTime.TryParse(conditionDic["収集開始日時"], out dtStart))
                        {
                            this.dateTimePicker_Start.Value = dtStart;
                        }
                        if (DateTime.TryParse(conditionDic["収集終了日時"], out dtEnd))
                        {
                            this.dateTimePicker_End_01.Value = dtEnd;
                        }
                        if (int.TryParse(conditionDic["更新周期"], out interval))
                        {
                            this.comboBox_Interval_01.Text = interval.ToString();
                        }
                        this.radioButton_FixingInterval.IsChecked = true;
                        break;
                    case "連続移動":
                        if (DateTime.TryParse(conditionDic["収集終了日時"], out dtEnd))
                        {
                            this.dateTimePicker_End_02.Value = dtEnd;
                        }
                        if (int.TryParse(conditionDic["更新周期"], out interval))
                        {
                            this.comboBox_Interval_02.Text = interval.ToString();
                        }
                        if (int.TryParse(conditionDic["表示期間"], out displayPeriod))
                        {
                            this.comboBox_DisplayPeriod_02.Text = displayPeriod.ToString();
                        }
                        this.radioButton_MovingContinuity.IsChecked = true;
                        break;
                    case "繰り返し移動":
                        if (DateTime.TryParse(paramMap["表示開始時刻"], out dtStart))
                        {
                            this.dateTimePicker_DisplayStart_03.Value = dtStart;
                        }
                        if (DateTime.TryParse(conditionDic["収集終了日時"], out dtEnd))
                        {
                            this.dateTimePicker_End_03.Value = dtEnd;
                        }
                        if (int.TryParse(conditionDic["更新周期"], out interval))
                        {
                            this.comboBox_Interval_03.Text = interval.ToString();
                        }
                        if (int.TryParse(conditionDic["表示期間"], out displayPeriod))
                        {
                            this.comboBox_DisplayPeriod_03.Text = displayPeriod.ToString();
                        }
                        this.radioButton_MovingLoop_01.IsChecked = true;
                        break;
                    case "一定時間繰り返し":
                        if (DateTime.TryParse(paramMap["表示開始時刻"], out dtStart))
                        {
                            this.dateTimePicker_DisplayStart_03.Value = dtStart;
                        }
                        if (DateTime.TryParse(conditionDic["収集終了日時"], out dtEnd))
                        {
                            this.dateTimePicker_End_03.Value = dtEnd;
                        }
                        if (int.TryParse(conditionDic["更新周期"], out interval))
                        {
                            this.comboBox_Interval_03.Text = interval.ToString();
                        }
                        if (int.TryParse(conditionDic["表示期間"], out displayPeriod))
                        {
                            this.comboBox_DisplayPeriod_03.Text = displayPeriod.ToString();
                        }
                        this.radioButton_MovingLoop_01.IsChecked = true;
                        break;
                    case "毎日定刻繰り返し":
                        if (DateTime.TryParse(paramMap["表示開始時刻"], out dtStart))
                        {
                            this.dateTimePicker_DisplayStart_04.Value = dtStart;
                        }
                        if (int.TryParse(conditionDic["表示期間"], out displayPeriod))
                        {
                            TimeSpan ts = new TimeSpan(0, 0, displayPeriod);
                            dtDisplayEnd = dtStart + ts;
                            this.dateTimePicker_DisplayEnd_04.Value = dtDisplayEnd;
                        }
                        if (DateTime.TryParse(conditionDic["収集終了日時"], out dtEnd))
                        {
                            this.dateTimePicker_End_04.Value = dtEnd;
                        }
                        if (int.TryParse(conditionDic["更新周期"], out interval))
                        {
                            this.comboBox_Interval_04.Text = interval.ToString();
                        }
                        this.radioButton_MovingLoop_02.IsChecked = true;
                        break;
                    default:
                        break;
                }
            }
            finally
            {
                if (book != null)
                {
                    book.Close();
                }
            }
        }

        private Dictionary<string, string> readCondition(SpreadsheetGear.IWorksheet worksheetCondition)
        {
            var dic = new Dictionary<string, string>();

            // 収集条件を出力
            SpreadsheetGear.IRange cellsCondition = worksheetCondition.Cells;
            for (int i = 0; i < 10; i++)   // とりあえず10×10のセルを検索
            {
                for (int j = 0; j < 10; j++)
                {
                    // null check
                    if (cellsCondition[i, j].Value == null)
                    {
                        continue;
                    }

                    // ミッション
                    if (cellsCondition[i, j].Text == "ミッション")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // ミッションID
                    if (cellsCondition[i, j].Text == "ミッションID")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // 収集開始日時
                    if (cellsCondition[i, j].Text == "収集開始日時")
                    {
                        string date = cellsCondition[i, j + 1].Text;
                        string time = cellsCondition[i, j + 2].Text;
                        string datetime = date + " " + time;
                        dic.Add(cellsCondition[i, j].Text, datetime);
                        continue;
                    }

                    // 収集終了日時
                    if (cellsCondition[i, j].Text == "収集終了日時")
                    {
                        string date = cellsCondition[i, j + 1].Text;
                        string time = cellsCondition[i, j + 2].Text;
                        string datetime = date + " " + time;
                        dic.Add(cellsCondition[i, j].Text, datetime);
                        continue;
                    }

                    // 更新周期
                    if (cellsCondition[i, j].Text == "更新周期")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // 表示期間
                    if (cellsCondition[i, j].Text == "表示期間")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // 取得期間
                    if (cellsCondition[i, j].Text == "取得期間")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }

                    // オンライン種別
                    if (cellsCondition[i, j].Text == "オンライン種別")
                    {
                        dic.Add(cellsCondition[i, j].Text, cellsCondition[i, j + 1].Text);
                        continue;
                    }
                }
            }


            return dic;
        }

        private Dictionary<string, string> readParam(SpreadsheetGear.IWorksheet worksheetParam)
        {
            var map = new Dictionary<string, string>();

            SpreadsheetGear.IRange paramCells = worksheetParam.Cells;
            for (int i = 0; i < 100; i++)   // とりあえず100行ほど検索
            {
                string paramName = paramCells[i, 0].Text;
                string paramValue = paramCells[i, 1].Text;

                // null check
                if (paramName == null)
                {
                    continue;
                }

                // サーバIPアドレス
                if (paramName == "サーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // サーバポート番号
                if (paramName == "サーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                if (paramName == "表示開始時刻")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                if (paramName == "表示終了時刻")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // 収集タイプ
                if (paramName == "収集タイプ")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバIPアドレス
                if (paramName == "GRIPサーバIPアドレス")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }

                // GRIPサーバポート番号
                if (paramName == "GRIPサーバポート番号")
                {
                    map.Add(paramName, paramValue);
                    continue;
                }
            }

            return map;
        }

        #endregion

        #region CTM編集Gridの設定

        public void setMission()
        {
            if (!this.conditionDictionary.ContainsKey("ミッションID"))
            {
                return;
            }

            TreeViewItem item = null;
            string missionId = this.conditionDictionary["ミッションID"];
            if (string.IsNullOrEmpty(missionId))
            {
                return;
            }

            this.dataGrid_Output.SetColumn(this.dtTemp);
            this.dataGrid_Output.SetDataTable(this.dtTemp);
            this.dataGrid_Output.AddNewColumn();
            this.dataGrid_Output.Esm.TableIsEmpty = false;

            this.IsSelectedProgramMission = true;

            // Mission Tree
            item = this.mainWindow.treeView_Mission_CTM.GetTreeViewItem(missionId);
            if (item == null)
            {
                if (this.SelectedMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid(this.SelectedMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_CTM.Focus();
            item.Focus();
        }

        public void SetGripMission()
        {
            if (!this.conditionDictionary.ContainsKey("ミッションID"))
            {
                return;
            }

            TreeViewItem item = null;
            string missionId = this.conditionDictionary["ミッションID"];
            if (string.IsNullOrEmpty(missionId))
            {
                return;
            }

            // Mission Tree
            item = this.mainWindow.treeView_Mission_Grip.GetTreeViewItem(missionId);
            if (item == null)
            {
                if (this.SelectedGripMission != null)
                {
                    this.mainWindow.CtmDtailsCtrl.SetElementDataToDataGrid_Grip(this.SelectedGripMission.Id);
                }
                return;
            }
            var parentNode = item.Parent as TreeViewItem;
            parentNode.IsExpanded = true;
            item.IsSelected = true;
            this.mainWindow.treeView_Mission_Grip.Focus();
            item.Focus();

            this.dataGrid_Output.SetColumn(this.dtTemp);
            this.dataGrid_Output.SetDataTable(this.dtTemp);
            this.dataGrid_Output.AddNewColumn();
            this.dataGrid_Output.Esm.TableIsEmpty = false;
        }

        public override void SetCtm2()
        {
            setCtm();
        }

        private void setCtm()
        {
            TreeViewItem item = null;
            if (this.dtId == null)
            {
                return;
            }

            this.mainWindow.TabCtrlDS.SelectedIndex = 0;
            foreach (var rowObject in this.dtId.Rows)
            {
                var row = rowObject as DataRow;
                for (int i = 0; i < row.ItemArray.Length; i++)
                {
                    item = this.mainWindow.treeView_Catalog.GetItem(row.ItemArray[0].ToString());
                    CheckBox cb = this.mainWindow.treeView_Catalog.GetCheckBox(item);
                    cb.IsChecked = true;
                }
            }

            if (item == null)
            {
                return;
            }
            item.IsSelected = true;
            List<string> list = this.mainWindow.treeView_Catalog.GetCheckedId(true);

            this.dataGrid_Output.SetColumn(this.dtTemp);
            this.dataGrid_Output.SetDataTable(this.dtTemp);
            this.dataGrid_Output.AddNewColumn();
            this.dataGrid_Output.Esm.TableIsEmpty = false;
        }

        #endregion

        /// <summary>
        /// 更新周期
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        List<KeyValuePair<string, TimeSpan>> intervalItems = new List<KeyValuePair<string, TimeSpan>>();

        /// <summary>
        /// 表示期間
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        List<KeyValuePair<string, TimeSpan>> displayPeriodItems = new List<KeyValuePair<string, TimeSpan>>();

        /// <summary>
        /// 取得期間
        /// Key: ComboBoxの値
        /// Value: エクセルの出力に使用する値
        /// </summary>
        List<KeyValuePair<string, TimeSpan>> getPeriodItems = new List<KeyValuePair<string, TimeSpan>>();

        private void setKeyValuePair()
        {
            // 更新周期
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("10", new TimeSpan(0, 0, 10)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("30", new TimeSpan(0, 0, 30)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("60", new TimeSpan(0, 1, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("120", new TimeSpan(0, 2, 0)));
            this.intervalItems.Add(new KeyValuePair<string, TimeSpan>("180", new TimeSpan(0, 3, 0)));

            // 表示期間
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("1", new TimeSpan(1, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("2", new TimeSpan(2, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("3", new TimeSpan(3, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("6", new TimeSpan(6, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("12", new TimeSpan(12, 0, 0)));
            this.displayPeriodItems.Add(new KeyValuePair<string, TimeSpan>("24", new TimeSpan(24, 0, 0)));

            // 取得期間
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("1", new TimeSpan(1, 0, 0)));
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("2", new TimeSpan(2, 0, 0)));
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("3", new TimeSpan(3, 0, 0)));
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("6", new TimeSpan(6, 0, 0)));
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("12", new TimeSpan(12, 0, 0)));
            this.getPeriodItems.Add(new KeyValuePair<string, TimeSpan>("24", new TimeSpan(24, 0, 0)));
        }

        private void setComboBoxItem()
        {
            // 更新周期
            foreach (var pair in this.intervalItems)
            {
                this.comboBox_Interval_01.Items.Add(pair.Key);
                this.comboBox_Interval_02.Items.Add(pair.Key);
                this.comboBox_Interval_03.Items.Add(pair.Key);
                this.comboBox_Interval_04.Items.Add(pair.Key);
            }
            this.comboBox_Interval_01.SelectedItem = "60";
            this.comboBox_Interval_02.SelectedItem = "60";
            this.comboBox_Interval_03.SelectedItem = "60";
            this.comboBox_Interval_04.SelectedItem = "60";

            // 表示期間
            foreach (var pair in this.displayPeriodItems)
            {
                this.comboBox_DisplayPeriod_02.Items.Add(pair.Key);
                this.comboBox_DisplayPeriod_03.Items.Add(pair.Key);
            }
            this.comboBox_DisplayPeriod_02.SelectedItem = "1";
            this.comboBox_DisplayPeriod_03.SelectedItem = "1";
        }

        public override void DataSourceSelectionChanged(string removingCtmName = null)
        {
            if (this.editControl.Ctms == null)
            {
                this.dataGrid_Output.SetInitialDataGrid();
                return;
            }

            if (string.IsNullOrEmpty(removingCtmName))
            {
                return;
            }

            DataTable dtClone = this.dataGrid_Output.Dt.Clone();
            foreach (object rowObj in this.dataGrid_Output.Dt.Rows)
            {
                DataRow row = rowObj as DataRow;
                if (row == null)
                {
                    continue;
                }

                string ctmName = row[0].ToString();
                if (ctmName == removingCtmName)
                {
                    continue;
                }

                DataRow rowClone = dtClone.NewRow();
                rowClone.ItemArray = row.ItemArray;
                dtClone.Rows.Add(rowClone);
            }

            if (dtClone.Rows.Count < 1)
            {
                this.dataGrid_Output.SetInitialDataGrid();
            }
            else
            {
                this.dataGrid_Output.SetDataTable(dtClone);
            }
        }

        #region Event Handler

        /// <summary>
        /// 「GET」
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void button_Get_Click(object sender, RoutedEventArgs e)
        {
            if (this.mainWindow.CtmDtailsCtrl.GetCtmDtailsEmpty() == string.Empty)
            {
                FoaMessageBox.ShowError("AIS_E_009");
                return;
            }

            if (this.edit_Grid.Visibility == Visibility.Visible)
            {
                if (this.dataGrid_Output.Dt.Rows.Count < 1 ||
                    this.dataGrid_Output.Dt.Rows[0].ToString() == string.Empty)
                {
                    FoaMessageBox.ShowError("AIS_E_007");
                    return;
                }
            }

            //if (this.dataGrid_Output.Dt.Rows.Count < 1 ||
            //    this.dataGrid_Output.Dt.Rows[0].ToString() == string.Empty)
            //{
            //    FoaMessageBox.ShowError("AIS_E_007");
            //    return;
            //}

            DateTime? start = null;
            DateTime? end = null;

            //AISTEMP-111,112 sunyi 2018/12/25 start
            if (this.dateTimePicker_DisplayStart_03.Value == null || this.dateTimePicker_DisplayStart_04.Value == null || this.dateTimePicker_DisplayEnd_04.Value == null)
            {
                //表示期間を正しく入力してください。
                FoaMessageBox.ShowError("AIS_E_020");
                return;
            }
            if (this.dateTimePicker_End_03.Value == null || this.dateTimePicker_End_04.Value == null)
            {
                //収集期間を設定してください。
                FoaMessageBox.ShowError("AIS_E_021");
                return;
            }
            //AISTEMP-111,112 sunyi 2018/12/25 end

            if ((bool)this.radioButton_FixingInterval.IsChecked)
            {
                start = this.dateTimePicker_Start.Value;
                end = this.dateTimePicker_End_01.Value;
            }
            else if ((bool)this.radioButton_MovingContinuity.IsChecked)
            {
                int displayPeriod = 0;
                if (!int.TryParse(this.comboBox_DisplayPeriod_02.Text, out displayPeriod))
                {
                    FoaMessageBox.ShowError("AIS_E_020");
                    return;
                }
                start = DateTime.Now - new TimeSpan(displayPeriod, 0, 0);
                end = this.dateTimePicker_End_02.Value;
            }
            else if ((bool)this.radioButton_MovingLoop_01.IsChecked)
            {
                int displayPeriod = 0;
                if (!int.TryParse(this.comboBox_DisplayPeriod_03.Text, out displayPeriod))
                {
                    FoaMessageBox.ShowError("AIS_E_020");
                    return;
                }

                DateTime now = DateTime.Now;
                DateTime inputValue = (DateTime)this.dateTimePicker_DisplayStart_03.Value;
                DateTime currentBlockStart = new DateTime(now.Year, now.Month, now.Day, inputValue.Hour, inputValue.Minute, inputValue.Second);
                currentBlockStart = inputValue; // ?
                while (currentBlockStart.AddHours(displayPeriod) < now)
                {
                    currentBlockStart = currentBlockStart.AddHours(displayPeriod);
                }

                start = currentBlockStart;
                end = start + new TimeSpan(displayPeriod, 0, 0);
            }
            else if ((bool)this.radioButton_MovingLoop_02.IsChecked)
            {
                DateTime now = DateTime.Now;
                DateTime inputStart = (DateTime)this.dateTimePicker_DisplayStart_04.Value;
                start = new DateTime(now.Year, now.Month, now.Day, inputStart.Hour, inputStart.Minute, inputStart.Second);
                DateTime inputEnd = (DateTime)this.dateTimePicker_DisplayEnd_04.Value;
                end = new DateTime(now.Year, now.Month, now.Day, inputEnd.Hour, inputEnd.Minute, inputEnd.Second);
            }

            if (start == null || end == null)
            {
                FoaMessageBox.ShowError("AIS_E_021");
                return;
            }

            // No.520,521 Check end time and current time. 終了日時が現在日時よりも前の場合はエラー
            if ((bool)this.radioButton_MovingLoop_01.IsChecked)      //一定時間繰り返し
            {
                if ((DateTime)this.dateTimePicker_End_03.Value <= DateTime.Now)
                {
                    FoaMessageBox.ShowError("AIS_E_024");
                    return;
                }
            }
            else if ((bool)this.radioButton_MovingLoop_02.IsChecked) //毎日定刻繰り返し
            {
                if ((DateTime)this.dateTimePicker_End_04.Value <= DateTime.Now)
                {
                    FoaMessageBox.ShowError("AIS_E_024");
                    return;
                }
            }
            else   //期間固定型、連続移動型
            {
                if (end <= DateTime.Now)
                {
                    FoaMessageBox.ShowError("AIS_E_024");
                    return;
                }
            }

            if (end <= start)
            {
                FoaMessageBox.ShowError("AIS_E_019");
                return;
            }

            DateTime start1 = start ?? DateTime.Now;
            DateTime end1 = end ?? DateTime.Now;

            preRetrieveCtmData();
            this.DownloadCts = new CancellationTokenSource();
            var resultFolder = await RetrieveCtmData(start1, end1, this.DownloadCts);

            if ((bool)this.radioButton_MovingLoop_01.IsChecked)
            {
                start1 = (DateTime)this.dateTimePicker_DisplayStart_03.Value;
                end1 = (DateTime)this.dateTimePicker_End_03.Value;
            }
            if ((bool)this.radioButton_MovingLoop_02.IsChecked)
            {
                end1 = (DateTime)this.dateTimePicker_End_04.Value;
            }
            postRetrieveCtmData(resultFolder != null, start1, end1);
            if (resultFolder == null)
            {
                image_OpenExcel.Deactivate();
                return;
            }

            this.tmpResultFolderPath = resultFolder;
            this.gotNewResult = true;
        }

        private void image_CancelToGet_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.Bw != null)
            {
                this.Bw.CancelAsync();
            }

            DownloadCts.Cancel();
            DownloadCts = new CancellationTokenSource();
            CancelGripData();
            AisUtil.LoadProgressBarImage(this.image_Gage, false);

            this.button_Get.IsEnabled = true;
            this.image_CancelToGet.IsEnabled = false;
        }

        private void image_OpenExcel_Tap(object sender, RoutedEventArgs e)
        {
            //AIS-TEN-126
            (App.Current.MainWindow as MainWindow).LoadinVisibleChange(false);
            WriteAndOpenExcel();
            //(App.Current.MainWindow as MainWindow).LoadinVisibleChange(true);
        }

        #endregion

        #region Excelファイルの作成、オープン

        private void WriteAndOpenExcel()
        {
            if (!this.gotNewResult)
            {
                //ExcelUtil.StartExcelProcess(this.excelFilePath, true);
                ExcelUtil.OpenExcelFile(this.excelFilePath);
                (App.Current.MainWindow as MainWindow).LoadinVisibleChange(true);
                return;
            }

            string onlineKind = string.Empty;
            int reloadInterval = 0;
            int displayPeriod = 0;
            int getPeriod = 0;
            DateTime displayStart = new DateTime();

            if ((bool)this.radioButton_FixingInterval.IsChecked)
            {
                if (!int.TryParse(this.comboBox_Interval_01.Text, out reloadInterval))
                {
                    FoaMessageBox.ShowError("AIS_E_022");
                    return;
                }
                onlineKind = "期間固定";
                this.isSelectedMovingLoop = false;
            }
            else if ((bool)this.radioButton_MovingContinuity.IsChecked)
            {
                if (!int.TryParse(this.comboBox_Interval_02.Text, out reloadInterval))
                {
                    FoaMessageBox.ShowError("AIS_E_022");
                    return;
                }
                if (!int.TryParse(this.comboBox_DisplayPeriod_02.Text, out displayPeriod))
                {
                    FoaMessageBox.ShowError("AIS_E_020");
                    return;
                }
                onlineKind = "連続移動";
                this.isSelectedMovingLoop = false;
            }
            else if ((bool)this.radioButton_MovingLoop_01.IsChecked)
            {
                if (!int.TryParse(this.comboBox_Interval_03.Text, out reloadInterval))
                {
                    FoaMessageBox.ShowError("AIS_E_022");
                    return;
                }
                if (!int.TryParse(this.comboBox_DisplayPeriod_03.Text, out displayPeriod))
                {
                    FoaMessageBox.ShowError("AIS_E_020");
                    return;
                }
                onlineKind = "一定時間繰り返し";
                this.isSelectedMovingLoop = true;
                displayStart = (DateTime)this.dateTimePicker_DisplayStart_03.Value;
            }
            else if ((bool)this.radioButton_MovingLoop_02.IsChecked)
            {
                if (!int.TryParse(this.comboBox_Interval_04.Text, out reloadInterval))
                {
                    FoaMessageBox.ShowError("AIS_E_022");
                    return;
                }
                onlineKind = "毎日定刻繰り返し";
                this.isSelectedMovingLoop = true;
                displayStart = (DateTime)this.dateTimePicker_DisplayStart_04.Value;
                TimeSpan ts = (DateTime)this.dateTimePicker_DisplayEnd_04.Value - (DateTime)this.dateTimePicker_DisplayStart_04.Value;
                displayPeriod = (int)ts.TotalSeconds;
            }

            // Set loading progress bar image..
            //AisUtil.LoadProgressBarImage(this.image_Gage, true);

            this.image_CancelToGet.IsEnabled = true;
            OpenExcelFile(DataSource, this.startR, this.endR, onlineKind, reloadInterval, getPeriod, displayPeriod, displayStart);
        }


        /// <summary>
        /// ドロップされたエレメントを追加
        /// </summary>
        /// <param name="ctmName">CTM名</param>
        /// <param name="headerIndex">追加する場所の列インデックス</param>
        /// <param name="dropedElements">ドロップされたエレメント</param>
        /// <param name="dt">DataTable</param>
        public void addElementToDataGrid(string ctmName, int headerIndex, List<string[]> dropedElements, DataTable dt)
        {
            int loopCount = -1;
            foreach (string[] elementData in dropedElements)
            {
                loopCount++;

                if (1 < dropedElements.Count &&
                    0 < dt.Rows.Count)
                {
                    // CTMがドロップされた
                    bool hitCtm = false;
                    DataRow row = null;
                    foreach (var rowObject in dt.Rows)
                    {
                        row = rowObject as DataRow;
                        if (row[Keywords.CTM_NAME].ToString() != ctmName)
                        {
                            continue;
                        }

                        hitCtm = true;
                        break;
                    }

                    if (hitCtm)
                    {
                        // 追加中CTM
                        int n = 0;
                        for (int i = 1; i < row.ItemArray.Length; i++)
                        {
                            string headerContent = dt.Columns[i].ColumnName;
                            if (!headerContent.StartsWith(elementData[0]) ||
                                headerContent.Length - 3 != elementData[0].Length)
                            {
                                continue;
                            }

                            n = i;
                            break;
                        }
                        if (n == 0)
                        {
                            n = row.ItemArray.Length;
                            if (this.ElementColumnCount.ContainsKey(elementData[0]))
                            {
                                this.ElementColumnCount[elementData[0]]++;
                            }
                            else
                            {
                                this.ElementColumnCount.Add(elementData[0], 1);
                            } 

                            int cnt = this.ElementColumnCount[elementData[0]];
                            
                            string content = string.Format("{0} {1:D2}", elementData[0], cnt);
                            dt = addColumn(dt, content);
                        }

                        row = null;
                        foreach (var rowObject in dt.Rows)
                        {
                            row = rowObject as DataRow;
                            if (row[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            // hit
                            break;
                        }

                        row[dt.Columns[n].ColumnName] = elementData[0];
                    }
                    else
                    {
                        // 新規CTM
                        int n = 0;
                        for (int i = 1; i < row.ItemArray.Length; i++)
                        {
                            string headerContent = dt.Columns[i].ColumnName;
                            if (!headerContent.StartsWith(elementData[0]) ||
                                headerContent.Length - 3 != elementData[0].Length)
                            {
                                continue;
                            }

                            n = i;
                            break;
                        }
                        if (n == 0)
                        {
                            n = row.ItemArray.Length;
                            if (this.ElementColumnCount.ContainsKey(elementData[0]))
                            {
                                this.ElementColumnCount[elementData[0]]++;
                            }
                            else
                            {
                                this.ElementColumnCount.Add(elementData[0], 1);
                            }

                            int cnt = this.ElementColumnCount[elementData[0]];
                            string content = string.Format("{0} {1:D2}", elementData[0], cnt);
                            dt = addColumn(dt, content);
                        }

                        // CTMが追加されていない→新規CTM
                        DataRow newRow = dt.NewRow();
                        newRow[Keywords.CTM_NAME] = ctmName;
                        newRow[dt.Columns[n].ColumnName] = elementData[0];
                        dt.Rows.Add(newRow);
                    }
                }
                else
                {
                    if (TableIsEmpty)
                    {
                        // 最初のエレメント
                        dt.Columns.Add(new DataColumn(Keywords.CTM_NAME));
                        dt.Columns.Add(new DataColumn(elementData[0] + " 01"));
                        DataRow row = dt.NewRow();
                        row[Keywords.CTM_NAME] = ctmName;
                        row[elementData[0] + " 01"] = elementData[0];
                        dt.Rows.Add(row);

                        if (this.ElementColumnCount.ContainsKey(elementData[0]))
                        {
                            this.ElementColumnCount[elementData[0]]++;
                        }
                        else
                        {
                            this.ElementColumnCount.Add(elementData[0], 1);
                        }

                        //this.Columns.Clear();
                        this.DataContext = dt.DefaultView;
                        //SetColumn(dt);
                        this.Dt = dt;

                        //this.Columns.Add(this.newColumn);
                        TableIsEmpty = false;
                        continue;
                    }

                    string elementName = string.Empty;
                    foreach (var rowObject in dt.Rows)
                    {
                        DataRow row = rowObject as DataRow;
                        if (row.ItemArray.Length - 1 < headerIndex + loopCount ||
                            row[headerIndex + loopCount].ToString() != elementData[0])
                        {
                            continue;
                        }

                        elementName = elementData[0];
                        break;
                    }

                    if (elementName.StartsWith(elementData[0]) &&
                        elementName.Length - 3 != elementData[0].Length)
                    {
                        // すでにあるカラム(同じエレメント)
                        bool hitCtm = false;
                        bool hitElement = false;
                        DataRow row = null;
                        foreach (var rowObject in dt.Rows)
                        {
                            row = rowObject as DataRow;
                            if (row[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            hitCtm = true;
                            if (row[dt.Columns[headerIndex + loopCount].ColumnName].ToString() == string.Empty)
                            {
                                break;
                            }

                            hitElement = true;
                            continue;
                        }
                        if (row != null &&
                            hitElement)
                        {
                            // 同じCTM、同じエレメントならば何もしない
                            continue;
                        }

                        if (row != null &&
                            hitCtm)
                        {
                            // CTMが既に追加されている
                            row[dt.Columns[headerIndex + loopCount].ColumnName] = elementData[0];
                        }
                        else
                        {
                            // CTMが追加されていない→新規CTM
                            DataRow newRow = dt.NewRow();
                            newRow[Keywords.CTM_NAME] = ctmName;
                            newRow[dt.Columns[headerIndex + loopCount].ColumnName] = elementData[0];
                            dt.Rows.Add(newRow);
                        }
                    }
                    else 
                    {
                        // 新規カラム
                        bool isAlreadyAddedElement = false;
                        foreach (var rowObject in dt.Rows)
                        {
                            var r = rowObject as DataRow;
                            if (r[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            foreach (var cell in r.ItemArray)
                            {
                                if (cell.ToString() != elementData[0])
                                {
                                    continue;
                                }

                                // すでに同じエレメントが同じCTMに追加されている
                                isAlreadyAddedElement = true;
                                break;
                            }

                            break;
                        }
                        if (isAlreadyAddedElement)
                        {
                            // すでに同じエレメントが同じCTMに追加されている
                            continue;
                        }

                        if (this.ElementColumnCount.ContainsKey(elementData[0]))
                        {
                            this.ElementColumnCount[elementData[0]]++;
                        }
                        else
                        {
                            this.ElementColumnCount.Add(elementData[0], 1);
                        }
                        int cnt = this.ElementColumnCount[elementData[0]];
                        string content = string.Format("{0} {1:D2}", elementData[0], cnt);
                        //dt = insertColumn(dt, content, dt.Columns.Count - 1);     //元なし
                        dt = addColumn(dt, content);

                        bool hit = false;
                        DataRow row = null;
                        foreach (var rowObject in dt.Rows)
                        {
                            row = rowObject as DataRow;
                            if (row[Keywords.CTM_NAME].ToString() != ctmName)
                            {
                                continue;
                            }

                            hit = true;
                            break;
                        }

                        if (row != null &
                            hit)
                        {
                            row[elementData[0] + " " + cnt.ToString("00")] = elementData[0];
                        }
                        else
                        {
                            DataRow newRow = dt.NewRow();
                            newRow[Keywords.CTM_NAME] = ctmName;
                            newRow[elementData[0] + " " + cnt.ToString("00")] = elementData[0];
                            dt.Rows.Add(newRow);
                        }
                    }
                }
            }

            this.DataContext = dt.DefaultView;
            this.Dt = dt;

        }

        private void OpenExcelFile(DataSourceType resultType,
            DateTime startTime, DateTime endTime,
            string onlineKind, int reloadInterval, int getPeriod, int displayPeriod, DateTime displayStart)
        {
            var resultFiles = Directory.GetFiles(tmpResultFolderPath, "*.csv");
            if (resultFiles.Length == 0)
            {
                //AisUtil.LoadProgressBarImage(this.image_Gage, false);
                (App.Current.MainWindow as MainWindow).LoadinVisibleChange(true);
                FoaMessageBox.ShowError("AIS_E_006");
                return;
            }

            string[] ctmNames = getCtmNamesFromDt(this.dataGrid_Output.Dt);

            //dataGrid_Outputのデータがない場合
            if (this.dataGrid_Output.Dt.Rows.Count == 0)
            {
                TableIsEmpty = true;
                ElementColumnCount = new Dictionary<string, int>();
                Dt = new DataTable();

                 for (int i =0; i< this.mainWindow.CtmDtailsCtrl.dataGrid_Element.Items.Count ;i++)
                 {
                     var tmprow = this.mainWindow.CtmDtailsCtrl.dataGrid_Element.Items[i] as DataRowView;

                     string content = tmprow[0].ToString();

                     if (content != Properties.Resources.CTM_UNIT)
                     {
                         string arg = this.mainWindow.CtmDtailsCtrl.GetArg(i);

                         string[] itemArray = arg.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                         List<string[]> argsData = convertItemArrayToElementData(itemArray);

                         addElementToDataGrid(content, 0, argsData, Dt);
                     }

                 }

                 ctmNames = getCtmNamesFromDt(Dt);

                this.dataGrid_Output.Dt = Dt;
            }

            string[] ctmIds = getCtmIdsFromNames(ctmNames, this.editControl.Ctms);

            if (this.DataSource != DataSourceType.GRIP)
            {
                if (!existsResultFiles(resultFiles, ctmIds))
                {
                    //AisUtil.LoadProgressBarImage(this.image_Gage, false);
                    (App.Current.MainWindow as MainWindow).LoadinVisibleChange(true);
                    FoaMessageBox.ShowError("AIS_E_006");
                    return;
                }
            }

            var cparams = new Dictionary<ExcelConfigParam, object>();
            cparams.Add(ExcelConfigParam.START, startTime);
            cparams.Add(ExcelConfigParam.END, endTime);
            cparams.Add(ExcelConfigParam.ONLINE_KIND, onlineKind);
            cparams.Add(ExcelConfigParam.RELOAD_INTERVAL, reloadInterval);
            cparams.Add(ExcelConfigParam.GET_PERIOD, getPeriod);
            cparams.Add(ExcelConfigParam.DISPLAY_PERIOD, displayPeriod);

            if (this.DataSource == DataSourceType.MISSION)
            {
                cparams.Add(ExcelConfigParam.MISSION_ID, this.SelectedMission.Id);
                cparams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetCatalogLang(this.SelectedMission));
            }
            else if (this.DataSource == DataSourceType.GRIP)
            {
                cparams.Add(ExcelConfigParam.MISSION_ID, this.SelectedGripMission.Id);
                cparams.Add(ExcelConfigParam.MISSION_NAME, AisUtil.GetGripCatalogLang(this.SelectedGripMission));
            }

            if (this.isSelectedMovingLoop)
            {
                cparams.Add(ExcelConfigParam.DISPLAY_START, displayStart);
            }

            executeOpenExcelTask(resultType, cparams);
        }

        private void executeOpenExcelTask(DataSourceType resultType, Dictionary<ExcelConfigParam, object> cParams)
        {
            string filePathDest = string.Empty;

            this.Bw = new BackgroundWorker();
            this.Bw.WorkerSupportsCancellation = true;

            // define the event handlers
            this.Bw.DoWork += delegate(object s, DoWorkEventArgs args)
            {
                // Will be run on background thread.
                // Do your lengthy stuff here -- this will happen in a separate thread to avoid freezing the UI thread

                // コピー元ファイルの絶対パス
                string filePathSrc = isFirstFile ? System.IO.Path.Combine(AisUtil.TEMPLATE_DIR_PATH, TEMPLATE) : this.excelFilePath;

                filePathDest = this.getExcelFilepath(resultType);
                File.Copy(filePathSrc, filePathDest);

                WriteRetrievedDataToExcel(this.tmpResultFolderPath, this.isFirstFile,
                    filePathSrc, filePathDest, cParams, Bw);

                if (this.Bw.CancellationPending)
                {
                    args.Cancel = true;
                }
            };

            this.Bw.RunWorkerCompleted += delegate(object s, RunWorkerCompletedEventArgs args)
            {
                //AisUtil.LoadProgressBarImage(this.image_Gage, false);
                (App.Current.MainWindow as MainWindow).LoadinVisibleChange(true);
                this.Bw = null;
                if (args.Cancelled)
                {
                    if (File.Exists(filePathDest))
                    {
                        File.Delete(filePathDest);
                    }
                    this.image_OpenExcel.Activate();
                    return;
                }

                if (args.Error != null)  // if an exception occurred during DoWork,
                {
                    // Do your error handling here
                    AisMessageBox.DisplayErrorMessageBox(args.Error.ToString());
                }

                // Do whatever else you need to do after the work is completed.
                // This happens in the main UI thread.
                this.button_Get.IsEnabled = true;
                if (string.IsNullOrEmpty(filePathDest))
                {
                    FoaMessageBox.ShowError("AIS_E_001");
                    return;
                }
                //ExcelUtil.StartExcelProcess(filePathDest, true);
                ExcelUtil.OpenExcelFile(filePathDest);

                this.excelFilePath = filePathDest;
                this.gotNewResult = false;
            };

            // Set loading progress bar image..
            //AisUtil.LoadProgressBarImage(this.image_Gage, true);

            Bw.RunWorkerAsync(); // starts the background worker
        }

        private void WriteRetrievedDataToExcel(string folderPath, bool isFirstTime,
            string filePathSrc, string filePathDest, Dictionary<ExcelConfigParam, object> configParams,
            BackgroundWorker bWorker)
        {
            if (isFirstFile)
            {
                var writer = new SSGExcelWriterOnline(this.Mode, this.DataSource, this.editControl.Ctms, this.dataGrid_Output.Dt, bWorker, this.SelectedMission);
                writer.WriteCtmData(filePathDest, folderPath, configParams);
            }
            else
            {
                configParams.Add(ExcelConfigParam.REGISTERED_FILENAME, System.IO.Path.GetFileName(filePathSrc));

                var writer = new InteropExcelWriterOnline(this.Mode, this.DataSource, this.editControl.Ctms, this.dataGrid_Output.Dt, bWorker, this.SelectedMission);
                writer.WriteCtmData(filePathDest, folderPath, configParams);
            }

        }

        #endregion

        #region CTMデータの取得

        private void preRetrieveCtmData()
        {

            this.button_Get.IsEnabled = false;
            this.image_CancelToGet.IsEnabled = true;
            AisUtil.LoadProgressBarImage(this.image_Gage, true);
        }

        private void postRetrieveCtmData(bool success, DateTime start, DateTime end)
        {
            if (success)
            {
                this.image_OpenExcel.Activate();

                this.startR = start;
                this.endR = end;
            }

            AisUtil.LoadProgressBarImage(this.image_Gage, false);
            this.image_CancelToGet.IsEnabled = false;
            this.button_Get.IsEnabled = true;
        }

        #endregion

        private bool isSelectedMovingLoop = false;

        public override void InitializeDataControl()
        {
            this.dataGrid_Output.SetInitialDataGrid();
        }

        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(UserControl_OnlineTemplate));

        private void image_Close_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.edit_Grid.Visibility = Visibility.Hidden;
            this.dataGrid_Output.SetInitialDataGrid();
        }

        private List<string[]> convertItemArrayToElementData(string[] itemArray)
        {
            List<string[]> data = new List<string[]>();

            for (int i = 2; i < itemArray.Length; i += 3)
            {
                string[] ary = new string[]
                {
                    itemArray[i],
                    itemArray[i + 1],
                    itemArray[i + 2]
                };
                data.Add(ary);
            }

            return data;
        }

        private DataTable addColumn(DataTable dt, string columnContent)
        {
            DataTable dtDest = new DataTable();

            int cnt = 0;
            foreach (var column in dt.Columns)
            {
                dtDest.Columns.Add(dt.Columns[cnt].ToString());
                cnt++;
            }
            dtDest.Columns.Add(columnContent);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dtDest.Rows.Add(dt.Rows[i].ItemArray);
            }

            return dtDest;
        }

        // No.493 Input control of period setting item. ----------------Start
        private void Validation_OnPreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.ImeProcessed // Japanese text encoding
                || e.Key == System.Windows.Input.Key.Back   // BackSpace
                || e.Key == System.Windows.Input.Key.Delete // Delete
                || e.Key == System.Windows.Input.Key.Space  // Space
                || e.Key == System.Windows.Input.Key.X      // CTRL + X -> Cut Function
                || e.Key == System.Windows.Input.Key.V)     // CTRL + V  Paste Function
            {
                e.Handled = true;
            }
        }

        // Accept numeric number only as input
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed(string text)
        {
            Regex regex = new Regex("[^0-9]+");
            return !regex.IsMatch(text);
        }

        // Accept numeric number only as input（更新周期用に.を許可）
        private void NumberValidationTextBox2(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed2(e.Text);
            if (e.Handled == true) return;
        }
        private static bool IsTextAllowed2(string text)
        {
            Regex regex = new Regex("[^0-9.]+");
            return !regex.IsMatch(text);
        }
        // No.493 Input control of period setting item. ----------------End

    }
}
