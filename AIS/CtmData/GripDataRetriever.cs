﻿//#define SEARCH_FROM_WEB

using DAC.Model;
using DAC.Model.Util;
using DAC.View.Helpers;
using FoaCore.Common.Util.GripR2;
using FoaCore.Common.Util.Json;
using FoaCore.Model.GripR2.Search.WebSearch;
using FoaCore.Net;
using log4net;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;



namespace DAC.CtmData
{
    class GripDataRetriever
    {
#if SEARCH_FROM_WEB
        public const string SEARCH_CONDITION_IN = "in";
        public const string SEARCH_CONDITION_SCOPE = "scope";
        public const string SEARCH_CONDITION_AUTO = "auto";

        public const int WAITTIME_1 = 1000;
        public const int WAITTIME_2 = 1000;

        public const int ROWS_PER_PAGE = 100;

        public async Task<string> GetGripDatatCsvAsync(GripMissionCtm mission, string searchId, long start, long end, string searchByInCondition, bool skipSameMainKey, CancellationToken cToken)
        {
            string textData = null;
            string response1 = null;
            string responseWarning = null;
            await Task.Run(() =>
            {
                try
                {
                    string url = string.Format(CmsUrl.Search.Mission.CreateCsv(), mission.Id, searchId, start, end, searchByInCondition, skipSameMainKey); logger.Debug(url);
                    var client = new FoaHttpClient();
                    var response = client.Get(url);
                    textData = response.Result;
                }
                catch (SocketException se)
                {
                    logger.Error("Search failed.", se);
                    response1 = se.Message;
                }
            });
            logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() textData:" + textData);
            if (cToken.IsCancellationRequested)
            {
                return textData;
            }

            string msg = DAC.Properties.Message.AIS_E_046;
            if (response1 != null)
            {
                //string msg = "検索データの取得に失敗しました。 " + response1;
                msg += response1;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (responseWarning != null)
            {
                msg = responseWarning;
                AisMessageBox.DisplayWarningMessageBoxWithCaption("NO DATA", msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "NO DATA", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            if (string.IsNullOrEmpty(textData))
            {
                msg += GripDataResponseType.ErrorNotFound;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorNotFound;
            }

#if false
            SearchResultCsv s = null;
            try
            {
                s = JsonUtil.Deserialize<SearchResultCsv>(textData);
            }
            catch
            {
                return GripDataResponseType.ErrorProcess;
            }
            if (null == s)
            {
                return GripDataResponseType.ErrorProcess;
            }
            if (null == s.blocks)
            {
                return GripDataResponseType.NoFolder;
            }
            bool isNoData = true;
            for (int i = 0; i < s.blocks.Count; i++)
            {
                List<SearchResultBlock> p = s.blocks[i];
                for (int j = 0; j < p.Count; j++)
                {
                    SearchResultBlock b = p[j];
                    if (b.blockRows > 0)
                    {
                        isNoData = false;
                        break;
                    }
                }
                if (!isNoData) break;
            }
            if (isNoData)
            {
                return GripDataResponseType.NoFolder;
            }

            // 一時ダウンロード用のディレクトリ作成
            string pathString = DlDir + "\\" + searchId;
            if (!Directory.Exists(DlDir)) Directory.CreateDirectory(DlDir);
            if (!Directory.Exists(DlDir + "\\" + searchId)) Directory.CreateDirectory(DlDir + "\\" + searchId);
            string nowDateTime = DateTime.Now.ToString(FileDateFormat2);

            await Task.Run(() =>
            {
                int resultNo = 0;
                for (int i = 0; i < s.blocks.Count; i++)
                {
                    List<SearchResultBlock> p = s.blocks[i];
                    for (int j = 0; j < p.Count; j++)
                    {
                        SearchResultBlock b = p[j];
                        string url2 = string.Format(CmsUrl.Search.Download(), b.searchId, b.endId, b.pathId);
                        string[] titleFields = GetTitleFields(b.data);
                        string ctmName = titleFields[b.rtCols[b.rtCols.Count - 1]];

                        //WebClient client2 = new WebClient();
                        WebClient client2 = AisUtil.getProxyWebClient(); 
 
                        byte[] fileData = client2.DownloadData(new System.Uri(url2));
                        string resultCtmName = TrimmingString(this.InvalidChars.Aggregate(ctmName, (str, c) => str.Replace(c.ToString(), string.Empty)));
                        //string wkbName = string.Format("[Result]{0}({1}{2})_{3}", resultCtmName, "route", b.pathId, nowDateTime);
                        string wkbName = string.Format("{0}_{1}_{2}{3}", resultNo, resultCtmName, "Route-", b.pathId);
                        string wkbPath = string.Format("{0}\\{1}\\{2}.csv", DlDir, searchId, wkbName);
                        using (FileStream fileStream = new FileStream(wkbPath, FileMode.Create))
                        {
                            fileStream.Write(fileData, 0, fileData.Length);
                            fileStream.Flush();
                            fileStream.Close();
                        }
                        string wkbHPath = string.Format("{0}\\{1}\\{2}.h", DlDir, searchId, wkbName);
                        File.WriteAllText(wkbHPath, string.Join(",", b.rtCols.Select(x => x.ToString())));
                        client2.Dispose();
                        resultNo++;
                    }
                }
            });

            //return textData;
            return pathString;
#else
            List<SearchResultPaths> s = null;
            if (string.IsNullOrEmpty(textData) || "[]" == textData)
            {
                msg += GripDataResponseType.ErrorNotFound;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorNotFound;
            }

            try
            {
                s = JsonUtil.Deserialize<List<SearchResultPaths>>(textData);
            }
            catch
            {
                msg += GripDataResponseType.ErrorProcess;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorProcess;
            }
            if (null == s)
            {
                msg += GripDataResponseType.ErrorProcess;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorProcess;
            }

            bool isNoData = true;
            await Task.Run(() =>
            {
                Thread.Sleep(WAITTIME_1);
                for (int i = 0; i < s.Count; i++)
                {
                    SearchResultPaths ps = s[i];
                    if (null == ps.paths || 0 == ps.paths.Count) continue;
                    for (int j = 0; j < ps.paths.Count; j++)
                    {
                        SearchResultPath p = ps.paths[j];
                        if (null == p.nodes || 2 > p.nodes.Count) continue;

                        string url2 = CmsUrl.Search.GetCsvBlock();
                        SearchResultBlock block = new SearchResultBlock
                        {
                            searchId = searchId,
                            endId = (i + 1),
                            pathId = (j + 1),
                            beginRow = 1,
                            endRow = ROWS_PER_PAGE
                        };
                        string json = JsonUtil.Serialize(block);
                        while (true)
                        {
                            Thread.Sleep(WAITTIME_2); logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() client2.Post: url2:" + url2 + " theContent:" + json);
                            StringContent theContent = new StringContent(json, new UTF8Encoding(false), "application/json");
                            var client2 = new FoaHttpClient();
                            var textData2 = client2.Post(url2, theContent); logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() client2.Post: textData2:" + textData2.Result);
                            var resultBlock = JsonUtil.Deserialize<SearchResultBlock>(textData2.Result);
                            if (resultBlock.isCreatingStatus()) continue;
                            p.resultBlock = resultBlock;
                            break;
                        }
                        if (0 < p.resultBlock.blockRows && p.resultBlock.isNormalStatus())
                        {
                            p.existsData = true;
                            isNoData = false;
                        }
                    }
                }
            }); logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() client2.Post: isNoData:" + isNoData);
            //AISMM No.85 sunyi 20181211 start
            //dataがない時も、シートを作成する
            //if (isNoData)
            //{
            //    return null;
            //    //return GripDataResponseType.NoFolder;
            //}
            //AISMM No.85 sunyi 20181211 end

            // 一時ダウンロード用のディレクトリ作成
            string pathString = DlDir + "\\" + searchId;
            if (!Directory.Exists(DlDir)) Directory.CreateDirectory(DlDir);
            if (!Directory.Exists(DlDir + "\\" + searchId)) Directory.CreateDirectory(DlDir + "\\" + searchId);
            string nowDateTime = DateTime.Now.ToString(FileDateFormat2);

            await Task.Run(() =>
            {
                int resultNo = 0;
                Thread.Sleep(WAITTIME_1);
                for (int i = 0; i < s.Count; i++)
                {
                    SearchResultPaths ps = s[i];
                    for (int j = 0; j < ps.paths.Count; j++)
                    {
                        SearchResultPath p = ps.paths[j];
                        //AISMM No.85 sunyi 20181211 start
                        //dataがない時も、シートを作成する
                        //if (!p.existsData) continue;
                        //AISMM No.85 sunyi 20181211 end

                        int endId = p.resultBlock.endId; int pathId = p.resultBlock.pathId;
                        string url2 = string.Format(CmsUrl.Search.Download(), searchId, endId, pathId);
                        string[] titleFields = GetTitleFields(p.resultBlock.data);
                        //string ctmName = titleFields[p.resultBlock.getRtCol(p.resultBlock.rtCols.Count - 1)];
                        string ctmName="";
                        for( int k= p.resultBlock.rtCols.Count-1; k >= 0; k--)
                        {
                            if (p.resultBlock.rtCols[k] < 0)
                                continue;
                            else
                            {
                                ctmName = titleFields[p.resultBlock.rtCols[k]];
                                break;
                            }
                        }

                        Thread.Sleep(WAITTIME_2);
                        //WebClient client2 = new WebClient();
                        WebClient client2 = AisUtil.getProxyWebClient(); 
 
                        logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() client2D.Post: url2:" + url2);
                        byte[] fileData = client2.DownloadData(new System.Uri(url2)); logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() client2D.Post: fileData:" + fileData.Length);
                        //string stringData = Encoding.GetEncoding("Shift_JIS").GetString(fileData);
                        string stringData = Encoding.GetEncoding("Shift_JIS").GetString(fileData);
                        string oldStringData = GripR2CsvTranslator.TranslateBack(stringData, p.resultBlock);
                        string resultCtmName = TrimmingString(this.InvalidChars.Aggregate(ctmName, (str, c) => str.Replace(c.ToString(), string.Empty)));
                        //string wkbName = string.Format("[Result]{0}({1}{2})_{3}", resultCtmName, "route", b.pathId, nowDateTime);
                        string wkbName = string.Format("{0}_{1}_{2}{3}", resultNo, resultCtmName, "Route-", p.resultBlock.pathId);
                        string wkbPath = string.Format("{0}\\{1}\\{2}.csv", DlDir, searchId, wkbName);
                        //File.WriteAllText(wkbPath, stringData, new UTF8Encoding(true));
                        File.WriteAllText(wkbPath, oldStringData, new UTF8Encoding(true));
                        //using (FileStream fileStream = new FileStream(wkbPath, FileMode.Create))
                        //{
                        //    fileStream.Write(fileData, 0, fileData.Length);
                        //    fileStream.Flush();
                        //    fileStream.Close();
                        //}
                        string wkbHPath = string.Format("{0}\\{1}\\{2}.h", DlDir, searchId, wkbName);
                        List<int> rtCols2 = new List<int>();
                        for (int rc = 0; rc < p.resultBlock.rtCols.Count; rc++)
                        {
                            rtCols2.Add(p.resultBlock.getRtCol(rc) + 1 + rc);
                        }
                        File.WriteAllText(wkbHPath, string.Join(",", rtCols2.Select(x => x.ToString())));
                        //File.WriteAllText(wkbHPath, string.Join(",", p.resultBlock.rtCols.Select(x => x.ToString())));
                        client2.Dispose();
                        resultNo++;
                    }
                }
            });

            return pathString;
#endif
        }

        
        public string GetGripDatatCsv(GripMissionCtm mission, string searchId, long start, long end, string searchByInCondition, bool skipSameMainKey, CancellationToken cToken)
        {
            string url = string.Format(CmsUrl.Search.Mission.CreateCsv(), mission.Id, searchId, start, end, searchByInCondition, skipSameMainKey);
            var client = new FoaHttpClient();
            var response = client.Get(url);
            var textData = response.Result;

            if (string.IsNullOrEmpty(textData))
            {
                return string.Empty;
                //return GripDataResponseType.ErrorNotFound;
            }

#if false
            SearchResultCsv s = null;
            try
            {
                s = JsonUtil.Deserialize<SearchResultCsv>(textData);
            }
            catch
            {
                return GripDataResponseType.ErrorProcess;
            }
            if (null == s)
            {
                return GripDataResponseType.ErrorProcess;
            }
            if (null == s.blocks)
            {
                return GripDataResponseType.NoFolder;
            }
            bool isNoData = true;
            for (int i = 0; i < s.blocks.Count; i++)
            {
                List<SearchResultBlock> p = s.blocks[i];
                for (int j = 0; j < p.Count; j++)
                {
                    SearchResultBlock b = p[j];
                    if (b.blockRows > 0)
                    {
                        isNoData = false;
                        break;
                    }
                }
                if (!isNoData) break;
            }
            if (isNoData)
            {
                return GripDataResponseType.NoFolder;
            }

            // 一時ダウンロード用のディレクトリ作成
            string pathString = DlDir + "\\" + searchId;
            if (!Directory.Exists(DlDir)) Directory.CreateDirectory(DlDir);
            if (!Directory.Exists(DlDir + "\\" + searchId)) Directory.CreateDirectory(DlDir + "\\" + searchId);
            string nowDateTime = DateTime.Now.ToString(FileDateFormat2);

            int resultNo = 0;
            for (int i = 0; i < s.blocks.Count; i++)
            {
                List<SearchResultBlock> p = s.blocks[i];
                for (int j = 0; j < p.Count; j++)
                {
                    SearchResultBlock b = p[j];
                    string url2 = string.Format(CmsUrl.Search.Download(), b.searchId, b.endId, b.pathId);
                    string[] titleFields = GetTitleFields(b.data);
                    string ctmName = titleFields[b.rtCols[b.rtCols.Count - 1]];

                    //WebClient client2 = new WebClient();
                    WebClient client2 = AisUtil.getProxyWebClient(); 
 
                    byte[] fileData = client2.DownloadData(new System.Uri(url2));
                    string resultCtmName = TrimmingString(this.InvalidChars.Aggregate(ctmName, (str, c) => str.Replace(c.ToString(), string.Empty)));
                    //string wkbName = string.Format("[Result]{0}({1}{2})_{3}", resultCtmName, "route", b.pathId, nowDateTime);
                    string wkbName = string.Format("{0}_{1}_{2}{3}", resultNo, resultCtmName, "Route-", b.pathId);
                    string wkbPath = string.Format("{0}\\{1}\\{2}.csv", DlDir, searchId, wkbName);
                    using (FileStream fileStream = new FileStream(wkbPath, FileMode.Create))
                    {
                        fileStream.Write(fileData, 0, fileData.Length);
                        fileStream.Flush();
                        fileStream.Close();
                    }
                    string wkbHPath = string.Format("{0}\\{1}\\{2}.h", DlDir, searchId, wkbName);
                    File.WriteAllText(wkbHPath, string.Join(",", b.rtCols.Select(x => x.ToString())));
                    client2.Dispose();
                    resultNo++;
                }
            }

            return pathString;
#else
            List<SearchResultPaths> s = null;
            if (string.IsNullOrEmpty(textData) || "[]" == textData)
            {
                return string.Empty;
                //return GripDataResponseType.ErrorNotFound;
            }

            try
            {
                s = JsonUtil.Deserialize<List<SearchResultPaths>>(textData);
            }
            catch
            {
                return string.Empty;
                //return GripDataResponseType.ErrorProcess;
            }
            if (null == s)
            {
                return string.Empty;
                //return GripDataResponseType.ErrorProcess;
            }

            // bool isNoData = true;
            Thread.Sleep(WAITTIME_1);
            for (int i = 0; i < s.Count; i++)
            {
                SearchResultPaths ps = s[i];
                if (null == ps.paths || 0 == ps.paths.Count) continue;
                for (int j = 0; j < ps.paths.Count; j++)
                {
                    SearchResultPath p = ps.paths[j];
                    if (null == p.nodes || 2 > p.nodes.Count) continue;

                    string url2 = CmsUrl.Search.GetCsvBlock();
                    SearchResultBlock block = new SearchResultBlock
                    {
                        searchId = searchId,
                        endId = (i + 1),
                        pathId = (j + 1),
                        beginRow = 1,
                        endRow = ROWS_PER_PAGE
                    };
                    string json = JsonUtil.Serialize(block);
                    while (true)
                    {
                        Thread.Sleep(WAITTIME_2);
                        StringContent theContent = new StringContent(json, new UTF8Encoding(false), "application/json");
                        var client2 = new FoaHttpClient();
                        var textData2 = client2.Post(url2, theContent);
                        var resultBlock = JsonUtil.Deserialize<SearchResultBlock>(textData2.Result);
                        if (resultBlock.isCreatingStatus()) continue;
                        p.resultBlock = resultBlock;
                        break;
                    }
                    if (0 < p.resultBlock.blockRows && p.resultBlock.isNormalStatus())
                    {
                        p.existsData = true;
                        // isNoData = false;
                    }
                }
            }
            //AISMM No.85 sunyi 20181211 start
            //dataがない時も、シートを作成する
            //if (isNoData)
            //{
            //    return string.Empty;
            //    //return GripDataResponseType.NoFolder;
            //}
            //AISMM No.85 sunyi 20181211 end

            // 一時ダウンロード用のディレクトリ作成
            string pathString = DlDir + "\\" + searchId;
            if (!Directory.Exists(DlDir)) Directory.CreateDirectory(DlDir);
            if (!Directory.Exists(DlDir + "\\" + searchId)) Directory.CreateDirectory(DlDir + "\\" + searchId);
            string nowDateTime = DateTime.Now.ToString(FileDateFormat2);

            int resultNo = 0;
            Thread.Sleep(WAITTIME_1);
            for (int i = 0; i < s.Count; i++)
            {
                SearchResultPaths ps = s[i];
                for (int j = 0; j < ps.paths.Count; j++)
                {
                    SearchResultPath p = ps.paths[j];
                    //AISMM No.85 sunyi 20181211 start
                    //dataがない時も、シートを作成する
                    //if (!p.existsData) continue;
                    //AISMM No.85 sunyi 20181211 end

                    int endId = p.resultBlock.endId; int pathId = p.resultBlock.pathId;
                    string url2 = string.Format(CmsUrl.Search.Download(), searchId, endId, pathId);
                    string[] titleFields = GetTitleFields(p.resultBlock.data);
                    string ctmName = titleFields[p.resultBlock.getRtCol(p.resultBlock.rtCols.Count - 1)];

                    Thread.Sleep(WAITTIME_2);
                    //WebClient client2 = new WebClient();
                    WebClient client2 = AisUtil.getProxyWebClient(); 
 
                    byte[] fileData = client2.DownloadData(new System.Uri(url2));
                    //string stringData = Encoding.GetEncoding("Shift_JIS").GetString(fileData);
                    string stringData = Encoding.GetEncoding("Shift_JIS").GetString(fileData);
                    string oldStringData = GripR2CsvTranslator.TranslateBack(stringData, p.resultBlock);
                    string resultCtmName = TrimmingString(this.InvalidChars.Aggregate(ctmName, (str, c) => str.Replace(c.ToString(), string.Empty)));
                    //string wkbName = string.Format("[Result]{0}({1}{2})_{3}", resultCtmName, "route", b.pathId, nowDateTime);
                    string wkbName = string.Format("{0}_{1}_{2}{3}", resultNo, resultCtmName, "Route-", p.resultBlock.pathId);
                    string wkbPath = string.Format("{0}\\{1}\\{2}.csv", DlDir, searchId, wkbName);
                    //File.WriteAllText(wkbPath, stringData, new UTF8Encoding(true));
                    File.WriteAllText(wkbPath, oldStringData, new UTF8Encoding(true));
                    //using (FileStream fileStream = new FileStream(wkbPath, FileMode.Create))
                    //{
                    //    fileStream.Write(fileData, 0, fileData.Length);
                    //    fileStream.Flush();
                    //    fileStream.Close();
                    //}
                    string wkbHPath = string.Format("{0}\\{1}\\{2}.h", DlDir, searchId, wkbName);
                    List<int> rtCols2 = new List<int>();
                    for (int rc = 0; rc < p.resultBlock.rtCols.Count; rc++)
                    {
                        rtCols2.Add(p.resultBlock.getRtCol(rc) + 1 + rc);
                    }
                    File.WriteAllText(wkbHPath, string.Join(",", rtCols2.Select(x => x.ToString())));
                    //File.WriteAllText(wkbHPath, string.Join(",", p.resultBlock.rtCols.Select(x => x.ToString())));
                    client2.Dispose();
                    resultNo++;
                }
            }

            return pathString;
#endif
        }

        public string CancelExecution()
        {
            return string.Empty;
        }

        public string GetStatus()
        {
            ////string missionId = mission.Id;
            //string ip = "localhost";
            //int port = 20000;

            //string response = string.Empty;
            //response = SendMessage(ip, port, GripDataCommandType.GetStatus);

            //return response;
            //return string.Empty;
            return GripDataResponseType.StatusIdle;
        }







        /// <summary>
        /// 一時ファイル置き場
        /// </summary>
        public static readonly string DlDir = System.AppDomain.CurrentDomain.BaseDirectory + "\\grip_temp";

        /// <summary>
        /// ファイル名に付与する時刻のフォーマット
        /// </summary>
        const string FileDateFormat = "yyyyMMddHHmmssfff";

        /// <summary>
        /// ファイル名に付与する時刻のフォーマット
        /// </summary>
        const string FileDateFormat2 = "yyyy-MM-dd-HH-mm-ss-fff";

        /// <summary>
        /// ファイル禁止文字列
        /// </summary>
        readonly char[] InvalidChars = System.IO.Path.GetInvalidFileNameChars();

        /// <summary>
        /// トリミングするときのデフォルト文字数
        /// </summary>
        const int MaxTrimLength = 25;

        /// <summary>
        /// 指定した文字列を指定した長さでトリミングします。
        /// 長さが0以下の場合は25文字でトリミングします。
        /// </summary>
        /// <param name="v">文字列</param>
        /// <param name="length">トリミングする桁数</param>
        /// <returns>トリミングした文字列</returns>
        public static string TrimmingString(string v, int length = MaxTrimLength)
        {
            if (length < 1) length = MaxTrimLength;
            if (!string.IsNullOrEmpty(v) && v.Length > length)
            {
                v = v.Substring(0, length);
            }
            return v;
        }

        private string[] GetTitleFields(string data)
        {
            MemoryStream titleStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(data));

            string[] titleFields = null;
            using (TextFieldParser parser = new TextFieldParser(titleStream))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(",");
                while (!parser.EndOfData)
                {
                    titleFields = parser.ReadFields();
                    break;
                }
                parser.Close();
            }
            titleStream.Close();

            return titleFields;
        }

        /// <summary>
        /// grip_tempを削除
        /// </summary>
        public static void DeleteTempDir()
        {
            try
            {
                if (Directory.Exists(DlDir))
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(DlDir);
                    DeleteTempDir(dirInfo);
                }
            }
#pragma warning disable
            catch (Exception e)
#pragma warning restore
            {
                //GripLogger.Warn(e.Message);
            }
        }

        /// <summary>
        /// grip_tempを削除
        /// </summary>
        /// <param name="dirInfo"></param>
        private static void DeleteTempDir(DirectoryInfo dirInfo)
        {
            try
            {
                // ディレクトリ内のファイルの読み取り専用解除
                FileInfo[] fileInfo = dirInfo.GetFiles();
                foreach (FileInfo f in fileInfo)
                {
                    if ((f.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                    {
                        f.Attributes = FileAttributes.Normal;
                    }
                }

                // ディレクトリ内のサブフォルダ確認
                foreach (System.IO.DirectoryInfo subDirInfo in dirInfo.GetDirectories())
                {
                    DeleteTempDir(subDirInfo);
                }

                // このディレクトリの読み取り専用解除
                if ((dirInfo.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    dirInfo.Attributes = FileAttributes.Directory;
                }

                // まとめて削除
                dirInfo.Delete(true);
            }
            catch
            {
                throw;
            }
        }

#else
        private const int POLLING_INTERVAL = 3 * 1000; // 3秒
        public async Task<string> GetGripDatatCsvAsync(GripMissionCtm mission, long start, long end, CancellationToken cToken)
        {
            string path = null;
            string response1 = null;
            string responseWarning = null;
            await Task.Run(() =>
            {
                string missionId = mission.Id;
                string ip = "localhost";
                int port = 20000;

                try
                {
                    string response = sendMessage(ip, port, GripDataCommandType.Execute, missionId, start, end);
                    if (response != GripDataResponseType.OnSearching)
                    {
                        logger.Error("Search failed: " + response);
                        response1 = response;
                        return;
                    }

                    while (true)
                    {
                        Thread.Sleep(POLLING_INTERVAL);

                        if (cToken.IsCancellationRequested)
                        {
                            break;
                        }

                        response = sendMessage(ip, port, GripDataCommandType.GetResult, missionId);
                        if (!GripDataResponseType.ErrorResponseList.Contains(response))
                        {
                            path = response;
                            break;
                        }
                        if (response == GripDataResponseType.NoFolder)
                        {
                            logger.Warn("Search No Data: " + response);
                            responseWarning = "指定した期間で、データが存在しません。";
                            break;
                        }
                    }
                }
                catch (SocketException se)
                {
                    logger.Error("Search failed.", se);
                    response1 = se.Message;
                }
            });

            if (cToken.IsCancellationRequested)
            {
                return path;
            }

            if (response1 != null)
            {
                string msg = "Gripミッションの取得に失敗しました。 " + response1;

                Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (responseWarning != null)
            {
                string msg = responseWarning;

                Xceed.Wpf.Toolkit.MessageBox.Show(msg, "NO DATA", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            return path;
        }

        public string GetGripDatatCsv(GripMissionCtm mission, long start, long end, CancellationToken cToken)
        {
            string missionId = mission.Id;
            string ip = "localhost";
            int port = 20000;

            string response = string.Empty;
            response = sendMessage(ip, port, GripDataCommandType.Execute, missionId, start, end);

            string path = string.Empty;
            while (true)
            {
                Thread.Sleep(3 * 1000);

                response = sendMessage(ip, port, GripDataCommandType.GetResult, missionId);
                if (!GripDataResponseType.ErrorResponseList.Contains(response))
                {
                    path = response;
                    break;
                }
                if (response == GripDataResponseType.NoFolder)
                {
                    //logger.Warn("Search No Data: " + response);
                    //responseWarning = "指定した期間で、データが存在しません。";
                    break;
                }
            }

            return path;
        }

        public string GetStatus()
        {
            //string missionId = mission.Id;
            string ip = "localhost";
            int port = 20000;

            string response = string.Empty;
            response = sendMessage(ip, port, GripDataCommandType.GetStatus);

            return response;
        }

        public string CancelExecution()
        {
            //string missionId = mission.Id;
            string ip = "localhost";
            int port = 20000;

            string response = string.Empty;
            response = sendMessage(ip, port, GripDataCommandType.CancelExecution);

            return response;
        }

        private string sendMessage(string ip, int port, string command, string missionId, long start = -1, long end = -1)
        {
            string requestText = createMessageText(command, missionId, start, end);
            byte[] requestBytes = createSendMessage(requestText);

            TcpClient client = new TcpClient(ip, port);
            NetworkStream stream = client.GetStream();
            stream.Write(requestBytes, 0, requestBytes.Length);

            string responseText = getResponseMessage(stream);
            return responseText;
        }
        private string sendMessage(string ip, int port, string command)
        {
            string requestText = createMessageText(command);
            byte[] requestBytes = createSendMessage(requestText);

            TcpClient client = new TcpClient(ip, port);
            NetworkStream stream = client.GetStream();
            stream.Write(requestBytes, 0, requestBytes.Length);

            string responseText = getResponseMessage(stream);
            return responseText;
        }

        private string createMessageText(string command, string missionId, long start = -1, long end = -1)
        {
            return "cmd=" + command + "&" + "mission_id=" + missionId + (start < 0 ? string.Empty : "&" + "start_time=" + start.ToString()) + (end < 0 ? string.Empty : "&" + "end_time=" + end.ToString());
        }
        private string createMessageText(string command)
        {
            return "cmd=" + command;
        }

        private byte[] createSendMessage(string message)
        {
            byte[] msgBytes = System.Text.Encoding.UTF8.GetBytes(message);
            int len = msgBytes.Length;
            byte[] lenBytes = BitConverter.GetBytes(len);
            byte[] headerBytes = new byte[4];
            for (int i = 0; i < 4; i++)
                headerBytes[i] = lenBytes[4 - 1 - i];

            byte[] tcpDataBytes = new byte[4 + len];
            System.Buffer.BlockCopy(headerBytes, 0, tcpDataBytes, 0, 4);
            System.Buffer.BlockCopy(msgBytes, 0, tcpDataBytes, 4, len);

            return tcpDataBytes;
        }

        private byte[] getResponseMessageByteBlock(NetworkStream stream)
        {
            //サーバーから送られたデータを受信する
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            byte[] resBytes = new byte[4096];
            int resSize = 0;

            //データの一部を受信する
            resSize = stream.Read(resBytes, 0, resBytes.Length);

            //Readが0を返した時はサーバーが切断したと判断
            if (resSize == 0)
            {
                Console.WriteLine("サーバーが切断しました。");
                //break;
            }

            //受信したデータを蓄積する
            ms.Write(resBytes, 0, resSize);

            byte[] responseBytes = ms.GetBuffer();
            // メッセージヘッダを読み込む
            byte[] headerBytes = new byte[4]; // 4バイトのヘッダ(メッセージ長)を受け取る

            // CGから送られるバイト長データがBIG ENDIANの場合、ここでReverseして、
            // INT型にパース出来るようにする。
            System.Buffer.BlockCopy(responseBytes, 0, headerBytes, 0, 4);
            Array.Reverse(headerBytes);
            int msgLen = BitConverter.ToInt32(headerBytes, 0);
            Console.WriteLine(msgLen.ToString());

            byte[] msgBytes = new byte[msgLen];
            System.Buffer.BlockCopy(responseBytes, 4, msgBytes, 0, msgLen);

            return msgBytes;
        }

        private string getResponseMessageText(byte[] msgBytes)
        {
            string responseText = System.Text.Encoding.UTF8.GetString(msgBytes, 0, msgBytes.Length);
            return responseText;
        }

        private string getResponseMessage(NetworkStream stream)
        {
            return getResponseMessageText(getResponseMessageByteBlock(stream));
        }
#endif

        private static readonly ILog logger = LogManager.GetLogger(typeof(GripDataRetriever));
        //AISMM-92 sunyi 20190227 start
        public async Task<string> GetGripDatatCsvAsyncByMissionId(JToken missionToken, string searchId, long start, long end, string searchByInCondition, bool skipSameMainKey, CancellationToken cToken, Dictionary<string, string> UUIDAndMissionIds = null)
        //AISMM-92 sunyi 20190227 end
        {
            string textData = null;
            string response1 = null;
            string responseWarning = null;
            string missionId = (string)missionToken["id"];
            //AISMM-92 sunyi 20190227 start
            if (UUIDAndMissionIds != null)
            {
                UUIDAndMissionIds.Add(searchId, missionId);
            }
            //AISMM-92 sunyi 20190227 end
            int routeIndex = int.Parse((string)missionToken["routeIndex"]);
            await Task.Run(() =>
            {
                try
                {
                    string url = string.Format(CmsUrl.Search.Mission.CreateCsv(), missionId, searchId, start, end, searchByInCondition, skipSameMainKey); logger.Debug(url);
                    var client = new FoaHttpClient();
                    var response = client.Get(url);
                    textData = response.Result;
                }
                catch (SocketException se)
                {
                    logger.Error("Search failed.", se);
                    response1 = se.Message;
                }
            });
            logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() textData:" + textData);
            if (cToken.IsCancellationRequested)
            {
                return textData;
            }
            string msg = DAC.Properties.Message.AIS_E_046;
            if (response1 != null)
            {
                msg += response1;
                AisMessageBox.DisplayErrorMessageBox(msg);
            }
            if (responseWarning != null)
            {
                msg = responseWarning;
                AisMessageBox.DisplayWarningMessageBoxWithCaption("NO DATA", msg);
            }
            if (string.IsNullOrEmpty(textData))
            {
                msg += GripDataResponseType.ErrorNotFound;
                AisMessageBox.DisplayErrorMessageBox(msg);
                return null;
            }
#if false
            SearchResultCsv s = null;
            try
            {
                s = JsonUtil.Deserialize<SearchResultCsv>(textData);
            }
            catch
            {
                return GripDataResponseType.ErrorProcess;
            }
            if (null == s)
            {
                return GripDataResponseType.ErrorProcess;
            }
            if (null == s.blocks)
            {
                return GripDataResponseType.NoFolder;
            }
            bool isNoData = true;
            for (int i = 0; i < s.blocks.Count; i++)
            {
                List<SearchResultBlock> p = s.blocks[i];
                for (int j = 0; j < p.Count; j++)
                {
                    SearchResultBlock b = p[j];
                    if (b.blockRows > 0)
                    {
                        isNoData = false;
                        break;
                    }
                }
                if (!isNoData) break;
            }
            if (isNoData)
            {
                return GripDataResponseType.NoFolder;
            }
            string pathString = DlDir + "\\" + searchId;
            if (!Directory.Exists(DlDir)) Directory.CreateDirectory(DlDir);
            if (!Directory.Exists(DlDir + "\\" + searchId)) Directory.CreateDirectory(DlDir + "\\" + searchId);
            string nowDateTime = DateTime.Now.ToString(FileDateFormat2);
            await Task.Run(() =>
            {
                int resultNo = 0;
                for (int i = 0; i < s.blocks.Count; i++)
                {
                    List<SearchResultBlock> p = s.blocks[i];
                    for (int j = 0; j < p.Count; j++)
                    {
                        SearchResultBlock b = p[j];
                        string url2 = string.Format(CmsUrl.Search.Download(), b.searchId, b.endId, b.pathId);
                        string[] titleFields = GetTitleFields(b.data);
                        string ctmName = titleFields[b.rtCols[b.rtCols.Count - 1]];
                        WebClient client2 = AisUtil.getProxyWebClient(); 
                        byte[] fileData = client2.DownloadData(new System.Uri(url2));
                        string resultCtmName = TrimmingString(this.InvalidChars.Aggregate(ctmName, (str, c) => str.Replace(c.ToString(), string.Empty)));
                        string wkbName = string.Format("{0}_{1}_{2}{3}", resultNo, resultCtmName, "Route-", b.pathId);
                        string wkbPath = string.Format("{0}\\{1}\\{2}.csv", DlDir, searchId, wkbName);
                        using (FileStream fileStream = new FileStream(wkbPath, FileMode.Create))
                        {
                            fileStream.Write(fileData, 0, fileData.Length);
                            fileStream.Flush();
                            fileStream.Close();
                        }
                        string wkbHPath = string.Format("{0}\\{1}\\{2}.h", DlDir, searchId, wkbName);
                        File.WriteAllText(wkbHPath, string.Join(",", b.rtCols.Select(x => x.ToString())));
                        client2.Dispose();
                        resultNo++;
                    }
                }
            });
            return pathString;
#else
            List<SearchResultPaths> s = null;
            if (string.IsNullOrEmpty(textData) || "[]" == textData)
            {
                msg += GripDataResponseType.ErrorNotFound;
                AisMessageBox.DisplayErrorMessageBox(msg);
                return null;
            }
            try
            {
                s = JsonUtil.Deserialize<List<SearchResultPaths>>(textData);
            }
            catch
            {
                msg += GripDataResponseType.ErrorProcess;
                AisMessageBox.DisplayErrorMessageBox(msg);
                return null;
            }
            if (null == s)
            {
                msg += GripDataResponseType.ErrorProcess;
                AisMessageBox.DisplayErrorMessageBox(msg);
                return null;
            }
            bool isNoData = true;
            await Task.Run(() =>
            {
                Thread.Sleep(WAITTIME_1);
                for (int i = 0; i < s.Count; i++)
                {
                    SearchResultPaths ps = s[i];
                    if (null == ps.paths || 0 == ps.paths.Count) continue;
                    for (int j = 0; j < ps.paths.Count; j++)
                    {
                        SearchResultPath p = ps.paths[j];
                        if (null == p.nodes || 2 > p.nodes.Count) continue;
                        string url2 = CmsUrl.Search.GetCsvBlock();
                        SearchResultBlock block = new SearchResultBlock
                        {
                            searchId = searchId,
                            endId = (i + 1),
                            pathId = (j + 1),
                            beginRow = 1,
                            endRow = ROWS_PER_PAGE
                        };
                        string json = JsonUtil.Serialize(block);
                        while (true)
                        {
                            Thread.Sleep(WAITTIME_2); logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() client2.Post: url2:" + url2 + " theContent:" + json);
                            StringContent theContent = new StringContent(json, new UTF8Encoding(false), "application/json");
                            var client2 = new FoaHttpClient();
                            var textData2 = client2.Post(url2, theContent); logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() client2.Post: textData2:" + textData2.Result);
                            var resultBlock = JsonUtil.Deserialize<SearchResultBlock>(textData2.Result);
                            if (resultBlock.isCreatingStatus()) continue;
                            p.resultBlock = resultBlock;
                            break;
                        }
                        if (0 < p.resultBlock.blockRows && p.resultBlock.isNormalStatus())
                        {
                            p.existsData = true;
                            isNoData = false;
                        }
                    }
                }
            }); logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() client2.Post: isNoData:" + isNoData);
            //AISMM No.85 sunyi 20181211 start
            //dataがない時も、シートを作成する
            //if (isNoData)
            //{
            //    return null;
            //}
            //AISMM No.85 sunyi 20181211 end
            string pathString = DlDir + "\\" + searchId;
            if (!Directory.Exists(DlDir)) Directory.CreateDirectory(DlDir);
            if (!Directory.Exists(DlDir + "\\" + searchId)) Directory.CreateDirectory(DlDir + "\\" + searchId);
            string nowDateTime = DateTime.Now.ToString(FileDateFormat2);
            await Task.Run(() =>
            {
                int resultNo = 0;
                Thread.Sleep(WAITTIME_1);
                for (int i = 0; i < s.Count; i++)
                {
                    SearchResultPaths ps = s[i];
                    for (int j = 0; j < ps.paths.Count; j++)
                    {
                        SearchResultPath p = ps.paths[j];
                        //AISMM No.85 sunyi 20181211 start
                        //dataがない時も、シートを作成する
                        //if (!p.existsData) continue;
                        //AISMM No.85 sunyi 20181211 end
                        int endId = p.resultBlock.endId; int pathId = p.resultBlock.pathId;
                        string url2 = string.Format(CmsUrl.Search.Download(), searchId, endId, pathId);
                        string[] titleFields = GetTitleFields(p.resultBlock.data);
                        //string ctmName = titleFields[p.resultBlock.rtCols[p.resultBlock.rtCols.Count - 1]];
                        //string ctmName = titleFields[p.resultBlock.getRtCol(p.resultBlock.rtCols.Count - 1)];
                        string ctmName = "";
                        for (int k = p.resultBlock.rtCols.Count - 1; k >=0; k--)
                        {
                            if (p.resultBlock.rtCols[k] < 0)
                                continue;
                            else
                            {
                                ctmName = titleFields[p.resultBlock.rtCols[k]];
                                break;
                            }
                        }
                        Thread.Sleep(WAITTIME_2);
                        WebClient client2 = AisUtil.getProxyWebClient();
                        logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() client2D.Post: url2:" + url2);
                        byte[] fileData = client2.DownloadData(new System.Uri(url2)); logger.Debug("AIS.CtmData.GripDataRetriever.GetGripDatatCsvAsync() client2D.Post: fileData:" + fileData.Length);
                        string stringData = Encoding.GetEncoding("Shift_JIS").GetString(fileData);
                        string oldStringData = GripR2CsvTranslator.TranslateBack(stringData, p.resultBlock);
                        string resultCtmName = TrimmingString(this.InvalidChars.Aggregate(ctmName, (str, c) => str.Replace(c.ToString(), string.Empty)));
                        string wkbName = string.Format("{0}_{1}_{2}{3}", resultNo, resultCtmName, "Route-", p.resultBlock.pathId);
                        wkbName = routeIndex + "_" + wkbName;
                        string wkbPath = string.Format("{0}\\{1}\\{2}.csv", DlDir, searchId, wkbName);
                        File.WriteAllText(wkbPath, oldStringData, new UTF8Encoding(true));
                        string wkbHPath = string.Format("{0}\\{1}\\{2}.h", DlDir, searchId, wkbName);
                        List<int> rtCols2 = new List<int>();
                        for (int rc = 0; rc < p.resultBlock.rtCols.Count; rc++)
                        {
                            rtCols2.Add(p.resultBlock.getRtCol(rc) + 1 + rc);
                        }
                        File.WriteAllText(wkbHPath, string.Join(",", rtCols2.Select(x => x.ToString())));
                        client2.Dispose();
                        resultNo++;
                    }
                }
            });
            return pathString;
#endif
        }
    }

    class GripDataCommandType
    {
        public const string Execute = "execute";
        public const string GetResult = "get_result";
        public const string GetStatus = "get_status";
        public const string CancelExecution = "cancel_execution";
    }

    class GripDataResponseType
    {
        public const string ErrorRequestEmpty = "ERR_REQUEST_EMPTY";
        public const string StatusBusy = "STATUS_BUSY";
        public const string NoFolder = "NO_FOLDER";
        public const string ErrorBusy = "ERR_BUSY";
        public const string ErrorNotFound = "ERR_NOT_FOUND";
        public const string ErrorProcess = "ERR_PROCESS";
        public const string ErrorNotFoundStartNode = "ERR_NOT_FOUND_START_NODE";
        public const string ErrorNotFoundEndNode = "ERR_NOT_FOUND_END_NODE";
        public const string ErrorNotFoundTraceNode = "ERR_NOT_FOUND_TRACE_NODE";
        public const string OnSearching = "ON_SEARCHING";
        public const string StatusIdle = "STATUS_IDLE";
        public const string OnCanceling = "ON_CANCELING";

        public static List<string> ErrorResponseList = new List<string>() { ErrorRequestEmpty, StatusBusy, NoFolder, ErrorBusy, ErrorNotFound, ErrorProcess, ErrorNotFoundStartNode, ErrorNotFoundEndNode, ErrorNotFoundTraceNode, OnSearching, StatusIdle, OnCanceling };
    }
}
