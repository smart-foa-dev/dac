﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoaCore;
using DAC.Model;

namespace DAC.CtmData
{
    public class InputDataFile
    {
        public Suid Id;
        public string Name;
        public string Filepath;

        public virtual Dictionary<string, string> GetElementInfo()
        {
            return null;
        }

        public virtual CtmObject GetCtmObject()
        {
            return null;
        }
    }
}
