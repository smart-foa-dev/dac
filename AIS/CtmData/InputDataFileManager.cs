﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.CtmData
{
   public class InputDataFileManager
    {

       private static readonly InputDataFileManager instance = new InputDataFileManager();

       private List<InputDataFile> idfList = new List<InputDataFile>();

       public static InputDataFileManager Instance
       {
           get { return instance; }
       }

       public void Add(InputDataFile df)
       {
           idfList.Add(df);
       }
    }
}
