﻿using DAC.Model;
using DAC.Model.Util;
using DAC.Util;
using DAC.View.Helpers;
using CsvHelper;
using FoaCore;
using FoaCore.Common.Util;
using FoaCore.Common.Util.Json;
using FoaCore.Model.GripR2.Search.WebSearch;
using FoaCore.Net;
using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using DAC.View;

namespace DAC.CtmData
{
    public class BulkyDataRetriever : CtmDataRetriever
    {
        //public bool isNoCsvOnly = true;
        public bool isNoCsvOnly = false;
        public bool isNoBulky = true;

        //AISBUL-31 sunyi 20181221 start
        //jpg優先判定する
        bool isNoJpg = true;
        //AISBUL-31 sunyi 20181221 end

        // Wang Issue MULTITEST-12 20190119 Start
        //public async Task<string> GetCtmDirectResultCsvAsync(string ctmId, long start, long end, CancellationToken cToken, string elementId)
        public async Task<string> GetCtmDirectResultCsvAsync(string ctmId, long start, long end, CancellationToken cToken, string elementId, BaseControl bc)
        // Wang Issue MULTITEST-12 20190119 End
        {
            AhInfo ahInfo = null;

            // ctmObjsの要素数が1以上であることは保障されている。
            if (AisConf.Config.CmsVersion == "3.5")
            {
                ahInfo = await AisUtil.GetAhInfoAsync(ctmId);
            }

            var folderPath = System.IO.Path.Combine(AisConf.TmpFolderPath, "ctm", Suid.NewID().ToString());
            Directory.CreateDirectory(folderPath);
            var tmpFilepathB = System.IO.Path.Combine(folderPath, elementId + ".csv");
            string url = string.Format("{0}{1}?testing=0", CmsUrl.GetMibBaseUrl(), "mib/ctm/find");

            // CTMを検索する為にPOSTするJSONを生成
            JToken postToken = JToken.Parse("{}");
            postToken["ctmId"] = ctmId;
            postToken["start"] = start;
            postToken["end"] = end;
            if (AisConf.Config.CmsVersion == "3.5")
            {
                postToken["mfHostName"] = ahInfo.Mf;
                postToken["mfPort"] = ahInfo.MfPort;
            }

            //HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(100) };
            HttpClient client = AisUtil.getProxyHttpClient();

            bool cancelled = false;
            bool failed = false;
            var req = new HttpRequestMessage(HttpMethod.Post, url);
            req.Content = new StringContent(postToken.ToString());

            var task = client.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cToken).ContinueWith(response0 =>
            {
                var tmpFilepath = System.IO.Path.Combine(folderPath, ctmId + ".csv");

                try
                {
                    var response = response0.Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        logger.Error("Failed to get CTM Direct result: " + ctmId + " StatusCode=" + (int)(response.StatusCode));
                        failed = true;
                    }
                    else
                    {
                        using (StreamWriter sw = new StreamWriter(tmpFilepath, false, new UTF8Encoding(false)))
                        using (CsvWriter writer = new CsvWriter(sw))
                        using (FileStream fs = File.Open(tmpFilepathB, FileMode.Create))
                        using (var stream = response.Content.ReadAsStreamAsync().Result)
                        using (var sr = new StreamReader(stream))
                        using (var iterator = new CtmDirectResultReader(sr).GetEnumerator(AisConf.Config.CmsVersion == "3.5"))
                        {
                            if (!iterator.MoveNext())
                            {
                                logger.Info(string.Format("CTM[{0}] is empty.", ctmId));
                            }
                            else
                            {
                                var rec = iterator.Current;

                                // header
                                List<string> keyOrder = new List<string>(rec.EL.Count);

                                writer.WriteField(Keywords.RECEIVE_TIME);
                                foreach (var el in rec.EL.Keys)
                                {
                                    writer.WriteField(el);
                                    keyOrder.Add(el); // For writing records
                                }

                                writer.NextRecord();
                                do
                                {
                                    rec = iterator.Current;

                                    // Records
                                    writer.WriteField<long>(rec.RT);
                                    foreach (var key in keyOrder)
                                    {
                                        var elem = rec.EL[key];
                                        //if (elem.T == "100")
                                        if (FoaDatatype.isBulky(int.Parse(elem.T)))
                                        {
                                            writer.WriteField(elem.FN);
                                            if (string.IsNullOrEmpty(elem.FN))
                                            {
                                                continue;
                                            }
                                            //AISBUL-31 sunyi 20181221 start
                                            //jpg優先判定する
                                            else if (Path.GetExtension(elem.FN).ToLower().Equals(".jpg"))
                                            {
                                                this.isNoJpg = false;
                                            }
                                            //AISBUL-31 sunyi 20181221 end
                                            else if (!Path.GetExtension(elem.FN).ToLower().Equals(".csv"))
                                            {
                                                this.isNoBulky = false;
                                                continue;
                                            }
                                            else
                                            {
                                                this.isNoBulky = false;
                                                this.isNoCsvOnly = false;
                                            }

                                            if (key == elementId && elem.V != null)
                                            {
                                                byte[] bulky = Convert.FromBase64String(elem.V);
                                                byte[] encodedBulky = bulky;
                                                if (AisUtil.IsBomUtf8(bulky))
                                                {
                                                    encodedBulky = AisUtil.RemoveBom(bulky);
                                                }
                                                else
                                                {
                                                    Encoding encoding = AisUtil.GetCode(bulky);
                                                    if (encoding != null)
                                                    {
                                                        if (!encoding.Equals(Encoding.UTF8))
                                                        {
                                                            encodedBulky = AisUtil.ConvertToWoUtf8Bytes(bulky, encoding);
                                                        }
                                                    }
                                                }
                                                fs.Write(encodedBulky, 0, encodedBulky.Length);
                                            }
                                        }
                                        else
                                        {
                                            writer.WriteField(elem.V);
                                        }
                                    }
                                    //AISBUL-31 sunyi 20181221 start
                                    //jpg優先判定する
                                    if (!this.isNoJpg)
                                    {
                                        this.isNoCsvOnly = true;
                                    }
                                    //AISBUL-31 sunyi 20181221 end
                                    writer.NextRecord(); // これなしだと直前のWriteFieldが書き込まれない。
                                } while (iterator.MoveNext());
                            }
                        }
                    }
                }
                catch (AggregateException e)
                {
                    Exception ex = e.InnerException;

                    if (ex is TaskCanceledException)
                    {
                        logger.Info("Cancelled by user.");
                        cancelled = true;

                        // Wang Issue MULTITEST-12 20190119 Start
                        bc.AfterCancel();
                        // Wang Issue MULTITEST-12 20190119 End
                    }
                    else
                    {
                        logger.Error("Failed to get CTM Direct result: " + ctmId, e);
                        failed = true;
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Failed to get CTM Direct result: " + ctmId, e);
                    failed = true;
                }
            });

            await task;

            if (cancelled || failed)
            {
                ClearDirectory(folderPath);
                folderPath = null;

                if (failed)
                {
                    FoaMessageBox.ShowError("AIS_E_055");
                    //string msg = "CTMの取得に失敗しました。";
                    //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            return folderPath;
        }

        // Wang Issue MULTITEST-12 20190119 Start
        //public async Task<string> GetMissionResultCsvAsync(string missionId, long start, long end, CancellationToken cancelToken, string ctmId, string elementId)
        public async Task<string> GetMissionResultCsvAsync(string missionId, long start, long end, CancellationToken cancelToken, string ctmId, string elementId, BaseControl bc)
        // Wang Issue MULTITEST-12 20190119 End
        {
            string limit = "0";
            string lang = "ja";
            //課題No.879 sunyi 2018/12/06 start
            //バルキの場合、降順⇒昇順
            int order = 1;
            //string url = string.Format("{0}{1}?id={2}&start={3}&end={4}&limit={5}&lang={6}", CmsUrl.GetMmsBaseUrl(), "mib/mission/pm", missionId, start, end, limit, lang);
            string url = string.Format("{0}{1}?id={2}&start={3}&end={4}&limit={5}&lang={6}&order={7}", CmsUrl.GetMmsBaseUrl(), "mib/mission/pm", missionId, start, end, limit, lang, order);
            //課題No.879 sunyi 2018/12/06 end
            var folderPath = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission", Suid.NewID().ToString());
            var tmpFilepathB = System.IO.Path.Combine(folderPath, elementId + ".csv");
            Directory.CreateDirectory(folderPath);
            //HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(100) };
            HttpClient client = AisUtil.getProxyHttpClient();

            bool failed = false;
            bool cancelled = false;

            var task = client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead, cancelToken).ContinueWith(response0 =>
            {
                try
                {
                    var response = response0.Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        logger.Error("Failed to get Mission result: " + missionId + " StatusCode=" + (int)(response.StatusCode));
                        failed = true;
                    }
                    else
                    {
                        using (var swB = File.Open(tmpFilepathB, FileMode.Create))
                        using (var stream = response.Content.ReadAsStreamAsync().Result)
                        using (var sr = new StreamReader(stream))
                        using (var iterator = new MissionResultReader(sr).GetEnumerator())
                        {
                            var tmpFilepath = System.IO.Path.Combine(folderPath, "tmp.csv");
                            StreamWriter sw = null;
                            CsvWriter writer = null;
                            bool wriitenHeader = false;
                            List<string> keyOrder = null;

                            try
                            {
                                while (iterator.MoveNext())
                                {
                                    var rec = iterator.Current;
                                    if (rec == null)
                                    {
                                        var iterator1 = (MissionResultReader.CtmRecordEnumerator)iterator;
                                        var id = iterator1.CurrentId;

                                        if (wriitenHeader)
                                        {
                                            writer.Dispose();
                                            writer = null;
                                            sw.Close();
                                            sw = null;

                                            if (id == ctmId)
                                            {
                                                var targetFilepath = System.IO.Path.Combine(folderPath, id + ".csv");
                                                File.Move(tmpFilepath, targetFilepath);
                                                break;
                                            }
                                            else
                                            {
                                                File.Delete(tmpFilepath);
                                            }
                                            wriitenHeader = false;
                                        }
                                        else
                                        {
                                            logger.Info(string.Format("Mission[{0}] -> CTM[{1} is empty.", missionId, id));
                                        }
                                    }
                                    else
                                    {
                                        // Header
                                        if (!wriitenHeader)
                                        {
                                            sw = new StreamWriter(tmpFilepath, false, new UTF8Encoding(false));
                                            writer = new CsvWriter(sw);
                                            keyOrder = new List<string>(rec.EL.Count);

                                            writer.WriteField(Keywords.RECEIVE_TIME);
                                            foreach (var el in rec.EL.Keys)
                                            {
                                                writer.WriteField(el);
                                                keyOrder.Add(el); // For writing records
                                            }
                                            writer.NextRecord();
                                            wriitenHeader = true;
                                        }

                                        // Record
                                        writer.WriteField<long>(rec.RT);
                                        foreach (var key in keyOrder)
                                        {
                                            var elem = rec.EL[key];
                                            if (elem != null)
                                            {
                                                if (elem.T == "100")
                                                {
                                                    writer.WriteField(elem.FN);
                                                    if (string.IsNullOrEmpty(elem.FN))
                                                    {
                                                        continue;
                                                    }
                                                    //AISBUL-31 sunyi 20181221 start
                                                    //jpg優先判定する
                                                    else if (Path.GetExtension(elem.FN).ToLower().Equals(".jpg"))
                                                    {
                                                        this.isNoJpg = false;
                                                    }
                                                    //AISBUL-31 sunyi 20181221 end
                                                    else if (!Path.GetExtension(elem.FN).ToLower().Equals(".csv"))
                                                    {
                                                        this.isNoBulky = false;
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        this.isNoBulky = false;
                                                        this.isNoCsvOnly = false;
                                                    }

                                                    if (key == elementId && elem.V != null)
                                                    {
                                                        byte[] bulky = Convert.FromBase64String(elem.V);
                                                        byte[] encodedBulky = bulky;
                                                        if (AisUtil.IsBomUtf8(bulky))
                                                        {
                                                            encodedBulky = AisUtil.RemoveBom(bulky);
                                                        }
                                                        else
                                                        {
                                                            Encoding encoding = AisUtil.GetCode(bulky);
                                                            if (encoding != null)
                                                            {
                                                                if (!encoding.Equals(Encoding.UTF8))
                                                                {
                                                                    encodedBulky = AisUtil.ConvertToWoUtf8Bytes(bulky, encoding);
                                                                }
                                                            }
                                                        }
                                                        swB.Write(encodedBulky, 0, encodedBulky.Length);
                                                    }
                                                }
                                                else
                                                {
                                                    writer.WriteField(elem.V);
                                                }
                                            }
                                            else
                                            {
                                                writer.WriteField(string.Empty);
                                            }
                                        }
                                        //AISBUL-31 sunyi 20181221 start
                                        //jpg優先判定する
                                        if (!this.isNoJpg)
                                        {
                                            this.isNoCsvOnly = true;
                                        }
                                        //AISBUL-31 sunyi 20181221 end
                                        writer.NextRecord();
                                    }
                                }
                            }
                            finally
                            {
                                if (writer != null)
                                {
                                    writer.Dispose();
                                }
                            }
                        }
                    }
                }
                catch (AggregateException e)
                {
                    Exception ex = e.InnerException;

                    if (ex is TaskCanceledException)
                    {
                        cancelled = true;
                        logger.Info("Cancelled by user.");

                        // Wang Issue MULTITEST-12 20190119 Start
                        bc.AfterCancel();
                        // Wang Issue MULTITEST-12 20190119 End
                    }
                    else
                    {
                        failed = true;
                    }
                }
                catch (Exception e)
                {
                    logger.Error("Failed to get Mission result: " + missionId, e);
                    failed = true;
                }
            });

            await task;

            if (cancelled || failed)
            {
                ClearDirectory(folderPath);
                folderPath = null;

                if (failed)
                {
                    FoaMessageBox.ShowError("AIS_E_045");
                    //var msg = "ミッション結果の取得に失敗しました。";
                    //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                return null;
            }

            return folderPath;
        }


        public const int WAITTIME_1 = 1000;
        public const int WAITTIME_2 = 1000;

        public const int ROWS_PER_PAGE = 100;

        public async Task<string> GetGripMissionResultCsvAsync(string missionId, long start, long end, CancellationToken cancelToken, string ctmId, string elementId)
        {
            string searchId = new GUID().ToString();
            string limit = "0";
            string lang = "ja";
            //string url = string.Format("{0}{1}?searchId={2}&missionId={3}&start={4}&end={5}&ctmId={6}&elementId={7}&limit={8}&lang={9}", CmsUrl.GetGripBaseUrl(), "search/ais2/bulkyTemplate/gripMission", searchId, missionId, start, end, ctmId, elementId, limit, lang);
            //logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() url:" + url);

            var folderPath = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission", Suid.NewID().ToString());
            var tmpFilepathB = System.IO.Path.Combine(folderPath, elementId + ".csv");
            Directory.CreateDirectory(folderPath);
            //HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(100) };
            bool failed = false;
            bool cancelled = false;
            logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() folderPath:" + folderPath + " tmpFilepathB:" + tmpFilepathB);

            string textData = null;
            string response1 = null;
            string responseWarning = null;
            await Task.Run(() =>
            {
                try
                {
                    //string url = string.Format(CmsUrl.Search.Mission.CreateCsv(), mission.Id, searchId, start, end, searchByInCondition, skipSameMainKey); logger.Debug(url);
                    string url = string.Format(CmsUrl.Search.Ais2.BulkyTemplate.GripMission.DoSearch(), searchId, missionId, start, end, ctmId, elementId, limit, lang); //logger.Debug(url);
                    logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() url:" + url);
                    var client = new FoaHttpClient();
                    var response = client.Get(url);
                    textData = response.Result;
                }
                catch (SocketException se)
                {
                    logger.Error("Search failed.", se);
                    response1 = se.Message;
                }
            });
            logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() textData:" + textData);
            if (cancelToken.IsCancellationRequested)
            {
                return textData;
            }

            string msg = DAC.Properties.Message.AIS_E_046;
            if (response1 != null)
            {
                //string msg = "検索データの取得に失敗しました。 " + response1;
                msg += response1;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            if (responseWarning != null)
            {
                msg = responseWarning;
                AisMessageBox.DisplayWarningMessageBoxWithCaption("NO DATA", msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "NO DATA", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

            if (string.IsNullOrEmpty(textData))
            {
                msg += GripDataResponseType.ErrorNotFound;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorNotFound;
            }



            List<SearchResultPaths> s = null;
            if (string.IsNullOrEmpty(textData) || "[]" == textData)
            {
                msg += GripDataResponseType.ErrorNotFound;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorNotFound;
            }

            try
            {
                s = JsonUtil.Deserialize<List<SearchResultPaths>>(textData);
            }
            catch
            {
                msg += GripDataResponseType.ErrorProcess;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorProcess;
            }
            if (null == s)
            {
                msg += GripDataResponseType.ErrorProcess;
                AisMessageBox.DisplayErrorMessageBox(msg);
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                return null;
                //return GripDataResponseType.ErrorProcess;
            }

            bool isNoData = true;
            await Task.Run(() =>
            {
                Thread.Sleep(WAITTIME_1);
                for (int i = 0; i < s.Count; i++)
                {
                    SearchResultPaths ps = s[i];
                    if (null == ps.paths || 0 == ps.paths.Count) continue;
                    for (int j = 0; j < ps.paths.Count; j++)
                    {
                        SearchResultPath p = ps.paths[j];
                        if (null == p.nodes || 2 > p.nodes.Count) continue;

                        string status = null;
                        int endId = i + 1;
                        int pathId = j + 1;
                        string url2 = string.Format(CmsUrl.Search.Ais2.BulkyTemplate.GripMission.GetStatus(), searchId, endId, pathId);
                        while (true)
                        {
                            Thread.Sleep(WAITTIME_2); logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2.Get: url2:" + url2);
                            var client2 = new FoaHttpClient();
                            var textData2 = client2.Get(url2); logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2.Get: textData2:" + textData2.Result);
                            var status2 = textData2.Result;
                            if (SearchResultBlock.STATUS_CREATING.Equals(status2)) continue;
                            status = status2;
                            break;
                        }
                        if (SearchResultBlock.STATUS_NORMAL.Equals(status))
                        {
                            p.existsData = true;
                            isNoData = false;
                        }
                    }
                }
            }); logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2.Get: isNoData:" + isNoData);
            if (isNoData)
            {
                return null;
                //return GripDataResponseType.NoFolder;
            }



            ProgramMission resultObject = new ProgramMission();
            Dictionary<long, ProgramMission.Ctms> ctmObjects = new Dictionary<long, ProgramMission.Ctms>();
            await Task.Run(() =>
            {
                int resultNo = 0;
                Thread.Sleep(WAITTIME_1);
                for (int i = 0; i < s.Count; i++)
                {
                    SearchResultPaths ps = s[i];
                    for (int j = 0; j < ps.paths.Count; j++)
                    {
                        SearchResultPath p = ps.paths[j];
                        if (!p.existsData) continue;

                        int endId = i + 1; int pathId = j + 1;
                        string url2 = string.Format(CmsUrl.Search.Ais2.BulkyTemplate.GripMission.DoDownload(), searchId, endId, pathId);

                        Thread.Sleep(WAITTIME_2);
                        //WebClient client2 = new WebClient();
                        WebClient client2 = AisUtil.getProxyWebClient();
                        logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2.Get: url:" + url2);
                        byte[] fileData = client2.DownloadData(new System.Uri(url2)); logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2.Get: fileData:" + fileData.Length);
						string stringData = Encoding.GetEncoding("Shift_JIS").GetString(fileData); logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() client2.Get: stringData:" + stringData);
                        JToken token = JToken.Parse(stringData);
                        if (string.IsNullOrEmpty(resultObject.id)) resultObject.id = (string)token["id"];
                        if (string.IsNullOrEmpty(resultObject.name)) resultObject.name = (string)token["name"];
                        JArray ctms = (JArray)token["ctms"];
                        foreach (JObject ctm in ctms)
                        {
                            long rt = (long)ctm["RT"];
                            JToken el = (JToken)ctm["EL"];
                            if (el[elementId] != null)
                            {
                                JToken element = el[elementId];
                                int t = (int)element["T"];
                                string fn = element["FN"] == null ? null : (string)element["FN"];
                                string v = element["V"] == null ? null : (string)element["V"];

                                if (!ctmObjects.ContainsKey(rt) && v != null)
                                {
                                    ProgramMission.Ctms ctmObject = new ProgramMission.Ctms();
                                    ctmObject.SetRT(rt);
                                    Dictionary<string, ProgramMission.Ctms.ELcls> elObjects = new Dictionary<string,ProgramMission.Ctms.ELcls>();
                                    ProgramMission.Ctms.ELcls elObject = new ProgramMission.Ctms.ELcls();
                                    elObject.SetT(t.ToString());
                                    elObject.SetFN(fn);
                                    elObject.V = v;
                                    elObjects[elementId] = elObject;
                                    ctmObject.SetEL(elObjects);
                                    ctmObjects[rt] = ctmObject;
                                }
                            }
                        }
                        client2.Dispose();
                        resultNo++;
                    }
                }
            });

            List<long> rts = new List<long>(ctmObjects.Keys);
            rts.Sort();
            rts.Reverse();

            List<ProgramMission.Ctms> ctmList = new List<ProgramMission.Ctms>();
            foreach (long rt in rts)
            {
                ctmList.Add(ctmObjects[rt]);
            }
            resultObject.SetCtms(ctmList);
            resultObject.num = ctmList.Count;
            //return pathString;

            var tmpFilepath = System.IO.Path.Combine(folderPath, "tmp.csv");
            StreamWriter sw = null;
            CsvWriter writer = null;
            bool wriitenHeader = false;
            List<string> keyOrder = null;

            using (var swB = File.Open(tmpFilepathB, FileMode.Create))
            {
                for (int i = 0; i < ctmList.Count; i++)
                {
                    var rec = ctmList[i];

                    if (i == 0)
                    {
                        sw = new StreamWriter(tmpFilepath, false, new UTF8Encoding(false));
                        writer = new CsvWriter(sw);
                        keyOrder = new List<string>(rec.EL.Count);

                        writer.WriteField(Keywords.RECEIVE_TIME);
                        foreach (var el in rec.EL.Keys)
                        {
                            writer.WriteField(el);
                            keyOrder.Add(el); // For writing records
                        }
                        writer.NextRecord();
                        wriitenHeader = true;
                    }

                    // Record
                    writer.WriteField<long>(rec.RT);
                    foreach (var key in keyOrder)
                    {
                        var elem = rec.EL[key];
                        //if (elem.T == "100")
                        if (int.Parse(elem.T) == FoaDatatype.BULKY)
                        {
                            writer.WriteField(elem.FN);
                            if (string.IsNullOrEmpty(elem.FN))
                            {
                                continue;
                            }
                            //AISBUL-31 sunyi 20181221 start
                            //jpg優先判定する
                            else if (Path.GetExtension(elem.FN).ToLower().Equals(".jpg"))
                            {
                                this.isNoJpg = false;
                            }
                            //AISBUL-31 sunyi 20181221 end
                            else if (!Path.GetExtension(elem.FN).ToLower().Equals(".csv"))
                            {
                                this.isNoBulky = false;
                                continue;
                            }
                            else
                            {
                                this.isNoBulky = false;
                                this.isNoCsvOnly = false;
                            }

                            if (key == elementId && elem.V != null)
                            {
								// 20190624 yakiyama start.
								byte[] bulky = null;
								if (elem.V.StartsWith("=HYPERLINK"))
								{
									Thread.Sleep(5);
									string[] vTexts = elem.V.Split('"');
									string hyperLink = vTexts[2];
									//logger.Debug(string.Join(", ", vTexts));
									//continue;
									WebClient clientHyperLink = AisUtil.getProxyWebClient();
									logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() clientHyperLink.Get: hyperLink:" + hyperLink);
									bulky = clientHyperLink.DownloadData(new System.Uri(hyperLink));
									logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() clientHyperLink.Get: fileData:" + bulky.Length);
								}
								else
								{
									bulky = Convert.FromBase64String(elem.V);
									logger.Debug("AIS.CtmData.BulkyDataRetriever.GetGripMissionResultCsvAsync() bulkyData:" + bulky.Length);
								}
								// 20190624 yakiyama end.
								byte[] encodedBulky = bulky;
                                if (AisUtil.IsBomUtf8(bulky))
                                {
                                    encodedBulky = AisUtil.RemoveBom(bulky);
                                }
                                else {
                                    Encoding encoding = AisUtil.GetCode(bulky);
                                    if (encoding != null)
                                    {
                                        if (!encoding.Equals(Encoding.UTF8))
                                        {
                                            encodedBulky = AisUtil.ConvertToWoUtf8Bytes(bulky, encoding);
                                        }
                                    }
                                }
                                swB.Write(encodedBulky, 0, encodedBulky.Length);
                            }
                        }
                        else
                        {
                            writer.WriteField(elem.V);
                        }
                    }
                    //AISBUL-31 sunyi 20181221 start
                    //jpg優先判定する
                    if (!this.isNoJpg)
                    {
                        this.isNoCsvOnly = true;
                    }
                    //AISBUL-31 sunyi 20181221 end
                    writer.NextRecord();
                }

                if (wriitenHeader)
                {
                    writer.Dispose();
                    writer = null;
                    sw.Close();
                    sw = null;

                    var targetFilepath = System.IO.Path.Combine(folderPath, ctmId + ".csv");
                    File.Move(tmpFilepath, targetFilepath);
                    wriitenHeader = false;
                }
                else
                {
                    logger.Info(string.Format("Mission[{0}] -> CTM[{1} is empty.", missionId, ctmId));
                }
            }

            if (cancelled || failed)
            {
                ClearDirectory(folderPath);
                folderPath = null;

                if (failed)
                {
                    //msg = "ミッション結果の取得に失敗しました。";
                    //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                    FoaMessageBox.ShowError("AIS_E_045");
                }
                return null;
            }

            return folderPath;
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(BulkyDataRetriever));
    }
}
