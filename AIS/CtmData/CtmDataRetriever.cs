using DAC.Model;
using DAC.Util;
using CsvHelper;
using FoaCore;
using log4net;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using DAC.Model.Util;
using FoaCore.Common.Util;
using FoaCore.Common;
using System.Windows.Threading;
using System.ComponentModel;
using FoaCore.Common.Control;
using DAC.View;
using System.Reflection;

namespace DAC.CtmData
{
    public class CtmDataRetriever
    {
        public const string FindBulkyUrlText = "mib/ctm/find/bulky";

        // Wang Issue MULTITEST-11 20190119 Start
        //public async Task<string> GetCtmDirectResultCsvAsync(List<CtmObject> ctmObjs, long start, long end, CancellationToken cToken)
        public async Task<string> GetCtmDirectResultCsvAsync(List<CtmObject> ctmObjs, long start, long end, CancellationToken cToken, BaseControl bc)
        // Wang Issue MULTITEST-11 20190119 End
        {
            AhInfo ahInfo = null;

            // ctmObjsの要素数が1以上であることは保障されている。
            var firstCtmId = ctmObjs[0].id.ToString();
            if (AisConf.Config.CmsVersion == "3.5")
            {
                ahInfo = await AisUtil.GetAhInfoAsync(firstCtmId);
            }

            var folderPath = System.IO.Path.Combine(AisConf.TmpFolderPath, "ctm", Suid.NewID().ToString());
            Directory.CreateDirectory(folderPath);
            string url = string.Format("{0}{1}?testing={2}&noBulky=true", CmsUrl.GetMibBaseUrl(), "mib/ctm/find", AisConf.Config.Testing);

            // CTMを検索する為にPOSTするJSONを生成
            List<string[]> postJsonList = new List<string[]>(ctmObjs.Count);
            foreach (var ctmObj in ctmObjs)
            {
                JToken postToken = JToken.Parse("{}");
                postToken["ctmId"] = ctmObj.id.ToString();
                postToken["start"] = start;
                postToken["end"] = end;
                if (AisConf.Config.CmsVersion == "3.5")
                {
                    postToken["mfHostName"] = ahInfo.Mf;
                    postToken["mfPort"] = ahInfo.MfPort;
                }
                string[] array = new string[] { postToken.ToString(), ctmObj.id.ToString() };
                postJsonList.Add(array);
            }

            //HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(100) };
            HttpClient client = AisUtil.getProxyHttpClient();
            bool cancelled = false;
            bool hasContent = false;
            List<string> failedCtmIds = new List<string>();
            List<Task> tasks = new List<Task>(postJsonList.Count);

            foreach (var a1 in postJsonList)
            {
                var req = new HttpRequestMessage(HttpMethod.Post, url);
                req.Content = new StringContent(a1[0]);

                var task = client.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cToken).ContinueWith(response0 =>
                {
                    try
                    {
                        if (cToken.IsCancellationRequested)
                        {
                            throw new TaskCanceledException();
                        }

                        var response = response0.Result;
                        if (!response.IsSuccessStatusCode)
                        {
                            logger.Error("Failed to get CTM Direct result: " + a1[1] + " StatusCode=" + (int)(response.StatusCode));
                            failedCtmIds.Add(a1[1]);
                        }
                        else
                        {
                            var tmpFilepath = System.IO.Path.Combine(folderPath, a1[1] + ".csv");

                            using (StreamWriter sw = new StreamWriter(tmpFilepath, false, new UTF8Encoding(false)))
                            using (CsvWriter writer = new CsvWriter(sw))
                            using (var stream = response.Content.ReadAsStreamAsync().Result)
                            using (var sr = new StreamReader(stream))
                            using (var iterator = new CtmDirectResultReader(sr).GetEnumerator(AisConf.Config.CmsVersion == "3.5"))
                            {
                                if (!iterator.MoveNext())
                                {
                                    logger.Info(string.Format("CTM[{0}] is empty.", a1[1]));
                                }
                                else
                                {
                                    var rec = iterator.Current;
                                    int r = 0;
                                    // header
                                    List<string> keyOrder = new List<string>(rec.EL.Count);

                                    writer.WriteField(Keywords.RECEIVE_TIME);
                                    foreach (var el in rec.EL.Keys)
                                    {
                                        writer.WriteField(el);
                                        keyOrder.Add(el); // For writing records
                                    }
                                   
                                    writer.NextRecord();
                                    do
                                    {
                                        if (cToken.IsCancellationRequested)
                                        {
                                            throw new TaskCanceledException();
                                        }

                                        rec = iterator.Current;

                                        // Records
                                        writer.WriteField<long>(rec.RT);
                                        foreach (var key in keyOrder)
                                        {
                                            var elem = rec.EL[key];

                                            //if (elem.T == "100")
                                            //CHU Yimin　linkBulky 2018.4.20　Start
                                            //Add the link bulky output
                                            if (FoaDatatype.isBulky(int.Parse(elem.T)))
                                            {
                                                int bulkySubtype = FoaDatatype.getBulkySubtype(int.Parse(elem.T));
                                                string base64;
                                                string elementvalue;
                                                if (elem.V != null)
                                                {
                                                    base64 = elem.V;
                                                    byte[] base64byte = System.Convert.FromBase64String(base64);
                                                    elementvalue = new StreamReader(new MemoryStream(base64byte)).ReadToEnd();
                                                    switch (bulkySubtype)
                                                    {
                                                        case FoaDatatype.LINKBULKY:
                                                            string hyperLink = string.Format("=HYPERLINK(\"" + elementvalue + "\",\"" + elem.FN + "\")");
                                                            elementvalue = hyperLink;//"\"" + hyperLink.Replace("\"", "\"\"") + "\"";
                                                            break;
                                                        case FoaDatatype.VIDEOFOA:
                                                            elementvalue = convertVideoFOALink(elementvalue, r, rec);
                                                            break;
                                                        case FoaDatatype.NORMALBULKY:
                                                        default:
                                                            elementvalue = elem.FN;
                                                            if (elementvalue.Contains("http") && elementvalue.Contains("kiridashi"))
                                                            {
                                                                elementvalue = convertVideoFOALink(elementvalue, r, rec);
                                                            }
                                                            else if (string.IsNullOrEmpty(elementvalue) == false)
                                                            {
                                                                string bulkyDownloadUrl = string.Format("{0}{1}?ctmId={2}&rt={3}&elementId={4}&filename={5}", CmsUrl.GetMibBaseUrl(), FindBulkyUrlText, a1[1], rec.RT, key, elem.FN);
                                                                elementvalue = convertNormalBulkyLink(bulkyDownloadUrl, r, rec);
                                                            }
                                                            else
                                                            {
                                                                elementvalue = string.Empty;
                                                            }
                                                            break;
                                                    }
                                                    //writer.WriteField(elem.FN);
                                                    
                                                }
                                                else
                                                {
                                                    elementvalue = elem.FN;
                                                    if (elementvalue.Contains("http") && elementvalue.Contains("kiridashi"))
                                                    {
                                                        elementvalue = convertVideoFOALink(elementvalue, r, rec);
                                                    }
                                                    else if (string.IsNullOrEmpty(elementvalue) == false)
                                                    {
                                                        string bulkyDownloadUrl = string.Format("{0}{1}?ctmId={2}&rt={3}&elementId={4}&filename={5}", CmsUrl.GetMibBaseUrl(), FindBulkyUrlText, a1[1], rec.RT, key, elem.FN);
                                                        elementvalue = convertNormalBulkyLink(bulkyDownloadUrl, r, rec);
                                                    }
                                                    else
                                                    {
                                                        elementvalue = string.Empty;
                                                    }
                                                }
                                                writer.WriteField(elementvalue);
                                            }
                                            //CHU Yimin　linkBulky 2018.4.20　End
                                            else
                                            {
                                                writer.WriteField(elem.V);
                                            }
                                        }
                                        writer.NextRecord(); // これなしだと直前のWriteFieldが書き込まれない。
                                        r++;
                                    } while (iterator.MoveNext());

                                    hasContent = true;
                                }
                            }
                        }

                        if (cToken.IsCancellationRequested)
                        {
                            throw new TaskCanceledException();
                        }
                    }
                    catch (AggregateException e)
                    {
                        Exception ex = e.InnerException;

                        if (ex is TaskCanceledException)
                        {
                            logger.Info("Cancelled by user.");
                            cancelled = true;

                            // Wang Issue MULTITEST-11 20190119 Start
                            bc.AfterCancel();
                            // Wang Issue MULTITEST-11 20190119 End
                        }
                        else
                        {
                            logger.Error("Failed to get CTM Direct result: " + a1[1], e);
                            failedCtmIds.Add(a1[1]);
                        }
                    }
                    catch (TaskCanceledException)
                    {
                        logger.Info("Cancelled by user.");
                        cancelled = true;

                        // Wang Issue MULTITEST-11 20190119 Start
                        bc.AfterCancel();
                        // Wang Issue MULTITEST-11 20190119 End
                    }
                    catch (Exception e)
                    {
                        logger.Error("Failed to get CTM Direct result: " + a1[1], e);
                        failedCtmIds.Add(a1[1]);
                    }
                });
                tasks.Add(task);
            }
            await Task.WhenAll(tasks);

            if (cancelled)
            {
                ClearDirectory(folderPath);
                folderPath = null;
            }
            else if (failedCtmIds.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (var failedId in failedCtmIds)
                {
                    foreach (var ctmObj in ctmObjs)
                    {
                        if (ctmObj.id.ToString() == failedId)
                        {
                            sb.Append(ctmObj.displayName.Get2(LoginInfo.GetInstance().GetCatalogLang(), true));
                            sb.Append(",");
                        }
                    }
                }

                if (sb.Length > 0)
                {
                    sb.Remove(sb.Length - 1, 1);
                }

                FoaMessageBox.ShowError("AIS_E_047", sb.ToString());
                //string msg = "CTMの取得に失敗しました。 " + sb.ToString();
                //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);

                if (!hasContent)
                {
                    ClearDirectory(folderPath);
                    folderPath = null;
                }
            }

            if (folderPath != null)
            {
                foreach (var file1 in Directory.GetFiles(folderPath, "*.csv"))
                {
                    var fileInfo1 = new FileInfo(file1);
                    if (fileInfo1.Length == 0)
                    {
                        fileInfo1.Delete();
                    }
                }
            }

            return folderPath;
        }

        private string convertVideoFOALink(string elementvalue, int r, ProgramMission.Ctms rec)
        {
            // No. 717 2018.06.04 Modified by Chu Yimin Start
            // VideoFOAリンクの中に、エラーコードが入っているケースもあります。
            // この時エラーコードを判断し、エラーメッセージに変換する
            if (string.IsNullOrEmpty(elementvalue))
            {
                return "";
            }
            int errorcode = 0;
            bool iserrorcode = int.TryParse(elementvalue, out errorcode);
            if (iserrorcode)
            {
                string msg;
                switch (errorcode)
                {
                    case 80:
                        msg = DAC.Properties.Message.AIS_E_058;
                        break;
                    case 200:
                        msg = DAC.Properties.Message.AIS_E_059;
                        break;
                    default:
                        msg = DAC.Properties.Message.AIS_E_060;
                        break;
                }
                return msg;                
            }
            // No. 717 2018.06.04 Modified by Chu Yimin End
            String parameter = elementvalue.Substring(elementvalue.LastIndexOf('?') + 1);
            String[] parameterpairs = parameter.Split('&');
            Dictionary<string, string> parameterMap = new Dictionary<string, string>();
            JToken token = JToken.Parse("{}");
            try
            {
                foreach (var parameterpair in parameterpairs)
                {
                    String[] parametervaluelist = parameterpair.Split('=');
                    String parameterName = parametervaluelist[0];
                    String parameterValue = parametervaluelist[1];
                    token[parameterName] = parameterValue;
                    parameterMap.Add(parameterName, parameterValue);
                    String[] parametervalues = parametervaluelist[1].Split(',');
                }

            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw;
            }
            DateTime dtAppTime = UnixTime.ToDateTime(rec.RT);
            string filePath;
            if (token["cameraId"] != null)
            {
                filePath = String.Format(@"{0:D5}_{1}_{2:00}{3:00}{4:00}{5:00}{6:00}{7:00}.mp4",
                    r + 1,
                    token["cameraId"].ToString(),
                    dtAppTime.Year,
                    dtAppTime.Month,
                    dtAppTime.Day,
                    dtAppTime.Hour,
                    dtAppTime.Minute,
                    dtAppTime.Second);
            }
            else
            {
                filePath = String.Format(@"{0:D5}_{1:00}{2:00}{3:00}{4:00}{5:00}{6:00}.mp4",
                    r + 1,
                    dtAppTime.Year,
                    dtAppTime.Month,
                    dtAppTime.Day,
                    dtAppTime.Hour,
                    dtAppTime.Minute,
                    dtAppTime.Second);
            }
            string hyperLink = string.Format("=HYPERLINK(\"" + elementvalue + "\",\"" + filePath + "\")");
            return hyperLink; 
        }

        private string convertNormalBulkyLink(string elementvalue, int r, ProgramMission.Ctms rec)
        {
            if (string.IsNullOrEmpty(elementvalue))
            {
                return "";
            }
            // No. 717 2018.06.04 Modified by Chu Yimin End
            String parameter = elementvalue.Substring(elementvalue.LastIndexOf('?') + 1);
            String[] parameterpairs = parameter.Split('&');
            Dictionary<string, string> parameterMap = new Dictionary<string, string>();
            JToken token = JToken.Parse("{}");
            try
            {
                foreach (var parameterpair in parameterpairs)
                {
                    String[] parametervaluelist = parameterpair.Split('=');
                    String parameterName = parametervaluelist[0];
                    String parameterValue = parametervaluelist[1];
                    token[parameterName] = parameterValue;
                    parameterMap.Add(parameterName, parameterValue);
                    String[] parametervalues = parametervaluelist[1].Split(',');
                }

            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw;
            }
            DateTime dtAppTime = UnixTime.ToDateTime(rec.RT);
            string filename = token["filename"].ToString();
            string filenameWithoutExtension = Path.GetFileNameWithoutExtension(filename);
            string extension = Path.GetExtension(filename);
            string filePath = String.Format(@"{0:D5}_{1}_{2:00}{3:00}{4:00}{5:00}{6:00}{7:00}{8}",
                r + 1,
                filenameWithoutExtension,
                dtAppTime.Year,
                dtAppTime.Month,
                dtAppTime.Day,
                dtAppTime.Hour,
                dtAppTime.Minute,
                dtAppTime.Second,
                extension);
            string hyperLink = string.Format("=HYPERLINK(\"" + elementvalue + "\",\"" + filePath + "\")");
            return hyperLink;
        }

        //Dac-86 sunyi 20181227 start
        //public async Task<string> GetProgramMissionResultCsvAsync(string missionId, long start, long end, CancellationToken cancelToken)
        // Wang Issue MULTITEST-11 20190119 Start
        //public async Task<string> GetProgramMissionResultCsvAsync(string missionId, long start, long end, CancellationToken cancelToken, EventHandler handler = null)
        public async Task<string> GetProgramMissionResultCsvAsync(string missionId, long start, long end, CancellationToken cancelToken, BaseControl bc, EventHandler handler = null, Dictionary<string, string> UUIDAndMissionIds = null)
        // Wang Issue MULTITEST-11 20190119 End
        //Dac-86 sunyi 20181227 end
        {
            string limit = "0";
            string lang = "ja";
            // string url = string.Format("{0}{1}?id={2}&start={3}&end={4}&limit={5}&lang={6}&testing={7}&noBulky=true", CmsUrl.GetMmsBaseUrl(), "mib/mission/pm", missionId, start, end, limit, lang, AisConf.Config.Testing);
            string url = string.Format("{0}{1}?id={2}&start={3}&end={4}&limit={5}&lang={6}&testing={7}", CmsUrl.GetMmsBaseUrl(), "mib/mission/pm", missionId, start, end, limit, lang, AisConf.Config.Testing);

            //AISMM-92 sunyi 20190227 start
            string uuid = Suid.NewID().ToString();
            if(UUIDAndMissionIds != null)
            {
                UUIDAndMissionIds.Add(uuid, missionId);
            }
            //AISMM-92 sunyi 20190227 end

            var folderPath = System.IO.Path.Combine(AisConf.TmpFolderPath, "mission", uuid);
            logger.Debug(MethodBase.GetCurrentMethod().Name + "() URL: " + url);
            logger.Debug(MethodBase.GetCurrentMethod().Name + "() folderPath: " + folderPath);

            Directory.CreateDirectory(folderPath);
            //HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(100) };
            HttpClient client = AisUtil.getProxyHttpClient();
            bool failed = false;
            bool cancelled = false;

            var task = client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead, cancelToken).ContinueWith(response0 =>
            {
                StreamWriter sw = null;
                CsvWriter writer = null;

                try
                {
                    if (cancelToken.IsCancellationRequested)
                    {
                        throw new TaskCanceledException();
                    }

                    var response = response0.Result;
                    if (!response.IsSuccessStatusCode)
                    {
                        logger.Error("Failed to get Mission result: " + missionId + " StatusCode=" + (int)(response.StatusCode));
                        failed = true;
                    }
                    else
                    {
                        int r = 0;
                        using (var stream = response.Content.ReadAsStreamAsync().Result)
                        using (var sr = new StreamReader(stream))
                        using (var iterator = new MissionResultReader(sr).GetEnumerator())
                        {
                            var tmpFilepath = System.IO.Path.Combine(folderPath, "tmp.csv");;
                            bool wriitenHeader = false;
                            List<string> keyOrder = null;
                            while (iterator.MoveNext())
                            {
                                if (cancelToken.IsCancellationRequested)
                                {
                                    throw new TaskCanceledException();
                                }

                                var rec = iterator.Current;
                                if (rec == null)
                                {
                                    var iterator1 = (MissionResultReader.CtmRecordEnumerator)iterator;
                                    var id = iterator1.CurrentId;

                                    if (wriitenHeader)
                                    {
                                        writer.Dispose();
                                        writer = null;
                                        sw.Close();
                                        sw = null;

                                        var targetFilepath = System.IO.Path.Combine(folderPath, id + ".csv");

                                        File.Move(tmpFilepath, targetFilepath);

                                        wriitenHeader = false;
                                    }
                                    else
                                    {
                                        logger.Info(string.Format("Mission[{0}] -> CTM[{1} is empty.", missionId, id));
                                    }
                                }
                                else
                                {
                                    var iterator1 = (MissionResultReader.CtmRecordEnumerator)iterator;
                                    var id = iterator1.CurrentId;

                                    // Header
                                    if (!wriitenHeader)
                                    {
                                        sw = new StreamWriter(tmpFilepath, false, new UTF8Encoding(false));
                                        writer = new CsvWriter(sw);
                                        keyOrder = new List<string>(rec.EL.Count);

                                        writer.WriteField(Keywords.RECEIVE_TIME);
                                        foreach (var el in rec.EL.Keys)
                                        {
                                            writer.WriteField(el);
                                            keyOrder.Add(el); // For writing records
                                        }
                                        writer.NextRecord();
                                        wriitenHeader = true;
                                    }
                                    // Record
                                    writer.WriteField<long>(rec.RT);
                                    foreach (var key in keyOrder)
                                    {
                                        if (cancelToken.IsCancellationRequested)
                                        {
                                            throw new TaskCanceledException();
                                        }

                                        var elem = rec.EL[key];
                                        if (elem == null)
                                        {
                                            writer.WriteField(string.Empty);
                                        }
                                        //CHU Yimin　linkBulky 2018.4.20　Start
                                        //Add the link bulky output
                                        else if (FoaDatatype.isBulky(int.Parse(elem.T)))
                                        {
                                            int bulkySubtype = FoaDatatype.getBulkySubtype(int.Parse(elem.T));
                                            string base64;
                                            string elementvalue;
                                            if (elem.V != null)
                                            {
                                                base64 = elem.V;
                                                byte[] base64byte = System.Convert.FromBase64String(base64);
                                                elementvalue = new StreamReader(new MemoryStream(base64byte)).ReadToEnd();
                                                switch (bulkySubtype)
                                                {
                                                    case FoaDatatype.LINKBULKY:
                                                        string hyperLink = string.Format("=HYPERLINK(\"" + elementvalue + "\",\"" + elem.FN + "\")");
                                                        elementvalue = hyperLink;//"\"" + hyperLink.Replace("\"", "\"\"") + "\"";
                                                        break;
                                                    case FoaDatatype.VIDEOFOA:
                                                        elementvalue = convertVideoFOALink(elementvalue, r, rec);
                                                        break;
                                                    case FoaDatatype.NORMALBULKY:
                                                        if (string.IsNullOrEmpty(elementvalue) == false)
                                                        {
                                                            string bulkyDownloadUrl = string.Format("{0}{1}?ctmId={2}&rt={3}&elementId={4}&filename={5}", CmsUrl.GetMibBaseUrl(), FindBulkyUrlText, id, rec.RT, key, elem.FN);
                                                            elementvalue = convertNormalBulkyLink(bulkyDownloadUrl, r, rec);
                                                        }
                                                        else
                                                        {
                                                            elementvalue = string.Empty;
                                                        }
                                                        break;
                                                    default:
                                                        elementvalue = elem.FN;
                                                        break;
                                                }
                                                //writer.WriteField(elem.FN);

                                            }
                                            else
                                            {
                                                elementvalue = elem.FN;
                                                if (elementvalue != null)
                                                {
                                                    if (elementvalue.Contains("http") && elementvalue.Contains("kiridashi"))
                                                    {
                                                        elementvalue = convertVideoFOALink(elementvalue, r, rec);
                                                    }
                                                    else if (string.IsNullOrEmpty(elementvalue) == false)
                                                    {
                                                        string bulkyDownloadUrl = string.Format("{0}{1}?ctmId={2}&rt={3}&elementId={4}&filename={5}", CmsUrl.GetMibBaseUrl(), FindBulkyUrlText, id, rec.RT, key, elem.FN);
                                                        elementvalue = convertNormalBulkyLink(bulkyDownloadUrl, r, rec);
                                                    }
                                                    else
                                                    {
                                                        elementvalue = string.Empty;
                                                    }
                                                }
                                                else
                                                {
                                                    if (string.IsNullOrEmpty(elementvalue) == false)
                                                    {
                                                        string bulkyDownloadUrl = string.Format("{0}{1}?ctmId={2}&rt={3}&elementId={4}&filename={5}", CmsUrl.GetMibBaseUrl(), FindBulkyUrlText, id, rec.RT, key, elem.FN);
                                                        elementvalue = convertNormalBulkyLink(bulkyDownloadUrl, r, rec);
                                                    }
                                                    else
                                                    {
                                                        elementvalue = string.Empty;
                                                    }
                                                }
                                            }
                                            writer.WriteField(elementvalue);
                                        }
                                        //CHU Yimin　linkBulky 2018.4.20　End
                                        /*
                                        else if (elem.T == "100")
                                        {
                                            writer.WriteField(elem.FN);
                                        }*/

                                        else
                                        {
                                            writer.WriteField(elem.V);
                                        }
                                    }
                                    r++;
                                    writer.NextRecord();
                                }
                            }
                        }
                    }

                    if (cancelToken.IsCancellationRequested)
                    {
                        throw new TaskCanceledException();
                    }
                }
                catch (AggregateException e)
                {
                    Exception ex = e.InnerException;

                    if (ex is TaskCanceledException)
                    {
                        cancelled = true;
                        logger.Info("Cancelled by user.");

                        // Wang Issue MULTITEST-11 20190119 Start
                        bc.AfterCancel();
                        // Wang Issue MULTITEST-11 20190119 End
                    }
                    else
                    {
                        logger.Error("Failed to get Mission result: " + missionId, ex);
                        failed = true;
                    }
                }
                catch (TaskCanceledException)
                {
                    cancelled = true;
                    logger.Info("Cancelled by user.");

                    // Wang Issue MULTITEST-11 20190119 Start
                    bc.AfterCancel();
                    // Wang Issue MULTITEST-11 20190119 End
                }
                catch (Exception e)
                {
                    logger.Error("Failed to get Mission result: " + missionId, e);
                    failed = true;
                }
                finally
                {
                    if (writer != null)
                    {
                        writer.Dispose();
                    }
                    if (sw != null)
                    {
                        sw.Close();
                    }
                }
            });

            await task;

            if (cancelled || failed)
            {
                ClearDirectory(folderPath);
                folderPath = null;

                if (failed)
                {
                    //var msg = string.Format("ミッション結果の取得に失敗しました。");
                    //Xceed.Wpf.Toolkit.MessageBox.Show(msg, "エラー", MessageBoxButton.OK, MessageBoxImage.Error);
                    //Dac-86 sunyi 20181227 start
                    //FoaMessageBox.ShowError("AIS_E_045");
                    if (handler != null)
                    {
                        handler(null, new EventArgsEx() { Key = "AIS_E_045" });
                    }
                    else
                    {
                        FoaMessageBox.ShowError("AIS_E_045");
                    }
                    //Dac-86 sunyi 20181227 end
                }
            }

            return folderPath;
        }

        protected static void ClearDirectory(string dirpath)
        {
            Task.Run(() =>
            {
                if (Directory.Exists(dirpath))
                {
                    Directory.Delete(dirpath, true);
                }
            });
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(CtmDataRetriever));
    }
}
