﻿using DAC.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace DAC.CtmData
{
    public class MissionResultReader : IEnumerable<DAC.Model.ProgramMission.Ctms>
    {
        private StreamReader streamReader;

        public MissionResultReader(StreamReader sr)
        {
            this.streamReader = sr;
        }


        public IEnumerator<ProgramMission.Ctms> GetEnumerator()
        {
            JsonTextReader jtr = new JsonTextReader(streamReader);
            return new CtmRecordEnumerator(jtr);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public List<ProgramMission> GetSummary()
        {
            var rtn = new List<ProgramMission>();

            JsonTextReader jtr = new JsonTextReader(streamReader);
            var iterator = new MissionResultReader.CtmRecordEnumerator(jtr, true);
            while (iterator.MoveNext()) {
                var pm = new ProgramMission();
                pm.id = iterator.CurrentId;
                pm.name = iterator.CurrentName;
                pm.num = iterator.CurrentNum;
                rtn.Add(pm);
            }

            return rtn;
        }


        public class CtmRecordEnumerator : IEnumerator<ProgramMission.Ctms>
        {
            private JsonTextReader reader;

            private string currentId;
            private string currentName;
            private int currentNum;

            private int state; // -1: 終わり, 0: 最初, 1: Array, 2: Object, 3: Switch, 10: ChildArray, 11: Child

            private bool forSommary;

            private ProgramMission.Ctms currentRec;


            public CtmRecordEnumerator(JsonTextReader jtr, bool summary = false)
            {
                this.reader = jtr;
                this.forSommary = summary;
            }

            public ProgramMission.Ctms Current
            {
                get { return currentRec; }
            }


            object System.Collections.IEnumerator.Current
            {
                get { return currentRec; }
            }

            public bool MoveNext()
            {
                while (true)
                {
                    switch (state)
                    {
                        case 0:
                            {
                                state = MoveNext0();
                                break;
                            }
                        case 1:
                            {
                                state = MoveNext1();
                                break;
                            }
                        case 2:
                            {
                                state = MoveNext2();
                                break;
                            }
                        case 3:
                            {
                                state = 1;
                                currentRec = null;
                                return true;
                            }
                        case 10:
                            {
                                state = MoveNext10();
                                break;
                            }
                        case 11:
                            {
                                state = MoveNext11();
                                return true;
                            }
                        default: // -1
                            {
                                Close();
                                return false;
                            }

                    }
                }
            }

            public int MoveNext0()
            {
                // Read StartArray
                reader.Read();
                if (reader.TokenType == JsonToken.StartArray)
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }

            public void Reset()
            {
                throw new NotImplementedException();
            }

            public string CurrentId
            {
                get { return currentId; }
            }

            public string CurrentName
            {
                get { return currentName; }
            }

            public int CurrentNum
            {
                get { return currentNum; }
            }

            private bool disposedValue = false;
            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }

            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposedValue)
                {
                    if (disposing)
                    {
                        // Dispose of managed resources.
                    }
                    currentRec = null;
                    Close();
                }

                this.disposedValue = true;
            }

            // Array
            private int MoveNext1()
            {
                reader.Read();
                if (reader.TokenType == JsonToken.StartObject)
                {
                    return 2;
                }
                else
                {
                    return -1;
                }
            }

            private int MoveNext2()
            {
                // PropertyName
                while (reader.Read())
                {
                    if (reader.TokenType == JsonToken.PropertyName)
                    {
                        string fieldName = (string)reader.Value;
                        //reader.Read();
                        switch (fieldName)
                        {
                            case "id":
                                {
                                    currentId = reader.ReadAsString();
                                    break;
                                }
                            case "name":
                                {
                                    currentName = reader.ReadAsString();
                                    break;
                                }
                            case "num":
                                {
                                    currentNum = reader.ReadAsInt32() ?? 0;
                                    break;
                                }
                            case "ctms":
                                {
                                    reader.Read(); // StartArray

                                    if (forSommary)
                                    {
                                        reader.Skip();
                                        break;
                                    }
                                    else
                                    {
                                        return 10;
                                    }
                                }
                            default:
                                {
                                    // ignore value
                                    reader.Read();
                                    break;
                                }
                        }
                    }
                    else
                    {
                        return 3;
                    }
                }
                return -1;
            }

            // Child Array
            private int MoveNext10()
            {
                reader.Read();
                if (reader.TokenType == JsonToken.StartObject)
                {
                    return 11;
                }
                return 2;
            }

            private int MoveNext11()
            {
                JsonSerializer serializer = new JsonSerializer();
                currentRec = serializer.Deserialize<ProgramMission.Ctms>(reader);
                return 10;
            }

            private void Close()
            {
                if (reader != null)
                {
                    reader.Close();
                    reader = null;
                }
            }
        }
    }
}
