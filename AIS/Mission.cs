﻿using FoaCore.Common.Entity;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace DAC.Model
{
    public class Mission: CmsEntity
    {
        public string AgentId { get; set; }

        public int MissionType { get; set; }
        public int ResultSavePeriod { get; set; }

        public int ResultType { get; set; }

        public long StartTime { get; set; }
        public long EndTime { get; set; }
        public int EndCount { get; set; }

        public bool IsRepeat { get; set; }
        public int RepeatCount { get; set; }
        public long RepeatEndTime { get; set; }

        public int AlertType { get; set; }
        public string ResultCopyDir { get; set; }
        public int ResultOutputType { get; set; }

        public string PostUrl { get; set; }
        public int Status { get; set; }

        public bool HasError { get; set; }

        public int Testing { get; set; }

        /// <summary>
        /// 表示名
        /// </summary>
        [JsonIgnore]
        public List<LangObject> DisplayNameList { get; set; }

        /// <summary>
        /// 表示名テキスト
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// MissionUserのコレクション
        /// </summary>
        public List<MissionUser> MissionUserList { get; set; }
    }
}
