﻿using FoaCore.Common.Entity;

namespace DAC.Model
{
    public class MissionUser : CmsEntity
    {
        public string MissionId { get; set; }
        public string UserId { get; set; }
    }
}
