﻿using FoaCore.Common;
using FoaCore.Common.Control;
using FoaCore.Common.Converter;
using FoaCore.Common.Entity;
using FoaCore.Common.Net;
using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using DAC.View;
using FoaCore.Common.Model;

namespace DAC.Model
{
    /// <summary>
    /// ミッションツリーコントローラ
    /// </summary>
    public class MissionTreeController : MibController
    {
        /// <summary>
        /// JsonTreeView
        /// </summary>
        public JsonTreeView tree;

        /// <summary>
        /// Newly added agent item ID
        /// </summary>
        private string newAgentItemId;

        /// <summary>
        /// ユーザ毎の表示オブジェクトのコレクション
        /// </summary>
        private Dictionary<string, List<LangObject>> userNames;
        // やむなし。持ちたくないが...
        // サーバ側でユーザ名をひっつける処理をしたくない。
        // MibMainにもったほうが楽だが、コントローラにMibMainの参照は持ってはいけない。

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="viewTree"></param>
        public MissionTreeController(JsonTreeView viewTree)
        {
            tree = viewTree;

            // Jsonキーの設定
            tree.IdKeyName = "id";
            tree.LabelKeyName = "name";
            tree.CollectionKeyName = "children";

            // アイコンの設定(前)
            tree.FrontIconFunction = FrontIconFunction;

            // アイコンの設定(後)
            tree.BackIconKeyName = "status";
            tree.BackIconMap = new Dictionary<string, string>(){
                { MissionStatus.New.ToString(), "transparent.png" },
                { MissionStatus.Running.ToString(), "mission-start.png" },
                { MissionStatus.Stopped.ToString(), "mission-stop.png" },
                { MissionStatus.Completed.ToString(), "mission-complete.png" },
                { MissionStatus.Making.ToString(), "mission-start.png" }
            };

            // ラベル
            tree.LabelFunction = LabelFunction;

            // ラベルの色
            //tree.ForegroundFunction = LabelForegroundFunction;
        }

        /// <summary>
        /// ユーザ名の表示オブジェクトをセットします。
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="names"></param>
        public void AddUserNames(string userId, List<LangObject> names)
        {
            if (userNames == null) userNames = new Dictionary<string, List<LangObject>>();
            userNames[userId] = names;
        }

        /// <summary>
        /// 前のアイコンファンクション
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private string FrontIconFunction(string arg)
        {
            JToken token = JToken.Parse(arg);
            string type = (string)token["t"];
            if (type == NodeType.Mission)
            {
                // ミッション
                int missionType = (int)token["missionType"];
                if (missionType == MissionType.Mobile)
                {
                    return "mission.png";
                }
                else if (missionType == MissionType.Ontime)
                {
                    return "mission-ontime.png";
                }
                else
                {
                    return "mission-pm.png";
                }
            }
            else if (type == NodeType.Mms)
            {
                return "mms.png";
            }
            else if (type == "myAgent")
            {
                return null;
            }
            else
            {
                // エージェント
                if (type == NodeType.Agent)
                {
                    return "user.png";
                }
                else
                {
                    return "users.png";
                }
            }
        }

        /// <summary>
        /// ツリーのラベルファンクション
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private string LabelFunction(string arg)
        {
            JToken token = JToken.Parse(arg);
            string type = (string)token["t"];
            string label = (string)token["name"];
            string displayName = null;
            string catalogLang = LoginInfo.GetInstance().GetCatalogLang();
            JArray displayNameArray = token["displayName"] == null ? new JArray() : string.IsNullOrEmpty((string)token["displayName"]) ? new JArray() : JArray.Parse((string)token["displayName"]);
            for (int i = 0; i < displayNameArray.Count; i++)
            {
                JToken dnToken = displayNameArray[i];
                if (catalogLang == (string)dnToken["lang"])
                {
                    displayName = (string)dnToken["text"];
                    break;
                }
            }

            if (type != NodeType.Mission && type != NodeType.Mms)
            {
                // エージェントの場合、作成ユーザと共有ユーザの名前を表示
                if (userNames == null)
                {
                    return (displayName != null ? displayName : label);
                }

                // 作成ユーザ
                string createdUserId = (string)token["uc"];

                // 共有ユーザ
                var users = token["agentUserList"];
                if (users == null) return (displayName != null ? displayName : label);
                List<AgentUser> agentUserList = JsonConvert.DeserializeObject<List<AgentUser>>(users.ToString());
                if (agentUserList == null)
                {
                    return (displayName != null ? displayName : label);
                }

                agentUserList.ForEach(au =>
                {
                });
            }
            else if (type == NodeType.Mission)
            {
                return (displayName != null ? displayName : label);
            }
            if (displayName != null)
            {
                label = displayName;
            }
            return label;
        }

        /// <summary>
        /// ツリーのラベルカラーファンクション
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        private Brush LabelForegroundFunction(string arg)
        {
            JToken token = JToken.Parse(arg);
            string type = (string)token["t"];
            if (type == NodeType.Mission)
            {
                bool hasError = (bool)token["hasError"];
                if (hasError)
                {
                    return Brushes.Red;
                }
            }

            return Brushes.Black;
        }

        public Action LoadCallback = null;

        /// <summary>
        /// ツリーのロード
        /// </summary>
        /// 
        //public void Load(bool isRetrievedAllMission = false, Action callback = null)
        public void Load(bool isRetrievedAllMission, bool isProgramMissionOnly, bool onInitWithE)
        {
            CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
            client.AddParam("withAgent", "true");
            client.AddParam("withoutontime", "true"); // ontime mission を入れない場合。

            if (isRetrievedAllMission) client.AddParam("all", "true"); // returns all mission from other users also

            UserInfo info = LoginInfo.GetInstance().GetLoginUserInfo();
            if (info.Role == (RoleEnum.ADMIN) || info.Role == (RoleEnum.ROOT)) client.AddParam("all", "true");

            client.CompleteHandler += resJson =>
            {
                // 取得したデータをツリー表示
                if (isRetrievedAllMission) // Retrieve all missions to be displayed on Mission Monitor page at Syb
                {
                    if (string.IsNullOrEmpty(resJson)) return;

                    HashSet<String> userIdsCreatedMission = new HashSet<string>();
                    JArray missionFolders = JArray.Parse(resJson);
                    foreach (JToken missionFolderToken in missionFolders)
                    {
                        if (missionFolderToken[MmsTvJsonKey.Children] != null)
                        {
                            if (missionFolderToken[MmsTvJsonKey.Children].ToString() != "[]")
                            {
                                JArray missions = (JArray)missionFolderToken[MmsTvJsonKey.Children];
                                foreach (JToken mission in missions)
                                {
                                    if (mission["uc"] != null)
                                    {
                                        //string userCreated = DisplayNameConverter.Convert2(userNames[mission["uc"].ToString()]);
                                        userIdsCreatedMission.Add(mission["uc"].ToString());
                                    }
                                }

                            }
                        }

                    }

                    string strItemSource = String.Empty;
                    int countUsers = userIdsCreatedMission.Count;
                    int index = 1;
                    foreach (string userId in userIdsCreatedMission)
                    {
                        string userName = "";
                        List<LangObject> lang;
                        if (userNames.TryGetValue(userId, out lang))
                        {
                            userName = DisplayNameConverter.Convert2(userNames[userId]);
                        }
                        else // The user is not exist, it might be already deleted?
                        {
                            continue; // skip
                        }
                        string myAgent = "My Agent " + userName;
                        string userAgent = userName + " Agent";
                        //string myAgent = "My Agent " + DisplayNameConverter.Convert2(userNames[userId]);
                        //string userAgent = DisplayNameConverter.Convert2(userNames[userId]) + " Agent";

                        //strItemSource += "{\"t\":\"myAgent\",\"name\":\"" + myAgent + "\",\"id\":\"" + userId + "\",\"children\":[{\"t\":\"mms\",\"name\":\""
                        //        + userAgent + "\",\"id\":\"" + (new GUID()).ToString() + "\",\"children\":" + (string.IsNullOrEmpty(resJson) ? "[]" : resJson) + "}]}";

                        JArray createdMissionFolder = new JArray();
                        foreach (JToken missionFolderToken in missionFolders)
                        {
                            string type = (string)missionFolderToken["t"];
                            if (type == NodeType.Agent || type == NodeType.AgentShare) // Mission Folder or Shared Mission Folder
                            {
                                if (missionFolderToken["uc"].ToString() == userId)
                                {
                                    if (missionFolderToken[MmsTvJsonKey.Children] != null)
                                    {
                                        if (missionFolderToken[MmsTvJsonKey.Children].ToString() != "[]")
                                        {
                                            createdMissionFolder.Add(missionFolderToken);
                                        }
                                    }
                                }
                            }
                        }

                        // Change the display name of Mission if the start and end time are not equal to 0 (Not Set)
                        foreach (JToken missionFolderToken in createdMissionFolder)
                        {
                            if (missionFolderToken[MmsTvJsonKey.Children] != null)
                            {
                                if (missionFolderToken[MmsTvJsonKey.Children].ToString() != "[]")
                                {
                                    JArray missions = (JArray)missionFolderToken[MmsTvJsonKey.Children];
                                    foreach (JToken mission in missions)
                                    {
                                        mission["originalName"] = mission["name"];
                                        mission["originalDisplayName"] = (mission["displayName"] != null ? mission["displayName"] : null);
                                        JArray displayNameJArray = (mission["displayName"] != null ? !string.IsNullOrEmpty((string)mission["displayName"]) ? JArray.Parse((string)mission["displayName"]) : new JArray() : new JArray());
                                        string additionalText = string.Empty;
                                        if (mission["missionType"].ToString() == "0") // mission type is mobile
                                        {
                                            if (mission["resultType"].ToString() == "1") // mission result type is 1 (Time)
                                            {
                                                if (mission["startTime"].ToString() != "0" && mission["endTime"].ToString() != "0")
                                                {
                                                    additionalText = " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ "
                                                        + UnixTime.ToDateTime((long)mission["endTime"]).ToString() + " ) ";
                                                    //mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ "
                                                    //    + UnixTime.ToDateTime((long)mission["endTime"]).ToString() + " ) ";
                                                }
                                                else if (mission["startTime"].ToString() != "0" && mission["endTime"].ToString() == "0")
                                                {
                                                    additionalText = " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                    //mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                }
                                                else if (mission["startTime"].ToString() == "0" && mission["endTime"].ToString() != "0")
                                                {
                                                    additionalText = " ( " + " ~ " + UnixTime.ToDateTime((long)mission["endTime"]).ToString() + " ) ";
                                                    //mission["name"] = mission["name"] + " ( " + " ~ " + UnixTime.ToDateTime((long)mission["endTime"]).ToString() + " ) ";
                                                }
                                            }
                                            else if (mission["resultType"].ToString() == "2") // mission result type is 2 (Count)
                                            {
                                                if (mission["startTime"].ToString() != "0") // only start time is available to set ...
                                                {
                                                    additionalText = " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                    //mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                }
                                            }
                                        }
                                        else if (mission["missionType"].ToString() == "2") // mission type is on-time
                                        {
                                            if (mission["startTime"].ToString() != "0") // only start time is available to set ...
                                            {
                                                additionalText = " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                //mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                            }
                                        }

                                        if (!string.IsNullOrEmpty(additionalText))
                                        {
                                            mission["name"] = mission["name"] + additionalText;
                                            if (displayNameJArray != null)
                                            {
                                                JArray displayNameJArray2 = new JArray();
                                                foreach (JToken displayNameTmp in displayNameJArray)
                                                {
                                                    JObject displayNameTmp2 = new JObject();
                                                    displayNameTmp2["lang"] = displayNameTmp["lang"];
                                                    displayNameTmp2["text"] = displayNameTmp["text"] + additionalText;
                                                    displayNameJArray2.Add(displayNameTmp2);
                                                }
                                                mission["displayName"] = displayNameJArray2.ToString();
                                            }
                                        }

                                    }
                                }
                            }
                        }

                        strItemSource += "{\"t\":\"myAgent\",\"name\":\"" + myAgent + "\",\"id\":\"" + userId + "\",\"children\":[{\"t\":\"mms\",\"name\":\""
                                + userAgent + "\",\"id\":\"" + (new GUID()).ToString() + "\",\"children\":" + createdMissionFolder.ToString() + "}]}";

                        if (index != countUsers) strItemSource += ",";

                        index++;
                    }

                    tree.JsonItemSource = "[" + strItemSource + "]";
                }
                else
                {
                    UserInfo _info = LoginInfo.GetInstance().GetLoginUserInfo();

                    string userAgent = DisplayNameConverter.Convert2(_info.DisplayNameObj) + " Agent";
                    //LoginInfo.GetInstance().GetLoginUserInfo().UserName + " Agent";

                    string userAgentId = LoginInfo.GetInstance().GetLoginUserInfo().UserId;

                    string jsonAddedUserAgent =
                        "{\"t\":\"mms\",\"name\":\"" + userAgent + "\",\"id\":\"" + userAgentId + "\",\"children\":" + (string.IsNullOrEmpty(resJson) ? "[]" : resJson) + "}";

                    tree.JsonItemSource = jsonAddedUserAgent;

                    // Expand the user agent item as default
                    TreeViewItem userAgentItem = tree.GetItem(userAgentId);
                    userAgentItem.IsExpanded = true;
                    setToolTip();
                }

                if (!string.IsNullOrEmpty(newAgentItemId))
                {
                    TreeViewItem agentItem = tree.GetItem(newAgentItemId);
                    if (agentItem != null)
                    {
                        agentItem.Focus();
                        agentItem.IsSelected = true;

                        // Re-set to null again
                        newAgentItemId = string.Empty;
                    }
                }

                if (LoadCallback != null)
                {
                    LoadCallback();
                }

                MainWindow mainWindow = Application.Current.MainWindow as MainWindow;

                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                //if (mainWindow.CurrentMode == ControlMode.Dac)
                if (mainWindow.CurrentMode == ControlMode.Dac || mainWindow.CurrentMode == ControlMode.MultiDac)
                // Wang Issue NO.687 2018/05/18 End
                {
                    UserControl_DACTemplate DACTemplateObj = (UserControl_DACTemplate)mainWindow.CurrentEditCtrl;
                    DACTemplateObj.AttachDragDropEventToTreeViewItems();
                }

                if (onInitWithE)
                {
                    if (mainWindow.CurrentMode == ControlMode.Spreadsheet)      // 集計表
                    {
                        mainWindow.CurrentEditCtrl.SetMission2();
                    }
                    // Wang Issue NO.687 2018/05/18 Start
                    // 1.階層DAC追加(オプションテンプレート表示統一)
                    // 2.DAC Excel メニュー整理
                    //else if (mainWindow.CurrentMode == ControlMode.Dac)         // DAC
                    else if (mainWindow.CurrentMode == ControlMode.Dac || mainWindow.CurrentMode == ControlMode.MultiDac)         // DAC
                    // Wang Issue NO.687 2018/05/18 End
                    {
                        mainWindow.CurrentEditCtrl.SetMission2();
                    }
                    else if (mainWindow.CtmDtailsCtrl.Online != null)           // オンライン
                    {
                        mainWindow.CtmDtailsCtrl.Online.setMission();
                    }
                    else if (mainWindow.CtmDtailsCtrl.BulkyEdit != null)        // Bulky
                    {
                        mainWindow.CtmDtailsCtrl.BulkyEdit.SetLoadedData(false, DataSourceType.MISSION);
                    }
                    else if (mainWindow.CtmDtailsCtrl.ContinuousGraph != null)  //連続グラフ(BULKY)
                    {
                        mainWindow.CtmDtailsCtrl.ContinuousGraph.SetLoadedData(false, DataSourceType.MISSION);
                    }
                    else if (mainWindow.CtmDtailsCtrl.StreamGraph != null)  //トルク波形
                    {
                        mainWindow.CtmDtailsCtrl.StreamGraph.SetLoadedData(false, DataSourceType.MISSION);
                    }
                    else if (mainWindow.CtmDtailsCtrl.LeadTime != null)         //リードタイム
                    {
                        mainWindow.CtmDtailsCtrl.LeadTime.SetMission();
                    }
                    else if (mainWindow.CtmDtailsCtrl.StockTime != null)        //在庫状況
                    {
                        mainWindow.CtmDtailsCtrl.StockTime.setMission();
                    }
                    else if (mainWindow.CtmDtailsCtrl.ChimneyChart != null)     //煙突チャート
                    {
                        mainWindow.CtmDtailsCtrl.ChimneyChart.setMission();
                    }
                    else if (mainWindow.CtmDtailsCtrl.Moment != null)           // レーダーチャート
                    {
                        mainWindow.CtmDtailsCtrl.Moment.SetMission();
                    }
                    else if (mainWindow.CtmDtailsCtrl.FIFO != null)              //先入・先出
                    {
                        mainWindow.CtmDtailsCtrl.FIFO.SetMission();
                    }
                    else if (mainWindow.CtmDtailsCtrl.Frequency != null)        //度数分布
                    {
                        mainWindow.CtmDtailsCtrl.Frequency.SetMission();
                    }
                    else if (mainWindow.CurrentMode == ControlMode.Comment)     // コメント
                    {
                        mainWindow.CtmDtailsCtrl.Comment.SetMission();
                    }
                    else if (mainWindow.CtmDtailsCtrl.StatusMonitor != null)    // ステータスモニター
                    {
                        mainWindow.CtmDtailsCtrl.StatusMonitor.SetMission2();
                    }
                    else if (mainWindow.CtmDtailsCtrl.MultiStatusMonitor != null)    // 階層ステータスモニター
                    {
                        mainWindow.CtmDtailsCtrl.MultiStatusMonitor.SetMission2();
                    }
                    else if (mainWindow.CtmDtailsCtrl.ProjectStatusMonitor != null)    // プロジェクトステータスモニター
                    {
                        mainWindow.CtmDtailsCtrl.ProjectStatusMonitor.SetMission2();
                    }
                    else if (mainWindow.CtmDtailsCtrl.Torque != null)    // トルク
                    {
                        mainWindow.CtmDtailsCtrl.Torque.SetMission2();
                    }
                    else if (mainWindow.CtmDtailsCtrl.PastStockTime != null)    // 過去在庫状況
                    {
                        mainWindow.CtmDtailsCtrl.PastStockTime.SetMission();
                    }
                    //AIS_WorkPlace
                    else if (mainWindow.CtmDtailsCtrl.WorkPlace != null)    // WorkPlace
                    {
                        mainWindow.CtmDtailsCtrl.WorkPlace.SetMission2();
                    }

                    mainWindow.CurrentEditCtrl.Initializing = false;
                }
            };
            client.Get(CmsUrl.GetMissionUrl());
        }
        public void Load2(bool displayTimeInfo, bool isRetrievedAllMission, List<string> ctmIds, Action callback)
        {
            if (ctmIds == null || ctmIds.Count == 0)
            {
                tree.JsonItemSource = "[{\"t\":\"mms\",\"name\":\""
                        + "(なし.)" + "\",\"id\":\"" + (new GUID()).ToString() + "\"}]";

                if (LoadCallback != null)
                {
                    LoadCallback();
                }

                if (callback != null)
                {
                    callback();
                }
                return;
            }

            CmsHttpClient client = new CmsHttpClient(Application.Current.MainWindow);
            client.AddParam("shrink", "true");
            foreach (string id in ctmIds)
            {
                client.AddParam("ctmId", id);
            }
            if (isRetrievedAllMission) client.AddParam("all", "true"); // returns all mission from other users also
            client.CompleteHandler += resJson =>
            {
                // 取得したデータをツリー表示
                if (isRetrievedAllMission) // Retrieve all missions to be displayed on Mission Monitor page at Syb
                {
                    if (string.IsNullOrEmpty(resJson) || resJson == "[]")
                    {
                        tree.JsonItemSource = "[{\"t\":\"mms\",\"name\":\""
                                + "(なし.)" + "\",\"id\":\"" + (new GUID()).ToString() + "\"}]";

                        if (LoadCallback != null)
                        {
                            LoadCallback();
                        }

                        if (callback != null)
                        {
                            callback();
                        }
                        return;
                    }

                    HashSet<String> userIdsCreatedMission = new HashSet<string>();
                    JArray missionFolders = JArray.Parse(resJson);
                    foreach (JToken missionFolderToken in missionFolders)
                    {
                        if (missionFolderToken[MmsTvJsonKey.Children] != null)
                        {
                            if (missionFolderToken[MmsTvJsonKey.Children].ToString() != "[]")
                            {
                                JArray missions = (JArray)missionFolderToken[MmsTvJsonKey.Children];
                                foreach (JToken mission in missions)
                                {
                                    if (mission["uc"] != null)
                                    {
                                        //string userCreated = DisplayNameConverter.Convert2(userNames[mission["uc"].ToString()]);
                                        userIdsCreatedMission.Add(mission["uc"].ToString());
                                    }
                                }

                            }
                        }

                    }

                    string strItemSource = String.Empty;
                    int countUsers = userIdsCreatedMission.Count;
                    int index = 1;
                    foreach (string userId in userIdsCreatedMission)
                    {
                        string userName = "";
                        List<LangObject> lang;
                        if (userNames.TryGetValue(userId, out lang))
                        {
                            userName = DisplayNameConverter.Convert2(userNames[userId]);
                        }
                        else // The user is not exist, it might be already deleted?
                        {
                            continue; // skip
                        }
                        string myAgent = "My Agent " + userName;
                        string userAgent = userName + " Agent";
                        //string myAgent = "My Agent " + DisplayNameConverter.Convert2(userNames[userId]);
                        //string userAgent = DisplayNameConverter.Convert2(userNames[userId]) + " Agent";

                        //strItemSource += "{\"t\":\"myAgent\",\"name\":\"" + myAgent + "\",\"id\":\"" + userId + "\",\"children\":[{\"t\":\"mms\",\"name\":\""
                        //        + userAgent + "\",\"id\":\"" + (new GUID()).ToString() + "\",\"children\":" + (string.IsNullOrEmpty(resJson) ? "[]" : resJson) + "}]}";

                        JArray createdMissionFolder = new JArray();
                        foreach (JToken missionFolderToken in missionFolders)
                        {
                            string type = (string)missionFolderToken["t"];
                            if (type == NodeType.Agent || type == NodeType.AgentShare) // Mission Folder or Shared Mission Folder
                            {
                                if (missionFolderToken["uc"].ToString() == userId)
                                {
                                    if (missionFolderToken[MmsTvJsonKey.Children] != null)
                                    {
                                        if (missionFolderToken[MmsTvJsonKey.Children].ToString() != "[]")
                                        {
                                            createdMissionFolder.Add(missionFolderToken);
                                        }
                                    }
                                }
                            }
                        }

                        // Change the display name of Mission if the start and end time are not equal to 0 (Not Set)
                        foreach (JToken missionFolderToken in createdMissionFolder)
                        {
                            if (missionFolderToken[MmsTvJsonKey.Children] != null)
                            {
                                if (missionFolderToken[MmsTvJsonKey.Children].ToString() != "[]")
                                {
                                    JArray missions = (JArray)missionFolderToken[MmsTvJsonKey.Children];
                                    foreach (JToken mission in missions)
                                    {
                                        mission["originalName"] = mission["name"];
                                        mission["originalDisplayName"] = (mission["displayName"] != null ? mission["displayName"] : null);
                                        JArray displayNameJArray = (mission["displayName"] != null ? !string.IsNullOrEmpty((string)mission["displayName"]) ? JArray.Parse((string)mission["displayName"]) : new JArray() : new JArray());
                                        if (displayTimeInfo)
                                        {
                                            string additionalText = string.Empty;
                                            if (mission["missionType"].ToString() == "0") // mission type is mobile
                                            {
                                                if (mission["resultType"].ToString() == "1") // mission result type is 1 (Time)
                                                {
                                                    if (mission["startTime"].ToString() != "0" && mission["endTime"].ToString() != "0")
                                                    {
                                                        additionalText = " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ "
                                                            + UnixTime.ToDateTime((long)mission["endTime"]).ToString() + " ) ";
                                                        //mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ "
                                                        //    + UnixTime.ToDateTime((long)mission["endTime"]).ToString() + " ) ";
                                                    }
                                                    else if (mission["startTime"].ToString() != "0" && mission["endTime"].ToString() == "0")
                                                    {
                                                        additionalText = " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                        //mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                    }
                                                    else if (mission["startTime"].ToString() == "0" && mission["endTime"].ToString() != "0")
                                                    {
                                                        additionalText = " ( " + " ~ " + UnixTime.ToDateTime((long)mission["endTime"]).ToString() + " ) ";
                                                        //mission["name"] = mission["name"] + " ( " + " ~ " + UnixTime.ToDateTime((long)mission["endTime"]).ToString() + " ) ";
                                                    }
                                                }
                                                else if (mission["resultType"].ToString() == "2") // mission result type is 2 (Count)
                                                {
                                                    if (mission["startTime"].ToString() != "0") // only start time is available to set ...
                                                    {
                                                        additionalText = " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                        //mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                    }
                                                }
                                            }
                                            else if (mission["missionType"].ToString() == "2") // mission type is on-time
                                            {
                                                // ontime ミッションはツリーに表示しない。

                                                if (mission["startTime"].ToString() != "0") // only start time is available to set ...
                                                {
                                                    additionalText = " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                    //mission["name"] = mission["name"] + " ( " + UnixTime.ToDateTime((long)mission["startTime"]).ToString() + " ~ " + " ) ";
                                                }
                                            }

                                            if (!string.IsNullOrEmpty(additionalText))
                                            {
                                                mission["name"] = mission["name"] + additionalText;
                                                if (displayNameJArray != null)
                                                {
                                                    JArray displayNameJArray2 = new JArray();
                                                    foreach (JToken displayNameTmp in displayNameJArray)
                                                    {
                                                        JObject displayNameTmp2 = new JObject();
                                                        displayNameTmp2["lang"] = displayNameTmp["lang"];
                                                        displayNameTmp2["text"] = displayNameTmp["text"] + additionalText;
                                                        displayNameJArray2.Add(displayNameTmp2);
                                                    }
                                                    mission["displayName"] = displayNameJArray2.ToString();
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (createdMissionFolder.Count > 0)
                        {
                            strItemSource += "{\"t\":\"mms\",\"name\":\""
                                + userAgent + "\",\"id\":\"" + (new GUID()).ToString() + "\",\"children\":" + createdMissionFolder.ToString() + "}";

                            if (index != countUsers) strItemSource += ",";
                        }
                        index++;
                    }
                    if (!string.IsNullOrEmpty(strItemSource))
                    {
                        tree.JsonItemSource = "[" + strItemSource + "]";
                    }
                    else
                    {
                        tree.JsonItemSource = "[{\"t\":\"mms\",\"name\":\""
                               + "(なし.)" + "\",\"id\":\"" + (new GUID()).ToString() + "\"}]";
                    }

                }
                else
                {
                    string userAgent =
                    LoginInfo.GetInstance().GetLoginUserInfo().UserName + " Agent";

                    string userAgentId = LoginInfo.GetInstance().GetLoginUserInfo().UserId;

                    string jsonAddedUserAgent =
                        "{\"t\":\"mms\",\"name\":\"" + userAgent + "\",\"id\":\"" + userAgentId + "\",\"children\":" + (string.IsNullOrEmpty(resJson) ? "[]" : resJson) + "}";

                    tree.JsonItemSource = jsonAddedUserAgent;

                    // Expand the user agent item as default
                    TreeViewItem userAgentItem = tree.GetItem(userAgentId);
                    userAgentItem.IsExpanded = true;
                    setToolTip();
                }

                if (!string.IsNullOrEmpty(newAgentItemId))
                {
                    TreeViewItem agentItem = tree.GetItem(newAgentItemId);
                    if (agentItem != null)
                    {
                        agentItem.Focus();
                        agentItem.IsSelected = true;

                        // Re-set to null again
                        newAgentItemId = string.Empty;
                    }
                }

                if (LoadCallback != null)
                {
                    LoadCallback();
                }

                if (callback != null)
                {
                    callback();
                }
            };
            client.Get(CmsUrl.GetMissionByCtmInUseUrl());
        }
        private void setToolTip()
        {
            JArray tvTopArray = JArray.Parse(tree.JsonItemSource);
            JToken token = tvTopArray[0];
            JToken topToken = token;
            JArray children = null;
            if (!string.IsNullOrEmpty(topToken[MmsTvJsonKey.Children].ToString()))
            {
                children = (JArray)topToken[MmsTvJsonKey.Children];
            }
            else
            {
                return;
            }

            if (children.Count > 0)
            {
                foreach (JToken childToken in children)
                {
                    string type = (string)childToken["t"];
                    if (type == NodeType.AgentShare) // Shared Mission Folder
                    {
                        string id = (string)childToken["id"];
                        var users = childToken["agentUserList"];
                        List<AgentUser> agentUserList = JsonConvert.DeserializeObject<List<AgentUser>>(users.ToString());
                        string agentUserLabel = "";
                        int count = agentUserList.Count;
                        int index = 1;
                        agentUserList.ForEach(au =>
                        {
                            if (!string.IsNullOrEmpty(au.UserId))
                            {
                                if (userNames.ContainsKey(au.UserId))
                                {
                                    if (index != count)
                                    {
                                        agentUserLabel += "\u2022 " + DisplayNameConverter.Convert2(userNames[au.UserId]) + "\n";
                                    }
                                    else
                                    {
                                        agentUserLabel += "\u2022 " + DisplayNameConverter.Convert2(userNames[au.UserId]);
                                    }
                                    index++;
                                }    
                            }
                            else
                            {
                                AgentUser autmp = au;
                            }
                        });

                        TreeViewItem tvItem = tree.GetItem(id);
                        ToolTip tooltip = new ToolTip { Content = agentUserLabel };
                        tooltip.FontSize = 14;
                        tooltip.Placement = System.Windows.Controls.Primitives.PlacementMode.Mouse;
                        tvItem.ToolTip = tooltip;
                    }
                }
            }
        }

        /// <summary>
        /// ミッションが選択されているか判定します。
        /// </summary>
        /// <returns></returns>
        public bool isMissionSelected()
        {
            string type = (string)tree.GetSelectedValueByKey("t");
            if (type == NodeType.Mission)
            {
                return true;
            }

            return false;
        }

        public bool isAgentUserSelected()
        {
            string type = (string)tree.GetSelectedValueByKey("t");
            if (type == NodeType.Mms)
            {
                return true;
            }

            return false;
        }
    }
}
