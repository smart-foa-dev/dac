﻿using FoaCore.Common.Entity;

namespace DAC.Model
{
    public class MissionCtmElement : CmsEntity
    {
        public string MissionCtmId { get; set; }
        public string ElementId { get; set; }
    }
}
