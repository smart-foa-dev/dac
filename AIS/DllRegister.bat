@ECHO OFF
rem コピー先のパス
SET DLL_PATH=c:\foa\dll

rem コピー・登録するDLLファイル
SET FOA_CORE_COM=FoaCoreCom.dll
SET GRIP_DATA_RETRIEVER=GripDataRetriever.dll
SET FOA_CONTAINER=FoaContainer.dll

rem 登録に必要なDLLファイル
SET NODA_TIME=NodaTime.dll
SET NEWTON_SOFT=Newtonsoft.Json.dll
SET CSV_HELP=CsvHelper.dll
SET FOA_CORE=FOA-Core.dll
SET SHARE_ZIP_LIB=ICSharpCode.SharpZipLib.dll

rem レジストリ操作コマンド
SET REG_CMD=C:\Windows\Microsoft.NET\Framework\v4.0.30319\Regasm.exe

rem BATファイルのディレクトリに移動
cd /d %~dp0

echo １．Excelの起動チェックを行います。
TASKLIST | FIND /I "excel" > NUL
IF NOT ERRORLEVEL 1 (
    ECHO excelが起動しています。 excelを終了してから再実行してください。
    goto exit1

)
echo Excelは起動していないので、次の処理に移動します。

echo ２．管理者として実行されているか確認します。

REM ▼管理者として実行されているか確認 START
for /f "tokens=1 delims=," %%i in ('whoami /groups /FO CSV /NH') do (
    if "%%~i"=="BUILTIN\Administrators" set ADMIN=yes
    if "%%~i"=="Mandatory Label\High Mandatory Level" set ELEVATED=yes
)

if "%ADMIN%" neq "yes" (
    echo このファイルは管理者権限での実行が必要です。 管理者として再実行してください。
   goto exit1
)
if "%ELEVATED%" neq "yes" (
    echo このファイルは管理者権限での実行が必要です。 管理者として再実行してください。
   goto exit1
)
REM ▲管理者として実行されているか確認 END


echo ３．DLL格納フォルダーを作成します。

rem ディレクトリの作成（あればエラーになる）
mkdir %DLL_PATH%

echo ４．DLLファイルをDLL格納フォルダーにコピーします。

rem ファイルのコピー
copy /B /Y %FOA_CONTAINER% %DLL_PATH%\%FOA_CONTAINER%
copy /B /Y %FOA_CORE_COM% %DLL_PATH%\%FOA_CORE_COM%
copy /B /Y %GRIP_DATA_RETRIEVER% %DLL_PATH%\%GRIP_DATA_RETRIEVER%
copy /B /Y %NODA_TIME% %DLL_PATH%\%NODA_TIME%
copy /B /Y %NEWTON_SOFT% %DLL_PATH%\%NEWTON_SOFT%
copy /B /Y %CSV_HELP% %DLL_PATH%\%CSV_HELP%
copy /B /Y %FOA_CORE% %DLL_PATH%\%FOA_CORE%
copy /B /Y %SHARE_ZIP_LIB% %DLL_PATH%\%SHARE_ZIP_LIB%

echo ５．レジストリの登録を一旦解除します。

rem レジストリの解除
%REG_CMD% /u %DLL_PATH%\%GRIP_DATA_RETRIEVER%  /codebase 
%REG_CMD% /u %DLL_PATH%\%FOA_CONTAINER%  /codebase 
%REG_CMD% /u %DLL_PATH%\%FOA_CORE_COM%  /codebase 

echo ６．レジストリの登録を行います。

rem レジストリの登録
%REG_CMD% /codebase /tlb %DLL_PATH%\%GRIP_DATA_RETRIEVER%
%REG_CMD% /codebase /tlb %DLL_PATH%\%FOA_CONTAINER%
%REG_CMD% /codebase /tlb %DLL_PATH%\%FOA_CORE_COM%

echo ７．全ての処理が終了しました。

:exit1
pause