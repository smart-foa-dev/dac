﻿using DAC.Model.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DAC.Model
{
    public class RegisteredTempleteItemInfo : ICloneable
    {
        public static readonly string IMAGE_PATH_FORMAT = "/DAC;component/Resources/Image/{0}";

        public static readonly SolidColorBrush GREEN_BRUSH = new SolidColorBrush(Colors.Green);
        public static readonly SolidColorBrush GRAY_BRUSH = new SolidColorBrush(Colors.Gray);

        public ControlMode Mode { get; set; }
        public string Name { get; set; }
        public string CreatedTime { get; set; }
        public string TemplateName { get; set; }
        public string FileName { get; set; }
        public bool IsOnline { get; set; }
        public string DacId { get; set; }
        private bool filePrepared;

        public string ImagePath
        {
            get
            {
                string thumbnail = Mode.GetThumbnailFilename();
                return string.Format(IMAGE_PATH_FORMAT, thumbnail);
            }
        }
        public RegisteredTempleteItemInfo()
        {

        }
        public RegisteredTempleteItemInfo(string dacId)
        {
            this.DacId = dacId;
        }
        public string CreateUserNameDisplay
        {
            get
            {
                string tooltipName = AExcel.ExcelUtil.GetToolTipDacFileName(this.FileName);
                string doc = string.Format("名称: {0}", tooltipName);
                return doc;
            }
        }

        public SolidColorBrush Border
        {
            get
            {
                if (!this.FileName.Equals(DAC.Properties.Resources.TEXT_MENU_MULTIDAC)
                    && !this.FileName.Equals(DAC.Properties.Resources.TEXT_MENU_MULTIDAC_SAMPLE))
                {
                    return GREEN_BRUSH;
                }
                else
                {
                    return GRAY_BRUSH;
                }
            }
        }
        public bool FilePrepared
        {
            get
            {
                return this.filePrepared;
            }
            set
            {
                this.filePrepared = value;
            }
        }
        public object Clone()
        {
            var x = new RegisteredTempleteItemInfo();
            x.Mode = this.Mode;
            x.Name = this.Name;
            x.CreatedTime = this.CreatedTime;
            x.TemplateName = this.TemplateName;
            x.FileName = this.FileName;
            x.IsOnline = this.IsOnline;
            x.DacId = this.DacId;
            return x;
        }

    }

    //// Wang Issue R2-143 20190329 Start
    ////public class FileObject : IFileObject
    //public class FileObject
    //// Wang Issue R2-143 20190329 End
    //{
    //    private string path;

    //    private string name;
    //    private string displayname;

    //    private string id;
    //    private string filename;
    //    private bool online;
    //    private bool filePrepared;



    //    public FileObject(string id, string filename, string path, bool? online, bool filePrepared)
    //    {
    //        this.filePrepared = filePrepared;
    //        this.id = id;
    //        this.filename = filename;
    //        this.path = path;
    //        this.name = System.IO.Path.GetFileNameWithoutExtension(filename);

    //        if (null != online)
    //        {
    //            this.online = online.Value;
    //        }
    //        else
    //        {
    //            this.online = false;
    //        }

    //        this.displayname = GetDisplayName(this.filename);
    //    }


    //    private string GetDisplayName(string filename)
    //    {
    //        return ParseFilename(filename)[1];
    //    }

    //    private string[] ParseFilename(string filename)
    //    {
    //        string[] rtn = new string[2];

    //        string[] xxx = filename.Split('_');
    //        if (xxx.Length < 2)
    //        {
    //            // Wang Issue R2-143 20190329 Start
    //            //rtn[0] = "Unknown";
    //            rtn[0] = "";
    //            // Wang Issue R2-143 20190329 End
    //            rtn[1] = System.IO.Path.GetFileNameWithoutExtension(filename);
    //            return rtn;
    //        }

    //        string kind = xxx[0];
    //        rtn[0] = kind;

    //        string dispName0 = filename.Substring(kind.Length + 1);
    //        string dispName = System.IO.Path.GetFileNameWithoutExtension(dispName0);
    //        rtn[1] = dispName;

    //        return rtn;
    //    }

        
    //}
}
