﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Model
{
    public class FileListData
    {
        public string FileName { get; set; }
        public string FilePath { get; set; }

        public SourceType sourceType { get; set; }
        public enum SourceType
        {
            LOCALFILE=0,
            URL=1,
            SHAREDFILE=2
        };
        public long UpdatedTime { get; set; }

        public override string ToString()
        {
            JObject jobj = new JObject();
            jobj.Add("fileName", FileName);
            jobj.Add("filePath", FilePath);
            jobj.Add("lastModifiedTime", UpdatedTime);
            jobj.Add("sourceType", sourceType.ToString());
            return jobj.ToString();
        }
    }
}
