﻿using FoaCore.Common;
using FoaCore.Common.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIS.Model
{
    class CtmClassObjectConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return typeof(CtmClassObject).IsAssignableFrom(objectType);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            JObject jObj = JObject.Load(reader);
            CtmClassObject ctmClass = new CtmClassObject();
            ctmClass.id = new GUID(jObj[MmsTvJsonKey.Id].ToString());
            ctmClass.cd = jObj[MmsTvJsonKey.Cd] != null ? jObj[MmsTvJsonKey.Cd].ToString() : null;
            ctmClass.revision = jObj[MmsTvJsonKey.Revision] != null ? (int)jObj[MmsTvJsonKey.Revision] : 0;
            ctmClass.lastUpdateTime = jObj["lastUpdateTime"] != null ? (long)jObj["lastUpdateTime"] : 0;
            ctmClass.updatedBy = jObj["updatedBy"] != null ? jObj["updatedBy"].ToString() : "";
            ctmClass.systemName = jObj["systemName"] != null ? jObj["systemName"].ToString() : "";
            ctmClass.domainId = new GUID(jObj[MmsTvJsonKey.DomainId].ToString());

            foreach (JToken dnToken in (JArray)jObj[MmsTvJsonKey.DisplayName])
            {
                ctmClass.displayName.Put(dnToken[MmsTvJsonKey.Lang].ToString(), dnToken[MmsTvJsonKey.Text].ToString());
            }
            foreach (JToken dnToken in (JArray)jObj[MmsTvJsonKey.Comment])
            {
                ctmClass.comment.Put(dnToken[MmsTvJsonKey.Lang].ToString(), dnToken[MmsTvJsonKey.Text].ToString());
            }

            if (jObj["ctmIds"] != null && jObj["ctmIds"] is JArray)
            {
                foreach (JToken childToken in (JArray)jObj["ctmIds"])
                {
                    string CtmId = childToken.ToString();
                    ctmClass.ctmIds.Add(CtmId);
                }
            }

            return ctmClass;
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            CtmClassObject ctmClass = (CtmClassObject)value;

            JObject token = new JObject();
            token[MmsTvJsonKey.Id] = ctmClass.id.ToString();
            //token[MmsTvJsonKey.Type] = NodeType.Ctm;
            token[MmsTvJsonKey.DisplayName] = JArray.Parse(JsonConvert.SerializeObject(ctmClass.displayName));
            token[MmsTvJsonKey.Comment] = JArray.Parse(JsonConvert.SerializeObject(ctmClass.comment));
            if (!string.IsNullOrEmpty(ctmClass.cd))
            {
                token[MmsTvJsonKey.Cd] = ctmClass.cd;
            }
            token[MmsTvJsonKey.Revision] = ctmClass.revision;
            token["lastUpdateTime"] = ctmClass.lastUpdateTime;
            token["updatedBy"] = ctmClass.updatedBy;
            token["systemName"] = ctmClass.systemName;
            token[MmsTvJsonKey.Status] = ctmClass.status;
            // token[MmsTvJsonKey.Status2] = ctm.status2;
            if (!string.IsNullOrEmpty(ctmClass.ctmClass))
            {
                token[MmsTvJsonKey.CtmClass] = ctmClass.ctmClass;
            }
            token[MmsTvJsonKey.UseCommentFile] = ctmClass.useCommentFile;
            token[MmsTvJsonKey.CommentFileVersion] = ctmClass.commentFileVersion;

            JArray ctmIds = new JArray();
            foreach (string ctmId in ctmClass.ctmIds)
            {
                JToken childToken = JToken.Parse(JsonConvert.SerializeObject(ctmId));
                ctmIds.Add(childToken);
            }
            if (ctmIds.Count > 0)
            {
                token["ctmIds"] = ctmIds;
            }
            writer.WriteRaw(token.ToString());
        }
    }
}
