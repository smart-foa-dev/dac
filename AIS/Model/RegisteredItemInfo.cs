﻿using DAC.Model.Util;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace DAC.Model
{
    public class RegisteredItemInfo : ICloneable
    {
        public static readonly string IMAGE_PATH_FORMAT = "/DAC;component/Resources/Image/{0}";

        public static readonly SolidColorBrush RED_BRUSH = new SolidColorBrush(Colors.Red);

        public ControlMode Mode { get; set; }
        public string Name { get; set; }
        public DateTime CreatedTimeDT { get; set; }
        public string CreatedTime { get; set; }
        public string TemplateName { get; set; }
        public string FileName { get; set; }
        public bool IsOnline { get; set; }
        public string DacId { get; set; }
        public string Info
        {
            get
            {
                return CreatedTime + " " + TemplateName + Environment.NewLine + Name;
            }
        }
        public string ImagePath
        {
            get
            {
                string thumbnail = Mode.GetThumbnailFilename();
                return string.Format(IMAGE_PATH_FORMAT, thumbnail);
            }
        }

        public string FilePath
        {
            get
            {
                //string filePath = Path.Combine(AisConf.RegistryFolderPath, this.FileName);
                string filePath = this.FileName;
                return filePath;
            }
        }

        public string MissionId { get; set; }
        public string CreateUserName { get; set; }
        public string CreateUserNameDisplay
        {
            get
            {
                string tooltipName = AExcel.ExcelUtil.GetToolTipDacFileName(this.FileName);
                //string doc = string.Format("名称: {0}", tooltipName) + Environment.NewLine + string.Format("ファイル作成者: {0}", this.CreateUserName);
                string doc = string.Format("名称: {0}", tooltipName);
                return doc;
            }
        }

        public bool CanEdit { get; set; }

        public SolidColorBrush Border
        {
            get
            {
                if (this.IsOnline)
                {
                    return RED_BRUSH;
                }
                else
                {
                    return null;
                }
            }
        }

        public object Clone()
        {
            var x = new RegisteredItemInfo();
            x.Mode = this.Mode;
            x.Name = this.Name;
            x.CreatedTime = this.CreatedTime;
            x.TemplateName = this.TemplateName;
            x.FileName = this.FileName;
            x.IsOnline = this.IsOnline;
            x.DacId = this.DacId;
            return x;
        }
    }
}
