﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAC.Model
{
    public class WorkPlaceSearchParam
    {
        public string workplaceId;
        public string displayName;
        public long start;
        public long end;
        public int elementInfoType;
        public List<string> missions;
    }
}
