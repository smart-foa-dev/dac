﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoaCore.Common.Util;
using log4net;
using DAC.View.Helpers;

namespace DAC.Model
{
    public class WebExceptionValue {
        public int index;
        public string value;
    }

    public class AisWebException
    {
        private static readonly ILog logger = LogManager.GetLogger(typeof(AisWebException));
        private static readonly Dictionary<string, string> msgmap = new Dictionary<string, string> {
            {"WorkPlaceNotFound", "AIS_E_064"},
            {"WorkPlaceHasNoMissions", "AIS_E_064"}
        };

        public string code;
        public List<WebExceptionValue> values;

        public void ShowMessage()
        {
            if (values != null && values.Count > 0)
            {
                WebExceptionValue value = values[0];
                string msgId = string.Empty;
                if (msgmap.TryGetValue(value.value, out msgId)) {
                    FoaMessageBox.ShowError(msgId);
                }
                else
                {
                    logger.Debug("Can't match message by id of " + value.value);
                    string msg = string.Format(DAC.Properties.Message.AIS_E_061, value.value);
                    AisMessageBox.DisplayErrorMessageBox(msg);
                }
            }
        }
    }
}
