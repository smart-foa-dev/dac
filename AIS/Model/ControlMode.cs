﻿using System;


namespace DAC.Model
{
    /// <summary>
    /// Main画面のモード
    /// </summary>
    public enum ControlMode
    {
        /// <summary>
        /// なし(一応)
        /// </summary>
        None = 0,

        /// <summary>
        /// 集計表
        /// </summary>
        Spreadsheet = 1,

        /// <summary>
        /// オンライン
        /// </summary>
        Online = 2,

        /// <summary>
        /// バルキー
        /// </summary>
        Bulky = 3,

        /// <summary>
        /// 緊急通報
        /// </summary>
        Emergency = 4,

        /// <summary>
        /// DAC
        /// </summary>
        Dac = 5,

        /// <summary>
        /// リードタイム分布
        /// </summary>
        LeadTime = 6,

        /// <summary>
        /// 煙突チャート
        /// </summary>
        ChimneyChart = 7,

        /// <summary>
        /// 在庫状況
        /// </summary>
        StockTime = 8,

        /// <summary>
        /// 先入/先出
        /// </summary>
        FIFO = 9,

        /// <summary>
        /// 度数分布
        /// </summary>
        Frequency = 10,

        /// <summary>
        /// 連続グラフ(Bulky)
        /// </summary>
        ContinuousGraph = 11,

        /// <summary>
        /// モーメント
        /// </summary>
        Moment = 12,

        /// <summary>
        /// コメント
        /// </summary>
        Comment = 13,

        /// <summary>
        /// ステータスモニター
        /// </summary>
        StatusMonitor = 14,

        /// <summary>
        /// 過去在庫状況グラフ
        /// </summary>
        PastStockTime = 15,

        /// <summary>
        /// 階層ステータスモニター
        /// </summary>
        MultiStatusMonitor = 16,

        /// <summary>
        /// プロジェクトステータスモニター
        /// </summary>
        ProjectStatusMonitor = 17,

        /// <summary>
        /// トルク
        /// </summary>
        Torque = 18,

        /// <summary>
        /// 波形トルク波形
        /// </summary>
        StreamGraph = 19,

        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        /// <summary>
        /// 階層DAC
        /// </summary>
        MultiDac = 20,
        // Wang Issue NO.687 2018/05/18 End

        // Wang Issue of quick graph 2018/06/01 Start
        /// <summary>
        /// クイックグラフ
        /// </summary>
        QuickGraph = 21,
        // Wang Issue of quick graph 2018/06/01 End

        /// <summary>
        /// WorkPlace
        /// </summary>
        WorkPlace = 22,

        DacTemplate = 23,
        DacSample = 24
    }

    static class ControlModeExt
    {
        private static readonly string FORMAT = "thumbnail-{0}.png";

        public static string GetThumbnailFilename(this ControlMode gender)
        {
            var name = Enum.GetName(typeof(ControlMode), gender);
            return string.Format(FORMAT, name.ToLower());
        }
    }

    class ControlModeUtil
    {
        public static ControlMode getControlModeFromString(string kind)
        {
            switch (kind)
            {
                case "集計表":
                    return ControlMode.Spreadsheet;
                case "オンライン":
                    return ControlMode.Online;
                case "Bulky":
                    return ControlMode.Bulky;
                case "リードタイム":
                    return ControlMode.LeadTime;
                case "煙突":
                    return ControlMode.ChimneyChart;
                case "在庫状況":
                    return ControlMode.StockTime;
                case "先入先出":
                    return ControlMode.FIFO;
                case "度数分布":
                    return ControlMode.Frequency;
                case "連続グラフB":
                    return ControlMode.ContinuousGraph;
                case "トルク波形":
                    return ControlMode.StreamGraph;
                case "モーメント":
                    return ControlMode.Moment;
                case "レーダーチャート":
                    return ControlMode.Moment;
                case "コメント":
                    return ControlMode.Comment;
                case "DAC" :
                    return ControlMode.Dac;
                case "ステータスモニター":
                    return ControlMode.StatusMonitor;
                case "過去在庫状況":
                    return ControlMode.PastStockTime;
                //foastudi AisAddin sunyi 2018/11/02
                //階層ステータスモニター ⇒ マルチモニタ
                case "マルチモニタ":
                    return ControlMode.MultiStatusMonitor;
                case "プロジェクトステータスモニター":
                    return ControlMode.ProjectStatusMonitor;
                case "トルク":
                    return ControlMode.Torque;
                // Wang Issue NO.687 2018/05/18 Start
                // 1.階層DAC追加(オプションテンプレート表示統一)
                // 2.DAC Excel メニュー整理
                case "階層DAC":
                    return ControlMode.MultiDac;
                // Wang Issue NO.687 2018/05/18 End
                // Wang Issue of quick graph 2018/06/01 Start
                case "クイックグラフ":
                    return ControlMode.QuickGraph;
                // Wang Issue of quick graph 2018/06/01 End
                //AIS_WorkPlace
                case "WorkPlace":
                    return ControlMode.WorkPlace;
                default:
                    return ControlMode.None;
            }
        }
    }
}
