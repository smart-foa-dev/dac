﻿using System;
using DAC.Properties;

namespace DAC.Model
{
    public class AisMenuItem : ICloneable
    {
        public static readonly string IMAGE_PATH_FORMAT = "/DAC;component/Resources/Image/{0}";


        private string name;
        private string name2;
       
        public AisMenuItem()
        {
            Mode = ControlMode.None;
            Opacity = 100;
        }
      
        public int Col { get; set; }
        public int Row { get; set; }
        public string Name
        {
            get
            {
#if MULTI_LANG
                return Resources.ResourceManager.GetString(name, Resources.Culture);
#else
                return name;
#endif
            }
            set
            {
                name = value;
            }
        }
        public string Name2
        {
            get
            {
                if (name2 == null)
                {
#if MULTI_LANG
                    return Name;
#else
                    return name;
#endif
                }
                else
                {
#if MULTI_LANG
                    return Resources.ResourceManager.GetString(name2, Resources.Culture);
#else
                    return name2;
#endif
                }

            }
            set
            {
                name2 = value;
            }
        }
#if MULTI_LANG
        public string NameId
        {
            get
            {
                return name;
            }
        }
        public string NameId2
        {
            get
            {
                return name2;
            }
        }
#else
        public string GetName2()
        {
            return name2;
        }
#endif

        public string ImagePath
        {
            get { 
                string thumbnail = Mode.GetThumbnailFilename();
                return string.Format(IMAGE_PATH_FORMAT, thumbnail);
            } 
        }

        public ControlMode Mode { get; set; }



       

        public double Opacity
        {
            get;
            set;
        }

        // Wang Issue NO.687 2018/05/18 Start
        // 1.階層DAC追加(オプションテンプレート表示統一)
        // 2.DAC Excel メニュー整理
        public string GroupKey
        {
            get;
            set;
        }
        // Wang Issue NO.687 2018/05/18 End

        public object Clone()
        {
            var ami = new AisMenuItem();
            ami.name = this.name;
            ami.name2 = this.name2;
            ami.Row = this.Row;
            ami.Col = this.Col;
            ami.Mode = this.Mode;
            ami.Opacity = this.Opacity;
            // Wang Issue NO.687 2018/05/18 Start
            // 1.階層DAC追加(オプションテンプレート表示統一)
            // 2.DAC Excel メニュー整理
            ami.GroupKey = this.GroupKey;
            // Wang Issue NO.687 2018/05/18 End
            return ami;
        }
    }
}
