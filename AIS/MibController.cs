﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    /// <summary>
    /// MIBの画面のコントローラの基本クラスです。
    /// </summary>
    public class MibController
    {
        /// <summary>
        /// ミッションの状態が変わった時のイベントハンドラ
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void MissionStatusChangeHandler(object sender, MissionStatusChangeEventArgs e);

        /// <summary>
        /// ミッションの状態が変わった時のイベント
        /// </summary>
        public event MissionStatusChangeHandler MissionStatusChange;

        /// <summary>
        /// ミッションの状態変更のイベントを送信します。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="mission"></param>
        protected void DispatchMissionStatusChangeEnvet(object sender, Mission mission)
        {
            // C#の言語使用で、イベントは自クラス内でしか実行出来ないので、
            // サブクラスからイベントを実行する場合には、ラップしたメソッドが必要。
            MissionStatusChangeEventArgs e = new MissionStatusChangeEventArgs(mission);
            MissionStatusChange(sender, e);
        }
    }

    /// <summary>
    /// ミッションの状態変更イベント
    /// </summary>
    public class MissionStatusChangeEventArgs : EventArgs
    {
        public MissionStatusChangeEventArgs(Mission mission) { this.Mission = mission; }
        public Mission Mission { get; private set; }
    }

}
