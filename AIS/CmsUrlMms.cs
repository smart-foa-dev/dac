﻿using System;

namespace DAC.Model
{
    /// <summary>
    /// CMS-UI(MMS)がアクセスする各アプリケーションのサーバのURLを取得します。
    /// </summary>
    static class CmsUrlMms
    {
        /// <summary>
        /// MMSサーバのAPIのベースURL
        /// </summary>
        /// <returns></returns>
        private static string GetMmsBaseUrl()
        {
            // return "http://" + ipaddr + ":" + port + "/cms/rest/";
            return CmsUrl.GetMmsBaseUrl();
        }

        /// <summary>
        /// 参照カタログツリーを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string RefDictUrl()
        {
            return GetMmsBaseUrl() + "refDict";
        }

        /// <summary>
        /// ドメインID指定で参照カタログツリーを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string RefDictByDomainIdUrl(string domainId)
        {
            return RefDictUrl() + "/domain?domainId=" + domainId;
        }

        /// <summary>
        /// API (/cms/rest/refDic/refNode)
        /// </summary>
        /// <returns></returns>
        public static string RefDictRefNode()
        {
            return RefDictUrl() + "/refNode";
        }

        /// <summary>
        /// API (/cms/rest/refDic/refNode/root)
        /// </summary>
        /// <returns></returns>
        public static string RefDictRefNodeRoot()
        {
            return RefDictRefNode() + "/root";
        }

        /// <summary>
        /// API (/cms/rest/refDic/name)
        /// </summary>
        /// <returns></returns>
        public static string RefDictName()
        {
            return RefDictUrl() + "/name";
        }

        public static string RootRefDictName()
        {
            return RefDictUrl() + "/rootRefDict/name";
        }

        public static class RefDict
        {
            public static string Base()
            {
                return GetMmsBaseUrl() + "refDict";
            }

            public static string Local()
            {
                return Base() + "/local";
            }
        }

        /// <summary>
        /// 管理カタログツリーを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string AdminDicTreeUrl()
        {
            return GetMmsBaseUrl() + "adminDict";
        }

        /// <summary>
        /// デバイス管理カタログツリーを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string DeviceAdminDicTreeUrl()
        {
            return GetMmsBaseUrl() + "adminDict/deviceDict";
        }

        /// <summary>
        /// API (/cms/rest/adminDic/ah)
        /// </summary>
        /// <returns></returns>
        public static string AdminDictAh()
        {
            return AdminDicTreeUrl() + "/ah";
        }

        /// <summary>
        /// API (/cms/rest/adminDic/ah) - MMS直下Ahノード・DELETE用
        /// </summary>
        /// <returns></returns>
        public static string AdminDictAhDelete(String id, Boolean useTx = false)
        {
            String url = AdminDictAh() + "?id=" + id;
            if (useTx)
            {
                url += "&useTx=" + useTx.ToString().ToLower();
            }

            return url;
        }

        /// <summary>
        /// API (/cms/rest/adminDic/name)
        /// </summary>
        /// <returns></returns>
        public static string AdminDictName()
        {
            return AdminDicTreeUrl() + "/name";
        }

        public static class AdminDict
        {
            public static string Base()
            {
                return GetMmsBaseUrl() + "adminDict";
            }
            public static string Device()
            {
                return Base() + "/device";
            }

            public static string DeviceSequence()
            {
                return Base() + "/device/sequence";
            }
        }

        public static class Ctm
        {
            public static string Base()
            {
                return GetMmsBaseUrl() + "ctm";
            }
            public static string List()
            {
                return Base() + "/list";
            }
            public static string Exist()
            {
                return Base() + "/exist";
            }
            public static string RsByBlock()
            {
                return Base() + "/rsByBlock";
            }
            public static string RsByTag()
            {
                return Base() + "/rsByTag";
            }
            public static string Approve()
            {
                return Base() + "/approve";
            }
            public static string Revert()
            {
                return Base() + "/revert";
            }
            public static class Comment
            {
                public static string Download()
                {
                    return Base() + "/sybMemo/download";
                }
                public static string Upload()
                {
                    return Base() + "/sybMemo/upload";
                }
                public static string Download2()
                {
                    return Base() + "/comment/download";
                }
                public static string Upload2()
                {
                    return Base() + "/comment/upload";
                }
            }
            public static string Search()
            {
                return Base() + "/myDomain/filter";
            }
        }

        public static string CurrentSeqNoUrl()
        {
            return GetMmsBaseUrl() + "change/currentSeqNo";
        }

        public static string ChangeHistory()
        {
            return GetMmsBaseUrl() + "change/changeHistory";
        }

        /// <summary>
        /// Txを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string GetTx()
        {
            return GetMmsBaseUrl() + "tx";
        }

        /// <summary>
        /// TxBeginを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string GetTxBegin()
        {
            return GetTx() + "/begin";
        }

        /// <summary>
        /// TxCommitを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string GetTxCommit()
        {
            return GetTx() + "/commit";
        }

        public static class EditLock
        {
            public static string Base()
            {
                return GetMmsBaseUrl() + "editLock";
            }

            public static string RefDict()
            {
                return Base() + "/refDict";
            }

            public static class AdminDict
            {
                public static string Base()
                {
                    return CmsUrlMms.EditLock.Base() + "/adminDict";
                }

                public static string User()
                {
                    return Base() + "/user3";
                }
            }

            public static class Device
            {
                public static string Base()
                {
                    return CmsUrlMms.EditLock.Base() + "/device";
                }

                public static string Get()
                {
                    return Base() + "/get";
                }

                public static string Release()
                {
                    return Base() + "/release";
                }

                public static string User()
                {
                    return Base() + "/user";
                }

                public static string List()
                {
                    return Base() + "/list";
                }
            }
        }

        /// <summary>
        /// EditLock/RefDict/Get2を取得するURL
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="clientId"></param>
        /// <param name="refNodeId"></param>
        /// <returns></returns>
        public static string Get2EditLockRefDict(string userId, string clientId, string refNodeId)
        {
            return EditLock.RefDict() + "/get2?userId=" + userId + "&clientId=" + clientId + "&refNodeId=" + refNodeId;
        }

        /// <summary>
        /// EditLock/RefDict/Release2を取得するURL
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public static string Release2EditLockRefDict(string userId, string clientId)
        {
            return EditLock.RefDict() + "/release2?userId=" + userId + "&clientId=" + clientId;
        }

        /// <summary>
        /// EditLock/RefDict/User2を取得するURL
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string User2EditLockRefDict(string userId, string clientId)
        {
            return EditLock.RefDict() + "/user2?userId=" + userId + "&clientId=" + clientId;
        }

        /// <summary>
        /// EditLock/RefDict/Listを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string ListEditLockRefDict()
        {
            return EditLock.RefDict() + "/list";
        }

        /// <summary>
        /// EditLock/AdminDict/Get2を取得するURL
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="clientId"></param>
        /// <param name="ahId"></param>
        /// <returns></returns>
        public static string Get2EditLockAdminDict(string userId, string clientId, string nodeId)
        {
            return EditLock.AdminDict.Base() + "/get3?userId=" + userId + "&clientId=" + clientId + "&nodeId=" + nodeId;
        }

        /// <summary>
        /// EditLock/AdminDict/Release2を取得するURL
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public static string Release2EditLockAdminDict(string userId, string clientId, string nodeId)
        {
            return EditLock.AdminDict.Base() + "/release3?userId=" + userId + "&clientId=" + clientId + "&nodeId=" + nodeId;
        }
        /// <summary>
        /// EditLock/AdminDict/Userを取得するURL
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static string UserEditLockAdminDict(string userId, string clientId)
        {
            return EditLock.AdminDict.Base() + "/user3?userId=" + userId + "&clientId=" + clientId;
        }

        /// <summary>
        /// EditLock/AdminDict/Listを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string ListEditLockAdminDict()
        {
            return EditLock.AdminDict.Base() + "/list";
        }

        /// <summary>
        /// Domainを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string GetDomain()
        {
            return GetMmsBaseUrl() + "domain";
        }

        /// <summary>
        /// AllDomainを取得するURL
        /// </summary>
        /// <returns></returns>
        public static string GetAllDomain()
        {
            return GetDomain() + "/all";
        }

        /// <summary>
        /// DomainMaskをPUTするURL (編集していない他のRefDictも込み)
        /// </summary>
        /// <returns></returns>
        public static string DomainMask()
        {
            return GetMmsBaseUrl() + "mask/domain";
        }

        /// <summary>
        /// DeviceBlockにアクセス(GET/POST/PUT/DELETE)するURL
        /// </summary>
        /// <returns></returns>
        public static string DeviceBlock()
        {
            return GetMmsBaseUrl() + "block";
        }

        /// <summary>
        /// DeviceBlock Listにアクセス(GET/POST/PUT/DELETE)するURL
        /// </summary>
        /// <returns></returns>
        public static string DeviceBlockList()
        {
            return DeviceBlock() + "/list";
        }

        public static string DeviceBlockPlcSetting()
        {
            return DeviceBlock() + "/plc/setting";
        }

        public static class Mapping
        {
            public static string Base()
            {
                return GetMmsBaseUrl() + "mapping";
            }

            public static string List()
            {
                return Base() + "/list2";
            }

            public static class P2dll
            {
                public static string Base()
                {
                    return Mapping.Base() + "/p2dll";
                }

                public static string Upload()
                {
                    return Base() + "/upload";
                }

                public static string Download()
                {
                    return Base() + "/download";
                }
            }
        }

        public static class CgGontrol
        {
            public static string Base() 
            {
                return GetMmsBaseUrl() + "cgCtrl";
            }

            public static class Block
            {
                public static string Base()
                {
                    return CmsUrlMms.CgGontrol.Base() + "/block";
                }
                public static string Start()
                {
                    return Base() + "/start";
                }
                public static string Stop()
                {
                    return Base() + "/stop";
                }
                public static string Status()
                {
                    return Base() + "/status";
                }
                public static string Status2()
                {
                    return Base() + "/status2";
                }
                public static string Ping()
                {
                    return Base() + "/ping";
                }
                public static string TestOn()
                {
                    return Base() + "/testOn";
                }
                public static string TestOn2()
                {
                    return Base() + "/testOn2";
                }
                public static string TestPut()
                {
                    // updated one
                    return Base() + "/testPut2";
                }
                public static string TestPutOff()
                {
                    return Base() + "/testPutOff";
                }
                public static string TestPutStatus()
                {
                    return Base() + "/testPutStatus";
                }
                public static string TestOff()
                {
                    return Base() + "/testOff";
                }
                public static string TestFile()
                {
                    return Base() + "/test/file";
                }
            }

            public static class Device
            {
                public static string Base()
                {
                    return CmsUrlMms.CgGontrol.Base() + "/device";
                }
                public static string Start()
                {
                    return Base() + "/start";
                }
                public static string Stop()
                {
                    return Base() + "/stop";
                }
                public static string Ping()
                {
                    return Base() + "/ping";
                }
            }

            public static class Cg
            {
                public static string Base()
                {
                    return CmsUrlMms.CgGontrol.Base() + "/cg";
                }
                public static string Startup()
                {
                    return Base() + "/startup";
                }
                public static string Terminate()
                {
                    return Base() + "/terminate";
                }
                public static string Start()
                {
                    return Base() + "/start";
                }
                public static string Stop()
                {
                    return Base() + "/stop";
                }
            }

            public static class Ctm
            {
                public static string Base()
                {
                    return CmsUrlMms.CgGontrol.Base() + "/ctm";
                }
                public static string Start()
                {
                    return Base() + "/start";
                }
                public static string Stop()
                {
                    return Base() + "/stop";
                }
                public static string Status()
                {
                    return Base() + "/status";
                }
                public static string StatusList()
                {
                    return Base() + "/statusList";
                }
                public static string Runing()
                {
                    return Base() + "/runing";
                }
            }

            public static class Send
            {
                public static string Base()
                {
                    return CmsUrlMms.CgGontrol.Base() + "/send";
                }
                public static string Start()
                {
                    return Base() + "/start";
                }
                public static string Stop()
                {
                    return Base() + "/stop";
                }
                public static string Status()
                {
                    return Base() + "/status";
                }
            }
        }

        /// <summary>
        /// MIBサーバのAPIのベースURL
        /// </summary>
        /// <returns></returns>
        private static string GetMibBaseUrl()
        {
            return CmsUrl.GetMibBaseUrl();
        }

        /// <summary>
        /// ミッションでCTMが使用されているかチェックするアクセスURL
        /// </summary>
        /// <returns></returns>
        public static string InUseCtmMissionMib()
        {
            return GetMibBaseUrl() + "mib/mission/ctm/inUse";
        }
    }
}
