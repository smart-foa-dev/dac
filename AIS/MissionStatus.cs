﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    /// <summary>
    /// ミッションの状態
    /// </summary>
    public class MissionStatus
    {
        public const int New = 0;
        public const int Running = 1;
        public const int Stopped = 2;
        public const int Completed = 3;
        public const int Making = 9;
    }
}
