﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DAC.Model
{
    public sealed class CtmTraverser : IEnumerable<CtmNode>
    {
        private CtmNode root;

        public CtmTraverser(CtmNode node)
        {
            root = node;
        }
        public IEnumerator<CtmNode> GetEnumerator()
        {
            return new CtmNodeIterator(root);
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private class CtmNodeIterator : IEnumerator<CtmNode>
        {
            private Stack<CtmNode> stack = new Stack<CtmNode>();
            private CtmNode currentValue;
            private CtmNode root;

            public CtmNodeIterator(CtmNode node)
            {
                root = node;
                Reset();
            }

            public CtmNode Current
            {
                get
                {
                    if (currentValue == null)
                    {
                        throw new InvalidOperationException("No more node.");
                    }
                    return currentValue;
                }
            }

            public void Dispose()
            {

            }

            object System.Collections.IEnumerator.Current
            {
                get { throw new NotImplementedException(); }
            }

            public bool MoveNext()
            {
                if (stack.Count == 0)
                {
                    currentValue = null;
                    return false;
                }

                var node = stack.Pop();

                if (node.IsCtm())
                {
                    pushAll(((CtmObject)node).children);
                }
                else
                {
                    CtmChildNode nodeC = (CtmChildNode)node;
                    if (!nodeC.IsElement())
                    {
                        pushAll(((CtmGroup)node).children);
                    }
                }

                currentValue = node;
                return true;
            }


            public void Reset()
            {
                currentValue = null;
                stack.Clear();
                stack.Push(root);
            }

            private void pushAll(List<CtmChildNode> children)
            {
                for (int i = children.Count - 1; i >= 0; i--)
                {
                    stack.Push((CtmNode)children[i]);
                }
            }
        }
    }
}
