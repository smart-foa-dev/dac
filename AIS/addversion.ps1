## AssembleInfo.cs にバージョンを入れ込む
## git command が使える必要性あり。
## $HOME\AppData\Local\Atlassian\SourceTree\git_local\bin\git.exe
## 
Param( [string]$ProjectDir )
$version = git describe --long
$re = New-Object regex("^([^.]+)\.([^.]+)\.([^.]+)-([^.]+)-([^.]+)$")
$m  = $re.match($version)
$majer = $m[0].Groups[1].Value
$miner = $m[0].Groups[2].Value
$min = $m[0].Groups[3].Value
$build = $m[0].Groups[4].Value
$sha1 = $m[0].Groups[5].Value

$numversion = $majer + "." + $miner + "." + $min + "." + $build

(Get-Content ${ProjectDir}Properties/AssemblyInfo.cs) |
  Foreach-Object {$_ -replace '^\[assembly: AssemblyFileVersion.+$',"[assembly: AssemblyFileVersion(`"${version}`")]" } |
  Foreach-Object {$_ -replace '^\[assembly: AssemblyVersion.+$',"[assembly: AssemblyVersion(`"${numversion}`")]" } |
  Out-File ${ProjectDir}Properties/AssemblyInfo.cs

exit 0
