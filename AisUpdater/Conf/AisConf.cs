﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AisUpdater.Conf
{
    public class AisConf
    {
        public const string BaseUrl = "http://{0}:{1}/cms/rest/";
        public const string GripServerBaseUrl = "http://{0}:{1}/grip/rest/";
        public static Conf Config { get; set; }
        public static readonly List<string> CatalogLangList = new List<string> { "ja", "en" };
        public static readonly List<string> CatalogLangListForDisplay = new List<string> { "Japanese", "English" };
        public static readonly List<string> UiLangList = new List<string> { "ja", "en" };
        public static readonly List<string> UiLangListForDisplay = new List<string> { "Japanese", "English" };

        static AisConf()
        {

        }

        /// <summary>
        /// Conf/Ais.conf 読み込み
        /// </summary>
        public static void LoadAisConf(string confPath)
        {
            using (StreamReader sr = new StreamReader(confPath))
            {
                string rte = sr.ReadToEnd().Replace("\\", "\\\\");
                Config = JsonConvert.DeserializeObject<Conf>(rte);
            }

            if (!CatalogLangList.Contains(Config.DefaultCatalogLang))
            {
                Config.DefaultCatalogLang = "ja";
            }
            if (!UiLangList.Contains(Config.DefaultUiLang))
            {
                Config.DefaultUiLang = "ja";
            }
        }

        public class Conf
        {
            public string CmsHost;
            public string CmsPort = "60000";
            public string DefaultCatalogLang;
            public string DefaultUiLang;
            public string GripServerHost;
            public string GripServerPort = "60000";
            public bool UpdateCheck = false;
        }

    }
}
