﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AisUpdater.MultiLangMessages
{
    public static class MultiLangMessage
    {
        public static Dictionary<string, Dictionary<string, string>> map = new Dictionary<string, Dictionary<string, string>>();

        public static void SetMessage()
        {
            try
            {
                createMessage();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message + Environment.NewLine + e.GetType().FullName + Environment.NewLine + e.Source + Environment.NewLine + e.StackTrace);
            }
        }

        private static void createMessage()
        {
            map.Add("E_001", new Dictionary<string, string>()); map["E_001"].Add(MessageLang.Ja, MessageJa.E_001); map["E_001"].Add(MessageLang.En, MessageEn.E_001);
            map.Add("I_001", new Dictionary<string, string>()); map["I_001"].Add(MessageLang.Ja, MessageJa.I_001); map["I_001"].Add(MessageLang.En, MessageEn.I_001);
            map.Add("I_002", new Dictionary<string, string>()); map["I_002"].Add(MessageLang.Ja, MessageJa.I_002); map["I_002"].Add(MessageLang.En, MessageEn.I_002);
            map.Add("C_001", new Dictionary<string, string>()); map["C_001"].Add(MessageLang.Ja, MessageJa.C_001); map["C_001"].Add(MessageLang.En, MessageEn.C_001);
            map.Add("I_003", new Dictionary<string, string>()); map["I_003"].Add(MessageLang.Ja, MessageJa.I_003); map["I_003"].Add(MessageLang.En, MessageEn.I_003);
            map.Add("E_002", new Dictionary<string, string>()); map["E_002"].Add(MessageLang.Ja, MessageJa.E_002); map["E_002"].Add(MessageLang.En, MessageEn.E_002);
            map.Add("E_003", new Dictionary<string, string>()); map["E_003"].Add(MessageLang.Ja, MessageJa.E_003); map["E_003"].Add(MessageLang.En, MessageEn.E_003);
            map.Add("E_004", new Dictionary<string, string>()); map["E_004"].Add(MessageLang.Ja, MessageJa.E_004); map["E_004"].Add(MessageLang.En, MessageEn.E_004);
        }

        public static string GetMessage(string code, string lang)
        {
            if (!map.ContainsKey(code)) return null;
            string lang2 = !(lang == MessageLang.Ja || lang == MessageLang.En) ? MessageLang.En : lang;

            return map[code][lang2];
        }

    }

    public static class MessageLang
    {
        public static string Ja = "ja";
        public static string En = "en";
    }

    public static class MessageJa
    {
        public static string E_001 = "サーバとの通信に失敗しました。\n{0}";
        public static string I_001 = "Ais.exe は、既に最新バージョンです。";
        public static string I_002 = "Enterキーを押すと、Ais.exe を起動します。";
        public static string C_001 = @"サーバに新バージョンが存在します。
バージョンを更新しますか？

現在のバージョン: {0}
新バージョン: {1}";
        public static string I_003 = "アップデートに成功しました。";
        public static string E_002 = "アップデートに失敗しました。\n{0}";
        public static string E_003 = "AISの起動に失敗しました。\n{0}";
        public static string E_004 = "エラーが発生しました。\n[{0}]\n[{1}]\n[{2}]";

    }
    public static class MessageEn
    {
        public static string E_001 = "Failed to connect to the server.\n{0}";
        public static string I_001 = "Ais.exe is aleady latest verision.";
        public static string I_002 = "Push Enter key, then Ais.exe will be started.";
        public static string C_001 = @"The newer version exists in the server.
Do you update this Ais.exe?

Current Version: {0}
New Version: {1}";
        public static string I_003 = "Succeed to update.";
        public static string E_002 = "Failed to update.\n{0}";
        public static string E_003 = "Failed to start AIS.\n{0}";
        public static string E_004 = "An Error occured.\n[{0}]\n[{1}]\n[{2}]";
    }
}
