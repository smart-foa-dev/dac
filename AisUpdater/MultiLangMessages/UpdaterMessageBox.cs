﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AisUpdater.MultiLangMessages
{
    public class UpdaterMessageBox
    {
        private static IntPtr hHook = IntPtr.Zero;

        /// 
        /// ボタンに表示するテキストを指定します。
        /// 

        public static CustomButtonText ButtonText { get; set; }

        /// フックを開始します。
        /// 
        static void BeginHook()
        {
            EndHook();
            UpdaterMessageBox.hHook = SetWindowsHookEx(WH_CBT, new HOOKPROC(UpdaterMessageBox.HookProc), IntPtr.Zero, GetCurrentThreadId());
        }

        static IntPtr HookProc(int nCode, IntPtr wParam, IntPtr lParam)
        {
            if (nCode == HCBT_ACTIVATE)
            {
                if (UpdaterMessageBox.ButtonText.Abort != null) SetDlgItemText(wParam, ID_BUT_ABORT, UpdaterMessageBox.ButtonText.Abort);
                if (UpdaterMessageBox.ButtonText.Cancel != null) SetDlgItemText(wParam, ID_BUT_CANCEL, UpdaterMessageBox.ButtonText.Cancel);
                if (UpdaterMessageBox.ButtonText.Ignore != null) SetDlgItemText(wParam, ID_BUT_IGNORE, UpdaterMessageBox.ButtonText.Ignore);
                if (UpdaterMessageBox.ButtonText.No != null) SetDlgItemText(wParam, ID_BUT_NO, UpdaterMessageBox.ButtonText.No);
                if (UpdaterMessageBox.ButtonText.OK != null) SetDlgItemText(wParam, ID_BUT_OK, UpdaterMessageBox.ButtonText.OK);
                if (UpdaterMessageBox.ButtonText.Retry != null) SetDlgItemText(wParam, ID_BUT_RETRY, UpdaterMessageBox.ButtonText.Retry);
                if (UpdaterMessageBox.ButtonText.Yes != null) SetDlgItemText(wParam, ID_BUT_YES, UpdaterMessageBox.ButtonText.Yes);

                EndHook();
            }

            return CallNextHookEx(UpdaterMessageBox.hHook, nCode, wParam, lParam);
        }

        /// 
        /// フックを終了します。何回呼んでもOKです。
        /// 
        static void EndHook()
        {
            if (UpdaterMessageBox.hHook != IntPtr.Zero)
            {
                UnhookWindowsHookEx(UpdaterMessageBox.hHook);
                UpdaterMessageBox.hHook = IntPtr.Zero;
            }
        }


        #region メッセージのテキストのクラス定義

        public class CustomButtonText
        {
            public string OK { get; set; }
            public string Cancel { get; set; }
            public string Abort { get; set; }
            public string Retry { get; set; }
            public string Ignore { get; set; }
            public string Yes { get; set; }
            public string No { get; set; }
        }

        #endregion

        #region Win32API

        const int WH_CBT = 5;
        const int HCBT_ACTIVATE = 5;

        const int ID_BUT_OK = 1;
        const int ID_BUT_CANCEL = 2;
        const int ID_BUT_ABORT = 3;
        const int ID_BUT_RETRY = 4;
        const int ID_BUT_IGNORE = 5;
        const int ID_BUT_YES = 6;
        const int ID_BUT_NO = 7;

        private delegate IntPtr HOOKPROC(int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll")]
        private static extern IntPtr SetWindowsHookEx(int idHook, HOOKPROC lpfn, IntPtr hInstance, IntPtr threadId);

        [DllImport("user32.dll")]
        private static extern bool UnhookWindowsHookEx(IntPtr hHook);

        [DllImport("user32.dll")]
        private static extern IntPtr CallNextHookEx(IntPtr hHook, int nCode, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll")]
        private static extern IntPtr GetCurrentThreadId();

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern bool SetDlgItemText(IntPtr hWnd, int nIDDlgItem, string lpString);

        #endregion




        /// <summary>
        /// コンストラクタ
        /// </summary>
        private UpdaterMessageBox()
        {
            UpdaterMessageBox.ButtonText = new CustomButtonText();
        }

        /// <summary>
        /// 確認のメッセージボックスを表示します。
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public static System.Windows.Forms.DialogResult DisplayConfirmMessageBox(string message)
        {
            try
            {
                if (UpdaterMessageBox.ButtonText == null) UpdaterMessageBox.ButtonText = new CustomButtonText();
                UpdaterMessageBox.ButtonText.Yes = "Yes";
                UpdaterMessageBox.ButtonText.No = "No";
                BeginHook();
                return System.Windows.Forms.MessageBox.Show(message.Replace("\\r\\n", Environment.NewLine), "AIS", System.Windows.Forms.MessageBoxButtons.YesNo, System.Windows.Forms.MessageBoxIcon.Question);
                //return MessageBox.Show(text, caption, buttons, icons);
            }
            catch (Exception ex)
            {

                System.Windows.Forms.MessageBox.Show(ex.Message + Environment.NewLine + ex.GetType().FullName + Environment.NewLine + ex.StackTrace);
                return System.Windows.Forms.DialogResult.Cancel;
            }
            finally
            {
                EndHook();
            }
        }
    }
}
