﻿using AisUpdater.Conf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace AisUpdater.Common
{

    public class ManagerExecUpdater
    {
        public static readonly string updaterName = "AisUpdater";

        public static void recordExecuteUpdater(string workingDirPath)
        {
            string tmpDirPath = Path.Combine(workingDirPath, "tmp");

            if (!Directory.Exists(tmpDirPath))
            {
                Directory.CreateDirectory(tmpDirPath);
            }

            File.Create(getRecordFileName(workingDirPath)).Close();
            
        }

        public static bool isExecutedUpdater(string workingDirPath)
        {
            if (File.Exists(getRecordFileName(workingDirPath)))
            {
                return true;
            }

            return false;
        }

        public static void clearRecordExecuteUpdater(string workingDirPath)
        {
            string recordFilePath = getRecordFileName(workingDirPath);

            if (File.Exists(recordFilePath))
            {
                File.Delete(recordFilePath);
            }
        }

        private static string getRecordFileName(string workingDirPath)
        {
            string tmpDirPath = Path.Combine(workingDirPath, "tmp");
            string recordFileName = updaterName;

            return Path.Combine(tmpDirPath, recordFileName);
        }
    }
}
