﻿using AisUpdater.Conf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using log4net;
using AisUpdater.MultiLangMessages;

namespace AisUpdater.Common
{
    public class AppUpdaterInfo
    {
        private static readonly string confPath = @"Conf\Ais.conf";
        private static readonly string latestVersion = "latest";

        public static readonly string exeName = "Dac.exe";
        public string currentDirPath;
        public bool isUpdateChecked;
        public string fileVersion;
        public string checkedVersion;

        public AppUpdaterInfo(string currentDirPath, bool isUpdatedChecked, string fileVersion, string checkedVersion) 
        {
            this.currentDirPath = currentDirPath;
            this.isUpdateChecked = isUpdatedChecked;
            this.fileVersion = fileVersion;
            this.checkedVersion = checkedVersion;
        }

        public bool isLatest()
        {
            return checkedVersion == latestVersion;
        }

        public static AppUpdaterInfo versionCheck()
        {
            // load config
            string currentDirPath = System.AppDomain.CurrentDomain.BaseDirectory;
            string exePath = Path.Combine(currentDirPath, exeName);
            string confFullPath = Path.Combine(currentDirPath, confPath);
#if DEBUG
            logger.DebugFormat("Config file path: {0}", confFullPath);
            logger.DebugFormat("R2.exe path: {0}", exePath);
#endif
            AisConf.LoadAisConf(confFullPath);
            if (!AisConf.Config.UpdateCheck)
            {
                return new AppUpdaterInfo(currentDirPath, false, string.Empty, string.Empty);
            }
            
            // Update check.
            string fileVersion = FileVersionInfo.GetVersionInfo(exePath).FileVersion;
            Console.WriteLine("AIS version: {0}", fileVersion);

            string checkedVersion = string.Empty;
            try
            {
                checkedVersion = latestCheck(AisConf.Config.CmsHost, AisConf.Config.CmsPort, fileVersion);
            }
            catch (Exception e)
            {
                string message = string.Format(MultiLangMessage.GetMessage("E_001", AisConf.Config.DefaultUiLang), e.Message);

#if DEBUG
                message += string.Format("\n" + e.StackTrace);
#endif
                logger.Error(message);
                return new AppUpdaterInfo(currentDirPath, true, fileVersion, string.Empty);
            }

            return new AppUpdaterInfo(currentDirPath, true, fileVersion, checkedVersion);
        }

        /// <summary>
        /// プログラム起動
        /// </summary>
        /// <param name="filePath">exeのパス</param>
        public static void activateProgram(string workingDirPath, string exeName, string arguments = "")
        {
            Process[] apps = Process.GetProcessesByName(Path.GetFileNameWithoutExtension(exeName));
            if (apps.Count() > 0)
            {
                return;
            }

            ProcessStartInfo processStartInfo = new ProcessStartInfo();
            processStartInfo.FileName = Path.Combine(workingDirPath, exeName);
            processStartInfo.WorkingDirectory = workingDirPath;
            processStartInfo.Arguments = arguments;

            using (Process process = new Process())
            {
                process.StartInfo = processStartInfo;
                process.Start();
            }
        }

        /// <summary>
        /// 最新版かどうかチェック
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="version"></param>
        /// <returns>最新版だと latest<br>
        /// そうでなければ最新版のバージョンを表す文字列</returns>
        private static string latestCheck(string host, string port, string version)
        {
            string url = string.Format("http://{0}:{1}/cms/rest/ais/check?version={2}",
                host,
                port,
                version.Replace(".", "_"));
            logger.DebugFormat("Checking: {0}", url);

            string result = string.Empty;
            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                result = client.DownloadString(url);
            }
#if DEBUG
            logger.DebugFormat("Result: {0}", result);
#endif

            return result;
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(AppUpdaterInfo));
    }
}
