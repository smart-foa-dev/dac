﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Diagnostics;
using System.Net;
using AisUpdater.Common;
using AisUpdater.Conf;
using System.Windows.Forms;
using log4net;
using log4net.Config;
using AisUpdater.MultiLangMessages;

namespace AisUpdater
{
    class AisUpdater
    {
        private static int killInterval = Properties.Settings.Default.killInterval;

        static void Main(string[] args)
        {
            try
            {
                MultiLangMessage.SetMessage();
                XmlConfigurator.Configure(new System.IO.FileInfo(Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, @"log4net.config")));
                bool isR2Called = args.Any(a => a == "R2");
                bool success = false;
                AppUpdaterInfo appUpdaterInfo = AppUpdaterInfo.versionCheck();

                string currentDirPath = appUpdaterInfo.currentDirPath;
                string fileVersion = appUpdaterInfo.fileVersion;
                string checkedVersion = appUpdaterInfo.checkedVersion;

                if (!appUpdaterInfo.isUpdateChecked)
                {
                    return;
                }

                if (checkedVersion == string.Empty)
                {
                    return;
                }


                if (appUpdaterInfo.isLatest())
                {
                    logger.Debug(MultiLangMessage.GetMessage("I_001", AisConf.Config.DefaultUiLang));
                    logger.Debug(MultiLangMessage.GetMessage("I_002", AisConf.Config.DefaultUiLang));
                    //logger.Debug("Ais.exe は、既に最新バージョンです。");
                    //logger.Debug("Enterキーを押すと、Ais.exe を起動します。");
                    return;
                }
                else
                {
                    StringBuilder displayCheckedVersion = new StringBuilder(checkedVersion);
                    string message = string.Format(MultiLangMessage.GetMessage("C_001", AisConf.Config.DefaultUiLang), fileVersion, displayCheckedVersion.Replace("_", "."));
                    //                string message = string.Format(
                    //@"サーバに新バージョンが存在します。
                    //バージョンを更新しますか？
                    //
                    //現在のバージョン: {0}
                    //新バージョン: {1}",
                    //                    fileVersion,
                    //                    displayCheckedVersion.Replace("_", "."));
                    //DialogResult dr = MessageBox.Show(message,
                    //    "AIS",
                    //    MessageBoxButtons.YesNo,
                    //    MessageBoxIcon.Information);
                    DialogResult dr = UpdaterMessageBox.DisplayConfirmMessageBox(message);
                    if (dr != DialogResult.Yes)
                    {
                        if (!isR2Called)
                        {
                            AppUpdaterInfo.activateProgram(currentDirPath, AppUpdaterInfo.exeName);
                        }
                        return;
                    }

                    // 最新版ではないので、KILLする
                    bool isKilled = killAis();
                    if (!isKilled)
                    {
                        return;
                    }

                    // Update.
                    DirectoryInfo di = new DirectoryInfo(currentDirPath);
                    string dirPath = di.Parent.FullName;
                    string zipName = string.Format("ais-{0}.zip", checkedVersion);
                    string zipPath = Path.Combine(dirPath, zipName);
                    try
                    {
                        downloadZip(AisConf.Config.CmsHost, AisConf.Config.CmsPort, checkedVersion, zipPath);

                        // Extarct
                        string tmpDir = Path.Combine(dirPath, "ais_tmp");
                        DirectoryInfo tmpDirInfo = new DirectoryInfo(tmpDir);
                        if (tmpDirInfo.Exists)
                        {
                            deleteFilesRecursive(tmpDirInfo);
                            tmpDirInfo.Delete(true);
                        }
                        ZipFile.ExtractToDirectory(zipPath, tmpDir);

                        // Replace
                        replaceFiles(currentDirPath, tmpDir);
                        FileInfo zip = new FileInfo(zipPath);
                        zip.Delete();

                        success = true;
                        logger.Debug(MultiLangMessage.GetMessage("I_003", AisConf.Config.DefaultUiLang));
                        //logger.Debug("アップデートに成功しました。");
                    }
                    catch (Exception e)
                    {
                        success = false;
                        string exMessage = string.Format(MultiLangMessage.GetMessage("E_002", AisConf.Config.DefaultUiLang), e.Message);
                        //string exMessage = string.Format("アップデートに失敗しました。\n{0}", e.Message);

#if DEBUG
                        exMessage += string.Format("\n" + e.StackTrace);
#endif
                        logger.Debug(exMessage);
                    }
                }

                // Activate AIS.
                if (success && !isR2Called)
                {
                    try
                    {
                        AppUpdaterInfo.activateProgram(currentDirPath, AppUpdaterInfo.exeName);
                    }
                    catch (Exception e)
                    {
                        string exMessage = string.Format(MultiLangMessage.GetMessage("E_003", AisConf.Config.DefaultUiLang), e.Message);
                        //string exMessage = string.Format("AISの起動に失敗しました。\n{0}", e.Message);

#if DEBUG
                        exMessage += string.Format("\n" + e.StackTrace);
#endif
                        logger.Error(exMessage);
                    }
                }

            }
            catch (Exception e)
            {
                logger.Error(string.Format(MultiLangMessage.GetMessage("E_004", AisConf.Config.DefaultUiLang), e.Message, e.GetType().FullName, e.StackTrace));
            }
        }


        /// <summary>
        /// ais-x.x.x.x.zip をダウンロード
        /// </summary>
        /// <param name="host"></param>
        /// <param name="port"></param>
        /// <param name="version"></param>
        /// <param name="filePath">ダウンロード先のファイルパス</param>
        private static void downloadZip(string host, string port, string version, string filePath)
        {
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }

            string url = string.Format("http://{0}:{1}/cms/rest/ais/download?version={2}",
                host,
                port,
                version);
            logger.DebugFormat("Downloading: {0}", url);

            using (WebClient client = new WebClient())
            {
                client.Encoding = Encoding.UTF8;
                client.DownloadFile(url, filePath);
            }
        }

        /// <summary>
        /// 各ファイルを最新版に入れ替える
        /// </summary>
        private static void replaceFiles(string currentDir, string tmpDir)
        {
            string[] excludeFileName = { "AisUpdater.exe", 
                                         "Newtonsoft.Json.dll", 
                                         "log4net.dll", 
                                         "FoaCoreCom.tlb", 
                                         "GripDataRetriever.tlb" };
            logger.DebugFormat("Replace Files{0}->{1}", tmpDir, currentDir);
            // 不要なファイル・フォルダを削除
            DirectoryInfo current = new DirectoryInfo(currentDir);
            List<string> deleteDirectoryList = new List<string>();
            foreach (var di in current.GetDirectories())
            {
                if (di.Name == "Conf" ||
                    di.Name == "Emergency" ||
                    di.Name == "Log" ||
                    di.Name == "export" ||
                    di.Name == "option" ||
                    di.Name == "registry" ||
                    di.Name == "tmp")
                {
                    continue;
                }

                deleteFilesRecursive(di);
                deleteDirectoryList.Add(di.FullName);
            }
            for (int i = 0; i < deleteDirectoryList.Count; i++)
            {
                logger.DebugFormat("Delete Directory{0}", deleteDirectoryList[i]);
                DirectoryInfo di = new DirectoryInfo(deleteDirectoryList[i]);
                di.Delete(true);
            }

            foreach (var fi in current.GetFiles())
            {
                if (excludeFileName.Any(f => f == fi.Name))
                {
                    continue;
                }

                logger.DebugFormat("Delete File{0}", fi.FullName);
                fi.Delete();
            }

            DirectoryInfo latest = new DirectoryInfo(tmpDir);
            foreach (DirectoryInfo di in latest.GetDirectories())
            {
                if (di.Name == "Conf")
                {
                    continue;
                }

                string dstPath = Path.Combine(currentDir, di.Name);
                directoryCopy(di.FullName, dstPath);
            }
            foreach (FileInfo fi in latest.GetFiles())
            {
                if (excludeFileName.Any(f => f == fi.Name))
                {
                    continue;
                }

                logger.DebugFormat("Copy File{0}", fi.FullName);
                string dstPath = Path.Combine(currentDir, fi.Name);
                fi.CopyTo(dstPath);
            }

            latest.Delete(true);
        }

        private static void deleteFilesRecursive(DirectoryInfo root)
        {
            logger.DebugFormat("Delete Directory Files{0}", root.FullName);
            foreach (var di in root.GetDirectories())
            {
                deleteFilesRecursive(di);
                di.Delete();
            }

            foreach (var fi in root.GetFiles())
            {
                fi.Delete();
            }
        }

        /// <summary>
        /// ディレクトリをコピー
        /// </summary>
        /// <param name="srcPath">コピー元のディレクトリパス</param>
        /// <param name="dstPath">コピー先のディレクトリパス</param>
        private static void directoryCopy(string srcPath, string dstPath)
        {
            DirectoryInfo srcDir = new DirectoryInfo(srcPath);
            DirectoryInfo dstDir = new DirectoryInfo(dstPath);

            logger.DebugFormat("Directory Copy {0}->{1}", srcPath, dstPath);
            // コピー先のディレクトリがなければ作成
            if (!dstDir.Exists)
            {
                dstDir.Create();
                dstDir.Attributes = srcDir.Attributes;
            }

            // ファイルコピー
            foreach (FileInfo fi in srcDir.GetFiles())
            {
                // 同じファイルが存在していたら上書き
                fi.CopyTo(Path.Combine(dstDir.FullName, fi.Name), true);
            }

            //ディレクトリのコピー（再帰を使用）
            foreach (DirectoryInfo di in srcDir.GetDirectories())
            {
                directoryCopy(di.FullName, Path.Combine(dstDir.FullName, di.Name));
            }
        }

        private static bool killAis()
        {
            bool isKilled = false;
            try
            {
                Process[] apps = Process.GetProcessesByName("AIS");
                foreach (Process app in apps)
                {
                    logger.Debug(app.ProcessName + " kill");
                    app.CloseMainWindow();
                    app.WaitForExit(1000);
                    if (!app.HasExited)
                    {
                        logger.Warn("force " + app.ProcessName + " to be killed");
                        app.Kill();
                    }
                }
                isKilled = true;
            }
            catch
            {
                logger.Warn("not killed");
            }

            return isKilled;
        }

        private static readonly ILog logger = LogManager.GetLogger(typeof(AisUpdater));

    }
}
