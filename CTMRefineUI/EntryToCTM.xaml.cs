﻿using FoaCore.Common;
using FoaCore.Common.Util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Xceed.Wpf.Toolkit;
using System.Windows.Media;
using CTMRefineUI.CtmData;
using Newtonsoft.Json.Linq;
using FoaCore.Common.Converter;
using CTMRefineUI.Conf;
using FoaCore.Common.Model;
using Newtonsoft.Json;

namespace CTMRefineUI
{
	public class GetCellTypeConverter : System.Windows.Data.IMultiValueConverter
	{
		public object Convert(object[] value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (value[0] == DependencyProperty.UnsetValue)
			{
				return Properties.Resources.ELEMENT_TYPE_DATA;
			}
			string rowType = (string)value[0];
			int displayIndex = (int)value[1];
			// File.AppendAllText("convert.txt", rowType + "," + displayIndex + Environment.NewLine);

			if (rowType.Equals(Properties.Resources.ELEMENT_TYPE_TYPE))
			{
				return rowType;
			}
			else if (rowType.Equals(Properties.Resources.ELEMENT_TYPE_DATA))
			{
				return rowType;
			}
			else
			{
				string[] cols = rowType.Split(',');
				for (int i = 0; i < cols.Length; i++)
				{
					if (int.Parse(cols[i]) == displayIndex)
					{
						return Properties.Resources.ELEMENT_TYPE_CTM_MODIFIED;
					}
				}
				return Properties.Resources.ELEMENT_TYPE_DATA;
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}


	/// <summary>
	/// .xaml の相互作用ロジック
	/// </summary>
	public partial class EntryToCTM
	{
		private Dictionary<string, string> paramDictionary = new Dictionary<string, string>();

		private ElementListInfoClass elementListInfo = null;
		private CTMDataClass ctmData = null;
		private CtmDataRetriever ctmDatatReader = null;
		private Dictionary<String, ElementInfo> elementList = null;

		private List<string> dispalyElementList = new List<string>();
		private List<string> editElementList = new List<string>();
		private bool SPECIFY_FLAG = false;
		private string ctmId = string.Empty;
		public const int MAX_DATA = 500;
		private string ctmName = string.Empty;

		public Dictionary<string, string> ctm_Info = new Dictionary<string, string>();

		//変更した値の格納先
		private List<changedInfo> changedInfoList = new List<changedInfo>();

		private Dictionary<string, CTMRefineUIConf.CTMRefineUICtmParam> jsonParam = null;

		private class changedInfo
		{
			public string ChangedElementId = "";
			public long ChangedRT = 0;
			public string ChangedValue = "";
			public DataGridCell cell = null;
		}

		private int currentDataCtm = -1;
		private DataGridCell errorCell = null;

		/// <summary>
		/// フォーム初期化
		/// </summary>
		public EntryToCTM()
		{
			//File.AppendAllText(System.AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + "\\" + "Param.txt", "EntryToCTM()" + Environment.NewLine);

			if (!App.mutex.WaitOne(0, false))
			{
				ShowMessage(Properties.Resources.C_ERROR_000, MessageBoxImage.Error);
				Application.Current.Shutdown();
				return;
			}

			InitializeComponent();

			// ComboBoxとDateTimePickerを紐付ける
			this.comboBox_Start.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_Start };
			this.comboBox_End.UpdateDateTimePickers = new DateTimePicker[] { this.dateTimePicker_End };

			// ComboBoxのアイテムソースを設定
			this.comboBox_Start.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISStart()).GetList();
			this.comboBox_End.ItemsSource = ((IListSource)CmsDataTable.GetSimpleDayWeekPatternForAISEnd()).GetList();

			if (0 < this.comboBox_Start.Items.Count)
			{
				this.comboBox_Start.SelectedIndex = 0;
			}
			if (0 < this.comboBox_End.Items.Count)
			{
				this.comboBox_End.SelectedIndex = 0;
			}
		}

		/// <summary>
		/// フォームロード
		/// </summary>
		private async void Window_Loaded(object sender, RoutedEventArgs e)
		{
			this.tbTitle.Text = Conf.CTMRefineUIConf.Config.Title;

			App app = Application.Current as App;
			SPECIFY_FLAG = app._args.ContainsKey(App.ARG_OPTION_SPECIFY);
			//app._args.ContainsKey(App.ARG_OPTION_SPECIFY
			if (SPECIFY_FLAG)
			{
				string SpecifyPath = string.Empty;
				foreach (var path in app._args)
				{
					if (path.Key.Equals(App.ARG_OPTION_SPECIFY))
					{
						SpecifyPath = path.Value;
					}
				}
				string readText = File.ReadAllText(SpecifyPath);
				//File.AppendAllText(System.AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + "\\" + "Param.txt", "Window_Loaded()" + Environment.NewLine + SpecifyPath + Environment.NewLine + readText);
				// System.Windows.MessageBox.Show(SpecifyPath + Environment.NewLine + readText);
				this.jsonParam = JsonConvert.DeserializeObject<Dictionary<string, CTMRefineUIConf.CTMRefineUICtmParam>>(readText);
				if (this.jsonParam.Keys.Count <= 1)
				{
					this.OptionGrid.Visibility = System.Windows.Visibility.Collapsed;
				}
				else
				{
					this.OptionGrid.Visibility = System.Windows.Visibility.Visible;
					this.OptionCtmSelection.Visibility = System.Windows.Visibility.Visible;
					this.OptionDateStart.Visibility = System.Windows.Visibility.Hidden;
					this.OptionText.Visibility = System.Windows.Visibility.Hidden;
					this.OptionDateEnd.Visibility = System.Windows.Visibility.Hidden;
				}

				this.Search_result.Visibility = System.Windows.Visibility.Collapsed;

				await this.InitializeCtms();
			}
			else
			{
				this.dateTimePicker_Start.Value = DateTime.Today;
				this.dateTimePicker_End.Value = DateTime.Now;
				await this.InitializeCtms();
			}

			this.Activate();
		}



		private async Task InitializeCtms()
		{
			int defaultCtm = 0;

			// CTMの結構とCTM名を取得する
			String ctmName = String.Empty;
			ctmDatatReader = new CtmDataRetriever(Conf.CTMRefineUIConf.Config.CmsHost, Conf.CTMRefineUIConf.Config.CmsPort, Conf.CTMRefineUIConf.Config.UseProxy, Conf.CTMRefineUIConf.Config.ProxyURI);
			App app = Application.Current as App;

			if (SPECIFY_FLAG)
			{
				ctm_Info = await ctmDatatReader.GetCtmCatalogName();

				// Add ctm
				foreach (var ctm in ctm_Info)
				{
					if (CTMRefineUIConf.Config.CtmIdAndElements.ContainsKey(ctm.Key) && this.jsonParam.ContainsKey(ctm.Value))
					{
						CTM_ComboBox.Items.Add(ctm.Key);
						if (ctm.Value.Equals(Conf.CTMRefineUIConf.Config.DefaultCtm))
						{
							defaultCtm = CTM_ComboBox.Items.Count - 1;
						}
					}
				}

				// Set default ctm
				if (!CTM_ComboBox.Items.IsEmpty)
				{
					CTM_ComboBox.SelectedIndex = defaultCtm;
				}

				//ステータスファイルを選択したデータを検索
				GetCTMInfoFromExcel();
			}
			else
			{
				ctm_Info = await ctmDatatReader.GetCtmCatalogName();

				// Add ctm
				foreach (var ctm in ctm_Info)
				{
					if (CTMRefineUIConf.Config.CtmIdAndElements.ContainsKey(ctm.Key))
					{
						CTM_ComboBox.Items.Add(ctm.Key);
						if (ctm.Value.Equals(Conf.CTMRefineUIConf.Config.DefaultCtm))
						{
							defaultCtm = CTM_ComboBox.Items.Count - 1;
						}
					}
				}

				// Set default ctm
				if (!CTM_ComboBox.Items.IsEmpty)
				{
					CTM_ComboBox.SelectedIndex = defaultCtm;
				}
			}
		}

		private void image_MouseEnter(object sender, MouseEventArgs e)
		{
			this.Cursor = Cursors.Hand;
		}

		private void image_MouseLeave(object sender, MouseEventArgs e)
		{
			this.Cursor = Cursors.Arrow;
		}

		/// <summary>
		/// 選択したデータを検索：CTM収集
		/// </summary>
		private bool firstSelectedCtmFromExcel = true;
		private async void GetCTMInfoFromExcel(string selectedCtmName = null)
		{
			if (selectedCtmName == null)
			{
				firstSelectedCtmFromExcel = true;
			}
			App app = Application.Current as App;

			//string SpecifyPath = string.Empty;
			//foreach (var path in app._args)
			//{
			//	if (path.Key.Equals(App.ARG_OPTION_SPECIFY))
			//	{
			//		SpecifyPath = path.Value;
			//	}
			//}
			//string readText = File.ReadAllText(SpecifyPath);
			//File.AppendAllText(System.AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + "\\" + "Param.txt", "GetCTMInfoFromExcel()" + Environment.NewLine + SpecifyPath + Environment.NewLine + readText);
			//// System.Windows.MessageBox.Show(SpecifyPath + Environment.NewLine + readText);
			//Dictionary<string, CTMRefineUIConf.CTMRefineUICtmParam> jsonParam = JsonConvert.DeserializeObject<Dictionary<string, CTMRefineUIConf.CTMRefineUICtmParam>>(readText);

			// Directory.Delete(Path.GetDirectoryName(SpecifyPath), true);

			string startTime = string.Empty;
			string endTime = string.Empty;
			HashSet<long> rtHash = new HashSet<long>();

			foreach (var param in this.jsonParam)
			{
				ctmId = param.Key;
				startTime = param.Value.MinRT.ToString();
				endTime = param.Value.MaxRT.ToString();
				rtHash = param.Value.RTs;
				if (selectedCtmName != null && this.ctm_Info[selectedCtmName] == ctmId)
				{
					break;
				}
			}

			string ctmName = string.Empty;
			foreach (var ctm in ctm_Info)
			{
				if (ctm.Value.Equals(ctmId))
				{
					ctmName = ctm.Key;
				}
			}
			this.ctmName = ctmName;
			this.tbTitle.Text = "[Title] " + Conf.CTMRefineUIConf.Config.Title + Environment.NewLine + "[CTM] " + ctmName;
			this.tbTitle.FontSize = 18.0;
			if (selectedCtmName == null && this.jsonParam.Keys.Count > 1)
			{
				this.CTM_ComboBox.SelectedValue = ctmName;
			}

			//CTMConfigの表示列、編集列により、表示列、編集列処理する
			dispalyElementList.Clear();
			editElementList.Clear();
			if (CTMRefineUI.Conf.CTMRefineUIConf.Config.CtmIdAndElements.ContainsKey(ctmName))
			{
				CTMRefineUI.Conf.CTMRefineUIConf.CtmConf CtmIdAndElement = CTMRefineUI.Conf.CTMRefineUIConf.Config.CtmIdAndElements[ctmName];
				dispalyElementList.AddRange(CtmIdAndElement.DisplayElement);
				editElementList.AddRange(CtmIdAndElement.EditElement);
			}

			elementListInfo = new ElementListInfoClass(Conf.CTMRefineUIConf.Config.CmsHost, Conf.CTMRefineUIConf.Config.CmsPort, Conf.CTMRefineUIConf.Config.UseProxy, Conf.CTMRefineUIConf.Config.ProxyURI);
			await elementListInfo.GetElementHeader(ctmId);
			elementList = elementListInfo.elementList;

			dataGrid_Output.DataContext = null;
			// 収集開始終了日時をエポックミリ秒に変換する
			ctmData = new CTMDataClass(Conf.CTMRefineUIConf.Config.CmsHost, Conf.CTMRefineUIConf.Config.CmsPort, Conf.CTMRefineUIConf.Config.UseProxy, Conf.CTMRefineUIConf.Config.ProxyURI);
			//String startTime = ctmData.UnixTimeChange(this.dateTimePicker_Start.Value.ToString());
			//String endTime = ctmData.UnixTimeChange(this.dateTimePicker_End.Value.ToString());


			// CTMデータを取得する
			var resultAsync = ctmDatatReader.GetCtmDirectResultAsync(startTime, endTime, elementListInfo.elementList, dispalyElementList, editElementList, ctmId, rtHash);
			String filePath = await resultAsync;

			if (ctmDatatReader.dt == null || ctmDatatReader.dt.Rows.Count == 0)
			{
				ShowMessage(Properties.Resources.C_INFO_001, MessageBoxImage.Information);
				return;
			}
			if (ctmDatatReader.dt.Rows.Count > MAX_DATA)
			{
				ShowMessage(string.Format(Properties.Resources.C_ERROR_003, MAX_DATA), MessageBoxImage.Error);
				return;
			}

			dataGrid_Output.DataContext = ctmDatatReader.dt;

			if (editElementList != null || editElementList.Count > 0)
			{
				int cnt = dataGrid_Output.Columns.Count;
				for (int j = 0; j < cnt; j++)
				{
					DataGridColumn col = dataGrid_Output.Columns[j];
					string colName = (String)col.Header;
					if (editElementList.Exists(o => o == colName))
					{
						col.CellStyle = this.FindResource("EditableColStyle") as Style;
					}
					if (colName.Equals("RT"))
					{
						col.CellStyle = new Style(typeof(DataGridCell));
						col.Visibility = Visibility.Collapsed;
					}
					if (colName.Equals(Properties.Resources.ELEMENT_TYPE_COLUMN))
					{
						col.CellStyle = new Style(typeof(DataGridCell));
						col.Visibility = Visibility.Collapsed;
					}
				}
			}

			dataGrid_Output.RowStyle = this.FindResource("TypeRowStyle") as Style;

			dataGrid_Output.CanUserAddRows = false;

			//currentDataCtm = CTM_ComboBox.SelectedIndex;
			firstSelectedCtmFromExcel = false;
		}

		/// <summary>
		/// CTM収集
		/// </summary>
		private async void image_GetCTMData_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (this.CTM_ComboBox.SelectedValue == null)
			{
				ShowMessage(Properties.Resources.C_INFO_003, MessageBoxImage.Information);
				return;
			}

			if (!CancelModifiedContent())
			{
				return;
			}

			//選択したCTMのIDを取得
			string value = this.CTM_ComboBox.SelectedValue.ToString();
			ctmId = ctm_Info[value];

			//CTMConfigの表示列、編集列により、表示列、編集列処理する
			dispalyElementList.Clear();
			editElementList.Clear();
			if (CTMRefineUI.Conf.CTMRefineUIConf.Config.CtmIdAndElements.ContainsKey(value))
			{
				CTMRefineUI.Conf.CTMRefineUIConf.CtmConf CtmIdAndElement = CTMRefineUI.Conf.CTMRefineUIConf.Config.CtmIdAndElements[value];
				dispalyElementList.AddRange(CtmIdAndElement.DisplayElement);
				editElementList.AddRange(CtmIdAndElement.EditElement);
			}

			elementListInfo = new ElementListInfoClass(Conf.CTMRefineUIConf.Config.CmsHost, Conf.CTMRefineUIConf.Config.CmsPort, Conf.CTMRefineUIConf.Config.UseProxy, Conf.CTMRefineUIConf.Config.ProxyURI);
			await elementListInfo.GetElementHeader(ctmId);
			elementList = elementListInfo.elementList;

			dataGrid_Output.DataContext = null;
			// 収集開始終了日時をエポックミリ秒に変換する
			ctmData = new CTMDataClass(Conf.CTMRefineUIConf.Config.CmsHost, Conf.CTMRefineUIConf.Config.CmsPort, Conf.CTMRefineUIConf.Config.UseProxy, Conf.CTMRefineUIConf.Config.ProxyURI);
			String startTime = ctmData.UnixTimeChange(this.dateTimePicker_Start.Value.ToString());
			String endTime = ctmData.UnixTimeChange(this.dateTimePicker_End.Value.ToString());


			// CTMデータを取得する
			var resultAsync = ctmDatatReader.GetCtmDirectResultAsync(startTime, endTime, elementListInfo.elementList, dispalyElementList, editElementList, ctmId);
			String filePath = await resultAsync;

			if (ctmDatatReader.dt == null || ctmDatatReader.dt.Rows.Count == 0)
			{
				ShowMessage(Properties.Resources.C_INFO_001, MessageBoxImage.Information);
				return;
			}
			if (ctmDatatReader.dt.Rows.Count > Conf.CTMRefineUIConf.Config.MaxData)
			{
				ShowMessage(string.Format(Properties.Resources.C_ERROR_003, Conf.CTMRefineUIConf.Config.MaxData), MessageBoxImage.Error);
				return;
			}

			dataGrid_Output.DataContext = ctmDatatReader.dt;

			if (editElementList != null || editElementList.Count > 0)
			{
				int cnt = dataGrid_Output.Columns.Count;
				for (int j = 0; j < cnt; j++)
				{
					DataGridColumn col = dataGrid_Output.Columns[j];
					string colName = (String)col.Header;
					if (editElementList.Exists(o => o == colName))
					{
						col.CellStyle = this.FindResource("EditableColStyle") as Style;
					}
					if (colName.Equals("RT"))
					{
						col.CellStyle = new Style(typeof(DataGridCell));
						col.Visibility = Visibility.Collapsed;
					}
					if (colName.Equals(Properties.Resources.ELEMENT_TYPE_COLUMN))
					{
						col.CellStyle = new Style(typeof(DataGridCell));
						col.Visibility = Visibility.Collapsed;
					}
				}
			}

			dataGrid_Output.RowStyle = this.FindResource("TypeRowStyle") as Style;

			dataGrid_Output.CanUserAddRows = false;

			currentDataCtm = CTM_ComboBox.SelectedIndex;
		}

		private string preValue = "";
		private string editedValue = "";

		private void dataGrid_Output_BeginningEdit(object sender, DataGridBeginningEditEventArgs e)
		{
			// System.Windows.MessageBox.Show("dataGrid_Output_BeginningEdit() " + (editElementList == null ? "NULL" : editElementList.Count.ToString()));
			// System.Windows.MessageBox.Show("[" + string.Join("],[", editElementList) + "]");
			// System.Windows.MessageBox.Show(e.Column.Header.ToString());
			string selectedCtmName = (string.IsNullOrEmpty(this.ctmName) ? this.CTM_ComboBox.SelectedValue.ToString() : this.ctmName);
			if (editElementList.Exists(o => o == (String)e.Column.Header))
			{
				//System.Windows.MessageBox.Show("editElementList.Exists(o => o == (String)e.Column.Header)" + " [" + selectedCtmName + "] [" + (String)e.Column.Header + "]");
				DataRowView view = e.Row.Item as DataRowView;
				DataRow row = view.Row;
				string rowType = row[Properties.Resources.ELEMENT_TYPE_COLUMN].ToString();

				if (rowType.Equals(Properties.Resources.ELEMENT_TYPE_DATA))
				{
					preValue = (e.Column.GetCellContent(e.Row) as TextBlock).Text;
				}
				else if (rowType.Equals(Properties.Resources.ELEMENT_TYPE_TYPE))
				{
					e.Cancel = true;
				}
				else
				{
					string[] cols = rowType.Split(',');
					for (int i = 0; i < cols.Length; i++)
					{
						if (int.Parse(cols[i]) == e.Column.DisplayIndex)
						{
							e.Cancel = true;
							return;
						}
					}
					preValue = (e.Column.GetCellContent(e.Row) as TextBlock).Text;
				}
				if (e.Cancel != true)
				{
					// System.Windows.MessageBox.Show("e.Cancel != true" + " [" + selectedCtmName + "] [" + (String)e.Column.Header + "]");

					if (Conf.CTMRefineUIConf.Config.Ctm != null)
					{
						// System.Windows.MessageBox.Show("Conf.CTMRefineUIConf.Config.Ctm != null" + " [" + String.Join("],[", Conf.CTMRefineUIConf.Config.Ctm.Keys) + "]");
						if (Conf.CTMRefineUIConf.Config.Ctm.ContainsKey(selectedCtmName))
						{
							// System.Windows.MessageBox.Show(selectedCtmName);
							if (Conf.CTMRefineUIConf.Config.Ctm[selectedCtmName].ContainsKey((String)e.Column.Header))
							{
								// System.Windows.MessageBox.Show((String)e.Column.Header);
								// System.Windows.MessageBox.Show("[" + String.Join("],[", Conf.CTMRefineUIConf.Config.Ctm[selectedCtmName][(String)e.Column.Header]) + "]");
								SelectValueListDialog dialog = new SelectValueListDialog();
								dialog.ValueList.ItemsSource = Conf.CTMRefineUIConf.Config.Ctm[selectedCtmName][(String)e.Column.Header];
								dialog.ShowInTaskbar = false;
								dialog.Title = selectedCtmName + " -> " + (String)e.Column.Header;
								dialog.Owner = this;
								dialog.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
								Nullable<bool> result = dialog.ShowDialog();
								// System.Windows.MessageBox.Show(result.ToString());

								if (result == true)
								{
									// System.Windows.MessageBox.Show(dialog.ValueList.SelectedValue.ToString());
									(e.Column.GetCellContent(e.Row) as TextBlock).Text = dialog.ValueList.SelectedValue.ToString();
			                        editedValue = dialog.ValueList.SelectedValue.ToString();

									e.Cancel = true;
									DataGridCell cell = dataGrid_Output.Columns[e.Column.DisplayIndex].GetCellContent(e.Row).Parent as DataGridCell;

									//値が変更した箇所の色を変更する、変更した値をListに追加する
									if (!preValue.Equals(editedValue))
									{
										DataTable dt = dataGrid_Output.DataContext as DataTable;
										string elementType = dt.Rows[0][e.Column.DisplayIndex].ToString();
   										if (!ValidateType(elementType, editedValue))
										{
											dataGrid_Output.CancelEdit();
											errorCell = cell;
											return;
										}

										errorCell = null;

										//ChangedElementId
										string changedelemetId = elementListInfo.elementList[e.Column.Header.ToString()].id;

										//ChangedRT
										long changedRt = 0;
										var RtValue = (e.Row.Item as DataRowView).Row.ItemArray;
										changedRt = Int64.Parse(RtValue[RtValue.Length - 1].ToString());

										//値追加
										changedInfo item = new changedInfo { ChangedElementId = changedelemetId, ChangedRT = changedRt, ChangedValue = editedValue, cell = cell };

										//同じ箇所の修正をチェックする
										foreach (changedInfo changedInfo in changedInfoList)
										{
											if (changedInfo.ChangedElementId.Equals(changedelemetId) && changedInfo.ChangedRT.Equals(changedRt))
											{
												changedInfoList.Remove(changedInfo);
												break;
											}
										}
										changedInfoList.Add(item);

										//色変更
										cell.Style = this.FindResource("ModifiedColStyle") as Style;
									}

								}
								else
								{
									e.Cancel = true;
								}
							}
						}
					}
				}
			}
			else
			{
				e.Cancel = true;
			}
		}

		private async void image_Save_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			dataGrid_Output.CommitEdit();
			App app = Application.Current as App;

			if (!SPECIFY_FLAG)
			{
				string value = this.CTM_ComboBox.SelectedValue.ToString();
				ctmId = this.ctm_Info[value];
			}

			if (changedInfoList.Count > 0)
			{
				foreach (var item in changedInfoList)
				{
					await ctmDatatReader.SetCtmDirectResultAsync(item.ChangedElementId, item.ChangedRT, item.ChangedValue, ctmId);
					item.cell.Style = this.FindResource("EditableColStyle") as Style;
				}

				changedInfoList.Clear();
				ShowMessage(Properties.Resources.C_INFO_002, MessageBoxImage.Information);
			}
		}

		private bool ValidateType(string elementType, string value)
		{
			bool ret = false;

			switch (ElementDataTypeUtil.GetElementDataType(elementType))
			{
				case ElementDataType.@byte:
					{
						byte result;
						ret = Byte.TryParse(value, out result);
						break;
					}
				case ElementDataType.@char:
					{
						Char result;
						ret = Char.TryParse(value, out result);
						break;
					}
				case ElementDataType.@float:
					{
						float result;
						ret = float.TryParse(value, out result);
						break;
					}
				case ElementDataType.@int:
					{
						int result;
						ret = int.TryParse(value, out result);
						break;
					}
				case ElementDataType.@short:
					{
						short result;
						ret = short.TryParse(value, out result);
						break;
					}
				case ElementDataType.@string:
					ret = value.Length <= 250;
					if (!ret)
					{
						ShowMessage(Properties.Resources.C_ERROR_002, MessageBoxImage.Error);
					}
					return ret;
				default:
					return false;
			}

			if (!ret)
			{
				ShowMessage(Properties.Resources.C_ERROR_001, MessageBoxImage.Error);
			}

			return ret;
		}

		private MessageBoxResult ShowMessage(string msg, MessageBoxImage icon)
		{
			if (icon.Equals(MessageBoxImage.Error))
			{
				return Xceed.Wpf.Toolkit.MessageBox.Show(msg, Properties.Resources.TEXT_TITLE, MessageBoxButton.OK, icon);
			}
			else if (icon.Equals(MessageBoxImage.Information))
			{
				return Xceed.Wpf.Toolkit.MessageBox.Show(msg, Properties.Resources.TEXT_TITLE, MessageBoxButton.OK, icon);
			}
			else if (icon.Equals(MessageBoxImage.Question))
			{
				return Xceed.Wpf.Toolkit.MessageBox.Show(msg, Properties.Resources.TEXT_TITLE, MessageBoxButton.YesNo, icon);
			}
			else
			{
				return Xceed.Wpf.Toolkit.MessageBox.Show(msg, Properties.Resources.TEXT_TITLE);
			}
		}

		private void dataGrid_Output_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
		{
			//ChangedValue
			var editedTextBox = e.EditingElement as TextBox;
			editedValue = editedTextBox.Text;
			DataGridCell cell = dataGrid_Output.Columns[e.Column.DisplayIndex].GetCellContent(e.Row).Parent as DataGridCell;

			//値が変更した箇所の色を変更する、変更した値をListに追加する
			if (!preValue.Equals(editedValue) && e.EditAction.Equals(DataGridEditAction.Commit))
			{
				DataTable dt = dataGrid_Output.DataContext as DataTable;
				string elementType = dt.Rows[0][e.Column.DisplayIndex].ToString();
				if (!ValidateType(elementType, editedValue))
				{
					dataGrid_Output.CancelEdit();
					errorCell = cell;
					return;
				}

				errorCell = null;

				//ChangedElementId
				string changedelemetId = elementListInfo.elementList[e.Column.Header.ToString()].id;

				//ChangedRT
				long changedRt = 0;
				var RtValue = (e.Row.Item as DataRowView).Row.ItemArray;
				changedRt = Int64.Parse(RtValue[RtValue.Length - 1].ToString());

				//値追加
				changedInfo item = new changedInfo { ChangedElementId = changedelemetId, ChangedRT = changedRt, ChangedValue = editedValue, cell = cell };

				//同じ箇所の修正をチェックする
				foreach (changedInfo changedInfo in changedInfoList)
				{
					if (changedInfo.ChangedElementId.Equals(changedelemetId) && changedInfo.ChangedRT.Equals(changedRt))
					{
						changedInfoList.Remove(changedInfo);
						break;
					}
				}
				changedInfoList.Add(item);

				//色変更
				cell.Style = this.FindResource("ModifiedColStyle") as Style;
			}
		}

		private void dataGrid_Output_SelectedCellsChanged(object sender, SelectedCellsChangedEventArgs e)
		{
			if (errorCell != null)
			{
				errorCell.Focus();
				errorCell.IsSelected = true;
				errorCell = null;
			}
		}

		private void CTM_ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (CTM_ComboBox.SelectedIndex != currentDataCtm)
			{
				if (!CancelModifiedContent())
				{
					CTM_ComboBox.SelectedIndex = currentDataCtm;
					e.Handled = true;
				}
				else
				{
					this.dataGrid_Output.DataContext = null;
					currentDataCtm = -1;
					if (this.SPECIFY_FLAG == true)
					{
						if (firstSelectedCtmFromExcel == false)
						{
							GetCTMInfoFromExcel(CTM_ComboBox.SelectedValue.ToString());
						}
					}
				}
			}
		}

		private bool CancelModifiedContent()
		{
			if (changedInfoList.Count > 0)
			{
				if (!MessageBoxResult.Yes.Equals(ShowMessage(Properties.Resources.C_CONFIRM_001, MessageBoxImage.Question)))
				{
					return false;
				}
			}

			changedInfoList.Clear();
			return true;
		}

		private void DataWindow_Closing(object sender, CancelEventArgs e)
		{
			dataGrid_Output.CommitEdit();
			if (changedInfoList.Count > 0)
			{
				if (!MessageBoxResult.Yes.Equals(ShowMessage(Properties.Resources.C_CONFIRM_001, MessageBoxImage.Question)))
				{
					e.Cancel = true;
				}
				else
				{
					e.Cancel = false;
				}
			}
		}

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
			dataGrid_Output.CommitEdit();
			App app = Application.Current as App;

			if (!SPECIFY_FLAG)
			{
				string value = this.CTM_ComboBox.SelectedValue.ToString();
				ctmId = this.ctm_Info[value];
			}

			if (changedInfoList.Count > 0)
			{
				foreach (var item in changedInfoList)
				{
					await ctmDatatReader.SetCtmDirectResultAsync(item.ChangedElementId, item.ChangedRT, item.ChangedValue, ctmId);
					item.cell.Style = this.FindResource("EditableColStyle") as Style;
				}

				changedInfoList.Clear();
				ShowMessage(Properties.Resources.C_INFO_002, MessageBoxImage.Information);
			}
		}
    }
}
