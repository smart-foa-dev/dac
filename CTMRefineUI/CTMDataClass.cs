﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Threading.Tasks;
using CTMRefineUI.Conf;
using CTMRefineUI.Util;
using Newtonsoft.Json.Linq;

namespace CTMRefineUI
{
    class CTMDataClass
    {
        private string _ipAddr;
        private string _portNo;
        private bool _UseProxy;
        private string _ProxyURI;

        public List<AllCTM> allCTM = new List<AllCTM>();

        public CTMDataClass(string ctmId)
        {
            _ipAddr = "10.25.66.60";
            _portNo = "60000";
        }

        public CTMDataClass(string ip, string port, bool UseProxy, string ProxyURI)
        {
            _ipAddr = ip;
            _portNo = port;
            _UseProxy = UseProxy;
            _ProxyURI = ProxyURI;
        }

        public void ChangeAddress(string ip, string port, bool UseProxy, string ProxyURI)
        {
            _ipAddr = ip;
            _portNo = port;
            _UseProxy = UseProxy;
            _ProxyURI = ProxyURI;
        }

        public String UnixTimeChange(String strDateTime)
        {
            String strWork;

            // DateTime型からUnixTimeへの変換
            var unixTime = ToUnixTime(DateTime.Parse(strDateTime));
            strWork = unixTime.ToString();

            // UnixTimeから日付型への変換
            //var dateTime = FromUnixTime(unixTime);

            return strWork;
        }

        // UNIX時間変換処理
        public static DateTime UNIX_EPOCH = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
        public static long ToUnixTime(DateTime targetTime)
        {
            // UTC時間に変換
            targetTime = targetTime.ToUniversalTime();
            // UNIXエポックからの経過時間を取得
            TimeSpan elapsedTime = targetTime - UNIX_EPOCH;
            // 経過秒数に変換
            return (long)elapsedTime.TotalSeconds * 1000;
        }

        public static DateTime FromUnixTime(long unixTime)
        {
            // UNIXエポックからの経過秒数で得られるローカル日付
            return UNIX_EPOCH.AddSeconds(unixTime / 1000).ToLocalTime();
        }
    }

    public class ElementValue
    {
        public ElementValue(String name, String id, String value)
        {
          Name = name;
          Id = id;
          Value = value;
        }

        public String Name { get; set; }
        public String Id { get; set; }
        public String Value { get; set; }
    }

    public class AllCTM
    {
        public List<ElementValue> ElementValue { get; set; }
        public String ReceiveTime { get; set; }

        public AllCTM(List<ElementValue> ev, String rt)
        {
          ElementValue = ev;
          ReceiveTime = rt;
        }

    }

}
