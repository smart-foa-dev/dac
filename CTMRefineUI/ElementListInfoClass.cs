﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Data;
using System.Net;
using System.Net.Cache;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using CTMRefineUI.Conf;
using CTMRefineUI.Util;
using CTMRefineUI.CtmData;
using FoaCore.Common;
using FoaCore.Common.Model;

namespace CTMRefineUI
{
    class ElementInfo
    {
        public string id;
        public string T;
        public string name;
    }

    class ElementListInfoClass
    {
        //private static ILog _log;

        private string _ipAddr;
        private string _portNo;
        private bool _UseProxy;
        private string _ProxyURI;
        
        // ヘッダの表示
        public Dictionary<String, ElementInfo> elementList = new Dictionary<String, ElementInfo>();

        public ElementListInfoClass(string ip, string port, bool UseProxy, string ProxyURI)
        {
            _ipAddr = ip;
            _portNo = port;
            _UseProxy = UseProxy;
            _ProxyURI = ProxyURI;
        }

        public void ChangeAddress(string ip, string port, bool UseProxy, string ProxyURI)
        {
            _ipAddr = ip;
            _portNo = port;
            _UseProxy = UseProxy;
            _ProxyURI = ProxyURI;
        }


        /// <summary>
        /// CTM名を取得する
        /// </summary>
        public async Task GetElementHeader(string ctmId)
        {
            String ctmName = String.Empty;
            //    bool cancelled = false;
            //    bool failed = false;

            CancellationTokenSource downloadCts = new CancellationTokenSource();
            CancellationToken cToken = downloadCts.Token;
            string url = string.Format("{0}{1}?id={2}", string.Format(CTMRefineUIConf.BaseUrl, _ipAddr, _portNo), "ctm/", ctmId);

            HttpClient client = CTMRefineUIUtil.getProxyHttpClient(_UseProxy, _ProxyURI);
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            var task = client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead).ContinueWith(response0 =>
            //var task = client.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cToken).ContinueWith(response0 =>
            {
                var response = response0.Result;
                if (!response.IsSuccessStatusCode)
                {
                    //failed = true;
                }
                else
                {
                    var stream = response.Content.ReadAsStreamAsync().Result;
                    var sr = new StreamReader(stream);
                    String strBody = sr.ReadToEnd();
                    sr.Close();
                    JObject obj = JObject.Parse(strBody);

                    if (obj != null)
                    {
                        // 辞書をクリアする
                        elementList.Clear();

                        JArray aryObj = (JArray)obj["children"];
                        //elementListに値設定
                        DecodeBody(aryObj);
                    }
                }

            });
            await task;
        }

        /// <summary>
        /// 受信したjson形式のファイルをデシリアライズ化する
        /// </summary>
        /// <param name="hwr">HTTPレスポンス</param>
        /// <returns></returns>
        private JObject DeserializeReqBody(HttpWebResponse hwr)
        {
            try
            {
                var utf8_encoding = new System.Text.UTF8Encoding(false);
                Stream resStream = hwr.GetResponseStream();
                StreamReader sr = new StreamReader(resStream, utf8_encoding);
                String strBody = sr.ReadToEnd();
                sr.Close();
                resStream.Close();

                hwr.Close();

                JObject obj = JObject.Parse(strBody);

                return obj;
            }
            catch (Exception ex)
            {
                //_log.Error("エレメントリストのデシリアライズに失敗しました。", ex);
                return null;
            }
        }

        // Wang Issue NO.691 2018/05/11 Start
        // 多言語対応CTMの場合は設定した言語（存在しなければ一番目）を表示するように修正
        /// <summary>
        /// エレメントリストを設定する
        /// </summary>
        /// <param name="info"></param>
        private void SetElements(JToken element, JToken displayName)
        {
            // 指定キーが存在するならば値のみをセット
            bool contains = elementList.ContainsKey(displayName["text"].ToString());
            if (!contains)
            {
                ElementInfo ei = new ElementInfo() { id = element["id"].ToString(), T = element["datatype"].ToString(), name = displayName["text"].ToString() };
                if (ei.T.Equals(((int)ElementDataType.bulky).ToString()))
                {
                    ei.T = (element["datatype"].Value<int>() + (element["bulkysubtype"] == null ? (int)BulkySubType.normal : element["bulkysubtype"].Value<int>())).ToString();
                }
                elementList.Add(displayName["text"].ToString(), ei);

            }
            else
            {
                ElementInfo ei = elementList[displayName["text"].ToString()];
                ei.id = element["id"].ToString();
                //ei.T = r["datatype"].ToString();
                if (ei.T.Equals(FoaCore.Common.Model.ElementDataTypeUtil.GetElementDataTypeText(ElementDataType.bulky, false)))
                {
                    ei.T = (element["datatype"].Value<int>() + (element["bulkysubtype"] == null ? (int)BulkySubType.normal : element["bulkysubtype"].Value<int>())).ToString();
                }
                ei.name = displayName["text"].ToString();
            }
        }
        // Wang Issue NO.691 2018/05/11 End

        /// <summary>
        /// デシリアライズしたCTMリストを辞書形式のリストに変換する
        /// </summary>
        /// <param name="info"></param>
        private bool DecodeBody(JArray obj)
        {
            try
            {
                foreach (var r in obj)
                {
                    // typeがElementならば情報を保存する
                    if (r["type"].ToString() == NodeType.Element)
                    {
                        // Wang Issue NO.691 2018/05/11 Start
                        // 多言語対応CTMの場合は設定した言語（存在しなければ一番目）を表示するように修正
                        //foreach (var rr in r["displayName"])
                        //{
                        //    Debug.WriteLine("  lang={0}, text={1}", rr["lang"].ToString(), rr["text"].ToString());
                        //    // 指定キーが存在するならば値のみをセット
                        //    bool contains = elementList.ContainsKey(rr["text"].ToString());
                        //    if (!contains)
                        //    {
                        //        ElementInfo ei = new ElementInfo() { id = r["id"].ToString(), T = r["datatype"].ToString(), name = rr["text"].ToString() };
                        //        if (ei.T.Equals(((int)ElementDataType.bulky).ToString()))
                        //        {
                        //            ei.T = (r["datatype"].Value<int>() + (r["bulkysubtype"] == null ? (int)BulkySubType.normal : r["bulkysubtype"].Value<int>())).ToString();
                        //        }
                        //        elementList.Add(rr["text"].ToString(), ei);

                        //    }
                        //    else
                        //    {
                        //        ElementInfo ei = elementList[rr["text"].ToString()];
                        //        ei.id = r["id"].ToString();
                        //        //ei.T = r["datatype"].ToString();
                        //        if (ei.T.Equals(FoaCore.Common.Model.ElementDataTypeUtil.GetElementDataTypeText(ElementDataType.bulky, false)))
                        //        {
                        //            ei.T = (r["datatype"].Value<int>() + (r["bulkysubtype"] == null ? (int)BulkySubType.normal : r["bulkysubtype"].Value<int>())).ToString();
                        //        }
                        //        ei.name = rr["text"].ToString();
                        //    }
                        //}
                        JToken displayName = null;

                        foreach (var rr in r["displayName"])
                        {
                            Debug.WriteLine("  lang={0}, text={1}", rr["lang"].ToString(), rr["text"].ToString());

                            if (displayName == null)
                            {
                                displayName = rr;
                            }

                            if (CTMRefineUIConf.Config.Language.Equals(rr["lang"].ToString()))
                            {
                                displayName = rr;
                                break;
                            }
                        }

                        if (displayName != null)
                        {
                            // 指定キーが存在するならば値のみをセット
                            SetElements(r, displayName);
                        }
                        // Wang Issue NO.691 2018/05/11 End
                    }
                    else
                    {
                        JArray objChild = (JArray)r["children"];
                        if (objChild != null)
                        {
                            DecodeBody(objChild);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 受信直後の情報をファイルに書き出す
        /// </summary>
        /// <param name="hwr"></param>
        private void OutputFileForJson(HttpWebResponse hwr)
        {
            try
            {
                var utf8_encoding = new System.Text.UTF8Encoding(false);
                Stream resStream = hwr.GetResponseStream();
                StreamReader sr = new StreamReader(resStream, utf8_encoding);
                string strBody = sr.ReadToEnd();
                sr.Close();
                resStream.Close();

                hwr.Close();

                StreamWriter sw = new StreamWriter(
                            "test.txt",
                            false,
                            utf8_encoding);

                sw.Write(strBody);

                sw.Close();
                sw.Dispose();
            }
            catch(Exception ex)
            {
                //_log.Error("ファイル書き出しに失敗しました。", ex);
            }
        }


    }


}
