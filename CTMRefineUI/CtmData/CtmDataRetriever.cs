﻿using System;
using System.IO;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Http;
using CTMRefineUI.Conf;
using CTMRefineUI.Util;
using Newtonsoft.Json.Linq;
using FoaCore.Common;
using FoaCore.Common.Model;

namespace CTMRefineUI.CtmData
{
    class CtmDataRetriever
    {
        public DataTable dt = null;

        private bool _UseProxy;
        private string _ProxyURI;
        private string _ipAddr;
        private string _portNo;

        public Dictionary<string, string> _ctmInfo = new Dictionary<string, string>();

        public CtmDataRetriever(string ip, string port, bool UseProxy, string ProxyURI)
        {
            _ipAddr = ip;
            _portNo = port;
            _UseProxy = UseProxy;
            _ProxyURI = ProxyURI;
        }

        //public StatefulJsonTreeView tree;

        public async Task<String> GetCtmDirectResultAsync(String start, String end, Dictionary<String, ElementInfo> elementList,
            List<string> dispalyElementList, List<string> editElementList, string ctmId, HashSet<long> rtHash = null)
        {
            CancellationTokenSource downloadCts = new CancellationTokenSource();
            CancellationToken cToken = downloadCts.Token;

            string url = string.Format("{0}{1}?testing={2}", string.Format(CTMRefineUIConf.BaseUrl, _ipAddr, _portNo), "mib/ctm/find", CTMRefineUIConf.Config.Testing);

            // CTMを検索する為にPOSTするJSONを生成
            JToken postToken = JToken.Parse("{}");
            postToken["ctmId"] = ctmId;
            postToken["start"] = start;
            postToken["end"] = end;

            HttpClient client = CTMRefineUIUtil.getProxyHttpClient(_UseProxy, _ProxyURI);
            var req = new HttpRequestMessage(HttpMethod.Post, url);
            req.Content = new StringContent(postToken.ToString());

            var task = client.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cToken).ContinueWith(response0 =>
            {
                if (cToken.IsCancellationRequested)
                {
                    throw new TaskCanceledException();
                }
                var response = response0.Result;
                if (response.IsSuccessStatusCode)
                {
                    DataSet ds = new DataSet();
                    dt = ds.Tables.Add("CTM");

                    using (var stream = response.Content.ReadAsStreamAsync().Result)
                    using (var sr = new StreamReader(stream))
                    using (var iterator = new CtmDirectResultReader(sr).GetEnumerator())
                    {
                        if (iterator.MoveNext())
                        {
                            var rec = iterator.Current;

                            // header
                            List<ElementInfo> keyOrder = new List<ElementInfo>(rec.EL.Count);
                            if (dispalyElementList == null || dispalyElementList.Count == 0)
                            {
                                foreach (KeyValuePair<string, ElementInfo> pair in elementList)
                                {
                                    dt.Columns.Add(pair.Key);
                                    keyOrder.Add(pair.Value);
                                }
                            }
                            else
                            {
                                for (int i = 0; i < dispalyElementList.Count; i++)
                                {
                                    if (elementList.ContainsKey(dispalyElementList[i]))
                                    {
                                        dt.Columns.Add(dispalyElementList[i]);
                                        keyOrder.Add(elementList[dispalyElementList[i]]);
                                    }
                                }
                            }

                            dt.Columns.Add(Properties.Resources.ELEMENT_TYPE_COLUMN);
                            if (!dt.Columns.Contains("RT"))
                            {
                                dt.Columns.Add("RT");
                            }

                            // add data rows
                            bool createTypeRow = true;
                            DataRow dh = dt.NewRow();
                            do
                            {
                                rec = iterator.Current;
                                DataRow dr = dt.NewRow();

                                // add type row
                                if (createTypeRow)
                                {
                                    foreach (ElementInfo key in keyOrder)
                                    {
                                        dh[key.name] = ElementDataTypeUtil.GetElementDataTypeText(ElementDataTypeUtil.GetElementDataType(key.T)).ToUpper();

                                    }
                                    dh[Properties.Resources.ELEMENT_TYPE_COLUMN] = Properties.Resources.ELEMENT_TYPE_TYPE;
                                    dh["RT"] = string.Empty;
                                    dt.Rows.Add(dh);
                                    createTypeRow = false;
                                }

                                // add data row
                                int index = 0;
                                string ctmModifiedCol = string.Empty;
                                foreach (ElementInfo key in keyOrder)
                                {
                                    if (rec.EL.ContainsKey(key.id))
                                    {
                                        ProgramMission.Ctms.ELcls elem = rec.EL[key.id];
                                        dr[key.name] = elem.V;
                                    }
                                    else
                                    {
                                        if (string.IsNullOrEmpty(ctmModifiedCol)) {
                                            ctmModifiedCol += index.ToString();
                                        } else {
                                            ctmModifiedCol += "," + index.ToString();
                                        }
                                    }
                                    index++;
                                }
                                dr[Properties.Resources.ELEMENT_TYPE_COLUMN] = string.IsNullOrEmpty(ctmModifiedCol) ?
                                    Properties.Resources.ELEMENT_TYPE_DATA :
                                    ctmModifiedCol;
                                dr["RT"] = rec.RT;

                                // add data row of selected
                                if (rtHash != null)
                                {
                                    if (rtHash.Contains(Convert.ToInt64(rec.RT)))
                                    {
                                        dt.Rows.Add(dr);
                                    }
                                }
                                else
                                {
                                    dt.Rows.Add(dr);
                                }
                            } while (iterator.MoveNext());
                        }
                    }
                }
            });
            await task;

            return "folderPath";
        }

        public async Task SetCtmDirectResultAsync(string elementId, long rt,string editedValue,string ctmId)
        {
            CancellationTokenSource downloadCts = new CancellationTokenSource();
            CancellationToken cToken = downloadCts.Token;

            HttpClient client = CTMRefineUIUtil.getProxyHttpClient(_UseProxy, _ProxyURI);

            // CTMを検索する為にPOSTするJSONを生成
            JToken postToken = JToken.Parse("{}");
            postToken["ctmId"] = ctmId;
            postToken["rt"] = rt;
            postToken["elementId"] = elementId;
            postToken["value"] = editedValue;

            string url = string.Format("{0}{1}?testing={2}", string.Format(CTMRefineUIConf.BaseUrl, _ipAddr, _portNo), "mib/ctm/mgmt/updateElement", CTMRefineUIConf.Config.Testing);
            var req = new HttpRequestMessage(HttpMethod.Post, url);
            req.Content = new StringContent(postToken.ToString());

            var task = client.SendAsync(req, HttpCompletionOption.ResponseHeadersRead, cToken).ContinueWith(response0 =>
            {
                if (cToken.IsCancellationRequested)
                {
                    throw new TaskCanceledException();
                }
            });
            await task;
        }

        public async Task<Dictionary<string, string>> GetCtmCatalogName()
        {
            CancellationTokenSource downloadCts = new CancellationTokenSource();
            CancellationToken cToken = downloadCts.Token;
            string url = string.Format("{0}{1}", string.Format(CTMRefineUIConf.BaseUrl, _ipAddr, _portNo), "refDict");

            HttpClient client = CTMRefineUIUtil.getProxyHttpClient(_UseProxy, _ProxyURI);
            var req = new HttpRequestMessage(HttpMethod.Get, url);
            var task = client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead).ContinueWith(response0 =>
            {
                var response = response0.Result;
                if (!response.IsSuccessStatusCode)
                {
                    //failed
                }
                else
                {
                    var stream = response.Content.ReadAsStreamAsync().Result;
                    var sr = new StreamReader(stream);
                    String strBody = sr.ReadToEnd();
                    sr.Close();
                    //再帰関数：CTMリストを取得、_ctmInfoにセットする
                    ParseElements(JToken.Parse(strBody)["children"]);
                }
            });
            await task;

            return _ctmInfo;
        }

        // Wang Issue NO.691 2018/05/11 Start
        // 多言語対応CTMの場合は設定した言語（存在しなければ一番目）を表示するように修正
        private void SetCtmInfo(JToken parentDisplayName, JToken ctmDisplayName, string id)
        {
            string ctmParent = string.Empty;
            if (parentDisplayName != null)
            {
                ctmParent = parentDisplayName["text"].ToString();
            }

            if (ctmDisplayName != null)
            {
                // _ctmInfo.Add(ctmParent + " - " + ctmDisplayName["text"].ToString(), id);
                _ctmInfo[ctmParent + " - " + ctmDisplayName["text"].ToString()] = id;

            }
        }
        // Wang Issue NO.691 2018/05/11 End

        private void ParseElements(JToken children)
        {
            foreach (JToken token in children)
            {
                var type = token["type"];
                if (type != null)
                {
                    if (type.ToString() == NodeType.Ctm)
                    {
                        // Wang Issue NO.691 2018/05/11 Start
                        // 多言語対応CTMの場合は設定した言語（存在しなければ一番目）を表示するように修正
                        //string ctmParent = "";
                        //foreach (var parent in children.Parent.Parent["displayName"])
                        //{
                        //    if ("ja".Equals(parent["lang"].ToString()))
                        //    {
                        //        ctmParent = parent["text"].ToString();
                        //    }
                        //}

                        //foreach (var test in token["displayName"])
                        //{
                        //    if ("ja".Equals(test["lang"].ToString()))
                        //    {
                        //        _ctmInfo.Add(ctmParent + " - " + test["text"].ToString(), token["id"].ToString());
                        //    }
                        //}
                        JToken parentDisplayName = null;
						try
						{
							if (children.Parent.Parent["displayName"] != null)
							{
                        foreach (var parent in children.Parent.Parent["displayName"])
                        {
                            if (parentDisplayName == null)
                            {
                                parentDisplayName = parent;
                            }

                            if (CTMRefineUIConf.Config.Language.Equals(parent["lang"].ToString()))
                            {
                                parentDisplayName = parent;
                                break;
                            }
                        }

							}
							else
							{
                                parentDisplayName = JToken.Parse("{\"lang\":\"ja\",\"text\":\"Space\"}");

							}

						}
						catch (Exception ex)
						{
							System.Windows.MessageBox.Show(children.Parent.Parent.ToString());
							throw ex;
						}

                        JToken ctmDisplayName = null;
                        foreach (var test in token["displayName"])
                        {
                            if (ctmDisplayName == null)
                            {
                                ctmDisplayName = test;
                            }

                            if (CTMRefineUIConf.Config.Language.Equals(test["lang"].ToString()))
                            {
                                ctmDisplayName = test;
                            }
                        }
                        SetCtmInfo(parentDisplayName, ctmDisplayName, token["id"].ToString());
                        // Wang Issue NO.691 2018/05/11 End
                    }
                    else
                    {
                        if(token["children"] != null)
                        {
                            ParseElements(token["children"]);
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
        }
    }
}
