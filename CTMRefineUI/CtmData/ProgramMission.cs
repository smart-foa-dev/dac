﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Win32;

namespace CTMRefineUI.CtmData
{
    /// <summary>
    /// ミッション投入で取得したJsonデータの処理クラス
    /// メンバーはgetJsonDataメソッドでのみ変更可能
    /// </summary>
    [DataContract]
    public class ProgramMission
    {
        /// <summary>
        /// CTMのID
        /// </summary>
        [DataMember]
        public string id { get; set; }

        /// <summary>
        /// CTM名称
        /// </summary>
        [DataMember]
        public string name { get; set; }

        /// <summary>
        /// CTMのコレクション数
        /// </summary>
        [DataMember]
        public int num { get; set; }

        /// <summary>
        /// CTMの実績データ
        /// </summary>
        [DataMember]
        public List<Ctms> ctms { get; private set; }

        public void SetCtms(List<Ctms> ctms) { this.ctms = ctms; }

        /// <summary>
        /// ミッションで取得したCTM
        /// </summary>
        [DataContract]
        public class Ctms
        {
            /// <summary>
            /// エレメントのリスト
            /// </summary>
            [DataMember]
            //public List<ELcls> EL { get; private set; }
            public Dictionary<string, ELcls> EL { get; private set; }
            /// <summary>
            /// CTMが生成された時刻
            /// </summary>
            [DataMember]
            public long RT { get; private set; }

            public void SetEL(Dictionary<string, ELcls> el) { this.EL = el; }
            public void SetRT(long rt) { this.RT = rt; }

            // Type created for JSON at <<root>> --> EL
            /// <summary>
            /// エレメントの内容
            /// </summary>
            [DataContract]
            public class ELcls
            {
                ///// <summary>
                ///// エレメントID
                ///// </summary>
                //[DataMember]
                //public string ID { get; private set; }

                /// <summary>
                /// エレメントの型
                /// </summary>
                [DataMember]
                public string T { get; private set; }

                /// <summary>
                /// エレメントの値
                /// </summary>
                [DataMember]
                public string V { get; set; }

                /// <summary>
                /// エレメントのバルキーリンク
                /// </summary>
                [DataMember]
                public string FN { get; private set; }

                public void SetT(string t) { this.T = t; }
                public void SetFN(string fn) { this.FN = fn; }
            }
        }
    }
}
