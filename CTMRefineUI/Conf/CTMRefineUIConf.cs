﻿using CTMRefineUI.Properties;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CTMRefineUI.Conf
{
    public class CTMRefineUIConf
    {
        public const string BaseUrl = "http://{0}:{1}/cms/rest/";
        public static Conf Config { get; set; }

        /// <summary>
        /// Conf/Ais.conf 読み込み
        /// </summary>
        public static void LoadCTMRefineUIConf()
        {
			try
			{
				var baseDir = AppDomain.CurrentDomain.BaseDirectory;
				var confPath = System.IO.Path.Combine(baseDir, @Settings.Default.ConfFile);
				// System.Windows.MessageBox.Show(confPath);

				using (StreamReader sr = new StreamReader(confPath))
				{
					string rte = sr.ReadToEnd().Replace("\\", "\\\\");
					Config = JsonConvert.DeserializeObject<Conf>(rte);
				}
			}
			catch (Exception ex)
			{
				System.Windows.MessageBox.Show("設定ファイルの読み込みに失敗しました。");
				throw ex;
			}
        }

        public class Conf
        {
            public string DefaultCtm = string.Empty;
            public string CmsHost = "192.192.192.131";
            public string CmsPort = "60000";
            public bool UseProxy = false;
            public string ProxyURI;
            public int Testing = 0;
            public string Title = "設備異常要因登録";
            public Dictionary<string,CtmConf> CtmIdAndElements = new Dictionary<string,CtmConf>();
            public Dictionary<string,Dictionary<string, List<string>>> Ctm = new Dictionary<string,Dictionary<string,List<string>>>();
			public int MaxData = EntryToCTM.MAX_DATA;
            // Wang Issue NO.691 2018/05/11 Start
            // 多言語対応CTMの場合は設定した言語（存在しなければ一番目）を表示するように修正
            public string Language = "ja";
            // Wang Issue NO.691 2018/05/11 End
        }

        public class CtmConf
        {
            public List<string> DisplayElement = new List<string>();
            public List<string> EditElement = new List<string>();
        }

        public class CTMRefineUICtmParam
        {
            public long MinRT;
            public long MaxRT;
            public HashSet<long> RTs;
        };
    }
}
