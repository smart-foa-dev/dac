﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CTMRefineUI.Conf;
using System.Net;
using System.Net.Http;

namespace CTMRefineUI.Util
{
    class CTMRefineUIUtil
    {

        public static HttpClient getProxyHttpClient(bool UseProxy, string ProxyURI)
        {
            HttpClient client = null;

            if (UseProxy)
            {
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.Proxy = new WebProxy("http://" + ProxyURI);
                clientHandler.UseProxy = true;
                client = new HttpClient(clientHandler);
            }
            else
            {
                HttpClientHandler clientHandler = new HttpClientHandler();
                clientHandler.UseProxy = false;
                client = new HttpClient(clientHandler);
            }

            client.Timeout = TimeSpan.FromSeconds(100);

            return client;

        }
    }
}
