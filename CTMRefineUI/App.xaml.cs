﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Diagnostics;
using System.Threading;

namespace CTMRefineUI
{
    /// <summary>
    /// App.xaml の相互作用ロジック
    /// </summary>
    public partial class App : Application
    {
        public static readonly string ARG_OPTION_HOST = "-h";
        public static readonly string ARG_OPTION_PORT = "-p";
        public static readonly string ARG_OPTION_CTM = "-ctm";
        public static readonly string ARG_OPTION_PROXY = "-proxy";
        public static readonly string ARG_OPTION_URI = "-uri";
        public static readonly string ARG_OPTION_SPECIFY = "-specify";

        private static string thisId = "160B4740-EC02-497C-88AD-4775FF9BC23F";

        public static Mutex mutex = new Mutex(false, "Global\\" + thisId);

        private HashSet<string> _options = new HashSet<string>() {ARG_OPTION_HOST, ARG_OPTION_PORT, ARG_OPTION_CTM, ARG_OPTION_PROXY, ARG_OPTION_URI, ARG_OPTION_SPECIFY};

        public Dictionary<string, string> _args = new Dictionary<string, string>();

        protected override void OnStartup(StartupEventArgs e)
        {
            try
            {
				System.IO.File.AppendAllText(System.AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + "\\" + "Log.txt",
					DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\t" + this.GetType().Name + "\t" + "ARGS: [" + string.Join(",", e.Args) + "]" + Environment.NewLine);
				Conf.CTMRefineUIConf.LoadCTMRefineUIConf();

                string currentOpt = string.Empty;

                for (int i=0; i<e.Args.Length; i++)
                {
                    if (_options.Contains(e.Args[i]))
                    {
                        currentOpt = e.Args[i];
                    }
                    else if (!string.IsNullOrEmpty(currentOpt) && !_args.ContainsKey(currentOpt))
                    {
                        _args.Add(currentOpt, e.Args[i]);
                        currentOpt = string.Empty;
                    }
                }

                Conf.CTMRefineUIConf.Config.DefaultCtm = _args.ContainsKey(ARG_OPTION_CTM) ? _args[ARG_OPTION_CTM] : Conf.CTMRefineUIConf.Config.DefaultCtm;
                Conf.CTMRefineUIConf.Config.CmsHost = _args.ContainsKey(ARG_OPTION_HOST) ? _args[ARG_OPTION_HOST] : Conf.CTMRefineUIConf.Config.CmsHost;
                Conf.CTMRefineUIConf.Config.CmsPort = _args.ContainsKey(ARG_OPTION_PORT) ? _args[ARG_OPTION_PORT] : Conf.CTMRefineUIConf.Config.CmsPort;
                Conf.CTMRefineUIConf.Config.UseProxy = _args.ContainsKey(ARG_OPTION_PROXY) ? Boolean.Parse(_args[ARG_OPTION_PROXY]) : Conf.CTMRefineUIConf.Config.UseProxy;
                Conf.CTMRefineUIConf.Config.ProxyURI = _args.ContainsKey(ARG_OPTION_URI) ? _args[ARG_OPTION_URI] : Conf.CTMRefineUIConf.Config.ProxyURI;
            }
            catch (Exception ex)
            {
				System.IO.File.AppendAllText(System.AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + "\\" + "Log.txt",
					DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "\t" + this.GetType().Name + "\t" + "ERROR: " + ex.GetType().FullName + "\t" + ex.Message + Environment.NewLine + ex.StackTrace + Environment.NewLine);
				System.Windows.MessageBox.Show("アプリケーション起動に失敗しました。");
				this.Shutdown();
            }
        }
    }
}
